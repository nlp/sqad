<s>
Mučení	mučení	k1gNnSc1	mučení
(	(	kIx(	(
<g/>
též	též	k9	též
tortura	tortura	k1gFnSc1	tortura
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
torquere	torqurat	k5eAaPmIp3nS	torqurat
–	–	k?	–
torze	torze	k1gFnSc1	torze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
protiprávní	protiprávní	k2eAgNnSc4d1	protiprávní
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
úmyslně	úmyslně	k6eAd1	úmyslně
způsobena	způsobit	k5eAaPmNgFnS	způsobit
silná	silný	k2eAgFnSc1d1	silná
bolest	bolest	k1gFnSc1	bolest
či	či	k8xC	či
tělesné	tělesný	k2eAgNnSc1d1	tělesné
nebo	nebo	k8xC	nebo
duševní	duševní	k2eAgNnSc1d1	duševní
utrpení	utrpení	k1gNnSc1	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
živočichy	živočich	k1gMnPc4	živočich
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
týrání	týrání	k1gNnPc2	týrání
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
hrubého	hrubý	k2eAgMnSc2d1	hrubý
nebo	nebo	k8xC	nebo
ponižujícího	ponižující	k2eAgNnSc2d1	ponižující
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přímo	přímo	k6eAd1	přímo
nespadají	spadat	k5eNaImIp3nP	spadat
pod	pod	k7c4	pod
mučení	mučení	k1gNnSc4	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgNnSc7	takový
jednáním	jednání	k1gNnSc7	jednání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
a	a	k8xC	a
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
jednání	jednání	k1gNnSc1	jednání
uskutečňující	uskutečňující	k2eAgNnSc1d1	uskutečňující
se	se	k3xPyFc4	se
při	při	k7c6	při
znásilnění	znásilnění	k1gNnSc6	znásilnění
<g/>
,	,	kIx,	,
sexuálním	sexuální	k2eAgNnSc6d1	sexuální
zneužívání	zneužívání	k1gNnSc6	zneužívání
a	a	k8xC	a
ponižování	ponižování	k1gNnSc6	ponižování
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
psychiku	psychika	k1gFnSc4	psychika
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pro	pro	k7c4	pro
mučení	mučení	k1gNnSc4	mučení
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jednorázový	jednorázový	k2eAgInSc4d1	jednorázový
akt	akt	k1gInSc4	akt
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
systematičnost	systematičnost	k1gFnSc4	systematičnost
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
útoky	útok	k1gInPc1	útok
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
jiné	jiný	k2eAgNnSc4d1	jiné
nelidské	lidský	k2eNgNnSc4d1	nelidské
a	a	k8xC	a
kruté	krutý	k2eAgNnSc4d1	kruté
zacházení	zacházení	k1gNnSc4	zacházení
zahrnout	zahrnout	k5eAaPmF	zahrnout
i	i	k9	i
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
akty	akt	k1gInPc4	akt
působící	působící	k2eAgInPc4d1	působící
méně	málo	k6eAd2	málo
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
tělesné	tělesný	k2eAgInPc4d1	tělesný
nebo	nebo	k8xC	nebo
duševní	duševní	k2eAgNnSc4d1	duševní
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
na	na	k7c4	na
oběť	oběť	k1gFnSc4	oběť
vystupňováno	vystupňován	k2eAgNnSc1d1	vystupňováno
delším	dlouhý	k2eAgMnSc7d2	delší
či	či	k8xC	či
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
působením	působení	k1gNnSc7	působení
<g/>
.	.	kIx.	.
</s>
<s>
Objektem	objekt	k1gInSc7	objekt
je	být	k5eAaImIp3nS	být
nedotknutelnost	nedotknutelnost	k1gFnSc1	nedotknutelnost
osoby	osoba	k1gFnSc2	osoba
zejména	zejména	k9	zejména
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jejího	její	k3xOp3gNnSc2	její
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
mučení	mučení	k1gNnSc6	mučení
vždy	vždy	k6eAd1	vždy
ohrožovány	ohrožován	k2eAgInPc1d1	ohrožován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
její	její	k3xOp3gFnSc1	její
lidská	lidský	k2eAgFnSc1d1	lidská
důstojnost	důstojnost	k1gFnSc1	důstojnost
<g/>
.	.	kIx.	.
</s>
<s>
Mučení	mučení	k1gNnSc1	mučení
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
prováděno	provádět	k5eAaImNgNnS	provádět
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
<g/>
:	:	kIx,	:
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
nebo	nebo	k8xC	nebo
od	od	k7c2	od
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
informace	informace	k1gFnSc2	informace
nebo	nebo	k8xC	nebo
přiznání	přiznání	k1gNnSc2	přiznání
potrestat	potrestat	k5eAaPmF	potrestat
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
on	on	k3xPp3gMnSc1	on
nebo	nebo	k8xC	nebo
třetí	třetí	k4xOgFnSc1	třetí
osoba	osoba	k1gFnSc1	osoba
nebo	nebo	k8xC	nebo
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
podezřelí	podezřelý	k2eAgMnPc1d1	podezřelý
zastrašit	zastrašit	k5eAaPmF	zastrašit
nebo	nebo	k8xC	nebo
přinutit	přinutit	k5eAaPmF	přinutit
jej	on	k3xPp3gMnSc4	on
nebo	nebo	k8xC	nebo
třetí	třetí	k4xOgFnSc4	třetí
osobu	osoba	k1gFnSc4	osoba
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c4	na
diskriminaci	diskriminace	k1gFnSc4	diskriminace
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
druhu	druh	k1gInSc2	druh
Toto	tento	k3xDgNnSc1	tento
vymezení	vymezení	k1gNnSc1	vymezení
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
bolest	bolest	k1gFnSc4	bolest
nebo	nebo	k8xC	nebo
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zákonných	zákonný	k2eAgFnPc2d1	zákonná
sankcí	sankce	k1gFnPc2	sankce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
sankcí	sankce	k1gFnPc2	sankce
neoddělitelné	oddělitelný	k2eNgFnSc2d1	neoddělitelná
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
vyvolány	vyvolat	k5eAaPmNgFnP	vyvolat
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
indiánském	indiánský	k2eAgInSc6d1	indiánský
rituálu	rituál	k1gInSc6	rituál
Tanec	tanec	k1gInSc1	tanec
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
sloužilo	sloužit	k5eAaImAgNnS	sloužit
mučení	mučení	k1gNnSc1	mučení
jako	jako	k8xS	jako
zkouška	zkouška	k1gFnSc1	zkouška
statečnosti	statečnost	k1gFnSc2	statečnost
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc2	schopnost
snášet	snášet	k5eAaImF	snášet
bolest	bolest	k1gFnSc4	bolest
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobného	podobný	k2eAgInSc2d1	podobný
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc4	některý
formy	forma	k1gFnPc4	forma
mučení	mučení	k1gNnPc2	mučení
stávaly	stávat	k5eAaImAgFnP	stávat
součástí	součást	k1gFnSc7	součást
iniciace	iniciace	k1gFnSc1	iniciace
mladíků	mladík	k1gMnPc2	mladík
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
tradičních	tradiční	k2eAgFnPc2d1	tradiční
kultur	kultura	k1gFnPc2	kultura
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
i	i	k8xC	i
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Trhání	trhání	k1gNnSc1	trhání
masa	maso	k1gNnSc2	maso
Oslí	oslí	k2eAgInSc4d1	oslí
hřbet	hřbet	k1gInSc4	hřbet
Waterboarding	Waterboarding	k1gInSc1	Waterboarding
Skřipec	skřipec	k1gInSc1	skřipec
Železná	železný	k2eAgFnSc1d1	železná
panna	panna	k1gFnSc1	panna
Kovová	kovový	k2eAgFnSc1d1	kovová
klec	klec	k1gFnSc1	klec
Upálení	upálení	k1gNnSc2	upálení
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
býk	býk	k1gMnSc1	býk
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
ozvali	ozvat	k5eAaPmAgMnP	ozvat
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Friedrich	Friedrich	k1gMnSc1	Friedrich
Spee	Spe	k1gFnSc2	Spe
a	a	k8xC	a
Anton	Anton	k1gMnSc1	Anton
Praetorius	Praetorius	k1gMnSc1	Praetorius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1613	[number]	k4	1613
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
vězňů	vězeň	k1gMnPc2	vězeň
v	v	k7c6	v
žalářích	žalář	k1gInPc6	žalář
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Gründlicher	Gründlichra	k1gFnPc2	Gründlichra
Bericht	Bericht	k2eAgInSc1d1	Bericht
Von	von	k1gInSc1	von
Zauberey	Zauberea	k1gFnSc2	Zauberea
und	und	k?	und
Zauberern	Zauberern	k1gInSc1	Zauberern
(	(	kIx(	(
<g/>
Vzevrubná	Vzevrubný	k2eAgFnSc1d1	Vzevrubný
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
čarodějnictví	čarodějnictví	k1gNnSc6	čarodějnictví
a	a	k8xC	a
čarodějích	čaroděj	k1gMnPc6	čaroděj
<g/>
/	/	kIx~	/
<g/>
nicích	nicích	k?	nicích
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgMnS	napsat
generálmajoru	generálmajor	k1gMnSc3	generálmajor
Berthiérovi	Berthiér	k1gMnSc3	Berthiér
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgInSc1	tento
barbarský	barbarský	k2eAgInSc1d1	barbarský
zvyk	zvyk	k1gInSc1	zvyk
bičování	bičování	k1gNnSc2	bičování
lidí	člověk	k1gMnPc2	člověk
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
důležitá	důležitý	k2eAgNnPc4d1	důležité
tajemství	tajemství	k1gNnPc4	tajemství
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
vyslýchání	vyslýchání	k1gNnSc2	vyslýchání
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
uvrhnutí	uvrhnutí	k1gNnSc2	uvrhnutí
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
mučidel	mučidlo	k1gNnPc2	mučidlo
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
užitku	užitek	k1gInSc3	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
ubožáci	ubožák	k1gMnPc1	ubožák
řeknou	říct	k5eAaPmIp3nP	říct
<g/>
,	,	kIx,	,
cokoli	cokoli	k3yInSc4	cokoli
je	on	k3xPp3gNnSc4	on
napadne	napadnout	k5eAaPmIp3nS	napadnout
a	a	k8xC	a
cokoli	cokoli	k3yInSc1	cokoli
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gMnPc1	jejich
trýznitelé	trýznitel	k1gMnPc1	trýznitel
<g/>
)	)	kIx)	)
chtějí	chtít	k5eAaImIp3nP	chtít
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
velitel	velitel	k1gMnSc1	velitel
zapovídá	zapovídat	k5eAaImIp3nS	zapovídat
použití	použití	k1gNnSc3	použití
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
lidskostí	lidskost	k1gFnSc7	lidskost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
papežské	papežský	k2eAgFnSc2d1	Papežská
buly	bula	k1gFnSc2	bula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
bylo	být	k5eAaImAgNnS	být
mučení	mučení	k1gNnSc1	mučení
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
v	v	k7c6	v
období	období	k1gNnSc6	období
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zákaz	zákaz	k1gInSc1	zákaz
mučení	mučení	k1gNnPc2	mučení
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
úmluvách	úmluva	k1gFnPc6	úmluva
a	a	k8xC	a
paktech	pakt	k1gInPc6	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgFnPc4	žádný
výjimky	výjimka	k1gFnPc4	výjimka
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mučení	mučení	k1gNnSc1	mučení
mohlo	moct	k5eAaImAgNnS	moct
nastat	nastat	k5eAaPmF	nastat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
právem	právem	k6eAd1	právem
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
kogentní	kogentní	k2eAgFnSc4d1	kogentní
normu	norma	k1gFnSc4	norma
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
–	–	k?	–
ius	ius	k?	ius
cogens	cogens	k1gInSc1	cogens
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
norma	norma	k1gFnSc1	norma
se	se	k3xPyFc4	se
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
a	a	k8xC	a
nezrušitelná	zrušitelný	k2eNgFnSc1d1	nezrušitelná
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc4d1	hlavní
dokumenty	dokument	k1gInPc4	dokument
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
mučení	mučení	k1gNnSc1	mučení
je	být	k5eAaImIp3nS	být
Úmluva	úmluva	k1gFnSc1	úmluva
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
a	a	k8xC	a
jinému	jiné	k1gNnSc3	jiné
krutému	krutý	k2eAgNnSc3d1	kruté
<g/>
,	,	kIx,	,
nelidskému	lidský	k2eNgNnSc3d1	nelidské
či	či	k8xC	či
ponižujícímu	ponižující	k2eAgNnSc3d1	ponižující
zacházení	zacházení	k1gNnSc3	zacházení
nebo	nebo	k8xC	nebo
trestání	trestání	k1gNnSc3	trestání
a	a	k8xC	a
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Úmluva	úmluva	k1gFnSc1	úmluva
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
a	a	k8xC	a
jinému	jiné	k1gNnSc3	jiné
krutému	krutý	k2eAgNnSc3d1	kruté
<g/>
,	,	kIx,	,
nelidskému	lidský	k2eNgNnSc3d1	nelidské
či	či	k8xC	či
ponižujícímu	ponižující	k2eAgNnSc3d1	ponižující
zacházení	zacházení	k1gNnSc3	zacházení
nebo	nebo	k8xC	nebo
trestání	trestání	k1gNnSc3	trestání
<g/>
.	.	kIx.	.
</s>
<s>
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
OSN	OSN	kA	OSN
přijalo	přijmout	k5eAaPmAgNnS	přijmout
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
Deklaraci	deklarace	k1gFnSc4	deklarace
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
všech	všecek	k3xTgFnPc2	všecek
osob	osoba	k1gFnPc2	osoba
před	před	k7c7	před
mučením	mučení	k1gNnSc7	mučení
a	a	k8xC	a
jiným	jiný	k2eAgNnSc7d1	jiné
krutým	krutý	k2eAgNnSc7d1	kruté
nelidským	lidský	k2eNgNnSc7d1	nelidské
či	či	k8xC	či
ponižujícím	ponižující	k2eAgNnSc7d1	ponižující
zacházením	zacházení	k1gNnSc7	zacházení
nebo	nebo	k8xC	nebo
trestáním	trestání	k1gNnSc7	trestání
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
navázalo	navázat	k5eAaPmAgNnS	navázat
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
sjednání	sjednání	k1gNnSc4	sjednání
Úmluvy	úmluva	k1gFnSc2	úmluva
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
a	a	k8xC	a
jinému	jiné	k1gNnSc3	jiné
krutému	krutý	k2eAgNnSc3d1	kruté
<g/>
,	,	kIx,	,
nelidskému	lidský	k2eNgNnSc3d1	nelidské
či	či	k8xC	či
ponižujícímu	ponižující	k2eAgNnSc3d1	ponižující
zacházení	zacházení	k1gNnSc3	zacházení
nebo	nebo	k8xC	nebo
trestání	trestání	k1gNnSc3	trestání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
mučení	mučení	k1gNnSc1	mučení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
3	[number]	k4	3
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
navázala	navázat	k5eAaPmAgFnS	navázat
Evropská	evropský	k2eAgFnSc1d1	Evropská
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c4	o
zabránění	zabránění	k1gNnSc4	zabránění
mučení	mučení	k1gNnPc2	mučení
a	a	k8xC	a
nelidskému	lidský	k2eNgNnSc3d1	nelidské
či	či	k8xC	či
ponižujícímu	ponižující	k2eAgNnSc3d1	ponižující
zacházení	zacházení	k1gNnSc3	zacházení
nebo	nebo	k8xC	nebo
trestání	trestání	k1gNnSc3	trestání
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
mučení	mučení	k1gNnSc4	mučení
zamezit	zamezit	k5eAaPmF	zamezit
<g/>
,	,	kIx,	,
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
i	i	k9	i
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mučení	mučení	k1gNnSc4	mučení
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
<g/>
:	:	kIx,	:
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
5	[number]	k4	5
<g/>
)	)	kIx)	)
Ženevská	ženevský	k2eAgFnSc1d1	Ženevská
úmluva	úmluva	k1gFnSc1	úmluva
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
obětí	oběť	k1gFnPc2	oběť
války	válka	k1gFnSc2	válka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
5	[number]	k4	5
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
pakt	pakt	k1gInSc1	pakt
o	o	k7c4	o
občanských	občanský	k2eAgNnPc2d1	občanské
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
<g />
.	.	kIx.	.
</s>
<s>
právech	právo	k1gNnPc6	právo
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
7	[number]	k4	7
+	+	kIx~	+
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
lékařskými	lékařský	k2eAgInPc7d1	lékařský
experimenty	experiment	k1gInPc7	experiment
Evropská	evropský	k2eAgFnSc1d1	Evropská
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čl	čl	kA	čl
<g/>
.	.	kIx.	.
3	[number]	k4	3
Úmluva	úmluva	k1gFnSc1	úmluva
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
a	a	k8xC	a
jinému	jiné	k1gNnSc3	jiné
krutému	krutý	k2eAgNnSc3d1	kruté
<g/>
,	,	kIx,	,
nelidskému	lidský	k2eNgNnSc3d1	nelidské
či	či	k8xC	či
ponižujícímu	ponižující	k2eAgNnSc3d1	ponižující
zacházení	zacházení	k1gNnSc3	zacházení
nebo	nebo	k8xC	nebo
trestání	trestání	k1gNnSc4	trestání
Tato	tento	k3xDgFnSc1	tento
úmluva	úmluva	k1gFnSc1	úmluva
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
výbor	výbor	k1gInSc1	výbor
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
přezkum	přezkum	k1gInSc1	přezkum
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
podvýbor	podvýbor	k1gInSc1	podvýbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
dodržování	dodržování	k1gNnSc4	dodržování
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
toto	tento	k3xDgNnSc4	tento
ještě	ještě	k9	ještě
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
o	o	k7c4	o
Speciální	speciální	k2eAgInSc4d1	speciální
zpravodaj	zpravodaj	k1gInSc4	zpravodaj
pro	pro	k7c4	pro
mučení	mučení	k1gNnSc4	mučení
a	a	k8xC	a
jiné	jiné	k1gNnSc4	jiné
kruté	krutý	k2eAgNnSc4d1	kruté
<g/>
,	,	kIx,	,
nelidské	lidský	k2eNgNnSc4d1	nelidské
nebo	nebo	k8xC	nebo
ponižující	ponižující	k2eAgNnSc4d1	ponižující
zacházení	zacházení	k1gNnSc4	zacházení
nebo	nebo	k8xC	nebo
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
čl	čl	kA	čl
<g/>
.	.	kIx.	.
7	[number]	k4	7
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Listiny	listina	k1gFnSc2	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
:	:	kIx,	:
Nedotknutelnost	nedotknutelnost	k1gFnSc1	nedotknutelnost
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
soukromí	soukromí	k1gNnSc2	soukromí
je	být	k5eAaImIp3nS	být
zaručena	zaručen	k2eAgFnSc1d1	zaručena
<g/>
.	.	kIx.	.
</s>
<s>
Omezena	omezen	k2eAgFnSc1d1	omezena
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
v	v	k7c6	v
případech	případ	k1gInPc6	případ
stanovených	stanovený	k2eAgMnPc2d1	stanovený
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
mučen	mučit	k5eAaImNgInS	mučit
ani	ani	k8xC	ani
podroben	podrobit	k5eAaPmNgInS	podrobit
krutému	krutý	k2eAgNnSc3d1	kruté
<g/>
,	,	kIx,	,
nelidskému	lidský	k2eNgNnSc3d1	nelidské
nebo	nebo	k8xC	nebo
ponižujícímu	ponižující	k2eAgNnSc3d1	ponižující
zacházení	zacházení	k1gNnSc3	zacházení
nebo	nebo	k8xC	nebo
trestu	trest	k1gInSc3	trest
<g/>
.	.	kIx.	.
§	§	k?	§
149	[number]	k4	149
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
:	:	kIx,	:
Za	za	k7c4	za
mučení	mučení	k1gNnSc4	mučení
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
nelidské	lidský	k2eNgNnSc4d1	nelidské
a	a	k8xC	a
kruté	krutý	k2eAgNnSc4d1	kruté
zacházení	zacházení	k1gNnSc4	zacházení
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc1	takový
to	ten	k3xDgNnSc1	ten
tresty	trest	k1gInPc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
trestem	trest	k1gInSc7	trest
je	být	k5eAaImIp3nS	být
odnětí	odnětí	k1gNnSc1	odnětí
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
až	až	k9	až
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
je	být	k5eAaImIp3nS	být
udělen	udělen	k2eAgInSc1d1	udělen
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
mučením	mučení	k1gNnSc7	mučení
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
nelidským	lidský	k2eNgNnSc7d1	nelidské
či	či	k8xC	či
krutým	krutý	k2eAgNnSc7d1	kruté
zacházením	zacházení	k1gNnSc7	zacházení
způsobí	způsobit	k5eAaPmIp3nS	způsobit
člověku	člověk	k1gMnSc3	člověk
tělesné	tělesný	k2eAgFnSc2d1	tělesná
či	či	k8xC	či
duševní	duševní	k2eAgNnSc4d1	duševní
utrpení	utrpení	k1gNnSc4	utrpení
Druhý	druhý	k4xOgInSc1	druhý
trest	trest	k1gInSc1	trest
je	být	k5eAaImIp3nS	být
odnětí	odnětí	k1gNnSc4	odnětí
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
až	až	k8xS	až
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
je	být	k5eAaImIp3nS	být
udělen	udělen	k2eAgInSc1d1	udělen
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
spáchá	spáchat	k5eAaPmIp3nS	spáchat
tento	tento	k3xDgInSc4	tento
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
úředníka	úředník	k1gMnSc2	úředník
<g/>
,	,	kIx,	,
také	také	k9	také
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jej	on	k3xPp3gMnSc4	on
spáchá	spáchat	k5eAaPmIp3nS	spáchat
na	na	k7c6	na
osobách	osoba	k1gFnPc6	osoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
popisu	popis	k1gInSc2	popis
jejich	jejich	k3xOp3gFnSc2	jejich
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
spáchá	spáchat	k5eAaPmIp3nS	spáchat
tento	tento	k3xDgInSc4	tento
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
kvůli	kvůli	k7c3	kvůli
nějaké	nějaký	k3yIgFnSc3	nějaký
formě	forma	k1gFnSc3	forma
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
čin	čin	k1gInSc4	čin
spáchá	spáchat	k5eAaPmIp3nS	spáchat
opakovaně	opakovaně	k6eAd1	opakovaně
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jej	on	k3xPp3gMnSc4	on
spáchá	spáchat	k5eAaPmIp3nS	spáchat
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
dvěma	dva	k4xCgFnPc7	dva
dalšími	další	k2eAgFnPc7d1	další
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
trest	trest	k1gInSc1	trest
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
odnětí	odnětí	k1gNnSc4	odnětí
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
pět	pět	k4xCc4	pět
až	až	k9	až
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
udělen	udělen	k2eAgInSc1d1	udělen
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
spáchá	spáchat	k5eAaPmIp3nS	spáchat
tento	tento	k3xDgInSc4	tento
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
těhotné	těhotný	k2eAgFnSc2d1	těhotná
ženě	žena	k1gFnSc3	žena
nebo	nebo	k8xC	nebo
dítěti	dítě	k1gNnSc3	dítě
mladšímu	mladý	k2eAgNnSc3d2	mladší
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
trestána	trestat	k5eAaImNgFnS	trestat
je	být	k5eAaImIp3nS	být
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
obzvláště	obzvláště	k6eAd1	obzvláště
surovým	surový	k2eAgInSc7d1	surový
způsobem	způsob	k1gInSc7	způsob
anebo	anebo	k8xC	anebo
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
těžkou	těžký	k2eAgFnSc4d1	těžká
újmu	újma	k1gFnSc4	újma
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
řadě	řada	k1gFnSc6	řada
zmíním	zmínit	k5eAaPmIp1nS	zmínit
největší	veliký	k2eAgInSc4d3	veliký
trest	trest	k1gInSc4	trest
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
trest	trest	k1gInSc4	trest
na	na	k7c4	na
osm	osm	k4xCc4	osm
až	až	k9	až
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bude	být	k5eAaImBp3nS	být
udělen	udělen	k2eAgInSc1d1	udělen
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
svým	svůj	k3xOyFgInSc7	svůj
činem	čin	k1gInSc7	čin
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trestná	trestný	k2eAgFnSc1d1	trestná
je	být	k5eAaImIp3nS	být
i	i	k9	i
příprava	příprava	k1gFnSc1	příprava
takového	takový	k3xDgInSc2	takový
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Mučení	mučení	k1gNnSc1	mučení
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Úmluvy	úmluva	k1gFnSc2	úmluva
naopak	naopak	k6eAd1	naopak
nepovažuje	považovat	k5eNaImIp3nS	považovat
bolest	bolest	k1gFnSc4	bolest
nebo	nebo	k8xC	nebo
utrpení	utrpení	k1gNnSc4	utrpení
vznikající	vznikající	k2eAgFnSc2d1	vznikající
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zákonných	zákonný	k2eAgFnPc2d1	zákonná
sankcí	sankce	k1gFnPc2	sankce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
sankcí	sankce	k1gFnPc2	sankce
neoddělitelné	oddělitelný	k2eNgFnSc2d1	neoddělitelná
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
náhodně	náhodně	k6eAd1	náhodně
způsobeny	způsobit	k5eAaPmNgInP	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Mučení	mučení	k1gNnSc1	mučení
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
zásadně	zásadně	k6eAd1	zásadně
způsobilé	způsobilý	k2eAgInPc1d1	způsobilý
vyvolat	vyvolat	k5eAaPmF	vyvolat
tzv.	tzv.	kA	tzv.
mučivé	mučivý	k2eAgFnPc4d1	mučivá
útrapy	útrapa	k1gFnPc4	útrapa
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
tak	tak	k6eAd1	tak
poškozenému	poškozený	k1gMnSc3	poškozený
těžkou	těžký	k2eAgFnSc4d1	těžká
újmu	újma	k1gFnSc4	újma
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
silná	silný	k2eAgFnSc1d1	silná
bolest	bolest	k1gFnSc1	bolest
nebo	nebo	k8xC	nebo
tělesné	tělesný	k2eAgNnSc1d1	tělesné
či	či	k8xC	či
duševní	duševní	k2eAgNnSc1d1	duševní
utrpení	utrpení	k1gNnSc1	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Mučení	mučení	k1gNnSc1	mučení
způsobené	způsobený	k2eAgNnSc1d1	způsobené
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
charakter	charakter	k1gInSc4	charakter
mučivých	mučivý	k2eAgFnPc2d1	mučivá
útrap	útrapa	k1gFnPc2	útrapa
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
§	§	k?	§
122	[number]	k4	122
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
h	h	k?	h
<g/>
)	)	kIx)	)
TrZ	TrZ	k1gMnSc2	TrZ
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zároveň	zároveň	k6eAd1	zároveň
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vážnou	vážný	k2eAgFnSc4d1	vážná
poruchu	porucha	k1gFnSc4	porucha
zdraví	zdraví	k1gNnSc2	zdraví
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc4d1	jiné
vážné	vážný	k2eAgNnSc4d1	vážné
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
používali	používat	k5eAaImAgMnP	používat
waterboarding	waterboarding	k1gInSc4	waterboarding
na	na	k7c4	na
vietnamské	vietnamský	k2eAgMnPc4d1	vietnamský
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
od	od	k7c2	od
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc7d3	nejnovější
aplikací	aplikace	k1gFnSc7	aplikace
mučení	mučení	k1gNnSc2	mučení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
získávání	získávání	k1gNnSc1	získávání
důležitých	důležitý	k2eAgFnPc2d1	důležitá
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
v	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bushe	Bush	k1gMnSc2	Bush
schváleno	schválen	k2eAgNnSc1d1	schváleno
mučení	mučení	k1gNnSc1	mučení
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
bylo	být	k5eAaImAgNnS	být
prezentované	prezentovaný	k2eAgNnSc1d1	prezentované
pod	pod	k7c7	pod
eufemističtějším	eufemistický	k2eAgInSc7d2	eufemistický
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
techniky	technika	k1gFnSc2	technika
<g/>
"	"	kIx"	"
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ji	on	k3xPp3gFnSc4	on
sděleno	sdělen	k2eAgNnSc1d1	sděleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
techniky	technika	k1gFnPc1	technika
jsou	být	k5eAaImIp3nP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
zajatých	zajatá	k1gFnPc2	zajatá
teroristů	terorista	k1gMnPc2	terorista
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
z	z	k7c2	z
terorismu	terorismus	k1gInSc2	terorismus
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
bojovníků	bojovník	k1gMnPc2	bojovník
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
angažovala	angažovat	k5eAaBmAgFnS	angažovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
teroru	teror	k1gInSc3	teror
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
senátních	senátní	k2eAgFnPc2d1	senátní
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
<g/>
)	)	kIx)	)
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
rozsahu	rozsah	k1gInSc6	rozsah
a	a	k8xC	a
podobě	podoba	k1gFnSc6	podoba
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
mučení	mučení	k1gNnSc1	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Senátní	senátní	k2eAgInSc1d1	senátní
report	report	k1gInSc1	report
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
o	o	k7c6	o
mučení	mučení	k1gNnSc6	mučení
vedeným	vedený	k2eAgNnSc7d1	vedené
CIA	CIA	kA	CIA
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
účelem	účel	k1gInSc7	účel
těchto	tento	k3xDgFnPc2	tento
technik	technika	k1gFnPc2	technika
nebylo	být	k5eNaImAgNnS	být
získání	získání	k1gNnSc4	získání
hodnotných	hodnotný	k2eAgFnPc2d1	hodnotná
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byly	být	k5eAaImAgFnP	být
prezentovány	prezentovat	k5eAaBmNgFnP	prezentovat
americkým	americký	k2eAgMnPc3d1	americký
občanům	občan	k1gMnPc3	občan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
mučení	mučení	k1gNnSc1	mučení
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
tzv.	tzv.	kA	tzv.
black	blacka	k1gFnPc2	blacka
sites	sitesa	k1gFnPc2	sitesa
(	(	kIx(	(
<g/>
černých	černý	k2eAgFnPc2d1	černá
místech	místo	k1gNnPc6	místo
<g/>
)	)	kIx)	)
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mediálně	mediálně	k6eAd1	mediálně
známé	známý	k2eAgInPc4d1	známý
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
věznice	věznice	k1gFnSc2	věznice
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Guantánamo	Guantánama	k1gFnSc5	Guantánama
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
centrální	centrální	k2eAgFnSc1d1	centrální
bagdádská	bagdádský	k2eAgFnSc1d1	bagdádská
věznice	věznice	k1gFnSc1	věznice
Abú	abú	k1gMnSc1	abú
Ghrajb	Ghrajb	k1gMnSc1	Ghrajb
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
obzvláště	obzvláště	k6eAd1	obzvláště
nelidskému	lidský	k2eNgNnSc3d1	nelidské
ponižování	ponižování	k1gNnSc3	ponižování
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
k	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
i	i	k9	i
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
tamních	tamní	k2eAgFnPc2d1	tamní
iráckých	irácký	k2eAgFnPc2d1	irácká
policejních	policejní	k2eAgFnPc2d1	policejní
a	a	k8xC	a
armádních	armádní	k2eAgFnPc2d1	armádní
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
civilním	civilní	k2eAgNnSc6d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
–	–	k?	–
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
koaliční	koaliční	k2eAgFnSc1d1	koaliční
správa	správa	k1gFnSc1	správa
podepsala	podepsat	k5eAaPmAgFnS	podepsat
tzv.	tzv.	kA	tzv.
fragmentální	fragmentální	k2eAgInPc4d1	fragmentální
rozkazy	rozkaz	k1gInPc4	rozkaz
(	(	kIx(	(
<g/>
fragmentary	fragmentara	k1gFnPc4	fragmentara
orders	orders	k6eAd1	orders
<g/>
/	/	kIx~	/
<g/>
Frago	Frago	k6eAd1	Frago
242	[number]	k4	242
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nadále	nadále	k6eAd1	nadále
nedávaly	dávat	k5eNaImAgFnP	dávat
za	za	k7c4	za
povinnost	povinnost	k1gFnSc4	povinnost
hlásit	hlásit	k5eAaImF	hlásit
a	a	k8xC	a
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
jakákoliv	jakýkoliv	k3yIgNnPc1	jakýkoliv
násilí	násilí	k1gNnPc1	násilí
páchané	páchaný	k2eAgMnPc4d1	páchaný
proamerickými	proamerický	k2eAgFnPc7d1	proamerická
iráckými	irácký	k2eAgFnPc7d1	irácká
jednotkami	jednotka	k1gFnPc7	jednotka
na	na	k7c6	na
civilních	civilní	k2eAgFnPc6d1	civilní
osobách	osoba	k1gFnPc6	osoba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mučení	mučení	k1gNnSc2	mučení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
ohavných	ohavný	k2eAgInPc2d1	ohavný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
si	se	k3xPyFc3	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
od	od	k7c2	od
CIA	CIA	kA	CIA
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
"	"	kIx"	"
<g/>
pokročilých	pokročilý	k2eAgFnPc6d1	pokročilá
vyšetřovacích	vyšetřovací	k2eAgFnPc6d1	vyšetřovací
technikách	technika	k1gFnPc6	technika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zahrnujících	zahrnující	k2eAgNnPc2d1	zahrnující
bití	bití	k1gNnPc2	bití
<g/>
,	,	kIx,	,
hladovění	hladovění	k1gNnSc2	hladovění
<g/>
,	,	kIx,	,
spánkové	spánkový	k2eAgFnSc2d1	spánková
deprivace	deprivace	k1gFnSc2	deprivace
<g/>
,	,	kIx,	,
waterboardingu	waterboarding	k1gInSc2	waterboarding
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
metod	metoda	k1gFnPc2	metoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
fyzický	fyzický	k2eAgInSc4d1	fyzický
a	a	k8xC	a
psychologický	psychologický	k2eAgInSc4d1	psychologický
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
zajaté	zajatý	k2eAgMnPc4d1	zajatý
vězně	vězeň	k1gMnPc4	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zprávy	zpráva	k1gFnSc2	zpráva
mj.	mj.	kA	mj.
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
tyto	tento	k3xDgFnPc1	tento
techniky	technika	k1gFnPc1	technika
byly	být	k5eAaImAgFnP	být
neefektivní	efektivní	k2eNgFnPc1d1	neefektivní
(	(	kIx(	(
<g/>
nezískávaly	získávat	k5eNaImAgFnP	získávat
hodnotné	hodnotný	k2eAgFnPc1d1	hodnotná
zpravodajské	zpravodajský	k2eAgFnPc1d1	zpravodajská
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
falešné	falešný	k2eAgFnPc1d1	falešná
a	a	k8xC	a
kontraproduktivní	kontraproduktivní	k2eAgFnPc1d1	kontraproduktivní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CIA	CIA	kA	CIA
je	být	k5eAaImIp3nS	být
používala	používat	k5eAaImAgFnS	používat
neadekvátně	adekvátně	k6eNd1	adekvátně
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
je	být	k5eAaImIp3nS	být
outsourcovala	outsourcovat	k5eAaBmAgFnS	outsourcovat
a	a	k8xC	a
dopouštěla	dopouštět	k5eAaImAgFnS	dopouštět
se	se	k3xPyFc4	se
hrubých	hrubý	k2eAgFnPc2d1	hrubá
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
program	program	k1gInSc4	program
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
v	v	k7c6	v
lepším	dobrý	k2eAgNnSc6d2	lepší
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
než	než	k8xS	než
který	který	k3yQgMnSc1	který
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
četnou	četný	k2eAgFnSc4d1	četná
interní	interní	k2eAgFnSc4d1	interní
kritiku	kritika	k1gFnSc4	kritika
a	a	k8xC	a
námitky	námitka	k1gFnPc4	námitka
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
svých	svůj	k3xOyFgMnPc2	svůj
insiderů	insider	k1gMnPc2	insider
a	a	k8xC	a
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
poškodila	poškodit	k5eAaPmAgFnS	poškodit
obraz	obraz	k1gInSc4	obraz
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
27	[number]	k4	27
<g/>
stránková	stránkový	k2eAgFnSc1d1	stránková
výpověď	výpověď	k1gFnSc1	výpověď
Majida	Majid	k1gMnSc2	Majid
Khana	Khan	k1gMnSc2	Khan
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
vězňů	vězeň	k1gMnPc2	vězeň
v	v	k7c6	v
Guantánamu	Guantánam	k1gInSc6	Guantánam
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
CIA	CIA	kA	CIA
využívala	využívat	k5eAaImAgFnS	využívat
ještě	ještě	k9	ještě
horší	zlý	k2eAgFnPc4d2	horší
praktiky	praktika	k1gFnPc4	praktika
než	než	k8xS	než
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
napadání	napadání	k1gNnSc2	napadání
<g/>
,	,	kIx,	,
zavěšování	zavěšování	k1gNnSc1	zavěšování
vězňů	vězeň	k1gMnPc2	vězeň
z	z	k7c2	z
traverzy	traverza	k1gFnSc2	traverza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
ponecháváni	ponechávat	k5eAaImNgMnP	ponechávat
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
až	až	k9	až
po	po	k7c4	po
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
násilného	násilný	k2eAgNnSc2d1	násilné
simulovaného	simulovaný	k2eAgNnSc2d1	simulované
topení	topení	k1gNnSc2	topení
vězňů	vězeň	k1gMnPc2	vězeň
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
ledové	ledový	k2eAgFnSc2d1	ledová
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
nezašlo	zajít	k5eNaPmAgNnS	zajít
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
někteří	některý	k3yIgMnPc1	některý
bývalí	bývalý	k2eAgMnPc1d1	bývalý
vězni	vězeň	k1gMnPc1	vězeň
mučení	mučení	k1gNnSc4	mučení
výše	vysoce	k6eAd2	vysoce
zmíněnými	zmíněný	k2eAgFnPc7d1	zmíněná
technikami	technika	k1gFnPc7	technika
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
ACLU	ACLU	kA	ACLU
žalují	žalovat	k5eAaImIp3nP	žalovat
dva	dva	k4xCgMnPc4	dva
psychology	psycholog	k1gMnPc4	psycholog
(	(	kIx(	(
<g/>
Jamese	Jamesa	k1gFnSc6	Jamesa
E.	E.	kA	E.
Mitchella	Mitchell	k1gMnSc4	Mitchell
a	a	k8xC	a
Johna	John	k1gMnSc4	John
Jessena	Jessen	k2eAgFnSc1d1	Jessen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
detaily	detail	k1gInPc7	detail
programu	program	k1gInSc2	program
mučení	mučení	k1gNnSc2	mučení
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
žaloby	žaloba	k1gFnSc2	žaloba
ACLU	ACLU	kA	ACLU
oba	dva	k4xCgMnPc1	dva
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mučení	mučený	k2eAgMnPc1d1	mučený
lidé	člověk	k1gMnPc1	člověk
budou	být	k5eAaImBp3nP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
"	"	kIx"	"
<g/>
neovladatelné	ovladatelný	k2eNgFnPc1d1	neovladatelná
bolesti	bolest	k1gFnPc1	bolest
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
psychické	psychický	k2eAgFnSc6d1	psychická
stránce	stránka	k1gFnSc6	stránka
povede	povést	k5eAaPmIp3nS	povést
k	k	k7c3	k
pocitu	pocit	k1gInSc3	pocit
beznaděje	beznaděj	k1gFnSc2	beznaděj
<g/>
;	;	kIx,	;
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
účelem	účel	k1gInSc7	účel
aplikace	aplikace	k1gFnSc2	aplikace
jimi	on	k3xPp3gMnPc7	on
navržených	navržený	k2eAgFnPc2d1	navržená
"	"	kIx"	"
<g/>
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1	vyšetřovací
technik	technika	k1gFnPc2	technika
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
získávání	získávání	k1gNnSc1	získávání
autentických	autentický	k2eAgFnPc2d1	autentická
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toliko	toliko	k6eAd1	toliko
způsobování	způsobování	k1gNnSc4	způsobování
této	tento	k3xDgFnSc3	tento
nekontrolovatelné	kontrolovatelný	k2eNgFnSc3d1	nekontrolovatelná
bolesti	bolest	k1gFnSc3	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Mitchell	Mitchell	k1gInSc4	Mitchell
a	a	k8xC	a
Jessen	Jessen	k1gInSc4	Jessen
byli	být	k5eAaImAgMnP	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
programu	program	k1gInSc2	program
dotováni	dotovat	k5eAaBmNgMnP	dotovat
81	[number]	k4	81
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
z	z	k7c2	z
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
CIA	CIA	kA	CIA
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
právní	právní	k2eAgFnSc4d1	právní
obhajobu	obhajoba	k1gFnSc4	obhajoba
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
i	i	k9	i
forma	forma	k1gFnSc1	forma
mučení	mučení	k1gNnSc2	mučení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vyhledávána	vyhledáván	k2eAgFnSc1d1	vyhledávána
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
erotických	erotický	k2eAgFnPc2d1	erotická
a	a	k8xC	a
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnně	souhrnně	k6eAd1	souhrnně
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
praktiky	praktika	k1gFnPc4	praktika
označeny	označen	k2eAgFnPc4d1	označena
jako	jako	k8xS	jako
BDSM	BDSM	kA	BDSM
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
mučírny	mučírna	k1gFnPc1	mučírna
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
nástrojů	nástroj	k1gInPc2	nástroj
používaných	používaný	k2eAgInPc2d1	používaný
po	po	k7c4	po
generace	generace	k1gFnPc4	generace
vybaveny	vybaven	k2eAgFnPc4d1	vybavena
i	i	k8xC	i
například	například	k6eAd1	například
zařízením	zařízení	k1gNnSc7	zařízení
využívajícím	využívající	k2eAgNnSc7d1	využívající
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
