<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pruh	pruh	k1gInSc4
osázený	osázený	k2eAgInSc4d1
trávou	tráva	k1gFnSc7
(	(	kIx(
<g/>
trsy	trs	k1gInPc1
trávy	tráva	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
trvalkami	trvalka	k1gFnPc7
na	na	k7c6
poli	pole	k1gNnSc6
nebo	nebo	k8xC
na	na	k7c6
zahradě	zahrada	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podporuje	podporovat	k5eAaImIp3nS
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
stanoviště	stanoviště	k1gNnPc4
pro	pro	k7c4
užitečný	užitečný	k2eAgInSc4d1
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
ptáky	pták	k1gMnPc4
a	a	k8xC
další	další	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
škůdci	škůdce	k1gMnPc1
<g/>
.	.	kIx.
</s>