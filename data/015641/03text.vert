<s>
Broučí	broučí	k2eAgFnSc1d1
násep	násep	k1gFnSc1
</s>
<s>
Pás	pás	k1gInSc4
kolem	kolem	k6eAd1
pole	pole	k1gNnSc1
ležící	ležící	k2eAgNnSc1d1
ladem	lad	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
broučí	broučí	k2eAgFnSc1d1
násep	násep	k1gFnSc1
</s>
<s>
Broučí	broučí	k2eAgFnSc1d1
násep	násep	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
forma	forma	k1gFnSc1
biologického	biologický	k2eAgInSc2d1
boje	boj	k1gInSc2
proti	proti	k7c3
škůdcům	škůdce	k1gMnPc3
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
a	a	k8xC
zahradnictví	zahradnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pruh	pruh	k1gInSc4
osázený	osázený	k2eAgInSc4d1
trávou	tráva	k1gFnSc7
(	(	kIx(
<g/>
trsy	trs	k1gInPc1
trávy	tráva	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
trvalkami	trvalka	k1gFnPc7
na	na	k7c6
poli	pole	k1gNnSc6
nebo	nebo	k8xC
na	na	k7c6
zahradě	zahrada	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podporuje	podporovat	k5eAaImIp3nS
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
stanoviště	stanoviště	k1gNnPc4
pro	pro	k7c4
užitečný	užitečný	k2eAgInSc4d1
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
ptáky	pták	k1gMnPc4
a	a	k8xC
další	další	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
škůdci	škůdce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Broučí	broučí	k2eAgInPc1d1
náspy	násep	k1gInPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
rostlin	rostlina	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
slunečnice	slunečnice	k1gFnSc1
<g/>
,	,	kIx,
bob	bob	k1gInSc1
<g/>
,	,	kIx,
chrpa	chrpa	k1gFnSc1
polní	polní	k2eAgFnSc1d1
<g/>
,	,	kIx,
koriandr	koriandr	k1gInSc1
<g/>
,	,	kIx,
brutnák	brutnák	k1gInSc1
<g/>
,	,	kIx,
Muhlenbergia	Muhlenbergia	k1gFnSc1
(	(	kIx(
<g/>
trávy	tráva	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
lipnicovité	lipnicovitý	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kavyl	kavyl	k1gInSc1
a	a	k8xC
rod	rod	k1gInSc1
eriogonum	eriogonum	k1gNnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
rdesnovité	rdesnovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Broučí	broučí	k2eAgInPc1d1
náspy	násep	k1gInPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
ke	k	k7c3
snížení	snížení	k1gNnSc3
nebo	nebo	k8xC
k	k	k7c3
náhradě	náhrada	k1gFnSc3
používání	používání	k1gNnSc2
insekticidů	insekticid	k1gInPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
mohou	moct	k5eAaImIp3nP
také	také	k9
sloužit	sloužit	k5eAaImF
jako	jako	k8xS,k8xC
útočiště	útočiště	k1gNnSc1
ptákům	pták	k1gMnPc3
a	a	k8xC
prospěšným	prospěšný	k2eAgMnPc3d1
hlodavcům	hlodavec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
hmyz	hmyz	k1gInSc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
zlatoočka	zlatoočka	k1gFnSc1
obecná	obecná	k1gFnSc1
nebo	nebo	k8xC
lumci	lumek	k1gMnPc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
škůdci	škůdce	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Koncept	koncept	k1gInSc1
náspů	násep	k1gInPc2
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
v	v	k7c6
organizaci	organizace	k1gFnSc6
Game	game	k1gInSc1
&	&	k?
Conservation	Conservation	k1gInSc1
Wildlife	Wildlif	k1gInSc5
Trust	trust	k1gInSc4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1
Southamptonskou	Southamptonský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
významné	významný	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
patří	patřit	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
může	moct	k5eAaImIp3nS
poskytovat	poskytovat	k5eAaImF
stanoviště	stanoviště	k1gNnPc4
pro	pro	k7c4
opylovače	opylovač	k1gInPc4
a	a	k8xC
ohrožené	ohrožený	k2eAgInPc4d1
druhy	druh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
místní	místní	k2eAgInPc1d1
druhy	druh	k1gInPc1
rostliny	rostlina	k1gFnSc2
<g/>
,	,	kIx,
endemická	endemický	k2eAgFnSc1d1
a	a	k8xC
původní	původní	k2eAgFnSc1d1
flóra	flóra	k1gFnSc1
a	a	k8xC
fauna	fauna	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tím	ten	k3xDgInSc7
zároveň	zároveň	k6eAd1
podporována	podporovat	k5eAaImNgFnS
tzv.	tzv.	kA
ekologie	ekologie	k1gFnSc1
obnovy	obnova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
pojmu	pojem	k1gInSc2
</s>
<s>
Podle	podle	k7c2
návrhu	návrh	k1gInSc2
pojmu	pojem	k1gInSc2
v	v	k7c4
Oxford	Oxford	k1gInSc4
English	English	k1gInSc4
Dictionary	Dictionara	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
března	březen	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
anglický	anglický	k2eAgInSc1d1
termín	termín	k1gInSc1
beetle	beetle	k6eAd1
bank	bank	k1gInSc1
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
na	na	k7c4
počátku	počátek	k1gInSc2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
publikovanými	publikovaný	k2eAgInPc7d1
příklady	příklad	k1gInPc7
z	z	k7c2
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1992	#num#	k4
ve	v	k7c6
vydání	vydání	k1gNnSc6
časopisu	časopis	k1gInSc2
New	New	k1gMnSc1
Scientist	Scientist	k1gMnSc1
nebo	nebo	k8xC
ze	z	k7c2
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1994	#num#	k4
v	v	k7c6
sekci	sekce	k1gFnSc6
společnost	společnost	k1gFnSc1
deníku	deník	k1gInSc2
Guardian	Guardiana	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
"	"	kIx"
<g/>
Broučí	broučí	k2eAgInPc1d1
náspy	násep	k1gInPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
nedávná	dávný	k2eNgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
společnosti	společnost	k1gFnSc2
Game	game	k1gInSc4
Conservancy	Conservanca	k1gFnSc2
Trust	trust	k1gInSc4
<g/>
,	,	kIx,
by	by	kYmCp3nP
také	také	k9
měly	mít	k5eAaImAgInP
přispívat	přispívat	k5eAaImF
k	k	k7c3
podpoře	podpora	k1gFnSc3
na	na	k7c6
zemi	zem	k1gFnSc6
hnízdících	hnízdící	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
a	a	k8xC
rovněž	rovněž	k9
vytvářet	vytvářet	k5eAaImF
kryt	kryt	k1gInSc4
pro	pro	k7c4
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
požírá	požírat	k5eAaImIp3nS
mšice	mšice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
přinést	přinést	k5eAaPmF
úspory	úspora	k1gFnPc4
prostředků	prostředek	k1gInPc2
na	na	k7c6
hubení	hubení	k1gNnSc6
mšic	mšice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Beetle	Beetle	k1gFnSc2
bank	banka	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MACHOVEC	Machovec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
typy	typ	k1gInPc1
balíků	balík	k1gMnPc2
biomasy	biomasa	k1gFnSc2
po	po	k7c6
sklizni	sklizeň	k1gFnSc6
jako	jako	k8xS,k8xC
mikrobiotop	mikrobiotop	k1gInSc4
pro	pro	k7c4
bezobratlé	bezobratlý	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
(	(	kIx(
<g/>
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
JIHOČESKÁ	jihočeský	k2eAgFnSc1d1
UNIVERZITA	univerzita	k1gFnSc1
V	v	k7c6
ČESKÝCH	český	k2eAgInPc6d1
BUDĚJOVICÍCH	Budějovice	k1gInPc6
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VONDRUŠKOVÁ	Vondrušková	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Způsoby	způsoba	k1gFnPc4
podpory	podpora	k1gFnSc2
pavouků	pavouk	k1gMnPc2
jako	jako	k8xC,k8xS
antagonistů	antagonista	k1gMnPc2
škůdců	škůdce	k1gMnPc2
v	v	k7c6
agroekosystémech	agroekosystém	k1gInPc6
-	-	kIx~
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
23,24	23,24	k4
<g/>
.	.	kIx.
is	is	k?
<g/>
.	.	kIx.
<g/>
mendelu	mendel	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medlova	Medlův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
23,24	23,24	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kevin	Kevin	k2eAgInSc4d1
McCullen	McCullen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Connell	Connell	k1gMnSc1
organic	organice	k1gFnPc2
farmer	farmer	k1gMnSc1
using	using	k1gMnSc1
'	'	kIx"
<g/>
beetle	beetle	k1gFnSc1
banks	banksa	k1gFnPc2
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tri-City	Tri-Cit	k2eAgFnPc4d1
Herald.	Herald.	k1gFnPc4
14	#num#	k4
June	jun	k1gMnSc5
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
17	#num#	k4
August	August	k1gMnSc1
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Felix	Felix	k1gMnSc1
Wäckers	Wäckers	k1gInSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
van	vana	k1gFnPc2
Rijn	Rijn	k1gMnSc1
and	and	k?
Jan	Jan	k1gMnSc1
Bruin	Bruin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plant-Provided	Plant-Provided	k1gInSc1
Food	Fooda	k1gFnPc2
for	forum	k1gNnPc2
Carnivorous	Carnivorous	k1gMnSc1
Insects	Insects	k1gInSc1
-	-	kIx~
a	a	k8xC
protective	protectiv	k1gInSc5
mutualism	mutualism	k1gFnPc3
and	and	k?
its	its	k?
applications	applications	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
UK	UK	kA
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
81941	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Beetle	Beetle	k1gFnSc1
bank	banka	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
Game	game	k1gInSc1
&	&	k?
Wildlife	Wildlif	k1gInSc5
Conservation	Conservation	k1gInSc1
Trust	trust	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Příroda	příroda	k1gFnSc1
|	|	kIx~
Zahrada	zahrada	k1gFnSc1
a	a	k8xC
zahradnictví	zahradnictví	k1gNnSc1
|	|	kIx~
Zemědělství	zemědělství	k1gNnSc1
</s>
