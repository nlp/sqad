<s>
Zeměplocha	Zeměploch	k1gMnSc4	Zeměploch
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Discworld	Discworld	k1gInSc1	Discworld
<g/>
;	;	kIx,	;
český	český	k2eAgMnSc1d1	český
vydavatel	vydavatel	k1gMnSc1	vydavatel
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
tuto	tento	k3xDgFnSc4	tento
sérii	série	k1gFnSc4	série
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xS	jako
Úžasná	úžasný	k2eAgFnSc1d1	úžasná
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
<g/>
,	,	kIx,	,
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
podloženo	podložit	k5eAaPmNgNnS	podložit
originálem	originál	k1gInSc7	originál
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
knižní	knižní	k2eAgFnPc4d1	knižní
série	série	k1gFnPc4	série
psaná	psaný	k2eAgFnSc1d1	psaná
Terrym	Terrym	k1gInSc1	Terrym
Pratchettem	Pratchett	k1gInSc7	Pratchett
čítající	čítající	k2eAgFnSc2d1	čítající
41	[number]	k4	41
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgFnPc2d1	různá
dalších	další	k2eAgFnPc2d1	další
publikací	publikace	k1gFnPc2	publikace
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnSc4	Zeměploš
<g/>
,	,	kIx,	,
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
ležícím	ležící	k2eAgInSc6d1	ležící
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
želvy	želva	k1gFnSc2	želva
plující	plující	k2eAgFnSc2d1	plující
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
.	.	kIx.	.
</s>
