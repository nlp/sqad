<s>
Zeměplocha	Zeměploch	k1gMnSc4	Zeměploch
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Discworld	Discworld	k1gInSc1	Discworld
<g/>
;	;	kIx,	;
český	český	k2eAgMnSc1d1	český
vydavatel	vydavatel	k1gMnSc1	vydavatel
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
tuto	tento	k3xDgFnSc4	tento
sérii	série	k1gFnSc4	série
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xS	jako
Úžasná	úžasný	k2eAgFnSc1d1	úžasná
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
<g/>
,	,	kIx,	,
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
podloženo	podložit	k5eAaPmNgNnS	podložit
originálem	originál	k1gInSc7	originál
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
knižní	knižní	k2eAgFnPc4d1	knižní
série	série	k1gFnPc4	série
psaná	psaný	k2eAgFnSc1d1	psaná
Terrym	Terrym	k1gInSc1	Terrym
Pratchettem	Pratchett	k1gInSc7	Pratchett
čítající	čítající	k2eAgFnSc2d1	čítající
41	[number]	k4	41
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgFnPc2d1	různá
dalších	další	k2eAgFnPc2d1	další
publikací	publikace	k1gFnPc2	publikace
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnSc4	Zeměploš
<g/>
,	,	kIx,	,
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
ležícím	ležící	k2eAgInSc6d1	ležící
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
želvy	želva	k1gFnSc2	želva
plující	plující	k2eAgFnSc2d1	plující
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Drží	držet	k5eAaImIp3nS	držet
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
zvyklostí	zvyklost	k1gFnPc2	zvyklost
moderní	moderní	k2eAgInPc1d1	moderní
fantasy	fantas	k1gInPc1	fantas
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
humoristické	humoristický	k2eAgFnPc1d1	humoristická
a	a	k8xC	a
satirické	satirický	k2eAgFnPc1d1	satirická
a	a	k8xC	a
často	často	k6eAd1	často
parodují	parodovat	k5eAaImIp3nP	parodovat
prvky	prvek	k1gInPc1	prvek
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
<g/>
,	,	kIx,	,
Roberta	Robert	k1gMnSc4	Robert
E.	E.	kA	E.
Howarda	Howard	k1gMnSc4	Howard
nebo	nebo	k8xC	nebo
H.	H.	kA	H.
P.	P.	kA	P.
Lovecrafta	Lovecrafta	k1gMnSc1	Lovecrafta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různé	různý	k2eAgInPc4d1	různý
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
folklóru	folklór	k1gInSc2	folklór
a	a	k8xC	a
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
série	série	k1gFnSc1	série
postupně	postupně	k6eAd1	postupně
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
a	a	k8xC	a
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
různé	různý	k2eAgInPc4d1	různý
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zeměplošskou	zeměplošský	k2eAgFnSc4d1	zeměplošská
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc1	komiks
nebo	nebo	k8xC	nebo
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
(	(	kIx(	(
<g/>
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pražské	pražský	k2eAgNnSc1d1	Pražské
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vydávané	vydávaný	k2eAgFnPc1d1	vydávaná
knihy	kniha	k1gFnPc1	kniha
obvykle	obvykle	k6eAd1	obvykle
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
v	v	k7c6	v
The	The	k1gFnPc6	The
Sunday	Sundaa	k1gMnSc2	Sundaa
Times	Times	k1gMnSc1	Times
<g/>
;	;	kIx,	;
Pratchett	Pratchett	k1gMnSc1	Pratchett
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
nejprodávanějším	prodávaný	k2eAgMnSc7d3	nejprodávanější
britským	britský	k2eAgMnSc7d1	britský
autorem	autor	k1gMnSc7	autor
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
překonán	překonat	k5eAaPmNgInS	překonat
knihami	kniha	k1gFnPc7	kniha
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
od	od	k7c2	od
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
však	však	k9	však
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
knihy	kniha	k1gFnPc1	kniha
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
kradou	krást	k5eAaImIp3nP	krást
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
knihy	kniha	k1gFnSc2	kniha
vydává	vydávat	k5eAaImIp3nS	vydávat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Talpress	Talpressa	k1gFnPc2	Talpressa
a	a	k8xC	a
překládá	překládat	k5eAaImIp3nS	překládat
je	on	k3xPp3gNnSc4	on
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Muži	muž	k1gMnSc3	muž
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc1	fantas
a	a	k8xC	a
hororu	horor	k1gInSc6	horor
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
fantasy	fantas	k1gInPc4	fantas
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995-1999	[number]	k4	1995-1999
byla	být	k5eAaImAgNnP	být
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
oceňována	oceňovat	k5eAaImNgFnS	oceňovat
i	i	k9	i
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
strávil	strávit	k5eAaPmAgMnS	strávit
prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgNnPc4d1	celé
devadesátá	devadesátý	k4xOgNnPc4	devadesátý
léta	léto	k1gNnPc4	léto
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
překladatel	překladatel	k1gMnSc1	překladatel
téže	týž	k3xTgFnSc2	týž
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
Zeměplochy	Zeměploch	k1gInPc4	Zeměploch
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ve	v	k7c6	v
41	[number]	k4	41
knihách	kniha	k1gFnPc6	kniha
a	a	k8xC	a
pěti	pět	k4xCc2	pět
povídkách	povídka	k1gFnPc6	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Obálky	obálka	k1gFnPc1	obálka
všech	všecek	k3xTgFnPc2	všecek
britských	britský	k2eAgFnPc2d1	britská
edicí	edice	k1gFnPc2	edice
knih	kniha	k1gFnPc2	kniha
až	až	k9	až
po	po	k7c4	po
Zloděje	zloděj	k1gMnSc4	zloděj
času	čas	k1gInSc2	čas
kreslil	kreslit	k5eAaImAgMnS	kreslit
Josh	Josh	k1gMnSc1	Josh
Kirby	Kirba	k1gFnSc2	Kirba
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
přebaly	přebal	k1gInPc1	přebal
vyšly	vyjít	k5eAaPmAgFnP	vyjít
knihy	kniha	k1gFnPc1	kniha
i	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Obálky	obálka	k1gFnPc1	obálka
amerických	americký	k2eAgFnPc2d1	americká
edic	edice	k1gFnPc2	edice
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
různými	různý	k2eAgInPc7d1	různý
kreslíři	kreslíř	k1gMnSc3	kreslíř
a	a	k8xC	a
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
Joshe	Josh	k1gMnSc2	Josh
Kirbyho	Kirby	k1gMnSc2	Kirby
jsou	být	k5eAaImIp3nP	být
celosvětově	celosvětově	k6eAd1	celosvětově
využívány	využíván	k2eAgFnPc1d1	využívána
ilustrace	ilustrace	k1gFnPc1	ilustrace
Paula	Paul	k1gMnSc2	Paul
Kidbyho	Kidby	k1gMnSc2	Kidby
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
zeměplošských	zeměplošský	k2eAgFnPc2d1	zeměplošská
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
;	;	kIx,	;
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
proplétajících	proplétající	k2eAgInPc2d1	proplétající
se	se	k3xPyFc4	se
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
v	v	k7c6	v
Zaslané	zaslaný	k2eAgFnSc6d1	zaslaná
poště	pošta	k1gFnSc6	pošta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kromě	kromě	k7c2	kromě
kapitol	kapitola	k1gFnPc2	kapitola
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
jejich	jejich	k3xOp3gInSc4	jejich
stručný	stručný	k2eAgInSc4d1	stručný
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
kniha	kniha	k1gFnSc1	kniha
Medvídek	medvídek	k1gMnSc1	medvídek
Pú	Pú	k1gMnSc1	Pú
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
později	pozdě	k6eAd2	pozdě
Pyramidy	pyramid	k1gInPc4	pyramid
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
úseků	úsek	k1gInPc2	úsek
zvaných	zvaný	k2eAgInPc2d1	zvaný
"	"	kIx"	"
<g/>
knihy	kniha	k1gFnPc1	kniha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pratchettových	Pratchettův	k2eAgNnPc2d1	Pratchettův
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
si	se	k3xPyFc3	se
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
nezvykl	zvyknout	k5eNaPmAgMnS	zvyknout
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
po	po	k7c6	po
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
je	být	k5eAaImIp3nS	být
použil	použít	k5eAaPmAgMnS	použít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
nakladatel	nakladatel	k1gMnSc1	nakladatel
křičel	křičet	k5eAaImAgMnS	křičet
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
nedal	dát	k5eNaPmAgMnS	dát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
knih	kniha	k1gFnPc2	kniha
sdílí	sdílet	k5eAaImIp3nS	sdílet
své	svůj	k3xOyFgFnPc4	svůj
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
jejich	jejich	k3xOp3gNnSc4	jejich
vývoj	vývoj	k1gInSc1	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
hlavní	hlavní	k2eAgInPc1d1	hlavní
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Mrakoplaš	Mrakoplaš	k1gInSc4	Mrakoplaš
v	v	k7c6	v
Mortovi	Mort	k1gMnSc6	Mort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
cyklu	cyklus	k1gInSc6	cyklus
běží	běžet	k5eAaImIp3nS	běžet
skutečný	skutečný	k2eAgInSc1d1	skutečný
čas	čas	k1gInSc1	čas
a	a	k8xC	a
věk	věk	k1gInSc1	věk
postav	postava	k1gFnPc2	postava
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kalendářními	kalendářní	k2eAgInPc7d1	kalendářní
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
bývají	bývat	k5eAaImIp3nP	bývat
rozdělovány	rozdělovat	k5eAaImNgFnP	rozdělovat
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hrdinů	hrdina	k1gMnPc2	hrdina
nebo	nebo	k8xC	nebo
témat	téma	k1gNnPc2	téma
<g/>
:	:	kIx,	:
Knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
Mrakoplašovi	Mrakoplaš	k1gMnSc6	Mrakoplaš
-	-	kIx~	-
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
nedostudovaný	dostudovaný	k2eNgInSc1d1	nedostudovaný
"	"	kIx"	"
<g/>
mák	mák	k1gInSc1	mák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
napsáno	napsat	k5eAaPmNgNnS	napsat
na	na	k7c6	na
klobouku	klobouk	k1gInSc6	klobouk
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
Mrakoplaš	Mrakoplaš	k1gMnSc1	Mrakoplaš
<g/>
,	,	kIx,	,
neschopný	schopný	k2eNgMnSc1d1	neschopný
použít	použít	k5eAaPmF	použít
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
proslulý	proslulý	k2eAgInSc1d1	proslulý
svým	svůj	k3xOyFgInSc7	svůj
pudem	pud	k1gInSc7	pud
sebezáchovy	sebezáchova	k1gFnSc2	sebezáchova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
s	s	k7c7	s
Mrakoplašem	Mrakoplaš	k1gInSc7	Mrakoplaš
také	také	k9	také
často	často	k6eAd1	často
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
mágové	mág	k1gMnPc1	mág
z	z	k7c2	z
Neviditelné	viditelný	k2eNgFnSc2d1	neviditelná
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
čarodějkách	čarodějka	k1gFnPc6	čarodějka
-	-	kIx~	-
Popisují	popisovat	k5eAaImIp3nP	popisovat
příběhy	příběh	k1gInPc4	příběh
čarodějek	čarodějka	k1gFnPc2	čarodějka
z	z	k7c2	z
Lancre	Lancr	k1gInSc5	Lancr
-	-	kIx~	-
Bábi	Bábi	k1gFnPc4	Bábi
Zlopočasné	Zlopočasný	k2eAgFnPc4d1	Zlopočasná
<g/>
,	,	kIx,	,
Stařenky	stařenka	k1gFnPc4	stařenka
Oggové	Oggová	k1gFnSc2	Oggová
a	a	k8xC	a
Magráty	Magrát	k1gInPc4	Magrát
Česnekové	česnekový	k2eAgInPc4d1	česnekový
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
trojici	trojice	k1gFnSc3	trojice
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přidává	přidávat	k5eAaImIp3nS	přidávat
také	také	k9	také
Anežka	Anežka	k1gFnSc1	Anežka
Nulíčková	Nulíčková	k1gFnSc1	Nulíčková
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
knihy	kniha	k1gFnPc4	kniha
o	o	k7c6	o
čarodějkách	čarodějka	k1gFnPc6	čarodějka
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Čaroprávnost	Čaroprávnost	k1gFnSc1	Čaroprávnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
<g/>
)	)	kIx)	)
mág	mág	k1gMnSc1	mág
-	-	kIx~	-
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Eskarína	Eskarína	k1gFnSc1	Eskarína
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Esk	Esk	k1gFnSc1	Esk
<g/>
.	.	kIx.	.
</s>
<s>
Umírající	umírající	k1gMnSc1	umírající
mág	mág	k1gMnSc1	mág
jí	on	k3xPp3gFnSc3	on
odkáže	odkázat	k5eAaPmIp3nS	odkázat
svoji	svůj	k3xOyFgFnSc4	svůj
hůl	hůl	k1gFnSc4	hůl
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
ověřil	ověřit	k5eAaPmAgMnS	ověřit
její	její	k3xOp3gNnSc4	její
pohlaví	pohlaví	k1gNnSc4	pohlaví
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ji	on	k3xPp3gFnSc4	on
předurčí	předurčit	k5eAaPmIp3nS	předurčit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
mágem	mág	k1gMnSc7	mág
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
proti	proti	k7c3	proti
všem	všecek	k3xTgNnPc3	všecek
pravidlům	pravidlo	k1gNnPc3	pravidlo
-	-	kIx~	-
mágové	mág	k1gMnPc1	mág
jsou	být	k5eAaImIp3nP	být
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
čarodějky	čarodějka	k1gFnPc4	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
Bábi	Bábi	k1gFnSc1	Bábi
Zlopočasná	Zlopočasný	k2eAgFnSc1d1	Zlopočasná
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
snaží	snažit	k5eAaImIp3nP	snažit
z	z	k7c2	z
Eskaríny	Eskarína	k1gFnSc2	Eskarína
udělat	udělat	k5eAaPmF	udělat
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěje	uspět	k5eNaPmIp3nS	uspět
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
jí	on	k3xPp3gFnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
Smrťovi	Smrťa	k1gMnSc6	Smrťa
-	-	kIx~	-
Smrť	smrtit	k5eAaImRp2nS	smrtit
je	být	k5eAaImIp3nS	být
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xC	jako
tradiční	tradiční	k2eAgMnSc1d1	tradiční
vysoký	vysoký	k2eAgMnSc1d1	vysoký
kostlivec	kostlivec	k1gMnSc1	kostlivec
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
kápi	kápě	k1gFnSc6	kápě
<g/>
.	.	kIx.	.
</s>
<s>
Smrťovi	Smrťův	k2eAgMnPc1d1	Smrťův
společníci	společník	k1gMnPc1	společník
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnSc1	jeho
vnučka	vnučka	k1gFnSc1	vnučka
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
sluha	sluha	k1gMnSc1	sluha
Albert	Albert	k1gMnSc1	Albert
a	a	k8xC	a
Krysí	krysí	k2eAgMnSc1d1	krysí
Smrť	smrtit	k5eAaImRp2nS	smrtit
<g/>
,	,	kIx,	,
hlodavčí	hlodavčí	k2eAgFnSc1d1	hlodavčí
obdoba	obdoba	k1gFnSc1	obdoba
Smrtě	smrtit	k5eAaImSgInS	smrtit
lidského	lidský	k2eAgMnSc2d1	lidský
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Mort	Morta	k1gFnPc2	Morta
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gMnSc1	jeho
učeň	učeň	k1gMnSc1	učeň
Mort	Mort	k1gMnSc1	Mort
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
knihách	kniha	k1gFnPc6	kniha
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
Smrť	smrtit	k5eAaImRp2nS	smrtit
však	však	k9	však
neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
na	na	k7c6	na
důležitosti	důležitost	k1gFnSc6	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Smrť	smrtit	k5eAaImRp2nS	smrtit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
postava	postava	k1gFnSc1	postava
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zeměplošských	zeměplošský	k2eAgFnPc6d1	zeměplošská
knihách	kniha	k1gFnPc6	kniha
kromě	kromě	k7c2	kromě
Svobodnýho	Svobodnýho	k?	Svobodnýho
národa	národ	k1gInSc2	národ
a	a	k8xC	a
Šňupce	šňupec	k1gInSc2	šňupec
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
s	s	k7c7	s
Městskou	městský	k2eAgFnSc7d1	městská
hlídkou	hlídka	k1gFnSc7	hlídka
-	-	kIx~	-
Tyto	tento	k3xDgInPc1	tento
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
činností	činnost	k1gFnSc7	činnost
ankh-morporské	ankhorporský	k2eAgFnSc2d1	ankh-morporský
obdoby	obdoba	k1gFnSc2	obdoba
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
Městské	městský	k2eAgFnSc2d1	městská
hlídky	hlídka	k1gFnSc2	hlídka
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
velitelem	velitel	k1gMnSc7	velitel
je	být	k5eAaImIp3nS	být
Samuel	Samuel	k1gMnSc1	Samuel
Elánius	Elánius	k1gMnSc1	Elánius
a	a	k8xC	a
mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
strážníky	strážník	k1gMnPc4	strážník
patří	patřit	k5eAaImIp3nS	patřit
Karotka	karotka	k1gFnSc1	karotka
Rudykopalsson	Rudykopalsson	k1gMnSc1	Rudykopalsson
<g/>
,	,	kIx,	,
Angua	Angua	k1gMnSc1	Angua
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Tračník	tračník	k1gInSc1	tračník
<g/>
,	,	kIx,	,
Navážka	navážka	k1gFnSc1	navážka
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Noby	Noba	k1gFnSc2	Noba
<g/>
"	"	kIx"	"
Nóblhóch	Nóblhóch	k1gMnSc1	Nóblhóch
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
Toničce	Tonička	k1gFnSc6	Tonička
Bolavé	bolavý	k2eAgNnSc1d1	bolavé
-	-	kIx~	-
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
knihách	kniha	k1gFnPc6	kniha
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
postavou	postava	k1gFnSc7	postava
mladá	mladá	k1gFnSc1	mladá
dívka	dívka	k1gFnSc1	dívka
Tonička	Tonička	k1gFnSc1	Tonička
<g/>
,	,	kIx,	,
spřátelená	spřátelený	k2eAgFnSc1d1	spřátelená
se	s	k7c7	s
zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
tvory	tvor	k1gMnPc4	tvor
známými	známý	k1gMnPc7	známý
jako	jako	k8xC	jako
Nac	Nac	k1gFnSc1	Nac
Mac	Mac	kA	Mac
Fíglové	Fíglová	k1gFnSc2	Fíglová
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
psané	psaný	k2eAgInPc1d1	psaný
stylem	styl	k1gInSc7	styl
dětských	dětský	k2eAgFnPc2d1	dětská
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
se	se	k3xPyFc4	se
knihám	kniha	k1gFnPc3	kniha
o	o	k7c6	o
čarodějkách	čarodějka	k1gFnPc6	čarodějka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Klobouk	klobouk	k1gInSc1	klobouk
s	s	k7c7	s
oblohou	obloha	k1gFnSc7	obloha
<g/>
,	,	kIx,	,
Obléknu	obléknout	k5eAaPmIp1nS	obléknout
si	se	k3xPyFc3	se
půlnoc	půlnoc	k1gFnSc1	půlnoc
a	a	k8xC	a
Pastýřská	pastýřský	k2eAgFnSc1d1	pastýřská
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
revoluci	revoluce	k1gFnSc6	revoluce
–	–	k?	–
Na	na	k7c6	na
Zeměplochu	Zeměploch	k1gInSc6	Zeměploch
dorazil	dorazit	k5eAaPmAgInS	dorazit
pokrok	pokrok	k1gInSc1	pokrok
a	a	k8xC	a
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
série	série	k1gFnSc2	série
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
příběhy	příběh	k1gInPc1	příběh
bývalého	bývalý	k2eAgMnSc2d1	bývalý
zločince	zločinec	k1gMnSc2	zločinec
Vlahoše	Vlahoš	k1gMnSc2	Vlahoš
von	von	k1gInSc4	von
Rosreta	Rosreto	k1gNnSc2	Rosreto
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
(	(	kIx(	(
<g/>
a	a	k8xC	a
podle	podle	k7c2	podle
vůle	vůle	k1gFnSc2	vůle
Patricije	patricij	k1gMnSc2	patricij
<g/>
)	)	kIx)	)
polepšen	polepšit	k5eAaPmNgInS	polepšit
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
Ankh-Morporku	Ankh-Morporek	k1gInSc6	Ankh-Morporek
zastává	zastávat	k5eAaImIp3nS	zastávat
důležité	důležitý	k2eAgFnPc4d1	důležitá
funkce	funkce	k1gFnPc4	funkce
v	v	k7c6	v
životě	život	k1gInSc6	život
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
poštmistra	poštmistr	k1gMnSc4	poštmistr
či	či	k8xC	či
vedoucího	vedoucí	k1gMnSc4	vedoucí
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
mincovny	mincovna	k1gFnSc2	mincovna
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
-	-	kIx~	-
Knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
výše	výše	k1gFnSc1	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jen	jen	k9	jen
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
Malí	malý	k2eAgMnPc1d1	malý
bohové	bůh	k1gMnPc1	bůh
nebo	nebo	k8xC	nebo
Pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
však	však	k9	však
nedá	dát	k5eNaPmIp3nS	dát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
příběhů	příběh	k1gInPc2	příběh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pravda	pravda	k1gFnSc1	pravda
nebo	nebo	k8xC	nebo
Zloděj	zloděj	k1gMnSc1	zloděj
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
propojují	propojovat	k5eAaImIp3nP	propojovat
se	se	k3xPyFc4	se
s	s	k7c7	s
několika	několik	k4yIc2	několik
hlavními	hlavní	k2eAgFnPc7d1	hlavní
dějovými	dějový	k2eAgFnPc7d1	dějová
liniemi	linie	k1gFnPc7	linie
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mágové	mág	k1gMnPc1	mág
z	z	k7c2	z
Neviditelné	viditelný	k2eNgFnSc2d1	neviditelná
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
elfové	elf	k1gMnPc1	elf
nebo	nebo	k8xC	nebo
mnichové	mnich	k1gMnPc1	mnich
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jako	jako	k9	jako
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
nejsou	být	k5eNaImIp3nP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
ani	ani	k8xC	ani
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nevázané	vázaný	k2eNgNnSc1d1	nevázané
na	na	k7c4	na
hlavní	hlavní	k2eAgInPc4d1	hlavní
děje	děj	k1gInPc4	děj
<g/>
,	,	kIx,	,
dokumentují	dokumentovat	k5eAaBmIp3nP	dokumentovat
vývoj	vývoj	k1gInSc4	vývoj
města	město	k1gNnSc2	město
Ankh-Morpork	Ankh-Morpork	k1gInSc1	Ankh-Morpork
na	na	k7c4	na
technologicky	technologicky	k6eAd1	technologicky
pokročilou	pokročilý	k2eAgFnSc4d1	pokročilá
metropoli	metropole	k1gFnSc4	metropole
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
Pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
městských	městský	k2eAgFnPc2d1	městská
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
Zaslané	zaslaný	k2eAgFnSc2d1	zaslaná
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
zaznamenávající	zaznamenávající	k2eAgInSc4d1	zaznamenávající
rozvoj	rozvoj	k1gInSc4	rozvoj
poštovních	poštovní	k2eAgFnPc2d1	poštovní
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
vzestup	vzestup	k1gInSc1	vzestup
zeměplošského	zeměplošský	k2eAgInSc2d1	zeměplošský
telekomunikačního	telekomunikační	k2eAgInSc2d1	telekomunikační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
semaforů	semafor	k1gInPc2	semafor
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
Úžasný	úžasný	k2eAgMnSc1d1	úžasný
Mauric	Mauric	k1gMnSc1	Mauric
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
romány	román	k1gInPc1	román
ze	z	k7c2	z
série	série	k1gFnSc2	série
o	o	k7c6	o
Toničce	Tonička	k1gFnSc6	Tonička
Bolavé	bolavý	k2eAgNnSc1d1	bolavé
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
mladším	mladý	k2eAgMnPc3d2	mladší
čtenářům	čtenář	k1gMnPc3	čtenář
a	a	k8xC	a
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vydány	vydán	k2eAgInPc1d1	vydán
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
Příběh	příběh	k1gInSc1	příběh
ze	z	k7c2	z
Zeměplochy	Zeměploch	k1gMnPc4	Zeměploch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
"	"	kIx"	"
<g/>
A	a	k9	a
Story	story	k1gFnSc1	story
of	of	k?	of
Discworld	Discworld	k1gInSc1	Discworld
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgInSc4d1	poslední
dva	dva	k4xCgInPc4	dva
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
Toničce	Tonička	k1gFnSc6	Tonička
však	však	k9	však
již	již	k6eAd1	již
v	v	k7c6	v
originále	originál	k1gInSc6	originál
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
standardní	standardní	k2eAgInSc4d1	standardní
podtitul	podtitul	k1gInSc4	podtitul
"	"	kIx"	"
<g/>
A	a	k9	a
Discworld	Discworld	k1gInSc1	Discworld
Novel	novela	k1gFnPc2	novela
<g/>
"	"	kIx"	"
používaný	používaný	k2eAgInSc1d1	používaný
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
románů	román	k1gInPc2	román
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
vyčleňovány	vyčleňovat	k5eAaImNgFnP	vyčleňovat
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
cyklu	cyklus	k1gInSc2	cyklus
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
dle	dle	k7c2	dle
anglického	anglický	k2eAgNnSc2d1	anglické
data	datum	k1gNnSc2	datum
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pořadí	pořadí	k1gNnSc6	pořadí
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
:	:	kIx,	:
Nohy	noha	k1gFnPc1	noha
z	z	k7c2	z
jílu	jíl	k1gInSc2	jíl
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
až	až	k9	až
po	po	k7c6	po
knize	kniha	k1gFnSc6	kniha
Hrr	Hrr	k1gFnPc2	Hrr
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
Maškarádě	maškaráda	k1gFnSc6	maškaráda
jako	jako	k8xS	jako
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
.	.	kIx.	.
</s>
<s>
Pratchett	Pratchett	k1gMnSc1	Pratchett
napsal	napsat	k5eAaPmAgMnS	napsat
také	také	k9	také
pět	pět	k4xCc4	pět
zeměplošských	zeměplošský	k2eAgFnPc2d1	zeměplošská
povídek	povídka	k1gFnPc2	povídka
<g/>
:	:	kIx,	:
Trollí	Trolle	k1gFnPc2	Trolle
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
krutosti	krutost	k1gFnSc2	krutost
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moře	moře	k1gNnSc1	moře
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
rybky	rybka	k1gFnPc1	rybka
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
co	co	k3yInSc1	co
přijde	přijít	k5eAaPmIp3nS	přijít
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
A	a	k9	a
Collegiate	Collegiat	k1gInSc5	Collegiat
Casting-Out	Casting-Out	k2eAgMnSc1d1	Casting-Out
of	of	k?	of
Devilish	Devilish	k1gMnSc1	Devilish
Devices	Devices	k1gMnSc1	Devices
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
povídkami	povídka	k1gFnPc7	povídka
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Divadlo	divadlo	k1gNnSc1	divadlo
krutosti	krutost	k1gFnSc2	krutost
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Talpress	Talpressa	k1gFnPc2	Talpressa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
také	také	k9	také
několik	několik	k4yIc1	několik
zeměplošských	zeměplošský	k2eAgFnPc2d1	zeměplošská
map	mapa	k1gFnPc2	mapa
<g/>
:	:	kIx,	:
Ulice	ulice	k1gFnSc1	ulice
Ankh-Morporku	Ankh-Morporek	k1gInSc2	Ankh-Morporek
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Streets	Streets	k1gInSc1	Streets
of	of	k?	of
Ankh-Morpork	Ankh-Morpork	k1gInSc1	Ankh-Morpork
<g/>
)	)	kIx)	)
Mapa	mapa	k1gFnSc1	mapa
Zeměplochy	Zeměploch	k1gInPc4	Zeměploch
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Discworld	Discworld	k1gMnSc1	Discworld
Mapp	Mapp	k1gMnSc1	Mapp
<g/>
)	)	kIx)	)
Smrťova	Smrťův	k2eAgFnSc1d1	Smrťova
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
Death	Death	k1gInSc1	Death
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Domain	Domain	k1gInSc1	Domain
<g/>
)	)	kIx)	)
Turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
Lancre	Lancr	k1gInSc5	Lancr
(	(	kIx(	(
<g/>
A	a	k9	a
Tourist	Tourist	k1gInSc1	Tourist
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Lancre	Lancr	k1gInSc5	Lancr
<g/>
)	)	kIx)	)
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
mapy	mapa	k1gFnPc4	mapa
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
Stephen	Stephen	k2eAgInSc1d1	Stephen
Player	Player	k1gInSc1	Player
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nákresů	nákres	k1gInPc2	nákres
Terryho	Terry	k1gMnSc4	Terry
Pratchetta	Pratchett	k1gMnSc4	Pratchett
a	a	k8xC	a
Stephena	Stephen	k1gMnSc4	Stephen
Briggse	Briggs	k1gMnSc4	Briggs
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
pak	pak	k6eAd1	pak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Paul	Paul	k1gMnSc1	Paul
Kidby	Kidba	k1gFnSc2	Kidba
<g/>
,	,	kIx,	,
Smrťovu	Smrťův	k2eAgFnSc4d1	Smrťova
říši	říše	k1gFnSc4	říše
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
Stephenem	Stephen	k1gMnSc7	Stephen
Briggsem	Briggs	k1gMnSc7	Briggs
<g/>
.	.	kIx.	.
</s>
<s>
Pratchett	Pratchett	k1gMnSc1	Pratchett
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ianem	Ianus	k1gMnSc7	Ianus
Stewartem	Stewart	k1gMnSc7	Stewart
a	a	k8xC	a
Jackem	Jacek	k1gMnSc7	Jacek
Cohenem	Cohen	k1gMnSc7	Cohen
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tři	tři	k4xCgFnPc4	tři
knihy	kniha	k1gFnPc4	kniha
využívající	využívající	k2eAgFnPc4d1	využívající
Zeměplochu	Zeměploch	k1gInSc2	Zeměploch
k	k	k7c3	k
ilustraci	ilustrace	k1gFnSc3	ilustrace
různých	různý	k2eAgNnPc2d1	různé
populárně	populárně	k6eAd1	populárně
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
střídají	střídat	k5eAaImIp3nP	střídat
klasický	klasický	k2eAgInSc4d1	klasický
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
<g/>
)	)	kIx)	)
zeměplošský	zeměplošský	k2eAgInSc1d1	zeměplošský
příběh	příběh	k1gInSc1	příběh
a	a	k8xC	a
populárně	populárně	k6eAd1	populárně
vědecké	vědecký	k2eAgInPc4d1	vědecký
komentáře	komentář	k1gInPc4	komentář
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
příběhu	příběh	k1gInSc3	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnSc4	Zeměploš
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Science	Science	k1gFnSc2	Science
of	of	k?	of
Discworld	Discworld	k1gInSc1	Discworld
<g/>
)	)	kIx)	)
Koule	koule	k1gFnSc1	koule
–	–	k?	–
Věda	věda	k1gFnSc1	věda
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnSc4	Zeměploš
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Science	Science	k1gFnSc2	Science
of	of	k?	of
Discworld	Discworld	k1gMnSc1	Discworld
II	II	kA	II
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Globe	globus	k1gInSc5	globus
<g/>
)	)	kIx)	)
Darwinovy	Darwinův	k2eAgFnPc4d1	Darwinova
hodinky	hodinka	k1gFnPc4	hodinka
–	–	k?	–
Věda	věda	k1gFnSc1	věda
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnSc4	Zeměploš
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Science	Science	k1gFnSc2	Science
of	of	k?	of
Discworld	Discworld	k1gMnSc1	Discworld
III	III	kA	III
<g/>
:	:	kIx,	:
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Watch	Watch	k1gInSc1	Watch
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Soudný	soudný	k2eAgInSc1d1	soudný
den	den	k1gInSc1	den
–	–	k?	–
Věda	věda	k1gFnSc1	věda
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnSc4	Zeměploš
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Science	Science	k1gFnSc2	Science
of	of	k?	of
Discworld	Discworld	k1gMnSc1	Discworld
IV	IV	kA	IV
<g/>
:	:	kIx,	:
Judgement	Judgement	k1gMnSc1	Judgement
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
Bylo	být	k5eAaImAgNnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
také	také	k9	také
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
zeměplošských	zeměplošský	k2eAgFnPc2d1	zeměplošská
publikací	publikace	k1gFnPc2	publikace
<g/>
:	:	kIx,	:
Terry	Terra	k1gFnPc4	Terra
Pratchett	Pratchett	k1gInSc4	Pratchett
Portfolio	portfolio	k1gNnSc1	portfolio
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Discworld	Discworld	k1gInSc1	Discworld
Portfolio	portfolio	k1gNnSc1	portfolio
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
kreseb	kresba	k1gFnPc2	kresba
Paula	Paul	k1gMnSc2	Paul
Kidbyho	Kidby	k1gMnSc2	Kidby
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
Zeměplochy	Zeměploch	k1gInPc7	Zeměploch
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Art	Art	k1gMnSc1	Art
of	of	k?	of
Discworld	Discworld	k1gMnSc1	Discworld
<g/>
)	)	kIx)	)
–	–	k?	–
výpravná	výpravný	k2eAgFnSc1d1	výpravná
obrazová	obrazový	k2eAgFnSc1d1	obrazová
publikace	publikace	k1gFnSc1	publikace
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Paula	Paul	k1gMnSc2	Paul
Kidbyho	Kidby	k1gMnSc2	Kidby
<g/>
.	.	kIx.	.
</s>
<s>
Kuchařka	kuchařka	k1gFnSc1	kuchařka
Stařenky	stařenka	k1gFnSc2	stařenka
Oggové	Oggová	k1gFnSc2	Oggová
(	(	kIx(	(
<g/>
Nanny	Nanen	k2eAgFnPc1d1	Nanna
Ogg	Ogg	k1gFnPc1	Ogg
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cookbook	Cookbook	k1gInSc1	Cookbook
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
zeměplošských	zeměplošský	k2eAgInPc2d1	zeměplošský
receptů	recept	k1gInPc2	recept
a	a	k8xC	a
"	"	kIx"	"
<g/>
rad	rad	k1gInSc1	rad
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
je	být	k5eAaImIp3nS	být
etiketa	etiketa	k1gFnSc1	etiketa
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
květin	květina	k1gFnPc2	květina
atd.	atd.	kA	atd.
Napsána	napsat	k5eAaPmNgFnS	napsat
Pratchettem	Pratchetto	k1gNnSc7	Pratchetto
<g/>
,	,	kIx,	,
Stephenem	Stephen	k1gMnSc7	Stephen
Briggsem	Briggs	k1gMnSc7	Briggs
a	a	k8xC	a
Tinou	Tina	k1gFnSc7	Tina
Hannan	Hannana	k1gFnPc2	Hannana
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c4	po
Zeměploše	Zeměploš	k1gMnPc4	Zeměploš
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
Discworld	Discworld	k1gMnSc1	Discworld
Companion	Companion	k1gInSc1	Companion
<g/>
)	)	kIx)	)
–	–	k?	–
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c4	o
Zeměploše	Zeměploš	k1gMnPc4	Zeměploš
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Pratchettem	Pratchetto	k1gNnSc7	Pratchetto
a	a	k8xC	a
Stephenem	Stephen	k1gMnSc7	Stephen
Briggsem	Briggs	k1gMnSc7	Briggs
<g/>
.	.	kIx.	.
</s>
<s>
Zeměplošský	Zeměplošský	k2eAgInSc1d1	Zeměplošský
almanach	almanach	k1gInSc1	almanach
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Discworld	Discworld	k1gMnSc1	Discworld
Almanak	Almanak	k1gMnSc1	Almanak
<g/>
)	)	kIx)	)
–	–	k?	–
almanach	almanach	k1gInSc1	almanach
na	na	k7c4	na
zeměplošský	zeměplošský	k2eAgInSc4d1	zeměplošský
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Kuchařky	kuchařka	k1gFnSc2	kuchařka
<g/>
,	,	kIx,	,
zhotoven	zhotoven	k2eAgInSc1d1	zhotoven
Pratchettem	Pratchett	k1gMnSc7	Pratchett
a	a	k8xC	a
Bernardem	Bernard	k1gMnSc7	Bernard
Pearsonem	Pearson	k1gMnSc7	Pearson
<g/>
.	.	kIx.	.
</s>
<s>
Kdepak	kdepak	k9	kdepak
je	být	k5eAaImIp3nS	být
má	můj	k3xOp1gFnSc1	můj
kravička	kravička	k1gFnSc1	kravička
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Where	Wher	k1gInSc5	Wher
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Cow	Cow	k1gMnSc5	Cow
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
zeměplošská	zeměplošský	k2eAgFnSc1d1	zeměplošská
ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
dětská	dětský	k2eAgFnSc1d1	dětská
kniha	kniha	k1gFnSc1	kniha
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
v	v	k7c6	v
Buchu	buch	k1gInSc6	buch
od	od	k7c2	od
Pratchetta	Pratchett	k1gInSc2	Pratchett
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
od	od	k7c2	od
Melvyna	Melvyn	k1gInSc2	Melvyn
Granta	Grant	k1gInSc2	Grant
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
<g/>
:	:	kIx,	:
Ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
scénář	scénář	k1gInSc1	scénář
(	(	kIx(	(
<g/>
Hogfather	Hogfathra	k1gFnPc2	Hogfathra
<g/>
:	:	kIx,	:
The	The	k1gFnPc1	The
Illustrated	Illustrated	k1gInSc4	Illustrated
Screenplay	Screenplaa	k1gFnSc2	Screenplaa
<g/>
)	)	kIx)	)
–	–	k?	–
scénář	scénář	k1gInSc4	scénář
filmu	film	k1gInSc2	film
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
z	z	k7c2	z
filmu	film	k1gInSc2	film
a	a	k8xC	a
nákresy	nákres	k1gInPc1	nákres
kostýmů	kostým	k1gInPc2	kostým
<g/>
,	,	kIx,	,
kulis	kulisa	k1gFnPc2	kulisa
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Stráže	stráž	k1gFnSc2	stráž
<g/>
!	!	kIx.	!
</s>
<s>
Stráže	stráž	k1gFnPc1	stráž
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Guards	Guardsa	k1gFnPc2	Guardsa
<g/>
!	!	kIx.	!
</s>
<s>
Guards	Guards	k6eAd1	Guards
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
komiksová	komiksový	k2eAgFnSc1d1	komiksová
verze	verze	k1gFnSc1	verze
románu	román	k1gInSc2	román
Stráže	stráž	k1gFnSc2	stráž
<g/>
!	!	kIx.	!
</s>
<s>
Stráže	stráž	k1gFnPc1	stráž
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
ilustrováno	ilustrován	k2eAgNnSc1d1	ilustrováno
Grahamem	Graham	k1gMnSc7	Graham
Higginsem	Higgins	k1gMnSc7	Higgins
<g/>
.	.	kIx.	.
</s>
<s>
Lu-Tzeho	Lu-Tzeze	k6eAd1	Lu-Tzeze
ročenka	ročenka	k1gFnSc1	ročenka
osvícení	osvícení	k1gNnPc2	osvícení
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
LU-TSE	LU-TSE	k1gFnSc2	LU-TSE
<g/>
'	'	kIx"	'
<g/>
S	s	k7c7	s
yearbook	yearbook	k1gInSc1	yearbook
of	of	k?	of
enlightenment	enlightenment	k1gInSc1	enlightenment
<g/>
)	)	kIx)	)
–	–	k?	–
diář	diář	k1gInSc1	diář
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Paula	Paul	k1gMnSc2	Paul
Kidbyho	Kidby	k1gMnSc2	Kidby
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ze	z	k7c2	z
Zeměplošských	Zeměplošský	k2eAgInPc2d1	Zeměplošský
diářů	diář	k1gInPc2	diář
a	a	k8xC	a
ročenek	ročenka	k1gFnPc2	ročenka
1998-2007	[number]	k4	1998-2007
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
–	–	k?	–
kompletní	kompletní	k2eAgFnSc1d1	kompletní
informace	informace	k1gFnSc1	informace
a	a	k8xC	a
ilustrace	ilustrace	k1gFnPc1	ilustrace
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
zeměplošských	zeměplošský	k2eAgInPc2d1	zeměplošský
diářů	diář	k1gInPc2	diář
<g/>
,	,	kIx,	,
vydaných	vydaný	k2eAgMnPc2d1	vydaný
anglicky	anglicky	k6eAd1	anglicky
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1998	[number]	k4	1998
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Paul	Paula	k1gFnPc2	Paula
Kidby	Kidba	k1gFnSc2	Kidba
<g/>
.	.	kIx.	.
</s>
<s>
Folklor	folklor	k1gInSc1	folklor
Zeměplochy	Zeměploch	k1gInPc1	Zeměploch
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Folklore	folklor	k1gInSc5	folklor
of	of	k?	of
Discworld	Discworldo	k1gNnPc2	Discworldo
<g/>
)	)	kIx)	)
–	–	k?	–
Pratchett	Pratchett	k1gMnSc1	Pratchett
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
folkloristkou	folkloristka	k1gFnSc7	folkloristka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Simpsonovou	Simpsonový	k2eAgFnSc4d1	Simpsonová
popisuje	popisovat	k5eAaImIp3nS	popisovat
mýty	mýtus	k1gInPc4	mýtus
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
na	na	k7c4	na
Zeměploše	Zeměploš	k1gMnPc4	Zeměploš
Překlad	překlad	k1gInSc1	překlad
Jana	Jan	k1gMnSc2	Jan
Kantůrka	kantůrek	k1gMnSc2	kantůrek
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
možná	možná	k9	možná
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
značnou	značný	k2eAgFnSc4d1	značná
oblibu	obliba	k1gFnSc4	obliba
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
Zeměplochy	Zeměploch	k1gInPc1	Zeměploch
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
obdivovaných	obdivovaný	k2eAgFnPc2d1	obdivovaná
věcí	věc	k1gFnPc2	věc
jsou	být	k5eAaImIp3nP	být
jména	jméno	k1gNnPc4	jméno
postav	postava	k1gFnPc2	postava
<g/>
;	;	kIx,	;
v	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
však	však	k9	však
u	u	k7c2	u
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
postav	postava	k1gFnPc2	postava
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
knihách	kniha	k1gFnPc6	kniha
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
překlady	překlad	k1gInPc4	překlad
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
podívat	podívat	k5eAaPmF	podívat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
zeměplošských	zeměplošský	k2eAgFnPc2d1	zeměplošská
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zeměplošských	zeměplošský	k2eAgInPc2d1	zeměplošský
románů	román	k1gInPc2	román
byla	být	k5eAaImAgFnS	být
zdramatizována	zdramatizován	k2eAgFnSc1d1	zdramatizována
Stephenem	Stephen	k1gInSc7	Stephen
Briggsem	Briggs	k1gInSc7	Briggs
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
divadelní	divadelní	k2eAgInPc4d1	divadelní
scénáře	scénář	k1gInPc4	scénář
vycházely	vycházet	k5eAaImAgFnP	vycházet
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uvádělo	uvádět	k5eAaImAgNnS	uvádět
Zeměplochu	Zeměploch	k1gMnSc3	Zeměploch
pražské	pražský	k2eAgNnSc1d1	Pražské
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
hrálo	hrát	k5eAaImAgNnS	hrát
inscenaci	inscenace	k1gFnSc4	inscenace
Soudné	soudný	k2eAgFnSc2d1	soudná
sestry	sestra	k1gFnSc2	sestra
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
–	–	k?	–
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
i	i	k9	i
Terry	Terr	k1gMnPc7	Terr
Pratchett	Pratchetta	k1gFnPc2	Pratchetta
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
záznam	záznam	k1gInSc1	záznam
hry	hra	k1gFnSc2	hra
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
inscenaci	inscenace	k1gFnSc6	inscenace
Maškaráda	maškaráda	k1gFnSc1	maškaráda
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
–	–	k?	–
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
i	i	k9	i
Terry	Terr	k1gInPc1	Terr
Pratchett	Pratchetta	k1gFnPc2	Pratchetta
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
záznam	záznam	k1gInSc1	záznam
hry	hra	k1gFnSc2	hra
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
VII	VII	kA	VII
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
Grand	grand	k1gMnSc1	grand
festival	festival	k1gInSc1	festival
smíchu	smích	k1gInSc2	smích
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Režisérkou	režisérka	k1gFnSc7	režisérka
obou	dva	k4xCgInPc2	dva
představení	představení	k1gNnPc2	představení
byla	být	k5eAaImAgFnS	být
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
profesionální	profesionální	k2eAgFnSc1d1	profesionální
divadelní	divadelní	k2eAgFnSc1d1	divadelní
zeměplošská	zeměplošský	k2eAgFnSc1d1	zeměplošská
inscenace	inscenace	k1gFnSc1	inscenace
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byla	být	k5eAaImAgNnP	být
odehrána	odehrát	k5eAaPmNgNnP	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Zinscenován	zinscenovat	k5eAaPmNgInS	zinscenovat
byl	být	k5eAaImAgInS	být
i	i	k9	i
román	román	k1gInSc1	román
Soudné	soudný	k2eAgFnSc2d1	soudná
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
derniéra	derniéra	k1gFnSc1	derniéra
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
má	mít	k5eAaImIp3nS	mít
Divadlo	divadlo	k1gNnSc4	divadlo
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
Maškarádu	maškaráda	k1gFnSc4	maškaráda
aneb	aneb	k?	aneb
Fantoma	Fantoma	k1gFnSc1	Fantoma
Opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Ankh-Morporkské	Ankh-Morporkská	k1gFnSc2	Ankh-Morporkská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
soubory	soubor	k1gInPc1	soubor
uvedly	uvést	k5eAaPmAgInP	uvést
zeměplošské	zeměplošský	k2eAgFnPc4d1	zeměplošská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Komorní	komorní	k2eAgFnSc1d1	komorní
scéna	scéna	k1gFnSc1	scéna
Aréna	aréna	k1gFnSc1	aréna
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
představila	představit	k5eAaPmAgFnS	představit
Morta	Morta	k1gFnSc1	Morta
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
Moravské	moravský	k2eAgNnSc1d1	Moravské
divadlo	divadlo	k1gNnSc1	divadlo
uvedlo	uvést	k5eAaPmAgNnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Maškarádu	maškaráda	k1gFnSc4	maškaráda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Muže	muž	k1gMnSc4	muž
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
inscenace	inscenace	k1gFnSc1	inscenace
Soudné	soudný	k2eAgFnSc2d1	soudná
sestry	sestra	k1gFnSc2	sestra
uvedena	uvést	k5eAaPmNgFnS	uvést
Západočeským	západočeský	k2eAgNnSc7d1	Západočeské
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jakuba	Jakub	k1gMnSc2	Jakub
Vašíčka	Vašíček	k1gMnSc2	Vašíček
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
inscenace	inscenace	k1gFnSc1	inscenace
českých	český	k2eAgInPc2d1	český
souborů	soubor	k1gInPc2	soubor
<g/>
:	:	kIx,	:
Stráže	stráž	k1gFnSc2	stráž
<g/>
!	!	kIx.	!
</s>
<s>
Stráže	stráž	k1gFnPc1	stráž
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
představení	představení	k1gNnPc4	představení
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Parconu	Parcon	k1gInSc2	Parcon
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kočovné	kočovný	k2eAgFnSc2d1	kočovná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
Richarda	Richard	k1gMnSc2	Richard
Klíčníka	klíčník	k1gMnSc2	klíčník
<g/>
,	,	kIx,	,
premiéra	premiér	k1gMnSc2	premiér
i	i	k8xC	i
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
.	.	kIx.	.
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
-	-	kIx~	-
představení	představení	k1gNnSc1	představení
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Euroconu	Eurocon	k1gInSc2	Eurocon
/	/	kIx~	/
Parconu	Parcon	k1gInSc2	Parcon
/	/	kIx~	/
Avalconu	Avalcon	k1gInSc2	Avalcon
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kočovné	kočovný	k2eAgFnSc2d1	kočovná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
Richarda	Richard	k1gMnSc2	Richard
Klíčníka	klíčník	k1gMnSc2	klíčník
<g/>
.	.	kIx.	.
</s>
<s>
Trollí	Trollet	k5eAaImIp3nS	Trollet
most	most	k1gInSc1	most
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc2d1	divadelní
inscenace	inscenace	k1gFnSc2	inscenace
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
Taurconu	Taurcon	k1gInSc2	Taurcon
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Festivalu	festival	k1gInSc2	festival
Fantazie	fantazie	k1gFnSc2	fantazie
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kočovné	kočovný	k2eAgFnSc2d1	kočovná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
bez	bez	k7c2	bez
kastelána	kastelán	k1gMnSc2	kastelán
Stráže	stráž	k1gFnSc2	stráž
<g/>
!	!	kIx.	!
</s>
<s>
Stráže	stráž	k1gFnPc1	stráž
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc2d1	divadelní
inscenace	inscenace	k1gFnSc2	inscenace
souboru	soubor	k1gInSc2	soubor
Ty-já-tr	Tyár	k1gInSc1	Ty-já-tr
NAČERNO	načerno	k6eAd1	načerno
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
aneb	aneb	k?	aneb
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
-	-	kIx~	-
ochotnická	ochotnický	k2eAgFnSc1d1	ochotnická
inscenace	inscenace	k1gFnSc1	inscenace
Divadýlka	Divadýlka	k?	Divadýlka
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
ze	z	k7c2	z
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Discworld	Discworld	k1gInSc1	Discworld
–	–	k?	–
krátký	krátký	k2eAgInSc1d1	krátký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
studiem	studio	k1gNnSc7	studio
Cosgrove	Cosgrov	k1gInSc5	Cosgrov
Hall	Hallum	k1gNnPc2	Hallum
pro	pro	k7c4	pro
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
adaptace	adaptace	k1gFnSc1	adaptace
části	část	k1gFnSc2	část
románu	román	k1gInSc2	román
Sekáč	sekáč	k1gInSc1	sekáč
<g/>
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
Music	Musice	k1gInPc2	Musice
–	–	k?	–
sedmidílný	sedmidílný	k2eAgInSc1d1	sedmidílný
animovaný	animovaný	k2eAgInSc1d1	animovaný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
studiem	studio	k1gNnSc7	studio
Cosgrove	Cosgrov	k1gInSc5	Cosgrov
Hall	Hallum	k1gNnPc2	Hallum
pro	pro	k7c4	pro
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
Těžké	těžký	k2eAgNnSc1d1	těžké
melodično	melodično	k1gNnSc1	melodično
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
epizoda	epizoda	k1gFnSc1	epizoda
vysílána	vysílán	k2eAgFnSc1d1	vysílána
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Wyrd	Wyrd	k6eAd1	Wyrd
Sisters	Sisters	k1gInSc1	Sisters
–	–	k?	–
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
animovaná	animovaný	k2eAgFnSc1d1	animovaná
televizní	televizní	k2eAgFnSc1d1	televizní
minisérie	minisérie	k1gFnSc1	minisérie
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
studiem	studio	k1gNnSc7	studio
Cosgrove	Cosgrov	k1gInSc5	Cosgrov
Hall	Hallum	k1gNnPc2	Hallum
pro	pro	k7c4	pro
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
Soudné	soudný	k2eAgFnSc2d1	soudná
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
epizoda	epizoda	k1gFnSc1	epizoda
vysílána	vysílán	k2eAgFnSc1d1	vysílána
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
dvojdílný	dvojdílný	k2eAgInSc4d1	dvojdílný
film	film	k1gInSc4	film
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
<g/>
,	,	kIx,	,
vysílán	vysílán	k2eAgMnSc1d1	vysílán
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Sky	Sky	k1gFnSc2	Sky
<g/>
1	[number]	k4	1
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
–	–	k?	–
dvojdílný	dvojdílný	k2eAgInSc4d1	dvojdílný
film	film	k1gInSc4	film
podle	podle	k7c2	podle
románů	román	k1gInPc2	román
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
Lehké	Lehké	k2eAgNnSc1d1	Lehké
fantastično	fantastično	k1gNnSc1	fantastično
<g/>
,	,	kIx,	,
vysílán	vysílán	k2eAgInSc1d1	vysílán
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Sky	Sky	k1gMnSc2	Sky
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
česká	český	k2eAgFnSc1d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
premiéra	premiéra	k1gFnSc1	premiéra
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Nova	nova	k1gFnSc1	nova
Cinema	Cinema	k1gFnSc1	Cinema
Zaslaná	zaslaný	k2eAgFnSc1d1	zaslaná
pošta	pošta	k1gFnSc1	pošta
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
–	–	k?	–
dvojdílný	dvojdílný	k2eAgInSc4d1	dvojdílný
film	film	k1gInSc4	film
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Zaslaná	zaslaný	k2eAgFnSc1d1	zaslaná
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
vysílán	vysílán	k2eAgInSc1d1	vysílán
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Sky	Sky	k1gMnSc2	Sky
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
HBO	HBO	kA	HBO
Dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
fanouškovské	fanouškovský	k2eAgInPc1d1	fanouškovský
adaptace	adaptace	k1gFnPc4	adaptace
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
fanouškovské	fanouškovský	k2eAgInPc4d1	fanouškovský
krátké	krátký	k2eAgInPc4d1	krátký
filmy	film	k1gInPc4	film
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Zeměplochy	Zeměploch	k1gInPc4	Zeměploch
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
skladatel	skladatel	k1gMnSc1	skladatel
Dave	Dav	k1gInSc5	Dav
Greenslade	Greenslad	k1gInSc5	Greenslad
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ze	z	k7c2	z
Zeměplochy	Zeměploch	k1gInPc1	Zeměploch
(	(	kIx(	(
<g/>
From	From	k1gInSc1	From
the	the	k?	the
Discworld	Discworld	k1gInSc1	Discworld
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgFnSc1d1	obsahující
14	[number]	k4	14
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgFnPc7	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
známá	známý	k2eAgFnSc1d1	známá
skladba	skladba	k1gFnSc1	skladba
Kouzelníkova	kouzelníkův	k2eAgFnSc1d1	Kouzelníkova
hůl	hůl	k1gFnSc1	hůl
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
knoflík	knoflík	k1gInSc1	knoflík
(	(	kIx(	(
<g/>
A	a	k9	a
Wizard	Wizard	k1gInSc1	Wizard
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Staff	Staff	k1gInSc1	Staff
Has	hasit	k5eAaImRp2nS	hasit
a	a	k8xC	a
Knob	Knob	k1gMnSc1	Knob
on	on	k3xPp3gMnSc1	on
the	the	k?	the
End	End	k1gMnPc7	End
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Colour	Colour	k1gMnSc1	Colour
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
(	(	kIx(	(
<g/>
Sinclair	Sinclair	k1gInSc1	Sinclair
Spectrum	Spectrum	k1gNnSc1	Spectrum
<g/>
)	)	kIx)	)
Discworld	Discworld	k1gMnSc1	Discworld
(	(	kIx(	(
<g/>
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
Discworld	Discworld	k1gInSc1	Discworld
2	[number]	k4	2
(	(	kIx(	(
<g/>
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
Discworld	Discworld	k1gMnSc1	Discworld
Noir	Noir	k1gMnSc1	Noir
(	(	kIx(	(
<g/>
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
PlayStation	PlayStation	k1gInSc1	PlayStation
<g/>
)	)	kIx)	)
Discworld	Discworld	k1gInSc1	Discworld
MUD	MUD	kA	MUD
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
úžasné	úžasný	k2eAgInPc1d1	úžasný
Zeměplochy	Zeměploch	k1gInPc1	Zeměploch
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
Další	další	k2eAgFnPc1d1	další
české	český	k2eAgFnPc4d1	Česká
fanouškovské	fanouškovský	k2eAgFnPc4d1	fanouškovská
stránky	stránka	k1gFnPc4	stránka
Nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
světový	světový	k2eAgInSc4d1	světový
fanouškovský	fanouškovský	k2eAgInSc4d1	fanouškovský
server	server	k1gInSc4	server
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
