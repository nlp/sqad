<s>
Epica	Epica	k1gFnSc1	Epica
je	být	k5eAaImIp3nS	být
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
symfonicmetalová	symfonicmetalový	k2eAgFnSc1d1	symfonicmetalový
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
operní	operní	k2eAgInPc4d1	operní
prvky	prvek	k1gInPc4	prvek
kombinované	kombinovaný	k2eAgInPc4d1	kombinovaný
s	s	k7c7	s
typickým	typický	k2eAgInSc7d1	typický
death	death	k1gInSc4	death
metalovým	metalový	k2eAgInSc7d1	metalový
chrapotem	chrapot	k1gInSc7	chrapot
(	(	kIx(	(
<g/>
growling	growling	k1gInSc1	growling
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
