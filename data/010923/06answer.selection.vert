<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
postupimská	postupimský	k2eAgFnSc1d1	Postupimská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
představitelé	představitel	k1gMnPc1	představitel
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
velmocí	velmoc	k1gFnPc2	velmoc
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
obsahující	obsahující	k2eAgFnSc2d1	obsahující
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c4	o
denacifikaci	denacifikace	k1gFnSc4	denacifikace
a	a	k8xC	a
demilitarizaci	demilitarizace	k1gFnSc4	demilitarizace
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
