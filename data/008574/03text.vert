<p>
<s>
Pixel	pixel	k1gInSc1	pixel
(	(	kIx(	(
<g/>
zkrácení	zkrácení	k1gNnSc1	zkrácení
anglických	anglický	k2eAgNnPc2d1	anglické
slov	slovo	k1gNnPc2	slovo
picture	pictur	k1gMnSc5	pictur
element	element	k1gInSc1	element
<g/>
,	,	kIx,	,
obrazový	obrazový	k2eAgInSc1d1	obrazový
prvek	prvek	k1gInSc1	prvek
<g/>
;	;	kIx,	;
zkráceně	zkráceně	k6eAd1	zkráceně
px	px	k?	px
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
pel	pel	k1gInSc1	pel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
(	(	kIx(	(
<g/>
bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
<g/>
)	)	kIx)	)
jednotka	jednotka	k1gFnSc1	jednotka
digitální	digitální	k2eAgFnSc2d1	digitální
rastrové	rastrový	k2eAgFnSc2d1	rastrová
(	(	kIx(	(
<g/>
bitmapové	bitmapový	k2eAgFnSc2d1	bitmapová
<g/>
)	)	kIx)	)
grafiky	grafika	k1gFnSc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
svítící	svítící	k2eAgInSc1d1	svítící
bod	bod	k1gInSc1	bod
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
charakterizovaný	charakterizovaný	k2eAgInSc4d1	charakterizovaný
jasem	jas	k1gInSc7	jas
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
RGB	RGB	kA	RGB
či	či	k8xC	či
CMYK	CMYK	kA	CMYK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Body	bod	k1gInPc4	bod
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
tvoří	tvořit	k5eAaImIp3nP	tvořit
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
pixel	pixel	k1gInSc4	pixel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jednoznačně	jednoznačně	k6eAd1	jednoznačně
identifikovat	identifikovat	k5eAaBmF	identifikovat
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
barevných	barevný	k2eAgFnPc2d1	barevná
obrazovek	obrazovka	k1gFnPc2	obrazovka
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
pixel	pixel	k1gInSc1	pixel
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
svítících	svítící	k2eAgInPc2d1	svítící
obrazců	obrazec	k1gInPc2	obrazec
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
základním	základní	k2eAgFnPc3d1	základní
barvám	barva	k1gFnPc3	barva
-	-	kIx~	-
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezenému	omezený	k2eAgNnSc3d1	omezené
množství	množství	k1gNnSc3	množství
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
omezené	omezený	k2eAgFnSc6d1	omezená
frekvenci	frekvence	k1gFnSc6	frekvence
vykreslování	vykreslování	k1gNnSc2	vykreslování
obrazu	obraz	k1gInSc2	obraz
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
zobrazování	zobrazování	k1gNnSc6	zobrazování
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
aliasing	aliasing	k1gInSc1	aliasing
<g/>
,	,	kIx,	,
moaré	moaré	k1gNnPc1	moaré
<g/>
,	,	kIx,	,
neostrosti	neostrost	k1gFnPc1	neostrost
<g/>
,	,	kIx,	,
mozaikové	mozaikový	k2eAgNnSc1d1	mozaikové
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
informací	informace	k1gFnPc2	informace
při	při	k7c6	při
zmenšování	zmenšování	k1gNnSc6	zmenšování
<g/>
,	,	kIx,	,
zvětšování	zvětšování	k1gNnSc1	zvětšování
nebo	nebo	k8xC	nebo
otáčení	otáčení	k1gNnSc1	otáčení
obrazu	obraz	k1gInSc2	obraz
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
pixelu	pixel	k1gInSc2	pixel
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
typu	typ	k1gInSc6	typ
monitoru	monitor	k1gInSc2	monitor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
analogových	analogový	k2eAgInPc2d1	analogový
typů	typ	k1gInPc2	typ
lze	lze	k6eAd1	lze
velikost	velikost	k1gFnSc4	velikost
pixelu	pixel	k1gInSc2	pixel
měnit	měnit	k5eAaImF	měnit
změnou	změna	k1gFnSc7	změna
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
LCD	LCD	kA	LCD
obrazovky	obrazovka	k1gFnPc1	obrazovka
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
mají	mít	k5eAaImIp3nP	mít
počet	počet	k1gInSc4	počet
fyzických	fyzický	k2eAgInPc2d1	fyzický
pixelů	pixel	k1gInPc2	pixel
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
nativní	nativní	k2eAgNnSc1d1	nativní
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
pevně	pevně	k6eAd1	pevně
vázaný	vázaný	k2eAgInSc1d1	vázaný
na	na	k7c4	na
používané	používaný	k2eAgNnSc4d1	používané
rozlišení	rozlišení	k1gNnSc4	rozlišení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
1024	[number]	k4	1024
<g/>
×	×	k?	×
<g/>
768	[number]	k4	768
-	-	kIx~	-
standard	standard	k1gInSc1	standard
XGA	XGA	kA	XGA
<g/>
)	)	kIx)	)
a	a	k8xC	a
zobrazování	zobrazování	k1gNnSc2	zobrazování
jiného	jiný	k2eAgNnSc2d1	jiné
rozlišení	rozlišení	k1gNnSc2	rozlišení
u	u	k7c2	u
takového	takový	k3xDgInSc2	takový
monitoru	monitor	k1gInSc2	monitor
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
deformaci	deformace	k1gFnSc3	deformace
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
počítačové	počítačový	k2eAgInPc1d1	počítačový
pixely	pixel	k1gInPc1	pixel
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
přepočítávány	přepočítáván	k2eAgFnPc1d1	přepočítávána
a	a	k8xC	a
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
přerozdělovány	přerozdělován	k2eAgInPc1d1	přerozdělován
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
počet	počet	k1gInSc4	počet
"	"	kIx"	"
<g/>
fyzických	fyzický	k2eAgInPc2d1	fyzický
pixelů	pixel	k1gInPc2	pixel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
režimech	režim	k1gInPc6	režim
má	mít	k5eAaImIp3nS	mít
obrazovka	obrazovka	k1gFnSc1	obrazovka
rozlišení	rozlišení	k1gNnSc2	rozlišení
od	od	k7c2	od
zhruba	zhruba	k6eAd1	zhruba
640	[number]	k4	640
<g/>
×	×	k?	×
<g/>
480	[number]	k4	480
po	po	k7c4	po
1600	[number]	k4	1600
<g/>
×	×	k?	×
<g/>
1200	[number]	k4	1200
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
patnáctipalcového	patnáctipalcový	k2eAgInSc2d1	patnáctipalcový
monitoru	monitor	k1gInSc2	monitor
při	při	k7c6	při
rozlišení	rozlišení	k1gNnSc6	rozlišení
1024	[number]	k4	1024
<g/>
×	×	k?	×
<g/>
768	[number]	k4	768
představuje	představovat	k5eAaImIp3nS	představovat
velikost	velikost	k1gFnSc1	velikost
jednoho	jeden	k4xCgInSc2	jeden
pixelu	pixel	k1gInSc2	pixel
sotva	sotva	k6eAd1	sotva
0,3	[number]	k4	0,3
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgNnSc1d1	maximální
možné	možný	k2eAgNnSc1d1	možné
rozlišení	rozlišení	k1gNnSc1	rozlišení
monitoru	monitor	k1gInSc2	monitor
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
"	"	kIx"	"
<g/>
bod	bod	k1gInSc4	bod
na	na	k7c4	na
palec	palec	k1gInSc4	palec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DPI	dpi	kA	dpi
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
dots	dots	k6eAd1	dots
per	pero	k1gNnPc2	pero
inch	inch	k1gInSc1	inch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Megapixel	Megapixlo	k1gNnPc2	Megapixlo
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
megapixel	megapixel	k1gInSc1	megapixel
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
milión	milión	k4xCgInSc1	milión
(	(	kIx(	(
<g/>
220	[number]	k4	220
=	=	kIx~	=
1048576	[number]	k4	1048576
<g/>
)	)	kIx)	)
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Zkráceně	zkráceně	k6eAd1	zkráceně
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
Mpx	Mpx	k1gFnSc1	Mpx
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
jen	jen	k9	jen
MP	MP	kA	MP
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
nejen	nejen	k6eAd1	nejen
počet	počet	k1gInSc1	počet
pixelů	pixel	k1gInPc2	pixel
v	v	k7c6	v
obrázku	obrázek	k1gInSc6	obrázek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnSc7d1	maximální
<g/>
)	)	kIx)	)
rozlišení	rozlišení	k1gNnSc1	rozlišení
digitálního	digitální	k2eAgInSc2d1	digitální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
citlivých	citlivý	k2eAgFnPc2d1	citlivá
buněk	buňka	k1gFnPc2	buňka
na	na	k7c6	na
obrazovém	obrazový	k2eAgInSc6d1	obrazový
senzoru	senzor	k1gInSc6	senzor
nebo	nebo	k8xC	nebo
počet	počet	k1gInSc1	počet
obrazových	obrazový	k2eAgFnPc2d1	obrazová
buněk	buňka	k1gFnPc2	buňka
na	na	k7c6	na
digitálním	digitální	k2eAgInSc6d1	digitální
displeji	displej	k1gInSc6	displej
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
s	s	k7c7	s
čipem	čip	k1gInSc7	čip
2048	[number]	k4	2048
<g/>
×	×	k?	×
<g/>
1536	[number]	k4	1536
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přesně	přesně	k6eAd1	přesně
"	"	kIx"	"
<g/>
3	[number]	k4	3
megapixelům	megapixel	k1gMnPc3	megapixel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2048	[number]	k4	2048
×	×	k?	×
1536	[number]	k4	1536
/	/	kIx~	/
220	[number]	k4	220
=	=	kIx~	=
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozlišení	rozlišení	k1gNnSc2	rozlišení
displeje	displej	k1gInSc2	displej
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
resolution	resolution	k1gInSc1	resolution
<g/>
)	)	kIx)	)
monitoru	monitor	k1gInSc2	monitor
nebo	nebo	k8xC	nebo
displeje	displej	k1gInSc2	displej
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
pixelů	pixel	k1gInPc2	pixel
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
maximální	maximální	k2eAgNnSc4d1	maximální
rozlišení	rozlišení	k1gNnSc4	rozlišení
obrazu	obraz	k1gInSc2	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
sloupců	sloupec	k1gInPc2	sloupec
(	(	kIx(	(
<g/>
horizontálně	horizontálně	k6eAd1	horizontálně
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
X	X	kA	X
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
vždy	vždy	k6eAd1	vždy
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
počet	počet	k1gInSc1	počet
řádků	řádek	k1gInPc2	řádek
(	(	kIx(	(
<g/>
vertikálně	vertikálně	k6eAd1	vertikálně
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Y	Y	kA	Y
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Momentálně	momentálně	k6eAd1	momentálně
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejpoužívanější	používaný	k2eAgNnSc1d3	nejpoužívanější
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1024	[number]	k4	1024
<g/>
×	×	k?	×
<g/>
768	[number]	k4	768
(	(	kIx(	(
<g/>
XGA	XGA	kA	XGA
<g/>
/	/	kIx~	/
<g/>
XVGA	XVGA	kA	XVGA
<g/>
,	,	kIx,	,
eXtended	eXtended	k1gMnSc1	eXtended
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1280	[number]	k4	1280
<g/>
×	×	k?	×
<g/>
800	[number]	k4	800
(	(	kIx(	(
<g/>
WXGA	WXGA	kA	WXGA
<g/>
,	,	kIx,	,
Wide	Wide	k1gNnSc1	Wide
XGA	XGA	kA	XGA
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
notebooků	notebook	k1gInPc2	notebook
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1366	[number]	k4	1366
<g/>
x	x	k?	x
<g/>
768	[number]	k4	768
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
notebooků	notebook	k1gInPc2	notebook
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1600	[number]	k4	1600
<g/>
×	×	k?	×
<g/>
1200	[number]	k4	1200
(	(	kIx(	(
<g/>
UXGA	UXGA	kA	UXGA
<g/>
,	,	kIx,	,
Ultra-eXtended	UltraXtended	k1gMnSc1	Ultra-eXtended
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
x	x	k?	x
<g/>
1080	[number]	k4	1080
FHD	FHD	kA	FHD
<g/>
,	,	kIx,	,
Full	Full	k1gInSc1	Full
HD	HD	kA	HD
nebo	nebo	k8xC	nebo
1080	[number]	k4	1080
<g/>
p	p	k?	p
</s>
</p>
<p>
<s>
2560	[number]	k4	2560
<g/>
x	x	k?	x
<g/>
1440	[number]	k4	1440
QHD	QHD	kA	QHD
<g/>
,	,	kIx,	,
QuadHD	QuadHD	k1gFnSc1	QuadHD
nebo	nebo	k8xC	nebo
WQHD	WQHD	kA	WQHD
</s>
</p>
<p>
<s>
3440	[number]	k4	3440
<g/>
x	x	k?	x
<g/>
1440	[number]	k4	1440
UWQHD	UWQHD	kA	UWQHD
<g/>
,	,	kIx,	,
Ultra-Wide	Ultra-Wid	k1gInSc5	Ultra-Wid
Quad	Quad	k1gInSc4	Quad
HDMnoho	HDMno	k1gMnSc2	HDMno
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
uživatelů	uživatel	k1gMnPc2	uživatel
CADu	CADus	k1gInSc2	CADus
a	a	k8xC	a
hráčů	hráč	k1gMnPc2	hráč
video	video	k1gNnSc4	video
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
rozlišení	rozlišení	k1gNnSc1	rozlišení
1600	[number]	k4	1600
<g/>
×	×	k?	×
<g/>
1200	[number]	k4	1200
(	(	kIx(	(
<g/>
UXGA	UXGA	kA	UXGA
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
odpovídající	odpovídající	k2eAgNnPc4d1	odpovídající
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
rozlišení	rozlišení	k1gNnSc1	rozlišení
obrazu	obraz	k1gInSc2	obraz
vyšší	vysoký	k2eAgNnSc1d2	vyšší
než	než	k8xS	než
fyzické	fyzický	k2eAgNnSc1d1	fyzické
rozlišení	rozlišení	k1gNnSc1	rozlišení
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
některé	některý	k3yIgInPc4	některý
systémy	systém	k1gInPc4	systém
využít	využít	k5eAaPmF	využít
virtuální	virtuální	k2eAgFnSc4d1	virtuální
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
digitální	digitální	k2eAgFnSc4d1	digitální
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
HDTV	HDTV	kA	HDTV
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
vertikální	vertikální	k2eAgNnSc1d1	vertikální
rozlišení	rozlišení	k1gNnSc1	rozlišení
720	[number]	k4	720
nebo	nebo	k8xC	nebo
1080	[number]	k4	1080
řádků	řádek	k1gInPc2	řádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc4d1	další
významy	význam	k1gInPc4	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
program	program	k1gInSc1	program
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
rastrové	rastrový	k2eAgFnSc2d1	rastrová
grafiky	grafika	k1gFnSc2	grafika
</s>
</p>
<p>
<s>
český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
o	o	k7c6	o
grafice	grafika	k1gFnSc6	grafika
</s>
</p>
<p>
<s>
pixel	pixel	k1gInSc1	pixel
v	v	k7c6	v
CSS	CSS	kA	CSS
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
konvenčnímu	konvenční	k2eAgInSc3d1	konvenční
významu	význam	k1gInSc3	význam
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Barevná	barevný	k2eAgFnSc1d1	barevná
hloubka	hloubka	k1gFnSc1	hloubka
</s>
</p>
<p>
<s>
Bitmapová	bitmapový	k2eAgFnSc1d1	bitmapová
grafika	grafika	k1gFnSc1	grafika
</s>
</p>
<p>
<s>
Subpixel	Subpixel	k1gMnSc1	Subpixel
</s>
</p>
<p>
<s>
Texel	Texel	k1gMnSc1	Texel
</s>
</p>
<p>
<s>
Voxel	Voxel	k1gMnSc1	Voxel
</s>
</p>
<p>
<s>
Pixel	pixel	k1gInSc1	pixel
art	art	k?	art
</s>
</p>
<p>
<s>
Foveon	Foveon	k1gMnSc1	Foveon
X3	X3	k1gMnSc1	X3
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pixel	pixel	k1gInSc1	pixel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
