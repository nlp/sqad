<s>
Brusel	Brusel	k1gInSc1	Brusel
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Bruxelles	Bruxelles	k1gInSc1	Bruxelles
[	[	kIx(	[
<g/>
bʁ	bʁ	k?	bʁ
<g/>
.	.	kIx.	.
<g/>
sɛ	sɛ	k?	sɛ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Brussel	Brussel	k1gInSc1	Brussel
[	[	kIx(	[
<g/>
brʏ	brʏ	k?	brʏ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Brüssel	Brüssel	k1gInSc1	Brüssel
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Brussels	Brussels	k1gInSc1	Brussels
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
NATO	nato	k6eAd1	nato
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
institucí	instituce	k1gFnPc2	instituce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
neoficiálně	neoficiálně	k6eAd1	neoficiálně
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Vlámského	vlámský	k2eAgNnSc2d1	vlámské
společenství	společenství	k1gNnSc2	společenství
a	a	k8xC	a
Valonsko-bruselské	valonskoruselský	k2eAgFnSc2d1	valonsko-bruselský
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
Francouzského	francouzský	k2eAgNnSc2d1	francouzské
společenství	společenství	k1gNnSc2	společenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
založil	založit	k5eAaPmAgMnS	založit
osadu	osada	k1gFnSc4	osada
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
580	[number]	k4	580
sv.	sv.	kA	sv.
Guegerich	Guegericha	k1gFnPc2	Guegericha
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Géry	Géra	k1gFnSc2	Géra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
názvu	název	k1gInSc2	název
Brucsella	Brucsello	k1gNnSc2	Brucsello
či	či	k8xC	či
Broekzelle	Broekzelle	k1gNnSc2	Broekzelle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
ves	ves	k1gFnSc4	ves
v	v	k7c6	v
bažinách	bažina	k1gFnPc6	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Brusel	Brusel	k1gInSc1	Brusel
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
metropole	metropole	k1gFnSc1	metropole
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
uprostřed	uprostřed	k7c2	uprostřed
Belgického	belgický	k2eAgNnSc2d1	Belgické
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
poloze	poloha	k1gFnSc6	poloha
vzkvétal	vzkvétat	k5eAaImAgInS	vzkvétat
rozvoj	rozvoj	k1gInSc1	rozvoj
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ves	ves	k1gFnSc1	ves
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
v	v	k7c6	v
opevněné	opevněný	k2eAgFnSc6d1	opevněná
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
představovala	představovat	k5eAaImAgFnS	představovat
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jako	jako	k9	jako
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Brusel	Brusel	k1gInSc4	Brusel
bombardován	bombardován	k2eAgMnSc1d1	bombardován
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
poškození	poškození	k1gNnSc3	poškození
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
následné	následný	k2eAgFnSc6d1	následná
obnově	obnova	k1gFnSc6	obnova
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
přestavbě	přestavba	k1gFnSc3	přestavba
<g/>
.	.	kIx.	.
</s>
<s>
Zničena	zničen	k2eAgFnSc1d1	zničena
byla	být	k5eAaImAgFnS	být
zhruba	zhruba	k6eAd1	zhruba
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
veškeré	veškerý	k3xTgFnSc2	veškerý
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
domů	dům	k1gInPc2	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Grande	grand	k1gMnSc5	grand
Place	plac	k1gInSc6	plac
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Brusel	Brusel	k1gInSc1	Brusel
jako	jako	k8xC	jako
metropole	metropole	k1gFnSc1	metropole
mladého	mladý	k2eAgInSc2d1	mladý
belgického	belgický	k2eAgInSc2d1	belgický
státu	stát	k1gInSc2	stát
dynamicky	dynamicky	k6eAd1	dynamicky
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgFnP	konat
časté	častý	k2eAgFnPc1d1	častá
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
městu	město	k1gNnSc3	město
přineslo	přinést	k5eAaPmAgNnS	přinést
velkou	velký	k2eAgFnSc4d1	velká
prestiž	prestiž	k1gFnSc4	prestiž
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
dramatickému	dramatický	k2eAgInSc3d1	dramatický
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zakrytí	zakrytí	k1gNnSc2	zakrytí
řeky	řeka	k1gFnSc2	řeka
Senne	Senn	k1gInSc5	Senn
a	a	k8xC	a
vybudování	vybudování	k1gNnSc4	vybudování
třídy	třída	k1gFnSc2	třída
Anspach	Anspacha	k1gFnPc2	Anspacha
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc4	zavedení
železnice	železnice	k1gFnSc2	železnice
aj.	aj.	kA	aj.
Během	během	k7c2	během
obou	dva	k4xCgInPc2	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgInS	být
Brusel	Brusel	k1gInSc1	Brusel
ušetřen	ušetřit	k5eAaPmNgInS	ušetřit
válečného	válečný	k2eAgMnSc4d1	válečný
ničení	ničení	k1gNnSc3	ničení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
docházelo	docházet	k5eAaImAgNnS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
necitlivým	citlivý	k2eNgInPc3d1	necitlivý
zásahům	zásah	k1gInPc3	zásah
do	do	k7c2	do
historické	historický	k2eAgFnSc2d1	historická
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc4d1	známý
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
bruselizace	bruselizace	k1gFnSc2	bruselizace
<g/>
.	.	kIx.	.
</s>
<s>
Brusel	Brusel	k1gInSc1	Brusel
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
19	[number]	k4	19
obcí	obec	k1gFnPc2	obec
Bruselského	bruselský	k2eAgInSc2d1	bruselský
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
přibližně	přibližně	k6eAd1	přibližně
175	[number]	k4	175
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
32,61	[number]	k4	32,61
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Brusel	Brusel	k1gInSc1	Brusel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
často	často	k6eAd1	často
také	také	k9	také
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
celého	celý	k2eAgInSc2d1	celý
Bruselského	bruselský	k2eAgInSc2d1	bruselský
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
161	[number]	k4	161
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechny	všechen	k3xTgFnPc4	všechen
obce	obec	k1gFnPc4	obec
Bruselského	bruselský	k2eAgInSc2d1	bruselský
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
i	i	k9	i
Brusel	Brusel	k1gInSc1	Brusel
oficiálně	oficiálně	k6eAd1	oficiálně
dvojjazyčný	dvojjazyčný	k2eAgInSc1d1	dvojjazyčný
-	-	kIx~	-
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
nizozemština	nizozemština	k1gFnSc1	nizozemština
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
instituce	instituce	k1gFnPc1	instituce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
-	-	kIx~	-
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
centrálu	centrála	k1gFnSc4	centrála
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Berlaymont	Berlaymonta	k1gFnPc2	Berlaymonta
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
EU	EU	kA	EU
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Justus	Justus	k1gMnSc1	Justus
Lipsius	Lipsius	k1gMnSc1	Lipsius
<g/>
.	.	kIx.	.
</s>
<s>
Zasedají	zasedat	k5eAaImIp3nP	zasedat
zde	zde	k6eAd1	zde
také	také	k9	také
výbory	výbor	k1gInPc1	výbor
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
EP	EP	kA	EP
je	být	k5eAaImIp3nS	být
však	však	k9	však
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
a	a	k8xC	a
sekretariát	sekretariát	k1gInSc1	sekretariát
parlamentu	parlament	k1gInSc2	parlament
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
budovu	budova	k1gFnSc4	budova
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
jeho	jeho	k3xOp3gFnPc3	jeho
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
může	moct	k5eAaImIp3nS	moct
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
či	či	k8xC	či
politického	politický	k2eAgNnSc2d1	politické
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
)	)	kIx)	)
nezávisle	závisle	k6eNd1	závisle
zasedat	zasedat	k5eAaImF	zasedat
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
Výbor	výbor	k1gInSc1	výbor
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
sídlo	sídlo	k1gNnSc4	sídlo
Evropské	evropský	k2eAgFnSc2d1	Evropská
služby	služba	k1gFnSc2	služba
pro	pro	k7c4	pro
vnější	vnější	k2eAgFnSc4d1	vnější
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
EEAS	EEAS	kA	EEAS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
frankofonní	frankofonní	k2eAgFnSc1d1	frankofonní
(	(	kIx(	(
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
(	(	kIx(	(
<g/>
marockého	marocký	k2eAgMnSc2d1	marocký
<g/>
,	,	kIx,	,
tureckého	turecký	k2eAgMnSc2d1	turecký
<g/>
,	,	kIx,	,
balkánského	balkánský	k2eAgMnSc2d1	balkánský
<g/>
,	,	kIx,	,
polského	polský	k2eAgMnSc2d1	polský
<g/>
,	,	kIx,	,
afrického	africký	k2eAgInSc2d1	africký
a	a	k8xC	a
jiného	jiný	k2eAgInSc2d1	jiný
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyjádření	vyjádření	k1gNnPc2	vyjádření
Josepha	Joseph	k1gMnSc2	Joseph
De	De	k?	De
Kesela	Kesel	k1gMnSc2	Kesel
<g/>
,	,	kIx,	,
belgického	belgický	k2eAgMnSc2d1	belgický
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
praktikujících	praktikující	k2eAgMnPc2d1	praktikující
muslimů	muslim	k1gMnPc2	muslim
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
již	již	k6eAd1	již
19	[number]	k4	19
<g/>
%	%	kIx~	%
oproti	oproti	k7c3	oproti
12	[number]	k4	12
<g/>
%	%	kIx~	%
praktikujících	praktikující	k2eAgMnPc2d1	praktikující
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Grande-Place	Grande-Place	k1gFnSc2	Grande-Place
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Grote	grot	k1gInSc5	grot
Markt	Markt	k1gInSc1	Markt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historickým	historický	k2eAgNnSc7d1	historické
jádrem	jádro	k1gNnSc7	jádro
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
)	)	kIx)	)
jej	on	k3xPp3gMnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejpůvabnějších	půvabný	k2eAgNnPc2d3	nejpůvabnější
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
dominantou	dominanta	k1gFnSc7	dominanta
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
radnice	radnice	k1gFnSc1	radnice
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
96	[number]	k4	96
m	m	kA	m
vysokou	vysoký	k2eAgFnSc7d1	vysoká
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
Maison	Maison	k1gInSc1	Maison
du	du	k?	du
Roi	Roi	k1gFnSc1	Roi
/	/	kIx~	/
Broodhuis	Broodhuis	k1gInSc1	Broodhuis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
náměstí	náměstí	k1gNnSc2	náměstí
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
Seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1695	[number]	k4	1695
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
postavených	postavený	k2eAgInPc2d1	postavený
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
zničena	zničen	k2eAgFnSc1d1	zničena
střelbou	střelba	k1gFnSc7	střelba
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
francouzského	francouzský	k2eAgNnSc2d1	francouzské
vojska	vojsko	k1gNnSc2	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
maršála	maršál	k1gMnSc2	maršál
François	François	k1gFnSc2	François
de	de	k?	de
Villeroy	Villeroa	k1gFnSc2	Villeroa
<g/>
.	.	kIx.	.
</s>
<s>
Nepoškozena	poškozen	k2eNgFnSc1d1	nepoškozena
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
věž	věž	k1gFnSc1	věž
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
několik	několik	k4yIc4	několik
kamenných	kamenný	k2eAgFnPc2d1	kamenná
zdí	zeď	k1gFnPc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
cechovní	cechovní	k2eAgInPc4d1	cechovní
domy	dům	k1gInPc4	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
průčelí	průčelí	k1gNnSc1	průčelí
každého	každý	k3xTgInSc2	každý
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
spolu	spolu	k6eAd1	spolu
architektonicky	architektonicky	k6eAd1	architektonicky
harmonický	harmonický	k2eAgInSc4d1	harmonický
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Manneken	Manneken	k1gInSc1	Manneken
Pis	Pisa	k1gFnPc2	Pisa
(	(	kIx(	(
<g/>
soška	soška	k1gFnSc1	soška
čurajícího	čurající	k2eAgMnSc2d1	čurající
chlapečka	chlapeček	k1gMnSc2	chlapeček
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Sochu	socha	k1gFnSc4	socha
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sochař	sochař	k1gMnSc1	sochař
Jérôme	Jérôm	k1gInSc5	Jérôm
Duquesnoy	Duquesnoy	k1gInPc5	Duquesnoy
<g/>
.	.	kIx.	.
</s>
<s>
Manneken	Manneken	k1gInSc1	Manneken
Pis	Pisa	k1gFnPc2	Pisa
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
800	[number]	k4	800
oblečky	obleček	k1gInPc1	obleček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgMnPc4d1	umístěn
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
kroj	kroj	k1gInSc1	kroj
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaImNgInS	věnovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1698	[number]	k4	1698
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1745	[number]	k4	1745
sochu	socha	k1gFnSc4	socha
Manneken	Mannekna	k1gFnPc2	Mannekna
Pis	Pisa	k1gFnPc2	Pisa
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
daroval	darovat	k5eAaPmAgMnS	darovat
Bruselu	Brusel	k1gInSc6	Brusel
kroj	kroj	k1gInSc4	kroj
ze	z	k7c2	z
zlatého	zlatý	k2eAgInSc2d1	zlatý
brokátu	brokát	k1gInSc2	brokát
<g/>
,	,	kIx,	,
zdobený	zdobený	k2eAgInSc1d1	zdobený
křížem	kříž	k1gInSc7	kříž
svatého	svatý	k2eAgMnSc2d1	svatý
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
sošku	soška	k1gFnSc4	soška
zcizil	zcizit	k5eAaPmAgMnS	zcizit
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
zničil	zničit	k5eAaPmAgMnS	zničit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vězeň	vězeň	k1gMnSc1	vězeň
Antoine	Antoin	k1gInSc5	Antoin
Lycas	Lycas	k1gInSc1	Lycas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
úlomků	úlomek	k1gInPc2	úlomek
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
model	model	k1gInSc1	model
pro	pro	k7c4	pro
sošku	soška	k1gFnSc4	soška
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
protějšek	protějšek	k1gInSc4	protějšek
-	-	kIx~	-
čůrající	čůrající	k2eAgFnSc4d1	čůrající
holčičku	holčička	k1gFnSc4	holčička
-	-	kIx~	-
Jaenneke	Jaenneke	k1gFnSc4	Jaenneke
Pis	Pisa	k1gFnPc2	Pisa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přibyla	přibýt	k5eAaPmAgFnS	přibýt
také	také	k9	také
socha	socha	k1gFnSc1	socha
čůrajícího	čůrající	k2eAgMnSc2d1	čůrající
psa	pes	k1gMnSc2	pes
-	-	kIx~	-
Zinneke	Zinneke	k1gNnSc1	Zinneke
Pis	Pisa	k1gFnPc2	Pisa
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
Katedrála	katedrál	k1gMnSc2	katedrál
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
archanděla	archanděl	k1gMnSc2	archanděl
a	a	k8xC	a
svaté	svatý	k2eAgFnSc2d1	svatá
Guduly	Gudula	k1gFnSc2	Gudula
Parc	Parc	k1gFnSc1	Parc
du	du	k?	du
Cinquantenaire	Cinquantenair	k1gMnSc5	Cinquantenair
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
oslav	oslava	k1gFnPc2	oslava
padesátého	padesátý	k4xOgNnSc2	padesátý
výročí	výročí	k1gNnSc2	výročí
belgické	belgický	k2eAgFnSc2d1	belgická
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
Palais	Palais	k1gFnSc2	Palais
du	du	k?	du
Cinquantenaire	Cinquantenair	k1gInSc5	Cinquantenair
a	a	k8xC	a
několik	několik	k4yIc4	několik
galerií	galerie	k1gFnPc2	galerie
a	a	k8xC	a
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Bruselský	bruselský	k2eAgInSc1d1	bruselský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Královským	královský	k2eAgInSc7d1	královský
palácem	palác	k1gInSc7	palác
a	a	k8xC	a
budovou	budova	k1gFnSc7	budova
belgického	belgický	k2eAgInSc2d1	belgický
parlamentu	parlament	k1gInSc2	parlament
Bazilika	bazilika	k1gFnSc1	bazilika
Sacré-Cœ	Sacré-Cœ	k1gFnSc2	Sacré-Cœ
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgInSc4	šestý
největší	veliký	k2eAgInSc4d3	veliký
kostel	kostel	k1gInSc4	kostel
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Atomium	Atomium	k1gNnSc1	Atomium
postavené	postavený	k2eAgNnSc1d1	postavené
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
výstavy	výstava	k1gFnSc2	výstava
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Mini-Europe	Mini-Europ	k1gMnSc5	Mini-Europ
-	-	kIx~	-
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
zmenšených	zmenšený	k2eAgInPc2d1	zmenšený
modelů	model	k1gInPc2	model
významných	významný	k2eAgFnPc2d1	významná
evropských	evropský	k2eAgFnPc2d1	Evropská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
Stoclet	Stoclet	k1gInSc1	Stoclet
-	-	kIx~	-
vila	vila	k1gFnSc1	vila
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
secese	secese	k1gFnSc2	secese
a	a	k8xC	a
art	art	k?	art
deco	deco	k6eAd1	deco
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
Královského	královský	k2eAgNnSc2d1	královské
muzea	muzeum	k1gNnSc2	muzeum
krásných	krásný	k2eAgNnPc2d1	krásné
umění	umění	k1gNnPc2	umění
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
městské	městský	k2eAgInPc1d1	městský
domy	dům	k1gInPc1	dům
architekta	architekt	k1gMnSc2	architekt
Victora	Victor	k1gMnSc2	Victor
Horty	hortus	k1gInPc1	hortus
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
-	-	kIx~	-
čtyři	čtyři	k4xCgInPc4	čtyři
domy	dům	k1gInPc4	dům
(	(	kIx(	(
<g/>
Hôtel	Hôtel	k1gMnSc1	Hôtel
Tassel	Tassel	k1gMnSc1	Tassel
<g/>
,	,	kIx,	,
Hôtel	Hôtel	k1gMnSc1	Hôtel
Solvay	Solvaa	k1gFnSc2	Solvaa
<g/>
,	,	kIx,	,
Hôtel	Hôtel	k1gInSc4	Hôtel
van	vana	k1gFnPc2	vana
Eetvelde	Eetveld	k1gMnSc5	Eetveld
<g/>
,	,	kIx,	,
and	and	k?	and
Hortům	Hort	k1gMnPc3	Hort
dům	dům	k1gInSc4	dům
<g/>
/	/	kIx~	/
<g/>
ateliér	ateliér	k1gInSc4	ateliér
<g/>
)	)	kIx)	)
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgNnSc1d1	kulturní
dědictví	dědictví	k1gNnSc1	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skleníky	skleník	k1gInPc1	skleník
Serres	Serresa	k1gFnPc2	Serresa
royales	royalesa	k1gFnPc2	royalesa
de	de	k?	de
Laeken	Laekna	k1gFnPc2	Laekna
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
architekta	architekt	k1gMnSc2	architekt
Alphonse	Alphonse	k1gFnSc2	Alphonse
Balata	balata	k1gFnSc1	balata
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
14	[number]	k4	14
dnů	den	k1gInPc2	den
zpřístupněny	zpřístupněn	k2eAgFnPc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnPc1	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
komiksů	komiks	k1gInPc2	komiks
<g/>
,	,	kIx,	,
zřízené	zřízený	k2eAgFnPc1d1	zřízená
v	v	k7c6	v
secesní	secesní	k2eAgFnSc6d1	secesní
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
projektované	projektovaný	k2eAgNnSc1d1	projektované
architektem	architekt	k1gMnSc7	architekt
Victorem	Victor	k1gMnSc7	Victor
Hortou	Horta	k1gMnSc7	Horta
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
belgické	belgický	k2eAgMnPc4d1	belgický
komiksové	komiksový	k2eAgMnPc4d1	komiksový
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Kuifje	Kuifj	k1gInPc1	Kuifj
<g/>
,	,	kIx,	,
Suske	Suske	k1gNnPc1	Suske
<g/>
,	,	kIx,	,
Wiske	Wisk	k1gInPc1	Wisk
<g/>
,	,	kIx,	,
Šmoulové	šmoula	k1gMnPc1	šmoula
a	a	k8xC	a
Tintin	Tintin	k1gInSc1	Tintin
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
nejvíce	nejvíce	k6eAd1	nejvíce
obrázkových	obrázkový	k2eAgFnPc2d1	obrázková
knížek	knížka	k1gFnPc2	knížka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
31	[number]	k4	31
000	[number]	k4	000
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zachycena	zachytit	k5eAaPmNgFnS	zachytit
historie	historie	k1gFnSc1	historie
kreslených	kreslený	k2eAgInPc2d1	kreslený
seriálů	seriál	k1gInPc2	seriál
na	na	k7c6	na
dílech	dílo	k1gNnPc6	dílo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
670	[number]	k4	670
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Brusel	Brusel	k1gInSc1	Brusel
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
dopravy	doprava	k1gFnSc2	doprava
celé	celý	k2eAgFnSc2d1	celá
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zdejším	zdejší	k2eAgNnSc6d1	zdejší
jižním	jižní	k2eAgNnSc6d1	jižní
vlakovém	vlakový	k2eAgNnSc6d1	vlakové
nádraží	nádraží	k1gNnSc6	nádraží
se	se	k3xPyFc4	se
potkávají	potkávat	k5eAaImIp3nP	potkávat
3	[number]	k4	3
vysokorychlostní	vysokorychlostní	k2eAgFnPc1d1	vysokorychlostní
tratě	trať	k1gFnPc1	trať
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
k	k	k7c3	k
francouzským	francouzský	k2eAgFnPc3d1	francouzská
<g/>
,	,	kIx,	,
německým	německý	k2eAgFnPc3d1	německá
a	a	k8xC	a
nizozemským	nizozemský	k2eAgFnPc3d1	nizozemská
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
vytížená	vytížený	k2eAgNnPc1d1	vytížené
nádraží	nádraží	k1gNnPc1	nádraží
jsou	být	k5eAaImIp3nP	být
Brusel	Brusel	k1gInSc4	Brusel
centrální	centrální	k2eAgNnSc4d1	centrální
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
Brusel	Brusel	k1gInSc4	Brusel
severní	severní	k2eAgInSc4d1	severní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
28	[number]	k4	28
km	km	kA	km
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
vodnímu	vodní	k2eAgInSc3d1	vodní
kanálu	kanál	k1gInSc3	kanál
Brusel-Šelda	Brusel-Šeldo	k1gNnSc2	Brusel-Šeldo
a	a	k8xC	a
75	[number]	k4	75
km	km	kA	km
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
kanálu	kanál	k1gInSc3	kanál
Brusel-Charleroi	Brusel-Charlero	k1gFnSc2	Brusel-Charlero
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
přístupné	přístupný	k2eAgNnSc1d1	přístupné
pro	pro	k7c4	pro
vnitrozemskou	vnitrozemský	k2eAgFnSc4d1	vnitrozemská
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
11	[number]	k4	11
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
dálniční	dálniční	k2eAgInSc4d1	dálniční
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
napojeno	napojit	k5eAaPmNgNnS	napojit
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
dálnic	dálnice	k1gFnPc2	dálnice
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
Gent	Genta	k1gFnPc2	Genta
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Lovaň	Lovaň	k1gMnSc1	Lovaň
<g/>
,	,	kIx,	,
Namur	Namur	k1gMnSc1	Namur
<g/>
,	,	kIx,	,
Charleroi	Charleroi	k1gNnSc1	Charleroi
<g/>
,	,	kIx,	,
Lille	Lille	k1gNnSc1	Lille
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
tramvaje	tramvaj	k1gFnPc1	tramvaj
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
4	[number]	k4	4
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
kolejí	kolej	k1gFnPc2	kolej
je	být	k5eAaImIp3nS	být
39,9	[number]	k4	39,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
má	mít	k5eAaImIp3nS	mít
souhrnnou	souhrnný	k2eAgFnSc4d1	souhrnná
délku	délka	k1gFnSc4	délka
140,9	[number]	k4	140,9
km	km	kA	km
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
17	[number]	k4	17
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
50	[number]	k4	50
denních	denní	k2eAgFnPc2d1	denní
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
11	[number]	k4	11
nočních	noční	k2eAgFnPc2d1	noční
<g/>
.	.	kIx.	.
</s>
