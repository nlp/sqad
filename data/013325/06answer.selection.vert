<s>
John	John	k1gMnSc1	John
Wycliffe	Wycliff	k1gInSc5	Wycliff
(	(	kIx(	(
<g/>
též	též	k9	též
Wyclif	Wyclif	k1gMnSc1	Wyclif
<g/>
,	,	kIx,	,
Wycliff	Wycliff	k1gMnSc1	Wycliff
nebo	nebo	k8xC	nebo
Wickliffe	Wickliff	k1gMnSc5	Wickliff
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1320	[number]	k4	1320
<g/>
/	/	kIx~	/
<g/>
1331	[number]	k4	1331
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1384	[number]	k4	1384
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
realista	realista	k1gMnSc1	realista
novoplatonského	novoplatonský	k2eAgNnSc2d1	novoplatonský
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
reforem	reforma	k1gFnPc2	reforma
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
