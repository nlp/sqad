<p>
<s>
John	John	k1gMnSc1	John
Wycliffe	Wycliff	k1gInSc5	Wycliff
(	(	kIx(	(
<g/>
též	též	k9	též
Wyclif	Wyclif	k1gMnSc1	Wyclif
<g/>
,	,	kIx,	,
Wycliff	Wycliff	k1gMnSc1	Wycliff
nebo	nebo	k8xC	nebo
Wickliffe	Wickliff	k1gMnSc5	Wickliff
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1320	[number]	k4	1320
<g/>
/	/	kIx~	/
<g/>
1331	[number]	k4	1331
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1384	[number]	k4	1384
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
realista	realista	k1gMnSc1	realista
novoplatonského	novoplatonský	k2eAgNnSc2d1	novoplatonský
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
reforem	reforma	k1gFnPc2	reforma
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gNnSc2	jeho
učení	učení	k1gNnSc2	učení
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
coby	coby	k?	coby
hereze	hereze	k1gFnSc1	hereze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
nepříliš	příliš	k6eNd1	příliš
bohatého	bohatý	k2eAgInSc2d1	bohatý
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
pozemkový	pozemkový	k2eAgInSc4d1	pozemkový
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Yorkshire	Yorkshir	k1gMnSc5	Yorkshir
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
je	být	k5eAaImIp3nS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
,	,	kIx,	,
odhady	odhad	k1gInPc1	odhad
badatelů	badatel	k1gMnPc2	badatel
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1320	[number]	k4	1320
<g/>
–	–	k?	–
<g/>
1331	[number]	k4	1331
<g/>
.	.	kIx.	.
</s>
<s>
Sporné	sporný	k2eAgNnSc1d1	sporné
je	být	k5eAaImIp3nS	být
též	též	k9	též
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
tvrz	tvrz	k1gFnSc1	tvrz
ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Wycliffu	Wycliff	k1gInSc2	Wycliff
nad	nad	k7c7	nad
Teesou	Teesa	k1gFnSc7	Teesa
(	(	kIx(	(
<g/>
Wycliffe-on-Tees	Wycliffen-Tees	k1gInSc1	Wycliffe-on-Tees
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
šlechtické	šlechtický	k2eAgNnSc1d1	šlechtické
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Hipswellu	Hipswell	k1gInSc2	Hipswell
<g/>
.	.	kIx.	.
<g/>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
přišel	přijít	k5eAaPmAgInS	přijít
Viklef	Viklef	k1gInSc1	Viklef
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
působil	působit	k5eAaImAgInS	působit
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
Merton	Merton	k1gInSc4	Merton
College	Colleg	k1gFnSc2	Colleg
a	a	k8xC	a
pak	pak	k6eAd1	pak
jako	jako	k9	jako
správce	správce	k1gMnSc1	správce
Balliol	Balliola	k1gFnPc2	Balliola
College	College	k1gFnPc2	College
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1361	[number]	k4	1361
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
doktorem	doktor	k1gMnSc7	doktor
teologie	teologie	k1gFnSc2	teologie
roku	rok	k1gInSc2	rok
1372	[number]	k4	1372
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1365	[number]	k4	1365
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
"	"	kIx"	"
<g/>
wardenem	warden	k1gMnSc7	warden
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dohlížitelem	dohlížitel	k1gMnSc7	dohlížitel
kněžského	kněžský	k2eAgInSc2d1	kněžský
semináře	seminář	k1gInSc2	seminář
(	(	kIx(	(
<g/>
koleje	kolej	k1gFnPc1	kolej
<g/>
)	)	kIx)	)
v	v	k7c4	v
Canterbury	Canterbura	k1gFnPc4	Canterbura
Hallu	Hall	k1gInSc2	Hall
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1367	[number]	k4	1367
byl	být	k5eAaImAgInS	být
řízení	řízení	k1gNnSc4	řízení
koleje	kolej	k1gFnSc2	kolej
zbaven	zbaven	k2eAgMnSc1d1	zbaven
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
ústavu	ústav	k1gInSc2	ústav
bylo	být	k5eAaImAgNnS	být
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
mnichům	mnich	k1gMnPc3	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
odvolal	odvolat	k5eAaPmAgMnS	odvolat
k	k	k7c3	k
papeži	papež	k1gMnSc3	papež
Urbanovi	Urban	k1gMnSc3	Urban
V.	V.	kA	V.
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
neprospěch	neprospěch	k1gInSc4	neprospěch
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rostla	růst	k5eAaImAgFnS	růst
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
anglických	anglický	k2eAgMnPc2d1	anglický
šlechticů	šlechtic	k1gMnPc2	šlechtic
se	s	k7c7	s
vzrůstem	vzrůst	k1gInSc7	vzrůst
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
i	i	k9	i
s	s	k7c7	s
papežskou	papežský	k2eAgFnSc7d1	Papežská
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Přehnané	přehnaný	k2eAgInPc1d1	přehnaný
finanční	finanční	k2eAgInPc1d1	finanční
nároky	nárok	k1gInPc1	nárok
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
a	a	k8xC	a
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
postavení	postavení	k1gNnSc4	postavení
kléru	klér	k1gInSc2	klér
budily	budit	k5eAaImAgInP	budit
proticírkevní	proticírkevní	k2eAgFnSc4d1	proticírkevní
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
před	před	k7c4	před
širší	široký	k2eAgFnSc4d2	širší
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
parlament	parlament	k1gInSc1	parlament
usnesl	usnést	k5eAaPmAgInS	usnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k9	již
papeži	papež	k1gMnSc3	papež
nebude	být	k5eNaImBp3nS	být
odvádět	odvádět	k5eAaImF	odvádět
lenní	lenní	k2eAgInSc4d1	lenní
poplatek	poplatek	k1gInSc4	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
toto	tento	k3xDgNnSc4	tento
usnesení	usnesení	k1gNnSc4	usnesení
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
roku	rok	k1gInSc2	rok
1309	[number]	k4	1309
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
Avignonu	Avignon	k1gInSc2	Avignon
a	a	k8xC	a
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
stoleté	stoletý	k2eAgFnSc6d1	stoletá
válce	válka	k1gFnSc6	válka
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
spojenci	spojenec	k1gMnPc7	spojenec
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
parlament	parlament	k1gInSc1	parlament
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
odlivu	odliv	k1gInSc3	odliv
peněz	peníze	k1gInPc2	peníze
do	do	k7c2	do
papežské	papežský	k2eAgFnSc2d1	Papežská
pokladny	pokladna	k1gFnSc2	pokladna
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zakazovaly	zakazovat	k5eAaImAgInP	zakazovat
přijímat	přijímat	k5eAaImF	přijímat
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
církevní	církevní	k2eAgFnPc1d1	církevní
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
platit	platit	k5eAaImF	platit
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
ně	on	k3xPp3gFnPc4	on
a	a	k8xC	a
uznávat	uznávat	k5eAaImF	uznávat
výnosy	výnos	k1gInPc4	výnos
jeho	jeho	k3xOp3gMnPc2	jeho
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
hrozbou	hrozba	k1gFnSc7	hrozba
církevní	církevní	k2eAgFnPc1d1	církevní
klatby	klatba	k1gFnPc1	klatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1374	[number]	k4	1374
vyslal	vyslat	k5eAaPmAgMnS	vyslat
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
do	do	k7c2	do
Brugg	Bruggy	k1gFnPc2	Bruggy
poselstvo	poselstvo	k1gNnSc1	poselstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tam	tam	k6eAd1	tam
mělo	mít	k5eAaImAgNnS	mít
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
plnomocníky	plnomocník	k1gMnPc7	plnomocník
papežskými	papežský	k2eAgMnPc7d1	papežský
o	o	k7c6	o
sporných	sporný	k2eAgFnPc6d1	sporná
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
poselstva	poselstvo	k1gNnSc2	poselstvo
stál	stát	k5eAaImAgMnS	stát
králův	králův	k2eAgMnSc1d1	králův
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lancasteru	Lancaster	k1gInSc2	Lancaster
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
odborný	odborný	k2eAgMnSc1d1	odborný
poradce	poradce	k1gMnSc1	poradce
se	se	k3xPyFc4	se
jednání	jednání	k1gNnSc2	jednání
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Viklefovým	Viklefův	k2eAgMnSc7d1	Viklefův
ochráncem	ochránce	k1gMnSc7	ochránce
a	a	k8xC	a
k	k	k7c3	k
hmotnému	hmotný	k2eAgInSc3d1	hmotný
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
faru	fara	k1gFnSc4	fara
v	v	k7c6	v
Lutterworthu	Lutterworth	k1gInSc6	Lutterworth
(	(	kIx(	(
<g/>
1374	[number]	k4	1374
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jako	jako	k8xC	jako
držitel	držitel	k1gMnSc1	držitel
této	tento	k3xDgFnSc2	tento
fary	fara	k1gFnSc2	fara
však	však	k9	však
Viklef	Viklef	k1gMnSc1	Viklef
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1382	[number]	k4	1382
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
spisech	spis	k1gInPc6	spis
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
traktátech	traktát	k1gInPc6	traktát
De	De	k?	De
dominio	dominio	k6eAd1	dominio
divino	divin	k2eAgNnSc1d1	Divino
(	(	kIx(	(
<g/>
1373	[number]	k4	1373
<g/>
–	–	k?	–
<g/>
1374	[number]	k4	1374
<g/>
)	)	kIx)	)
a	a	k8xC	a
De	De	k?	De
civili	civit	k5eAaBmAgMnP	civit
dominio	dominio	k6eAd1	dominio
(	(	kIx(	(
<g/>
1370	[number]	k4	1370
<g/>
–	–	k?	–
<g/>
1375	[number]	k4	1375
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Viklef	Viklef	k1gInSc1	Viklef
horlil	horlit	k5eAaImAgInS	horlit
proti	proti	k7c3	proti
papežskému	papežský	k2eAgNnSc3d1	papežské
zdaňování	zdaňování	k1gNnSc3	zdaňování
a	a	k8xC	a
obsazování	obsazování	k1gNnSc3	obsazování
anglických	anglický	k2eAgFnPc2d1	anglická
beneficií	beneficie	k1gFnPc2	beneficie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mnišské	mnišský	k2eAgInPc1d1	mnišský
řády	řád	k1gInPc1	řád
přijaly	přijmout	k5eAaPmAgInP	přijmout
zásadu	zásada	k1gFnSc4	zásada
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
všechno	všechen	k3xTgNnSc1	všechen
své	svůj	k3xOyFgNnSc4	svůj
nezměrné	nezměrný	k2eAgNnSc4d1	nezměrné
bohatství	bohatství	k1gNnSc4	bohatství
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
chudobě	chudoba	k1gFnSc3	chudoba
časů	čas	k1gInPc2	čas
apoštolských	apoštolský	k2eAgInPc2d1	apoštolský
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
veliký	veliký	k2eAgInSc4d1	veliký
majetek	majetek	k1gInSc4	majetek
jí	on	k3xPp3gFnSc7	on
nejen	nejen	k6eAd1	nejen
neprospívá	prospívat	k5eNaImIp3nS	prospívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
škodí	škodit	k5eAaImIp3nS	škodit
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc1	kněz
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
věnovat	věnovat	k5eAaPmF	věnovat
jen	jen	k6eAd1	jen
pastýřským	pastýřský	k2eAgFnPc3d1	pastýřská
povinnostem	povinnost	k1gFnPc3	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
světských	světský	k2eAgFnPc6d1	světská
moc	moc	k6eAd1	moc
králova	králův	k2eAgMnSc2d1	králův
není	být	k5eNaImIp3nS	být
podřízena	podřídit	k5eAaPmNgFnS	podřídit
moci	moct	k5eAaImF	moct
papeže	papež	k1gMnSc4	papež
a	a	k8xC	a
světská	světský	k2eAgFnSc1d1	světská
moc	moc	k1gFnSc1	moc
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
odejmout	odejmout	k5eAaPmF	odejmout
kléru	klér	k1gInSc3	klér
jeho	jeho	k3xOp3gInPc4	jeho
statky	statek	k1gInPc4	statek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
civili	civit	k5eAaImAgMnP	civit
dominio	dominio	k6eAd1	dominio
Viklef	Viklef	k1gMnSc1	Viklef
také	také	k9	také
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
hříchu	hřích	k1gInSc6	hřích
nemůže	moct	k5eNaImIp3nS	moct
nic	nic	k6eAd1	nic
užívat	užívat	k5eAaImF	užívat
spravedlivě	spravedlivě	k6eAd1	spravedlivě
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
milosti	milost	k1gFnSc2	milost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oprávněný	oprávněný	k2eAgInSc1d1	oprávněný
k	k	k7c3	k
vykonávání	vykonávání	k1gNnSc3	vykonávání
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
i	i	k8xC	i
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
<g/>
Již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
proti	proti	k7c3	proti
Viklefovi	Viklef	k1gMnSc3	Viklef
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
církevních	církevní	k2eAgInPc2d1	církevní
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stálé	stálý	k2eAgFnPc4d1	stálá
žaloby	žaloba	k1gFnPc4	žaloba
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
XI	XI	kA	XI
<g/>
.	.	kIx.	.
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
r.	r.	kA	r.
1377	[number]	k4	1377
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
pět	pět	k4xCc4	pět
bul	bula	k1gFnPc2	bula
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
<g/>
,	,	kIx,	,
univerzitu	univerzita	k1gFnSc4	univerzita
i	i	k8xC	i
krále	král	k1gMnSc2	král
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
ráznému	rázný	k2eAgNnSc3d1	rázné
zakročení	zakročení	k1gNnSc3	zakročení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buly	bula	k1gFnPc1	bula
se	se	k3xPyFc4	se
však	však	k9	však
minuly	minout	k5eAaImAgInP	minout
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
svým	svůj	k3xOyFgInSc7	svůj
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1377	[number]	k4	1377
zemřel	zemřít	k5eAaPmAgMnS	zemřít
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
desetiletý	desetiletý	k2eAgMnSc1d1	desetiletý
Richard	Richard	k1gMnSc1	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
podléhal	podléhat	k5eAaImAgMnS	podléhat
úplně	úplně	k6eAd1	úplně
vlivu	vliv	k1gInSc2	vliv
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Lancasteru	Lancaster	k1gInSc2	Lancaster
<g/>
,	,	kIx,	,
Viklefova	Viklefův	k2eAgMnSc4d1	Viklefův
příznivce	příznivec	k1gMnSc4	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
r.	r.	kA	r.
1378	[number]	k4	1378
byl	být	k5eAaImAgMnS	být
Viklef	Viklef	k1gMnSc1	Viklef
povolán	povolat	k5eAaPmNgMnS	povolat
k	k	k7c3	k
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Lambeth	Lambetha	k1gFnPc2	Lambetha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
prelátů	prelát	k1gInPc2	prelát
se	se	k3xPyFc4	se
však	však	k9	však
osobně	osobně	k6eAd1	osobně
dostavila	dostavit	k5eAaPmAgFnS	dostavit
matka	matka	k1gFnSc1	matka
krále	král	k1gMnSc2	král
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
varovala	varovat	k5eAaImAgFnS	varovat
přítomné	přítomný	k1gMnPc4	přítomný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
proti	proti	k7c3	proti
Viklefovi	Viklef	k1gMnSc3	Viklef
nic	nic	k6eAd1	nic
nepodnikali	podnikat	k5eNaImAgMnP	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Postrašení	postrašený	k2eAgMnPc1d1	postrašený
preláti	prelát	k1gMnPc1	prelát
proto	proto	k8xC	proto
jen	jen	k6eAd1	jen
zakázali	zakázat	k5eAaPmAgMnP	zakázat
Viklefovi	Viklefův	k2eAgMnPc1d1	Viklefův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
šířil	šířit	k5eAaImAgInS	šířit
veřejně	veřejně	k6eAd1	veřejně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
Viklefovo	Viklefův	k2eAgNnSc1d1	Viklefovo
tvůrčí	tvůrčí	k2eAgNnSc1d1	tvůrčí
úsilí	úsilí	k1gNnSc1	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
veritate	veritat	k1gMnSc5	veritat
sacre	sacr	k1gMnSc5	sacr
Scripture	Scriptur	k1gMnSc5	Scriptur
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Písmo	písmo	k1gNnSc1	písmo
svaté	svatá	k1gFnSc2	svatá
je	být	k5eAaImIp3nS	být
neomylný	omylný	k2eNgInSc4d1	neomylný
pramen	pramen	k1gInSc4	pramen
učení	učení	k1gNnSc2	učení
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poměřovat	poměřovat	k5eAaImF	poměřovat
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
koncily	koncil	k1gInPc4	koncil
i	i	k8xC	i
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začínají	začínat	k5eAaImIp3nP	začínat
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
popudu	popud	k1gInSc2	popud
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
</s>
</p>
<p>
<s>
anglickém	anglický	k2eAgInSc6d1	anglický
překladu	překlad	k1gInSc6	překlad
Bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Viklifovská	Viklifovská	k1gFnSc1	Viklifovská
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
Wycliffite	Wycliffit	k1gMnSc5	Wycliffit
Bible	bible	k1gFnPc1	bible
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ovšem	ovšem	k9	ovšem
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
překládání	překládání	k1gNnSc2	překládání
zapojil	zapojit	k5eAaPmAgMnS	zapojit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
ecclesia	ecclesius	k1gMnSc2	ecclesius
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
nepředstavují	představovat	k5eNaImIp3nP	představovat
papež	papež	k1gMnSc1	papež
a	a	k8xC	a
duchovenstvo	duchovenstvo	k1gNnSc1	duchovenstvo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
vyvolení	vyvolený	k2eAgMnPc1d1	vyvolený
Boží	božit	k5eAaImIp3nP	božit
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
hlavou	hlava	k1gFnSc7	hlava
je	být	k5eAaImIp3nS	být
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
papež	papež	k1gMnSc1	papež
a	a	k8xC	a
preláti	prelát	k1gMnPc1	prelát
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
vyvoleným	vyvolená	k1gFnPc3	vyvolená
Božím	boží	k2eAgFnPc3d1	boží
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
duchovně	duchovně	k6eAd1	duchovně
vést	vést	k5eAaImF	vést
církev	církev	k1gFnSc4	církev
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
sborem	sborem	k6eAd1	sborem
satanovým	satanův	k2eAgNnSc7d1	Satanovo
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gInSc1	Viklef
zde	zde	k6eAd1	zde
odmítá	odmítat	k5eAaImIp3nS	odmítat
i	i	k9	i
odpustky	odpustek	k1gInPc4	odpustek
<g/>
,	,	kIx,	,
odpouštět	odpouštět	k5eAaImF	odpouštět
hříšníku	hříšník	k1gMnSc3	hříšník
může	moct	k5eAaImIp3nS	moct
jen	jen	k6eAd1	jen
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
kněz	kněz	k1gMnSc1	kněz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
potestate	potestat	k1gMnSc5	potestat
papae	papaus	k1gMnSc5	papaus
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
)	)	kIx)	)
odmítá	odmítat	k5eAaImIp3nS	odmítat
božský	božský	k2eAgInSc4d1	božský
původ	původ	k1gInSc4	původ
papežství	papežství	k1gNnSc2	papežství
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
není	být	k5eNaImIp3nS	být
opravdovým	opravdový	k2eAgMnSc7d1	opravdový
následovníkem	následovník	k1gMnSc7	následovník
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Antikrist	Antikrist	k1gMnSc1	Antikrist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
apostasia	apostasius	k1gMnSc2	apostasius
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
)	)	kIx)	)
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
Boží	boží	k2eAgFnSc2d1	boží
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
člověka	člověk	k1gMnSc4	člověk
vyobcovat	vyobcovat	k5eAaPmF	vyobcovat
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
ji	on	k3xPp3gFnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
ani	ani	k9	ani
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
ledaže	ledaže	k8xS	ledaže
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
vůle	vůle	k1gFnSc1	vůle
Boží	boží	k2eAgFnSc1d1	boží
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
eucharistia	eucharistius	k1gMnSc2	eucharistius
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
)	)	kIx)	)
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
Viklef	Viklef	k1gMnSc1	Viklef
<g />
.	.	kIx.	.
</s>
<s>
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
dogmatu	dogma	k1gNnSc6	dogma
transsubstanciace	transsubstanciace	k1gFnSc2	transsubstanciace
(	(	kIx(	(
<g/>
přepodstatnění	přepodstatnění	k1gNnSc1	přepodstatnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c6	o
učení	učení	k1gNnSc6	učení
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
mešní	mešní	k2eAgFnSc6d1	mešní
oběti	oběť	k1gFnSc6	oběť
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
v	v	k7c4	v
pravé	pravý	k2eAgNnSc4d1	pravé
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
proměňování	proměňování	k1gNnSc6	proměňování
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
vnímatelné	vnímatelný	k2eAgFnPc4d1	vnímatelná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
(	(	kIx(	(
<g/>
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
<g/>
,	,	kIx,	,
vůně	vůně	k1gFnSc1	vůně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstata	podstata	k1gFnSc1	podstata
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
podstatou	podstata	k1gFnSc7	podstata
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
ztotožnění	ztotožnění	k1gNnSc3	ztotožnění
chleba	chléb	k1gInSc2	chléb
(	(	kIx(	(
<g/>
hostie	hostie	k1gFnSc1	hostie
<g/>
)	)	kIx)	)
s	s	k7c7	s
Kristovým	Kristův	k2eAgNnSc7d1	Kristovo
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
duchovnějšímu	duchovní	k2eAgNnSc3d2	duchovnější
chápání	chápání	k1gNnSc3	chápání
eucharistie	eucharistie	k1gFnSc2	eucharistie
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
dle	dle	k7c2	dle
něho	on	k3xPp3gMnSc2	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
prvotní	prvotní	k2eAgFnSc6d1	prvotní
<g/>
.	.	kIx.	.
</s>
<s>
Hrozil	hrozit	k5eAaImAgInS	hrozit
se	se	k3xPyFc4	se
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
kněz	kněz	k1gMnSc1	kněz
"	"	kIx"	"
<g/>
dělá	dělat	k5eAaImIp3nS	dělat
tělo	tělo	k1gNnSc4	tělo
Boží	boží	k2eAgFnSc2d1	boží
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
že	že	k8xS	že
věřící	věřící	k1gMnPc1	věřící
mohou	moct	k5eAaImIp3nP	moct
tělo	tělo	k1gNnSc4	tělo
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
drtit	drtit	k5eAaImF	drtit
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
víno	víno	k1gNnSc1	víno
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
i	i	k9	i
po	po	k7c6	po
pronesení	pronesení	k1gNnSc6	pronesení
konsekračních	konsekrační	k2eAgNnPc2d1	konsekrační
slov	slovo	k1gNnPc2	slovo
zachovány	zachován	k2eAgInPc1d1	zachován
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
remanence	remanence	k1gFnSc2	remanence
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kristus	Kristus	k1gMnSc1	Kristus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hostii	hostie	k1gFnSc6	hostie
přítomen	přítomen	k2eAgMnSc1d1	přítomen
svátostně	svátostně	k6eAd1	svátostně
a	a	k8xC	a
duchovně	duchovně	k6eAd1	duchovně
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jako	jako	k8xS	jako
duše	duše	k1gFnSc1	duše
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Platné	platný	k2eAgNnSc1d1	platné
učení	učení	k1gNnSc1	učení
církve	církev	k1gFnSc2	církev
o	o	k7c6	o
eucharistii	eucharistie	k1gFnSc6	eucharistie
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
papežem	papež	k1gMnSc7	papež
Innocencem	Innocenec	k1gMnSc7	Innocenec
III	III	kA	III
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
dogma	dogma	k1gNnSc1	dogma
o	o	k7c6	o
transsubstaciaci	transsubstaciace	k1gFnSc6	transsubstaciace
roku	rok	k1gInSc2	rok
1215	[number]	k4	1215
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
lateránský	lateránský	k2eAgInSc1d1	lateránský
koncil	koncil	k1gInSc1	koncil
<g/>
)	)	kIx)	)
s	s	k7c7	s
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
by	by	kYmCp3nS	by
věřil	věřit	k5eAaImAgMnS	věřit
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
Viklefa	Viklef	k1gMnSc2	Viklef
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
mnišských	mnišský	k2eAgInPc2d1	mnišský
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
zasypávali	zasypávat	k5eAaImAgMnP	zasypávat
obviněními	obvinění	k1gNnPc7	obvinění
z	z	k7c2	z
kacířství	kacířství	k1gNnPc2	kacířství
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
Viklefovi	Viklef	k1gMnSc3	Viklef
dáván	dáván	k2eAgInSc4d1	dáván
titul	titul	k1gInSc4	titul
venerabilis	venerabilis	k1gFnPc2	venerabilis
doctor	doctor	k1gMnSc1	doctor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ctihodný	ctihodný	k2eAgMnSc1d1	ctihodný
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
doctor	doctor	k1gMnSc1	doctor
evangelicus	evangelicus	k1gMnSc1	evangelicus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
evangelický	evangelický	k2eAgMnSc1d1	evangelický
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1379	[number]	k4	1379
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
exsecrabilis	exsecrabilis	k1gInSc1	exsecrabilis
seductor	seductor	k1gInSc1	seductor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zlořečený	zlořečený	k2eAgMnSc1d1	zlořečený
svůdce	svůdce	k1gMnSc1	svůdce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gInSc1	Viklef
ale	ale	k9	ale
nemínil	mínit	k5eNaImAgInS	mínit
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1381	[number]	k4	1381
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
spis	spis	k1gInSc1	spis
De	De	k?	De
eucharistia	eucharistia	k1gFnSc1	eucharistia
confessio	confessio	k6eAd1	confessio
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
hájil	hájit	k5eAaImAgMnS	hájit
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
o	o	k7c4	o
eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viklefovy	Viklefův	k2eAgFnSc2d1	Viklefova
myšlenky	myšlenka	k1gFnSc2	myšlenka
někteří	některý	k3yIgMnPc1	některý
kněží	kněz	k1gMnPc1	kněz
ve	v	k7c6	v
zjednodušené	zjednodušený	k2eAgFnSc6d1	zjednodušená
podobě	podoba	k1gFnSc6	podoba
zprostředkovávali	zprostředkovávat	k5eAaImAgMnP	zprostředkovávat
širší	široký	k2eAgFnSc3d2	širší
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
podnikali	podnikat	k5eAaImAgMnP	podnikat
kazatelské	kazatelský	k2eAgFnPc4d1	kazatelská
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
lollardi	lollard	k1gMnPc1	lollard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1381	[number]	k4	1381
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
veliké	veliký	k2eAgNnSc1d1	veliké
selské	selský	k2eAgNnSc1d1	selské
povstání	povstání	k1gNnSc1	povstání
vedené	vedený	k2eAgNnSc1d1	vedené
Watem	Wat	k1gMnSc7	Wat
Tylerem	Tyler	k1gMnSc7	Tyler
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
povstání	povstání	k1gNnSc2	povstání
zavrhoval	zavrhovat	k5eAaImAgMnS	zavrhovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vina	vina	k1gFnSc1	vina
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gMnSc3	jeho
odpůrci	odpůrce	k1gMnSc3	odpůrce
sváděna	sváděn	k2eAgFnSc1d1	sváděna
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
blasphemia	blasphemius	k1gMnSc2	blasphemius
(	(	kIx(	(
<g/>
1381	[number]	k4	1381
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dožadoval	dožadovat	k5eAaImAgMnS	dožadovat
mírnějšího	mírný	k2eAgInSc2d2	mírnější
soudu	soud	k1gInSc2	soud
vůči	vůči	k7c3	vůči
rebelům	rebel	k1gMnPc3	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
též	též	k9	též
poukazováno	poukazovat	k5eAaImNgNnS	poukazovat
na	na	k7c4	na
nespornou	sporný	k2eNgFnSc4d1	nesporná
shodu	shoda	k1gFnSc4	shoda
mezi	mezi	k7c7	mezi
hesly	heslo	k1gNnPc7	heslo
rebelů	rebel	k1gMnPc2	rebel
a	a	k8xC	a
Viklefovým	Viklefův	k2eAgNnSc7d1	Viklefovo
učením	učení	k1gNnSc7	učení
o	o	k7c6	o
pravé	pravý	k2eAgFnSc6d1	pravá
a	a	k8xC	a
nepravé	pravý	k2eNgFnSc6d1	nepravá
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
i	i	k8xC	i
vévoda	vévoda	k1gMnSc1	vévoda
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
již	již	k9	již
Viklefa	Viklef	k1gMnSc4	Viklef
účinně	účinně	k6eAd1	účinně
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1382	[number]	k4	1382
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
shromáždění	shromáždění	k1gNnSc1	shromáždění
anglických	anglický	k2eAgInPc2d1	anglický
prelátů	prelát	k1gInPc2	prelát
a	a	k8xC	a
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1382	[number]	k4	1382
odsoudilo	odsoudit	k5eAaPmAgNnS	odsoudit
29	[number]	k4	29
článků	článek	k1gInPc2	článek
z	z	k7c2	z
Viklefova	Viklefův	k2eAgNnSc2d1	Viklefovo
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
Viklef	Viklef	k1gMnSc1	Viklef
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
v	v	k7c6	v
usnesení	usnesení	k1gNnSc6	usnesení
jmenován	jmenován	k2eAgMnSc1d1	jmenován
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Královským	královský	k2eAgInSc7d1	královský
mandátem	mandát	k1gInSc7	mandát
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1382	[number]	k4	1382
bylo	být	k5eAaImAgNnS	být
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
odstranit	odstranit	k5eAaPmF	odstranit
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
všechny	všechen	k3xTgMnPc4	všechen
přívržence	přívrženec	k1gMnPc4	přívrženec
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Viklef	Viklef	k1gMnSc1	Viklef
byl	být	k5eAaImAgMnS	být
donucen	donucen	k2eAgInSc4d1	donucen
Oxford	Oxford	k1gInSc4	Oxford
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1382	[number]	k4	1382
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
faře	fara	k1gFnSc6	fara
v	v	k7c6	v
Lutterworthu	Lutterworth	k1gInSc6	Lutterworth
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
faře	fara	k1gFnSc6	fara
Viklef	Viklef	k1gMnSc1	Viklef
dokončil	dokončit	k5eAaPmAgMnS	dokončit
traktát	traktát	k1gInSc4	traktát
Dialogus	Dialogus	k1gMnSc1	Dialogus
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1383	[number]	k4	1383
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
opět	opět	k6eAd1	opět
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kristus	Kristus	k1gMnSc1	Kristus
vyvolil	vyvolit	k5eAaPmAgMnS	vyvolit
duchovenstvo	duchovenstvo	k1gNnSc4	duchovenstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
staralo	starat	k5eAaImAgNnS	starat
o	o	k7c4	o
věci	věc	k1gFnPc4	věc
duchovní	duchovní	k2eAgFnPc4d1	duchovní
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
aby	aby	kYmCp3nS	aby
mělo	mít	k5eAaImAgNnS	mít
starost	starost	k1gFnSc4	starost
o	o	k7c4	o
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
světské	světský	k2eAgNnSc4d1	světské
panování	panování	k1gNnSc4	panování
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k1gMnPc1	duchovní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nešetří	šetřit	k5eNaImIp3nP	šetřit
příkazů	příkaz	k1gInPc2	příkaz
Kristových	Kristův	k2eAgInPc2d1	Kristův
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kněžími	kněz	k1gMnPc7	kněz
Antikristovými	Antikristův	k2eAgMnPc7d1	Antikristův
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
řady	řada	k1gFnSc2	řada
polemických	polemický	k2eAgInPc2d1	polemický
traktátů	traktát	k1gInPc2	traktát
proti	proti	k7c3	proti
papeži	papež	k1gMnSc3	papež
<g/>
,	,	kIx,	,
prelátům	prelát	k1gInPc3	prelát
a	a	k8xC	a
mnichům	mnich	k1gMnPc3	mnich
zde	zde	k6eAd1	zde
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
poslední	poslední	k2eAgNnSc1d1	poslední
velké	velký	k2eAgNnSc1d1	velké
Viklefovo	Viklefův	k2eAgNnSc1d1	Viklefovo
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Trialogus	Trialogus	k1gInSc1	Trialogus
(	(	kIx(	(
<g/>
1382	[number]	k4	1382
<g/>
–	–	k?	–
<g/>
1383	[number]	k4	1383
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
souhrnem	souhrn	k1gInSc7	souhrn
jeho	jeho	k3xOp3gInPc2	jeho
teologických	teologický	k2eAgInPc2d1	teologický
i	i	k8xC	i
filozofických	filozofický	k2eAgInPc2d1	filozofický
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
byl	být	k5eAaImAgInS	být
Viklef	Viklef	k1gInSc1	Viklef
postižen	postihnout	k5eAaPmNgInS	postihnout
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
záchvatem	záchvat	k1gInSc7	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
v	v	k7c6	v
Lutterworthu	Lutterworth	k1gInSc6	Lutterworth
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
tažení	tažení	k1gNnSc1	tažení
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInPc3	jeho
názorům	názor	k1gInPc3	názor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1388	[number]	k4	1388
nařídil	nařídit	k5eAaPmAgMnS	nařídit
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	k9	aby
všechna	všechen	k3xTgNnPc1	všechen
Viklefova	Viklefův	k2eAgNnPc1d1	Viklefovo
díla	dílo	k1gNnPc1	dílo
i	i	k8xC	i
pojednání	pojednání	k1gNnSc1	pojednání
jeho	jeho	k3xOp3gMnPc2	jeho
přívrženců	přívrženec	k1gMnPc2	přívrženec
byla	být	k5eAaImAgFnS	být
konfiskována	konfiskován	k2eAgFnSc1d1	konfiskována
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
je	on	k3xPp3gMnPc4	on
nesmí	smět	k5eNaImIp3nS	smět
vlastnit	vlastnit	k5eAaImF	vlastnit
ani	ani	k8xC	ani
opisovat	opisovat	k5eAaImF	opisovat
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1401	[number]	k4	1401
(	(	kIx(	(
<g/>
De	De	k?	De
heretico	heretico	k6eAd1	heretico
comburendo	comburendo	k6eAd1	comburendo
<g/>
)	)	kIx)	)
hrozil	hrozit	k5eAaImAgMnS	hrozit
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
šíření	šíření	k1gNnSc4	šíření
kacířských	kacířský	k2eAgFnPc2d1	kacířská
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
trestem	trest	k1gInSc7	trest
smrti	smrt	k1gFnSc2	smrt
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Zajíc	Zajíc	k1gMnSc1	Zajíc
z	z	k7c2	z
Hazmburka	Hazmburek	k1gMnSc2	Hazmburek
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1410	[number]	k4	1410
příkaz	příkaz	k1gInSc4	příkaz
shromáždit	shromáždit	k5eAaPmF	shromáždit
Viklefovy	Viklefův	k2eAgInPc4d1	Viklefův
spisy	spis	k1gInPc4	spis
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
veřejně	veřejně	k6eAd1	veřejně
spálit	spálit	k5eAaPmF	spálit
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
svého	svůj	k3xOyFgInSc2	svůj
malostranského	malostranský	k2eAgInSc2d1	malostranský
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1410	[number]	k4	1410
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
kostnickém	kostnický	k2eAgInSc6d1	kostnický
koncilu	koncil	k1gInSc6	koncil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
bylo	být	k5eAaImAgNnS	být
zatraceno	zatratit	k5eAaPmNgNnS	zatratit
45	[number]	k4	45
vět	věta	k1gFnPc2	věta
z	z	k7c2	z
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
kacíře	kacíř	k1gMnPc4	kacíř
a	a	k8xC	a
koncil	koncil	k1gInSc4	koncil
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Viklefovy	Viklefův	k2eAgFnPc1d1	Viklefova
kosti	kost	k1gFnPc1	kost
byly	být	k5eAaImAgFnP	být
vyňaty	vynít	k5eAaPmNgFnP	vynít
z	z	k7c2	z
posvěcené	posvěcený	k2eAgFnSc2d1	posvěcená
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
spáleny	spálen	k2eAgInPc1d1	spálen
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1428	[number]	k4	1428
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
papeže	papež	k1gMnSc2	papež
Martina	Martin	k1gMnSc2	Martin
V.	V.	kA	V.
byly	být	k5eAaImAgFnP	být
v	v	k7c4	v
Lutterworthu	Lutterwortha	k1gFnSc4	Lutterwortha
ostatky	ostatek	k1gInPc1	ostatek
Viklefovy	Viklefův	k2eAgInPc1d1	Viklefův
vykopány	vykopán	k2eAgInPc1d1	vykopán
<g/>
,	,	kIx,	,
spáleny	spálen	k2eAgInPc1d1	spálen
a	a	k8xC	a
popel	popel	k1gInSc1	popel
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
vhozen	vhodit	k5eAaPmNgMnS	vhodit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Swiftu	Swift	k1gInSc2	Swift
<g/>
.	.	kIx.	.
<g/>
Traktáty	traktát	k1gInPc1	traktát
Jana	Jan	k1gMnSc2	Jan
Viklefa	Viklef	k1gMnSc2	Viklef
přinášeli	přinášet	k5eAaImAgMnP	přinášet
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
studenti	student	k1gMnPc1	student
vracející	vracející	k2eAgMnSc1d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
anglických	anglický	k2eAgFnPc2d1	anglická
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
česko-anglických	českonglický	k2eAgInPc2d1	česko-anglický
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zesílily	zesílit	k5eAaPmAgInP	zesílit
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
Anny	Anna	k1gFnSc2	Anna
Lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
zastala	zastat	k5eAaPmAgFnS	zastat
povstalců	povstalec	k1gMnPc2	povstalec
a	a	k8xC	a
prosila	prosit	k5eAaImAgFnS	prosit
o	o	k7c4	o
shovívavost	shovívavost	k1gFnSc4	shovívavost
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
potrestání	potrestání	k1gNnSc6	potrestání
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viklefovy	Viklefův	k2eAgFnPc1d1	Viklefova
myšlenky	myšlenka	k1gFnPc1	myšlenka
silně	silně	k6eAd1	silně
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
české	český	k2eAgMnPc4d1	český
vzdělance	vzdělanec	k1gMnPc4	vzdělanec
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jmenovitě	jmenovitě	k6eAd1	jmenovitě
např.	např.	kA	např.
Stanislava	Stanislava	k1gFnSc1	Stanislava
ze	z	k7c2	z
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
Jeronýma	Jeroným	k1gMnSc2	Jeroným
Pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
,	,	kIx,	,
Jakoubka	Jakoubek	k1gMnSc4	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
viklefovec	viklefovec	k1gMnSc1	viklefovec
Petr	Petr	k1gMnSc1	Petr
Payne	Payn	k1gInSc5	Payn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1415	[number]	k4	1415
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
významnou	významný	k2eAgFnSc7d1	významná
postavou	postava	k1gFnSc7	postava
českého	český	k2eAgNnSc2d1	české
reformního	reformní	k2eAgNnSc2d1	reformní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
názorů	názor	k1gInPc2	názor
později	pozdě	k6eAd2	pozdě
převzala	převzít	k5eAaPmAgFnS	převzít
protestantská	protestantský	k2eAgFnSc1d1	protestantská
reformace	reformace	k1gFnSc1	reformace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
36	[number]	k4	36
svazků	svazek	k1gInPc2	svazek
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
spisů	spis	k1gInPc2	spis
(	(	kIx(	(
<g/>
přetisk	přetisk	k1gInSc1	přetisk
ve	v	k7c6	v
Fankfurtu	Fankfurt	k1gInSc6	Fankfurt
v	v	k7c6	v
r.	r.	kA	r.
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
díla	dílo	k1gNnPc1	dílo
však	však	k9	však
ještě	ještě	k6eAd1	ještě
nejsou	být	k5eNaImIp3nP	být
vydána	vydán	k2eAgNnPc1d1	vydáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
ujaly	ujmout	k5eAaPmAgFnP	ujmout
Viklefova	Viklefův	k2eAgNnSc2d1	Viklefovo
odkazu	odkaz	k1gInSc2	odkaz
všechny	všechen	k3xTgInPc4	všechen
husitské	husitský	k2eAgInPc4d1	husitský
směry	směr	k1gInPc4	směr
a	a	k8xC	a
za	za	k7c4	za
dochování	dochování	k1gNnSc4	dochování
monumentálního	monumentální	k2eAgNnSc2d1	monumentální
Viklefova	Viklefův	k2eAgNnSc2d1	Viklefovo
díla	dílo	k1gNnSc2	dílo
vděčíme	vděčit	k5eAaImIp1nP	vděčit
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
husitským	husitský	k2eAgFnPc3d1	husitská
písařským	písařský	k2eAgFnPc3d1	písařská
dílnám	dílna	k1gFnPc3	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
knihovnách	knihovna	k1gFnPc6	knihovna
zachováno	zachovat	k5eAaPmNgNnS	zachovat
více	hodně	k6eAd2	hodně
rukopisných	rukopisný	k2eAgInPc2d1	rukopisný
opisů	opis	k1gInPc2	opis
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
než	než	k8xS	než
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Některé	některý	k3yIgFnPc4	některý
důležitější	důležitý	k2eAgFnPc4d2	důležitější
práce	práce	k1gFnPc4	práce
===	===	k?	===
</s>
</p>
<p>
<s>
De	De	k?	De
logica	logica	k1gFnSc1	logica
(	(	kIx(	(
<g/>
O	o	k7c6	o
logice	logika	k1gFnSc6	logika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1360	[number]	k4	1360
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Summa	Summa	k1gFnSc1	Summa
de	de	k?	de
ente	ente	k1gFnSc1	ente
(	(	kIx(	(
<g/>
Suma	suma	k1gFnSc1	suma
o	o	k7c6	o
jsoucnu	jsoucno	k1gNnSc6	jsoucno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1362	[number]	k4	1362
<g/>
–	–	k?	–
<g/>
1372	[number]	k4	1372
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
materia	materium	k1gNnSc2	materium
et	et	k?	et
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
O	o	k7c6	o
látce	látka	k1gFnSc6	látka
a	a	k8xC	a
tvaru	tvar	k1gInSc6	tvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1370	[number]	k4	1370
<g/>
–	–	k?	–
<g/>
1372	[number]	k4	1372
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tractatus	Tractatus	k1gMnSc1	Tractatus
de	de	k?	de
benedicta	benedicta	k1gMnSc1	benedicta
incarnatione	incarnation	k1gInSc5	incarnation
Verbi	Verbi	k1gNnPc1	Verbi
(	(	kIx(	(
<g/>
Traktát	traktát	k1gInSc1	traktát
o	o	k7c6	o
blahořečeném	blahořečený	k2eAgNnSc6d1	blahořečený
vtělení	vtělení	k1gNnSc6	vtělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1372	[number]	k4	1372
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
dominio	dominio	k6eAd1	dominio
divino	divin	k2eAgNnSc1d1	Divino
(	(	kIx(	(
<g/>
O	o	k7c6	o
božím	boží	k2eAgNnSc6d1	boží
panství	panství	k1gNnSc6	panství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1373	[number]	k4	1373
<g/>
–	–	k?	–
<g/>
1374	[number]	k4	1374
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
civili	civit	k5eAaBmAgMnP	civit
dominio	dominio	k6eAd1	dominio
(	(	kIx(	(
<g/>
O	o	k7c6	o
civilním	civilní	k2eAgNnSc6d1	civilní
panství	panství	k1gNnSc6	panství
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gFnSc1	Summa
theologiae	theologiae	k1gFnSc1	theologiae
III-V	III-V	k1gFnSc1	III-V
<g/>
]	]	kIx)	]
<g/>
,1370	,1370	k4	,1370
<g/>
–	–	k?	–
<g/>
1375	[number]	k4	1375
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
veritate	veritat	k1gMnSc5	veritat
Sacrae	Sacraus	k1gMnSc5	Sacraus
Scripturae	Scripturaus	k1gMnSc5	Scripturaus
(	(	kIx(	(
<g/>
O	o	k7c6	o
pravdivosti	pravdivost	k1gFnSc6	pravdivost
svatého	svatý	k2eAgNnSc2d1	svaté
Písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gNnSc1	Summa
theologiae	theologiae	k1gFnPc2	theologiae
VI	VI	kA	VI
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
(	(	kIx(	(
<g/>
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gNnSc1	Summa
theologiae	theologiae	k1gFnPc2	theologiae
VII	VII	kA	VII
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
officio	officio	k1gNnSc1	officio
regis	regis	k1gFnSc1	regis
(	(	kIx(	(
<g/>
O	o	k7c6	o
povinnostech	povinnost	k1gFnPc6	povinnost
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gNnSc1	Summa
theologiae	theologiae	k1gFnPc2	theologiae
VIII	VIII	kA	VIII
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
potestate	potestat	k1gMnSc5	potestat
papae	papaus	k1gMnSc5	papaus
(	(	kIx(	(
<g/>
O	o	k7c6	o
moci	moc	k1gFnSc6	moc
papeže	papež	k1gMnSc4	papež
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gNnSc1	Summa
theologiae	theologiae	k1gFnPc2	theologiae
IX	IX	kA	IX
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
apostasia	apostasia	k1gFnSc1	apostasia
(	(	kIx(	(
<g/>
O	o	k7c6	o
odpadnutí	odpadnutí	k1gNnSc6	odpadnutí
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gNnSc1	Summa
theologiae	theologiae	k1gFnPc2	theologiae
XI	XI	kA	XI
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
eucharistia	eucharistia	k1gFnSc1	eucharistia
(	(	kIx(	(
<g/>
O	o	k7c6	o
eucharistii	eucharistie	k1gFnSc6	eucharistie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
eucharistia	eucharistia	k1gFnSc1	eucharistia
confessio	confessio	k1gMnSc1	confessio
(	(	kIx(	(
<g/>
Vyznání	vyznání	k1gNnSc1	vyznání
o	o	k7c4	o
eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
1381	[number]	k4	1381
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
blasphemia	blasphemia	k1gFnSc1	blasphemia
(	(	kIx(	(
<g/>
O	o	k7c6	o
rouhání	rouhání	k1gNnSc6	rouhání
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Summa	Summa	k1gNnSc1	Summa
theologiae	theologiae	k1gFnPc2	theologiae
XII	XII	kA	XII
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1381	[number]	k4	1381
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Summa	Summa	k1gFnSc1	Summa
theologiae	theologiae	k1gFnSc1	theologiae
(	(	kIx(	(
<g/>
Suma	suma	k1gFnSc1	suma
teologická	teologický	k2eAgFnSc1d1	teologická
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
12	[number]	k4	12
traktátů	traktát	k1gInPc2	traktát
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1375	[number]	k4	1375
<g/>
–	–	k?	–
<g/>
1381	[number]	k4	1381
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
contrarietate	contrarietat	k1gMnSc5	contrarietat
duorum	duorum	k1gInSc4	duorum
dominorum	dominorum	k1gInSc4	dominorum
(	(	kIx(	(
<g/>
O	o	k7c6	o
protikladnosti	protikladnost	k1gFnSc6	protikladnost
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1382	[number]	k4	1382
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
Cruciata	Cruciata	k1gFnSc1	Cruciata
<g/>
.	.	kIx.	.
</s>
<s>
Contra	Contra	k1gFnSc1	Contra
bella	belt	k5eAaBmAgFnS	belt
clericorum	clericorum	k1gInSc4	clericorum
(	(	kIx(	(
<g/>
O	o	k7c6	o
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
kněžským	kněžský	k2eAgFnPc3d1	kněžská
válkám	válka	k1gFnPc3	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dialogus	Dialogus	k1gInSc1	Dialogus
sive	sivat	k5eAaPmIp3nS	sivat
Speculum	Speculum	k1gInSc4	Speculum
Ecclesiae	Ecclesiae	k1gFnSc1	Ecclesiae
Militantis	Militantis	k1gFnSc1	Militantis
(	(	kIx(	(
<g/>
Dialog	dialog	k1gInSc1	dialog
aneb	aneb	k?	aneb
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
církve	církev	k1gFnSc2	církev
bojující	bojující	k2eAgMnSc1d1	bojující
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
perfectione	perfection	k1gInSc5	perfection
statuum	statuum	k1gNnSc4	statuum
(	(	kIx(	(
<g/>
O	o	k7c4	o
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
stavů	stav	k1gInPc2	stav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
triplici	triplice	k1gFnSc4	triplice
vinculo	vincout	k5eAaPmAgNnS	vincout
amoris	amoris	k1gFnSc2	amoris
(	(	kIx(	(
<g/>
Trojnásobný	trojnásobný	k2eAgInSc1d1	trojnásobný
svazek	svazek	k1gInSc1	svazek
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
citationibus	citationibus	k1gInSc1	citationibus
frivolis	frivolis	k1gInSc1	frivolis
(	(	kIx(	(
<g/>
O	o	k7c6	o
bezpodstatných	bezpodstatný	k2eAgNnPc6d1	bezpodstatné
předvoláních	předvolání	k1gNnPc6	předvolání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trialogus	Trialogus	k1gInSc1	Trialogus
(	(	kIx(	(
<g/>
Rozhovor	rozhovor	k1gInSc1	rozhovor
tří	tři	k4xCgFnPc2	tři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1382	[number]	k4	1382
<g/>
–	–	k?	–
<g/>
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opus	opus	k1gInSc1	opus
evangelicum	evangelicum	k1gInSc1	evangelicum
(	(	kIx(	(
<g/>
Evangelické	evangelický	k2eAgNnSc1d1	evangelické
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1383	[number]	k4	1383
<g/>
–	–	k?	–
<g/>
1384	[number]	k4	1384
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
částem	část	k1gFnPc3	část
evangelia	evangelium	k1gNnSc2	evangelium
Matoušova	Matoušův	k2eAgNnSc2d1	Matoušovo
a	a	k8xC	a
Janova	Janův	k2eAgNnSc2d1	Janovo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Viklef	Viklef	k1gMnSc1	Viklef
již	již	k6eAd1	již
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
===	===	k?	===
</s>
</p>
<p>
<s>
WYCLIFFE	WYCLIFFE	kA	WYCLIFFE
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Mistra	mistr	k1gMnSc4	mistr
Jakoubka	Jakoubek	k1gMnSc4	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
Překlad	překlad	k1gInSc4	překlad
Viklefova	Viklefův	k2eAgInSc2d1	Viklefův
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
slovesnost	slovesnost	k1gFnSc1	slovesnost
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
XXXVIII	XXXVIII	kA	XXXVIII
<g/>
,	,	kIx,	,
197	[number]	k4	197
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Vydání	vydání	k1gNnSc1	vydání
staročeského	staročeský	k2eAgInSc2d1	staročeský
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
KAŇÁK	KAŇÁK	kA	KAŇÁK
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
anglického	anglický	k2eAgMnSc2d1	anglický
Husova	Husův	k2eAgMnSc2d1	Husův
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
církevní	církevní	k2eAgNnSc1d1	církevní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
233	[number]	k4	233
s.	s.	k?	s.
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
následujících	následující	k2eAgInPc2d1	následující
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
spisů	spis	k1gInPc2	spis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dialogus	Dialogus	k1gInSc1	Dialogus
sive	sivat	k5eAaPmIp3nS	sivat
Speculum	Speculum	k1gInSc4	Speculum
Ecclesiae	Ecclesiae	k1gFnSc1	Ecclesiae
Militantis	Militantis	k1gFnSc1	Militantis
(	(	kIx(	(
<g/>
Dialog	dialog	k1gInSc1	dialog
aneb	aneb	k?	aneb
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
církve	církev	k1gFnSc2	církev
bojující	bojující	k2eAgMnSc1d1	bojující
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výňatek	výňatek	k1gInSc1	výňatek
v	v	k7c6	v
novočeském	novočeský	k2eAgInSc6d1	novočeský
překladu	překlad	k1gInSc6	překlad
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
102	[number]	k4	102
<g/>
–	–	k?	–
<g/>
111	[number]	k4	111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Summa	Summa	k1gFnSc1	Summa
de	de	k?	de
ente	ente	k1gFnSc1	ente
(	(	kIx(	(
<g/>
Suma	suma	k1gFnSc1	suma
o	o	k7c6	o
jsoucnu	jsoucno	k1gNnSc6	jsoucno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výňatek	výňatek	k1gInSc1	výňatek
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
113	[number]	k4	113
<g/>
–	–	k?	–
<g/>
131	[number]	k4	131
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
citationibus	citationibus	k1gInSc1	citationibus
frivolis	frivolis	k1gInSc1	frivolis
(	(	kIx(	(
<g/>
O	o	k7c6	o
bezpodstatných	bezpodstatný	k2eAgNnPc6d1	bezpodstatné
předvoláních	předvolání	k1gNnPc6	předvolání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
133	[number]	k4	133
<g/>
–	–	k?	–
<g/>
143	[number]	k4	143
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
contrarietate	contrarietat	k1gMnSc5	contrarietat
duorum	duorum	k1gInSc4	duorum
dominorum	dominorum	k1gInSc4	dominorum
(	(	kIx(	(
<g/>
O	o	k7c6	o
protikladnosti	protikladnost	k1gFnSc6	protikladnost
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
145	[number]	k4	145
<g/>
–	–	k?	–
<g/>
156	[number]	k4	156
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
Cruciata	Cruciata	k1gFnSc1	Cruciata
<g/>
.	.	kIx.	.
</s>
<s>
Contra	Contra	k1gFnSc1	Contra
bella	belnout	k5eAaImAgFnS	belnout
clericorum	clericorum	k1gInSc4	clericorum
(	(	kIx(	(
<g/>
O	o	k7c6	o
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
kněžským	kněžský	k2eAgFnPc3d1	kněžská
válkám	válka	k1gFnPc3	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
159	[number]	k4	159
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
biblických	biblický	k2eAgInPc2d1	biblický
komentářů	komentář	k1gInPc2	komentář
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
183	[number]	k4	183
<g/>
–	–	k?	–
<g/>
198	[number]	k4	198
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trialogus	Trialogus	k1gInSc1	Trialogus
(	(	kIx(	(
<g/>
Rozhovor	rozhovor	k1gInSc1	rozhovor
tří	tři	k4xCgFnPc2	tři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výňatek	výňatek	k1gInSc1	výňatek
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
201	[number]	k4	201
<g/>
–	–	k?	–
<g/>
211	[number]	k4	211
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MOLNÁR	MOLNÁR	kA	MOLNÁR
<g/>
,	,	kIx,	,
Amedeo	Amedeo	k1gMnSc1	Amedeo
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Slovem	slovem	k6eAd1	slovem
obnovená	obnovený	k2eAgNnPc1d1	obnovené
<g/>
:	:	kIx,	:
čtení	čtení	k1gNnSc1	čtení
o	o	k7c4	o
reformaci	reformace	k1gFnSc4	reformace
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
církevní	církevní	k2eAgNnSc1d1	církevní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
265	[number]	k4	265
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
32	[number]	k4	32
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
De	De	k?	De
civili	civit	k5eAaBmAgMnP	civit
dominio	dominio	k6eAd1	dominio
(	(	kIx(	(
<g/>
O	o	k7c6	o
civilním	civilní	k2eAgNnSc6d1	civilní
panství	panství	k1gNnSc6	panství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
výňatek	výňatek	k1gInSc4	výňatek
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
veritate	veritat	k1gMnSc5	veritat
Sacrae	Sacraus	k1gMnSc5	Sacraus
Scripturae	Scripturaus	k1gMnSc5	Scripturaus
(	(	kIx(	(
<g/>
O	o	k7c6	o
pravdivosti	pravdivost	k1gFnSc6	pravdivost
svatého	svatý	k2eAgNnSc2d1	svaté
Písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výňatek	výňatek	k1gInSc1	výňatek
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dialogus	Dialogus	k1gInSc1	Dialogus
sive	sivat	k5eAaPmIp3nS	sivat
Speculum	Speculum	k1gInSc4	Speculum
Ecclesiae	Ecclesiae	k1gFnSc1	Ecclesiae
Militantis	Militantis	k1gFnSc1	Militantis
(	(	kIx(	(
<g/>
Dialog	dialog	k1gInSc1	dialog
aneb	aneb	k?	aneb
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
církve	církev	k1gFnSc2	církev
bojující	bojující	k2eAgMnSc1d1	bojující
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výňatek	výňatek	k1gInSc1	výňatek
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Jakoubka	jakoubka	k1gFnSc1	jakoubka
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CRAIG	CRAIG	kA	CRAIG
<g/>
,	,	kIx,	,
Hardin	Hardin	k2eAgMnSc1d1	Hardin
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
anglické	anglický	k2eAgFnSc2d1	anglická
literatury	literatura	k1gFnSc2	literatura
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
422	[number]	k4	422
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAŇÁK	KAŇÁK	kA	KAŇÁK
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Viklef	Viklef	k1gMnSc1	Viklef
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
anglického	anglický	k2eAgMnSc2d1	anglický
Husova	Husův	k2eAgMnSc2d1	Husův
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
církevní	církevní	k2eAgNnSc1d1	církevní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
233	[number]	k4	233
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KVAČALA	KVAČALA	kA	KVAČALA
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
<g/>
.	.	kIx.	.
</s>
<s>
Wiklef	Wiklef	k1gMnSc1	Wiklef
a	a	k8xC	a
Hus	Hus	k1gMnSc1	Hus
ako	ako	k?	ako
filosofi	filosof	k1gFnSc2	filosof
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
čes.	čes.	k?	čes.
spol	spol	k1gInSc4	spol
<g/>
.	.	kIx.	.
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
91	[number]	k4	91
s.	s.	k?	s.
Věstník	věstník	k1gMnSc1	věstník
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
čes.	čes.	k?	čes.
spol	spol	k1gInSc4	spol
<g/>
.	.	kIx.	.
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Tř	tř	kA	tř
<g/>
.	.	kIx.	.
fil.	fil.	k?	fil.
<g/>
-	-	kIx~	-
<g/>
hist.	hist.	k?	hist.
<g/>
-jazykozp	azykozp	k1gInSc1	-jazykozp
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LANE	LANE	kA	LANE
<g/>
,	,	kIx,	,
A.	A.	kA	A.
N.	N.	kA	N.
S.	S.	kA	S.
Dějiny	dějiny	k1gFnPc1	dějiny
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Bartoň	Bartoň	k1gMnSc1	Bartoň
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1996	[number]	k4	1996
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
286	[number]	k4	286
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85495	[number]	k4	85495
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MOLNÁR	MOLNÁR	kA	MOLNÁR
<g/>
,	,	kIx,	,
Amedeo	Amedeo	k6eAd1	Amedeo
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
teologického	teologický	k2eAgNnSc2d1	teologické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
:	:	kIx,	:
Přehledné	přehledný	k2eAgFnPc1d1	přehledná
dějiny	dějiny	k1gFnPc1	dějiny
dogmatu	dogma	k1gNnSc2	dogma
<g/>
:	:	kIx,	:
Skripta	skripta	k1gNnPc4	skripta
pro	pro	k7c4	pro
stud	stud	k1gInSc4	stud
<g/>
.	.	kIx.	.
účely	účel	k1gInPc4	účel
Komenského	Komenského	k2eAgFnSc2d1	Komenského
evangelické	evangelický	k2eAgFnSc2d1	evangelická
bohoslovecké	bohoslovecký	k2eAgFnSc2d1	bohoslovecká
fak	fak	k?	fak
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
církevní	církevní	k2eAgNnSc1d1	církevní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
440	[number]	k4	440
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MOLNÁR	MOLNÁR	kA	MOLNÁR
<g/>
,	,	kIx,	,
Amedeo	Amedeo	k6eAd1	Amedeo
<g/>
.	.	kIx.	.
</s>
<s>
Stárnoucí	stárnoucí	k2eAgMnSc1d1	stárnoucí
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
a	a	k8xC	a
logika	logika	k1gFnSc1	logika
Písma	písmo	k1gNnSc2	písmo
svatého	svatý	k2eAgNnSc2d1	svaté
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BONDY	bond	k1gInPc1	bond
<g/>
,	,	kIx,	,
Egon	Egon	k1gMnSc1	Egon
a	a	k8xC	a
Machovec	Machovec	k1gMnSc1	Machovec
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
sklonku	sklonek	k1gInSc2	sklonek
antiky	antika	k1gFnSc2	antika
a	a	k8xC	a
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
254	[number]	k4	254
s.	s.	k?	s.
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85239	[number]	k4	85239
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
především	především	k9	především
str	str	kA	str
<g/>
.	.	kIx.	.
235	[number]	k4	235
<g/>
–	–	k?	–
<g/>
237	[number]	k4	237
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gInSc4	Viklef
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Viklef	Viklef	k1gMnSc1	Viklef
</s>
</p>
<p>
<s>
WYCLIFFE	WYCLIFFE	kA	WYCLIFFE
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Tractatus	Tractatus	k1gInSc4	Tractatus
theologici	theologik	k1gMnPc1	theologik
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
1444	[number]	k4	1444
a	a	k8xC	a
1449	[number]	k4	1449
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
http://www.filosof.cz	[url]	k1gMnSc1	http://www.filosof.cz
</s>
</p>
<p>
<s>
https://web.archive.org/web/20160304124918/http://wyclif.wz.cz/	[url]	k4	https://web.archive.org/web/20160304124918/http://wyclif.wz.cz/
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
John	John	k1gMnSc1	John
Wycliffe	Wycliff	k1gInSc5	Wycliff
-	-	kIx~	-
en	en	k?	en
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Wyclif	Wyclif	k1gMnSc1	Wyclif
heslo	heslo	k1gNnSc4	heslo
v	v	k7c4	v
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
-	-	kIx~	-
en	en	k?	en
</s>
</p>
<p>
<s>
Wycliffe	Wycliff	k1gMnSc5	Wycliff
Memorial	Memorial	k1gMnSc1	Memorial
Page	Pag	k1gFnSc2	Pag
–	–	k?	–
Protestantský	protestantský	k2eAgInSc4d1	protestantský
pohled	pohled	k1gInSc4	pohled
-	-	kIx~	-
en	en	k?	en
</s>
</p>
<p>
<s>
ReligionFacts	ReligionFacts	k1gInSc1	ReligionFacts
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
Wycliffe	Wycliff	k1gInSc5	Wycliff
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
–	–	k?	–
katolický	katolický	k2eAgInSc4d1	katolický
pohled	pohled	k1gInSc4	pohled
-	-	kIx~	-
en	en	k?	en
</s>
</p>
<p>
<s>
Wyclif	Wyclif	k1gInSc1	Wyclif
ve	v	k7c4	v
Stanford	Stanford	k1gInSc4	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Philosophy	Philosoph	k1gInPc7	Philosoph
</s>
</p>
<p>
<s>
Wyclifovo	Wyclifův	k2eAgNnSc1d1	Wyclifův
politické	politický	k2eAgNnSc1d1	politické
myšlení	myšlení	k1gNnSc1	myšlení
-	-	kIx~	-
en	en	k?	en
</s>
</p>
<p>
<s>
Viklef	Viklef	k1gMnSc1	Viklef
Jan	Jan	k1gMnSc1	Jan
na	na	k7c4	na
www.husitstvi.cz	www.husitstvi.cz	k1gInSc4	www.husitstvi.cz
</s>
</p>
