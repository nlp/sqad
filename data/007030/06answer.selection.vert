<s>
První	první	k4xOgFnSc4	první
systematicky	systematicky	k6eAd1	systematicky
fotografovanou	fotografovaný	k2eAgFnSc4d1	fotografovaná
válečnou	válečná	k1gFnSc4	válečná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
byla	být	k5eAaImAgFnS	být
veřejnost	veřejnost	k1gFnSc1	veřejnost
informována	informovat	k5eAaBmNgFnS	informovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zpráv	zpráva	k1gFnPc2	zpráva
posílaných	posílaný	k2eAgFnPc2d1	posílaná
telegrafem	telegraf	k1gInSc7	telegraf
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
