<s>
Řečtina	řečtina	k1gFnSc1	řečtina
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
ε	ε	k?	ε
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
)	)	kIx)	)
–	–	k?	–
ellinikí	ellinikí	k1gFnSc1	ellinikí
(	(	kIx(	(
<g/>
glóssa	glóssa	k1gFnSc1	glóssa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
řecký	řecký	k2eAgInSc1d1	řecký
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ε	ε	k?	ε
–	–	k?	–
elliniká	ellinikat	k5eAaPmIp3nS	ellinikat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
indoevropský	indoevropský	k2eAgInSc1d1	indoevropský
jazyk	jazyk	k1gInSc1	jazyk
používaný	používaný	k2eAgInSc1d1	používaný
autochtonním	autochtonní	k2eAgNnSc7d1	autochtonní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
především	především	k9	především
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
a	a	k8xC	a
v	v	k7c6	v
částech	část	k1gFnPc6	část
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
emigračních	emigrační	k2eAgFnPc6d1	emigrační
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
aj.	aj.	kA	aj.
Novořečtina	novořečtina	k1gFnSc1	novořečtina
čili	čili	k8xC	čili
současná	současný	k2eAgFnSc1d1	současná
řečtina	řečtina	k1gFnSc1	řečtina
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ze	z	k7c2	z
starořečtiny	starořečtina	k1gFnSc2	starořečtina
<g/>
.	.	kIx.	.
</s>
<s>
Řečtina	řečtina	k1gFnSc1	řečtina
tvoří	tvořit	k5eAaImIp3nS	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
větev	větev	k1gFnSc4	větev
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
vedle	vedle	k7c2	vedle
aramejštiny	aramejština	k1gFnSc2	aramejština
a	a	k8xC	a
čínštiny	čínština	k1gFnSc2	čínština
je	být	k5eAaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
s	s	k7c7	s
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
literární	literární	k2eAgFnSc7d1	literární
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
jazykem	jazyk	k1gInSc7	jazyk
řecké	řecký	k2eAgFnSc2d1	řecká
jazykové	jazykový	k2eAgFnSc2d1	jazyková
větve	větev	k1gFnSc2	větev
je	být	k5eAaImIp3nS	být
mykénština	mykénština	k1gFnSc1	mykénština
(	(	kIx(	(
<g/>
Kréta	Kréta	k1gFnSc1	Kréta
a	a	k8xC	a
pevninské	pevninský	k2eAgNnSc1d1	pevninské
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
zapisována	zapisovat	k5eAaImNgFnS	zapisovat
pomocí	pomocí	k7c2	pomocí
dosud	dosud	k6eAd1	dosud
nerozluštěného	rozluštěný	k2eNgNnSc2d1	nerozluštěné
lineárního	lineární	k2eAgNnSc2d1	lineární
písma	písmo	k1gNnSc2	písmo
A	A	kA	A
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
lineární	lineární	k2eAgNnSc4d1	lineární
písmo	písmo	k1gNnSc4	písmo
B	B	kA	B
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgFnPc1d1	psána
první	první	k4xOgFnPc1	první
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
(	(	kIx(	(
<g/>
tabulky	tabulka	k1gFnPc1	tabulka
z	z	k7c2	z
Knóssu	Knóss	k1gInSc2	Knóss
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1	lineární
písmo	písmo	k1gNnSc1	písmo
B	B	kA	B
rozluštil	rozluštit	k5eAaPmAgInS	rozluštit
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
Michael	Michael	k1gMnSc1	Michael
Ventris	Ventris	k1gFnSc4	Ventris
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
John	John	k1gMnSc1	John
Chadwick	Chadwick	k1gMnSc1	Chadwick
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Bartoněk	Bartoněk	k1gMnSc1	Bartoněk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
zachované	zachovaný	k2eAgInPc1d1	zachovaný
doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
podobě	podoba	k1gFnSc6	podoba
řečtiny	řečtina	k1gFnSc2	řečtina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Homérovy	Homérův	k2eAgFnPc1d1	Homérova
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
psané	psaný	k2eAgFnPc1d1	psaná
v	v	k7c6	v
iónském	iónský	k2eAgInSc6d1	iónský
dialektu	dialekt	k1gInSc6	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnPc1d1	literární
památky	památka	k1gFnPc1	památka
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dokládají	dokládat	k5eAaImIp3nP	dokládat
klasickou	klasický	k2eAgFnSc4d1	klasická
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
attická	attický	k2eAgFnSc1d1	attický
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
i	i	k9	i
dnes	dnes	k6eAd1	dnes
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
jako	jako	k9	jako
starořečtina	starořečtina	k1gFnSc1	starořečtina
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
klasická	klasický	k2eAgFnSc1d1	klasická
řečtina	řečtina	k1gFnSc1	řečtina
totiž	totiž	k9	totiž
neměla	mít	k5eNaImAgFnS	mít
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
dialektech	dialekt	k1gInPc6	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
kmen	kmen	k1gInSc1	kmen
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nářečí	nářečí	k1gNnSc6	nářečí
určitý	určitý	k2eAgInSc4d1	určitý
literární	literární	k2eAgInSc4d1	literární
druh	druh	k1gInSc4	druh
<g/>
:	:	kIx,	:
Dórové	Dór	k1gMnPc1	Dór
→	→	k?	→
dórština	dórština	k1gFnSc1	dórština
(	(	kIx(	(
<g/>
sborová	sborový	k2eAgFnSc1d1	sborová
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
)	)	kIx)	)
Attičané	Attičan	k1gMnPc1	Attičan
→	→	k?	→
attičtina	attičtina	k1gFnSc1	attičtina
(	(	kIx(	(
<g/>
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
politologická	politologický	k2eAgFnSc1d1	politologická
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
Iónové	Ión	k1gMnPc1	Ión
→	→	k?	→
ionština	ionština	k1gFnSc1	ionština
(	(	kIx(	(
<g/>
epika	epika	k1gFnSc1	epika
<g/>
,	,	kIx,	,
historiografická	historiografický	k2eAgFnSc1d1	historiografická
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
Aiolové	Aiolové	k2eAgFnSc1d1	Aiolové
→	→	k?	→
aiolština	aiolština	k1gFnSc1	aiolština
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Koiné	koiné	k1gFnSc2	koiné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
athénské	athénský	k2eAgFnSc2d1	Athénská
demokracie	demokracie	k1gFnSc2	demokracie
nastává	nastávat	k5eAaImIp3nS	nastávat
období	období	k1gNnSc1	období
helénismu	helénismus	k1gInSc2	helénismus
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlavně	hlavně	k6eAd1	hlavně
attického	attický	k2eAgInSc2d1	attický
dialektu	dialekt	k1gInSc2	dialekt
postupně	postupně	k6eAd1	postupně
vznikal	vznikat	k5eAaImAgInS	vznikat
tzv.	tzv.	kA	tzv.
koiné	koiné	k1gFnSc2	koiné
dialektos	dialektos	k1gInSc1	dialektos
–	–	k?	–
obecný	obecný	k2eAgInSc1d1	obecný
řecký	řecký	k2eAgInSc1d1	řecký
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
stručně	stručně	k6eAd1	stručně
koiné	koiné	k1gFnSc1	koiné
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
dhimotikí	dhimotikí	k1gMnSc1	dhimotikí
ghlósa	ghlós	k1gMnSc2	ghlós
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
řečtinou	řečtina	k1gFnSc7	řečtina
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
<g/>
:	:	kIx,	:
melodický	melodický	k2eAgInSc1d1	melodický
přízvuk	přízvuk	k1gInSc1	přízvuk
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
v	v	k7c4	v
dynamický	dynamický	k2eAgInSc4d1	dynamický
(	(	kIx(	(
<g/>
důrazový	důrazový	k2eAgInSc4d1	důrazový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zanikly	zaniknout	k5eAaPmAgInP	zaniknout
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
krátkými	krátký	k2eAgFnPc7d1	krátká
a	a	k8xC	a
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
,	,	kIx,	,
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
(	(	kIx(	(
<g/>
diftongy	diftong	k1gInPc1	diftong
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
proměnily	proměnit	k5eAaPmAgFnP	proměnit
v	v	k7c4	v
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
samohlásky	samohláska	k1gFnPc4	samohláska
(	(	kIx(	(
<g/>
monoftongy	monoftong	k1gInPc4	monoftong
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
už	už	k6eAd1	už
blíží	blížit	k5eAaImIp3nS	blížit
novořečtině	novořečtina	k1gFnSc3	novořečtina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podstatným	podstatný	k2eAgFnPc3d1	podstatná
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c6	v
tvarosloví	tvarosloví	k1gNnSc6	tvarosloví
<g/>
,	,	kIx,	,
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
tendence	tendence	k1gFnPc1	tendence
analytické	analytický	k2eAgFnSc2d1	analytická
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
některé	některý	k3yIgInPc4	některý
gramatické	gramatický	k2eAgInPc4d1	gramatický
významy	význam	k1gInPc4	význam
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
pomocí	pomocí	k7c2	pomocí
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
slov	slovo	k1gNnPc2	slovo
místo	místo	k1gNnSc4	místo
změnou	změna	k1gFnSc7	změna
tvaru	tvar	k1gInSc2	tvar
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
časů	čas	k1gInPc2	čas
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
opisy	opis	k1gInPc4	opis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skloňování	skloňování	k1gNnSc6	skloňování
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
redukci	redukce	k1gFnSc4	redukce
pádů	pád	k1gInPc2	pád
atd.	atd.	kA	atd.
Tato	tento	k3xDgFnSc1	tento
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
koiné	koiné	k1gFnSc1	koiné
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výbojů	výboj	k1gInPc2	výboj
Alexandra	Alexandra	k1gFnSc1	Alexandra
Velikého	veliký	k2eAgInSc2d1	veliký
(	(	kIx(	(
<g/>
356	[number]	k4	356
<g/>
–	–	k?	–
<g/>
323	[number]	k4	323
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
i	i	k8xC	i
obecně	obecně	k6eAd1	obecně
komunikační	komunikační	k2eAgInSc4d1	komunikační
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lingva	lingv	k1gMnSc2	lingv
franka	franek	k1gMnSc2	franek
<g/>
)	)	kIx)	)
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
řeckých	řecký	k2eAgFnPc6d1	řecká
osadách	osada	k1gFnPc6	osada
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
jazykem	jazyk	k1gInSc7	jazyk
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
řečtina	řečtina	k1gFnSc1	řečtina
dlouho	dlouho	k6eAd1	dlouho
užívala	užívat	k5eAaImAgFnS	užívat
jako	jako	k9	jako
jazyk	jazyk	k1gInSc4	jazyk
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
snažili	snažit	k5eAaImAgMnP	snažit
udržovat	udržovat	k5eAaImF	udržovat
klasickou	klasický	k2eAgFnSc4d1	klasická
attičtinu	attičtina	k1gFnSc4	attičtina
(	(	kIx(	(
<g/>
atticismus	atticismus	k1gInSc4	atticismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
a	a	k8xC	a
pádu	pád	k1gInSc6	pád
západní	západní	k2eAgFnSc2d1	západní
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
476	[number]	k4	476
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řečtina	řečtina	k1gFnSc1	řečtina
stala	stát	k5eAaPmAgFnS	stát
jedinou	jediný	k2eAgFnSc7d1	jediná
úřední	úřední	k2eAgFnSc7d1	úřední
řečí	řeč	k1gFnSc7	řeč
východní	východní	k2eAgFnSc2d1	východní
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
latinsky	latinsky	k6eAd1	latinsky
mluvících	mluvící	k2eAgFnPc2d1	mluvící
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
se	se	k3xPyFc4	se
z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
říše	říš	k1gFnSc2	říš
stala	stát	k5eAaPmAgFnS	stát
Byzanc	Byzanc	k1gFnSc1	Byzanc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prodělala	prodělat	k5eAaPmAgFnS	prodělat
jistou	jistý	k2eAgFnSc4d1	jistá
kulturní	kulturní	k2eAgFnSc4d1	kulturní
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
gramatického	gramatický	k2eAgNnSc2d1	gramatické
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
elit	elita	k1gFnPc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
začala	začít	k5eAaPmAgFnS	začít
koncem	koncem	k7c2	koncem
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
oživovat	oživovat	k5eAaImF	oživovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
už	už	k9	už
řečtina	řečtina	k1gFnSc1	řečtina
spíše	spíše	k9	spíše
hovorová	hovorový	k2eAgFnSc1d1	hovorová
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
blízká	blízký	k2eAgFnSc1d1	blízká
novořečtině	novořečtina	k1gFnSc3	novořečtina
<g/>
.	.	kIx.	.
</s>
<s>
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
i	i	k9	i
silnými	silný	k2eAgFnPc7d1	silná
místními	místní	k2eAgFnPc7d1	místní
zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
působily	působit	k5eAaImAgFnP	působit
odlišné	odlišný	k2eAgInPc4d1	odlišný
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
kulturní	kulturní	k2eAgInPc4d1	kulturní
vlivy	vliv	k1gInPc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
četné	četný	k2eAgFnPc4d1	četná
přejímky	přejímka	k1gFnPc4	přejímka
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
,	,	kIx,	,
italština	italština	k1gFnSc1	italština
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
dnešní	dnešní	k2eAgFnSc2d1	dnešní
řečtiny	řečtina	k1gFnSc2	řečtina
původní	původní	k2eAgNnPc4d1	původní
řecká	řecký	k2eAgNnPc4d1	řecké
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Přídechy	přídech	k1gInPc1	přídech
se	se	k3xPyFc4	se
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
dříve	dříve	k6eAd2	dříve
psaly	psát	k5eAaImAgFnP	psát
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
řečtině	řečtina	k1gFnSc6	řečtina
se	se	k3xPyFc4	se
nepíší	psát	k5eNaImIp3nP	psát
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
s	s	k7c7	s
přídechem	přídech	k1gInSc7	přídech
a	a	k8xC	a
bez	bez	k7c2	bez
něj	on	k3xPp3gMnSc2	on
už	už	k6eAd1	už
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ostrý	ostrý	k2eAgInSc1d1	ostrý
–	–	k?	–
̇	̇	k?	̇
[	[	kIx(	[
<g/>
h	h	k?	h
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
̇	̇	k?	̇
<g/>
Aν	Aν	k1gFnSc1	Aν
<g/>
;	;	kIx,	;
̇	̇	k?	̇
<g/>
α	α	k?	α
<g/>
;	;	kIx,	;
̇	̇	k?	̇
<g/>
ρ	ρ	k?	ρ
<g/>
;	;	kIx,	;
α	α	k?	α
<g/>
̇	̇	k?	̇
<g />
.	.	kIx.	.
</s>
<s>
<g/>
υ	υ	k?	υ
<g/>
;	;	kIx,	;
ε	ε	k?	ε
<g/>
̇	̇	k?	̇
<g/>
υ	υ	k?	υ
<g/>
;	;	kIx,	;
ο	ο	k?	ο
<g/>
̇	̇	k?	̇
<g/>
υ	υ	k?	υ
jemný	jemný	k2eAgInSc1d1	jemný
-	-	kIx~	-
̉	̉	k?	̉
[	[	kIx(	[
<g/>
]	]	kIx)	]
-	-	kIx~	-
̉	̉	k?	̉
<g/>
Aθ	Aθ	k1gFnSc1	Aθ
<g/>
;	;	kIx,	;
̉	̉	k?	̉
<g/>
α	α	k?	α
<g/>
;	;	kIx,	;
α	α	k?	α
<g/>
̉	̉	k?	̉
<g/>
υ	υ	k?	υ
<g/>
;	;	kIx,	;
ε	ε	k?	ε
<g/>
̉	̉	k?	̉
<g/>
υ	υ	k?	υ
<g/>
;	;	kIx,	;
ο	ο	k?	ο
<g/>
̉	̉	k?	̉
<g/>
υ	υ	k?	υ
Přízvuk	přízvuk	k1gInSc1	přízvuk
bývá	bývat	k5eAaImIp3nS	bývat
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
čárkou	čárka	k1gFnSc7	čárka
nad	nad	k7c7	nad
písmenem	písmeno	k1gNnSc7	písmeno
reprezentujícím	reprezentující	k2eAgNnSc7d1	reprezentující
přízvučnou	přízvučný	k2eAgFnSc4d1	přízvučná
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nápisů	nápis	k1gInPc2	nápis
pouze	pouze	k6eAd1	pouze
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
přízvuk	přízvuk	k1gInSc1	přízvuk
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
nebývá	bývat	k5eNaImIp3nS	bývat
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
<g/>
-li	i	k?	-li
slovo	slovo	k1gNnSc1	slovo
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
a	a	k8xC	a
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
,	,	kIx,	,
čárka	čárka	k1gFnSc1	čárka
se	se	k3xPyFc4	se
vyznačí	vyznačit	k5eAaPmIp3nS	vyznačit
před	před	k7c4	před
písmeno	písmeno	k1gNnSc4	písmeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
́	́	k?	́
<g/>
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnPc1d1	základní
pravidla	pravidlo	k1gNnPc1	pravidlo
čtení	čtení	k1gNnSc2	čtení
pro	pro	k7c4	pro
novořečtinu	novořečtina	k1gFnSc4	novořečtina
<g/>
:	:	kIx,	:
α	α	k?	α
=	=	kIx~	=
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
ο	ο	k?	ο
=	=	kIx~	=
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
ε	ε	k?	ε
=	=	kIx~	=
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
υ	υ	k?	υ
=	=	kIx~	=
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
však	však	k9	však
nad	nad	k7c7	nad
ι	ι	k?	ι
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
(	(	kIx(	(
<g/>
ϊ	ϊ	k?	ϊ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
písmeno	písmeno	k1gNnSc1	písmeno
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
ο	ο	k?	ο
=	=	kIx~	=
[	[	kIx(	[
<g/>
oj	oj	k1gInSc1	oj
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
α	α	k?	α
=	=	kIx~	=
[	[	kIx(	[
<g/>
aj	aj	kA	aj
<g/>
]	]	kIx)	]
apod.	apod.	kA	apod.
ο	ο	k?	ο
=	=	kIx~	=
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
ε	ε	k?	ε
=	=	kIx~	=
[	[	kIx(	[
<g/>
ef	ef	k?	ef
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
ev	ev	k?	ev
<g/>
]	]	kIx)	]
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
následující	následující	k2eAgInPc1d1	následující
hlásky	hlásek	k1gInPc1	hlásek
-	-	kIx~	-
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
neznělá	znělý	k2eNgFnSc1d1	neznělá
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
ef	ef	k?	ef
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
znělá	znělý	k2eAgFnSc1d1	znělá
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
ev	ev	k?	ev
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
efcharisto	efcharista	k1gMnSc5	efcharista
(	(	kIx(	(
<g/>
ε	ε	k?	ε
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Evropi	Evropi	k1gNnSc1	Evropi
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Ε	Ε	k?	Ε
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
β	β	k?	β
=	=	kIx~	=
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
μ	μ	k?	μ
=	=	kIx~	=
[	[	kIx(	[
<g/>
b	b	k?	b
<g/>
]	]	kIx)	]
či	či	k8xC	či
[	[	kIx(	[
<g/>
mb	mb	k?	mb
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
[	[	kIx(	[
<g/>
mp	mp	k?	mp
<g/>
]	]	kIx)	]
ν	ν	k?	ν
=	=	kIx~	=
[	[	kIx(	[
<g/>
d	d	k?	d
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nd	nd	k?	nd
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
[	[	kIx(	[
<g/>
nt	nt	k?	nt
<g/>
]	]	kIx)	]
γ	γ	k?	γ
=	=	kIx~	=
znělé	znělý	k2eAgFnSc2d1	znělá
ch	ch	k0	ch
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
ch	ch	k0	ch
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
zní	znět	k5eAaImIp3nS	znět
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
γ	γ	k?	γ
a	a	k8xC	a
γ	γ	k?	γ
však	však	k9	však
zní	znět	k5eAaImIp3nS	znět
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
γ	γ	k?	γ
=	=	kIx~	=
obvykle	obvykle	k6eAd1	obvykle
[	[	kIx(	[
<g/>
ng	ng	k?	ng
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
γ	γ	k?	γ
=	=	kIx~	=
[	[	kIx(	[
<g/>
ng	ng	k?	ng
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
otazníku	otazník	k1gInSc2	otazník
se	se	k3xPyFc4	se
v	v	k7c6	v
řeckém	řecký	k2eAgInSc6d1	řecký
textu	text	k1gInSc6	text
užívá	užívat	k5eAaImIp3nS	užívat
středník	středník	k1gInSc1	středník
<g/>
,	,	kIx,	,
českému	český	k2eAgInSc3d1	český
středníku	středník	k1gInSc3	středník
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
povýšená	povýšený	k2eAgFnSc1d1	povýšená
tečka	tečka	k1gFnSc1	tečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
téměř	téměř	k6eAd1	téměř
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Apostrof	apostrof	k1gInSc1	apostrof
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
zkrácení	zkrácení	k1gNnSc4	zkrácení
slova	slovo	k1gNnSc2	slovo
o	o	k7c4	o
samohlásku	samohláska	k1gFnSc4	samohláska
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýká	stýkat	k5eAaImIp3nS	stýkat
se	s	k7c7	s
samohláskou	samohláska	k1gFnSc7	samohláska
jiného	jiný	k2eAgNnSc2d1	jiné
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ja	ja	k?	ja
afto	afta	k1gFnSc5	afta
(	(	kIx(	(
<g/>
γ	γ	k?	γ
α	α	k?	α
<g/>
)	)	kIx)	)
→	→	k?	→
j	j	k?	j
<g/>
'	'	kIx"	'
afto	afta	k1gFnSc5	afta
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
'	'	kIx"	'
α	α	k?	α
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DOSTÁLOVÁ	Dostálová	k1gFnSc1	Dostálová
<g/>
,	,	kIx,	,
Růžena	Růžena	k1gFnSc1	Růžena
-	-	kIx~	-
FRANC-SGOURDEOU	FRANC-SGOURDEOU	k1gFnSc1	FRANC-SGOURDEOU
<g/>
,	,	kIx,	,
Catherine	Catherin	k1gInSc5	Catherin
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInSc1d1	základní
kurz	kurz	k1gInSc1	kurz
novořeckého	novořecký	k2eAgInSc2d1	novořecký
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Set	set	k1gInSc1	set
Out	Out	k1gFnSc2	Out
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86277	[number]	k4	86277
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ZERVA	ZERVA	kA	ZERVA
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebojte	bát	k5eNaImRp2nP	bát
se	se	k3xPyFc4	se
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Malý	Malý	k1gMnSc1	Malý
průvodce	průvodce	k1gMnSc1	průvodce
novořeckým	novořecký	k2eAgInSc7d1	novořecký
jazykem	jazyk	k1gInSc7	jazyk
-	-	kIx~	-
"	"	kIx"	"
<g/>
Μ	Μ	k?	Μ
φ	φ	k?	φ
τ	τ	k?	τ
ε	ε	k?	ε
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Μ	Μ	k?	Μ
ο	ο	k?	ο
τ	τ	k?	τ
ν	ν	k?	ν
ε	ε	k?	ε
γ	γ	k?	γ
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Athis	Athis	k1gFnSc1	Athis
Imaging	Imaging	k1gInSc1	Imaging
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
3614	[number]	k4	3614
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Pimsleur	Pimsleur	k1gMnSc1	Pimsleur
Greek	Greek	k1gMnSc1	Greek
Michel	Michel	k1gMnSc1	Michel
Thomas	Thomas	k1gMnSc1	Thomas
Greek	Greek	k1gMnSc1	Greek
Foundation	Foundation	k1gInSc4	Foundation
Rosetta	Rosetto	k1gNnSc2	Rosetto
Stone	ston	k1gInSc5	ston
Greek	Greek	k1gMnSc1	Greek
</s>
