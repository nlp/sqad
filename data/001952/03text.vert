<s>
Oslo	Oslo	k1gNnSc1	Oslo
[	[	kIx(	[
<g/>
v	v	k7c6	v
norštině	norština	k1gFnSc6	norština
čti	číst	k5eAaImRp2nS	číst
Ušlu	Ušl	k2eAgFnSc4d1	Ušl
nebo	nebo	k8xC	nebo
Ošlo	Ošlo	k6eAd1	Ošlo
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Christiania	Christianium	k1gNnSc2	Christianium
a	a	k8xC	a
Kristiania	Kristianium	k1gNnSc2	Kristianium
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
správní	správní	k2eAgNnSc4d1	správní
město	město	k1gNnSc4	město
kraje	kraj	k1gInSc2	kraj
Akershus	Akershus	k1gInSc1	Akershus
a	a	k8xC	a
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
buď	buď	k8xC	buď
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
áss	áss	k?	áss
lo	lo	k?	lo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
boží	boží	k2eAgFnSc1d1	boží
planina	planina	k1gFnSc1	planina
<g/>
)	)	kIx)	)
ve	v	k7c6	v
staronorském	staronorský	k2eAgInSc6d1	staronorský
jazyce	jazyk	k1gInSc6	jazyk
norrø	norrø	k?	norrø
nebo	nebo	k8xC	nebo
výrazu	výraz	k1gInSc3	výraz
ansu	ansus	k1gInSc2	ansus
lo	lo	k?	lo
(	(	kIx(	(
<g/>
planina	planina	k1gFnSc1	planina
nad	nad	k7c7	nad
ústím	ústí	k1gNnSc7	ústí
řeky	řeka	k1gFnSc2	řeka
<g/>
)	)	kIx)	)
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
urnordisk	urnordisk	k1gInSc1	urnordisk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
často	často	k6eAd1	často
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k9	jako
Anslo	Anslo	k1gNnSc1	Anslo
<g/>
,	,	kIx,	,
Ásló	Ásló	k1gFnSc1	Ásló
nebo	nebo	k8xC	nebo
Ósló	Ósló	k1gFnSc1	Ósló
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
nebo	nebo	k8xC	nebo
holandských	holandský	k2eAgInPc6d1	holandský
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latině	latina	k1gFnSc6	latina
pak	pak	k6eAd1	pak
Ansloa	Anslo	k1gInSc2	Anslo
nebo	nebo	k8xC	nebo
Ansloia	Ansloium	k1gNnSc2	Ansloium
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Norska	Norsko	k1gNnSc2	Norsko
u	u	k7c2	u
Oslofjordu	Oslofjord	k1gInSc2	Oslofjord
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1048	[number]	k4	1048
králem	král	k1gMnSc7	král
Haraldem	Harald	k1gMnSc7	Harald
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
rozvoj	rozvoj	k1gInSc1	rozvoj
v	v	k7c4	v
moderní	moderní	k2eAgNnSc4d1	moderní
město	město	k1gNnSc4	město
nastal	nastat	k5eAaPmAgInS	nastat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
převzalo	převzít	k5eAaPmAgNnS	převzít
roli	role	k1gFnSc4	role
nejdůležitějšího	důležitý	k2eAgNnSc2d3	nejdůležitější
norského	norský	k2eAgNnSc2d1	norské
města	město	k1gNnSc2	město
od	od	k7c2	od
Bergenu	Bergen	k1gInSc2	Bergen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Velké	velký	k2eAgNnSc1d1	velké
Oslo	Oslo	k1gNnSc1	Oslo
(	(	kIx(	(
<g/>
Stor	Stor	k1gInSc1	Stor
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
žilo	žít	k5eAaImAgNnS	žít
906	[number]	k4	906
357	[number]	k4	357
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
25	[number]	k4	25
%	%	kIx~	%
tvořili	tvořit	k5eAaImAgMnP	tvořit
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
imigranti	imigrant	k1gMnPc1	imigrant
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
230	[number]	k4	230
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
i	i	k8xC	i
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
UNPD	UNPD	kA	UNPD
(	(	kIx(	(
<g/>
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
Population	Population	k1gInSc1	Population
Division	Division	k1gInSc1	Division
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Oslo	Oslo	k1gNnSc7	Oslo
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
evropským	evropský	k2eAgNnSc7d1	Evropské
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
výrazně	výrazně	k6eAd1	výrazně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
díky	díky	k7c3	díky
liberální	liberální	k2eAgFnSc3d1	liberální
přistěhovalecké	přistěhovalecký	k2eAgFnSc3d1	přistěhovalecká
politice	politika	k1gFnSc3	politika
státu	stát	k1gInSc2	stát
a	a	k8xC	a
otevřenému	otevřený	k2eAgInSc3d1	otevřený
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
trhu	trh	k1gInSc3	trh
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Byrå	Byrå	k1gFnSc1	Byrå
<g/>
)	)	kIx)	)
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
překročí	překročit	k5eAaPmIp3nS	překročit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
454	[number]	k4	454
km2	km2	k4	km2
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k6eAd1	tak
mezi	mezi	k7c4	mezi
nejrozlehlejší	rozlehlý	k2eAgNnPc4d3	nejrozlehlejší
hlavní	hlavní	k2eAgNnPc4d1	hlavní
města	město	k1gNnPc4	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
norských	norský	k2eAgInPc2d1	norský
námořních	námořní	k2eAgInPc2d1	námořní
přístavů	přístav	k1gInPc2	přístav
a	a	k8xC	a
o	o	k7c4	o
důležitý	důležitý	k2eAgInSc4d1	důležitý
železniční	železniční	k2eAgInSc4d1	železniční
a	a	k8xC	a
silniční	silniční	k2eAgInSc4d1	silniční
uzel	uzel	k1gInSc4	uzel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mezinárodnímu	mezinárodní	k2eAgNnSc3d1	mezinárodní
letišti	letiště	k1gNnSc3	letiště
Gardermoen	Gardermona	k1gFnPc2	Gardermona
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Oslo	Oslo	k1gNnSc2	Oslo
vede	vést	k5eAaImIp3nS	vést
moderní	moderní	k2eAgFnSc1d1	moderní
železniční	železniční	k2eAgFnSc1d1	železniční
rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
Flytoget	Flytogeta	k1gFnPc2	Flytogeta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgNnSc7d1	kulturní
a	a	k8xC	a
politickým	politický	k2eAgNnSc7d1	politické
centrem	centrum	k1gNnSc7	centrum
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
Norská	norský	k2eAgFnSc1d1	norská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
udílení	udílení	k1gNnSc4	udílení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
v	v	k7c6	v
norštině	norština	k1gFnSc6	norština
čte	číst	k5eAaImIp3nS	číst
ušlu	ušlu	k6eAd1	ušlu
nebo	nebo	k8xC	nebo
ošlo	ošlo	k6eAd1	ošlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
však	však	k9	však
přípustná	přípustný	k2eAgFnSc1d1	přípustná
a	a	k8xC	a
běžná	běžný	k2eAgFnSc1d1	běžná
výslovnost	výslovnost	k1gFnSc1	výslovnost
oslo	oslo	k6eAd1	oslo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
nesklonný	sklonný	k2eNgInSc1d1	nesklonný
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
skloňovat	skloňovat	k5eAaImF	skloňovat
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
názvu	název	k1gInSc2	název
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
osloský	osloský	k2eAgMnSc1d1	osloský
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
oselský	oselský	k2eAgMnSc1d1	oselský
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1048	[number]	k4	1048
králem	král	k1gMnSc7	král
Haraldem	Harald	k1gMnSc7	Harald
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1299	[number]	k4	1299
<g/>
,	,	kIx,	,
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Hå	Hå	k1gFnSc2	Hå
V.	V.	kA	V.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
hrad	hrad	k1gInSc1	hrad
Akershus	Akershus	k1gInSc1	Akershus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dánské	dánský	k2eAgFnSc2d1	dánská
nadvlády	nadvláda	k1gFnSc2	nadvláda
město	město	k1gNnSc1	město
tento	tento	k3xDgInSc4	tento
status	status	k1gInSc4	status
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Hanzovní	hanzovní	k2eAgFnSc2d1	hanzovní
ligy	liga	k1gFnSc2	liga
-	-	kIx~	-
obchodního	obchodní	k2eAgInSc2d1	obchodní
svazu	svaz	k1gInSc2	svaz
středověkých	středověký	k2eAgNnPc2d1	středověké
německých	německý	k2eAgNnPc2d1	německé
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
město	město	k1gNnSc1	město
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
zálivu	záliv	k1gInSc2	záliv
dánským	dánský	k2eAgMnSc7d1	dánský
králem	král	k1gMnSc7	král
Kristiánem	Kristián	k1gMnSc7	Kristián
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
čas	čas	k1gInSc4	čas
jméno	jméno	k1gNnSc4	jméno
Christiania	Christianium	k1gNnSc2	Christianium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
(	(	kIx(	(
<g/>
Universitetet	Universitetet	k1gInSc1	Universitetet
i	i	k8xC	i
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
se	se	k3xPyFc4	se
Oslo	Oslo	k1gNnSc2	Oslo
stalo	stát	k5eAaPmAgNnS	stát
opět	opět	k6eAd1	opět
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
(	(	kIx(	(
<g/>
Stortinget	stortinget	k1gInSc1	stortinget
<g/>
)	)	kIx)	)
slavnostně	slavnostně	k6eAd1	slavnostně
přijata	přijmout	k5eAaPmNgFnS	přijmout
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
norským	norský	k2eAgInSc7d1	norský
svátkem	svátek	k1gInSc7	svátek
(	(	kIx(	(
<g/>
Den	den	k1gInSc1	den
ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
ještě	ještě	k6eAd1	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
na	na	k7c6	na
Kristianii	Kristianie	k1gFnSc6	Kristianie
a	a	k8xC	a
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
opět	opět	k6eAd1	opět
na	na	k7c6	na
Oslo	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
historie	historie	k1gFnSc1	historie
vzniku	vznik	k1gInSc2	vznik
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
Osla	Oslo	k1gNnSc2	Oslo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
postava	postava	k1gFnSc1	postava
svatého	svatý	k2eAgMnSc2d1	svatý
Hallvarda	Hallvard	k1gMnSc2	Hallvard
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
sedícího	sedící	k2eAgNnSc2d1	sedící
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nP	držet
tři	tři	k4xCgInPc4	tři
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
v	v	k7c6	v
pravé	pravá	k1gFnSc6	pravá
mlýnský	mlýnský	k2eAgInSc4d1	mlýnský
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
znaku	znak	k1gInSc2	znak
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
heslo	heslo	k1gNnSc1	heslo
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
a	a	k8xC	a
pevné	pevný	k2eAgNnSc1d1	pevné
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Hallvard	Hallvard	k1gMnSc1	Hallvard
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
náčelníka	náčelník	k1gMnSc2	náčelník
Vebjø	Vebjø	k1gMnSc2	Vebjø
Husaby	Husaba	k1gMnSc2	Husaba
z	z	k7c2	z
osady	osada	k1gFnSc2	osada
Lier	Liera	k1gFnPc2	Liera
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
kraji	kraj	k1gInSc6	kraj
Drammen	Drammen	k1gInSc4	Drammen
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Osla	Oslo	k1gNnSc2	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
obchodníkem	obchodník	k1gMnSc7	obchodník
a	a	k8xC	a
plavil	plavit	k5eAaImAgInS	plavit
se	se	k3xPyFc4	se
často	často	k6eAd1	často
za	za	k7c7	za
obchodem	obchod	k1gInSc7	obchod
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
seděl	sedět	k5eAaImAgMnS	sedět
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Drammensfjordu	Drammensfjord	k1gInSc2	Drammensfjord
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
náhle	náhle	k6eAd1	náhle
přišla	přijít	k5eAaPmAgFnS	přijít
těhotná	těhotný	k2eAgFnSc1d1	těhotná
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
prosila	prosít	k5eAaPmAgFnS	prosít
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
vzal	vzít	k5eAaPmAgMnS	vzít
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Hallvard	Hallvard	k1gInSc1	Hallvard
jí	on	k3xPp3gFnSc7	on
tedy	tedy	k9	tedy
vzal	vzít	k5eAaPmAgInS	vzít
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
člun	člun	k1gInSc4	člun
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
vypluli	vyplout	k5eAaPmAgMnP	vyplout
na	na	k7c4	na
fjord	fjord	k1gInSc4	fjord
<g/>
,	,	kIx,	,
všiml	všimnout	k5eAaPmAgMnS	všimnout
si	se	k3xPyFc3	se
Hallvard	Hallvard	k1gMnSc1	Hallvard
jiné	jiný	k2eAgFnSc2d1	jiná
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rychle	rychle	k6eAd1	rychle
plula	plout	k5eAaImAgFnS	plout
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Zeptal	zeptat	k5eAaPmAgMnS	zeptat
se	se	k3xPyFc4	se
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
zná	znát	k5eAaImIp3nS	znát
ty	ten	k3xDgMnPc4	ten
tři	tři	k4xCgMnPc4	tři
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
připlouvají	připlouvat	k5eAaImIp3nP	připlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
odvětila	odvětit	k5eAaPmAgFnS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
a	a	k8xC	a
že	že	k8xS	že
připlouvají	připlouvat	k5eAaImIp3nP	připlouvat
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gNnSc3	její
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
krádeže	krádež	k1gFnSc2	krádež
věcí	věc	k1gFnPc2	věc
z	z	k7c2	z
domu	dům	k1gInSc2	dům
jejich	jejich	k3xOp3gMnSc2	jejich
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
jí	on	k3xPp3gFnSc3	on
prý	prý	k9	prý
chtějí	chtít	k5eAaImIp3nP	chtít
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
nevinná	vinný	k2eNgFnSc1d1	nevinná
<g/>
.	.	kIx.	.
</s>
<s>
Hallvard	Hallvard	k1gMnSc1	Hallvard
jim	on	k3xPp3gMnPc3	on
tedy	tedy	k9	tedy
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
dívku	dívka	k1gFnSc4	dívka
vydat	vydat	k5eAaPmF	vydat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepřivedou	přivést	k5eNaPmIp3nP	přivést
svědka	svědek	k1gMnSc4	svědek
nebo	nebo	k8xC	nebo
nepředloží	předložit	k5eNaPmIp3nS	předložit
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezbranná	bezbranný	k2eAgFnSc1d1	bezbranná
těhotná	těhotný	k2eAgFnSc1d1	těhotná
dívka	dívka	k1gFnSc1	dívka
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
možnému	možný	k2eAgNnSc3d1	možné
bezpráví	bezpráví	k1gNnSc3	bezpráví
<g/>
.	.	kIx.	.
</s>
<s>
Cizí	cizí	k2eAgMnPc1d1	cizí
muži	muž	k1gMnPc1	muž
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
však	však	k9	však
velmi	velmi	k6eAd1	velmi
rozhněvali	rozhněvat	k5eAaPmAgMnP	rozhněvat
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
svůj	svůj	k3xOyFgInSc4	svůj
šíp	šíp	k1gInSc4	šíp
Hallvardovi	Hallvardův	k2eAgMnPc1d1	Hallvardův
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
pak	pak	k6eAd1	pak
zabili	zabít	k5eAaPmAgMnP	zabít
svými	svůj	k3xOyFgMnPc7	svůj
šípy	šíp	k1gInPc1	šíp
tu	ten	k3xDgFnSc4	ten
dívku	dívka	k1gFnSc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvému	mrtvý	k2eAgMnSc3d1	mrtvý
Hallvardovi	Hallvard	k1gMnSc3	Hallvard
pověsili	pověsit	k5eAaPmAgMnP	pověsit
na	na	k7c4	na
krk	krk	k1gInSc4	krk
mlýnský	mlýnský	k2eAgInSc4d1	mlýnský
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
a	a	k8xC	a
hodili	hodit	k5eAaImAgMnP	hodit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dívku	dívka	k1gFnSc4	dívka
pak	pak	k6eAd1	pak
pohřbili	pohřbít	k5eAaPmAgMnP	pohřbít
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
fjordu	fjord	k1gInSc2	fjord
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
našli	najít	k5eAaPmAgMnP	najít
rybáři	rybář	k1gMnPc1	rybář
z	z	k7c2	z
Lieru	Lier	k1gInSc2	Lier
tělo	tělo	k1gNnSc4	tělo
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
Hallvarda	Hallvard	k1gMnSc2	Hallvard
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyplavalo	vyplavat	k5eAaPmAgNnS	vyplavat
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
i	i	k9	i
s	s	k7c7	s
těžkým	těžký	k2eAgInSc7d1	těžký
mlýnským	mlýnský	k2eAgInSc7d1	mlýnský
kamenem	kámen	k1gInSc7	kámen
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
považovali	považovat	k5eAaImAgMnP	považovat
všichni	všechen	k3xTgMnPc1	všechen
za	za	k7c4	za
boží	boží	k2eAgNnSc4d1	boží
znamení	znamení	k1gNnSc4	znamení
a	a	k8xC	a
Hallvard	Hallvard	k1gMnSc1	Hallvard
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1043	[number]	k4	1043
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
města	město	k1gNnSc2	město
Osla	osel	k1gMnSc2	osel
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Hallvard	Hallvard	k1gMnSc1	Hallvard
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
patronem	patron	k1gMnSc7	patron
Osla	osel	k1gMnSc2	osel
<g/>
,	,	kIx,	,
na	na	k7c4	na
věčnou	věčný	k2eAgFnSc4d1	věčná
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
města	město	k1gNnSc2	město
i	i	k9	i
s	s	k7c7	s
vražednými	vražedný	k2eAgInPc7d1	vražedný
šípy	šíp	k1gInPc7	šíp
<g/>
,	,	kIx,	,
mlýnským	mlýnský	k2eAgNnSc7d1	mlýnské
kolem	kolo	k1gNnSc7	kolo
i	i	k8xC	i
ubohou	ubohý	k2eAgFnSc7d1	ubohá
dívkou	dívka	k1gFnSc7	dívka
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
květen	květen	k1gInSc1	květen
byl	být	k5eAaImAgInS	být
také	také	k9	také
stanoven	stanovit	k5eAaPmNgInS	stanovit
dnem	den	k1gInSc7	den
svatého	svatý	k2eAgMnSc2d1	svatý
Hallvarda	Hallvard	k1gMnSc2	Hallvard
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Norů	Nor	k1gMnPc2	Nor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
těch	ten	k3xDgMnPc2	ten
mladších	mladý	k2eAgMnPc2d2	mladší
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
žertem	žertem	k6eAd1	žertem
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světec	světec	k1gMnSc1	světec
Hallvard	Hallvard	k1gMnSc1	Hallvard
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
CD	CD	kA	CD
coby	coby	k?	coby
symbol	symbol	k1gInSc1	symbol
moderní	moderní	k2eAgFnSc2d1	moderní
současnosti	současnost	k1gFnSc2	současnost
Osla	Oslo	k1gNnSc2	Oslo
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
blízko	blízko	k7c2	blízko
vládní	vládní	k2eAgFnSc2d1	vládní
budovy	budova	k1gFnSc2	budova
bombový	bombový	k2eAgInSc1d1	bombový
útok	útok	k1gInSc1	útok
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
norská	norský	k2eAgFnSc1d1	norská
rezidence	rezidence	k1gFnSc1	rezidence
švédsko-norského	švédskoorský	k2eAgMnSc2d1	švédsko-norský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
rezidencí	rezidence	k1gFnSc7	rezidence
norské	norský	k2eAgFnSc2d1	norská
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
pevnost	pevnost	k1gFnSc1	pevnost
Akershus	Akershus	k1gMnSc1	Akershus
(	(	kIx(	(
<g/>
Akershus	Akershus	k1gMnSc1	Akershus
slott	slott	k1gMnSc1	slott
og	og	k?	og
festning	festning	k1gInSc1	festning
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
třináctého	třináctý	k4xOgNnSc2	třináctý
století	století	k1gNnSc2	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Haakona	Haakon	k1gMnSc2	Haakon
V.	V.	kA	V.
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
pevnosti	pevnost	k1gFnSc6	pevnost
totiž	totiž	k9	totiž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
-	-	kIx~	-
z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
krále	král	k1gMnSc2	král
Haakona	Haakon	k1gMnSc2	Haakon
kostelu	kostel	k1gInSc3	kostel
Mariakirke	Mariakirke	k1gNnPc2	Mariakirke
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
pevnosti	pevnost	k1gFnSc2	pevnost
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
54	[number]	k4	54
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
přístupné	přístupný	k2eAgNnSc1d1	přístupné
pouze	pouze	k6eAd1	pouze
Muzeum	muzeum	k1gNnSc1	muzeum
domácího	domácí	k2eAgInSc2d1	domácí
odboje	odboj	k1gInSc2	odboj
za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
Museum	museum	k1gNnSc1	museum
av	av	k?	av
hjemmefront	hjemmefront	k1gMnSc1	hjemmefront
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norsk	Norsk	k1gInSc1	Norsk
Folkemuseum	Folkemuseum	k1gInSc1	Folkemuseum
-	-	kIx~	-
norské	norský	k2eAgNnSc1d1	norské
muzeum	muzeum	k1gNnSc1	muzeum
kulturní	kulturní	k2eAgFnSc2d1	kulturní
historie	historie	k1gFnSc2	historie
v	v	k7c6	v
Bygdø	Bygdø	k1gFnSc6	Bygdø
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
skanzen	skanzen	k1gInSc4	skanzen
Radnice	radnice	k1gFnSc1	radnice
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
udílí	udílet	k5eAaImIp3nS	udílet
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
dvou	dva	k4xCgInPc2	dva
66	[number]	k4	66
metrů	metr	k1gInPc2	metr
vysokých	vysoký	k2eAgFnPc2d1	vysoká
věží	věž	k1gFnPc2	věž
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
zvonkohra	zvonkohra	k1gFnSc1	zvonkohra
se	s	k7c7	s
49	[number]	k4	49
zvony	zvon	k1gInPc7	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Výzdoba	výzdoba	k1gFnSc1	výzdoba
budovy	budova	k1gFnSc2	budova
trvala	trvat	k5eAaImAgFnS	trvat
19	[number]	k4	19
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
přerušila	přerušit	k5eAaPmAgFnS	přerušit
německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
Norska	Norsko	k1gNnSc2	Norsko
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Skokanský	skokanský	k2eAgInSc1d1	skokanský
můstek	můstek	k1gInSc1	můstek
Holmenkollen	Holmenkollen	k1gInSc1	Holmenkollen
-	-	kIx~	-
dějiště	dějiště	k1gNnSc1	dějiště
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lyžařské	lyžařský	k2eAgNnSc4d1	lyžařské
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
můstek	můstek	k1gInSc1	můstek
nový	nový	k2eAgInSc1d1	nový
a	a	k8xC	a
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Kon-Tiki	Kon-Tik	k1gFnSc2	Kon-Tik
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Bygdø	Bygdø	k1gFnSc2	Bygdø
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
původní	původní	k2eAgInSc1d1	původní
balzový	balzový	k2eAgInSc1d1	balzový
vor	vor	k1gInSc1	vor
Kon-Tiki	Kon-Tik	k1gFnSc2	Kon-Tik
norského	norský	k2eAgMnSc2d1	norský
cestovatele	cestovatel	k1gMnSc2	cestovatel
Thora	Thor	k1gMnSc2	Thor
Heyerdahla	Heyerdahla	k1gMnSc2	Heyerdahla
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
přeplul	přeplout	k5eAaPmAgInS	přeplout
přes	přes	k7c4	přes
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
z	z	k7c2	z
Peru	Peru	k1gNnSc2	Peru
do	do	k7c2	do
Polynésie	Polynésie	k1gFnSc2	Polynésie
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
exponátem	exponát	k1gInSc7	exponát
je	být	k5eAaImIp3nS	být
vor	vor	k1gInSc1	vor
Ra	ra	k0	ra
II	II	kA	II
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
palubě	paluba	k1gFnSc6	paluba
Heyerdahl	Heyerdahl	k1gMnSc1	Heyerdahl
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
staří	starý	k2eAgMnPc1d1	starý
egyptští	egyptský	k2eAgMnPc1d1	egyptský
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
doplout	doplout	k5eAaPmF	doplout
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
plavidlech	plavidlo	k1gNnPc6	plavidlo
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
do	do	k7c2	do
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Munchovo	Munchův	k2eAgNnSc1d1	Munchovo
muzeum	muzeum	k1gNnSc1	muzeum
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Tø	Tø	k1gFnSc2	Tø
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
soubor	soubor	k1gInSc4	soubor
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
děl	dělo	k1gNnPc2	dělo
malíře	malíř	k1gMnSc2	malíř
Edvarda	Edvard	k1gMnSc2	Edvard
Muncha	Munch	k1gMnSc2	Munch
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umělec	umělec	k1gMnSc1	umělec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
odkázal	odkázat	k5eAaPmAgInS	odkázat
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgMnSc2	stý
výročí	výročí	k1gNnSc3	výročí
autorova	autorův	k2eAgNnSc2d1	autorovo
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
o	o	k7c6	o
přestěhování	přestěhování	k1gNnSc6	přestěhování
celé	celý	k2eAgFnSc2d1	celá
expozice	expozice	k1gFnSc2	expozice
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
opery	opera	k1gFnSc2	opera
u	u	k7c2	u
zálivu	záliv	k1gInSc2	záliv
Bjø	Bjø	k1gFnSc2	Bjø
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Osla	Oslo	k1gNnSc2	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Budovu	budova	k1gFnSc4	budova
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
španělská	španělský	k2eAgFnSc1d1	španělská
architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
Herreros	Herrerosa	k1gFnPc2	Herrerosa
Arquitectos	Arquitectosa	k1gFnPc2	Arquitectosa
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Akerselva	Akerselvo	k1gNnSc2	Akerselvo
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
řek	řeka	k1gFnPc2	řeka
z	z	k7c2	z
norské	norský	k2eAgFnSc2d1	norská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
měla	mít	k5eAaImAgFnS	mít
jméno	jméno	k1gNnSc4	jméno
Frysja	Frysj	k1gInSc2	Frysj
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
studená	studený	k2eAgFnSc1d1	studená
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
pouhých	pouhý	k2eAgInPc2d1	pouhý
8,2	[number]	k4	8,2
km	km	kA	km
(	(	kIx(	(
<g/>
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
jezera	jezero	k1gNnSc2	jezero
Maridalsvannet	Maridalsvannet	k1gInSc1	Maridalsvannet
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
výškový	výškový	k2eAgInSc4d1	výškový
spád	spád	k1gInSc4	spád
149	[number]	k4	149
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Den	den	k1gInSc1	den
Norske	Norske	k1gInSc1	Norske
Opera	opera	k1gFnSc1	opera
&	&	k?	&
Ballet	Ballet	k1gInSc1	Ballet
<g/>
)	)	kIx)	)
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
dominantou	dominanta	k1gFnSc7	dominanta
panoramatu	panorama	k1gNnSc2	panorama
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Oslofjordu	Oslofjord	k1gInSc2	Oslofjord
<g/>
.	.	kIx.	.
</s>
<s>
Výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
budovy	budova	k1gFnSc2	budova
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
domácí	domácí	k2eAgFnSc1d1	domácí
společnost	společnost	k1gFnSc1	společnost
Snø	Snø	k1gFnSc2	Snø
AS	as	k9	as
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otevření	otevření	k1gNnSc6	otevření
získala	získat	k5eAaPmAgFnS	získat
stavba	stavba	k1gFnSc1	stavba
řadu	řad	k1gInSc2	řad
ocenění	ocenění	k1gNnSc2	ocenění
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Stortinget	stortinget	k1gInSc1	stortinget
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
norského	norský	k2eAgInSc2d1	norský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
navržena	navržen	k2eAgFnSc1d1	navržena
architektem	architekt	k1gMnSc7	architekt
Emilem	Emil	k1gMnSc7	Emil
Victorem	Victor	k1gMnSc7	Victor
Langletem	Langle	k1gNnSc7	Langle
a	a	k8xC	a
otevřena	otevřen	k2eAgFnSc1d1	otevřena
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Frogner	Frogner	k1gInSc1	Frogner
park	park	k1gInSc1	park
a	a	k8xC	a
Vigelandův	Vigelandův	k2eAgInSc1d1	Vigelandův
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
Vigelandsanlegget	Vigelandsanlegget	k1gInSc1	Vigelandsanlegget
<g/>
)	)	kIx)	)
-	-	kIx~	-
park	park	k1gInSc1	park
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
32	[number]	k4	32
hektarů	hektar	k1gInPc2	hektar
s	s	k7c7	s
214	[number]	k4	214
originálními	originální	k2eAgFnPc7d1	originální
skulpturami	skulptura	k1gFnPc7	skulptura
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
sochaře	sochař	k1gMnSc2	sochař
Gustava	Gustav	k1gMnSc2	Gustav
Vigelanda	Vigelanda	k1gFnSc1	Vigelanda
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
jimi	on	k3xPp3gMnPc7	on
osazován	osazován	k2eAgInSc4d1	osazován
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
světově	světově	k6eAd1	světově
ojedinělé	ojedinělý	k2eAgNnSc1d1	ojedinělé
dílo	dílo	k1gNnSc1	dílo
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
vztyčením	vztyčení	k1gNnSc7	vztyčení
centrálního	centrální	k2eAgInSc2d1	centrální
žulového	žulový	k2eAgInSc2d1	žulový
monolitu	monolit	k1gInSc2	monolit
vysokého	vysoký	k2eAgInSc2d1	vysoký
17	[number]	k4	17
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
se	s	k7c7	s
121	[number]	k4	121
lidskými	lidský	k2eAgFnPc7d1	lidská
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
vikinských	vikinský	k2eAgFnPc2d1	vikinská
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
Vikingskipshuset	Vikingskipshuset	k1gInSc1	Vikingskipshuset
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
Fram	Frama	k1gFnPc2	Frama
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vystavena	vystaven	k2eAgFnSc1d1	vystavena
celá	celý	k2eAgFnSc1d1	celá
loď	loď	k1gFnSc1	loď
Fram	Frama	k1gFnPc2	Frama
(	(	kIx(	(
<g/>
Vpřed	vpřed	k6eAd1	vpřed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yRgFnSc7	který
podnikl	podniknout	k5eAaPmAgMnS	podniknout
polární	polární	k2eAgMnSc1d1	polární
badatel	badatel	k1gMnSc1	badatel
Roald	Roalda	k1gFnPc2	Roalda
Amundsen	Amundsen	k1gInSc4	Amundsen
plavbu	plavba	k1gFnSc4	plavba
do	do	k7c2	do
Antarktidy	Antarktida	k1gFnSc2	Antarktida
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
výpravě	výprava	k1gFnSc6	výprava
stanul	stanout	k5eAaPmAgInS	stanout
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1911	[number]	k4	1911
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
zemském	zemský	k2eAgNnSc6d1	zemské
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Tryvannstå	Tryvannstå	k?	Tryvannstå
-	-	kIx~	-
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
přilehlým	přilehlý	k2eAgInSc7d1	přilehlý
lyžařským	lyžařský	k2eAgInSc7d1	lyžařský
areálem	areál	k1gInSc7	areál
Muzeum	muzeum	k1gNnSc1	muzeum
holocaustu	holocaust	k1gInSc2	holocaust
na	na	k7c6	na
Bygdø	Bygdø	k1gFnSc6	Bygdø
Dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Oslu	osel	k1gMnSc6	osel
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
šestilinkové	šestilinkový	k2eAgNnSc1d1	šestilinkový
metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
Oslo	Oslo	k1gNnSc1	Oslo
T-bane	Tan	k1gMnSc5	T-ban
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
(	(	kIx(	(
<g/>
jízdenka	jízdenka	k1gFnSc1	jízdenka
na	na	k7c6	na
MHD	MHD	kA	MHD
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
první	první	k4xOgFnSc6	první
zóně	zóna	k1gFnSc6	zóna
32	[number]	k4	32
NOK	NOK	kA	NOK
<g/>
)	)	kIx)	)
a	a	k8xC	a
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Oslo	Oslo	k1gNnSc2	Oslo
Gardemoen	Gardemona	k1gFnPc2	Gardemona
(	(	kIx(	(
<g/>
OSL	OSL	kA	OSL
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
tuto	tento	k3xDgFnSc4	tento
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
ceny	cena	k1gFnPc1	cena
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
vlastním	vlastní	k2eAgInSc7d1	vlastní
automobilem	automobil	k1gInSc7	automobil
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
E6	E6	k1gFnPc2	E6
směr	směr	k1gInSc1	směr
Trondheim	Trondheim	k1gMnSc1	Trondheim
regionálním	regionální	k2eAgInSc7d1	regionální
vlakem	vlak	k1gInSc7	vlak
-	-	kIx~	-
90	[number]	k4	90
NOK	NOK	kA	NOK
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
autobusem	autobus	k1gInSc7	autobus
-	-	kIx~	-
cca	cca	kA	cca
180	[number]	k4	180
NOK	NOK	kA	NOK
rychlovlakem	rychlovlak	k1gInSc7	rychlovlak
-	-	kIx~	-
200	[number]	k4	200
NOK	NOK	kA	NOK
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
180	[number]	k4	180
NOK	NOK	kA	NOK
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
-	-	kIx~	-
cesta	cesta	k1gFnSc1	cesta
trvá	trvat	k5eAaImIp3nS	trvat
cca	cca	kA	cca
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
taxíkem	taxík	k1gInSc7	taxík
-	-	kIx~	-
cca	cca	kA	cca
800	[number]	k4	800
-	-	kIx~	-
1	[number]	k4	1
300	[number]	k4	300
NOK	NOK	kA	NOK
Letecké	letecký	k2eAgFnSc2d1	letecká
linky	linka	k1gFnSc2	linka
z	z	k7c2	z
destinací	destinace	k1gFnPc2	destinace
uvnitř	uvnitř	k7c2	uvnitř
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
využívají	využívat	k5eAaImIp3nP	využívat
nově	nově	k6eAd1	nově
i	i	k9	i
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
menší	malý	k2eAgFnPc4d2	menší
"	"	kIx"	"
<g/>
záložní	záložní	k2eAgNnSc4d1	záložní
<g/>
"	"	kIx"	"
letiště	letiště	k1gNnSc4	letiště
Osla	Oslo	k1gNnSc2	Oslo
-	-	kIx~	-
Rygge	Rygge	k1gInSc1	Rygge
a	a	k8xC	a
Torp	Torp	k1gInSc1	Torp
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
vzdálenější	vzdálený	k2eAgInPc4d2	vzdálenější
než	než	k8xS	než
letiště	letiště	k1gNnSc4	letiště
Gardermoen	Gardermona	k1gFnPc2	Gardermona
(	(	kIx(	(
<g/>
80	[number]	k4	80
a	a	k8xC	a
100	[number]	k4	100
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
domácí	domácí	k2eAgFnSc1d1	domácí
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
Norwegian	Norwegiana	k1gFnPc2	Norwegiana
nabízí	nabízet	k5eAaImIp3nS	nabízet
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
letů	let	k1gInPc2	let
právě	právě	k9	právě
sem	sem	k6eAd1	sem
lepší	dobrý	k2eAgFnPc4d2	lepší
ceny	cena	k1gFnPc4	cena
-	-	kIx~	-
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
například	například	k6eAd1	například
již	již	k6eAd1	již
od	od	k7c2	od
190	[number]	k4	190
NOK	NOK	kA	NOK
pro	pro	k7c4	pro
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
linkové	linkový	k2eAgInPc4d1	linkový
lety	let	k1gInPc4	let
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
(	(	kIx(	(
<g/>
úterý	úterý	k1gNnSc4	úterý
<g/>
,	,	kIx,	,
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
dopravy	doprava	k1gFnSc2	doprava
do	do	k7c2	do
<g/>
/	/	kIx~	/
<g/>
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
expresními	expresní	k2eAgInPc7d1	expresní
autobusy	autobus	k1gInPc7	autobus
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
na	na	k7c4	na
Gardermoen	Gardermoen	k1gInSc4	Gardermoen
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgNnSc1d1	jednotlivé
jízdné	jízdné	k1gNnSc1	jízdné
v	v	k7c6	v
prostředcích	prostředek	k1gInPc6	prostředek
MHD	MHD	kA	MHD
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
již	již	k9	již
50	[number]	k4	50
NOK	NOK	kA	NOK
(	(	kIx(	(
<g/>
dospělí	dospělý	k2eAgMnPc1d1	dospělý
<g/>
)	)	kIx)	)
a	a	k8xC	a
25	[number]	k4	25
NOK	NOK	kA	NOK
(	(	kIx(	(
<g/>
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
senioři	senior	k1gMnPc1	senior
nad	nad	k7c4	nad
67	[number]	k4	67
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Oslo	Oslo	k1gNnSc1	Oslo
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
Stor	Stor	k1gInSc1	Stor
Oslo	Oslo	k1gNnSc1	Oslo
-	-	kIx~	-
Velké	velký	k2eAgNnSc1d1	velké
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
613	[number]	k4	613
282	[number]	k4	282
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Statistisk	Statistisko	k1gNnPc2	Statistisko
Sentralbyrå	Sentralbyrå	k1gMnSc2	Sentralbyrå
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
přistěhovalecký	přistěhovalecký	k2eAgInSc1d1	přistěhovalecký
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
181	[number]	k4	181
343	[number]	k4	343
(	(	kIx(	(
<g/>
29,6	[number]	k4	29,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
přistěhovalecká	přistěhovalecký	k2eAgFnSc1d1	přistěhovalecká
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
127	[number]	k4	127
590	[number]	k4	590
-	-	kIx~	-
70,4	[number]	k4	70,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
53	[number]	k4	53
753	[number]	k4	753
obyvatel	obyvatel	k1gMnPc2	obyvatel
Osla	Oslo	k1gNnSc2	Oslo
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
42	[number]	k4	42
262	[number]	k4	262
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
narozené	narozený	k2eAgFnPc4d1	narozená
již	již	k6eAd1	již
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
městských	městský	k2eAgFnPc6d1	městská
částech	část	k1gFnPc6	část
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
počet	počet	k1gInSc1	počet
zde	zde	k6eAd1	zde
usídlených	usídlený	k2eAgMnPc2d1	usídlený
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
(	(	kIx(	(
<g/>
Nordstrand	Nordstrand	k1gInSc1	Nordstrand
49,2	[number]	k4	49,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
Stovner	Stovner	k1gInSc1	Stovner
48,1	[number]	k4	48,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
Alna	Alna	k1gFnSc1	Alna
47,3	[number]	k4	47,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
Grorud	Grorud	k1gInSc1	Grorud
43,6	[number]	k4	43,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnějšími	početní	k2eAgFnPc7d3	nejpočetnější
přistěhovaleckými	přistěhovalecký	k2eAgFnPc7d1	přistěhovalecká
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
Pákistánci	Pákistánec	k1gMnPc1	Pákistánec
(	(	kIx(	(
<g/>
32	[number]	k4	32
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Somálci	Somálec	k1gMnPc1	Somálec
(	(	kIx(	(
<g/>
13	[number]	k4	13
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédové	Švéd	k1gMnPc1	Švéd
(	(	kIx(	(
<g/>
15	[number]	k4	15
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
(	(	kIx(	(
<g/>
14	[number]	k4	14
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Tamilové	Tamil	k1gMnPc1	Tamil
ze	z	k7c2	z
Srí	Srí	k1gMnPc1	Srí
Lanky	lanko	k1gNnPc7	lanko
(	(	kIx(	(
<g/>
8	[number]	k4	8
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iráčané	Iráčan	k1gMnPc1	Iráčan
(	(	kIx(	(
<g/>
7	[number]	k4	7
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turci	Turek	k1gMnPc1	Turek
(	(	kIx(	(
<g/>
6	[number]	k4	6
300	[number]	k4	300
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maročané	Maročan	k1gMnPc1	Maročan
(	(	kIx(	(
<g/>
6	[number]	k4	6
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
(	(	kIx(	(
<g/>
5	[number]	k4	5
700	[number]	k4	700
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Íránci	Íránec	k1gMnPc1	Íránec
(	(	kIx(	(
<g/>
5	[number]	k4	5
300	[number]	k4	300
<g/>
)	)	kIx)	)
a	a	k8xC	a
Filipínci	Filipínec	k1gMnSc3	Filipínec
(	(	kIx(	(
<g/>
4	[number]	k4	4
300	[number]	k4	300
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
početná	početný	k2eAgFnSc1d1	početná
muslimská	muslimský	k2eAgFnSc1d1	muslimská
komunita	komunita	k1gFnSc1	komunita
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
48	[number]	k4	48
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
především	především	k9	především
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Grø	Grø	k1gFnSc2	Grø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rovněž	rovněž	k9	rovněž
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
12	[number]	k4	12
městských	městský	k2eAgFnPc2d1	městská
mešit	mešita	k1gFnPc2	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Buddhistů	buddhista	k1gMnPc2	buddhista
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
okolo	okolo	k7c2	okolo
9	[number]	k4	9
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Osla	Oslo	k1gNnSc2	Oslo
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
evangelicko-luteránské	evangelickouteránský	k2eAgFnSc3d1	evangelicko-luteránská
státní	státní	k2eAgFnSc3d1	státní
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
4,5	[number]	k4	4,5
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
římskokatolické	římskokatolický	k2eAgInPc1d1	římskokatolický
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
probíhají	probíhat	k5eAaImIp3nP	probíhat
mše	mše	k1gFnPc1	mše
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
<g/>
,	,	kIx,	,
vietnamštině	vietnamština	k1gFnSc6	vietnamština
a	a	k8xC	a
tamilštině	tamilština	k1gFnSc6	tamilština
<g/>
.	.	kIx.	.
</s>
<s>
Kjetil	Kjetit	k5eAaImAgMnS	Kjetit
André	André	k1gMnSc1	André
Aamodt	Aamodt	k1gMnSc1	Aamodt
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžař	lyžař	k1gMnSc1	lyžař
Vilhelm	Vilhelm	k1gMnSc1	Vilhelm
Bjerknes	Bjerknes	k1gMnSc1	Bjerknes
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
meteorolog	meteorolog	k1gMnSc1	meteorolog
Espen	Espen	k1gInSc4	Espen
Bredesen	Bredesen	k2eAgInSc4d1	Bredesen
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Ragnar	Ragnar	k1gMnSc1	Ragnar
Frisch	Frisch	k1gMnSc1	Frisch
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Odd	odd	kA	odd
Hassel	Hassel	k1gMnSc1	Hassel
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgMnSc1d1	fyzikální
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Sonja	Sonj	k2eAgFnSc1d1	Sonja
Henie	Henie	k1gFnSc1	Henie
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
Lars	Larsa	k1gFnPc2	Larsa
Onsager	Onsagra	k1gFnPc2	Onsagra
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Steve	Steve	k1gMnSc1	Steve
Sem-Sandberg	Sem-Sandberg	k1gMnSc1	Sem-Sandberg
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jo	jo	k9	jo
Nesbø	Nesbø	k1gMnSc1	Nesbø
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
hudeník	hudeník	k1gInSc4	hudeník
Oslo	Oslo	k1gNnSc1	Oslo
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
městy	město	k1gNnPc7	město
a	a	k8xC	a
regiony	region	k1gInPc7	region
<g/>
:	:	kIx,	:
Göteborg	Göteborg	k1gInSc1	Göteborg
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
Oslo	Oslo	k1gNnSc6	Oslo
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
posílá	posílat	k5eAaImIp3nS	posílat
vánoční	vánoční	k2eAgInPc4d1	vánoční
stromy	strom	k1gInPc4	strom
do	do	k7c2	do
měst	město	k1gNnPc2	město
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
<g/>
,	,	kIx,	,
Glasgow	Glasgow	k1gNnSc1	Glasgow
a	a	k8xC	a
Belfast	Belfast	k1gInSc1	Belfast
<g/>
.	.	kIx.	.
</s>
