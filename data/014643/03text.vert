<s>
Gradačac	Gradačac	k6eAd1
</s>
<s>
Gradačac	Gradačac	k6eAd1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
44	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
164	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Gradačac	Gradačac	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
218	#num#	k4
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.gradacac.ba	www.gradacac.ba	k1gFnSc1
PSČ	PSČ	kA
</s>
<s>
76250	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gradačac	Gradačac	k6eAd1
(	(	kIx(
<g/>
srbskou	srbský	k2eAgFnSc7d1
cyrlicí	cyrlice	k1gFnSc7
Г	Г	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
Federaci	federace	k1gFnSc6
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
kantonu	kanton	k1gInSc6
Tuzla	Tuzlo	k1gNnSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Gradašnica	Gradašnic	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
župě	župa	k1gFnSc6
Gradačac	Gradačac	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1302	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
město	město	k1gNnSc1
samotné	samotný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
připomínáno	připomínán	k2eAgNnSc1d1
(	(	kIx(
<g/>
pod	pod	k7c7
starým	starý	k2eAgInSc7d1
názvem	název	k1gInSc7
Gračac	Gračac	k1gInSc1
<g/>
)	)	kIx)
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1465	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1512	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
zmiňováno	zmiňován	k2eAgNnSc1d1
až	až	k9
z	z	k7c2
tureckých	turecký	k2eAgInPc2d1
daňových	daňový	k2eAgInPc2d1
zápisů	zápis	k1gInPc2
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1533	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1765	#num#	k4
a	a	k8xC
1821	#num#	k4
Osmané	Osman	k1gMnPc1
vybudovali	vybudovat	k5eAaPmAgMnP
18	#num#	k4
m	m	kA
vysoké	vysoký	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
s	s	k7c7
22	#num#	k4
m	m	kA
vysokou	vysoký	k2eAgFnSc7d1
strážní	strážní	k2eAgFnSc7d1
věží	věž	k1gFnSc7
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
místě	místo	k1gNnSc6
starých	stará	k1gFnPc2
<g/>
,	,	kIx,
římských	římský	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
-	-	kIx~
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
totiž	totiž	k9
důležitým	důležitý	k2eAgInSc7d1
pohraničním	pohraniční	k2eAgInSc7d1
bodem	bod	k1gInSc7
Turků	Turek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
provázející	provázející	k2eAgInSc1d1
rozpad	rozpad	k1gInSc1
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1992	#num#	k4
-	-	kIx~
1995	#num#	k4
bylo	být	k5eAaImAgNnS
několikrát	několikrát	k6eAd1
bombardováno	bombardován	k2eAgNnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
sloužilo	sloužit	k5eAaImAgNnS
Srbům	Srb	k1gMnPc3
a	a	k8xC
mělo	mít	k5eAaImAgNnS
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
ohromný	ohromný	k2eAgInSc1d1
strategický	strategický	k2eAgInSc1d1
význam	význam	k1gInSc1
<g/>
;	;	kIx,
Daytonskou	daytonský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
však	však	k9
bylo	být	k5eAaImAgNnS
stanoveno	stanovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
připadne	připadnout	k5eAaPmIp3nS
Chorvatům	Chorvat	k1gMnPc3
a	a	k8xC
Muslimům	muslim	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Etnické	etnický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Následující	následující	k2eAgNnSc1d1
etnické	etnický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
60,2	60,2	k4
%	%	kIx~
Bosňáci	Bosňáci	k?
</s>
<s>
19,8	19,8	k4
%	%	kIx~
Srbové	Srb	k1gMnPc1
</s>
<s>
15,1	15,1	k4
%	%	kIx~
Chorvati	Chorvat	k1gMnPc1
</s>
<s>
4,9	4,9	k4
%	%	kIx~
ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mešita	mešita	k1gFnSc1
Husejnija	Husejnij	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gradačac	Gradačac	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnSc2
a	a	k8xC
města	město	k1gNnSc2
Tuzlanského	Tuzlanský	k2eAgInSc2d1
kantonu	kanton	k1gInSc2
</s>
<s>
Banovići	Banovići	k6eAd1
•	•	k?
Čelić	Čelić	k1gMnSc1
•	•	k?
Doboj	Doboj	k1gInSc1
Východ	východ	k1gInSc1
•	•	k?
Gračanica	Gračanic	k1gInSc2
•	•	k?
Gradačac	Gradačac	k1gInSc1
•	•	k?
Kalesija	Kalesij	k1gInSc2
•	•	k?
Kladanj	Kladanj	k1gMnSc1
•	•	k?
Lukavac	Lukavac	k1gInSc1
•	•	k?
Sapna	Sapn	k1gInSc2
•	•	k?
Srebrenik	Srebrenik	k1gMnSc1
•	•	k?
Teočak	Teočak	k1gMnSc1
•	•	k?
Tuzla	Tuzla	k1gMnSc1
•	•	k?
Živinice	Živinice	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
334560	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83058921	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
136078397	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83058921	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
