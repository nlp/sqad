<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1765	#num#	k4
a	a	k8xC
1821	#num#	k4
Osmané	Osman	k1gMnPc1
vybudovali	vybudovat	k5eAaPmAgMnP
18	#num#	k4
m	m	kA
vysoké	vysoký	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
s	s	k7c7
22	#num#	k4
m	m	kA
vysokou	vysoký	k2eAgFnSc7d1
strážní	strážní	k2eAgFnSc7d1
věží	věž	k1gFnSc7
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
místě	místo	k1gNnSc6
starých	stará	k1gFnPc2
<g/>
,	,	kIx,
římských	římský	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
-	-	kIx~
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
totiž	totiž	k9
důležitým	důležitý	k2eAgInSc7d1
pohraničním	pohraniční	k2eAgInSc7d1
bodem	bod	k1gInSc7
Turků	Turek	k1gMnPc2
<g/>
.	.	kIx.
</s>