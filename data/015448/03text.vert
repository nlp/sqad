<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
G-8	G-8	k1gFnSc1
</s>
<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
G-8	G-8	k1gMnSc5
D.	D.	kA
Košic	Košice	k1gInPc2
u	u	k7c2
letounu	letoun	k1gInSc2
Gribovskij	Gribovskij	k1gMnSc1
G-8	G-8	k1gMnSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Určení	určení	k1gNnSc1
</s>
<s>
sportovní	sportovní	k2eAgNnSc1d1
a	a	k8xC
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Moskevská	moskevský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
Osoaviachim	Osoaviachim	k1gMnSc1
<g/>
)	)	kIx)
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Konstantinovič	Konstantinovič	k1gMnSc1
Gribovskij	Gribovskij	k1gMnSc1
První	první	k4xOgFnSc4
let	léto	k1gNnPc2
</s>
<s>
listopad	listopad	k1gInSc4
1931	#num#	k4
Uživatel	uživatel	k1gMnSc1
</s>
<s>
SSSR	SSSR	kA
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
1	#num#	k4
Vyvinuto	vyvinout	k5eAaPmNgNnS
z	z	k7c2
typu	typ	k1gInSc2
</s>
<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
G-5	G-5	k1gFnSc2
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
G-10	G-10	k1gMnSc5
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gribovskij	Gribovskij	k1gInSc1
G-8	G-8	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
Г	Г	k?
Г	Г	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
jednomístné	jednomístný	k2eAgNnSc4d1
sportovní	sportovní	k2eAgNnSc4d1
a	a	k8xC
cvičné	cvičný	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
navržené	navržený	k2eAgInPc4d1
a	a	k8xC
postavené	postavený	k2eAgInPc4d1
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
začátkem	začátkem	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Letoun	letoun	k1gInSc1
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
přeškolování	přeškolování	k1gNnSc4
pilotů	pilot	k1gInPc2
na	na	k7c4
vojenské	vojenský	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
byl	být	k5eAaImAgMnS
V.	V.	kA
K.	K.	kA
Gribovskij	Gribovskij	k1gMnSc1
<g/>
,	,	kIx,
úspěšný	úspěšný	k2eAgMnSc1d1
konstruktér	konstruktér	k1gMnSc1
kluzáků	kluzák	k1gInPc2
<g/>
,	,	kIx,
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
funkce	funkce	k1gFnSc2
ředitele	ředitel	k1gMnSc2
Moskevské	moskevský	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
vlastnil	vlastnit	k5eAaImAgInS
Osoaviachim	Osoaviachim	k1gInSc1
(	(	kIx(
<g/>
О	О	k?
<g/>
,	,	kIx,
později	pozdě	k6eAd2
DOSAAF	DOSAAF	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
Frunzeho	Frunze	k1gMnSc4
ústředním	ústřední	k2eAgNnSc7d1
letišti	letiště	k1gNnSc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
její	její	k3xOp3gFnSc1
existence	existence	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
významně	významně	k6eAd1
"	"	kIx"
<g/>
narušována	narušován	k2eAgFnSc1d1
<g/>
"	"	kIx"
sousedstvím	sousedství	k1gNnSc7
dalších	další	k2eAgInPc2d1
„	„	k?
<g/>
důležitějších	důležitý	k2eAgInPc2d2
<g/>
“	“	k?
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
přijatý	přijatý	k2eAgInSc1d1
náčelník	náčelník	k1gInSc1
školy	škola	k1gFnSc2
proto	proto	k8xC
začal	začít	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
hledáním	hledání	k1gNnSc7
nového	nový	k2eAgNnSc2d1
umístění	umístění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
nedaleko	nedaleko	k7c2
obce	obec	k1gFnSc2
Tušino	Tušino	k1gNnSc1
(	(	kIx(
<g/>
Т	Т	k?
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
součást	součást	k1gFnSc1
Moskvy	Moskva	k1gFnSc2
<g/>
)	)	kIx)
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
vhodné	vhodný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
vzniklo	vzniknout	k5eAaPmAgNnS
letiště	letiště	k1gNnSc1
Moskva-Tušino	Moskva-Tušin	k2eAgNnSc1d1
a	a	k8xC
byl	být	k5eAaImAgInS
zde	zde	k6eAd1
založen	založit	k5eAaPmNgInS
ústřední	ústřední	k2eAgInSc1d1
aeroklub	aeroklub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
centrem	centrum	k1gNnSc7
sportovního	sportovní	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
SSSR	SSSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
navrhl	navrhnout	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
letoun	letoun	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
dílnách	dílna	k1gFnPc6
Moskevské	moskevský	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Osoavichim	Osoavichima	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Lehký	lehký	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
G-8	G-8	k1gFnSc2
absolvoval	absolvovat	k5eAaPmAgInS
první	první	k4xOgInSc4
let	let	k1gInSc4
s	s	k7c7
motorem	motor	k1gInSc7
Walter	Walter	k1gMnSc1
NZ-60	NZ-60	k1gMnSc1
v	v	k7c6
listopadu	listopad	k1gInSc6
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letounu	letoun	k1gInSc2
G-8	G-8	k1gMnSc1
vybaveném	vybavený	k2eAgInSc6d1
motorem	motor	k1gInSc7
Waller	Waller	k1gInSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
44	#num#	k4
kW	kW	kA
<g/>
/	/	kIx~
<g/>
60	#num#	k4
byla	být	k5eAaImAgFnS
naměřena	naměřen	k2eAgFnSc1d1
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
160	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
V	v	k7c6
srpnu	srpen	k1gInSc6
1932	#num#	k4
letadlo	letadlo	k1gNnSc1
prošlo	projít	k5eAaPmAgNnS
celým	celý	k2eAgInSc7d1
letovým	letový	k2eAgInSc7d1
zkušebním	zkušební	k2eAgInSc7d1
cyklem	cyklus	k1gInSc7
s	s	k7c7
pozitivním	pozitivní	k2eAgNnSc7d1
hodnocením	hodnocení	k1gNnSc7
stability	stabilita	k1gFnSc2
a	a	k8xC
ovladatelnosti	ovladatelnost	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
jej	on	k3xPp3gInSc2
otestovat	otestovat	k5eAaPmF
na	na	k7c6
dlouhém	dlouhý	k2eAgInSc6d1
letu	let	k1gInSc6
naplánovaném	naplánovaný	k2eAgInSc6d1
na	na	k7c6
září	září	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Pilot	pilot	k1gMnSc1
D.	D.	kA
Košic	Košice	k1gInPc2
uskuteční	uskutečnit	k5eAaPmIp3nS
v	v	k7c6
tomto	tento	k3xDgNnSc6
letadle	letadlo	k1gNnSc6
propagandistický	propagandistický	k2eAgInSc4d1
let	let	k1gInSc4
dlouhý	dlouhý	k2eAgInSc4d1
5	#num#	k4
200	#num#	k4
kilometrů	kilometr	k1gInPc2
za	za	k7c4
deset	deset	k4xCc4
dní	den	k1gInPc2
podél	podél	k7c2
celého	celý	k2eAgInSc2d1
SSSR	SSSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
letoun	letoun	k1gInSc1
osazen	osadit	k5eAaPmNgInS
sedmiválcovým	sedmiválcový	k2eAgInSc7d1
motorem	motor	k1gInSc7
Walter	Walter	k1gMnSc1
NZ-	NZ-	k1gMnSc1
<g/>
85	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
byl	být	k5eAaImAgInS
důsledným	důsledný	k2eAgMnSc7d1
zastáncem	zastánce	k1gMnSc7
dolnoplošníků	dolnoplošník	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
přece	přece	k9
tuto	tento	k3xDgFnSc4
linii	linie	k1gFnSc4
přerušil	přerušit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
u	u	k7c2
následujícího	následující	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Gribovskij	Gribovskij	k1gMnSc1
G-10	G-10	k1gMnSc1
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
uvedl	uvést	k5eAaPmAgMnS
s	s	k7c7
lehkým	lehký	k2eAgInSc7d1
vyztuženým	vyztužený	k2eAgInSc7d1
hornoplošníkem	hornoplošník	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
NZ-60	NZ-60	k1gMnSc1
</s>
<s>
Popis	popis	k1gInSc1
letounu	letoun	k1gInSc2
</s>
<s>
G-8	G-8	k4
byla	být	k5eAaImAgFnS
Gribovského	Gribovského	k2eAgFnSc1d1
již	již	k6eAd1
třetí	třetí	k4xOgFnSc1
konstrukce	konstrukce	k1gFnSc1
po	po	k7c6
strojích	stroj	k1gInPc6
G-4	G-4	k1gMnSc1
a	a	k8xC
G-5	G-5	k1gMnSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
-	-	kIx~
jako	jako	k9
předchozí	předchozí	k2eAgInPc4d1
letouny	letoun	k1gInPc4
-	-	kIx~
lehký	lehký	k2eAgInSc1d1
jednomístný	jednomístný	k2eAgInSc1d1
<g/>
,	,	kIx,
jednomotorový	jednomotorový	k2eAgInSc1d1
dolnoplošník	dolnoplošník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
konstrukce	konstrukce	k1gFnSc2
se	se	k3xPyFc4
Gribovskij	Gribovskij	k1gMnSc1
držel	držet	k5eAaImAgMnS
i	i	k8xC
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyvinul	vyvinout	k5eAaPmAgMnS
dolnoplošníky	dolnoplošník	k1gInPc4
<g/>
:	:	kIx,
dvoumístné	dvoumístný	k2eAgFnPc4d1
<g/>
,	,	kIx,
turistické	turistický	k2eAgFnPc4d1
G-15	G-15	k1gFnPc4
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
G-20	G-20	k1gMnSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letoun	letoun	k1gInSc1
G-8	G-8	k1gMnPc2
byl	být	k5eAaImAgInS
jednoduchý	jednoduchý	k2eAgInSc1d1
samonosný	samonosný	k2eAgInSc1d1
jednoplošník	jednoplošník	k1gInSc1
převážně	převážně	k6eAd1
dřevěné	dřevěný	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křídlo	křídlo	k1gNnSc1
<g/>
,	,	kIx,
zapuštěné	zapuštěný	k2eAgNnSc1d1
do	do	k7c2
spodní	spodní	k2eAgFnSc2d1
části	část	k1gFnSc2
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
dvounosníkové	dvounosníkový	k2eAgNnSc1d1
<g/>
,	,	kIx,
náběžná	náběžný	k2eAgFnSc1d1
hrana	hrana	k1gFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
překližky	překližka	k1gFnSc2
o	o	k7c6
tloušťce	tloušťka	k1gFnSc6
1,5	1,5	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
povrchu	povrch	k1gInSc2
byl	být	k5eAaImAgInS
potažen	potáhnout	k5eAaPmNgInS
plátnem	plátno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křídlo	křídlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
zapuštěné	zapuštěný	k2eAgNnSc1d1
do	do	k7c2
spodní	spodní	k2eAgFnSc2d1
části	část	k1gFnSc2
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
typické	typický	k2eAgNnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
Gribovského	Gribovského	k2eAgInPc4d1
letouny	letoun	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Zaoblený	zaoblený	k2eAgInSc1d1
trup	trup	k1gInSc1
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
ze	z	k7c2
dřeva	dřevo	k1gNnSc2
s	s	k7c7
plátěným	plátěný	k2eAgInSc7d1
potahem	potah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kokpit	kokpit	k1gInSc4
pilota	pilot	k1gMnSc4
na	na	k7c6
úrovni	úroveň	k1gFnSc6
odtokové	odtokový	k2eAgFnSc2d1
hrany	hrana	k1gFnSc2
křídla	křídlo	k1gNnSc2
byl	být	k5eAaImAgInS
otevřený	otevřený	k2eAgInSc1d1
a	a	k8xC
chráněný	chráněný	k2eAgMnSc1d1
jen	jen	k9
s	s	k7c7
malým	malý	k2eAgInSc7d1
štítem	štít	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Motor	motor	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
zúžené	zúžený	k2eAgFnSc6d1
přídi	příď	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
válce	válec	k1gInSc2
vystupovaly	vystupovat	k5eAaImAgInP
z	z	k7c2
povrchu	povrch	k1gInSc2
přídě	příď	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
vystaveny	vystavit	k5eAaPmNgFnP
chlazení	chlazení	k1gNnSc4
vzduchem	vzduch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
G-8	G-8	k1gFnSc1
měl	mít	k5eAaImAgInS
pevný	pevný	k2eAgInSc1d1
<g/>
,	,	kIx,
beznápravový	beznápravový	k2eAgInSc1d1
dvoukolový	dvoukolový	k2eAgInSc1d1
podvozek	podvozek	k1gInSc1
s	s	k7c7
rozchodem	rozchod	k1gInSc7
kol	kolo	k1gNnPc2
1,6	1,6	k4
m	m	kA
a	a	k8xC
vzadu	vzadu	k6eAd1
ostruhu	ostruh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Kola	Kola	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
V-vzpěrách	V-vzpěra	k1gFnPc6
zavěšených	zavěšený	k2eAgFnPc2d1
ke	k	k7c3
středu	střed	k1gInSc3
spodní	spodní	k2eAgFnSc2d1
části	část	k1gFnSc2
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
kolo	kolo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
odkryté	odkrytý	k2eAgNnSc1d1
svislé	svislý	k2eAgNnSc1d1
rameno	rameno	k1gNnSc1
tlumiče	tlumič	k1gInSc2
kmitů	kmit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
NZ-85	NZ-85	k1gMnSc1
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Letoun	letoun	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
úspěšně	úspěšně	k6eAd1
složil	složit	k5eAaPmAgMnS
letové	letový	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
a	a	k8xC
byl	být	k5eAaImAgInS
zanesen	zanést	k5eAaPmNgInS
do	do	k7c2
sovětského	sovětský	k2eAgInSc2d1
leteckého	letecký	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
s	s	k7c7
imatrikulací	imatrikulace	k1gFnSc7
CCCP-S	CCCP-S	k1gFnSc7
<g/>
141	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Plánovaný	plánovaný	k2eAgInSc1d1
let	let	k1gInSc1
na	na	k7c6
září	září	k1gNnSc6
1932	#num#	k4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
měsíci	měsíc	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pilot	pilot	k1gMnSc1
D.	D.	kA
A.	A.	kA
Košic	Košice	k1gInPc2
(	(	kIx(
<g/>
К	К	k?
<g/>
)	)	kIx)
na	na	k7c6
G-8	G-8	k1gFnSc6
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
na	na	k7c4
velkou	velký	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
asi	asi	k9
5	#num#	k4
200	#num#	k4
km	km	kA
dlouhou	dlouhý	k2eAgFnSc4d1
po	po	k7c6
trase	trasa	k1gFnSc6
Moskva-Gorkyj-Kazaň-Stalingrad-Rostov	Moskva-Gorkyj-Kazaň-Stalingrad-Rostov	k1gInSc1
na	na	k7c4
Donu-Koktebel	Donu-Koktebel	k1gInSc4
na	na	k7c4
Krymu-Simferopol-Zaporoží-Kyjev-Charkov-Orel	Krymu-Simferopol-Zaporoží-Kyjev-Charkov-Orel	k1gInSc4
a	a	k8xC
zpět	zpět	k6eAd1
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
plánované	plánovaný	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
byla	být	k5eAaImAgFnS
za	za	k7c4
10	#num#	k4
dnů	den	k1gInPc2
prolétnuta	prolétnut	k2eAgFnSc1d1
valná	valný	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
500	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
letoun	letoun	k1gInSc1
nedoletěl	doletět	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
Charkova	Charkov	k1gInSc2
kvůli	kvůli	k7c3
chybnému	chybný	k2eAgNnSc3d1
přepínání	přepínání	k1gNnSc3
palivových	palivový	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
Košic	Košice	k1gInPc2
havaroval	havarovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
tohoto	tento	k3xDgNnSc2
nouzového	nouzový	k2eAgNnSc2d1
přistání	přistání	k1gNnSc2
letadlo	letadlo	k1gNnSc4
rozbil	rozbít	k5eAaPmAgInS
a	a	k8xC
již	již	k6eAd1
nebylo	být	k5eNaImAgNnS
opraveno	opraven	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgMnS
nový	nový	k2eAgMnSc1d1
G-8	G-8	k1gMnSc1
s	s	k7c7
motorem	motor	k1gInSc7
Walter	Walter	k1gMnSc1
NZ-	NZ-	k1gMnSc1
<g/>
85	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
pak	pak	k6eAd1
používal	používat	k5eAaImAgInS
stejnou	stejný	k2eAgFnSc4d1
imatrikulaci	imatrikulace	k1gFnSc4
CCCP-S	CCCP-S	k1gFnSc2
<g/>
141	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
letouny	letoun	k1gInPc4
byly	být	k5eAaImAgFnP
použity	použít	k5eAaPmNgInP
modernější	moderní	k2eAgInPc1d2
materiály	materiál	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yRnSc2,k3yQnSc2
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
byla	být	k5eAaImAgFnS
snížena	snížit	k5eAaPmNgFnS
o	o	k7c4
15	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Osoaviachim	Osoaviachim	k1gMnSc1
(	(	kIx(
<g/>
DOSAAF	DOSAAF	kA
<g/>
)	)	kIx)
</s>
<s>
Gribovskij	Gribovskít	k5eAaPmRp2nS
G-8	G-8	k1gFnSc1
(	(	kIx(
<g/>
skica	skica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
Údaje	údaj	k1gInPc1
dle	dle	k7c2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
pilot	pilot	k1gMnSc1
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
křídla	křídlo	k1gNnSc2
<g/>
:	:	kIx,
8,00	8,00	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
5,00	5,00	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
1,80	1,80	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
9,00	9,00	k4
m	m	kA
<g/>
2	#num#	k4
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
<g/>
:	:	kIx,
53,6	53,6	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
letounu	letoun	k1gInSc2
<g/>
:	:	kIx,
320	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
325	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
483	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
468	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
vzduchem	vzduch	k1gInSc7
chlazený	chlazený	k2eAgInSc1d1
hvězdicový	hvězdicový	k2eAgInSc1d1
pětiválcový	pětiválcový	k2eAgInSc1d1
motor	motor	k1gInSc1
Walter	Walter	k1gMnSc1
NZ-	NZ-	k1gMnSc1
<g/>
60	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
sedmiválcový	sedmiválcový	k2eAgInSc1d1
motor	motor	k1gInSc1
Walter	Walter	k1gMnSc1
NZ-85	NZ-85	k1gMnSc1
</s>
<s>
nominální	nominální	k2eAgInSc1d1
výkon	výkon	k1gInSc1
<g/>
:	:	kIx,
60	#num#	k4
k	k	k7c3
(	(	kIx(
<g/>
44	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
při	při	k7c6
1400	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
85	#num#	k4
k	k	k7c3
(	(	kIx(
<g/>
62,5	62,5	k4
kW	kW	kA
<g/>
)	)	kIx)
při	při	k7c6
1400	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
vzletový	vzletový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
<g/>
:	:	kIx,
75	#num#	k4
k	k	k7c3
(	(	kIx(
<g/>
55	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
při	při	k7c6
1750	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
100	#num#	k4
k	k	k7c3
(	(	kIx(
<g/>
73,5	73,5	k4
kW	kW	kA
<g/>
)	)	kIx)
při	při	k7c6
1750	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vrtule	vrtule	k1gFnSc1
<g/>
:	:	kIx,
dřevěná	dřevěný	k2eAgFnSc1d1
<g/>
,	,	kIx,
dvoulistá	dvoulistý	k2eAgFnSc1d1
vrtule	vrtule	k1gFnSc1
s	s	k7c7
pevnými	pevný	k2eAgInPc7d1
listy	list	k1gInPc7
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
150	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
125	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
150	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
80-85	80-85	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
550	#num#	k4
km	km	kA
</s>
<s>
Vytrvalost	vytrvalost	k1gFnSc1
<g/>
:	:	kIx,
4	#num#	k4
h	h	k?
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
3	#num#	k4
000	#num#	k4
m	m	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
000	#num#	k4
m	m	kA
(	(	kIx(
<g/>
NZ-	NZ-	k1gFnSc1
<g/>
85	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
143	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
1000	#num#	k4
m	m	kA
7	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
do	do	k7c2
2	#num#	k4
000	#num#	k4
m	m	kA
18	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
do	do	k7c2
3	#num#	k4
000	#num#	k4
m	m	kA
24	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Gribovsky	Gribovsko	k1gNnPc7
G-8	G-8	k1gMnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Gribovski	Gribovski	k1gNnSc6
G-8	G-8	k1gFnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
GUNSTON	GUNSTON	kA
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Osprey	Osprea	k1gFnSc2
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Russian	Russian	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
1875	#num#	k4
<g/>
-	-	kIx~
<g/>
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Osprey	Osprey	k1gInPc1
(	(	kIx(
<g/>
Reed	Reed	k1gInSc1
Consumer	Consumer	k1gInSc1
Books	Books	k1gInSc4
Ltd	ltd	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
85532	#num#	k4
405	#num#	k4
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
78	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Sovětská	sovětský	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
od	od	k7c2
A	A	kA
do	do	k7c2
Z.	Z.	kA
Křídla	křídlo	k1gNnPc1
vlasti	vlast	k1gFnSc2
<g/>
.	.	kIx.
1960	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1960	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
16	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Г	Г	k?
В	В	k?
К	К	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
А	А	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Г	Г	k?
Г	Г	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
У	У	k?
н	н	k?
(	(	kIx(
<g/>
airwar	airwar	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Le	Le	k1gMnSc4
constructeur	constructeur	k1gMnSc1
Gribovski	Gribovsk	k1gFnSc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Les	les	k1gInSc1
Ailes	Ailes	k1gInSc4
<g/>
.	.	kIx.
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
592	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Tableau	Tableaum	k1gNnSc3
des	des	k1gNnSc2
Avions	Avionsa	k1gFnPc2
Légers	Légers	k1gInSc4
de	de	k?
L	L	kA
<g/>
´	´	k?
<g/>
U.	U.	kA
<g/>
R.	R.	kA
<g/>
S.	S.	kA
<g/>
S.	S.	kA
Créés	Créésa	k1gFnPc2
Entre	Entr	k1gInSc5
1923	#num#	k4
et	et	k?
1935	#num#	k4
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
'	'	kIx"
<g/>
Aérophile	Aérophila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
1936	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1936	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PROUST	PROUST	kA
<g/>
,	,	kIx,
Max	max	kA
<g/>
.	.	kIx.
L	L	kA
<g/>
'	'	kIx"
<g/>
aviation	aviation	k1gInSc1
de	de	k?
tourisme	tourismus	k1gInSc5
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
'	'	kIx"
<g/>
Aérophile	Aérophila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janvier	Janvier	k1gInSc1
1937	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
45	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
193	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NĚMEČEK	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
historie	historie	k1gFnSc2
sovětského	sovětský	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
(	(	kIx(
<g/>
Cvičná	cvičný	k2eAgNnPc1d1
a	a	k8xC
sportovní	sportovní	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnPc1
<g/>
.	.	kIx.
1950	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
20	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
466	#num#	k4
<g/>
-	-	kIx~
<g/>
470	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Г	Г	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sovplane	sovplanout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Civil	civil	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
Register	registrum	k1gNnPc2
-	-	kIx~
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
airhistory	airhistor	k1gInPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠAVROV	ŠAVROV	kA
<g/>
,	,	kIx,
Vadim	Vadim	k?
Borisovič	Borisovič	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
konstrukce	konstrukce	k1gFnSc2
letadel	letadlo	k1gNnPc2
v	v	k7c6
SSSR	SSSR	kA
do	do	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
-	-	kIx~
И	И	k?
к	к	k?
с	с	k?
в	в	k?
С	С	k?
д	д	k?
1938	#num#	k4
г	г	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
481	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
Mašinostroenie	Mašinostroenie	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
5-217-02528-X	5-217-02528-X	k4
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1
Letadla	letadlo	k1gNnSc2
země	zem	k1gFnSc2
sovětů	sovět	k1gInPc2
(	(	kIx(
<g/>
Э	Э	k?
С	С	k?
с	с	k?
с	с	k?
<g/>
,	,	kIx,
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
))	))	k?
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
vydavatelství	vydavatelství	k1gNnSc1
DOSAAF	DOSAAF	kA
(	(	kIx(
<g/>
Д	Д	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
107066	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Walter	Walter	k1gMnSc1
NZ-60	NZ-60	k1gMnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
NZ-85	NZ-85	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gribovskij	Gribovskij	k1gFnSc2
G-8	G-8	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Г	Г	k?
na	na	k7c6
dic	dic	k?
<g/>
.	.	kIx.
<g/>
academic	academic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
G-8	G-8	k1gFnSc1
by	by	kYmCp3nS
V.K.	V.K.	k1gFnSc4
<g/>
Gribovskij	Gribovskij	k1gFnSc2
na	na	k7c4
ram-home	ram-hom	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Г	Г	k?
Г	Г	k?
na	na	k7c6
AviaDejaVu	AviaDejaVus	k1gInSc6
<g/>
.	.	kIx.
<g/>
ru	ru	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
