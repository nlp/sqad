<p>
<s>
Horní	horní	k2eAgInPc4d1	horní
ochozy	ochoz	k1gInPc4	ochoz
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc1	vrch
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
poblíže	poblíže	k7c2	poblíže
obce	obec	k1gFnSc2	obec
Němčičky	Němčička	k1gFnSc2	Němčička
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kobylí	kobylí	k2eAgNnSc4d1	kobylí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
327	[number]	k4	327
m	m	kA	m
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Němčičské	Němčičský	k2eAgFnSc2d1	Němčičská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
chráněná	chráněný	k2eAgFnSc1d1	chráněná
území	území	k1gNnSc4	území
Jesličky	jesličky	k1gFnPc4	jesličky
<g/>
,	,	kIx,	,
Nosperk	Nosperk	k1gInSc4	Nosperk
a	a	k8xC	a
Zázmoníky	Zázmoník	k1gInPc4	Zázmoník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnPc1	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
mezi	mezi	k7c7	mezi
Horními	horní	k2eAgInPc7d1	horní
ochozy	ochoz	k1gInPc7	ochoz
<g/>
,	,	kIx,	,
Ochozy	ochoz	k1gInPc7	ochoz
a	a	k8xC	a
Kuntinovem	Kuntinovo	k1gNnSc7	Kuntinovo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Panský	panský	k2eAgInSc1d1	panský
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgFnSc2d1	zbylá
světové	světový	k2eAgFnSc2d1	světová
strany	strana	k1gFnSc2	strana
jsou	být	k5eAaImIp3nP	být
pokryty	pokryt	k2eAgInPc1d1	pokryt
vinohrady	vinohrad	k1gInPc1	vinohrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Horních	horní	k2eAgInPc2d1	horní
ochozů	ochoz	k1gInPc2	ochoz
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
rozhledna	rozhledna	k1gFnSc1	rozhledna
Kraví	kraví	k2eAgFnSc1d1	kraví
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
</p>
