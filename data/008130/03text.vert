<s>
Traktor	traktor	k1gInSc1	traktor
(	(	kIx(	(
<g/>
internacionalismus	internacionalismus	k1gInSc1	internacionalismus
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
tractor	tractor	k1gInSc1	tractor
'	'	kIx"	'
<g/>
tahač	tahač	k1gInSc1	tahač
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
trahere	trahrat	k5eAaPmIp3nS	trahrat
–	–	k?	–
táhnout	táhnout	k5eAaImF	táhnout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
trakční	trakční	k2eAgInSc1d1	trakční
stroj	stroj	k1gInSc1	stroj
resp.	resp.	kA	resp.
motorové	motorový	k2eAgNnSc1d1	motorové
vozidlo	vozidlo	k1gNnSc1	vozidlo
sloužící	sloužící	k1gFnSc2	sloužící
především	především	k9	především
k	k	k7c3	k
tahu	tah	k1gInSc3	tah
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
ale	ale	k8xC	ale
také	také	k9	také
k	k	k7c3	k
nesení	nesení	k1gNnSc3	nesení
<g/>
,	,	kIx,	,
tlačení	tlačení	k1gNnSc6	tlačení
a	a	k8xC	a
pohonu	pohon	k1gInSc6	pohon
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
i	i	k8xC	i
dopravu	doprava	k1gFnSc4	doprava
zejména	zejména	k9	zejména
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
lese	les	k1gInSc6	les
nebo	nebo	k8xC	nebo
jiném	jiný	k2eAgInSc6d1	jiný
nerovném	rovný	k2eNgInSc6d1	nerovný
nebo	nebo	k8xC	nebo
nezpevněném	zpevněný	k2eNgInSc6d1	nezpevněný
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Traktor	traktor	k1gInSc1	traktor
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
údržbě	údržba	k1gFnSc6	údržba
silnic	silnice	k1gFnPc2	silnice
nebo	nebo	k8xC	nebo
stavebních	stavební	k2eAgFnPc6d1	stavební
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
jsou	být	k5eAaImIp3nP	být
vojenské	vojenský	k2eAgInPc1d1	vojenský
traktory	traktor	k1gInPc1	traktor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
a	a	k8xC	a
někde	někde	k6eAd1	někde
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xS	jako
tahače	tahač	k1gInPc4	tahač
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
čtyři	čtyři	k4xCgNnPc4	čtyři
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k8xC	ale
i	i	k9	i
pásový	pásový	k2eAgInSc4d1	pásový
traktor	traktor	k1gInSc4	traktor
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
zvaný	zvaný	k2eAgInSc1d1	zvaný
pásák	pásák	k?	pásák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
tanku	tank	k1gInSc2	tank
či	či	k8xC	či
sněžné	sněžný	k2eAgFnSc3d1	sněžná
rolbě	rolba	k1gFnSc3	rolba
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
dieselovým	dieselový	k2eAgInSc7d1	dieselový
motorem	motor	k1gInSc7	motor
s	s	k7c7	s
hnanou	hnaný	k2eAgFnSc7d1	hnaná
zadní	zadní	k2eAgFnSc7d1	zadní
nebo	nebo	k8xC	nebo
přední	přední	k2eAgFnSc7d1	přední
nápravou	náprava	k1gFnSc7	náprava
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
oběma	dva	k4xCgFnPc3	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Traktor	traktor	k1gInSc1	traktor
je	být	k5eAaImIp3nS	být
přizpůsoben	přizpůsobit	k5eAaPmNgInS	přizpůsobit
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
v	v	k7c6	v
náročném	náročný	k2eAgInSc6d1	náročný
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc3d2	vyšší
svahové	svahový	k2eAgFnSc3d1	Svahová
dostupnosti	dostupnost	k1gFnSc3	dostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
traktoru	traktor	k1gInSc2	traktor
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c6	na
přídi	příď	k1gFnSc6	příď
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nachází	nacházet	k5eAaImIp3nS	nacházet
tříbodové	tříbodový	k2eAgNnSc1d1	tříbodové
hydraulické	hydraulický	k2eAgNnSc1d1	hydraulické
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
upnout	upnout	k5eAaPmF	upnout
další	další	k2eAgNnSc4d1	další
příslušenství	příslušenství	k1gNnSc4	příslušenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
instalována	instalován	k2eAgFnSc1d1	instalována
vývodová	vývodový	k2eAgFnSc1d1	Vývodová
hřídel	hřídel	k1gFnSc1	hřídel
předávající	předávající	k2eAgFnSc2d1	předávající
točivý	točivý	k2eAgInSc4d1	točivý
moment	moment	k1gInSc4	moment
motoru	motor	k1gInSc2	motor
zapřaženým	zapřažený	k2eAgNnPc3d1	zapřažené
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hřídel	hřídel	k1gFnSc1	hřídel
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
traktorů	traktor	k1gInPc2	traktor
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
i	i	k9	i
vpředu	vpředu	k6eAd1	vpředu
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
traktoru	traktor	k1gInSc2	traktor
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
dobytka	dobytek	k1gInSc2	dobytek
(	(	kIx(	(
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
vůl	vůl	k1gMnSc1	vůl
<g/>
,	,	kIx,	,
kráva	kráva	k1gFnSc1	kráva
<g/>
)	)	kIx)	)
též	též	k9	též
parní	parní	k2eAgFnSc1d1	parní
lokomobila	lokomobila	k1gFnSc1	lokomobila
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
institutu	institut	k1gInSc2	institut
World	World	k1gMnSc1	World
Resources	Resources	k1gMnSc1	Resources
Institute	institut	k1gInSc5	institut
je	on	k3xPp3gInPc4	on
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
přes	přes	k7c4	přes
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
traktorů	traktor	k1gInPc2	traktor
(	(	kIx(	(
<g/>
96	[number]	k4	96
tisíc	tisíc	k4xCgInSc4	tisíc
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
všeobecně	všeobecně	k6eAd1	všeobecně
použitelný	použitelný	k2eAgInSc4d1	použitelný
traktor	traktor	k1gInSc4	traktor
sestrojili	sestrojit	k5eAaPmAgMnP	sestrojit
Američané	Američan	k1gMnPc1	Američan
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
byly	být	k5eAaImAgFnP	být
firmy	firma	k1gFnPc1	firma
Kolben	Kolbna	k1gFnPc2	Kolbna
a	a	k8xC	a
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
Wichterle-Kovařík	Wichterle-Kovařík	k1gMnSc1	Wichterle-Kovařík
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Éru	éra	k1gFnSc4	éra
malých	malý	k2eAgInPc2d1	malý
traktorů	traktor	k1gInPc2	traktor
ukončily	ukončit	k5eAaPmAgInP	ukončit
velké	velký	k2eAgInPc4d1	velký
lány	lán	k1gInPc4	lán
při	při	k7c6	při
kolektivizaci	kolektivizace	k1gFnSc6	kolektivizace
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
výrobců	výrobce	k1gMnPc2	výrobce
traktorů	traktor	k1gInPc2	traktor
dodnes	dodnes	k6eAd1	dodnes
přežil	přežít	k5eAaPmAgInS	přežít
jen	jen	k9	jen
Zetor	zetor	k1gInSc1	zetor
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
Zetor	zetor	k1gInSc1	zetor
15	[number]	k4	15
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
měl	mít	k5eAaImAgInS	mít
15	[number]	k4	15
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
klasické	klasický	k2eAgInPc1d1	klasický
traktory	traktor	k1gInPc1	traktor
výkonu	výkon	k1gInSc2	výkon
až	až	k9	až
160	[number]	k4	160
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
118	[number]	k4	118
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
260	[number]	k4	260
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
191	[number]	k4	191
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
klasické	klasický	k2eAgInPc1d1	klasický
traktory	traktor	k1gInPc1	traktor
výkonu	výkon	k1gInSc2	výkon
350	[number]	k4	350
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
257	[number]	k4	257
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kloubové	kloubový	k2eAgInPc1d1	kloubový
a	a	k8xC	a
pásové	pásový	k2eAgInPc1d1	pásový
traktory	traktor	k1gInPc1	traktor
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
550	[number]	k4	550
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
405	[number]	k4	405
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
Škoda	škoda	k1gFnSc1	škoda
Wikov	Wikov	k1gInSc4	Wikov
Zetor	zetor	k1gInSc1	zetor
ZTS	ZTS	kA	ZTS
Zahrada-Dílna-Stroje	Zahrada-Dílna-Stroj	k1gInPc1	Zahrada-Dílna-Stroj
Antonio	Antonio	k1gMnSc1	Antonio
Carraro	Carrara	k1gFnSc5	Carrara
Belarus	Belarus	k1gMnSc1	Belarus
Uralvagonzavod	Uralvagonzavod	k1gInSc4	Uralvagonzavod
Čeljabinský	čeljabinský	k2eAgInSc4d1	čeljabinský
traktorový	traktorový	k2eAgInSc4d1	traktorový
závod	závod	k1gInSc4	závod
Branson	Branson	k1gMnSc1	Branson
Case	Cas	k1gMnSc2	Cas
IH	IH	kA	IH
CAT	CAT	kA	CAT
Claas	Claas	k1gMnSc1	Claas
Deutz-Fahr	Deutz-Fahr	k1gMnSc1	Deutz-Fahr
Fendt	Fendt	k1gMnSc1	Fendt
Fordson	Fordson	k1gMnSc1	Fordson
Fortschritt	Fortschritt	k1gMnSc1	Fortschritt
Goldoni	Goldon	k1gMnPc1	Goldon
Challenger	Challenger	k1gMnSc1	Challenger
Iseki	Iseki	k1gNnSc2	Iseki
JCB	JCB	kA	JCB
John	John	k1gMnSc1	John
Deere	Deer	k1gInSc5	Deer
Kubota	Kubota	k1gFnSc1	Kubota
Lamborghini	Lamborghin	k2eAgMnPc1d1	Lamborghin
Landini	Landin	k2eAgMnPc1d1	Landin
Lanz-Bulldog	Lanz-Bulldog	k1gMnSc1	Lanz-Bulldog
Lindner	Lindner	k1gMnSc1	Lindner
Massey	Massea	k1gFnSc2	Massea
Ferguson	Ferguson	k1gMnSc1	Ferguson
McCormick	McCormick	k1gMnSc1	McCormick
New	New	k1gMnSc1	New
Holland	Holland	k1gInSc4	Holland
Same	Sam	k1gMnSc5	Sam
Steyr	Steyra	k1gFnPc2	Steyra
Tym	Tym	k?	Tym
Valtra	Valtr	k1gMnSc2	Valtr
ZTS	ZTS	kA	ZTS
LS	LS	kA	LS
Traktor	traktor	k1gInSc4	traktor
</s>
