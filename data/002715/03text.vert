<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kahleberg	Kahleberg	k1gInSc1	Kahleberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
Českého	český	k2eAgInSc2d1	český
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
hory	hora	k1gFnSc2	hora
činí	činit	k5eAaImIp3nS	činit
1344	[number]	k4	1344
m	m	kA	m
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
sedačková	sedačkový	k2eAgFnSc1d1	sedačková
lanovka	lanovka	k1gFnSc1	lanovka
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
1310	[number]	k4	1310
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
její	její	k3xOp3gFnSc1	její
horní	horní	k2eAgFnSc1d1	horní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
je	být	k5eAaImIp3nS	být
Lysá	lysý	k2eAgFnSc1d1	Lysá
Hora	hora	k1gFnSc1	hora
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hřebenem	hřeben	k1gInSc7	hřeben
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
k	k	k7c3	k
zahlédnutí	zahlédnutí	k1gNnSc3	zahlédnutí
i	i	k9	i
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
120	[number]	k4	120
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
zimního	zimní	k2eAgNnSc2d1	zimní
střediska	středisko	k1gNnSc2	středisko
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kupovitou	kupovitý	k2eAgFnSc4d1	kupovitá
horu	hora	k1gFnSc4	hora
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
vrcholem	vrchol	k1gInSc7	vrchol
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
západě	západ	k1gInSc6	západ
mezi	mezi	k7c7	mezi
vrcholem	vrchol	k1gInSc7	vrchol
Plešivec	plešivec	k1gMnSc1	plešivec
a	a	k8xC	a
horou	hora	k1gFnSc7	hora
Kotel	kotel	k1gInSc1	kotel
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
vrchol	vrchol	k1gInSc1	vrchol
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
upevněn	upevněn	k2eAgInSc1d1	upevněn
geodetický	geodetický	k2eAgInSc1d1	geodetický
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
klečovým	klečový	k2eAgInSc7d1	klečový
porostem	porost	k1gInSc7	porost
s	s	k7c7	s
občasnými	občasný	k2eAgFnPc7d1	občasná
holinami	holina	k1gFnPc7	holina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
smilkové	smilkový	k2eAgFnPc1d1	Smilková
loučky	loučka	k1gFnPc1	loučka
se	s	k7c7	s
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
květenou	květena	k1gFnSc7	květena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
rostou	růst	k5eAaImIp3nP	růst
poměrně	poměrně	k6eAd1	poměrně
zachovalé	zachovalý	k2eAgInPc1d1	zachovalý
smrkové	smrkový	k2eAgInPc1d1	smrkový
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
samotný	samotný	k2eAgInSc4d1	samotný
vrcholek	vrcholek	k1gInSc4	vrcholek
hory	hora	k1gFnSc2	hora
nevede	vést	k5eNaImIp3nS	vést
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
cesta	cesta	k1gFnSc1	cesta
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
I.	I.	kA	I.
zóně	zóna	k1gFnSc6	zóna
KRNAP	KRNAP	kA	KRNAP
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
zakázán	zakázán	k2eAgInSc4d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
vrchol	vrchol	k1gInSc4	vrchol
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
dostat	dostat	k5eAaPmF	dostat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
lanovkou	lanovka	k1gFnSc7	lanovka
<g/>
,	,	kIx,	,
od	od	k7c2	od
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
vzdálen	vzdálen	k2eAgMnSc1d1	vzdálen
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
horní	horní	k2eAgFnSc2d1	horní
stanice	stanice	k1gFnSc2	stanice
lanovky	lanovka	k1gFnSc2	lanovka
zimní	zimní	k2eAgFnSc1d1	zimní
běžecká	běžecký	k2eAgFnSc1d1	běžecká
cesta	cesta	k1gFnSc1	cesta
opatřená	opatřený	k2eAgFnSc1d1	opatřená
tyčovým	tyčový	k2eAgNnSc7d1	tyčové
značením	značení	k1gNnSc7	značení
<g/>
.	.	kIx.	.
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
-	-	kIx~	-
Lysá	lysat	k5eAaImIp3nS	lysat
hora	hora	k1gFnSc1	hora
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lysá	Lysá	k1gFnSc1	Lysá
hora	hora	k1gFnSc1	hora
na	na	k7c4	na
Tisicovky	Tisicovka	k1gFnPc4	Tisicovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Provozovatel	provozovatel	k1gMnSc1	provozovatel
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
na	na	k7c4	na
Lysou	lysý	k2eAgFnSc4d1	Lysá
horu	hora	k1gFnSc4	hora
</s>
