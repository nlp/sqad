<p>
<s>
Aude	Audat	k5eAaPmIp3nS	Audat
[	[	kIx(	[
<g/>
o	o	k7c4	o
<g/>
:	:	kIx,	:
<g/>
d	d	k?	d
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
departement	departement	k1gInSc1	departement
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
département	département	k1gInSc1	département
<g/>
)	)	kIx)	)
číslo	číslo	k1gNnSc1	číslo
11	[number]	k4	11
v	v	k7c6	v
jihofrancouzském	jihofrancouzský	k2eAgInSc6d1	jihofrancouzský
regionu	region	k1gInSc6	region
Languedoc-Roussillon-Midi-Pyrénées	Languedoc-Roussillon-Midi-Pyrénées	k1gInSc1	Languedoc-Roussillon-Midi-Pyrénées
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
jím	on	k3xPp3gNnSc7	on
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
<g/>
.	.	kIx.	.
</s>
<s>
Departement	departement	k1gInSc1	departement
Aude	Aud	k1gFnSc2	Aud
bývá	bývat	k5eAaImIp3nS	bývat
též	též	k9	též
nazýván	nazývat	k5eAaImNgInS	nazývat
Země	zem	k1gFnSc2	zem
katarů	katar	k1gMnPc2	katar
(	(	kIx(	(
<g/>
Pays	Pays	k1gInSc1	Pays
Cathare	Cathar	k1gMnSc5	Cathar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Aude	Aude	k1gFnSc1	Aude
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
83	[number]	k4	83
departementů	departement	k1gInPc2	departement
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
během	během	k7c2	během
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1790	[number]	k4	1790
aplikací	aplikace	k1gFnPc2	aplikace
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
část	část	k1gFnSc1	část
bývalého	bývalý	k2eAgInSc2d1	bývalý
kraje	kraj	k1gInSc2	kraj
Languedoc	Languedoc	k1gFnSc1	Languedoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Aude	Aude	k6eAd1	Aude
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
regionu	region	k1gInSc2	region
Languedoc-Roussillon	Languedoc-Roussillon	k1gInSc1	Languedoc-Roussillon
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
departementy	departement	k1gInPc7	departement
Pyrénées-Orientales	Pyrénées-Orientalesa	k1gFnPc2	Pyrénées-Orientalesa
<g/>
,	,	kIx,	,
Ariè	Ariè	k1gFnPc2	Ariè
<g/>
,	,	kIx,	,
Haute-Garonne	Haute-Garonn	k1gMnSc5	Haute-Garonn
<g/>
,	,	kIx,	,
Tarn	Tarno	k1gNnPc2	Tarno
a	a	k8xC	a
Hérault	Héraulta	k1gFnPc2	Héraulta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
(	(	kIx(	(
<g/>
Lví	lví	k2eAgFnSc1d1	lví
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
Madrè	Madrè	k1gFnSc1	Madrè
2469	[number]	k4	2469
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Arrondissementy	Arrondissement	k1gInPc7	Arrondissement
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc7	okres
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
jsou	být	k5eAaImIp3nP	být
vinařství	vinařství	k1gNnPc1	vinařství
<g/>
,	,	kIx,	,
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
a	a	k8xC	a
báňský	báňský	k2eAgInSc1d1	báňský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
Aude	Aude	k1gFnPc2	Aude
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
panuje	panovat	k5eAaImIp3nS	panovat
středozemské	středozemský	k2eAgNnSc1d1	středozemské
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
velmi	velmi	k6eAd1	velmi
stálé	stálý	k2eAgNnSc4d1	stálé
a	a	k8xC	a
slunečné	slunečný	k2eAgNnSc4d1	slunečné
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
a	a	k8xC	a
k	k	k7c3	k
Pyrenejím	Pyreneje	k1gFnPc3	Pyreneje
je	být	k5eAaImIp3nS	být
chladněji	chladně	k6eAd2	chladně
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
území	území	k1gNnSc2	území
již	již	k6eAd1	již
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Regiony	region	k1gInPc1	region
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Aude	Aud	k1gFnSc2	Aud
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Aude	Aud	k1gFnSc2	Aud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Aude	Aude	k1gFnSc4	Aude
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
místní	místní	k2eAgFnSc2d1	místní
prefektury	prefektura	k1gFnSc2	prefektura
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
</s>
</p>
