<s>
Osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
toku	tok	k1gInSc2	tok
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
pronikajícího	pronikající	k2eAgNnSc2d1	pronikající
přes	přes	k7c4	přes
semipermeabilní	semipermeabilní	k2eAgFnSc4d1	semipermeabilní
(	(	kIx(	(
<g/>
polopropustnou	polopropustný	k2eAgFnSc4d1	polopropustná
<g/>
)	)	kIx)	)
membránu	membrána	k1gFnSc4	membrána
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
molekul	molekula	k1gFnPc2	molekula
nebo	nebo	k8xC	nebo
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
koncentraci	koncentrace	k1gFnSc6	koncentrace
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
pronikat	pronikat	k5eAaImF	pronikat
přes	přes	k7c4	přes
polopropustné	polopropustný	k2eAgFnPc4d1	polopropustná
membrány	membrána	k1gFnPc4	membrána
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
osmoticky	osmoticky	k6eAd1	osmoticky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
ředit	ředit	k5eAaImF	ředit
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
roztoky	roztok	k1gInPc1	roztok
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
membrány	membrána	k1gFnSc2	membrána
stejně	stejně	k6eAd1	stejně
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
<g/>
.	.	kIx.	.
</s>
<s>
Osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
živé	živý	k2eAgFnPc1d1	živá
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
polopropustná	polopropustný	k2eAgFnSc1d1	polopropustná
<g/>
.	.	kIx.	.
</s>
<s>
Osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
zředěného	zředěný	k2eAgInSc2d1	zředěný
roztoku	roztok	k1gInSc2	roztok
značíme	značit	k5eAaImIp1nP	značit
řeckým	řecký	k2eAgNnSc7d1	řecké
písmenem	písmeno	k1gNnSc7	písmeno
π	π	k?	π
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vypočítán	vypočítat	k5eAaPmNgInS	vypočítat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vzorce	vzorec	k1gInPc1	vzorec
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
=	=	kIx~	=
c	c	k0	c
R	R	kA	R
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
pi	pi	k0	pi
=	=	kIx~	=
<g/>
cRT	cRT	k?	cRT
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k9	kde
c	c	k0	c
je	být	k5eAaImIp3nS	být
molární	molární	k2eAgFnSc2d1	molární
koncentrace	koncentrace	k1gFnSc2	koncentrace
R	R	kA	R
je	být	k5eAaImIp3nS	být
molární	molární	k2eAgFnSc1d1	molární
plynová	plynový	k2eAgFnSc1d1	plynová
konstanta	konstanta	k1gFnSc1	konstanta
T	T	kA	T
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
Pro	pro	k7c4	pro
jednobuněčné	jednobuněčný	k2eAgInPc4d1	jednobuněčný
organismy	organismus	k1gInPc4	organismus
je	být	k5eAaImIp3nS	být
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgNnSc1d1	důležité
umět	umět	k5eAaImF	umět
se	se	k3xPyFc4	se
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
s	s	k7c7	s
neustálým	neustálý	k2eAgInSc7d1	neustálý
vstupem	vstup	k1gInSc7	vstup
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
způsobeným	způsobený	k2eAgInSc7d1	způsobený
osmotickým	osmotický	k2eAgInSc7d1	osmotický
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Pulsující	pulsující	k2eAgFnPc1d1	pulsující
vakuoly	vakuola	k1gFnPc1	vakuola
některých	některý	k3yIgMnPc2	některý
prvoků	prvok	k1gMnPc2	prvok
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
pumpy	pumpa	k1gFnPc1	pumpa
přečerpávající	přečerpávající	k2eAgFnSc4d1	přečerpávající
nadbytečnou	nadbytečný	k2eAgFnSc4d1	nadbytečná
vodu	voda	k1gFnSc4	voda
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Turgor	turgor	k1gInSc1	turgor
<g/>
.	.	kIx.	.
</s>
<s>
Osmotický	osmotický	k2eAgInSc1d1	osmotický
potenciál	potenciál	k1gInSc1	potenciál
buňky	buňka	k1gFnSc2	buňka
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
do	do	k7c2	do
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
přes	přes	k7c4	přes
cytoplazmatickou	cytoplazmatický	k2eAgFnSc4d1	cytoplazmatická
membránu	membrána	k1gFnSc4	membrána
vniká	vnikat	k5eAaImIp3nS	vnikat
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
osmoticky	osmoticky	k6eAd1	osmoticky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rostlinné	rostlinný	k2eAgFnPc1d1	rostlinná
buňky	buňka	k1gFnPc1	buňka
mají	mít	k5eAaImIp3nP	mít
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
pronikající	pronikající	k2eAgFnSc1d1	pronikající
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
ji	on	k3xPp3gFnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
rozpínat	rozpínat	k5eAaImF	rozpínat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
napětí	napětí	k1gNnSc1	napětí
působící	působící	k2eAgFnSc2d1	působící
proti	proti	k7c3	proti
buněčné	buněčný	k2eAgFnSc3d1	buněčná
stěně	stěna	k1gFnSc3	stěna
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
turgor	turgor	k1gInSc1	turgor
<g/>
.	.	kIx.	.
</s>
<s>
Turgor	turgor	k1gInSc1	turgor
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
buněk	buňka	k1gFnPc2	buňka
tvoří	tvořit	k5eAaImIp3nP	tvořit
oporu	opora	k1gFnSc4	opora
rostlinným	rostlinný	k2eAgFnPc3d1	rostlinná
tkáním	tkáň	k1gFnPc3	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
turgor	turgor	k1gInSc1	turgor
snižuje	snižovat	k5eAaImIp3nS	snižovat
a	a	k8xC	a
rostlina	rostlina	k1gFnSc1	rostlina
vadne	vadnout	k5eAaImIp3nS	vadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
extracelulární	extracelulární	k2eAgFnSc6d1	extracelulární
tekutině	tekutina	k1gFnSc6	tekutina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
přesně	přesně	k6eAd1	přesně
daná	daný	k2eAgFnSc1d1	daná
koncentrace	koncentrace	k1gFnSc1	koncentrace
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Extracelulární	Extracelulární	k2eAgNnSc1d1	Extracelulární
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
k	k	k7c3	k
prostředí	prostředí	k1gNnSc3	prostředí
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
isotonické	isotonický	k2eAgFnSc2d1	isotonická
a	a	k8xC	a
buňky	buňka	k1gFnSc2	buňka
tak	tak	k6eAd1	tak
nejsou	být	k5eNaImIp3nP	být
poškozovány	poškozován	k2eAgMnPc4d1	poškozován
nadměrným	nadměrný	k2eAgInSc7d1	nadměrný
přísunem	přísun	k1gInSc7	přísun
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
jejím	její	k3xOp3gInSc7	její
přílišným	přílišný	k2eAgInSc7d1	přílišný
úbytkem	úbytek	k1gInSc7	úbytek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
iontů	ion	k1gInPc2	ion
solí	sůl	k1gFnPc2	sůl
se	se	k3xPyFc4	se
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
osmolalitě	osmolalita	k1gFnSc6	osmolalita
prostředí	prostředí	k1gNnSc2	prostředí
podílí	podílet	k5eAaImIp3nS	podílet
anionty	anion	k1gInPc4	anion
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Koloidně-osmotický	Koloidněsmotický	k2eAgInSc1d1	Koloidně-osmotický
tlak	tlak	k1gInSc1	tlak
proteinů	protein	k1gInPc2	protein
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tlakem	tlak	k1gInSc7	tlak
onkotickým	onkotický	k2eAgInSc7d1	onkotický
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
onkotického	onkotický	k2eAgInSc2d1	onkotický
tlaku	tlak	k1gInSc2	tlak
proteinů	protein	k1gInPc2	protein
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
umožnění	umožnění	k1gNnSc6	umožnění
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
následné	následný	k2eAgInPc4d1	následný
resorbce	resorbec	k1gInPc4	resorbec
tkáňového	tkáňový	k2eAgInSc2d1	tkáňový
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
kapilár	kapilára	k1gFnPc2	kapilára
je	být	k5eAaImIp3nS	být
hydrostatický	hydrostatický	k2eAgInSc1d1	hydrostatický
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
generovaný	generovaný	k2eAgInSc1d1	generovaný
srdcem	srdce	k1gNnSc7	srdce
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vlásečnici	vlásečnice	k1gFnSc6	vlásečnice
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
onkotický	onkotický	k2eAgInSc1d1	onkotický
tlak	tlak	k1gInSc1	tlak
bílkovin	bílkovina	k1gFnPc2	bílkovina
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgNnSc1d1	krevní
plazma	plazma	k1gNnSc1	plazma
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
tlakem	tlak	k1gInSc7	tlak
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
jako	jako	k8xS	jako
tkáňový	tkáňový	k2eAgInSc4d1	tkáňový
mok	mok	k1gInSc4	mok
nesoucí	nesoucí	k2eAgFnSc2d1	nesoucí
živiny	živina	k1gFnSc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
kapilár	kapilára	k1gFnPc2	kapilára
hydrostatický	hydrostatický	k2eAgInSc1d1	hydrostatický
tlak	tlak	k1gInSc1	tlak
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
<g/>
,	,	kIx,	,
onkotický	onkotický	k2eAgInSc1d1	onkotický
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
vyšší	vysoký	k2eAgInSc1d2	vyšší
a	a	k8xC	a
tkáňový	tkáňový	k2eAgInSc1d1	tkáňový
mok	mok	k1gInSc1	mok
(	(	kIx(	(
<g/>
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
metabolity	metabolit	k1gInPc1	metabolit
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nasáván	nasávat	k5eAaImNgInS	nasávat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
koncentrace	koncentrace	k1gFnSc2	koncentrace
bílkovin	bílkovina	k1gFnPc2	bílkovina
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
otoky	otok	k1gInPc1	otok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
onkotický	onkotický	k2eAgInSc1d1	onkotický
tlak	tlak	k1gInSc1	tlak
už	už	k6eAd1	už
nestačí	stačit	k5eNaBmIp3nS	stačit
k	k	k7c3	k
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
nadbytečného	nadbytečný	k2eAgInSc2d1	nadbytečný
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Plazmolýza	Plazmolýza	k1gFnSc1	Plazmolýza
Deplazmolýza	Deplazmolýza	k1gFnSc1	Deplazmolýza
Plazmoptýza	Plazmoptýza	k1gFnSc1	Plazmoptýza
Plazmorhiza	Plazmorhiza	k1gFnSc1	Plazmorhiza
Difúze	difúze	k1gFnSc1	difúze
Osmóza	osmóza	k1gFnSc1	osmóza
Tonicita	tonicita	k1gFnSc1	tonicita
Osmotický	osmotický	k2eAgInSc4d1	osmotický
potenciál	potenciál	k1gInSc4	potenciál
Turgor	turgor	k1gInSc1	turgor
Vodní	vodní	k2eAgInSc1d1	vodní
potenciál	potenciál	k1gInSc4	potenciál
Raoultův	Raoultův	k2eAgInSc4d1	Raoultův
zákon	zákon	k1gInSc4	zákon
</s>
