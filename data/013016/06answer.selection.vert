<s>
Protože	protože	k8xS	protože
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Saturnu	Saturn	k1gInSc2	Saturn
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
35,49	[number]	k4	35,49
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
daleko	daleko	k6eAd1	daleko
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
rychlost	rychlost	k1gFnSc4	rychlost
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
nejspíše	nejspíše	k9	nejspíše
původní	původní	k2eAgNnPc4d1	původní
složení	složení	k1gNnPc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nabalil	nabalit	k5eAaPmAgMnS	nabalit
už	už	k6eAd1	už
během	během	k7c2	během
vzniku	vznik	k1gInSc2	vznik
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
