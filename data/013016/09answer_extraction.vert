<s>
Protože	protože	k8xS	protože
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Saturnu	Saturn	k1gInSc2	Saturn
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
35,49	[number]	k4	35,49
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
daleko	daleko	k6eAd1	daleko
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
rychlost	rychlost	k1gFnSc4	rychlost
molekul	molekula	k1gFnPc2	molekula
</s>
