<s>
Proč	proč	k6eAd1	proč
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c6	na
Saturnu	Saturn	k1gInSc6	Saturn
nejspíše	nejspíše	k9	nejspíše
původní	původní	k2eAgNnSc1d1	původní
složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nabalil	nabalit	k5eAaPmAgMnS	nabalit
už	už	k6eAd1	už
během	během	k7c2	během
vzniku	vznik	k1gInSc2	vznik
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
?	?	kIx.	?
</s>
