<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
<g/>
)	)	kIx)	)
či	či	k8xC	či
pouze	pouze	k6eAd1	pouze
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
leopard	leopard	k1gMnSc1	leopard
<g/>
,	,	kIx,	,
panter	panter	k1gMnSc1	panter
<g/>
,	,	kIx,	,
panther	panthra	k1gFnPc2	panthra
nebo	nebo	k8xC	nebo
pardál	pardál	k1gMnSc1	pardál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gMnSc1	druh
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Systema	Systema	k1gFnSc1	Systema
naturae	naturae	k1gFnSc1	naturae
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
asi	asi	k9	asi
před	před	k7c7	před
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
je	být	k5eAaImIp3nS	být
statná	statný	k2eAgFnSc1d1	statná
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
končetinami	končetina	k1gFnPc7	končetina
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
jaguárovi	jaguár	k1gMnSc3	jaguár
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
robustnější	robustní	k2eAgFnSc1d2	robustnější
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
kratší	krátký	k2eAgInSc4d2	kratší
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
větší	veliký	k2eAgFnPc4d2	veliký
rozety	rozeta	k1gFnPc4	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
17	[number]	k4	17
kg	kg	kA	kg
do	do	k7c2	do
90	[number]	k4	90
kg	kg	kA	kg
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
včetně	včetně	k7c2	včetně
ocasu	ocas	k1gInSc2	ocas
od	od	k7c2	od
140	[number]	k4	140
do	do	k7c2	do
240	[number]	k4	240
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
výrazný	výrazný	k2eAgMnSc1d1	výrazný
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
bývají	bývat	k5eAaImIp3nP	bývat
minimálně	minimálně	k6eAd1	minimálně
o	o	k7c4	o
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
těžší	těžký	k2eAgFnSc2d2	těžší
než	než	k8xS	než
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
žlutou	žlutý	k2eAgFnSc7d1	žlutá
<g/>
,	,	kIx,	,
pískovou	pískový	k2eAgFnSc7d1	písková
<g/>
,	,	kIx,	,
okrovou	okrový	k2eAgFnSc7d1	okrová
<g/>
,	,	kIx,	,
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
či	či	k8xC	či
cihlovou	cihlový	k2eAgFnSc7d1	cihlová
barvou	barva	k1gFnSc7	barva
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
černé	černý	k2eAgFnPc1d1	černá
skvrny	skvrna	k1gFnPc1	skvrna
a	a	k8xC	a
rozety	rozeta	k1gFnPc1	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
většinou	většinou	k6eAd1	většinou
nebývají	bývat	k5eNaImIp3nP	bývat
vyplněny	vyplněn	k2eAgInPc1d1	vyplněn
menšími	malý	k2eAgFnPc7d2	menší
skvrnkami	skvrnka	k1gFnPc7	skvrnka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srst	srst	k1gFnSc1	srst
uvnitř	uvnitř	k7c2	uvnitř
rozet	rozeta	k1gFnPc2	rozeta
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
poněkud	poněkud	k6eAd1	poněkud
tmavší	tmavý	k2eAgFnSc4d2	tmavší
barvu	barva	k1gFnSc4	barva
než	než	k8xS	než
srst	srst	k1gFnSc4	srst
okolo	okolo	k6eAd1	okolo
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgInSc1d2	světlejší
než	než	k8xS	než
boky	boka	k1gFnPc1	boka
a	a	k8xC	a
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
produkci	produkce	k1gFnSc4	produkce
pigmentu	pigment	k1gInSc2	pigment
melaninu	melanin	k1gInSc2	melanin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
černé	černý	k2eAgNnSc4d1	černé
zbarvení	zbarvení	k1gNnSc4	zbarvení
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Takoví	takový	k3xDgMnPc1	takový
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
označování	označování	k1gNnSc4	označování
jako	jako	k8xC	jako
černí	černý	k2eAgMnPc1d1	černý
levharti	levhart	k1gMnPc1	levhart
či	či	k8xC	či
černí	černý	k2eAgMnPc1d1	černý
panteři	panter	k1gMnPc1	panter
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
velkých	velký	k2eAgFnPc2d1	velká
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
často	často	k6eAd1	často
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
vynáší	vynášet	k5eAaImIp3nS	vynášet
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
před	před	k7c7	před
dalšími	další	k2eAgMnPc7d1	další
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lvy	lev	k1gInPc7	lev
a	a	k8xC	a
hyenami	hyena	k1gFnPc7	hyena
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
uzpůsobenou	uzpůsobený	k2eAgFnSc4d1	uzpůsobená
ke	k	k7c3	k
šplhání	šplhání	k1gNnSc3	šplhání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
na	na	k7c4	na
strom	strom	k1gInSc4	strom
zvíře	zvíře	k1gNnSc1	zvíře
podstatně	podstatně	k6eAd1	podstatně
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
kořistí	kořist	k1gFnSc7	kořist
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
především	především	k9	především
antilopy	antilopa	k1gFnPc4	antilopa
<g/>
,	,	kIx,	,
gazely	gazela	k1gFnPc4	gazela
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnPc4d1	divoká
ovce	ovce	k1gFnPc4	ovce
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc4	koza
a	a	k8xC	a
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInPc4d1	regionální
druhy	druh	k1gInPc4	druh
turů	tur	k1gMnPc2	tur
a	a	k8xC	a
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
šelmy	šelma	k1gFnPc1	šelma
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
i	i	k8xC	i
menší	malý	k2eAgMnSc1d2	menší
živočichové	živočich	k1gMnPc1	živočich
jako	jako	k8xS	jako
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
plazi	plaz	k1gMnPc1	plaz
a	a	k8xC	a
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vyhlédnutou	vyhlédnutý	k2eAgFnSc4d1	vyhlédnutá
oběť	oběť	k1gFnSc4	oběť
útočí	útočit	k5eAaImIp3nS	útočit
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
<g/>
,	,	kIx,	,
před	před	k7c7	před
finálním	finální	k2eAgInSc7d1	finální
výpadem	výpad	k1gInSc7	výpad
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
co	co	k3yRnSc4	co
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přiblížení	přiblížení	k1gNnSc3	přiblížení
využívá	využívat	k5eAaImIp3nS	využívat
křoviny	křovina	k1gFnPc4	křovina
<g/>
,	,	kIx,	,
podrost	podrost	k1gInSc1	podrost
a	a	k8xC	a
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
si	se	k3xPyFc3	se
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
počíhá	počíhat	k5eAaPmIp3nS	počíhat
i	i	k9	i
na	na	k7c6	na
stromě	strom	k1gInSc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
prchající	prchající	k2eAgFnSc1d1	prchající
oběť	oběť	k1gFnSc1	oběť
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
než	než	k8xS	než
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Krátkodobě	krátkodobě	k6eAd1	krátkodobě
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
okolo	okolo	k7c2	okolo
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skrytě	skrytě	k6eAd1	skrytě
žijící	žijící	k2eAgNnSc1d1	žijící
samotářské	samotářský	k2eAgNnSc1d1	samotářské
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
skupinky	skupinka	k1gFnPc1	skupinka
levhartů	levhart	k1gMnPc2	levhart
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
nebo	nebo	k8xC	nebo
o	o	k7c6	o
pár	pár	k4xCyI	pár
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
spíše	spíše	k9	spíše
nočního	noční	k2eAgMnSc4d1	noční
živočicha	živočich	k1gMnSc4	živočich
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
ruší	rušit	k5eAaImIp3nS	rušit
lidské	lidský	k2eAgFnPc4d1	lidská
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
dne	den	k1gInSc2	den
tráví	trávit	k5eAaImIp3nP	trávit
odpočinkem	odpočinek	k1gInSc7	odpočinek
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
mu	on	k3xPp3gNnSc3	on
většinou	většinou	k6eAd1	většinou
slouží	sloužit	k5eAaImIp3nS	sloužit
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
skály	skála	k1gFnPc1	skála
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
nepřátele	nepřítel	k1gMnPc4	nepřítel
a	a	k8xC	a
konkurenty	konkurent	k1gMnPc4	konkurent
patří	patřit	k5eAaImIp3nP	patřit
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
hyeny	hyena	k1gFnPc1	hyena
<g/>
,	,	kIx,	,
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
hyenovití	hyenovitý	k2eAgMnPc1d1	hyenovitý
<g/>
,	,	kIx,	,
dhoulové	dhoul	k1gMnPc1	dhoul
<g/>
,	,	kIx,	,
vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
paviáni	pavián	k1gMnPc1	pavián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
důležité	důležitý	k2eAgNnSc1d1	důležité
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obdivován	obdivovat	k5eAaImNgInS	obdivovat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
obáván	obáván	k2eAgMnSc1d1	obáván
<g/>
,	,	kIx,	,
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
divokost	divokost	k1gFnSc4	divokost
<g/>
,	,	kIx,	,
eleganci	elegance	k1gFnSc4	elegance
a	a	k8xC	a
lstivost	lstivost	k1gFnSc4	lstivost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
heraldických	heraldický	k2eAgNnPc2d1	heraldické
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
státních	státní	k2eAgInPc6d1	státní
znacích	znak	k1gInPc6	znak
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
lvy	lev	k1gMnPc7	lev
a	a	k8xC	a
tygry	tygr	k1gMnPc7	tygr
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
levharti	levhart	k1gMnPc1	levhart
nejčastěji	často	k6eAd3	často
stávají	stávat	k5eAaImIp3nP	stávat
lidožrouty	lidožrout	k1gMnPc4	lidožrout
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
především	především	k6eAd1	především
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
stále	stále	k6eAd1	stále
rozsévají	rozsévat	k5eAaImIp3nP	rozsévat
hrůzu	hrůza	k1gFnSc4	hrůza
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
nezřídka	nezřídka	k6eAd1	nezřídka
napadají	napadat	k5eAaPmIp3nP	napadat
lidmi	člověk	k1gMnPc7	člověk
chovaná	chovaný	k2eAgNnPc1d1	chované
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
a	a	k8xC	a
zabíjeni	zabíjet	k5eAaImNgMnP	zabíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
cca	cca	kA	cca
45	[number]	k4	45
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
typech	typ	k1gInPc6	typ
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
pouštích	poušť	k1gFnPc6	poušť
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
území	území	k1gNnSc1	území
výskytu	výskyt	k1gInSc2	výskyt
bylo	být	k5eAaImAgNnS	být
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgNnSc1d2	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
pronásledování	pronásledování	k1gNnSc3	pronásledování
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
lidským	lidský	k2eAgFnPc3d1	lidská
aktivitám	aktivita	k1gFnPc3	aktivita
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
již	již	k6eAd1	již
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
v	v	k7c6	v
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
<g/>
,	,	kIx,	,
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
Tunisku	Tunisko	k1gNnSc6	Tunisko
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k8xC	i
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
levharti	levhart	k1gMnPc1	levhart
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
odsud	odsud	k6eAd1	odsud
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
regionálních	regionální	k2eAgFnPc2d1	regionální
forem	forma	k1gFnPc2	forma
či	či	k8xC	či
poddruhů	poddruh	k1gInPc2	poddruh
čelí	čelit	k5eAaImIp3nS	čelit
akutní	akutní	k2eAgFnSc3d1	akutní
hrozbě	hrozba	k1gFnSc3	hrozba
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
IUCN	IUCN	kA	IUCN
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
levharta	levhart	k1gMnSc4	levhart
skvrnitého	skvrnitý	k2eAgMnSc4d1	skvrnitý
jako	jako	k8xS	jako
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
levhart	levhart	k1gMnSc1	levhart
se	se	k3xPyFc4	se
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
dostalo	dostat	k5eAaPmAgNnS	dostat
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
lewehart	leweharta	k1gFnPc2	leweharta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
poněkud	poněkud	k6eAd1	poněkud
zkomolilo	zkomolit	k5eAaPmAgNnS	zkomolit
latinský	latinský	k2eAgMnSc1d1	latinský
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
řecký	řecký	k2eAgInSc4d1	řecký
název	název	k1gInSc4	název
leopard	leopard	k1gMnSc1	leopard
či	či	k8xC	či
leopardus	leopardus	k1gMnSc1	leopardus
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
-hart	arta	k1gFnPc2	-harta
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
utvořena	utvořit	k5eAaPmNgFnS	utvořit
podle	podle	k7c2	podle
obdobného	obdobný	k2eAgNnSc2d1	obdobné
zakončení	zakončení	k1gNnSc2	zakončení
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
Leonhart	Leonhart	k1gInSc1	Leonhart
(	(	kIx(	(
<g/>
Leonard	Leonard	k1gMnSc1	Leonard
<g/>
,	,	kIx,	,
Linhart	Linhart	k1gMnSc1	Linhart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rī	Rī	k1gMnSc1	Rī
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
<g/>
)	)	kIx)	)
či	či	k8xC	či
Bernhart	Bernhart	k1gInSc1	Bernhart
(	(	kIx(	(
<g/>
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
)	)	kIx)	)
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
leopard	leopard	k1gMnSc1	leopard
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
synonymum	synonymum	k1gNnSc4	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
pravidelně	pravidelně	k6eAd1	pravidelně
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgInPc2	dva
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
leo	leo	k?	leo
případně	případně	k6eAd1	případně
leon	leon	k1gNnSc1	leon
(	(	kIx(	(
<g/>
lev	lev	k1gMnSc1	lev
<g/>
)	)	kIx)	)
a	a	k8xC	a
pardos	pardos	k1gInSc1	pardos
či	či	k8xC	či
pardus	pardus	k1gInSc1	pardus
(	(	kIx(	(
<g/>
pardál	pardál	k1gMnSc1	pardál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
křížence	kříženec	k1gMnSc4	kříženec
lva	lev	k1gMnSc4	lev
a	a	k8xC	a
pardála	pardál	k1gMnSc4	pardál
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
pantera	panter	k1gMnSc4	panter
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jiné	jiný	k2eAgNnSc4d1	jiné
zvíře	zvíře	k1gNnSc4	zvíře
než	než	k8xS	než
levhart	levhart	k1gMnSc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
použil	použít	k5eAaPmAgMnS	použít
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Naturalis	Naturalis	k1gInSc4	Naturalis
historia	historium	k1gNnSc2	historium
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pardos	pardos	k1gMnSc1	pardos
(	(	kIx(	(
<g/>
pardál	pardál	k1gMnSc1	pardál
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
spojitost	spojitost	k1gFnSc1	spojitost
se	s	k7c7	s
sanskrtským	sanskrtský	k2eAgInSc7d1	sanskrtský
prdakuh	prdakuha	k1gFnPc2	prdakuha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
pantera	panter	k1gMnSc4	panter
či	či	k8xC	či
tygra	tygr	k1gMnSc4	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
slovo	slovo	k1gNnSc4	slovo
leopard	leopard	k1gMnSc1	leopard
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
název	název	k1gInSc1	název
gepard	gepard	k1gMnSc1	gepard
–	–	k?	–
z	z	k7c2	z
italského	italský	k2eAgNnSc2d1	italské
slova	slovo	k1gNnSc2	slovo
gattopardo	gattopardo	k1gNnSc1	gattopardo
složením	složení	k1gNnSc7	složení
gatto	gatto	k1gNnSc1	gatto
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pardo	pardo	k1gNnSc1	pardo
(	(	kIx(	(
<g/>
pardál	pardál	k1gMnSc1	pardál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
vědecké	vědecký	k2eAgNnSc1d1	vědecké
pojmenování	pojmenování	k1gNnSc1	pojmenování
levharta	levhart	k1gMnSc2	levhart
Felis	Felis	k1gFnSc1	Felis
pardus	pardus	k1gInSc1	pardus
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Systema	System	k1gMnSc4	System
naturae	natura	k1gMnSc4	natura
<g/>
.	.	kIx.	.
</s>
<s>
Kombinaci	kombinace	k1gFnSc4	kombinace
jmen	jméno	k1gNnPc2	jméno
Panthera	Panther	k1gMnSc4	Panther
pardus	pardus	k1gInSc1	pardus
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
Leopold	Leopold	k1gMnSc1	Leopold
Fitzinger	Fitzinger	k1gMnSc1	Fitzinger
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
původně	původně	k6eAd1	původně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
řecké	řecký	k2eAgFnSc6d1	řecká
π	π	k?	π
(	(	kIx(	(
<g/>
pánthē	pánthē	k?	pánthē
<g/>
)	)	kIx)	)
a	a	k8xC	a
π	π	k?	π
(	(	kIx(	(
<g/>
pardos	pardos	k1gMnSc1	pardos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pan-	Pan-	k?	Pan-
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
<g/>
"	"	kIx"	"
a	a	k8xC	a
thē	thē	k?	thē
(	(	kIx(	(
<g/>
θ	θ	k?	θ
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
kořist	kořist	k1gFnSc1	kořist
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kořistí	kořist	k1gFnSc7	kořist
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
vše	všechen	k3xTgNnSc4	všechen
<g/>
"	"	kIx"	"
anebo	anebo	k8xC	anebo
volněji	volně	k6eAd2	volně
"	"	kIx"	"
<g/>
predátor	predátor	k1gMnSc1	predátor
všech	všecek	k3xTgNnPc2	všecek
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
druhové	druhový	k2eAgInPc4d1	druhový
pardos	pardos	k1gInSc4	pardos
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
panteří	panteří	k2eAgInSc1d1	panteří
samec	samec	k1gInSc1	samec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
kočkovití	kočkovití	k1gMnPc1	kočkovití
a	a	k8xC	a
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
velké	velký	k2eAgFnSc2d1	velká
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
Pantherinae	Pantherinae	k1gInSc1	Pantherinae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tygry	tygr	k1gMnPc7	tygr
<g/>
,	,	kIx,	,
lvy	lev	k1gMnPc7	lev
<g/>
,	,	kIx,	,
jaguáry	jaguár	k1gMnPc7	jaguár
a	a	k8xC	a
irbisy	irbis	k1gInPc7	irbis
je	být	k5eAaImIp3nS	být
řazen	řazen	k2eAgInSc1d1	řazen
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Panthera	Panthero	k1gNnSc2	Panthero
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
nemá	mít	k5eNaImIp3nS	mít
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
zařazení	zařazení	k1gNnSc4	zařazení
zavedl	zavést	k5eAaPmAgMnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
Reginald	Reginald	k1gMnSc1	Reginald
Innes	Innes	k1gMnSc1	Innes
Pocock	Pocock	k1gMnSc1	Pocock
<g/>
,	,	kIx,	,
když	když	k8xS	když
prováděl	provádět	k5eAaImAgMnS	provádět
revizi	revize	k1gFnSc4	revize
systematiky	systematika	k1gFnSc2	systematika
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
rodu	rod	k1gInSc2	rod
Panthera	Panthero	k1gNnSc2	Panthero
není	být	k5eNaImIp3nS	být
doposud	doposud	k6eAd1	doposud
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
ve	v	k7c6	v
fylogenezi	fylogeneze	k1gFnSc6	fylogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
uskutečněny	uskutečněn	k2eAgFnPc1d1	uskutečněna
několikeré	několikerý	k4xRyIgFnPc4	několikerý
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
přinesla	přinést	k5eAaPmAgFnS	přinést
poněkud	poněkud	k6eAd1	poněkud
odlišné	odlišný	k2eAgInPc4d1	odlišný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Panthera	Panthero	k1gNnSc2	Panthero
se	se	k3xPyFc4	se
od	od	k7c2	od
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
kočkami	kočka	k1gFnPc7	kočka
oddělil	oddělit	k5eAaPmAgMnS	oddělit
asi	asi	k9	asi
před	před	k7c7	před
11	[number]	k4	11
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
druh	druh	k1gInSc1	druh
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
asi	asi	k9	asi
před	před	k7c7	před
2,8	[number]	k4	2,8
<g/>
–	–	k?	–
<g/>
4,35	[number]	k4	4,35
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
i	i	k9	i
fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
druhu	druh	k1gInSc2	druh
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
pleistocénu	pleistocén	k1gInSc2	pleistocén
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgMnPc2	tento
původních	původní	k2eAgMnPc2d1	původní
levhartů	levhart	k1gMnPc2	levhart
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
vyhynula	vyhynout	k5eAaPmAgNnP	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
moderní	moderní	k2eAgFnSc6d1	moderní
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
asi	asi	k9	asi
před	před	k7c7	před
470	[number]	k4	470
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
850	[number]	k4	850
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pak	pak	k6eAd1	pak
migroval	migrovat	k5eAaImAgMnS	migrovat
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
fylogenetických	fylogenetický	k2eAgInPc2d1	fylogenetický
vývodů	vývod	k1gInPc2	vývod
je	být	k5eAaImIp3nS	být
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
příbuzným	příbuzný	k1gMnSc7	příbuzný
levharta	levhart	k1gMnSc2	levhart
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následován	následován	k2eAgInSc1d1	následován
jaguárem	jaguár	k1gMnSc7	jaguár
(	(	kIx(	(
<g/>
Panthera	Panther	k1gMnSc2	Panther
onca	oncus	k1gMnSc2	oncus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
irbis	irbis	k1gFnSc1	irbis
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
uncia	uncia	k1gFnSc1	uncia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
sněžný	sněžný	k2eAgMnSc1d1	sněžný
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
s	s	k7c7	s
levhartem	levhart	k1gMnSc7	levhart
skvrnitým	skvrnitý	k2eAgMnSc7d1	skvrnitý
tak	tak	k8xC	tak
úzce	úzko	k6eAd1	úzko
spřízněn	spřízněn	k2eAgInSc1d1	spřízněn
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nejnovější	nový	k2eAgFnSc1d3	nejnovější
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
jadernou	jaderný	k2eAgFnSc7d1	jaderná
i	i	k8xC	i
mitochondriální	mitochondriální	k2eAgFnSc7d1	mitochondriální
DNA	DNA	kA	DNA
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
výsledky	výsledek	k1gInPc1	výsledek
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
následující	následující	k2eAgInPc1d1	následující
kladogramy	kladogram	k1gInPc1	kladogram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genetickou	genetický	k2eAgFnSc4d1	genetická
výbavu	výbava	k1gFnSc4	výbava
druhu	druh	k1gInSc2	druh
tvoří	tvořit	k5eAaImIp3nP	tvořit
běžný	běžný	k2eAgInSc4d1	běžný
diploidní	diploidní	k2eAgInSc4d1	diploidní
karyotyp	karyotyp	k1gInSc4	karyotyp
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
36	[number]	k4	36
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Levhart	levhart	k1gMnSc1	levhart
vs	vs	k?	vs
<g/>
.	.	kIx.	.
pardál	pardál	k1gMnSc1	pardál
vs	vs	k?	vs
<g/>
.	.	kIx.	.
panter	panter	k1gMnSc1	panter
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zmatek	zmatek	k1gInSc1	zmatek
v	v	k7c4	v
pojmenování	pojmenování	k1gNnSc4	pojmenování
levharta	levhart	k1gMnSc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
používali	používat	k5eAaImAgMnP	používat
jméno	jméno	k1gNnSc4	jméno
pardus	pardus	k1gInSc1	pardus
(	(	kIx(	(
<g/>
pardál	pardál	k1gMnSc1	pardál
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
samce	samec	k1gMnSc4	samec
levharta	levhart	k1gMnSc4	levhart
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
panthera	panthera	k1gFnSc1	panthera
(	(	kIx(	(
<g/>
panter	panter	k1gInSc1	panter
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
geparda	gepard	k1gMnSc4	gepard
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
samici	samice	k1gFnSc4	samice
levharta	levhart	k1gMnSc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
levhart	levhart	k1gMnSc1	levhart
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
označovalo	označovat	k5eAaImAgNnS	označovat
údajného	údajný	k2eAgMnSc2d1	údajný
křížence	kříženec	k1gMnSc2	kříženec
pardála	pardál	k1gMnSc2	pardál
a	a	k8xC	a
lvice	lvice	k1gFnSc2	lvice
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
anglický	anglický	k2eAgMnSc1d1	anglický
klerik	klerik	k1gMnSc1	klerik
Edward	Edward	k1gMnSc1	Edward
Topsell	Topsell	k1gMnSc1	Topsell
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
kompilačním	kompilační	k2eAgNnSc6d1	kompilační
díle	dílo	k1gNnSc6	dílo
History	Histor	k1gMnPc4	Histor
of	of	k?	of
Four-footed	Fourooted	k1gInSc1	Four-footed
Beasts	Beasts	k1gInSc1	Beasts
and	and	k?	and
Serpents	Serpents	k1gInSc1	Serpents
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
panuje	panovat	k5eAaImIp3nS	panovat
nejasnost	nejasnost	k1gFnSc4	nejasnost
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
pardál	pardál	k1gMnSc1	pardál
a	a	k8xC	a
panther	panthra	k1gFnPc2	panthra
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
kočky	kočka	k1gFnPc1	kočka
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
mísí	mísit	k5eAaImIp3nP	mísit
se	s	k7c7	s
lvem	lev	k1gMnSc7	lev
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
těžko	těžko	k6eAd1	těžko
rozlišitelné	rozlišitelný	k2eAgInPc4d1	rozlišitelný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
systematika	systematika	k1gFnSc1	systematika
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
levhart	levhart	k1gMnSc1	levhart
"	"	kIx"	"
<g/>
a	a	k8xC	a
"	"	kIx"	"
<g/>
panter	panter	k1gInSc1	panter
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
"	"	kIx"	"
<g/>
pardál	pardál	k1gMnSc1	pardál
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
velikostech	velikost	k1gFnPc6	velikost
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
barevných	barevný	k2eAgInPc6d1	barevný
odstínech	odstín	k1gInPc6	odstín
srsti	srst	k1gFnSc2	srst
<g/>
:	:	kIx,	:
panter	panter	k1gMnSc1	panter
byl	být	k5eAaImAgMnS	být
oproti	oproti	k7c3	oproti
levhartovi	levhart	k1gMnSc3	levhart
údajně	údajně	k6eAd1	údajně
menší	malý	k2eAgFnSc4d2	menší
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
kulatější	kulatý	k2eAgFnSc4d2	kulatější
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
rozmazanější	rozmazaný	k2eAgFnPc4d2	rozmazanější
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
melanická	melanický	k2eAgFnSc1d1	melanická
forma	forma	k1gFnSc1	forma
byla	být	k5eAaImAgFnS	být
některými	některý	k3yIgFnPc7	některý
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
biologie	biologie	k1gFnSc1	biologie
považuje	považovat	k5eAaImIp3nS	považovat
levharta	levhart	k1gMnSc4	levhart
skvrnitého	skvrnitý	k2eAgMnSc2d1	skvrnitý
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
množství	množství	k1gNnSc1	množství
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
subspecií	subspecie	k1gFnPc2	subspecie
<g/>
)	)	kIx)	)
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
a	a	k8xC	a
formy	forma	k1gFnPc4	forma
===	===	k?	===
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
uznávaných	uznávaný	k2eAgFnPc2d1	uznávaná
subspecií	subspecie	k1gFnPc2	subspecie
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vedeno	vést	k5eAaImNgNnS	vést
okolo	okolo	k7c2	okolo
30	[number]	k4	30
<g/>
,	,	kIx,	,
po	po	k7c6	po
taxonomické	taxonomický	k2eAgFnSc6d1	Taxonomická
revizi	revize	k1gFnSc6	revize
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetických	genetický	k2eAgNnPc2d1	genetické
zkoumání	zkoumání	k1gNnSc2	zkoumání
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
8	[number]	k4	8
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Nejzásadnější	zásadní	k2eAgFnSc7d3	nejzásadnější
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
zahrnutí	zahrnutí	k1gNnSc1	zahrnutí
všech	všecek	k3xTgMnPc2	všecek
afrických	africký	k2eAgMnPc2d1	africký
levhartů	levhart	k1gMnPc2	levhart
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
poddruhu	poddruh	k1gInSc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
západoasijských	západoasijský	k2eAgMnPc2d1	západoasijský
levhartů	levhart	k1gMnPc2	levhart
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
věnovali	věnovat	k5eAaPmAgMnP	věnovat
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
zvláštnosti	zvláštnost	k1gFnSc3	zvláštnost
levharta	levhart	k1gMnSc2	levhart
jávského	jávský	k2eAgMnSc2d1	jávský
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
značně	značně	k6eAd1	značně
odlišný	odlišný	k2eAgMnSc1d1	odlišný
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
afrických	africký	k2eAgMnPc2d1	africký
i	i	k8xC	i
asijských	asijský	k2eAgMnPc2d1	asijský
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žije	žít	k5eAaImIp3nS	žít
již	již	k6eAd1	již
od	od	k7c2	od
středního	střední	k2eAgInSc2d1	střední
pleistocénu	pleistocén	k1gInSc2	pleistocén
(	(	kIx(	(
<g/>
cca	cca	kA	cca
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
<g/>
Soudobé	soudobý	k2eAgFnSc2d1	soudobá
studie	studie	k1gFnSc2	studie
tedy	tedy	k8xC	tedy
většinou	většina	k1gFnSc7	většina
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rozdělení	rozdělení	k1gNnSc2	rozdělení
na	na	k7c4	na
9	[number]	k4	9
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgInPc2d1	uvedený
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
subspecií	subspecie	k1gFnPc2	subspecie
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhubení	vyhubení	k1gNnSc2	vyhubení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
pardus	pardus	k1gInSc1	pardus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
a	a	k8xC	a
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
poddruh	poddruh	k1gInSc4	poddruh
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
všechny	všechen	k3xTgFnPc1	všechen
africké	africký	k2eAgFnPc1d1	africká
formy	forma	k1gFnPc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
regionálně	regionálně	k6eAd1	regionálně
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
–	–	k?	–
některé	některý	k3yIgFnPc1	některý
místní	místní	k2eAgFnPc1d1	místní
populace	populace	k1gFnPc1	populace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velkého	velký	k2eAgInSc2d1	velký
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
naopak	naopak	k6eAd1	naopak
malého	malý	k2eAgNnSc2d1	malé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
arabský	arabský	k2eAgMnSc1d1	arabský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
nimr	nimr	k1gInSc1	nimr
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
západě	západ	k1gInSc6	západ
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejmenším	malý	k2eAgInSc7d3	nejmenší
poddruhem	poddruh	k1gInSc7	poddruh
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	s	k7c7	s
světlým	světlý	k2eAgNnSc7d1	světlé
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
ho	on	k3xPp3gMnSc4	on
chovají	chovat	k5eAaImIp3nP	chovat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
arabské	arabský	k2eAgFnPc1d1	arabská
zoo	zoo	k1gFnPc1	zoo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
chovnou	chovný	k2eAgFnSc4d1	chovná
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgMnSc1d1	perský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
saxicolor	saxicolor	k1gInSc1	saxicolor
<g/>
)	)	kIx)	)
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
od	od	k7c2	od
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
až	až	k6eAd1	až
do	do	k7c2	do
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
samců	samec	k1gMnPc2	samec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
67	[number]	k4	67
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
šedookrové	šedookrový	k2eAgNnSc4d1	šedookrový
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnPc4d2	veliký
rozety	rozeta	k1gFnPc4	rozeta
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
<g/>
,	,	kIx,	,
hustou	hustý	k2eAgFnSc4d1	hustá
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
chová	chovat	k5eAaImIp3nS	chovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
tento	tento	k3xDgInSc4	tento
poddruh	poddruh	k1gInSc4	poddruh
chovají	chovat	k5eAaImIp3nP	chovat
Zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Zoo	zoo	k1gNnSc7	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
fusca	fusca	k1gFnSc1	fusca
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Indii	Indie	k1gFnSc6	Indie
od	od	k7c2	od
Himálaje	Himálaj	k1gFnSc2	Himálaj
na	na	k7c6	na
severu	sever	k1gInSc6	sever
až	až	k9	až
po	po	k7c4	po
mys	mys	k1gInSc4	mys
Comorin	Comorin	k1gInSc4	Comorin
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
na	na	k7c4	na
východ	východ	k1gInSc4	východ
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
po	po	k7c4	po
Myanmar	Myanmar	k1gInSc4	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
okrově	okrově	k6eAd1	okrově
žlutě	žlutě	k6eAd1	žlutě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
s	s	k7c7	s
pravidelně	pravidelně	k6eAd1	pravidelně
utvářenými	utvářený	k2eAgFnPc7d1	utvářená
velkými	velký	k2eAgFnPc7d1	velká
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
řídce	řídce	k6eAd1	řídce
rozmístěnými	rozmístěný	k2eAgFnPc7d1	rozmístěná
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Dvůrek	dvůrek	k1gInSc1	dvůrek
rozet	rozeta	k1gFnPc2	rozeta
nebývá	bývat	k5eNaImIp3nS	bývat
výrazně	výrazně	k6eAd1	výrazně
tmavší	tmavý	k2eAgNnSc1d2	tmavší
než	než	k8xS	než
okolní	okolní	k2eAgNnSc1d1	okolní
základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
cejlonský	cejlonský	k2eAgMnSc1d1	cejlonský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
kotiya	kotiya	k1gFnSc1	kotiya
<g/>
)	)	kIx)	)
velký	velký	k2eAgInSc1d1	velký
poddruh	poddruh	k1gInSc1	poddruh
s	s	k7c7	s
tmavším	tmavý	k2eAgNnSc7d2	tmavší
zbarvením	zbarvení	k1gNnSc7	zbarvení
a	a	k8xC	a
malými	malý	k2eAgFnPc7d1	malá
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
chovná	chovný	k2eAgFnSc1d1	chovná
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
ho	on	k3xPp3gMnSc2	on
chová	chovat	k5eAaImIp3nS	chovat
Zoo	zoo	k1gFnSc7	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
indočínský	indočínský	k2eAgMnSc1d1	indočínský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc4	pardus
delacouri	delacour	k1gFnSc2	delacour
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
živě	živě	k6eAd1	živě
okrově	okrově	k6eAd1	okrově
oranžové	oranžový	k2eAgFnPc4d1	oranžová
se	se	k3xPyFc4	se
středně	středně	k6eAd1	středně
velkými	velký	k2eAgFnPc7d1	velká
až	až	k8xS	až
velkými	velký	k2eAgFnPc7d1	velká
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
šíje	šíj	k1gFnSc2	šíj
Kra	kra	k1gFnSc1	kra
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
jedinci	jedinec	k1gMnPc1	jedinec
melaničtí	melanický	k2eAgMnPc1d1	melanický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
jávský	jávský	k2eAgMnSc1d1	jávský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
melas	melasa	k1gFnPc2	melasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malým	malý	k2eAgMnPc3d1	malý
<g/>
,	,	kIx,	,
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
poddruhem	poddruh	k1gInSc7	poddruh
z	z	k7c2	z
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
narudlou	narudlý	k2eAgFnSc4d1	narudlá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
melanismus	melanismus	k1gInSc1	melanismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evolučního	evoluční	k2eAgNnSc2d1	evoluční
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
značně	značně	k6eAd1	značně
odlišný	odlišný	k2eAgInSc4d1	odlišný
poddruh	poddruh	k1gInSc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
japonensis	japonensis	k1gFnSc1	japonensis
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
delší	dlouhý	k2eAgFnSc4d2	delší
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
okrové	okrový	k2eAgNnSc4d1	okrové
až	až	k8xS	až
cihlové	cihlový	k2eAgNnSc4d1	cihlové
zbarvení	zbarvení	k1gNnSc4	zbarvení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
středního	střední	k2eAgInSc2d1	střední
až	až	k8xS	až
většího	veliký	k2eAgInSc2d2	veliký
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Rozety	rozeta	k1gFnPc4	rozeta
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
chován	chovat	k5eAaImNgInS	chovat
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
orientalis	orientalis	k1gFnSc1	orientalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
forma	forma	k1gFnSc1	forma
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
srstí	srst	k1gFnSc7	srst
žijící	žijící	k2eAgFnSc2d1	žijící
Přímořském	přímořský	k2eAgInSc6d1	přímořský
kraji	kraj	k1gInSc6	kraj
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgInSc1d2	světlejší
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nepřevyšuje	převyšovat	k5eNaImIp3nS	převyšovat
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvzácnějších	vzácný	k2eAgFnPc2d3	nejvzácnější
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Zoologické	zoologický	k2eAgFnPc1d1	zoologická
zahrady	zahrada	k1gFnPc1	zahrada
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
záchranu	záchrana	k1gFnSc4	záchrana
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc1d1	dobrá
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
chovem	chov	k1gInSc7	chov
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Regionální	regionální	k2eAgFnPc4d1	regionální
formy	forma	k1gFnPc4	forma
nebo	nebo	k8xC	nebo
předpokládané	předpokládaný	k2eAgFnPc4d1	předpokládaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
<g/>
/	/	kIx~	/
<g/>
zatím	zatím	k6eAd1	zatím
neuznávané	uznávaný	k2eNgInPc1d1	neuznávaný
poddruhy	poddruh	k1gInPc1	poddruh
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
berberský	berberský	k2eAgMnSc1d1	berberský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
panthera	panthera	k1gFnSc1	panthera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
zřejmě	zřejmě	k6eAd1	zřejmě
vyhubená	vyhubený	k2eAgFnSc1d1	vyhubená
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Atlas	Atlas	k1gInSc1	Atlas
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
eritrejský	eritrejský	k2eAgMnSc1d1	eritrejský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
antinorii	antinorie	k1gFnSc4	antinorie
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
Eritreu	Eritrea	k1gFnSc4	Eritrea
a	a	k8xC	a
Etiopii	Etiopie	k1gFnSc4	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
etiopský	etiopský	k2eAgMnSc1d1	etiopský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
adusta	adusta	k1gFnSc1	adusta
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
pohoří	pohoří	k1gNnSc4	pohoří
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Černě	černě	k6eAd1	černě
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
relativně	relativně	k6eAd1	relativně
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc4	pardus
shortridgei	shortridge	k1gFnSc2	shortridge
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
forma	forma	k1gFnSc1	forma
se	s	k7c7	s
žlutooranžovým	žlutooranžový	k2eAgNnSc7d1	žlutooranžové
zbarvením	zbarvení	k1gNnSc7	zbarvení
žijící	žijící	k2eAgMnSc1d1	žijící
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
kapský	kapský	k2eAgMnSc1d1	kapský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
melanotica	melanotica	k1gFnSc1	melanotica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
regionální	regionální	k2eAgFnSc1d1	regionální
forma	forma	k1gFnSc1	forma
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Kapsko	Kapsko	k1gNnSc1	Kapsko
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
konžský	konžský	k2eAgMnSc1d1	konžský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
iturensis	iturensis	k1gFnSc1	iturensis
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
tropické	tropický	k2eAgInPc4d1	tropický
pralesy	prales	k1gInPc4	prales
Konga	Kongo	k1gNnSc2	Kongo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
menšího	malý	k2eAgInSc2d2	menší
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
šedookrové	šedookrový	k2eAgNnSc1d1	šedookrový
až	až	k6eAd1	až
nahnědlé	nahnědlý	k2eAgNnSc1d1	nahnědlé
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
senegalský	senegalský	k2eAgMnSc1d1	senegalský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
leopardus	leopardus	k1gInSc1	leopardus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
pralesních	pralesní	k2eAgFnPc2d1	pralesní
oblastí	oblast	k1gFnPc2	oblast
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Afriky	Afrika	k1gFnSc2	Afrika
od	od	k7c2	od
Senegalu	Senegal	k1gInSc2	Senegal
až	až	k9	až
po	po	k7c4	po
Gabon	Gabon	k1gInSc4	Gabon
<g/>
.	.	kIx.	.
</s>
<s>
Středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
poměrně	poměrně	k6eAd1	poměrně
tmavě	tmavě	k6eAd1	tmavě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
forma	forma	k1gFnSc1	forma
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
somálský	somálský	k2eAgMnSc1d1	somálský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
nanopardus	nanopardus	k1gInSc1	nanopardus
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
Somálsko	Somálsko	k1gNnSc1	Somálsko
a	a	k8xC	a
část	část	k1gFnSc1	část
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
krátkosrstá	krátkosrstý	k2eAgFnSc1d1	krátkosrstá
forma	forma	k1gFnSc1	forma
<g/>
;	;	kIx,	;
základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
světlé	světlý	k2eAgNnSc1d1	světlé
s	s	k7c7	s
převažujícími	převažující	k2eAgInPc7d1	převažující
šedavými	šedavý	k2eAgInPc7d1	šedavý
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
rozety	rozeta	k1gFnPc4	rozeta
malé	malý	k2eAgFnPc4d1	malá
a	a	k8xC	a
hustě	hustě	k6eAd1	hustě
umístěné	umístěný	k2eAgInPc1d1	umístěný
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
súdánský	súdánský	k2eAgMnSc1d1	súdánský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
pardus	pardus	k1gInSc1	pardus
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
za	za	k7c4	za
nominátní	nominátní	k2eAgInSc4d1	nominátní
poddruh	poddruh	k1gInSc4	poddruh
levharta	levhart	k1gMnSc2	levhart
afrického	africký	k2eAgMnSc2d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
ugandský	ugandský	k2eAgMnSc1d1	ugandský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc4	pardus
chui	chu	k1gFnSc2	chu
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
střední	střední	k2eAgFnSc4d1	střední
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Ugandu	Uganda	k1gFnSc4	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
východoafrický	východoafrický	k2eAgMnSc1d1	východoafrický
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
suahelicus	suahelicus	k1gInSc1	suahelicus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
světle	světle	k6eAd1	světle
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
forma	forma	k1gFnSc1	forma
ze	z	k7c2	z
savan	savana	k1gFnPc2	savana
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zařazen	zařazen	k2eAgMnSc1d1	zařazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
zanzibarský	zanzibarský	k2eAgMnSc1d1	zanzibarský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
adersi	aderse	k1gFnSc4	aderse
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
světleji	světle	k6eAd2	světle
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
hustě	hustě	k6eAd1	hustě
rozmístěnými	rozmístěný	k2eAgFnPc7d1	rozmístěná
rozetami	rozeta	k1gFnPc7	rozeta
a	a	k8xC	a
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
západoafrický	západoafrický	k2eAgMnSc1d1	západoafrický
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc4	pardus
reichenowi	reichenow	k1gFnSc2	reichenow
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
západní	západní	k2eAgFnSc3d1	západní
lesnaté	lesnatý	k2eAgFnSc3d1	lesnatá
části	část	k1gFnSc3	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgInSc1d1	africký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
sinajský	sinajský	k2eAgMnSc1d1	sinajský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
jarvisi	jarvise	k1gFnSc4	jarvise
<g/>
)	)	kIx)	)
obýval	obývat	k5eAaImAgInS	obývat
oblast	oblast	k1gFnSc4	oblast
sinajského	sinajský	k2eAgInSc2d1	sinajský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
vyhubený	vyhubený	k2eAgInSc1d1	vyhubený
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
arabský	arabský	k2eAgInSc1d1	arabský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
balúčistánský	balúčistánský	k2eAgMnSc1d1	balúčistánský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
sindica	sindica	k1gFnSc1	sindica
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
oblast	oblast	k1gFnSc1	oblast
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgInSc1d1	perský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
středoasijský	středoasijský	k2eAgMnSc1d1	středoasijský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
tulliana	tulliana	k1gFnSc1	tulliana
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
především	především	k9	především
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgInPc6d1	malý
počtech	počet	k1gInPc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgInSc1d1	perský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
středoperský	středoperský	k2eAgMnSc1d1	středoperský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc4	pardus
datbei	datbe	k1gFnSc2	datbe
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgInSc1d1	perský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
zakavkazský	zakavkazský	k2eAgMnSc1d1	zakavkazský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
ciscaucasica	ciscaucasica	k1gFnSc1	ciscaucasica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
levharta	levhart	k1gMnSc4	levhart
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgInSc1d1	perský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
kašmírský	kašmírský	k2eAgMnSc1d1	kašmírský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc4	pardus
millardi	millard	k1gMnPc1	millard
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
v	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
Kašmíru	Kašmír	k1gInSc2	Kašmír
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
světle	světle	k6eAd1	světle
šedoplavě	šedoplavě	k6eAd1	šedoplavě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Rozety	rozeta	k1gFnPc1	rozeta
jsou	být	k5eAaImIp3nP	být
husté	hustý	k2eAgInPc1d1	hustý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
drobné	drobný	k2eAgFnPc1d1	drobná
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
dvůrkem	dvůrek	k1gInSc7	dvůrek
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
indický	indický	k2eAgMnSc1d1	indický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
nepálský	nepálský	k2eAgMnSc1d1	nepálský
nebo	nebo	k8xC	nebo
levhart	levhart	k1gMnSc1	levhart
tibetský	tibetský	k2eAgMnSc1d1	tibetský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
pernigra	pernigra	k1gFnSc1	pernigra
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
jižní	jižní	k2eAgNnSc4d1	jižní
podhůří	podhůří	k1gNnSc4	podhůří
a	a	k8xC	a
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
část	část	k1gFnSc4	část
Himálaje	Himálaj	k1gFnSc2	Himálaj
(	(	kIx(	(
<g/>
Nepál	Nepál	k1gInSc1	Nepál
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Tibet	Tibet	k1gInSc1	Tibet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
,	,	kIx,	,
zbarvení	zbarvení	k1gNnSc1	zbarvení
matně	matně	k6eAd1	matně
okrově	okrově	k6eAd1	okrově
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
rozety	rozeta	k1gFnPc4	rozeta
velké	velký	k2eAgFnPc4d1	velká
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
husté	hustý	k2eAgInPc1d1	hustý
<g/>
.	.	kIx.	.
</s>
<s>
Řazen	řazen	k2eAgMnSc1d1	řazen
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
indický	indický	k2eAgMnSc1d1	indický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
revize	revize	k1gFnSc1	revize
2017	[number]	k4	2017
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vydali	vydat	k5eAaPmAgMnP	vydat
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
SSC	SSC	kA	SSC
Cat	Cat	k1gFnPc2	Cat
Specialist	Specialist	k1gMnSc1	Specialist
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
odnož	odnož	k1gFnSc4	odnož
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
svazu	svaz	k1gInSc2	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
taxonomickou	taxonomický	k2eAgFnSc4d1	Taxonomická
revizi	revize	k1gFnSc4	revize
recentních	recentní	k2eAgFnPc2d1	recentní
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dělen	dělen	k2eAgInSc4d1	dělen
do	do	k7c2	do
8	[number]	k4	8
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
jsou	být	k5eAaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
jasné	jasný	k2eAgInPc1d1	jasný
a	a	k8xC	a
nezpochybnitelné	zpochybnitelný	k2eNgInPc1d1	nezpochybnitelný
<g/>
:	:	kIx,	:
P.	P.	kA	P.
p.	p.	k?	p.
pardus	pardus	k1gInSc1	pardus
<g/>
,	,	kIx,	,
P.	P.	kA	P.
p.	p.	k?	p.
orientalis	orientalis	k1gInSc1	orientalis
a	a	k8xC	a
P.	P.	kA	P.
p.	p.	k?	p.
melas	melasa	k1gFnPc2	melasa
<g/>
.	.	kIx.	.
</s>
<s>
Konstatována	konstatován	k2eAgFnSc1d1	konstatována
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgFnSc1d1	minimální
molekulární	molekulární	k2eAgFnSc1d1	molekulární
odlišnost	odlišnost	k1gFnSc1	odlišnost
mezi	mezi	k7c7	mezi
kontinentálními	kontinentální	k2eAgInPc7d1	kontinentální
asijskými	asijský	k2eAgInPc7d1	asijský
poddruhy	poddruh	k1gInPc7	poddruh
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
poddruhu	poddruh	k1gInSc2	poddruh
kotyia	kotyium	k1gNnSc2	kotyium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jedinou	jediný	k2eAgFnSc7d1	jediná
subspecií	subspecie	k1gFnSc7	subspecie
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
změna	změna	k1gFnSc1	změna
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c4	v
sloučení	sloučení	k1gNnSc4	sloučení
levharta	levhart	k1gMnSc2	levhart
čínského	čínský	k2eAgMnSc2d1	čínský
a	a	k8xC	a
levharta	levhart	k1gMnSc2	levhart
mandžuského	mandžuský	k2eAgMnSc2d1	mandžuský
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
poddruhu	poddruh	k1gInSc2	poddruh
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc4	pardus
orientalis	orientalis	k1gFnSc2	orientalis
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
studie	studie	k1gFnSc1	studie
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
používání	používání	k1gNnSc4	používání
trinomického	trinomický	k2eAgNnSc2d1	trinomický
jména	jméno	k1gNnSc2	jméno
P.	P.	kA	P.
p.	p.	k?	p.
tulliana	tullian	k1gMnSc2	tullian
pro	pro	k7c4	pro
západoasijské	západoasijský	k2eAgMnPc4d1	západoasijský
levharty	levhart	k1gMnPc4	levhart
místo	místo	k7c2	místo
dosud	dosud	k6eAd1	dosud
užívaného	užívaný	k2eAgInSc2d1	užívaný
P.	P.	kA	P.
p.	p.	k?	p.
saxicolor	saxicolor	k1gInSc1	saxicolor
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
monografie	monografie	k1gFnSc2	monografie
nepovažují	považovat	k5eNaImIp3nP	považovat
výsledky	výsledek	k1gInPc4	výsledek
za	za	k7c4	za
definitivní	definitivní	k2eAgFnSc4d1	definitivní
a	a	k8xC	a
konstatují	konstatovat	k5eAaBmIp3nP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
další	další	k2eAgNnSc4d1	další
zkoumání	zkoumání	k1gNnSc4	zkoumání
i	i	k9	i
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
muzejních	muzejní	k2eAgInPc2d1	muzejní
exponátů	exponát	k1gInPc2	exponát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vyhynulé	vyhynulý	k2eAgInPc1d1	vyhynulý
poddruhy	poddruh	k1gInPc1	poddruh
====	====	k?	====
</s>
</p>
<p>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
begoueni	begouit	k5eAaBmNgMnP	begouit
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
před	před	k7c7	před
cca	cca	kA	cca
2,5	[number]	k4	2,5
až	až	k9	až
1,1	[number]	k4	1,1
milionem	milion	k4xCgInSc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
nalezišť	naleziště	k1gNnPc2	naleziště
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ČR	ČR	kA	ČR
není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
poddruh	poddruh	k1gInSc4	poddruh
P.	P.	kA	P.
p.	p.	k?	p.
sickenbergi	sickenberg	k1gFnSc2	sickenberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
zařazení	zařazení	k1gNnSc1	zařazení
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejisté	jistý	k2eNgNnSc4d1	nejisté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
sickenbergi	sickenbergi	k6eAd1	sickenbergi
žil	žít	k5eAaImAgInS	žít
před	před	k7c7	před
cca	cca	kA	cca
800	[number]	k4	800
000	[number]	k4	000
až	až	k9	až
360	[number]	k4	360
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
několika	několik	k4yIc2	několik
nalezišť	naleziště	k1gNnPc2	naleziště
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
zařazení	zařazení	k1gNnSc1	zařazení
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejisté	jistý	k2eNgNnSc4d1	nejisté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
antiqua	antiqua	k6eAd1	antiqua
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
před	před	k7c7	před
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
až	až	k9	až
126	[number]	k4	126
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Migroval	migrovat	k5eAaImAgMnS	migrovat
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
v	v	k7c6	v
období	období	k1gNnSc6	období
klimatického	klimatický	k2eAgNnSc2d1	klimatické
oteplení	oteplení	k1gNnSc2	oteplení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
nalezišť	naleziště	k1gNnPc2	naleziště
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
zařazení	zařazení	k1gNnSc1	zařazení
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejisté	jistý	k2eNgNnSc4d1	nejisté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
spelaea	spelaea	k1gFnSc1	spelaea
(	(	kIx(	(
<g/>
†	†	k?	†
před	před	k7c4	před
cca	cca	kA	cca
24	[number]	k4	24
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
)	)	kIx)	)
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
období	období	k1gNnSc6	období
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
asi	asi	k9	asi
70	[number]	k4	70
nalezišť	naleziště	k1gNnPc2	naleziště
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
blízký	blízký	k2eAgMnSc1d1	blízký
příbuzný	příbuzný	k1gMnSc1	příbuzný
levharta	levhart	k1gMnSc2	levhart
perského	perský	k2eAgMnSc2d1	perský
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jiné	jiný	k2eAgInPc1d1	jiný
poddruhy	poddruh	k1gInPc1	poddruh
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
podobal	podobat	k5eAaImAgInS	podobat
irbisovi	irbis	k1gMnSc3	irbis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
Panthera	Panther	k1gMnSc2	Panther
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
irbisem	irbis	k1gInSc7	irbis
k	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
nejmenším	malý	k2eAgInPc3d3	nejmenší
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
snoubí	snoubit	k5eAaImIp3nS	snoubit
zdánlivě	zdánlivě	k6eAd1	zdánlivě
protichůdné	protichůdný	k2eAgFnPc4d1	protichůdná
charakteristiky	charakteristika	k1gFnPc4	charakteristika
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
eleganci	elegance	k1gFnSc4	elegance
a	a	k8xC	a
štíhlost	štíhlost	k1gFnSc4	štíhlost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
robustnost	robustnost	k1gFnSc4	robustnost
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Guggisberg	Guggisberg	k1gMnSc1	Guggisberg
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kompilaci	kompilace	k1gFnSc6	kompilace
Wild	Wild	k1gMnSc1	Wild
Cats	Catsa	k1gFnPc2	Catsa
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaPmAgInS	napsat
tuto	tento	k3xDgFnSc4	tento
obdivnou	obdivný	k2eAgFnSc4d1	obdivná
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Levharta	levhart	k1gMnSc2	levhart
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zajisté	zajisté	k9	zajisté
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
nejdokonalejší	dokonalý	k2eAgFnSc4d3	nejdokonalejší
velkou	velký	k2eAgFnSc4d1	velká
kočku	kočka	k1gFnSc4	kočka
<g/>
,	,	kIx,	,
nádhernou	nádherný	k2eAgFnSc4d1	nádherná
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
ladnou	ladný	k2eAgFnSc7d1	ladná
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nohy	noha	k1gFnPc4	noha
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
středně	středně	k6eAd1	středně
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
a	a	k8xC	a
silné	silný	k2eAgNnSc4d1	silné
<g/>
.	.	kIx.	.
</s>
<s>
Tlapy	tlapa	k1gFnPc1	tlapa
jsou	být	k5eAaImIp3nP	být
široké	široký	k2eAgFnPc1d1	široká
a	a	k8xC	a
kulaté	kulatý	k2eAgFnPc1d1	kulatá
a	a	k8xC	a
disponují	disponovat	k5eAaBmIp3nP	disponovat
velkými	velký	k2eAgInPc7d1	velký
zatažitelnými	zatažitelný	k2eAgInPc7d1	zatažitelný
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
irbis	irbis	k1gFnSc1	irbis
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
obláčkový	obláčkový	k2eAgMnSc1d1	obláčkový
a	a	k8xC	a
levhart	levhart	k1gMnSc1	levhart
Diardův	Diardův	k2eAgMnSc1d1	Diardův
ho	on	k3xPp3gNnSc4	on
mají	mít	k5eAaImIp3nP	mít
absolutně	absolutně	k6eAd1	absolutně
i	i	k9	i
relativně	relativně	k6eAd1	relativně
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
se	se	k3xPyFc4	se
levhart	levhart	k1gMnSc1	levhart
nejvíce	hodně	k6eAd3	hodně
podobá	podobat	k5eAaImIp3nS	podobat
jaguárovi	jaguár	k1gMnSc3	jaguár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
subtilnější	subtilní	k2eAgFnSc1d2	subtilnější
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnPc4d2	menší
rozety	rozeta	k1gFnPc4	rozeta
a	a	k8xC	a
delší	dlouhý	k2eAgInSc4d2	delší
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozměry	rozměr	k1gInPc4	rozměr
===	===	k?	===
</s>
</p>
<p>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
levharti	levhart	k1gMnPc1	levhart
váží	vážit	k5eAaImIp3nP	vážit
obvykle	obvykle	k6eAd1	obvykle
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc4	tělo
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
po	po	k7c4	po
konec	konec	k1gInSc4	konec
ocasu	ocas	k1gInSc2	ocas
měří	měřit	k5eAaImIp3nS	měřit
140	[number]	k4	140
<g/>
–	–	k?	–
<g/>
240	[number]	k4	240
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
ocas	ocas	k1gInSc4	ocas
připadá	připadat	k5eAaImIp3nS	připadat
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
cm	cm	kA	cm
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
82	[number]	k4	82
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
velcí	velký	k2eAgMnPc1d1	velký
jedinci	jedinec	k1gMnPc1	jedinec
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
okolo	okolo	k7c2	okolo
270	[number]	k4	270
cm	cm	kA	cm
a	a	k8xC	a
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
kus	kus	k1gInSc1	kus
měřil	měřit	k5eAaImAgInS	měřit
292	[number]	k4	292
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
velmi	velmi	k6eAd1	velmi
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
poddruhu	poddruh	k1gInSc6	poddruh
respektive	respektive	k9	respektive
na	na	k7c6	na
regionálních	regionální	k2eAgFnPc6d1	regionální
formách	forma	k1gFnPc6	forma
a	a	k8xC	a
habitatu	habitat	k1gInSc6	habitat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
jsou	být	k5eAaImIp3nP	být
levharti	levhart	k1gMnPc1	levhart
perští	perský	k2eAgMnPc1d1	perský
(	(	kIx(	(
<g/>
až	až	k9	až
91	[number]	k4	91
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cejlonští	cejlonský	k2eAgMnPc1d1	cejlonský
(	(	kIx(	(
<g/>
až	až	k9	až
77	[number]	k4	77
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
regionální	regionální	k2eAgFnSc2d1	regionální
populace	populace	k1gFnSc2	populace
levharta	levhart	k1gMnSc2	levhart
afrického	africký	k2eAgMnSc2d1	africký
(	(	kIx(	(
<g/>
až	až	k8xS	až
96	[number]	k4	96
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
indického	indický	k2eAgMnSc2d1	indický
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
70	[number]	k4	70
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejmenší	malý	k2eAgFnSc4d3	nejmenší
bývají	bývat	k5eAaImIp3nP	bývat
levharti	levhart	k1gMnPc1	levhart
arabští	arabský	k2eAgMnPc1d1	arabský
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jávští	jávský	k2eAgMnPc1d1	jávský
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
levharti	levhart	k1gMnPc1	levhart
afričtí	africký	k2eAgMnPc1d1	africký
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Kapsko	Kapsko	k1gNnSc1	Kapsko
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
levhartích	levhartí	k2eAgInPc2d1	levhartí
poddruhů	poddruh	k1gInPc2	poddruh
platí	platit	k5eAaImIp3nS	platit
Bergmannovo	Bergmannův	k2eAgNnSc1d1	Bergmannovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
jen	jen	k9	jen
zčásti	zčásti	k6eAd1	zčásti
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
například	například	k6eAd1	například
levhart	levhart	k1gMnSc1	levhart
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
žijící	žijící	k2eAgFnSc4d1	žijící
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
oblastech	oblast	k1gFnPc6	oblast
dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
průměrné	průměrný	k2eAgFnSc3d1	průměrná
velikosti	velikost	k1gFnSc3	velikost
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
divoký	divoký	k2eAgMnSc1d1	divoký
leopard	leopard	k1gMnSc1	leopard
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgMnSc1	jaký
byl	být	k5eAaImAgInS	být
kdy	kdy	k6eAd1	kdy
zvážen	zvážit	k5eAaPmNgInS	zvážit
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
uváděn	uvádět	k5eAaImNgInS	uvádět
96	[number]	k4	96
kg	kg	kA	kg
těžký	těžký	k2eAgMnSc1d1	těžký
jedinec	jedinec	k1gMnSc1	jedinec
z	z	k7c2	z
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
samec	samec	k1gInSc1	samec
levharta	levhart	k1gMnSc2	levhart
indického	indický	k2eAgMnSc2d1	indický
jménem	jméno	k1gNnSc7	jméno
Balaji	Balaj	k1gMnSc6	Balaj
vážil	vážit	k5eAaImAgMnS	vážit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
v	v	k7c6	v
době	doba	k1gFnSc6	doba
odchytu	odchyt	k1gInSc2	odchyt
pro	pro	k7c4	pro
zoologickou	zoologický	k2eAgFnSc4d1	zoologická
zahradu	zahrada	k1gFnSc4	zahrada
Tirupati	Tirupati	k1gFnSc1	Tirupati
108	[number]	k4	108
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
byl	být	k5eAaImAgInS	být
vykrmen	vykrmit	k5eAaPmNgInS	vykrmit
do	do	k7c2	do
obezity	obezita	k1gFnSc2	obezita
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
přibral	přibrat	k5eAaPmAgMnS	přibrat
na	na	k7c4	na
konečných	konečný	k2eAgInPc2d1	konečný
143	[number]	k4	143
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
podstatně	podstatně	k6eAd1	podstatně
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
patrný	patrný	k2eAgInSc1d1	patrný
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
samců	samec	k1gMnPc2	samec
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
o	o	k7c4	o
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
o	o	k7c4	o
100	[number]	k4	100
%	%	kIx~	%
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
hmotnost	hmotnost	k1gFnSc1	hmotnost
samic	samice	k1gFnPc2	samice
stanovená	stanovený	k2eAgFnSc1d1	stanovená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
měření	měření	k1gNnSc1	měření
34	[number]	k4	34
jedinců	jedinec	k1gMnPc2	jedinec
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
afrických	africký	k2eAgFnPc2d1	africká
populací	populace	k1gFnPc2	populace
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
oscilovala	oscilovat	k5eAaImAgFnS	oscilovat
mezi	mezi	k7c7	mezi
21,2	[number]	k4	21,2
a	a	k8xC	a
54	[number]	k4	54
kg	kg	kA	kg
<g/>
,	,	kIx,	,
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
při	při	k7c6	při
počtu	počet	k1gInSc6	počet
47	[number]	k4	47
zvážených	zvážený	k2eAgInPc2d1	zvážený
kusů	kus	k1gInPc2	kus
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
30,9	[number]	k4	30,9
až	až	k9	až
62,6	[number]	k4	62,6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlava	hlava	k1gFnSc1	hlava
===	===	k?	===
</s>
</p>
<p>
<s>
Hlava	hlava	k1gFnSc1	hlava
samic	samice	k1gFnPc2	samice
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
štíhlejší	štíhlý	k2eAgInSc1d2	štíhlejší
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
téměř	téměř	k6eAd1	téměř
gracilní	gracilní	k2eAgMnSc1d1	gracilní
(	(	kIx(	(
<g/>
levhart	levhart	k1gMnSc1	levhart
arabský	arabský	k2eAgMnSc1d1	arabský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samců	samec	k1gInPc2	samec
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
robustní	robustní	k2eAgFnSc1d1	robustní
až	až	k8xS	až
masivní	masivní	k2eAgFnSc1d1	masivní
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
lebky	lebka	k1gFnSc2	lebka
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
180	[number]	k4	180
do	do	k7c2	do
260	[number]	k4	260
mm	mm	kA	mm
a	a	k8xC	a
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
od	od	k7c2	od
160	[number]	k4	160
do	do	k7c2	do
218	[number]	k4	218
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
změřená	změřený	k2eAgFnSc1d1	změřená
lebka	lebka	k1gFnSc1	lebka
patřila	patřit	k5eAaImAgFnS	patřit
samci	samec	k1gInSc3	samec
perského	perský	k2eAgMnSc2d1	perský
levharta	levhart	k1gMnSc2	levhart
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
maximální	maximální	k2eAgFnSc4d1	maximální
délku	délka	k1gFnSc4	délka
281	[number]	k4	281
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Lebky	lebka	k1gFnPc1	lebka
samců	samec	k1gMnPc2	samec
mají	mít	k5eAaImIp3nP	mít
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
sagitální	sagitální	k2eAgInSc4d1	sagitální
hřeben	hřeben	k1gInSc4	hřeben
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
obvykle	obvykle	k6eAd1	obvykle
téměř	téměř	k6eAd1	téměř
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zubů	zub	k1gInPc2	zub
mají	mít	k5eAaImIp3nP	mít
levharti	levhart	k1gMnPc1	levhart
stejný	stejný	k2eAgMnSc1d1	stejný
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Felidae	Felidae	k1gFnSc1	Felidae
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
chybět	chybět	k5eAaImF	chybět
první	první	k4xOgInSc4	první
horní	horní	k2eAgInSc4d1	horní
třenový	třenový	k2eAgInSc4d1	třenový
zub	zub	k1gInSc4	zub
či	či	k8xC	či
horní	horní	k2eAgFnSc1d1	horní
stolička	stolička	k1gFnSc1	stolička
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
kolísat	kolísat	k5eAaImF	kolísat
od	od	k7c2	od
26	[number]	k4	26
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
30	[number]	k4	30
zubům	zub	k1gInPc3	zub
je	on	k3xPp3gMnPc4	on
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
3	[number]	k4	3
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
U	u	k7c2	u
mléčného	mléčný	k2eAgInSc2d1	mléčný
chrupu	chrup	k1gInSc2	chrup
chybí	chybit	k5eAaPmIp3nP	chybit
stoličky	stolička	k1gFnPc1	stolička
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
počet	počet	k1gInSc1	počet
zubů	zub	k1gInPc2	zub
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Špičáky	špičák	k1gInPc1	špičák
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
masivní	masivní	k2eAgFnSc4d1	masivní
s	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
základnou	základna	k1gFnSc7	základna
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
minimalizována	minimalizován	k2eAgFnSc1d1	minimalizována
možnost	možnost	k1gFnSc1	možnost
ulomení	ulomení	k1gNnSc2	ulomení
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
z	z	k7c2	z
dásně	dáseň	k1gFnSc2	dáseň
vyčuhující	vyčuhující	k2eAgFnSc2d1	vyčuhující
části	část	k1gFnSc2	část
horního	horní	k2eAgInSc2d1	horní
špičáku	špičák	k1gInSc2	špičák
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
37	[number]	k4	37
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ostré	ostrý	k2eAgFnSc3d1	ostrá
zpětně	zpětně	k6eAd1	zpětně
tvarované	tvarovaný	k2eAgFnPc4d1	tvarovaná
papily	papila	k1gFnPc4	papila
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
oddělování	oddělování	k1gNnSc3	oddělování
masa	maso	k1gNnSc2	maso
kořisti	kořist	k1gFnSc2	kořist
od	od	k7c2	od
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Srst	srst	k1gFnSc4	srst
===	===	k?	===
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
žlutavé	žlutavý	k2eAgNnSc1d1	žlutavé
<g/>
,	,	kIx,	,
okrové	okrový	k2eAgFnPc1d1	okrová
<g/>
,	,	kIx,	,
žlutošedé	žlutošedý	k2eAgFnPc1d1	žlutošedá
<g/>
,	,	kIx,	,
žlutooranžové	žlutooranžový	k2eAgFnPc1d1	žlutooranžová
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
velmi	velmi	k6eAd1	velmi
tmavě	tmavě	k6eAd1	tmavě
oranžové	oranžový	k2eAgFnPc1d1	oranžová
či	či	k8xC	či
cihlové	cihlový	k2eAgFnPc1d1	cihlová
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
nohou	noha	k1gFnPc2	noha
jsou	být	k5eAaImIp3nP	být
světlé	světlý	k2eAgInPc1d1	světlý
až	až	k9	až
téměř	téměř	k6eAd1	téměř
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
kresba	kresba	k1gFnSc1	kresba
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
od	od	k7c2	od
drobných	drobný	k2eAgFnPc2d1	drobná
okrouhlých	okrouhlý	k2eAgFnPc2d1	okrouhlá
a	a	k8xC	a
oválných	oválný	k2eAgFnPc2d1	oválná
skvrn	skvrna	k1gFnPc2	skvrna
až	až	k9	až
po	po	k7c4	po
tmavé	tmavý	k2eAgFnPc4d1	tmavá
rozety	rozeta	k1gFnPc4	rozeta
bez	bez	k7c2	bez
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Skvrny	skvrna	k1gFnPc1	skvrna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
spodních	spodní	k2eAgFnPc6d1	spodní
částech	část	k1gFnPc6	část
nohou	noha	k1gFnPc6	noha
a	a	k8xC	a
ocase	ocas	k1gInSc6	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
bocích	bok	k1gInPc6	bok
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
stehnech	stehno	k1gNnPc6	stehno
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
obvykle	obvykle	k6eAd1	obvykle
prázdné	prázdný	k2eAgFnPc1d1	prázdná
rozety	rozeta	k1gFnPc1	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
indočínský	indočínský	k2eAgMnSc1d1	indočínský
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
indický	indický	k2eAgMnSc1d1	indický
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
malé	malý	k2eAgFnPc4d1	malá
skvrnky	skvrnka	k1gFnPc4	skvrnka
uvnitř	uvnitř	k7c2	uvnitř
rozet	rozeta	k1gFnPc2	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Odstín	odstín	k1gInSc1	odstín
i	i	k8xC	i
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
skvrn	skvrna	k1gFnPc2	skvrna
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
geografických	geografický	k2eAgInPc2d1	geografický
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
unikátní	unikátní	k2eAgFnSc4d1	unikátní
kresbu	kresba	k1gFnSc4	kresba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
levhartím	levhartí	k2eAgInSc7d1	levhartí
otiskem	otisk	k1gInSc7	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
uvnitř	uvnitř	k7c2	uvnitř
rozet	rozeta	k1gFnPc2	rozeta
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
barva	barva	k1gFnSc1	barva
jejich	jejich	k3xOp3gNnSc2	jejich
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
<g/>
Relativně	relativně	k6eAd1	relativně
pestré	pestrý	k2eAgNnSc1d1	pestré
zbarvení	zbarvení	k1gNnSc1	zbarvení
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
účinné	účinný	k2eAgNnSc1d1	účinné
maskování	maskování	k1gNnSc1	maskování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
somatolýzu	somatolýza	k1gFnSc4	somatolýza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jakési	jakýsi	k3yIgNnSc1	jakýsi
rozostření	rozostření	k1gNnSc1	rozostření
či	či	k8xC	či
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
obrysů	obrys	k1gInPc2	obrys
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
travním	travní	k2eAgInSc6d1	travní
podrostu	podrost	k1gInSc6	podrost
<g/>
,	,	kIx,	,
polopouštním	polopouštní	k2eAgNnSc6d1	polopouštní
<g/>
,	,	kIx,	,
křovinatém	křovinatý	k2eAgMnSc6d1	křovinatý
a	a	k8xC	a
lesním	lesní	k2eAgInSc6d1	lesní
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Rudyard	Rudyard	k1gMnSc1	Rudyard
Kipling	Kipling	k1gInSc4	Kipling
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
levhart	levhart	k1gMnSc1	levhart
získal	získat	k5eAaPmAgMnS	získat
své	svůj	k3xOyFgFnPc4	svůj
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Bajky	bajka	k1gFnPc4	bajka
i	i	k8xC	i
nebajky	nebajka	k1gFnPc4	nebajka
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
Nyní	nyní	k6eAd1	nyní
jsi	být	k5eAaImIp2nS	být
krasavec	krasavec	k1gMnSc1	krasavec
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Etiopan	Etiopan	k1gMnSc1	Etiopan
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Můžeš	moct	k5eAaImIp2nS	moct
ležet	ležet	k5eAaImF	ležet
venku	venku	k6eAd1	venku
na	na	k7c6	na
suché	suchý	k2eAgFnSc6d1	suchá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
budeš	být	k5eAaImBp2nS	být
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
hromada	hromada	k1gFnSc1	hromada
oblázků	oblázek	k1gInPc2	oblázek
<g/>
.	.	kIx.	.
</s>
<s>
Můžeš	moct	k5eAaImIp2nS	moct
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
holé	holý	k2eAgFnSc6d1	holá
skále	skála	k1gFnSc6	skála
a	a	k8xC	a
budeš	být	k5eAaImBp2nS	být
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
kus	kus	k1gInSc4	kus
slepence	slepenec	k1gInSc2	slepenec
<g/>
.	.	kIx.	.
</s>
<s>
Můžeš	moct	k5eAaImIp2nS	moct
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
a	a	k8xC	a
budeš	být	k5eAaImBp2nS	být
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
procházející	procházející	k2eAgInSc1d1	procházející
skrz	skrz	k7c4	skrz
listy	list	k1gInPc4	list
<g/>
;	;	kIx,	;
a	a	k8xC	a
můžeš	moct	k5eAaImIp2nS	moct
ležet	ležet	k5eAaImF	ležet
přímo	přímo	k6eAd1	přímo
uprostřed	uprostřed	k7c2	uprostřed
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
nebudeš	být	k5eNaImBp2nS	být
připomínat	připomínat	k5eAaImF	připomínat
nic	nic	k3yNnSc1	nic
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
Délka	délka	k1gFnSc1	délka
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
v	v	k7c6	v
teplých	teplý	k2eAgInPc6d1	teplý
regionech	region	k1gInPc6	region
měří	měřit	k5eAaImIp3nP	měřit
chlupy	chlup	k1gInPc1	chlup
většinou	většinou	k6eAd1	většinou
1,5	[number]	k4	1,5
až	až	k9	až
2,5	[number]	k4	2,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
a	a	k8xC	a
nejhustší	hustý	k2eAgFnSc4d3	nejhustší
srst	srst	k1gFnSc4	srst
mívají	mívat	k5eAaImIp3nP	mívat
levharti	levhart	k1gMnPc1	levhart
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
tedy	tedy	k9	tedy
levharti	levhart	k1gMnPc1	levhart
perští	perský	k2eAgMnPc1d1	perský
<g/>
,	,	kIx,	,
mandžuští	mandžuský	k2eAgMnPc1d1	mandžuský
a	a	k8xC	a
indičtí	indický	k2eAgMnPc1d1	indický
z	z	k7c2	z
podhůří	podhůří	k1gNnPc2	podhůří
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
může	moct	k5eAaImIp3nS	moct
délka	délka	k1gFnSc1	délka
zimní	zimní	k2eAgFnSc1d1	zimní
srsti	srst	k1gFnSc3	srst
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
přes	přes	k7c4	přes
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chlupů	chlup	k1gInPc2	chlup
na	na	k7c4	na
1	[number]	k4	1
cm2	cm2	k4	cm2
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2000	[number]	k4	2000
do	do	k7c2	do
3000	[number]	k4	3000
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
u	u	k7c2	u
irbisa	irbis	k1gMnSc2	irbis
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
4000	[number]	k4	4000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
pesíkových	pesíkový	k2eAgInPc2d1	pesíkový
chlupů	chlup	k1gInPc2	chlup
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
chlupů	chlup	k1gInPc2	chlup
podsady	podsada	k1gFnSc2	podsada
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgInPc1d1	Černé
chlupy	chlup	k1gInPc1	chlup
bývají	bývat	k5eAaImIp3nP	bývat
delší	dlouhý	k2eAgInPc1d2	delší
a	a	k8xC	a
jemnější	jemný	k2eAgInPc1d2	jemnější
než	než	k8xS	než
chlupy	chlup	k1gInPc1	chlup
světlé	světlý	k2eAgInPc1d1	světlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barevné	barevný	k2eAgFnPc4d1	barevná
odchylky	odchylka	k1gFnPc4	odchylka
===	===	k?	===
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
nezvykle	zvykle	k6eNd1	zvykle
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
levharti	levhart	k1gMnPc1	levhart
představují	představovat	k5eAaImIp3nP	představovat
pouhou	pouhý	k2eAgFnSc4d1	pouhá
barevnou	barevný	k2eAgFnSc4d1	barevná
odchylku	odchylka	k1gFnSc4	odchylka
či	či	k8xC	či
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejde	jít	k5eNaImIp3nS	jít
tudíž	tudíž	k8xC	tudíž
o	o	k7c4	o
samostatné	samostatný	k2eAgInPc4d1	samostatný
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Barevnými	barevný	k2eAgFnPc7d1	barevná
odchylkami	odchylka	k1gFnPc7	odchylka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
melanismus	melanismus	k1gInSc1	melanismus
<g/>
,	,	kIx,	,
abundismus	abundismus	k1gInSc1	abundismus
<g/>
,	,	kIx,	,
erytrismus	erytrismus	k1gInSc1	erytrismus
a	a	k8xC	a
albinismus	albinismus	k1gInSc1	albinismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Melanismus	Melanismus	k1gInSc4	Melanismus
====	====	k?	====
</s>
</p>
<p>
<s>
Doposud	doposud	k6eAd1	doposud
byl	být	k5eAaImAgInS	být
melanismus	melanismus	k1gInSc1	melanismus
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
černé	černý	k2eAgNnSc1d1	černé
zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
zaznamenán	zaznamenán	k2eAgInSc4d1	zaznamenán
u	u	k7c2	u
13	[number]	k4	13
z	z	k7c2	z
37	[number]	k4	37
druhů	druh	k1gInPc2	druh
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
levhartů	levhart	k1gMnPc2	levhart
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
zřejmě	zřejmě	k6eAd1	zřejmě
způsoben	způsobit	k5eAaPmNgInS	způsobit
mutací	mutace	k1gFnSc7	mutace
genu	gen	k1gInSc2	gen
Agouti	Agouť	k1gFnSc2	Agouť
Signaling	Signaling	k1gInSc1	Signaling
Protein	protein	k1gInSc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autozomálně	autozomálně	k6eAd1	autozomálně
recesivní	recesivní	k2eAgFnSc1d1	recesivní
(	(	kIx(	(
<g/>
u	u	k7c2	u
jaguárů	jaguár	k1gMnPc2	jaguár
naopak	naopak	k6eAd1	naopak
autozomálně	autozomálně	k6eAd1	autozomálně
dominantní	dominantní	k2eAgFnSc1d1	dominantní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
dva	dva	k4xCgMnPc1	dva
skvrnití	skvrnitý	k2eAgMnPc1d1	skvrnitý
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
nositeli	nositel	k1gMnPc7	nositel
genu	gen	k1gInSc2	gen
melanismu	melanismus	k1gInSc2	melanismus
po	po	k7c6	po
rodičích	rodič	k1gMnPc6	rodič
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
černého	černý	k2eAgMnSc4d1	černý
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
melanismus	melanismus	k1gInSc1	melanismus
poměrně	poměrně	k6eAd1	poměrně
běžný	běžný	k2eAgInSc1d1	běžný
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
černé	černý	k2eAgMnPc4d1	černý
jedince	jedinec	k1gMnPc4	jedinec
najdeme	najít	k5eAaPmIp1nP	najít
vzácně	vzácně	k6eAd1	vzácně
(	(	kIx(	(
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
(	(	kIx(	(
<g/>
Arabský	arabský	k2eAgInSc1d1	arabský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
Dálný	dálný	k2eAgInSc1d1	dálný
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
šíje	šíj	k1gFnSc2	šíj
Kra	kra	k1gFnSc1	kra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
černí	černý	k2eAgMnPc1d1	černý
levharti	levhart	k1gMnPc1	levhart
dominantní	dominantní	k2eAgMnPc1d1	dominantní
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
běžně	běžně	k6eAd1	běžně
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
vzácní	vzácný	k2eAgMnPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Melaničtí	Melanický	k2eAgMnPc1d1	Melanický
jedinci	jedinec	k1gMnPc1	jedinec
zvaní	zvaný	k2eAgMnPc1d1	zvaný
též	též	k6eAd1	též
černí	černý	k2eAgMnPc1d1	černý
panteři	panter	k1gMnPc1	panter
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
kříží	křížit	k5eAaImIp3nP	křížit
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
příslušníky	příslušník	k1gMnPc7	příslušník
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
černá	černý	k2eAgFnSc1d1	černá
i	i	k8xC	i
skvrnitá	skvrnitý	k2eAgNnPc1d1	skvrnité
koťata	kotě	k1gNnPc1	kotě
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
zbarvení	zbarvení	k1gNnSc4	zbarvení
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
melanické	melanický	k2eAgFnPc1d1	melanická
samice	samice	k1gFnPc1	samice
mívají	mívat	k5eAaImIp3nP	mívat
nižší	nízký	k2eAgInSc4d2	nižší
počet	počet	k1gInSc4	počet
mláďat	mládě	k1gNnPc2	mládě
ve	v	k7c6	v
vrhu	vrh	k1gInSc6	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
má	můj	k3xOp1gFnSc1	můj
srst	srst	k1gFnSc1	srst
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
pod	pod	k7c7	pod
určitým	určitý	k2eAgInSc7d1	určitý
úhlem	úhel	k1gInSc7	úhel
viditelné	viditelný	k2eAgFnSc2d1	viditelná
skvrny	skvrna	k1gFnSc2	skvrna
a	a	k8xC	a
rozety	rozeta	k1gFnSc2	rozeta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
odstín	odstín	k1gInSc1	odstín
černé	černá	k1gFnSc2	černá
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
tmavší	tmavý	k2eAgFnSc1d2	tmavší
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
melanismu	melanismus	k1gInSc2	melanismus
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druh	druh	k1gInSc4	druh
kamufláže	kamufláž	k1gFnSc2	kamufláž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c4	na
tmavší	tmavý	k2eAgNnSc4d2	tmavší
prostředí	prostředí	k1gNnSc4	prostředí
pralesa	prales	k1gInSc2	prales
(	(	kIx(	(
<g/>
88	[number]	k4	88
%	%	kIx~	%
melanických	melanický	k2eAgMnPc2d1	melanický
jedinců	jedinec	k1gMnPc2	jedinec
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
deštném	deštný	k2eAgInSc6d1	deštný
lese	les	k1gInSc6	les
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
o	o	k7c4	o
odpověď	odpověď	k1gFnSc4	odpověď
organismu	organismus	k1gInSc2	organismus
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
regionální	regionální	k2eAgInPc1d1	regionální
formy	forma	k1gFnPc4	forma
levharta	levhart	k1gMnSc2	levhart
prošly	projít	k5eAaPmAgFnP	projít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Abundismus	Abundismus	k1gInSc4	Abundismus
====	====	k?	====
</s>
</p>
<p>
<s>
Abundismus	Abundismus	k1gInSc1	Abundismus
či	či	k8xC	či
někdy	někdy	k6eAd1	někdy
též	též	k9	též
pseudomelanismus	pseudomelanismus	k1gInSc1	pseudomelanismus
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
změnou	změna	k1gFnSc7	změna
kresby	kresba	k1gFnSc2	kresba
skvrn	skvrna	k1gFnPc2	skvrna
a	a	k8xC	a
rozet	rozeta	k1gFnPc2	rozeta
do	do	k7c2	do
atypických	atypický	k2eAgInPc2d1	atypický
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
levharti	levhart	k1gMnPc1	levhart
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
hustěji	husto	k6eAd2	husto
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
skvrny	skvrna	k1gFnSc2	skvrna
propojené	propojený	k2eAgFnPc1d1	propojená
často	často	k6eAd1	často
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
a	a	k8xC	a
obrazců	obrazec	k1gInPc2	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Abundismus	Abundismus	k1gInSc1	Abundismus
je	být	k5eAaImIp3nS	být
vzácnější	vzácný	k2eAgInSc1d2	vzácnější
než	než	k8xS	než
melanismus	melanismus	k1gInSc1	melanismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
takto	takto	k6eAd1	takto
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k9	jako
levharti	levhart	k1gMnPc1	levhart
královští	královský	k2eAgMnPc1d1	královský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Erytrismus	Erytrismus	k1gInSc1	Erytrismus
<g/>
,	,	kIx,	,
albinismus	albinismus	k1gInSc1	albinismus
====	====	k?	====
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
melanismu	melanismus	k1gInSc2	melanismus
a	a	k8xC	a
abundismu	abundismus	k1gInSc2	abundismus
se	se	k3xPyFc4	se
vzácně	vzácně	k6eAd1	vzácně
u	u	k7c2	u
levhartů	levhart	k1gMnPc2	levhart
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
erytrismus	erytrismus	k1gInSc4	erytrismus
a	a	k8xC	a
albinismus	albinismus	k1gInSc4	albinismus
<g/>
.	.	kIx.	.
</s>
<s>
Erytričtí	Erytrický	k2eAgMnPc1d1	Erytrický
neboli	neboli	k8xC	neboli
červení	červený	k2eAgMnPc1d1	červený
levharti	levhart	k1gMnPc1	levhart
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
červenohnědou	červenohnědý	k2eAgFnSc7d1	červenohnědá
barvou	barva	k1gFnSc7	barva
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dvě	dva	k4xCgFnPc4	dva
podoby	podoba	k1gFnPc4	podoba
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
červenohnědě	červenohnědě	k6eAd1	červenohnědě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
všechna	všechen	k3xTgFnSc1	všechen
srst	srst	k1gFnSc1	srst
anebo	anebo	k8xC	anebo
jen	jen	k9	jen
skvrny	skvrna	k1gFnPc1	skvrna
a	a	k8xC	a
rozety	rozeta	k1gFnPc1	rozeta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
mimo	mimo	k7c4	mimo
rozety	rozeta	k1gFnPc4	rozeta
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
relativně	relativně	k6eAd1	relativně
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
extrémně	extrémně	k6eAd1	extrémně
vzácní	vzácný	k2eAgMnPc1d1	vzácný
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
regionu	region	k1gInSc6	region
Lydenburg	Lydenburg	k1gInSc1	Lydenburg
(	(	kIx(	(
<g/>
Mpumalanga	Mpumalanga	k1gFnSc1	Mpumalanga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zbarveným	zbarvený	k2eAgMnPc3d1	zbarvený
jedincům	jedinec	k1gMnPc3	jedinec
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
růžoví	růžový	k2eAgMnPc1d1	růžový
panteři	panter	k1gMnPc1	panter
či	či	k8xC	či
jahodoví	jahodový	k2eAgMnPc1d1	jahodový
levharti	levhart	k1gMnPc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
tohoto	tento	k3xDgNnSc2	tento
zbarvení	zbarvení	k1gNnSc2	zbarvení
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
neznámé	známý	k2eNgFnPc1d1	neznámá
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
<g/>
.	.	kIx.	.
<g/>
Albinismus	albinismus	k1gInSc1	albinismus
či	či	k8xC	či
částečný	částečný	k2eAgInSc1d1	částečný
albinismus	albinismus	k1gInSc1	albinismus
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
levhartů	levhart	k1gMnPc2	levhart
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
částí	část	k1gFnPc2	část
areálu	areál	k1gInSc2	areál
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
melanických	melanický	k2eAgMnPc2d1	melanický
<g/>
,	,	kIx,	,
abundických	abundický	k2eAgMnPc2d1	abundický
a	a	k8xC	a
erytrických	erytrický	k2eAgMnPc2d1	erytrický
jedinců	jedinec	k1gMnPc2	jedinec
mají	mít	k5eAaImIp3nP	mít
albíni	albín	k1gMnPc1	albín
nižší	nízký	k2eAgFnSc4d2	nižší
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bílá	bílý	k2eAgFnSc1d1	bílá
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
činí	činit	k5eAaImIp3nP	činit
nápadnými	nápadný	k2eAgMnPc7d1	nápadný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
lov	lov	k1gInSc1	lov
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
samotné	samotný	k2eAgNnSc1d1	samotné
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
puškám	puška	k1gFnPc3	puška
lovců	lovec	k1gMnPc2	lovec
jako	jako	k9	jako
dobře	dobře	k6eAd1	dobře
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
a	a	k8xC	a
žádoucí	žádoucí	k2eAgFnSc4d1	žádoucí
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smysly	smysl	k1gInPc4	smysl
a	a	k8xC	a
výkony	výkon	k1gInPc4	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
je	být	k5eAaImIp3nS	být
univerzálně	univerzálně	k6eAd1	univerzálně
stavěná	stavěný	k2eAgFnSc1d1	stavěná
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
se	s	k7c7	s
zrakem	zrak	k1gInSc7	zrak
přizpůsobeným	přizpůsobený	k2eAgInSc7d1	přizpůsobený
k	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
zorném	zorný	k2eAgNnSc6d1	zorné
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
vidění	vidění	k1gNnSc4	vidění
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
rozlišování	rozlišování	k1gNnSc1	rozlišování
barev	barva	k1gFnPc2	barva
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nehraje	hrát	k5eNaImIp3nS	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
sluch	sluch	k1gInSc1	sluch
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zachytit	zachytit	k5eAaPmF	zachytit
cca	cca	kA	cca
pětkrát	pětkrát	k6eAd1	pětkrát
vyšší	vysoký	k2eAgFnSc4d2	vyšší
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
frekvenci	frekvence	k1gFnSc4	frekvence
než	než	k8xS	než
sluch	sluch	k1gInSc4	sluch
lidský	lidský	k2eAgInSc4d1	lidský
(	(	kIx(	(
<g/>
až	až	k9	až
100	[number]	k4	100
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čich	čich	k1gInSc1	čich
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
jiných	jiný	k2eAgFnPc2d1	jiná
čeledí	čeleď	k1gFnPc2	čeleď
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
získání	získání	k1gNnSc3	získání
hmatových	hmatový	k2eAgInPc2d1	hmatový
vjemů	vjem	k1gInPc2	vjem
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k9	především
vousky	vouska	k1gFnPc1	vouska
na	na	k7c6	na
čumáku	čumák	k1gInSc6	čumák
–	–	k?	–
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
orientovat	orientovat	k5eAaBmF	orientovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
stísněném	stísněný	k2eAgInSc6d1	stísněný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
informují	informovat	k5eAaBmIp3nP	informovat
šelmu	šelma	k1gFnSc4	šelma
o	o	k7c6	o
přesné	přesný	k2eAgFnSc6d1	přesná
poloze	poloha	k1gFnSc6	poloha
kořisti	kořist	k1gFnSc2	kořist
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc2	jejich
zranitelných	zranitelný	k2eAgFnPc2d1	zranitelná
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
se	se	k3xPyFc4	se
umí	umět	k5eAaImIp3nS	umět
pohybovat	pohybovat	k5eAaImF	pohybovat
velmi	velmi	k6eAd1	velmi
mrštně	mrštně	k6eAd1	mrštně
a	a	k8xC	a
neslyšně	slyšně	k6eNd1	slyšně
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
kočky	kočka	k1gFnPc1	kočka
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
čelistmi	čelist	k1gFnPc7	čelist
a	a	k8xC	a
ostrými	ostrý	k2eAgInPc7d1	ostrý
zatažitelnými	zatažitelný	k2eAgInPc7d1	zatažitelný
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
dobře	dobře	k6eAd1	dobře
běhat	běhat	k5eAaImF	běhat
<g/>
,	,	kIx,	,
skákat	skákat	k5eAaImF	skákat
a	a	k8xC	a
šplhat	šplhat	k5eAaImF	šplhat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
síle	síla	k1gFnSc3	síla
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
zabít	zabít	k5eAaPmF	zabít
a	a	k8xC	a
odtáhnout	odtáhnout	k5eAaPmF	odtáhnout
kořist	kořist	k1gFnSc4	kořist
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vyvinout	vyvinout	k5eAaPmF	vyvinout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Tuto	tento	k3xDgFnSc4	tento
rychlost	rychlost	k1gFnSc4	rychlost
však	však	k9	však
udrží	udržet	k5eAaPmIp3nS	udržet
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
koček	kočka	k1gFnPc2	kočka
není	být	k5eNaImIp3nS	být
vytrvalý	vytrvalý	k2eAgMnSc1d1	vytrvalý
běžec	běžec	k1gMnSc1	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
skočit	skočit	k5eAaPmF	skočit
6,5	[number]	k4	6,5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
m	m	kA	m
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
a	a	k8xC	a
3	[number]	k4	3
m	m	kA	m
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
obratnost	obratnost	k1gFnSc1	obratnost
a	a	k8xC	a
anatomie	anatomie	k1gFnSc1	anatomie
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
podélně	podélně	k6eAd1	podélně
umístěné	umístěný	k2eAgFnPc1d1	umístěná
lopatky	lopatka	k1gFnPc1	lopatka
<g/>
)	)	kIx)	)
uzpůsobená	uzpůsobený	k2eAgFnSc1d1	uzpůsobená
ke	k	k7c3	k
šplhání	šplhání	k1gNnSc3	šplhání
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vylézt	vylézt	k5eAaPmF	vylézt
na	na	k7c4	na
jakkoliv	jakkoliv	k6eAd1	jakkoliv
rozměrné	rozměrný	k2eAgInPc4d1	rozměrný
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
těch	ten	k3xDgMnPc2	ten
nemnoho	mnoho	k6eNd1	mnoho
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umějí	umět	k5eAaImIp3nP	umět
slézat	slézat	k5eAaImF	slézat
zpět	zpět	k6eAd1	zpět
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
koček	kočka	k1gFnPc2	kočka
to	ten	k3xDgNnSc4	ten
jinak	jinak	k6eAd1	jinak
zvládá	zvládat	k5eAaImIp3nS	zvládat
jen	jen	k9	jen
levhart	levhart	k1gMnSc1	levhart
obláčkový	obláčkový	k2eAgMnSc1d1	obláčkový
a	a	k8xC	a
Diardův	Diardův	k2eAgMnSc1d1	Diardův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
vynést	vynést	k5eAaPmF	vynést
do	do	k7c2	do
korun	koruna	k1gFnPc2	koruna
stromů	strom	k1gInPc2	strom
živočichy	živočich	k1gMnPc4	živočich
podstatně	podstatně	k6eAd1	podstatně
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
pozorován	pozorován	k2eAgMnSc1d1	pozorován
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
do	do	k7c2	do
rozsochy	rozsocha	k1gFnSc2	rozsocha
stromu	strom	k1gInSc2	strom
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3,5	[number]	k4	3,5
m	m	kA	m
91	[number]	k4	91
<g/>
kg	kg	kA	kg
mládě	mládě	k1gNnSc4	mládě
žirafy	žirafa	k1gFnSc2	žirafa
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
vážící	vážící	k2eAgInSc1d1	vážící
až	až	k9	až
125	[number]	k4	125
kg	kg	kA	kg
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
jiný	jiný	k2eAgMnSc1d1	jiný
jedinec	jedinec	k1gMnSc1	jedinec
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5,7	[number]	k4	5,7
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
měl	mít	k5eAaImAgMnS	mít
historicky	historicky	k6eAd1	historicky
(	(	kIx(	(
<g/>
cca	cca	kA	cca
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
obrovský	obrovský	k2eAgInSc1d1	obrovský
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Zabíral	zabírat	k5eAaImAgInS	zabírat
téměř	téměř	k6eAd1	téměř
35	[number]	k4	35
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
mezi	mezi	k7c7	mezi
koncem	konec	k1gInSc7	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zmenšení	zmenšení	k1gNnSc3	zmenšení
a	a	k8xC	a
fragmentaci	fragmentace	k1gFnSc3	fragmentace
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
jde	jít	k5eAaImIp3nS	jít
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
areálů	areál	k1gInPc2	areál
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
kočkami	kočka	k1gFnPc7	kočka
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
pumou	puma	k1gFnSc7	puma
americkou	americký	k2eAgFnSc7d1	americká
a	a	k8xC	a
rysem	rys	k1gMnSc7	rys
ostrovidem	ostrovid	k1gMnSc7	ostrovid
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
výskyt	výskyt	k1gInSc1	výskyt
levharta	levhart	k1gMnSc2	levhart
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
8,5	[number]	k4	8,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pouhých	pouhý	k2eAgNnPc2d1	pouhé
25	[number]	k4	25
%	%	kIx~	%
původní	původní	k2eAgFnSc2d1	původní
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dalších	další	k2eAgFnPc2d1	další
4,2	[number]	k4	4,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
nejistý	jistý	k2eNgInSc1d1	nejistý
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zřejmě	zřejmě	k6eAd1	zřejmě
vyhynulý	vyhynulý	k2eAgInSc1d1	vyhynulý
nebo	nebo	k8xC	nebo
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přítomný	přítomný	k2eAgInSc1d1	přítomný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
posledních	poslední	k2eAgInPc2d1	poslední
cca	cca	kA	cca
200	[number]	k4	200
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
začátek	začátek	k1gInSc1	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
až	až	k8xS	až
začátek	začátek	k1gInSc1	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
zmizel	zmizet	k5eAaPmAgMnS	zmizet
levhart	levhart	k1gMnSc1	levhart
z	z	k7c2	z
asi	asi	k9	asi
21,9	[number]	k4	21,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
spodního	spodní	k2eAgInSc2d1	spodní
pleistocénu	pleistocén	k1gInSc2	pleistocén
žili	žít	k5eAaImAgMnP	žít
levharti	levhart	k1gMnPc1	levhart
kromě	kromě	k7c2	kromě
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
subspecií	subspecie	k1gFnPc2	subspecie
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
begoueni	begouen	k2eAgMnPc1d1	begouen
<g/>
,	,	kIx,	,
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
sickenbergi	sickenberg	k1gFnSc2	sickenberg
<g/>
,	,	kIx,	,
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
antiqua	antiqua	k6eAd1	antiqua
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
spelaea	spelaea	k1gFnSc1	spelaea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
lze	lze	k6eAd1	lze
potvrdit	potvrdit	k5eAaPmF	potvrdit
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgInPc2d1	různý
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jeskynních	jeskynní	k2eAgNnPc2d1	jeskynní
nalezišť	naleziště	k1gNnPc2	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
pleistocénu	pleistocén	k1gInSc6	pleistocén
(	(	kIx(	(
<g/>
před	před	k7c4	před
cca	cca	kA	cca
100	[number]	k4	100
000	[number]	k4	000
až	až	k9	až
24	[number]	k4	24
000	[number]	k4	000
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
šelma	šelma	k1gFnSc1	šelma
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
kočkami	kočka	k1gFnPc7	kočka
poměrně	poměrně	k6eAd1	poměrně
hojnou	hojný	k2eAgFnSc7d1	hojná
součástí	součást	k1gFnSc7	součást
evropské	evropský	k2eAgFnSc2d1	Evropská
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
zmizela	zmizet	k5eAaPmAgFnS	zmizet
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
pleistocénní	pleistocénní	k2eAgFnSc2d1	pleistocénní
fauny	fauna	k1gFnSc2	fauna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mamuti	mamut	k1gMnPc1	mamut
<g/>
,	,	kIx,	,
lvi	lev	k1gMnPc1	lev
jeskynní	jeskynní	k2eAgMnPc1d1	jeskynní
<g/>
,	,	kIx,	,
nosorožci	nosorožec	k1gMnPc1	nosorožec
srstnatí	srstnatý	k2eAgMnPc1d1	srstnatý
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
jeskynní	jeskynní	k2eAgMnPc1d1	jeskynní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
přežívaly	přežívat	k5eAaImAgFnP	přežívat
lokální	lokální	k2eAgFnPc1d1	lokální
populace	populace	k1gFnPc1	populace
levhartů	levhart	k1gMnPc2	levhart
i	i	k8xC	i
v	v	k7c6	v
holocénu	holocén	k1gInSc6	holocén
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
asi	asi	k9	asi
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
Nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
cíp	cíp	k1gInSc4	cíp
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
podhůří	podhůří	k1gNnSc2	podhůří
Kavkazu	Kavkaz	k1gInSc6	Kavkaz
levharti	levhart	k1gMnPc1	levhart
(	(	kIx(	(
<g/>
poddruhu	poddruh	k1gInSc6	poddruh
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
saxicolor	saxicolor	k1gInSc1	saxicolor
<g/>
)	)	kIx)	)
obývali	obývat	k5eAaImAgMnP	obývat
ještě	ještě	k9	ještě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
evropské	evropský	k2eAgFnPc1d1	Evropská
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
výběžek	výběžek	k1gInSc1	výběžek
asijské	asijský	k2eAgFnSc2d1	asijská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Afrika	Afrika	k1gFnSc1	Afrika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
poddruh	poddruh	k1gInSc1	poddruh
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
levhart	levhart	k1gMnSc1	levhart
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
pardus	pardus	k1gInSc1	pardus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obýval	obývat	k5eAaImAgMnS	obývat
levhart	levhart	k1gMnSc1	levhart
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytoval	vyskytovat	k5eNaImAgMnS	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
malých	malý	k2eAgInPc2d1	malý
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
rozlohou	rozloha	k1gFnSc7	rozloha
marginálních	marginální	k2eAgFnPc6d1	marginální
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
vrcholech	vrchol	k1gInPc6	vrchol
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
pohoří	pohořet	k5eAaPmIp3nS	pohořet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
97	[number]	k4	97
%	%	kIx~	%
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
levharti	levhart	k1gMnPc1	levhart
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
poslední	poslední	k2eAgInPc1d1	poslední
nepatrné	patrný	k2eNgInPc1d1	patrný
zbytky	zbytek	k1gInPc1	zbytek
přežívají	přežívat	k5eAaImIp3nP	přežívat
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Ahaggar	Ahaggara	k1gFnPc2	Ahaggara
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
to	ten	k3xDgNnSc4	ten
potvrdit	potvrdit	k5eAaPmF	potvrdit
ještě	ještě	k9	ještě
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masivní	masivní	k2eAgFnSc3d1	masivní
fragmentaci	fragmentace	k1gFnSc3	fragmentace
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Senegalu	Senegal	k1gInSc6	Senegal
<g/>
,	,	kIx,	,
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
,	,	kIx,	,
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
Burkině	Burkina	k1gFnSc3	Burkina
Faso	Faso	k6eAd1	Faso
<g/>
,	,	kIx,	,
Ghaně	Ghana	k1gFnSc3	Ghana
<g/>
,	,	kIx,	,
Beninu	Benin	k1gInSc3	Benin
<g/>
,	,	kIx,	,
Nigeru	Niger	k1gInSc3	Niger
a	a	k8xC	a
Nigérii	Nigérie	k1gFnSc3	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
nemnohé	mnohý	k2eNgInPc4d1	nemnohý
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
lze	lze	k6eAd1	lze
levharty	levhart	k1gMnPc4	levhart
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
Demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kamerunu	Kamerun	k1gInSc6	Kamerun
<g/>
,	,	kIx,	,
Gabonu	Gabon	k1gInSc6	Gabon
<g/>
,	,	kIx,	,
Konžské	konžský	k2eAgFnSc6d1	Konžská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Středoafrické	středoafrický	k2eAgFnSc6d1	Středoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Čadu	Čad	k1gInSc6	Čad
a	a	k8xC	a
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc1	Kongo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozkouskování	rozkouskování	k1gNnSc3	rozkouskování
areálu	areál	k1gInSc2	areál
kvůli	kvůli	k7c3	kvůli
lidským	lidský	k2eAgFnPc3d1	lidská
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Jádrovými	jádrový	k2eAgFnPc7d1	jádrová
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgNnPc1d1	velké
a	a	k8xC	a
kompaktní	kompaktní	k2eAgNnPc1d1	kompaktní
území	území	k1gNnPc1	území
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc4	Kongo
a	a	k8xC	a
Gabonu	Gabona	k1gFnSc4	Gabona
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
ve	v	k7c6	v
Středoafrické	středoafrický	k2eAgFnSc6d1	Středoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
a	a	k8xC	a
Tanzanie	Tanzanie	k1gFnPc1	Tanzanie
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výskytu	výskyt	k1gInSc2	výskyt
levharta	levhart	k1gMnSc2	levhart
nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
státy	stát	k1gInPc7	stát
Východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Ugandě	Uganda	k1gFnSc6	Uganda
a	a	k8xC	a
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
částech	část	k1gFnPc6	část
Somálska	Somálsko	k1gNnSc2	Somálsko
a	a	k8xC	a
Eritrey	Eritrea	k1gFnSc2	Eritrea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Džibutska	Džibutsk	k1gInSc2	Džibutsk
a	a	k8xC	a
Súdánu	Súdán	k1gInSc6	Súdán
tato	tento	k3xDgFnSc1	tento
šelma	šelma	k1gFnSc1	šelma
buď	buď	k8xC	buď
zmizela	zmizet	k5eAaPmAgFnS	zmizet
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
v	v	k7c6	v
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
se	se	k3xPyFc4	se
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rovněž	rovněž	k9	rovněž
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
oblastí	oblast	k1gFnSc7	oblast
s	s	k7c7	s
nejpočetnějšími	početní	k2eAgFnPc7d3	nejpočetnější
a	a	k8xC	a
nejzdravějšími	zdravý	k2eAgFnPc7d3	nejzdravější
populacemi	populace	k1gFnPc7	populace
levharta	levhart	k1gMnSc2	levhart
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jakousi	jakýsi	k3yIgFnSc4	jakýsi
pevnost	pevnost	k1gFnSc4	pevnost
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
,	,	kIx,	,
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
,	,	kIx,	,
Botswaně	Botswana	k1gFnSc6	Botswana
<g/>
,	,	kIx,	,
Zambii	Zambie	k1gFnSc6	Zambie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
a	a	k8xC	a
Mosambiku	Mosambik	k1gInSc6	Mosambik
<g/>
.	.	kIx.	.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
veškerou	veškerý	k3xTgFnSc4	veškerý
centrální	centrální	k2eAgFnSc4d1	centrální
populaci	populace	k1gFnSc4	populace
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
levharti	levhart	k1gMnPc1	levhart
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
podél	podél	k7c2	podél
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pobřežních	pobřežní	k2eAgInPc6d1	pobřežní
a	a	k8xC	a
horských	horský	k2eAgNnPc6d1	horské
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Asie	Asie	k1gFnSc2	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
žili	žít	k5eAaImAgMnP	žít
levharti	levhart	k1gMnPc1	levhart
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
Rudého	rudý	k2eAgInSc2d1	rudý
<g/>
,	,	kIx,	,
Středozemního	středozemní	k2eAgMnSc2d1	středozemní
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
Čínu	Čína	k1gFnSc4	Čína
až	až	k9	až
po	po	k7c4	po
Koreu	Korea	k1gFnSc4	Korea
a	a	k8xC	a
oblasti	oblast	k1gFnPc4	oblast
východního	východní	k2eAgNnSc2d1	východní
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obývali	obývat	k5eAaImAgMnP	obývat
i	i	k8xC	i
řadu	řada	k1gFnSc4	řada
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
především	především	k9	především
Jávu	Jáva	k1gFnSc4	Jáva
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc4	Srí
Lanku	lanko	k1gNnSc3	lanko
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozšíření	rozšíření	k1gNnSc1	rozšíření
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
platné	platný	k2eAgFnPc4d1	platná
i	i	k9	i
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
areál	areál	k1gInSc1	areál
byl	být	k5eAaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
fragmentován	fragmentovat	k5eAaPmNgInS	fragmentovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
žije	žít	k5eAaImIp3nS	žít
poddruh	poddruh	k1gInSc4	poddruh
levhart	levhart	k1gMnSc1	levhart
arabský	arabský	k2eAgMnSc1d1	arabský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
nimr	nimr	k1gInSc1	nimr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
baštou	bašta	k1gFnSc7	bašta
je	být	k5eAaImIp3nS	být
Omán	Omán	k1gInSc1	Omán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
(	(	kIx(	(
<g/>
Jemen	Jemen	k1gInSc1	Jemen
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
možná	možná	k9	možná
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
přežívá	přežívat	k5eAaImIp3nS	přežívat
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgMnSc1d1	perský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
saxicolor	saxicolor	k1gInSc1	saxicolor
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
populačně	populačně	k6eAd1	populačně
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
<g/>
,	,	kIx,	,
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
Turkmenistánu	Turkmenistán	k1gInSc6	Turkmenistán
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
žijí	žít	k5eAaImIp3nP	žít
jen	jen	k9	jen
malé	malý	k2eAgFnPc1d1	malá
roztříštěné	roztříštěný	k2eAgFnPc1d1	roztříštěná
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
je	být	k5eAaImIp3nS	být
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Pákistán	Pákistán	k1gInSc1	Pákistán
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
styku	styk	k1gInSc6	styk
dvou	dva	k4xCgInPc2	dva
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
l.	l.	k?	l.
perský	perský	k2eAgInSc1d1	perský
a	a	k8xC	a
l.	l.	k?	l.
indický	indický	k2eAgInSc4d1	indický
<g/>
)	)	kIx)	)
a	a	k8xC	a
levhart	levhart	k1gMnSc1	levhart
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
hojných	hojný	k2eAgInPc6d1	hojný
počtech	počet	k1gInPc6	počet
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
i	i	k9	i
odtud	odtud	k6eAd1	odtud
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
míst	místo	k1gNnPc2	místo
vymizel	vymizet	k5eAaPmAgInS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc6	Bhútán
<g/>
,	,	kIx,	,
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
<g/>
,	,	kIx,	,
Myanmaru	Myanmar	k1gInSc6	Myanmar
a	a	k8xC	a
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
i	i	k8xC	i
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Číně	Čína	k1gFnSc6	Čína
žijící	žijící	k2eAgMnSc1d1	žijící
levhart	levhart	k1gMnSc1	levhart
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
fusca	fusca	k1gFnSc1	fusca
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
početný	početný	k2eAgInSc1d1	početný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
oblastí	oblast	k1gFnSc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
podhůří	podhůří	k1gNnSc4	podhůří
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poddruh	poddruh	k1gInSc1	poddruh
levhart	levhart	k1gMnSc1	levhart
cejlonský	cejlonský	k2eAgMnSc1d1	cejlonský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
kotyia	kotyia	k1gFnSc1	kotyia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
a	a	k8xC	a
státech	stát	k1gInPc6	stát
Indočíny	Indočína	k1gFnSc2	Indočína
žijící	žijící	k2eAgInSc4d1	žijící
poddruh	poddruh	k1gInSc4	poddruh
levhart	levhart	k1gMnSc1	levhart
indočínský	indočínský	k2eAgMnSc1d1	indočínský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
dalacouri	dalacour	k1gFnSc2	dalacour
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
donedávna	donedávna	k6eAd1	donedávna
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejnovější	nový	k2eAgInPc1d3	nejnovější
výzkumy	výzkum	k1gInPc1	výzkum
a	a	k8xC	a
data	datum	k1gNnPc1	datum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
opak	opak	k1gInSc4	opak
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
relativně	relativně	k6eAd1	relativně
početné	početný	k2eAgFnSc2d1	početná
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
jedinců	jedinec	k1gMnPc2	jedinec
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
<g/>
,	,	kIx,	,
Kambodži	Kambodža	k1gFnSc6	Kambodža
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
z	z	k7c2	z
Laosu	Laos	k1gInSc2	Laos
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc2	Vietnam
šelma	šelma	k1gFnSc1	šelma
zřejmě	zřejmě	k6eAd1	zřejmě
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
levhart	levhart	k1gMnSc1	levhart
jávský	jávský	k2eAgMnSc1d1	jávský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
melas	melasa	k1gFnPc2	melasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
vědce	vědec	k1gMnPc4	vědec
je	být	k5eAaImIp3nS	být
záhadou	záhada	k1gFnSc7	záhada
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
levhart	levhart	k1gMnSc1	levhart
nežije	žít	k5eNaImIp3nS	žít
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
blíž	blízce	k6eAd2	blízce
kontinentální	kontinentální	k2eAgFnSc3d1	kontinentální
Asii	Asie	k1gFnSc3	Asie
než	než	k8xS	než
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
nebyly	být	k5eNaImAgFnP	být
vyhodnoceny	vyhodnocen	k2eAgInPc4d1	vyhodnocen
přímé	přímý	k2eAgInPc4d1	přímý
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
levhart	levhart	k1gMnSc1	levhart
vůbec	vůbec	k9	vůbec
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
všeobecně	všeobecně	k6eAd1	všeobecně
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
studie	studie	k1gFnSc1	studie
potvrzující	potvrzující	k2eAgFnSc4d1	potvrzující
přítomnost	přítomnost	k1gFnSc4	přítomnost
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
až	až	k9	až
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vymizení	vymizení	k1gNnSc1	vymizení
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
tlakem	tlak	k1gInSc7	tlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
množících	množící	k2eAgFnPc2d1	množící
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
žil	žít	k5eAaImAgMnS	žít
levhart	levhart	k1gMnSc1	levhart
i	i	k9	i
na	na	k7c6	na
Kangeanských	Kangeanský	k2eAgInPc6d1	Kangeanský
ostrovech	ostrov	k1gInPc6	ostrov
východně	východně	k6eAd1	východně
od	od	k7c2	od
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
zavlečen	zavlečen	k2eAgInSc1d1	zavlečen
lidmi	člověk	k1gMnPc7	člověk
jako	jako	k8xC	jako
potenciální	potenciální	k2eAgFnSc1d1	potenciální
lovná	lovný	k2eAgFnSc1d1	lovná
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poddruh	poddruh	k1gInSc1	poddruh
levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
japonensis	japonensis	k1gInSc1	japonensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
celkový	celkový	k2eAgInSc1d1	celkový
areál	areál	k1gInSc1	areál
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
odhadnutelný	odhadnutelný	k2eAgInSc1d1	odhadnutelný
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodněji	východně	k6eAd3	východně
žijící	žijící	k2eAgMnSc1d1	žijící
levhart	levhart	k1gMnSc1	levhart
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
orientalis	orientalis	k1gInSc1	orientalis
<g/>
)	)	kIx)	)
obývá	obývat	k5eAaImIp3nS	obývat
v	v	k7c6	v
nepatrných	patrný	k2eNgInPc6d1	patrný
počtech	počet	k1gInPc6	počet
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
východního	východní	k2eAgNnSc2d1	východní
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Populace	populace	k1gFnSc2	populace
==	==	k?	==
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
divoká	divoký	k2eAgFnSc1d1	divoká
populace	populace	k1gFnSc1	populace
levharta	levhart	k1gMnSc2	levhart
skvrnitého	skvrnitý	k2eAgInSc2d1	skvrnitý
zřejmě	zřejmě	k6eAd1	zřejmě
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
několika	několik	k4yIc3	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
spolehlivá	spolehlivý	k2eAgNnPc4d1	spolehlivé
data	datum	k1gNnPc4	datum
pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
vyčíslení	vyčíslení	k1gNnSc4	vyčíslení
ale	ale	k8xC	ale
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
minimum	minimum	k1gNnSc1	minimum
údajů	údaj	k1gInPc2	údaj
k	k	k7c3	k
africké	africký	k2eAgFnSc3d1	africká
populaci	populace	k1gFnSc3	populace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
mnoho	mnoho	k4c4	mnoho
desítek	desítka	k1gFnPc2	desítka
až	až	k6eAd1	až
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k9	tak
většinu	většina	k1gFnSc4	většina
všech	všecek	k3xTgMnPc2	všecek
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
subsaharská	subsaharský	k2eAgFnSc1d1	subsaharská
populace	populace	k1gFnSc1	populace
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
cca	cca	kA	cca
714	[number]	k4	714
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
(	(	kIx(	(
<g/>
rozptyl	rozptyl	k1gInSc1	rozptyl
598	[number]	k4	598
000	[number]	k4	000
až	až	k9	až
854	[number]	k4	854
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
údaj	údaj	k1gInSc1	údaj
byl	být	k5eAaImAgInS	být
záhy	záhy	k6eAd1	záhy
silně	silně	k6eAd1	silně
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
jako	jako	k8xC	jako
nadhodnocený	nadhodnocený	k2eAgMnSc1d1	nadhodnocený
a	a	k8xC	a
rozhodně	rozhodně	k6eAd1	rozhodně
nepřesný	přesný	k2eNgInSc4d1	nepřesný
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
na	na	k7c6	na
základě	základ	k1gInSc6	základ
neúplných	úplný	k2eNgInPc2d1	neúplný
údajů	údaj	k1gInPc2	údaj
a	a	k8xC	a
neberouc	brát	k5eNaImSgFnS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
množství	množství	k1gNnSc2	množství
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
lidských	lidský	k2eAgFnPc2d1	lidská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
shodla	shodnout	k5eAaBmAgFnS	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečné	skutečný	k2eAgNnSc1d1	skutečné
číslo	číslo	k1gNnSc1	číslo
bude	být	k5eAaImBp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
chybějí	chybět	k5eAaImIp3nP	chybět
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gabonu	Gabon	k1gInSc6	Gabon
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
levhartí	levhartí	k2eAgFnSc1d1	levhartí
populace	populace	k1gFnSc1	populace
na	na	k7c4	na
38	[number]	k4	38
463	[number]	k4	463
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozdější	pozdní	k2eAgInSc1d2	pozdější
podrobný	podrobný	k2eAgInSc1d1	podrobný
průzkum	průzkum	k1gInSc1	průzkum
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
jich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
2358	[number]	k4	2358
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
mimo	mimo	k7c4	mimo
parky	park	k1gInPc4	park
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
nedávný	dávný	k2eNgInSc1d1	nedávný
odhad	odhad	k1gInSc1	odhad
počtu	počet	k1gInSc2	počet
levhartů	levhart	k1gMnPc2	levhart
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
žilo	žít	k5eAaImAgNnS	žít
okolo	okolo	k7c2	okolo
4250	[number]	k4	4250
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
několika	několik	k4yIc3	několik
asijským	asijský	k2eAgInPc3d1	asijský
poddruhům	poddruh	k1gInPc3	poddruh
existují	existovat	k5eAaImIp3nP	existovat
celkem	celkem	k6eAd1	celkem
přesné	přesný	k2eAgInPc1d1	přesný
údaje	údaj	k1gInPc1	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
levhartů	levhart	k1gMnPc2	levhart
mandžuských	mandžuský	k2eAgNnPc2d1	mandžuské
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
čínských	čínský	k2eAgMnPc2d1	čínský
174	[number]	k4	174
<g/>
–	–	k?	–
<g/>
348	[number]	k4	348
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
jávských	jávský	k2eAgNnPc2d1	jávské
méně	málo	k6eAd2	málo
než	než	k8xS	než
250	[number]	k4	250
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
optimistický	optimistický	k2eAgInSc1d1	optimistický
odhad	odhad	k1gInSc1	odhad
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
681	[number]	k4	681
kusech	kus	k1gInPc6	kus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
cejlonských	cejlonský	k2eAgFnPc2d1	cejlonská
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
950	[number]	k4	950
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
perských	perský	k2eAgFnPc2d1	perská
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
a	a	k8xC	a
levhartů	levhart	k1gMnPc2	levhart
arabských	arabský	k2eAgNnPc2d1	arabské
méně	málo	k6eAd2	málo
než	než	k8xS	než
250	[number]	k4	250
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
dlouho	dlouho	k6eAd1	dlouho
neuváděla	uvádět	k5eNaImAgFnS	uvádět
žádné	žádný	k3yNgInPc4	žádný
relevantní	relevantní	k2eAgInPc4d1	relevantní
údaje	údaj	k1gInPc4	údaj
k	k	k7c3	k
početnosti	početnost	k1gFnSc3	početnost
své	svůj	k3xOyFgFnSc2	svůj
levhartí	levhartí	k2eAgFnSc2d1	levhartí
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
shromážděna	shromážděn	k2eAgFnSc1d1	shromážděna
a	a	k8xC	a
publikována	publikován	k2eAgNnPc1d1	Publikováno
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
12	[number]	k4	12
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
000	[number]	k4	000
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
levhartů	levhart	k1gMnPc2	levhart
indických	indický	k2eAgMnPc2d1	indický
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
žije	žít	k5eAaImIp3nS	žít
maximálně	maximálně	k6eAd1	maximálně
20	[number]	k4	20
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
levharta	levhart	k1gMnSc2	levhart
indočínského	indočínský	k2eAgMnSc2d1	indočínský
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
svého	svůj	k3xOyFgNnSc2	svůj
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
se	se	k3xPyFc4	se
rapidní	rapidní	k2eAgInSc1d1	rapidní
pokles	pokles	k1gInSc1	pokles
jak	jak	k8xC	jak
území	území	k1gNnSc1	území
výskytu	výskyt	k1gInSc2	výskyt
(	(	kIx(	(
<g/>
6,2	[number]	k4	6,2
%	%	kIx~	%
z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
<g/>
)	)	kIx)	)
tak	tak	k8xC	tak
počtu	počet	k1gInSc2	počet
jedinců	jedinec	k1gMnPc2	jedinec
–	–	k?	–
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
jich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
973	[number]	k4	973
<g/>
–	–	k?	–
<g/>
2503	[number]	k4	2503
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
chovu	chov	k1gInSc2	chov
levhartů	levhart	k1gMnPc2	levhart
nebyla	být	k5eNaImAgFnS	být
řešena	řešen	k2eAgFnSc1d1	řešena
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
poddruhům	poddruh	k1gInPc3	poddruh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
nekontrolované	kontrolovaný	k2eNgFnSc3d1	nekontrolovaná
hybridizaci	hybridizace	k1gFnSc3	hybridizace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
potýkají	potýkat	k5eAaImIp3nP	potýkat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
populace	populace	k1gFnPc1	populace
levhartů	levhart	k1gMnPc2	levhart
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
orientační	orientační	k2eAgInPc1d1	orientační
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
několika	několik	k4yIc3	několik
poddruhům	poddruh	k1gInPc3	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chovaných	chovaný	k2eAgMnPc2d1	chovaný
levhartů	levhart	k1gMnPc2	levhart
arabských	arabský	k2eAgFnPc2d1	arabská
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
perských	perský	k2eAgFnPc2d1	perská
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
100	[number]	k4	100
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
cejlonských	cejlonský	k2eAgMnPc2d1	cejlonský
60	[number]	k4	60
až	až	k6eAd1	až
70	[number]	k4	70
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
jávských	jávský	k2eAgFnPc2d1	jávská
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
a	a	k8xC	a
levhartů	levhart	k1gMnPc2	levhart
mandžuských	mandžuský	k2eAgMnPc2d1	mandžuský
okolo	okolo	k6eAd1	okolo
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
afričtí	africký	k2eAgMnPc1d1	africký
<g/>
,	,	kIx,	,
indičtí	indický	k2eAgMnPc1d1	indický
<g/>
,	,	kIx,	,
indočínští	indočínský	k2eAgMnPc1d1	indočínský
a	a	k8xC	a
čínští	čínský	k2eAgMnPc1d1	čínský
jsou	být	k5eAaImIp3nP	být
chováni	chován	k2eAgMnPc1d1	chován
především	především	k9	především
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
svého	svůj	k3xOyFgInSc2	svůj
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jakékoliv	jakýkoliv	k3yIgFnPc1	jakýkoliv
přesnější	přesný	k2eAgFnPc1d2	přesnější
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
celkových	celkový	k2eAgInPc6d1	celkový
početních	početní	k2eAgInPc6d1	početní
stavech	stav	k1gInPc6	stav
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
ale	ale	k9	ale
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
indických	indický	k2eAgFnPc6d1	indická
zoo	zoo	k1gFnPc6	zoo
395	[number]	k4	395
jedinců	jedinec	k1gMnPc2	jedinec
levharta	levhart	k1gMnSc2	levhart
indického	indický	k2eAgMnSc2d1	indický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biotop	biotop	k1gInSc4	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
velkého	velký	k2eAgInSc2d1	velký
rozsahu	rozsah	k1gInSc2	rozsah
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
levharti	levhart	k1gMnPc1	levhart
dokáží	dokázat	k5eAaPmIp3nP	dokázat
žít	žít	k5eAaImF	žít
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
typech	typ	k1gInPc6	typ
přírodního	přírodní	k2eAgNnSc2d1	přírodní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nežijí	žít	k5eNaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
suchých	suchý	k2eAgFnPc6d1	suchá
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
vysokohorských	vysokohorský	k2eAgInPc6d1	vysokohorský
biotopech	biotop	k1gInPc6	biotop
(	(	kIx(	(
<g/>
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Africe	Afrika	k1gFnSc6	Afrika
nad	nad	k7c7	nad
cca	cca	kA	cca
4600	[number]	k4	4600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
nad	nad	k7c7	nad
cca	cca	kA	cca
5200	[number]	k4	5200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
nad	nad	k7c7	nad
cca	cca	kA	cca
3500	[number]	k4	3500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mimo	mimo	k7c4	mimo
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
šířku	šířka	k1gFnSc4	šířka
svého	svůj	k3xOyFgNnSc2	svůj
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
například	například	k6eAd1	například
v	v	k7c6	v
tundrách	tundra	k1gFnPc6	tundra
a	a	k8xC	a
polárních	polární	k2eAgNnPc6d1	polární
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
určují	určovat	k5eAaImIp3nP	určovat
jejich	jejich	k3xOp3gInSc4	jejich
výskyt	výskyt	k1gInSc4	výskyt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dostatek	dostatek	k1gInSc4	dostatek
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
minimální	minimální	k2eAgNnSc4d1	minimální
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
možnosti	možnost	k1gFnSc2	možnost
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
levhartům	levhart	k1gMnPc3	levhart
stačí	stačit	k5eAaBmIp3nS	stačit
pít	pít	k5eAaImF	pít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
zhruba	zhruba	k6eAd1	zhruba
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
vydrží	vydržet	k5eAaPmIp3nS	vydržet
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
až	až	k9	až
deset	deset	k4xCc1	deset
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pohybovat	pohybovat	k5eAaImF	pohybovat
i	i	k9	i
v	v	k7c6	v
aridním	aridní	k2eAgNnSc6d1	aridní
klimatu	klima	k1gNnSc6	klima
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Kalahari	Kalahar	k1gFnSc6	Kalahar
či	či	k8xC	či
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
tak	tak	k9	tak
se	se	k3xPyFc4	se
umí	umět	k5eAaImIp3nS	umět
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
s	s	k7c7	s
tuhými	tuhý	k2eAgFnPc7d1	tuhá
zimami	zima	k1gFnPc7	zima
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
či	či	k8xC	či
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
jim	on	k3xPp3gMnPc3	on
křovino-lesnatý	křovinoesnatý	k2eAgInSc1d1	křovino-lesnatý
biotop	biotop	k1gInSc1	biotop
subtropů	subtropy	k1gInPc2	subtropy
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
adaptovali	adaptovat	k5eAaBmAgMnP	adaptovat
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
bezlesý	bezlesý	k2eAgInSc4d1	bezlesý
skalnatý	skalnatý	k2eAgInSc4d1	skalnatý
terén	terén	k1gInSc4	terén
(	(	kIx(	(
<g/>
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
levharti	levhart	k1gMnPc1	levhart
nějaké	nějaký	k3yIgNnSc4	nějaký
prostředí	prostředí	k1gNnSc4	prostředí
přece	přece	k9	přece
jen	jen	k6eAd1	jen
preferují	preferovat	k5eAaImIp3nP	preferovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
stromy	strom	k1gInPc1	strom
–	–	k?	–
ať	ať	k8xC	ať
už	už	k6eAd1	už
poměrně	poměrně	k6eAd1	poměrně
řídký	řídký	k2eAgInSc4d1	řídký
porost	porost	k1gInSc4	porost
akácií	akácie	k1gFnPc2	akácie
v	v	k7c6	v
savanách	savana	k1gFnPc6	savana
či	či	k8xC	či
husté	hustý	k2eAgInPc1d1	hustý
lesy	les	k1gInPc1	les
tropických	tropický	k2eAgFnPc2d1	tropická
oblastí	oblast	k1gFnPc2	oblast
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
limitují	limitovat	k5eAaBmIp3nP	limitovat
jejich	jejich	k3xOp3gFnSc4	jejich
habitat	habitat	k5eAaPmF	habitat
–	–	k?	–
konkurence	konkurence	k1gFnSc2	konkurence
jiných	jiný	k2eAgFnPc2d1	jiná
velkých	velký	k2eAgFnPc2d1	velká
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
především	především	k9	především
tygrů	tygr	k1gMnPc2	tygr
a	a	k8xC	a
lvů	lev	k1gMnPc2	lev
<g/>
,	,	kIx,	,
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc1	přítomnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
většinou	většinou	k6eAd1	většinou
spíše	spíše	k9	spíše
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jedinci	jedinec	k1gMnPc1	jedinec
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
koexistovat	koexistovat	k5eAaImF	koexistovat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byli	být	k5eAaImAgMnP	být
nalezeni	naleznout	k5eAaPmNgMnP	naleznout
tři	tři	k4xCgMnPc1	tři
levharti	levhart	k1gMnPc1	levhart
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
parní	parní	k2eAgFnSc6d1	parní
lokomotivě	lokomotiva	k1gFnSc6	lokomotiva
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
v	v	k7c6	v
Kampale	Kampala	k1gFnSc6	Kampala
(	(	kIx(	(
<g/>
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byla	být	k5eAaImAgFnS	být
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
přítomnost	přítomnost	k1gFnSc1	přítomnost
několika	několik	k4yIc2	několik
levhartů	levhart	k1gMnPc2	levhart
v	v	k7c6	v
Nairobi	Nairobi	k1gNnSc6	Nairobi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indických	indický	k2eAgNnPc6d1	indické
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
spíše	spíše	k9	spíše
pravidlem	pravidlo	k1gNnSc7	pravidlo
než	než	k8xS	než
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
biologie	biologie	k1gFnSc1	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chování	chování	k1gNnSc1	chování
===	===	k?	===
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
je	být	k5eAaImIp3nS	být
skrytě	skrytě	k6eAd1	skrytě
žijící	žijící	k2eAgFnSc1d1	žijící
samotářská	samotářský	k2eAgFnSc1d1	samotářská
šelma	šelma	k1gFnSc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
spatřen	spatřit	k5eAaPmNgInS	spatřit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
nebo	nebo	k8xC	nebo
pár	pár	k4xCyI	pár
během	běh	k1gInSc7	běh
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
spolu	spolu	k6eAd1	spolu
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
koexistovat	koexistovat	k5eAaImF	koexistovat
několik	několik	k4yIc1	několik
dospělých	dospělý	k2eAgNnPc2d1	dospělé
zvířat	zvíře	k1gNnPc2	zvíře
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
noční	noční	k2eAgMnSc1d1	noční
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
loven	lovit	k5eAaImNgMnS	lovit
a	a	k8xC	a
obtěžován	obtěžovat	k5eAaImNgMnS	obtěžovat
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgNnPc2d1	jiné
studií	studio	k1gNnPc2	studio
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
dokáže	dokázat	k5eAaPmIp3nS	dokázat
být	být	k5eAaImF	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
či	či	k8xC	či
svítání	svítání	k1gNnSc2	svítání
nebo	nebo	k8xC	nebo
kdykoliv	kdykoliv	k6eAd1	kdykoliv
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
teplých	teplý	k2eAgInPc6d1	teplý
regionech	region	k1gInPc6	region
(	(	kIx(	(
<g/>
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
námaze	námaha	k1gFnSc3	námaha
v	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
poledne	poledne	k1gNnSc2	poledne
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
brzy	brzy	k6eAd1	brzy
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
nijak	nijak	k6eAd1	nijak
nevyhledává	vyhledávat	k5eNaImIp3nS	vyhledávat
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tygrů	tygr	k1gMnPc2	tygr
nebo	nebo	k8xC	nebo
jaguárů	jaguár	k1gMnPc2	jaguár
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
překročení	překročení	k1gNnPc2	překročení
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
či	či	k8xC	či
mělké	mělký	k2eAgInPc1d1	mělký
brody	brod	k1gInPc1	brod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přeplavat	přeplavat	k5eAaPmF	přeplavat
i	i	k9	i
větší	veliký	k2eAgFnPc4d2	veliký
řeky	řeka	k1gFnPc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
zároveň	zároveň	k6eAd1	zároveň
sledovat	sledovat	k5eAaImF	sledovat
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
především	především	k9	především
o	o	k7c4	o
větší	veliký	k2eAgInPc4d2	veliký
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
skály	skála	k1gFnPc4	skála
<g/>
,	,	kIx,	,
návrší	návrší	k1gNnPc4	návrší
<g/>
,	,	kIx,	,
kopečky	kopeček	k1gInPc4	kopeček
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
úkryt	úkryt	k1gInSc4	úkryt
a	a	k8xC	a
útočiště	útočiště	k1gNnSc4	útočiště
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
jeskyně	jeskyně	k1gFnPc4	jeskyně
<g/>
,	,	kIx,	,
hustý	hustý	k2eAgInSc4d1	hustý
podrost	podrost	k1gInSc4	podrost
a	a	k8xC	a
členité	členitý	k2eAgFnSc2d1	členitá
skály	skála	k1gFnSc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
doupata	doupě	k1gNnPc4	doupě
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
zabil	zabít	k5eAaPmAgMnS	zabít
či	či	k8xC	či
která	který	k3yRgFnSc1	který
místo	místo	k6eAd1	místo
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dikobrazí	dikobrazí	k2eAgFnSc1d1	dikobrazí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teritorialita	teritorialita	k1gFnSc1	teritorialita
===	===	k?	===
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
domovského	domovský	k2eAgNnSc2d1	domovské
území	území	k1gNnSc2	území
levhartů	levhart	k1gMnPc2	levhart
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
pohlaví	pohlaví	k1gNnSc6	pohlaví
a	a	k8xC	a
biotopu	biotop	k1gInSc6	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgNnSc4d2	veliký
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
území	území	k1gNnSc1	území
několika	několik	k4yIc2	několik
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
úměra	úměra	k1gFnSc1	úměra
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yQnSc7	co
méně	málo	k6eAd2	málo
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
teritorium	teritorium	k1gNnSc1	teritorium
šelma	šelma	k1gFnSc1	šelma
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
bohatých	bohatý	k2eAgMnPc2d1	bohatý
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
(	(	kIx(	(
<g/>
NP	NP	kA	NP
Čitvan	Čitvan	k1gMnSc1	Čitvan
<g/>
,	,	kIx,	,
NP	NP	kA	NP
Tsavo	Tsavo	k1gNnSc1	Tsavo
<g/>
,	,	kIx,	,
NP	NP	kA	NP
Serengeti	Serenget	k1gMnPc1	Serenget
<g/>
,	,	kIx,	,
Krugerův	Krugerův	k2eAgMnSc1d1	Krugerův
NP	NP	kA	NP
<g/>
,	,	kIx,	,
Huai	Huai	k1gNnSc1	Huai
Kha	Kha	k1gFnPc2	Kha
Kaeng	Kaenga	k1gFnPc2	Kaenga
NP	NP	kA	NP
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
žijí	žít	k5eAaImIp3nP	žít
samice	samice	k1gFnPc1	samice
na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
km2	km2	k4	km2
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
samci	samec	k1gInPc7	samec
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
76	[number]	k4	76
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Botswaně	Botswana	k1gFnSc6	Botswana
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
mírně	mírně	k6eAd1	mírně
vzrůstají	vzrůstat	k5eAaImIp3nP	vzrůstat
na	na	k7c4	na
33	[number]	k4	33
km2	km2	k4	km2
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
40	[number]	k4	40
až	až	k9	až
69	[number]	k4	69
km2	km2	k4	km2
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aridních	aridní	k2eAgFnPc6d1	aridní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
chudších	chudý	k2eAgFnPc2d2	chudší
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
se	se	k3xPyFc4	se
plocha	plocha	k1gFnSc1	plocha
postupně	postupně	k6eAd1	postupně
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
izraelských	izraelský	k2eAgFnPc6d1	izraelská
pouštích	poušť	k1gFnPc6	poušť
žijí	žít	k5eAaImIp3nP	žít
samice	samice	k1gFnPc1	samice
průměrně	průměrně	k6eAd1	průměrně
na	na	k7c4	na
84	[number]	k4	84
km2	km2	k4	km2
a	a	k8xC	a
samci	samec	k1gMnPc1	samec
na	na	k7c4	na
137	[number]	k4	137
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
jde	jít	k5eAaImIp3nS	jít
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
až	až	k9	až
o	o	k7c4	o
280	[number]	k4	280
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Namibii	Namibie	k1gFnSc6	Namibie
měří	měřit	k5eAaImIp3nS	měřit
teritoria	teritorium	k1gNnSc2	teritorium
samců	samec	k1gMnPc2	samec
průměrně	průměrně	k6eAd1	průměrně
451	[number]	k4	451
km2	km2	k4	km2
(	(	kIx(	(
<g/>
od	od	k7c2	od
210	[number]	k4	210
po	po	k7c4	po
1164	[number]	k4	1164
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samic	samice	k1gFnPc2	samice
mezi	mezi	k7c7	mezi
183	[number]	k4	183
a	a	k8xC	a
194	[number]	k4	194
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Legendární	legendární	k2eAgMnSc1d1	legendární
lidožrout	lidožrout	k1gMnSc1	lidožrout
z	z	k7c2	z
Rudraprayagu	Rudraprayag	k1gInSc2	Rudraprayag
operoval	operovat	k5eAaImAgMnS	operovat
na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1285	[number]	k4	1285
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
i	i	k8xC	i
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
dlouho	dlouho	k6eAd1	dlouho
nedařilo	dařit	k5eNaImAgNnS	dařit
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zjištěné	zjištěný	k2eAgNnSc1d1	zjištěné
teritorium	teritorium	k1gNnSc1	teritorium
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
samci	samec	k1gMnPc1	samec
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Kalahari	Kalahar	k1gFnSc2	Kalahar
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
2182	[number]	k4	2182
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
si	se	k3xPyFc3	se
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
jedinci	jedinec	k1gMnPc1	jedinec
postupně	postupně	k6eAd1	postupně
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
trvá	trvat	k5eAaImIp3nS	trvat
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
jen	jen	k9	jen
o	o	k7c4	o
orientační	orientační	k2eAgInSc4d1	orientační
údaj	údaj	k1gInSc4	údaj
<g/>
:	:	kIx,	:
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
zdrží	zdržet	k5eAaPmIp3nP	zdržet
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
projdou	projít	k5eAaPmIp3nP	projít
celé	celý	k2eAgNnSc4d1	celé
teritorium	teritorium	k1gNnSc4	teritorium
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
ujdou	ujít	k5eAaPmIp3nP	ujít
průměrně	průměrně	k6eAd1	průměrně
9,8	[number]	k4	9,8
km	km	kA	km
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
rozsah	rozsah	k1gInSc1	rozsah
0,8	[number]	k4	0,8
<g/>
–	–	k?	–
<g/>
17,8	[number]	k4	17,8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
značení	značení	k1gNnSc3	značení
teritoria	teritorium	k1gNnSc2	teritorium
používá	používat	k5eAaImIp3nS	používat
levhart	levhart	k1gMnSc1	levhart
několik	několik	k4yIc4	několik
metod	metoda	k1gFnPc2	metoda
–	–	k?	–
postřik	postřik	k1gInSc1	postřik
nápadných	nápadný	k2eAgNnPc2d1	nápadné
míst	místo	k1gNnPc2	místo
směsí	směs	k1gFnPc2	směs
moči	moč	k1gFnSc2	moč
a	a	k8xC	a
sekretu	sekret	k1gInSc2	sekret
z	z	k7c2	z
anální	anální	k2eAgFnSc2d1	anální
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
drásání	drásání	k1gNnSc1	drásání
kůry	kůra	k1gFnSc2	kůra
stromů	strom	k1gInPc2	strom
drápy	dráp	k1gInPc1	dráp
<g/>
,	,	kIx,	,
hrabání	hrabání	k1gNnSc1	hrabání
země	zem	k1gFnSc2	zem
a	a	k8xC	a
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pak	pak	k6eAd1	pak
pokládání	pokládání	k1gNnSc1	pokládání
exkrementů	exkrement	k1gInPc2	exkrement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Komunikace	komunikace	k1gFnSc1	komunikace
====	====	k?	====
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
komunikuji	komunikovat	k5eAaImIp1nS	komunikovat
pomocí	pomocí	k7c2	pomocí
pachů	pach	k1gInPc2	pach
<g/>
,	,	kIx,	,
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
vizuálně	vizuálně	k6eAd1	vizuálně
<g/>
.	.	kIx.	.
</s>
<s>
Pachy	pach	k1gInPc4	pach
produkuje	produkovat	k5eAaImIp3nS	produkovat
moč	moč	k1gFnSc1	moč
<g/>
,	,	kIx,	,
anální	anální	k2eAgFnSc1d1	anální
žláza	žláza	k1gFnSc1	žláza
a	a	k8xC	a
výkaly	výkal	k1gInPc1	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
čichové	čichový	k2eAgFnSc2d1	čichová
stopy	stopa	k1gFnSc2	stopa
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
výdrž	výdrž	k1gFnSc1	výdrž
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
rozeznat	rozeznat	k5eAaPmF	rozeznat
jí	jíst	k5eAaImIp3nS	jíst
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
viditelnost	viditelnost	k1gFnSc4	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
levhartí	levhartí	k2eAgInSc1d1	levhartí
pach	pach	k1gInSc1	pach
zachytitelný	zachytitelný	k2eAgInSc1d1	zachytitelný
asi	asi	k9	asi
v	v	k7c4	v
30	[number]	k4	30
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
se	se	k3xPyFc4	se
rádi	rád	k2eAgMnPc1d1	rád
válejí	válet	k5eAaImIp3nP	válet
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
moči	moč	k1gFnSc6	moč
a	a	k8xC	a
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
přímorožce	přímorožec	k1gMnSc2	přímorožec
jihoafrického	jihoafrický	k2eAgMnSc2d1	jihoafrický
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
prvního	první	k4xOgNnSc2	první
chování	chování	k1gNnSc2	chování
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
lepší	dobrý	k2eAgNnSc4d2	lepší
šíření	šíření	k1gNnSc4	šíření
vlastních	vlastní	k2eAgInPc2d1	vlastní
chemických	chemický	k2eAgInPc2d1	chemický
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
důvod	důvod	k1gInSc1	důvod
druhého	druhý	k4xOgMnSc4	druhý
je	on	k3xPp3gInPc4	on
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Joy	Joy	k?	Joy
Adamsonová	Adamsonová	k1gFnSc1	Adamsonová
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
levhartice	levhartice	k1gFnSc1	levhartice
Penny	penny	k1gFnSc1	penny
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
a	a	k8xC	a
navracela	navracet	k5eAaImAgFnS	navracet
do	do	k7c2	do
divočiny	divočina	k1gFnSc2	divočina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ráda	rád	k2eAgFnSc1d1	ráda
válela	válet	k5eAaImAgFnS	válet
v	v	k7c6	v
buvolím	buvolí	k2eAgInSc6d1	buvolí
a	a	k8xC	a
sloním	sloní	k2eAgInSc6d1	sloní
trusu	trus	k1gInSc6	trus
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
tichá	tichý	k2eAgFnSc1d1	tichá
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
vydává	vydávat	k5eAaPmIp3nS	vydávat
tato	tento	k3xDgFnSc1	tento
šelma	šelma	k1gFnSc1	šelma
různorodé	různorodý	k2eAgInPc1d1	různorodý
zvuky	zvuk	k1gInPc7	zvuk
<g/>
:	:	kIx,	:
řev	řev	k1gInSc1	řev
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
připomínající	připomínající	k2eAgInSc1d1	připomínající
štěkání	štěkání	k1gNnSc4	štěkání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
vokalizace	vokalizace	k1gFnSc1	vokalizace
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
řezání	řezání	k1gNnSc1	řezání
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Řvaní	řvaní	k1gNnSc1	řvaní
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
tygřímu	tygří	k2eAgNnSc3d1	tygří
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
podstatně	podstatně	k6eAd1	podstatně
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
levhart	levhart	k1gMnSc1	levhart
vydávat	vydávat	k5eAaPmF	vydávat
kdykoliv	kdykoliv	k6eAd1	kdykoliv
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
častější	častý	k2eAgInSc1d2	častější
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
či	či	k8xC	či
svítání	svítání	k1gNnSc2	svítání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
důležitý	důležitý	k2eAgInSc4d1	důležitý
teritoriální	teritoriální	k2eAgInSc4d1	teritoriální
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
levhart	levhart	k1gMnSc1	levhart
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
specifický	specifický	k2eAgInSc4d1	specifický
hlasový	hlasový	k2eAgInSc4d1	hlasový
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
rozeznatelný	rozeznatelný	k2eAgMnSc1d1	rozeznatelný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
dokáží	dokázat	k5eAaPmIp3nP	dokázat
levharti	levhart	k1gMnPc1	levhart
funět	funět	k5eAaImF	funět
(	(	kIx(	(
<g/>
přátelské	přátelský	k2eAgNnSc4d1	přátelské
setkání	setkání	k1gNnSc4	setkání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrnět	vrnět	k5eAaImF	vrnět
<g/>
,	,	kIx,	,
prskat	prskat	k5eAaImF	prskat
<g/>
,	,	kIx,	,
syčet	syčet	k5eAaImF	syčet
<g/>
,	,	kIx,	,
vrčet	vrčet	k5eAaImF	vrčet
a	a	k8xC	a
mňoukat	mňoukat	k5eAaImF	mňoukat
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
umějí	umět	k5eAaImIp3nP	umět
naříkat	naříkat	k5eAaBmF	naříkat
nízkým	nízký	k2eAgInSc7d1	nízký
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
především	především	k9	především
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
hladová	hladový	k2eAgFnSc1d1	hladová
nebo	nebo	k8xC	nebo
nespokojená	spokojený	k2eNgFnSc1d1	nespokojená
<g/>
.	.	kIx.	.
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1	vizuální
komunikace	komunikace	k1gFnSc1	komunikace
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
mimice	mimika	k1gFnSc6	mimika
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
cenění	cenění	k1gNnSc2	cenění
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
měnění	měnění	k1gNnSc1	měnění
pozice	pozice	k1gFnSc2	pozice
uší	ucho	k1gNnPc2	ucho
a	a	k8xC	a
pohybování	pohybování	k1gNnSc6	pohybování
ocasem	ocas	k1gInSc7	ocas
nebývá	bývat	k5eNaImIp3nS	bývat
u	u	k7c2	u
levhartů	levhart	k1gMnPc2	levhart
tak	tak	k6eAd1	tak
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
samotářská	samotářský	k2eAgNnPc4d1	samotářské
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
málokdy	málokdy	k6eAd1	málokdy
potkávají	potkávat	k5eAaImIp3nP	potkávat
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
takto	takto	k6eAd1	takto
"	"	kIx"	"
<g/>
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
"	"	kIx"	"
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
mládětem	mládě	k1gNnSc7	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
je	být	k5eAaImIp3nS	být
masožravec	masožravec	k1gMnSc1	masožravec
a	a	k8xC	a
predátor	predátor	k1gMnSc1	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uzpůsoben	uzpůsoben	k2eAgInSc1d1	uzpůsoben
k	k	k7c3	k
aktivnímu	aktivní	k2eAgNnSc3d1	aktivní
lapání	lapání	k1gNnSc3	lapání
kořisti	kořist	k1gFnSc2	kořist
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgFnPc4d1	silná
čelisti	čelist	k1gFnPc4	čelist
<g/>
,	,	kIx,	,
ostré	ostrý	k2eAgInPc4d1	ostrý
zatažitelné	zatažitelný	k2eAgInPc4d1	zatažitelný
drápy	dráp	k1gInPc4	dráp
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgInPc4d1	vynikající
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
svalnaté	svalnatý	k2eAgNnSc1d1	svalnaté
a	a	k8xC	a
pružné	pružný	k2eAgNnSc1d1	pružné
tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
účinné	účinný	k2eAgNnSc1d1	účinné
maskování	maskování	k1gNnSc1	maskování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potravní	potravní	k2eAgMnSc1d1	potravní
oportunista	oportunista	k1gMnSc1	oportunista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
loví	lovit	k5eAaImIp3nS	lovit
velkou	velký	k2eAgFnSc4d1	velká
škálu	škála	k1gFnSc4	škála
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
afričtí	africký	k2eAgMnPc1d1	africký
levharti	levhart	k1gMnPc1	levhart
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
jídelníčku	jídelníček	k1gInSc6	jídelníček
nejméně	málo	k6eAd3	málo
92	[number]	k4	92
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přidá	přidat	k5eAaPmIp3nS	přidat
asijská	asijský	k2eAgFnSc1d1	asijská
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
na	na	k7c4	na
vysoko	vysoko	k6eAd1	vysoko
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
studie	studie	k1gFnSc1	studie
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
levharti	levhart	k1gMnPc1	levhart
výrazně	výrazně	k6eAd1	výrazně
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
zvířata	zvíře	k1gNnPc4	zvíře
ve	v	k7c6	v
váhovém	váhový	k2eAgInSc6d1	váhový
rozsahu	rozsah	k1gInSc6	rozsah
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
kg	kg	kA	kg
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejčastější	častý	k2eAgFnSc1d3	nejčastější
hmotnost	hmotnost	k1gFnSc1	hmotnost
jejich	jejich	k3xOp3gFnSc2	jejich
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
široce	široko	k6eAd1	široko
pojatého	pojatý	k2eAgInSc2d1	pojatý
srovnávacího	srovnávací	k2eAgInSc2d1	srovnávací
výzkumu	výzkum	k1gInSc2	výzkum
jsou	být	k5eAaImIp3nP	být
nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
kořistí	kořist	k1gFnSc7	kořist
levhartů	levhart	k1gMnPc2	levhart
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
různí	různit	k5eAaImIp3nP	různit
kopytníci	kopytník	k1gMnPc1	kopytník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
především	především	k9	především
impala	impala	k1gFnSc1	impala
(	(	kIx(	(
<g/>
Aepyceros	Aepycerosa	k1gFnPc2	Aepycerosa
melampus	melampus	k1gInSc1	melampus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chocholatka	chocholatka	k1gFnSc1	chocholatka
křovinná	křovinný	k2eAgFnSc1d1	křovinná
(	(	kIx(	(
<g/>
Cephalophus	Cephalophus	k1gMnSc1	Cephalophus
natalensis	natalensis	k1gFnSc2	natalensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chocholatka	chocholatka	k1gFnSc1	chocholatka
schovávaná	schovávaná	k1gFnSc1	schovávaná
(	(	kIx(	(
<g/>
Sylvicapra	Sylvicapra	k1gFnSc1	Sylvicapra
grimmia	grimmia	k1gFnSc1	grimmia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antilopa	antilopa	k1gFnSc1	antilopa
travní	travní	k2eAgFnSc1d1	travní
(	(	kIx(	(
<g/>
Raphicerus	Raphicerus	k1gMnSc1	Raphicerus
campestris	campestris	k1gFnSc2	campestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lesoň	lesonit	k5eAaPmRp2nS	lesonit
(	(	kIx(	(
<g/>
Tragelaphus	Tragelaphus	k1gMnSc1	Tragelaphus
scriptus	scriptus	k1gMnSc1	scriptus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
bradavičnaté	bradavičnatý	k2eAgNnSc1d1	bradavičnaté
(	(	kIx(	(
<g/>
Phacochoerus	Phacochoerus	k1gInSc1	Phacochoerus
sp	sp	k?	sp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pakůň	pakůň	k1gMnSc1	pakůň
žíhaný	žíhaný	k2eAgMnSc1d1	žíhaný
(	(	kIx(	(
<g/>
Connochaetes	Connochaetes	k1gMnSc1	Connochaetes
taurinus	taurinus	k1gMnSc1	taurinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gazela	gazela	k1gFnSc1	gazela
Thomsonova	Thomsonův	k2eAgFnSc1d1	Thomsonova
(	(	kIx(	(
<g/>
Gazella	Gazella	k1gFnSc1	Gazella
thomsonii	thomsonium	k1gNnPc7	thomsonium
<g/>
)	)	kIx)	)
a	a	k8xC	a
kančil	kančit	k5eAaImAgInS	kančit
vodní	vodní	k2eAgInSc1d1	vodní
(	(	kIx(	(
<g/>
Hyemoschus	Hyemoschus	k1gInSc1	Hyemoschus
aquaticus	aquaticus	k1gInSc1	aquaticus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jelen	jelen	k1gMnSc1	jelen
čital	čital	k1gMnSc1	čital
neboli	neboli	k8xC	neboli
axis	axis	k6eAd1	axis
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Axis	Axis	k1gInSc1	Axis
axis	axis	k1gInSc1	axis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sambar	sambar	k1gMnSc1	sambar
(	(	kIx(	(
<g/>
Rusa	Rus	k1gMnSc4	Rus
sp	sp	k?	sp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
muntžak	muntžak	k1gMnSc1	muntžak
(	(	kIx(	(
<g/>
Muntiacus	Muntiacus	k1gMnSc1	Muntiacus
sp	sp	k?	sp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
levhart	levhart	k1gMnSc1	levhart
podle	podle	k7c2	podle
oblasti	oblast	k1gFnSc2	oblast
výskytu	výskyt	k1gInSc2	výskyt
loví	lovit	k5eAaImIp3nP	lovit
následující	následující	k2eAgNnPc1d1	následující
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
:	:	kIx,	:
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
hlodavců	hlodavec	k1gMnPc2	hlodavec
od	od	k7c2	od
myší	myš	k1gFnPc2	myš
a	a	k8xC	a
hrabošů	hraboš	k1gMnPc2	hraboš
po	po	k7c4	po
krysy	krysa	k1gFnPc4	krysa
<g/>
,	,	kIx,	,
zajícovce	zajícovec	k1gInPc4	zajícovec
<g/>
,	,	kIx,	,
místní	místní	k2eAgInPc4d1	místní
druhy	druh	k1gInPc4	druh
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
srnců	srnec	k1gMnPc2	srnec
a	a	k8xC	a
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
damany	daman	k1gMnPc4	daman
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgMnPc4d1	divoký
ovce	ovce	k1gFnSc2	ovce
a	a	k8xC	a
kozy	koza	k1gFnSc2	koza
a	a	k8xC	a
dikobraze	dikobraz	k1gMnSc5	dikobraz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
jde	jít	k5eAaImIp3nS	jít
nejčastěji	často	k6eAd3	často
o	o	k7c4	o
holuby	holub	k1gMnPc4	holub
<g/>
,	,	kIx,	,
perličky	perlička	k1gFnPc4	perlička
<g/>
,	,	kIx,	,
koroptve	koroptev	k1gFnPc4	koroptev
<g/>
,	,	kIx,	,
pávy	páv	k1gMnPc4	páv
<g/>
,	,	kIx,	,
bažanty	bažant	k1gMnPc4	bažant
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
pštrosy	pštros	k1gMnPc4	pštros
<g/>
,	,	kIx,	,
supy	sup	k1gMnPc4	sup
a	a	k8xC	a
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
schopnosti	schopnost	k1gFnSc3	schopnost
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
šplhat	šplhat	k5eAaImF	šplhat
je	on	k3xPp3gFnPc4	on
levhart	levhart	k1gMnSc1	levhart
zřejmě	zřejmě	k6eAd1	zřejmě
největším	veliký	k2eAgMnSc7d3	veliký
přirozeným	přirozený	k2eAgMnSc7d1	přirozený
nepřítelem	nepřítel	k1gMnSc7	nepřítel
primátů	primát	k1gMnPc2	primát
–	–	k?	–
napadá	napadat	k5eAaImIp3nS	napadat
především	především	k9	především
hulmany	hulman	k1gMnPc4	hulman
<g/>
,	,	kIx,	,
makaky	makak	k1gMnPc4	makak
<g/>
,	,	kIx,	,
guerézy	gueréza	k1gFnPc4	gueréza
<g/>
,	,	kIx,	,
kočkodany	kočkodan	k1gMnPc4	kočkodan
<g/>
,	,	kIx,	,
mangabeje	mangabej	k1gMnPc4	mangabej
<g/>
,	,	kIx,	,
šimpanze	šimpanz	k1gMnPc4	šimpanz
<g/>
,	,	kIx,	,
bonoby	bonoba	k1gFnPc4	bonoba
<g/>
,	,	kIx,	,
paviány	pavián	k1gMnPc4	pavián
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
šelmy	šelma	k1gFnPc4	šelma
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
promyky	promyka	k1gFnPc1	promyka
<g/>
,	,	kIx,	,
cibetky	cibetka	k1gFnPc1	cibetka
<g/>
,	,	kIx,	,
ženetky	ženetka	k1gFnPc1	ženetka
a	a	k8xC	a
jezevci	jezevec	k1gMnPc1	jezevec
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
si	se	k3xPyFc3	se
troufne	troufnout	k5eAaPmIp3nS	troufnout
na	na	k7c4	na
medojeda	medojed	k1gMnSc4	medojed
nebo	nebo	k8xC	nebo
geparda	gepard	k1gMnSc4	gepard
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
i	i	k9	i
na	na	k7c4	na
mladého	mladý	k2eAgMnSc4d1	mladý
medvěda	medvěd	k1gMnSc4	medvěd
ušatého	ušatý	k2eAgInSc2d1	ušatý
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gMnSc1	Ursus
thibetanus	thibetanus	k1gMnSc1	thibetanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obzvláštní	obzvláštní	k2eAgFnSc6d1	obzvláštní
oblibě	obliba	k1gFnSc6	obliba
má	mít	k5eAaImIp3nS	mít
psovité	psovitý	k2eAgMnPc4d1	psovitý
šelmy	šelma	k1gMnPc4	šelma
–	–	k?	–
napadá	napadat	k5eAaPmIp3nS	napadat
šakaly	šakal	k1gMnPc4	šakal
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc4	liška
<g/>
,	,	kIx,	,
dhouly	dhoul	k1gMnPc4	dhoul
<g/>
,	,	kIx,	,
psíky	psík	k1gMnPc4	psík
mývalovité	mývalovitý	k2eAgMnPc4d1	mývalovitý
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kořist	kořist	k1gFnSc4	kořist
mu	on	k3xPp3gInSc3	on
mnohdy	mnohdy	k6eAd1	mnohdy
padnou	padnout	k5eAaPmIp3nP	padnout
i	i	k9	i
různí	různý	k2eAgMnPc1d1	různý
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
varani	varan	k1gMnPc1	varan
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
dostupnosti	dostupnost	k1gFnPc4	dostupnost
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
faktorech	faktor	k1gInPc6	faktor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
kořisti	kořist	k1gFnSc2	kořist
či	či	k8xC	či
samotného	samotný	k2eAgMnSc2d1	samotný
lovce	lovec	k1gMnSc2	lovec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
výrazně	výrazně	k6eAd1	výrazně
preferovali	preferovat	k5eAaImAgMnP	preferovat
dikobraze	dikobraz	k1gMnSc5	dikobraz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
které	který	k3yQgNnSc1	který
"	"	kIx"	"
<g/>
bavilo	bavit	k5eAaImAgNnS	bavit
<g/>
"	"	kIx"	"
lovit	lovit	k5eAaImF	lovit
veverky	veverka	k1gFnPc4	veverka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
levhart	levhart	k1gMnSc1	levhart
z	z	k7c2	z
Ngorongoro	Ngorongora	k1gFnSc5	Ngorongora
zabil	zabít	k5eAaPmAgMnS	zabít
a	a	k8xC	a
snědl	sníst	k5eAaPmAgMnS	sníst
během	během	k7c2	během
21	[number]	k4	21
dnů	den	k1gInPc2	den
11	[number]	k4	11
šakalů	šakal	k1gMnPc2	šakal
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgMnPc2d2	veliký
živočichů	živočich	k1gMnPc2	živočich
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zabít	zabít	k5eAaPmF	zabít
mladé	mladý	k2eAgMnPc4d1	mladý
buvoly	buvol	k1gMnPc4	buvol
<g/>
,	,	kIx,	,
žirafy	žirafa	k1gFnPc4	žirafa
<g/>
,	,	kIx,	,
velbloudy	velbloud	k1gMnPc4	velbloud
a	a	k8xC	a
velké	velký	k2eAgMnPc4d1	velký
druhy	druh	k1gMnPc4	druh
antilop	antilopa	k1gFnPc2	antilopa
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
napadne	napadnout	k5eAaPmIp3nS	napadnout
i	i	k9	i
takové	takový	k3xDgNnSc1	takový
zvíře	zvíře	k1gNnSc1	zvíře
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
gorila	gorila	k1gFnSc1	gorila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
bývá	bývat	k5eAaImIp3nS	bývat
jeho	jeho	k3xOp3gFnSc7	jeho
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
kořistí	kořist	k1gFnSc7	kořist
banteng	bantenga	k1gFnPc2	bantenga
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vážící	vážící	k2eAgMnSc1d1	vážící
okolo	okolo	k7c2	okolo
500	[number]	k4	500
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
zaznamenaný	zaznamenaný	k2eAgInSc4d1	zaznamenaný
úlovek	úlovek	k1gInSc4	úlovek
byla	být	k5eAaImAgFnS	být
900	[number]	k4	900
kg	kg	kA	kg
těžká	těžký	k2eAgFnSc1d1	těžká
antilopa	antilopa	k1gFnSc1	antilopa
Derbyho	Derby	k1gMnSc2	Derby
(	(	kIx(	(
<g/>
Taurotragus	Taurotragus	k1gMnSc1	Taurotragus
derbianus	derbianus	k1gMnSc1	derbianus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
velké	velký	k2eAgFnSc2d1	velká
kořisti	kořist	k1gFnSc2	kořist
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
C.	C.	kA	C.
Eloff	Eloff	k1gMnSc1	Eloff
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
studoval	studovat	k5eAaImAgMnS	studovat
kalaharské	kalaharský	k2eAgInPc4d1	kalaharský
lvy	lev	k1gInPc4	lev
<g/>
,	,	kIx,	,
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
asi	asi	k9	asi
175	[number]	k4	175
kg	kg	kA	kg
těžkého	těžký	k2eAgMnSc4d1	těžký
přímorožce	přímorožec	k1gMnSc4	přímorožec
jihoafrického	jihoafrický	k2eAgMnSc4d1	jihoafrický
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
rozích	roh	k1gInPc6	roh
napíchnutého	napíchnutý	k2eAgMnSc2d1	napíchnutý
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
již	již	k6eAd1	již
rozloženého	rozložený	k2eAgMnSc2d1	rozložený
levharta	levhart	k1gMnSc2	levhart
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
populace	populace	k1gFnSc2	populace
levharta	levhart	k1gMnSc2	levhart
změnit	změnit	k5eAaPmF	změnit
druh	druh	k1gInSc4	druh
preferované	preferovaný	k2eAgFnSc2d1	preferovaná
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
provincii	provincie	k1gFnSc6	provincie
Sečuán	Sečuán	k1gMnSc1	Sečuán
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
výzkumu	výzkum	k1gInSc2	výzkum
publikovaného	publikovaný	k2eAgInSc2d1	publikovaný
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
hlavní	hlavní	k2eAgFnSc7d1	hlavní
kořistí	kořist	k1gFnSc7	kořist
nejprve	nejprve	k6eAd1	nejprve
muntžak	muntžak	k6eAd1	muntžak
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Elaphodus	Elaphodus	k1gMnSc1	Elaphodus
cephalophus	cephalophus	k1gMnSc1	cephalophus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
levharti	levhart	k1gMnPc1	levhart
přeorientovali	přeorientovat	k5eAaPmAgMnP	přeorientovat
na	na	k7c4	na
hlodouna	hlodoun	k1gMnSc4	hlodoun
čínského	čínský	k2eAgMnSc2d1	čínský
(	(	kIx(	(
<g/>
Rhizomys	Rhizomys	k1gInSc1	Rhizomys
sinense	sinense	k1gFnSc2	sinense
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
blízko	blízko	k6eAd1	blízko
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
napadají	napadat	k5eAaPmIp3nP	napadat
často	často	k6eAd1	často
domácí	domácí	k2eAgNnPc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
od	od	k7c2	od
koček	kočka	k1gFnPc2	kočka
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnPc4	ovce
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc4	koza
až	až	k9	až
po	po	k7c4	po
skot	skot	k1gInSc4	skot
a	a	k8xC	a
vodní	vodní	k2eAgMnPc4d1	vodní
buvoly	buvol	k1gMnPc4	buvol
<g/>
.	.	kIx.	.
</s>
<s>
Psy	pes	k1gMnPc4	pes
domácí	domácí	k2eAgMnPc4d1	domácí
<g/>
,	,	kIx,	,
záhadně	záhadně	k6eAd1	záhadně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
mizící	mizící	k2eAgInSc4d1	mizící
z	z	k7c2	z
rančů	ranč	k1gInPc2	ranč
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
levhart	levhart	k1gMnSc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2007	[number]	k4	2007
až	až	k6eAd1	až
2009	[number]	k4	2009
provedeného	provedený	k2eAgMnSc2d1	provedený
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západní	západní	k2eAgInPc4d1	západní
Maháráštry	Maháráštr	k1gInPc4	Maháráštr
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
psi	pes	k1gMnPc1	pes
39	[number]	k4	39
%	%	kIx~	%
zdejší	zdejší	k2eAgFnSc2d1	zdejší
levhartí	levhartí	k2eAgFnSc2d1	levhartí
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
ani	ani	k8xC	ani
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
obojživelníky	obojživelník	k1gMnPc7	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Stravu	strava	k1gFnSc4	strava
si	se	k3xPyFc3	se
někdy	někdy	k6eAd1	někdy
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vodními	vodní	k2eAgInPc7d1	vodní
melouny	meloun	k1gInPc7	meloun
či	či	k8xC	či
okurkami	okurka	k1gFnPc7	okurka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
pouště	poušť	k1gFnSc2	poušť
Kalahari	Kalahar	k1gMnPc7	Kalahar
takto	takto	k6eAd1	takto
levharti	levhart	k1gMnPc1	levhart
získávají	získávat	k5eAaImIp3nP	získávat
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
mezi	mezi	k7c4	mezi
levharty	levhart	k1gMnPc4	levhart
ke	k	k7c3	k
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
infanticidy	infanticida	k1gFnSc2	infanticida
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
loveckém	lovecký	k2eAgInSc6d1	lovecký
okrsku	okrsek	k1gInSc6	okrsek
levharta	levhart	k1gMnSc4	levhart
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
větší	veliký	k2eAgMnPc1d2	veliký
konkurenti	konkurent	k1gMnPc1	konkurent
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
kořisti	kořist	k1gFnSc2	kořist
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
posunu	posun	k1gInSc3	posun
dojde	dojít	k5eAaPmIp3nS	dojít
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
levhartovi	levhartův	k2eAgMnPc1d1	levhartův
konkurují	konkurovat	k5eAaImIp3nP	konkurovat
lidé	člověk	k1gMnPc1	člověk
lovící	lovící	k2eAgNnPc4d1	lovící
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
kvůli	kvůli	k7c3	kvůli
masu	maso	k1gNnSc3	maso
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
loví	lovit	k5eAaImIp3nP	lovit
častěji	často	k6eAd2	často
a	a	k8xC	a
menší	malý	k2eAgNnPc1d2	menší
zvířata	zvíře	k1gNnPc1	zvíře
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Konzumace	konzumace	k1gFnSc2	konzumace
kořisti	kořist	k1gFnSc2	kořist
====	====	k?	====
</s>
</p>
<p>
<s>
Když	když	k8xS	když
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
se	se	k3xPyFc4	se
levhart	levhart	k1gMnSc1	levhart
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ji	on	k3xPp3gFnSc4	on
nezkonzumuje	zkonzumovat	k5eNaPmIp3nS	zkonzumovat
celou	celý	k2eAgFnSc4d1	celá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgMnPc2d2	veliký
živočichů	živočich	k1gMnPc2	živočich
zanechá	zanechat	k5eAaPmIp3nS	zanechat
mnohdy	mnohdy	k6eAd1	mnohdy
jen	jen	k9	jen
bachor	bachor	k1gInSc1	bachor
<g/>
,	,	kIx,	,
rohy	roh	k1gInPc1	roh
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
a	a	k8xC	a
kopyta	kopyto	k1gNnPc1	kopyto
<g/>
.	.	kIx.	.
</s>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc1	maso
již	již	k6eAd1	již
hnijící	hnijící	k2eAgFnPc4d1	hnijící
a	a	k8xC	a
plné	plný	k2eAgFnPc4d1	plná
červů	červ	k1gMnPc2	červ
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
neulovené	ulovený	k2eNgFnPc4d1	ulovený
mršiny	mršina	k1gFnPc4	mršina
pojídá	pojídat	k5eAaImIp3nS	pojídat
rovněž	rovněž	k9	rovněž
<g/>
.	.	kIx.	.
</s>
<s>
Zvíře	zvíře	k1gNnSc1	zvíře
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
již	již	k6eAd1	již
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
například	například	k6eAd1	například
prase	prase	k1gNnSc1	prase
bradavičnaté	bradavičnatý	k2eAgNnSc1d1	bradavičnaté
požíral	požírat	k5eAaImAgMnS	požírat
zaživa	zaživa	k6eAd1	zaživa
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1	[number]	k4	1
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
kg	kg	kA	kg
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
sníst	sníst	k5eAaPmF	sníst
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
své	svůj	k3xOyFgFnSc2	svůj
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
dny	den	k1gInPc4	den
hladovět	hladovět	k5eAaImF	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
lovu	lov	k1gInSc2	lov
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
naposledy	naposledy	k6eAd1	naposledy
zabité	zabitý	k2eAgFnSc2d1	zabitá
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
(	(	kIx(	(
<g/>
častější	častý	k2eAgInSc4d2	častější
lov	lov	k1gInSc4	lov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
uchránit	uchránit	k5eAaPmF	uchránit
mršinu	mršina	k1gFnSc4	mršina
před	před	k7c7	před
konkurenty	konkurent	k1gMnPc7	konkurent
a	a	k8xC	a
mrchožrouty	mrchožrout	k1gMnPc7	mrchožrout
<g/>
.	.	kIx.	.
</s>
<s>
Ukrývání	ukrývání	k1gNnSc1	ukrývání
kořisti	kořist	k1gFnSc2	kořist
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
častý	častý	k2eAgInSc4d1	častý
postup	postup	k1gInSc4	postup
po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
si	se	k3xPyFc3	se
levharti	levhart	k1gMnPc1	levhart
mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
těla	tělo	k1gNnPc4	tělo
zvířat	zvíře	k1gNnPc2	zvíře
vynášejí	vynášet	k5eAaImIp3nP	vynášet
většinou	většinou	k6eAd1	většinou
do	do	k7c2	do
korun	koruna	k1gFnPc2	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
preferují	preferovat	k5eAaImIp3nP	preferovat
spíše	spíše	k9	spíše
hustou	hustý	k2eAgFnSc4d1	hustá
vegetaci	vegetace	k1gFnSc4	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
však	však	k9	však
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
přítomnosti	přítomnost	k1gFnSc6	přítomnost
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
šelem	šelma	k1gFnPc2	šelma
a	a	k8xC	a
mrchožroutů	mrchožrout	k1gMnPc2	mrchožrout
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
NP	NP	kA	NP
Matopos	Matopos	k1gInSc4	Matopos
v	v	k7c6	v
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nežijí	žít	k5eNaImIp3nP	žít
šakali	šakal	k1gMnPc1	šakal
a	a	k8xC	a
hyeny	hyena	k1gFnPc1	hyena
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
38	[number]	k4	38
úlovků	úlovek	k1gInPc2	úlovek
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
vytažen	vytažen	k2eAgMnSc1d1	vytažen
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
Sabi	Sab	k1gFnSc2	Sab
Sand	Sand	k1gInSc1	Sand
Game	game	k1gInSc4	game
Reserve	Reserev	k1gFnSc2	Reserev
vynášejí	vynášet	k5eAaImIp3nP	vynášet
na	na	k7c4	na
strom	strom	k1gInSc4	strom
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgMnPc2	všecek
úlovků	úlovek	k1gInPc2	úlovek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lovecká	lovecký	k2eAgFnSc1d1	lovecká
taktika	taktika	k1gFnSc1	taktika
a	a	k8xC	a
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
====	====	k?	====
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
používají	používat	k5eAaImIp3nP	používat
několik	několik	k4yIc4	několik
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zmocnit	zmocnit	k5eAaPmF	zmocnit
kořisti	kořist	k1gFnSc3	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
přikrčeni	přikrčit	k5eAaPmNgMnP	přikrčit
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
využívajíce	využívat	k5eAaImSgFnP	využívat
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
přiblížit	přiblížit	k5eAaPmF	přiblížit
na	na	k7c4	na
co	co	k3yQnSc4	co
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
k	k	k7c3	k
vyhlédnuté	vyhlédnutý	k2eAgFnSc3d1	vyhlédnutá
oběti	oběť	k1gFnSc3	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
okamžiky	okamžik	k1gInPc1	okamžik
nehýbat	hýbat	k5eNaImF	hýbat
<g/>
,	,	kIx,	,
pozorujíce	pozorovat	k5eAaImSgMnP	pozorovat
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
kořist	kořist	k1gFnSc1	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Plížení	plížení	k1gNnPc1	plížení
bývají	bývat	k5eAaImIp3nP	bývat
různě	různě	k6eAd1	různě
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
pohybu	pohyb	k1gInSc6	pohyb
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Kalahari	Kalahari	k1gNnSc2	Kalahari
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
stopovací	stopovací	k2eAgFnSc1d1	stopovací
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
i	i	k9	i
3400	[number]	k4	3400
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
lovy	lov	k1gInPc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
travnatých	travnatý	k2eAgFnPc6d1	travnatá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
lepšího	dobrý	k2eAgInSc2d2	lepší
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
detekovat	detekovat	k5eAaImF	detekovat
plížícího	plížící	k2eAgInSc2d1	plížící
se	se	k3xPyFc4	se
levharta	levhart	k1gMnSc4	levhart
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgFnSc1d2	hustší
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
snadněji	snadno	k6eAd2	snadno
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dostat	dostat	k5eAaPmF	dostat
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
metodou	metoda	k1gFnSc7	metoda
lovu	lov	k1gInSc2	lov
je	být	k5eAaImIp3nS	být
ležení	ležení	k1gNnSc4	ležení
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
a	a	k8xC	a
čekání	čekání	k1gNnSc6	čekání
<g/>
,	,	kIx,	,
až	až	k8xS	až
kořist	kořist	k1gFnSc1	kořist
sama	sám	k3xTgMnSc4	sám
dorazí	dorazit	k5eAaPmIp3nS	dorazit
na	na	k7c4	na
co	co	k3yQnSc4	co
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c6	o
křoví	křoví	k1gNnSc6	křoví
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
trávu	tráva	k1gFnSc4	tráva
<g/>
,	,	kIx,	,
skálu	skála	k1gFnSc4	skála
či	či	k8xC	či
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
uplatňovanou	uplatňovaný	k2eAgFnSc7d1	uplatňovaná
především	především	k6eAd1	především
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
kořist	kořist	k1gFnSc4	kořist
aktivně	aktivně	k6eAd1	aktivně
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
ohrad	ohrada	k1gFnPc2	ohrada
<g/>
,	,	kIx,	,
chlívků	chlívek	k1gInPc2	chlívek
či	či	k8xC	či
stájí	stáj	k1gFnPc2	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Primáty	primát	k1gInPc1	primát
mnohdy	mnohdy	k6eAd1	mnohdy
uchvátí	uchvátit	k5eAaPmIp3nP	uchvátit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
předstírá	předstírat	k5eAaImIp3nS	předstírat
výpad	výpad	k1gInSc1	výpad
do	do	k7c2	do
korun	koruna	k1gFnPc2	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
opice	opice	k1gFnPc1	opice
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zpanikaří	zpanikařit	k5eAaPmIp3nP	zpanikařit
a	a	k8xC	a
seskočí	seskočit	k5eAaPmIp3nP	seskočit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
levhart	levhart	k1gMnSc1	levhart
snadněji	snadno	k6eAd2	snadno
chytí	chytit	k5eAaPmIp3nS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
levhart	levhart	k1gMnSc1	levhart
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
možné	možný	k2eAgFnSc2d1	možná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
jen	jen	k9	jen
na	na	k7c4	na
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
či	či	k8xC	či
dvěma	dva	k4xCgInPc7	dva
skoky	skok	k1gInPc7	skok
se	se	k3xPyFc4	se
kořisti	kořist	k1gFnSc2	kořist
zmocní	zmocnit	k5eAaPmIp3nS	zmocnit
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
ji	on	k3xPp3gFnSc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
cca	cca	kA	cca
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
typu	typ	k1gInSc6	typ
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
terénu	terén	k1gInSc6	terén
Kalahari	Kalahar	k1gFnSc2	Kalahar
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
běhu	běh	k1gInSc2	běh
za	za	k7c7	za
kořistí	kořist	k1gFnSc7	kořist
64	[number]	k4	64
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnPc1d2	menší
zvířata	zvíře	k1gNnPc1	zvíře
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
prokousnutím	prokousnutí	k1gNnSc7	prokousnutí
šíje	šíj	k1gFnSc2	šíj
či	či	k8xC	či
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
větší	veliký	k2eAgMnSc1d2	veliký
obvykle	obvykle	k6eAd1	obvykle
zardousí	zardousit	k5eAaPmIp3nS	zardousit
silným	silný	k2eAgInSc7d1	silný
stiskem	stisk	k1gInSc7	stisk
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
oběť	oběť	k1gFnSc1	oběť
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
prokousnutím	prokousnutí	k1gNnSc7	prokousnutí
jejího	její	k3xOp3gInSc2	její
čumáku	čumák	k1gInSc2	čumák
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
lovu	lov	k1gInSc2	lov
znovu	znovu	k6eAd1	znovu
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
5	[number]	k4	5
%	%	kIx~	%
do	do	k7c2	do
38,1	[number]	k4	38,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnPc4	rozmnožování
<g/>
,	,	kIx,	,
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
dožití	dožití	k1gNnSc4	dožití
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Páření	páření	k1gNnSc2	páření
====	====	k?	====
</s>
</p>
<p>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
levhartů	levhart	k1gMnPc2	levhart
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
asijských	asijský	k2eAgFnPc2d1	asijská
populací	populace	k1gFnPc2	populace
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
prakticky	prakticky	k6eAd1	prakticky
celoročně	celoročně	k6eAd1	celoročně
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
dávají	dávat	k5eAaImIp3nP	dávat
samcům	samec	k1gMnPc3	samec
najevo	najevo	k6eAd1	najevo
připravenost	připravenost	k1gFnSc4	připravenost
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
svým	svůj	k3xOyFgInSc7	svůj
pachem	pach	k1gInSc7	pach
<g/>
.	.	kIx.	.
</s>
<s>
Estrus	Estrus	k1gInSc1	Estrus
trvá	trvat	k5eAaImIp3nS	trvat
obvykle	obvykle	k6eAd1	obvykle
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
zabřeznutí	zabřeznutí	k1gNnSc3	zabřeznutí
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
říje	říje	k1gFnPc4	říje
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
samec	samec	k1gMnSc1	samec
najde	najít	k5eAaPmIp3nS	najít
partnerku	partnerka	k1gFnSc4	partnerka
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
seznamovacím	seznamovací	k2eAgInPc3d1	seznamovací
rituálům	rituál	k1gInPc3	rituál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
většinou	většinou	k6eAd1	většinou
vrčením	vrčení	k1gNnSc7	vrčení
a	a	k8xC	a
mírnou	mírný	k2eAgFnSc7d1	mírná
agresí	agrese	k1gFnSc7	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
zvířata	zvíře	k1gNnPc1	zvíře
již	již	k6eAd1	již
znají	znát	k5eAaImIp3nP	znát
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
před	před	k7c7	před
samcem	samec	k1gInSc7	samec
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
válí	válet	k5eAaImIp3nS	válet
<g/>
,	,	kIx,	,
otírá	otírat	k5eAaImIp3nS	otírat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
svůj	svůj	k3xOyFgInSc4	svůj
pach	pach	k1gInSc4	pach
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgInSc6	ten
sténá	sténat	k5eAaImIp3nS	sténat
či	či	k8xC	či
vrčí	vrčet	k5eAaImIp3nS	vrčet
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přikrčí	přikrčit	k5eAaPmIp3nP	přikrčit
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
samec	samec	k1gInSc4	samec
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
vleze	vlézt	k5eAaPmIp3nS	vlézt
seshora	seshora	k6eAd1	seshora
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
<s>
Kopulace	kopulace	k1gFnSc1	kopulace
trvá	trvat	k5eAaImIp3nS	trvat
okolo	okolo	k7c2	okolo
3	[number]	k4	3
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ejakulace	ejakulace	k1gFnSc2	ejakulace
kouše	kousat	k5eAaImIp3nS	kousat
samec	samec	k1gInSc1	samec
samici	samice	k1gFnSc4	samice
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
náznakem	náznak	k1gInSc7	náznak
výpadu	výpad	k1gInSc2	výpad
otočí	otočit	k5eAaPmIp3nS	otočit
a	a	k8xC	a
samec	samec	k1gMnSc1	samec
odskočí	odskočit	k5eAaPmIp3nS	odskočit
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
penis	penis	k1gInSc1	penis
opatřený	opatřený	k2eAgInSc1d1	opatřený
malými	malý	k2eAgInPc7d1	malý
zpětnými	zpětný	k2eAgInPc7d1	zpětný
háčky	háček	k1gInPc7	háček
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
sice	sice	k8xC	sice
samici	samice	k1gFnSc4	samice
bolí	bolet	k5eAaImIp3nP	bolet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
ovulaci	ovulace	k1gFnSc4	ovulace
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
kopulacemi	kopulace	k1gFnPc7	kopulace
bývají	bývat	k5eAaImIp3nP	bývat
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnPc1	spojení
bývají	bývat	k5eAaImIp3nP	bývat
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
hlasitým	hlasitý	k2eAgNnSc7d1	hlasité
vrčením	vrčení	k1gNnSc7	vrčení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgNnPc4d1	časté
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k4xCyI	pár
jich	on	k3xPp3gInPc2	on
měl	mít	k5eAaImAgMnS	mít
60	[number]	k4	60
během	během	k7c2	během
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
pozorování	pozorování	k1gNnPc2	pozorování
byla	být	k5eAaImAgFnS	být
vypočtena	vypočten	k2eAgFnSc1d1	vypočtena
frekvence	frekvence	k1gFnSc1	frekvence
100	[number]	k4	100
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgInPc4d1	odlišný
údaje	údaj	k1gInPc4	údaj
přinesla	přinést	k5eAaPmAgFnS	přinést
studie	studie	k1gFnSc1	studie
levhartů	levhart	k1gMnPc2	levhart
indických	indický	k2eAgMnPc2d1	indický
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
provedená	provedený	k2eAgFnSc1d1	provedená
na	na	k7c4	na
38	[number]	k4	38
jedincích	jedinec	k1gMnPc6	jedinec
(	(	kIx(	(
<g/>
20	[number]	k4	20
samců	samec	k1gInPc2	samec
<g/>
,	,	kIx,	,
18	[number]	k4	18
samic	samice	k1gFnPc2	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
trvala	trvat	k5eAaImAgFnS	trvat
průměrná	průměrný	k2eAgFnSc1d1	průměrná
kopulace	kopulace	k1gFnSc1	kopulace
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
intervaly	interval	k1gInPc4	interval
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
spojeními	spojení	k1gNnPc7	spojení
byly	být	k5eAaImAgInP	být
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
181	[number]	k4	181
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
dvojice	dvojice	k1gFnSc1	dvojice
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
jeden	jeden	k4xCgMnSc1	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k8xS	až
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
spolu	spolu	k6eAd1	spolu
pár	pár	k4xCyI	pár
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Samcova	samcův	k2eAgFnSc1d1	Samcova
role	role	k1gFnSc1	role
pářením	páření	k1gNnSc7	páření
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
na	na	k7c6	na
výchově	výchova	k1gFnSc6	výchova
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nepodílí	podílet	k5eNaImIp3nS	podílet
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
nakrátko	nakrátko	k6eAd1	nakrátko
přidružil	přidružit	k5eAaPmAgMnS	přidružit
k	k	k7c3	k
samici	samice	k1gFnSc3	samice
a	a	k8xC	a
svým	svůj	k3xOyFgMnPc3	svůj
potomkům	potomek	k1gMnPc3	potomek
<g/>
.	.	kIx.	.
<g/>
Výjimečně	výjimečně	k6eAd1	výjimečně
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
situacím	situace	k1gFnPc3	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
tři	tři	k4xCgMnPc1	tři
levharti	levhart	k1gMnPc1	levhart
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dosud	dosud	k6eAd1	dosud
zaznamenaných	zaznamenaný	k2eAgInPc6d1	zaznamenaný
případech	případ	k1gInPc6	případ
trojic	trojice	k1gFnPc2	trojice
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
levharti	levhart	k1gMnPc1	levhart
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
příbuzní	příbuzný	k1gMnPc1	příbuzný
(	(	kIx(	(
<g/>
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
otec-syn	otecyn	k1gInSc1	otec-syn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pářením	páření	k1gNnSc7	páření
s	s	k7c7	s
více	hodně	k6eAd2	hodně
samci	samec	k1gInPc7	samec
chce	chtít	k5eAaImIp3nS	chtít
samice	samice	k1gFnSc1	samice
omezit	omezit	k5eAaPmF	omezit
následnou	následný	k2eAgFnSc4d1	následná
možnost	možnost	k1gFnSc4	možnost
infanticidy	infanticida	k1gFnSc2	infanticida
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
usmrcení	usmrcení	k1gNnSc2	usmrcení
svých	svůj	k3xOyFgNnPc2	svůj
budoucích	budoucí	k2eAgNnPc2d1	budoucí
mláďat	mládě	k1gNnPc2	mládě
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
rozeznat	rozeznat	k5eAaPmF	rozeznat
silného	silný	k2eAgMnSc4d1	silný
a	a	k8xC	a
dominantního	dominantní	k2eAgMnSc4d1	dominantní
samce	samec	k1gMnSc4	samec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
rovněž	rovněž	k9	rovněž
snižuje	snižovat	k5eAaImIp3nS	snižovat
možnost	možnost	k1gFnSc4	možnost
infanticidy	infanticida	k1gFnSc2	infanticida
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
slabšího	slabý	k2eAgMnSc4d2	slabší
jedince	jedinec	k1gMnSc4	jedinec
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
potlačit	potlačit	k5eAaPmF	potlačit
svou	svůj	k3xOyFgFnSc4	svůj
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neplýtvala	plýtvat	k5eNaImAgFnS	plýtvat
drahocennou	drahocenný	k2eAgFnSc7d1	drahocenná
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Březost	březost	k1gFnSc1	březost
a	a	k8xC	a
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
====	====	k?	====
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
březosti	březost	k1gFnSc2	březost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
88	[number]	k4	88
do	do	k7c2	do
112	[number]	k4	112
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
96	[number]	k4	96
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
mezitím	mezitím	k6eAd1	mezitím
najdou	najít	k5eAaPmIp3nP	najít
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
doupě	doupě	k1gNnSc4	doupě
umístěné	umístěný	k2eAgFnSc2d1	umístěná
často	často	k6eAd1	často
v	v	k7c6	v
husté	hustý	k2eAgFnSc3d1	hustá
vegetaci	vegetace	k1gFnSc3	vegetace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
<g/>
,	,	kIx,	,
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
,	,	kIx,	,
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
či	či	k8xC	či
zemních	zemní	k2eAgFnPc6d1	zemní
norách	nora	k1gFnPc6	nora
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
jednoho	jeden	k4xCgNnSc2	jeden
do	do	k7c2	do
šesti	šest	k4xCc3	šest
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgInPc4d3	nejčastější
počet	počet	k1gInSc4	počet
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Vrhy	vrh	k1gInPc1	vrh
s	s	k7c7	s
pěti	pět	k4xCc7	pět
nebo	nebo	k8xC	nebo
šesti	šest	k4xCc7	šest
mláďaty	mládě	k1gNnPc7	mládě
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dané	daný	k2eAgNnSc1d1	dané
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
struky	struk	k1gInPc4	struk
<g/>
.	.	kIx.	.
</s>
<s>
Melanické	Melanický	k2eAgFnPc1d1	Melanický
levhartice	levhartice	k1gFnPc1	levhartice
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgInSc1d2	nižší
průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
1,7	[number]	k4	1,7
<g/>
)	)	kIx)	)
než	než	k8xS	než
normálně	normálně	k6eAd1	normálně
zbarvené	zbarvený	k2eAgNnSc1d1	zbarvené
(	(	kIx(	(
<g/>
2,07	[number]	k4	2,07
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
pohlaví	pohlaví	k1gNnSc2	pohlaví
hovoří	hovořit	k5eAaImIp3nS	hovořit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
údaje	údaj	k1gInPc1	údaj
oscilují	oscilovat	k5eAaImIp3nP	oscilovat
od	od	k7c2	od
1,19	[number]	k4	1,19
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
až	až	k6eAd1	až
k	k	k7c3	k
1,7	[number]	k4	1,7
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
36	[number]	k4	36
až	až	k9	až
48	[number]	k4	48
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
od	od	k7c2	od
280	[number]	k4	280
(	(	kIx(	(
<g/>
430	[number]	k4	430
<g/>
)	)	kIx)	)
do	do	k7c2	do
1000	[number]	k4	1000
g.	g.	k?	g.
Prvních	první	k4xOgInPc2	první
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
věnuje	věnovat	k5eAaPmIp3nS	věnovat
matka	matka	k1gFnSc1	matka
koťatům	kotě	k1gNnPc3	kotě
plnou	plný	k2eAgFnSc4d1	plná
péči	péče	k1gFnSc4	péče
a	a	k8xC	a
zprvu	zprvu	k6eAd1	zprvu
ani	ani	k9	ani
neloví	lovit	k5eNaImIp3nP	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Vidět	vidět	k5eAaImF	vidět
začínají	začínat	k5eAaImIp3nP	začínat
po	po	k7c6	po
čtyřech	čtyři	k4xCgFnPc6	čtyři
až	až	k8xS	až
devíti	devět	k4xCc6	devět
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
mezitím	mezitím	k6eAd1	mezitím
začne	začít	k5eAaPmIp3nS	začít
opouštět	opouštět	k5eAaImF	opouštět
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
mláďat	mládě	k1gNnPc2	mládě
klíčových	klíčový	k2eAgMnPc2d1	klíčový
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
především	především	k9	především
nedostupnost	nedostupnost	k1gFnSc1	nedostupnost
doupěte	doupě	k1gNnSc2	doupě
pro	pro	k7c4	pro
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
minimalizovala	minimalizovat	k5eAaBmAgFnS	minimalizovat
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
samice	samice	k1gFnSc1	samice
mláďata	mládě	k1gNnPc4	mládě
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
doupat	doupě	k1gNnPc2	doupě
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
přesunů	přesun	k1gInPc2	přesun
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgNnPc4d1	různé
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
vydávají	vydávat	k5eAaImIp3nP	vydávat
na	na	k7c4	na
dlouhotrvající	dlouhotrvající	k2eAgInPc4d1	dlouhotrvající
lovy	lov	k1gInPc4	lov
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
nechávají	nechávat	k5eAaImIp3nP	nechávat
bez	bez	k7c2	bez
dozoru	dozor	k1gInSc2	dozor
někdy	někdy	k6eAd1	někdy
až	až	k9	až
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc4	maso
začínají	začínat	k5eAaImIp3nP	začínat
koťata	kotě	k1gNnPc4	kotě
jíst	jíst	k5eAaImF	jíst
při	při	k7c6	při
hmotnosti	hmotnost	k1gFnSc6	hmotnost
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
kg	kg	kA	kg
<g/>
,	,	kIx,	,
kojení	kojení	k1gNnSc1	kojení
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
6	[number]	k4	6
kg	kg	kA	kg
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
si	se	k3xPyFc3	se
své	své	k1gNnSc4	své
lovecké	lovecký	k2eAgFnSc2d1	lovecká
schopnosti	schopnost	k1gFnSc2	schopnost
trénují	trénovat	k5eAaImIp3nP	trénovat
hrami	hra	k1gFnPc7	hra
a	a	k8xC	a
pronásledováním	pronásledování	k1gNnSc7	pronásledování
malých	malý	k2eAgMnPc2d1	malý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kobylky	kobylka	k1gFnPc1	kobylka
<g/>
,	,	kIx,	,
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
lovit	lovit	k5eAaImF	lovit
začínají	začínat	k5eAaImIp3nP	začínat
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
7	[number]	k4	7
až	až	k9	až
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
trvalé	trvalý	k2eAgInPc4d1	trvalý
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Levhartice	levhartice	k1gFnPc1	levhartice
nevěnují	věnovat	k5eNaImIp3nP	věnovat
výcviku	výcvik	k1gInSc2	výcvik
svých	svůj	k3xOyFgNnPc2	svůj
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
lovu	lov	k1gInSc2	lov
takovou	takový	k3xDgFnSc4	takový
péči	péče	k1gFnSc4	péče
jako	jako	k8xS	jako
například	například	k6eAd1	například
tygřice	tygřice	k1gFnSc1	tygřice
nebo	nebo	k8xC	nebo
lvice	lvice	k1gFnSc1	lvice
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgNnSc4d2	vyšší
procento	procento	k1gNnSc4	procento
malé	malý	k2eAgFnSc2d1	malá
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
chytání	chytání	k1gNnSc1	chytání
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
tolik	tolik	k4yIc4	tolik
naučených	naučený	k2eAgFnPc2d1	naučená
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
postačí	postačit	k5eAaPmIp3nS	postačit
instinkt	instinkt	k1gInSc1	instinkt
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
bývá	bývat	k5eAaImIp3nS	bývat
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
padnou	padnout	k5eAaPmIp3nP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
lvům	lev	k1gMnPc3	lev
<g/>
,	,	kIx,	,
tygrům	tygr	k1gMnPc3	tygr
<g/>
,	,	kIx,	,
hyenám	hyena	k1gFnPc3	hyena
<g/>
,	,	kIx,	,
cizím	cizit	k5eAaImIp1nS	cizit
levhartím	levhartí	k2eAgInPc3d1	levhartí
samcům	samec	k1gInPc3	samec
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
šelmám	šelma	k1gFnPc3	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mívají	mívat	k5eAaImIp3nP	mívat
větší	veliký	k2eAgFnSc4d2	veliký
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
publikoval	publikovat	k5eAaBmAgMnS	publikovat
tým	tým	k1gInSc4	tým
vědců	vědec	k1gMnPc2	vědec
vedených	vedený	k2eAgFnPc2d1	vedená
Guy	Guy	k1gFnPc2	Guy
Balmem	Balm	k1gMnSc7	Balm
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
Levhartího	levhartí	k2eAgInSc2d1	levhartí
programu	program	k1gInSc2	program
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
organizace	organizace	k1gFnSc2	organizace
Panthera	Panthera	k1gFnSc1	Panthera
<g/>
,	,	kIx,	,
sumarizaci	sumarizace	k1gFnSc4	sumarizace
údajů	údaj	k1gInPc2	údaj
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
reprodukčních	reprodukční	k2eAgFnPc2d1	reprodukční
schopností	schopnost	k1gFnPc2	schopnost
levhartích	levhartí	k2eAgFnPc2d1	levhartí
samic	samice	k1gFnPc2	samice
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Sabi	Sab	k1gFnSc2	Sab
Sand	Sand	k1gInSc1	Sand
Game	game	k1gInSc1	game
Reserve	Reserev	k1gFnSc2	Reserev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Krugerovým	Krugerův	k2eAgInSc7d1	Krugerův
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
byla	být	k5eAaImAgNnP	být
sbírána	sbírat	k5eAaImNgNnP	sbírat
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1979	[number]	k4	1979
až	až	k8xS	až
2010	[number]	k4	2010
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
nejucelenější	ucelený	k2eAgInSc4d3	nejucelenější
soubor	soubor	k1gInSc4	soubor
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
reprodukci	reprodukce	k1gFnSc4	reprodukce
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
údaje	údaj	k1gInPc1	údaj
o	o	k7c4	o
celkem	celkem	k6eAd1	celkem
140	[number]	k4	140
vrzích	vrh	k1gInPc6	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
samic	samice	k1gFnPc2	samice
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
vrhu	vrh	k1gInSc6	vrh
byl	být	k5eAaImAgInS	být
46	[number]	k4	46
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
1,9	[number]	k4	1,9
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc4d1	průměrný
interval	interval	k1gInSc4	interval
mezi	mezi	k7c7	mezi
porody	porod	k1gInPc7	porod
25	[number]	k4	25
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
věk	věk	k1gInSc4	věk
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
mláďat	mládě	k1gNnPc2	mládě
19	[number]	k4	19
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
vrh	vrh	k1gInSc1	vrh
neměl	mít	k5eNaImAgInS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
koťata	kotě	k1gNnPc1	kotě
<g/>
.	.	kIx.	.
37	[number]	k4	37
%	%	kIx~	%
mláďat	mládě	k1gNnPc2	mládě
přežilo	přežít	k5eAaPmAgNnS	přežít
až	až	k6eAd1	až
do	do	k7c2	do
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
důvodem	důvod	k1gInSc7	důvod
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
byla	být	k5eAaImAgFnS	být
infanticida	infanticida	k1gFnSc1	infanticida
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
dospělých	dospělý	k2eAgMnPc2d1	dospělý
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
predací	predace	k1gFnSc7	predace
od	od	k7c2	od
lvů	lev	k1gInPc2	lev
a	a	k8xC	a
hyen	hyena	k1gFnPc2	hyena
skvrnitých	skvrnitý	k2eAgFnPc2d1	skvrnitá
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
úmrtí	úmrtí	k1gNnPc2	úmrtí
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
mláďat	mládě	k1gNnPc2	mládě
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
hlady	hlady	k6eAd1	hlady
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
matku	matka	k1gFnSc4	matka
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc1	čtyři
koťata	kotě	k1gNnPc1	kotě
se	se	k3xPyFc4	se
utopila	utopit	k5eAaPmAgNnP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
nemoc	nemoc	k1gFnSc4	nemoc
(	(	kIx(	(
<g/>
prašivina	prašivina	k1gFnSc1	prašivina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
porodila	porodit	k5eAaPmAgFnS	porodit
celkem	celkem	k6eAd1	celkem
19	[number]	k4	19
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
11	[number]	k4	11
vrzích	vrh	k1gInPc6	vrh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
jen	jen	k9	jen
čtyři	čtyři	k4xCgFnPc1	čtyři
dožily	dožít	k5eAaPmAgFnP	dožít
nezávislosti	nezávislost	k1gFnPc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
měla	mít	k5eAaImAgNnP	mít
mláďata	mládě	k1gNnPc1	mládě
osmiletých	osmiletý	k2eAgFnPc2d1	osmiletá
samic	samice	k1gFnPc2	samice
(	(	kIx(	(
<g/>
cca	cca	kA	cca
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
zdokumentoval	zdokumentovat	k5eAaPmAgInS	zdokumentovat
i	i	k9	i
dosud	dosud	k6eAd1	dosud
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
případ	případ	k1gInSc1	případ
adopce	adopce	k1gFnSc2	adopce
u	u	k7c2	u
divokých	divoký	k2eAgMnPc2d1	divoký
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
samice	samice	k1gFnSc1	samice
přijala	přijmout	k5eAaPmAgFnS	přijmout
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
péče	péče	k1gFnSc2	péče
sedmiměsíční	sedmiměsíční	k2eAgNnSc1d1	sedmiměsíční
mládě	mládě	k1gNnSc1	mládě
své	svůj	k3xOyFgFnSc2	svůj
devítileté	devítiletý	k2eAgFnSc2d1	devítiletá
dcery	dcera	k1gFnSc2	dcera
a	a	k8xC	a
odchovala	odchovat	k5eAaPmAgFnS	odchovat
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
čtyřměsíčního	čtyřměsíční	k2eAgMnSc4d1	čtyřměsíční
samečka	sameček	k1gMnSc4	sameček
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
události	událost	k1gFnSc3	událost
došlo	dojít	k5eAaPmAgNnS	dojít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
rodiny	rodina	k1gFnPc1	rodina
podělily	podělit	k5eAaPmAgFnP	podělit
o	o	k7c4	o
mrtvolu	mrtvola	k1gFnSc4	mrtvola
impaly	impala	k1gFnSc2	impala
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
tohoto	tento	k3xDgNnSc2	tento
chování	chování	k1gNnSc2	chování
nebyl	být	k5eNaImAgInS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
biologická	biologický	k2eAgFnSc1d1	biologická
matka	matka	k1gFnSc1	matka
adoptovaného	adoptovaný	k2eAgInSc2d1	adoptovaný
samce	samec	k1gInSc2	samec
žila	žít	k5eAaImAgFnS	žít
normálně	normálně	k6eAd1	normálně
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
odchovala	odchovat	k5eAaPmAgFnS	odchovat
jeho	on	k3xPp3gMnSc4	on
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Dospělost	dospělost	k1gFnSc1	dospělost
<g/>
,	,	kIx,	,
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
12	[number]	k4	12
až	až	k9	až
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
mladí	mladý	k2eAgMnPc1d1	mladý
levharti	levhart	k1gMnPc1	levhart
stávají	stávat	k5eAaImIp3nP	stávat
potravně	potravně	k6eAd1	potravně
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
na	na	k7c6	na
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
většinou	většinou	k6eAd1	většinou
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgFnSc2d1	skutečná
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hledání	hledání	k1gNnSc6	hledání
a	a	k8xC	a
zakládání	zakládání	k1gNnSc6	zakládání
vlastního	vlastní	k2eAgNnSc2d1	vlastní
teritoria	teritorium	k1gNnSc2	teritorium
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
14	[number]	k4	14
až	až	k9	až
36	[number]	k4	36
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
samice	samice	k1gFnPc1	samice
obvykle	obvykle	k6eAd1	obvykle
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
teritorium	teritorium	k1gNnSc4	teritorium
si	se	k3xPyFc3	se
zakládají	zakládat	k5eAaImIp3nP	zakládat
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hledání	hledání	k1gNnSc2	hledání
nového	nový	k2eAgNnSc2d1	nové
území	území	k1gNnSc2	území
mají	mít	k5eAaImIp3nP	mít
levharti	levhart	k1gMnPc1	levhart
stále	stále	k6eAd1	stále
velkou	velký	k2eAgFnSc4d1	velká
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
svědomí	svědomí	k1gNnSc4	svědomí
kromě	kromě	k7c2	kromě
velkých	velký	k2eAgFnPc2d1	velká
šelem	šelma	k1gFnPc2	šelma
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
vlastního	vlastní	k2eAgInSc2d1	vlastní
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
opatřeni	opatřit	k5eAaPmNgMnP	opatřit
tři	tři	k4xCgFnPc4	tři
13	[number]	k4	13
měsíců	měsíc	k1gInPc2	měsíc
staří	starý	k2eAgMnPc1d1	starý
jedinci	jedinec	k1gMnPc1	jedinec
sledovacími	sledovací	k2eAgNnPc7d1	sledovací
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
přežil	přežít	k5eAaPmAgMnS	přežít
dostatečně	dostatečně	k6eAd1	dostatečně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
páření	páření	k1gNnPc1	páření
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
24	[number]	k4	24
až	až	k9	až
52	[number]	k4	52
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
vrhy	vrh	k1gInPc7	vrh
bývá	bývat	k5eAaImIp3nS	bývat
cca	cca	kA	cca
15	[number]	k4	15
až	až	k9	až
29	[number]	k4	29
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
plodné	plodný	k2eAgFnPc1d1	plodná
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
věku	věk	k1gInSc2	věk
13	[number]	k4	13
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
rekord	rekord	k1gInSc1	rekord
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
let	léto	k1gNnPc2	léto
stará	starý	k2eAgFnSc1d1	stará
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
mláďata	mládě	k1gNnPc4	mládě
i	i	k8xC	i
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgMnSc1d3	nejstarší
zaznamenaný	zaznamenaný	k2eAgMnSc1d1	zaznamenaný
otec	otec	k1gMnSc1	otec
měl	mít	k5eAaImAgMnS	mít
21	[number]	k4	21
let	let	k1gInSc4	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
bývají	bývat	k5eAaImIp3nP	bývat
nejagresivnější	agresivní	k2eAgMnPc1d3	nejagresivnější
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
mírou	míra	k1gFnSc7	míra
zabíjení	zabíjení	k1gNnSc1	zabíjení
mláďat	mládě	k1gNnPc2	mládě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
definitivně	definitivně	k6eAd1	definitivně
dokončují	dokončovat	k5eAaImIp3nP	dokončovat
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
upevňují	upevňovat	k5eAaImIp3nP	upevňovat
si	se	k3xPyFc3	se
svá	svůj	k3xOyFgNnPc4	svůj
nově	nově	k6eAd1	nově
ustavená	ustavený	k2eAgNnPc4d1	ustavené
teritoria	teritorium	k1gNnPc4	teritorium
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
s	s	k7c7	s
plozením	plození	k1gNnSc7	plození
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Zabitím	zabití	k1gNnSc7	zabití
cizích	cizí	k2eAgNnPc2d1	cizí
mláďat	mládě	k1gNnPc2	mládě
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
zplození	zplození	k1gNnSc4	zplození
svých	svůj	k3xOyFgInPc2	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnPc1	naděje
na	na	k7c4	na
dožití	dožití	k1gNnSc4	dožití
bývá	bývat	k5eAaImIp3nS	bývat
různá	různý	k2eAgFnSc1d1	různá
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
faktorech	faktor	k1gInPc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
sice	sice	k8xC	sice
několik	několik	k4yIc4	několik
odhadů	odhad	k1gInPc2	odhad
roční	roční	k2eAgFnSc2d1	roční
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
dospělých	dospělý	k2eAgNnPc2d1	dospělé
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
18	[number]	k4	18
%	%	kIx~	%
až	až	k9	až
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
orientační	orientační	k2eAgFnPc4d1	orientační
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
žijící	žijící	k2eAgMnPc1d1	žijící
jedinci	jedinec	k1gMnPc1	jedinec
umírají	umírat	k5eAaImIp3nP	umírat
většinou	většina	k1gFnSc7	většina
do	do	k7c2	do
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
přes	přes	k7c4	přes
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
známý	známý	k2eAgMnSc1d1	známý
divoký	divoký	k2eAgMnSc1d1	divoký
levhart	levhart	k1gMnSc1	levhart
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
18,6	[number]	k4	18,6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
jedinec	jedinec	k1gMnSc1	jedinec
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc2	věk
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Hybridizace	hybridizace	k1gFnSc2	hybridizace
====	====	k?	====
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
křížit	křížit	k5eAaImF	křížit
se	s	k7c7	s
lvy	lev	k1gMnPc7	lev
<g/>
,	,	kIx,	,
tygry	tygr	k1gMnPc7	tygr
<g/>
,	,	kIx,	,
pumami	puma	k1gFnPc7	puma
a	a	k8xC	a
jaguáry	jaguár	k1gMnPc7	jaguár
<g/>
,	,	kIx,	,
hybridizace	hybridizace	k1gFnSc1	hybridizace
s	s	k7c7	s
irbisy	irbis	k1gInPc7	irbis
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
bývá	bývat	k5eAaImIp3nS	bývat
pár	pár	k4xCyI	pár
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
levhartice	levhartice	k1gFnSc2	levhartice
a	a	k8xC	a
samce	samec	k1gMnSc4	samec
jiné	jiný	k2eAgFnSc2d1	jiná
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
opačné	opačný	k2eAgInPc1d1	opačný
případy	případ	k1gInPc1	případ
jsou	být	k5eAaImIp3nP	být
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
<g/>
.	.	kIx.	.
</s>
<s>
Kříženec	kříženec	k1gMnSc1	kříženec
levharta	levhart	k1gMnSc2	levhart
s	s	k7c7	s
jaguářicí	jaguářice	k1gFnSc7	jaguářice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
lepjag	lepjag	k1gInSc1	lepjag
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
leguar	leguar	k1gInSc1	leguar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jaguára	jaguár	k1gMnSc2	jaguár
s	s	k7c7	s
levharticí	levhartice	k1gFnSc7	levhartice
"	"	kIx"	"
<g/>
jagulep	jagulep	k1gInSc1	jagulep
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jaguleop	jaguleop	k1gInSc1	jaguleop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
levharta	levhart	k1gMnSc2	levhart
se	s	k7c7	s
lvicí	lvice	k1gFnSc7	lvice
"	"	kIx"	"
<g/>
leopon	leopon	k1gInSc1	leopon
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
lepon	lepon	k1gNnSc1	lepon
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
lva	lev	k1gMnSc2	lev
s	s	k7c7	s
levharticí	levhartice	k1gFnSc7	levhartice
"	"	kIx"	"
<g/>
liard	liard	k6eAd1	liard
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
lipard	lipard	k1gInSc1	lipard
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
levharta	levhart	k1gMnSc2	levhart
s	s	k7c7	s
tygřicí	tygřice	k1gFnSc7	tygřice
"	"	kIx"	"
<g/>
leoger	leoger	k1gInSc1	leoger
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
dogla	dogla	k1gFnSc1	dogla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tygra	tygr	k1gMnSc2	tygr
s	s	k7c7	s
levharticí	levhartice	k1gFnSc7	levhartice
"	"	kIx"	"
<g/>
tigard	tigard	k1gInSc1	tigard
<g/>
"	"	kIx"	"
a	a	k8xC	a
pumy	puma	k1gFnSc2	puma
s	s	k7c7	s
levharticí	levhartice	k1gFnSc7	levhartice
"	"	kIx"	"
<g/>
pumapard	pumapard	k1gInSc1	pumapard
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hybridi	Hybrid	k1gMnPc1	Hybrid
pocházejí	pocházet	k5eAaImIp3nP	pocházet
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
umělého	umělý	k2eAgInSc2d1	umělý
odchovu	odchov	k1gInSc2	odchov
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgNnPc4d1	přírodní
míšení	míšení	k1gNnPc4	míšení
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nepotvrzené	potvrzený	k2eNgInPc1d1	nepotvrzený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
muselo	muset	k5eAaImAgNnS	muset
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
docházet	docházet	k5eAaImF	docházet
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
důležitý	důležitý	k2eAgInSc4d1	důležitý
evoluční	evoluční	k2eAgInSc4d1	evoluční
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradičních	tradiční	k2eAgNnPc2d1	tradiční
indických	indický	k2eAgNnPc2d1	indické
vyprávění	vyprávění	k1gNnPc2	vyprávění
dochází	docházet	k5eAaImIp3nS	docházet
občas	občas	k6eAd1	občas
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgInPc7d1	velký
levhartími	levhartí	k2eAgInPc7d1	levhartí
samci	samec	k1gInPc7	samec
a	a	k8xC	a
tygřicemi	tygřice	k1gFnPc7	tygřice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
potomci	potomek	k1gMnPc1	potomek
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
dogla	dogla	k6eAd1	dogla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
lovec	lovec	k1gMnSc1	lovec
F.	F.	kA	F.
C.	C.	kA	C.
Hicks	Hicks	k1gInSc4	Hicks
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Forty	Forty	k?	Forty
Years	Years	k1gInSc1	Years
Among	Among	k1gMnSc1	Among
The	The	k1gMnSc1	The
Wild	Wild	k1gMnSc1	Wild
Animals	Animals	k1gInSc4	Animals
Of	Of	k1gFnSc2	Of
India	indium	k1gNnSc2	indium
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
možného	možný	k2eAgMnSc4d1	možný
hybrida	hybrid	k1gMnSc4	hybrid
doglu	doglat	k5eAaPmIp1nS	doglat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
prosekával	prosekávat	k5eAaImAgMnS	prosekávat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
vynořila	vynořit	k5eAaPmAgFnS	vynořit
se	se	k3xPyFc4	se
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
hlava	hlava	k1gFnSc1	hlava
nezvykle	zvykle	k6eNd1	zvykle
velkého	velký	k2eAgMnSc2d1	velký
pantera	panter	k1gMnSc2	panter
následována	následován	k2eAgFnSc1d1	následována
nezaměnitelnými	zaměnitelný	k2eNgFnPc7d1	nezaměnitelná
pruhovanými	pruhovaný	k2eAgNnPc7d1	pruhované
rameny	rameno	k1gNnPc7	rameno
a	a	k8xC	a
tělem	tělo	k1gNnSc7	tělo
tygra	tygr	k1gMnSc2	tygr
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
trochu	trochu	k6eAd1	trochu
jako	jako	k8xC	jako
špinavé	špinavý	k2eAgNnSc1d1	špinavé
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vyválelo	vyválet	k5eAaPmAgNnS	vyválet
v	v	k7c6	v
popelu	popel	k1gInSc6	popel
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
toto	tento	k3xDgNnSc1	tento
neobyčejné	obyčejný	k2eNgNnSc1d1	neobyčejné
stvoření	stvoření	k1gNnSc1	stvoření
skolit	skolit	k5eAaImF	skolit
jednou	jeden	k4xCgFnSc7	jeden
ranou	rána	k1gFnSc7	rána
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
prozkoumal	prozkoumat	k5eAaPmAgMnS	prozkoumat
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
starého	starý	k2eAgMnSc2d1	starý
hybridního	hybridní	k2eAgMnSc2d1	hybridní
samce	samec	k1gMnSc2	samec
se	s	k7c7	s
zuby	zub	k1gInPc7	zub
a	a	k8xC	a
drápy	dráp	k1gInPc7	dráp
značně	značně	k6eAd1	značně
opotřebovanými	opotřebovaný	k2eAgInPc7d1	opotřebovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
byly	být	k5eAaImAgInP	být
čistě	čistě	k6eAd1	čistě
panteří	panteří	k2eAgInPc1d1	panteří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc1	rameno
a	a	k8xC	a
krk	krk	k1gInSc1	krk
tygří	tygří	k2eAgInSc1d1	tygří
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Samci	Samek	k1gMnPc1	Samek
kříženců	kříženec	k1gMnPc2	kříženec
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
neplodní	plodní	k2eNgFnPc1d1	neplodní
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
další	další	k2eAgMnPc4d1	další
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konkurenti	konkurent	k1gMnPc1	konkurent
a	a	k8xC	a
nepřátelé	nepřítel	k1gMnPc1	nepřítel
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
lvi	lev	k1gMnPc1	lev
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
kočky	kočka	k1gFnPc1	kočka
====	====	k?	====
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k6eAd1	mnoho
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
konkurentů	konkurent	k1gMnPc2	konkurent
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
musejí	muset	k5eAaImIp3nP	muset
potýkat	potýkat	k5eAaImF	potýkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
především	především	k9	především
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
případném	případný	k2eAgInSc6d1	případný
střetu	střet	k1gInSc6	střet
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
vždy	vždy	k6eAd1	vždy
potenciálními	potenciální	k2eAgMnPc7d1	potenciální
vítězi	vítěz	k1gMnPc7	vítěz
a	a	k8xC	a
levharti	levhart	k1gMnPc1	levhart
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
chovají	chovat	k5eAaImIp3nP	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
tygr	tygr	k1gMnSc1	tygr
tu	tu	k6eAd1	tu
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
levharta	levhart	k1gMnSc4	levhart
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
denní	denní	k2eAgFnSc1d1	denní
aktivita	aktivita	k1gFnSc1	aktivita
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
levharti	levhart	k1gMnPc1	levhart
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
tygrům	tygr	k1gMnPc3	tygr
prostorově	prostorově	k6eAd1	prostorově
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
jejich	jejich	k3xOp3gNnPc2	jejich
teritorií	teritorium	k1gNnPc2	teritorium
či	či	k8xC	či
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
citlivější	citlivý	k2eAgMnPc4d2	citlivější
tygry	tygr	k1gMnPc4	tygr
ruší	rušit	k5eAaImIp3nS	rušit
přítomnost	přítomnost	k1gFnSc1	přítomnost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
poučka	poučka	k1gFnSc1	poučka
hovořící	hovořící	k2eAgFnSc1d1	hovořící
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
hodně	hodně	k6eAd1	hodně
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
levhartů	levhart	k1gMnPc2	levhart
nalézt	nalézt	k5eAaBmF	nalézt
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
,	,	kIx,	,
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
sice	sice	k8xC	sice
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
záleží	záležet	k5eAaImIp3nS	záležet
také	také	k9	také
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
vhodné	vhodný	k2eAgFnSc2d1	vhodná
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
loví	lovit	k5eAaImIp3nP	lovit
většinou	většinou	k6eAd1	většinou
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgNnPc1d2	menší
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
cca	cca	kA	cca
25	[number]	k4	25
kg	kg	kA	kg
vůči	vůči	k7c3	vůči
97	[number]	k4	97
kg	kg	kA	kg
u	u	k7c2	u
tygrů	tygr	k1gMnPc2	tygr
<g/>
)	)	kIx)	)
a	a	k8xC	a
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
druhů	druh	k1gInPc2	druh
než	než	k8xS	než
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Rajaji	Rajaj	k1gFnSc3	Rajaj
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
reintrodukci	reintrodukce	k1gFnSc3	reintrodukce
tygrů	tygr	k1gMnPc2	tygr
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
levharti	levhart	k1gMnPc1	levhart
dominantním	dominantní	k2eAgMnSc7d1	dominantní
predátorem	predátor	k1gMnSc7	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
levhartů	levhart	k1gMnPc2	levhart
(	(	kIx(	(
<g/>
hustota	hustota	k1gFnSc1	hustota
klesla	klesnout	k5eAaPmAgFnS	klesnout
z	z	k7c2	z
9,76	[number]	k4	9,76
na	na	k7c4	na
2,07	[number]	k4	2,07
na	na	k7c4	na
100	[number]	k4	100
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
kořist	kořist	k1gFnSc1	kořist
posunula	posunout	k5eAaPmAgFnS	posunout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
menším	malý	k2eAgNnPc3d2	menší
a	a	k8xC	a
domácím	domácí	k2eAgNnPc3d1	domácí
zvířatům	zvíře	k1gNnPc3	zvíře
(	(	kIx(	(
<g/>
vzrůst	vzrůst	k5eAaPmF	vzrůst
z	z	k7c2	z
6,8	[number]	k4	6,8
%	%	kIx~	%
na	na	k7c4	na
31,8	[number]	k4	31,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
šelmy	šelma	k1gFnPc1	šelma
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
levharti	levhart	k1gMnPc1	levhart
velmi	velmi	k6eAd1	velmi
pozorní	pozorný	k2eAgMnPc1d1	pozorný
<g/>
.	.	kIx.	.
</s>
<s>
Indický	indický	k2eAgMnSc1d1	indický
ochránce	ochránce	k1gMnSc1	ochránce
přírody	příroda	k1gFnSc2	příroda
Billy	Bill	k1gMnPc4	Bill
Arjan	Arjan	k1gMnSc1	Arjan
Singh	Singh	k1gMnSc1	Singh
popsal	popsat	k5eAaPmAgMnS	popsat
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ochočenou	ochočený	k2eAgFnSc7d1	ochočená
levharticí	levhartice	k1gFnSc7	levhartice
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
bezstarostné	bezstarostný	k2eAgNnSc1d1	bezstarostné
a	a	k8xC	a
sebevědomé	sebevědomý	k2eAgNnSc1d1	sebevědomé
až	až	k6eAd1	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
přítomnost	přítomnost	k1gFnSc1	přítomnost
tygřice	tygřice	k1gFnSc1	tygřice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
asi	asi	k9	asi
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
přešla	přejít	k5eAaPmAgNnP	přejít
přes	přes	k7c4	přes
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Levhartice	levhartice	k1gFnSc1	levhartice
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
doslova	doslova	k6eAd1	doslova
přitiskla	přitisknout	k5eAaPmAgFnS	přitisknout
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
odplížila	odplížit	k5eAaPmAgFnS	odplížit
a	a	k8xC	a
zmizela	zmizet	k5eAaPmAgFnS	zmizet
v	v	k7c6	v
buši	buš	k1gInSc6	buš
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počínala	počínat	k5eAaImAgFnS	počínat
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
extrémně	extrémně	k6eAd1	extrémně
opatrně	opatrně	k6eAd1	opatrně
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
snahou	snaha	k1gFnSc7	snaha
se	se	k3xPyFc4	se
ukrýt	ukrýt	k5eAaPmF	ukrýt
<g/>
.	.	kIx.	.
<g/>
Vztah	vztah	k1gInSc1	vztah
levhartů	levhart	k1gMnPc2	levhart
ke	k	k7c3	k
lvům	lev	k1gMnPc3	lev
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
ale	ale	k9	ale
musí	muset	k5eAaImIp3nP	muset
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
velké	velký	k2eAgFnPc1d1	velká
kočky	kočka	k1gFnPc1	kočka
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
smečkách	smečka	k1gFnPc6	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
častou	častý	k2eAgFnSc7d1	častá
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
levhart	levhart	k1gMnSc1	levhart
zachrání	zachránit	k5eAaPmIp3nS	zachránit
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
kořist	kořist	k1gFnSc1	kořist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
útěk	útěk	k1gInSc4	útěk
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
George	Georg	k1gMnSc2	Georg
Schaller	Schaller	k1gMnSc1	Schaller
odhadl	odhadnout	k5eAaPmAgMnS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Serengeti	Serenge	k1gNnSc6	Serenge
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
lví	lví	k2eAgFnSc2d1	lví
spotřeby	spotřeba	k1gFnSc2	spotřeba
masa	maso	k1gNnSc2	maso
získáno	získán	k2eAgNnSc4d1	získáno
z	z	k7c2	z
kořisti	kořist	k1gFnSc2	kořist
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
Sabi	Sab	k1gFnSc6	Sab
Sand	Sand	k1gInSc1	Sand
Game	game	k1gInSc4	game
Reserve	Reserev	k1gFnSc2	Reserev
si	se	k3xPyFc3	se
lvi	lev	k1gMnPc1	lev
přivlastnili	přivlastnit	k5eAaPmAgMnP	přivlastnit
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
levhartích	levhartí	k2eAgInPc2d1	levhartí
úlovků	úlovek	k1gInPc2	úlovek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lvi	lev	k1gMnPc1	lev
můžou	můžou	k?	můžou
za	za	k7c2	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
%	%	kIx~	%
veškeré	veškerý	k3xTgFnSc2	veškerý
levhartí	levhartí	k2eAgFnSc2d1	levhartí
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
levharti	levhart	k1gMnPc1	levhart
schopni	schopen	k2eAgMnPc1d1	schopen
žít	žít	k5eAaImF	žít
v	v	k7c6	v
relativní	relativní	k2eAgFnSc6d1	relativní
lví	lví	k2eAgFnSc6d1	lví
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
nějak	nějak	k6eAd1	nějak
zásadně	zásadně	k6eAd1	zásadně
ovlivňovalo	ovlivňovat	k5eAaImAgNnS	ovlivňovat
jejich	jejich	k3xOp3gFnSc4	jejich
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
levhart	levhart	k1gMnSc1	levhart
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
oběma	dva	k4xCgInPc3	dva
silnějším	silný	k2eAgMnPc3d2	silnější
"	"	kIx"	"
<g/>
bratrancům	bratranec	k1gMnPc3	bratranec
<g/>
"	"	kIx"	"
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
<g/>
Vztah	vztah	k1gInSc1	vztah
levhartů	levhart	k1gMnPc2	levhart
a	a	k8xC	a
irbisů	irbis	k1gInPc2	irbis
nebyl	být	k5eNaImAgMnS	být
doposud	doposud	k6eAd1	doposud
prozkoumán	prozkoumán	k2eAgMnSc1d1	prozkoumán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chybějí	chybět	k5eAaImIp3nP	chybět
jakákoliv	jakýkoliv	k3yIgNnPc1	jakýkoliv
pozorování	pozorování	k1gNnPc1	pozorování
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Komparační	komparační	k2eAgFnSc1d1	komparační
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
nicméně	nicméně	k8xC	nicméně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
eventuálním	eventuální	k2eAgInSc6d1	eventuální
konkurenčním	konkurenční	k2eAgInSc6d1	konkurenční
střetu	střet	k1gInSc6	střet
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
úspěšnější	úspěšný	k2eAgFnSc4d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
gepardovi	gepard	k1gMnSc3	gepard
bývá	bývat	k5eAaImIp3nS	bývat
levhart	levhart	k1gMnSc1	levhart
obvykle	obvykle	k6eAd1	obvykle
dominantním	dominantní	k2eAgMnSc7d1	dominantní
druhem	druh	k1gMnSc7	druh
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
ho	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
sežere	sežrat	k5eAaPmIp3nS	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
dvě	dva	k4xCgFnPc4	dva
kočkovité	kočkovitý	k2eAgFnPc4d1	kočkovitá
šelmy	šelma	k1gFnPc4	šelma
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
přímými	přímý	k2eAgMnPc7d1	přímý
konkurenty	konkurent	k1gMnPc7	konkurent
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
preferují	preferovat	k5eAaImIp3nP	preferovat
přibližně	přibližně	k6eAd1	přibližně
stejnou	stejný	k2eAgFnSc4d1	stejná
kořist	kořist	k1gFnSc4	kořist
(	(	kIx(	(
<g/>
potravní	potravní	k2eAgNnPc4d1	potravní
překrytí	překrytí	k1gNnPc4	překrytí
68,7	[number]	k4	68,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
menším	malý	k2eAgFnPc3d2	menší
kočkám	kočka	k1gFnPc3	kočka
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
levhart	levhart	k1gMnSc1	levhart
s	s	k7c7	s
jasnou	jasný	k2eAgFnSc7d1	jasná
převahou	převaha	k1gFnSc7	převaha
a	a	k8xC	a
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
může	moct	k5eAaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Schaller	Schaller	k1gMnSc1	Schaller
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
příhodu	příhoda	k1gFnSc4	příhoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
levhart	levhart	k1gMnSc1	levhart
vnikl	vniknout	k5eAaPmAgMnS	vniknout
do	do	k7c2	do
ložnice	ložnice	k1gFnSc2	ložnice
jeho	jeho	k3xOp3gMnSc2	jeho
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
leželi	ležet	k5eAaImAgMnP	ležet
dva	dva	k4xCgMnPc4	dva
ochočení	ochočený	k2eAgMnPc1d1	ochočený
servalové	serval	k1gMnPc1	serval
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
popadl	popadnout	k5eAaPmAgMnS	popadnout
a	a	k8xC	a
odnesl	odnést	k5eAaPmAgMnS	odnést
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
levhart	levhart	k1gMnSc1	levhart
nad	nad	k7c7	nad
menšími	malý	k2eAgFnPc7d2	menší
kočkovitými	kočkovitý	k2eAgFnPc7d1	kočkovitá
šelmami	šelma	k1gFnPc7	šelma
při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
střetu	střet	k1gInSc6	střet
dominuje	dominovat	k5eAaImIp3nS	dominovat
<g/>
,	,	kIx,	,
neznamená	znamenat	k5eNaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nad	nad	k7c7	nad
nimi	on	k3xPp3gInPc7	on
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
modelových	modelový	k2eAgInPc2d1	modelový
testů	test	k1gInPc2	test
právě	právě	k9	právě
konkurence	konkurence	k1gFnSc1	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
množících	množící	k2eAgInPc2d1	množící
soupeřů	soupeř	k1gMnPc2	soupeř
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
levharta	levhart	k1gMnSc2	levhart
Diardova	Diardův	k2eAgMnSc2d1	Diardův
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnSc2	kočka
Temminckovy	Temminckův	k2eAgFnSc2d1	Temminckův
a	a	k8xC	a
psovité	psovitý	k2eAgFnSc2d1	psovitá
šelmy	šelma	k1gFnSc2	šelma
dhoula	dhoout	k5eAaPmAgFnS	dhoout
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
vymizení	vymizení	k1gNnSc4	vymizení
levharta	levhart	k1gMnSc2	levhart
ze	z	k7c2	z
Sumatry	Sumatra	k1gFnSc2	Sumatra
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
pleistocénu	pleistocén	k1gInSc6	pleistocén
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
soupeření	soupeření	k1gNnSc1	soupeření
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
a	a	k8xC	a
člověkem	člověk	k1gMnSc7	člověk
zřejmě	zřejmě	k6eAd1	zřejmě
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
jen	jen	k6eAd1	jen
marginální	marginální	k2eAgFnSc4d1	marginální
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hyeny	hyena	k1gFnPc1	hyena
<g/>
,	,	kIx,	,
psovité	psovitý	k2eAgFnPc1d1	psovitá
šelmy	šelma	k1gFnPc1	šelma
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
====	====	k?	====
</s>
</p>
<p>
<s>
Hyeny	hyena	k1gFnPc1	hyena
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
(	(	kIx(	(
<g/>
Crocuta	Crocut	k1gMnSc2	Crocut
crocuta	crocut	k1gMnSc2	crocut
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dalšími	další	k2eAgFnPc7d1	další
šelmami	šelma	k1gFnPc7	šelma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
sužovat	sužovat	k5eAaImF	sužovat
levharty	levhart	k1gMnPc4	levhart
a	a	k8xC	a
připravovat	připravovat	k5eAaImF	připravovat
je	on	k3xPp3gNnSc4	on
o	o	k7c4	o
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
levhart	levhart	k1gMnSc1	levhart
včas	včas	k6eAd1	včas
neskryje	skrýt	k5eNaPmIp3nS	skrýt
<g/>
,	,	kIx,	,
hyeny	hyena	k1gFnPc1	hyena
obvykle	obvykle	k6eAd1	obvykle
uspějí	uspět	k5eAaPmIp3nP	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
samci	samec	k1gMnPc1	samec
levhartů	levhart	k1gMnPc2	levhart
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
odhodlané	odhodlaný	k2eAgFnSc2d1	odhodlaná
samice	samice	k1gFnSc2	samice
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
hyenám	hyena	k1gFnPc3	hyena
občas	občas	k6eAd1	občas
postaví	postavit	k5eAaPmIp3nS	postavit
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
uhájí	uhájit	k5eAaPmIp3nP	uhájit
<g/>
.	.	kIx.	.
</s>
<s>
Psovité	psovitý	k2eAgFnPc1d1	psovitá
šelmy	šelma	k1gFnPc1	šelma
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
úhlavní	úhlavní	k2eAgMnPc4d1	úhlavní
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
,	,	kIx,	,
konkurenty	konkurent	k1gMnPc4	konkurent
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
potravu	potrava	k1gFnSc4	potrava
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
hyenovití	hyenovitý	k2eAgMnPc1d1	hyenovitý
(	(	kIx(	(
<g/>
Lycaon	Lycaon	k1gMnSc1	Lycaon
pictus	pictus	k1gMnSc1	pictus
<g/>
)	)	kIx)	)
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
dhoulové	dhoul	k1gMnPc1	dhoul
(	(	kIx(	(
<g/>
Cuon	Cuon	k1gMnSc1	Cuon
alpinus	alpinus	k1gMnSc1	alpinus
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlci	vlk	k1gMnPc1	vlk
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
představují	představovat	k5eAaImIp3nP	představovat
významné	významný	k2eAgMnPc4d1	významný
soupeře	soupeř	k1gMnPc4	soupeř
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
operují	operovat	k5eAaImIp3nP	operovat
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
<g/>
,	,	kIx,	,
levharty	levhart	k1gMnPc4	levhart
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
<g/>
,	,	kIx,	,
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
a	a	k8xC	a
berou	brát	k5eAaImIp3nP	brát
jim	on	k3xPp3gMnPc3	on
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
levhartovi	levhartův	k2eAgMnPc1d1	levhartův
překvapit	překvapit	k5eAaPmF	překvapit
osamocené	osamocený	k2eAgMnPc4d1	osamocený
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
vítězem	vítěz	k1gMnSc7	vítěz
bývá	bývat	k5eAaImIp3nS	bývat
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Nesmiřitelnými	smiřitelný	k2eNgMnPc7d1	nesmiřitelný
nepřáteli	nepřítel	k1gMnPc7	nepřítel
levhartů	levhart	k1gMnPc2	levhart
jsou	být	k5eAaImIp3nP	být
paviáni	pavián	k1gMnPc1	pavián
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
tlupa	tlupa	k1gFnSc1	tlupa
vedená	vedený	k2eAgFnSc1d1	vedená
velkými	velký	k2eAgFnPc7d1	velká
samci	samec	k1gMnSc3	samec
tuto	tento	k3xDgFnSc4	tento
šelmu	šelma	k1gFnSc4	šelma
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
napadá	napadat	k5eAaPmIp3nS	napadat
ji	on	k3xPp3gFnSc4	on
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
neopustí	opustit	k5eNaPmIp3nS	opustit
oblast	oblast	k1gFnSc1	oblast
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
neskryje	skrýt	k5eNaPmIp3nS	skrýt
na	na	k7c4	na
nějaké	nějaký	k3yIgNnSc4	nějaký
nedostupné	dostupný	k2eNgNnSc4d1	nedostupné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
útocích	útok	k1gInPc6	útok
levhart	levhart	k1gMnSc1	levhart
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Paviáni	pavián	k1gMnPc1	pavián
nechránění	chráněný	k2eNgMnPc1d1	nechráněný
tlupou	tlupa	k1gFnSc7	tlupa
bývají	bývat	k5eAaImIp3nP	bývat
naopak	naopak	k6eAd1	naopak
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nepříliš	příliš	k6eNd1	příliš
častou	častý	k2eAgFnSc7d1	častá
kořistí	kořist	k1gFnSc7	kořist
levharta	levhart	k1gMnSc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
krokodýli	krokodýl	k1gMnPc1	krokodýl
nilští	nilský	k2eAgMnPc1d1	nilský
<g/>
,	,	kIx,	,
mořští	mořský	k2eAgMnPc1d1	mořský
a	a	k8xC	a
bahenní	bahenní	k2eAgMnPc1d1	bahenní
jsou	být	k5eAaImIp3nP	být
příležitostnými	příležitostný	k2eAgMnPc7d1	příležitostný
predátory	predátor	k1gMnPc7	predátor
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
může	moct	k5eAaImIp3nS	moct
šelmu	šelma	k1gFnSc4	šelma
zabít	zabít	k5eAaPmF	zabít
a	a	k8xC	a
sežrat	sežrat	k5eAaPmF	sežrat
nějaký	nějaký	k3yIgInSc4	nějaký
velký	velký	k2eAgInSc4d1	velký
škrtič	škrtič	k1gInSc4	škrtič
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
krajta	krajta	k1gFnSc1	krajta
tmavá	tmavý	k2eAgFnSc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
konkurentem	konkurent	k1gMnSc7	konkurent
levharta	levhart	k1gMnSc2	levhart
je	být	k5eAaImIp3nS	být
i	i	k9	i
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
lidských	lidský	k2eAgMnPc2d1	lidský
pastevců	pastevec	k1gMnPc2	pastevec
a	a	k8xC	a
lovců	lovec	k1gMnPc2	lovec
pravidelně	pravidelně	k6eAd1	pravidelně
loupí	loupit	k5eAaImIp3nS	loupit
jeho	jeho	k3xOp3gFnSc4	jeho
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paraziti	parazit	k1gMnPc1	parazit
a	a	k8xC	a
nemoci	nemoc	k1gFnPc1	nemoc
===	===	k?	===
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
jsou	být	k5eAaImIp3nP	být
hostitelé	hostitel	k1gMnPc1	hostitel
mnoha	mnoho	k4c2	mnoho
parazitů	parazit	k1gMnPc2	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
ekto-	ekto-	k?	ekto-
a	a	k8xC	a
endoparazitů	endoparazit	k1gMnPc2	endoparazit
je	být	k5eAaImIp3nS	být
obdobné	obdobný	k2eAgFnPc4d1	obdobná
jako	jako	k8xC	jako
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ektoparazitů	ektoparazit	k1gMnPc2	ektoparazit
napadají	napadat	k5eAaBmIp3nP	napadat
levharty	levhart	k1gMnPc4	levhart
klíšťata	klíště	k1gNnPc1	klíště
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Amblyomma	Amblyomma	k1gNnSc1	Amblyomma
<g/>
,	,	kIx,	,
Haemaphysalis	Haemaphysalis	k1gFnSc1	Haemaphysalis
<g/>
,	,	kIx,	,
Ixodes	Ixodes	k1gMnSc1	Ixodes
<g/>
,	,	kIx,	,
Rhipicephalus	Rhipicephalus	k1gMnSc1	Rhipicephalus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
blechy	blecha	k1gFnSc2	blecha
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Echidnophaga	Echidnophag	k1gMnSc2	Echidnophag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kloši	kloš	k1gMnPc1	kloš
(	(	kIx(	(
<g/>
Lipoptena	Lipopten	k2eAgFnSc1d1	Lipopten
chalcomelaena	chalcomelaena	k1gFnSc1	chalcomelaena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
masařky	masařka	k1gFnSc2	masařka
(	(	kIx(	(
<g/>
Wohlfahrtia	Wohlfahrtia	k1gFnSc1	Wohlfahrtia
magnifica	magnifica	k1gFnSc1	magnifica
<g/>
)	)	kIx)	)
a	a	k8xC	a
roztoči	roztoč	k1gMnPc1	roztoč
(	(	kIx(	(
<g/>
Notoedres	Notoedres	k1gInSc1	Notoedres
cati	cat	k1gFnSc2	cat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
endoparazitů	endoparazit	k1gMnPc2	endoparazit
to	ten	k3xDgNnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
motolice	motolice	k1gFnSc1	motolice
(	(	kIx(	(
<g/>
Paragonimus	Paragonimus	k1gInSc1	Paragonimus
westermani	westerman	k1gMnPc5	westerman
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlístice	hlístice	k1gFnSc1	hlístice
(	(	kIx(	(
<g/>
Aelurostrongylus	Aelurostrongylus	k1gInSc1	Aelurostrongylus
<g/>
)	)	kIx)	)
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
mohou	moct	k5eAaImIp3nP	moct
parazitovat	parazitovat	k5eAaImF	parazitovat
některé	některý	k3yIgFnPc4	některý
tasemnice	tasemnice	k1gFnPc4	tasemnice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
škulovec	škulovec	k1gMnSc1	škulovec
široký	široký	k2eAgMnSc1d1	široký
<g/>
)	)	kIx)	)
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
hlístic	hlístice	k1gFnPc2	hlístice
(	(	kIx(	(
<g/>
Ancylostoma	Ancylostoma	k1gFnSc1	Ancylostoma
<g/>
,	,	kIx,	,
Toxocara	Toxocara	k1gFnSc1	Toxocara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
svalovci	svalovec	k1gMnPc1	svalovec
(	(	kIx(	(
<g/>
Trichinella	Trichinella	k1gMnSc1	Trichinella
<g/>
)	)	kIx)	)
a	a	k8xC	a
svalovky	svalovka	k1gFnSc2	svalovka
(	(	kIx(	(
<g/>
Sarcocystis	Sarcocystis	k1gInSc1	Sarcocystis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
54	[number]	k4	54
vzorků	vzorek	k1gInPc2	vzorek
trusu	trus	k1gInSc2	trus
v	v	k7c6	v
thajské	thajský	k2eAgFnSc6d1	thajská
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Huai	Hua	k1gFnSc2	Hua
Kha	Kha	k1gFnSc2	Kha
ukázala	ukázat	k5eAaPmAgNnP	ukázat
přítomnost	přítomnost	k1gFnSc4	přítomnost
parazita	parazit	k1gMnSc2	parazit
v	v	k7c4	v
94	[number]	k4	94
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dvou	dva	k4xCgMnPc2	dva
levhartů	levhart	k1gMnPc2	levhart
perských	perský	k2eAgMnPc2d1	perský
byly	být	k5eAaImAgInP	být
identifikovány	identifikován	k2eAgMnPc4d1	identifikován
škrkavky	škrkavka	k1gMnPc4	škrkavka
kočicí	kočice	k1gFnPc2	kočice
(	(	kIx(	(
<g/>
Toxocara	Toxocara	k1gFnSc1	Toxocara
cati	cat	k1gFnSc2	cat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
může	moct	k5eAaImIp3nS	moct
levharta	levhart	k1gMnSc4	levhart
napadnout	napadnout	k5eAaPmF	napadnout
i	i	k9	i
srdeční	srdeční	k2eAgMnSc1d1	srdeční
červ	červ	k1gMnSc1	červ
vlasovec	vlasovec	k1gMnSc1	vlasovec
psí	psí	k1gNnSc2	psí
(	(	kIx(	(
<g/>
Dirofilaria	Dirofilarium	k1gNnSc2	Dirofilarium
immitis	immitis	k1gFnSc1	immitis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Šelmu	šelma	k1gFnSc4	šelma
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
řada	řada	k1gFnSc1	řada
virových	virový	k2eAgFnPc2d1	virová
a	a	k8xC	a
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
felinní	felinný	k2eAgMnPc1d1	felinný
imunodeficientní	imunodeficientní	k2eAgFnSc1d1	imunodeficientní
virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
FIV	FIV	kA	FIV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
felinní	felinný	k2eAgMnPc1d1	felinný
coronavirus	coronavirus	k1gMnSc1	coronavirus
(	(	kIx(	(
<g/>
FCoV	FCoV	k1gMnSc1	FCoV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
chřipky	chřipka	k1gFnSc2	chřipka
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vzteklina	vzteklina	k1gFnSc1	vzteklina
<g/>
,	,	kIx,	,
bovinní	bovinní	k2eAgFnSc1d1	bovinní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
anthrax	anthrax	k1gInSc1	anthrax
nebo	nebo	k8xC	nebo
hepatitida	hepatitida	k1gFnSc1	hepatitida
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
populace	populace	k1gFnPc1	populace
levhartů	levhart	k1gMnPc2	levhart
může	moct	k5eAaImIp3nS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
psinka	psinka	k1gFnSc1	psinka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
psovité	psovitý	k2eAgFnPc1d1	psovitá
šelmy	šelma	k1gFnPc1	šelma
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
vyhledávanou	vyhledávaný	k2eAgFnSc4d1	vyhledávaná
kořist	kořist	k1gFnSc4	kořist
tohoto	tento	k3xDgMnSc2	tento
predátora	predátor	k1gMnSc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
samotářský	samotářský	k2eAgInSc1d1	samotářský
život	život	k1gInSc1	život
skýtá	skýtat	k5eAaImIp3nS	skýtat
levhartům	levhart	k1gMnPc3	levhart
jisté	jistý	k2eAgFnSc2d1	jistá
výhody	výhoda	k1gFnSc2	výhoda
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
epizootických	epizootický	k2eAgFnPc2d1	epizootický
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Toxoplasma	Toxoplasmum	k1gNnSc2	Toxoplasmum
====	====	k?	====
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
jsou	být	k5eAaImIp3nP	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiné	jiný	k2eAgFnPc1d1	jiná
kočky	kočka	k1gFnPc1	kočka
konečnými	konečný	k2eAgMnPc7d1	konečný
hostiteli	hostitel	k1gMnPc7	hostitel
parazita	parazit	k1gMnSc2	parazit
Toxoplasma	Toxoplasm	k1gMnSc2	Toxoplasm
gondii	gondie	k1gFnSc4	gondie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
organismus	organismus	k1gInSc1	organismus
má	mít	k5eAaImIp3nS	mít
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
do	do	k7c2	do
levhartího	levhartí	k2eAgNnSc2d1	levhartí
(	(	kIx(	(
<g/>
kočičího	kočičí	k2eAgNnSc2d1	kočičí
<g/>
)	)	kIx)	)
těla	tělo	k1gNnSc2	tělo
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
mezihostiteli	mezihostitel	k1gMnPc7	mezihostitel
bývají	bývat	k5eAaImIp3nP	bývat
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
má	mít	k5eAaImIp3nS	mít
následně	následně	k6eAd1	následně
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Parazit	parazit	k1gMnSc1	parazit
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
snižuje	snižovat	k5eAaImIp3nS	snižovat
rychlost	rychlost	k1gFnSc1	rychlost
reakce	reakce	k1gFnSc1	reakce
a	a	k8xC	a
pocit	pocit	k1gInSc1	pocit
pudu	pud	k1gInSc2	pud
sebezáchovy	sebezáchova	k1gFnSc2	sebezáchova
<g/>
.	.	kIx.	.
</s>
<s>
Nedávný	dávný	k2eNgInSc1d1	nedávný
výzkum	výzkum	k1gInSc1	výzkum
na	na	k7c6	na
šimpanzích	šimpanzí	k2eAgFnPc6d1	šimpanzí
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
jediným	jediný	k2eAgMnSc7d1	jediný
přirozeným	přirozený	k2eAgMnSc7d1	přirozený
predátorem	predátor	k1gMnSc7	predátor
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toxoplasmou	toxoplasmat	k5eAaPmIp3nP	toxoplasmat
nakažení	nakažený	k2eAgMnPc1d1	nakažený
primáti	primát	k1gMnPc1	primát
ztratili	ztratit	k5eAaPmAgMnP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
averzi	averze	k1gFnSc4	averze
vůči	vůči	k7c3	vůči
levhartí	levhartí	k2eAgFnSc3d1	levhartí
moči	moč	k1gFnSc3	moč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nevyhýbají	vyhýbat	k5eNaImIp3nP	vyhýbat
místům	místo	k1gNnPc3	místo
s	s	k7c7	s
pachem	pach	k1gInSc7	pach
predátora	predátor	k1gMnSc2	predátor
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tak	tak	k9	tak
šance	šance	k1gFnPc1	šance
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
uloveni	ulovit	k5eAaPmNgMnP	ulovit
<g/>
.	.	kIx.	.
<g/>
Toxoplasma	Toxoplasma	k1gFnSc1	Toxoplasma
zřejmě	zřejmě	k6eAd1	zřejmě
může	moct	k5eAaImIp3nS	moct
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
levhartí	levhartí	k2eAgInSc4d1	levhartí
pud	pud	k1gInSc4	pud
sebezáchovy	sebezáchova	k1gFnSc2	sebezáchova
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mezihostitelů	mezihostitel	k1gMnPc2	mezihostitel
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
tří	tři	k4xCgMnPc2	tři
levhartů	levhart	k1gMnPc2	levhart
přejetých	přejetý	k2eAgMnPc2d1	přejetý
automobily	automobil	k1gInPc1	automobil
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Golestan	Golestan	k1gInSc1	Golestan
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
odhalil	odhalit	k5eAaPmAgInS	odhalit
u	u	k7c2	u
dvou	dva	k4xCgInPc2	dva
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
akutní	akutní	k2eAgFnSc4d1	akutní
toxoplasmózu	toxoplasmóza	k1gFnSc4	toxoplasmóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Levhart	levhart	k1gMnSc1	levhart
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
měl	mít	k5eAaImAgMnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
společnost	společnost	k1gFnSc4	společnost
silný	silný	k2eAgInSc4d1	silný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
<g/>
,	,	kIx,	,
výraznou	výrazný	k2eAgFnSc4d1	výrazná
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
šelmu	šelma	k1gFnSc4	šelma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
člověku	člověk	k1gMnSc3	člověk
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
tygra	tygr	k1gMnSc2	tygr
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
kultury	kultura	k1gFnPc4	kultura
a	a	k8xC	a
etnika	etnikum	k1gNnPc4	etnikum
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
rozličné	rozličný	k2eAgMnPc4d1	rozličný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
kontroverzní	kontroverzní	k2eAgFnPc4d1	kontroverzní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
krásu	krása	k1gFnSc4	krása
a	a	k8xC	a
lstivost	lstivost	k1gFnSc4	lstivost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
divokost	divokost	k1gFnSc4	divokost
a	a	k8xC	a
krutost	krutost	k1gFnSc4	krutost
<g/>
.	.	kIx.	.
</s>
<s>
Špatnou	špatný	k2eAgFnSc4d1	špatná
reputaci	reputace	k1gFnSc4	reputace
požívali	požívat	k5eAaImAgMnP	požívat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
černí	černit	k5eAaImIp3nP	černit
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
zvláště	zvláště	k6eAd1	zvláště
divoké	divoký	k2eAgFnSc2d1	divoká
a	a	k8xC	a
krvežíznivé	krvežíznivý	k2eAgFnSc2d1	krvežíznivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Pravěk	pravěk	k1gInSc4	pravěk
====	====	k?	====
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
známé	známý	k2eAgNnSc1d1	známé
zobrazení	zobrazení	k1gNnSc1	zobrazení
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Chauvetově	Chauvetův	k2eAgFnSc6d1	Chauvetova
jeskyni	jeskyně	k1gFnSc6	jeskyně
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
období	období	k1gNnSc2	období
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
nejstarší	starý	k2eAgFnSc1d3	nejstarší
malba	malba	k1gFnSc1	malba
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
<g/>
)	)	kIx)	)
stará	starý	k2eAgFnSc1d1	stará
cca	cca	kA	cca
8000	[number]	k4	8000
let	léto	k1gNnPc2	léto
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tureckého	turecký	k2eAgMnSc2d1	turecký
Çatal	Çatal	k1gInSc4	Çatal
Hüyük	Hüyüko	k1gNnPc2	Hüyüko
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dva	dva	k4xCgMnPc4	dva
levharty	levhart	k1gMnPc4	levhart
natočené	natočený	k2eAgMnPc4d1	natočený
hlavami	hlava	k1gFnPc7	hlava
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Plastika	plastika	k1gFnSc1	plastika
byla	být	k5eAaImAgFnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přemalovávána	přemalováván	k2eAgFnSc1d1	přemalováván
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
její	její	k3xOp3gInSc1	její
původní	původní	k2eAgInSc1d1	původní
vzhled	vzhled	k1gInSc1	vzhled
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
nálezů	nález	k1gInPc2	nález
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
soška	soška	k1gFnSc1	soška
kypré	kyprý	k2eAgFnSc2d1	kyprá
bohyně-matky	bohyněatka	k1gFnSc2	bohyně-matka
sedící	sedící	k2eAgFnSc1d1	sedící
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
levharty	levhart	k1gMnPc4	levhart
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kult	kult	k1gInSc1	kult
velkých	velký	k2eAgFnPc2d1	velká
koček	kočka	k1gFnPc2	kočka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
trval	trvat	k5eAaImAgMnS	trvat
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Starověký	starověký	k2eAgInSc4d1	starověký
Přední	přední	k2eAgInSc4d1	přední
východ	východ	k1gInSc4	východ
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
kulturách	kultura	k1gFnPc6	kultura
Středomoří	středomoří	k1gNnSc2	středomoří
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
hrál	hrát	k5eAaImAgMnS	hrát
levhart	levhart	k1gMnSc1	levhart
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
staré	starý	k2eAgFnSc2d1	stará
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
často	často	k6eAd1	často
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
různá	různý	k2eAgNnPc4d1	různé
božstva	božstvo	k1gNnPc4	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
ukazovali	ukazovat	k5eAaImAgMnP	ukazovat
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
elitě	elita	k1gFnSc3	elita
<g/>
,	,	kIx,	,
symbolizovali	symbolizovat	k5eAaImAgMnP	symbolizovat
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
plodnost	plodnost	k1gFnSc1	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Faraoni	faraon	k1gMnPc1	faraon
<g/>
,	,	kIx,	,
vysocí	vysoký	k2eAgMnPc1d1	vysoký
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
prominenti	prominent	k1gMnPc1	prominent
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
svůj	svůj	k3xOyFgInSc4	svůj
status	status	k1gInSc4	status
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
nošením	nošení	k1gNnSc7	nošení
vzácných	vzácný	k2eAgFnPc2d1	vzácná
levhartích	levhartí	k2eAgFnPc2d1	levhartí
kožešin	kožešina	k1gFnPc2	kožešina
(	(	kIx(	(
<g/>
dodávaných	dodávaný	k2eAgFnPc2d1	dodávaná
z	z	k7c2	z
Núbie	Núbie	k1gFnSc2	Núbie
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
forma	forma	k1gFnSc1	forma
tributu	tribut	k1gInSc2	tribut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
první	první	k4xOgFnSc1	první
malba	malba	k1gFnSc1	malba
krotkého	krotký	k2eAgMnSc2d1	krotký
levharta	levhart	k1gMnSc2	levhart
kráčejícího	kráčející	k2eAgMnSc2d1	kráčející
na	na	k7c6	na
vodítku	vodítko	k1gNnSc6	vodítko
po	po	k7c6	po
boku	bok	k1gInSc6	bok
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
mytologii	mytologie	k1gFnSc6	mytologie
tvořil	tvořit	k5eAaImAgMnS	tvořit
levhart	levhart	k1gMnSc1	levhart
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
podob	podoba	k1gFnPc2	podoba
bohyně	bohyně	k1gFnSc2	bohyně
Mafdet	Mafdet	k1gInSc1	Mafdet
<g/>
,	,	kIx,	,
ochránkyně	ochránkyně	k1gFnSc1	ochránkyně
před	před	k7c4	před
hady	had	k1gMnPc4	had
a	a	k8xC	a
štíry	štír	k1gMnPc4	štír
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
i	i	k9	i
první	první	k4xOgFnSc1	první
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
levhart	levhart	k1gMnSc1	levhart
získal	získat	k5eAaPmAgMnS	získat
své	svůj	k3xOyFgFnPc4	svůj
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
:	:	kIx,	:
bůh	bůh	k1gMnSc1	bůh
Anup	Anup	k1gMnSc1	Anup
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
zkazit	zkazit	k5eAaPmF	zkazit
Sutechovi	Sutechův	k2eAgMnPc1d1	Sutechův
maskování	maskování	k1gNnSc2	maskování
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pouštního	pouštní	k2eAgInSc2d1	pouštní
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pískově	pískově	k6eAd1	pískově
zbarveného	zbarvený	k2eAgMnSc2d1	zbarvený
leoparda	leopard	k1gMnSc2	leopard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
Řím	Řím	k1gInSc1	Řím
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
býval	bývat	k5eAaImAgMnS	bývat
leopard	leopard	k1gMnSc1	leopard
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
kultem	kult	k1gInSc7	kult
boha	bůh	k1gMnSc2	bůh
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
často	často	k6eAd1	často
nosil	nosit	k5eAaImAgInS	nosit
jeho	jeho	k3xOp3gFnSc4	jeho
kůži	kůže	k1gFnSc4	kůže
či	či	k8xC	či
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
dokonce	dokonce	k9	dokonce
jezdil	jezdit	k5eAaImAgMnS	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Šelma	šelma	k1gFnSc1	šelma
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
divokou	divoký	k2eAgFnSc4d1	divoká
až	až	k9	až
divošskou	divošský	k2eAgFnSc4d1	divošská
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
nevázanost	nevázanost	k1gFnSc4	nevázanost
a	a	k8xC	a
smyslnost	smyslnost	k1gFnSc4	smyslnost
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
zde	zde	k6eAd1	zde
obvykle	obvykle	k6eAd1	obvykle
platil	platit	k5eAaImAgInS	platit
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
statečnosti	statečnost	k1gFnSc2	statečnost
a	a	k8xC	a
bojovnosti	bojovnost	k1gFnSc2	bojovnost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
ho	on	k3xPp3gNnSc4	on
naopak	naopak	k6eAd1	naopak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
králíkem	králík	k1gMnSc7	králík
<g/>
,	,	kIx,	,
myší	myš	k1gFnSc7	myš
<g/>
,	,	kIx,	,
oslem	osel	k1gMnSc7	osel
a	a	k8xC	a
hyenou	hyena	k1gFnSc7	hyena
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
zvířata	zvíře	k1gNnPc4	zvíře
bázlivá	bázlivý	k2eAgNnPc4d1	bázlivé
a	a	k8xC	a
zbabělá	zbabělý	k2eAgNnPc4d1	zbabělé
<g/>
.	.	kIx.	.
</s>
<s>
Etruskové	Etrusk	k1gMnPc1	Etrusk
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
vztah	vztah	k1gInSc4	vztah
–	–	k?	–
v	v	k7c6	v
Tarquinii	Tarquinie	k1gFnSc6	Tarquinie
se	se	k3xPyFc4	se
našla	najít	k5eAaPmAgFnS	najít
místnost	místnost	k1gFnSc1	místnost
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Hrobka	hrobka	k1gFnSc1	hrobka
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
stěně	stěna	k1gFnSc6	stěna
jsou	být	k5eAaImIp3nP	být
vyobrazeny	vyobrazen	k2eAgFnPc1d1	vyobrazena
dvě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
šelmy	šelma	k1gFnPc1	šelma
představující	představující	k2eAgFnPc1d1	představující
zřejmě	zřejmě	k6eAd1	zřejmě
ochránce	ochránce	k1gMnSc1	ochránce
hodující	hodující	k2eAgFnSc2d1	hodující
společnosti	společnost	k1gFnSc2	společnost
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
<g/>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
odchytávali	odchytávat	k5eAaImAgMnP	odchytávat
levharty	levhart	k1gMnPc4	levhart
a	a	k8xC	a
využívali	využívat	k5eAaPmAgMnP	využívat
je	on	k3xPp3gMnPc4	on
jako	jako	k8xC	jako
bojová	bojový	k2eAgNnPc4d1	bojové
zvířata	zvíře	k1gNnPc4	zvíře
pro	pro	k7c4	pro
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
arénách	aréna	k1gFnPc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
gladiátory	gladiátor	k1gMnPc7	gladiátor
nebo	nebo	k8xC	nebo
mohli	moct	k5eAaImAgMnP	moct
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
přežít	přežít	k5eAaPmF	přežít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byli	být	k5eAaImAgMnP	být
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
k	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
odsouzenců	odsouzenec	k1gMnPc2	odsouzenec
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
byly	být	k5eAaImAgFnP	být
mnohdy	mnohdy	k6eAd1	mnohdy
enormní	enormní	k2eAgFnPc1d1	enormní
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
100	[number]	k4	100
dní	den	k1gInPc2	den
trvajících	trvající	k2eAgFnPc2d1	trvající
her	hra	k1gFnPc2	hra
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
otevření	otevření	k1gNnSc1	otevření
Kolosea	Koloseum	k1gNnSc2	Koloseum
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
asi	asi	k9	asi
9000	[number]	k4	9000
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
410	[number]	k4	410
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
končily	končit	k5eAaImAgInP	končit
poslední	poslední	k2eAgInPc4d1	poslední
dozvuky	dozvuk	k1gInPc4	dozvuk
těchto	tento	k3xDgInPc2	tento
masakrů	masakr	k1gInPc2	masakr
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
levharti	levhart	k1gMnPc1	levhart
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nežili	žít	k5eNaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Trajána	Traján	k1gMnSc2	Traján
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
11	[number]	k4	11
000	[number]	k4	000
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vymizení	vymizení	k1gNnSc3	vymizení
lokálních	lokální	k2eAgFnPc2d1	lokální
populací	populace	k1gFnPc2	populace
šelem	šelma	k1gFnPc2	šelma
přispívalo	přispívat	k5eAaImAgNnS	přispívat
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohatí	bohatý	k2eAgMnPc1d1	bohatý
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
provozovali	provozovat	k5eAaImAgMnP	provozovat
soukromé	soukromý	k2eAgFnPc4d1	soukromá
menažerie	menažerie	k1gFnPc4	menažerie
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
nacházet	nacházet	k5eAaImF	nacházet
tisíce	tisíc	k4xCgInPc1	tisíc
různých	různý	k2eAgFnPc2d1	různá
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
císař	císař	k1gMnSc1	císař
Octavianus	Octavianus	k1gMnSc1	Octavianus
Augustus	Augustus	k1gMnSc1	Augustus
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
3500	[number]	k4	3500
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
bylo	být	k5eAaImAgNnS	být
680	[number]	k4	680
lvů	lev	k1gMnPc2	lev
a	a	k8xC	a
600	[number]	k4	600
levhartů	levhart	k1gMnPc2	levhart
a	a	k8xC	a
gepardů	gepard	k1gMnPc2	gepard
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgInPc4	všechen
nechal	nechat	k5eAaPmAgInS	nechat
během	během	k7c2	během
26	[number]	k4	26
slavností	slavnost	k1gFnPc2	slavnost
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Říma	Řím	k1gInSc2	Řím
umírali	umírat	k5eAaImAgMnP	umírat
levharti	levhart	k1gMnPc1	levhart
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
arénách	aréna	k1gFnPc6	aréna
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Africké	africký	k2eAgInPc1d1	africký
kmeny	kmen	k1gInPc1	kmen
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
původních	původní	k2eAgNnPc6d1	původní
afrických	africký	k2eAgNnPc6d1	africké
kmenových	kmenový	k2eAgNnPc6d1	kmenové
společenstvech	společenstvo	k1gNnPc6	společenstvo
byl	být	k5eAaImAgMnS	být
levhart	levhart	k1gMnSc1	levhart
obávaný	obávaný	k2eAgMnSc1d1	obávaný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
obdivovaný	obdivovaný	k2eAgMnSc1d1	obdivovaný
predátor	predátor	k1gMnSc1	predátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
moc	moc	k6eAd1	moc
válečníků	válečník	k1gMnPc2	válečník
a	a	k8xC	a
náčelníků	náčelník	k1gMnPc2	náčelník
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
odvážný	odvážný	k2eAgInSc1d1	odvážný
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
mazaný	mazaný	k2eAgInSc1d1	mazaný
<g/>
.	.	kIx.	.
</s>
<s>
Leoparda	leopard	k1gMnSc4	leopard
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
lva	lev	k1gInSc2	lev
považují	považovat	k5eAaImIp3nP	považovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
kmeny	kmen	k1gInPc1	kmen
za	za	k7c4	za
skutečného	skutečný	k2eAgMnSc4d1	skutečný
krále	král	k1gMnSc4	král
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
levhart	levhart	k1gMnSc1	levhart
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
chytřejší	chytrý	k2eAgMnSc1d2	chytřejší
a	a	k8xC	a
lepší	dobrý	k2eAgMnSc1d2	lepší
lovec	lovec	k1gMnSc1	lovec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
hůře	zle	k6eAd2	zle
ulovitelný	ulovitelný	k2eAgMnSc1d1	ulovitelný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Zuluů	Zulu	k1gMnPc2	Zulu
a	a	k8xC	a
tradičních	tradiční	k2eAgFnPc2d1	tradiční
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
Beninu	Benin	k1gInSc6	Benin
nošení	nošení	k1gNnSc2	nošení
leopardí	leopardí	k2eAgFnSc2d1	leopardí
kůže	kůže	k1gFnSc2	kůže
stále	stále	k6eAd1	stále
označuje	označovat	k5eAaImIp3nS	označovat
královskou	královský	k2eAgFnSc4d1	královská
(	(	kIx(	(
<g/>
náčelnickou	náčelnický	k2eAgFnSc4d1	náčelnická
<g/>
)	)	kIx)	)
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
králům	král	k1gMnPc3	král
je	být	k5eAaImIp3nS	být
připisováno	připisován	k2eAgNnSc1d1	připisováno
spojení	spojení	k1gNnSc1	spojení
lidské	lidský	k2eAgFnSc2d1	lidská
a	a	k8xC	a
levhartí	levhartí	k2eAgFnSc2d1	levhartí
osobnosti	osobnost	k1gFnSc2	osobnost
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Beninští	Beninský	k2eAgMnPc1d1	Beninský
králové	král	k1gMnPc1	král
titulovaní	titulovaný	k2eAgMnPc1d1	titulovaný
"	"	kIx"	"
<g/>
Oba	dva	k4xCgMnPc1	dva
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
obvykle	obvykle	k6eAd1	obvykle
vydržovali	vydržovat	k5eAaImAgMnP	vydržovat
několik	několik	k4yIc4	několik
levhartů	levhart	k1gMnPc2	levhart
odchycených	odchycený	k2eAgMnPc2d1	odchycený
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
a	a	k8xC	a
předváděli	předvádět	k5eAaImAgMnP	předvádět
je	on	k3xPp3gMnPc4	on
při	při	k7c6	při
ceremoniálech	ceremoniál	k1gInPc6	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
slavnosti	slavnost	k1gFnSc2	slavnost
"	"	kIx"	"
<g/>
Igue	Igue	k1gInSc1	Igue
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
býval	bývat	k5eAaImAgMnS	bývat
jeden	jeden	k4xCgMnSc1	jeden
levhart	levhart	k1gMnSc1	levhart
obětován	obětovat	k5eAaBmNgMnS	obětovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
obnovení	obnovení	k1gNnSc1	obnovení
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
Igbů	Igb	k1gInPc2	Igb
z	z	k7c2	z
Nigérie	Nigérie	k1gFnSc2	Nigérie
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
znovuzrodí	znovuzrodit	k5eAaPmIp3nP	znovuzrodit
jako	jako	k9	jako
sloni	slon	k1gMnPc1	slon
nebo	nebo	k8xC	nebo
levharti	levhart	k1gMnPc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
negativní	negativní	k2eAgInSc4d1	negativní
obraz	obraz	k1gInSc4	obraz
požíval	požívat	k5eAaImAgMnS	požívat
levhart	levhart	k1gMnSc1	levhart
mezi	mezi	k7c7	mezi
domorodci	domorodec	k1gMnPc7	domorodec
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
průvodce	průvodce	k1gMnPc4	průvodce
kouzelníků	kouzelník	k1gMnPc2	kouzelník
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
prý	prý	k9	prý
mohli	moct	k5eAaImAgMnP	moct
za	za	k7c4	za
veškeré	veškerý	k3xTgNnSc4	veškerý
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
místní	místní	k2eAgMnPc4d1	místní
lidi	člověk	k1gMnPc4	člověk
postihovalo	postihovat	k5eAaImAgNnS	postihovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
existoval	existovat	k5eAaImAgInS	existovat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
krvavý	krvavý	k2eAgInSc4d1	krvavý
kult	kult	k1gInSc4	kult
tzv.	tzv.	kA	tzv.
leopardích	leopardí	k2eAgMnPc2d1	leopardí
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Říkali	říkat	k5eAaImAgMnP	říkat
si	se	k3xPyFc3	se
Anioto	Aniota	k1gFnSc5	Aniota
<g/>
,	,	kIx,	,
nosili	nosit	k5eAaImAgMnP	nosit
levhartí	levhartí	k2eAgFnPc4d1	levhartí
kůže	kůže	k1gFnPc4	kůže
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
ozbrojeni	ozbrojit	k5eAaPmNgMnP	ozbrojit
ostrými	ostrý	k2eAgInPc7d1	ostrý
kovovými	kovový	k2eAgInPc7d1	kovový
háky	hák	k1gInPc7	hák
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
ubodávali	ubodávat	k5eAaImAgMnP	ubodávat
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Provozovali	provozovat	k5eAaImAgMnP	provozovat
rituály	rituál	k1gInPc4	rituál
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kanibalismus	kanibalismus	k1gInSc4	kanibalismus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
posílili	posílit	k5eAaPmAgMnP	posílit
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
kmen	kmen	k1gInSc4	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Žido-křesťanský	Židořesťanský	k2eAgInSc4d1	Žido-křesťanský
pohled	pohled	k1gInSc4	pohled
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
respektive	respektive	k9	respektive
křesťanství	křesťanství	k1gNnSc1	křesťanství
vychází	vycházet	k5eAaImIp3nS	vycházet
obraz	obraz	k1gInSc1	obraz
leoparda	leopard	k1gMnSc4	leopard
především	především	k6eAd1	především
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zpodobňován	zpodobňován	k2eAgInSc1d1	zpodobňován
jako	jako	k8xC	jako
divoký	divoký	k2eAgInSc1d1	divoký
<g/>
,	,	kIx,	,
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
mrštný	mrštný	k2eAgMnSc1d1	mrštný
predátor	predátor	k1gMnSc1	predátor
<g/>
,	,	kIx,	,
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
domácí	domácí	k2eAgNnPc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vypodobnění	vypodobnění	k1gNnSc6	vypodobnění
království	království	k1gNnSc2	království
Božího	boží	k2eAgNnSc2d1	boží
<g/>
,	,	kIx,	,
symbolizujícího	symbolizující	k2eAgNnSc2d1	symbolizující
mír	mír	k1gInSc4	mír
a	a	k8xC	a
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
levhart	levhart	k1gMnSc1	levhart
harmonicky	harmonicky	k6eAd1	harmonicky
polehávat	polehávat	k5eAaImF	polehávat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
šelmami	šelma	k1gFnPc7	šelma
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
pozemskou	pozemský	k2eAgFnSc7d1	pozemská
kořistí	kořist	k1gFnSc7	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
praví	pravit	k5eAaBmIp3nS	pravit
v	v	k7c6	v
Izajášově	Izajášův	k2eAgNnSc6d1	Izajášovo
proroctví	proroctví	k1gNnSc6	proroctví
–	–	k?	–
Iz	Iz	k1gFnSc1	Iz
11	[number]	k4	11
<g/>
,	,	kIx,	,
6	[number]	k4	6
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vlk	Vlk	k1gMnSc1	Vlk
bude	být	k5eAaImBp3nS	být
pobývat	pobývat	k5eAaImF	pobývat
s	s	k7c7	s
beránkem	beránek	k1gMnSc7	beránek
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
s	s	k7c7	s
kůzletem	kůzle	k1gNnSc7	kůzle
odpočívat	odpočívat	k5eAaImF	odpočívat
<g/>
.	.	kIx.	.
</s>
<s>
Tele	tele	k1gNnSc4	tele
a	a	k8xC	a
lvíče	lvíče	k1gNnSc4	lvíče
i	i	k8xC	i
žírný	žírný	k2eAgInSc4d1	žírný
dobytek	dobytek	k1gInSc4	dobytek
budou	být	k5eAaImBp3nP	být
spolu	spolu	k6eAd1	spolu
a	a	k8xC	a
malý	malý	k2eAgMnSc1d1	malý
hoch	hoch	k1gMnSc1	hoch
je	on	k3xPp3gFnPc4	on
bude	být	k5eAaImBp3nS	být
vodit	vodit	k5eAaImF	vodit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
====	====	k?	====
Středověké	středověký	k2eAgFnSc2d1	středověká
menažerie	menažerie	k1gFnSc2	menažerie
====	====	k?	====
</s>
</p>
<p>
<s>
Vládci	vládce	k1gMnPc1	vládce
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
říší	říš	k1gFnPc2	říš
Asie	Asie	k1gFnSc2	Asie
si	se	k3xPyFc3	se
často	často	k6eAd1	často
vydržovali	vydržovat	k5eAaImAgMnP	vydržovat
velké	velký	k2eAgFnPc4d1	velká
menažerie	menažerie	k1gFnPc4	menažerie
plné	plný	k2eAgFnPc4d1	plná
exotických	exotický	k2eAgMnPc2d1	exotický
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgMnPc4d1	oblíbený
tvory	tvor	k1gMnPc4	tvor
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k8xC	i
levharti	levhart	k1gMnPc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
v	v	k7c6	v
nich	on	k3xPp3gMnPc2	on
sloužila	sloužit	k5eAaImAgFnS	sloužit
především	především	k9	především
na	na	k7c6	na
předvádění	předvádění	k1gNnSc6	předvádění
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
prestiže	prestiž	k1gFnSc2	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnSc3d1	významná
zvěřince	zvěřinka	k1gFnSc3	zvěřinka
měli	mít	k5eAaImAgMnP	mít
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
dvorech	dvůr	k1gInPc6	dvůr
panovníci	panovník	k1gMnPc1	panovník
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
londýnský	londýnský	k2eAgInSc1d1	londýnský
Tower	Tower	k1gInSc1	Tower
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chán	chán	k1gMnSc1	chán
Kublaj	Kublaj	k1gMnSc1	Kublaj
(	(	kIx(	(
<g/>
Šang-tu	Šang	k1gInSc2	Šang-t
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Královská	královský	k2eAgFnSc1d1	královská
obora	obora	k1gFnSc1	obora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
zvířata	zvíře	k1gNnPc4	zvíře
vzal	vzít	k5eAaPmAgMnS	vzít
i	i	k8xC	i
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
Anglickou	anglický	k2eAgFnSc7d1	anglická
do	do	k7c2	do
Wormsu	Worms	k1gInSc2	Worms
<g/>
.	.	kIx.	.
</s>
<s>
Menažerii	Menažerie	k1gFnSc4	Menažerie
s	s	k7c7	s
levharty	levhart	k1gMnPc7	levhart
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
dokonce	dokonce	k9	dokonce
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
X.	X.	kA	X.
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Asie	Asie	k1gFnSc2	Asie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
perském	perský	k2eAgNnSc6d1	perské
kulturním	kulturní	k2eAgNnSc6d1	kulturní
prostředí	prostředí	k1gNnSc6	prostředí
hraje	hrát	k5eAaImIp3nS	hrát
levhart	levhart	k1gMnSc1	levhart
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
dvanáct	dvanáct	k4xCc4	dvanáct
zvířat	zvíře	k1gNnPc2	zvíře
tradičního	tradiční	k2eAgInSc2d1	tradiční
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
symbolem	symbol	k1gInSc7	symbol
íránského	íránský	k2eAgInSc2d1	íránský
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
obvykle	obvykle	k6eAd1	obvykle
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
nebojácnost	nebojácnost	k1gFnSc1	nebojácnost
<g/>
,	,	kIx,	,
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zuřivost	zuřivost	k1gFnSc4	zuřivost
<g/>
,	,	kIx,	,
zášť	zášť	k1gFnSc4	zášť
<g/>
,	,	kIx,	,
marnivost	marnivost	k1gFnSc4	marnivost
a	a	k8xC	a
pýchu	pýcha	k1gFnSc4	pýcha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobře	dobře	k6eAd1	dobře
známém	známý	k2eAgInSc6d1	známý
příběhu	příběh	k1gInSc6	příběh
Levhart	levhart	k1gMnSc1	levhart
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
leopard	leopard	k1gMnSc1	leopard
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
strhnout	strhnout	k5eAaPmF	strhnout
Měsíc	měsíc	k1gInSc1	měsíc
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vyleze	vylézt	k5eAaPmIp3nS	vylézt
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
útes	útes	k1gInSc4	útes
<g/>
,	,	kIx,	,
skočí	skočit	k5eAaPmIp3nS	skočit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měsíc	měsíc	k1gInSc4	měsíc
popadl	popadnout	k5eAaPmAgMnS	popadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
padá	padat	k5eAaImIp3nS	padat
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Indii	Indie	k1gFnSc6	Indie
v	v	k7c4	v
pohoří	pohoří	k1gNnSc4	pohoří
Sátpurá	Sátpurý	k2eAgNnPc4d1	Sátpurý
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
některými	některý	k3yIgMnPc7	některý
místními	místní	k2eAgMnPc7d1	místní
domorodci	domorodec	k1gMnPc7	domorodec
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
jakési	jakýsi	k3yIgInPc4	jakýsi
"	"	kIx"	"
<g/>
panterodlaky	panterodlak	k1gInPc4	panterodlak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
můžou	můžou	k?	můžou
měnit	měnit	k5eAaImF	měnit
svůj	svůj	k3xOyFgInSc4	svůj
zevnějšek	zevnějšek	k1gInSc4	zevnějšek
z	z	k7c2	z
lidského	lidský	k2eAgMnSc2d1	lidský
na	na	k7c4	na
levhartí	levhartí	k2eAgFnSc4d1	levhartí
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Lidožravý	lidožravý	k2eAgMnSc1d1	lidožravý
levhart	levhart	k1gMnSc1	levhart
z	z	k7c2	z
Kahání	Kahání	k1gNnSc2	Kahání
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zabil	zabít	k5eAaPmAgMnS	zabít
desítky	desítka	k1gFnPc4	desítka
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
prý	prý	k9	prý
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Číně	Čína	k1gFnSc6	Čína
byl	být	k5eAaImAgMnS	být
levhart	levhart	k1gMnSc1	levhart
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vtělení	vtělení	k1gNnPc4	vtělení
boha	bůh	k1gMnSc2	bůh
Ťi	Ťi	k1gMnSc2	Ťi
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
zvířata	zvíře	k1gNnPc4	zvíře
</s>
<s>
symbolizující	symbolizující	k2eAgFnPc1d1	symbolizující
bojové	bojový	k2eAgFnPc1d1	bojová
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
výšivky	výšivka	k1gFnPc1	výšivka
na	na	k7c6	na
úředních	úřední	k2eAgInPc6d1	úřední
oděvech	oděv	k1gInPc6	oděv
označovaly	označovat	k5eAaImAgInP	označovat
třetí	třetí	k4xOgFnSc4	třetí
nebo	nebo	k8xC	nebo
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hodnost	hodnost	k1gFnSc1	hodnost
čínských	čínský	k2eAgMnPc2d1	čínský
vojenských	vojenský	k2eAgMnPc2d1	vojenský
hodnostářů	hodnostář	k1gMnPc2	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
šelma	šelma	k1gFnSc1	šelma
nehraje	hrát	k5eNaImIp3nS	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
v	v	k7c4	v
čínské	čínský	k2eAgMnPc4d1	čínský
respektive	respektive	k9	respektive
obecně	obecně	k6eAd1	obecně
dálněvýchodní	dálněvýchodní	k2eAgFnSc4d1	dálněvýchodní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
mytologii	mytologie	k1gFnSc4	mytologie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šaolinském	šaolinský	k2eAgNnSc6d1	šaolinské
bojovém	bojový	k2eAgNnSc6d1	bojové
umění	umění	k1gNnSc6	umění
kung-fu	kung	k1gInSc2	kung-f
představoval	představovat	k5eAaImAgMnS	představovat
styl	styl	k1gInSc4	styl
levharta	levhart	k1gMnSc2	levhart
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
zvířecích	zvířecí	k2eAgInPc2d1	zvířecí
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
byli	být	k5eAaImAgMnP	být
levharti	levhart	k1gMnPc1	levhart
součástí	součást	k1gFnPc2	součást
obětního	obětní	k2eAgInSc2d1	obětní
obřadu	obřad	k1gInSc2	obřad
Rampok	Rampok	k1gInSc1	Rampok
(	(	kIx(	(
<g/>
Rampog	Rampog	k1gInSc1	Rampog
<g/>
)	)	kIx)	)
Macan	Macan	k1gInSc1	Macan
<g/>
,	,	kIx,	,
rozšířeném	rozšířený	k2eAgMnSc6d1	rozšířený
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
dvorech	dvůr	k1gInPc6	dvůr
místních	místní	k2eAgMnPc2d1	místní
vládců	vládce	k1gMnPc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Rituál	rituál	k1gInSc1	rituál
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
zabíjení	zabíjení	k1gNnSc6	zabíjení
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
odchycených	odchycený	k2eAgMnPc2d1	odchycený
tygrů	tygr	k1gMnPc2	tygr
a	a	k8xC	a
levhartů	levhart	k1gMnPc2	levhart
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
očištění	očištění	k1gNnSc1	očištění
země	zem	k1gFnSc2	zem
od	od	k7c2	od
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
v	v	k7c4	v
jakousi	jakýsi	k3yIgFnSc4	jakýsi
šou	šou	k?	šou
určenou	určený	k2eAgFnSc4d1	určená
k	k	k7c3	k
pobavení	pobavení	k1gNnSc3	pobavení
místního	místní	k2eAgNnSc2d1	místní
publika	publikum	k1gNnSc2	publikum
a	a	k8xC	a
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
zde	zde	k6eAd1	zde
většinou	většinou	k6eAd1	většinou
sloužili	sloužit	k5eAaImAgMnP	sloužit
jako	jako	k8xC	jako
náhražka	náhražka	k1gFnSc1	náhražka
za	za	k7c4	za
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
vzácnější	vzácný	k2eAgMnPc4d2	vzácnější
tygry	tygr	k1gMnPc4	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Rituál	rituál	k1gInSc1	rituál
byl	být	k5eAaImAgInS	být
členěn	členit	k5eAaImNgInS	členit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
boj	boj	k1gInSc4	boj
tygra	tygr	k1gMnSc2	tygr
s	s	k7c7	s
vodním	vodní	k2eAgMnSc7d1	vodní
buvolem	buvol	k1gMnSc7	buvol
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
šelma	šelma	k1gFnSc1	šelma
většinou	většinou	k6eAd1	většinou
prohrála	prohrát	k5eAaPmAgFnS	prohrát
(	(	kIx(	(
<g/>
sima-maésa	simaaésa	k1gFnSc1	sima-maésa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
představením	představení	k1gNnSc7	představení
bylo	být	k5eAaImAgNnS	být
umístění	umístění	k1gNnSc4	umístění
odchycených	odchycený	k2eAgNnPc2d1	odchycené
zvířat	zvíře	k1gNnPc2	zvíře
na	na	k7c6	na
prostranství	prostranství	k1gNnSc6	prostranství
obestoupené	obestoupený	k2eAgMnPc4d1	obestoupený
kopiníky	kopiník	k1gMnPc4	kopiník
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
a	a	k8xC	a
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
obklíčení	obklíčení	k1gNnSc2	obklíčení
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
obvykle	obvykle	k6eAd1	obvykle
propíchnuti	propíchnut	k2eAgMnPc1d1	propíchnut
kopími	kopí	k1gNnPc7	kopí
a	a	k8xC	a
zabiti	zabít	k5eAaPmNgMnP	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lovecký	lovecký	k2eAgMnSc1d1	lovecký
levhart	levhart	k1gMnSc1	levhart
====	====	k?	====
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
tak	tak	k6eAd1	tak
populární	populární	k2eAgNnSc4d1	populární
lovecké	lovecký	k2eAgNnSc4d1	lovecké
zvíře	zvíře	k1gNnSc4	zvíře
jako	jako	k8xS	jako
například	například	k6eAd1	například
gepard	gepard	k1gMnSc1	gepard
či	či	k8xC	či
karakal	karakal	k1gMnSc1	karakal
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
doklad	doklad	k1gInSc1	doklad
o	o	k7c4	o
využívání	využívání	k1gNnSc4	využívání
levhartů	levhart	k1gMnPc2	levhart
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
pochází	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
z	z	k7c2	z
Uruku	Uruk	k1gInSc2	Uruk
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2600	[number]	k4	2600
až	až	k6eAd1	až
2360	[number]	k4	2360
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Archeologové	archeolog	k1gMnPc1	archeolog
objevili	objevit	k5eAaPmAgMnP	objevit
válcové	válcový	k2eAgNnSc4d1	válcové
pečetidlo	pečetidlo	k1gNnSc4	pečetidlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
členitém	členitý	k2eAgInSc6d1	členitý
terénu	terén	k1gInSc6	terén
vyobrazeni	vyobrazen	k2eAgMnPc1d1	vyobrazen
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
lovící	lovící	k2eAgFnSc2d1	lovící
kozy	koza	k1gFnSc2	koza
a	a	k8xC	a
držící	držící	k2eAgMnPc1d1	držící
na	na	k7c6	na
vodítku	vodítko	k1gNnSc6	vodítko
levharta	levhart	k1gMnSc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Indii	Indie	k1gFnSc6	Indie
byli	být	k5eAaImAgMnP	být
levharti	levhart	k1gMnPc1	levhart
rovněž	rovněž	k9	rovněž
používáni	používán	k2eAgMnPc1d1	používán
jako	jako	k8xS	jako
lovecká	lovecký	k2eAgNnPc4d1	lovecké
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Cvičení	cvičený	k2eAgMnPc1d1	cvičený
levharti	levhart	k1gMnPc1	levhart
lovili	lovit	k5eAaImAgMnP	lovit
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
gepardi	gepard	k1gMnPc1	gepard
a	a	k8xC	a
karakalové	karakal	k1gMnPc1	karakal
–	–	k?	–
využívali	využívat	k5eAaImAgMnP	využívat
svou	svůj	k3xOyFgFnSc4	svůj
mrštnost	mrštnost	k1gFnSc4	mrštnost
a	a	k8xC	a
šplhací	šplhací	k2eAgFnPc4d1	šplhací
schopnosti	schopnost	k1gFnPc4	schopnost
ve	v	k7c6	v
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
a	a	k8xC	a
nepřehledném	přehledný	k2eNgInSc6d1	nepřehledný
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
chytali	chytat	k5eAaImAgMnP	chytat
divoké	divoký	k2eAgFnPc4d1	divoká
ovce	ovce	k1gFnPc4	ovce
a	a	k8xC	a
kozy	koza	k1gFnPc4	koza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Heraldika	heraldika	k1gFnSc1	heraldika
====	====	k?	====
</s>
</p>
<p>
<s>
Historický	historický	k2eAgMnSc1d1	historický
heraldický	heraldický	k2eAgMnSc1d1	heraldický
levhart	levhart	k1gMnSc1	levhart
byl	být	k5eAaImAgMnS	být
především	především	k6eAd1	především
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xS	jako
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
s	s	k7c7	s
hřívou	hříva	k1gFnSc7	hříva
a	a	k8xC	a
bez	bez	k7c2	bez
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
křížence	kříženec	k1gMnSc4	kříženec
(	(	kIx(	(
<g/>
bastarda	bastard	k1gMnSc4	bastard
<g/>
)	)	kIx)	)
lva	lev	k1gMnSc4	lev
a	a	k8xC	a
pardála	pardál	k1gMnSc4	pardál
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
erbu	erb	k1gInSc2	erb
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
proto	proto	k6eAd1	proto
mnohdy	mnohdy	k6eAd1	mnohdy
dávali	dávat	k5eAaImAgMnP	dávat
významní	významný	k2eAgMnPc1d1	významný
levobočci	levoboček	k1gMnPc1	levoboček
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Vilém	Vilém	k1gMnSc1	Vilém
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
heraldického	heraldický	k2eAgMnSc2d1	heraldický
lva	lev	k1gMnSc2	lev
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgMnS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kráčel	kráčet	k5eAaImAgMnS	kráčet
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
natočený	natočený	k2eAgInSc4d1	natočený
hlavou	hlava	k1gFnSc7	hlava
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pozice	pozice	k1gFnSc1	pozice
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
passant	passant	k1gInSc4	passant
guardant	guardanta	k1gFnPc2	guardanta
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
však	však	k9	však
zvíře	zvíře	k1gNnSc1	zvíře
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
představovalo	představovat	k5eAaImAgNnS	představovat
levharta	levhart	k1gMnSc4	levhart
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
šlo	jít	k5eAaImAgNnS	jít
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
lva	lev	k1gMnSc4	lev
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
mnohdy	mnohdy	k6eAd1	mnohdy
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
historie	historie	k1gFnSc2	historie
či	či	k8xC	či
prostředí	prostředí	k1gNnSc2	prostředí
vzniku	vznik	k1gInSc3	vznik
erbu	erb	k1gInSc2	erb
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
heraldika	heraldika	k1gFnSc1	heraldika
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
anglické	anglický	k2eAgFnSc2d1	anglická
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lva	lev	k1gInSc2	lev
passant	passant	k1gMnSc1	passant
guardant	guardant	k1gMnSc1	guardant
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
leoparda	leopard	k1gMnSc4	leopard
<g/>
.	.	kIx.	.
</s>
<s>
Klasicky	klasicky	k6eAd1	klasicky
zobrazovaný	zobrazovaný	k2eAgMnSc1d1	zobrazovaný
levhart	levhart	k1gMnSc1	levhart
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
heraldice	heraldika	k1gFnSc6	heraldika
rozhodně	rozhodně	k6eAd1	rozhodně
tak	tak	k6eAd1	tak
populární	populární	k2eAgMnSc1d1	populární
jako	jako	k8xC	jako
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
heraldice	heraldika	k1gFnSc6	heraldika
je	být	k5eAaImIp3nS	být
levhart	levhart	k1gMnSc1	levhart
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpoznatelný	rozpoznatelný	k2eAgMnSc1d1	rozpoznatelný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mívá	mívat	k5eAaImIp3nS	mívat
skvrny	skvrna	k1gFnPc4	skvrna
a	a	k8xC	a
rozety	rozeta	k1gFnPc4	rozeta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
hřívy	hříva	k1gFnSc2	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
má	mít	k5eAaImIp3nS	mít
levharta	levhart	k1gMnSc4	levhart
Benin	Benin	k2eAgInSc1d1	Benin
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Gabon	Gabon	k1gNnSc1	Gabon
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
,	,	kIx,	,
Somálsko	Somálsko	k1gNnSc1	Somálsko
a	a	k8xC	a
jako	jako	k9	jako
lva	lev	k1gMnSc4	lev
passant	passant	k1gMnSc1	passant
guardant	guardant	k1gMnSc1	guardant
například	například	k6eAd1	například
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Fidži	Fidž	k1gFnSc6	Fidž
či	či	k8xC	či
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
Hohenlohe	Hohenloh	k1gFnSc2	Hohenloh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Levhart	levhart	k1gMnSc1	levhart
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
doba	doba	k1gFnSc1	doba
====	====	k?	====
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
si	se	k3xPyFc3	se
vybralo	vybrat	k5eAaPmAgNnS	vybrat
levharta	levhart	k1gMnSc4	levhart
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
ceněné	ceněný	k2eAgNnSc1d1	ceněné
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
atletický	atletický	k2eAgInSc4d1	atletický
klub	klub	k1gInSc4	klub
Laffayette	Laffayett	k1gInSc5	Laffayett
Leopards	Leopards	k1gInSc1	Leopards
z	z	k7c2	z
Eastonu	Easton	k1gInSc2	Easton
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
tým	tým	k1gInSc1	tým
AFC	AFC	kA	AFC
Leopards	Leopards	k1gInSc1	Leopards
z	z	k7c2	z
Nairobi	Nairobi	k1gNnSc2	Nairobi
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
tým	tým	k1gInSc4	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
The	The	k1gFnSc2	The
Atlanta	Atlanta	k1gFnSc1	Atlanta
Leopards	Leopards	k1gInSc1	Leopards
<g/>
.	.	kIx.	.
</s>
<s>
Hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
NHL	NHL	kA	NHL
Florida	Florida	k1gFnSc1	Florida
Panthers	Panthers	k1gInSc1	Panthers
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
po	po	k7c6	po
panterovi	panter	k1gMnSc6	panter
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
teoreticky	teoreticky	k6eAd1	teoreticky
po	po	k7c6	po
levhartovi	levhart	k1gMnSc6	levhart
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
pumu	puma	k1gFnSc4	puma
floridskou	floridský	k2eAgFnSc4d1	floridská
<g/>
.	.	kIx.	.
</s>
<s>
Autobus	autobus	k1gInSc1	autobus
firmy	firma	k1gFnSc2	firma
Leyland	Leylanda	k1gFnPc2	Leylanda
a	a	k8xC	a
osobní	osobní	k2eAgInSc4d1	osobní
vůz	vůz	k1gInSc4	vůz
firmy	firma	k1gFnSc2	firma
Nissan	nissan	k1gInSc1	nissan
nesly	nést	k5eAaImAgInP	nést
název	název	k1gInSc4	název
Leopard	leopard	k1gMnSc1	leopard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konkurence	konkurence	k1gFnSc1	konkurence
a	a	k8xC	a
tradice	tradice	k1gFnSc1	tradice
značky	značka	k1gFnSc2	značka
Jaguar	Jaguara	k1gFnPc2	Jaguara
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
větší	veliký	k2eAgNnSc4d2	veliký
prosazení	prosazení	k1gNnSc4	prosazení
v	v	k7c6	v
autoprůmyslu	autoprůmysl	k1gInSc6	autoprůmysl
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
jako	jako	k8xS	jako
na	na	k7c4	na
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
imitaci	imitace	k1gFnSc6	imitace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vydal	vydat	k5eAaPmAgInS	vydat
Apple	Apple	kA	Apple
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
10.5	[number]	k4	10.5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
určený	určený	k2eAgMnSc1d1	určený
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
nahrazený	nahrazený	k2eAgInSc4d1	nahrazený
verzí	verze	k1gFnSc7	verze
10.6	[number]	k4	10.6
Snow	Snow	k1gFnPc2	Snow
Leopard	leopard	k1gMnSc1	leopard
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
užití	užití	k1gNnSc4	užití
symbolu	symbol	k1gInSc2	symbol
leoparda	leopard	k1gMnSc2	leopard
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
černošském	černošský	k2eAgNnSc6d1	černošské
levicovém	levicový	k2eAgNnSc6d1	levicové
hnutí	hnutí	k1gNnSc6	hnutí
Černí	černit	k5eAaImIp3nP	černit
panteři	panter	k1gMnPc1	panter
(	(	kIx(	(
<g/>
Black	Black	k1gInSc1	Black
Panther	Panthra	k1gFnPc2	Panthra
Party	parta	k1gFnSc2	parta
<g/>
)	)	kIx)	)
ustaveném	ustavený	k2eAgInSc6d1	ustavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
logu	log	k1gInSc6	log
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
černý	černý	k2eAgMnSc1d1	černý
levhart	levhart	k1gMnSc1	levhart
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
s	s	k7c7	s
rozevřenými	rozevřený	k2eAgFnPc7d1	rozevřená
čelistmi	čelist	k1gFnPc7	čelist
a	a	k8xC	a
vytasenými	vytasený	k2eAgInPc7d1	vytasený
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
<g/>
Kožešiny	kožešina	k1gFnPc1	kožešina
levhartů	levhart	k1gMnPc2	levhart
či	či	k8xC	či
jejich	jejich	k3xOp3gFnSc1	jejich
imitace	imitace	k1gFnSc1	imitace
<g/>
,	,	kIx,	,
barevné	barevný	k2eAgInPc4d1	barevný
vzory	vzor	k1gInPc4	vzor
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc4	zobrazení
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
hojně	hojně	k6eAd1	hojně
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
<g/>
,	,	kIx,	,
dekoraci	dekorace	k1gFnSc6	dekorace
<g/>
,	,	kIx,	,
šperkařství	šperkařství	k1gNnSc6	šperkařství
či	či	k8xC	či
tatérství	tatérství	k1gNnSc6	tatérství
<g/>
.	.	kIx.	.
</s>
<s>
Konžský	konžský	k2eAgMnSc1d1	konžský
diktátor	diktátor	k1gMnSc1	diktátor
Mobutu	Mobut	k1gInSc2	Mobut
Sese	Sese	k?	Sese
Seko	seko	k1gNnSc1	seko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
objevoval	objevovat	k5eAaImAgInS	objevovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
levhartí	levhartí	k2eAgFnSc6d1	levhartí
čepici	čepice	k1gFnSc6	čepice
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
příklad	příklad	k1gInSc1	příklad
využití	využití	k1gNnSc2	využití
kožešiny	kožešina	k1gFnSc2	kožešina
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
<g/>
.	.	kIx.	.
<g/>
Leopardi	leopard	k1gMnPc1	leopard
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
využívaných	využívaný	k2eAgNnPc2d1	využívané
v	v	k7c6	v
cirkusech	cirkus	k1gInPc6	cirkus
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k6eAd1	tak
hojně	hojně	k6eAd1	hojně
jako	jako	k8xS	jako
tygři	tygr	k1gMnPc1	tygr
a	a	k8xC	a
lvi	lev	k1gMnPc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zde	zde	k6eAd1	zde
živoří	živořit	k5eAaImIp3nP	živořit
v	v	k7c6	v
nuzných	nuzný	k2eAgFnPc6d1	nuzná
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
krutému	krutý	k2eAgInSc3d1	krutý
zacházení	zacházení	k1gNnSc1	zacházení
<g/>
.	.	kIx.	.
<g/>
Levharti	levhart	k1gMnPc1	levhart
byli	být	k5eAaImAgMnP	být
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
námětem	námět	k1gInSc7	námět
pro	pro	k7c4	pro
numizmatické	numizmatický	k2eAgInPc4d1	numizmatický
vzory	vzor	k1gInPc4	vzor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
i	i	k9	i
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Šelmu	šelma	k1gFnSc4	šelma
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaBmF	nalézt
také	také	k9	také
na	na	k7c6	na
známkách	známka	k1gFnPc6	známka
především	především	k6eAd1	především
afrických	africký	k2eAgMnPc2d1	africký
a	a	k8xC	a
západoasijských	západoasijský	k2eAgFnPc2d1	západoasijský
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
figurují	figurovat	k5eAaImIp3nP	figurovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
výtvarných	výtvarný	k2eAgInPc6d1	výtvarný
a	a	k8xC	a
literárních	literární	k2eAgInPc6d1	literární
dílech	díl	k1gInPc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
knižním	knižní	k2eAgMnSc7d1	knižní
levhartem	levhart	k1gMnSc7	levhart
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgMnSc1d1	černý
samec	samec	k1gMnSc1	samec
Baghíra	Baghír	k1gMnSc2	Baghír
z	z	k7c2	z
Knih	kniha	k1gFnPc2	kniha
džunglí	džungle	k1gFnPc2	džungle
od	od	k7c2	od
Rudyarda	Rudyard	k1gMnSc2	Rudyard
Kiplinga	Kipling	k1gMnSc2	Kipling
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
komiksovou	komiksový	k2eAgFnSc7d1	komiksová
postavou	postava	k1gFnSc7	postava
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Marvel	Marvel	k1gInSc1	Marvel
Comics	comics	k1gInSc1	comics
je	být	k5eAaImIp3nS	být
černošský	černošský	k2eAgInSc1d1	černošský
superhrdina	superhrdina	k1gFnSc1	superhrdina
Black	Blacka	k1gFnPc2	Blacka
Panther	Panthra	k1gFnPc2	Panthra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
první	první	k4xOgInSc1	první
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Populární	populární	k2eAgMnPc1d1	populární
levharti	levhart	k1gMnPc1	levhart
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Popularitu	popularita	k1gFnSc4	popularita
levhartů	levhart	k1gMnPc2	levhart
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgInPc1d1	známý
osudy	osud	k1gInPc1	osud
různých	různý	k2eAgMnPc2d1	různý
krotkých	krotký	k2eAgMnPc2d1	krotký
a	a	k8xC	a
polodivokých	polodivoký	k2eAgMnPc2d1	polodivoký
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
natočeny	natočit	k5eAaBmNgInP	natočit
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
sepsány	sepsán	k2eAgFnPc1d1	sepsána
knihy	kniha	k1gFnPc1	kniha
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
prostoru	prostora	k1gFnSc4	prostora
v	v	k7c6	v
masmédiích	masmédium	k1gNnPc6	masmédium
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
monografií	monografie	k1gFnSc7	monografie
pojednávající	pojednávající	k2eAgFnSc7d1	pojednávající
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
krotkého	krotký	k2eAgMnSc4d1	krotký
levharta	levhart	k1gMnSc4	levhart
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
Michaely	Michaela	k1gFnSc2	Michaela
Dennisové	Dennisový	k2eAgFnSc2d1	Dennisová
Leopard	leopard	k1gMnSc1	leopard
in	in	k?	in
my	my	k3xPp1nPc1	my
Lap	lap	k1gInSc1	lap
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
chovatelem	chovatel	k1gMnSc7	chovatel
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Billy	Bill	k1gMnPc4	Bill
Arjan	Arjan	k1gMnSc1	Arjan
Singh	Singh	k1gMnSc1	Singh
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
napsal	napsat	k5eAaPmAgInS	napsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
svých	svůj	k3xOyFgFnPc6	svůj
polodivokých	polodivoký	k2eAgMnPc6d1	polodivoký
levhartech	levhart	k1gMnPc6	levhart
(	(	kIx(	(
<g/>
Prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
Harriet	Harriet	k1gInSc1	Harriet
a	a	k8xC	a
Juliette	Juliett	k1gMnSc5	Juliett
<g/>
)	)	kIx)	)
nazvanou	nazvaný	k2eAgFnSc7d1	nazvaná
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Cats	Cats	k1gInSc1	Cats
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
filmu	film	k1gInSc2	film
The	The	k1gMnSc1	The
Leopard	leopard	k1gMnSc1	leopard
That	That	k1gMnSc1	That
Changed	Changed	k1gMnSc1	Changed
Its	Its	k1gMnSc1	Its
Spots	Spots	k1gInSc4	Spots
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
levhartice	levhartice	k1gFnSc2	levhartice
Harriet	Harrieta	k1gFnPc2	Harrieta
<g/>
.	.	kIx.	.
</s>
<s>
Levhartům	levhart	k1gMnPc3	levhart
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
také	také	k9	také
chovatelka	chovatelka	k1gFnSc1	chovatelka
velkých	velký	k2eAgFnPc2d1	velká
šelem	šelma	k1gFnPc2	šelma
Joy	Joy	k1gFnSc1	Joy
Adamsonová	Adamsonová	k1gFnSc1	Adamsonová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
Příběh	příběh	k1gInSc1	příběh
levhartice	levhartice	k1gFnSc2	levhartice
Penny	penny	k1gFnSc2	penny
(	(	kIx(	(
<g/>
Queen	Queen	k2eAgMnSc1d1	Queen
of	of	k?	of
Shaba	Shaba	k1gMnSc1	Shaba
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
o	o	k7c6	o
navracení	navracení	k1gNnSc6	navracení
osiřelého	osiřelý	k2eAgNnSc2d1	osiřelé
mláděte	mládě	k1gNnSc2	mládě
do	do	k7c2	do
divočiny	divočina	k1gFnSc2	divočina
nicméně	nicméně	k8xC	nicméně
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
takové	takový	k3xDgFnPc4	takový
popularity	popularita	k1gFnPc4	popularita
jako	jako	k8xS	jako
osudy	osud	k1gInPc4	osud
lvice	lvice	k1gFnSc2	lvice
Elsy	Elsa	k1gFnSc2	Elsa
ve	v	k7c6	v
Volání	volání	k1gNnSc6	volání
divočiny	divočina	k1gFnSc2	divočina
(	(	kIx(	(
<g/>
Born	Born	k1gInSc1	Born
Free	Fre	k1gInSc2	Fre
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Útoky	útok	k1gInPc1	útok
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
===	===	k?	===
</s>
</p>
<p>
<s>
Levharti	levhart	k1gMnPc1	levhart
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
lvy	lev	k1gMnPc7	lev
a	a	k8xC	a
tygry	tygr	k1gMnPc4	tygr
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
zástupcům	zástupce	k1gMnPc3	zástupce
velkých	velký	k2eAgFnPc2d1	velká
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
pravidelně	pravidelně	k6eAd1	pravidelně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
lidožraví	lidožravý	k2eAgMnPc1d1	lidožravý
jedinci	jedinec	k1gMnPc1	jedinec
(	(	kIx(	(
<g/>
u	u	k7c2	u
jaguárů	jaguár	k1gMnPc2	jaguár
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vzácnost	vzácnost	k1gFnSc4	vzácnost
<g/>
,	,	kIx,	,
u	u	k7c2	u
irbisů	irbis	k1gInPc2	irbis
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nedochází	docházet	k5eNaImIp3nS	docházet
nikdy	nikdy	k6eAd1	nikdy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
zvířat	zvíře	k1gNnPc2	zvíře
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc4d1	malé
procento	procento	k1gNnSc4	procento
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
již	již	k9	již
stovky	stovka	k1gFnPc1	stovka
případů	případ	k1gInPc2	případ
lidožravých	lidožravý	k2eAgMnPc2d1	lidožravý
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidožrouty	lidožrout	k1gMnPc7	lidožrout
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
napadli	napadnout	k5eAaPmAgMnP	napadnout
člověka	člověk	k1gMnSc4	člověk
ve	v	k7c6	v
stresu	stres	k1gInSc6	stres
či	či	k8xC	či
sebeobraně	sebeobrana	k1gFnSc6	sebeobrana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cílení	cílený	k2eAgMnPc1d1	cílený
vyhledávači	vyhledávač	k1gMnPc1	vyhledávač
lidí	člověk	k1gMnPc2	člověk
jako	jako	k8xC	jako
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Důvody	důvod	k1gInPc1	důvod
lidožroutství	lidožroutství	k1gNnSc1	lidožroutství
====	====	k?	====
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
z	z	k7c2	z
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
naši	náš	k3xOp1gMnPc1	náš
pravěcí	pravěký	k2eAgMnPc1d1	pravěký
předkové	předek	k1gMnPc1	předek
se	se	k3xPyFc4	se
stávali	stávat	k5eAaImAgMnP	stávat
relativně	relativně	k6eAd1	relativně
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
leopardího	leopardí	k2eAgInSc2d1	leopardí
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc4	tento
šelmy	šelma	k1gFnPc4	šelma
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
instinktivně	instinktivně	k6eAd1	instinktivně
daný	daný	k2eAgInSc1d1	daný
léty	léto	k1gNnPc7	léto
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
soužití	soužití	k1gNnSc2	soužití
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
respekt	respekt	k1gInSc4	respekt
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
se	se	k3xPyFc4	se
vyhnou	vyhnout	k5eAaPmIp3nP	vyhnout
anebo	anebo	k8xC	anebo
jeho	jeho	k3xOp3gFnSc4	jeho
přítomnost	přítomnost	k1gFnSc4	přítomnost
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
zvyknou	zvyknout	k5eAaPmIp3nP	zvyknout
na	na	k7c4	na
turisty	turist	k1gMnPc4	turist
na	na	k7c4	na
safari	safari	k1gNnSc4	safari
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
událostem	událost	k1gFnPc3	událost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
šelmu	šelma	k1gFnSc4	šelma
navedou	navést	k5eAaPmIp3nP	navést
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
lidožrouta	lidožrout	k1gMnSc2	lidožrout
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
zranění	zranění	k1gNnSc4	zranění
šelmy	šelma	k1gFnSc2	šelma
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
způsobené	způsobený	k2eAgFnPc1d1	způsobená
člověkem	člověk	k1gMnSc7	člověk
samým	samý	k3xTgMnSc7	samý
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jí	on	k3xPp3gFnSc3	on
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
lovit	lovit	k5eAaImF	lovit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
hladové	hladový	k2eAgNnSc4d1	hladové
zvíře	zvíře	k1gNnSc4	zvíře
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ulovit	ulovit	k5eAaPmF	ulovit
lidi	člověk	k1gMnPc4	člověk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
.	.	kIx.	.
</s>
<s>
Propagátorem	propagátor	k1gMnSc7	propagátor
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byl	být	k5eAaImAgInS	být
především	především	k6eAd1	především
britský	britský	k2eAgMnSc1d1	britský
lovec	lovec	k1gMnSc1	lovec
Jim	on	k3xPp3gMnPc3	on
Corbett	Corbett	k2eAgInSc4d1	Corbett
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
vycházel	vycházet	k5eAaImAgInS	vycházet
především	především	k6eAd1	především
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
tygřích	tygří	k2eAgMnPc2d1	tygří
lidožroutů	lidožrout	k1gMnPc2	lidožrout
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
staré	starý	k2eAgMnPc4d1	starý
jedince	jedinec	k1gMnPc4	jedinec
neschopné	schopný	k2eNgMnPc4d1	neschopný
zabít	zabít	k5eAaPmF	zabít
rychlé	rychlý	k2eAgNnSc4d1	rychlé
divoké	divoký	k2eAgNnSc4d1	divoké
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
může	moct	k5eAaImIp3nS	moct
levhart	levhart	k1gMnSc1	levhart
sežrat	sežrat	k5eAaPmF	sežrat
člověka	člověk	k1gMnSc2	člověk
víceméně	víceméně	k9	víceméně
omylem	omylem	k6eAd1	omylem
(	(	kIx(	(
<g/>
náhodné	náhodný	k2eAgNnSc1d1	náhodné
zabití	zabití	k1gNnSc1	zabití
<g/>
)	)	kIx)	)
či	či	k8xC	či
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
nepohřbenou	pohřbený	k2eNgFnSc4d1	nepohřbená
lidskou	lidský	k2eAgFnSc4d1	lidská
mrtvolu	mrtvola	k1gFnSc4	mrtvola
(	(	kIx(	(
<g/>
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
maso	maso	k1gNnSc4	maso
mu	on	k3xPp3gMnSc3	on
zachutná	zachutnat	k5eAaPmIp3nS	zachutnat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
kořist	kořist	k1gFnSc4	kořist
začne	začít	k5eAaPmIp3nS	začít
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
případ	případ	k1gInSc1	případ
lidožroutů	lidožrout	k1gMnPc2	lidožrout
z	z	k7c2	z
Panaru	Panar	k1gInSc2	Panar
(	(	kIx(	(
<g/>
cholera	cholera	k1gFnSc1	cholera
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rudraprayagu	Rudraprayaga	k1gFnSc4	Rudraprayaga
(	(	kIx(	(
<g/>
španělská	španělský	k2eAgFnSc1d1	španělská
chřipka	chřipka	k1gFnSc1	chřipka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lidožroutství	lidožroutství	k1gNnSc1	lidožroutství
z	z	k7c2	z
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nedostatek	nedostatek	k1gInSc1	nedostatek
jiné	jiný	k2eAgFnSc2d1	jiná
kořisti	kořist	k1gFnSc2	kořist
donutí	donutit	k5eAaPmIp3nP	donutit
levharta	levhart	k1gMnSc4	levhart
napadat	napadat	k5eAaImF	napadat
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
jen	jen	k9	jen
krůček	krůček	k1gInSc4	krůček
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidožravá	lidožravý	k2eAgFnSc1d1	lidožravá
samice	samice	k1gFnSc1	samice
naučí	naučit	k5eAaPmIp3nS	naučit
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
lovit	lovit	k5eAaImF	lovit
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nalákat	nalákat	k5eAaPmF	nalákat
levharty	levhart	k1gMnPc4	levhart
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
může	moct	k5eAaImIp3nS	moct
i	i	k8xC	i
nestandardní	standardní	k2eNgNnSc4d1	nestandardní
lidské	lidský	k2eAgNnSc4d1	lidské
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
potácení	potácení	k1gNnSc2	potácení
se	se	k3xPyFc4	se
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
kategorie	kategorie	k1gFnPc4	kategorie
spadají	spadat	k5eAaImIp3nP	spadat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
levharti	levhart	k1gMnPc1	levhart
napadli	napadnout	k5eAaPmAgMnP	napadnout
člověka	člověk	k1gMnSc4	člověk
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
pomsty	pomsta	k1gFnSc2	pomsta
za	za	k7c4	za
utrpěné	utrpěný	k2eAgNnSc4d1	utrpěné
příkoří	příkoří	k1gNnSc4	příkoří
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
takový	takový	k3xDgInSc4	takový
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
transportovaný	transportovaný	k2eAgMnSc1d1	transportovaný
levhart	levhart	k1gMnSc1	levhart
napadl	napadnout	k5eAaPmAgMnS	napadnout
skrz	skrz	k7c4	skrz
okénko	okénko	k1gNnSc4	okénko
vozu	vůz	k1gInSc2	vůz
strážce	strážce	k1gMnSc2	strážce
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
předtím	předtím	k6eAd1	předtím
snažil	snažit	k5eAaImAgMnS	snažit
vystrnadit	vystrnadit	k5eAaPmF	vystrnadit
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
špičatým	špičatý	k2eAgInSc7d1	špičatý
klackem	klacek	k1gInSc7	klacek
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
zalezl	zalézt	k5eAaPmAgMnS	zalézt
do	do	k7c2	do
kabiny	kabina	k1gFnSc2	kabina
nákladního	nákladní	k2eAgInSc2d1	nákladní
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
pootevřené	pootevřený	k2eAgNnSc4d1	pootevřené
okénko	okénko	k1gNnSc4	okénko
<g/>
,	,	kIx,	,
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
známý	známý	k2eAgMnSc1d1	známý
britský	britský	k2eAgMnSc1d1	britský
vědec	vědec	k1gMnSc1	vědec
Desmond	Desmond	k1gMnSc1	Desmond
Morris	Morris	k1gFnSc1	Morris
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Analýza	analýza	k1gFnSc1	analýza
levhartích	levhartí	k2eAgMnPc2d1	levhartí
lidožroutů	lidožrout	k1gMnPc2	lidožrout
====	====	k?	====
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
studií	studio	k1gNnPc2	studio
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vzorků	vzorek	k1gInPc2	vzorek
tří	tři	k4xCgMnPc2	tři
hlavních	hlavní	k2eAgMnPc2d1	hlavní
lidožroutů	lidožrout	k1gMnPc2	lidožrout
(	(	kIx(	(
<g/>
lvů	lev	k1gMnPc2	lev
<g/>
,	,	kIx,	,
levhartů	levhart	k1gMnPc2	levhart
a	a	k8xC	a
tygrů	tygr	k1gMnPc2	tygr
<g/>
)	)	kIx)	)
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
levhartů	levhart	k1gMnPc2	levhart
(	(	kIx(	(
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
požírají	požírat	k5eAaImIp3nP	požírat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zdraví	zdravý	k2eAgMnPc1d1	zdravý
jedinci	jedinec	k1gMnPc1	jedinec
v	v	k7c6	v
nejlepších	dobrý	k2eAgNnPc6d3	nejlepší
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
především	především	k9	především
od	od	k7c2	od
tygrů	tygr	k1gMnPc2	tygr
(	(	kIx(	(
<g/>
38	[number]	k4	38
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nicméně	nicméně	k8xC	nicméně
výraznou	výrazný	k2eAgFnSc4d1	výrazná
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
diskrepanci	diskrepance	k1gFnSc4	diskrepance
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Turnbull-Kemp	Turnbull-Kemp	k1gMnSc1	Turnbull-Kemp
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
The	The	k1gMnSc1	The
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
známých	známý	k2eAgInPc2d1	známý
152	[number]	k4	152
lidožroutů	lidožrout	k1gMnPc2	lidožrout
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
9	[number]	k4	9
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
už	už	k6eAd1	už
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
lidožrouta	lidožrout	k1gMnSc2	lidožrout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
velmi	velmi	k6eAd1	velmi
nebezpečným	bezpečný	k2eNgFnPc3d1	nebezpečná
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
polapitelným	polapitelný	k2eAgInSc7d1	polapitelný
zabijákem	zabiják	k1gInSc7	zabiják
<g/>
.	.	kIx.	.
</s>
<s>
Operuje	operovat	k5eAaImIp3nS	operovat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
být	být	k5eAaImF	být
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
,	,	kIx,	,
lstivý	lstivý	k2eAgInSc1d1	lstivý
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
vynalézavý	vynalézavý	k2eAgMnSc1d1	vynalézavý
(	(	kIx(	(
<g/>
otevíraní	otevíraný	k2eAgMnPc1d1	otevíraný
dveří	dveře	k1gFnPc2	dveře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
měnit	měnit	k5eAaImF	měnit
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
působit	působit	k5eAaImF	působit
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
návnady	návnada	k1gFnPc4	návnada
a	a	k8xC	a
pasti	past	k1gFnPc4	past
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ho	on	k3xPp3gMnSc4	on
většinou	většinou	k6eAd1	většinou
přestane	přestat	k5eAaPmIp3nS	přestat
zajímat	zajímat	k5eAaImF	zajímat
jiná	jiný	k2eAgFnSc1d1	jiná
kořist	kořist	k1gFnSc1	kořist
než	než	k8xS	než
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
indické	indický	k2eAgFnSc2d1	indická
přírody	příroda	k1gFnSc2	příroda
Jim	on	k3xPp3gMnPc3	on
Corbett	Corbett	k1gInSc1	Corbett
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
legendární	legendární	k2eAgMnSc1d1	legendární
lidožrout	lidožrout	k1gMnSc1	lidožrout
z	z	k7c2	z
Rudraprayagu	Rudraprayag	k1gInSc2	Rudraprayag
si	se	k3xPyFc3	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
případě	případ	k1gInSc6	případ
došel	dojít	k5eAaPmAgInS	dojít
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
natěsnáno	natěsnat	k5eAaPmNgNnS	natěsnat
40	[number]	k4	40
zcela	zcela	k6eAd1	zcela
bezbranných	bezbranný	k2eAgFnPc2d1	bezbranná
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
musel	muset	k5eAaImAgMnS	muset
buď	buď	k8xC	buď
přeskákat	přeskákat	k5eAaPmF	přeskákat
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gInPc4	jejich
hřbety	hřbet	k1gInPc4	hřbet
či	či	k8xC	či
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
doslova	doslova	k6eAd1	doslova
prodrat	prodrat	k5eAaPmF	prodrat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
jedinou	jediný	k2eAgFnSc4d1	jediná
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
z	z	k7c2	z
Panaru	Panar	k1gInSc2	Panar
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nezajímal	zajímat	k5eNaImAgMnS	zajímat
o	o	k7c4	o
návnadu	návnada	k1gFnSc4	návnada
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kozy	koza	k1gFnSc2	koza
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
Corbetta	Corbetto	k1gNnPc4	Corbetto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
chráněn	chránit	k5eAaImNgInS	chránit
trnitými	trnitý	k2eAgFnPc7d1	trnitá
větvemi	větev	k1gFnPc7	větev
<g/>
,	,	kIx,	,
číhal	číhat	k5eAaImAgMnS	číhat
na	na	k7c6	na
stromě	strom	k1gInSc6	strom
<g/>
.	.	kIx.	.
<g/>
Lidožravý	lidožravý	k2eAgMnSc1d1	lidožravý
levhart	levhart	k1gMnSc1	levhart
napadá	napadat	k5eAaBmIp3nS	napadat
většinou	většina	k1gFnSc7	většina
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
tygr	tygr	k1gMnSc1	tygr
může	moct	k5eAaImIp3nS	moct
snad	snad	k9	snad
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
rozeznat	rozeznat	k5eAaPmF	rozeznat
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečného	bezpečný	k2eNgNnSc2d1	nebezpečné
(	(	kIx(	(
<g/>
ozbrojeného	ozbrojený	k2eAgMnSc2d1	ozbrojený
<g/>
)	)	kIx)	)
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
vyhne	vyhnout	k5eAaPmIp3nS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
faktorů	faktor	k1gInPc2	faktor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
obrovský	obrovský	k2eAgInSc4d1	obrovský
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
lidožrouta	lidožrout	k1gMnSc4	lidožrout
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenaným	zaznamenaný	k2eAgMnSc7d1	zaznamenaný
rekordmanem	rekordman	k1gMnSc7	rekordman
byl	být	k5eAaImAgMnS	být
panarský	panarský	k2eAgMnSc1d1	panarský
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
distriktu	distrikt	k1gInSc6	distrikt
Almora	Almor	k1gMnSc2	Almor
zabil	zabít	k5eAaPmAgMnS	zabít
okolo	okolo	k7c2	okolo
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Levhartí	levhartí	k2eAgMnPc1d1	levhartí
lidožrouti	lidožrout	k1gMnPc1	lidožrout
</s>
<s>
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k9	ještě
některá	některý	k3yIgNnPc4	některý
další	další	k2eAgNnPc4d1	další
specifika	specifikon	k1gNnPc4	specifikon
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
případů	případ	k1gInPc2	případ
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Nepálu	Nepál	k1gInSc2	Nepál
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
masivní	masivní	k2eAgFnSc2d1	masivní
urbanizace	urbanizace	k1gFnSc2	urbanizace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
nejsou	být	k5eNaImIp3nP	být
útoky	útok	k1gInPc1	útok
tak	tak	k6eAd1	tak
časté	častý	k2eAgInPc1d1	častý
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
rovněž	rovněž	k9	rovněž
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
zvířecích	zvířecí	k2eAgInPc6d1	zvířecí
zabijácích	zabiják	k1gInPc6	zabiják
<g/>
,	,	kIx,	,
když	když	k8xS	když
například	například	k6eAd1	například
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
jeden	jeden	k4xCgMnSc1	jeden
levhart	levhart	k1gMnSc1	levhart
asi	asi	k9	asi
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
napadení	napadení	k1gNnSc1	napadení
<g />
.	.	kIx.	.
</s>
<s>
levharty	levhart	k1gMnPc4	levhart
především	především	k6eAd1	především
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
poměrně	poměrně	k6eAd1	poměrně
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
většinou	většina	k1gFnSc7	většina
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
klasické	klasický	k2eAgMnPc4d1	klasický
lidožrouty	lidožrout	k1gMnPc4	lidožrout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
o	o	k7c4	o
útoky	útok	k1gInPc4	útok
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
lidskými	lidský	k2eAgFnPc7d1	lidská
aktivitami	aktivita	k1gFnPc7	aktivita
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
si	se	k3xPyFc3	se
někdy	někdy	k6eAd1	někdy
zakládají	zakládat	k5eAaImIp3nP	zakládat
doupata	doupě	k1gNnPc1	doupě
v	v	k7c6	v
třtinových	třtinový	k2eAgFnPc6d1	třtinová
polích	pole	k1gFnPc6	pole
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nedopatřením	nedopatření	k1gNnSc7	nedopatření
vlezou	vlézt	k5eAaPmIp3nP	vlézt
do	do	k7c2	do
hustě	hustě	k6eAd1	hustě
obydlených	obydlený	k2eAgNnPc2d1	obydlené
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nedávným	dávný	k2eNgMnPc3d1	nedávný
a	a	k8xC	a
filmově	filmově	k6eAd1	filmově
částečně	částečně	k6eAd1	částečně
zachyceným	zachycený	k2eAgInSc7d1	zachycený
příkladem	příklad	k1gInSc7	příklad
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
levhartího	levhartí	k2eAgInSc2d1	levhartí
samce	samec	k1gInSc2	samec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Bengalúru	Bengalúr	k1gInSc6	Bengalúr
a	a	k8xC	a
následně	následně	k6eAd1	následně
zranil	zranit	k5eAaPmAgMnS	zranit
6	[number]	k4	6
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
chytit	chytit	k5eAaPmF	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
kupříkladu	kupříkladu	k6eAd1	kupříkladu
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
levharty	levhart	k1gMnPc7	levhart
lovit	lovit	k5eAaImF	lovit
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
mnohdy	mnohdy	k6eAd1	mnohdy
následkem	následkem	k7c2	následkem
těchto	tento	k3xDgInPc2	tento
útoků	útok	k1gInPc2	útok
vybíjení	vybíjení	k1gNnSc2	vybíjení
levhartů	levhart	k1gMnPc2	levhart
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
šelem	šelma	k1gFnPc2	šelma
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Podstatu	podstata	k1gFnSc4	podstata
lidožroutů	lidožrout	k1gMnPc2	lidožrout
dobře	dobře	k6eAd1	dobře
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
Jim	on	k3xPp3gMnPc3	on
Corbett	Corbett	k2eAgInSc4d1	Corbett
<g/>
,	,	kIx,	,
když	když	k8xS	když
zabil	zabít	k5eAaPmAgMnS	zabít
levharta	levhart	k1gMnSc4	levhart
z	z	k7c2	z
Rudraprayagu	Rudraprayag	k1gInSc2	Rudraprayag
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ležel	ležet	k5eAaImAgInS	ležet
zde	zde	k6eAd1	zde
jen	jen	k9	jen
starý	starý	k2eAgMnSc1d1	starý
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
ostatních	ostatní	k1gNnPc2	ostatní
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
šedivý	šedivý	k2eAgInSc4d1	šedivý
čenich	čenich	k1gInSc4	čenich
a	a	k8xC	a
chyběly	chybět	k5eAaImAgInP	chybět
mu	on	k3xPp3gMnSc3	on
hmatové	hmatový	k2eAgInPc1d1	hmatový
vousky	vousek	k1gInPc1	vousek
<g/>
;	;	kIx,	;
nejnenáviděnější	nenáviděný	k2eAgNnSc1d3	nenáviděný
a	a	k8xC	a
nejobávanější	obávaný	k2eAgNnSc1d3	nejobávanější
zvíře	zvíře	k1gNnSc1	zvíře
celé	celý	k2eAgFnSc2d1	celá
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
jediným	jediný	k2eAgInSc7d1	jediný
zločinem	zločin	k1gInSc7	zločin
–	–	k?	–
nikoli	nikoli	k9	nikoli
však	však	k9	však
proti	proti	k7c3	proti
zákonům	zákon	k1gInPc3	zákon
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
zákonům	zákon	k1gInPc3	zákon
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
prolil	prolít	k5eAaPmAgInS	prolít
lidskou	lidský	k2eAgFnSc4d1	lidská
krev	krev	k1gFnSc4	krev
<g/>
;	;	kIx,	;
ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gFnPc4	on
terorizoval	terorizovat	k5eAaImAgMnS	terorizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
hrozbami	hrozba	k1gFnPc7	hrozba
pro	pro	k7c4	pro
levharta	levhart	k1gMnSc4	levhart
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgNnSc4	prvý
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lov	lov	k1gInSc1	lov
kvůli	kvůli	k7c3	kvůli
trofejím	trofej	k1gFnPc3	trofej
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
kožešinám	kožešina	k1gFnPc3	kožešina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
hlavně	hlavně	k6eAd1	hlavně
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stále	stále	k6eAd1	stále
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lov	lov	k1gInSc1	lov
kvůli	kvůli	k7c3	kvůli
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
částmi	část	k1gFnPc7	část
levhartích	levhartí	k2eAgFnPc2d1	levhartí
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
orientální	orientální	k2eAgFnSc6d1	orientální
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
hrozbou	hrozba	k1gFnSc7	hrozba
je	být	k5eAaImIp3nS	být
fragmentace	fragmentace	k1gFnSc1	fragmentace
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přirozený	přirozený	k2eAgInSc1d1	přirozený
biotop	biotop	k1gInSc1	biotop
levhartů	levhart	k1gMnPc2	levhart
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
či	či	k8xC	či
obydlenou	obydlený	k2eAgFnSc4d1	obydlená
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tím	ten	k3xDgNnSc7	ten
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
či	či	k8xC	či
vytěsňují	vytěsňovat	k5eAaImIp3nP	vytěsňovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
potravu	potrava	k1gFnSc4	potrava
této	tento	k3xDgFnSc2	tento
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
levhart	levhart	k1gMnSc1	levhart
přizpůsobivější	přizpůsobivý	k2eAgMnSc1d2	přizpůsobivější
než	než	k8xS	než
například	například	k6eAd1	například
tygr	tygr	k1gMnSc1	tygr
či	či	k8xC	či
lev	lev	k1gMnSc1	lev
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
poměrně	poměrně	k6eAd1	poměrně
úspěšně	úspěšně	k6eAd1	úspěšně
žít	žít	k5eAaImF	žít
i	i	k9	i
v	v	k7c6	v
takto	takto	k6eAd1	takto
pozměněném	pozměněný	k2eAgInSc6d1	pozměněný
biotopu	biotop	k1gInSc6	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
krajině	krajina	k1gFnSc6	krajina
však	však	k9	však
následně	následně	k6eAd1	následně
zažehává	zažehávat	k5eAaImIp3nS	zažehávat
další	další	k2eAgInPc4d1	další
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
levharti	levhart	k1gMnPc1	levhart
pak	pak	k6eAd1	pak
začnou	začít	k5eAaPmIp3nP	začít
napadat	napadat	k5eAaPmF	napadat
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
samotné	samotný	k2eAgMnPc4d1	samotný
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ty	k3xPp2nSc3	ty
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
snaží	snažit	k5eAaImIp3nS	snažit
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
vystrnadit	vystrnadit	k5eAaPmF	vystrnadit
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
pro	pro	k7c4	pro
levharty	levhart	k1gMnPc4	levhart
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
konkurence	konkurence	k1gFnSc1	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
každoročně	každoročně	k6eAd1	každoročně
uloví	ulovit	k5eAaPmIp3nS	ulovit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
kvůli	kvůli	k7c3	kvůli
masu	maso	k1gNnSc3	maso
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Nigérie	Nigérie	k1gFnSc2	Nigérie
a	a	k8xC	a
Kamerunu	Kamerun	k1gInSc2	Kamerun
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
000	[number]	k4	000
živočichů	živočich	k1gMnPc2	živočich
neboli	neboli	k8xC	neboli
12	[number]	k4	12
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
masa	maso	k1gNnSc2	maso
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Útoky	útok	k1gInPc4	útok
levhartů	levhart	k1gMnPc2	levhart
na	na	k7c4	na
dobytek	dobytek	k1gInSc4	dobytek
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	on	k3xPp3gInPc4	on
lidé	člověk	k1gMnPc1	člověk
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
zabili	zabít	k5eAaPmAgMnP	zabít
levharti	levhart	k1gMnPc1	levhart
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
966	[number]	k4	966
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
tygři	tygr	k1gMnPc1	tygr
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
232	[number]	k4	232
<g/>
,	,	kIx,	,
irbisové	irbisový	k2eAgFnSc2d1	irbisový
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nenávist	nenávist	k1gFnSc1	nenávist
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
ještě	ještě	k6eAd1	ještě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
šelmy	šelma	k1gFnPc1	šelma
mnohdy	mnohdy	k6eAd1	mnohdy
využívají	využívat	k5eAaPmIp3nP	využívat
bezbrannosti	bezbrannost	k1gFnPc1	bezbrannost
domácích	domácí	k1gMnPc2	domácí
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
takzvaně	takzvaně	k6eAd1	takzvaně
do	do	k7c2	do
zásoby	zásoba	k1gFnSc2	zásoba
(	(	kIx(	(
<g/>
surplus	surplus	k1gInSc1	surplus
killing	killing	k1gInSc1	killing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
najednou	najednou	k6eAd1	najednou
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
zvířat	zvíře	k1gNnPc2	zvíře
než	než	k8xS	než
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Provincii	provincie	k1gFnSc6	provincie
Kapsko	Kapsko	k1gNnSc1	Kapsko
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
levhart	levhart	k1gMnSc1	levhart
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
incidentu	incident	k1gInSc6	incident
pozabíjel	pozabíjet	k5eAaPmAgMnS	pozabíjet
51	[number]	k4	51
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
jehňat	jehně	k1gNnPc2	jehně
<g/>
,,	,,	k?	,,
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
22	[number]	k4	22
ovcí	ovce	k1gFnPc2	ovce
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
odveta	odveta	k1gFnSc1	odveta
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
střílení	střílení	k1gNnSc2	střílení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
častěji	často	k6eAd2	často
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kladení	kladení	k1gNnSc1	kladení
otrávených	otrávený	k2eAgFnPc2d1	otrávená
návnad	návnada	k1gFnPc2	návnada
či	či	k8xC	či
pastí	past	k1gFnPc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
pronásledování	pronásledování	k1gNnSc1	pronásledování
jsou	být	k5eAaImIp3nP	být
levharti	levhart	k1gMnPc1	levhart
velmi	velmi	k6eAd1	velmi
zranitelní	zranitelný	k2eAgMnPc1d1	zranitelný
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělcům	zemědělec	k1gMnPc3	zemědělec
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
povolováno	povolován	k2eAgNnSc1d1	povolováno
zabíjet	zabíjet	k5eAaImF	zabíjet
levharty	levhart	k1gMnPc4	levhart
jako	jako	k8xC	jako
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
i	i	k9	i
retrospektivně	retrospektivně	k6eAd1	retrospektivně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
můžou	můžou	k?	můžou
šelmu	šelma	k1gFnSc4	šelma
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
získat	získat	k5eAaPmF	získat
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
stály	stát	k5eAaImAgFnP	stát
za	za	k7c7	za
pronásledováním	pronásledování	k1gNnSc7	pronásledování
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
iracionální	iracionální	k2eAgInPc1d1	iracionální
důvody	důvod	k1gInPc1	důvod
–	–	k?	–
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
byli	být	k5eAaImAgMnP	být
levharti	levhart	k1gMnPc1	levhart
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
společníky	společník	k1gMnPc4	společník
kouzelníků	kouzelník	k1gMnPc2	kouzelník
a	a	k8xC	a
původce	původce	k1gMnSc4	původce
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
po	po	k7c4	po
generace	generace	k1gFnPc4	generace
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
museli	muset	k5eAaImAgMnP	muset
čelit	čelit	k5eAaImF	čelit
vyhlazovací	vyhlazovací	k2eAgFnSc3d1	vyhlazovací
vládní	vládní	k2eAgFnSc3d1	vládní
kampani	kampaň	k1gFnSc3	kampaň
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
svůj	svůj	k3xOyFgInSc4	svůj
účel	účel	k1gInSc4	účel
naplnit	naplnit	k5eAaPmF	naplnit
a	a	k8xC	a
šelmu	šelma	k1gFnSc4	šelma
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
vyhubit	vyhubit	k5eAaPmF	vyhubit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lov	lov	k1gInSc1	lov
====	====	k?	====
</s>
</p>
<p>
<s>
Trofejní	trofejní	k2eAgInSc4d1	trofejní
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInSc4d1	obchodní
a	a	k8xC	a
rituální	rituální	k2eAgInSc4d1	rituální
lov	lov	k1gInSc4	lov
na	na	k7c4	na
levharty	levhart	k1gMnPc4	levhart
probíhal	probíhat	k5eAaImAgMnS	probíhat
již	již	k6eAd1	již
od	od	k7c2	od
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Organizací	organizace	k1gFnSc7	organizace
velkých	velký	k2eAgFnPc2d1	velká
loveckých	lovecký	k2eAgFnPc2d1	lovecká
akcí	akce	k1gFnPc2	akce
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
především	především	k6eAd1	především
na	na	k7c4	na
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
druhotně	druhotně	k6eAd1	druhotně
též	též	k9	též
na	na	k7c4	na
levharty	levhart	k1gMnPc4	levhart
<g/>
,	,	kIx,	,
prosluli	proslout	k5eAaPmAgMnP	proslout
indičtí	indický	k2eAgMnPc1d1	indický
maharádžové	maharádž	k1gMnPc1	maharádž
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
hony	hon	k1gInPc4	hon
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
často	často	k6eAd1	často
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
britskou	britský	k2eAgFnSc7d1	britská
účastí	účast	k1gFnSc7	účast
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
pořádány	pořádat	k5eAaImNgFnP	pořádat
až	až	k6eAd1	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Lovci	lovec	k1gMnPc1	lovec
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
civilizačního	civilizační	k2eAgInSc2d1	civilizační
okruhu	okruh	k1gInSc2	okruh
se	se	k3xPyFc4	se
ale	ale	k9	ale
angažovali	angažovat	k5eAaBmAgMnP	angažovat
především	především	k9	především
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Leopard	leopard	k1gMnSc1	leopard
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
veden	vést	k5eAaImNgMnS	vést
jako	jako	k8xC	jako
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
škůdce	škůdce	k1gMnSc1	škůdce
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
bylo	být	k5eAaImAgNnS	být
přistupováno	přistupován	k2eAgNnSc1d1	přistupováno
<g/>
.	.	kIx.	.
</s>
<s>
Neexistovaly	existovat	k5eNaImAgInP	existovat
žádné	žádný	k3yNgFnPc4	žádný
kvóty	kvóta	k1gFnPc4	kvóta
omezující	omezující	k2eAgMnSc1d1	omezující
jeho	jeho	k3xOp3gNnSc2	jeho
zabíjení	zabíjení	k1gNnSc2	zabíjení
a	a	k8xC	a
záleželo	záležet	k5eAaImAgNnS	záležet
jen	jen	k9	jen
na	na	k7c6	na
schopnostech	schopnost	k1gFnPc6	schopnost
lovců	lovec	k1gMnPc2	lovec
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
jich	on	k3xPp3gNnPc2	on
usmrtí	usmrtit	k5eAaPmIp3nP	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
Velkou	velký	k2eAgFnSc4d1	velká
pětku	pětka	k1gFnSc4	pětka
afrických	africký	k2eAgNnPc2d1	africké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
střílení	střílení	k1gNnSc1	střílení
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
ukázku	ukázka	k1gFnSc4	ukázka
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
,	,	kIx,	,
ba	ba	k9	ba
téměř	téměř	k6eAd1	téměř
za	za	k7c4	za
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
o	o	k7c4	o
sport	sport	k1gInSc4	sport
bohatých	bohatý	k2eAgFnPc2d1	bohatá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Účastnily	účastnit	k5eAaImAgFnP	účastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
či	či	k8xC	či
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
výrazněji	výrazně	k6eAd2	výrazně
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
"	"	kIx"	"
<g/>
sportu	sport	k1gInSc2	sport
<g/>
"	"	kIx"	"
postupně	postupně	k6eAd1	postupně
měnil	měnit	k5eAaImAgMnS	měnit
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
masakry	masakr	k1gInPc1	masakr
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
kritizovány	kritizován	k2eAgInPc1d1	kritizován
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k9	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
až	až	k9	až
1969	[number]	k4	1969
však	však	k9	však
činily	činit	k5eAaImAgInP	činit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lovu	lov	k1gInSc2	lov
ztráty	ztráta	k1gFnSc2	ztráta
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
populace	populace	k1gFnSc2	populace
levhartů	levhart	k1gMnPc2	levhart
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
mnohé	mnohý	k2eAgInPc4d1	mnohý
africké	africký	k2eAgInPc4d1	africký
státy	stát	k1gInPc4	stát
nastaveny	nastaven	k2eAgFnPc1d1	nastavena
kvóty	kvóta	k1gFnSc2	kvóta
na	na	k7c4	na
lov	lov	k1gInSc4	lov
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
500	[number]	k4	500
kusů	kus	k1gInPc2	kus
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
podnikání	podnikání	k1gNnSc2	podnikání
je	být	k5eAaImIp3nS	být
nemalý	malý	k2eNgMnSc1d1	nemalý
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mnoha	mnoho	k4c2	mnoho
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
levharta	levhart	k1gMnSc4	levhart
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byl	být	k5eAaImAgMnS	být
levhart	levhart	k1gMnSc1	levhart
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
za	za	k7c4	za
"	"	kIx"	"
<g/>
škůdce	škůdce	k1gMnPc4	škůdce
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
"	"	kIx"	"
a	a	k8xC	a
nemilosrdně	milosrdně	k6eNd1	milosrdně
pronásledován	pronásledován	k2eAgInSc1d1	pronásledován
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
bylo	být	k5eAaImAgNnS	být
zabíjeno	zabíjet	k5eAaImNgNnS	zabíjet
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
tisíce	tisíc	k4xCgInPc1	tisíc
levhartů	levhart	k1gMnPc2	levhart
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
šelma	šelma	k1gFnSc1	šelma
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
chráněná	chráněný	k2eAgFnSc1d1	chráněná
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zdecimovaná	zdecimovaný	k2eAgFnSc1d1	zdecimovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
největší	veliký	k2eAgFnSc7d3	veliký
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
levharty	levhart	k1gMnPc4	levhart
nelegální	legální	k2eNgInSc4d1	nelegální
lov	lov	k1gInSc4	lov
kvůli	kvůli	k7c3	kvůli
částem	část	k1gFnPc3	část
jejich	jejich	k3xOp3gNnPc2	jejich
těl	tělo	k1gNnPc2	tělo
následně	následně	k6eAd1	následně
využívaným	využívaný	k2eAgInSc7d1	využívaný
v	v	k7c6	v
orientální	orientální	k2eAgFnSc6d1	orientální
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
náhražka	náhražka	k1gFnSc1	náhražka
za	za	k7c4	za
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
podstatně	podstatně	k6eAd1	podstatně
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
<g/>
.	.	kIx.	.
</s>
<s>
Pytláci	pytlák	k1gMnPc1	pytlák
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
také	také	k9	také
o	o	k7c4	o
jiná	jiný	k2eAgNnPc4d1	jiné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
doslova	doslova	k6eAd1	doslova
zamořeny	zamořen	k2eAgFnPc1d1	zamořena
různými	různý	k2eAgNnPc7d1	různé
oky	oko	k1gNnPc7	oko
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
pastmi	past	k1gFnPc7	past
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
decimují	decimovat	k5eAaBmIp3nP	decimovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
defaunace	defaunace	k1gFnSc1	defaunace
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
vymizení	vymizení	k1gNnSc4	vymizení
populace	populace	k1gFnSc2	populace
středních	střední	k1gMnPc2	střední
a	a	k8xC	a
velkých	velký	k2eAgMnPc2d1	velký
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
daného	daný	k2eAgNnSc2d1	dané
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
až	až	k9	až
2015	[number]	k4	2015
podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
Wildlife	Wildlif	k1gInSc5	Wildlif
Protection	Protection	k1gInSc1	Protection
Society	societa	k1gFnSc2	societa
of	of	k?	of
India	indium	k1gNnSc2	indium
(	(	kIx(	(
<g/>
WPSI	WPSI	kA	WPSI
<g/>
)	)	kIx)	)
ročně	ročně	k6eAd1	ročně
upytlačeno	upytlačit	k5eAaPmNgNnS	upytlačit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
192	[number]	k4	192
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
sama	sám	k3xTgFnSc1	sám
WPSI	WPSI	kA	WPSI
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
zlomek	zlomek	k1gInSc4	zlomek
skutečného	skutečný	k2eAgNnSc2d1	skutečné
množství	množství	k1gNnSc2	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
kroky	krok	k1gInPc1	krok
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
levhartů	levhart	k1gMnPc2	levhart
započaly	započnout	k5eAaPmAgFnP	započnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
místo	místo	k7c2	místo
pušek	puška	k1gFnPc2	puška
raději	rád	k6eAd2	rád
používat	používat	k5eAaImF	používat
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
a	a	k8xC	a
kamery	kamera	k1gFnSc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
docházelo	docházet	k5eAaImAgNnS	docházet
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
obdivovaných	obdivovaný	k2eAgMnPc2d1	obdivovaný
lovců	lovec	k1gMnPc2	lovec
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stávali	stávat	k5eAaImAgMnP	stávat
veřejností	veřejnost	k1gFnSc7	veřejnost
odsuzovaní	odsuzovaný	k2eAgMnPc1d1	odsuzovaný
barbaři	barbar	k1gMnPc1	barbar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
velkého	velký	k2eAgInSc2d1	velký
lovu	lov	k1gInSc2	lov
na	na	k7c4	na
divokou	divoký	k2eAgFnSc4d1	divoká
zvěř	zvěř	k1gFnSc4	zvěř
účastnil	účastnit	k5eAaImAgMnS	účastnit
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
maharádžy	maharádža	k1gFnSc2	maharádža
z	z	k7c2	z
Džajpuru	Džajpur	k1gInSc2	Džajpur
manžel	manžel	k1gMnSc1	manžel
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
princ	princ	k1gMnSc1	princ
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
ironicky	ironicky	k6eAd1	ironicky
nově	nově	k6eAd1	nově
designovaný	designovaný	k2eAgMnSc1d1	designovaný
prezident	prezident	k1gMnSc1	prezident
britské	britský	k2eAgFnSc2d1	britská
sekce	sekce	k1gFnSc2	sekce
Světového	světový	k2eAgInSc2d1	světový
fondu	fond	k1gInSc2	fond
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
velké	velký	k2eAgNnSc1d1	velké
pobouření	pobouření	k1gNnSc1	pobouření
britské	britský	k2eAgFnSc2d1	britská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gMnSc4	on
donutilo	donutit	k5eAaPmAgNnS	donutit
pomocí	pomocí	k7c2	pomocí
malého	malý	k2eAgInSc2d1	malý
triku	trik	k1gInSc2	trik
(	(	kIx(	(
<g/>
údajný	údajný	k2eAgInSc1d1	údajný
abses	abses	k1gInSc1	abses
na	na	k7c6	na
ukazováčku	ukazováček	k1gInSc6	ukazováček
<g/>
)	)	kIx)	)
z	z	k7c2	z
akce	akce	k1gFnSc2	akce
vycouvat	vycouvat	k5eAaPmF	vycouvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vydala	vydat	k5eAaPmAgFnS	vydat
indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
Wildlife	Wildlif	k1gInSc5	Wildlif
Protection	Protection	k1gInSc1	Protection
Act	Act	k1gMnSc3	Act
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
stále	stále	k6eAd1	stále
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgInSc1	který
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
lov	lov	k1gInSc4	lov
levhartů	levhart	k1gMnPc2	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Případná	případný	k2eAgNnPc1d1	případné
problémová	problémový	k2eAgNnPc1d1	problémové
zvířata	zvíře	k1gNnPc1	zvíře
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
a	a	k8xC	a
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
veškerý	veškerý	k3xTgInSc1	veškerý
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
levhartími	levhartí	k2eAgFnPc7d1	levhartí
trofejemi	trofej	k1gFnPc7	trofej
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
rovněž	rovněž	k9	rovněž
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
lovecké	lovecký	k2eAgFnSc2d1	lovecká
lobby	lobby	k1gFnSc2	lobby
opět	opět	k6eAd1	opět
obnoven	obnovit	k5eAaPmNgInS	obnovit
pro	pro	k7c4	pro
nekomerční	komerční	k2eNgNnSc4d1	nekomerční
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
<g/>
Levhart	levhart	k1gMnSc1	levhart
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
veden	veden	k2eAgInSc1d1	veden
v	v	k7c6	v
CITES	CITES	kA	CITES
Appendix	Appendix	k1gInSc4	Appendix
I	i	k8xC	i
jako	jako	k9	jako
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
obchod	obchod	k1gInSc1	obchod
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
2560	[number]	k4	2560
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
11	[number]	k4	11
zemí	zem	k1gFnPc2	zem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
africké	africký	k2eAgFnPc1d1	africká
země	zem	k1gFnPc1	zem
však	však	k9	však
postupně	postupně	k6eAd1	postupně
zavádějí	zavádět	k5eAaImIp3nP	zavádět
moratorium	moratorium	k1gNnSc4	moratorium
na	na	k7c4	na
komerční	komerční	k2eAgInSc4d1	komerční
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
levhartí	levhartí	k2eAgFnSc2d1	levhartí
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
preferovat	preferovat	k5eAaImF	preferovat
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
větší	veliký	k2eAgInPc4d2	veliký
zisky	zisk	k1gInPc4	zisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
asijských	asijský	k2eAgFnPc6d1	asijská
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
levhart	levhart	k1gMnSc1	levhart
většinou	většinou	k6eAd1	většinou
chráněn	chráněn	k2eAgMnSc1d1	chráněn
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
různých	různý	k2eAgFnPc2d1	různá
rezervací	rezervace	k1gFnPc2	rezervace
a	a	k8xC	a
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
však	však	k9	však
často	často	k6eAd1	často
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
udržely	udržet	k5eAaPmAgFnP	udržet
genetickou	genetický	k2eAgFnSc4d1	genetická
diverzitu	diverzita	k1gFnSc4	diverzita
populace	populace	k1gFnSc2	populace
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
<g/>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
organizacemi	organizace	k1gFnPc7	organizace
s	s	k7c7	s
globální	globální	k2eAgFnSc7d1	globální
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
projekty	projekt	k1gInPc4	projekt
zabývající	zabývající	k2eAgInPc4d1	zabývající
se	se	k3xPyFc4	se
monitoringem	monitoring	k1gInSc7	monitoring
a	a	k8xC	a
ochranou	ochrana	k1gFnSc7	ochrana
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
včetně	včetně	k7c2	včetně
levharta	levhart	k1gMnSc2	levhart
jsou	být	k5eAaImIp3nP	být
IUCN	IUCN	kA	IUCN
<g/>
/	/	kIx~	/
<g/>
SSC	SSC	kA	SSC
Cat	Cat	k1gMnSc1	Cat
Specialist	Specialist	k1gMnSc1	Specialist
Group	Group	k1gMnSc1	Group
a	a	k8xC	a
Panthera	Panthera	k1gFnSc1	Panthera
Corporation	Corporation	k1gInSc1	Corporation
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
levharta	levhart	k1gMnSc4	levhart
jako	jako	k8xC	jako
živočicha	živočich	k1gMnSc4	živočich
zranitelného	zranitelný	k2eAgInSc2d1	zranitelný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
6	[number]	k4	6
z	z	k7c2	z
9	[number]	k4	9
poddruhů	poddruh	k1gInPc2	poddruh
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
buď	buď	k8xC	buď
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
nebo	nebo	k8xC	nebo
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
není	být	k5eNaImIp3nS	být
ještě	ještě	k9	ještě
vyhodnocen	vyhodnocen	k2eAgInSc1d1	vyhodnocen
status	status	k1gInSc1	status
levhartů	levhart	k1gMnPc2	levhart
indočínského	indočínský	k2eAgMnSc2d1	indočínský
a	a	k8xC	a
čínského	čínský	k2eAgMnSc2d1	čínský
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
počty	počet	k1gInPc1	počet
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
provedl	provést	k5eAaPmAgMnS	provést
IUCN	IUCN	kA	IUCN
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
reklasifikaci	reklasifikace	k1gFnSc4	reklasifikace
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
totéž	týž	k3xTgNnSc1	týž
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
subspecií	subspecie	k1gFnPc2	subspecie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
projekty	projekt	k1gInPc1	projekt
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
a	a	k8xC	a
reintrodukci	reintrodukce	k1gFnSc4	reintrodukce
levhartů	levhart	k1gMnPc2	levhart
perských	perský	k2eAgInPc2d1	perský
<g/>
,	,	kIx,	,
mandžuských	mandžuský	k2eAgInPc2d1	mandžuský
a	a	k8xC	a
arabských	arabský	k2eAgInPc2d1	arabský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAILEY	BAILEY	kA	BAILEY
<g/>
,	,	kIx,	,
Theodore	Theodor	k1gMnSc5	Theodor
N.	N.	kA	N.
The	The	k1gMnSc1	The
African	African	k1gMnSc1	African
Leopard	leopard	k1gMnSc1	leopard
<g/>
:	:	kIx,	:
Ecology	Ecolog	k1gInPc1	Ecolog
and	and	k?	and
Behavior	Behavior	k1gInSc1	Behavior
of	of	k?	of
a	a	k8xC	a
Solitary	Solitara	k1gFnSc2	Solitara
Felid	Felida	k1gFnPc2	Felida
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7872	[number]	k4	7872
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EDGAONKAR	EDGAONKAR	kA	EDGAONKAR
<g/>
,	,	kIx,	,
Advait	Advait	k2eAgInSc1d1	Advait
<g/>
.	.	kIx.	.
</s>
<s>
Ecology	Ecologa	k1gFnPc1	Ecologa
of	of	k?	of
the	the	k?	the
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
<g/>
)	)	kIx)	)
in	in	k?	in
Bori	Bor	k1gMnSc5	Bor
Wildlife	Wildlif	k1gMnSc5	Wildlif
Sanctuary	Sanctuar	k1gMnPc4	Sanctuar
and	and	k?	and
Satpura	Satpura	k1gFnSc1	Satpura
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
India	indium	k1gNnPc1	indium
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Florida	Florida	k1gFnSc1	Florida
(	(	kIx(	(
<g/>
PhD	PhD	k1gFnSc1	PhD
Dissertation	Dissertation	k1gInSc1	Dissertation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GUGGISBERG	GUGGISBERG	kA	GUGGISBERG
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
A.	A.	kA	A.
W.	W.	kA	W.
Wild	Wild	k1gInSc1	Wild
Cats	Cats	k1gInSc1	Cats
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Taplinger	Taplinger	k1gInSc1	Taplinger
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HAMILTON	HAMILTON	kA	HAMILTON
<g/>
,	,	kIx,	,
P.	P.	kA	P.
H.	H.	kA	H.
The	The	k1gMnSc1	The
Leopard	leopard	k1gMnSc1	leopard
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
and	and	k?	and
Cheetah	Cheetah	k1gInSc1	Cheetah
Acinonyx	Acinonyx	k1gInSc1	Acinonyx
jubatus	jubatus	k1gInSc4	jubatus
in	in	k?	in
Kenya	Keny	k1gInSc2	Keny
<g/>
:	:	kIx,	:
Ecology	Ecologa	k1gFnPc1	Ecologa
<g/>
,	,	kIx,	,
Status	status	k1gInSc1	status
<g/>
,	,	kIx,	,
Conservation	Conservation	k1gInSc1	Conservation
Management	management	k1gInSc1	management
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HANCOCK	HANCOCK	kA	HANCOCK
<g/>
,	,	kIx,	,
Dale	Dale	k1gNnSc2	Dale
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Time	Time	k1gNnSc1	Time
with	witha	k1gFnPc2	witha
Leopards	Leopardsa	k1gFnPc2	Leopardsa
<g/>
.	.	kIx.	.
</s>
<s>
Schrewsbury	Schrewsbura	k1gFnPc1	Schrewsbura
<g/>
:	:	kIx,	:
Swan-Hill	Swan-Hill	k1gInSc1	Swan-Hill
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1840371943	[number]	k4	1840371943
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HENSCHEL	HENSCHEL	kA	HENSCHEL
<g/>
,	,	kIx,	,
Philipp	Philipp	k1gInSc1	Philipp
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
conservation	conservation	k1gInSc1	conservation
biology	biolog	k1gMnPc4	biolog
of	of	k?	of
the	the	k?	the
leopard	leopard	k1gMnSc1	leopard
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
in	in	k?	in
Gabon	Gabon	k1gInSc1	Gabon
<g/>
:	:	kIx,	:
Status	status	k1gInSc1	status
<g/>
,	,	kIx,	,
threats	threats	k1gInSc1	threats
and	and	k?	and
strategies	strategies	k1gInSc1	strategies
for	forum	k1gNnPc2	forum
conservation	conservation	k1gInSc1	conservation
<g/>
.	.	kIx.	.
</s>
<s>
Göttingen	Göttingen	k1gNnSc1	Göttingen
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
97	[number]	k4	97
s.	s.	k?	s.
Dissertation	Dissertation	k1gInSc1	Dissertation
zur	zur	k?	zur
Erlangung	Erlangung	k1gInSc1	Erlangung
des	des	k1gNnSc1	des
Doktorgrades	Doktorgrades	k1gInSc1	Doktorgrades
<g/>
.	.	kIx.	.
</s>
<s>
Mathematisch-Naturwissenschaftlichen	Mathematisch-Naturwissenschaftlichen	k2eAgInSc1d1	Mathematisch-Naturwissenschaftlichen
Fakultäten	Fakultäten	k2eAgInSc1d1	Fakultäten
der	drát	k5eAaImRp2nS	drát
Georg-August-Universität	Georg-August-Universität	k1gInSc1	Georg-August-Universität
zu	zu	k?	zu
Göttingen	Göttingen	k1gInSc1	Göttingen
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Prof.	prof.	kA	prof.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Michael	Michael	k1gMnSc1	Michael
Mühlenberg	Mühlenberg	k1gMnSc1	Mühlenberg
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HEPTNER	HEPTNER	kA	HEPTNER
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
G.	G.	kA	G.
<g/>
;	;	kIx,	;
SLUDSKII	SLUDSKII	kA	SLUDSKII
<g/>
,	,	kIx,	,
A.	A.	kA	A.
A.	A.	kA	A.
Mammals	Mammalsa	k1gFnPc2	Mammalsa
of	of	k?	of
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
part	part	k1gInSc4	part
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Carnivora	Carnivora	k1gFnSc1	Carnivora
(	(	kIx(	(
<g/>
Hyaenas	Hyaenas	k1gInSc1	Hyaenas
and	and	k?	and
Cats	Cats	k1gInSc1	Cats
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnPc2	D.C.
<g/>
:	:	kIx,	:
Smithonian	Smithoniana	k1gFnPc2	Smithoniana
Institution	Institution	k1gInSc1	Institution
Libraries	Libraries	k1gMnSc1	Libraries
and	and	k?	and
National	National	k1gMnSc1	National
Science	Science	k1gFnSc2	Science
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MAZÁK	mazák	k1gMnSc1	mazák
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
kočky	kočka	k1gFnPc1	kočka
a	a	k8xC	a
gepardi	gepard	k1gMnPc1	gepard
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MORRIS	MORRIS	kA	MORRIS
<g/>
,	,	kIx,	,
Desmond	Desmond	k1gInSc1	Desmond
<g/>
.	.	kIx.	.
</s>
<s>
Leopard	leopard	k1gMnSc1	leopard
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Reaktion	Reaktion	k1gInSc1	Reaktion
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1780232799	[number]	k4	1780232799
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MYERS	MYERS	kA	MYERS
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Leopard	leopard	k1gMnSc1	leopard
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
in	in	k?	in
Africa	Africa	k1gFnSc1	Africa
<g/>
.	.	kIx.	.
</s>
<s>
Morges	Morges	k1gInSc1	Morges
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
IUCN	IUCN	kA	IUCN
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SUNQUIST	SUNQUIST	kA	SUNQUIST
<g/>
,	,	kIx,	,
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
;	;	kIx,	;
SUNQUIST	SUNQUIST	kA	SUNQUIST
<g/>
,	,	kIx,	,
Fiona	Fiono	k1gNnSc2	Fiono
<g/>
.	.	kIx.	.
</s>
<s>
Wild	Wild	k6eAd1	Wild
Cats	Cats	k1gInSc1	Cats
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TURNBULL-KEMP	TURNBULL-KEMP	k?	TURNBULL-KEMP
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Leopard	leopard	k1gMnSc1	leopard
<g/>
.	.	kIx.	.
</s>
<s>
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
<g/>
:	:	kIx,	:
Howard	Howard	k1gInSc1	Howard
Timmons	Timmonsa	k1gFnPc2	Timmonsa
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VAŠÁK	Vašák	k1gMnSc1	Vašák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
chovu	chov	k1gInSc2	chov
levharta	levhart	k1gMnSc2	levhart
skvrnitého	skvrnitý	k2eAgMnSc2d1	skvrnitý
v	v	k7c4	v
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
102	[number]	k4	102
s.	s.	k?	s.
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Agronomická	agronomický	k2eAgFnSc1d1	Agronomická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Laštůvka	laštůvka	k1gFnSc1	laštůvka
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Filmy	film	k1gInPc4	film
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Arabian	Arabian	k1gMnSc1	Arabian
Leopard	leopard	k1gMnSc1	leopard
of	of	k?	of
Oman	oman	k1gInSc1	oman
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Levhart	levhart	k1gMnSc1	levhart
arabský	arabský	k2eAgInSc1d1	arabský
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
Beauty	Beaut	k1gInPc1	Beaut
and	and	k?	and
the	the	k?	the
Beasts	Beasts	k1gInSc1	Beasts
<g/>
:	:	kIx,	:
Leopard	leopard	k1gMnSc1	leopard
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Story	story	k1gFnSc7	story
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
netvor	netvor	k1gMnSc1	netvor
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
Levhartovi	levhart	k1gMnSc6	levhart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
Eye	Eye	k?	Eye
of	of	k?	of
the	the	k?	the
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Očima	oko	k1gNnPc7	oko
levharta	levhart	k1gMnSc2	levhart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
In	In	k?	In
Search	Search	k1gInSc1	Search
of	of	k?	of
a	a	k8xC	a
Legend	legenda	k1gFnPc2	legenda
–	–	k?	–
Black	Black	k1gMnSc1	Black
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hledání	hledání	k1gNnSc1	hledání
legendy	legenda	k1gFnSc2	legenda
<g/>
:	:	kIx,	:
Černý	Černý	k1gMnSc1	Černý
leopard	leopard	k1gMnSc1	leopard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
JAR	jar	k1gFnSc1	jar
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
The	The	k?	The
Invisible	Invisible	k1gMnSc1	Invisible
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
levhart	levhart	k1gMnSc1	levhart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
Leopards	Leopards	k1gInSc1	Leopards
of	of	k?	of
Dead	Dead	k1gInSc1	Dead
Tree	Tree	k1gNnSc1	Tree
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
IMDb	IMDb	k1gMnSc1	IMDb
</s>
</p>
<p>
<s>
Leopard-	Leopard-	k?	Leopard-
A	a	k9	a
Darkness	Darkness	k1gInSc4	Darkness
in	in	k?	in
the	the	k?	the
Grass	Grass	k1gInSc1	Grass
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
IMDb	IMDb	k1gMnSc1	IMDb
</s>
</p>
<p>
<s>
The	The	k?	The
Man-Eating	Man-Eating	k1gInSc1	Man-Eating
Leopard	leopard	k1gMnSc1	leopard
of	of	k?	of
Rudraprayag	Rudraprayag	k1gMnSc1	Rudraprayag
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
Stalking	Stalking	k1gInSc1	Stalking
Leopards	Leopards	k1gInSc1	Leopards
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
:	:	kIx,	:
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
IMDb	IMDb	k1gMnSc1	IMDb
</s>
</p>
<p>
<s>
The	The	k?	The
Unlikely	Unlikela	k1gFnSc2	Unlikela
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Neobyčejný	obyčejný	k2eNgMnSc1d1	neobyčejný
levhart	levhart	k1gMnSc1	levhart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IUCN	IUCN	kA	IUCN
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profiles	Profilesa	k1gFnPc2	Profilesa
for	forum	k1gNnPc2	forum
Leopard	leopard	k1gMnSc1	leopard
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
<g/>
)	)	kIx)	)
Range	Range	k1gNnSc1	Range
Countries	Countriesa	k1gFnPc2	Countriesa
<g/>
.	.	kIx.	.
</s>
<s>
Supplemental	Supplemental	k1gMnSc1	Supplemental
Document	Document	k1gInSc4	Document
1	[number]	k4	1
to	ten	k3xDgNnSc4	ten
Jacobson	Jacobson	k1gNnSc1	Jacobson
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
