<s>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
fylogenetických	fylogenetický	k2eAgInPc2d1	fylogenetický
vývodů	vývod	k1gInPc2	vývod
je	být	k5eAaImIp3nS	být
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
příbuzným	příbuzný	k1gMnSc7	příbuzný
levharta	levhart	k1gMnSc2	levhart
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následován	následován	k2eAgInSc1d1	následován
jaguárem	jaguár	k1gMnSc7	jaguár
(	(	kIx(	(
<g/>
Panthera	Panther	k1gMnSc2	Panther
onca	oncus	k1gMnSc2	oncus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
