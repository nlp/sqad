<s>
Koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
hydrologický	hydrologický	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stálý	stálý	k2eAgInSc1d1
oběh	oběh	k1gInSc1
povrchové	povrchový	k2eAgFnSc2d1
a	a	k8xC
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
doprovázený	doprovázený	k2eAgInSc1d1
změnami	změna	k1gFnPc7
skupenství	skupenství	k1gNnSc2
<g/>
.	.	kIx.
</s>