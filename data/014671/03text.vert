<s>
Koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
</s>
<s>
Schéma	schéma	k1gNnSc1
koloběhu	koloběh	k1gInSc2
vody	voda	k1gFnSc2
</s>
<s>
Koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
hydrologický	hydrologický	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stálý	stálý	k2eAgInSc1d1
oběh	oběh	k1gInSc1
povrchové	povrchový	k2eAgFnSc2d1
a	a	k8xC
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
doprovázený	doprovázený	k2eAgInSc1d1
změnami	změna	k1gFnPc7
skupenství	skupenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
K	k	k7c3
oběhu	oběh	k1gInSc3
dochází	docházet	k5eAaImIp3nS
účinkem	účinek	k1gInSc7
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
zemské	zemský	k2eAgFnSc2d1
gravitace	gravitace	k1gFnSc2
a	a	k8xC
rotace	rotace	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	Voda	k1gMnSc1
se	se	k3xPyFc4
vypařuje	vypařovat	k5eAaImIp3nS
z	z	k7c2
oceánů	oceán	k1gInPc2
<g/>
,	,	kIx,
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
a	a	k8xC
nádrží	nádrž	k1gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
(	(	kIx(
<g/>
výpar	výpar	k1gInSc1
<g/>
,	,	kIx,
evaporace	evaporace	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
transpirace	transpirace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dohromady	dohromady	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pojem	pojem	k1gInSc1
evapotranspirace	evapotranspirace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgInPc1d1
páry	pár	k1gInPc1
a	a	k8xC
drobounké	drobounký	k2eAgFnPc1d1
kapičky	kapička	k1gFnPc1
vody	voda	k1gFnSc2
v	v	k7c6
oblacích	oblak	k1gInPc6
se	se	k3xPyFc4
pak	pak	k6eAd1
v	v	k7c6
ovzduší	ovzduší	k1gNnSc6
pohybem	pohyb	k1gInSc7
vzduchových	vzduchový	k2eAgFnPc2d1
mas	masa	k1gFnPc2
způsobených	způsobený	k2eAgFnPc2d1
nestejným	stejný	k2eNgNnSc7d1
zahříváním	zahřívání	k1gNnSc7
vzduchu	vzduch	k1gInSc2
nad	nad	k7c7
pevninou	pevnina	k1gFnSc7
a	a	k8xC
oceány	oceán	k1gInPc7
i	i	k8xC
zemskou	zemský	k2eAgFnSc7d1
rotací	rotace	k1gFnSc7
neustále	neustále	k6eAd1
přemisťují	přemisťovat	k5eAaImIp3nP
(	(	kIx(
<g/>
cirkulace	cirkulace	k1gFnSc1
atmosféry	atmosféra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
kondenzaci	kondenzace	k1gFnSc6
páry	pára	k1gFnSc2
z	z	k7c2
ovzduší	ovzduší	k1gNnSc2
dopadá	dopadat	k5eAaImIp3nS
voda	voda	k1gFnSc1
ve	v	k7c6
formě	forma	k1gFnSc6
srážek	srážka	k1gFnPc2
na	na	k7c4
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
formě	forma	k1gFnSc6
deště	dešť	k1gInSc2
a	a	k8xC
sněhu	sníh	k1gInSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
hydrometeory	hydrometeor	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
část	část	k1gFnSc1
vody	voda	k1gFnSc2
hromadí	hromadit	k5eAaImIp3nS
a	a	k8xC
odtéká	odtékat	k5eAaImIp3nS
jako	jako	k9
povrchová	povrchový	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
vypařuje	vypařovat	k5eAaImIp3nS
se	se	k3xPyFc4
zpět	zpět	k6eAd1
do	do	k7c2
ovzduší	ovzduší	k1gNnSc2
nebo	nebo	k8xC
se	se	k3xPyFc4
vsakuje	vsakovat	k5eAaImIp3nS
pod	pod	k7c4
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
a	a	k8xC
doplňuje	doplňovat	k5eAaImIp3nS
zásoby	zásoba	k1gFnPc4
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
infiltrace	infiltrace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podzemní	podzemní	k2eAgFnSc1d1
voda	voda	k1gFnSc1
po	po	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
znovu	znovu	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
ve	v	k7c6
formě	forma	k1gFnSc6
pramenů	pramen	k1gInPc2
nebo	nebo	k8xC
dotuje	dotovat	k5eAaBmIp3nS
vodní	vodní	k2eAgInPc1d1
toky	tok	k1gInPc1
(	(	kIx(
<g/>
drenáž	drenáž	k1gFnSc1
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bilanční	bilanční	k2eAgInPc1d1
prvky	prvek	k1gInPc1
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
výparu	výpar	k1gInSc2
a	a	k8xC
srážek	srážka	k1gFnPc2
v	v	k7c6
mm	mm	kA
<g/>
/	/	kIx~
<g/>
den	den	k1gInSc1
</s>
<s>
Uvedené	uvedený	k2eAgInPc1d1
procesy	proces	k1gInPc1
(	(	kIx(
<g/>
výpar	výpar	k1gInSc1
<g/>
,	,	kIx,
odtok	odtok	k1gInSc1
a	a	k8xC
infiltrace	infiltrace	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
kvantitativně	kvantitativně	k6eAd1
vyjadřují	vyjadřovat	k5eAaImIp3nP
jako	jako	k9
tzv.	tzv.	kA
bilanční	bilanční	k2eAgInPc4d1
prvky	prvek	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
hydrologické	hydrologický	k2eAgFnSc2d1
bilance	bilance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hydrologická	hydrologický	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
je	být	k5eAaImIp3nS
porovnání	porovnání	k1gNnSc4
příjmových	příjmový	k2eAgFnPc2d1
a	a	k8xC
ztrátových	ztrátový	k2eAgFnPc2d1
složek	složka	k1gFnPc2
(	(	kIx(
<g/>
bilančních	bilanční	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
)	)	kIx)
hydrologického	hydrologický	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
určit	určit	k5eAaPmF
velikost	velikost	k1gFnSc4
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
vody	voda	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
možnosti	možnost	k1gFnPc1
jejich	jejich	k3xOp3gNnSc2
využití	využití	k1gNnSc2
v	v	k7c6
určitém	určitý	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
vody	voda	k1gFnSc2
</s>
<s>
Voda	voda	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
kapalném	kapalný	k2eAgInSc6d1
a	a	k8xC
plynném	plynný	k2eAgInSc6d1
stavu	stav	k1gInSc6
v	v	k7c6
pozemském	pozemský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
prakticky	prakticky	k6eAd1
neustále	neustále	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
a	a	k8xC
mění	měnit	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
skupenství	skupenství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neustálý	neustálý	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
vody	voda	k1gFnSc2
probíhá	probíhat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
mořích	moře	k1gNnPc6
a	a	k8xC
oceánech	oceán	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jde	jít	k5eAaImIp3nS
zejména	zejména	k9
o	o	k7c4
mořské	mořský	k2eAgInPc4d1
proudy	proud	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pohyb	pohyb	k1gInSc4
vody	voda	k1gFnSc2
má	mít	k5eAaImIp3nS
<g/>
,	,	kIx,
kromě	kromě	k7c2
odlišného	odlišný	k2eAgNnSc2d1
zahřívání	zahřívání	k1gNnSc2
vody	voda	k1gFnSc2
a	a	k8xC
vzduchu	vzduch	k1gInSc2
nad	nad	k7c7
pevninou	pevnina	k1gFnSc7
a	a	k8xC
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
vliv	vliv	k1gInSc4
gravitační	gravitační	k2eAgNnSc4d1
působení	působení	k1gNnSc4
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
působením	působení	k1gNnSc7
gravitačních	gravitační	k2eAgFnPc2d1
sil	síla	k1gFnPc2
teče	téct	k5eAaImIp3nS
vždy	vždy	k6eAd1
dolů	dolů	k6eAd1
<g/>
,	,	kIx,
tedy	tedy	k8xC
stéká	stékat	k5eAaImIp3nS
z	z	k7c2
vyšších	vysoký	k2eAgNnPc2d2
míst	místo	k1gNnPc2
na	na	k7c6
zemském	zemský	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
do	do	k7c2
nižších	nízký	k2eAgNnPc2d2
míst	místo	k1gNnPc2
(	(	kIx(
<g/>
vodní	vodní	k2eAgFnSc1d1
toky	toka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohyb	pohyb	k1gInSc1
vody	voda	k1gFnSc2
v	v	k7c6
kapalném	kapalný	k2eAgInSc6d1
stavu	stav	k1gInSc6
dále	daleko	k6eAd2
také	také	k9
ovlivňuje	ovlivňovat	k5eAaImIp3nS
i	i	k9
rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
působí	působit	k5eAaImIp3nS
unášivá	unášivý	k2eAgFnSc1d1
Coriolisova	Coriolisův	k2eAgFnSc1d1
síla	síla	k1gFnSc1
a	a	k8xC
odstředivá	odstředivý	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pohyb	pohyb	k1gInSc4
vody	voda	k1gFnSc2
mají	mít	k5eAaImIp3nP
vliv	vliv	k1gInSc4
i	i	k8xC
slapové	slapový	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Slunce	slunce	k1gNnSc2
a	a	k8xC
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
způsobují	způsobovat	k5eAaImIp3nP
slapové	slapový	k2eAgInPc4d1
jevy	jev	k1gInPc4
zvané	zvaný	k2eAgInPc4d1
příliv	příliv	k1gInSc4
a	a	k8xC
odliv	odliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
zemského	zemský	k2eAgInSc2d1
pláště	plášť	k1gInSc2
nyní	nyní	k6eAd1
mizí	mizet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
400	#num#	k4
miliard	miliarda	k4xCgFnPc2
kg	kg	kA
vody	voda	k1gFnSc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ale	ale	k8xC
může	moct	k5eAaImIp3nS
to	ten	k3xDgNnSc1
být	být	k5eAaImF
i	i	k9
třikrát	třikrát	k6eAd1
více	hodně	k6eAd2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
než	než	k8xS
se	se	k3xPyFc4
odhadovalo	odhadovat	k5eAaImAgNnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
takže	takže	k8xS
se	se	k3xPyFc4
ani	ani	k8xC
nestačí	stačit	k5eNaBmIp3nS
dostávat	dostávat	k5eAaImF
tolik	tolik	k6eAd1
vody	voda	k1gFnSc2
zpět	zpět	k6eAd1
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
i	i	k9
sopečné	sopečný	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
mohou	moct	k5eAaImIp3nP
na	na	k7c4
povrch	povrch	k1gInSc4
přivádět	přivádět	k5eAaImF
více	hodně	k6eAd2
vody	voda	k1gFnPc1
než	než	k8xS
se	se	k3xPyFc4
dříve	dříve	k6eAd2
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
v	v	k7c6
zemském	zemský	k2eAgInSc6d1
plášti	plášť	k1gInSc6
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
možný	možný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dělení	dělení	k1gNnSc1
</s>
<s>
Ve	v	k7c6
velkém	velký	k2eAgInSc6d1
koloběhu	koloběh	k1gInSc6
vody	voda	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
přesunům	přesun	k1gInPc3
vody	voda	k1gFnSc2
mezi	mezi	k7c7
světovým	světový	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
a	a	k8xC
pevninou	pevnina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Malý	malý	k2eAgInSc1d1
koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
probíhá	probíhat	k5eAaImIp3nS
pouze	pouze	k6eAd1
nad	nad	k7c7
oceány	oceán	k1gInPc7
nebo	nebo	k8xC
pouze	pouze	k6eAd1
nad	nad	k7c7
bezodtokovými	bezodtokový	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
pevniny	pevnina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Počasí	počasí	k1gNnSc1
</s>
<s>
Koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
počasí	počasí	k1gNnSc1
respektive	respektive	k9
klima	klima	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
jako	jako	k8xC,k8xS
déšť	déšť	k1gInSc4
či	či	k8xC
oblaka	oblaka	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vodní	vodní	k2eAgFnSc1d1
pára	pára	k1gFnSc1
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgInSc4d3
skleníkový	skleníkový	k2eAgInSc4d1
plyn	plyn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://rsta.royalsocietypublishing.org/content/375/2094/20150393	http://rsta.royalsocietypublishing.org/content/375/2094/20150393	k4
-	-	kIx~
Global	globat	k5eAaImAgMnS
water	water	k1gMnSc1
cycle	cycle	k1gFnSc2
and	and	k?
the	the	k?
coevolution	coevolution	k1gInSc1
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
interior	interior	k1gMnSc1
and	and	k?
surface	surface	k1gFnSc1
environment	environment	k1gInSc1
<g/>
↑	↑	k?
https://www.nature.com/articles/s41586-018-0655-4?WT.feed_name=subjects_physical-sciences#ref-CR3	https://www.nature.com/articles/s41586-018-0655-4?WT.feed_name=subjects_physical-sciences#ref-CR3	k4
-	-	kIx~
Water	Water	k1gMnSc1
input	input	k1gMnSc1
into	into	k1gMnSc1
the	the	k?
Mariana	Mariana	k1gFnSc1
subduction	subduction	k1gInSc1
zone	zonat	k5eAaPmIp3nS
estimated	estimated	k1gInSc4
from	from	k6eAd1
ocean-bottom	ocean-bottom	k1gInSc1
seismic	seismice	k1gFnPc2
data	datum	k1gNnSc2
<g/>
↑	↑	k?
https://www.researchgate.net/publication/216830996_Subduction_factory_4_Depth-dependent_flux_of_H2O_from_subducting_slabs_worldwide	https://www.researchgate.net/publication/216830996_Subduction_factory_4_Depth-dependent_flux_of_H2O_from_subducting_slabs_worldwid	k1gInSc5
-	-	kIx~
Subduction	Subduction	k1gInSc1
factory	factor	k1gInPc1
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Depth-dependent	Depth-dependent	k1gMnSc1
flux	flux	k1gInSc1
of	of	k?
H2O	H2O	k1gFnSc2
from	froma	k1gFnPc2
subducting	subducting	k1gInSc1
slabs	slabs	k6eAd1
worldwide	worldwid	k1gInSc5
<g/>
↑	↑	k?
http://www.osel.cz/10220-zemske-nitro-polyka-ohromnou-spoustu-vody.html	http://www.osel.cz/10220-zemske-nitro-polyka-ohromnou-spoustu-vody.html	k1gInSc1
-	-	kIx~
Zemské	zemský	k2eAgNnSc1d1
nitro	nitro	k1gNnSc1
polyká	polykat	k5eAaImIp3nS
ohromnou	ohromný	k2eAgFnSc4d1
spoustu	spousta	k1gFnSc4
vody	voda	k1gFnSc2
<g/>
↑	↑	k?
https://phys.org/news/2019-07-explosive-eruptions-magma-wetter-thought.html	https://phys.org/news/2019-07-explosive-eruptions-magma-wetter-thought.html	k1gMnSc1
-	-	kIx~
Water	Water	k1gMnSc1
drives	drives	k1gMnSc1
explosive	explosivat	k5eAaPmIp3nS
eruptions	eruptions	k1gInSc1
<g/>
:	:	kIx,
Magma	magma	k1gNnSc1
is	is	k?
wetter	wetter	k1gMnSc1
than	than	k1gMnSc1
we	we	k?
thought	thought	k1gMnSc1
<g/>
↑	↑	k?
https://phys.org/news/2019-12-deep-earth-planetary-evolution.html	https://phys.org/news/2019-12-deep-earth-planetary-evolution.html	k1gMnSc1
-	-	kIx~
Would	Would	k1gMnSc1
a	a	k8xC
deep-Earth	deep-Earth	k1gMnSc1
water	water	k1gMnSc1
cycle	cycle	k6eAd1
change	change	k1gFnSc2
our	our	k?
understanding	understanding	k1gInSc1
of	of	k?
planetary	planetara	k1gFnSc2
evolution	evolution	k1gInSc1
<g/>
?	?	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
hydrografie	hydrografie	k1gFnSc1
</s>
<s>
hydrologie	hydrologie	k1gFnSc1
</s>
<s>
hydrometeor	hydrometeor	k1gInSc1
</s>
<s>
hydrometrie	hydrometrie	k1gFnSc1
</s>
<s>
hydrosféra	hydrosféra	k1gFnSc1
</s>
<s>
meteorologie	meteorologie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
koloběh	koloběh	k1gInSc1
vody	voda	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oběh	oběh	k1gInSc1
vody	voda	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
U.	U.	kA
<g/>
S.	S.	kA
Geological	Geological	k1gMnSc2
Survey	Survea	k1gMnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Českým	český	k2eAgInSc7d1
hydrometeorologickým	hydrometeorologický	k2eAgInSc7d1
ústavem	ústav	k1gInSc7
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4189218-5	4189218-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12015	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85063450	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85063450	#num#	k4
</s>
