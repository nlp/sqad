<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
založil	založit	k5eAaPmAgInS	založit
Galton	Galton	k1gInSc1	Galton
Národní	národní	k2eAgFnSc4d1	národní
eugenickou	eugenický	k2eAgFnSc4d1	eugenická
laboratoř	laboratoř	k1gFnSc4	laboratoř
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
i	i	k9	i
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
eugenickou	eugenický	k2eAgFnSc4d1	eugenická
osvětu	osvěta	k1gFnSc4	osvěta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
kladla	klást	k5eAaImAgFnS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
sterilizaci	sterilizace	k1gFnSc4	sterilizace
duševně	duševně	k6eAd1	duševně
nemocných	nemocný	k1gMnPc2	nemocný
a	a	k8xC	a
neduživých	duživý	k2eNgMnPc2d1	neduživý
<g/>
.	.	kIx.	.
</s>
