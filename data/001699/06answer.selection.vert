<s>
Henryk	Henryk	k1gMnSc1	Henryk
Adam	Adam	k1gMnSc1	Adam
Aleksander	Aleksander	k1gMnSc1	Aleksander
Pius	Pius	k1gMnSc1	Pius
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
alɛ	alɛ	k?	alɛ
ˈ	ˈ	k?	ˈ
ɕ	ɕ	k?	ɕ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
Wola	Wol	k2eAgFnSc1d1	Wola
Okrzejska	Okrzejska	k1gFnSc1	Okrzejska
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Vevey	Vevea	k1gFnPc1	Vevea
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
