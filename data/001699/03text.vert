<s>
Henryk	Henryk	k1gMnSc1	Henryk
Adam	Adam	k1gMnSc1	Adam
Aleksander	Aleksander	k1gMnSc1	Aleksander
Pius	Pius	k1gMnSc1	Pius
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
alɛ	alɛ	k?	alɛ
ˈ	ˈ	k?	ˈ
ɕ	ɕ	k?	ɕ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
Wola	Wol	k2eAgFnSc1d1	Wola
Okrzejska	Okrzejska	k1gFnSc1	Okrzejska
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Vevey	Vevea	k1gFnPc1	Vevea
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
slávu	sláva	k1gFnSc4	sláva
získal	získat	k5eAaPmAgMnS	získat
svými	svůj	k3xOyFgInPc7	svůj
historickými	historický	k2eAgInPc7d1	historický
romány	román	k1gInPc7	román
<g/>
,	,	kIx,	,
týkajícími	týkající	k2eAgInPc7d1	týkající
se	se	k3xPyFc4	se
polských	polský	k2eAgFnPc2d1	polská
a	a	k8xC	a
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Představitel	představitel	k1gMnSc1	představitel
polského	polský	k2eAgInSc2d1	polský
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
rovněž	rovněž	k9	rovněž
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Litwos	Litwosa	k1gFnPc2	Litwosa
<g/>
.	.	kIx.	.
</s>
<s>
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Wola	Wolus	k1gMnSc2	Wolus
Okrzejska	Okrzejsek	k1gMnSc2	Okrzejsek
v	v	k7c6	v
Łukówském	Łukówský	k2eAgInSc6d1	Łukówský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
středního	střední	k2eAgInSc2d1	střední
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
Józef	Józef	k1gInSc4	Józef
Sienkiewicz	Sienkiewicza	k1gFnPc2	Sienkiewicza
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stefania	Stefanium	k1gNnSc2	Stefanium
roz	roz	k?	roz
Cieciszowska	Cieciszowsko	k1gNnSc2	Cieciszowsko
Sienkiewicz	Sienkiewicza	k1gFnPc2	Sienkiewicza
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Józef	Józef	k1gMnSc1	Józef
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
byl	být	k5eAaImAgMnS	být
zámožným	zámožný	k2eAgMnSc7d1	zámožný
statkářem	statkář	k1gMnSc7	statkář
<g/>
.	.	kIx.	.
</s>
<s>
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
proto	proto	k8xC	proto
prožil	prožít	k5eAaPmAgMnS	prožít
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
mezi	mezi	k7c7	mezi
vesnickým	vesnický	k2eAgInSc7d1	vesnický
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
námětech	námět	k1gInPc6	námět
jeho	jeho	k3xOp3gFnPc2	jeho
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celým	celý	k2eAgNnSc7d1	celé
založením	založení	k1gNnSc7	založení
jeho	jeho	k3xOp3gFnSc2	jeho
povahy	povaha	k1gFnSc2	povaha
a	a	k8xC	a
sociálních	sociální	k2eAgInPc2d1	sociální
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
byl	být	k5eAaImAgMnS	být
Sienkiewiczův	Sienkiewiczův	k2eAgMnSc1d1	Sienkiewiczův
otec	otec	k1gMnSc1	otec
donucen	donucen	k2eAgInSc4d1	donucen
svůj	svůj	k3xOyFgInSc4	svůj
statek	statek	k1gInSc4	statek
prodat	prodat	k5eAaPmF	prodat
a	a	k8xC	a
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mu	on	k3xPp3gMnSc3	on
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
nejvíc	nejvíc	k6eAd1	nejvíc
námětů	námět	k1gInPc2	námět
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
prodchnuté	prodchnutý	k2eAgNnSc1d1	prodchnuté
autorovým	autorův	k2eAgNnSc7d1	autorovo
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
pochopením	pochopení	k1gNnSc7	pochopení
pro	pro	k7c4	pro
neustálé	neustálý	k2eAgNnSc4d1	neustálé
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
krvavé	krvavý	k2eAgNnSc1d1	krvavé
úsilí	úsilí	k1gNnSc1	úsilí
vlastního	vlastní	k2eAgInSc2d1	vlastní
národa	národ	k1gInSc2	národ
o	o	k7c4	o
znovunabytí	znovunabytí	k1gNnSc4	znovunabytí
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jeho	jeho	k3xOp3gNnPc2	jeho
studií	studio	k1gNnPc2	studio
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
obdobím	období	k1gNnSc7	období
nástupu	nástup	k1gInSc2	nástup
kritické	kritický	k2eAgFnSc2d1	kritická
generace	generace	k1gFnSc2	generace
pozitivistů	pozitivista	k1gMnPc2	pozitivista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tehdy	tehdy	k6eAd1	tehdy
formovali	formovat	k5eAaImAgMnP	formovat
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
velmi	velmi	k6eAd1	velmi
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
včlenil	včlenit	k5eAaPmAgMnS	včlenit
do	do	k7c2	do
pozitivistické	pozitivistický	k2eAgFnSc2d1	pozitivistická
publicistiky	publicistika	k1gFnSc2	publicistika
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
divadelní	divadelní	k2eAgFnPc4d1	divadelní
recenze	recenze	k1gFnPc4	recenze
i	i	k9	i
literárně	literárně	k6eAd1	literárně
historické	historický	k2eAgFnPc4d1	historická
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k9	i
jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
beletristické	beletristický	k2eAgInPc1d1	beletristický
pokusy	pokus	k1gInPc1	pokus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
povídka	povídka	k1gFnSc1	povídka
Na	na	k7c4	na
zmar	zmar	k1gInSc4	zmar
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
brzy	brzy	k6eAd1	brzy
svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
získaly	získat	k5eAaPmAgInP	získat
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
novinářskou	novinářský	k2eAgFnSc7d1	novinářská
tvorbou	tvorba	k1gFnSc7	tvorba
Obzory	obzor	k1gInPc4	obzor
si	se	k3xPyFc3	se
Sienkiewicz	Sienkiewicz	k1gInSc1	Sienkiewicz
rozšiřoval	rozšiřovat	k5eAaImAgInS	rozšiřovat
také	také	k9	také
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
cestami	cesta	k1gFnPc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgInS	žít
romantickým	romantický	k2eAgInSc7d1	romantický
životem	život	k1gInSc7	život
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
fourierovské	fourierovský	k2eAgFnSc2d1	fourierovský
komunity	komunita	k1gFnSc2	komunita
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
uprostřed	uprostřed	k7c2	uprostřed
přírody	příroda	k1gFnSc2	příroda
jižní	jižní	k2eAgFnSc2d1	jižní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
odjel	odjet	k5eAaPmAgInS	odjet
na	na	k7c4	na
lovecké	lovecký	k2eAgNnSc4d1	lovecké
safari	safari	k1gNnSc4	safari
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jako	jako	k9	jako
už	už	k6eAd1	už
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
spisovatel	spisovatel	k1gMnSc1	spisovatel
hlavním	hlavní	k2eAgMnSc7d1	hlavní
redaktorem	redaktor	k1gMnSc7	redaktor
varšavského	varšavský	k2eAgInSc2d1	varšavský
Słowa	Słow	k1gInSc2	Słow
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
črt	črta	k1gFnPc2	črta
<g/>
,	,	kIx,	,
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
realistických	realistický	k2eAgInPc2d1	realistický
obrazů	obraz	k1gInPc2	obraz
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
vykreslil	vykreslit	k5eAaPmAgInS	vykreslit
obraz	obraz	k1gInSc1	obraz
bídy	bída	k1gFnSc2	bída
a	a	k8xC	a
zaostalosti	zaostalost	k1gFnSc2	zaostalost
nižších	nízký	k2eAgFnPc2d2	nižší
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozpory	rozpor	k1gInPc7	rozpor
zmítané	zmítaný	k2eAgFnSc2d1	zmítaná
postavy	postava	k1gFnSc2	postava
inteligentů	inteligent	k1gMnPc2	inteligent
<g/>
,	,	kIx,	,
snažících	snažící	k2eAgFnPc2d1	snažící
se	se	k3xPyFc4	se
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Nesmírný	smírný	k2eNgInSc1d1	nesmírný
zájem	zájem	k1gInSc1	zájem
však	však	k9	však
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
teprve	teprve	k6eAd1	teprve
svým	svůj	k3xOyFgInSc7	svůj
prvním	první	k4xOgMnSc6	první
románem	román	k1gInSc7	román
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
Ogniem	Ognium	k1gNnSc7	Ognium
i	i	k8xC	i
mieczem	miecz	k1gInSc7	miecz
<g/>
)	)	kIx)	)
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
veliké	veliký	k2eAgFnSc2d1	veliká
historické	historický	k2eAgFnSc2d1	historická
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k9	ještě
Potopa	potopa	k1gFnSc1	potopa
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Potop	potopit	k5eAaPmRp2nS	potopit
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pan	Pan	k1gMnSc1	Pan
Wołodyjowski	Wołodyjowsk	k1gFnSc2	Wołodyjowsk
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Světové	světový	k2eAgFnSc2d1	světová
proslulosti	proslulost	k1gFnSc2	proslulost
pak	pak	k6eAd1	pak
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svým	svůj	k3xOyFgInSc7	svůj
románem	román	k1gInSc7	román
z	z	k7c2	z
římských	římský	k2eAgFnPc2d1	římská
dějin	dějiny	k1gFnPc2	dějiny
Quo	Quo	k1gFnSc1	Quo
vadis	vadis	k1gFnSc1	vadis
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
a	a	k8xC	a
uznávaným	uznávaný	k2eAgInSc7d1	uznávaný
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
mu	on	k3xPp3gNnSc3	on
vděčný	vděčný	k2eAgInSc4d1	vděčný
národ	národ	k1gInSc4	národ
daroval	darovat	k5eAaPmAgInS	darovat
statek	statek	k1gInSc1	statek
Oblegorek	Oblegorek	k1gInSc4	Oblegorek
<g/>
,	,	kIx,	,
zakoupený	zakoupený	k2eAgInSc4d1	zakoupený
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
darů	dar	k1gInPc2	dar
<g/>
.	.	kIx.	.
</s>
<s>
Vysokých	vysoká	k1gFnPc2	vysoká
poct	pocta	k1gFnPc2	pocta
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
dostávalo	dostávat	k5eAaImAgNnS	dostávat
od	od	k7c2	od
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
domácích	domácí	k2eAgMnPc2d1	domácí
i	i	k8xC	i
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
,	,	kIx,	,
od	od	k7c2	od
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
univerzit	univerzita	k1gFnPc2	univerzita
i	i	k8xC	i
soukromých	soukromý	k2eAgFnPc2d1	soukromá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
za	za	k7c2	za
velkolepé	velkolepý	k2eAgFnSc2d1	velkolepá
zásluhy	zásluha	k1gFnSc2	zásluha
epického	epický	k2eAgMnSc2d1	epický
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
usadil	usadit	k5eAaPmAgMnS	usadit
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
městě	město	k1gNnSc6	město
Vevey	Vevea	k1gFnSc2	Vevea
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
organizoval	organizovat	k5eAaBmAgMnS	organizovat
pomoc	pomoc	k1gFnSc4	pomoc
polským	polský	k2eAgFnPc3d1	polská
obětem	oběť	k1gFnPc3	oběť
války	válka	k1gFnSc2	válka
a	a	k8xC	a
kde	kde	k6eAd1	kde
také	také	k9	také
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zmar	zmar	k1gInSc4	zmar
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
Na	na	k7c6	na
marne	marn	k1gMnSc5	marn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Vniveč	vniveč	k6eAd1	vniveč
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
ze	z	k7c2	z
studentského	studentský	k2eAgInSc2d1	studentský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Humoresky	humoreska	k1gFnPc1	humoreska
ze	z	k7c2	z
zápisků	zápisek	k1gInPc2	zápisek
Voršily	Voršila	k1gFnSc2	Voršila
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
Humoreski	Humoresk	k1gFnPc1	Humoresk
z	z	k7c2	z
teki	tek	k1gFnSc2	tek
Worszyły	Worszyła	k1gFnSc2	Worszyła
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
Nikdo	nikdo	k3yNnSc1	nikdo
není	být	k5eNaImIp3nS	být
doma	doma	k6eAd1	doma
prorokem	prorok	k1gMnSc7	prorok
(	(	kIx(	(
<g/>
Nikt	Nikt	k2eAgInSc1d1	Nikt
nie	nie	k?	nie
jest	být	k5eAaImIp3nS	být
prorokiem	prorokius	k1gMnSc7	prorokius
między	międza	k1gFnSc2	międza
swymi	swy	k1gFnPc7	swy
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dvě	dva	k4xCgFnPc1	dva
cesty	cesta	k1gFnPc1	cesta
(	(	kIx(	(
<g/>
Dwie	Dwie	k1gFnSc1	Dwie
drogi	drog	k1gFnSc2	drog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Selanka	selanka	k1gFnSc1	selanka
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Sielanka	Sielanka	k1gFnSc1	Sielanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
tzv.	tzv.	kA	tzv.
Malá	malý	k2eAgFnSc1d1	malá
trilogie	trilogie	k1gFnSc1	trilogie
(	(	kIx(	(
<g/>
Mała	Mał	k2eAgFnSc1d1	Mał
trylogia	trylogia	k1gFnSc1	trylogia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novely	novela	k1gFnSc2	novela
ze	z	k7c2	z
života	život	k1gInSc2	život
venkovské	venkovský	k2eAgFnSc2d1	venkovská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
vyprávěné	vyprávěný	k2eAgNnSc1d1	vyprávěné
mladým	mladý	k1gMnSc7	mladý
šlechticem	šlechtic	k1gMnSc7	šlechtic
Jindřichem	Jindřich	k1gMnSc7	Jindřich
<g/>
:	:	kIx,	:
Starý	Starý	k1gMnSc1	Starý
sluha	sluha	k1gMnSc1	sluha
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Stary	star	k1gInPc1	star
sługa	sług	k1gMnSc2	sług
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátká	krátký	k2eAgFnSc1d1	krátká
charakteristika	charakteristika	k1gFnSc1	charakteristika
<g />
.	.	kIx.	.
</s>
<s>
sluhy	sluha	k1gMnSc2	sluha
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
končící	končící	k2eAgMnSc1d1	končící
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
Haňa	Haňa	k1gFnSc1	Haňa
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
Hania	Hania	k1gFnSc1	Hania
<g/>
)	)	kIx)	)
-	-	kIx~	-
milostný	milostný	k2eAgInSc1d1	milostný
příběh	příběh	k1gInSc1	příběh
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
Haňa	Haňum	k1gNnSc2	Haňum
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
a	a	k8xC	a
Selim	Selim	k?	Selim
Mirza	Mirz	k1gMnSc2	Mirz
Selim	Selim	k?	Selim
Mirza	Mirz	k1gMnSc2	Mirz
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jindřich	Jindřich	k1gMnSc1	Jindřich
se	s	k7c7	s
Selimem	Selim	k1gInSc7	Selim
bojují	bojovat	k5eAaImIp3nP	bojovat
za	za	k7c4	za
Francii	Francie	k1gFnSc4	Francie
v	v	k7c6	v
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
Črty	črta	k1gFnSc2	črta
uhlem	uhel	k1gInSc7	uhel
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
Szkice	Szkice	k1gFnSc1	Szkice
<g />
.	.	kIx.	.
</s>
<s>
węglem	węgl	k1gInSc7	węgl
<g/>
)	)	kIx)	)
-	-	kIx~	-
tragický	tragický	k2eAgInSc1d1	tragický
příběh	příběh	k1gInSc1	příběh
rodiny	rodina	k1gFnSc2	rodina
dřevorubce	dřevorubec	k1gMnSc2	dřevorubec
Řepy	řepa	k1gFnSc2	řepa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doplatila	doplatit	k5eAaPmAgFnS	doplatit
na	na	k7c4	na
zvůli	zvůle	k1gFnSc4	zvůle
vrchnosti	vrchnost	k1gFnSc2	vrchnost
Komedie	komedie	k1gFnSc2	komedie
z	z	k7c2	z
omylů	omyl	k1gInPc2	omyl
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
Komedia	Komedium	k1gNnSc2	Komedium
z	z	k7c2	z
pomyłek	pomyłky	k1gFnPc2	pomyłky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
Po	po	k7c6	po
stepích	step	k1gFnPc6	step
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Przez	Przez	k1gInSc1	Przez
stepy	step	k1gInPc1	step
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
črta	črta	k1gFnSc1	črta
<g/>
,	,	kIx,	,
Tatarské	tatarský	k2eAgNnSc1d1	tatarské
zajetí	zajetí	k1gNnSc1	zajetí
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
Niewola	Niewola	k1gFnSc1	Niewola
tatarska	tatarska	k1gFnSc1	tatarska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
autorova	autorův	k2eAgFnSc1d1	autorova
historická	historický	k2eAgFnSc1d1	historická
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
Na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
kartu	karta	k1gFnSc4	karta
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
Na	na	k7c6	na
jedną	jedną	k?	jedną
kartę	kartę	k?	kartę
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
novely	novela	k1gFnPc1	novela
<g/>
:	:	kIx,	:
Orso	Orso	k1gNnSc1	Orso
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pamětí	paměť	k1gFnPc2	paměť
poznaňského	poznaňský	k2eAgMnSc2d1	poznaňský
učitele	učitel	k1gMnSc2	učitel
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pamiętnika	pamiętnik	k1gMnSc2	pamiętnik
poznańskiego	poznańskiego	k6eAd1	poznańskiego
nauczyciela	nauczyciet	k5eAaImAgFnS	nauczyciet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
-	-	kIx~	-
Polák	Polák	k1gMnSc1	Polák
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Maryša	Maryša	k1gFnSc1	Maryša
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
za	za	k7c7	za
vidinou	vidina	k1gFnSc7	vidina
lepšího	dobrý	k2eAgInSc2d2	lepší
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
však	však	k9	však
pravý	pravý	k2eAgInSc4d1	pravý
opak	opak	k1gInSc4	opak
<g/>
.	.	kIx.	.
</s>
<s>
Honzík	Honzík	k1gMnSc1	Honzík
muzikant	muzikant	k1gMnSc1	muzikant
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
Janko	Janko	k1gMnSc1	Janko
muzykant	muzykant	k1gMnSc1	muzykant
<g/>
)	)	kIx)	)
-	-	kIx~	-
hudební	hudební	k2eAgNnSc1d1	hudební
nadání	nadání	k1gNnSc1	nadání
Honzíka	Honzík	k1gMnSc2	Honzík
nemá	mít	k5eNaImIp3nS	mít
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
chudobě	chudoba	k1gFnSc3	chudoba
šanci	šance	k1gFnSc3	šance
na	na	k7c4	na
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Bartek	Bartek	k1gMnSc1	Bartek
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
Vítěz	vítěz	k1gMnSc1	vítěz
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Bartek	Bartek	k1gMnSc1	Bartek
zwycięzca	zwycięzca	k1gMnSc1	zwycięzca
<g/>
)	)	kIx)	)
-	-	kIx~	-
sedlák	sedlák	k1gMnSc1	sedlák
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
voják	voják	k1gMnSc1	voják
v	v	k7c6	v
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
selhává	selhávat	k5eAaImIp3nS	selhávat
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jamioł	Jamioł	k?	Jamioł
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strážce	strážce	k1gMnSc1	strážce
majáku	maják	k1gInSc2	maják
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Latarnik	Latarnik	k1gInSc1	Latarnik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čí	čí	k3xOyRgFnSc7	čí
vinou	vina	k1gFnSc7	vina
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Czyja	Czyj	k2eAgFnSc1d1	Czyj
wina	wina	k1gFnSc1	wina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k8xC	jako
Kdo	kdo	k3yRnSc1	kdo
to	ten	k3xDgNnSc4	ten
zavinil	zavinit	k5eAaPmAgMnS	zavinit
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Trilogie	trilogie	k1gFnSc1	trilogie
(	(	kIx(	(
<g/>
Trylogia	Trylogia	k1gFnSc1	Trylogia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monumentální	monumentální	k2eAgFnSc1d1	monumentální
románová	románový	k2eAgFnSc1d1	románová
trilogie	trilogie	k1gFnSc1	trilogie
z	z	k7c2	z
polských	polský	k2eAgFnPc2d1	polská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgInPc1d1	pojednávající
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nejdivočejších	divoký	k2eAgFnPc2d3	nejdivočejší
epoch	epocha	k1gFnPc2	epocha
polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Polsko	Polsko	k1gNnSc1	Polsko
bojovalo	bojovat	k5eAaImAgNnS	bojovat
na	na	k7c4	na
život	život	k1gInSc4	život
a	a	k8xC	a
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
s	s	k7c7	s
kozáky	kozák	k1gInPc7	kozák
během	během	k7c2	během
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
Chmelnického	Chmelnický	k2eAgNnSc2d1	Chmelnický
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Švédy	švéda	k1gFnPc1	švéda
a	a	k8xC	a
Turky	Turek	k1gMnPc4	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
se	s	k7c7	s
tří	tři	k4xCgNnPc2	tři
samostatných	samostatný	k2eAgNnPc2d1	samostatné
děl	dělo	k1gNnPc2	dělo
volně	volně	k6eAd1	volně
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
navazujících	navazující	k2eAgFnPc2d1	navazující
a	a	k8xC	a
spojených	spojený	k2eAgFnPc2d1	spojená
některými	některý	k3yIgFnPc7	některý
postavami	postava	k1gFnPc7	postava
<g/>
:	:	kIx,	:
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
Ogniem	Ognium	k1gNnSc7	Ognium
i	i	k8xC	i
mieczem	miecz	k1gInSc7	miecz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Potopa	potopa	k1gFnSc1	potopa
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Potop	potopit	k5eAaPmRp2nS	potopit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Wołodyjowski	Wołodyjowsk	k1gFnSc2	Wołodyjowsk
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
novely	novela	k1gFnPc4	novela
Výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Wycieczka	Wycieczka	k1gFnSc1	Wycieczka
do	do	k7c2	do
Aten	Atena	k1gFnPc2	Atena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sachem	Sach	k1gInSc7	Sach
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
z	z	k7c2	z
Maripozy	Maripoz	k1gInPc1	Maripoz
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Wspomnienia	Wspomnienium	k1gNnSc2	Wspomnienium
z	z	k7c2	z
Maripozy	Maripoz	k1gInPc4	Maripoz
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ta	ten	k3xDgFnSc1	ten
třetí	třetí	k4xOgFnSc2	třetí
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Ta	ten	k3xDgFnSc1	ten
trzecia	trzecia	k1gFnSc1	trzecia
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salabova	Salabův	k2eAgFnSc1d1	Salabův
pohádka	pohádka	k1gFnSc1	pohádka
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Salabowa	Salabow	k2eAgFnSc1d1	Salabow
bajka	bajka	k1gFnSc1	bajka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bez	bez	k7c2	bez
dogmatu	dogma	k1gNnSc2	dogma
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
hlavním	hlavní	k2eAgMnSc6d1	hlavní
hrdinovi	hrdina	k1gMnSc6	hrdina
Leonu	Leo	k1gMnSc6	Leo
Ploszowském	Ploszowský	k1gMnSc6	Ploszowský
autor	autor	k1gMnSc1	autor
vykreslil	vykreslit	k5eAaPmAgInS	vykreslit
typ	typ	k1gInSc4	typ
vysoce	vysoce	k6eAd1	vysoce
inteligentního	inteligentní	k2eAgMnSc4d1	inteligentní
Poláka	Polák	k1gMnSc4	Polák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
změkčilého	změkčilý	k2eAgMnSc4d1	změkčilý
<g/>
,	,	kIx,	,
nervosního	nervosní	k2eAgMnSc4d1	nervosní
slabocha	slaboch	k1gMnSc4	slaboch
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Listy	list	k1gInPc1	list
z	z	k7c2	z
Afryki	Afryk	k1gFnSc2	Afryk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novely	novela	k1gFnSc2	novela
U	u	k7c2	u
zřídla	zřídlo	k1gNnSc2	zřídlo
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
U	u	k7c2	u
źródła	źródł	k1gInSc2	źródł
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Buď	buď	k8xC	buď
blahoslavena	blahoslaven	k2eAgFnSc1d1	blahoslavena
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Bądź	Bądź	k1gFnSc1	Bądź
błogosławiona	błogosławiona	k1gFnSc1	błogosławiona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pojďme	jít	k5eAaImRp1nP	jít
za	za	k7c7	za
Ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Pójdźmy	Pójdźm	k1gInPc1	Pójdźm
za	za	k7c4	za
Nim	on	k3xPp3gFnPc3	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Varhaník	varhaník	k1gMnSc1	varhaník
z	z	k7c2	z
Ponikly	Ponikl	k1gInPc1	Ponikl
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Organista	organista	k1gMnSc1	organista
z	z	k7c2	z
Ponikły	Ponikła	k1gFnSc2	Ponikła
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lux	Lux	k1gMnSc1	Lux
in	in	k?	in
tenebris	tenebris	k1gInSc1	tenebris
lucet	lucet	k1gInSc1	lucet
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Diův	Diův	k2eAgInSc4d1	Diův
rozsudek	rozsudek	k1gInSc4	rozsudek
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Wyrok	Wyrok	k1gInSc1	Wyrok
Zeusa	Zeusa	k1gFnSc1	Zeusa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rodina	rodina	k1gFnSc1	rodina
Polanieckých	Polaniecká	k1gFnPc2	Polaniecká
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Rodzina	Rodzina	k1gFnSc1	Rodzina
Połanieckich	Połanieckich	k1gMnSc1	Połanieckich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
hrdina	hrdina	k1gMnSc1	hrdina
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
typem	typ	k1gInSc7	typ
člověka	člověk	k1gMnSc2	člověk
agilního	agilní	k2eAgMnSc2d1	agilní
<g/>
,	,	kIx,	,
silného	silný	k2eAgMnSc2d1	silný
a	a	k8xC	a
podnikavého	podnikavý	k2eAgInSc2d1	podnikavý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
však	však	k9	však
při	při	k7c6	při
veškeré	veškerý	k3xTgFnSc6	veškerý
své	svůj	k3xOyFgFnSc6	svůj
inteligenci	inteligence	k1gFnSc6	inteligence
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
pravého	pravý	k2eAgNnSc2d1	pravé
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzrůstání	vzrůstání	k1gNnSc6	vzrůstání
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Quo	Quo	k?	Quo
vadis	vadis	k1gInSc1	vadis
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
o	o	k7c6	o
pronásledování	pronásledování	k1gNnSc6	pronásledování
a	a	k8xC	a
osudech	osud	k1gInPc6	osud
křesťanů	křesťan	k1gMnPc2	křesťan
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Neronově	Neronův	k2eAgFnSc6d1	Neronova
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
podtextu	podtext	k1gInSc6	podtext
posílit	posílit	k5eAaPmF	posílit
sebevědomí	sebevědomí	k1gNnSc4	sebevědomí
Poláků	Polák	k1gMnPc2	Polák
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
krásná	krásný	k2eAgFnSc1d1	krásná
princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
křesťanka	křesťanka	k1gFnSc1	křesťanka
Lygie	Lygie	k1gFnSc1	Lygie
i	i	k8xC	i
její	její	k3xOp3gMnSc1	její
ochránce	ochránce	k1gMnSc1	ochránce
neobyčejně	obyčejně	k6eNd1	obyčejně
silný	silný	k2eAgMnSc1d1	silný
otrok	otrok	k1gMnSc1	otrok
Ursus	Ursus	k1gMnSc1	Ursus
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
polských	polský	k2eAgNnPc2d1	polské
území	území	k1gNnPc2	území
<g/>
)	)	kIx)	)
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc4	Prusko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
dostal	dostat	k5eAaPmAgMnS	dostat
spisovatel	spisovatel	k1gMnSc1	spisovatel
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
(	(	kIx(	(
<g/>
epizody	epizoda	k1gFnPc1	epizoda
vycházely	vycházet	k5eAaImAgFnP	vycházet
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1897-1899	[number]	k4	1897-1899
časopisecky	časopisecky	k6eAd1	časopisecky
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
kompletně	kompletně	k6eAd1	kompletně
knižně	knižně	k6eAd1	knižně
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Krzyżacy	Krzyżaca	k1gFnPc1	Krzyżaca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Řádem	řád	k1gInSc7	řád
Německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
popisem	popis	k1gInSc7	popis
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Grunwaldu	Grunwald	k1gInSc2	Grunwald
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
bitev	bitva	k1gFnPc2	bitva
středověku	středověk	k1gInSc2	středověk
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
polsko-litevská	polskoitevský	k2eAgNnPc4d1	polsko-litevské
vojska	vojsko	k1gNnPc4	vojsko
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
porazila	porazit	k5eAaPmAgFnS	porazit
křižáky	křižák	k1gInPc4	křižák
<g/>
.	.	kIx.	.
novely	novela	k1gFnPc4	novela
Plavecká	plavecký	k2eAgFnSc1d1	plavecká
legenda	legenda	k1gFnSc1	legenda
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
żeglarska	żeglarska	k1gFnSc1	żeglarska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
loutky	loutka	k1gFnPc1	loutka
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Dwie	Dwie	k1gFnSc1	Dwie
łąki	łąk	k1gFnSc2	łąk
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Olympu	Olymp	k1gInSc6	Olymp
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Olimpie	Olimpie	k1gFnPc4	Olimpie
<g/>
)	)	kIx)	)
a	a	k8xC	a
povídka	povídka	k1gFnSc1	povídka
V	v	k7c6	v
Bělověžském	Bělověžský	k2eAgInSc6d1	Bělověžský
pralese	prales	k1gInSc6	prales
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Z	z	k7c2	z
puszczy	puszcza	k1gFnSc2	puszcza
Białowieskiej	Białowieskiej	k1gInSc1	Białowieskiej
<g/>
)	)	kIx)	)
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Na	na	k7c4	na
polu	pola	k1gFnSc4	pola
chwały	chwała	k1gFnSc2	chwała
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Sobieského	Sobieský	k2eAgMnSc2d1	Sobieský
<g/>
.	.	kIx.	.
</s>
<s>
Víry	víra	k1gFnPc1	víra
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Wiry	Wira	k1gFnPc1	Wira
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc1d1	společenský
román	román	k1gInSc1	román
z	z	k7c2	z
revolučního	revoluční	k2eAgInSc2d1	revoluční
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Pouští	poušť	k1gFnSc7	poušť
a	a	k8xC	a
pralesem	prales	k1gInSc7	prales
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
W	W	kA	W
pustyni	pustyně	k1gFnSc4	pustyně
i	i	k8xC	i
w	w	k?	w
puszczy	puszcza	k1gFnSc2	puszcza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
V	v	k7c6	v
pustinách	pustina	k1gFnPc6	pustina
nebo	nebo	k8xC	nebo
Temnou	temný	k2eAgFnSc7d1	temná
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
,	,	kIx,	,
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
román	román	k1gInSc4	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgFnSc4d1	pojednávající
o	o	k7c6	o
únosu	únos	k1gInSc6	únos
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
následném	následný	k2eAgInSc6d1	následný
útěku	útěk	k1gInSc6	útěk
a	a	k8xC	a
putování	putování	k1gNnSc6	putování
za	za	k7c7	za
rodiči	rodič	k1gMnPc7	rodič
napříč	napříč	k7c7	napříč
Afrikou	Afrika	k1gFnSc7	Afrika
za	za	k7c2	za
časů	čas	k1gInPc2	čas
Mahdího	Mahdí	k2eAgNnSc2d1	Mahdí
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
Legie	legie	k1gFnSc2	legie
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Legiony	legion	k1gInPc1	legion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
Zapomenuté	zapomenutý	k2eAgInPc4d1	zapomenutý
a	a	k8xC	a
nevydané	vydaný	k2eNgInPc4d1	nevydaný
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgNnPc4d1	vydané
díla	dílo	k1gNnPc4	dílo
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
.	.	kIx.	.
</s>
<s>
Čí	čí	k3xOyRgFnSc7	čí
vinou	vina	k1gFnSc7	vina
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sehnal	Sehnal	k1gMnSc1	Sehnal
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Coelestin	Coelestin	k1gMnSc1	Coelestin
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
Na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sehnal	Sehnal	k1gMnSc1	Sehnal
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Coelestin	Coelestin	k1gMnSc1	Coelestin
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
Pro	pro	k7c4	pro
kousek	kousek	k1gInSc4	kousek
chleba	chléb	k1gInSc2	chléb
:	:	kIx,	:
Osudy	osud	k1gInPc1	osud
polského	polský	k2eAgMnSc2d1	polský
vystěhovalce	vystěhovalec	k1gMnSc2	vystěhovalec
<g/>
,	,	kIx,	,
Papežská	papežský	k2eAgFnSc1d1	Papežská
knihtiskárna	knihtiskárna	k1gFnSc1	knihtiskárna
benediktinů	benediktin	k1gMnPc2	benediktin
rajhradských	rajhradský	k2eAgInPc2d1	rajhradský
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Na	na	k7c4	na
zmar	zmar	k1gInSc4	zmar
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Šolc	Šolc	k1gMnSc1	Šolc
<g/>
,	,	kIx,	,
Telč	Telč	k1gFnSc1	Telč
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
C.	C.	kA	C.
Moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
povídku	povídka	k1gFnSc4	povídka
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc4	Beaufort
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Šímáček	Šímáček	k1gInSc1	Šímáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vácslav	Vácslav	k1gMnSc1	Vácslav
Hanus	Hanus	k1gMnSc1	Hanus
<g/>
,	,	kIx,	,
Pojďme	jít	k5eAaImRp1nP	jít
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Šímáček	Šímáček	k1gInSc1	Šímáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Julius	Julius	k1gMnSc1	Julius
Košnář	Košnář	k1gMnSc1	Košnář
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
novelu	novela	k1gFnSc4	novela
ještě	ještě	k6eAd1	ještě
pražský	pražský	k2eAgMnSc1d1	pražský
nakladatel	nakladatel	k1gMnSc1	nakladatel
Václav	Václav	k1gMnSc1	Václav
Kotrba	kotrba	k1gFnSc1	kotrba
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slunném	slunný	k2eAgNnSc6d1	slunné
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lipenský	lipenský	k2eAgMnSc1d1	lipenský
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
tuto	tento	k3xDgFnSc4	tento
novelu	novela	k1gFnSc4	novela
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
zavinil	zavinit	k5eAaPmAgMnS	zavinit
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Arnošt	Arnošt	k1gMnSc1	Arnošt
Schwab-Polabský	Schwab-Polabský	k2eAgMnSc1d1	Schwab-Polabský
<g/>
,	,	kIx,	,
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Přítel	přítel	k1gMnSc1	přítel
domoviny	domovina	k1gFnPc1	domovina
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
až	až	k8xS	až
7	[number]	k4	7
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
románu	román	k1gInSc2	román
vycházející	vycházející	k2eAgFnPc4d1	vycházející
na	na	k7c4	na
pokračování	pokračování	k1gNnPc4	pokračování
<g/>
,	,	kIx,	,
Quo	Quo	k1gFnPc4	Quo
vadis	vadis	k1gFnSc2	vadis
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc4	Beaufort
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
a	a	k8xC	a
1924	[number]	k4	1924
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
dogmatu	dogma	k1gNnSc2	dogma
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
a	a	k8xC	a
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Potopa	potopa	k1gFnSc1	potopa
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
a	a	k8xC	a
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Ohněm	oheň	k1gInSc7	oheň
i	i	k9	i
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Coelestin	Coelestin	k1gMnSc1	Coelestin
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
a	a	k8xC	a
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Wołodyjowski	Wołodyjowski	k1gNnSc2	Wołodyjowski
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
a	a	k8xC	a
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
vydalo	vydat	k5eAaPmAgNnS	vydat
pražské	pražský	k2eAgNnSc1d1	Pražské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Drobných	drobný	k2eAgInPc2d1	drobný
spisů	spis	k1gInPc2	spis
autorovy	autorův	k2eAgFnSc2d1	autorova
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
novely	novela	k1gFnSc2	novela
Bartek	Bartek	k1gMnSc1	Bartek
<g />
.	.	kIx.	.
</s>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Sidoně	Sidona	k1gFnSc6	Sidona
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ještě	ještě	k9	ještě
Sen	sen	k1gInSc1	sen
<g/>
,	,	kIx,	,
Plamének	plamének	k1gInSc1	plamének
<g/>
,	,	kIx,	,
Zvoník	zvoník	k1gMnSc1	zvoník
<g/>
,	,	kIx,	,
Při	při	k7c6	při
večeři	večeře	k1gFnSc6	večeře
<g/>
,	,	kIx,	,
Lux	Lux	k1gMnSc1	Lux
in	in	k?	in
tenebris	tenebris	k1gFnSc2	tenebris
lucet	luceta	k1gFnPc2	luceta
a	a	k8xC	a
Jeřábi	jeřáb	k1gMnPc1	jeřáb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Janko	Janko	k1gMnSc1	Janko
muzikant	muzikant	k1gMnSc1	muzikant
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgInS	přeložit
Jaro	jaro	k6eAd1	jaro
Žalov	Žalov	k1gInSc1	Žalov
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ještě	ještě	k6eAd1	ještě
Črty	črta	k1gFnSc2	črta
uhlem	uhel	k1gInSc7	uhel
<g/>
,	,	kIx,	,
Hlídač	hlídač	k1gMnSc1	hlídač
majáku	maják	k1gInSc2	maják
a	a	k8xC	a
Z	z	k7c2	z
památníku	památník	k1gInSc2	památník
poznaňského	poznaňský	k2eAgMnSc2d1	poznaňský
učitele	učitel	k1gMnSc2	učitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Na	na	k7c6	na
jasném	jasný	k2eAgInSc6d1	jasný
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
Selanka	selanka	k1gFnSc1	selanka
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vácslav	Vácslav	k1gMnSc1	Vácslav
Hanus	Hanus	k1gMnSc1	Hanus
a	a	k8xC	a
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pojďme	jít	k5eAaImRp1nP	jít
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
a	a	k8xC	a
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ještě	ještě	k9	ještě
Zevův	Zevův	k2eAgInSc4d1	Zevův
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
z	z	k7c2	z
Maripozy	Maripoz	k1gInPc4	Maripoz
a	a	k8xC	a
Plaveckou	plavecký	k2eAgFnSc4d1	plavecká
legendu	legenda	k1gFnSc4	legenda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Po	po	k7c6	po
stepích	step	k1gFnPc6	step
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Schwab-Polabský	Schwab-Polabský	k2eAgMnSc1d1	Schwab-Polabský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Selim	Selim	k?	Selim
Mirza	Mirza	k1gFnSc1	Mirza
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Starý	Starý	k1gMnSc1	Starý
sluha	sluha	k1gMnSc1	sluha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ta	ten	k3xDgFnSc1	ten
třetí	třetí	k4xOgFnSc3	třetí
a	a	k8xC	a
Sachem	Sach	k1gInSc7	Sach
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vácslav	Vácslav	k1gMnSc1	Vácslav
Hanus	Hanus	k1gMnSc1	Hanus
a	a	k8xC	a
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
U	u	k7c2	u
zřídla	zřídlo	k1gNnSc2	zřídlo
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ještě	ještě	k9	ještě
Varhaník	varhaník	k1gMnSc1	varhaník
z	z	k7c2	z
Ponikly	Ponikl	k1gInPc4	Ponikl
<g/>
,	,	kIx,	,
Buď	buď	k8xC	buď
blahoslavena	blahoslaven	k2eAgFnSc1d1	blahoslavena
<g/>
,	,	kIx,	,
Salabova	Salabův	k2eAgFnSc1d1	Salabův
pohádka	pohádka	k1gFnSc1	pohádka
a	a	k8xC	a
Orso	Orso	k1gNnSc1	Orso
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
tatarském	tatarský	k2eAgNnSc6d1	tatarské
a	a	k8xC	a
Komedie	komedie	k1gFnPc1	komedie
z	z	k7c2	z
omylů	omyl	k1gInPc2	omyl
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ze	z	k7c2	z
zápisků	zápisek	k1gInPc2	zápisek
Voršily	Voršila	k1gFnSc2	Voršila
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rodina	rodina	k1gFnSc1	rodina
Polanieckých	Polaniecká	k1gFnPc2	Polaniecká
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Duchoslav	Duchoslava	k1gFnPc2	Duchoslava
Panýrek	Panýrka	k1gFnPc2	Panýrka
<g/>
,	,	kIx,	,
Dojmy	dojem	k1gInPc1	dojem
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
,	,	kIx,	,
Listy	list	k1gInPc1	list
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Paulík	Paulík	k1gMnSc1	Paulík
<g/>
,	,	kIx,	,
Listy	list	k1gInPc1	list
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
Skizzy	Skizza	k1gFnSc2	Skizza
<g/>
,	,	kIx,	,
Teplice	teplice	k1gFnSc1	teplice
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaro	jaro	k6eAd1	jaro
Žalov	Žalov	k1gInSc4	Žalov
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Janko	Janko	k1gMnSc1	Janko
muzikant	muzikant	k1gMnSc1	muzikant
<g/>
,	,	kIx,	,
Črty	črta	k1gFnPc4	črta
uhlem	uhel	k1gInSc7	uhel
a	a	k8xC	a
Hlídač	hlídač	k1gInSc4	hlídač
majáku	maják	k1gInSc2	maják
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langner	k1gMnSc1	Langner
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Selim	Selim	k?	Selim
Mirza	Mirza	k1gFnSc1	Mirza
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langner	k1gMnSc1	Langner
<g/>
,	,	kIx,	,
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
J.	J.	kA	J.
Langner	Langnra	k1gFnPc2	Langnra
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Augustin	Augustin	k1gMnSc1	Augustin
Spáčil	Spáčil	k1gMnSc1	Spáčil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
a	a	k8xC	a
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Jej	on	k3xPp3gNnSc4	on
následujme	následovat	k5eAaImRp1nP	následovat
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Alfréd	Alfréd	k1gMnSc1	Alfréd
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Wołodyjowski	Wołodyjowsk	k1gFnSc2	Wołodyjowsk
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Augustin	Augustin	k1gMnSc1	Augustin
Spáčil	Spáčil	k1gMnSc1	Spáčil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšel	vyjít	k5eAaPmAgInS	vyjít
román	román	k1gInSc1	román
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Kvasnička	kvasnička	k1gFnSc1	kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
a	a	k8xC	a
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g/>
,	,	kIx,	,
Potopa	potopa	k1gFnSc1	potopa
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Augustin	Augustin	k1gMnSc1	Augustin
Spáčil	Spáčil	k1gMnSc1	Spáčil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Josef	Josefa	k1gFnPc2	Josefa
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
drobných	drobný	k2eAgFnPc2d1	drobná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Dvě	dva	k4xCgFnPc1	dva
loutky	loutka	k1gFnPc1	loutka
<g/>
,	,	kIx,	,
Diokles	Diokles	k1gInSc1	Diokles
<g/>
,	,	kIx,	,
Aristoklesova	Aristoklesův	k2eAgFnSc1d1	Aristoklesův
příhoda	příhoda	k1gFnSc1	příhoda
(	(	kIx(	(
<g/>
Przygoda	Przygoda	k1gFnSc1	Przygoda
Arystoklesa	Arystoklesa	k1gFnSc1	Arystoklesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Na	na	k7c6	na
Olympu	Olymp	k1gInSc6	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Quo	Quo	k?	Quo
vadis	vadis	k1gInSc1	vadis
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Alex	Alex	k1gMnSc1	Alex
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Zdeborský	Zdeborský	k2eAgMnSc1d1	Zdeborský
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
a	a	k8xC	a
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Augustin	Augustin	k1gMnSc1	Augustin
Spáčil	Spáčil	k1gMnSc1	Spáčil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
román	román	k1gInSc4	román
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vírech	vír	k1gInPc6	vír
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
,	,	kIx,	,
Quo	Quo	k1gMnSc1	Quo
vadis	vadis	k1gInSc1	vadis
<g/>
,	,	kIx,	,
Rokycany	Rokycany	k1gInPc1	Rokycany
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Hodek	Hodek	k1gMnSc1	Hodek
<g/>
,	,	kIx,	,
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
a	a	k8xC	a
pustině	pustina	k1gFnSc6	pustina
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
a	a	k8xC	a
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Orso	Orso	k1gNnSc1	Orso
<g/>
,	,	kIx,	,
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
z	z	k7c2	z
Maripozy	Maripoz	k1gInPc7	Maripoz
<g/>
,	,	kIx,	,
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
H.	H.	kA	H.
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Quo	Quo	k1gMnSc1	Quo
vadis	vadis	k1gFnSc2	vadis
<g/>
,	,	kIx,	,
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pustinách	pustina	k1gFnPc6	pustina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rozvoda	Rozvoda	k1gMnSc1	Rozvoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Josef	Josefa	k1gFnPc2	Josefa
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Legie	legie	k1gFnSc1	legie
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Antonín	Antonín	k1gMnSc1	Antonín
Spáčil	Spáčil	k1gMnSc1	Spáčil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Ohněm	oheň	k1gInSc7	oheň
i	i	k9	i
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Josef	Josefa	k1gFnPc2	Josefa
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Temnou	temný	k2eAgFnSc7d1	temná
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
,	,	kIx,	,
Bačkovský	Bačkovský	k2eAgMnSc1d1	Bačkovský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Smrčka	Smrčka	k1gMnSc1	Smrčka
<g/>
,	,	kIx,	,
Potopa	potopa	k1gFnSc1	potopa
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Vondráček	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Wołodyjowski	Wołodyjowsk	k1gFnSc2	Wołodyjowsk
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Pouští	poušť	k1gFnSc7	poušť
a	a	k8xC	a
pralesem	prales	k1gInSc7	prales
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Vondráček	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Selim	Selim	k?	Selim
Mirza	Mirz	k1gMnSc2	Mirz
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Bez	bez	k7c2	bez
dogmatu	dogma	k1gNnSc2	dogma
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Ve	v	k7c6	v
vírech	vír	k1gInPc6	vír
<g/>
,	,	kIx,	,
Kvasnička	kvasnička	k1gFnSc1	kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Bartek	Bartek	k1gMnSc1	Bartek
vítěz	vítěz	k1gMnSc1	vítěz
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
a	a	k8xC	a
František	František	k1gMnSc1	František
Vondráček	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Po	po	k7c6	po
stepích	step	k1gFnPc6	step
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
črty	črta	k1gFnPc4	črta
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Quo	Quo	k1gMnSc1	Quo
vadis	vadis	k1gFnSc2	vadis
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Potulky	potulka	k1gFnPc1	potulka
Afrikou	Afrika	k1gFnSc7	Afrika
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
cestopisné	cestopisný	k2eAgInPc4d1	cestopisný
črty	črt	k1gInPc4	črt
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Potulky	potulka	k1gFnSc2	potulka
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Rodina	rodina	k1gFnSc1	rodina
Polanieckých	Polaniecká	k1gFnPc2	Polaniecká
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
,	,	kIx,	,
Svatba	svatba	k1gFnSc1	svatba
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Bečka	Bečka	k1gMnSc1	Bečka
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc4	výbor
z	z	k7c2	z
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgFnSc2d1	vydaná
knihy	kniha	k1gFnSc2	kniha
Zapomenuté	zapomenutý	k2eAgInPc1d1	zapomenutý
a	a	k8xC	a
nevydané	vydaný	k2eNgInPc4d1	nevydaný
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
Práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Janouch	Janouch	k1gMnSc1	Janouch
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Práce	práce	k1gFnSc2	práce
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Naše	náš	k3xOp1gNnSc4	náš
vojsko	vojsko	k1gNnSc4	vojsko
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
a	a	k8xC	a
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Sfinga	sfinga	k1gFnSc1	sfinga
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Blok	blok	k1gInSc1	blok
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Črty	črta	k1gFnSc2	črta
uhlem	uhel	k1gInSc7	uhel
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Rumler	Rumler	k1gMnSc1	Rumler
<g/>
,	,	kIx,	,
Strážce	strážce	k1gMnSc1	strážce
majáku	maják	k1gInSc2	maják
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Helena	Helena	k1gFnSc1	Helena
Teigová	Teigový	k2eAgFnSc1d1	Teigový
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Málek	Málek	k1gMnSc1	Málek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Strážce	strážce	k1gMnSc1	strážce
majáku	maják	k1gInSc2	maják
<g/>
,	,	kIx,	,
Za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
<g/>
,	,	kIx,	,
V	v	k7c6	v
Bělověžském	Bělověžský	k2eAgInSc6d1	Bělověžský
pralese	prales	k1gInSc6	prales
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
Janko	Janko	k1gMnSc1	Janko
muzikant	muzikant	k1gMnSc1	muzikant
a	a	k8xC	a
Selim	Selim	k?	Selim
Mirza	Mirza	k1gFnSc1	Mirza
<g/>
.	.	kIx.	.
</s>
<s>
Pouští	poušť	k1gFnSc7	poušť
a	a	k8xC	a
pralesem	prales	k1gInSc7	prales
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Málek	Málek	k1gMnSc1	Málek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1962	[number]	k4	1962
a	a	k8xC	a
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
a	a	k8xC	a
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Vyškov	Vyškov	k1gInSc1	Vyškov
1991	[number]	k4	1991
a	a	k8xC	a
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Potopa	potopa	k1gFnSc1	potopa
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vendulka	Vendulka	k1gFnSc1	Vendulka
Zapletalová	Zapletalová	k1gFnSc1	Zapletalová
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
ještě	ještě	k9	ještě
román	román	k1gInSc4	román
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Blok	blok	k1gInSc1	blok
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Quo	Quo	k?	Quo
vadis	vadis	k1gInSc1	vadis
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Erich	Erich	k1gMnSc1	Erich
Sojka	Sojka	k1gMnSc1	Sojka
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšel	vyjít	k5eAaPmAgMnS	vyjít
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc4	odeon
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
opět	opět	k6eAd1	opět
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vyšehradu	Vyšehrad	k1gInSc2	Vyšehrad
opět	opět	k6eAd1	opět
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vendulka	Vendulka	k1gFnSc1	Vendulka
Zapletalová	Zapletalová	k1gFnSc1	Zapletalová
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšel	vyjít	k5eAaPmAgMnS	vyjít
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc4	odeon
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Blok	blok	k1gInSc1	blok
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Wołodyjowski	Wołodyjowsk	k1gFnSc2	Wołodyjowsk
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vlasta	Vlasta	k1gFnSc1	Vlasta
Dvořáčková	Dvořáčková	k1gFnSc1	Dvořáčková
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšel	vyjít	k5eAaPmAgMnS	vyjít
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc4	odeon
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Práce	práce	k1gFnSc2	práce
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
a	a	k8xC	a
1995	[number]	k4	1995
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
sluha	sluha	k1gMnSc1	sluha
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Málek	Málek	k1gMnSc1	Málek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Rumler	Rumler	k1gMnSc1	Rumler
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Teigová	Teigový	k2eAgFnSc1d1	Teigový
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Starý	Starý	k1gMnSc1	Starý
sluha	sluha	k1gMnSc1	sluha
<g/>
,	,	kIx,	,
Haňa	Haňa	k1gMnSc1	Haňa
<g/>
,	,	kIx,	,
Selim	Selim	k?	Selim
Mirza	Mirz	k1gMnSc2	Mirz
<g/>
,	,	kIx,	,
Črty	črta	k1gFnSc2	črta
uhlem	uhel	k1gInSc7	uhel
<g/>
,	,	kIx,	,
Honzík	Honzík	k1gMnSc1	Honzík
muzikant	muzikant	k1gMnSc1	muzikant
<g/>
,	,	kIx,	,
Vítěz	vítěz	k1gMnSc1	vítěz
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pamětí	paměť	k1gFnPc2	paměť
poznaňského	poznaňský	k2eAgMnSc2d1	poznaňský
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
Za	za	k7c7	za
chlebem	chléb	k1gInSc7	chléb
<g/>
,	,	kIx,	,
Sachem	Sach	k1gInSc7	Sach
a	a	k8xC	a
Strážce	strážce	k1gMnSc1	strážce
majáku	maják	k1gInSc2	maják
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Anetta	Anetto	k1gNnSc2	Anetto
Balajková	Balajková	k1gFnSc1	Balajková
<g/>
.	.	kIx.	.
</s>
<s>
Quo	Quo	k?	Quo
vadis	vadis	k1gFnSc1	vadis
<g/>
,	,	kIx,	,
Omega	omega	k1gFnSc1	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
.	.	kIx.	.
</s>
<s>
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
Omega	omega	k1gFnSc1	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
Omega	omega	k1gNnSc1	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Wolodyjowski	Wolodyjowski	k1gNnSc2	Wolodyjowski
<g/>
,	,	kIx,	,
Omega	omega	k1gNnSc2	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
.	.	kIx.	.
</s>
<s>
Potopa	potopa	k1gFnSc1	potopa
<g/>
,	,	kIx,	,
Omega	omega	k1gFnSc1	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Kredba	Kredba	k1gMnSc1	Kredba
<g/>
.	.	kIx.	.
</s>
