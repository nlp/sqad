<p>
<s>
Posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
(	(	kIx(	(
<g/>
rigor	rigor	k1gInSc1	rigor
mortis	mortis	k1gInSc1	mortis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
mrtvém	mrtvý	k2eAgNnSc6d1	mrtvé
těle	tělo	k1gNnSc6	tělo
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
mají	mít	k5eAaImIp3nP	mít
chemický	chemický	k2eAgInSc4d1	chemický
původ	původ	k1gInSc4	původ
-	-	kIx~	-
jejich	jejich	k3xOp3gFnSc7	jejich
primární	primární	k2eAgFnSc7d1	primární
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
ATP	atp	kA	atp
a	a	k8xC	a
únik	únik	k1gInSc1	únik
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
kationtů	kation	k1gInPc2	kation
ze	z	k7c2	z
sarkoplazmatického	sarkoplazmatický	k2eAgNnSc2d1	sarkoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
ochabnutí	ochabnutí	k1gNnSc1	ochabnutí
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ztuhnutí	ztuhnutí	k1gNnSc1	ztuhnutí
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
1-6	[number]	k4	1-6
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
vyvinutý	vyvinutý	k2eAgMnSc1d1	vyvinutý
pak	pak	k6eAd1	pak
rigor	rigor	k1gInSc1	rigor
mortis	mortis	k1gFnSc2	mortis
bývá	bývat	k5eAaImIp3nS	bývat
až	až	k9	až
po	po	k7c6	po
12-36	[number]	k4	12-36
hodinách	hodina	k1gFnPc6	hodina
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
obličejové	obličejový	k2eAgFnSc6d1	obličejová
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
při	při	k7c6	při
zevní	zevní	k2eAgFnSc6d1	zevní
prohlídce	prohlídka	k1gFnSc6	prohlídka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
končetiny	končetina	k1gFnPc1	končetina
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
ohýbat	ohýbat	k5eAaImF	ohýbat
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
"	"	kIx"	"
<g/>
násilnému	násilný	k2eAgNnSc3d1	násilné
rozrušení	rozrušení	k1gNnSc3	rozrušení
<g/>
"	"	kIx"	"
ztuhlosti	ztuhlost	k1gFnSc3	ztuhlost
dojde	dojít	k5eAaPmIp3nS	dojít
většinou	většinou	k6eAd1	většinou
při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tak	tak	k9	tak
6-8	[number]	k4	6-8
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neobjeví	objevit	k5eNaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozrušení	rozrušení	k1gNnSc6	rozrušení
do	do	k7c2	do
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ještě	ještě	k6eAd1	ještě
vyvinout	vyvinout	k5eAaPmF	vyvinout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
už	už	k6eAd1	už
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
nástupu	nástup	k1gInSc2	nástup
posmrtné	posmrtný	k2eAgFnSc2d1	posmrtná
ztuhlosti	ztuhlost	k1gFnSc2	ztuhlost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
jej	on	k3xPp3gMnSc4	on
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgMnPc4d2	vyšší
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
především	především	k6eAd1	především
autolytických	autolytický	k2eAgFnPc2d1	autolytický
a	a	k8xC	a
hnilobných	hnilobný	k2eAgFnPc2d1	hnilobná
změn	změna	k1gFnPc2	změna
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
začne	začít	k5eAaPmIp3nS	začít
pomíjet	pomíjet	k5eAaImF	pomíjet
asi	asi	k9	asi
za	za	k7c4	za
2	[number]	k4	2
dny	den	k1gInPc4	den
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c7	za
3-4	[number]	k4	3-4
dny	den	k1gInPc7	den
zmizí	zmizet	k5eAaPmIp3nP	zmizet
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
kolem	kolem	k7c2	kolem
20	[number]	k4	20
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
začíná	začínat	k5eAaImIp3nS	začínat
pomíjet	pomíjet	k5eAaImF	pomíjet
asi	asi	k9	asi
za	za	k7c4	za
36-48	[number]	k4	36-48
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
naopak	naopak	k6eAd1	naopak
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
více	hodně	k6eAd2	hodně
dnů	den	k1gInPc2	den
i	i	k8xC	i
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
kolem	kolem	k7c2	kolem
30-40	[number]	k4	30-40
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
začíná	začínat	k5eAaImIp3nS	začínat
posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
už	už	k6eAd1	už
asi	asi	k9	asi
za	za	k7c4	za
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
již	již	k9	již
za	za	k7c4	za
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
rychleji	rychle	k6eAd2	rychle
také	také	k9	také
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jistých	jistý	k2eAgFnPc2d1	jistá
známek	známka	k1gFnPc2	známka
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
odhadovat	odhadovat	k5eAaImF	odhadovat
dobu	doba	k1gFnSc4	doba
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
mj.	mj.	kA	mj.
soudních	soudní	k2eAgMnPc2d1	soudní
lékařů	lékař	k1gMnPc2	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ŠTEFAN	Štefan	k1gMnSc1	Štefan
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
MACH	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Soudně	soudně	k6eAd1	soudně
lékařská	lékařský	k2eAgFnSc1d1	lékařská
a	a	k8xC	a
medicínsko-právní	medicínskorávní	k2eAgFnSc1d1	medicínsko-právní
problematika	problematika	k1gFnSc1	problematika
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
931	[number]	k4	931
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Posmrtné	posmrtný	k2eAgFnSc2d1	posmrtná
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
s.	s.	k?	s.
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
</p>
