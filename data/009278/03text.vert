<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Angoly	Angola	k1gFnSc2	Angola
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Angola	Angola	k1gFnSc1	Angola
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
obdélníku	obdélník	k1gInSc2	obdélník
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
červeným	červený	k2eAgInSc7d1	červený
a	a	k8xC	a
černým	černý	k2eAgMnSc7d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Angoly	Angola	k1gFnSc2	Angola
prolili	prolít	k5eAaPmAgMnP	prolít
během	během	k7c2	během
válek	válka	k1gFnPc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
černá	černé	k1gNnPc4	černé
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
africký	africký	k2eAgInSc1d1	africký
kontinent	kontinent	k1gInSc1	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc1	symbol
ozubeného	ozubený	k2eAgNnSc2d1	ozubené
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
mačety	mačeta	k1gFnSc2	mačeta
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
dělníky	dělník	k1gMnPc4	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
podoba	podoba	k1gFnSc1	podoba
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
srpu	srp	k1gInSc2	srp
a	a	k8xC	a
kladiva	kladivo	k1gNnSc2	kladivo
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
nová	nový	k2eAgFnSc1d1	nová
podoba	podoba	k1gFnSc1	podoba
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
schválena	schválit	k5eAaPmNgFnS	schválit
ani	ani	k8xC	ani
formálně	formálně	k6eAd1	formálně
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
jeskynních	jeskynní	k2eAgFnPc2d1	jeskynní
maleb	malba	k1gFnPc2	malba
nalezených	nalezený	k2eAgFnPc2d1	nalezená
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Tchitundo-Hulu	Tchitundo-Hul	k1gInSc2	Tchitundo-Hul
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nevidí	vidět	k5eNaImIp3nS	vidět
žádný	žádný	k3yNgInSc4	žádný
reálný	reálný	k2eAgInSc4d1	reálný
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
stará	starý	k2eAgFnSc1d1	stará
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
jasné	jasný	k2eAgFnPc4d1	jasná
historické	historický	k2eAgFnPc4d1	historická
souvislosti	souvislost	k1gFnPc4	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k1gMnPc1	jiný
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlajka	vlajka	k1gFnSc1	vlajka
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
používána	používán	k2eAgFnSc1d1	používána
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
podobná	podobný	k2eAgFnSc1d1	podobná
kostarické	kostarický	k2eAgFnSc3d1	kostarická
a	a	k8xC	a
severokorejské	severokorejský	k2eAgFnSc3d1	severokorejská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Angoly	Angola	k1gFnSc2	Angola
</s>
</p>
<p>
<s>
Angolská	angolský	k2eAgFnSc1d1	angolská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Angolská	angolský	k2eAgFnSc1d1	angolská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
