<s>
Hmatové	hmatový	k2eAgInPc1d1	hmatový
receptory	receptor	k1gInPc1	receptor
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
kůži	kůže	k1gFnSc4	kůže
rozprostřeny	rozprostřen	k2eAgFnPc4d1	rozprostřena
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
hustotou	hustota	k1gFnSc7	hustota
-	-	kIx~	-
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
místo	místo	k1gNnSc1	místo
hmatu	hmat	k1gInSc2	hmat
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konečcích	koneček	k1gInPc6	koneček
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nejméně	málo	k6eAd3	málo
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
<g/>
.	.	kIx.	.
</s>
