<s>
Mulat	mulat	k1gMnSc1	mulat
(	(	kIx(	(
<g/>
také	také	k9	také
Mulato	Mulat	k2eAgNnSc1d1	Mulato
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
španělského	španělský	k2eAgInSc2d1	španělský
nebo	nebo	k8xC	nebo
portugalského	portugalský	k2eAgInSc2d1	portugalský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
potomka	potomek	k1gMnSc4	potomek
bělocha	běloch	k1gMnSc4	běloch
a	a	k8xC	a
černocha	černoch	k1gMnSc4	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
hispánských	hispánský	k2eAgMnPc2d1	hispánský
Američanů	Američan	k1gMnPc2	Američan
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
mulaty	mulat	k1gMnPc4	mulat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
koloniální	koloniální	k2eAgFnSc2d1	koloniální
éry	éra	k1gFnSc2	éra
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
používal	používat	k5eAaImAgInS	používat
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
jednoho	jeden	k4xCgInSc2	jeden
evropského	evropský	k2eAgInSc2d1	evropský
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
afrického	africký	k2eAgMnSc2d1	africký
rodiče	rodič	k1gMnSc2	rodič
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
dvou	dva	k4xCgMnPc2	dva
mulatů	mulat	k1gMnPc2	mulat
<g/>
.	.	kIx.	.
mestic	mestic	k1gMnSc1	mestic
métisové	métisový	k2eAgFnSc2d1	métisový
zambo	zambo	k1gMnSc1	zambo
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mulat	mulat	k1gMnSc1	mulat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
