<s>
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
La	la	k1gNnSc1	la
Tour	Toura	k1gFnPc2	Toura
Eiffel	Eiffela	k1gFnPc2	Eiffela
/	/	kIx~	/
<g/>
tuʀ	tuʀ	k?	tuʀ
ɛ	ɛ	k?	ɛ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ocelová	ocelový	k2eAgFnSc1d1	ocelová
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejznámější	známý	k2eAgFnSc1d3	nejznámější
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
dominanta	dominanta	k1gFnSc1	dominanta
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
až	až	k9	až
1889	[number]	k4	1889
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc4	Building
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
300,65	[number]	k4	300,65
metru	metr	k1gInSc2	metr
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
měří	měřit	k5eAaImIp3nS	měřit
včetně	včetně	k7c2	včetně
antény	anténa	k1gFnSc2	anténa
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
324	[number]	k4	324
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
konstruktérovi	konstruktér	k1gMnSc6	konstruktér
Gustavu	Gustav	k1gMnSc6	Gustav
Eiffelovi	Eiffel	k1gMnSc6	Eiffel
<g/>
.	.	kIx.	.
</s>
<s>
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnSc2	výročí
velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
konala	konat	k5eAaImAgFnS	konat
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
zde	zde	k6eAd1	zde
původně	původně	k6eAd1	původně
stát	stát	k5eAaPmF	stát
jen	jen	k9	jen
20	[number]	k4	20
let	léto	k1gNnPc2	léto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
významu	význam	k1gInSc3	význam
coby	coby	k?	coby
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
a	a	k8xC	a
televizní	televizní	k2eAgInSc4d1	televizní
vysílač	vysílač	k1gInSc4	vysílač
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Pařížané	Pařížan	k1gMnPc1	Pařížan
tuto	tento	k3xDgFnSc4	tento
stavbu	stavba	k1gFnSc4	stavba
nejprve	nejprve	k6eAd1	nejprve
nenáviděli	nenávidět	k5eAaImAgMnP	nenávidět
a	a	k8xC	a
označovali	označovat	k5eAaImAgMnP	označovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
trn	trn	k1gInSc4	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
(	(	kIx(	(
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
umístěnou	umístěný	k2eAgFnSc4d1	umístěná
restauraci	restaurace	k1gFnSc4	restaurace
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
na	na	k7c4	na
věž	věž	k1gFnSc4	věž
nemusí	muset	k5eNaImIp3nS	muset
dívat	dívat	k5eAaImF	dívat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kritiky	kritik	k1gMnPc7	kritik
stavby	stavba	k1gFnSc2	stavba
patřili	patřit	k5eAaImAgMnP	patřit
také	také	k9	také
Émile	Émile	k1gFnPc4	Émile
Zola	Zolum	k1gNnSc2	Zolum
nebo	nebo	k8xC	nebo
Alexander	Alexandra	k1gFnPc2	Alexandra
Dumas	Dumas	k1gMnSc1	Dumas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
věž	věž	k1gFnSc4	věž
významným	významný	k2eAgInSc7d1	významný
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
jsou	být	k5eAaImIp3nP	být
Pařížané	Pařížan	k1gMnPc1	Pařížan
hrdí	hrdý	k2eAgMnPc1d1	hrdý
a	a	k8xC	a
který	který	k3yRgInSc1	který
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
asi	asi	k9	asi
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
plošiny	plošina	k1gFnPc4	plošina
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
57	[number]	k4	57
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
115	[number]	k4	115
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
276	[number]	k4	276
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Třemi	tři	k4xCgFnPc7	tři
nohami	noha	k1gFnPc7	noha
věže	věž	k1gFnPc4	věž
jezdí	jezdit	k5eAaImIp3nP	jezdit
lanovky	lanovka	k1gFnPc1	lanovka
do	do	k7c2	do
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
noze	noha	k1gFnSc6	noha
jsou	být	k5eAaImIp3nP	být
schodiště	schodiště	k1gNnSc4	schodiště
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
pak	pak	k6eAd1	pak
vedou	vést	k5eAaImIp3nP	vést
výtahy	výtah	k1gInPc1	výtah
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
oscilace	oscilace	k1gFnSc1	oscilace
vrcholu	vrchol	k1gInSc2	vrchol
i	i	k9	i
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
silném	silný	k2eAgInSc6d1	silný
větru	vítr	k1gInSc6	vítr
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
může	moct	k5eAaImIp3nS	moct
vlivem	vliv	k1gInSc7	vliv
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
tepelná	tepelný	k2eAgFnSc1d1	tepelná
roztažnost	roztažnost	k1gFnSc1	roztažnost
<g/>
)	)	kIx)	)
kolísat	kolísat	k5eAaImF	kolísat
až	až	k9	až
o	o	k7c4	o
18	[number]	k4	18
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
optimální	optimální	k2eAgFnSc6d1	optimální
viditelnosti	viditelnost	k1gFnSc6	viditelnost
lze	lze	k6eAd1	lze
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
dohlédnout	dohlédnout	k5eAaPmF	dohlédnout
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
67	[number]	k4	67
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
věží	věž	k1gFnSc7	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
busta	busta	k1gFnSc1	busta
A.	A.	kA	A.
G.	G.	kA	G.
Eiffela	Eiffel	k1gMnSc2	Eiffel
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sem	sem	k6eAd1	sem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
umístil	umístit	k5eAaPmAgMnS	umístit
Antoine	Antoin	k1gInSc5	Antoin
Bourdelle	Bourdelle	k1gNnSc1	Bourdelle
jako	jako	k8xS	jako
ocenění	ocenění	k1gNnSc1	ocenění
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
balkonem	balkon	k1gInSc7	balkon
první	první	k4xOgFnSc2	první
plošiny	plošina	k1gFnSc2	plošina
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obvodu	obvod	k1gInSc6	obvod
nachází	nacházet	k5eAaImIp3nS	nacházet
nápis	nápis	k1gInSc4	nápis
složený	složený	k2eAgInSc4d1	složený
ze	z	k7c2	z
72	[number]	k4	72
jmen	jméno	k1gNnPc2	jméno
významných	významný	k2eAgMnPc2d1	významný
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
předlohou	předloha	k1gFnSc7	předloha
mnoha	mnoho	k4c2	mnoho
imitací	imitace	k1gFnPc2	imitace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
pro	pro	k7c4	pro
Petřínskou	petřínský	k2eAgFnSc4d1	Petřínská
rozhlednu	rozhledna	k1gFnSc4	rozhledna
či	či	k8xC	či
Blackpool	Blackpool	k1gInSc4	Blackpool
Tower	Towra	k1gFnPc2	Towra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
plány	plán	k1gInPc1	plán
stavby	stavba	k1gFnSc2	stavba
byly	být	k5eAaImAgInP	být
zahájeny	zahájit	k5eAaPmNgInP	zahájit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
až	až	k9	až
1889	[number]	k4	1889
jako	jako	k8xC	jako
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
oslav	oslava	k1gFnPc2	oslava
100	[number]	k4	100
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Eiffel	Eiffel	k1gInSc1	Eiffel
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
plány	plán	k1gInPc7	plán
původně	původně	k6eAd1	původně
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
věže	věž	k1gFnSc2	věž
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Barceloně	Barcelona	k1gFnSc6	Barcelona
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tam	tam	k6eAd1	tam
probíhala	probíhat	k5eAaImAgFnS	probíhat
Světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Radní	radní	k1gMnPc1	radní
města	město	k1gNnSc2	město
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
moc	moc	k6eAd1	moc
nákladný	nákladný	k2eAgMnSc1d1	nákladný
a	a	k8xC	a
v	v	k7c6	v
ohledu	ohled	k1gInSc6	ohled
architektury	architektura	k1gFnSc2	architektura
města	město	k1gNnSc2	město
také	také	k9	také
nevkusný	vkusný	k2eNgInSc1d1	nevkusný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
Eiffel	Eiffel	k1gInSc1	Eiffel
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
odmítnut	odmítnut	k2eAgMnSc1d1	odmítnut
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
Paříži	Paříž	k1gFnSc3	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
výstava	výstava	k1gFnSc1	výstava
konala	konat	k5eAaImAgFnS	konat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
bylo	být	k5eAaImAgNnS	být
předvedeno	předvést	k5eAaPmNgNnS	předvést
mnoho	mnoho	k4c1	mnoho
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
obří	obří	k2eAgFnSc1d1	obří
gilotina	gilotina	k1gFnSc1	gilotina
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
obětem	oběť	k1gFnPc3	oběť
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vysoký	vysoký	k2eAgInSc1d1	vysoký
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vrcholu	vrchol	k1gInSc6	vrchol
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
osvětlovalo	osvětlovat	k5eAaImAgNnS	osvětlovat
noční	noční	k2eAgNnSc1d1	noční
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
obří	obří	k2eAgInSc1d1	obří
220	[number]	k4	220
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yIgFnSc2	který
by	by	kYmCp3nS	by
tryskala	tryskat	k5eAaImAgFnS	tryskat
voda	voda	k1gFnSc1	voda
široko	široko	k6eAd1	široko
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
plán	plán	k1gInSc4	plán
Gustava	Gustav	k1gMnSc2	Gustav
Eiffela	Eiffel	k1gMnSc2	Eiffel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
pracovalo	pracovat	k5eAaImAgNnS	pracovat
50	[number]	k4	50
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vypracovalo	vypracovat	k5eAaPmAgNnS	vypracovat
5300	[number]	k4	5300
výkresů	výkres	k1gInPc2	výkres
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgMnPc2	všecek
18	[number]	k4	18
038	[number]	k4	038
železných	železný	k2eAgInPc2d1	železný
trámů	trám	k1gInPc2	trám
bylo	být	k5eAaImAgNnS	být
detailně	detailně	k6eAd1	detailně
vykresleno	vykreslen	k2eAgNnSc1d1	vykresleno
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
ze	z	k7c2	z
svářkové	svářkový	k2eAgFnSc2d1	svářková
oceli	ocel	k1gFnSc2	ocel
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
pudlováním	pudlování	k1gNnSc7	pudlování
váží	vážit	k5eAaImIp3nP	vážit
asi	asi	k9	asi
7300	[number]	k4	7300
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
100	[number]	k4	100
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
18	[number]	k4	18
038	[number]	k4	038
ocelových	ocelový	k2eAgInPc2d1	ocelový
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
a	a	k8xC	a
půl	půl	k1xP	půl
milionů	milion	k4xCgInPc2	milion
nýtů	nýt	k1gInPc2	nýt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátěr	nátěr	k1gInSc4	nátěr
bylo	být	k5eAaImAgNnS	být
spotřebováno	spotřebovat	k5eAaPmNgNnS	spotřebovat
60	[number]	k4	60
tun	tuna	k1gFnPc2	tuna
barvy	barva	k1gFnSc2	barva
<g/>
..	..	k?	..
Práce	práce	k1gFnSc1	práce
začaly	začít	k5eAaPmAgFnP	začít
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
na	na	k7c4	na
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
skoro	skoro	k6eAd1	skoro
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
na	na	k7c4	na
26	[number]	k4	26
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
cena	cena	k1gFnSc1	cena
prodražila	prodražit	k5eAaPmAgFnS	prodražit
o	o	k7c4	o
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věži	věž	k1gFnSc6	věž
pracovalo	pracovat	k5eAaImAgNnS	pracovat
150	[number]	k4	150
dělníků	dělník	k1gMnPc2	dělník
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
v	v	k7c4	v
Levallois-Perret	Levallois-Perret	k1gInSc4	Levallois-Perret
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
150	[number]	k4	150
-	-	kIx~	-
300	[number]	k4	300
dělníků	dělník	k1gMnPc2	dělník
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
namontováno	namontovat	k5eAaPmNgNnS	namontovat
asi	asi	k9	asi
jen	jen	k9	jen
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
nýtů	nýt	k1gInPc2	nýt
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgNnPc2d1	zbylé
1	[number]	k4	1
500	[number]	k4	500
000	[number]	k4	000
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
konstrukce	konstrukce	k1gFnSc2	konstrukce
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
základy	základ	k1gInPc1	základ
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
stavba	stavba	k1gFnSc1	stavba
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
zeminu	zemina	k1gFnSc4	zemina
pouhých	pouhý	k2eAgInPc2d1	pouhý
4,5	[number]	k4	4,5
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
hodnota	hodnota	k1gFnSc1	hodnota
sedícího	sedící	k2eAgMnSc2d1	sedící
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
židli	židle	k1gFnSc6	židle
<g/>
.	.	kIx.	.
</s>
<s>
Montáž	montáž	k1gFnSc1	montáž
kovových	kovový	k2eAgFnPc2d1	kovová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
stavbyvedoucí	stavbyvedoucí	k1gMnSc1	stavbyvedoucí
působil	působit	k5eAaImAgMnS	působit
Jean	Jean	k1gMnSc1	Jean
Compagnon	Compagnon	k1gMnSc1	Compagnon
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
pomocí	pomocí	k7c2	pomocí
jeřábů	jeřáb	k1gInPc2	jeřáb
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zvedaly	zvedat	k5eAaImAgInP	zvedat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
někdy	někdy	k6eAd1	někdy
až	až	k9	až
30	[number]	k4	30
metrové	metrový	k2eAgFnSc2d1	metrová
složené	složený	k2eAgFnSc2d1	složená
železné	železný	k2eAgFnSc2d1	železná
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
lešení	lešení	k1gNnSc2	lešení
a	a	k8xC	a
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
sloupů	sloup	k1gInPc2	sloup
stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
až	až	k9	až
do	do	k7c2	do
výstavby	výstavba	k1gFnSc2	výstavba
první	první	k4xOgFnSc2	první
plošiny	plošina	k1gFnSc2	plošina
(	(	kIx(	(
<g/>
57	[number]	k4	57
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
druhé	druhý	k4xOgFnSc2	druhý
plošiny	plošina	k1gFnSc2	plošina
ve	v	k7c6	v
115	[number]	k4	115
metrech	metr	k1gInPc6	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1888	[number]	k4	1888
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
dostavěno	dostavět	k5eAaPmNgNnS	dostavět
2	[number]	k4	2
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
pracovníci	pracovník	k1gMnPc1	pracovník
stávkovat	stávkovat	k5eAaImF	stávkovat
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěli	chtít	k5eNaImAgMnP	chtít
pracovat	pracovat	k5eAaImF	pracovat
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
a	a	k8xC	a
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nelíbily	líbit	k5eNaImAgInP	líbit
jejich	jejich	k3xOp3gInPc1	jejich
platy	plat	k1gInPc1	plat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
znamenala	znamenat	k5eAaImAgFnS	znamenat
velké	velký	k2eAgNnSc4d1	velké
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
měli	mít	k5eAaImAgMnP	mít
pracovníci	pracovník	k1gMnPc1	pracovník
lepší	dobrý	k2eAgFnSc2d2	lepší
finanční	finanční	k2eAgFnSc2d1	finanční
podmínky	podmínka	k1gFnSc2	podmínka
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
řemesle	řemeslo	k1gNnSc6	řemeslo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jim	on	k3xPp3gMnPc3	on
Gustave	Gustav	k1gMnSc5	Gustav
Eiffel	Eiffel	k1gInSc4	Eiffel
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
a	a	k8xC	a
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
jim	on	k3xPp3gMnPc3	on
platy	plat	k1gInPc4	plat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nerozlišovala	rozlišovat	k5eNaImAgFnS	rozlišovat
se	se	k3xPyFc4	se
výše	výše	k1gFnSc1	výše
platů	plat	k1gInPc2	plat
podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Gustave	Gustav	k1gMnSc5	Gustav
Eiffel	Eiffel	k1gMnSc1	Eiffel
to	ten	k3xDgNnSc4	ten
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
žádný	žádný	k3yNgInSc4	žádný
rozdíl	rozdíl	k1gInSc4	rozdíl
jestli	jestli	k8xS	jestli
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
50	[number]	k4	50
m	m	kA	m
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
200	[number]	k4	200
metrové	metrový	k2eAgInPc1d1	metrový
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
druhá	druhý	k4xOgFnSc1	druhý
stávka	stávka	k1gFnSc1	stávka
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
úrazů	úraz	k1gInPc2	úraz
bylo	být	k5eAaImAgNnS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
věž	věž	k1gFnSc1	věž
neměla	mít	k5eNaImAgFnS	mít
žádná	žádný	k3yNgNnPc4	žádný
patra	patro	k1gNnPc4	patro
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dvou	dva	k4xCgFnPc2	dva
plošin	plošina	k1gFnPc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
použity	použit	k2eAgInPc4d1	použit
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xS	jako
například	například	k6eAd1	například
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
zábradlí	zábradlí	k1gNnPc4	zábradlí
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stavby	stavba	k1gFnSc2	stavba
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ztratil	ztratit	k5eAaPmAgMnS	ztratit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
spadl	spadnout	k5eAaPmAgInS	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
Každých	každý	k3xTgMnPc2	každý
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
probíhají	probíhat	k5eAaImIp3nP	probíhat
nátěry	nátěr	k1gInPc1	nátěr
a	a	k8xC	a
ošetření	ošetření	k1gNnSc4	ošetření
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
železné	železný	k2eAgFnSc2d1	železná
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
rekonstrukcích	rekonstrukce	k1gFnPc6	rekonstrukce
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
asi	asi	k9	asi
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
tun	tuna	k1gFnPc2	tuna
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1889	[number]	k4	1889
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
otevřena	otevřít	k5eAaPmNgFnS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
bylo	být	k5eAaImAgNnS	být
uvítáno	uvítat	k5eAaPmNgNnS	uvítat
21	[number]	k4	21
salvami	salva	k1gFnPc7	salva
z	z	k7c2	z
děla	dělo	k1gNnSc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
na	na	k7c4	na
probíhající	probíhající	k2eAgInPc4d1	probíhající
veletrhy	veletrh	k1gInPc4	veletrh
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
končily	končit	k5eAaImAgFnP	končit
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
ještě	ještě	k6eAd1	ještě
nefungovaly	fungovat	k5eNaImAgInP	fungovat
výtahy	výtah	k1gInPc1	výtah
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
Eiffelovu	Eiffelův	k2eAgFnSc4d1	Eiffelova
věž	věž	k1gFnSc4	věž
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
za	za	k7c4	za
ten	ten	k3xDgInSc4	ten
týden	týden	k1gInSc4	týden
28	[number]	k4	28
922	[number]	k4	922
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyšlapali	vyšlapat	k5eAaPmAgMnP	vyšlapat
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
až	až	k9	až
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
provozu	provoz	k1gInSc2	provoz
ji	on	k3xPp3gFnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
vstupné	vstupný	k2eAgNnSc1d1	vstupné
5	[number]	k4	5
franků	frank	k1gInPc2	frank
za	za	k7c4	za
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
veletrhů	veletrh	k1gInPc2	veletrh
a	a	k8xC	a
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
ubyli	ubýt	k5eAaPmAgMnP	ubýt
návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
ji	on	k3xPp3gFnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
pouze	pouze	k6eAd1	pouze
150	[number]	k4	150
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Oživení	oživení	k1gNnSc1	oživení
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
probíhalo	probíhat	k5eAaImAgNnS	probíhat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
opět	opět	k6eAd1	opět
probíhala	probíhat	k5eAaImAgFnS	probíhat
Světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
ten	ten	k3xDgInSc4	ten
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
několikanásobek	několikanásobek	k1gInSc4	několikanásobek
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgInPc3d1	minulý
rokům	rok	k1gInPc3	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
veletrhů	veletrh	k1gInPc2	veletrh
opět	opět	k6eAd1	opět
navštěvovalo	navštěvovat	k5eAaImAgNnS	navštěvovat
věž	věž	k1gFnSc4	věž
minimum	minimum	k1gNnSc1	minimum
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
věže	věž	k1gFnSc2	věž
tehdy	tehdy	k6eAd1	tehdy
nebyla	být	k5eNaImAgFnS	být
zaručena	zaručit	k5eAaPmNgFnS	zaručit
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
i	i	k9	i
zbourána	zbourán	k2eAgFnSc1d1	zbourána
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
posledním	poslední	k2eAgNnSc6d1	poslední
patře	patro	k1gNnSc6	patro
pro	pro	k7c4	pro
Ústřední	ústřední	k2eAgInSc4d1	ústřední
meteorologický	meteorologický	k2eAgInSc4d1	meteorologický
úřad	úřad	k1gInSc4	úřad
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Gustava	Gustav	k1gMnSc2	Gustav
Eiffela	Eiffel	k1gMnSc2	Eiffel
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
Evžen	Evžen	k1gMnSc1	Evžen
Ducretet	Ducretet	k1gMnSc1	Ducretet
provedl	provést	k5eAaPmAgMnS	provést
první	první	k4xOgNnSc4	první
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
telegrafní	telegrafní	k2eAgNnSc4d1	telegrafní
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Eiffelovou	Eiffelův	k2eAgFnSc7d1	Eiffelova
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
Pantheonem	Pantheon	k1gInSc7	Pantheon
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
asi	asi	k9	asi
4	[number]	k4	4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
Eiffel	Eiffel	k1gMnSc1	Eiffel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hledal	hledat	k5eAaImAgInS	hledat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zabránit	zabránit	k5eAaPmF	zabránit
likvidaci	likvidace	k1gFnSc4	likvidace
své	svůj	k3xOyFgFnSc2	svůj
věže	věž	k1gFnSc2	věž
po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
koncese	koncese	k1gFnSc2	koncese
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
kapitána	kapitán	k1gMnSc4	kapitán
Gustava	Gustav	k1gMnSc4	Gustav
Ferrié	Ferrié	k1gNnSc7	Ferrié
odpovědného	odpovědný	k2eAgInSc2d1	odpovědný
za	za	k7c4	za
bezdrátová	bezdrátový	k2eAgNnPc4d1	bezdrátové
spojení	spojení	k1gNnPc4	spojení
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
o	o	k7c6	o
vhodnosti	vhodnost	k1gFnSc6	vhodnost
využití	využití	k1gNnSc2	využití
věže	věž	k1gFnSc2	věž
k	k	k7c3	k
experimentům	experiment	k1gInPc3	experiment
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
financoval	financovat	k5eAaBmAgInS	financovat
instalaci	instalace	k1gFnSc4	instalace
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
spojení	spojení	k1gNnSc4	spojení
až	až	k9	až
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
400	[number]	k4	400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
armáda	armáda	k1gFnSc1	armáda
zřídila	zřídit	k5eAaPmAgFnS	zřídit
u	u	k7c2	u
věže	věž	k1gFnSc2	věž
podzemní	podzemní	k2eAgFnSc4d1	podzemní
radiotelegrafickou	radiotelegrafický	k2eAgFnSc4d1	radiotelegrafická
ústřednu	ústředna	k1gFnSc4	ústředna
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1910	[number]	k4	1910
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
Eiffelovi	Eiffelův	k2eAgMnPc1d1	Eiffelův
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
koncese	koncese	k1gFnSc1	koncese
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
mělo	mít	k5eAaImAgNnS	mít
vysílání	vysílání	k1gNnSc1	vysílání
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
dosah	dosah	k1gInSc4	dosah
6	[number]	k4	6
000	[number]	k4	000
km	km	kA	km
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zachycené	zachycený	k2eAgNnSc1d1	zachycené
i	i	k9	i
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
na	na	k7c6	na
oceáně	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
věže	věž	k1gFnSc2	věž
postaven	postaven	k2eAgInSc4d1	postaven
malý	malý	k2eAgInSc4d1	malý
pokusný	pokusný	k2eAgInSc4d1	pokusný
aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
tunel	tunel	k1gInSc4	tunel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
asi	asi	k9	asi
5	[number]	k4	5
000	[number]	k4	000
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
vysílač	vysílač	k1gInSc1	vysílač
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgFnPc4d1	vojenská
sítě	síť	k1gFnPc4	síť
přestavěn	přestavěn	k2eAgMnSc1d1	přestavěn
i	i	k8xC	i
na	na	k7c4	na
civilní	civilní	k2eAgMnPc4d1	civilní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
začalo	začít	k5eAaPmAgNnS	začít
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
vysílání	vysílání	k1gNnSc4	vysílání
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
programů	program	k1gInPc2	program
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgFnP	začít
vysílat	vysílat	k5eAaImF	vysílat
první	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
rádiový	rádiový	k2eAgInSc1d1	rádiový
vysílač	vysílač	k1gInSc1	vysílač
strategický	strategický	k2eAgInSc1d1	strategický
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgInPc1	první
i	i	k8xC	i
druhé	druhý	k4xOgFnSc3	druhý
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
strategický	strategický	k2eAgMnSc1d1	strategický
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
započalo	započnout	k5eAaPmAgNnS	započnout
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
první	první	k4xOgNnSc4	první
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
60	[number]	k4	60
řádků	řádek	k1gInPc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
instalovány	instalovat	k5eAaBmNgFnP	instalovat
první	první	k4xOgFnPc1	první
satelitní	satelitní	k2eAgFnPc1d1	satelitní
antény	anténa	k1gFnPc1	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nové	nový	k2eAgFnSc3d1	nová
anténě	anténa	k1gFnSc3	anténa
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
věž	věž	k1gFnSc1	věž
výšky	výška	k1gFnSc2	výška
320,75	[number]	k4	320,75
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
instalována	instalován	k2eAgFnSc1d1	instalována
vysokofrekvenční	vysokofrekvenční	k2eAgFnSc1d1	vysokofrekvenční
anténa	anténa	k1gFnSc1	anténa
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
tím	ten	k3xDgNnSc7	ten
povyrostla	povyrůst	k5eAaPmAgFnS	povyrůst
na	na	k7c4	na
324	[number]	k4	324
m.	m.	k?	m.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
úpravy	úprava	k1gFnPc1	úprava
pro	pro	k7c4	pro
přechod	přechod	k1gInSc4	přechod
na	na	k7c4	na
digitální	digitální	k2eAgNnSc4d1	digitální
vysílání	vysílání	k1gNnSc4	vysílání
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
region	region	k1gInSc4	region
Ile-de-France	Ilee-France	k1gFnSc2	Ile-de-France
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
ze	z	k7c2	z
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Gustave	Gustav	k1gMnSc5	Gustav
Eiffel	Eiffel	k1gMnSc1	Eiffel
<g/>
,	,	kIx,	,
Édouard	Édouard	k1gMnSc1	Édouard
Lockroy	Lockroa	k1gFnSc2	Lockroa
a	a	k8xC	a
Eugè	Eugè	k1gFnSc2	Eugè
Poubelle	Poubelle	k1gFnSc2	Poubelle
<g/>
,	,	kIx,	,
zařídila	zařídit	k5eAaPmAgFnS	zařídit
provoz	provoz	k1gInSc4	provoz
věže	věž	k1gFnSc2	věž
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
od	od	k7c2	od
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
životnost	životnost	k1gFnSc1	životnost
stavby	stavba	k1gFnSc2	stavba
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
strategickému	strategický	k2eAgInSc3d1	strategický
významu	význam	k1gInSc3	význam
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
telegrafie	telegrafie	k1gFnSc2	telegrafie
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
bylo	být	k5eAaImAgNnS	být
ideální	ideální	k2eAgNnSc1d1	ideální
umístění	umístění	k1gNnSc1	umístění
vysílací	vysílací	k2eAgFnSc2d1	vysílací
antény	anténa	k1gFnSc2	anténa
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
17	[number]	k4	17
února	únor	k1gInSc2	únor
1981	[number]	k4	1981
bylo	být	k5eAaImAgNnS	být
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
schváleno	schválen	k2eAgNnSc4d1	schváleno
prodloužení	prodloužení	k1gNnSc4	prodloužení
koncese	koncese	k1gFnSc2	koncese
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
25	[number]	k4	25
let	léto	k1gNnPc2	léto
do	do	k7c2	do
Silvestra	Silvestr	k1gInSc2	Silvestr
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
díky	díky	k7c3	díky
vysílání	vysílání	k1gNnSc3	vysílání
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
turistické	turistický	k2eAgFnSc6d1	turistická
atraktivitě	atraktivita	k1gFnSc6	atraktivita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
provoz	provoz	k1gInSc4	provoz
věže	věž	k1gFnSc2	věž
organizace	organizace	k1gFnSc2	organizace
SETE	set	k1gInSc5	set
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
získala	získat	k5eAaPmAgFnS	získat
kontrakt	kontrakt	k1gInSc4	kontrakt
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stoprocentním	stoprocentní	k2eAgMnSc7d1	stoprocentní
vlastníkem	vlastník	k1gMnSc7	vlastník
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
pozemku	pozemek	k1gInSc6	pozemek
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
strany	strana	k1gFnSc2	strana
125	[number]	k4	125
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
324	[number]	k4	324
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
33	[number]	k4	33
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
stavby	stavba	k1gFnSc2	stavba
tvoří	tvořit	k5eAaImIp3nP	tvořit
4	[number]	k4	4
betonové	betonový	k2eAgInPc1d1	betonový
bloky	blok	k1gInPc1	blok
<g/>
.	.	kIx.	.
2	[number]	k4	2
bloky	blok	k1gInPc7	blok
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
2	[number]	k4	2
m	m	kA	m
silného	silný	k2eAgInSc2d1	silný
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
7	[number]	k4	7
<g/>
metrové	metrový	k2eAgFnSc6d1	metrová
vrstvě	vrstva	k1gFnSc6	vrstva
štěrku	štěrk	k1gInSc2	štěrk
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
dva	dva	k4xCgInPc1	dva
bloky	blok	k1gInPc1	blok
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Seině	Seina	k1gFnSc6	Seina
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
prosakující	prosakující	k2eAgFnSc3d1	prosakující
vodě	voda	k1gFnSc3	voda
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
postaveny	postavit	k5eAaPmNgInP	postavit
pomocí	pomoc	k1gFnSc7	pomoc
ocelových	ocelový	k2eAgInPc2d1	ocelový
kesonů	keson	k1gInPc2	keson
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
byl	být	k5eAaImAgInS	být
stlačen	stlačen	k2eAgInSc1d1	stlačen
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
beton	beton	k1gInSc1	beton
mohl	moct	k5eAaImAgInS	moct
ztvrdnout	ztvrdnout	k5eAaPmF	ztvrdnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
betonových	betonový	k2eAgInPc6d1	betonový
kvádrech	kvádr	k1gInPc6	kvádr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
16	[number]	k4	16
menších	malý	k2eAgInPc2d2	menší
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svírají	svírat	k5eAaImIp3nP	svírat
stavbu	stavba	k1gFnSc4	stavba
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nerozjela	rozjet	k5eNaPmAgFnS	rozjet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
bloků	blok	k1gInPc2	blok
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
54	[number]	k4	54
stupňů	stupeň	k1gInPc2	stupeň
ocelová	ocelový	k2eAgFnSc1d1	ocelová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
ukotvena	ukotven	k2eAgMnSc4d1	ukotven
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedmimetrovými	sedmimetrový	k2eAgInPc7d1	sedmimetrový
šrouby	šroub	k1gInPc7	šroub
<g/>
.	.	kIx.	.
</s>
<s>
Oblouky	oblouk	k1gInPc1	oblouk
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
74	[number]	k4	74
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
oblouku	oblouk	k1gInSc2	oblouk
je	být	k5eAaImIp3nS	být
39	[number]	k4	39
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pilíře	pilíř	k1gInPc1	pilíř
-	-	kIx~	-
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
pilířích	pilíř	k1gInPc6	pilíř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
lanovka	lanovka	k1gFnSc1	lanovka
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
57	[number]	k4	57
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
360	[number]	k4	360
schodů	schod	k1gInPc2	schod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
4	[number]	k4	4
200	[number]	k4	200
m	m	kA	m
<g/>
2	[number]	k4	2
může	moct	k5eAaImIp3nS	moct
pojmout	pojmout	k5eAaPmF	pojmout
až	až	k9	až
3000	[number]	k4	3000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
restaurace	restaurace	k1gFnSc1	restaurace
58	[number]	k4	58
Tour	Tour	k1gMnSc1	Tour
Eiffel	Eiffel	k1gMnSc1	Eiffel
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc1d1	krásný
panoramatický	panoramatický	k2eAgInSc1d1	panoramatický
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
vnitřek	vnitřek	k1gInSc4	vnitřek
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
,	,	kIx,	,
bufet	bufet	k1gNnSc1	bufet
<g/>
,	,	kIx,	,
kino	kino	k1gNnSc1	kino
uvádějící	uvádějící	k2eAgInPc1d1	uvádějící
filmy	film	k1gInPc1	film
o	o	k7c6	o
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
směnárna	směnárna	k1gFnSc1	směnárna
a	a	k8xC	a
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
prodává	prodávat	k5eAaImIp3nS	prodávat
poštovní	poštovní	k2eAgFnPc4d1	poštovní
známky	známka	k1gFnPc4	známka
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
La	la	k1gNnSc7	la
Tour	Tour	k1gMnSc1	Tour
Eiffel	Eiffel	k1gMnSc1	Eiffel
-	-	kIx~	-
Paris	Paris	k1gMnSc1	Paris
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
patra	patro	k1gNnSc2	patro
se	se	k3xPyFc4	se
dostaneme	dostat	k5eAaPmIp1nP	dostat
jak	jak	k6eAd1	jak
po	po	k7c6	po
schodišti	schodiště	k1gNnSc6	schodiště
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
třemi	tři	k4xCgInPc7	tři
výtahy	výtah	k1gInPc7	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
výtahy	výtah	k1gInPc1	výtah
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
firmou	firma	k1gFnSc7	firma
Combaluzier	Combaluzira	k1gFnPc2	Combaluzira
Lepape	Lepap	k1gInSc5	Lepap
Roux	Roux	k1gInSc1	Roux
a	a	k8xC	a
americkou	americký	k2eAgFnSc7d1	americká
firmou	firma	k1gFnSc7	firma
Otis	Otisa	k1gFnPc2	Otisa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
výtazích	výtah	k1gInPc6	výtah
byly	být	k5eAaImAgFnP	být
instalovány	instalovat	k5eAaBmNgFnP	instalovat
sedačky	sedačka	k1gFnPc1	sedačka
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
pár	pár	k4xCyI	pár
minutovou	minutový	k2eAgFnSc4d1	minutová
jízdu	jízda	k1gFnSc4	jízda
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
patře	patro	k1gNnSc6	patro
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nová	nový	k2eAgFnSc1d1	nová
prosklená	prosklený	k2eAgFnSc1d1	prosklená
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
115	[number]	k4	115
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
359	[number]	k4	359
schodů	schod	k1gInPc2	schod
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
plochu	plocha	k1gFnSc4	plocha
1	[number]	k4	1
650	[number]	k4	650
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
nápor	nápor	k1gInSc4	nápor
až	až	k9	až
1	[number]	k4	1
600	[number]	k4	600
lidí	člověk	k1gMnPc2	člověk
naráz	naráz	k6eAd1	naráz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
výhled	výhled	k1gInSc4	výhled
i	i	k8xC	i
70	[number]	k4	70
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
proslulá	proslulý	k2eAgFnSc1d1	proslulá
restaurace	restaurace	k1gFnSc1	restaurace
Le	Le	k1gMnSc1	Le
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
asi	asi	k9	asi
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dostaneme	dostat	k5eAaPmIp1nP	dostat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
výtahy	výtah	k1gInPc4	výtah
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pěšky	pěšky	k6eAd1	pěšky
po	po	k7c6	po
schodišti	schodiště	k1gNnSc6	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
275	[number]	k4	275
m	m	kA	m
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
unese	unést	k5eAaPmIp3nS	unést
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
350	[number]	k4	350
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
patra	patro	k1gNnSc2	patro
se	se	k3xPyFc4	se
široká	široký	k2eAgFnSc1d1	široká
veřejnost	veřejnost	k1gFnSc1	veřejnost
dostane	dostat	k5eAaPmIp3nS	dostat
pouze	pouze	k6eAd1	pouze
výtahem	výtah	k1gInSc7	výtah
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
je	být	k5eAaImIp3nS	být
schodiště	schodiště	k1gNnSc1	schodiště
vedoucí	vedoucí	k1gFnSc2	vedoucí
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
patra	patro	k1gNnSc2	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vysílač	vysílač	k1gInSc1	vysílač
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nainstalován	nainstalovat	k5eAaPmNgInS	nainstalovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
signál	signál	k1gInSc4	signál
asi	asi	k9	asi
pro	pro	k7c4	pro
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Oscilace	oscilace	k1gFnSc1	oscilace
vrchního	vrchní	k2eAgNnSc2d1	vrchní
patra	patro	k1gNnSc2	patro
při	při	k7c6	při
silném	silný	k2eAgInSc6d1	silný
větru	vítr	k1gInSc6	vítr
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
jasných	jasný	k2eAgInPc6d1	jasný
a	a	k8xC	a
horkých	horký	k2eAgInPc6d1	horký
dnech	den	k1gInPc6	den
může	moct	k5eAaImIp3nS	moct
věž	věž	k1gFnSc4	věž
vyrůst	vyrůst	k5eAaPmF	vyrůst
díky	díky	k7c3	díky
roztažnosti	roztažnost	k1gFnSc3	roztažnost
kovů	kov	k1gInPc2	kov
až	až	k6eAd1	až
o	o	k7c4	o
18	[number]	k4	18
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
restaurace	restaurace	k1gFnSc1	restaurace
s	s	k7c7	s
názvem	název	k1gInSc7	název
58	[number]	k4	58
Tour	Toura	k1gFnPc2	Toura
Eiffel	Eiffel	k1gInSc4	Eiffel
nabízí	nabízet	k5eAaImIp3nS	nabízet
panoramatický	panoramatický	k2eAgInSc4d1	panoramatický
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
Le	Le	k1gFnSc2	Le
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dokonce	dokonce	k9	dokonce
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
soukromý	soukromý	k2eAgInSc4d1	soukromý
výtah	výtah	k1gInSc4	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
restaurace	restaurace	k1gFnSc1	restaurace
obdržela	obdržet	k5eAaPmAgFnS	obdržet
slavnou	slavný	k2eAgFnSc4d1	slavná
hvězdu	hvězda	k1gFnSc4	hvězda
od	od	k7c2	od
Průvodce	průvodce	k1gMnSc4	průvodce
Michelin	Michelin	k2eAgInSc1d1	Michelin
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
90	[number]	k4	90
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
30	[number]	k4	30
kuchařů	kuchař	k1gMnPc2	kuchař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začala	začít	k5eAaPmAgFnS	začít
rapidně	rapidně	k6eAd1	rapidně
narůstat	narůstat	k5eAaImF	narůstat
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
věž	věž	k1gFnSc1	věž
asi	asi	k9	asi
milion	milion	k4xCgInSc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
dokonce	dokonce	k9	dokonce
5,5	[number]	k4	5,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
věž	věž	k1gFnSc1	věž
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
symbolů	symbol	k1gInPc2	symbol
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stavby	stavba	k1gFnSc2	stavba
začaly	začít	k5eAaPmAgInP	začít
probíhat	probíhat	k5eAaImF	probíhat
koncerty	koncert	k1gInPc1	koncert
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
odehrávajícím	odehrávající	k2eAgInSc6d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
už	už	k6eAd1	už
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
běžně	běžně	k6eAd1	běžně
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
věž	věž	k1gFnSc1	věž
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
zrenovovat	zrenovovat	k5eAaPmF	zrenovovat
výtahy	výtah	k1gInPc4	výtah
<g/>
,	,	kIx,	,
instalovat	instalovat	k5eAaBmF	instalovat
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
věž	věž	k1gFnSc4	věž
pro	pro	k7c4	pro
obrovský	obrovský	k2eAgInSc4d1	obrovský
nápor	nápor	k1gInSc4	nápor
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
dokončení	dokončení	k1gNnSc2	dokončení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
už	už	k6eAd1	už
věž	věž	k1gFnSc4	věž
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
stavbu	stavba	k1gFnSc4	stavba
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
6	[number]	k4	6
719	[number]	k4	719
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
číslu	číslo	k1gNnSc3	číslo
se	se	k3xPyFc4	se
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgFnPc2d3	nejnavštěvovanější
památek	památka	k1gFnPc2	památka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
dokončení	dokončení	k1gNnSc2	dokončení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
přerostl	přerůst	k5eAaPmAgInS	přerůst
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc1	Building
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
městě	město	k1gNnSc6	město
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
překročily	překročit	k5eAaPmAgFnP	překročit
výšku	výška	k1gFnSc4	výška
150	[number]	k4	150
m.	m.	k?	m.
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
stavby	stavba	k1gFnPc1	stavba
katedrála	katedrál	k1gMnSc2	katedrál
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Rouenu	Roueno	k1gNnSc6	Roueno
(	(	kIx(	(
<g/>
150	[number]	k4	150
m	m	kA	m
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
katedrála	katedrál	k1gMnSc2	katedrál
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
(	(	kIx(	(
<g/>
169	[number]	k4	169
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Washingtonův	Washingtonův	k2eAgInSc1d1	Washingtonův
památník	památník	k1gInSc1	památník
(	(	kIx(	(
<g/>
170	[number]	k4	170
m	m	kA	m
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
tak	tak	k6eAd1	tak
překonala	překonat	k5eAaPmAgFnS	překonat
tyto	tento	k3xDgFnPc4	tento
stavby	stavba	k1gFnPc4	stavba
skoro	skoro	k6eAd1	skoro
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
drží	držet	k5eAaImIp3nS	držet
prvenství	prvenství	k1gNnSc1	prvenství
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Burdž	Burdž	k1gFnSc1	Burdž
Chalífa	chalífa	k1gMnSc1	chalífa
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
828	[number]	k4	828
m	m	kA	m
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
postavenou	postavený	k2eAgFnSc4d1	postavená
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgNnSc1d1	předchozí
prvenství	prvenství	k1gNnSc1	prvenství
náleželo	náležet	k5eAaImAgNnS	náležet
polskému	polský	k2eAgInSc3d1	polský
vysílači	vysílač	k1gInSc3	vysílač
Konstantynow	Konstantynow	k1gFnSc2	Konstantynow
<g/>
.	.	kIx.	.
</s>
<s>
Měřil	měřit	k5eAaImAgMnS	měřit
646	[number]	k4	646
<g/>
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
ocelový	ocelový	k2eAgInSc4d1	ocelový
stožár	stožár	k1gInSc4	stožár
se	se	k3xPyFc4	se
ale	ale	k9	ale
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
zřítil	zřítit	k5eAaPmAgMnS	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
stožár	stožár	k1gInSc1	stožár
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Dakotě	Dakota	k1gFnSc6	Dakota
(	(	kIx(	(
<g/>
629	[number]	k4	629
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Taipei	Taipei	k1gNnSc1	Taipei
101	[number]	k4	101
s	s	k7c7	s
508	[number]	k4	508
metry	metro	k1gNnPc7	metro
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Willis	Willis	k1gFnPc2	Willis
Tower	Towra	k1gFnPc2	Towra
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Sears	Sears	k1gInSc1	Sears
Tower	Tower	k1gInSc1	Tower
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
527,3	[number]	k4	527,3
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
bod	bod	k1gInSc4	bod
se	se	k3xPyFc4	se
ale	ale	k9	ale
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
antény	anténa	k1gFnSc2	anténa
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgNnP	být
přimontována	přimontovat	k5eAaPmNgNnP	přimontovat
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
oficiálně	oficiálně	k6eAd1	oficiálně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
výška	výška	k1gFnSc1	výška
442	[number]	k4	442
m	m	kA	m
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výška	výška	k1gFnSc1	výška
střechy	střecha	k1gFnSc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
120	[number]	k4	120
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
člověkem	člověk	k1gMnSc7	člověk
postavené	postavený	k2eAgFnSc2d1	postavená
konstrukce	konstrukce	k1gFnSc2	konstrukce
2,7	[number]	k4	2,7
<g/>
x	x	k?	x
znásobila	znásobit	k5eAaPmAgFnS	znásobit
(	(	kIx(	(
<g/>
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
má	mít	k5eAaImIp3nS	mít
300	[number]	k4	300
m	m	kA	m
a	a	k8xC	a
Burdž	Burdž	k1gFnSc4	Burdž
Chalífa	chalífa	k1gMnSc1	chalífa
má	mít	k5eAaImIp3nS	mít
828	[number]	k4	828
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
nebyla	být	k5eNaImAgFnS	být
prvním	první	k4xOgNnSc7	první
návrhem	návrh	k1gInSc7	návrh
takto	takto	k6eAd1	takto
vysoké	vysoký	k2eAgFnSc2d1	vysoká
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
Angličan	Angličan	k1gMnSc1	Angličan
Richard	Richard	k1gMnSc1	Richard
Trevithick	Trevithick	k1gMnSc1	Trevithick
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
železnou	železný	k2eAgFnSc4d1	železná
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tyčila	tyčit	k5eAaImAgFnS	tyčit
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
304	[number]	k4	304
<g/>
m.	m.	k?	m.
Američané	Američan	k1gMnPc1	Američan
Clarke	Clarke	k1gNnSc2	Clarke
a	a	k8xC	a
Reeves	Reevesa	k1gFnPc2	Reevesa
představili	představit	k5eAaPmAgMnP	představit
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
věž	věž	k1gFnSc4	věž
vysokou	vysoký	k2eAgFnSc4d1	vysoká
1000	[number]	k4	1000
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
asi	asi	k9	asi
300	[number]	k4	300
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouz	Francouz	k1gMnSc1	Francouz
Jules	Jules	k1gMnSc1	Jules
Bourdais	Bourdais	k1gInSc4	Bourdais
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
žulovou	žulový	k2eAgFnSc4d1	Žulová
300	[number]	k4	300
metrovou	metrový	k2eAgFnSc4d1	metrová
věž	věž	k1gFnSc4	věž
začátkem	začátkem	k7c2	začátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
projektů	projekt	k1gInPc2	projekt
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
návrh	návrh	k1gInSc1	návrh
Gustava	Gustav	k1gMnSc2	Gustav
Eiffela	Eiffel	k1gMnSc2	Eiffel
spatřil	spatřit	k5eAaPmAgMnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
dostavbě	dostavba	k1gFnSc6	dostavba
se	se	k3xPyFc4	se
všude	všude	k6eAd1	všude
po	po	k7c6	po
světě	svět	k1gInSc6	svět
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
projekty	projekt	k1gInPc1	projekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
překonávaly	překonávat	k5eAaImAgFnP	překonávat
výšku	výška	k1gFnSc4	výška
300	[number]	k4	300
m.	m.	k?	m.
Například	například	k6eAd1	například
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
svou	svůj	k3xOyFgFnSc4	svůj
hrdost	hrdost	k1gFnSc4	hrdost
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
prezentoval	prezentovat	k5eAaBmAgInS	prezentovat
její	její	k3xOp3gFnSc4	její
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
konkurz	konkurz	k1gInSc4	konkurz
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
překonala	překonat	k5eAaPmAgFnS	překonat
360	[number]	k4	360
<g/>
m.	m.	k?	m.
Avšak	avšak	k8xC	avšak
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
také	také	k9	také
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnSc1	první
replika	replika	k1gFnSc1	replika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podobala	podobat	k5eAaImAgFnS	podobat
z	z	k7c2	z
části	část	k1gFnSc2	část
Eiffelovce	Eiffelovka	k1gFnSc3	Eiffelovka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
další	další	k2eAgFnSc1d1	další
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
menší	malý	k2eAgFnSc1d2	menší
napodobenina	napodobenina	k1gFnSc1	napodobenina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
světě	svět	k1gInSc6	svět
jen	jen	k9	jen
hemžilo	hemžit	k5eAaImAgNnS	hemžit
výstavbou	výstavba	k1gFnSc7	výstavba
podobných	podobný	k2eAgFnPc2d1	podobná
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Replika	replika	k1gFnSc1	replika
Eiffelovky	Eiffelovka	k1gFnSc2	Eiffelovka
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
hotelu	hotel	k1gInSc2	hotel
Paris	Paris	k1gMnSc1	Paris
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc4	Vegas
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
165	[number]	k4	165
m.	m.	k?	m.
Další	další	k2eAgInSc4d1	další
v	v	k7c4	v
Tchien-tu-čcheng	Tchienu-čcheng	k1gInSc4	Tchien-tu-čcheng
<g/>
,	,	kIx,	,
Chang-čou	Chang-čou	k1gNnSc4	Chang-čou
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
108	[number]	k4	108
m.	m.	k?	m.
Asi	asi	k9	asi
sto	sto	k4xCgNnSc4	sto
metrová	metrový	k2eAgFnSc1d1	metrová
napodobenina	napodobenina	k1gFnSc1	napodobenina
byla	být	k5eAaImAgFnS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
v	v	k7c6	v
zábavním	zábavní	k2eAgInSc6d1	zábavní
parku	park	k1gInSc6	park
Kings	Kings	k1gInSc1	Kings
Island	Island	k1gInSc1	Island
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
parku	park	k1gInSc6	park
Kings	Kingsa	k1gFnPc2	Kingsa
Dominion	dominion	k1gNnSc4	dominion
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
100	[number]	k4	100
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kopie	kopie	k1gFnSc1	kopie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Shenzhen	Shenzhna	k1gFnPc2	Shenzhna
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
kopií	kopie	k1gFnPc2	kopie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
...	...	k?	...
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
coby	coby	k?	coby
novodobý	novodobý	k2eAgInSc4d1	novodobý
symbol	symbol	k1gInSc4	symbol
Paříže	Paříž	k1gFnPc4	Paříž
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
pařížským	pařížský	k2eAgInSc7d1	pařížský
symbolem	symbol	k1gInSc7	symbol
byl	být	k5eAaImAgInS	být
kohout	kohout	k1gInSc1	kohout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vděčným	vděčný	k2eAgInSc7d1	vděčný
objektem	objekt	k1gInSc7	objekt
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
fotografy	fotograf	k1gMnPc4	fotograf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
filmaře	filmař	k1gMnPc4	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
hraných	hraný	k2eAgInPc6d1	hraný
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
