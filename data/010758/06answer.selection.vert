<s>
Válku	válka	k1gFnSc4	válka
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mocenská	mocenský	k2eAgFnSc1d1	mocenská
expanze	expanze	k1gFnSc1	expanze
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
obava	obava	k1gFnSc1	obava
o	o	k7c4	o
osud	osud	k1gInSc4	osud
"	"	kIx"	"
<g/>
nemocného	nemocný	k1gMnSc2	nemocný
muže	muž	k1gMnSc2	muž
na	na	k7c6	na
Bosporu	Bospor	k1gInSc6	Bospor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
byla	být	k5eAaImAgFnS	být
oslabená	oslabený	k2eAgFnSc1d1	oslabená
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
označována	označovat	k5eAaImNgFnS	označovat
<g/>
.	.	kIx.	.
</s>
