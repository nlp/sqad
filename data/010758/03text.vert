<p>
<s>
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
byl	být	k5eAaImAgInS	být
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
letech	let	k1gInPc6	let
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
mezi	mezi	k7c7	mezi
carským	carský	k2eAgNnSc7d1	carské
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
podpořenou	podpořený	k2eAgFnSc7d1	podpořená
koalicí	koalice	k1gFnSc7	koalice
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Sardinského	sardinský	k2eAgNnSc2d1	Sardinské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgInS	odehrávat
především	především	k6eAd1	především
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Sevastopolu	Sevastopol	k1gInSc6	Sevastopol
námořní	námořní	k2eAgFnSc1d1	námořní
základna	základna	k1gFnSc1	základna
ruské	ruský	k2eAgFnSc2d1	ruská
černomořské	černomořský	k2eAgFnSc2d1	černomořská
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mocenská	mocenský	k2eAgFnSc1d1	mocenská
expanze	expanze	k1gFnSc1	expanze
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
obava	obava	k1gFnSc1	obava
o	o	k7c4	o
osud	osud	k1gInSc4	osud
"	"	kIx"	"
<g/>
nemocného	nemocný	k1gMnSc2	nemocný
muže	muž	k1gMnSc2	muž
na	na	k7c6	na
Bosporu	Bospor	k1gInSc6	Bospor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
byla	být	k5eAaImAgFnS	být
oslabená	oslabený	k2eAgFnSc1d1	oslabená
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
označována	označovat	k5eAaImNgFnS	označovat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
porážka	porážka	k1gFnSc1	porážka
Ruska	Ruska	k1gFnSc1	Ruska
stvrzená	stvrzený	k2eAgFnSc1d1	stvrzená
Pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
mírovou	mírový	k2eAgFnSc7d1	mírová
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Začátek	začátek	k1gInSc1	začátek
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
konfliktem	konflikt	k1gInSc7	konflikt
mezi	mezi	k7c7	mezi
carským	carský	k2eAgNnSc7d1	carské
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
započetí	započetí	k1gNnSc3	započetí
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
překvapivě	překvapivě	k6eAd1	překvapivě
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
si	se	k3xPyFc3	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
odvykala	odvykat	k5eAaImAgFnS	odvykat
od	od	k7c2	od
nepřetržitých	přetržitý	k2eNgInPc2d1	nepřetržitý
válečných	válečný	k2eAgInPc2d1	válečný
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
prožívala	prožívat	k5eAaImAgFnS	prožívat
Průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Francie	Francie	k1gFnSc2	Francie
stál	stát	k5eAaImAgMnS	stát
prezident	prezident	k1gMnSc1	prezident
Ludvík	Ludvík	k1gMnSc1	Ludvík
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
úspěchu	úspěch	k1gInSc2	úspěch
na	na	k7c6	na
území	území	k1gNnSc6	území
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Osmanský	osmanský	k2eAgMnSc1d1	osmanský
sultán	sultán	k1gMnSc1	sultán
Abdülmecid	Abdülmecida	k1gFnPc2	Abdülmecida
I.	I.	kA	I.
předal	předat	k5eAaPmAgInS	předat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Francouzů	Francouz	k1gMnPc2	Francouz
správu	správa	k1gFnSc4	správa
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
chrámů	chrám	k1gInPc2	chrám
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
udělována	udělovat	k5eAaImNgFnS	udělovat
pravoslavným	pravoslavný	k2eAgMnPc3d1	pravoslavný
Rusům	Rus	k1gMnPc3	Rus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tím	ten	k3xDgMnSc7	ten
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
nanejvýš	nanejvýš	k6eAd1	nanejvýš
pobouřen	pobouřit	k5eAaPmNgInS	pobouřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osmanský	osmanský	k2eAgMnSc1d1	osmanský
sultán	sultán	k1gMnSc1	sultán
sice	sice	k8xC	sice
cara	car	k1gMnSc4	car
uklidňoval	uklidňovat	k5eAaImAgMnS	uklidňovat
a	a	k8xC	a
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
nedorozumění	nedorozumění	k1gNnSc4	nedorozumění
a	a	k8xC	a
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
napraví	napravit	k5eAaPmIp3nS	napravit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
už	už	k9	už
Abdülmecid	Abdülmecid	k1gInSc1	Abdülmecid
I.	I.	kA	I.
dávno	dávno	k6eAd1	dávno
vsadil	vsadit	k5eAaPmAgInS	vsadit
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
mocností	mocnost	k1gFnPc2	mocnost
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgInS	mít
tedy	tedy	k9	tedy
žádný	žádný	k3yNgInSc4	žádný
důvod	důvod	k1gInSc4	důvod
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
slibů	slib	k1gInPc2	slib
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
tak	tak	k6eAd1	tak
udržoval	udržovat	k5eAaImAgInS	udržovat
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
v	v	k7c6	v
nejistotě	nejistota	k1gFnSc6	nejistota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mikuláš	mikuláš	k1gInSc1	mikuláš
I.	I.	kA	I.
vyslal	vyslat	k5eAaPmAgInS	vyslat
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
misi	mise	k1gFnSc4	mise
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
knížete	kníže	k1gMnSc2	kníže
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Menšikova	Menšikův	k2eAgFnSc1d1	Menšikova
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k8xC	jako
legenda	legenda	k1gFnSc1	legenda
ruského	ruský	k2eAgNnSc2d1	ruské
válečnictví	válečnictví	k1gNnSc2	válečnictví
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vyhnání	vyhnání	k1gNnSc6	vyhnání
Napoleona	Napoleon	k1gMnSc2	Napoleon
I.	I.	kA	I.
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Rusku	Rusko	k1gNnSc6	Rusko
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
na	na	k7c4	na
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
záležitostí	záležitost	k1gFnPc2	záležitost
pravoslavného	pravoslavný	k2eAgNnSc2d1	pravoslavné
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Francii	Francie	k1gFnSc4	Francie
u	u	k7c2	u
katolických	katolický	k2eAgInPc2d1	katolický
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
však	však	k9	však
nikam	nikam	k6eAd1	nikam
nevedla	vést	k5eNaImAgFnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Menšikov	Menšikov	k1gInSc1	Menšikov
například	například	k6eAd1	například
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
sultánem	sultán	k1gMnSc7	sultán
urazil	urazit	k5eAaPmAgMnS	urazit
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
když	když	k8xS	když
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
civilu	civil	k1gMnSc3	civil
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
obvyklé	obvyklý	k2eAgFnSc6d1	obvyklá
vojenské	vojenský	k2eAgFnSc6d1	vojenská
uniformě	uniforma	k1gFnSc6	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
také	také	k6eAd1	také
ústupky	ústupek	k1gInPc4	ústupek
Turků	turek	k1gInPc2	turek
Francouzům	Francouz	k1gMnPc3	Francouz
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
výměnu	výměna	k1gFnSc4	výměna
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
osmanských	osmanský	k2eAgMnPc2d1	osmanský
státních	státní	k2eAgMnPc2d1	státní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
se	se	k3xPyFc4	se
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
jednání	jednání	k1gNnSc6	jednání
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
k	k	k7c3	k
drastičtějším	drastický	k2eAgNnPc3d2	drastičtější
řešením	řešení	k1gNnPc3	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
otázky	otázka	k1gFnSc2	otázka
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
car	car	k1gMnSc1	car
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turci	Turek	k1gMnPc1	Turek
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
nedobře	dobře	k6eNd1	dobře
zacházeli	zacházet	k5eAaImAgMnP	zacházet
s	s	k7c7	s
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
populací	populace	k1gFnSc7	populace
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
usilovali	usilovat	k5eAaImAgMnP	usilovat
Rusové	Rus	k1gMnPc1	Rus
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
přístup	přístup	k1gInSc4	přístup
z	z	k7c2	z
Černého	černé	k1gNnSc2	černé
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
přes	přes	k7c4	přes
Bospor	Bospor	k1gInSc4	Bospor
a	a	k8xC	a
Dardanely	Dardanely	k1gFnPc4	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
důvody	důvod	k1gInPc1	důvod
byly	být	k5eAaImAgInP	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
Mikulášova	Mikulášův	k2eAgNnPc4d1	Mikulášovo
carská	carský	k2eAgNnPc4d1	carské
vojska	vojsko	k1gNnPc4	vojsko
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Valašského	valašský	k2eAgNnSc2d1	Valašské
a	a	k8xC	a
Moldavského	moldavský	k2eAgNnSc2d1	moldavské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
osmanskými	osmanský	k2eAgMnPc7d1	osmanský
podunajskými	podunajský	k2eAgMnPc7d1	podunajský
vazaly	vazal	k1gMnPc7	vazal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Válka	válka	k1gFnSc1	válka
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Abdülmecid	Abdülmecid	k1gInSc1	Abdülmecid
I.	I.	kA	I.
zprvu	zprvu	k6eAd1	zprvu
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vyřešit	vyřešit	k5eAaPmF	vyřešit
spor	spor	k1gInSc4	spor
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Svolal	svolat	k5eAaPmAgMnS	svolat
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
kongres	kongres	k1gInSc4	kongres
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevyřešilo	vyřešit	k5eNaPmAgNnS	vyřešit
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
podporu	podpor	k1gInSc2	podpor
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gMnSc1	její
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
George	Georg	k1gMnSc2	Georg
Hamilton-Gordon	Hamilton-Gordon	k1gMnSc1	Hamilton-Gordon
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
důvodů	důvod	k1gInPc2	důvod
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
stvrdit	stvrdit	k5eAaPmF	stvrdit
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
spojenectví	spojenectví	k1gNnSc2	spojenectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
sultán	sultán	k1gMnSc1	sultán
do	do	k7c2	do
Valašska	Valašsko	k1gNnSc2	Valašsko
a	a	k8xC	a
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
ruskými	ruský	k2eAgMnPc7d1	ruský
šiky	šik	k1gInPc1	šik
odražena	odrazit	k5eAaPmNgFnS	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
sice	sice	k8xC	sice
vítězství	vítězství	k1gNnPc1	vítězství
u	u	k7c2	u
města	město	k1gNnSc2	město
Olteniţa	Olteniţ	k1gInSc2	Olteniţ
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInPc4d1	počáteční
neúspěchy	neúspěch	k1gInPc4	neúspěch
dobyli	dobýt	k5eAaPmAgMnP	dobýt
přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
Silistra	Silistrum	k1gNnSc2	Silistrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
znovu	znovu	k6eAd1	znovu
získat	získat	k5eAaPmF	získat
Rusy	Rus	k1gMnPc4	Rus
obsazené	obsazený	k2eAgFnSc2d1	obsazená
valašské	valašský	k2eAgFnSc2d1	Valašská
město	město	k1gNnSc1	město
Cetate	Cetat	k1gInSc5	Cetat
<g/>
.	.	kIx.	.
</s>
<s>
Abdülmecid	Abdülmecid	k1gInSc1	Abdülmecid
se	se	k3xPyFc4	se
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
a	a	k8xC	a
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
začal	začít	k5eAaPmAgInS	začít
vážně	vážně	k6eAd1	vážně
obávat	obávat	k5eAaImF	obávat
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
navíc	navíc	k6eAd1	navíc
ruská	ruský	k2eAgFnSc1d1	ruská
válečná	válečný	k2eAgFnSc1d1	válečná
flotila	flotila	k1gFnSc1	flotila
napadla	napadnout	k5eAaPmAgFnS	napadnout
černomořský	černomořský	k2eAgInSc4d1	černomořský
přístav	přístav	k1gInSc4	přístav
Sinop	Sinop	k1gInSc1	Sinop
a	a	k8xC	a
ve	v	k7c6	v
snadném	snadný	k2eAgInSc6d1	snadný
boji	boj	k1gInSc6	boj
ho	on	k3xPp3gMnSc4	on
vyrvala	vyrvat	k5eAaPmAgFnS	vyrvat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Turků	Turek	k1gMnPc2	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
to	ten	k3xDgNnSc4	ten
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
velký	velký	k2eAgInSc4d1	velký
válečný	válečný	k2eAgInSc4d1	válečný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
právě	právě	k9	právě
dobytí	dobytí	k1gNnSc4	dobytí
Sinopu	Sinopa	k1gFnSc4	Sinopa
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k9	nakonec
zlomilo	zlomit	k5eAaPmAgNnS	zlomit
vaz	vaz	k1gInSc4	vaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abdülmecid	Abdülmecid	k1gInSc4	Abdülmecid
I.	I.	kA	I.
kvůli	kvůli	k7c3	kvůli
přímému	přímý	k2eAgNnSc3d1	přímé
ohrožení	ohrožení	k1gNnSc3	ohrožení
Istanbulu	Istanbul	k1gInSc2	Istanbul
požádal	požádat	k5eAaPmAgMnS	požádat
francouzského	francouzský	k2eAgMnSc4d1	francouzský
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
britského	britský	k2eAgMnSc4d1	britský
premiéra	premiér	k1gMnSc4	premiér
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mu	on	k3xPp3gNnSc3	on
vyhověli	vyhovět	k5eAaPmAgMnP	vyhovět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byla	být	k5eAaImAgFnS	být
děsivá	děsivý	k2eAgFnSc1d1	děsivá
vize	vize	k1gFnSc1	vize
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
úžina	úžina	k1gFnSc1	úžina
Bospor	Bospor	k1gInSc4	Bospor
a	a	k8xC	a
Dardanely	Dardanely	k1gFnPc4	Dardanely
padla	padnout	k5eAaImAgFnS	padnout
do	do	k7c2	do
carových	carův	k2eAgFnPc2d1	carova
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
posíleno	posílit	k5eAaPmNgNnS	posílit
územími	území	k1gNnPc7	území
rozpadající	rozpadající	k2eAgFnSc7d1	rozpadající
se	se	k3xPyFc4	se
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
byli	být	k5eAaImAgMnP	být
motivováni	motivovat	k5eAaBmNgMnP	motivovat
i	i	k9	i
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
a	a	k8xC	a
obchodními	obchodní	k2eAgFnPc7d1	obchodní
neshodami	neshoda	k1gFnPc7	neshoda
s	s	k7c7	s
Rusy	Rus	k1gMnPc7	Rus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1853	[number]	k4	1853
a	a	k8xC	a
1854	[number]	k4	1854
obě	dva	k4xCgFnPc1	dva
velmoci	velmoc	k1gFnPc1	velmoc
vyslaly	vyslat	k5eAaPmAgFnP	vyslat
do	do	k7c2	do
Černého	černé	k1gNnSc2	černé
moře	moře	k1gNnSc2	moře
své	svůj	k3xOyFgFnSc2	svůj
flotily	flotila	k1gFnSc2	flotila
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohly	pomoct	k5eAaPmAgFnP	pomoct
bránit	bránit	k5eAaImF	bránit
turecké	turecký	k2eAgFnPc4d1	turecká
pobřeží	pobřeží	k1gNnPc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Posíleny	posílen	k2eAgFnPc1d1	posílena
ještě	ještě	k9	ještě
o	o	k7c4	o
několik	několik	k4yIc4	několik
málo	málo	k6eAd1	málo
osmanských	osmanský	k2eAgFnPc2d1	Osmanská
lodí	loď	k1gFnPc2	loď
pak	pak	k6eAd1	pak
vypluly	vyplout	k5eAaPmAgFnP	vyplout
z	z	k7c2	z
Istanbulu	Istanbul	k1gInSc2	Istanbul
a	a	k8xC	a
napadly	napadnout	k5eAaPmAgFnP	napadnout
přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
centrem	centrum	k1gNnSc7	centrum
administrativy	administrativa	k1gFnSc2	administrativa
ruského	ruský	k2eAgNnSc2d1	ruské
černomořského	černomořský	k2eAgNnSc2d1	černomořské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oděsa	Oděsa	k1gFnSc1	Oděsa
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
palbě	palba	k1gFnSc3	palba
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nasazením	nasazení	k1gNnSc7	nasazení
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
nakonec	nakonec	k6eAd1	nakonec
těžké	těžký	k2eAgInPc4d1	těžký
boje	boj	k1gInPc4	boj
přečkala	přečkat	k5eAaPmAgFnS	přečkat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
padla	padnout	k5eAaPmAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
dopustili	dopustit	k5eAaPmAgMnP	dopustit
zásadních	zásadní	k2eAgFnPc2d1	zásadní
strategických	strategický	k2eAgFnPc2d1	strategická
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
neopatrnost	neopatrnost	k1gFnSc1	neopatrnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
pozice	pozice	k1gFnPc4	pozice
na	na	k7c4	na
území	území	k1gNnSc4	území
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Dobrudži	Dobrudža	k1gFnSc6	Dobrudža
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
Bukovině	Bukovina	k1gFnSc6	Bukovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přirozeně	přirozeně	k6eAd1	přirozeně
nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
nadšen	nadchnout	k5eAaPmNgInS	nadchnout
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
kromě	kromě	k7c2	kromě
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Německého	německý	k2eAgInSc2d1	německý
spolku	spolek	k1gInSc2	spolek
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
i	i	k9	i
Prusko	Prusko	k1gNnSc1	Prusko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Vilémem	Vilém	k1gMnSc7	Vilém
IV	IV	kA	IV
<g/>
..	..	k?	..
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
dovršení	dovršení	k1gNnSc3	dovršení
všeho	všecek	k3xTgNnSc2	všecek
se	se	k3xPyFc4	se
ke	k	k7c3	k
spojencům	spojenec	k1gMnPc3	spojenec
přidala	přidat	k5eAaPmAgFnS	přidat
i	i	k9	i
Sardinie	Sardinie	k1gFnSc1	Sardinie
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Savojců	Savojec	k1gMnPc2	Savojec
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Viktora	Viktora	k1gMnSc1	Viktora
Emanuela	Emanuel	k1gMnSc2	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rázem	rázem	k6eAd1	rázem
se	se	k3xPyFc4	se
tak	tak	k9	tak
celá	celý	k2eAgFnSc1d1	celá
situace	situace	k1gFnSc1	situace
obrátila	obrátit	k5eAaPmAgFnS	obrátit
proti	proti	k7c3	proti
carovi	car	k1gMnSc3	car
<g/>
.	.	kIx.	.
</s>
<s>
Stála	stát	k5eAaImAgFnS	stát
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInSc1d1	další
průběh	průběh	k1gInSc1	průběh
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
po	po	k7c6	po
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
Oděsu	Oděsa	k1gFnSc4	Oděsa
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
poloostrova	poloostrov	k1gInSc2	poloostrov
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
sem	sem	k6eAd1	sem
museli	muset	k5eAaImAgMnP	muset
rychle	rychle	k6eAd1	rychle
přesunout	přesunout	k5eAaPmF	přesunout
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
nařídil	nařídit	k5eAaPmAgMnS	nařídit
ustoupit	ustoupit	k5eAaPmF	ustoupit
vojskům	vojsko	k1gNnPc3	vojsko
z	z	k7c2	z
podunajských	podunajský	k2eAgNnPc2d1	podunajské
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
padla	padnout	k5eAaPmAgNnP	padnout
v	v	k7c6	v
nastalém	nastalý	k2eAgInSc6d1	nastalý
zmatku	zmatek	k1gInSc6	zmatek
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Rakušanů	Rakušan	k1gMnPc2	Rakušan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
špatně	špatně	k6eAd1	špatně
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
sice	sice	k8xC	sice
byl	být	k5eAaImAgInS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezachoval	zachovat	k5eNaPmAgMnS	zachovat
se	se	k3xPyFc4	se
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
velitele	velitel	k1gMnSc4	velitel
totiž	totiž	k9	totiž
zvolil	zvolit	k5eAaPmAgInS	zvolit
knížete	kníže	k1gNnSc4wR	kníže
Menšikova	Menšikův	k2eAgInSc2d1	Menšikův
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgInS	dostat
šanci	šance	k1gFnSc4	šance
napravit	napravit	k5eAaPmF	napravit
svou	svůj	k3xOyFgFnSc4	svůj
pověst	pověst	k1gFnSc4	pověst
po	po	k7c6	po
fiasku	fiasko	k1gNnSc6	fiasko
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
suchozemském	suchozemský	k2eAgInSc6d1	suchozemský
střetu	střet	k1gInSc6	střet
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1854	[number]	k4	1854
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Almě	alma	k1gFnSc6	alma
<g/>
,	,	kIx,	,
ztratil	ztratit	k5eAaPmAgInS	ztratit
Menšikov	Menšikov	k1gInSc1	Menšikov
téměř	téměř	k6eAd1	téměř
6000	[number]	k4	6000
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
33	[number]	k4	33
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
špatný	špatný	k2eAgInSc4d1	špatný
stav	stav	k1gInSc4	stav
britského	britský	k2eAgNnSc2d1	Britské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vítězná	vítězný	k2eAgFnSc1d1	vítězná
koalice	koalice	k1gFnSc1	koalice
obléhá	obléhat	k5eAaImIp3nS	obléhat
ruské	ruský	k2eAgNnSc1d1	ruské
opevněné	opevněný	k2eAgNnSc1d1	opevněné
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
dny	dna	k1gFnSc2	dna
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
hrozivé	hrozivý	k2eAgNnSc1d1	hrozivé
palbě	palba	k1gFnSc6	palba
nepřátelských	přátelský	k2eNgNnPc2d1	nepřátelské
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
prolomení	prolomení	k1gNnSc4	prolomení
obléhání	obléhání	k1gNnSc2	obléhání
vedla	vést	k5eAaImAgFnS	vést
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Balaklavy	Balaklava	k1gFnSc2	Balaklava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
následující	následující	k2eAgFnSc2d1	následující
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Inkermanu	Inkerman	k1gInSc2	Inkerman
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
skončila	skončit	k5eAaPmAgFnS	skončit
nerozhodně	rozhodně	k6eNd1	rozhodně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Inkermanu	Inkerman	k1gInSc2	Inkerman
byli	být	k5eAaImAgMnP	být
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
opět	opět	k6eAd1	opět
vedl	vést	k5eAaImAgMnS	vést
kníže	kníže	k1gMnSc1	kníže
Menšikov	Menšikov	k1gInSc4	Menšikov
<g/>
,	,	kIx,	,
drtivě	drtivě	k6eAd1	drtivě
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
významná	významný	k2eAgFnSc1d1	významná
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mohl	moct	k5eAaImAgMnS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
strhla	strhnout	k5eAaPmAgFnS	strhnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
u	u	k7c2	u
města	město	k1gNnSc2	město
Eutropia	Eutropium	k1gNnSc2	Eutropium
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
poražen	porazit	k5eAaPmNgInS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
války	válka	k1gFnSc2	válka
za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
vedlo	vést	k5eAaImAgNnS	vést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snad	snad	k9	snad
i	i	k9	i
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zmírnit	zmírnit	k5eAaPmF	zmírnit
krutou	krutý	k2eAgFnSc4d1	krutá
situaci	situace	k1gFnSc4	situace
obleženého	obležený	k2eAgInSc2d1	obležený
Sevastopolu	Sevastopol	k1gInSc2	Sevastopol
vyslal	vyslat	k5eAaPmAgMnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Kars	Kars	k1gInSc4	Kars
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
pevnost	pevnost	k1gFnSc1	pevnost
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
trvalo	trvat	k5eAaImAgNnS	trvat
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
Sevastopolu	Sevastopol	k1gInSc2	Sevastopol
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
třikrát	třikrát	k6eAd1	třikrát
pokusili	pokusit	k5eAaPmAgMnP	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
také	také	k9	také
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Taganrog	Taganroga	k1gFnPc2	Taganroga
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
stálo	stát	k5eAaImAgNnS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Rostovu	Rostův	k2eAgInSc3d1	Rostův
na	na	k7c6	na
Donu	Don	k1gMnSc6	Don
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgNnSc7d1	významné
centrem	centrum	k1gNnSc7	centrum
ruských	ruský	k2eAgFnPc2d1	ruská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Rusům	Rus	k1gMnPc3	Rus
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
útoky	útok	k1gInPc4	útok
ustát	ustát	k5eAaPmF	ustát
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
bylo	být	k5eAaImAgNnS	být
obležené	obležený	k2eAgNnSc1d1	obležené
i	i	k8xC	i
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gFnSc2	Petropavlovsk-Kamčatskij
na	na	k7c6	na
Kamčatce	Kamčatka	k1gFnSc6	Kamčatka
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
ale	ale	k9	ale
byli	být	k5eAaImAgMnP	být
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
donuceni	donucen	k2eAgMnPc1d1	donucen
se	se	k3xPyFc4	se
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
i	i	k9	i
z	z	k7c2	z
ruských	ruský	k2eAgFnPc2d1	ruská
vod	voda	k1gFnPc2	voda
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
krutým	krutý	k2eAgFnPc3d1	krutá
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obléhatelům	obléhatel	k1gMnPc3	obléhatel
Sevastopolu	Sevastopol	k1gInSc3	Sevastopol
připravilo	připravit	k5eAaPmAgNnS	připravit
tamější	tamější	k2eAgNnSc1d1	tamější
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
kapituluje	kapitulovat	k5eAaBmIp3nS	kapitulovat
město	město	k1gNnSc1	město
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Kapitulaci	kapitulace	k1gFnSc4	kapitulace
předcházela	předcházet	k5eAaImAgFnS	předcházet
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Čorna	Čorn	k1gInSc2	Čorn
a	a	k8xC	a
spojenecký	spojenecký	k2eAgInSc4d1	spojenecký
útok	útok	k1gInSc4	útok
na	na	k7c4	na
pevnůstku	pevnůstka	k1gFnSc4	pevnůstka
Malakoff	Malakoff	k1gInSc1	Malakoff
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
koalice	koalice	k1gFnSc1	koalice
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
definitivního	definitivní	k2eAgNnSc2d1	definitivní
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
==	==	k?	==
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyslala	vyslat	k5eAaPmAgFnS	vyslat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
války	válka	k1gFnSc2	válka
než	než	k8xS	než
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
válku	válka	k1gFnSc4	válka
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
ustaly	ustat	k5eAaPmAgInP	ustat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1856	[number]	k4	1856
je	být	k5eAaImIp3nS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
Pařížského	pařížský	k2eAgInSc2d1	pařížský
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vzdávají	vzdávat	k5eAaImIp3nP	vzdávat
Karsu	Kars	k1gInSc3	Kars
a	a	k8xC	a
Budžaku	Budžak	k1gInSc3	Budžak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
připadly	připadnout	k5eAaPmAgInP	připadnout
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
Moldávii	Moldávie	k1gFnSc3	Moldávie
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
a	a	k8xC	a
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
opustí	opustit	k5eAaPmIp3nS	opustit
všechna	všechen	k3xTgNnPc4	všechen
ruská	ruský	k2eAgNnPc4d1	ruské
města	město	k1gNnPc4	město
a	a	k8xC	a
přístavy	přístav	k1gInPc4	přístav
obsazené	obsazený	k2eAgInPc4d1	obsazený
spojeneckými	spojenecký	k2eAgNnPc7d1	spojenecké
vojsky	vojsko	k1gNnPc7	vojsko
(	(	kIx(	(
<g/>
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
,	,	kIx,	,
Balaklava	Balaklava	k1gFnSc1	Balaklava
<g/>
,	,	kIx,	,
Kamiš	Kamiš	k1gMnSc1	Kamiš
<g/>
,	,	kIx,	,
Jevpatorija	Jevpatorija	k1gMnSc1	Jevpatorija
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
a	a	k8xC	a
sultán	sultána	k1gFnPc2	sultána
museli	muset	k5eAaImAgMnP	muset
uznat	uznat	k5eAaPmF	uznat
neutralitu	neutralita	k1gFnSc4	neutralita
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Novináři	novinář	k1gMnPc1	novinář
==	==	k?	==
</s>
</p>
<p>
<s>
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc4	první
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
veřejnost	veřejnost	k1gFnSc1	veřejnost
informována	informovat	k5eAaBmNgFnS	informovat
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
posílaných	posílaný	k2eAgMnPc2d1	posílaný
telegrafem	telegraf	k1gInSc7	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
reportérem	reportér	k1gMnSc7	reportér
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
W.	W.	kA	W.
H.	H.	kA	H.
Russell	Russell	k1gMnSc1	Russell
z	z	k7c2	z
The	The	k1gFnSc2	The
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
novinář	novinář	k1gMnSc1	novinář
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
válečný	válečný	k2eAgMnSc1d1	válečný
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
zprávám	zpráva	k1gFnPc3	zpráva
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
nekompetentnosti	nekompetentnost	k1gFnSc6	nekompetentnost
politického	politický	k2eAgNnSc2d1	politické
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1855	[number]	k4	1855
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
pád	pád	k1gInSc4	pád
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důležitá	důležitý	k2eAgNnPc1d1	důležité
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
květen	květen	k1gInSc1	květen
1853	[number]	k4	1853
–	–	k?	–
Rusové	Rus	k1gMnPc1	Rus
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Moldávii	Moldávie	k1gFnSc4	Moldávie
a	a	k8xC	a
Valašsko	Valašsko	k1gNnSc4	Valašsko
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říší	říš	k1gFnSc7	říš
</s>
</p>
<p>
<s>
říjen	říjen	k1gInSc1	říjen
1853	[number]	k4	1853
–	–	k?	–
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
válku	válka	k1gFnSc4	válka
Rusku	Rusko	k1gNnSc3	Rusko
</s>
</p>
<p>
<s>
březen	březen	k1gInSc1	březen
1854	[number]	k4	1854
–	–	k?	–
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
válku	válka	k1gFnSc4	válka
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
září	září	k1gNnSc1	září
1854	[number]	k4	1854
–	–	k?	–
Spojenci	spojenec	k1gMnPc1	spojenec
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
bitvu	bitva	k1gFnSc4	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Almě	alma	k1gFnSc6	alma
</s>
</p>
<p>
<s>
říjen	říjen	k1gInSc1	říjen
1854	[number]	k4	1854
–	–	k?	–
Obléhání	obléhání	k1gNnSc1	obléhání
Sevastopolu	Sevastopol	k1gInSc2	Sevastopol
<g/>
;	;	kIx,	;
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Balaklavy	Balaklava	k1gFnSc2	Balaklava
(	(	kIx(	(
<g/>
útok	útok	k1gInSc1	útok
britské	britský	k2eAgFnSc2d1	britská
lehké	lehký	k2eAgFnSc2d1	lehká
jízdy	jízda	k1gFnSc2	jízda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
listopad	listopad	k1gInSc1	listopad
1854	[number]	k4	1854
–	–	k?	–
Rusové	Rus	k1gMnPc1	Rus
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
těžkou	těžký	k2eAgFnSc4d1	těžká
porážku	porážka	k1gFnSc4	porážka
u	u	k7c2	u
Inkermanu	Inkerman	k1gInSc2	Inkerman
</s>
</p>
<p>
<s>
září	září	k1gNnSc1	září
1855	[number]	k4	1855
–	–	k?	–
Pád	Pád	k1gInSc1	Pád
Sevastopolu	Sevastopol	k1gInSc2	Sevastopol
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Čorna	Čorn	k1gInSc2	Čorn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
únor	únor	k1gInSc1	únor
1856	[number]	k4	1856
–	–	k?	–
Ukončení	ukončení	k1gNnSc2	ukončení
bojů	boj	k1gInPc2	boj
</s>
</p>
<p>
<s>
březen	březen	k1gInSc1	březen
1856	[number]	k4	1856
–	–	k?	–
Podepsání	podepsání	k1gNnSc4	podepsání
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
smlouvy	smlouva	k1gFnSc2	smlouva
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
-	-	kIx~	-
ukončila	ukončit	k5eAaPmAgFnS	ukončit
krymskou	krymský	k2eAgFnSc4d1	Krymská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Florence	Florenc	k1gFnPc1	Florenc
Nightingalová	Nightingalová	k1gFnSc1	Nightingalová
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Balaklavy	Balaklava	k1gFnSc2	Balaklava
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
