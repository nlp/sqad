<s>
Tapas	Tapas	k1gMnSc1
</s>
<s>
Tapas	Tapas	k1gInSc1
v	v	k7c6
Zaragoze	Zaragoz	k1gInSc5
</s>
<s>
Tapas	Tapas	k1gInSc1
je	být	k5eAaImIp3nS
synonymem	synonymum	k1gNnSc7
pro	pro	k7c4
malé	malý	k2eAgNnSc4d1
občerstvení	občerstvení	k1gNnSc4
<g/>
,	,	kIx,
degustační	degustační	k2eAgInSc4d1
talířek	talířek	k1gInSc4
či	či	k8xC
jednohubky	jednohubka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
tapas	tapas	k1gInSc4
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
stravování	stravování	k1gNnSc2
velice	velice	k6eAd1
oblíben	oblíbit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
způsob	způsob	k1gInSc4
stravování	stravování	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
je	být	k5eAaImIp3nS
realizován	realizovat	k5eAaBmNgInS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
přátel	přítel	k1gMnPc2
nebo	nebo	k8xC
více	hodně	k6eAd2
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
spolu	spolu	k6eAd1
sdílejí	sdílet	k5eAaImIp3nP
stůl	stůl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
Španělé	Španěl	k1gMnPc1
i	i	k8xC
obyvatelé	obyvatel	k1gMnPc1
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
velmi	velmi	k6eAd1
družní	družný	k2eAgMnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tapas	tapas	k1gInSc4
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
národní	národní	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
jde	jít	k5eAaImIp3nS
hlavně	hlavně	k9
o	o	k7c4
společenskou	společenský	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
<g/>
,	,	kIx,
tapas	tapas	k1gInSc4
jsou	být	k5eAaImIp3nP
připravovány	připravovat	k5eAaImNgInP
při	při	k7c6
rodinných	rodinný	k2eAgFnPc6d1
sešlostech	sešlost	k1gFnPc6
a	a	k8xC
posezeních	posezení	k1gNnPc6
s	s	k7c7
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
tapas	tapasa	k1gFnPc2
patří	patřit	k5eAaImIp3nS
sklenka	sklenka	k1gFnSc1
dobrého	dobrý	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
si	se	k3xPyFc3
je	on	k3xPp3gInPc4
můžete	moct	k5eAaImIp2nP
objednat	objednat	k5eAaPmF
i	i	k9
ve	v	k7c6
vinárnách	vinárna	k1gFnPc6
nebo	nebo	k8xC
restauracích	restaurace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
pro	pro	k7c4
nabídku	nabídka	k1gFnSc4
barů	bar	k1gInPc2
a	a	k8xC
nočních	noční	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
chodí	chodit	k5eAaImIp3nP
bavit	bavit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
španělského	španělský	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
tapar	tapar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
přikrýt	přikrýt	k5eAaPmF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
bylo	být	k5eAaImAgNnS
zvykem	zvyk	k1gInSc7
si	se	k3xPyFc3
při	při	k7c6
večerním	večerní	k2eAgNnSc6d1
popíjení	popíjení	k1gNnSc6
na	na	k7c6
čerstvém	čerstvý	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
zakrývat	zakrývat	k5eAaImF
sklenice	sklenice	k1gFnPc4
se	s	k7c7
sherry	sherry	k1gNnSc7
krajíčky	krajíček	k1gInPc4
chleba	chléb	k1gInSc2
nebo	nebo	k8xC
plátky	plátek	k1gInPc4
šunky	šunka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
nich	on	k3xPp3gMnPc2
nepadal	padat	k5eNaImAgMnS
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
pohotoví	pohotový	k2eAgMnPc1d1
hostinští	hostinský	k1gMnPc1
začali	začít	k5eAaPmAgMnP
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
podávat	podávat	k5eAaImF
talířky	talířek	k1gInPc4
s	s	k7c7
různými	různý	k2eAgFnPc7d1
pochutinami	pochutina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
stál	stát	k5eAaImAgInS
u	u	k7c2
zrodu	zrod	k1gInSc2
tohoto	tento	k3xDgInSc2
způsobu	způsob	k1gInSc2
stolování	stolování	k1gNnSc2
král	král	k1gMnSc1
Alfons	Alfons	k1gMnSc1
X.	X.	kA
Kastilský	kastilský	k2eAgMnSc1d1
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
lékaři	lékař	k1gMnPc1
doporučili	doporučit	k5eAaPmAgMnP
prokládat	prokládat	k5eAaImF
víno	víno	k1gNnSc1
malými	malý	k2eAgFnPc7d1
porcemi	porce	k1gFnPc7
jídla	jídlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Typické	typický	k2eAgFnPc1d1
potraviny	potravina	k1gFnPc1
</s>
<s>
Tapas	Tapas	k1gInSc1
můžou	můžou	k?
obsahovat	obsahovat	k5eAaImF
jamón	jamón	k1gInSc4
(	(	kIx(
<g/>
sušená	sušený	k2eAgFnSc1d1
vepřová	vepřový	k2eAgFnSc1d1
šunka	šunka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyzrálý	vyzrálý	k2eAgInSc1d1
sýr	sýr	k1gInSc1
<g/>
,	,	kIx,
olivy	oliva	k1gFnPc1
a	a	k8xC
různé	různý	k2eAgInPc1d1
salámy	salám	k1gInPc1
a	a	k8xC
klobásy	klobás	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
přikusuje	přikusovat	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
bílý	bílý	k2eAgInSc1d1
chléb	chléb	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hojné	hojný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
se	se	k3xPyFc4
přidávají	přidávat	k5eAaImIp3nP
i	i	k9
ančovičky	ančovička	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jamón	Jamón	k1gMnSc1
</s>
<s>
Salchichon	Salchichon	k1gMnSc1
</s>
<s>
Meze	mez	k1gFnPc1
(	(	kIx(
<g/>
předkrm	předkrm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Labužník	labužník	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tapas	Tapasa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4295233-5	4295233-5	k4
</s>
