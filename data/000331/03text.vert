<s>
Ural	Ural	k1gInSc1	Ural
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
У	У	k?	У
<g/>
́	́	k?	́
<g/>
л	л	k?	л
г	г	k?	г
<g/>
́	́	k?	́
<g/>
р	р	k?	р
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc4	pohoří
v	v	k7c6	v
Ruském	ruský	k2eAgInSc6d1	ruský
Uralském	uralský	k2eAgInSc6d1	uralský
federálním	federální	k2eAgInSc6d1	federální
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
evropské	evropský	k2eAgNnSc1d1	Evropské
a	a	k8xC	a
asijské	asijský	k2eAgNnSc1d1	asijské
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
také	také	k9	také
geografickou	geografický	k2eAgFnSc7d1	geografická
hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
považuje	považovat	k5eAaImIp3nS	považovat
východní	východní	k2eAgNnPc4d1	východní
úpatí	úpatí	k1gNnPc4	úpatí
Uralu	Ural	k1gInSc2	Ural
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnSc2	pohoří
tedy	tedy	k8xC	tedy
leží	ležet	k5eAaImIp3nS	ležet
celé	celý	k2eAgNnSc1d1	celé
ještě	ještě	k9	ještě
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
ruské	ruský	k2eAgNnSc1d1	ruské
pohoří	pohoří	k1gNnSc1	pohoří
táhnoucí	táhnoucí	k2eAgNnSc1d1	táhnoucí
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
500	[number]	k4	500
km	km	kA	km
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever-jih	severih	k1gInSc1	sever-jih
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starého	starý	k2eAgInSc2d1	starý
názvu	název	k1gInSc2	název
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
pohoří	pohoří	k1gNnSc4	pohoří
Montes	Montesa	k1gFnPc2	Montesa
Riphaeus	Riphaeus	k1gMnSc1	Riphaeus
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ural	Ural	k1gInSc4	Ural
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
známých	známý	k2eAgNnPc2d1	známé
pásmových	pásmový	k2eAgNnPc2d1	pásmové
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
prvohorách	prvohory	k1gFnPc6	prvohory
během	během	k7c2	během
karbonu	karbon	k1gInSc2	karbon
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
tektonické	tektonický	k2eAgFnSc2d1	tektonická
srážky	srážka	k1gFnSc2	srážka
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
Baltiky	Baltik	k1gInPc4	Baltik
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geografického	geografický	k2eAgNnSc2d1	geografické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
Ural	Ural	k1gInSc1	Ural
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
5	[number]	k4	5
samostatných	samostatný	k2eAgInPc2d1	samostatný
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
Geo-Koordinaten	Geo-Koordinaten	k2eAgInSc1d1	Geo-Koordinaten
(	(	kIx(	(
<g/>
šířka	šířka	k1gFnSc1	šířka
=	=	kIx~	=
š	š	k?	š
<g/>
;	;	kIx,	;
délka	délka	k1gFnSc1	délka
=	=	kIx~	=
d	d	k?	d
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Ural	Ural	k1gInSc1	Ural
<g/>
:	:	kIx,	:
š	š	k?	š
=	=	kIx~	=
55	[number]	k4	55
<g/>
°	°	k?	°
<g/>
40	[number]	k4	40
<g/>
'	'	kIx"	'
až	až	k9	až
52	[number]	k4	52
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
d	d	k?	d
=	=	kIx~	=
60	[number]	k4	60
<g/>
<g />
.	.	kIx.	.
</s>
<s>
°	°	k?	°
až	až	k9	až
57	[number]	k4	57
<g/>
°	°	k?	°
v.	v.	k?	v.
d	d	k?	d
<g/>
;	;	kIx,	;
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Ufy	Ufy	k1gFnPc2	Ufy
až	až	k9	až
po	po	k7c4	po
město	město	k1gNnSc4	město
Orsk	Orsk	k1gInSc4	Orsk
Střední	střední	k2eAgInSc4d1	střední
Ural	Ural	k1gInSc4	Ural
<g/>
:	:	kIx,	:
š	š	k?	š
=	=	kIx~	=
59	[number]	k4	59
<g/>
°	°	k?	°
až	až	k9	až
55	[number]	k4	55
<g/>
°	°	k?	°
<g/>
40	[number]	k4	40
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
d	d	k?	d
=	=	kIx~	=
58	[number]	k4	58
<g/>
°	°	k?	°
až	až	k9	až
61	[number]	k4	61
<g/>
°	°	k?	°
v.	v.	k?	v.
d.	d.	k?	d.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ufy	Ufy	k1gFnSc2	Ufy
Severní	severní	k2eAgInSc4d1	severní
Ural	Ural	k1gInSc4	Ural
<g/>
:	:	kIx,	:
š	š	k?	š
=	=	kIx~	=
64	[number]	k4	64
<g/>
°	°	k?	°
až	až	k9	až
59	[number]	k4	59
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
d	d	k?	d
=	=	kIx~	=
asi	asi	k9	asi
<g/>
.	.	kIx.	.
59	[number]	k4	59
<g/>
°	°	k?	°
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
;	;	kIx,	;
severně	severně	k6eAd1	severně
od	od	k7c2	od
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Kosvy	Kosva	k1gFnSc2	Kosva
Připolární	Připolární	k2eAgInSc4d1	Připolární
Ural	Ural	k1gInSc4	Ural
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Subpolární	subpolární	k2eAgInSc4d1	subpolární
Ural	Ural	k1gInSc4	Ural
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
š	š	k?	š
=	=	kIx~	=
65	[number]	k4	65
<g/>
°	°	k?	°
<g/>
40	[number]	k4	40
<g/>
'	'	kIx"	'
až	až	k9	až
64	[number]	k4	64
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
d	d	k?	d
=	=	kIx~	=
62	[number]	k4	62
<g/>
°	°	k?	°
až	až	k9	až
59	[number]	k4	59
<g/>
°	°	k?	°
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
;	;	kIx,	;
severně	severně	k6eAd1	severně
od	od	k7c2	od
průlomu	průlom	k1gInSc2	průlom
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Ural	Ural	k1gInSc1	Ural
tvořeného	tvořený	k2eAgInSc2d1	tvořený
řekou	řeka	k1gFnSc7	řeka
Ščugor	Ščugor	k1gInSc4	Ščugor
Polární	polární	k2eAgInSc4d1	polární
Ural	Ural	k1gInSc4	Ural
<g/>
:	:	kIx,	:
š	š	k?	š
=	=	kIx~	=
69	[number]	k4	69
<g/>
°	°	k?	°
až	až	k9	až
65	[number]	k4	65
<g/>
°	°	k?	°
<g/>
40	[number]	k4	40
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
d	d	k?	d
=	=	kIx~	=
67	[number]	k4	67
<g/>
°	°	k?	°
až	až	k9	až
62	[number]	k4	62
<g/>
°	°	k?	°
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
;	;	kIx,	;
severně	severně	k6eAd1	severně
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
řeky	řeka	k1gFnSc2	řeka
Chulgy	Chulga	k1gFnSc2	Chulga
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
k	k	k7c3	k
Severnímu	severní	k2eAgInSc3d1	severní
ledovému	ledový	k2eAgInSc3d1	ledový
oceánu	oceán	k1gInSc3	oceán
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
Kazachstánem	Kazachstán	k1gInSc7	Kazachstán
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
1	[number]	k4	1
895	[number]	k4	895
m	m	kA	m
hora	hora	k1gFnSc1	hora
Narodnaja	Narodnaja	k1gFnSc1	Narodnaja
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Uralu	Ural	k1gInSc2	Ural
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
před	před	k7c7	před
postupujícími	postupující	k2eAgInPc7d1	postupující
německými	německý	k2eAgInPc7d1	německý
vojsky	vojsko	k1gNnPc7	vojsko
přestěhována	přestěhován	k2eAgFnSc1d1	přestěhována
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
sovětského	sovětský	k2eAgInSc2d1	sovětský
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
nikl	nikl	k1gInSc1	nikl
a	a	k8xC	a
i	i	k9	i
další	další	k2eAgFnPc4d1	další
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgFnPc4d1	důležitá
surovinové	surovinový	k2eAgFnPc4d1	surovinová
základny	základna	k1gFnPc4	základna
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
na	na	k7c6	na
Urale	Ural	k1gInSc6	Ural
jsou	být	k5eAaImIp3nP	být
obývány	obýván	k2eAgFnPc1d1	obývána
zvířaty	zvíře	k1gNnPc7	zvíře
typickými	typický	k2eAgNnPc7d1	typické
Sibiře	Sibiř	k1gFnSc2	Sibiř
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
los	los	k1gInSc1	los
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
rosomák	rosomák	k1gMnSc1	rosomák
<g/>
,	,	kIx,	,
rys	rys	k1gMnSc1	rys
a	a	k8xC	a
sobol	sobol	k1gMnSc1	sobol
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
Uralu	Ural	k1gInSc2	Ural
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
Sverdlovsk	Sverdlovsk	k1gInSc1	Sverdlovsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
sídla	sídlo	k1gNnPc4	sídlo
na	na	k7c6	na
Urale	Ural	k1gInSc6	Ural
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
také	také	k9	také
Magnitogorsk	Magnitogorsk	k1gInSc1	Magnitogorsk
<g/>
,	,	kIx,	,
Miass	Miass	k1gInSc1	Miass
<g/>
,	,	kIx,	,
Serov	Serov	k1gInSc1	Serov
<g/>
,	,	kIx,	,
Zlatoust	Zlatoust	k1gMnSc1	Zlatoust
<g/>
,	,	kIx,	,
Nižnij	Nižnij	k1gMnSc1	Nižnij
Tagil	Tagil	k1gMnSc1	Tagil
<g/>
,	,	kIx,	,
Kaměnsk-Ural	Kaměnsk-Ural	k1gMnSc1	Kaměnsk-Ural
<g/>
'	'	kIx"	'
<g/>
skij	skij	k1gMnSc1	skij
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
centra	centrum	k1gNnPc1	centrum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Perm	perm	k1gInSc1	perm
<g/>
,	,	kIx,	,
Ufa	Ufa	k1gFnSc1	Ufa
<g/>
,	,	kIx,	,
Vorkuta	Vorkut	k2eAgFnSc1d1	Vorkuta
nebo	nebo	k8xC	nebo
Čeljabinsk	Čeljabinsk	k1gInSc1	Čeljabinsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Urale	Ural	k1gInSc6	Ural
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
48	[number]	k4	48
druhů	druh	k1gInPc2	druh
hospodářsky	hospodářsky	k6eAd1	hospodářsky
cenných	cenný	k2eAgFnPc2d1	cenná
rud	ruda	k1gFnPc2	ruda
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vzácné	vzácný	k2eAgInPc1d1	vzácný
kameny	kámen	k1gInPc1	kámen
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
smaragd	smaragd	k1gInSc1	smaragd
<g/>
,	,	kIx,	,
ametyst	ametyst	k1gInSc1	ametyst
<g/>
,	,	kIx,	,
akvamarín	akvamarín	k1gInSc1	akvamarín
<g/>
,	,	kIx,	,
jaspis	jaspis	k1gInSc1	jaspis
<g/>
,	,	kIx,	,
rodonit	rodonit	k1gInSc1	rodonit
<g/>
,	,	kIx,	,
malachit	malachit	k1gInSc1	malachit
a	a	k8xC	a
diamant	diamant	k1gInSc1	diamant
<g/>
.	.	kIx.	.
</s>
