<s>
Ural	Ural	k1gInSc1	Ural
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
У	У	k?	У
<g/>
́	́	k?	́
<g/>
л	л	k?	л
г	г	k?	г
<g/>
́	́	k?	́
<g/>
р	р	k?	р
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc4	pohoří
v	v	k7c6	v
Ruském	ruský	k2eAgInSc6d1	ruský
Uralském	uralský	k2eAgInSc6d1	uralský
federálním	federální	k2eAgInSc6d1	federální
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
evropské	evropský	k2eAgNnSc1d1	Evropské
a	a	k8xC	a
asijské	asijský	k2eAgNnSc1d1	asijské
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
také	také	k9	také
geografickou	geografický	k2eAgFnSc7d1	geografická
hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
