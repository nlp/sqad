<s>
Omezená	omezený	k2eAgFnSc1d1	omezená
schopnost	schopnost	k1gFnSc1	schopnost
sluchu	sluch	k1gInSc2	sluch
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nedoslýchavost	nedoslýchavost	k1gFnSc1	nedoslýchavost
<g/>
,	,	kIx,	,
úplná	úplný	k2eAgFnSc1d1	úplná
ztráta	ztráta	k1gFnSc1	ztráta
sluchu	sluch	k1gInSc2	sluch
je	být	k5eAaImIp3nS	být
hluchota	hluchota	k1gFnSc1	hluchota
<g/>
.	.	kIx.	.
</s>
