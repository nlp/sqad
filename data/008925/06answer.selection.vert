<s>
Sókratés	Sókratés	k1gInSc1	Sókratés
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
469	[number]	k4	469
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
–	–	k?	–
399	[number]	k4	399
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
Sokrates	Sokrates	k1gMnSc1	Sokrates
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
athénský	athénský	k2eAgMnSc1d1	athénský
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
Platónův	Platónův	k2eAgMnSc1d1	Platónův
<g/>
.	.	kIx.	.
</s>
