<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
<g/>
,	,	kIx,	,
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Kresčak	Kresčak	k1gInSc4	Kresčak
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Crécy-en-Ponthieu	Crécyn-Ponthie	k2eAgFnSc4d1	Crécy-en-Ponthie
<g/>
)	)	kIx)	)
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
králem	král	k1gMnSc7	král
Eduardem	Eduard	k1gMnSc7	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
drtivě	drtivě	k6eAd1	drtivě
porazila	porazit	k5eAaPmAgFnS	porazit
početně	početně	k6eAd1	početně
silnější	silný	k2eAgNnSc4d2	silnější
vojsko	vojsko	k1gNnSc4	vojsko
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
Angličanů	Angličan	k1gMnPc2	Angličan
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
lučištníci	lučištník	k1gMnPc1	lučištník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
opakovaně	opakovaně	k6eAd1	opakovaně
odolali	odolat	k5eAaPmAgMnP	odolat
marným	marný	k2eAgInPc3d1	marný
útokům	útok	k1gInPc3	útok
neukázněné	ukázněný	k2eNgFnSc2d1	neukázněná
těžké	těžký	k2eAgFnSc2d1	těžká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
příslušníci	příslušník	k1gMnPc1	příslušník
svého	svůj	k3xOyFgMnSc4	svůj
protivníka	protivník	k1gMnSc4	protivník
fatálně	fatálně	k6eAd1	fatálně
podcenili	podcenit	k5eAaPmAgMnP	podcenit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
horizontu	horizont	k1gInSc6	horizont
pro	pro	k7c4	pro
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
monarchii	monarchie	k1gFnSc4	monarchie
znamenala	znamenat	k5eAaImAgFnS	znamenat
porážka	porážka	k1gFnSc1	porážka
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
takřka	takřka	k6eAd1	takřka
národní	národní	k2eAgFnSc4d1	národní
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jejích	její	k3xOp3gMnPc2	její
dalších	další	k2eAgMnPc2d1	další
vojenských	vojenský	k2eAgMnPc2d1	vojenský
neúspěchů	neúspěch	k1gInPc2	neúspěch
a	a	k8xC	a
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ji	on	k3xPp3gFnSc4	on
přivedly	přivést	k5eAaPmAgInP	přivést
do	do	k7c2	do
hluboké	hluboký	k2eAgFnSc2d1	hluboká
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
střetnutí	střetnutí	k1gNnSc2	střetnutí
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
stovkami	stovka	k1gFnPc7	stovka
dalších	další	k2eAgMnPc2d1	další
šlechticů	šlechtic	k1gMnPc2	šlechtic
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
zabit	zabít	k5eAaPmNgMnS	zabít
i	i	k9	i
slepý	slepý	k2eAgMnSc1d1	slepý
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1346	[number]	k4	1346
se	se	k3xPyFc4	se
vylodily	vylodit	k5eAaPmAgFnP	vylodit
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
poloostrova	poloostrov	k1gInSc2	poloostrov
Cotentin	Cotentin	k1gInSc1	Cotentin
poblíž	poblíž	k7c2	poblíž
Saint-Vaast-la-Hougue	Saint-Vaasta-Hougu	k1gInSc2	Saint-Vaast-la-Hougu
v	v	k7c6	v
normanském	normanský	k2eAgNnSc6d1	normanské
vévodství	vévodství	k1gNnSc6	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
letopisců	letopisec	k1gMnPc2	letopisec
prý	prý	k9	prý
toto	tento	k3xDgNnSc1	tento
vojsko	vojsko	k1gNnSc1	vojsko
čítalo	čítat	k5eAaImAgNnS	čítat
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
000	[number]	k4	000
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
údaj	údaj	k1gInSc1	údaj
4	[number]	k4	4
000	[number]	k4	000
těžkooděnců	těžkooděnec	k1gMnPc2	těžkooděnec
a	a	k8xC	a
10	[number]	k4	10
000	[number]	k4	000
lučištníků	lučištník	k1gMnPc2	lučištník
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
dalších	další	k2eAgFnPc2d1	další
složek	složka	k1gFnPc2	složka
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Vpád	vpád	k1gInSc1	vpád
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
domácí	domácí	k2eAgFnPc4d1	domácí
jednotky	jednotka	k1gFnPc4	jednotka
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
naprosté	naprostý	k2eAgFnSc2d1	naprostá
nepřipravenosti	nepřipravenost	k1gFnSc2	nepřipravenost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angličané	Angličan	k1gMnPc1	Angličan
svůj	svůj	k3xOyFgInSc4	svůj
úder	úder	k1gInSc4	úder
povedou	vést	k5eAaImIp3nP	vést
z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
vylodění	vylodění	k1gNnSc6	vylodění
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
okolnosti	okolnost	k1gFnSc3	okolnost
dlouho	dlouho	k6eAd1	dlouho
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
směrem	směr	k1gInSc7	směr
protivník	protivník	k1gMnSc1	protivník
vyrazí	vyrazit	k5eAaPmIp3nS	vyrazit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
neprodleně	prodleně	k6eNd1	prodleně
rozeslal	rozeslat	k5eAaPmAgMnS	rozeslat
listy	list	k1gInPc4	list
s	s	k7c7	s
výzvou	výzva	k1gFnSc7	výzva
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
svým	svůj	k3xOyFgMnPc3	svůj
evropským	evropský	k2eAgMnPc3d1	evropský
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patřili	patřit	k5eAaImAgMnP	patřit
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Saumský	Saumský	k2eAgMnSc1d1	Saumský
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Flanderský	flanderský	k2eAgMnSc1d1	flanderský
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Guillaume	Guillaum	k1gInSc5	Guillaum
z	z	k7c2	z
Namuru	Namur	k1gInSc2	Namur
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
shromaždiště	shromaždiště	k1gNnSc1	shromaždiště
francouzských	francouzský	k2eAgInPc2d1	francouzský
a	a	k8xC	a
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
oddílů	oddíl	k1gInPc2	oddíl
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
města	město	k1gNnSc2	město
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
Amiens	Amiens	k1gInSc1	Amiens
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Jean	Jean	k1gMnSc1	Jean
Froissart	Froissart	k1gInSc4	Froissart
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Kronikách	kronika	k1gFnPc6	kronika
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
tažení	tažení	k1gNnSc6	tažení
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
pochod	pochod	k1gInSc1	pochod
posléze	posléze	k6eAd1	posléze
obrátil	obrátit	k5eAaPmAgInS	obrátit
k	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
Normandie	Normandie	k1gFnSc2	Normandie
Caen	Caena	k1gFnPc2	Caena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
rukou	ruka	k1gFnPc2	ruka
po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
odporu	odpor	k1gInSc6	odpor
padlo	padnout	k5eAaPmAgNnS	padnout
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
dní	den	k1gInPc2	den
po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
tohoto	tento	k3xDgInSc2	tento
úspěchu	úspěch	k1gInSc2	úspěch
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
údolím	údolí	k1gNnSc7	údolí
Seiny	Seina	k1gFnSc2	Seina
k	k	k7c3	k
Paříži	Paříž	k1gFnSc3	Paříž
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
důsledně	důsledně	k6eAd1	důsledně
plenil	plenit	k5eAaImAgInS	plenit
francouzský	francouzský	k2eAgInSc1d1	francouzský
venkov	venkov	k1gInSc1	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prozatím	prozatím	k6eAd1	prozatím
disponoval	disponovat	k5eAaBmAgInS	disponovat
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc7d2	menší
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
opatství	opatství	k1gNnSc6	opatství
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
posvátný	posvátný	k2eAgMnSc1d1	posvátný
Oriflamme	Oriflamme	k1gMnSc1	Oriflamme
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
válečnou	válečný	k2eAgFnSc4d1	válečná
korouhev	korouhev	k1gFnSc4	korouhev
zasvěcenou	zasvěcený	k2eAgFnSc7d1	zasvěcená
sv.	sv.	kA	sv.
Divišovi	Divišův	k2eAgMnPc1d1	Divišův
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
do	do	k7c2	do
Rouenu	Rouen	k1gInSc2	Rouen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
ustupoval	ustupovat	k5eAaImAgMnS	ustupovat
po	po	k7c6	po
opačném	opačný	k2eAgInSc6d1	opačný
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Paříži	Paříž	k1gFnSc3	Paříž
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
protivníkem	protivník	k1gMnSc7	protivník
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
ničil	ničit	k5eAaImAgMnS	ničit
mosty	most	k1gInPc4	most
přes	přes	k7c4	přes
dolní	dolní	k2eAgInSc4d1	dolní
tok	tok	k1gInSc4	tok
Seiny	Seina	k1gFnSc2	Seina
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
zesiloval	zesilovat	k5eAaImAgMnS	zesilovat
jejich	jejich	k3xOp3gFnSc4	jejich
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Angličanům	Angličan	k1gMnPc3	Angličan
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
flanderskými	flanderský	k2eAgMnPc7d1	flanderský
(	(	kIx(	(
<g/>
vlámskými	vlámský	k2eAgMnPc7d1	vlámský
<g/>
)	)	kIx)	)
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
srpna	srpen	k1gInSc2	srpen
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
městu	město	k1gNnSc3	město
Harfleur	Harfleura	k1gFnPc2	Harfleura
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
stojí	stát	k5eAaImIp3nP	stát
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgNnSc2	jenž
ústí	ústit	k5eAaImIp3nS	ústit
Seina	Seina	k1gFnSc1	Seina
<g/>
,	,	kIx,	,
flotila	flotila	k1gFnSc1	flotila
janovských	janovský	k2eAgFnPc2d1	janovská
galér	galéra	k1gFnPc2	galéra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
číhaly	číhat	k5eAaImAgFnP	číhat
na	na	k7c4	na
Angličany	Angličan	k1gMnPc4	Angličan
u	u	k7c2	u
La	la	k1gNnSc2	la
Rochelle	Rochelle	k1gFnSc2	Rochelle
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
na	na	k7c4	na
námořní	námořní	k2eAgFnSc4d1	námořní
intervenci	intervence	k1gFnSc4	intervence
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
připojili	připojit	k5eAaPmAgMnP	připojit
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
na	na	k7c6	na
palubách	paluba	k1gFnPc6	paluba
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
střelci	střelec	k1gMnPc1	střelec
z	z	k7c2	z
kuší	kuše	k1gFnPc2	kuše
a	a	k8xC	a
pavézníci	pavézník	k1gMnPc1	pavézník
<g/>
,	,	kIx,	,
k	k	k7c3	k
vojsku	vojsko	k1gNnSc3	vojsko
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Angličané	Angličan	k1gMnPc1	Angličan
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
k	k	k7c3	k
francouzské	francouzský	k2eAgFnSc3d1	francouzská
metropoli	metropol	k1gFnSc3	metropol
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
svízelné	svízelný	k2eAgNnSc1d1	svízelné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
začaly	začít	k5eAaPmAgFnP	začít
stahovat	stahovat	k5eAaImF	stahovat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
u	u	k7c2	u
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
dorazil	dorazit	k5eAaPmAgMnS	dorazit
pomocný	pomocný	k2eAgInSc4d1	pomocný
lucemburský	lucemburský	k2eAgInSc4d1	lucemburský
kontingent	kontingent	k1gInSc4	kontingent
čítající	čítající	k2eAgInSc4d1	čítající
na	na	k7c4	na
500	[number]	k4	500
rytířů	rytíř	k1gMnPc2	rytíř
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
osobními	osobní	k2eAgFnPc7d1	osobní
družinami	družina	k1gFnPc7	družina
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
však	však	k9	však
výhodné	výhodný	k2eAgFnSc2d1	výhodná
situace	situace	k1gFnSc2	situace
nevyužil	využít	k5eNaPmAgInS	využít
a	a	k8xC	a
namísto	namísto	k7c2	namísto
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
konfrontace	konfrontace	k1gFnSc2	konfrontace
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
dostát	dostát	k5eAaPmF	dostát
rytířským	rytířský	k2eAgNnPc3d1	rytířské
pravidlům	pravidlo	k1gNnPc3	pravidlo
a	a	k8xC	a
ve	v	k7c6	v
dvoudenním	dvoudenní	k2eAgNnSc6d1	dvoudenní
jednání	jednání	k1gNnSc6	jednání
se	se	k3xPyFc4	se
s	s	k7c7	s
Eduardem	Eduard	k1gMnSc7	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vést	vést	k5eAaImF	vést
rozhovory	rozhovor	k1gInPc4	rozhovor
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
svedena	sveden	k2eAgFnSc1d1	svedena
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
bitva	bitva	k1gFnSc1	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
však	však	k9	však
využil	využít	k5eAaPmAgMnS	využít
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
a	a	k8xC	a
opravil	opravit	k5eAaPmAgMnS	opravit
most	most	k1gInSc4	most
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Poissy	Poissa	k1gFnSc2	Poissa
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
pak	pak	k6eAd1	pak
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
a	a	k8xC	a
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
Beauvais	Beauvais	k1gFnSc3	Beauvais
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
poslal	poslat	k5eAaPmAgInS	poslat
svému	svůj	k3xOyFgMnSc3	svůj
sokovi	sok	k1gMnSc3	sok
výsměšnou	výsměšný	k2eAgFnSc7d1	výsměšná
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
marně	marně	k6eAd1	marně
čekal	čekat	k5eAaImAgInS	čekat
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
svým	svůj	k3xOyFgMnPc3	svůj
flanderským	flanderský	k2eAgMnPc3d1	flanderský
přátelům	přítel	k1gMnPc3	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
posílení	posílení	k1gNnSc1	posílení
čerstvými	čerstvý	k2eAgInPc7d1	čerstvý
oddíly	oddíl	k1gInPc7	oddíl
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kontingentu	kontingent	k1gInSc2	kontingent
sesazeného	sesazený	k2eAgMnSc2d1	sesazený
krále	král	k1gMnSc2	král
Mallorky	Mallorka	k1gFnSc2	Mallorka
<g/>
,	,	kIx,	,
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
pochod	pochod	k1gInSc4	pochod
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
trasou	trasa	k1gFnSc7	trasa
pochodu	pochod	k1gInSc2	pochod
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
směr	směr	k1gInSc1	směr
jeho	on	k3xPp3gInSc2	on
postupu	postup	k1gInSc2	postup
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Amiensu	Amiens	k1gInSc2	Amiens
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
Airaines	Airainesa	k1gFnPc2	Airainesa
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
překročit	překročit	k5eAaPmF	překročit
řeku	řeka	k1gFnSc4	řeka
Sommu	Somm	k1gInSc2	Somm
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
díky	díky	k7c3	díky
informacím	informace	k1gFnPc3	informace
o	o	k7c6	o
brodu	brod	k1gInSc6	brod
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
Abbeville	Abbevill	k1gMnSc2	Abbevill
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
schůdný	schůdný	k2eAgMnSc1d1	schůdný
jen	jen	k9	jen
při	při	k7c6	při
odlivu	odliv	k1gInSc6	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přebrodění	přebrodění	k1gNnSc6	přebrodění
řeky	řeka	k1gFnSc2	řeka
zamířil	zamířit	k5eAaPmAgMnS	zamířit
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Pikardie	Pikardie	k1gFnSc2	Pikardie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nalézt	nalézt	k5eAaBmF	nalézt
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
svedení	svedení	k1gNnSc4	svedení
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
nalezl	nalézt	k5eAaBmAgMnS	nalézt
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Kresčak	Kresčak	k1gInSc1	Kresčak
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vedla	vést	k5eAaImAgFnS	vést
trasa	trasa	k1gFnSc1	trasa
jeho	on	k3xPp3gInSc2	on
pochodu	pochod	k1gInSc2	pochod
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zdejší	zdejší	k2eAgFnSc1d1	zdejší
krajina	krajina	k1gFnSc1	krajina
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
a	a	k8xC	a
bažinatá	bažinatý	k2eAgFnSc1d1	bažinatá
<g/>
.	.	kIx.	.
</s>
<s>
Bitevní	bitevní	k2eAgNnPc1d1	bitevní
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
utkala	utkat	k5eAaPmAgNnP	utkat
vojska	vojsko	k1gNnPc1	vojsko
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
asi	asi	k9	asi
25	[number]	k4	25
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
nehlubokého	hluboký	k2eNgNnSc2d1	nehluboké
údolí	údolí	k1gNnSc2	údolí
říčky	říčka	k1gFnSc2	říčka
Maye	May	k1gMnSc2	May
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
vesnicemi	vesnice	k1gFnPc7	vesnice
Kresčak	Kresčak	k1gInSc4	Kresčak
a	a	k8xC	a
Wadicourt	Wadicourt	k1gInSc4	Wadicourt
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zvlněné	zvlněný	k2eAgFnSc2d1	zvlněná
pláně	pláň	k1gFnSc2	pláň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
okolí	okolí	k1gNnSc1	okolí
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
značně	značně	k6eAd1	značně
kopcovité	kopcovitý	k2eAgFnSc6d1	kopcovitá
a	a	k8xC	a
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
obklopené	obklopený	k2eAgFnPc1d1	obklopená
sady	sada	k1gFnPc1	sada
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
hřeben	hřeben	k1gInSc1	hřeben
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
pozici	pozice	k1gFnSc4	pozice
anglické	anglický	k2eAgNnSc1d1	anglické
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
kilometry	kilometr	k1gInPc4	kilometr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
Kresčakem	Kresčak	k1gInSc7	Kresčak
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vrcholu	vrchol	k1gInSc6	vrchol
prý	prý	k9	prý
stál	stát	k5eAaImAgMnS	stát
větrný	větrný	k2eAgInSc4d1	větrný
mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
údajně	údajně	k6eAd1	údajně
řídil	řídit	k5eAaImAgMnS	řídit
bitvu	bitva	k1gFnSc4	bitva
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Kresčak	Kresčak	k1gInSc1	Kresčak
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
podél	podél	k7c2	podél
cesty	cesta	k1gFnSc2	cesta
vedoucí	vedoucí	k1gFnSc2	vedoucí
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
k	k	k7c3	k
Arrasu	Arras	k1gInSc3	Arras
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgMnS	tvořit
jej	on	k3xPp3gInSc4	on
kostel	kostel	k1gInSc4	kostel
obklopený	obklopený	k2eAgInSc4d1	obklopený
hřbitovem	hřbitov	k1gInSc7	hřbitov
a	a	k8xC	a
několik	několik	k4yIc1	několik
domů	dům	k1gInPc2	dům
se	s	k7c7	s
zemědělským	zemědělský	k2eAgNnSc7d1	zemědělské
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hřebenem	hřeben	k1gInSc7	hřeben
stál	stát	k5eAaImAgInS	stát
les	les	k1gInSc4	les
patřící	patřící	k2eAgInSc4d1	patřící
ke	k	k7c3	k
Kresčackému	Kresčacký	k2eAgInSc3d1	Kresčacký
dvorci	dvorec	k1gInSc3	dvorec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
části	část	k1gFnSc2	část
kryl	krýt	k5eAaImAgMnS	krýt
anglický	anglický	k2eAgInSc4d1	anglický
týl	týl	k1gInSc4	týl
a	a	k8xC	a
pravý	pravý	k2eAgInSc4d1	pravý
bok	bok	k1gInSc4	bok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Wadicourtu	Wadicourt	k1gInSc2	Wadicourt
k	k	k7c3	k
říčce	říčka	k1gFnSc3	říčka
Maye	May	k1gMnSc2	May
se	se	k3xPyFc4	se
rozprostírala	rozprostírat	k5eAaImAgNnP	rozprostírat
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
kilometry	kilometr	k1gInPc4	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
prohloubenina	prohloubenina	k1gFnSc1	prohloubenina
Vallée	Vallé	k1gFnSc2	Vallé
de	de	k?	de
Clercs	Clercs	k1gInSc1	Clercs
(	(	kIx(	(
<g/>
Údolí	údolí	k1gNnSc1	údolí
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nejhlubším	hluboký	k2eAgInSc6d3	nejhlubší
bodě	bod	k1gInSc6	bod
asi	asi	k9	asi
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
okolní	okolní	k2eAgFnSc2d1	okolní
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francouzi	Francouz	k1gMnPc1	Francouz
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
angličtí	anglický	k2eAgMnPc1d1	anglický
vojáci	voják	k1gMnPc1	voják
vyhloubili	vyhloubit	k5eAaPmAgMnP	vyhloubit
řadu	řada	k1gFnSc4	řada
jamek	jamka	k1gFnPc2	jamka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
čtvereční	čtvereční	k2eAgFnSc2d1	čtvereční
stopy	stopa	k1gFnSc2	stopa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
narušit	narušit	k5eAaPmF	narušit
útok	útok	k1gInSc4	útok
protivníkova	protivníkův	k2eAgNnSc2d1	protivníkovo
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
odpoledne	odpoledne	k6eAd1	odpoledne
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
letní	letní	k2eAgFnSc3d1	letní
bouřce	bouřka	k1gFnSc3	bouřka
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
koně	kůň	k1gMnSc4	kůň
Francouzů	Francouz	k1gMnPc2	Francouz
útočící	útočící	k2eAgFnSc2d1	útočící
do	do	k7c2	do
svahu	svah	k1gInSc2	svah
smekali	smekat	k5eAaImAgMnP	smekat
a	a	k8xC	a
klouzali	klouzat	k5eAaImAgMnP	klouzat
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
vojska	vojsko	k1gNnSc2	vojsko
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
tvořili	tvořit	k5eAaImAgMnP	tvořit
především	především	k9	především
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
řádný	řádný	k2eAgInSc4d1	řádný
žold	žold	k1gInSc4	žold
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
pak	pak	k6eAd1	pak
doplňovali	doplňovat	k5eAaImAgMnP	doplňovat
odvedenci	odvedenec	k1gMnPc1	odvedenec
a	a	k8xC	a
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
za	za	k7c4	za
kriminální	kriminální	k2eAgInPc4d1	kriminální
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
prominut	prominut	k2eAgInSc4d1	prominut
jejich	jejich	k3xOp3gInSc4	jejich
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
tažení	tažení	k1gNnSc2	tažení
účastnili	účastnit	k5eAaImAgMnP	účastnit
i	i	k9	i
vojáci	voják	k1gMnPc1	voják
ze	z	k7c2	z
zaostalých	zaostalý	k2eAgFnPc2d1	zaostalá
keltských	keltský	k2eAgFnPc2d1	keltská
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
Cornwall	Cornwall	k1gInSc1	Cornwall
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kontingenty	kontingent	k1gInPc1	kontingent
z	z	k7c2	z
Angličany	Angličan	k1gMnPc7	Angličan
obsazeného	obsazený	k2eAgNnSc2d1	obsazené
Gaskoňska	Gaskoňsko	k1gNnSc2	Gaskoňsko
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
z	z	k7c2	z
Bretaně	Bretaň	k1gFnSc2	Bretaň
a	a	k8xC	a
Flander	Flandry	k1gInPc2	Flandry
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
těchto	tento	k3xDgMnPc2	tento
shromážděných	shromážděný	k2eAgMnPc2d1	shromážděný
mužů	muž	k1gMnPc2	muž
tvořili	tvořit	k5eAaImAgMnP	tvořit
lučištníci	lučištník	k1gMnPc1	lučištník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP	vyzbrojit
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
metry	metr	k1gInPc7	metr
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
luky	luk	k1gInPc7	luk
<g/>
,	,	kIx,	,
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
tisu	tis	k1gInSc2	tis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
jilmu	jilm	k1gInSc2	jilm
nebo	nebo	k8xC	nebo
jasanu	jasan	k1gInSc2	jasan
<g/>
.	.	kIx.	.
</s>
<s>
Loket	loket	k1gInSc1	loket
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
šípy	šíp	k1gInPc1	šíp
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
dostřel	dostřel	k1gInSc4	dostřel
až	až	k6eAd1	až
270	[number]	k4	270
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
úzkými	úzký	k2eAgInPc7d1	úzký
hroty	hrot	k1gInPc7	hrot
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dokázaly	dokázat	k5eAaPmAgInP	dokázat
proniknout	proniknout	k5eAaPmF	proniknout
kroužkovou	kroužkový	k2eAgFnSc7d1	kroužková
výstrojí	výstroj	k1gFnSc7	výstroj
a	a	k8xC	a
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
i	i	k8xC	i
plátovou	plátový	k2eAgFnSc7d1	plátová
zbrojí	zbroj	k1gFnSc7	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Zbraně	zbraň	k1gFnPc1	zbraň
se	se	k3xPyFc4	se
napínaly	napínat	k5eAaImAgFnP	napínat
silným	silný	k2eAgInSc7d1	silný
plynulým	plynulý	k2eAgInSc7d1	plynulý
pohybem	pohyb	k1gInSc7	pohyb
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
angličtí	anglický	k2eAgMnPc1d1	anglický
nebo	nebo	k8xC	nebo
waleští	waleský	k2eAgMnPc1d1	waleský
lučištníci	lučištník	k1gMnPc1	lučištník
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
udržovat	udržovat	k5eAaImF	udržovat
kadenci	kadence	k1gFnSc4	kadence
střelby	střelba	k1gFnSc2	střelba
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
šípy	šíp	k1gInPc4	šíp
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
své	své	k1gNnSc4	své
muže	muž	k1gMnSc2	muž
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
šiků	šik	k1gInPc2	šik
(	(	kIx(	(
<g/>
batailles	batailles	k1gInSc1	batailles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
historická	historický	k2eAgFnSc1d1	historická
obec	obec	k1gFnSc1	obec
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
shodnout	shodnout	k5eAaBmF	shodnout
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dvě	dva	k4xCgFnPc1	dva
dobové	dobový	k2eAgFnPc1d1	dobová
relace	relace	k1gFnPc1	relace
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
různých	různý	k2eAgNnPc6d1	různé
číslech	číslo	k1gNnPc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Froissart	Froissart	k1gInSc4	Froissart
v	v	k7c6	v
Kronice	kronika	k1gFnSc6	kronika
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc4	první
linii	linie	k1gFnSc4	linie
tvořilo	tvořit	k5eAaImAgNnS	tvořit
800	[number]	k4	800
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
2	[number]	k4	2
000	[number]	k4	000
lučištníků	lučištník	k1gMnPc2	lučištník
a	a	k8xC	a
1	[number]	k4	1
000	[number]	k4	000
waleských	waleský	k2eAgMnPc2d1	waleský
kopiníků	kopiník	k1gMnPc2	kopiník
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
králova	králův	k2eAgMnSc2d1	králův
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
šestnáctiletého	šestnáctiletý	k2eAgMnSc2d1	šestnáctiletý
Eduarda	Eduard	k1gMnSc2	Eduard
<g/>
,	,	kIx,	,
prince	princ	k1gMnSc2	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
oddíl	oddíl	k1gInSc1	oddíl
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Northamptonu	Northampton	k1gInSc2	Northampton
a	a	k8xC	a
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Arundelu	Arundel	k1gInSc2	Arundel
měl	mít	k5eAaImAgInS	mít
čítat	čítat	k5eAaImF	čítat
800	[number]	k4	800
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
1	[number]	k4	1
200	[number]	k4	200
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
odřad	odřad	k1gInSc1	odřad
pod	pod	k7c7	pod
přímým	přímý	k2eAgNnSc7d1	přímé
královým	králův	k2eAgNnSc7d1	královo
velením	velení	k1gNnSc7	velení
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc7d1	stojící
na	na	k7c6	na
vyvýšené	vyvýšený	k2eAgFnSc6d1	vyvýšená
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
čítal	čítat	k5eAaImAgInS	čítat
700	[number]	k4	700
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
2	[number]	k4	2
000	[number]	k4	000
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
kronika	kronika	k1gFnSc1	kronika
Jeana	Jean	k1gMnSc2	Jean
Le	Le	k1gMnSc2	Le
Bela	Bela	k1gFnSc1	Bela
Vrayes	Vrayes	k1gMnSc1	Vrayes
Chroniques	Chroniques	k1gMnSc1	Chroniques
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
částečně	částečně	k6eAd1	částečně
čerpal	čerpat	k5eAaImAgInS	čerpat
i	i	k9	i
Froissart	Froissart	k1gInSc1	Froissart
<g/>
,	,	kIx,	,
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc1	první
sled	sled	k1gInSc1	sled
čítal	čítat	k5eAaImAgInS	čítat
1	[number]	k4	1
200	[number]	k4	200
těžkooděnců	těžkooděnec	k1gMnPc2	těžkooděnec
<g/>
,	,	kIx,	,
3	[number]	k4	3
000	[number]	k4	000
lučištníků	lučištník	k1gMnPc2	lučištník
a	a	k8xC	a
3	[number]	k4	3
000	[number]	k4	000
Walesanů	Walesan	k1gMnPc2	Walesan
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
sled	sled	k1gInSc4	sled
tvořilo	tvořit	k5eAaImAgNnS	tvořit
1	[number]	k4	1
200	[number]	k4	200
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
3	[number]	k4	3
000	[number]	k4	000
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
batailles	batailles	k1gInSc4	batailles
pak	pak	k6eAd1	pak
stálo	stát	k5eAaImAgNnS	stát
1	[number]	k4	1
600	[number]	k4	600
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
3	[number]	k4	3
000	[number]	k4	000
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
revolučním	revoluční	k2eAgInSc7d1	revoluční
počinem	počin	k1gInSc7	počin
byl	být	k5eAaImAgInS	být
králův	králův	k2eAgInSc1d1	králův
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezdci	jezdec	k1gMnPc1	jezdec
mají	mít	k5eAaImIp3nP	mít
sesednout	sesednout	k5eAaPmF	sesednout
z	z	k7c2	z
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
vyplnit	vyplnit	k5eAaPmF	vyplnit
mezery	mezera	k1gFnPc4	mezera
mezi	mezi	k7c7	mezi
pěšáky	pěšák	k1gMnPc7	pěšák
a	a	k8xC	a
lučištníky	lučištník	k1gMnPc7	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
taktický	taktický	k2eAgInSc1d1	taktický
prvek	prvek	k1gInSc1	prvek
jednak	jednak	k8xC	jednak
posílil	posílit	k5eAaPmAgInS	posílit
morálku	morálka	k1gFnSc4	morálka
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tak	tak	k6eAd1	tak
zabránil	zabránit	k5eAaPmAgMnS	zabránit
jezdcům	jezdec	k1gMnPc3	jezdec
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nepříznivého	příznivý	k2eNgInSc2d1	nepříznivý
vývoje	vývoj	k1gInSc2	vývoj
střetnutí	střetnutí	k1gNnSc4	střetnutí
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
byli	být	k5eAaImAgMnP	být
odvedeni	odvést	k5eAaPmNgMnP	odvést
do	do	k7c2	do
provizorní	provizorní	k2eAgFnSc2d1	provizorní
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
za	za	k7c7	za
pozicemi	pozice	k1gFnPc7	pozice
středu	střed	k1gInSc2	střed
a	a	k8xC	a
pravého	pravý	k2eAgNnSc2d1	pravé
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Jean	Jean	k1gMnSc1	Jean
Froissart	Froissart	k1gInSc4	Froissart
popisuje	popisovat	k5eAaImIp3nS	popisovat
postavení	postavení	k1gNnSc1	postavení
Angličanů	Angličan	k1gMnPc2	Angličan
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Mimo	mimo	k6eAd1	mimo
spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
leníků	leník	k1gMnPc2	leník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
přímými	přímý	k2eAgInPc7d1	přímý
nebo	nebo	k8xC	nebo
nepřímými	přímý	k2eNgInPc7d1	nepřímý
vazaly	vazal	k1gMnPc4	vazal
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
vojsko	vojsko	k1gNnSc4	vojsko
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
muži	muž	k1gMnPc1	muž
povolaní	povolaný	k2eAgMnPc1d1	povolaný
výzvou	výzva	k1gFnSc7	výzva
arriere	arrirat	k5eAaPmIp3nS	arrirat
<g/>
-	-	kIx~	-
<g/>
ban	ban	k?	ban
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
či	či	k8xC	či
dodatečná	dodatečný	k2eAgFnSc1d1	dodatečná
výzva	výzva	k1gFnSc1	výzva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc3	ten
byli	být	k5eAaImAgMnP	být
povinni	povinen	k2eAgMnPc1d1	povinen
uposlechnout	uposlechnout	k5eAaPmF	uposlechnout
všichni	všechen	k3xTgMnPc1	všechen
bojeschopní	bojeschopný	k2eAgMnPc1d1	bojeschopný
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
postižených	postižený	k2eAgFnPc2d1	postižená
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
výzva	výzva	k1gFnSc1	výzva
podobala	podobat	k5eAaImAgFnS	podobat
moderní	moderní	k2eAgFnSc4d1	moderní
mobilizaci	mobilizace	k1gFnSc4	mobilizace
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mnohdy	mnohdy	k6eAd1	mnohdy
znamenala	znamenat	k5eAaImAgFnS	znamenat
spíše	spíše	k9	spíše
kvantitu	kvantita	k1gFnSc4	kvantita
nežli	nežli	k8xS	nežli
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hlavní	hlavní	k2eAgFnSc4d1	hlavní
masu	masa	k1gFnSc4	masa
neurozené	urozený	k2eNgFnSc2d1	neurozená
části	část	k1gFnSc2	část
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
tvořili	tvořit	k5eAaImAgMnP	tvořit
často	často	k6eAd1	často
lidé	člověk	k1gMnPc1	člověk
nevycvičení	vycvičený	k2eNgMnPc1d1	nevycvičený
a	a	k8xC	a
nezkušení	zkušený	k2eNgMnPc1d1	nezkušený
<g/>
.	.	kIx.	.
</s>
<s>
Všem	všecek	k3xTgMnPc3	všecek
mužům	muž	k1gMnPc3	muž
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
vojska	vojsko	k1gNnSc2	vojsko
náležely	náležet	k5eAaImAgInP	náležet
pevně	pevně	k6eAd1	pevně
stanovené	stanovený	k2eAgFnPc1d1	stanovená
denní	denní	k2eAgFnPc1d1	denní
dávky	dávka	k1gFnPc1	dávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ve	v	k7c4	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
představovaly	představovat	k5eAaImAgInP	představovat
20	[number]	k4	20
sous	sous	k6eAd1	sous
pro	pro	k7c4	pro
korouhevní	korouhevní	k2eAgMnPc4d1	korouhevní
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
,	,	kIx,	,
10	[number]	k4	10
sous	sousa	k1gFnPc2	sousa
pro	pro	k7c4	pro
rytíře	rytíř	k1gMnSc4	rytíř
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
sous	sousa	k1gFnPc2	sousa
pro	pro	k7c4	pro
panoše	panoše	k1gNnSc4	panoše
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc4	vojsko
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
však	však	k9	však
postrádalo	postrádat	k5eAaImAgNnS	postrádat
pevnou	pevný	k2eAgFnSc4d1	pevná
strukturu	struktura	k1gFnSc4	struktura
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
Vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
osobně	osobně	k6eAd1	osobně
velel	velet	k5eAaImAgMnS	velet
<g/>
,	,	kIx,	,
jedinými	jediný	k2eAgNnPc7d1	jediné
jeho	jeho	k3xOp3gMnPc7	jeho
pravidelnými	pravidelný	k2eAgMnPc7d1	pravidelný
důstojníky	důstojník	k1gMnPc7	důstojník
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
konstábl	konstábl	k1gMnSc1	konstábl
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
maršálkové	maršálek	k1gMnPc1	maršálek
s	s	k7c7	s
blíže	blízce	k6eAd2	blízce
neurčenými	určený	k2eNgFnPc7d1	neurčená
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
vojenských	vojenský	k2eAgNnPc2d1	vojenské
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
se	se	k3xPyFc4	se
rodila	rodit	k5eAaImAgFnS	rodit
na	na	k7c4	na
králem	král	k1gMnSc7	král
svolávaných	svolávaný	k2eAgFnPc6d1	svolávaná
poradách	porada	k1gFnPc6	porada
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
francouzského	francouzský	k2eAgNnSc2d1	francouzské
vojska	vojsko	k1gNnSc2	vojsko
nehovoří	hovořit	k5eNaImIp3nP	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
údajem	údaj	k1gInSc7	údaj
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
opřít	opřít	k5eAaPmF	opřít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
konstatování	konstatování	k1gNnSc1	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
anglického	anglický	k2eAgMnSc4d1	anglický
rytíře	rytíř	k1gMnSc4	rytíř
připadalo	připadat	k5eAaImAgNnS	připadat
až	až	k9	až
osm	osm	k4xCc1	osm
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
odhad	odhad	k1gInSc1	odhad
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
zveličený	zveličený	k2eAgInSc1d1	zveličený
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francouzi	Francouz	k1gMnPc1	Francouz
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
ve	v	k7c6	v
výhodě	výhoda	k1gFnSc6	výhoda
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
pak	pak	k6eAd1	pak
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
protivníka	protivník	k1gMnSc2	protivník
tvořila	tvořit	k5eAaImAgFnS	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
francouzských	francouzský	k2eAgInPc2d1	francouzský
vojů	voj	k1gInPc2	voj
těžká	těžký	k2eAgFnSc1d1	těžká
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
reprezentovaná	reprezentovaný	k2eAgFnSc1d1	reprezentovaná
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
rytíři	rytíř	k1gMnPc7	rytíř
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
družinami	družina	k1gFnPc7	družina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kopí	kopí	k1gNnSc1	kopí
nebo	nebo	k8xC	nebo
meče	meč	k1gInSc2	meč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
doplňovaly	doplňovat	k5eAaImAgFnP	doplňovat
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
pěší	pěší	k2eAgInPc1d1	pěší
oddíly	oddíl	k1gInPc1	oddíl
a	a	k8xC	a
italští	italský	k2eAgMnPc1d1	italský
pavézníci	pavézník	k1gMnPc1	pavézník
a	a	k8xC	a
střelci	střelec	k1gMnPc1	střelec
z	z	k7c2	z
kuší	kuše	k1gFnPc2	kuše
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
tento	tento	k3xDgInSc1	tento
šestitisícový	šestitisícový	k2eAgInSc1d1	šestitisícový
sbor	sbor	k1gInSc1	sbor
vedený	vedený	k2eAgInSc1d1	vedený
kondotiéry	kondotiér	k1gMnPc7	kondotiér
Odonem	Odon	k1gMnSc7	Odon
Doriou	Doria	k1gMnSc7	Doria
a	a	k8xC	a
Carlem	Carl	k1gMnSc7	Carl
Grimaldim	Grimaldima	k1gFnPc2	Grimaldima
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Janované	Janovan	k1gMnPc5	Janovan
<g/>
,	,	kIx,	,
pocházeli	pocházet	k5eAaImAgMnP	pocházet
jeho	jeho	k3xOp3gMnPc1	jeho
příslušníci	příslušník	k1gMnPc1	příslušník
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
i	i	k9	i
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Bitvy	bitva	k1gFnPc1	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
případům	případ	k1gInPc3	případ
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
jako	jako	k9	jako
žoldnéři	žoldnér	k1gMnPc1	žoldnér
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
pomocný	pomocný	k2eAgInSc1d1	pomocný
kontingent	kontingent	k1gInSc1	kontingent
tradičního	tradiční	k2eAgMnSc2d1	tradiční
spojence	spojenec	k1gMnSc2	spojenec
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
událostech	událost	k1gFnPc6	událost
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
hovoří	hovořit	k5eAaImIp3nS	hovořit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejzevrubnější	zevrubný	k2eAgInSc1d3	zevrubný
popis	popis	k1gInSc1	popis
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kronik	kronika	k1gFnPc2	kronika
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Flander	Flandry	k1gInPc2	Flandry
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
francouzsky	francouzsky	k6eAd1	francouzsky
píšícího	píšící	k2eAgMnSc2d1	píšící
henegavského	henegavský	k2eAgMnSc2d1	henegavský
kronikáře	kronikář	k1gMnSc2	kronikář
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
své	svůj	k3xOyFgFnPc4	svůj
informace	informace	k1gFnPc4	informace
získával	získávat	k5eAaImAgMnS	získávat
především	především	k9	především
od	od	k7c2	od
přímých	přímý	k2eAgMnPc2d1	přímý
pamětníků	pamětník	k1gMnPc2	pamětník
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Časově	časově	k6eAd1	časově
bližší	blízký	k2eAgFnSc6d2	bližší
relaci	relace	k1gFnSc6	relace
však	však	k9	však
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
letopis	letopis	k1gInSc1	letopis
Vrayes	Vrayesa	k1gFnPc2	Vrayesa
Chroniques	Chroniquesa	k1gFnPc2	Chroniquesa
(	(	kIx(	(
<g/>
Pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
kroniky	kronika	k1gFnPc1	kronika
<g/>
)	)	kIx)	)
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
vlámského	vlámský	k2eAgMnSc2d1	vlámský
kronikáře	kronikář	k1gMnSc2	kronikář
Jeana	Jean	k1gMnSc2	Jean
Le	Le	k1gFnSc1	Le
Bela	Bela	k1gFnSc1	Bela
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
zachytil	zachytit	k5eAaPmAgInS	zachytit
éru	éra	k1gFnSc4	éra
panování	panování	k1gNnSc2	panování
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
o	o	k7c6	o
bitvě	bitva	k1gFnSc6	bitva
informuje	informovat	k5eAaBmIp3nS	informovat
Kronika	kronika	k1gFnSc1	kronika
pražského	pražský	k2eAgInSc2d1	pražský
kostela	kostel	k1gInSc2	kostel
od	od	k7c2	od
Beneš	Beneš	k1gMnSc1	Beneš
Krabice	krabice	k1gFnSc1	krabice
z	z	k7c2	z
Weitmile	Weitmil	k1gMnSc5	Weitmil
(	(	kIx(	(
<g/>
kterého	který	k3yQgMnSc4	který
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
informovali	informovat	k5eAaBmAgMnP	informovat
zpravodajové	zpravodaj	k1gMnPc1	zpravodaj
z	z	k7c2	z
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kronika	kronika	k1gFnSc1	kronika
Františka	František	k1gMnSc2	František
Pražského	pražský	k2eAgMnSc2d1	pražský
a	a	k8xC	a
Stručné	stručný	k2eAgNnSc1d1	stručné
sepsání	sepsání	k1gNnSc1	sepsání
kroniky	kronika	k1gFnSc2	kronika
římské	římský	k2eAgFnSc3d1	římská
a	a	k8xC	a
české	český	k2eAgFnSc3d1	Česká
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Jana	Jan	k1gMnSc2	Jan
Neplacha	Neplach	k1gMnSc2	Neplach
<g/>
.	.	kIx.	.
</s>
<s>
Důkladným	důkladný	k2eAgFnPc3d1	důkladná
přípravám	příprava	k1gFnPc3	příprava
na	na	k7c4	na
bitvu	bitva	k1gFnSc4	bitva
se	se	k3xPyFc4	se
Angličané	Angličan	k1gMnPc1	Angličan
věnovali	věnovat	k5eAaImAgMnP	věnovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
pátek	pátek	k1gInSc4	pátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Navečer	navečer	k6eAd1	navečer
pak	pak	k6eAd1	pak
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
pozval	pozvat	k5eAaPmAgMnS	pozvat
nejvýznačnější	význačný	k2eAgMnPc4d3	nejvýznačnější
šlechtice	šlechtic	k1gMnPc4	šlechtic
na	na	k7c4	na
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
následovala	následovat	k5eAaImAgFnS	následovat
modlitba	modlitba	k1gFnSc1	modlitba
u	u	k7c2	u
polního	polní	k2eAgInSc2d1	polní
oltáře	oltář	k1gInSc2	oltář
uvnitř	uvnitř	k7c2	uvnitř
králova	králův	k2eAgInSc2d1	králův
stanu	stan	k1gInSc2	stan
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
strávit	strávit	k5eAaPmF	strávit
v	v	k7c6	v
Abbeville	Abbevilla	k1gFnSc6	Abbevilla
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
pozicemi	pozice	k1gFnPc7	pozice
protivníka	protivník	k1gMnSc2	protivník
patrně	patrně	k6eAd1	patrně
obeznámen	obeznámit	k5eAaPmNgMnS	obeznámit
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
vyslal	vyslat	k5eAaPmAgMnS	vyslat
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
své	svůj	k3xOyFgFnSc2	svůj
dva	dva	k4xCgInPc1	dva
marešaly	marešal	k1gInPc1	marešal
Charlese	Charles	k1gMnSc2	Charles
z	z	k7c2	z
Montmorency	Montmorenca	k1gMnSc2	Montmorenca
a	a	k8xC	a
pana	pan	k1gMnSc2	pan
de	de	k?	de
Saint	Saint	k1gMnSc1	Saint
Venant	Venant	k1gMnSc1	Venant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
devatenáctikilometrový	devatenáctikilometrový	k2eAgInSc4d1	devatenáctikilometrový
pochod	pochod	k1gInSc4	pochod
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jej	on	k3xPp3gMnSc4	on
dělil	dělit	k5eAaImAgMnS	dělit
od	od	k7c2	od
Krečaku	Krečak	k1gInSc2	Krečak
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
východu	východ	k1gInSc6	východ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
několik	několik	k4yIc1	několik
svých	svůj	k3xOyFgMnPc2	svůj
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
obhlédnout	obhlédnout	k5eAaPmF	obhlédnout
anglické	anglický	k2eAgNnSc4d1	anglické
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
těmito	tento	k3xDgFnPc7	tento
muži	muž	k1gMnSc3	muž
byli	být	k5eAaImAgMnP	být
páni	pan	k1gMnPc1	pan
de	de	k?	de
Noyers	Noyers	k1gInSc1	Noyers
<g/>
,	,	kIx,	,
de	de	k?	de
Beaujeu	Beaujea	k1gFnSc4	Beaujea
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aubigny	Aubigna	k1gMnSc2	Aubigna
a	a	k8xC	a
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
překládáno	překládat	k5eAaImNgNnS	překládat
jako	jako	k8xC	jako
Mnich	mnich	k1gInSc1	mnich
Basilejský	basilejský	k2eAgInSc1d1	basilejský
<g/>
.	.	kIx.	.
</s>
<s>
Zvědové	zvěd	k1gMnPc1	zvěd
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
upozornili	upozornit	k5eAaPmAgMnP	upozornit
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angličané	Angličan	k1gMnPc1	Angličan
drží	držet	k5eAaImIp3nP	držet
silně	silně	k6eAd1	silně
hájené	hájený	k2eAgFnPc4d1	hájená
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nejprve	nejprve	k6eAd1	nejprve
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
své	svůj	k3xOyFgFnPc4	svůj
rozptýlené	rozptýlený	k2eAgFnPc4d1	rozptýlená
a	a	k8xC	a
dezorganizované	dezorganizovaný	k2eAgFnPc4d1	dezorganizovaná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
až	až	k6eAd1	až
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Froissart	Froissart	k1gInSc1	Froissart
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Le	Le	k1gFnSc2	Le
Bel	Bela	k1gFnPc2	Bela
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
dal	dát	k5eAaPmAgMnS	dát
svým	svůj	k3xOyFgMnPc3	svůj
rytířům	rytíř	k1gMnPc3	rytíř
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
vyvolat	vyvolat	k5eAaPmF	vyvolat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
korouhvi	korouhev	k1gFnSc3	korouhev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
jednotkami	jednotka	k1gFnPc7	jednotka
díky	díky	k7c3	díky
absolutní	absolutní	k2eAgFnSc3d1	absolutní
nekázni	nekázeň	k1gFnSc3	nekázeň
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
chaos	chaos	k1gInSc1	chaos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
Filipovi	Filip	k1gMnSc3	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
již	již	k6eAd1	již
nepodařil	podařit	k5eNaPmAgMnS	podařit
zvládnout	zvládnout	k5eAaPmF	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
situaci	situace	k1gFnSc6	situace
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
všeobecné	všeobecný	k2eAgFnSc3d1	všeobecná
náladě	nálada	k1gFnSc3	nálada
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vzpěnila	vzpěnit	k5eAaPmAgFnS	vzpěnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolem	kolem	k7c2	kolem
páté	pátý	k4xOgFnSc2	pátý
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
nešporami	nešpora	k1gFnPc7	nešpora
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
povel	povel	k1gInSc1	povel
k	k	k7c3	k
iracionálnímu	iracionální	k2eAgMnSc3d1	iracionální
a	a	k8xC	a
sebevražednému	sebevražedný	k2eAgInSc3d1	sebevražedný
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
straně	strana	k1gFnSc6	strana
veškeré	veškerý	k3xTgFnSc2	veškerý
nedělní	nedělní	k2eAgFnSc2d1	nedělní
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
bitvu	bitva	k1gFnSc4	bitva
probíhaly	probíhat	k5eAaImAgInP	probíhat
v	v	k7c6	v
poklidu	poklid	k1gInSc6	poklid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ranní	ranní	k2eAgFnSc6d1	ranní
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
nejprve	nejprve	k6eAd1	nejprve
promluvil	promluvit	k5eAaPmAgInS	promluvit
s	s	k7c7	s
prostými	prostý	k2eAgMnPc7d1	prostý
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
nechal	nechat	k5eAaPmAgMnS	nechat
celé	celý	k2eAgNnSc4d1	celé
vojsko	vojsko	k1gNnSc4	vojsko
sešikovat	sešikovat	k5eAaPmF	sešikovat
do	do	k7c2	do
bitevních	bitevní	k2eAgFnPc2d1	bitevní
formací	formace	k1gFnPc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
desáté	desátá	k1gFnSc2	desátá
hodiny	hodina	k1gFnSc2	hodina
pak	pak	k6eAd1	pak
pronesl	pronést	k5eAaPmAgMnS	pronést
krátký	krátký	k2eAgInSc4d1	krátký
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
své	svůj	k3xOyFgMnPc4	svůj
poddané	poddaný	k1gMnPc4	poddaný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zmužile	zmužile	k6eAd1	zmužile
hájili	hájit	k5eAaImAgMnP	hájit
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
anglické	anglický	k2eAgFnSc2d1	anglická
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
na	na	k7c4	na
výšiny	výšina	k1gFnPc4	výšina
poblíž	poblíž	k7c2	poblíž
kresčackého	kresčacký	k2eAgInSc2d1	kresčacký
mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zřídil	zřídit	k5eAaPmAgMnS	zřídit
své	svůj	k3xOyFgNnSc4	svůj
velitelské	velitelský	k2eAgNnSc4d1	velitelské
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
protivník	protivník	k1gMnSc1	protivník
dorazí	dorazit	k5eAaPmIp3nS	dorazit
až	až	k9	až
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
povolil	povolit	k5eAaPmAgInS	povolit
vojákům	voják	k1gMnPc3	voják
usednout	usednout	k5eAaPmF	usednout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
odložit	odložit	k5eAaPmF	odložit
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
helmu	helma	k1gFnSc4	helma
a	a	k8xC	a
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
komukoliv	kdokoliv	k3yInSc3	kdokoliv
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
opustit	opustit	k5eAaPmF	opustit
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
šiku	šik	k1gInSc6	šik
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
proti	proti	k7c3	proti
disciplinovaným	disciplinovaný	k2eAgMnPc3d1	disciplinovaný
Angličanům	Angličan	k1gMnPc3	Angličan
nasadit	nasadit	k5eAaPmF	nasadit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
janovští	janovský	k2eAgMnPc1d1	janovský
střelci	střelec	k1gMnPc1	střelec
z	z	k7c2	z
kuší	kuše	k1gFnPc2	kuše
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťovat	zajišťovat	k5eAaImF	zajišťovat
je	on	k3xPp3gInPc4	on
měla	mít	k5eAaImAgFnS	mít
čelní	čelní	k2eAgFnSc1d1	čelní
linie	linie	k1gFnSc1	linie
těžké	těžký	k2eAgFnSc2d1	těžká
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
řady	řada	k1gFnPc1	řada
byly	být	k5eAaImAgFnP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
i	i	k9	i
pěšími	pěší	k2eAgMnPc7d1	pěší
sudličníky	sudličník	k1gMnPc7	sudličník
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
byli	být	k5eAaImAgMnP	být
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
pochodem	pochod	k1gInSc7	pochod
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pavézy	pavéza	k1gFnSc2	pavéza
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimiž	jenž	k3xRgMnPc7	jenž
obvykle	obvykle	k6eAd1	obvykle
nabíjeli	nabíjet	k5eAaImAgMnP	nabíjet
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
seřadili	seřadit	k5eAaPmAgMnP	seřadit
a	a	k8xC	a
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
kronikářů	kronikář	k1gMnPc2	kronikář
informují	informovat	k5eAaBmIp3nP	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
spustila	spustit	k5eAaPmAgFnS	spustit
prudká	prudký	k2eAgFnSc1d1	prudká
letní	letní	k2eAgFnSc1d1	letní
bouřka	bouřka	k1gFnSc1	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
vyjasnilo	vyjasnit	k5eAaPmAgNnS	vyjasnit
<g/>
,	,	kIx,	,
Janované	Janovan	k1gMnPc1	Janovan
za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
trubek	trubka	k1gFnPc2	trubka
a	a	k8xC	a
bubnů	buben	k1gInPc2	buben
postupně	postupně	k6eAd1	postupně
třikrát	třikrát	k6eAd1	třikrát
vykročili	vykročit	k5eAaPmAgMnP	vykročit
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
sborem	sborem	k6eAd1	sborem
vykřikli	vykřiknout	k5eAaPmAgMnP	vykřiknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
protivníkovi	protivníkův	k2eAgMnPc1d1	protivníkův
nahnali	nahnat	k5eAaPmAgMnP	nahnat
strach	strach	k1gInSc4	strach
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
přinutili	přinutit	k5eAaPmAgMnP	přinutit
vyrazit	vyrazit	k5eAaPmF	vyrazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
pokřiku	pokřik	k1gInSc6	pokřik
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
anglických	anglický	k2eAgFnPc2d1	anglická
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
napjali	napnout	k5eAaPmAgMnP	napnout
své	svůj	k3xOyFgFnPc4	svůj
kuše	kuše	k1gFnPc4	kuše
a	a	k8xC	a
vystřelili	vystřelit	k5eAaPmAgMnP	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
deště	dešť	k1gInPc4	dešť
prý	prý	k9	prý
tětivy	tětiva	k1gFnPc4	tětiva
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zbraní	zbraň	k1gFnPc2	zbraň
sundali	sundat	k5eAaPmAgMnP	sundat
a	a	k8xC	a
schovali	schovat	k5eAaPmAgMnP	schovat
pod	pod	k7c4	pod
přilbice	přilbice	k1gFnPc4	přilbice
<g/>
,	,	kIx,	,
nasákly	nasáknout	k5eAaPmAgFnP	nasáknout
tětivy	tětiva	k1gFnPc1	tětiva
kušičníků	kušičník	k1gInPc2	kušičník
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
ztratily	ztratit	k5eAaPmAgFnP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
jejich	jejich	k3xOp3gFnSc1	jejich
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
Janovanů	Janovan	k1gMnPc2	Janovan
navíc	navíc	k6eAd1	navíc
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
nutnost	nutnost	k1gFnSc1	nutnost
střílet	střílet	k5eAaImF	střílet
do	do	k7c2	do
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
ostré	ostrý	k2eAgNnSc1d1	ostré
slunce	slunce	k1gNnSc1	slunce
svítící	svítící	k2eAgNnSc1d1	svítící
jim	on	k3xPp3gFnPc3	on
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
salvě	salva	k1gFnSc6	salva
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
do	do	k7c2	do
boje	boj	k1gInSc2	boj
zapojili	zapojit	k5eAaPmAgMnP	zapojit
angličtí	anglický	k2eAgMnPc1d1	anglický
lučištníci	lučištník	k1gMnPc1	lučištník
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
snad	snad	k9	snad
podpořila	podpořit	k5eAaPmAgFnS	podpořit
i	i	k9	i
artilerie	artilerie	k1gFnSc1	artilerie
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
odpověď	odpověď	k1gFnSc1	odpověď
byla	být	k5eAaImAgFnS	být
zničující	zničující	k2eAgFnSc1d1	zničující
<g/>
.	.	kIx.	.
</s>
<s>
Zaskočení	zaskočený	k2eAgMnPc1d1	zaskočený
Janované	Janovan	k1gMnPc1	Janovan
po	po	k7c6	po
několika	několik	k4yIc6	několik
salvách	salva	k1gFnPc6	salva
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c6	v
panice	panika	k1gFnSc6	panika
opouštět	opouštět	k5eAaImF	opouštět
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
dal	dát	k5eAaPmAgMnS	dát
povel	povel	k1gInSc4	povel
těžkooděncům	těžkooděnec	k1gMnPc3	těžkooděnec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
začali	začít	k5eAaPmAgMnP	začít
prchající	prchající	k2eAgFnSc4d1	prchající
pěchotu	pěchota	k1gFnSc4	pěchota
pobíjet	pobíjet	k5eAaImF	pobíjet
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
se	se	k3xPyFc4	se
chopilo	chopit	k5eAaPmAgNnS	chopit
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
zápalem	zápal	k1gInSc7	zápal
a	a	k8xC	a
od	od	k7c2	od
masakrování	masakrování	k1gNnSc2	masakrování
spojenců	spojenec	k1gMnPc2	spojenec
je	být	k5eAaImIp3nS	být
neodradila	odradit	k5eNaPmAgFnS	odradit
ani	ani	k8xC	ani
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
palba	palba	k1gFnSc1	palba
lučištníků	lučištník	k1gMnPc2	lučištník
z	z	k7c2	z
protilehlého	protilehlý	k2eAgNnSc2d1	protilehlé
návrší	návrší	k1gNnSc2	návrší
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
bylo	být	k5eAaImAgNnS	být
krvavé	krvavý	k2eAgNnSc1d1	krvavé
dílo	dílo	k1gNnSc1	dílo
završeno	završen	k2eAgNnSc1d1	završeno
<g/>
,	,	kIx,	,
rozjeli	rozjet	k5eAaPmAgMnP	rozjet
se	se	k3xPyFc4	se
nedisciplinovaní	disciplinovaný	k2eNgMnPc1d1	nedisciplinovaný
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velel	velet	k5eAaImAgMnS	velet
králův	králův	k2eAgMnSc1d1	králův
bratr	bratr	k1gMnSc1	bratr
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
<g/>
,	,	kIx,	,
do	do	k7c2	do
svahu	svah	k1gInSc2	svah
proti	proti	k7c3	proti
Angličanům	Angličan	k1gMnPc3	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Nepřetržitý	přetržitý	k2eNgInSc1d1	nepřetržitý
příval	příval	k1gInSc1	příval
šípů	šíp	k1gInPc2	šíp
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
jejich	jejich	k3xOp3gFnPc4	jejich
řady	řada	k1gFnPc4	řada
okamžitě	okamžitě	k6eAd1	okamžitě
decimovat	decimovat	k5eAaBmF	decimovat
a	a	k8xC	a
málokterý	málokterý	k3yIgMnSc1	málokterý
z	z	k7c2	z
Francouzů	Francouz	k1gMnPc2	Francouz
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
přímé	přímý	k2eAgFnSc2d1	přímá
konfrontace	konfrontace	k1gFnSc2	konfrontace
s	s	k7c7	s
protivníkovými	protivníkův	k2eAgMnPc7d1	protivníkův
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
zneškodnili	zneškodnit	k5eAaPmAgMnP	zneškodnit
angličtí	anglický	k2eAgMnPc1d1	anglický
rytíři	rytíř	k1gMnPc1	rytíř
nebo	nebo	k8xC	nebo
walesští	walesský	k1gMnPc1	walesský
a	a	k8xC	a
irští	irský	k2eAgMnPc1d1	irský
pěšáci	pěšák	k1gMnPc1	pěšák
<g/>
,	,	kIx,	,
vyzbrojení	vyzbrojení	k1gNnSc1	vyzbrojení
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
noži	nůž	k1gInPc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bitevní	bitevní	k2eAgNnSc4d1	bitevní
pole	pole	k1gNnSc4	pole
začaly	začít	k5eAaPmAgFnP	začít
postupně	postupně	k6eAd1	postupně
dorážet	dorážet	k5eAaImF	dorážet
nové	nový	k2eAgInPc4d1	nový
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
kontingenty	kontingent	k1gInPc4	kontingent
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zapojovaly	zapojovat	k5eAaImAgInP	zapojovat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
rovnou	rovnou	k6eAd1	rovnou
z	z	k7c2	z
pochodu	pochod	k1gInSc2	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
texty	text	k1gInPc1	text
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
Angličanům	Angličan	k1gMnPc3	Angličan
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
patnáct	patnáct	k4xCc4	patnáct
či	či	k8xC	či
šestnáct	šestnáct	k4xCc4	šestnáct
podobně	podobně	k6eAd1	podobně
chaotických	chaotický	k2eAgInPc2d1	chaotický
nájezdů	nájezd	k1gInPc2	nájezd
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
všechny	všechen	k3xTgFnPc1	všechen
byly	být	k5eAaImAgFnP	být
zakončeny	zakončen	k2eAgInPc1d1	zakončen
prakticky	prakticky	k6eAd1	prakticky
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přesnější	přesný	k2eAgInSc4d2	přesnější
popis	popis	k1gInSc4	popis
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
několik	několik	k4yIc1	několik
epizod	epizoda	k1gFnPc2	epizoda
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
průběhu	průběh	k1gInSc2	průběh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zevrubněji	zevrubně	k6eAd2	zevrubně
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
korektní	korektní	k2eAgFnSc6d1	korektní
časové	časový	k2eAgFnSc6d1	časová
posloupnosti	posloupnost	k1gFnSc6	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
některé	některý	k3yIgMnPc4	některý
z	z	k7c2	z
dobových	dobový	k2eAgFnPc2d1	dobová
relací	relace	k1gFnPc2	relace
přivedly	přivést	k5eAaPmAgInP	přivést
část	část	k1gFnSc4	část
historiků	historik	k1gMnPc2	historik
(	(	kIx(	(
<g/>
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Nicolle	Nicolle	k1gFnSc1	Nicolle
<g/>
)	)	kIx)	)
k	k	k7c3	k
domněnce	domněnka	k1gFnSc3	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
chronologii	chronologie	k1gFnSc6	chronologie
uvažovat	uvažovat	k5eAaImF	uvažovat
lze	lze	k6eAd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
první	první	k4xOgInSc4	první
nápor	nápor	k1gInSc4	nápor
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc1d1	směřující
proti	proti	k7c3	proti
korouhvi	korouhev	k1gFnSc3	korouhev
prince	princ	k1gMnSc2	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
těžkooděnci	těžkooděnec	k1gMnPc7	těžkooděnec
po	po	k7c6	po
vysokých	vysoký	k2eAgFnPc6d1	vysoká
ztrátách	ztráta	k1gFnPc6	ztráta
projeli	projet	k5eAaPmAgMnP	projet
či	či	k8xC	či
prorazili	prorazit	k5eAaPmAgMnP	prorazit
anglickými	anglický	k2eAgMnPc7d1	anglický
lučištníky	lučištník	k1gMnPc7	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Protivníkovy	protivníkův	k2eAgFnPc1d1	protivníkova
řady	řada	k1gFnPc1	řada
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
opět	opět	k6eAd1	opět
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
a	a	k8xC	a
králův	králův	k2eAgMnSc1d1	králův
bratr	bratr	k1gMnSc1	bratr
uvázl	uváznout	k5eAaPmAgMnS	uváznout
v	v	k7c6	v
těžkém	těžký	k2eAgInSc6d1	těžký
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zápolení	zápolení	k1gNnSc1	zápolení
zřejmě	zřejmě	k6eAd1	zřejmě
velmi	velmi	k6eAd1	velmi
závažně	závažně	k6eAd1	závažně
narušilo	narušit	k5eAaPmAgNnS	narušit
anglickou	anglický	k2eAgFnSc4d1	anglická
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
princovy	princův	k2eAgFnPc1d1	princova
potíže	potíž	k1gFnPc1	potíž
vyburcovaly	vyburcovat	k5eAaPmAgFnP	vyburcovat
k	k	k7c3	k
aktivitě	aktivita	k1gFnSc3	aktivita
velitele	velitel	k1gMnSc2	velitel
druhé	druhý	k4xOgFnSc2	druhý
bataille	bataille	k1gFnSc2	bataille
hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Northamptonu	Northampton	k1gInSc2	Northampton
a	a	k8xC	a
Arundelu	Arundel	k1gInSc2	Arundel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
tvrdě	tvrdě	k6eAd1	tvrdě
bojujícím	bojující	k2eAgFnPc3d1	bojující
Alençonovým	Alençonův	k2eAgFnPc3d1	Alençonův
jednotkám	jednotka	k1gFnPc3	jednotka
do	do	k7c2	do
boku	bok	k1gInSc2	bok
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
do	do	k7c2	do
boje	boj	k1gInSc2	boj
zasáhly	zasáhnout	k5eAaPmAgInP	zasáhnout
i	i	k9	i
oddíly	oddíl	k1gInPc1	oddíl
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Slepý	slepý	k2eAgMnSc1d1	slepý
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
informoval	informovat	k5eAaBmAgInS	informovat
jeho	jeho	k3xOp3gInSc1	jeho
doprovod	doprovod	k1gInSc1	doprovod
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
patrně	patrně	k6eAd1	patrně
ihned	ihned	k6eAd1	ihned
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
do	do	k7c2	do
jakých	jaký	k3yIgFnPc2	jaký
potíží	potíž	k1gFnPc2	potíž
se	se	k3xPyFc4	se
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
dostal	dostat	k5eAaPmAgMnS	dostat
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
nájezdu	nájezd	k1gInSc2	nájezd
se	se	k3xPyFc4	se
dotázal	dotázat	k5eAaPmAgMnS	dotázat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
poručil	poručit	k5eAaPmAgMnS	poručit
dvěma	dva	k4xCgFnPc7	dva
svým	svůj	k3xOyFgMnPc3	svůj
rytířům	rytíř	k1gMnPc3	rytíř
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přivázali	přivázat	k5eAaPmAgMnP	přivázat
uzdy	uzda	k1gFnPc4	uzda
svých	svůj	k3xOyFgMnPc2	svůj
koní	kůň	k1gMnPc2	kůň
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
koni	kůň	k1gMnPc1	kůň
a	a	k8xC	a
vedli	vést	k5eAaImAgMnP	vést
jej	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
bitevní	bitevní	k2eAgFnSc2d1	bitevní
vřavy	vřava	k1gFnSc2	vřava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
nepřátele	nepřítel	k1gMnPc4	nepřítel
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
útok	útok	k1gInSc1	útok
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
zpočátku	zpočátku	k6eAd1	zpočátku
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
princ	princ	k1gMnSc1	princ
Eduard	Eduard	k1gMnSc1	Eduard
dvakrát	dvakrát	k6eAd1	dvakrát
klesl	klesnout	k5eAaPmAgMnS	klesnout
na	na	k7c4	na
kolena	koleno	k1gNnPc4	koleno
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
jej	on	k3xPp3gMnSc4	on
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
teprve	teprve	k6eAd1	teprve
jeho	jeho	k3xOp3gMnSc1	jeho
korouhevník	korouhevník	k1gMnSc1	korouhevník
Richard	Richard	k1gMnSc1	Richard
Fitz	Fitz	k1gMnSc1	Fitz
Simonn	Simonn	k1gMnSc1	Simonn
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
anglické	anglický	k2eAgFnPc4d1	anglická
bataille	bataille	k1gFnPc4	bataille
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
oddílů	oddíl	k1gInPc2	oddíl
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
zakolísaly	zakolísat	k5eAaPmAgFnP	zakolísat
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rytířů	rytíř	k1gMnPc2	rytíř
z	z	k7c2	z
princovy	princův	k2eAgFnSc2d1	princova
družiny	družina	k1gFnSc2	družina
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
ke	k	k7c3	k
králi	král	k1gMnSc3	král
Eduardovi	Eduard	k1gMnSc3	Eduard
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
měl	mít	k5eAaImAgMnS	mít
z	z	k7c2	z
vyššího	vysoký	k2eAgNnSc2d2	vyšší
postavení	postavení	k1gNnSc2	postavení
patrně	patrně	k6eAd1	patrně
dobrý	dobrý	k2eAgInSc1d1	dobrý
přehled	přehled	k1gInSc1	přehled
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
a	a	k8xC	a
vyhodnotil	vyhodnotit	k5eAaPmAgMnS	vyhodnotit
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
nemá	mít	k5eNaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
razanci	razance	k1gFnSc4	razance
a	a	k8xC	a
pozvolna	pozvolna	k6eAd1	pozvolna
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
odhad	odhad	k1gInSc1	odhad
byl	být	k5eAaImAgInS	být
správný	správný	k2eAgMnSc1d1	správný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
protivník	protivník	k1gMnSc1	protivník
byl	být	k5eAaImAgMnS	být
pomalu	pomalu	k6eAd1	pomalu
vytlačován	vytlačován	k2eAgMnSc1d1	vytlačován
z	z	k7c2	z
dobytých	dobytý	k2eAgFnPc2d1	dobytá
pozic	pozice	k1gFnPc2	pozice
a	a	k8xC	a
výkvět	výkvět	k1gInSc1	výkvět
francouzské	francouzský	k2eAgFnSc2d1	francouzská
a	a	k8xC	a
říšské	říšský	k2eAgFnSc2d1	říšská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
<g/>
,	,	kIx,	,
umíral	umírat	k5eAaImAgMnS	umírat
na	na	k7c6	na
rozbahněné	rozbahněný	k2eAgFnSc6d1	rozbahněná
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
svým	svůj	k3xOyFgNnPc3	svůj
slovům	slovo	k1gNnPc3	slovo
však	však	k9	však
v	v	k7c6	v
tichosti	tichost	k1gFnSc6	tichost
vyslal	vyslat	k5eAaPmAgMnS	vyslat
k	k	k7c3	k
princi	princ	k1gMnSc3	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
posilu	posila	k1gFnSc4	posila
dvacet	dvacet	k4xCc1	dvacet
rytířů	rytíř	k1gMnPc2	rytíř
vedených	vedený	k2eAgMnPc2d1	vedený
biskupem	biskup	k1gInSc7	biskup
z	z	k7c2	z
Durhamu	Durham	k1gInSc2	Durham
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
francouzské	francouzský	k2eAgInPc1d1	francouzský
nájezdy	nájezd	k1gInPc1	nájezd
již	již	k6eAd1	již
ztratily	ztratit	k5eAaPmAgFnP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
pravidelnost	pravidelnost	k1gFnSc4	pravidelnost
i	i	k8xC	i
údernost	údernost	k1gFnSc4	údernost
a	a	k8xC	a
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
soumraku	soumrak	k1gInSc2	soumrak
dal	dát	k5eAaPmAgMnS	dát
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
svým	svůj	k3xOyFgFnPc3	svůj
jednotkám	jednotka	k1gFnPc3	jednotka
povel	povel	k1gInSc4	povel
postoupit	postoupit	k5eAaPmF	postoupit
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rytíři	rytíř	k1gMnPc1	rytíř
nasedli	nasednout	k5eAaPmAgMnP	nasednout
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
koně	kůň	k1gMnPc4	kůň
a	a	k8xC	a
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
armády	armáda	k1gFnSc2	armáda
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jen	jen	k9	jen
král	král	k1gMnSc1	král
samotný	samotný	k2eAgMnSc1d1	samotný
s	s	k7c7	s
družinou	družina	k1gFnSc7	družina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
50	[number]	k4	50
až	až	k8xS	až
70	[number]	k4	70
kopiníků	kopiník	k1gMnPc2	kopiník
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jezdci	jezdec	k1gMnPc1	jezdec
a	a	k8xC	a
orleánská	orleánský	k2eAgFnSc1d1	Orleánská
domobrana	domobrana	k1gFnSc1	domobrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
i	i	k9	i
osobně	osobně	k6eAd1	osobně
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
Filipem	Filip	k1gMnSc7	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
šípem	šíp	k1gInSc7	šíp
do	do	k7c2	do
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Bojiště	bojiště	k1gNnPc4	bojiště
opustil	opustit	k5eAaPmAgMnS	opustit
teprve	teprve	k6eAd1	teprve
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
Jana	Jan	k1gMnSc2	Jan
Henegavského	Henegavský	k2eAgMnSc2d1	Henegavský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
uchopil	uchopit	k5eAaPmAgInS	uchopit
uzdu	uzda	k1gFnSc4	uzda
jeho	on	k3xPp3gMnSc2	on
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
odvedl	odvést	k5eAaPmAgMnS	odvést
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přicházející	přicházející	k2eAgFnSc7d1	přicházející
tmou	tma	k1gFnSc7	tma
prý	prý	k9	prý
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgMnS	nechat
zapálit	zapálit	k5eAaPmF	zapálit
větrný	větrný	k2eAgInSc4d1	větrný
mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
osvětlil	osvětlit	k5eAaPmAgMnS	osvětlit
bojiště	bojiště	k1gNnSc4	bojiště
a	a	k8xC	a
vpřed	vpřed	k6eAd1	vpřed
postoupila	postoupit	k5eAaPmAgFnS	postoupit
i	i	k9	i
anglická	anglický	k2eAgFnSc1d1	anglická
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dobila	dobít	k5eAaPmAgFnS	dobít
raněné	raněný	k1gMnPc4	raněný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
francouzského	francouzský	k2eAgInSc2d1	francouzský
panovníka	panovník	k1gMnSc4	panovník
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
nedal	dát	k5eNaPmAgMnS	dát
své	svůj	k3xOyFgNnSc4	svůj
svolení	svolení	k1gNnSc4	svolení
<g/>
,	,	kIx,	,
snad	snad	k9	snad
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
z	z	k7c2	z
Abbeville	Abbevill	k1gMnSc2	Abbevill
postupují	postupovat	k5eAaImIp3nP	postupovat
další	další	k2eAgInPc4d1	další
protivníkovy	protivníkův	k2eAgInPc4d1	protivníkův
kontingenty	kontingent	k1gInPc4	kontingent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynulé	uplynulý	k2eAgFnSc6d1	uplynulá
noci	noc	k1gFnSc6	noc
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
nařídil	nařídit	k5eAaPmAgMnS	nařídit
siru	sir	k1gMnSc3	sir
Reginaldu	Reginald	k1gMnSc3	Reginald
Cobhamovi	Cobham	k1gMnSc3	Cobham
a	a	k8xC	a
panu	pan	k1gMnSc3	pan
Staffordovi	Stafforda	k1gMnSc3	Stafforda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
heroldy	herold	k1gMnPc7	herold
a	a	k8xC	a
dvěma	dva	k4xCgMnPc7	dva
písaři	písař	k1gMnPc1	písař
prošli	projít	k5eAaPmAgMnP	projít
bojiště	bojiště	k1gNnSc4	bojiště
a	a	k8xC	a
zjistili	zjistit	k5eAaPmAgMnP	zjistit
počet	počet	k1gInSc4	počet
a	a	k8xC	a
jména	jméno	k1gNnPc4	jméno
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Nejurozenější	urozený	k2eAgMnPc1d3	nejurozenější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
posléze	posléze	k6eAd1	posléze
slavnostně	slavnostně	k6eAd1	slavnostně
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
zaopatřeni	zaopatřen	k2eAgMnPc1d1	zaopatřen
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
byli	být	k5eAaImAgMnP	být
spáleni	spálit	k5eAaPmNgMnP	spálit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
vedl	vést	k5eAaImAgInS	vést
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
zpustošil	zpustošit	k5eAaPmAgMnS	zpustošit
území	území	k1gNnSc4	území
v	v	k7c6	v
šířce	šířka	k1gFnSc6	šířka
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
dobýt	dobýt	k5eAaPmF	dobýt
Calais	Calais	k1gNnSc4	Calais
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
postavilo	postavit	k5eAaPmAgNnS	postavit
na	na	k7c4	na
odhodlaný	odhodlaný	k2eAgInSc4d1	odhodlaný
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
obléhání	obléhání	k1gNnSc1	obléhání
se	se	k3xPyFc4	se
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
na	na	k7c4	na
celých	celý	k2eAgInPc2d1	celý
jedenáct	jedenáct	k4xCc4	jedenáct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
Angličany	Angličan	k1gMnPc4	Angličan
zraněné	zraněný	k2eAgMnPc4d1	zraněný
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
pečovali	pečovat	k5eAaImAgMnP	pečovat
mniši	mnich	k1gMnPc1	mnich
z	z	k7c2	z
Kresčackého	Kresčacký	k2eAgInSc2d1	Kresčacký
dvorce	dvorec	k1gInSc2	dvorec
<g/>
,	,	kIx,	,
těla	tělo	k1gNnSc2	tělo
těch	ten	k3xDgMnPc2	ten
co	co	k9	co
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
péči	péče	k1gFnSc6	péče
byla	být	k5eAaImAgFnS	být
spálena	spálit	k5eAaPmNgFnS	spálit
v	v	k7c6	v
ohradě	ohrada	k1gFnSc6	ohrada
v	v	k7c6	v
cípu	cíp	k1gInSc6	cíp
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebylo	být	k5eNaImAgNnS	být
zoráno	zorat	k5eAaPmNgNnS	zorat
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
skupinou	skupina	k1gFnSc7	skupina
nejvěrnějších	věrný	k2eAgMnPc2d3	nejvěrnější
bojovníků	bojovník	k1gMnPc2	bojovník
večer	večer	k6eAd1	večer
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
La	la	k1gNnSc2	la
Broye	Broy	k1gFnSc2	Broy
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
posádku	posádka	k1gFnSc4	posádka
musel	muset	k5eAaImAgMnS	muset
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
identitě	identita	k1gFnSc6	identita
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přenocoval	přenocovat	k5eAaPmAgMnS	přenocovat
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Doullens	Doullensa	k1gFnPc2	Doullensa
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Amiensu	Amiens	k1gInSc2	Amiens
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Henegavským	Henegavský	k2eAgMnSc7d1	Henegavský
<g/>
,	,	kIx,	,
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Namuru	Namur	k1gInSc2	Namur
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
flanderským	flanderský	k2eAgMnSc7d1	flanderský
hrabětem	hrabě	k1gMnSc7	hrabě
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
znova	znova	k6eAd1	znova
shromáždit	shromáždit	k5eAaPmF	shromáždit
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
když	když	k8xS	když
však	však	k9	však
zjistil	zjistit	k5eAaPmAgMnS	zjistit
rozsah	rozsah	k1gInSc4	rozsah
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
odevzdaně	odevzdaně	k6eAd1	odevzdaně
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
akce	akce	k1gFnPc4	akce
proti	proti	k7c3	proti
Angličanům	Angličan	k1gMnPc3	Angličan
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
rozkázal	rozkázat	k5eAaPmAgInS	rozkázat
popravit	popravit	k5eAaPmF	popravit
všechny	všechen	k3xTgMnPc4	všechen
Janovany	Janovan	k1gMnPc4	Janovan
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
zrádce	zrádce	k1gMnSc4	zrádce
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
opadl	opadnout	k5eAaPmAgMnS	opadnout
jeho	jeho	k3xOp3gInSc4	jeho
hněv	hněv	k1gInSc4	hněv
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
vojáků	voják	k1gMnPc2	voják
skutečně	skutečně	k6eAd1	skutečně
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
zbytku	zbytek	k1gInSc2	zbytek
byl	být	k5eAaImAgInS	být
povolen	povolit	k5eAaPmNgInS	povolit
odchod	odchod	k1gInSc1	odchod
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
dalších	další	k2eAgMnPc2d1	další
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ještě	ještě	k6eAd1	ještě
následujícího	následující	k2eAgNnSc2d1	následující
mlhavého	mlhavý	k2eAgNnSc2d1	mlhavé
rána	ráno	k1gNnSc2	ráno
bloudili	bloudit	k5eAaImAgMnP	bloudit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Abbevile	Abbevila	k1gFnSc6	Abbevila
a	a	k8xC	a
vyvolávali	vyvolávat	k5eAaImAgMnP	vyvolávat
hesla	heslo	k1gNnPc4	heslo
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
nalézt	nalézt	k5eAaPmF	nalézt
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
velitele	velitel	k1gMnSc4	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
řadu	řad	k1gInSc2	řad
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
objevili	objevit	k5eAaPmAgMnP	objevit
a	a	k8xC	a
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
nebrali	brát	k5eNaImAgMnP	brát
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyčíslení	vyčíslení	k1gNnSc6	vyčíslení
ztrát	ztráta	k1gFnPc2	ztráta
se	se	k3xPyFc4	se
středověcí	středověký	k2eAgMnPc1d1	středověký
kronikáři	kronikář	k1gMnPc1	kronikář
tradičně	tradičně	k6eAd1	tradičně
rozchází	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
pramenů	pramen	k1gInPc2	pramen
Jean	Jean	k1gMnSc1	Jean
Froissart	Froissart	k1gInSc4	Froissart
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reginald	Reginald	k1gMnSc1	Reginald
Cobham	Cobham	k1gInSc1	Cobham
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Stafford	Stafford	k1gMnSc1	Stafford
napočítali	napočítat	k5eAaPmAgMnP	napočítat
jedenáct	jedenáct	k4xCc4	jedenáct
francouzských	francouzský	k2eAgMnPc2d1	francouzský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
80	[number]	k4	80
korouhevních	korouhevní	k2eAgMnPc2d1	korouhevní
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
1	[number]	k4	1
200	[number]	k4	200
rytířů	rytíř	k1gMnPc2	rytíř
jednoštítných	jednoštítný	k2eAgFnPc2d1	jednoštítný
a	a	k8xC	a
kolem	kolem	k7c2	kolem
30	[number]	k4	30
000	[number]	k4	000
ostatního	ostatní	k2eAgInSc2d1	ostatní
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
prý	prý	k9	prý
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
80	[number]	k4	80
korouhví	korouhev	k1gFnPc2	korouhev
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
i	i	k9	i
červenozlatý	červenozlatý	k2eAgMnSc1d1	červenozlatý
<g/>
,	,	kIx,	,
čtvercový	čtvercový	k2eAgMnSc1d1	čtvercový
Oriflamme	Oriflamme	k1gMnSc1	Oriflamme
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Chronique	Chroniqu	k1gFnSc2	Chroniqu
de	de	k?	de
Berne	Bern	k1gInSc5	Bern
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
z	z	k7c2	z
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
3	[number]	k4	3
800	[number]	k4	800
mrtvých	mrtvý	k2eAgInPc2d1	mrtvý
1	[number]	k4	1
200	[number]	k4	200
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomuto	tento	k3xDgInSc3	tento
střízlivému	střízlivý	k2eAgInSc3d1	střízlivý
odhadu	odhad	k1gInSc3	odhad
Jean	Jean	k1gMnSc1	Jean
Le	Le	k1gMnSc1	Le
Bel	bel	k1gInSc4	bel
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
devět	devět	k4xCc4	devět
knížat	kníže	k1gNnPc2	kníže
<g/>
,	,	kIx,	,
1	[number]	k4	1
200	[number]	k4	200
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
000	[number]	k4	000
panošů	panoš	k1gMnPc2	panoš
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
.	.	kIx.	.
</s>
<s>
Geoffrey	Geoffrea	k1gMnSc2	Geoffrea
la	la	k1gNnSc1	la
Baker	Baker	k1gMnSc1	Baker
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
padlo	padnout	k5eAaImAgNnS	padnout
na	na	k7c4	na
4	[number]	k4	4
000	[number]	k4	000
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
rytířů	rytíř	k1gMnPc2	rytíř
celkem	celkem	k6eAd1	celkem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
padlým	padlý	k1gMnPc3	padlý
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
patřili	patřit	k5eAaImAgMnP	patřit
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
<g/>
,	,	kIx,	,
lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
vévoda	vévoda	k1gMnSc1	vévoda
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
flanderský	flanderský	k2eAgMnSc1d1	flanderský
hrabě	hrabě	k1gMnSc1	hrabě
Ludvík	Ludvík	k1gMnSc1	Ludvík
z	z	k7c2	z
Naves	Navesa	k1gFnPc2	Navesa
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
z	z	k7c2	z
Bolois	Bolois	k1gFnSc2	Bolois
<g/>
,	,	kIx,	,
hrabata	hrabě	k1gNnPc4	hrabě
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Auxerre	Auxerr	k1gInSc5	Auxerr
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
ze	z	k7c2	z
Sancerre	Sancerr	k1gInSc5	Sancerr
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Harcourtu	Harcourt	k1gInSc2	Harcourt
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Granpré	Granprý	k2eAgFnSc2d1	Granprý
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
ze	z	k7c2	z
Saint-Pol	Saint-Pola	k1gFnPc2	Saint-Pola
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Le	Le	k1gFnSc2	Le
Bel	bel	k1gInSc1	bel
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
300	[number]	k4	300
rytířích	rytíř	k1gMnPc6	rytíř
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
přičíst	přičíst	k5eAaPmF	přičíst
neznámý	známý	k2eNgInSc4d1	neznámý
počet	počet	k1gInSc4	počet
lučištníků	lučištník	k1gMnPc2	lučištník
a	a	k8xC	a
kopiníků	kopiník	k1gMnPc2	kopiník
<g/>
.	.	kIx.	.
</s>
<s>
Výzva	výzva	k1gFnSc1	výzva
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
v	v	k7c6	v
Trevíru	Trevír	k1gInSc6	Trevír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Karlem	Karel	k1gMnSc7	Karel
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
strýcem	strýc	k1gMnSc7	strýc
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Balduinem	Balduin	k1gMnSc7	Balduin
radil	radit	k5eAaImAgMnS	radit
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
postupu	postup	k1gInSc6	postup
proti	proti	k7c3	proti
Ludvíku	Ludvík	k1gMnSc3	Ludvík
Bavorovi	Bavor	k1gMnSc3	Bavor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
Karlovým	Karlův	k2eAgMnSc7d1	Karlův
protikandidátem	protikandidát	k1gMnSc7	protikandidát
na	na	k7c4	na
římskoněmecký	římskoněmecký	k2eAgInSc4d1	římskoněmecký
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
pověsti	pověst	k1gFnSc3	pověst
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
patrně	patrně	k6eAd1	patrně
nebylo	být	k5eNaImAgNnS	být
větším	veliký	k2eAgInSc7d2	veliký
problémem	problém	k1gInSc7	problém
narychlo	narychlo	k6eAd1	narychlo
shromáždit	shromáždit	k5eAaPmF	shromáždit
početnou	početný	k2eAgFnSc4d1	početná
družinu	družina	k1gFnSc4	družina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
dobových	dobový	k2eAgInPc2d1	dobový
záznamů	záznam	k1gInPc2	záznam
čítala	čítat	k5eAaImAgFnS	čítat
na	na	k7c4	na
500	[number]	k4	500
těžkooděnců	těžkooděnec	k1gMnPc2	těžkooděnec
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Kontingent	kontingent	k1gInSc1	kontingent
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
povinován	povinovat	k5eAaImNgMnS	povinovat
velet	velet	k5eAaImF	velet
samostatnému	samostatný	k2eAgInSc3d1	samostatný
oddílu	oddíl	k1gInSc3	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
u	u	k7c2	u
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
pomocné	pomocný	k2eAgNnSc1d1	pomocné
vojsko	vojsko	k1gNnSc1	vojsko
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
asi	asi	k9	asi
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
přivedl	přivést	k5eAaPmAgMnS	přivést
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
s	s	k7c7	s
několikadenním	několikadenní	k2eAgNnSc7d1	několikadenní
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gMnSc4	on
vázaly	vázat	k5eAaImAgFnP	vázat
povinnosti	povinnost	k1gFnPc1	povinnost
v	v	k7c6	v
Německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
předkládán	předkládat	k5eAaImNgInS	předkládat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
královskou	královský	k2eAgFnSc4d1	královská
družinu	družina	k1gFnSc4	družina
tvořili	tvořit	k5eAaImAgMnP	tvořit
většinou	většinou	k6eAd1	většinou
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
drtivou	drtivý	k2eAgFnSc4d1	drtivá
převahu	převaha	k1gFnSc4	převaha
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
bojovníci	bojovník	k1gMnPc1	bojovník
shromáždění	shromáždění	k1gNnSc2	shromáždění
v	v	k7c6	v
Porýní	Porýní	k1gNnSc6	Porýní
a	a	k8xC	a
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
příslušníky	příslušník	k1gMnPc4	příslušník
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
tažení	tažení	k1gNnSc2	tažení
účastnili	účastnit	k5eAaImAgMnP	účastnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
získali	získat	k5eAaPmAgMnP	získat
válečnické	válečnický	k2eAgFnPc4d1	válečnická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
rytířské	rytířský	k2eAgInPc4d1	rytířský
ostruhy	ostruh	k1gInPc4	ostruh
<g/>
,	,	kIx,	,
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
patřili	patřit	k5eAaImAgMnP	patřit
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Bítovský	Bítovský	k1gMnSc1	Bítovský
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Klingenberka	Klingenberka	k1gFnSc1	Klingenberka
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc1d2	pozdější
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
soudobé	soudobý	k2eAgFnPc1d1	soudobá
relace	relace	k1gFnPc1	relace
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Dalibora	Dalibor	k1gMnSc4	Dalibor
z	z	k7c2	z
Kozojed	Kozojed	k1gMnSc1	Kozojed
<g/>
,	,	kIx,	,
Ješka	Ješka	k1gMnSc1	Ješka
z	z	k7c2	z
Rožďálovic	Rožďálovice	k1gFnPc2	Rožďálovice
<g/>
,	,	kIx,	,
Licka	Licka	k1gFnSc1	Licka
z	z	k7c2	z
Riseburka	Riseburek	k1gMnSc2	Riseburek
<g/>
,	,	kIx,	,
Valkouna	Valkoun	k1gMnSc2	Valkoun
z	z	k7c2	z
Pořešína	Pořešín	k1gInSc2	Pořešín
<g/>
,	,	kIx,	,
Bolka	Bolek	k1gMnSc4	Bolek
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
a	a	k8xC	a
Kunarta	Kunarta	k1gFnSc1	Kunarta
z	z	k7c2	z
Pavlovic	Pavlovice	k1gFnPc2	Pavlovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
kresčackého	kresčacký	k2eAgNnSc2d1	kresčacký
tažení	tažení	k1gNnSc2	tažení
se	se	k3xPyFc4	se
české	český	k2eAgInPc1d1	český
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
římsko-německé	římskoěmecký	k2eAgInPc4d1	římsko-německý
oddíly	oddíl	k1gInPc4	oddíl
obou	dva	k4xCgInPc2	dva
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
účastnily	účastnit	k5eAaImAgInP	účastnit
několika	několik	k4yIc2	několik
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
samotné	samotný	k2eAgNnSc1d1	samotné
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jejich	jejich	k3xOp3gFnSc1	jejich
role	role	k1gFnSc1	role
značně	značně	k6eAd1	značně
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
otazník	otazník	k1gInSc1	otazník
se	se	k3xPyFc4	se
vznáší	vznášet	k5eAaImIp3nS	vznášet
nad	nad	k7c7	nad
útokem	útok	k1gInSc7	útok
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Le	Le	k1gMnSc1	Le
Bel	bel	k1gInSc4	bel
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slepý	slepý	k2eAgMnSc1d1	slepý
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
šel	jít	k5eAaImAgMnS	jít
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Janovanů	Janovan	k1gMnPc2	Janovan
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Jean	Jean	k1gMnSc1	Jean
Froissart	Froissarta	k1gFnPc2	Froissarta
zařadil	zařadit	k5eAaPmAgMnS	zařadit
zteč	zteč	k1gFnSc4	zteč
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
události	událost	k1gFnPc4	událost
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
boje	boj	k1gInSc2	boj
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
střetnutí	střetnutí	k1gNnSc2	střetnutí
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
a	a	k8xC	a
francouzské	francouzský	k2eAgFnPc1d1	francouzská
jednotky	jednotka	k1gFnPc1	jednotka
prchaly	prchat	k5eAaImAgFnP	prchat
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
závěru	závěr	k1gInSc3	závěr
některé	některý	k3yIgMnPc4	některý
historiky	historik	k1gMnPc4	historik
(	(	kIx(	(
<g/>
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
Ivanov	Ivanov	k1gInSc1	Ivanov
<g/>
)	)	kIx)	)
vedou	vést	k5eAaImIp3nP	vést
nejen	nejen	k6eAd1	nejen
české	český	k2eAgInPc4d1	český
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
záznam	záznam	k1gInSc1	záznam
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
událostí	událost	k1gFnPc2	událost
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
zeptal	zeptat	k5eAaPmAgMnS	zeptat
svých	svůj	k3xOyFgMnPc2	svůj
rytířů	rytíř	k1gMnPc2	rytíř
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
stojí	stát	k5eAaImIp3nS	stát
bitva	bitva	k1gFnSc1	bitva
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Sire	sir	k1gMnSc5	sir
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
nešporách	nešpora	k1gFnPc6	nešpora
<g/>
,	,	kIx,	,
a	a	k8xC	a
budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
mít	mít	k5eAaImF	mít
slunce	slunce	k1gNnSc4	slunce
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
naši	náš	k3xOp1gFnSc4	náš
tak	tak	k9	tak
špatně	špatně	k6eAd1	špatně
uspořádáni	uspořádán	k2eAgMnPc1d1	uspořádán
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
lučištníky	lučištník	k1gMnPc4	lučištník
a	a	k8xC	a
zbytečně	zbytečně	k6eAd1	zbytečně
se	se	k3xPyFc4	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
pobíjet	pobíjet	k5eAaImF	pobíjet
<g/>
,	,	kIx,	,
ničeho	nic	k3yNnSc2	nic
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nedá	dát	k5eNaPmIp3nS	dát
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
Mnich	mnich	k1gMnSc1	mnich
Basilejský	basilejský	k2eAgMnSc1d1	basilejský
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
věta	věta	k1gFnSc1	věta
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
reálném	reálný	k2eAgInSc6d1	reálný
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
by	by	kYmCp3nP	by
podobnou	podobný	k2eAgFnSc4d1	podobná
otázku	otázka	k1gFnSc4	otázka
nepoložil	položit	k5eNaPmAgMnS	položit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
útoku	útok	k1gInSc2	útok
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
jen	jen	k9	jen
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
zpráv	zpráva	k1gFnPc2	zpráva
kolem	kolem	k7c2	kolem
padesátky	padesátka	k1gFnSc2	padesátka
nebo	nebo	k8xC	nebo
stovky	stovka	k1gFnSc2	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
handicapovaného	handicapovaný	k2eAgMnSc2d1	handicapovaný
krále	král	k1gMnSc2	král
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sebevražednou	sebevražedný	k2eAgFnSc4d1	sebevražedná
jízdu	jízda	k1gFnSc4	jízda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
život	život	k1gInSc1	život
slepce	slepec	k1gMnSc2	slepec
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ztratil	ztratit	k5eAaPmAgInS	ztratit
smysl	smysl	k1gInSc1	smysl
a	a	k8xC	a
hledal	hledat	k5eAaImAgInS	hledat
smrt	smrt	k1gFnSc4	smrt
hodnou	hodný	k2eAgFnSc4d1	hodná
jeho	jeho	k3xOp3gFnSc3	jeho
rytířské	rytířský	k2eAgFnSc3d1	rytířská
pověsti	pověst	k1gFnSc3	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc7	svůj
ztečí	zteč	k1gFnSc7	zteč
chtěl	chtít	k5eAaImAgMnS	chtít
podpořit	podpořit	k5eAaPmF	podpořit
jednotky	jednotka	k1gFnPc4	jednotka
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
útočil	útočit	k5eAaImAgMnS	útočit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
morfologického	morfologický	k2eAgNnSc2d1	morfologické
ohledání	ohledání	k1gNnSc2	ohledání
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
prováděl	provádět	k5eAaImAgMnS	provádět
přední	přední	k2eAgMnSc1d1	přední
česky	česky	k6eAd1	česky
antropolog	antropolog	k1gMnSc1	antropolog
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
jednomu	jeden	k4xCgNnSc3	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
zjistitelných	zjistitelný	k2eAgNnPc2d1	zjistitelné
poranění	poranění	k1gNnPc2	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
zprávy	zpráva	k1gFnSc2	zpráva
je	být	k5eAaImIp3nS	být
nejzávažnějším	závažný	k2eAgNnSc7d3	nejzávažnější
traumatem	trauma	k1gNnSc7	trauma
bodná	bodný	k2eAgFnSc1d1	bodná
rána	rána	k1gFnSc1	rána
vedená	vedený	k2eAgFnSc1d1	vedená
do	do	k7c2	do
levého	levý	k2eAgNnSc2d1	levé
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pronikla	proniknout	k5eAaPmAgFnS	proniknout
očnicovou	očnicový	k2eAgFnSc7d1	očnicový
štěrbinou	štěrbina	k1gFnSc7	štěrbina
až	až	k6eAd1	až
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
malých	malý	k2eAgNnPc2d1	malé
a	a	k8xC	a
velkých	velký	k2eAgNnPc2d1	velké
křídel	křídlo	k1gNnPc2	křídlo
kosti	kost	k1gFnSc2	kost
klínové	klínový	k2eAgNnSc1d1	klínové
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úder	úder	k1gInSc1	úder
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
nástrojem	nástroj	k1gInSc7	nástroj
trojbokého	trojboký	k2eAgInSc2d1	trojboký
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
smrtelnému	smrtelný	k2eAgNnSc3d1	smrtelné
poranění	poranění	k1gNnSc3	poranění
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
lopatce	lopatka	k1gFnSc6	lopatka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
kruhovitý	kruhovitý	k2eAgInSc1d1	kruhovitý
otvor	otvor	k1gInSc1	otvor
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
10	[number]	k4	10
mm	mm	kA	mm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
průnikem	průnik	k1gInSc7	průnik
hrotnatého	hrotnatý	k2eAgNnSc2d1	hrotnatý
tělesa	těleso	k1gNnSc2	těleso
do	do	k7c2	do
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Zkosení	zkosení	k1gNnSc1	zkosení
okrajů	okraj	k1gInPc2	okraj
rány	rána	k1gFnSc2	rána
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
bodnou	bodný	k2eAgFnSc4d1	bodná
ránu	rána	k1gFnSc4	rána
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pronikla	proniknout	k5eAaPmAgFnS	proniknout
lopatkou	lopatka	k1gFnSc7	lopatka
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
poloviny	polovina	k1gFnSc2	polovina
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nebo	nebo	k8xC	nebo
další	další	k2eAgNnPc4d1	další
zranění	zranění	k1gNnPc4	zranění
pro	pro	k7c4	pro
život	život	k1gInSc4	život
důležitých	důležitý	k2eAgInPc2d1	důležitý
orgánů	orgán	k1gInPc2	orgán
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
a	a	k8xC	a
přivodily	přivodit	k5eAaPmAgFnP	přivodit
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
poškození	poškození	k1gNnPc1	poškození
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
způsobeny	způsoben	k2eAgInPc1d1	způsoben
až	až	k9	až
po	po	k7c6	po
panovníkově	panovníkův	k2eAgFnSc6d1	panovníkova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
či	či	k8xC	či
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
bitvy	bitva	k1gFnSc2	bitva
anglickými	anglický	k2eAgMnPc7d1	anglický
marodéry	marodér	k1gMnPc4	marodér
oloupen	oloupen	k2eAgInSc1d1	oloupen
o	o	k7c4	o
výzbroj	výzbroj	k1gFnSc4	výzbroj
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
výstroj	výstroj	k1gFnSc4	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
indiciím	indicie	k1gFnPc3	indicie
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
skutečnosti	skutečnost	k1gFnSc6	skutečnost
patři	patřit	k5eAaImRp2nS	patřit
zejména	zejména	k9	zejména
tři	tři	k4xCgFnPc4	tři
sečné	sečný	k2eAgFnPc4d1	sečná
rány	rána	k1gFnPc4	rána
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
zápěstí	zápěstí	k1gNnSc6	zápěstí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
oddělit	oddělit	k5eAaPmF	oddělit
ruku	ruka	k1gFnSc4	ruka
od	od	k7c2	od
paže	paže	k1gFnSc2	paže
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
záměrem	záměr	k1gInSc7	záměr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
stisk	stisk	k1gInSc1	stisk
dlaně	dlaň	k1gFnSc2	dlaň
třímající	třímající	k2eAgFnSc4d1	třímající
drahocennou	drahocenný	k2eAgFnSc4d1	drahocenná
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
ze	z	k7c2	z
ztuhlých	ztuhlý	k2eAgInPc2d1	ztuhlý
prstů	prst	k1gInPc2	prst
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
sejmout	sejmout	k5eAaPmF	sejmout
prsteny	prsten	k1gInPc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
důkazy	důkaz	k1gInPc1	důkaz
svědčí	svědčit	k5eAaImIp3nP	svědčit
pro	pro	k7c4	pro
verzi	verze	k1gFnSc4	verze
vyprávění	vyprávění	k1gNnSc2	vyprávění
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
zemřel	zemřít	k5eAaPmAgMnS	zemřít
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
a	a	k8xC	a
ne	ne	k9	ne
ve	v	k7c6	v
stanu	stan	k1gInSc6	stan
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
interpretováno	interpretován	k2eAgNnSc1d1	interpretováno
Františkem	František	k1gMnSc7	František
Palackým	Palacký	k1gMnSc7	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
identifikovaná	identifikovaný	k2eAgFnSc1d1	identifikovaná
mrtvola	mrtvola	k1gFnSc1	mrtvola
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
vzdali	vzdát	k5eAaPmAgMnP	vzdát
mu	on	k3xPp3gNnSc3	on
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
bylo	být	k5eAaImAgNnS	být
slouženo	sloužen	k2eAgNnSc1d1	slouženo
exequie	exequie	k?	exequie
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
opatství	opatství	k1gNnSc6	opatství
Vallores	Valloresa	k1gFnPc2	Valloresa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Nabalzamované	nabalzamovaný	k2eAgNnSc1d1	nabalzamované
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
předáno	předat	k5eAaPmNgNnS	předat
římskoněmeckému	římskoněmecký	k2eAgMnSc3d1	římskoněmecký
králi	král	k1gMnSc3	král
Karlovi	Karel	k1gMnSc3	Karel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jej	on	k3xPp3gMnSc4	on
odvezl	odvézt	k5eAaPmAgMnS	odvézt
do	do	k7c2	do
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gInSc4	jeho
otec	otec	k1gMnSc1	otec
přál	přát	k5eAaImAgMnS	přát
být	být	k5eAaImF	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Převoz	převoz	k1gInSc4	převoz
těla	tělo	k1gNnSc2	tělo
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
monarchy	monarcha	k1gMnSc2	monarcha
stál	stát	k5eAaImAgInS	stát
957	[number]	k4	957
zlatých	zlatý	k2eAgFnPc2d1	zlatá
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnSc4	zařízení
pohřbu	pohřeb	k1gInSc2	pohřeb
677	[number]	k4	677
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Karel	Karel	k1gMnSc1	Karel
nedisponoval	disponovat	k5eNaBmAgMnS	disponovat
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
hotovostí	hotovost	k1gFnSc7	hotovost
<g/>
,	,	kIx,	,
vypůjčil	vypůjčit	k5eAaPmAgInS	vypůjčit
si	se	k3xPyFc3	se
peníze	peníz	k1gInPc4	peníz
u	u	k7c2	u
příznivě	příznivě	k6eAd1	příznivě
nakloněného	nakloněný	k2eAgMnSc2d1	nakloněný
arlonského	arlonský	k2eAgMnSc2d1	arlonský
probošta	probošt	k1gMnSc2	probošt
Arnolda	Arnold	k1gMnSc2	Arnold
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
účasti	účast	k1gFnSc6	účast
samotného	samotný	k2eAgMnSc2d1	samotný
Karla	Karel	k1gMnSc2	Karel
Českého	český	k2eAgInSc2d1	český
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
nejasného	jasný	k2eNgInSc2d1	nejasný
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
bataille	bataill	k1gInSc6	bataill
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
družina	družina	k1gFnSc1	družina
nacházela	nacházet	k5eAaImAgFnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bojoval	bojovat	k5eAaImAgMnS	bojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Alençonu	Alençon	k1gInSc2	Alençon
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
nacházet	nacházet	k5eAaImF	nacházet
i	i	k9	i
poblíž	poblíž	k7c2	poblíž
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
popisuje	popisovat	k5eAaImIp3nS	popisovat
Kronika	kronika	k1gFnSc1	kronika
českého	český	k2eAgInSc2d1	český
kostela	kostel	k1gInSc2	kostel
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Beneše	Beneš	k1gMnSc2	Beneš
Krabice	krabice	k1gFnSc2	krabice
z	z	k7c2	z
Weitmile	Weitmil	k1gInSc5	Weitmil
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
literát	literát	k1gMnSc1	literát
současně	současně	k6eAd1	současně
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
za	za	k7c2	za
jakých	jaký	k3yIgFnPc2	jaký
okolností	okolnost	k1gFnPc2	okolnost
budoucí	budoucí	k2eAgMnSc1d1	budoucí
císař	císař	k1gMnSc1	císař
opustil	opustit	k5eAaPmAgMnS	opustit
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
:	:	kIx,	:
Ačkoli	ačkoli	k8xS	ačkoli
ani	ani	k8xC	ani
toto	tento	k3xDgNnSc4	tento
vyprávění	vyprávění	k1gNnSc4	vyprávění
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
skutečně	skutečně	k6eAd1	skutečně
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
jedno	jeden	k4xCgNnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
poranění	poranění	k1gNnSc1	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
českých	český	k2eAgMnPc2d1	český
historiků	historik	k1gMnPc2	historik
(	(	kIx(	(
<g/>
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
Spěváček	Spěváček	k1gMnSc1	Spěváček
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
postřelen	postřelit	k5eAaPmNgInS	postřelit
šípem	šíp	k1gInSc7	šíp
do	do	k7c2	do
paže	paže	k1gFnSc2	paže
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jakub	Jakub	k1gMnSc1	Jakub
Pavel	Pavel	k1gMnSc1	Pavel
v	v	k7c6	v
Karlově	Karlův	k2eAgInSc6d1	Karlův
itineráři	itinerář	k1gInSc6	itinerář
přiloženém	přiložený	k2eAgInSc6d1	přiložený
k	k	k7c3	k
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
Vita	vit	k2eAgFnSc1d1	Vita
Caroli	Carole	k1gFnSc4	Carole
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
poranění	poranění	k1gNnPc1	poranění
byla	být	k5eAaImAgNnP	být
nejspíš	nejspíš	k9	nejspíš
jen	jen	k6eAd1	jen
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
monarcha	monarcha	k1gMnSc1	monarcha
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
cestovat	cestovat	k5eAaImF	cestovat
na	na	k7c4	na
delší	dlouhý	k2eAgFnPc4d2	delší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
již	již	k6eAd1	již
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
byl	být	k5eAaImAgInS	být
povrch	povrch	k1gInSc1	povrch
země	zem	k1gFnSc2	zem
mezi	mezi	k7c7	mezi
Kresčakem	Kresčak	k1gInSc7	Kresčak
a	a	k8xC	a
Wadicourtem	Wadicourt	k1gInSc7	Wadicourt
narušen	narušit	k5eAaPmNgInS	narušit
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
odkud	odkud	k6eAd1	odkud
bitvu	bitva	k1gFnSc4	bitva
řídil	řídit	k5eAaImAgMnS	řídit
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
stojí	stát	k5eAaImIp3nS	stát
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
středověkému	středověký	k2eAgInSc3d1	středověký
větrnému	větrný	k2eAgInSc3d1	větrný
mlýnu	mlýn	k1gInSc3	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
podzemní	podzemní	k2eAgFnSc1d1	podzemní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
informační	informační	k2eAgFnPc4d1	informační
tabule	tabule	k1gFnPc4	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgInSc1d1	jihozápadní
svah	svah	k1gInSc1	svah
návrší	návrší	k1gNnSc3	návrší
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
školních	školní	k2eAgFnPc2d1	školní
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc2d1	sportovní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
větší	veliký	k2eAgFnSc7d2	veliký
stavbou	stavba	k1gFnSc7	stavba
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
cukrovaru	cukrovar	k1gInSc2	cukrovar
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
situovaná	situovaný	k2eAgFnSc1d1	situovaná
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
údolí	údolí	k1gNnSc2	údolí
Vallée	Vallé	k1gInSc2	Vallé
de	de	k?	de
Clers	Clersa	k1gFnPc2	Clersa
protíná	protínat	k5eAaImIp3nS	protínat
silnice	silnice	k1gFnSc1	silnice
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
od	od	k7c2	od
východního	východní	k2eAgInSc2d1	východní
okraje	okraj	k1gInSc2	okraj
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Kresčaku	Kresčak	k1gInSc2	Kresčak
na	na	k7c4	na
Fontaine-sur-Maye	Fontaineur-Maye	k1gFnSc4	Fontaine-sur-Maye
stojí	stát	k5eAaImIp3nS	stát
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
český	český	k2eAgInSc1d1	český
kříž	kříž	k1gInSc1	kříž
(	(	kIx(	(
<g/>
Croix	Croix	k1gInSc1	Croix
de	de	k?	de
Bohê	Bohê	k1gFnSc2	Bohê
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
nápisu	nápis	k1gInSc2	nápis
připomíná	připomínat	k5eAaImIp3nS	připomínat
hrdinský	hrdinský	k2eAgInSc1d1	hrdinský
konec	konec	k1gInSc1	konec
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
českého	český	k2eAgMnSc2d1	český
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
kříž	kříž	k1gInSc1	kříž
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1360	[number]	k4	1360
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
byl	být	k5eAaImAgInS	být
restaurován	restaurovat	k5eAaBmNgInS	restaurovat
a	a	k8xC	a
instalován	instalovat	k5eAaBmNgInS	instalovat
na	na	k7c4	na
podstavec	podstavec	k1gInSc4	podstavec
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
byla	být	k5eAaImAgNnP	být
vryta	vryt	k2eAgNnPc1d1	vryto
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
Froissartových	Froissartův	k2eAgFnPc2d1	Froissartův
Kronik	kronika	k1gFnPc2	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Žádám	žádat	k5eAaImIp1nS	žádat
vás	vy	k3xPp2nPc4	vy
důrazně	důrazně	k6eAd1	důrazně
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
mě	já	k3xPp1nSc4	já
vedli	vést	k5eAaImAgMnP	vést
tak	tak	k9	tak
daleko	daleko	k6eAd1	daleko
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
mohl	moct	k5eAaImAgInS	moct
dát	dát	k5eAaPmF	dát
ránu	rána	k1gFnSc4	rána
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
boční	boční	k2eAgFnSc6d1	boční
straně	strana	k1gFnSc6	strana
pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Statečným	statečný	k2eAgMnPc3d1	statečný
rytířům	rytíř	k1gMnPc3	rytíř
bylo	být	k5eAaImAgNnS	být
dražší	drahý	k2eAgNnSc1d2	dražší
umírat	umírat	k5eAaImF	umírat
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
vyčítán	vyčítat	k5eAaImNgInS	vyčítat
zbabělý	zbabělý	k2eAgInSc1d1	zbabělý
útěk	útěk	k1gInSc1	útěk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
levé	levá	k1gFnSc2	levá
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jan	Jan	k1gMnSc1	Jan
dal	dát	k5eAaPmAgMnS	dát
ránu	rána	k1gFnSc4	rána
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
snad	snad	k9	snad
tři	tři	k4xCgInPc4	tři
snad	snad	k9	snad
čtyři	čtyři	k4xCgInPc4	čtyři
-	-	kIx~	-
a	a	k8xC	a
bil	bít	k5eAaImAgMnS	bít
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
statečně	statečně	k6eAd1	statečně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
nápisem	nápis	k1gInSc7	nápis
je	být	k5eAaImIp3nS	být
erb	erb	k1gInSc1	erb
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
