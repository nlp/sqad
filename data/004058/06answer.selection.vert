<s>
Banán	banán	k1gInSc1	banán
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rostlina	rostlina	k1gFnSc1	rostlina
banánovník	banánovník	k1gInSc1	banánovník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
domestikován	domestikovat	k5eAaBmNgInS	domestikovat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
botanické	botanický	k2eAgInPc1d1	botanický
druhy	druh	k1gInPc1	druh
banánovníku	banánovník	k1gInSc2	banánovník
<g/>
)	)	kIx)	)
a	a	k8xC	a
podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
nejspíše	nejspíše	k9	nejspíše
být	být	k5eAaImF	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Papuy-Nové	Papuy-Nové	k2eAgFnSc2d1	Papuy-Nové
Guineje	Guinea	k1gFnSc2	Guinea
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
již	již	k9	již
8000	[number]	k4	8000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
