<s>
Vesuv	Vesuv	k1gInSc1	Vesuv
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Monte	Mont	k1gMnSc5	Mont
Vesuvio	Vesuvia	k1gMnSc5	Vesuvia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
činný	činný	k2eAgInSc1d1	činný
stratovulkán	stratovulkán	k1gInSc1	stratovulkán
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Sopka	sopka	k1gFnSc1	sopka
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
1281	[number]	k4	1281
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
Neapolským	neapolský	k2eAgInSc7d1	neapolský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Vesuv	Vesuv	k1gInSc1	Vesuv
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
sopek	sopka	k1gFnPc2	sopka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
Vesuvu	Vesuv	k1gInSc2	Vesuv
tvoří	tvořit	k5eAaImIp3nS	tvořit
sopka	sopka	k1gFnSc1	sopka
Monte	Mont	k1gInSc5	Mont
Somma	Somma	k1gFnSc1	Somma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
kaldera	kaldera	k1gFnSc1	kaldera
Monte	Mont	k1gInSc5	Mont
Sommy	Somma	k1gFnSc2	Somma
zaplněna	zaplnit	k5eAaPmNgFnS	zaplnit
a	a	k8xC	a
původní	původní	k2eAgFnSc1d1	původní
hora	hora	k1gFnSc1	hora
tvoří	tvořit	k5eAaImIp3nS	tvořit
hřeben	hřeben	k1gInSc1	hřeben
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hřeben	hřeben	k1gInSc1	hřeben
omezuje	omezovat	k5eAaImIp3nS	omezovat
odtok	odtok	k1gInSc4	odtok
lávy	láva	k1gFnSc2	láva
a	a	k8xC	a
odvod	odvod	k1gInSc1	odvod
části	část	k1gFnSc2	část
pyroklastik	pyroklastika	k1gFnPc2	pyroklastika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Monte	Mont	k1gInSc5	Mont
Sommy	Somm	k1gInPc4	Somm
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
sopečných	sopečný	k2eAgInPc2d1	sopečný
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
somma	somma	k1gFnSc1	somma
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
erupcí	erupce	k1gFnSc7	erupce
byla	být	k5eAaImAgFnS	být
erupce	erupce	k1gFnSc1	erupce
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
79	[number]	k4	79
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
města	město	k1gNnSc2	město
Pompeje	Pompeje	k1gInPc4	Pompeje
<g/>
,	,	kIx,	,
Herculaneum	Herculaneum	k1gNnSc1	Herculaneum
<g/>
,	,	kIx,	,
Oplontis	Oplontis	k1gFnSc1	Oplontis
a	a	k8xC	a
Stabie	Stabie	k1gFnSc1	Stabie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
vyvrženo	vyvržen	k2eAgNnSc1d1	vyvrženo
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
sopečného	sopečný	k2eAgInSc2d1	sopečný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
mračno	mračno	k1gNnSc4	mračno
zasahující	zasahující	k2eAgNnSc4d1	zasahující
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k8xS	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
37	[number]	k4	37
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
exploze	exploze	k1gFnSc2	exploze
se	se	k3xPyFc4	se
ze	z	k7c2	z
sopky	sopka	k1gFnSc2	sopka
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
žhavá	žhavý	k2eAgNnPc4d1	žhavé
oblaka	oblaka	k1gNnPc4	oblaka
pyroklastik	pyroklastika	k1gFnPc2	pyroklastika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahubila	zahubit	k5eAaPmAgFnS	zahubit
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Plinia	Plinium	k1gNnSc2	Plinium
staršího	starší	k1gMnSc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
natolik	natolik	k6eAd1	natolik
hrůzná	hrůzný	k2eAgFnSc1d1	hrůzná
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
popsalo	popsat	k5eAaPmAgNnS	popsat
mnoho	mnoho	k4c1	mnoho
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Plinia	Plinium	k1gNnSc2	Plinium
mladšího	mladý	k2eAgNnSc2d2	mladší
<g/>
,	,	kIx,	,
synovce	synovka	k1gFnSc3	synovka
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
Plinia	Plinium	k1gNnSc2	Plinium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
exploze	exploze	k1gFnSc1	exploze
nazývána	nazývat	k5eAaImNgFnS	nazývat
pliniovskou	pliniovský	k2eAgFnSc7d1	pliniovský
nebo	nebo	k8xC	nebo
též	též	k6eAd1	též
plinijskou	plinijský	k2eAgFnSc7d1	plinijský
explozí	exploze	k1gFnSc7	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgFnPc1d1	Krátké
erupce	erupce	k1gFnPc1	erupce
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
79	[number]	k4	79
následovány	následovat	k5eAaImNgFnP	následovat
častějšími	častý	k2eAgFnPc7d2	častější
relativně	relativně	k6eAd1	relativně
delšími	dlouhý	k2eAgFnPc7d2	delší
explozivními	explozivní	k2eAgFnPc7d1	explozivní
a	a	k8xC	a
efuzivními	efuzivní	k2eAgFnPc7d1	efuzivní
erupcemi	erupce	k1gFnPc7	erupce
začínajícími	začínající	k2eAgFnPc7d1	začínající
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1631	[number]	k4	1631
a	a	k8xC	a
končícími	končící	k2eAgFnPc7d1	končící
prozatím	prozatím	k6eAd1	prozatím
rokem	rok	k1gInSc7	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnPc1	erupce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
byla	být	k5eAaImAgFnS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
od	od	k7c2	od
roku	rok	k1gInSc2	rok
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizovaly	charakterizovat	k5eAaBmAgFnP	charakterizovat
ji	on	k3xPp3gFnSc4	on
zničující	zničující	k2eAgFnPc1d1	zničující
pyroklastické	pyroklastický	k2eAgFnPc1d1	pyroklastická
výrony	výron	k1gInPc4	výron
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
způsobily	způsobit	k5eAaPmAgFnP	způsobit
škody	škoda	k1gFnPc1	škoda
široko	široko	k6eAd1	široko
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sopky	sopka	k1gFnSc2	sopka
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgInPc1d1	častý
úniky	únik	k1gInPc1	únik
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
evakuační	evakuační	k2eAgInSc4d1	evakuační
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgInPc4d1	týkající
se	s	k7c7	s
25	[number]	k4	25
nejohroženějších	ohrožený	k2eAgNnPc2d3	nejohroženější
měst	město	k1gNnPc2	město
a	a	k8xC	a
7	[number]	k4	7
tzv.	tzv.	kA	tzv.
červených	červený	k2eAgFnPc2d1	červená
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
evakuací	evakuace	k1gFnSc7	evakuace
cca	cca	kA	cca
700	[number]	k4	700
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
městečka	městečko	k1gNnSc2	městečko
Ercolano	Ercolana	k1gFnSc5	Ercolana
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
Vesuvu	Vesuv	k1gInSc3	Vesuv
11	[number]	k4	11
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
silnice	silnice	k1gFnSc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
sopky	sopka	k1gFnSc2	sopka
je	být	k5eAaImIp3nS	být
parkoviště	parkoviště	k1gNnSc4	parkoviště
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
lze	lze	k6eAd1	lze
pěšky	pěšky	k6eAd1	pěšky
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
kráteru	kráter	k1gInSc3	kráter
za	za	k7c4	za
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sopce	sopka	k1gFnSc3	sopka
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
sedačková	sedačkový	k2eAgFnSc1d1	sedačková
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
zničena	zničit	k5eAaPmNgFnS	zničit
výbuchem	výbuch	k1gInSc7	výbuch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Vesuvem	Vesuv	k1gInSc7	Vesuv
leží	ležet	k5eAaImIp3nS	ležet
nejen	nejen	k6eAd1	nejen
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potenciálně	potenciálně	k6eAd1	potenciálně
ohrožené	ohrožený	k2eAgFnSc6d1	ohrožená
oblasti	oblast	k1gFnSc6	oblast
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
