<s>
Thomas	Thomas	k1gMnSc1	Thomas
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
předního	přední	k2eAgMnSc2d1	přední
presbytariánského	presbytariánský	k2eAgMnSc2d1	presbytariánský
pastora	pastor	k1gMnSc2	pastor
a	a	k8xC	a
docenta	docent	k1gMnSc2	docent
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
panovala	panovat	k5eAaImAgFnS	panovat
přísná	přísný	k2eAgFnSc1d1	přísná
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
Wilson	Wilson	k1gInSc1	Wilson
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
vírou	víra	k1gFnSc7	víra
odnesl	odnést	k5eAaPmAgInS	odnést
i	i	k9	i
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
školách	škola	k1gFnPc6	škola
politologii	politologie	k1gFnSc6	politologie
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc1	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
advokátskou	advokátský	k2eAgFnSc7d1	advokátská
praxí	praxe	k1gFnSc7	praxe
<g/>
,	,	kIx,	,
než	než	k8xS	než
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
politologie	politologie	k1gFnSc2	politologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
vydal	vydat	k5eAaPmAgInS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
nejznámější	známý	k2eAgFnSc4d3	nejznámější
knihu	kniha	k1gFnSc4	kniha
Vláda	vláda	k1gFnSc1	vláda
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
obhajoval	obhajovat	k5eAaImAgInS	obhajovat
prezidentovu	prezidentův	k2eAgFnSc4d1	prezidentova
pravomoc	pravomoc	k1gFnSc4	pravomoc
koordinovat	koordinovat	k5eAaBmF	koordinovat
práci	práce	k1gFnSc4	práce
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ellen	Ellna	k1gFnPc2	Ellna
Axson	Axson	k1gNnSc1	Axson
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učít	k5eAaPmAgMnS	učít
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
na	na	k7c4	na
Princeton	Princeton	k1gInSc4	Princeton
University	universita	k1gFnSc2	universita
politickou	politický	k2eAgFnSc4d1	politická
ekonomii	ekonomie	k1gFnSc4	ekonomie
a	a	k8xC	a
právní	právní	k2eAgFnPc4d1	právní
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Sedmkrát	sedmkrát	k6eAd1	sedmkrát
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejoblíbenějším	oblíbený	k2eAgMnSc7d3	nejoblíbenější
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
rektorem	rektor	k1gMnSc7	rektor
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
široký	široký	k2eAgInSc4d1	široký
program	program	k1gInSc4	program
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
"	"	kIx"	"
<g/>
image	image	k1gFnSc4	image
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterého	který	k3yQgMnSc4	který
byly	být	k5eAaImAgFnP	být
poctivost	poctivost	k1gFnSc4	poctivost
absolutním	absolutní	k2eAgInSc7d1	absolutní
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
vzbuzoval	vzbuzovat	k5eAaImAgMnS	vzbuzovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
spravedlivější	spravedlivý	k2eAgFnSc4d2	spravedlivější
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
když	když	k8xS	když
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
hledala	hledat	k5eAaImAgFnS	hledat
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
guvernéra	guvernér	k1gMnSc2	guvernér
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
tento	tento	k3xDgMnSc1	tento
rektor	rektor	k1gMnSc1	rektor
Princetonu	Princeton	k1gInSc2	Princeton
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
nulové	nulový	k2eAgFnPc1d1	nulová
praktické	praktický	k2eAgFnPc1d1	praktická
politické	politický	k2eAgFnPc1d1	politická
zkušenosti	zkušenost	k1gFnPc1	zkušenost
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejvhodnějším	vhodný	k2eAgMnSc7d3	nejvhodnější
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
praktické	praktický	k2eAgFnSc3d1	praktická
nezkušenosti	nezkušenost	k1gFnSc3	nezkušenost
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
pro	pro	k7c4	pro
státní	státní	k2eAgFnPc4d1	státní
instituce	instituce	k1gFnPc4	instituce
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
spoléhal	spoléhat	k5eAaImAgInS	spoléhat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
sbor	sbor	k1gInSc4	sbor
loajálních	loajální	k2eAgMnPc2d1	loajální
poradců	poradce	k1gMnPc2	poradce
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
Edvarda	Edvard	k1gMnSc4	Edvard
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dorovnával	dorovnávat	k5eAaImAgInS	dorovnávat
Wilsonův	Wilsonův	k2eAgInSc4d1	Wilsonův
nedostatek	nedostatek	k1gInSc4	nedostatek
taktu	takt	k1gInSc2	takt
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
mu	on	k3xPp3gMnSc3	on
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
nástupu	nástup	k1gInSc2	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
guvernéra	guvernér	k1gMnSc2	guvernér
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhého	druhý	k4xOgNnSc2	druhý
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
guvernér	guvernér	k1gMnSc1	guvernér
začal	začít	k5eAaPmAgMnS	začít
Wilson	Wilson	k1gNnSc4	Wilson
kritizovat	kritizovat	k5eAaImF	kritizovat
korupci	korupce	k1gFnSc4	korupce
i	i	k8xC	i
klientelismus	klientelismus	k1gInSc4	klientelismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
reformy	reforma	k1gFnPc1	reforma
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
také	také	k9	také
kontrol	kontrola	k1gFnPc2	kontrola
cen	cena	k1gFnPc2	cena
komunálních	komunální	k2eAgFnPc2d1	komunální
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
sociálního	sociální	k2eAgNnSc2d1	sociální
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
kampaň	kampaň	k1gFnSc1	kampaň
za	za	k7c4	za
Wilsonovu	Wilsonův	k2eAgFnSc4d1	Wilsonova
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
téměř	téměř	k6eAd1	téměř
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zvolení	zvolení	k1gNnSc6	zvolení
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
Wilsona	Wilsona	k1gFnSc1	Wilsona
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgFnPc4d1	mnohá
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
faktem	fakt	k1gInSc7	fakt
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
rozštěpení	rozštěpení	k1gNnSc6	rozštěpení
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
mezi	mezi	k7c4	mezi
stoupence	stoupenec	k1gMnSc4	stoupenec
T.	T.	kA	T.
<g/>
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
a	a	k8xC	a
Tafta	Tafta	k1gMnSc1	Tafta
byl	být	k5eAaImAgMnS	být
úspěch	úspěch	k1gInSc4	úspěch
demokratů	demokrat	k1gMnPc2	demokrat
téměř	téměř	k6eAd1	téměř
zaručen	zaručit	k5eAaPmNgInS	zaručit
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
společný	společný	k2eAgInSc1d1	společný
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pokrokový	pokrokový	k2eAgMnSc1d1	pokrokový
<g/>
,	,	kIx,	,
program	program	k1gInSc4	program
obou	dva	k4xCgMnPc2	dva
hlavních	hlavní	k2eAgMnPc2d1	hlavní
favoritů	favorit	k1gMnPc2	favorit
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
i	i	k8xC	i
Wilsona	Wilson	k1gMnSc4	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Předvolební	předvolební	k2eAgFnPc1d1	předvolební
debaty	debata	k1gFnPc1	debata
měly	mít	k5eAaImAgFnP	mít
nezvykle	zvykle	k6eNd1	zvykle
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sčítání	sčítání	k1gNnSc6	sčítání
hlasů	hlas	k1gInPc2	hlas
získal	získat	k5eAaPmAgInS	získat
Wilson	Wilson	k1gInSc1	Wilson
6	[number]	k4	6
286	[number]	k4	286
214	[number]	k4	214
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
za	za	k7c4	za
progresivistickou	progresivistický	k2eAgFnSc4d1	progresivistická
stranu	strana	k1gFnSc4	strana
4	[number]	k4	4
216	[number]	k4	216
0	[number]	k4	0
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
republikánský	republikánský	k2eAgMnSc1d1	republikánský
prezident	prezident	k1gMnSc1	prezident
Taft	taft	k1gInSc4	taft
3	[number]	k4	3
486	[number]	k4	486
922	[number]	k4	922
a	a	k8xC	a
socialistický	socialistický	k2eAgMnSc1d1	socialistický
kandidát	kandidát	k1gMnSc1	kandidát
Debs	Debs	k1gInSc4	Debs
897	[number]	k4	897
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
ale	ale	k8xC	ale
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
pouze	pouze	k6eAd1	pouze
58	[number]	k4	58
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
chtěl	chtít	k5eAaImAgMnS	chtít
jednat	jednat	k5eAaImF	jednat
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
hned	hned	k6eAd1	hned
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
zlomit	zlomit	k5eAaPmF	zlomit
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
reformám	reforma	k1gFnPc3	reforma
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
tedy	tedy	k9	tedy
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
demokratů	demokrat	k1gMnPc2	demokrat
zasahovat	zasahovat	k5eAaImF	zasahovat
i	i	k9	i
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Wilson	Wilson	k1gInSc1	Wilson
program	program	k1gInSc4	program
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgFnSc2d1	Nové
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
spočíval	spočívat	k5eAaImAgInS	spočívat
na	na	k7c6	na
zvýšených	zvýšený	k2eAgInPc6d1	zvýšený
zásazích	zásah	k1gInPc6	zásah
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
nekontrolovaly	kontrolovat	k5eNaImAgFnP	kontrolovat
úzké	úzký	k2eAgFnPc1d1	úzká
skupiny	skupina	k1gFnPc1	skupina
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
měli	mít	k5eAaImAgMnP	mít
občané	občan	k1gMnPc1	občan
znovu	znovu	k6eAd1	znovu
stejné	stejný	k2eAgFnPc4d1	stejná
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dokázal	dokázat	k5eAaPmAgInS	dokázat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
drastického	drastický	k2eAgNnSc2d1	drastické
snížení	snížení	k1gNnSc2	snížení
dovozních	dovozní	k2eAgNnPc2d1	dovozní
cel	clo	k1gNnPc2	clo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ustanovením	ustanovení	k1gNnSc7	ustanovení
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
antimonopolního	antimonopolní	k2eAgInSc2d1	antimonopolní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgMnS	prosadit
také	také	k9	také
několik	několik	k4yIc1	několik
základních	základní	k2eAgFnPc2d1	základní
sociálních	sociální	k2eAgFnPc2d1	sociální
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
mělo	mít	k5eAaImAgNnS	mít
největší	veliký	k2eAgInSc4d3	veliký
dopad	dopad	k1gInSc4	dopad
zavedení	zavedení	k1gNnSc1	zavedení
systému	systém	k1gInSc2	systém
federálních	federální	k2eAgFnPc2d1	federální
bankovních	bankovní	k2eAgFnPc2d1	bankovní
rezerv	rezerva	k1gFnPc2	rezerva
(	(	kIx(	(
<g/>
výnosem	výnos	k1gInSc7	výnos
Federal	Federal	k1gFnSc2	Federal
Reserves	Reserves	k1gInSc1	Reserves
Act	Act	k1gFnSc4	Act
z	z	k7c2	z
konce	konec	k1gInSc2	konec
prosince	prosinec	k1gInSc2	prosinec
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
slibovala	slibovat	k5eAaImAgFnS	slibovat
stabilizace	stabilizace	k1gFnSc1	stabilizace
měny	měna	k1gFnSc2	měna
po	po	k7c6	po
tehdy	tehdy	k6eAd1	tehdy
nedávné	dávný	k2eNgFnSc6d1	nedávná
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
recesi	recese	k1gFnSc3	recese
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
coby	coby	k?	coby
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
USA	USA	kA	USA
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Federal	Federal	k1gFnSc1	Federal
Reserve	Reserev	k1gFnSc2	Reserev
Bank	bank	k1gInSc1	bank
(	(	kIx(	(
<g/>
banka	banka	k1gFnSc1	banka
federálních	federální	k2eAgFnPc2d1	federální
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
legislativě	legislativa	k1gFnSc6	legislativa
zakotveny	zakotvit	k5eAaPmNgFnP	zakotvit
její	její	k3xOp3gFnPc1	její
funkce	funkce	k1gFnPc1	funkce
v	v	k7c6	v
stanovování	stanovování	k1gNnSc6	stanovování
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
a	a	k8xC	a
emisí	emise	k1gFnPc2	emise
národní	národní	k2eAgFnSc2d1	národní
měny	měna	k1gFnSc2	měna
do	do	k7c2	do
monetárního	monetární	k2eAgInSc2d1	monetární
systému	systém	k1gInSc2	systém
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
působení	působení	k1gNnSc2	působení
28	[number]	k4	28
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c6	na
vnitropolitické	vnitropolitický	k2eAgFnSc6d1	vnitropolitická
scéně	scéna	k1gFnSc6	scéna
dalo	dát	k5eAaPmAgNnS	dát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
zázračně	zázračně	k6eAd1	zázračně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nutné	nutný	k2eAgNnSc4d1	nutné
již	již	k6eAd1	již
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
podařilo	podařit	k5eAaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
úspěchy	úspěch	k1gInPc4	úspěch
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
připomenout	připomenout	k5eAaPmF	připomenout
jeho	jeho	k3xOp3gInSc4	jeho
rasistický	rasistický	k2eAgInSc4d1	rasistický
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
afroamerické	afroamerický	k2eAgFnSc3d1	afroamerická
menšině	menšina	k1gFnSc3	menšina
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
otevřené	otevřený	k2eAgFnSc3d1	otevřená
podpoře	podpora	k1gFnSc3	podpora
segregace	segregace	k1gFnSc2	segregace
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
hodnocení	hodnocení	k1gNnSc1	hodnocení
Wilsonova	Wilsonův	k2eAgNnSc2d1	Wilsonovo
působení	působení	k1gNnSc2	působení
méně	málo	k6eAd2	málo
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jen	jen	k9	jen
jako	jako	k9	jako
turista	turista	k1gMnSc1	turista
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
znal	znát	k5eAaImAgInS	znát
špatně	špatně	k6eAd1	špatně
evropské	evropský	k2eAgFnPc4d1	Evropská
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
o	o	k7c6	o
zbytku	zbytek	k1gInSc6	zbytek
světa	svět	k1gInSc2	svět
ani	ani	k8xC	ani
nemluvě	nemluva	k1gFnSc6	nemluva
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
zapáleným	zapálený	k2eAgInSc7d1	zapálený
idealismem	idealismus	k1gInSc7	idealismus
tvořila	tvořit	k5eAaImAgFnS	tvořit
tato	tento	k3xDgFnSc1	tento
neznalost	neznalost	k1gFnSc1	neznalost
výbušnou	výbušný	k2eAgFnSc4d1	výbušná
směs	směs	k1gFnSc4	směs
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Prvním	první	k4xOgMnSc7	první
problémem	problém	k1gInSc7	problém
byl	být	k5eAaImAgMnS	být
postoj	postoj	k1gInSc4	postoj
USA	USA	kA	USA
k	k	k7c3	k
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
moci	moc	k1gFnPc4	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
diktátor	diktátor	k1gMnSc1	diktátor
Huerta	Huert	k1gMnSc2	Huert
<g/>
.	.	kIx.	.
</s>
<s>
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
snaha	snaha	k1gFnSc1	snaha
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
výsledek	výsledek	k1gInSc4	výsledek
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
okupaci	okupace	k1gFnSc3	okupace
Veracruzu	Veracruz	k1gInSc2	Veracruz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Nevěděl	vědět	k5eNaImAgInS	vědět
si	se	k3xPyFc3	se
také	také	k6eAd1	také
rady	rada	k1gFnSc2	rada
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
Dominikánskou	dominikánský	k2eAgFnSc4d1	Dominikánská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc4	Haiti
a	a	k8xC	a
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
okupovány	okupován	k2eAgInPc1d1	okupován
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ovládány	ovládat	k5eAaImNgInP	ovládat
ekonomicky	ekonomicky	k6eAd1	ekonomicky
americkými	americký	k2eAgMnPc7d1	americký
investory	investor	k1gMnPc7	investor
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
Ameriky	Amerika	k1gFnPc1	Amerika
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
obydlených	obydlený	k2eAgInPc2d1	obydlený
světadílů	světadíl	k1gInPc2	světadíl
asi	asi	k9	asi
nejvíc	hodně	k6eAd3	hodně
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
na	na	k7c6	na
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Wilsonovi	Wilsonův	k2eAgMnPc1d1	Wilsonův
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
válčících	válčící	k2eAgFnPc2d1	válčící
stran	strana	k1gFnPc2	strana
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
bližší	blízký	k2eAgFnSc3d2	bližší
Dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
soukromých	soukromý	k2eAgInPc6d1	soukromý
dopisech	dopis	k1gInPc6	dopis
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
poměrně	poměrně	k6eAd1	poměrně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
demokratické	demokratický	k2eAgInPc1d1	demokratický
a	a	k8xC	a
pacifistické	pacifistický	k2eAgInPc1d1	pacifistický
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
militarismem	militarismus	k1gInSc7	militarismus
nikdy	nikdy	k6eAd1	nikdy
neshodnou	shodnout	k5eNaPmIp3nP	shodnout
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
sarajevském	sarajevský	k2eAgInSc6d1	sarajevský
atentátu	atentát	k1gInSc6	atentát
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
málo	málo	k6eAd1	málo
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
částečně	částečně	k6eAd1	částečně
probudila	probudit	k5eAaPmAgFnS	probudit
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
napadení	napadení	k1gNnSc1	napadení
Belgie	Belgie	k1gFnSc2	Belgie
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Wilson	Wilson	k1gMnSc1	Wilson
proklamaci	proklamace	k1gFnSc4	proklamace
neutrality	neutralita	k1gFnSc2	neutralita
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
všechny	všechen	k3xTgMnPc4	všechen
Američany	Američan	k1gMnPc4	Američan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
nestranní	stranní	k2eNgMnPc1d1	nestranní
v	v	k7c6	v
myšlení	myšlení	k1gNnSc6	myšlení
i	i	k8xC	i
činech	čin	k1gInPc6	čin
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Ellen	Ellen	k1gInSc4	Ellen
Wilsonová	Wilsonový	k2eAgFnSc1d1	Wilsonová
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentovi	prezidentův	k2eAgMnPc1d1	prezidentův
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
odešel	odejít	k5eAaPmAgMnS	odejít
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
rádce	rádce	k1gMnSc1	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1915	[number]	k4	1915
s	s	k7c7	s
washingtonskou	washingtonský	k2eAgFnSc7d1	Washingtonská
vdovou	vdova	k1gFnSc7	vdova
Edith	Edith	k1gInSc1	Edith
Galtovou	Galtová	k1gFnSc4	Galtová
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
jasnější	jasný	k2eAgNnSc1d2	jasnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
uvízla	uvíznout	k5eAaPmAgFnS	uvíznout
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
zákopů	zákop	k1gInPc2	zákop
a	a	k8xC	a
Británie	Británie	k1gFnSc1	Británie
i	i	k8xC	i
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
zničit	zničit	k5eAaPmF	zničit
jeden	jeden	k4xCgMnSc1	jeden
druhého	druhý	k4xOgInSc2	druhý
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
uvalili	uvalit	k5eAaPmAgMnP	uvalit
omezení	omezení	k1gNnSc4	omezení
i	i	k8xC	i
na	na	k7c4	na
neutrální	neutrální	k2eAgInSc4d1	neutrální
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
přístavy	přístav	k1gInPc1	přístav
byly	být	k5eAaImAgInP	být
blokovány	blokován	k2eAgInPc1d1	blokován
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
zaminováno	zaminován	k2eAgNnSc1d1	zaminováno
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
tedy	tedy	k9	tedy
používala	používat	k5eAaImAgFnS	používat
miny	mina	k1gFnPc4	mina
a	a	k8xC	a
válečné	válečný	k2eAgFnPc1d1	válečná
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
ponorky	ponorka	k1gFnSc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
ekonomiku	ekonomika	k1gFnSc4	ekonomika
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
cynicky	cynicky	k6eAd1	cynicky
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
skutečným	skutečný	k2eAgNnSc7d1	skutečné
požehnáním	požehnání	k1gNnSc7	požehnání
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stupňující	stupňující	k2eAgInSc4d1	stupňující
boj	boj	k1gInSc4	boj
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
ale	ale	k9	ale
znamenal	znamenat	k5eAaImAgInS	znamenat
ohrožení	ohrožení	k1gNnSc3	ohrožení
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
i	i	k8xC	i
lidské	lidský	k2eAgInPc4d1	lidský
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
bez	bez	k1gInSc1	bez
varování	varování	k1gNnPc2	varování
potopen	potopit	k5eAaPmNgInS	potopit
u	u	k7c2	u
irských	irský	k2eAgInPc2d1	irský
břehů	břeh	k1gInPc2	břeh
anglický	anglický	k2eAgInSc1d1	anglický
zámořský	zámořský	k2eAgInSc1d1	zámořský
parník	parník	k1gInSc1	parník
Luisitania	Luisitanium	k1gNnSc2	Luisitanium
německou	německý	k2eAgFnSc7d1	německá
ponorkou	ponorka	k1gFnSc7	ponorka
U-	U-	k1gFnSc2	U-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
1198	[number]	k4	1198
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
139	[number]	k4	139
Američanů	Američan	k1gMnPc2	Američan
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
reakce	reakce	k1gFnSc1	reakce
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
nepochopitelná	pochopitelný	k2eNgFnSc1d1	nepochopitelná
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
příliš	příliš	k6eAd1	příliš
hrdý	hrdý	k2eAgMnSc1d1	hrdý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bojoval	bojovat	k5eAaImAgMnS	bojovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
pojetí	pojetí	k1gNnSc4	pojetí
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
jist	jist	k2eAgMnSc1d1	jist
svým	svůj	k3xOyFgNnSc7	svůj
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
jiné	jiná	k1gFnPc4	jiná
použitím	použití	k1gNnSc7	použití
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
měla	mít	k5eAaImAgNnP	mít
pro	pro	k7c4	pro
zastánce	zastánce	k1gMnSc4	zastánce
Británie	Británie	k1gFnSc2	Británie
účinek	účinek	k1gInSc4	účinek
rudého	rudý	k2eAgInSc2d1	rudý
hadru	hadr	k1gInSc2	hadr
na	na	k7c4	na
býka	býk	k1gMnSc4	býk
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
byl	být	k5eAaImAgMnS	být
obviňován	obviňován	k2eAgInSc4d1	obviňován
z	z	k7c2	z
příliš	příliš	k6eAd1	příliš
slabošského	slabošský	k2eAgInSc2d1	slabošský
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednáváním	vyjednávání	k1gNnSc7	vyjednávání
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
zmírnit	zmírnit	k5eAaPmF	zmírnit
ponorkovou	ponorkový	k2eAgFnSc4d1	ponorková
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
očividné	očividný	k2eAgNnSc1d1	očividné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
konečné	konečný	k2eAgNnSc4d1	konečné
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Wilson	Wilson	k1gMnSc1	Wilson
stále	stále	k6eAd1	stále
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c4	v
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
jen	jen	k9	jen
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
pro	pro	k7c4	pro
válečné	válečný	k2eAgFnPc4d1	válečná
přípravy	příprava	k1gFnPc4	příprava
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
chtěl	chtít	k5eAaImAgMnS	chtít
zabodovat	zabodovat	k5eAaPmF	zabodovat
u	u	k7c2	u
pacifistických	pacifistický	k2eAgMnPc2d1	pacifistický
voličů	volič	k1gMnPc2	volič
v	v	k7c6	v
blížících	blížící	k2eAgFnPc6d1	blížící
se	se	k3xPyFc4	se
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
slogan	slogan	k1gInSc1	slogan
"	"	kIx"	"
<g/>
udržel	udržet	k5eAaPmAgInS	udržet
nás	my	k3xPp1nPc2	my
mimo	mimo	k7c4	mimo
válku	válka	k1gFnSc4	válka
<g/>
"	"	kIx"	"
bodoval	bodovat	k5eAaImAgMnS	bodovat
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Wilson	Wilson	k1gMnSc1	Wilson
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
za	za	k7c4	za
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
vděčil	vděčit	k5eAaImAgMnS	vděčit
hlavně	hlavně	k6eAd1	hlavně
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
středozápadu	středozápad	k1gInSc2	středozápad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Německo	Německo	k1gNnSc4	Německo
neomezenou	omezený	k2eNgFnSc4d1	neomezená
ponorkovou	ponorkový	k2eAgFnSc4d1	ponorková
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zachycena	zachytit	k5eAaPmNgFnS	zachytit
nóta	nóta	k1gFnSc1	nóta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
poslal	poslat	k5eAaPmAgMnS	poslat
německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Zimmermann	Zimmermann	k1gMnSc1	Zimmermann
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
Eckhardtovi	Eckhardtův	k2eAgMnPc1d1	Eckhardtův
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
návrh	návrh	k1gInSc4	návrh
nabídnout	nabídnout	k5eAaPmF	nabídnout
Mexiku	Mexiko	k1gNnSc3	Mexiko
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
pokud	pokud	k8xS	pokud
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
na	na	k7c4	na
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
kapka	kapka	k1gFnSc1	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Wilsonovou	Wilsonový	k2eAgFnSc7d1	Wilsonová
povinností	povinnost	k1gFnSc7	povinnost
nyní	nyní	k6eAd1	nyní
bylo	být	k5eAaImAgNnS	být
rychle	rychle	k6eAd1	rychle
připravit	připravit	k5eAaPmF	připravit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1917	[number]	k4	1917
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
deset	deset	k4xCc1	deset
miliónů	milión	k4xCgInPc2	milión
mužů	muž	k1gMnPc2	muž
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
jedenadvaceti	jedenadvacet	k4xCc2	jedenadvacet
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
amerických	americký	k2eAgFnPc2d1	americká
jednotek	jednotka	k1gFnPc2	jednotka
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Pershinga	Pershing	k1gMnSc2	Pershing
se	se	k3xPyFc4	se
zúčastňovalo	zúčastňovat	k5eAaImAgNnS	zúčastňovat
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
od	od	k7c2	od
června	červen	k1gInSc2	červen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Dohodu	dohoda	k1gFnSc4	dohoda
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
vyčerpané	vyčerpaný	k2eAgFnPc1d1	vyčerpaná
německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
nemohly	moct	k5eNaImAgFnP	moct
jejich	jejich	k3xOp3gInSc4	jejich
postup	postup	k1gInSc4	postup
úplně	úplně	k6eAd1	úplně
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
frontě	fronta	k1gFnSc6	fronta
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
vybojovat	vybojovat	k5eAaPmF	vybojovat
nejednu	nejeden	k4xCyIgFnSc4	nejeden
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
válečný	válečný	k2eAgInSc4d1	válečný
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
geniálním	geniální	k2eAgMnSc7d1	geniální
bankéřem	bankéř	k1gMnSc7	bankéř
Baruchem	Baruch	k1gMnSc7	Baruch
<g/>
,	,	kIx,	,
reorganizoval	reorganizovat	k5eAaBmAgMnS	reorganizovat
průmysl	průmysl	k1gInSc4	průmysl
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
plnit	plnit	k5eAaImF	plnit
vládní	vládní	k2eAgFnPc4d1	vládní
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
také	také	k9	také
zvýšeny	zvýšen	k2eAgFnPc1d1	zvýšena
daně	daň	k1gFnPc1	daň
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
financování	financování	k1gNnSc4	financování
války	válka	k1gFnSc2	válka
ale	ale	k8xC	ale
přišla	přijít	k5eAaPmAgFnS	přijít
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
"	"	kIx"	"
<g/>
půjčky	půjčka	k1gFnSc2	půjčka
svobody	svoboda	k1gFnSc2	svoboda
<g/>
"	"	kIx"	"
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
"	"	kIx"	"
<g/>
půjčka	půjčka	k1gFnSc1	půjčka
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
"	"	kIx"	"
vynesly	vynést	k5eAaPmAgInP	vynést
dohromady	dohromady	k6eAd1	dohromady
21	[number]	k4	21
400	[number]	k4	400
000	[number]	k4	000
000	[number]	k4	000
$	$	kIx~	$
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
vydatné	vydatný	k2eAgFnSc3d1	vydatná
propagandistické	propagandistický	k2eAgFnSc3d1	propagandistická
podpoře	podpora	k1gFnSc3	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
informaci	informace	k1gFnSc4	informace
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zahájil	zahájit	k5eAaPmAgInS	zahájit
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
pacifistické	pacifistický	k2eAgMnPc4d1	pacifistický
Američany	Američan	k1gMnPc4	Američan
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
protišpionážního	protišpionážní	k2eAgInSc2d1	protišpionážní
zákona	zákon	k1gInSc2	zákon
byly	být	k5eAaImAgFnP	být
uvězněny	uvězněn	k2eAgFnPc1d1	uvězněna
stovky	stovka	k1gFnPc1	stovka
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nevinně	vinně	k6eNd1	vinně
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
vůdce	vůdce	k1gMnSc1	vůdce
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
Eugene	Eugen	k1gInSc5	Eugen
V.	V.	kA	V.
Debs	Debs	k1gInSc1	Debs
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
bezprecedentní	bezprecedentní	k2eAgFnSc1d1	bezprecedentní
jednota	jednota	k1gFnSc1	jednota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
ohromné	ohromný	k2eAgNnSc1d1	ohromné
nadšení	nadšení	k1gNnSc1	nadšení
pro	pro	k7c4	pro
společné	společný	k2eAgNnSc4d1	společné
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
samozřejmě	samozřejmě	k6eAd1	samozřejmě
miliardy	miliarda	k4xCgFnPc4	miliarda
investované	investovaný	k2eAgFnPc4d1	investovaná
do	do	k7c2	do
státních	státní	k2eAgInPc2d1	státní
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
léta	léto	k1gNnPc4	léto
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
průmyslu	průmysl	k1gInSc2	průmysl
změnila	změnit	k5eAaPmAgFnS	změnit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
navždy	navždy	k6eAd1	navždy
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
Wilsonova	Wilsonův	k2eAgNnSc2d1	Wilsonovo
snažení	snažení	k1gNnSc2	snažení
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
ekonomika	ekonomika	k1gFnSc1	ekonomika
jako	jako	k8xC	jako
ideové	ideový	k2eAgNnSc1d1	ideové
obhájení	obhájení	k1gNnSc1	obhájení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
takové	takový	k3xDgInPc4	takový
válečné	válečný	k2eAgInPc4d1	válečný
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
zaručovaly	zaručovat	k5eAaImAgInP	zaručovat
budoucí	budoucí	k2eAgFnSc4d1	budoucí
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1918	[number]	k4	1918
přednesl	přednést	k5eAaPmAgInS	přednést
Kongresu	kongres	k1gInSc2	kongres
svých	svůj	k3xOyFgInPc2	svůj
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Volal	volat	k5eAaImAgMnS	volat
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
po	po	k7c6	po
otevřené	otevřený	k2eAgFnSc6d1	otevřená
diplomacii	diplomacie	k1gFnSc6	diplomacie
<g/>
,	,	kIx,	,
svobodě	svoboda	k1gFnSc6	svoboda
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
odzbrojení	odzbrojení	k1gNnSc6	odzbrojení
<g/>
,	,	kIx,	,
právu	právo	k1gNnSc6	právo
národů	národ	k1gInPc2	národ
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
vzniku	vznik	k1gInSc2	vznik
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
ztělesňovala	ztělesňovat	k5eAaImAgFnS	ztělesňovat
princip	princip	k1gInSc4	princip
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nečinila	činit	k5eNaImAgFnS	činit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgInPc7d1	velký
a	a	k8xC	a
malými	malý	k2eAgInPc7d1	malý
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
chtěl	chtít	k5eAaImAgMnS	chtít
tímto	tento	k3xDgInSc7	tento
programem	program	k1gInSc7	program
ukázat	ukázat	k5eAaPmF	ukázat
Spojencům	spojenec	k1gMnPc3	spojenec
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mír	mír	k1gInSc1	mír
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgMnS	poznamenat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zničilo	zničit	k5eAaPmAgNnS	zničit
všechny	všechen	k3xTgFnPc4	všechen
míry	míra	k1gFnPc4	míra
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
:	:	kIx,	:
snahou	snaha	k1gFnSc7	snaha
vítězů	vítěz	k1gMnPc2	vítěz
"	"	kIx"	"
<g/>
nahrabat	nahrabat	k5eAaPmF	nahrabat
si	se	k3xPyFc3	se
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
prosazována	prosazovat	k5eAaImNgFnS	prosazovat
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgNnSc1d1	oficiální
stanovisko	stanovisko	k1gNnSc1	stanovisko
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
primární	primární	k2eAgInSc4d1	primární
válečný	válečný	k2eAgInSc4d1	válečný
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
bodů	bod	k1gInPc2	bod
nadchlo	nadchnout	k5eAaPmAgNnS	nadchnout
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
intelektuálů	intelektuál	k1gMnPc2	intelektuál
i	i	k8xC	i
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
američtí	americký	k2eAgMnPc1d1	americký
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
a	a	k8xC	a
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
vzhlíželo	vzhlížet	k5eAaImAgNnS	vzhlížet
k	k	k7c3	k
prezidentu	prezident	k1gMnSc3	prezident
Wilsonovi	Wilson	k1gMnSc3	Wilson
jako	jako	k8xC	jako
k	k	k7c3	k
naději	naděje	k1gFnSc3	naděje
na	na	k7c4	na
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wilsonově	Wilsonův	k2eAgFnSc6d1	Wilsonova
koncepci	koncepce	k1gFnSc6	koncepce
měla	mít	k5eAaImAgFnS	mít
býti	být	k5eAaImF	být
střední	střední	k2eAgFnSc1d1	střední
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
Evropa	Evropa	k1gFnSc1	Evropa
malých	malý	k2eAgInPc2d1	malý
národů	národ	k1gInPc2	národ
vybudována	vybudován	k2eAgFnSc1d1	vybudována
jako	jako	k8xS	jako
velká	velký	k2eAgFnSc1d1	velká
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
plán	plán	k1gInSc4	plán
sloužila	sloužit	k5eAaImAgFnS	sloužit
Američanům	Američan	k1gMnPc3	Američan
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
jejich	jejich	k3xOp3gFnSc2	jejich
vlastní	vlastní	k2eAgFnSc2d1	vlastní
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
byl	být	k5eAaImAgMnS	být
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
staré	starý	k2eAgNnSc1d1	staré
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
přebudované	přebudovaný	k2eAgFnPc4d1	přebudovaná
a	a	k8xC	a
zorganizované	zorganizovaný	k2eAgFnPc4d1	zorganizovaná
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
národnostním	národnostní	k2eAgInSc6d1	národnostní
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
státi	stát	k5eAaImF	stát
jádrem	jádro	k1gNnSc7	jádro
takové	takový	k3xDgFnSc2	takový
příští	příští	k2eAgFnSc2d1	příští
evropské	evropský	k2eAgFnSc2d1	Evropská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
Wilson	Wilson	k1gInSc1	Wilson
v	v	k7c6	v
době	doba	k1gFnSc6	doba
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
upřímným	upřímný	k2eAgMnSc7d1	upřímný
přívržencem	přívrženec	k1gMnSc7	přívrženec
maximálního	maximální	k2eAgInSc2d1	maximální
programu	program	k1gInSc2	program
československé	československý	k2eAgFnSc2d1	Československá
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
samostatnost	samostatnost	k1gFnSc4	samostatnost
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Wilsona	Wilson	k1gMnSc4	Wilson
věcí	věc	k1gFnSc7	věc
méně	málo	k6eAd2	málo
významnou	významný	k2eAgFnSc7d1	významná
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
nebyl	být	k5eNaImAgMnS	být
býval	bývat	k5eAaImAgInS	bývat
vzat	vzat	k2eAgInSc1d1	vzat
náležitý	náležitý	k2eAgInSc1d1	náležitý
zřetel	zřetel	k1gInSc1	zřetel
na	na	k7c6	na
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Tekla	Tekla	k1gFnSc1	Tekla
krev	krev	k1gFnSc4	krev
statisíců	statisíce	k1gInPc2	statisíce
<g/>
,	,	kIx,	,
trpěly	trpět	k5eAaImAgInP	trpět
miliony	milion	k4xCgInPc1	milion
a	a	k8xC	a
pacifista	pacifista	k1gMnSc1	pacifista
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukončil	ukončit	k5eAaPmAgMnS	ukončit
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
obětovat	obětovat	k5eAaBmF	obětovat
naši	náš	k3xOp1gFnSc4	náš
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
zaručiti	zaručit	k5eAaPmF	zaručit
celistvost	celistvost	k1gFnSc4	celistvost
habsburské	habsburský	k2eAgFnSc2d1	habsburská
říše	říš	k1gFnSc2	říš
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
separátní	separátní	k2eAgInSc4d1	separátní
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
především	především	k6eAd1	především
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čtrnáct	čtrnáct	k4xCc4	čtrnáct
bodů	bod	k1gInPc2	bod
nikdo	nikdo	k3yNnSc1	nikdo
nebral	brát	k5eNaImAgMnS	brát
vážně	vážně	k6eAd1	vážně
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
potrestat	potrestat	k5eAaPmF	potrestat
Německo	Německo	k1gNnSc1	Německo
tak	tak	k6eAd1	tak
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
jako	jako	k9	jako
evropská	evropský	k2eAgFnSc1d1	Evropská
velmoc	velmoc	k1gFnSc1	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
ale	ale	k8xC	ale
plný	plný	k2eAgMnSc1d1	plný
nadšení	nadšení	k1gNnSc4	nadšení
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1918	[number]	k4	1918
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
triumfální	triumfální	k2eAgFnSc4d1	triumfální
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
hlavních	hlavní	k2eAgNnPc6d1	hlavní
městech	město	k1gNnPc6	město
zemí	zem	k1gFnPc2	zem
Dohody	dohoda	k1gFnPc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
následné	následný	k2eAgFnSc6d1	následná
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Paříží	Paříž	k1gFnPc2	Paříž
se	se	k3xPyFc4	se
tvrdohlavě	tvrdohlavě	k6eAd1	tvrdohlavě
držel	držet	k5eAaImAgInS	držet
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
vznik	vznik	k1gInSc1	vznik
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
svým	svůj	k3xOyFgMnPc3	svůj
spojencům	spojenec	k1gMnPc3	spojenec
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
reparací	reparace	k1gFnPc2	reparace
či	či	k8xC	či
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
životní	životní	k2eAgFnSc1d1	životní
prohra	prohra	k1gFnSc1	prohra
ale	ale	k9	ale
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
Wilsona	Wilson	k1gMnSc4	Wilson
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
idea	idea	k1gFnSc1	idea
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
nepochopením	nepochopení	k1gNnSc7	nepochopení
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
nebyla	být	k5eNaImAgFnS	být
připravena	připraven	k2eAgFnSc1d1	připravena
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
konsenzu	konsenz	k1gInSc3	konsenz
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
nezíská	získat	k5eNaPmIp3nS	získat
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
dvoutřetinovou	dvoutřetinový	k2eAgFnSc4d1	dvoutřetinová
většinu	většina	k1gFnSc4	většina
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
Wilson	Wilson	k1gMnSc1	Wilson
změnit	změnit	k5eAaPmF	změnit
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
propagační	propagační	k2eAgFnSc7d1	propagační
cestou	cesta	k1gFnSc7	cesta
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ohromné	ohromný	k2eAgNnSc1d1	ohromné
pracovní	pracovní	k2eAgNnSc1d1	pracovní
nasazení	nasazení	k1gNnSc1	nasazení
se	se	k3xPyFc4	se
ale	ale	k9	ale
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
1919	[number]	k4	1919
po	po	k7c6	po
záchvatu	záchvat	k1gInSc6	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
částečně	částečně	k6eAd1	částečně
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
neobjevil	objevit	k5eNaPmAgInS	objevit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byla	být	k5eAaImAgFnS	být
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
Senátem	senát	k1gInSc7	senát
definitivně	definitivně	k6eAd1	definitivně
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
do	do	k7c2	do
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
nikdy	nikdy	k6eAd1	nikdy
nevstoupily	vstoupit	k5eNaPmAgFnP	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
už	už	k9	už
nikdy	nikdy	k6eAd1	nikdy
neopustil	opustit	k5eNaPmAgInS	opustit
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
praktický	praktický	k2eAgInSc4d1	praktický
výsledek	výsledek	k1gInSc4	výsledek
Wilsonova	Wilsonův	k2eAgNnSc2d1	Wilsonovo
působení	působení	k1gNnSc2	působení
nejednoznačný	jednoznačný	k2eNgInSc1d1	nejednoznačný
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
důležitým	důležitý	k2eAgMnSc7d1	důležitý
průkopníkem	průkopník	k1gMnSc7	průkopník
<g/>
.	.	kIx.	.
</s>
<s>
Spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
současné	současný	k2eAgFnSc2d1	současná
OSN	OSN	kA	OSN
Franklin	Franklina	k1gFnPc2	Franklina
Delano	Delana	k1gFnSc5	Delana
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
dědicem	dědic	k1gMnSc7	dědic
jeho	jeho	k3xOp3gInSc2	jeho
odkazu	odkaz	k1gInSc2	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Wilsonových	Wilsonův	k2eAgInPc2d1	Wilsonův
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
nenaplněno	naplněn	k2eNgNnSc1d1	nenaplněno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
položily	položit	k5eAaPmAgInP	položit
základ	základ	k1gInSc4	základ
mnoha	mnoho	k4c3	mnoho
moderním	moderní	k2eAgFnPc3d1	moderní
institucím	instituce	k1gFnPc3	instituce
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
