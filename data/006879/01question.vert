<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
český	český	k2eAgMnSc1d1	český
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
postavičky	postavička	k1gFnSc2	postavička
Krtečka	krteček	k1gMnSc2	krteček
<g/>
?	?	kIx.	?
</s>
