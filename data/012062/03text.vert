<p>
<s>
Denver	Denver	k1gInSc1	Denver
Rangers	Rangersa	k1gFnPc2	Rangersa
byl	být	k5eAaImAgInS	být
profesionální	profesionální	k2eAgInSc1d1	profesionální
americký	americký	k2eAgInSc1d1	americký
klub	klub	k1gInSc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
profesionální	profesionální	k2eAgFnSc6d1	profesionální
soutěži	soutěž	k1gFnSc6	soutěž
International	International	k1gMnSc2	International
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
Rangers	Rangers	k6eAd1	Rangers
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
sezóně	sezóna	k1gFnSc6	sezóna
v	v	k7c6	v
IHL	IHL	kA	IHL
skončily	skončit	k5eAaPmAgFnP	skončit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
play-off	playff	k1gInSc1	play-off
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
farmu	farma	k1gFnSc4	farma
New	New	k1gMnPc2	New
Yorku	York	k1gInSc2	York
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrával	odehrávat	k5eAaImAgInS	odehrávat
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
McNichols	McNicholsa	k1gFnPc2	McNicholsa
Sports	Sports	k1gInSc1	Sports
Arena	Aren	k1gInSc2	Aren
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
16	[number]	k4	16
061	[number]	k4	061
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1	klubová
barvy	barva	k1gFnPc1	barva
byly	být	k5eAaImAgFnP	být
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
týmu	tým	k1gInSc2	tým
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
Checkers	Checkersa	k1gFnPc2	Checkersa
do	do	k7c2	do
Denveru	Denver	k1gInSc2	Denver
<g/>
.	.	kIx.	.
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgMnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
přestěhováním	přestěhování	k1gNnSc7	přestěhování
do	do	k7c2	do
Phoenixu	Phoenix	k1gInSc2	Phoenix
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
tým	tým	k1gInSc1	tým
Phoenix	Phoenix	k1gInSc1	Phoenix
Roadrunners	Roadrunners	k1gInSc1	Roadrunners
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Colorado	Colorado	k1gNnSc1	Colorado
Rangers	Rangersa	k1gFnPc2	Rangersa
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Denver	Denver	k1gInSc1	Denver
Rangers	Rangers	k1gInSc1	Rangers
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
ligové	ligový	k2eAgFnSc2d1	ligová
účasti	účast	k1gFnSc2	účast
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
International	International	k1gFnSc2	International
Hockey	Hockea	k1gFnSc2	Hockea
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc2d1	západní
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
klubu	klub	k1gInSc2	klub
na	na	k7c4	na
hockeydb	hockeydb	k1gInSc4	hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
