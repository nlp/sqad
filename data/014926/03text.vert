<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gMnSc1
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
Vrchol	vrchol	k1gInSc1
</s>
<s>
5321	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
2046	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Izolace	izolace	k1gFnPc1
</s>
<s>
231	#num#	k4
km	km	kA
→	→	k?
Nevado	Nevada	k1gFnSc5
del	del	k?
Huila	Huila	k1gFnSc1
Seznamy	seznam	k1gInPc4
</s>
<s>
Ultraprominentní	Ultraprominentní	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
Kolumbie	Kolumbie	k1gFnSc2
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Centrální	centrální	k2eAgMnSc1d1
Kordillera	Kordiller	k1gMnSc4
<g/>
,	,	kIx,
Andy	Anda	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
75	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc4
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
stratovulkán	stratovulkán	k1gInSc1
Erupce	erupce	k1gFnSc2
</s>
<s>
1991	#num#	k4
Hornina	hornina	k1gFnSc1
</s>
<s>
sopečné	sopečný	k2eAgInPc1d1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiza	k1gFnPc2
je	být	k5eAaImIp3nS
masivní	masivní	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
nacházející	nacházející	k2eAgFnSc1d1
se	se	k3xPyFc4
asi	asi	k9
129	#num#	k4
kilometrů	kilometr	k1gInPc2
západně	západně	k6eAd1
od	od	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Bogotá	Bogotá	k1gFnSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stratovulkán	stratovulkán	k1gInSc1
složený	složený	k2eAgInSc1d1
z	z	k7c2
mnoha	mnoho	k4c2
vrstev	vrstva	k1gFnPc2
lávy	láva	k1gFnSc2
střídající	střídající	k2eAgFnSc2d1
se	se	k3xPyFc4
z	z	k7c2
tvrzeného	tvrzený	k2eAgInSc2d1
sopečného	sopečný	k2eAgInSc2d1
popelu	popel	k1gInSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
pyroklastických	pyroklastický	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgInSc4d1
už	už	k6eAd1
asi	asi	k9
dva	dva	k4xCgInPc1
miliony	milion	k4xCgInPc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vytvořily	vytvořit	k5eAaPmAgFnP
jej	on	k3xPp3gNnSc4
pleistocénní	pleistocénní	k2eAgFnPc1d1
andezitové	andezitový	k2eAgFnPc1d1
a	a	k8xC
dacitové	dacitový	k2eAgFnPc1d1
lávy	láva	k1gFnPc1
a	a	k8xC
pyroklastika	pyroklastika	k1gFnSc1
nad	nad	k7c7
subdukční	subdukční	k2eAgFnSc7d1
zónou	zóna	k1gFnSc7
desky	deska	k1gFnSc2
Nazca	Nazc	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jako	jako	k9
většinu	většina	k1gFnSc4
kolumbijských	kolumbijský	k2eAgFnPc2d1
sopek	sopka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sopka	sopka	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známou	známá	k1gFnSc7
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
katastrofickou	katastrofický	k2eAgFnSc4d1
erupci	erupce	k1gFnSc4
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1985	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
horké	horký	k2eAgInPc1d1
pyroklastické	pyroklastický	k2eAgInPc1d1
proudy	proud	k1gInPc1
roztavily	roztavit	k5eAaPmAgInP
sněhový	sněhový	k2eAgInSc4d1
pokryv	pokryv	k1gInSc4
vrcholu	vrchol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
lahary	lahar	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
dosáhly	dosáhnout	k5eAaPmAgFnP
vzdálenosti	vzdálenost	k1gFnPc4
až	až	k9
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
úplně	úplně	k6eAd1
zalily	zalít	k5eAaPmAgFnP
město	město	k1gNnSc4
Armero	Armero	k1gNnSc1
(	(	kIx(
<g/>
28	#num#	k4
700	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
několik	několik	k4yIc4
menších	malý	k2eAgFnPc2d2
vesnic	vesnice	k1gFnPc2
6	#num#	k4
metrů	metr	k1gInPc2
silnou	silný	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
bahna	bahno	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zahynulo	zahynout	k5eAaPmAgNnS
přibližně	přibližně	k6eAd1
25	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
erupci	erupce	k1gFnSc6
sopky	sopka	k1gFnSc2
Mont	Mont	k2eAgInSc1d1
Pelée	Pelée	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1902	#num#	k4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
co	co	k9
do	do	k7c2
počtu	počet	k1gInSc2
lidských	lidský	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
druhá	druhý	k4xOgFnSc1
největší	veliký	k2eAgFnSc1d3
sopečná	sopečný	k2eAgFnSc1d1
katastrofa	katastrofa	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
podobným	podobný	k2eAgMnPc3d1
<g/>
,	,	kIx,
ale	ale	k8xC
méně	málo	k6eAd2
smrtícím	smrtící	k2eAgInPc3d1
incidentům	incident	k1gInPc3
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
také	také	k9
v	v	k7c6
letech	léto	k1gNnPc6
1595	#num#	k4
a	a	k8xC
1845	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
malých	malý	k2eAgFnPc2d1
erupcí	erupce	k1gFnPc2
staly	stát	k5eAaPmAgInP
velké	velký	k2eAgInPc1d1
lahary	lahar	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
2	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
1	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
3	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
2	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
4	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
3	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
5	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
4	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
6	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
5	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
7	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
6	#num#	k4
ul	ul	kA
<g/>
{	{	kIx(
<g/>
display	displaa	k1gMnSc2
<g/>
:	:	kIx,
<g/>
none	none	k6eAd1
<g/>
}	}	kIx)
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
pohoří	pohoří	k1gNnSc2
Andy	Anda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sopka	sopka	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
sopečných	sopečný	k2eAgInPc2d1
masivů	masiv	k1gInPc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
dalších	další	k2eAgFnPc2d1
pěti	pět	k4xCc2
sopek	sopka	k1gFnPc2
pokrytých	pokrytý	k2eAgFnPc2d1
velkými	velký	k2eAgInPc7d1
masivy	masiv	k1gInPc7
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
jsou	být	k5eAaImIp3nP
čtyři	čtyři	k4xCgMnPc1
stále	stále	k6eAd1
aktivní	aktivní	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
Ohnivém	ohnivý	k2eAgInSc6d1
kruhu	kruh	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
obklopena	obklopit	k5eAaPmNgFnS
Tichým	tichý	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
některé	některý	k3yIgNnSc1
z	z	k7c2
nejvíce	hodně	k6eAd3,k6eAd1
aktivních	aktivní	k2eAgFnPc2d1
sopek	sopka	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
sopka	sopka	k1gFnSc1
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc7
nejsevernější	severní	k2eAgFnSc7d3
sopkou	sopka	k1gFnSc7
ležící	ležící	k2eAgFnSc7d1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
vulkanické	vulkanický	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
andského	andský	k2eAgInSc2d1
vulkanického	vulkanický	k2eAgInSc2d1
pásu	pás	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
může	moct	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
explozivní	explozivní	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
horkého	horký	k2eAgInSc2d1
plynu	plyn	k1gInSc2
spojené	spojený	k2eAgInPc1d1
s	s	k7c7
pyroklastickými	pyroklastický	k2eAgInPc7d1
toky	tok	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mohou	moct	k5eAaImIp3nP
roztát	roztát	k5eAaPmF
sníh	sníh	k1gInSc4
z	z	k7c2
ledovce	ledovec	k1gInSc2
poblíž	poblíž	k7c2
vrcholu	vrchol	k1gInSc2
a	a	k8xC
vyprodukovat	vyprodukovat	k5eAaPmF
velké	velký	k2eAgInPc4d1
a	a	k8xC
někdy	někdy	k6eAd1
i	i	k9
zničující	zničující	k2eAgInPc4d1
lahary	lahar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
táhnoucí	táhnoucí	k2eAgInPc4d1
se	s	k7c7
65	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
východu	východ	k1gInSc2
k	k	k7c3
západu	západ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc1
vrchol	vrchol	k1gInSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
kráter	kráter	k1gInSc1
Arenas	Arenas	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
kilometr	kilometr	k1gInSc4
v	v	k7c6
průměru	průměr	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
240	#num#	k4
metrů	metr	k1gInPc2
hluboký	hluboký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ledovce	ledovec	k1gInPc1
</s>
<s>
Ledovec	ledovec	k1gInSc1
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
je	být	k5eAaImIp3nS
pokryt	pokrýt	k5eAaPmNgInS
ledovcem	ledovec	k1gInSc7
(	(	kIx(
<g/>
nevado	nevada	k1gFnSc5
znamená	znamenat	k5eAaImIp3nS
ve	v	k7c6
španělštině	španělština	k1gFnSc6
„	„	k?
<g/>
zasněžené	zasněžený	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nP
už	už	k9
tisíce	tisíc	k4xCgInPc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
28	#num#	k4
000	#num#	k4
až	až	k9
21	#num#	k4
000	#num#	k4
lety	let	k1gInPc7
byl	být	k5eAaImAgInS
ledovec	ledovec	k1gInSc1
obsazen	obsazen	k2eAgInSc1d1
1500	#num#	k4
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
masivu	masiv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
12	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ledové	ledový	k2eAgInPc4d1
příkrovy	příkrov	k1gInPc4
z	z	k7c2
poslední	poslední	k2eAgFnSc2d1
doby	doba	k1gFnSc2
ledové	ledový	k2eAgFnPc1d1
ustupovaly	ustupovat	k5eAaImAgFnP
<g/>
,	,	kIx,
pokrývaly	pokrývat	k5eAaImAgFnP
800	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
malé	malý	k2eAgFnSc2d1
doby	doba	k1gFnSc2
ledové	ledový	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
trvala	trvat	k5eAaImAgFnS
asi	asi	k9
od	od	k7c2
roku	rok	k1gInSc2
1600	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
<g/>
,	,	kIx,
masiv	masiv	k1gInSc1
pokrýval	pokrývat	k5eAaImAgInS
100	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
vrcholu	vrchol	k1gInSc3
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ledová	ledový	k2eAgFnSc1d1
pokrývka	pokrývka	k1gFnSc1
je	být	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
50	#num#	k4
metrů	metr	k1gInPc2
silná	silný	k2eAgFnSc1d1
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejsilnější	silný	k2eAgFnSc1d3
část	část	k1gFnSc1
vrcholu	vrchol	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
pod	pod	k7c7
ledovcem	ledovec	k1gInSc7
Nereides	Nereidesa	k1gFnPc2
na	na	k7c6
jihozápadních	jihozápadní	k2eAgInPc6d1
svazích	svah	k1gInPc6
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
ledovce	ledovec	k1gInSc2
hluboká	hluboký	k2eAgFnSc1d1
190	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ledovec	ledovec	k1gInSc1
na	na	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
a	a	k8xC
na	na	k7c6
východních	východní	k2eAgInPc6d1
svazích	svah	k1gInPc6
ztratil	ztratit	k5eAaPmAgMnS
většinu	většina	k1gFnSc4
ledu	led	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
při	při	k7c6
erupci	erupce	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
30	#num#	k4
metrů	metr	k1gInPc2
hluboký	hluboký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
z	z	k7c2
ledovce	ledovec	k1gInSc2
odtéká	odtékat	k5eAaImIp3nS
do	do	k7c2
řek	řeka	k1gFnPc2
Cauca	Cauc	k1gInSc2
a	a	k8xC
Magdalena	Magdalena	k1gFnSc1
přes	přes	k7c4
západní	západní	k2eAgNnPc4d1
a	a	k8xC
východní	východní	k2eAgNnPc4d1
úbočí	úbočí	k1gNnPc4
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
toky	toka	k1gFnSc2
této	tento	k3xDgFnSc2
vody	voda	k1gFnSc2
jsou	být	k5eAaImIp3nP
zásobou	zásoba	k1gFnSc7
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
pro	pro	k7c4
40	#num#	k4
okolních	okolní	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
a	a	k8xC
fauna	fauna	k1gFnSc1
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
málo	málo	k6eAd1
zalesněné	zalesněný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
nadmořské	nadmořský	k2eAgFnSc3d1
výšce	výška	k1gFnSc3
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
lesní	lesní	k2eAgInSc4d1
porost	porost	k1gInSc4
s	s	k7c7
rostoucí	rostoucí	k2eAgFnSc7d1
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Živočichové	živočich	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
žijí	žít	k5eAaImIp3nP
v	v	k7c6
okolí	okolí	k1gNnSc6
sopky	sopka	k1gFnSc2
jsou	být	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
jako	jako	k8xC,k8xS
ohrožení	ohrožení	k1gNnSc1
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
tapír	tapír	k1gMnSc1
horský	horský	k2eAgMnSc1d1
a	a	k8xC
medvěd	medvěd	k1gMnSc1
brýlatý	brýlatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sopka	sopka	k1gFnSc1
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
pro	pro	k7c4
27	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
15	#num#	k4
druhů	druh	k1gInPc2
ohroženo	ohrozit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgFnPc1d1
hrozby	hrozba	k1gFnPc1
</s>
<s>
Sopka	sopka	k1gFnSc1
i	i	k9
nadále	nadále	k6eAd1
představuje	představovat	k5eAaImIp3nS
vážnou	vážný	k2eAgFnSc4d1
hrozbu	hrozba	k1gFnSc4
pro	pro	k7c4
okolní	okolní	k2eAgNnPc4d1
města	město	k1gNnPc4
a	a	k8xC
vesnice	vesnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
je	být	k5eAaImIp3nS
riziko	riziko	k1gNnSc1
menších	malý	k2eAgFnPc2d2
erupcí	erupce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
způsobit	způsobit	k5eAaPmF
ztrátu	ztráta	k1gFnSc4
rovnováhy	rovnováha	k1gFnSc2
ledovců	ledovec	k1gInPc2
a	a	k8xC
spustit	spustit	k5eAaPmF
lahary	lahar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgInPc1
lahary	lahar	k1gInPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
pohybovat	pohybovat	k5eAaImF
až	až	k9
100	#num#	k4
km	km	kA
podél	podél	k7c2
říčního	říční	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
i	i	k8xC
několik	několik	k4yIc1
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
až	až	k9
500	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
žijících	žijící	k2eAgInPc2d1
v	v	k7c6
blízkém	blízký	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
jsou	být	k5eAaImIp3nP
ohroženy	ohrozit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgFnPc1d2
erupce	erupce	k1gFnPc1
pravděpodobnější	pravděpodobný	k2eAgFnPc1d2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
se	se	k3xPyFc4
brát	brát	k5eAaImF
navědomí	navědomí	k1gNnSc3
i	i	k8xC
vypuknutí	vypuknutí	k1gNnSc3
velkých	velký	k2eAgFnPc2d1
erupcí	erupce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
tato	tento	k3xDgFnSc1
sopka	sopka	k1gFnSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
již	již	k6eAd1
několikrát	několikrát	k6eAd1
způsobila	způsobit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
Kolumbie	Kolumbie	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
speciální	speciální	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgNnSc3,k3yIgNnSc3,k3yRgNnSc3
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
předcházet	předcházet	k5eAaImF
podobným	podobný	k2eAgFnPc3d1
tragédiím	tragédie	k1gFnPc3
jako	jako	k8xC,k8xS
té	ten	k3xDgFnSc3
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
lahary	lahar	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vznikly	vzniknout	k5eAaPmAgInP
při	při	k7c6
erupci	erupce	k1gFnSc6
<g/>
,	,	kIx,
zničily	zničit	k5eAaPmAgFnP
město	město	k1gNnSc1
Armero	Armero	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
vybuchla	vybuchnout	k5eAaPmAgFnS
znovu	znovu	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
evakuováno	evakuovat	k5eAaBmNgNnS
asi	asi	k9
2300	#num#	k4
lidí	člověk	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
podél	podél	k7c2
pěti	pět	k4xCc2
okolních	okolní	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiza	k1gFnPc2
</s>
<s>
Silný	silný	k2eAgInSc1d1
déšť	déšť	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
způsobil	způsobit	k5eAaPmAgInS
sesuv	sesuv	k1gInSc1
půdy	půda	k1gFnSc2
při	při	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
devět	devět	k4xCc1
mladíků	mladík	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
na	na	k7c6
průzkumných	průzkumný	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
v	v	k7c6
okolí	okolí	k1gNnSc6
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
</s>
<s>
Během	během	k7c2
září	září	k1gNnSc2
a	a	k8xC
října	říjen	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenán	k2eAgNnSc1d1
postupné	postupný	k2eAgNnSc1d1
zvýšení	zvýšení	k1gNnSc1
seismické	seismický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
poblíž	poblíž	k7c2
kráteru	kráter	k1gInSc2
Arenas	Arenasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujících	následující	k2eAgInPc2d1
čtyř	čtyři	k4xCgInPc2
měsíců	měsíc	k1gInPc2
bylo	být	k5eAaImAgNnS
dlouhé	dlouhý	k2eAgNnSc4d1
období	období	k1gNnSc4
zemětřesení	zemětřesení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
obavy	obava	k1gFnPc4
z	z	k7c2
vybuchnutí	vybuchnutí	k1gNnSc2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
bylo	být	k5eAaImAgNnS
vulkanické	vulkanický	k2eAgNnSc1d1
chvění	chvění	k1gNnSc1
zaznamenáno	zaznamenat	k5eAaPmNgNnS
zhruba	zhruba	k6eAd1
osmkrát	osmkrát	k6eAd1
tolik	tolik	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
letech	léto	k1gNnPc6
2009	#num#	k4
až	až	k9
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
větší	veliký	k2eAgInSc1d2
výskyt	výskyt	k1gInSc1
oxidu	oxid	k1gInSc2
siřičitého	siřičitý	k2eAgInSc2d1
doprovázený	doprovázený	k2eAgInSc4d1
malými	malý	k2eAgFnPc7d1
erupcemi	erupce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
února	únor	k1gInSc2
2012	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dramatickému	dramatický	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
zemětřesení	zemětřesení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vědci	vědec	k1gMnPc1
při	při	k7c6
přeletech	přelet	k1gInPc6
nad	nad	k7c7
sopkou	sopka	k1gFnSc7
v	v	k7c6
březnu	březen	k1gInSc6
2012	#num#	k4
zaznamenali	zaznamenat	k5eAaPmAgMnP
čerstvé	čerstvý	k2eAgInPc4d1
nánosy	nános	k1gInPc4
popelu	popel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhlá	náhlý	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
vrcholu	vrchol	k1gInSc2
však	však	k9
nevyvrcholila	vyvrcholit	k5eNaPmAgFnS
ve	v	k7c4
velké	velký	k2eAgFnPc4d1
erupce	erupce	k1gFnPc4
a	a	k8xC
činnost	činnost	k1gFnSc1
sopky	sopka	k1gFnSc2
poklesla	poklesnout	k5eAaPmAgFnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
úroveň	úroveň	k1gFnSc1
upozornění	upozornění	k1gNnSc2
byla	být	k5eAaImAgFnS
snížena	snížen	k2eAgFnSc1d1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
se	se	k3xPyFc4
seismická	seismický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
prudce	prudko	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
a	a	k8xC
úroveň	úroveň	k1gFnSc1
upozornění	upozornění	k1gNnSc2
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
popel	popel	k1gInSc1
spadl	spadnout	k5eAaPmAgInS
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
okolních	okolní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
několika	několik	k4yIc2
příštích	příští	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
se	se	k3xPyFc4
popel	popel	k1gInSc1
objevil	objevit	k5eAaPmAgInS
mnohokrát	mnohokrát	k6eAd1
znovu	znovu	k6eAd1
a	a	k8xC
opět	opět	k6eAd1
se	se	k3xPyFc4
zvýšilo	zvýšit	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
sopečné	sopečný	k2eAgFnPc1d1
události	událost	k1gFnPc1
a	a	k8xC
erupce	erupce	k1gFnPc1
se	se	k3xPyFc4
opakovaly	opakovat	k5eAaImAgFnP
nepravidelně	pravidelně	k6eNd1
od	od	k7c2
července	červenec	k1gInSc2
do	do	k7c2
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výskyt	výskyt	k1gInSc1
oxidu	oxid	k1gInSc2
siřičitého	siřičitý	k2eAgInSc2d1
se	se	k3xPyFc4
objevoval	objevovat	k5eAaImAgInS
až	až	k9
do	do	k7c2
ledna	leden	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiza	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc4
na	na	k7c4
Peakbagger	Peakbagger	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.volcano.si.edu	www.volcano.si.edu	k6eAd1
–	–	k?
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc4
na	na	k7c4
Global	globat	k5eAaImAgInS
Volcanism	Volcanism	k1gInSc1
Program	program	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Popis	popis	k1gInSc1
a	a	k8xC
fotografie	fotografie	k1gFnSc1
událostí	událost	k1gFnPc2
13	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1985	#num#	k4
na	na	k7c6
stránkách	stránka	k1gFnPc6
USGS	USGS	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4331771-6	4331771-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
87004613	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
247807415	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
87004613	#num#	k4
</s>
