<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vlnění	vlnění	k1gNnSc1	vlnění
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
rozhraní	rozhraní	k1gNnSc3	rozhraní
dvou	dva	k4xCgMnPc2	dva
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
má	mít	k5eAaImIp3nS	mít
vlnění	vlnění	k1gNnSc4	vlnění
různou	různý	k2eAgFnSc4d1	různá
fázovou	fázový	k2eAgFnSc4d1	fázová
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
vlnění	vlnění	k1gNnSc2	vlnění
tímto	tento	k3xDgNnSc7	tento
rozhraním	rozhraní	k1gNnSc7	rozhraní
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
šíření	šíření	k1gNnSc2	šíření
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
lom	lom	k1gInSc1	lom
vlnění	vlnění	k1gNnSc2	vlnění
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
refrakce	refrakce	k1gFnSc1	refrakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lom	lom	k1gInSc1	lom
vlnění	vlnění	k1gNnSc2	vlnění
je	být	k5eAaImIp3nS	být
obecná	obecný	k2eAgFnSc1d1	obecná
vlastnost	vlastnost	k1gFnSc1	vlastnost
vlnění	vlnění	k1gNnSc2	vlnění
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
Huygensova	Huygensův	k2eAgInSc2d1	Huygensův
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
lomu	lom	k1gInSc2	lom
popsán	popsat	k5eAaPmNgInS	popsat
Snellovým	Snellův	k2eAgInSc7d1	Snellův
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
optický	optický	k2eAgInSc4d1	optický
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc2	rozhraní
dvou	dva	k4xCgNnPc2	dva
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
světlo	světlo	k1gNnSc4	světlo
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
různých	různý	k2eAgFnPc2d1	různá
rychlostí	rychlost	k1gFnPc2	rychlost
šíření	šíření	k1gNnSc4	šíření
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
prostředích	prostředí	k1gNnPc6	prostředí
a	a	k8xC	a
kromě	kromě	k7c2	kromě
světla	světlo	k1gNnSc2	světlo
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
veškeré	veškerý	k3xTgNnSc4	veškerý
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Zpomalení	zpomalení	k1gNnSc4	zpomalení
světla	světlo	k1gNnSc2	světlo
oproti	oproti	k7c3	oproti
jeho	jeho	k3xOp3gFnSc3	jeho
rychlosti	rychlost	k1gFnSc3	rychlost
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
popisuje	popisovat	k5eAaImIp3nS	popisovat
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
prostředí	prostředí	k1gNnSc2	prostředí
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
geometrické	geometrický	k2eAgFnSc2d1	geometrická
optiky	optika	k1gFnSc2	optika
pomocí	pomocí	k7c2	pomocí
Snellova	Snellův	k2eAgInSc2d1	Snellův
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
světlo	světlo	k1gNnSc1	světlo
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
materiálu	materiál	k1gInSc2	materiál
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
frekvence	frekvence	k1gFnSc1	frekvence
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
závisí	záviset	k5eAaImIp3nS	záviset
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
látek	látka	k1gFnPc2	látka
i	i	k9	i
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
díky	díky	k7c3	díky
lomu	lom	k1gInSc3	lom
na	na	k7c6	na
rozhraních	rozhraní	k1gNnPc6	rozhraní
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc4	světlo
rozkládat	rozkládat	k5eAaImF	rozkládat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
barevné	barevný	k2eAgFnPc4d1	barevná
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
hranolu	hranol	k1gInSc2	hranol
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
duha	duha	k1gFnSc1	duha
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lomu	lom	k1gInSc2	lom
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
na	na	k7c6	na
vodních	vodní	k2eAgFnPc6d1	vodní
kapkách	kapka	k1gFnPc6	kapka
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Lom	lom	k1gInSc1	lom
na	na	k7c6	na
čočce	čočka	k1gFnSc6	čočka
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
mj.	mj.	kA	mj.
v	v	k7c6	v
brýlích	brýle	k1gFnPc6	brýle
<g/>
,	,	kIx,	,
lupách	lupa	k1gFnPc6	lupa
<g/>
,	,	kIx,	,
kontaktních	kontaktní	k2eAgFnPc6d1	kontaktní
čočkách	čočka	k1gFnPc6	čočka
<g/>
,	,	kIx,	,
mikroskopech	mikroskop	k1gInPc6	mikroskop
a	a	k8xC	a
refrakčních	refrakční	k2eAgInPc6d1	refrakční
teleskopech	teleskop	k1gInPc6	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
se	se	k3xPyFc4	se
lomí	lomit	k5eAaImIp3nS	lomit
např.	např.	kA	např.
vlnění	vlnění	k1gNnSc1	vlnění
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
kapaliny	kapalina	k1gFnSc2	kapalina
a	a	k8xC	a
akustické	akustický	k2eAgNnSc4d1	akustické
vlnění	vlnění	k1gNnSc4	vlnění
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
rozhraním	rozhraní	k1gNnSc7	rozhraní
dvou	dva	k4xCgMnPc6	dva
prostředí	prostředí	k1gNnSc2	prostředí
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
rychlostmi	rychlost	k1gFnPc7	rychlost
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
podstaty	podstata	k1gFnSc2	podstata
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
láme	lámat	k5eAaImIp3nS	lámat
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
prostředími	prostředí	k1gNnPc7	prostředí
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
však	však	k9	však
nabízí	nabízet	k5eAaImIp3nS	nabízet
až	až	k9	až
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
postup	postup	k1gInSc1	postup
dráhového	dráhový	k2eAgInSc2d1	dráhový
integrálu	integrál	k1gInSc2	integrál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
plně	plně	k6eAd1	plně
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
.	.	kIx.	.
</s>
<s>
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
základní	základní	k2eAgFnPc1d1	základní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
světla	světlo	k1gNnSc2	světlo
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
hezky	hezky	k6eAd1	hezky
a	a	k8xC	a
intuitivně	intuitivně	k6eAd1	intuitivně
vysvětleny	vysvětlit	k5eAaPmNgFnP	vysvětlit
v	v	k7c6	v
nahrávce	nahrávka	k1gFnSc6	nahrávka
"	"	kIx"	"
<g/>
QED	QED	kA	QED
<g/>
:	:	kIx,	:
Fits	Fits	k1gInSc1	Fits
of	of	k?	of
Reflection	Reflection	k1gInSc1	Reflection
and	and	k?	and
Transmission	Transmission	k1gInSc1	Transmission
-	-	kIx~	-
Quantum	Quantum	k1gNnSc1	Quantum
Behaviour	Behavioura	k1gFnPc2	Behavioura
-	-	kIx~	-
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Sir	sir	k1gMnSc1	sir
Douglas	Douglas	k1gMnSc1	Douglas
Robb	Robb	k1gMnSc1	Robb
Lectures	Lectures	k1gMnSc1	Lectures
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Auckland	Auckland	k1gInSc1	Auckland
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
šíří	šíř	k1gFnPc2	šíř
drahou	drahá	k1gFnSc4	drahá
s	s	k7c7	s
nejkratším	krátký	k2eAgInSc7d3	nejkratší
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přesně	přesně	k6eAd1	přesně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
výše	vysoce	k6eAd2	vysoce
popsaným	popsaný	k2eAgInPc3d1	popsaný
zákonům	zákon	k1gInPc3	zákon
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
použil	použít	k5eAaPmAgMnS	použít
následující	následující	k2eAgFnSc4d1	následující
zábavnou	zábavný	k2eAgFnSc4d1	zábavná
analogii	analogie	k1gFnSc4	analogie
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Představte	představit	k5eAaPmRp2nP	představit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
plavčík	plavčík	k1gMnSc1	plavčík
a	a	k8xC	a
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
topí	topit	k5eAaImIp3nS	topit
krásná	krásný	k2eAgFnSc1d1	krásná
dívka	dívka	k1gFnSc1	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
po	po	k7c6	po
pláži	pláž	k1gFnSc6	pláž
umíte	umět	k5eAaImIp2nP	umět
běžet	běžet	k5eAaImF	běžet
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
plavat	plavat	k5eAaImF	plavat
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
jedna	jeden	k4xCgFnSc1	jeden
optimální	optimální	k2eAgFnSc1d1	optimální
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
co	co	k8xS	co
nejrychleji	rychle	k6eAd3	rychle
dostat	dostat	k5eAaPmF	dostat
-	-	kIx~	-
kde	kde	k6eAd1	kde
přesně	přesně	k6eAd1	přesně
skočit	skočit	k5eAaPmF	skočit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
takový	takový	k3xDgInSc4	takový
výpočet	výpočet	k1gInSc4	výpočet
jako	jako	k8xS	jako
plavčík	plavčík	k1gMnSc1	plavčík
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
okolnostem	okolnost	k1gFnPc3	okolnost
asi	asi	k9	asi
dělat	dělat	k5eAaImF	dělat
nebudete	být	k5eNaImBp2nP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
světlo	světlo	k1gNnSc1	světlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dráhový	dráhový	k2eAgInSc1d1	dráhový
integrál	integrál	k1gInSc1	integrál
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatní	ostatní	k2eAgFnPc1d1	ostatní
možné	možný	k2eAgFnPc1d1	možná
dráhy	dráha	k1gFnPc1	dráha
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
interferenci	interference	k1gFnSc3	interference
vyruší	vyrušit	k5eAaPmIp3nS	vyrušit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nejvíce	hodně	k6eAd3	hodně
přispěje	přispět	k5eAaPmIp3nS	přispět
k	k	k7c3	k
výsledku	výsledek	k1gInSc3	výsledek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
časem	čas	k1gInSc7	čas
šíření	šíření	k1gNnSc2	šíření
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Planparalelní	planparalelní	k2eAgFnSc1d1	planparalelní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
skleněná	skleněný	k2eAgFnSc1d1	skleněná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
přesně	přesně	k6eAd1	přesně
rovinné	rovinný	k2eAgFnPc1d1	rovinná
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
<g/>
.	.	kIx.	.
</s>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
paprsek	paprsek	k1gInSc1	paprsek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
planparalelní	planparalelní	k2eAgFnSc4d1	planparalelní
desku	deska	k1gFnSc4	deska
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
planparalelní	planparalelní	k2eAgFnSc2d1	planparalelní
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
z	z	k7c2	z
planparalelní	planparalelní	k2eAgFnSc2d1	planparalelní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Paprsek	paprsek	k1gInSc1	paprsek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
rovnoběžný	rovnoběžný	k2eAgMnSc1d1	rovnoběžný
s	s	k7c7	s
paprskem	paprsek	k1gInSc7	paprsek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
do	do	k7c2	do
desky	deska	k1gFnSc2	deska
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obrázku	obrázek	k1gInSc2	obrázek
platí	platit	k5eAaImIp3nP	platit
vztahy	vztah	k1gInPc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
současně	současně	k6eAd1	současně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Pomocí	pomocí	k7c2	pomocí
Snellova	Snellův	k2eAgInSc2d1	Snellův
zákona	zákon	k1gInSc2	zákon
lze	lze	k6eAd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
=	=	kIx~	=
l	l	kA	l
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
=	=	kIx~	=
<g/>
l	l	kA	l
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}}	}}}}	k?	}}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgInSc4d1	relativní
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
lomu	lom	k1gInSc2	lom
na	na	k7c6	na
planparalelní	planparalelní	k2eAgFnSc6d1	planparalelní
desce	deska	k1gFnSc6	deska
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
např.	např.	kA	např.
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
soustavy	soustava	k1gFnSc2	soustava
rovnoběžných	rovnoběžný	k2eAgFnPc2d1	rovnoběžná
vrstev	vrstva	k1gFnPc2	vrstva
o	o	k7c6	o
různém	různý	k2eAgInSc6d1	různý
indexu	index	k1gInSc6	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
lámání	lámání	k1gNnSc3	lámání
paprsku	paprsek	k1gInSc2	paprsek
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
planparalelních	planparalelní	k2eAgFnPc6d1	planparalelní
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
plynule	plynule	k6eAd1	plynule
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
lomená	lomený	k2eAgFnSc1d1	lomená
čára	čára	k1gFnSc1	čára
paprsku	paprsek	k1gInSc2	paprsek
v	v	k7c4	v
plynulou	plynulý	k2eAgFnSc4d1	plynulá
křivku	křivka	k1gFnSc4	křivka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takovému	takový	k3xDgInSc3	takový
jevu	jev	k1gInSc3	jev
dochází	docházet	k5eAaImIp3nS	docházet
např.	např.	kA	např.
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
světla	světlo	k1gNnSc2	světlo
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
atmosférické	atmosférický	k2eAgFnSc6d1	atmosférická
refrakci	refrakce	k1gFnSc6	refrakce
<g/>
.	.	kIx.	.
</s>
<s>
Snellův	Snellův	k2eAgInSc4d1	Snellův
zákon	zákon	k1gInSc4	zákon
Index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
Ohyb	ohyb	k1gInSc1	ohyb
světla	světlo	k1gNnSc2	světlo
Optický	optický	k2eAgInSc1d1	optický
hranol	hranol	k1gInSc1	hranol
Spektroskop	spektroskop	k1gInSc4	spektroskop
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lom	lom	k1gInSc1	lom
vlnění	vlnění	k1gNnSc2	vlnění
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Reflection	Reflection	k1gInSc4	Reflection
and	and	k?	and
Refraction	Refraction	k1gInSc1	Refraction
(	(	kIx(	(
<g/>
Java	Java	k1gFnSc1	Java
<g/>
)	)	kIx)	)
http://qed.ben.cz/lom-paprsku-svetla	[url]	k5eAaPmAgFnS	http://qed.ben.cz/lom-paprsku-svetla
-	-	kIx~	-
animace	animace	k1gFnSc1	animace
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
</s>
