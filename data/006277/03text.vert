<s>
Invaze	invaze	k1gFnSc1	invaze
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
byla	být	k5eAaImAgFnS	být
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
operace	operace	k1gFnSc1	operace
kubánských	kubánský	k2eAgMnPc2d1	kubánský
exulantů	exulant	k1gMnPc2	exulant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
CIA	CIA	kA	CIA
<g/>
)	)	kIx)	)
napadli	napadnout	k5eAaPmAgMnP	napadnout
jižní	jižní	k2eAgFnSc4d1	jižní
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
financovala	financovat	k5eAaBmAgFnS	financovat
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
americká	americký	k2eAgFnSc1d1	americká
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
svržení	svržení	k1gNnSc1	svržení
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
stal	stát	k5eAaPmAgInS	stát
35	[number]	k4	35
<g/>
.	.	kIx.	.
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
podcenění	podcenění	k1gNnSc3	podcenění
příprav	příprava	k1gFnPc2	příprava
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
naprostou	naprostý	k2eAgFnSc7d1	naprostá
porážkou	porážka	k1gFnSc7	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Kubánské	kubánský	k2eAgFnPc1d1	kubánská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
vycvičené	vycvičený	k2eAgInPc1d1	vycvičený
a	a	k8xC	a
vybavené	vybavený	k2eAgInPc1d1	vybavený
státy	stát	k1gInPc1	stát
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
rozdrtily	rozdrtit	k5eAaPmAgInP	rozdrtit
invazní	invazní	k2eAgMnPc4d1	invazní
bojovníky	bojovník	k1gMnPc4	bojovník
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
Castro	Castro	k1gNnSc1	Castro
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
přimkl	přimknout	k5eAaPmAgInS	přimknout
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgNnSc2	tento
vylodění	vylodění	k1gNnSc2	vylodění
byla	být	k5eAaImAgFnS	být
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
USA	USA	kA	USA
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
španělsko-americké	španělskomerický	k2eAgFnSc2d1	španělsko-americká
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ochabl	ochabnout	k5eAaPmAgMnS	ochabnout
až	až	k6eAd1	až
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
prezidenta	prezident	k1gMnSc2	prezident
Batisty	batist	k1gInPc5	batist
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
silně	silně	k6eAd1	silně
levicově	levicově	k6eAd1	levicově
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
Castro	Castro	k1gNnSc4	Castro
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyhrotil	vyhrotit	k5eAaPmAgInS	vyhrotit
vztahy	vztah	k1gInPc4	vztah
znárodněním	znárodnění	k1gNnSc7	znárodnění
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
těžebních	těžební	k2eAgInPc2d1	těžební
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
ekonomika	ekonomika	k1gFnSc1	ekonomika
sice	sice	k8xC	sice
následně	následně	k6eAd1	následně
vlivem	vliv	k1gInSc7	vliv
obchodní	obchodní	k2eAgFnSc2d1	obchodní
války	válka	k1gFnSc2	válka
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc2	situace
však	však	k9	však
využil	využít	k5eAaPmAgInS	využít
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
odkoupení	odkoupení	k1gNnSc4	odkoupení
části	část	k1gFnSc2	část
budoucí	budoucí	k2eAgFnSc2d1	budoucí
sklizně	sklizeň	k1gFnSc2	sklizeň
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
výhodné	výhodný	k2eAgFnPc4d1	výhodná
půjčky	půjčka	k1gFnPc4	půjčka
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
znárodňování	znárodňování	k1gNnSc6	znárodňování
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
podniků	podnik	k1gInPc2	podnik
(	(	kIx(	(
<g/>
především	především	k9	především
cukrovarů	cukrovar	k1gInPc2	cukrovar
<g/>
)	)	kIx)	)
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
podílem	podíl	k1gInSc7	podíl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velení	velení	k1gNnSc1	velení
CIA	CIA	kA	CIA
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
odstranit	odstranit	k5eAaPmF	odstranit
kubánského	kubánský	k2eAgMnSc4d1	kubánský
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
1960	[number]	k4	1960
probíhal	probíhat	k5eAaImAgInS	probíhat
výcvik	výcvik	k1gInSc1	výcvik
kubánských	kubánský	k2eAgMnPc2d1	kubánský
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
podpořit	podpořit	k5eAaPmF	podpořit
všelidové	všelidový	k2eAgNnSc4d1	všelidové
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
byli	být	k5eAaImAgMnP	být
vytipováni	vytipován	k2eAgMnPc1d1	vytipován
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
do	do	k7c2	do
Brigády	brigáda	k1gFnSc2	brigáda
2506	[number]	k4	2506
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
místo	místo	k1gNnSc1	místo
vylodění	vylodění	k1gNnSc2	vylodění
bylo	být	k5eAaImAgNnS	být
vybráno	vybrán	k2eAgNnSc1d1	vybráno
město	město	k1gNnSc1	město
Trinidad	Trinidad	k1gInSc1	Trinidad
<g/>
.	.	kIx.	.
</s>
<s>
Bojovým	bojový	k2eAgInSc7d1	bojový
křtem	křest	k1gInSc7	křest
prošly	projít	k5eAaPmAgFnP	projít
invazní	invazní	k2eAgFnPc1d1	invazní
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1960	[number]	k4	1960
při	při	k7c6	při
potlačování	potlačování	k1gNnSc6	potlačování
guatemalské	guatemalský	k2eAgFnSc2d1	guatemalská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1961	[number]	k4	1961
přešel	přejít	k5eAaPmAgInS	přejít
projekt	projekt	k1gInSc1	projekt
pod	pod	k7c4	pod
administrativu	administrativa	k1gFnSc4	administrativa
nově	nově	k6eAd1	nově
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
operaci	operace	k1gFnSc3	operace
stavěl	stavět	k5eAaImAgInS	stavět
spíše	spíše	k9	spíše
skepticky	skepticky	k6eAd1	skepticky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1961	[number]	k4	1961
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
cíl	cíl	k1gInSc4	cíl
invaze	invaze	k1gFnSc1	invaze
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
Zátoka	zátoka	k1gFnSc1	zátoka
sviní	sviní	k2eAgFnSc1d1	sviní
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
místem	místo	k1gNnSc7	místo
vylodění	vylodění	k1gNnSc2	vylodění
byla	být	k5eAaImAgFnS	být
pláž	pláž	k1gFnSc1	pláž
''	''	k?	''
<g/>
Playa	Playa	k1gMnSc1	Playa
Giron	Giron	k1gMnSc1	Giron
<g/>
''	''	k?	''
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
těžko	těžko	k6eAd1	těžko
překonatelný	překonatelný	k2eAgInSc4d1	překonatelný
bažinatý	bažinatý	k2eAgInSc4d1	bažinatý
terén	terén	k1gInSc4	terén
<g/>
.	.	kIx.	.
</s>
<s>
CIA	CIA	kA	CIA
však	však	k9	však
doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odlehlost	odlehlost	k1gFnSc1	odlehlost
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
ztíží	ztížet	k5eAaPmIp3nS	ztížet
přísun	přísun	k1gInSc1	přísun
posil	posít	k5eAaPmAgMnS	posít
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Invazní	invazní	k2eAgInPc1d1	invazní
síly	síl	k1gInPc1	síl
ovšem	ovšem	k9	ovšem
ztratily	ztratit	k5eAaPmAgInP	ztratit
možnost	možnost	k1gFnSc4	možnost
podpory	podpora	k1gFnSc2	podpora
od	od	k7c2	od
místních	místní	k2eAgFnPc2d1	místní
guerrilových	guerrilův	k2eAgFnPc2d1	guerrilův
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
základny	základna	k1gFnPc1	základna
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
nakonec	nakonec	k6eAd1	nakonec
určil	určit	k5eAaPmAgInS	určit
termín	termín	k1gInSc1	termín
akce	akce	k1gFnSc2	akce
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
akci	akce	k1gFnSc4	akce
předcházela	předcházet	k5eAaImAgFnS	předcházet
propagandistická	propagandistický	k2eAgFnSc1d1	propagandistická
příprava	příprava	k1gFnSc1	příprava
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
proti	proti	k7c3	proti
Castrovi	Castr	k1gMnSc3	Castr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předevečer	předevečer	k1gInSc4	předevečer
invaze	invaze	k1gFnSc2	invaze
podnikli	podniknout	k5eAaPmAgMnP	podniknout
Američané	Američan	k1gMnPc1	Američan
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
klíčová	klíčový	k2eAgNnPc4d1	klíčové
letiště	letiště	k1gNnPc4	letiště
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vylodili	vylodit	k5eAaPmAgMnP	vylodit
příslušníci	příslušník	k1gMnPc1	příslušník
speciálního	speciální	k2eAgInSc2d1	speciální
obojživelného	obojživelný	k2eAgInSc2d1	obojživelný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
překvapení	překvapení	k1gNnSc3	překvapení
však	však	k8xC	však
zde	zde	k6eAd1	zde
objevili	objevit	k5eAaPmAgMnP	objevit
ozbrojenou	ozbrojený	k2eAgFnSc4d1	ozbrojená
hlídku	hlídka	k1gFnSc4	hlídka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zmařila	zmařit	k5eAaPmAgFnS	zmařit
veškeré	veškerý	k3xTgFnPc4	veškerý
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
moment	moment	k1gInSc4	moment
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
i	i	k9	i
u	u	k7c2	u
druhé	druhý	k4xOgFnSc2	druhý
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
několik	několik	k4yIc1	několik
člunů	člun	k1gInPc2	člun
narazilo	narazit	k5eAaPmAgNnS	narazit
na	na	k7c4	na
skalnaté	skalnatý	k2eAgNnSc4d1	skalnaté
dno	dno	k1gNnSc4	dno
a	a	k8xC	a
potopilo	potopit	k5eAaPmAgNnS	potopit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
kompletní	kompletní	k2eAgFnSc7d1	kompletní
výbavou	výbava	k1gFnSc7	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc3	třetí
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
obsadit	obsadit	k5eAaPmF	obsadit
nedalekou	daleký	k2eNgFnSc4d1	nedaleká
vesnici	vesnice	k1gFnSc4	vesnice
a	a	k8xC	a
místní	místní	k2eAgNnSc4d1	místní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rána	ráno	k1gNnSc2	ráno
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
pláže	pláž	k1gFnPc4	pláž
dobyty	dobyt	k2eAgFnPc4d1	dobyta
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
vyloďování	vyloďování	k1gNnSc4	vyloďování
těžké	těžký	k2eAgFnSc2d1	těžká
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
leteckému	letecký	k2eAgNnSc3d1	letecké
krytí	krytí	k1gNnSc3	krytí
však	však	k8xC	však
Brigáda	brigáda	k1gFnSc1	brigáda
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
loď	loď	k1gFnSc4	loď
vezoucí	vezoucí	k2eAgFnSc4d1	vezoucí
zdravotnické	zdravotnický	k2eAgNnSc4d1	zdravotnické
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
poškozena	poškozen	k2eAgFnSc1d1	poškozena
byla	být	k5eAaImAgFnS	být
komunikační	komunikační	k2eAgNnSc4d1	komunikační
zařízení	zařízení	k1gNnSc4	zařízení
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
výsadek	výsadek	k1gInSc1	výsadek
parašutistů	parašutista	k1gMnPc2	parašutista
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zajištění	zajištění	k1gNnSc1	zajištění
silnic	silnice	k1gFnPc2	silnice
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vykoupen	vykoupit	k5eAaPmNgInS	vykoupit
velkými	velký	k2eAgFnPc7d1	velká
ztrátami	ztráta	k1gFnPc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc4	ten
Američané	Američan	k1gMnPc1	Američan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
dvě	dva	k4xCgFnPc4	dva
transportní	transportní	k2eAgFnPc4d1	transportní
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
palivem	palivo	k1gNnSc7	palivo
a	a	k8xC	a
municí	munice	k1gFnSc7	munice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
místu	místo	k1gNnSc3	místo
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stahovat	stahovat	k5eAaImF	stahovat
kubánské	kubánský	k2eAgFnPc4d1	kubánská
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
i	i	k9	i
se	s	k7c7	s
samotným	samotný	k2eAgMnSc7d1	samotný
Fidelem	Fidel	k1gMnSc7	Fidel
Castrem	Castr	k1gMnSc7	Castr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
chopil	chopit	k5eAaPmAgMnS	chopit
velení	velení	k1gNnSc4	velení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
pokusily	pokusit	k5eAaPmAgFnP	pokusit
zaútočit	zaútočit	k5eAaPmF	zaútočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
odraženy	odrazit	k5eAaPmNgFnP	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
invazní	invazní	k2eAgFnPc1d1	invazní
síly	síla	k1gFnPc1	síla
musely	muset	k5eAaImAgFnP	muset
dopoledne	dopoledne	k1gNnSc4	dopoledne
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
munice	munice	k1gFnSc2	munice
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vládní	vládní	k2eAgFnSc2d1	vládní
jednotky	jednotka	k1gFnSc2	jednotka
přebraly	přebrat	k5eAaPmAgFnP	přebrat
iniciativu	iniciativa	k1gFnSc4	iniciativa
a	a	k8xC	a
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
letectva	letectvo	k1gNnSc2	letectvo
se	se	k3xPyFc4	se
pokusily	pokusit	k5eAaPmAgFnP	pokusit
útočníky	útočník	k1gMnPc7	útočník
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
invazních	invazní	k2eAgFnPc2d1	invazní
sil	síla	k1gFnPc2	síla
byl	být	k5eAaImAgInS	být
zpečetěn	zpečetit	k5eAaPmNgInS	zpečetit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Brigáda	brigáda	k1gFnSc1	brigáda
ztratila	ztratit	k5eAaPmAgFnS	ztratit
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgNnPc2	svůj
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
zásobovací	zásobovací	k2eAgInPc1d1	zásobovací
čluny	člun	k1gInPc1	člun
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
krytí	krytí	k1gNnSc2	krytí
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
přiblížit	přiblížit	k5eAaPmF	přiblížit
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
obrana	obrana	k1gFnSc1	obrana
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
a	a	k8xC	a
během	během	k7c2	během
odpoledne	odpoledne	k1gNnSc2	odpoledne
padla	padnout	k5eAaPmAgFnS	padnout
poslední	poslední	k2eAgFnSc1d1	poslední
bašta	bašta	k1gFnSc1	bašta
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
Girón	Girón	k1gInSc1	Girón
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
bažin	bažina	k1gFnPc2	bažina
a	a	k8xC	a
některým	některý	k3yIgMnPc3	některý
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
zajata	zajat	k2eAgFnSc1d1	zajata
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
118	[number]	k4	118
vojáků	voják	k1gMnPc2	voják
invazních	invazní	k2eAgFnPc2d1	invazní
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
přes	přes	k7c4	přes
1	[number]	k4	1
200	[number]	k4	200
zajato	zajmout	k5eAaPmNgNnS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Kubánské	kubánský	k2eAgFnPc1d1	kubánská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
ztratily	ztratit	k5eAaPmAgFnP	ztratit
176	[number]	k4	176
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
000	[number]	k4	000
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
zajatců	zajatec	k1gMnPc2	zajatec
Castro	Castro	k1gNnSc4	Castro
následně	následně	k6eAd1	následně
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
za	za	k7c4	za
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
znamenala	znamenat	k5eAaImAgFnS	znamenat
velkou	velký	k2eAgFnSc4d1	velká
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
ostudu	ostuda	k1gFnSc4	ostuda
pro	pro	k7c4	pro
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
většímu	veliký	k2eAgNnSc3d2	veliký
sblížení	sblížení	k1gNnSc3	sblížení
Kuby	Kuba	k1gFnSc2	Kuba
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrocená	vyhrocený	k2eAgFnSc1d1	vyhrocená
situace	situace	k1gFnSc1	situace
později	pozdě	k6eAd2	pozdě
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
Karibskou	karibský	k2eAgFnSc4d1	karibská
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
