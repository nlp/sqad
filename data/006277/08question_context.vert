<s>
Invaze	invaze	k1gFnSc1	invaze
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
byla	být	k5eAaImAgFnS	být
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
operace	operace	k1gFnSc1	operace
kubánských	kubánský	k2eAgMnPc2d1	kubánský
exulantů	exulant	k1gMnPc2	exulant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
CIA	CIA	kA	CIA
<g/>
)	)	kIx)	)
napadli	napadnout	k5eAaPmAgMnP	napadnout
jižní	jižní	k2eAgFnSc4d1	jižní
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
financovala	financovat	k5eAaBmAgFnS	financovat
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
americká	americký	k2eAgFnSc1d1	americká
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
svržení	svržení	k1gNnSc1	svržení
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
.	.	kIx.	.
</s>

