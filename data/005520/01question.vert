<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
sladina	sladina	k1gFnSc1	sladina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
varu	var	k1gInSc2	var
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
60-90	[number]	k4	60-90
minut	minuta	k1gFnPc2	minuta
<g/>
?	?	kIx.	?
</s>
