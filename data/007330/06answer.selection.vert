<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Číny	Čína	k1gFnSc2	Čína
je	být	k5eAaImIp3nS	být
konzumováno	konzumován	k2eAgNnSc1d1	konzumováno
<g/>
/	/	kIx~	/
<g/>
konzervováno	konzervován	k2eAgNnSc1d1	konzervováno
kočičí	kočičí	k2eAgNnSc1d1	kočičí
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
rovněž	rovněž	k9	rovněž
kožešinovým	kožešinový	k2eAgNnSc7d1	kožešinové
zvířetem	zvíře	k1gNnSc7	zvíře
a	a	k8xC	a
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
kočičí	kočičí	k2eAgFnSc2d1	kočičí
kožešiny	kožešina	k1gFnSc2	kožešina
se	se	k3xPyFc4	se
dostávaly	dostávat	k5eAaImAgInP	dostávat
i	i	k9	i
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
