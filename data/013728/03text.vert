<s>
Alexander	Alexandra	k1gFnPc2
Horák	Horák	k1gMnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
h.	h.	k?
c.	c.	k?
</s>
<s>
poslanec	poslanec	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1948	#num#	k4
–	–	k?
1976	#num#	k4
</s>
<s>
poslanec	poslanec	k1gMnSc1
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
(	(	kIx(
<g/>
SN	SN	kA
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1969	#num#	k4
–	–	k?
1971	#num#	k4
</s>
<s>
pověřenec	pověřenec	k1gMnSc1
pošt	pošta	k1gFnPc2
(	(	kIx(
<g/>
spojů	spoj	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1948	#num#	k4
–	–	k?
1955	#num#	k4
</s>
<s>
pověřenec	pověřenec	k1gMnSc1
místního	místní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1956	#num#	k4
–	–	k?
1958	#num#	k4
</s>
<s>
pověřenec	pověřenec	k1gMnSc1
-	-	kIx~
předs	předs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovenského	slovenský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
výstavbu	výstavba	k1gFnSc4
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1958	#num#	k4
–	–	k?
1960	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
bezpartijní	bezpartijní	k1gMnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1911	#num#	k4
HaličRakousko-Uhersko	HaličRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
83	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
BratislavaSlovensko	BratislavaSlovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
katolictví	katolictví	k1gNnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Alexander	Alexander	k1gMnSc1
Horák	Horák	k1gMnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1911	#num#	k4
Halič	Halič	k1gFnSc1
okr	okr	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lučenec	Lučenec	k1gInSc1
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
Bratislava	Bratislava	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
a	a	k8xC
československý	československý	k2eAgMnSc1d1
římskokatolický	římskokatolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
spolupracující	spolupracující	k2eAgFnSc2d1
s	s	k7c7
komunistickým	komunistický	k2eAgInSc7d1
režimem	režim	k1gInSc7
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
bezpartijní	bezpartijní	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1948	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
,	,	kIx,
člen	člen	k1gInSc1
Sboru	sbor	k1gInSc2
pověřenců	pověřenec	k1gMnPc2
a	a	k8xC
poslanec	poslanec	k1gMnSc1
Sněmovny	sněmovna	k1gFnSc2
národů	národ	k1gInPc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
normalizace	normalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1929	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
studoval	studovat	k5eAaImAgMnS
teologii	teologie	k1gFnSc4
v	v	k7c6
Rožňavě	Rožňava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
správcem	správce	k1gMnSc7
Spolku	spolek	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
tam	tam	k6eAd1
časopis	časopis	k1gInSc1
Sloboda	sloboda	k1gFnSc1
a	a	k8xC
redigoval	redigovat	k5eAaImAgInS
časopis	časopis	k1gInSc1
Duchovný	duchovný	k2eAgInSc4d1
pastier	pastier	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
stal	stát	k5eAaPmAgMnS
kulturním	kulturní	k2eAgMnSc7d1
referentem	referent	k1gMnSc7
Spolku	spolek	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
poúnorové	poúnorový	k2eAgFnSc6d1
době	doba	k1gFnSc6
působil	působit	k5eAaImAgMnS
jako	jako	k9
prokomunistický	prokomunistický	k2eAgMnSc1d1
římskokatolický	římskokatolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
konce	konec	k1gInSc2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
angažoval	angažovat	k5eAaBmAgInS
v	v	k7c6
prorežimních	prorežimní	k2eAgFnPc6d1
organizacích	organizace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
předsedou	předseda	k1gMnSc7
slovenského	slovenský	k2eAgInSc2d1
Výboru	výbor	k1gInSc2
obránců	obránce	k1gMnPc2
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
funkci	funkce	k1gFnSc6
byl	být	k5eAaImAgInS
potvrzen	potvrzen	k2eAgInSc1d1
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
rovněž	rovněž	k9
aktivní	aktivní	k2eAgMnSc1d1
v	v	k7c6
organizaci	organizace	k1gFnSc6
Mírové	mírový	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
katolického	katolický	k2eAgNnSc2d1
duchovenstva	duchovenstvo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
navazovala	navazovat	k5eAaImAgFnS
na	na	k7c4
obdobně	obdobně	k6eAd1
prokomunistickou	prokomunistický	k2eAgFnSc4d1
Katolickou	katolický	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
mu	on	k3xPp3gMnSc3
Katolická	katolický	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
udělila	udělit	k5eAaPmAgFnS
doktorát	doktorát	k1gInSc4
teologie	teologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1959-1990	1959-1990	k4
přednášel	přednášet	k5eAaImAgMnS
katechetiku	katechetika	k1gFnSc4
a	a	k8xC
pedagogiku	pedagogika	k1gFnSc4
na	na	k7c6
Římskokatolické	římskokatolický	k2eAgFnSc6d1
cyrilometodějské	cyrilometodějský	k2eAgFnSc6d1
bohoslovecké	bohoslovecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Komenského	Komenský	k1gMnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgInS
spojit	spojit	k5eAaPmF
tradice	tradice	k1gFnPc4
křesťanstva	křesťanstvo	k1gNnSc2
s	s	k7c7
vlivy	vliv	k1gInPc7
marxismu	marxismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1948	#num#	k4
zasedl	zasednout	k5eAaPmAgMnS
ve	v	k7c6
Slovenské	slovenský	k2eAgFnSc6d1
národní	národní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
členem	člen	k1gInSc7
Sboru	sbor	k1gInSc2
pověřenců	pověřenec	k1gMnPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbor	sbor	k1gInSc1
pověřenců	pověřenec	k1gMnPc2
–	–	k?
pověřenec	pověřenec	k1gMnSc1
pošt	pošta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
svým	svůj	k3xOyFgFnPc3
politickým	politický	k2eAgFnPc3d1
aktivitám	aktivita	k1gFnPc3
čelil	čelit	k5eAaImAgMnS
koncem	konec	k1gInSc7
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
suspendování	suspendování	k1gNnSc2
z	z	k7c2
církevních	církevní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandát	mandát	k1gInSc1
v	v	k7c6
SNR	SNR	kA
obhájil	obhájit	k5eAaPmAgInS
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nich	on	k3xPp3gInPc6
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
v	v	k7c4
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sboru	sbor	k1gInSc2
pověřenců	pověřenec	k1gMnPc2
do	do	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
coby	coby	k?
pověřenec	pověřenec	k1gMnSc1
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1956	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
v	v	k7c4
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sboru	sbor	k1gInSc2
pověřenců	pověřenec	k1gMnPc2
jako	jako	k9
pověřenec	pověřenec	k1gMnSc1
místního	místní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1958	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
coby	coby	k?
člen	člen	k1gInSc1
Sboru	sbor	k1gInSc2
pověřenců	pověřenec	k1gMnPc2
s	s	k7c7
funkcí	funkce	k1gFnSc7
předsedy	předseda	k1gMnSc2
Slovenského	slovenský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
výstavbu	výstavba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opětovně	opětovně	k6eAd1
se	se	k3xPyFc4
poslancem	poslanec	k1gMnSc7
SNR	SNR	kA
stal	stát	k5eAaPmAgInS
i	i	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1960	#num#	k4
a	a	k8xC
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1964	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Sboru	sbor	k1gInSc6
pověřenců	pověřenec	k1gMnPc2
zasedal	zasedat	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1948	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
(	(	kIx(
<g/>
pověřenec	pověřenec	k1gMnSc1
pošt	pošta	k1gFnPc2
<g/>
,	,	kIx,
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
místního	místní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
a	a	k8xC
výstavby	výstavba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Slovenské	slovenský	k2eAgFnSc6d1
národní	národní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
1948	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
federalizaci	federalizace	k1gFnSc6
Československa	Československo	k1gNnSc2
usedl	usednout	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
1969	#num#	k4
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
národů	národ	k1gInPc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nominovala	nominovat	k5eAaBmAgFnS
ho	on	k3xPp3gNnSc2
Slovenska	Slovensko	k1gNnSc2
národní	národní	k2eAgMnSc1d1
rada	rada	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
federálním	federální	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
setrval	setrvat	k5eAaPmAgInS
do	do	k7c2
konce	konec	k1gInSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Alexander	Alexandra	k1gFnPc2
Horák	Horák	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Teologické	teologický	k2eAgFnSc2d1
vedomie	vedomie	k1gFnSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
pod	pod	k7c4
vplyvom	vplyvom	k1gInSc4
marxizmu	marxizmus	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
teoforum	teoforum	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Horák	Horák	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
litdok	litdok	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Československé	československý	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
501	#num#	k4
<g/>
,	,	kIx,
521	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cirkev	Cirkev	k1gFnSc1
<g/>
,	,	kIx,
Mierové	Mierové	k2eAgFnSc1d1
hnutie	hnutie	k1gFnSc1
katolíckeho	katolíckeze	k6eAd1
duchovenstva	duchovenstvo	k1gNnSc2
a	a	k8xC
Dielo	Dielo	k1gNnSc4
koncilovej	koncilovat	k5eAaPmRp2nS,k5eAaImRp2nS
obnovy	obnova	k1gFnSc2
v	v	k7c6
roku	rok	k1gInSc6
1968	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
impulzrevue	impulzrevue	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
12	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Alexander	Alexandra	k1gFnPc2
Horák	Horák	k1gMnSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20050502019	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5709	#num#	k4
2939	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84320253	#num#	k4
</s>
