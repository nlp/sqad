<s desamb="1">
Lučenec	Lučenec	k1gInSc1
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
Bratislava	Bratislava	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
a	a	k8xC
československý	československý	k2eAgMnSc1d1
římskokatolický	římskokatolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
spolupracující	spolupracující	k2eAgFnSc2d1
s	s	k7c7
komunistickým	komunistický	k2eAgInSc7d1
režimem	režim	k1gInSc7
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
bezpartijní	bezpartijní	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1948	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
,	,	kIx,
člen	člen	k1gInSc1
Sboru	sbor	k1gInSc2
pověřenců	pověřenec	k1gMnPc2
a	a	k8xC
poslanec	poslanec	k1gMnSc1
Sněmovny	sněmovna	k1gFnSc2
národů	národ	k1gInPc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
normalizace	normalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>