<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
philosophiae	philosophiae	k1gInSc1	philosophiae
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
PhDr.	PhDr.	kA	PhDr.
psané	psaný	k2eAgInPc4d1	psaný
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
udělován	udělovat	k5eAaImNgInS	udělovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitních	humanitní	k2eAgInPc2d1	humanitní
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgInPc2d1	společenský
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
od	od	k7c2	od
zrušení	zrušení	k1gNnSc2	zrušení
udělování	udělování	k1gNnSc2	udělování
titulu	titul	k1gInSc2	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
PaedDr	PaedDr	kA	PaedDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
i	i	k8xC	i
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
příslušných	příslušný	k2eAgFnPc6d1	příslušná
fakultách	fakulta	k1gFnPc6	fakulta
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
obdržení	obdržení	k1gNnSc3	obdržení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
získaný	získaný	k2eAgInSc4d1	získaný
titul	titul	k1gInSc4	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
složení	složení	k1gNnPc4	složení
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
zkoušky	zkouška	k1gFnPc4	zkouška
z	z	k7c2	z
příslušného	příslušný	k2eAgInSc2d1	příslušný
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
fakultativní	fakultativní	k2eAgFnSc4d1	fakultativní
zkoušku	zkouška	k1gFnSc4	zkouška
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
poplatky	poplatek	k1gInPc7	poplatek
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
–	–	k?	–
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typ	typ	k1gInSc4	typ
studia	studio	k1gNnSc2	studio
třetího	třetí	k4xOgMnSc2	třetí
stupně	stupeň	k1gInSc2	stupeň
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
doktor	doktor	k1gMnSc1	doktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
PhDr.	PhDr.	kA	PhDr.
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
titul	titul	k1gInSc1	titul
specifický	specifický	k2eAgInSc1d1	specifický
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
a	a	k8xC	a
slovenské	slovenský	k2eAgNnSc4d1	slovenské
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
za	za	k7c2	za
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
i	i	k9	i
v	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
školství	školství	k1gNnSc6	školství
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
tzv.	tzv.	kA	tzv.
malými	malý	k2eAgInPc7d1	malý
doktoráty	doktorát	k1gInPc7	doktorát
(	(	kIx(	(
<g/>
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hovorově	hovorově	k6eAd1	hovorově
se	se	k3xPyFc4	se
tituly	titul	k1gInPc1	titul
získané	získaný	k2eAgInPc1d1	získaný
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
<g/>
,	,	kIx,	,
RNDr.	RNDr.	kA	RNDr.
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
obecně	obecně	k6eAd1	obecně
označují	označovat	k5eAaImIp3nP	označovat
právě	právě	k6eAd1	právě
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
malé	malý	k2eAgInPc1d1	malý
doktoráty	doktorát	k1gInPc1	doktorát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rovněž	rovněž	k9	rovněž
také	také	k9	také
překlad	překlad	k1gInSc4	překlad
původního	původní	k2eAgInSc2d1	původní
významu	význam	k1gInSc2	význam
zkratky	zkratka	k1gFnSc2	zkratka
jiného	jiný	k2eAgInSc2d1	jiný
titulu	titul	k1gInSc2	titul
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
lat.	lat.	k?	lat.
philosophiæ	philosophiæ	k?	philosophiæ
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
CSc.	CSc.	kA	CSc.
či	či	k8xC	či
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
vědecký	vědecký	k2eAgInSc1d1	vědecký
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
dosahován	dosahovat	k5eAaImNgInS	dosahovat
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
dalším	další	k2eAgNnSc7d1	další
tříletým	tříletý	k2eAgNnSc7d1	tříleté
či	či	k8xC	či
čtyřletým	čtyřletý	k2eAgNnSc7d1	čtyřleté
doktorským	doktorský	k2eAgNnSc7d1	doktorské
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
završený	završený	k2eAgInSc4d1	završený
obhajobou	obhajoba	k1gFnSc7	obhajoba
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
není	být	k5eNaImIp3nS	být
dosahován	dosahovat	k5eAaImNgInS	dosahovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rigorózního	rigorózní	k2eAgNnSc2d1	rigorózní
řízení	řízení	k1gNnSc2	řízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
titulu	titul	k1gInSc2	titul
PhDr.	PhDr.	kA	PhDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
titul	titul	k1gInSc4	titul
magisterské	magisterský	k2eAgFnSc2d1	magisterská
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
de	de	k?	de
facto	facto	k1gNnSc1	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
jako	jako	k8xS	jako
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgInS	moct
též	též	k9	též
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
požádat	požádat	k5eAaPmF	požádat
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
fakultách	fakulta	k1gFnPc6	fakulta
i	i	k9	i
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
to	ten	k3xDgNnSc1	ten
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
příslušný	příslušný	k2eAgInSc1d1	příslušný
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
předpis	předpis	k1gInSc1	předpis
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
předložená	předložený	k2eAgFnSc1d1	předložená
magisterská	magisterský	k2eAgFnSc1d1	magisterská
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc1	práce
rovněž	rovněž	k9	rovněž
uznána	uznat	k5eAaPmNgFnS	uznat
i	i	k9	i
jako	jako	k9	jako
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
podrobnosti	podrobnost	k1gFnPc4	podrobnost
většinou	většinou	k6eAd1	většinou
upravoval	upravovat	k5eAaImAgMnS	upravovat
pokyn	pokyn	k1gInSc4	pokyn
rektora	rektor	k1gMnSc2	rektor
<g/>
,	,	kIx,	,
pokyn	pokyn	k1gInSc4	pokyn
děkana	děkan	k1gMnSc2	děkan
atp.	atp.	kA	atp.
Rigorózum	rigorózum	k1gNnSc1	rigorózum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
Slovensku	Slovensko	k1gNnSc6	Slovensko
finančně	finančně	k6eAd1	finančně
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
–	–	k?	–
poplatky	poplatek	k1gInPc7	poplatek
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spojené	spojený	k2eAgInPc1d1	spojený
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zápis	zápis	k1gInSc1	zápis
==	==	k?	==
</s>
</p>
<p>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
jak	jak	k8xC	jak
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
smlouvě	smlouva	k1gFnSc3	smlouva
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podstupovat	podstupovat	k5eAaImF	podstupovat
tzv.	tzv.	kA	tzv.
nostrifikaci	nostrifikace	k1gFnSc6	nostrifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
i	i	k8xC	i
stejný	stejný	k2eAgInSc1d1	stejný
zápis	zápis	k1gInSc1	zápis
–	–	k?	–
zkratka	zkratka	k1gFnSc1	zkratka
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
mezerou	mezera	k1gFnSc7	mezera
<g/>
,	,	kIx,	,
např.	např.	kA	např.
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
větného	větný	k2eAgInSc2d1	větný
celku	celek	k1gInSc2	celek
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
/	/	kIx~	/
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
filozofie	filozofie	k1gFnSc2	filozofie
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
získat	získat	k5eAaPmF	získat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
tří	tři	k4xCgFnPc2	tři
rigorózních	rigorózní	k2eAgFnPc2d1	rigorózní
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
pevně	pevně	k6eAd1	pevně
dány	dán	k2eAgFnPc1d1	dána
a	a	k8xC	a
které	který	k3yRgFnPc1	který
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
základní	základní	k2eAgInPc4d1	základní
obory	obor	k1gInPc4	obor
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
studií	studie	k1gFnPc2	studie
na	na	k7c6	na
filosofických	filosofický	k2eAgFnPc6d1	filosofická
fakultách	fakulta	k1gFnPc6	fakulta
(	(	kIx(	(
<g/>
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Thunových	Thunův	k2eAgFnPc6d1	Thunova
školských	školský	k2eAgFnPc6d1	školská
reformách	reforma	k1gFnPc6	reforma
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
vydán	vydán	k2eAgInSc1d1	vydán
nový	nový	k2eAgInSc1d1	nový
rigorózní	rigorózní	k2eAgInSc1d1	rigorózní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
titulu	titul	k1gInSc2	titul
PhDr.	PhDr.	kA	PhDr.
zavedl	zavést	k5eAaPmAgMnS	zavést
nutnost	nutnost	k1gFnSc4	nutnost
vypracování	vypracování	k1gNnSc3	vypracování
a	a	k8xC	a
obhájení	obhájení	k1gNnSc3	obhájení
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
mohla	moct	k5eAaImAgNnP	moct
být	být	k5eAaImF	být
sepsána	sepsat	k5eAaPmNgNnP	sepsat
podle	podle	k7c2	podle
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
fakulty	fakulta	k1gFnSc2	fakulta
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
nebo	nebo	k8xC	nebo
také	také	k9	také
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ovšem	ovšem	k9	ovšem
snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kratší	krátký	k2eAgFnSc1d2	kratší
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
z	z	k7c2	z
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
déletrvající	déletrvající	k2eAgFnSc1d1	déletrvající
druhá	druhý	k4xOgFnSc1	druhý
už	už	k9	už
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
vědecký	vědecký	k2eAgInSc4d1	vědecký
obor	obor	k1gInSc4	obor
podle	podle	k7c2	podle
výběru	výběr	k1gInSc2	výběr
kandidáta	kandidát	k1gMnSc2	kandidát
(	(	kIx(	(
<g/>
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
latina	latina	k1gFnSc1	latina
nebo	nebo	k8xC	nebo
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
;	;	kIx,	;
klasická	klasický	k2eAgFnSc1d1	klasická
filologie	filologie	k1gFnSc1	filologie
a	a	k8xC	a
starověké	starověký	k2eAgFnPc1d1	starověká
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
;	;	kIx,	;
dvojkombinace	dvojkombinace	k1gFnPc1	dvojkombinace
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
či	či	k8xC	či
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
přírodovědeckých	přírodovědecký	k2eAgInPc2d1	přírodovědecký
oborů	obor	k1gInPc2	obor
–	–	k?	–
botaniky	botanika	k1gFnPc4	botanika
<g/>
,	,	kIx,	,
zoologie	zoologie	k1gFnSc1	zoologie
nebo	nebo	k8xC	nebo
mineralogie	mineralogie	k1gFnSc1	mineralogie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
podoba	podoba	k1gFnSc1	podoba
získání	získání	k1gNnSc2	získání
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
pozdější	pozdní	k2eAgFnPc4d2	pozdější
obměny	obměna	k1gFnPc4	obměna
volitelných	volitelný	k2eAgInPc2d1	volitelný
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
i	i	k8xC	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
udělován	udělován	k2eAgInSc4d1	udělován
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
používali	používat	k5eAaImAgMnP	používat
studenti	student	k1gMnPc1	student
filosofických	filosofický	k2eAgFnPc2d1	filosofická
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
složili	složit	k5eAaPmAgMnP	složit
alespoň	alespoň	k9	alespoň
jednu	jeden	k4xCgFnSc4	jeden
státní	státní	k2eAgFnSc4d1	státní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
neoficiální	neoficiální	k2eAgInSc4d1	neoficiální
kandidátský	kandidátský	k2eAgInSc4d1	kandidátský
titul	titul	k1gInSc4	titul
PhC	PhC	k1gFnSc2	PhC
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
philosophiae	philosophiaat	k5eAaPmIp3nS	philosophiaat
candidatus	candidatus	k1gMnSc1	candidatus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
už	už	k6eAd1	už
nebyly	být	k5eNaImAgInP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
akademické	akademický	k2eAgInPc1d1	akademický
tituly	titul	k1gInPc1	titul
udělovány	udělován	k2eAgInPc1d1	udělován
a	a	k8xC	a
návrat	návrat	k1gInSc1	návrat
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc2	doktor
filosofie	filosofie	k1gFnSc2	filosofie
tak	tak	k6eAd1	tak
znamenal	znamenat	k5eAaImAgInS	znamenat
až	až	k9	až
další	další	k2eAgInSc1d1	další
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
ale	ale	k8xC	ale
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
akademicko-vědecké	akademickoědecký	k2eAgNnSc4d1	akademicko-vědecký
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
po	po	k7c6	po
sovětském	sovětský	k2eAgInSc6d1	sovětský
vzoru	vzor	k1gInSc6	vzor
tzv.	tzv.	kA	tzv.
vědecké	vědecký	k2eAgFnSc2d1	vědecká
hodnosti	hodnost	k1gFnSc2	hodnost
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
a	a	k8xC	a
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
PhDr.	PhDr.	kA	PhDr.
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
doktor	doktor	k1gMnSc1	doktor
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
)	)	kIx)	)
nicméně	nicméně	k8xC	nicméně
opět	opět	k6eAd1	opět
nebyl	být	k5eNaImAgInS	být
automaticky	automaticky	k6eAd1	automaticky
udělován	udělovat	k5eAaImNgInS	udělovat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
udělen	udělit	k5eAaPmNgMnS	udělit
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
absolvent	absolvent	k1gMnSc1	absolvent
opět	opět	k6eAd1	opět
obhájil	obhájit	k5eAaPmAgMnS	obhájit
písemnou	písemný	k2eAgFnSc4d1	písemná
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
složil	složit	k5eAaPmAgMnS	složit
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
ze	z	k7c2	z
zvoleného	zvolený	k2eAgInSc2d1	zvolený
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
širšího	široký	k2eAgInSc2d2	širší
vědního	vědní	k2eAgInSc2d1	vědní
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
fakultativní	fakultativní	k2eAgInPc1d1	fakultativní
malé	malý	k2eAgInPc1d1	malý
doktoráty	doktorát	k1gInPc1	doktorát
<g/>
,	,	kIx,	,
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
školy	škola	k1gFnSc2	škola
udělován	udělován	k2eAgInSc4d1	udělován
titul	titul	k1gInSc4	titul
magistra	magistra	k1gFnSc1	magistra
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
,	,	kIx,	,
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
pak	pak	k6eAd1	pak
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
stavem	stav	k1gInSc7	stav
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc1	titul
znovu	znovu	k6eAd1	znovu
zaveden	zavést	k5eAaPmNgInS	zavést
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nyní	nyní	k6eAd1	nyní
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
a	a	k8xC	a
zpoplatněné	zpoplatněný	k2eAgFnSc6d1	zpoplatněná
rigorózní	rigorózní	k2eAgFnSc6d1	rigorózní
zkoušce	zkouška	k1gFnSc6	zkouška
–	–	k?	–
jeho	jeho	k3xOp3gNnSc4	jeho
udělení	udělení	k1gNnSc4	udělení
tak	tak	k6eAd1	tak
nepředchází	předcházet	k5eNaImIp3nS	předcházet
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jak	jak	k6eAd1	jak
PhDr.	PhDr.	kA	PhDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
označují	označovat	k5eAaImIp3nP	označovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
zde	zde	k6eAd1	zde
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
standardní	standardní	k2eAgInSc1d1	standardní
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
značí	značit	k5eAaImIp3nS	značit
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
správně	správně	k6eAd1	správně
píše	psát	k5eAaImIp3nS	psát
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
oddělen	oddělit	k5eAaPmNgMnS	oddělit
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tuto	tento	k3xDgFnSc4	tento
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc4	činnost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-1998	[number]	k4	1990-1998
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
–	–	k?	–
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
PhDr.	PhDr.	kA	PhDr.
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
