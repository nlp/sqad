<s>
Doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
philosophiae	philosophiae	k1gInSc1	philosophiae
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
PhDr.	PhDr.	kA	PhDr.
psané	psaný	k2eAgInPc4d1	psaný
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
udělován	udělovat	k5eAaImNgInS	udělovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitních	humanitní	k2eAgInPc2d1	humanitní
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgInPc2d1	společenský
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
od	od	k7c2	od
zrušení	zrušení	k1gNnSc2	zrušení
udělování	udělování	k1gNnSc2	udělování
titulu	titul	k1gInSc2	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
PaedDr	PaedDr	kA	PaedDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
i	i	k8xC	i
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
příslušných	příslušný	k2eAgFnPc6d1	příslušná
fakultách	fakulta	k1gFnPc6	fakulta
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
