<p>
<s>
Kenozoikum	kenozoikum	k1gNnSc1	kenozoikum
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
κ	κ	k?	κ
kainos	kainos	k1gInSc1	kainos
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
nedávný	dávný	k2eNgInSc1d1	nedávný
a	a	k8xC	a
ζ	ζ	k?	ζ
zóé	zóé	k?	zóé
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
geologická	geologický	k2eAgFnSc1d1	geologická
éra	éra	k1gFnSc1	éra
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
před	před	k7c7	před
65,5	[number]	k4	65,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
±	±	k?	±
0,3	[number]	k4	0,3
m.	m.	k?	m.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
po	po	k7c6	po
velkém	velký	k2eAgNnSc6d1	velké
vymírání	vymírání	k1gNnSc6	vymírání
živočišných	živočišný	k2eAgInPc2d1	živočišný
a	a	k8xC	a
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
skončila	skončit	k5eAaPmAgFnS	skončit
éra	éra	k1gFnSc1	éra
druhohor	druhohory	k1gFnPc2	druhohory
–	–	k?	–
mezozoikum	mezozoikum	k1gNnSc4	mezozoikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kenozoikum	kenozoikum	k1gNnSc1	kenozoikum
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
éra	éra	k1gFnSc1	éra
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Kenozoikum	kenozoikum	k1gNnSc1	kenozoikum
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
periody	perioda	k1gFnPc4	perioda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kvartér	kvartér	k1gInSc1	kvartér
(	(	kIx(	(
<g/>
čtvrtohory	čtvrtohory	k1gFnPc1	čtvrtohory
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2,588	[number]	k4	2,588
Ma	Ma	k1gFnPc2	Ma
až	až	k6eAd1	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neogén	neogén	k1gInSc1	neogén
(	(	kIx(	(
<g/>
asi	asi	k9	asi
23,0	[number]	k4	23,0
Ma	Ma	k1gFnPc2	Ma
–	–	k?	–
2,588	[number]	k4	2,588
Ma	Ma	k1gFnSc1	Ma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paleogén	paleogén	k1gInSc1	paleogén
(	(	kIx(	(
<g/>
asi	asi	k9	asi
65,5	[number]	k4	65,5
–	–	k?	–
23,0	[number]	k4	23,0
Ma	Ma	k1gFnSc1	Ma
<g/>
)	)	kIx)	)
<g/>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
neogén	neogén	k1gInSc1	neogén
a	a	k8xC	a
paleogén	paleogén	k1gInSc1	paleogén
spojoval	spojovat	k5eAaImAgInS	spojovat
do	do	k7c2	do
éry	éra	k1gFnSc2	éra
s	s	k7c7	s
názvem	název	k1gInSc7	název
třetihory	třetihory	k1gFnPc1	třetihory
(	(	kIx(	(
<g/>
terciér	terciér	k1gInSc1	terciér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
již	již	k6eAd1	již
ale	ale	k8xC	ale
podle	podle	k7c2	podle
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
stratigrafii	stratigrafie	k1gFnSc4	stratigrafie
(	(	kIx(	(
<g/>
ICS	ICS	kA	ICS
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
stratigrafickou	stratigrafický	k2eAgFnSc7d1	stratigrafická
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
pomocná	pomocný	k2eAgFnSc1d1	pomocná
jednotka	jednotka	k1gFnSc1	jednotka
nadále	nadále	k6eAd1	nadále
užívá	užívat	k5eAaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologický	geologický	k2eAgInSc4d1	geologický
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
kenozoiku	kenozoikum	k1gNnSc6	kenozoikum
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
dělení	dělení	k1gNnSc1	dělení
kontinentů	kontinent	k1gInPc2	kontinent
až	až	k9	až
po	po	k7c4	po
jejich	jejich	k3xOp3gFnSc4	jejich
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
procesem	proces	k1gInSc7	proces
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
posun	posun	k1gInSc1	posun
fragmetů	fragmet	k1gMnPc2	fragmet
Gondwany	Gondwana	k1gFnSc2	Gondwana
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
Laurasií	Laurasie	k1gFnSc7	Laurasie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
otevření	otevření	k1gNnSc4	otevření
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
též	též	k9	též
směřovala	směřovat	k5eAaImAgFnS	směřovat
severně	severně	k6eAd1	severně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úplné	úplný	k2eAgFnSc3d1	úplná
kolizi	kolize	k1gFnSc3	kolize
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
zabránily	zabránit	k5eAaPmAgFnP	zabránit
menší	malý	k2eAgFnPc1d2	menší
tektonické	tektonický	k2eAgFnPc1d1	tektonická
desky	deska	k1gFnPc1	deska
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působily	působit	k5eAaImAgFnP	působit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
airbagy	airbag	k1gInPc1	airbag
<g/>
"	"	kIx"	"
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
utlumily	utlumit	k5eAaPmAgInP	utlumit
náraz	náraz	k1gInSc4	náraz
v	v	k7c6	v
čase	čas	k1gInSc6	čas
středního	střední	k2eAgInSc2d1	střední
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
tvoří	tvořit	k5eAaImIp3nS	tvořit
recentní	recentní	k2eAgInSc1d1	recentní
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střet	střet	k1gInSc1	střet
Afriky	Afrika	k1gFnSc2	Afrika
s	s	k7c7	s
Euroasijskou	euroasijský	k2eAgFnSc7d1	euroasijská
deskou	deska	k1gFnSc7	deska
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
poklidný	poklidný	k2eAgMnSc1d1	poklidný
<g/>
,	,	kIx,	,
panovaly	panovat	k5eAaImAgFnP	panovat
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
kompresní	kompresní	k2eAgFnSc1d1	kompresní
sily	sít	k5eAaImAgInP	sít
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgInSc2	tento
střetu	střet	k1gInSc2	střet
byl	být	k5eAaImAgInS	být
zánik	zánik	k1gInSc1	zánik
moře	moře	k1gNnSc2	moře
Tethys	Tethysa	k1gFnPc2	Tethysa
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInPc1	jeho
zbytky	zbytek	k1gInPc1	zbytek
jsou	být	k5eAaImIp3nP	být
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
a	a	k8xC	a
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Komprese	komprese	k1gFnSc1	komprese
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
vznik	vznik	k1gInSc4	vznik
pohoří	pohoří	k1gNnSc2	pohoří
Alpy	alpa	k1gFnSc2	alpa
a	a	k8xC	a
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
velký	velký	k2eAgInSc4d1	velký
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
morfologii	morfologie	k1gFnSc4	morfologie
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
měl	mít	k5eAaImAgInS	mít
střet	střet	k1gInSc1	střet
Indie	Indie	k1gFnSc2	Indie
s	s	k7c7	s
Asijským	asijský	k2eAgInSc7d1	asijský
kontinentem	kontinent	k1gInSc7	kontinent
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
několika	několik	k4yIc2	několik
pásmových	pásmový	k2eAgNnPc2d1	pásmové
pohoří	pohoří	k1gNnPc2	pohoří
(	(	kIx(	(
<g/>
Himálaj	Himálaj	k1gFnSc1	Himálaj
<g/>
,	,	kIx,	,
Pamír	Pamír	k1gInSc1	Pamír
<g/>
,	,	kIx,	,
Hindúkuš	Hindúkuš	k1gInSc1	Hindúkuš
<g/>
,	,	kIx,	,
atd	atd	kA	atd
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrásnění	vrásnění	k1gNnSc4	vrásnění
které	který	k3yQgNnSc1	který
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
vznik	vznik	k1gInSc4	vznik
pásmových	pásmový	k2eAgNnPc2d1	pásmové
pohoří	pohoří	k1gNnPc2	pohoří
v	v	k7c6	v
kenozoiku	kenozoikum	k1gNnSc6	kenozoikum
(	(	kIx(	(
<g/>
příp	příp	kA	příp
<g/>
.	.	kIx.	.
koncem	koncem	k7c2	koncem
mezozoika	mezozoikum	k1gNnSc2	mezozoikum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Alpinské	Alpinský	k2eAgNnSc1d1	Alpinské
vrásnění	vrásnění	k1gNnSc1	vrásnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
klimatologie	klimatologie	k1gFnSc2	klimatologie
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
teplé	teplý	k2eAgNnSc4d1	teplé
období	období	k1gNnSc4	období
z	z	k7c2	z
druhohor	druhohory	k1gFnPc2	druhohory
<g/>
,	,	kIx,	,
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
eocénu	eocén	k1gInSc6	eocén
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
mnohem	mnohem	k6eAd1	mnohem
teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
vlhčí	vlhký	k2eAgInSc1d2	vlhčí
podnebí	podnebí	k1gNnSc2	podnebí
než	než	k8xS	než
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Nástupem	nástup	k1gInSc7	nástup
neogénu	neogén	k1gInSc2	neogén
se	se	k3xPyFc4	se
klima	klima	k1gNnSc1	klima
začíná	začínat	k5eAaImIp3nS	začínat
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
<g/>
,	,	kIx,	,
možná	možný	k2eAgFnSc1d1	možná
příčina	příčina	k1gFnSc1	příčina
tkví	tkvět	k5eAaImIp3nS	tkvět
ve	v	k7c6	v
vyzdvihnutí	vyzdvihnutí	k1gNnSc6	vyzdvihnutí
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ochlazování	ochlazování	k1gNnSc2	ochlazování
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
ledových	ledový	k2eAgFnPc2d1	ledová
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
přerušených	přerušený	k2eAgFnPc2d1	přerušená
teplejšími	teplý	k2eAgFnPc7d2	teplejší
periodami	perioda	k1gFnPc7	perioda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
ledovců	ledovec	k1gInPc2	ledovec
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
hladiny	hladina	k1gFnSc2	hladina
moří	mořit	k5eAaImIp3nS	mořit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nových	nový	k2eAgFnPc2d1	nová
pevnin	pevnina	k1gFnPc2	pevnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
křídy	křída	k1gFnSc2	křída
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masívnímu	masívní	k2eAgNnSc3d1	masívní
vyhynutí	vyhynutí	k1gNnSc3	vyhynutí
většiny	většina	k1gFnSc2	většina
mořských	mořský	k2eAgInPc2d1	mořský
(	(	kIx(	(
<g/>
amoniti	amonit	k5eAaImF	amonit
<g/>
,	,	kIx,	,
belemiti	belemit	k5eAaPmF	belemit
<g/>
,	,	kIx,	,
mořští	mořský	k2eAgMnPc1d1	mořský
plazi	plaz	k1gMnPc1	plaz
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
suchozemských	suchozemský	k2eAgNnPc2d1	suchozemské
(	(	kIx(	(
<g/>
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
)	)	kIx)	)
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
savci	savec	k1gMnPc1	savec
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
rychle	rychle	k6eAd1	rychle
využili	využít	k5eAaPmAgMnP	využít
volný	volný	k2eAgInSc4d1	volný
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
začátkem	začátkem	k7c2	začátkem
paleogénu	paleogén	k1gInSc2	paleogén
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
stávají	stávat	k5eAaImIp3nP	stávat
vládci	vládce	k1gMnPc1	vládce
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
vysocí	vysoký	k2eAgMnPc1d1	vysoký
nelétaví	létavý	k2eNgMnPc1d1	nelétavý
dravci	dravec	k1gMnPc1	dravec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
zkameněliny	zkamenělina	k1gFnPc1	zkamenělina
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
v	v	k7c6	v
Laurasii	Laurasie	k1gFnSc6	Laurasie
a	a	k8xC	a
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
postrachem	postrach	k1gInSc7	postrach
všeho	všecek	k3xTgNnSc2	všecek
živého	živé	k1gNnSc2	živé
v	v	k7c6	v
kenozoiku	kenozoikum	k1gNnSc6	kenozoikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
biostratigrafickému	biostratigrafický	k2eAgNnSc3d1	biostratigrafický
členění	členění	k1gNnSc3	členění
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
nanoplankton	nanoplankton	k1gInSc1	nanoplankton
a	a	k8xC	a
planktonní	planktonní	k2eAgMnPc1d1	planktonní
dírkovci	dírkovec	k1gMnPc1	dírkovec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
fauně	fauna	k1gFnSc3	fauna
patří	patřit	k5eAaImIp3nS	patřit
měkkýši	měkkýš	k1gMnPc1	měkkýš
a	a	k8xC	a
žraloci	žralok	k1gMnPc1	žralok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
faunu	fauna	k1gFnSc4	fauna
v	v	k7c6	v
neogénu	neogén	k1gInSc6	neogén
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
druhů	druh	k1gInPc2	druh
spásajících	spásající	k2eAgInPc2d1	spásající
trávu	tráva	k1gFnSc4	tráva
na	na	k7c6	na
savanách	savana	k1gFnPc6	savana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
spásání	spásání	k1gNnSc4	spásání
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
adaptovali	adaptovat	k5eAaBmAgMnP	adaptovat
někteří	některý	k3yIgMnPc1	některý
býložraví	býložravý	k2eAgMnPc1d1	býložravý
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
lichokopytníci	lichokopytník	k1gMnPc1	lichokopytník
<g/>
,	,	kIx,	,
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
větev	větev	k1gFnSc4	větev
představuje	představovat	k5eAaImIp3nS	představovat
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
relativně	relativně	k6eAd1	relativně
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přinesla	přinést	k5eAaPmAgFnS	přinést
mnoho	mnoho	k4c4	mnoho
životních	životní	k2eAgFnPc2d1	životní
forem	forma	k1gFnPc2	forma
<g/>
:	:	kIx,	:
rody	rod	k1gInPc1	rod
Litopterna	Litopterna	k1gFnSc1	Litopterna
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstate	podstat	k1gInSc5	podstat
kombinace	kombinace	k1gFnPc4	kombinace
velblouda	velbloud	k1gMnSc2	velbloud
a	a	k8xC	a
koně	kůň	k1gMnSc2	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Notoungulata	Notoungule	k1gNnPc1	Notoungule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
šavlozubí	šavlozubý	k2eAgMnPc1d1	šavlozubý
tygři	tygr	k1gMnPc1	tygr
nebo	nebo	k8xC	nebo
jeskynní	jeskynní	k2eAgMnPc1d1	jeskynní
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
chobotnatci	chobotnatec	k1gMnPc1	chobotnatec
i	i	k8xC	i
kytovci	kytovec	k1gMnPc1	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
primátů	primát	k1gMnPc2	primát
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
neogénu	neogén	k1gInSc2	neogén
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
předkové	předek	k1gMnPc1	předek
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
australopithekové	australopitheková	k1gFnPc1	australopitheková
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c6	na
chůzi	chůze	k1gFnSc6	chůze
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
končetinách	končetina	k1gFnPc6	končetina
(	(	kIx(	(
<g/>
bipedie	bipedie	k1gFnSc1	bipedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1	předchůdce
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
období	období	k1gNnSc1	období
začínající	začínající	k2eAgFnSc2d1	začínající
právě	právě	k6eAd1	právě
tímto	tento	k3xDgInSc7	tento
počátkem	počátek	k1gInSc7	počátek
užívání	užívání	k1gNnSc2	užívání
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
nazývá	nazývat	k5eAaImIp3nS	nazývat
paleolit	paleolit	k1gInSc4	paleolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
pleistocénu	pleistocén	k1gInSc2	pleistocén
na	na	k7c6	na
konci	konec	k1gInSc6	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
–	–	k?	–
würmu	würm	k1gInSc2	würm
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vymírání	vymírání	k1gNnSc3	vymírání
řady	řada	k1gFnSc2	řada
druhů	druh	k1gInPc2	druh
velkých	velký	k2eAgMnPc2d1	velký
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mamutů	mamut	k1gMnPc2	mamut
<g/>
,	,	kIx,	,
obřích	obří	k2eAgMnPc2d1	obří
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
srstnatých	srstnatý	k2eAgMnPc2d1	srstnatý
nosorožců	nosorožec	k1gMnPc2	nosorožec
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
spekulací	spekulace	k1gFnPc2	spekulace
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc1	vliv
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tehdejšímu	tehdejší	k2eAgNnSc3d1	tehdejší
řídkému	řídký	k2eAgNnSc3d1	řídké
lidskému	lidský	k2eAgNnSc3d1	lidské
osídlení	osídlení	k1gNnSc3	osídlení
lze	lze	k6eAd1	lze
však	však	k9	však
příčinu	příčina	k1gFnSc4	příčina
vymírání	vymírání	k1gNnSc2	vymírání
spíš	spát	k5eAaImIp2nS	spát
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
globální	globální	k2eAgFnSc6d1	globální
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
vymírá	vymírat	k5eAaImIp3nS	vymírat
člověk	člověk	k1gMnSc1	člověk
neandrtálský	neandrtálský	k2eAgMnSc1d1	neandrtálský
a	a	k8xC	a
homo	homo	k6eAd1	homo
sapiens	sapiens	k6eAd1	sapiens
sapiens	sapiens	k6eAd1	sapiens
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
posledním	poslední	k2eAgMnSc7d1	poslední
příslušníkem	příslušník	k1gMnSc7	příslušník
rodu	rod	k1gInSc2	rod
homo	homo	k1gNnSc1	homo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
primitivních	primitivní	k2eAgFnPc2d1	primitivní
kultur	kultura	k1gFnPc2	kultura
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
v	v	k7c6	v
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kultury	kultura	k1gFnPc4	kultura
lovců	lovec	k1gMnPc2	lovec
sobů	sob	k1gMnPc2	sob
v	v	k7c6	v
mezolitu	mezolit	k1gInSc6	mezolit
člověk	člověk	k1gMnSc1	člověk
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
neolitu	neolit	k1gInSc6	neolit
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
neolitu	neolit	k1gInSc2	neolit
poznává	poznávat	k5eAaImIp3nS	poznávat
možnosti	možnost	k1gFnPc4	možnost
využití	využití	k1gNnSc2	využití
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
objevují	objevovat	k5eAaImIp3nP	objevovat
nové	nový	k2eAgFnPc1d1	nová
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
lidské	lidský	k2eAgFnPc1d1	lidská
kultury	kultura	k1gFnPc1	kultura
nebo	nebo	k8xC	nebo
celé	celý	k2eAgFnPc1d1	celá
civilizace	civilizace	k1gFnPc1	civilizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Vymírání	vymírání	k1gNnSc1	vymírání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
postihlo	postihnout	k5eAaPmAgNnS	postihnout
i	i	k9	i
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
hlavně	hlavně	k9	hlavně
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
cykasorostů	cykasorost	k1gInPc2	cykasorost
a	a	k8xC	a
gingorostů	gingorost	k1gInPc2	gingorost
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozvoje	rozvoj	k1gInSc2	rozvoj
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
krytosemenné	krytosemenný	k2eAgFnPc1d1	krytosemenná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dominují	dominovat	k5eAaImIp3nP	dominovat
suchozemské	suchozemský	k2eAgFnSc3d1	suchozemská
flóře	flóra	k1gFnSc3	flóra
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
však	však	k9	však
stále	stále	k6eAd1	stále
zažívají	zažívat	k5eAaImIp3nP	zažívat
i	i	k9	i
borovicorosty	borovicorost	k1gInPc1	borovicorost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
místy	místy	k6eAd1	místy
dominují	dominovat	k5eAaImIp3nP	dominovat
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
tajga	tajga	k1gFnSc1	tajga
<g/>
,	,	kIx,	,
vysoká	vysoká	k1gFnSc1	vysoká
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
bylo	být	k5eAaImAgNnS	být
klima	klima	k1gNnSc1	klima
teplé	teplý	k2eAgNnSc1d1	teplé
bez	bez	k7c2	bez
zalednění	zalednění	k1gNnSc2	zalednění
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemu	co	k3yQnSc3	co
rostly	růst	k5eAaImAgInP	růst
subtropické	subtropický	k2eAgInPc1d1	subtropický
lesy	les	k1gInPc1	les
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
čtvrtohorních	čtvrtohorní	k2eAgNnPc2d1	čtvrtohorní
zalednění	zalednění	k1gNnPc2	zalednění
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
vícenásobné	vícenásobný	k2eAgNnSc1d1	vícenásobné
střídání	střídání	k1gNnSc1	střídání
s	s	k7c7	s
dobami	doba	k1gFnPc7	doba
meziledovými	meziledový	k2eAgFnPc7d1	meziledová
způsobil	způsobit	k5eAaPmAgInS	způsobit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
zátěž	zátěž	k1gFnSc4	zátěž
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
flóru	flóra	k1gFnSc4	flóra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
ledových	ledový	k2eAgFnPc6d1	ledová
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
rostlin	rostlina	k1gFnPc2	rostlina
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c6	na
nejjižnějších	jižní	k2eAgInPc6d3	nejjižnější
poloostrovech	poloostrov	k1gInPc6	poloostrov
kontinentu	kontinent	k1gInSc2	kontinent
(	(	kIx(	(
<g/>
Pyrenejský	pyrenejský	k2eAgMnSc1d1	pyrenejský
<g/>
,	,	kIx,	,
Apeninský	apeninský	k2eAgMnSc1d1	apeninský
a	a	k8xC	a
jih	jih	k1gInSc1	jih
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
svojí	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
nejsou	být	k5eNaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
pronikání	pronikání	k1gNnSc6	pronikání
na	na	k7c4	na
jih	jih	k1gInSc4	jih
jim	on	k3xPp3gMnPc3	on
částečně	částečně	k6eAd1	částečně
bránilo	bránit	k5eAaImAgNnS	bránit
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
klimatu	klima	k1gNnSc2	klima
spojené	spojený	k2eAgFnPc1d1	spojená
se	s	k7c7	s
soustředěním	soustředění	k1gNnSc7	soustředění
druhů	druh	k1gInPc2	druh
na	na	k7c6	na
omezeném	omezený	k2eAgNnSc6d1	omezené
území	území	k1gNnSc6	území
způsobovaly	způsobovat	k5eAaImAgFnP	způsobovat
opakované	opakovaný	k2eAgNnSc4d1	opakované
vymírání	vymírání	k1gNnSc4	vymírání
mnohých	mnohý	k2eAgInPc2d1	mnohý
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
ochuzování	ochuzování	k1gNnSc2	ochuzování
evropské	evropský	k2eAgFnSc2d1	Evropská
flóry	flóra	k1gFnSc2	flóra
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
částečně	částečně	k6eAd1	částečně
doplněná	doplněný	k2eAgFnSc1d1	doplněná
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
druhově	druhově	k6eAd1	druhově
chudší	chudý	k2eAgFnSc1d2	chudší
než	než	k8xS	než
asijská	asijský	k2eAgFnSc1d1	asijská
nebo	nebo	k8xC	nebo
severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
flóry	flóra	k1gFnPc4	flóra
zalednění	zalednění	k1gNnSc2	zalednění
představovala	představovat	k5eAaImAgFnS	představovat
menší	malý	k2eAgFnSc1d2	menší
zátěž	zátěž	k1gFnSc1	zátěž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Evropy	Evropa	k1gFnSc2	Evropa
pronikání	pronikání	k1gNnSc2	pronikání
druhů	druh	k1gInPc2	druh
na	na	k7c4	na
teplejší	teplý	k2eAgInSc4d2	teplejší
jih	jih	k1gInSc4	jih
nebránilo	bránit	k5eNaImAgNnS	bránit
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
ani	ani	k8xC	ani
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
žádné	žádný	k3yNgNnSc1	žádný
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kenozoikum	kenozoikum	k1gNnSc4	kenozoikum
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Geologický	geologický	k2eAgInSc1d1	geologický
čas	čas	k1gInSc1	čas
</s>
</p>
<p>
<s>
Terciér	terciér	k1gInSc1	terciér
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kenozoikum	kenozoikum	k1gNnSc4	kenozoikum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
sopky	sopka	k1gFnPc1	sopka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
