<s>
Příčiny	příčina	k1gFnPc4	příčina
nového	nový	k2eAgInSc2d1	nový
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
konfliktu	konflikt	k1gInSc2	konflikt
tkvěly	tkvět	k5eAaImAgFnP	tkvět
v	v	k7c6	v
nespokojenosti	nespokojenost	k1gFnSc6	nespokojenost
s	s	k7c7	s
výsledky	výsledek	k1gInPc4	výsledek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
menších	malý	k2eAgFnPc2d2	menší
zemí	zem	k1gFnPc2	zem
zahnízdila	zahnízdit	k5eAaPmAgFnS	zahnízdit
především	především	k6eAd1	především
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
velkých	velký	k2eAgFnPc6d1	velká
mocnostech	mocnost	k1gFnPc6	mocnost
<g/>
.	.	kIx.	.
</s>
