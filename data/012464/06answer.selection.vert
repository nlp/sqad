<s>
Mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
mléko	mléko	k1gNnSc1	mléko
produkované	produkovaný	k2eAgNnSc1d1	produkované
laktací	laktace	k1gFnSc7	laktace
v	v	k7c6	v
mléčných	mléčný	k2eAgFnPc6d1	mléčná
žlázách	žláza	k1gFnPc6	žláza
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
prsou	prsa	k1gNnPc6	prsa
sloužící	sloužící	k2eAgFnSc2d1	sloužící
k	k	k7c3	k
výživě	výživa	k1gFnSc3	výživa
novorozeného	novorozený	k2eAgMnSc2d1	novorozený
potomka	potomek	k1gMnSc2	potomek
<g/>
.	.	kIx.	.
</s>
