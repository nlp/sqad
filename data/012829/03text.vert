<p>
<s>
Arachnologie	Arachnologie	k1gFnSc1	Arachnologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Arachnida	Arachnid	k1gMnSc2	Arachnid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jejich	jejich	k3xOp3gFnSc4	jejich
anatomii	anatomie	k1gFnSc4	anatomie
<g/>
,	,	kIx,	,
ekologii	ekologie	k1gFnSc6	ekologie
<g/>
,	,	kIx,	,
embryologii	embryologie	k1gFnSc6	embryologie
<g/>
,	,	kIx,	,
fyziologii	fyziologie	k1gFnSc6	fyziologie
<g/>
,	,	kIx,	,
fylogenii	fylogenie	k1gFnSc6	fylogenie
<g/>
,	,	kIx,	,
klasifikaci	klasifikace	k1gFnSc6	klasifikace
a	a	k8xC	a
zoogeografii	zoogeografie	k1gFnSc6	zoogeografie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zoologie	zoologie	k1gFnSc2	zoologie
je	být	k5eAaImIp3nS	být
nesprávně	správně	k6eNd1	správně
používán	používat	k5eAaImNgInS	používat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
studia	studio	k1gNnSc2	studio
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgInSc4	jeden
řád	řád	k1gInSc4	řád
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
pavoukovců	pavoukovec	k1gInPc2	pavoukovec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
obor	obor	k1gInSc1	obor
se	se	k3xPyFc4	se
od	od	k7c2	od
arachnologie	arachnologie	k1gFnSc2	arachnologie
odštěpila	odštěpit	k5eAaPmAgFnS	odštěpit
akarologie	akarologie	k1gFnSc1	akarologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
roztočů	roztoč	k1gMnPc2	roztoč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
oborem	obor	k1gInSc7	obor
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
arachnolog	arachnolog	k1gMnSc1	arachnolog
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
arachnologů	arachnolog	k1gMnPc2	arachnolog
byl	být	k5eAaImAgMnS	být
Eugè	Eugè	k1gMnSc1	Eugè
Simon	Simon	k1gMnSc1	Simon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
rozšířenému	rozšířený	k2eAgInSc3d1	rozšířený
odporu	odpor	k1gInSc3	odpor
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
pavoukům	pavouk	k1gMnPc3	pavouk
je	být	k5eAaImIp3nS	být
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
oblastmi	oblast	k1gFnPc7	oblast
zoologie	zoologie	k1gFnSc2	zoologie
méně	málo	k6eAd2	málo
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Pavoukovci	Pavoukovec	k1gMnPc1	Pavoukovec
obývají	obývat	k5eAaImIp3nP	obývat
všechny	všechen	k3xTgInPc4	všechen
pozemské	pozemský	k2eAgInPc4d1	pozemský
ekosystémy	ekosystém	k1gInPc4	ekosystém
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
specializace	specializace	k1gFnSc2	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosud	dosud	k6eAd1	dosud
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
žijící	žijící	k2eAgInPc1d1	žijící
i	i	k8xC	i
jen	jen	k9	jen
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
arachnologie	arachnologie	k1gFnSc1	arachnologie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
písemné	písemný	k2eAgFnPc1d1	písemná
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
arachnofauně	arachnofauna	k1gFnSc6	arachnofauna
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Jana	Jan	k1gMnSc2	Jan
Daniela	Daniel	k1gMnSc2	Daniel
Preyslera	Preysler	k1gMnSc2	Preysler
<g/>
.	.	kIx.	.
</s>
<s>
Soustavný	soustavný	k2eAgInSc1d1	soustavný
výzkum	výzkum	k1gInSc1	výzkum
započal	započnout	k5eAaPmAgInS	započnout
až	až	k9	až
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
arachnologům	arachnolog	k1gMnPc3	arachnolog
patří	patřit	k5eAaImIp3nP	patřit
profesor	profesor	k1gMnSc1	profesor
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
František	František	k1gMnSc1	František
Miller	Miller	k1gMnSc1	Miller
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
směr	směr	k1gInSc1	směr
tohoto	tento	k3xDgInSc2	tento
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
milníkem	milník	k1gInSc7	milník
české	český	k2eAgFnSc2d1	Česká
arachnologie	arachnologie	k1gFnSc2	arachnologie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
uspořádání	uspořádání	k1gNnSc2	uspořádání
V.	V.	kA	V.
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
arachnologického	arachnologický	k2eAgInSc2d1	arachnologický
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
právě	právě	k9	právě
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
profesora	profesor	k1gMnSc2	profesor
Millera	Miller	k1gMnSc2	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgMnS	Internationat
Society	societa	k1gFnSc2	societa
of	of	k?	of
Arachnology	Arachnolog	k1gMnPc4	Arachnolog
</s>
</p>
