<p>
<s>
Jako	jako	k8xC	jako
nitril	nitril	k1gInSc1	nitril
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
-C	-C	k?	-C
<g/>
≡	≡	k?	≡
<g/>
N.	N.	kA	N.
Skupina	skupina	k1gFnSc1	skupina
-C	-C	k?	-C
<g/>
≡	≡	k?	≡
<g/>
N	N	kA	N
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nitrilová	nitrilový	k2eAgFnSc1d1	nitrilový
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
též	též	k9	též
jen	jen	k9	jen
nitril	nitril	k1gInSc1	nitril
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
-CN	-CN	k?	-CN
jsou	být	k5eAaImIp3nP	být
uhlíkový	uhlíkový	k2eAgInSc4d1	uhlíkový
a	a	k8xC	a
dusíkový	dusíkový	k2eAgInSc4d1	dusíkový
atom	atom	k1gInSc4	atom
vázány	vázat	k5eAaImNgFnP	vázat
trojnou	trojný	k2eAgFnSc7d1	trojná
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgNnSc6d1	chemické
názvosloví	názvosloví	k1gNnSc6	názvosloví
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
indikaci	indikace	k1gFnSc4	indikace
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nitrilové	nitrilové	k2eAgFnSc2d1	nitrilové
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
prefix	prefix	k1gInSc1	prefix
kyan	kyan	k1gInSc1	kyan
<g/>
.	.	kIx.	.
</s>
<s>
Kyanidový	kyanidový	k2eAgInSc1d1	kyanidový
iont	iont	k1gInSc1	iont
má	mít	k5eAaImIp3nS	mít
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
a	a	k8xC	a
vzorec	vzorec	k1gInSc1	vzorec
CN	CN	kA	CN
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
-CN	-CN	k?	-CN
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
kyanidová	kyanidový	k2eAgFnSc1d1	kyanidová
skupina	skupina	k1gFnSc1	skupina
nebo	nebo	k8xC	nebo
kyanoskupina	kyanoskupina	k1gFnSc1	kyanoskupina
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
ji	on	k3xPp3gFnSc4	on
obsahující	obsahující	k2eAgFnSc4d1	obsahující
jako	jako	k8xS	jako
kyanidy	kyanid	k1gInPc7	kyanid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nitrily	nitril	k1gInPc1	nitril
někdy	někdy	k6eAd1	někdy
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
vysoce	vysoce	k6eAd1	vysoce
toxický	toxický	k2eAgInSc1d1	toxický
kyanidový	kyanidový	k2eAgInSc1d1	kyanidový
aniont	aniont	k1gInSc1	aniont
CN	CN	kA	CN
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc4	článek
kyanid	kyanid	k1gInSc4	kyanid
pro	pro	k7c4	pro
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
biologických	biologický	k2eAgInPc6d1	biologický
účincích	účinek	k1gInPc6	účinek
a	a	k8xC	a
toxicitě	toxicita	k1gFnSc3	toxicita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nitrile	nitril	k1gInSc5	nitril
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
