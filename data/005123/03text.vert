<s>
2315	[number]	k4	2315
Czechoslovakia	Czechoslovakia	k1gFnSc1	Czechoslovakia
je	být	k5eAaImIp3nS	být
planetka	planetka	k1gFnSc1	planetka
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pásu	pás	k1gInSc6	pás
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
českou	český	k2eAgFnSc7d1	Česká
astronomkou	astronomka	k1gFnSc7	astronomka
Zdeňkou	Zdeňka	k1gFnSc7	Zdeňka
Vávrovou	Vávrová	k1gFnSc7	Vávrová
z	z	k7c2	z
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
Kleť	Kleť	k1gFnSc1	Kleť
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
vlasti	vlast	k1gFnSc6	vlast
objevitelky	objevitelka	k1gFnSc2	objevitelka
<g/>
,	,	kIx,	,
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
5,23	[number]	k4	5,23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc4	seznam
planetek	planetka	k1gFnPc2	planetka
2251-2500	[number]	k4	2251-2500
2315	[number]	k4	2315
Czechoslovakia	Czechoslovakia	k1gFnSc1	Czechoslovakia
na	na	k7c4	na
IAU	IAU	kA	IAU
Minor	minor	k2eAgFnPc2d1	minor
Planet	planeta	k1gFnPc2	planeta
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
2315	[number]	k4	2315
Czechoslovakia	Czechoslovakium	k1gNnPc4	Czechoslovakium
na	na	k7c4	na
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
<g/>
astro	astra	k1gFnSc5	astra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
JPL	JPL	kA	JPL
Small-Body	Small-Boda	k1gFnSc2	Small-Boda
Database	Databasa	k1gFnSc3	Databasa
Browser	Browser	k1gMnSc1	Browser
on	on	k3xPp3gMnSc1	on
2315	[number]	k4	2315
Czechoslovakia	Czechoslovakium	k1gNnSc2	Czechoslovakium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
