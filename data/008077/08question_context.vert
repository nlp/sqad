<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
kvašený	kvašený	k2eAgInSc4d1	kvašený
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
hořké	hořký	k2eAgFnSc2d1	hořká
chuti	chuť	k1gFnSc2	chuť
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
v	v	k7c6	v
pivovaru	pivovar	k1gInSc6	pivovar
z	z	k7c2	z
obilného	obilný	k2eAgInSc2d1	obilný
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
nezbytně	zbytně	k6eNd1	zbytně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
<g/>
)	)	kIx)	)
chmele	chmel	k1gInSc2	chmel
pomocí	pomocí	k7c2	pomocí
pivovarských	pivovarský	k2eAgFnPc2d1	Pivovarská
kvasinek	kvasinka	k1gFnPc2	kvasinka
(	(	kIx(	(
<g/>
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisia	k1gFnSc2	cerevisia
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
eventuálně	eventuálně	k6eAd1	eventuálně
divokých	divoký	k2eAgFnPc2d1	divoká
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
značné	značný	k2eAgFnSc3d1	značná
oblibě	obliba	k1gFnSc3	obliba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejkonzumovanější	konzumovaný	k2eAgInSc4d3	konzumovaný
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>

