<p>
<s>
Benitoit	benitoit	k1gInSc1	benitoit
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc1d1	vzácný
Ba	ba	k9	ba
a	a	k8xC	a
Ti	ten	k3xDgMnPc1	ten
cyklosilikát	cyklosilikát	k1gInSc4	cyklosilikát
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
San	San	k1gFnSc2	San
Benito	Benita	k1gMnSc5	Benita
River	Rivra	k1gFnPc2	Rivra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
byl	být	k5eAaImAgMnS	být
také	také	k9	také
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určujícím	určující	k2eAgInSc7d1	určující
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
fluorescence	fluorescence	k1gFnSc2	fluorescence
pod	pod	k7c7	pod
krátkými	krátký	k2eAgFnPc7d1	krátká
vlnami	vlna	k1gFnPc7	vlna
UV	UV	kA	UV
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
září	zářit	k5eAaImIp3nS	zářit
silně	silně	k6eAd1	silně
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
menší	malý	k2eAgFnSc4d2	menší
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
odolnost	odolnost	k1gFnSc4	odolnost
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
drahokamy	drahokam	k1gInPc7	drahokam
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
ho	on	k3xPp3gMnSc4	on
nelze	lze	k6eNd1	lze
tepelně	tepelně	k6eAd1	tepelně
upravovat	upravovat	k5eAaImF	upravovat
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
kontrastu	kontrast	k1gInSc2	kontrast
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oficiální	oficiální	k2eAgInSc4d1	oficiální
drahokam	drahokam	k1gInSc4	drahokam
státu	stát	k1gInSc2	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
jediný	jediný	k2eAgInSc4d1	jediný
komerční	komerční	k2eAgInSc4d1	komerční
důl	důl	k1gInSc4	důl
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
těžbou	těžba	k1gFnSc7	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vůdčí	vůdčí	k2eAgInSc4d1	vůdčí
minerál	minerál	k1gInSc4	minerál
benitoitové	benitoitový	k2eAgFnSc2d1	benitoitový
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
také	také	k9	také
titanem	titan	k1gInSc7	titan
obohacený	obohacený	k2eAgInSc4d1	obohacený
analog	analog	k1gInSc4	analog
minerálu	minerál	k1gInSc2	minerál
pabstitu	pabstit	k1gInSc2	pabstit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Benitoit	benitoit	k1gInSc1	benitoit
je	být	k5eAaImIp3nS	být
hydrotermálního	hydrotermální	k2eAgInSc2d1	hydrotermální
původu	původ	k1gInSc2	původ
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
asociaci	asociace	k1gFnSc6	asociace
s	s	k7c7	s
natrolitem	natrolit	k1gInSc7	natrolit
<g/>
,	,	kIx,	,
neptunitem	neptunit	k1gInSc7	neptunit
a	a	k8xC	a
albitem	albit	k1gInSc7	albit
(	(	kIx(	(
<g/>
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obalen	obalen	k2eAgMnSc1d1	obalen
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
louhovat	louhovat	k5eAaImF	louhovat
v	v	k7c6	v
HCl	HCl	k1gFnSc6	HCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kontaktech	kontakt	k1gInPc6	kontakt
serpentinitových	serpentinitový	k2eAgNnPc2d1	serpentinitový
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
zelených	zelený	k2eAgFnPc2d1	zelená
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
6	[number]	k4	6
–	–	k?	–
6,5	[number]	k4	6,5
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
3,61	[number]	k4	3,61
-	-	kIx~	-
3,68	[number]	k4	3,68
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
nezřetelná	zřetelný	k2eNgFnSc1d1	nezřetelná
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
1011	[number]	k4	1011
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
lastrunatý	lastrunatý	k2eAgInSc1d1	lastrunatý
<g/>
.	.	kIx.	.
</s>
<s>
Září	zářit	k5eAaImIp3nS	zářit
sytě	sytě	k6eAd1	sytě
modře	modro	k6eAd1	modro
pod	pod	k7c7	pod
krátkými	krátký	k2eAgFnPc7d1	krátká
vlnami	vlna	k1gFnPc7	vlna
UV	UV	kA	UV
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
průhledný	průhledný	k2eAgInSc1d1	průhledný
i	i	k8xC	i
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Ba	ba	k9	ba
33,21	[number]	k4	33,21
%	%	kIx~	%
<g/>
,	,	kIx,	,
Ti	ten	k3xDgMnPc1	ten
11,58	[number]	k4	11,58
%	%	kIx~	%
<g/>
,	,	kIx,	,
Si	se	k3xPyFc3	se
20,38	[number]	k4	20,38
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
34,83	[number]	k4	34,83
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
HF	HF	kA	HF
a	a	k8xC	a
slabě	slabě	k6eAd1	slabě
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
HCl	HCl	k1gFnSc6	HCl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Brousí	brousit	k5eAaImIp3nS	brousit
se	se	k3xPyFc4	se
jako	jako	k9	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
drahokamy	drahokam	k1gInPc7	drahokam
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ceněn	cenit	k5eAaImNgMnS	cenit
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
šperk	šperk	k1gInSc1	šperk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
benitoit	benitoit	k1gInSc4	benitoit
větší	veliký	k2eAgFnSc2d2	veliký
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
1	[number]	k4	1
karát	karát	k1gInSc1	karát
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
raritou	rarita	k1gFnSc7	rarita
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
7,5	[number]	k4	7,5
karátový	karátový	k2eAgInSc1d1	karátový
vzorek	vzorek	k1gInSc1	vzorek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
ve	v	k7c4	v
Smithsonian	Smithsonian	k1gInSc4	Smithsonian
museum	museum	k1gNnSc1	museum
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
již	již	k6eAd1	již
vybroušeného	vybroušený	k2eAgInSc2d1	vybroušený
vzorku	vzorek	k1gInSc2	vzorek
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
3000	[number]	k4	3000
$	$	kIx~	$
až	až	k9	až
4000	[number]	k4	4000
$	$	kIx~	$
za	za	k7c4	za
karát	karát	k1gInSc4	karát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
San	San	k?	San
Benito	Benit	k2eAgNnSc4d1	Benito
County	Counta	k1gFnPc4	Counta
<g/>
,	,	kIx,	,
stát	stát	k1gInSc4	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
jediný	jediný	k2eAgInSc4d1	jediný
důl	důl	k1gInSc4	důl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vzorky	vzorek	k1gInPc1	vzorek
drahokamové	drahokamový	k2eAgFnSc2d1	drahokamová
kvality	kvalita	k1gFnSc2	kvalita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ohmi	Ohmi	k1gNnSc1	Ohmi
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
vzorky	vzorek	k1gInPc1	vzorek
menší	malý	k2eAgFnSc2d2	menší
kvality	kvalita	k1gFnSc2	kvalita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šebkovice	Šebkovice	k1gFnSc1	Šebkovice
<g/>
,	,	kIx,	,
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
peralkalická	peralkalický	k2eAgFnSc1d1	peralkalický
žíla	žíla	k1gFnSc1	žíla
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
hranice	hranice	k1gFnSc2	hranice
Moldanubika	Moldanubika	k1gFnSc1	Moldanubika
<g/>
,	,	kIx,	,
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c6	na
Ba	ba	k9	ba
<g/>
–	–	k?	–
<g/>
Ti	ten	k3xDgMnPc1	ten
<g/>
–	–	k?	–
<g/>
Zr	Zr	k1gFnSc1	Zr
prvky	prvek	k1gInPc4	prvek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
benitoit	benitoit	k1gInSc1	benitoit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
benitoit	benitoit	k1gInSc1	benitoit
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaImF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
benitoit	benitoit	k1gInSc1	benitoit
na	na	k7c4	na
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
benitoit	benitoit	k1gInSc1	benitoit
na	na	k7c6	na
webu	web	k1gInSc6	web
webmineral	webminerat	k5eAaPmAgMnS	webminerat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
benitoit	benitoit	k1gInSc1	benitoit
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
benitoit	benitoit	k1gInSc1	benitoit
v	v	k7c6	v
atlase	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
