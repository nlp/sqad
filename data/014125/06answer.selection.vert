<s desamb="1">
Od	od	k7c2
ostatních	ostatní	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
škol	škola	k1gFnPc2
myšlení	myšlení	k1gNnSc2
se	se	k3xPyFc4
odlišuje	odlišovat	k5eAaImIp3nS
zejména	zejména	k9
svou	svůj	k3xOyFgFnSc7
metodologií	metodologie	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
podstatou	podstata	k1gFnSc7
je	být	k5eAaImIp3nS
analýza	analýza	k1gFnSc1
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
(	(	kIx(
<g/>
praxeologie	praxeologie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
využívání	využívání	k1gNnSc1
deduktivní	deduktivní	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
<g/>
.	.	kIx.
</s>
