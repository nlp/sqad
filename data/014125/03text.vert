<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
směry	směr	k1gInPc1
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
(	(	kIx(
<g/>
ortodoxní	ortodoxní	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
</s>
<s>
Neokeynesiánství	Neokeynesiánství	k1gNnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
keynesiánská	keynesiánský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
</s>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
</s>
<s>
Heterodoxní	heterodoxní	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
</s>
<s>
Ekologická	ekologický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Feministická	feministický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Georgismus	Georgismus	k1gInSc1
</s>
<s>
Institucionální	institucionální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Termoekonomie	Termoekonomie	k1gFnSc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
institucionální	institucionální	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
A	a	k9
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
...	...	k?
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
škola	škola	k1gFnSc1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
vzniklá	vzniklý	k2eAgFnSc1d1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
dílech	díl	k1gInPc6
vídeňských	vídeňský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
Carla	Carl	k1gMnSc2
Mengera	Menger	k1gMnSc2
<g/>
,	,	kIx,
Friedricha	Friedrich	k1gMnSc2
von	von	k1gInSc4
Wiesera	Wiesero	k1gNnSc2
a	a	k8xC
Eugena	Eugena	k1gFnSc1
von	von	k1gInSc1
Böhm-Bawerka	Böhm-Bawerka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
říká	říkat	k5eAaImIp3nS
„	„	k?
<g/>
rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
dnes	dnes	k6eAd1
rakouští	rakouský	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
působí	působit	k5eAaImIp3nP
v	v	k7c6
mnoha	mnoho	k4c6
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
jiných	jiný	k2eAgFnPc2d1
vědeckých	vědecký	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
etiky	etika	k1gFnSc2
a	a	k8xC
práva	právo	k1gNnSc2
a	a	k8xC
představuje	představovat	k5eAaImIp3nS
tak	tak	k9
ve	v	k7c6
společenských	společenský	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
ucelený	ucelený	k2eAgInSc4d1
myšlenkový	myšlenkový	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
ostatních	ostatní	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
škol	škola	k1gFnPc2
myšlení	myšlení	k1gNnSc2
se	se	k3xPyFc4
odlišuje	odlišovat	k5eAaImIp3nS
zejména	zejména	k9
svou	svůj	k3xOyFgFnSc7
metodologií	metodologie	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
podstatou	podstata	k1gFnSc7
je	být	k5eAaImIp3nS
analýza	analýza	k1gFnSc1
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
(	(	kIx(
<g/>
praxeologie	praxeologie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
využívání	využívání	k1gNnSc1
deduktivní	deduktivní	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
skeptická	skeptický	k2eAgFnSc1d1
k	k	k7c3
využívání	využívání	k1gNnSc3
empirických	empirický	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
(	(	kIx(
<g/>
zejména	zejména	k9
statistickým	statistický	k2eAgFnPc3d1
metodám	metoda	k1gFnPc3
a	a	k8xC
laboratorním	laboratorní	k2eAgInPc3d1
experimentům	experiment	k1gInPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
někteří	některý	k3yIgMnPc1
její	její	k3xOp3gMnPc1
moderní	moderní	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
se	se	k3xPyFc4
jim	on	k3xPp3gInPc3
nebrání	bránit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
alternativním	alternativní	k2eAgInSc7d1
proudem	proud	k1gInSc7
k	k	k7c3
hlavnímu	hlavní	k2eAgInSc3d1
ekonomickému	ekonomický	k2eAgInSc3d1
proudu	proud	k1gInSc3
představovanému	představovaný	k2eAgInSc3d1
neoklasickou	neoklasický	k2eAgFnSc7d1
syntézou	syntéza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politicky	politicky	k6eAd1
se	se	k3xPyFc4
její	její	k3xOp3gMnPc1
představitelé	představitel	k1gMnPc1
většinou	většinou	k6eAd1
řadí	řadit	k5eAaImIp3nP
k	k	k7c3
klasickému	klasický	k2eAgInSc3d1
liberalismu	liberalismus	k1gInSc3
či	či	k8xC
libertarianismu	libertarianismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Čelní	čelní	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
Hayek	Hayek	k1gMnSc1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
Cenu	cena	k1gFnSc4
Švédské	švédský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
za	za	k7c4
rozvoj	rozvoj	k1gInSc4
ekonomické	ekonomický	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
na	na	k7c4
památku	památka	k1gFnSc4
Alfreda	Alfred	k1gMnSc2
Nobela	Nobel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
její	její	k3xOp3gMnSc1
zakladatel	zakladatel	k1gMnSc1
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
vydal	vydat	k5eAaPmAgMnS
průkopnické	průkopnický	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Základy	základ	k1gInPc4
národohospodářské	národohospodářský	k2eAgFnSc2d1
nauky	nauka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Spolu	spolu	k6eAd1
s	s	k7c7
W.	W.	kA
S.	S.	kA
Jevonsem	Jevons	k1gMnSc7
a	a	k8xC
Léonem	Léon	k1gMnSc7
Walrasem	Walras	k1gMnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
tvůrci	tvůrce	k1gMnPc1
marginalistické	marginalistický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
znamenala	znamenat	k5eAaImAgFnS
ukončení	ukončení	k1gNnSc4
dominance	dominance	k1gFnSc2
klasické	klasický	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
a	a	k8xC
změnu	změna	k1gFnSc4
paradigmatu	paradigma	k1gNnSc2
v	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
vědě	věda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Menger	Menger	k1gMnSc1
položil	položit	k5eAaPmAgMnS
základy	základ	k1gInPc4
teorie	teorie	k1gFnSc2
mezního	mezní	k2eAgInSc2d1
užitku	užitek	k1gInSc2
a	a	k8xC
na	na	k7c6
jejím	její	k3xOp3gInSc6
základě	základ	k1gInSc6
vysvětlil	vysvětlit	k5eAaPmAgMnS
další	další	k2eAgInPc4d1
ekonomické	ekonomický	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menger	Menger	k1gInSc1
do	do	k7c2
ekonomie	ekonomie	k1gFnSc2
především	především	k9
vrátil	vrátit	k5eAaPmAgMnS
ty	ten	k3xDgInPc4
ekonomické	ekonomický	k2eAgInPc4d1
principy	princip	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
důsledku	důsledek	k1gInSc6
nástupu	nástup	k1gInSc2
Adama	Adam	k1gMnSc2
Smithe	Smith	k1gFnSc2
zapomenuty	zapomnět	k5eAaImNgInP,k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
především	především	k6eAd1
o	o	k7c4
subjektivní	subjektivní	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
zastávána	zastáván	k2eAgFnSc1d1
středověkými	středověký	k2eAgMnPc7d1
scholastiky	scholastik	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračovateli	pokračovatel	k1gMnPc7
Carla	Carl	k1gMnSc2
Mengera	Menger	k1gMnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
stali	stát	k5eAaPmAgMnP
současníci	současník	k1gMnPc1
Eugen	Eugen	k2eAgInSc4d1
von	von	k1gInSc4
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Friedrich	Friedrich	k1gMnSc1
von	von	k1gInSc4
Wieser	Wieser	k1gInSc1
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
představitelé	představitel	k1gMnPc1
druhé	druhý	k4xOgFnPc4
generace	generace	k1gFnPc4
rakouských	rakouský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
toto	tento	k3xDgNnSc4
období	období	k1gNnSc4
je	být	k5eAaImIp3nS
typické	typický	k2eAgNnSc4d1
rozvíjení	rozvíjení	k1gNnSc4
dílčích	dílčí	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
především	především	k9
rozvíjení	rozvíjení	k1gNnSc4
teorie	teorie	k1gFnSc2
kapitálu	kapitál	k1gInSc2
a	a	k8xC
úroku	úrok	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Wieser	Wieser	k1gMnSc1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
širší	široký	k2eAgFnSc3d2
problematice	problematika	k1gFnSc3
fungování	fungování	k1gNnSc2
tržního	tržní	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
vypracoval	vypracovat	k5eAaPmAgInS
vlastní	vlastní	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
teorii	teorie	k1gFnSc4
imputace	imputace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
období	období	k1gNnSc1
prvních	první	k4xOgMnPc2
představitelů	představitel	k1gMnPc2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
bylo	být	k5eAaImAgNnS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
provázeno	provázet	k5eAaImNgNnS
sporem	spor	k1gInSc7
o	o	k7c4
metodu	metoda	k1gFnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
Methodenstreit	Methodenstreit	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
Německou	německý	k2eAgFnSc7d1
historickou	historický	k2eAgFnSc7d1
školou	škola	k1gFnSc7
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
dominantní	dominantní	k2eAgMnSc1d1
v	v	k7c6
německy	německy	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
prosazovala	prosazovat	k5eAaImAgFnS
popisnou	popisný	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
bez	bez	k7c2
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
toto	tento	k3xDgNnSc4
první	první	k4xOgNnSc4
období	období	k1gNnSc4
navázali	navázat	k5eAaPmAgMnP
pravděpodobně	pravděpodobně	k6eAd1
nejznámější	známý	k2eAgMnPc1d3
představitelé	představitel	k1gMnPc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
žák	žák	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k1gInSc1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
třetí	třetí	k4xOgFnSc4
generaci	generace	k1gFnSc4
rakouských	rakouský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc4
Mises	Mises	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
nejslavnějším	slavný	k2eAgInSc7d3
dílem	díl	k1gInSc7
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
(	(	kIx(
<g/>
Human	Human	k1gInSc1
Action	Action	k1gInSc1
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
)	)	kIx)
komplexní	komplexní	k2eAgNnSc1d1
pojednání	pojednání	k1gNnSc1
o	o	k7c4
ekonomii	ekonomie	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
krok	krok	k1gInSc1
za	za	k7c7
krokem	krok	k1gInSc7
vybudoval	vybudovat	k5eAaPmAgMnS
jednu	jeden	k4xCgFnSc4
teorii	teorie	k1gFnSc4
za	za	k7c7
druhou	druhý	k4xOgFnSc7
na	na	k7c6
základě	základ	k1gInSc6
ucelené	ucelený	k2eAgFnSc2d1
metodologie	metodologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
pojednáním	pojednání	k1gNnSc7
Mises	Mises	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
tzv.	tzv.	kA
praxeologii	praxeologie	k1gFnSc4
neboli	neboli	k8xC
vědu	věda	k1gFnSc4
o	o	k7c6
lidském	lidský	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
ekonomie	ekonomie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
tak	tak	k9
je	být	k5eAaImIp3nS
ojedinělé	ojedinělý	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
v	v	k7c6
historii	historie	k1gFnSc6
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Misesovým	Misesový	k2eAgInSc7d1
nejčastějším	častý	k2eAgInSc7d3
objektem	objekt	k1gInSc7
zájmu	zájem	k1gInSc2
byla	být	k5eAaImAgFnS
teorie	teorie	k1gFnSc1
peněz	peníze	k1gInPc2
a	a	k8xC
teorie	teorie	k1gFnSc2
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k1gMnSc1
je	být	k5eAaImIp3nS
ekonom	ekonom	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
od	od	k7c2
praxeologie	praxeologie	k1gFnSc2
odchýlil	odchýlit	k5eAaPmAgInS
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgInS
tak	tak	k6eAd1
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
alternativní	alternativní	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
výrazným	výrazný	k2eAgFnPc3d1
odlišnostem	odlišnost	k1gFnPc3
v	v	k7c6
metodologii	metodologie	k1gFnSc6
tak	tak	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
dvě	dva	k4xCgFnPc4
myšlenkové	myšlenkový	k2eAgFnPc4d1
linie	linie	k1gFnPc4
<g/>
:	:	kIx,
misesovská	misesovský	k2eAgNnPc4d1
a	a	k8xC
hayekovská	hayekovský	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
stál	stát	k5eAaImAgInS
Hayek	Hayek	k6eAd1
po	po	k7c6
boku	bok	k1gInSc6
Misese	Misese	k1gFnSc2
v	v	k7c6
tzv.	tzv.	kA
sporu	spora	k1gFnSc4
o	o	k7c4
ekonomickou	ekonomický	k2eAgFnSc4d1
racionalitu	racionalita	k1gFnSc4
socialismu	socialismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
jakýmsi	jakýsi	k3yIgNnSc7
pokračováním	pokračování	k1gNnSc7
sporu	spor	k1gInSc2
o	o	k7c4
metodu	metoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
svém	svůj	k3xOyFgNnSc6
působení	působení	k1gNnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
však	však	k9
poznává	poznávat	k5eAaImIp3nS
filosofa	filosof	k1gMnSc2
Karla	Karel	k1gMnSc2
Raimunda	Raimunda	k1gFnSc1
Poppera	Poppera	k1gFnSc1
a	a	k8xC
opouští	opouštět	k5eAaImIp3nS
metodu	metoda	k1gFnSc4
praxeologie	praxeologie	k1gFnSc2
a	a	k8xC
rozvíjí	rozvíjet	k5eAaImIp3nS
popperovskou	popperovský	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
falzifikace	falzifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
zájem	zájem	k1gInSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
obrací	obracet	k5eAaImIp3nS
spíše	spíše	k9
na	na	k7c4
celospolečenské	celospolečenský	k2eAgInPc4d1
problémy	problém	k1gInPc4
než	než	k8xS
na	na	k7c4
užší	úzký	k2eAgFnSc4d2
problematiku	problematika	k1gFnSc4
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
proto	proto	k8xC
rozlišují	rozlišovat	k5eAaImIp3nP
Misesovskou	Misesovský	k2eAgFnSc4d1
a	a	k8xC
Hayekovskou	Hayekovský	k2eAgFnSc4d1
větev	větev	k1gFnSc4
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátek	počátek	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
přinutil	přinutit	k5eAaPmAgInS
oba	dva	k4xCgMnPc4
ekonomy	ekonom	k1gMnPc4
emigrovat	emigrovat	k5eAaBmF
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
začíná	začínat	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
etapa	etapa	k1gFnSc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
centrum	centrum	k1gNnSc1
se	se	k3xPyFc4
přesouvá	přesouvat	k5eAaImIp3nS
do	do	k7c2
USA	USA	kA
a	a	k8xC
nastává	nastávat	k5eAaImIp3nS
téměř	téměř	k6eAd1
30	#num#	k4
let	léto	k1gNnPc2
izolace	izolace	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
rakouská	rakouský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
na	na	k7c6
okraji	okraj	k1gInSc6
zájmu	zájem	k1gInSc2
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Naději	naděje	k1gFnSc4
na	na	k7c4
obnovu	obnova	k1gFnSc4
představovaly	představovat	k5eAaImAgFnP
nové	nový	k2eAgFnPc1d1
generace	generace	k1gFnPc1
rakouských	rakouský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Murrayem	Murray	k1gMnSc7
N.	N.	kA
Rothbardem	Rothbard	k1gMnSc7
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Israelem	Israel	k1gMnSc7
Kirznerem	Kirzner	k1gMnSc7
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
Misesovými	Misesový	k2eAgMnPc7d1
studenty	student	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbard	Rothbard	k1gMnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
stal	stát	k5eAaPmAgInS
přímým	přímý	k2eAgMnSc7d1
pokračovatelem	pokračovatel	k1gMnSc7
Misese	Misese	k1gFnSc2
a	a	k8xC
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
nejen	nejen	k6eAd1
ekonomii	ekonomie	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
filosofii	filosofie	k1gFnSc3
<g/>
,	,	kIx,
historii	historie	k1gFnSc6
a	a	k8xC
právu	právo	k1gNnSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vytvořil	vytvořit	k5eAaPmAgMnS
velmi	velmi	k6eAd1
široké	široký	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
ucelené	ucelený	k2eAgNnSc4d1
pojednání	pojednání	k1gNnSc4
o	o	k7c4
ekonomii	ekonomie	k1gFnSc4
Zásady	zásada	k1gFnSc2
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Man	mana	k1gFnPc2
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
později	pozdě	k6eAd2
doplnil	doplnit	k5eAaPmAgMnS
o	o	k7c4
analýzu	analýza	k1gFnSc4
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
Ekonomie	ekonomie	k1gFnSc2
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Power	Power	k1gInSc1
and	and	k?
Market	market	k1gInSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Israel	Israel	k1gMnSc1
Krizner	Krizner	k1gMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
syntézu	syntéza	k1gFnSc4
Misesových	Misesový	k2eAgFnPc2d1
a	a	k8xC
Hayekových	Hayekův	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
teorii	teorie	k1gFnSc4
podnikatelského	podnikatelský	k2eAgNnSc2d1
objevování	objevování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
spíše	spíše	k9
pokračovatelem	pokračovatel	k1gMnSc7
hayekovské	hayekovský	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rothbard	Rothbarda	k1gFnPc2
i	i	k8xC
Kirzner	Kirznra	k1gFnPc2
zažili	zažít	k5eAaPmAgMnP
obrat	obrat	k1gInSc4
ve	v	k7c6
vnímání	vnímání	k1gNnSc6
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
dostal	dostat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
F.	F.	kA
A.	A.	kA
Hayek	Hayek	k6eAd1
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
průkopnickou	průkopnický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
teorie	teorie	k1gFnPc1
peněz	peníze	k1gInPc2
a	a	k8xC
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
jako	jako	k9
za	za	k7c4
analýzu	analýza	k1gFnSc4
vzájemného	vzájemný	k2eAgNnSc2d1
působení	působení	k1gNnSc2
ekonomických	ekonomický	k2eAgMnPc2d1
<g/>
,	,	kIx,
společenských	společenský	k2eAgInPc2d1
a	a	k8xC
institucionálních	institucionální	k2eAgInPc2d1
jevů	jev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
však	však	k9
přišla	přijít	k5eAaPmAgFnS
rok	rok	k1gInSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
původního	původní	k2eAgMnSc2d1
autora	autor	k1gMnSc2
rakouské	rakouský	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
,	,	kIx,
Ludwiga	Ludwiga	k1gFnSc1
von	von	k1gInSc1
Misese	Misese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pátou	pátý	k4xOgFnSc4
generaci	generace	k1gFnSc4
„	„	k?
<g/>
rakouských	rakouský	k2eAgInPc2d1
<g/>
“	“	k?
ekonomů	ekonom	k1gMnPc2
tak	tak	k8xC,k8xS
tvoří	tvořit	k5eAaImIp3nP
především	především	k9
ekonomové	ekonom	k1gMnPc1
působící	působící	k2eAgMnPc1d1
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
nejvýraznější	výrazný	k2eAgFnPc4d3
osobnosti	osobnost	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
tak	tak	k9
jako	jako	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
ekonomických	ekonomický	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
i	i	k8xC
zde	zde	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
roztříštění	roztříštění	k1gNnSc3
zájmu	zájem	k1gInSc2
jednotlivých	jednotlivý	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
na	na	k7c4
partikulární	partikulární	k2eAgInPc4d1
ekonomické	ekonomický	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
čelné	čelný	k2eAgMnPc4d1
představitele	představitel	k1gMnPc4
současné	současný	k2eAgFnSc2d1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Hans	Hans	k1gMnSc1
Hermann	Hermann	k1gMnSc1
Hoppe	Hopp	k1gInSc5
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
především	především	k6eAd1
rozvoji	rozvoj	k1gInSc3
metodologie	metodologie	k1gFnSc2
<g/>
,	,	kIx,
etiky	etika	k1gFnSc2
a	a	k8xC
libertarianismu	libertarianismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickou	charakteristický	k2eAgFnSc4d1
metodologii	metodologie	k1gFnSc4
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
rozvíjejí	rozvíjet	k5eAaImIp3nP
i	i	k9
Roderick	Roderick	k1gMnSc1
T.	T.	kA
Long	Long	k1gMnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Barry	Barr	k1gInPc1
Smith	Smith	k1gInSc1
a	a	k8xC
David	David	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
spolu	spolu	k6eAd1
s	s	k7c7
Hoppem	Hopp	k1gInSc7
tvoří	tvořit	k5eAaImIp3nS
„	„	k?
<g/>
kvarteto	kvarteto	k1gNnSc1
<g/>
“	“	k?
současných	současný	k2eAgMnPc2d1
metodologických	metodologický	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
Block	Block	k1gMnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
základními	základní	k2eAgFnPc7d1
ekonomickými	ekonomický	k2eAgFnPc7d1
teoriemi	teorie	k1gFnPc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
jako	jako	k9
konceptem	koncept	k1gInSc7
vlastnictví	vlastnictví	k1gNnSc2
a	a	k8xC
libertarianismem	libertarianismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roger	Roger	k1gMnSc1
Garrison	Garrison	k1gMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vybudovat	vybudovat	k5eAaPmF
„	„	k?
<g/>
rakouskou	rakouský	k2eAgFnSc4d1
makroekonomii	makroekonomie	k1gFnSc4
<g/>
“	“	k?
na	na	k7c6
čistě	čistě	k6eAd1
mikroekonomických	mikroekonomický	k2eAgInPc6d1
základech	základ	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joseph	Joseph	k1gInSc1
Salerno	Salerna	k1gFnSc5
následuje	následovat	k5eAaImIp3nS
Misese	Misese	k1gFnPc1
ve	v	k7c6
zkoumání	zkoumání	k1gNnSc6
společenských	společenský	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
teorie	teorie	k1gFnSc1
peněz	peníze	k1gInPc2
a	a	k8xC
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ralph	Ralph	k1gMnSc1
Raico	Raico	k1gMnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
zejména	zejména	k9
ekonomickou	ekonomický	k2eAgFnSc7d1
historií	historie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
opětovnému	opětovný	k2eAgNnSc3d1
obnovení	obnovení	k1gNnSc3
rakouských	rakouský	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
a	a	k8xC
nejvýznamnějším	významný	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
Jörg	Jörg	k1gInSc1
Guido	Guido	k1gNnSc1
Hülsmann	Hülsmann	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
širokému	široký	k2eAgNnSc3d1
spektru	spektrum	k1gNnSc3
ekonomických	ekonomický	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jesús	Jesúsa	k1gFnPc2
Huerta	Huerta	k1gFnSc1
de	de	k?
Soto	Soto	k1gMnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
zejména	zejména	k9
teorií	teorie	k1gFnSc7
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
a	a	k8xC
Pascal	pascal	k1gInSc1
Salin	salina	k1gFnPc2
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
boji	boj	k1gInSc3
proti	proti	k7c3
údajným	údajný	k2eAgNnPc3d1
soudobým	soudobý	k2eAgNnPc3d1
makroekonomickým	makroekonomický	k2eAgNnPc3d1
sofizmatům	sofizma	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
šíření	šíření	k1gNnSc3
myšlenek	myšlenka	k1gFnPc2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohá	mnohé	k1gNnPc1
děl	dělo	k1gNnPc2
autorů	autor	k1gMnPc2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
přeložil	přeložit	k5eAaPmAgInS
a	a	k8xC
vydal	vydat	k5eAaPmAgInS
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
český	český	k2eAgMnSc1d1
think	think	k1gMnSc1
tank	tank	k1gInSc4
zabývající	zabývající	k2eAgInSc4d1
se	s	k7c7
šířením	šíření	k1gNnSc7
liberálních	liberální	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zde	zde	k6eAd1
působí	působit	k5eAaImIp3nP
Ludwig	Ludwig	k1gInSc4
von	von	k1gInSc1
Mises	Misesa	k1gFnPc2
Institut	institut	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vzdělávání	vzdělávání	k1gNnSc1
české	český	k2eAgFnSc2d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
v	v	k7c6
otázkách	otázka	k1gFnPc6
ekonomie	ekonomie	k1gFnSc2
z	z	k7c2
pohledu	pohled	k1gInSc2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelným	čelný	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Šíma	Šíma	k1gMnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
integrací	integrace	k1gFnSc7
ekonomie	ekonomie	k1gFnPc1
a	a	k8xC
práva	právo	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Metodologie	metodologie	k1gFnSc1
</s>
<s>
Apriorismus	apriorismus	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Apriorismus	apriorismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zásadním	zásadní	k2eAgNnSc7d1
epistemologickým	epistemologický	k2eAgNnSc7d1
východiskem	východisko	k1gNnSc7
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
je	být	k5eAaImIp3nS
apriorismus	apriorismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
komplexním	komplexní	k2eAgNnSc7d1
vysvětlením	vysvětlení	k1gNnSc7
této	tento	k3xDgFnSc2
epistemologie	epistemologie	k1gFnSc2
přišel	přijít	k5eAaPmAgInS
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Human	Human	k1gMnSc1
Action	Action	k1gInSc1
(	(	kIx(
<g/>
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomie	ekonomie	k1gFnSc1
v	v	k7c6
pojetí	pojetí	k1gNnSc6
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
není	být	k5eNaImIp3nS
empirickou	empirický	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
vědou	věda	k1gFnSc7
apriorní	apriorní	k2eAgFnSc7d1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
počátek	počátek	k1gInSc4
u	u	k7c2
konceptu	koncept	k1gInSc2
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
koncept	koncept	k1gInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
jako	jako	k9
axiom	axiom	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
nevyvratitelný	vyvratitelný	k2eNgInSc4d1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
z	z	k7c2
něho	on	k3xPp3gMnSc2
lze	lze	k6eAd1
logickou	logický	k2eAgFnSc7d1
dedukcí	dedukce	k1gFnSc7
a	a	k8xC
také	také	k9
s	s	k7c7
pomocí	pomoc	k1gFnSc7
podpůrných	podpůrný	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
odvodit	odvodit	k5eAaPmF
celou	celý	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
„	„	k?
<g/>
jednoho	jeden	k4xCgInSc2
nevyvratitelného	vyvratitelný	k2eNgInSc2d1
axiomu	axiom	k1gInSc2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
zajištěna	zajištěn	k2eAgFnSc1d1
„	„	k?
<g/>
apriorní	apriorní	k2eAgFnSc1d1
platnost	platnost	k1gFnSc1
jejích	její	k3xOp3gFnPc2
tezí	teze	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
tak	tak	k6eAd1
mluvíme	mluvit	k5eAaImIp1nP
o	o	k7c6
axiomaticko-deduktivní	axiomaticko-deduktivní	k2eAgFnSc6d1
metodě	metoda	k1gFnSc6
rakouské	rakouský	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomické	ekonomický	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
empiricky	empiricky	k6eAd1
testovatelné	testovatelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
verifikovatelné	verifikovatelný	k2eAgNnSc1d1
či	či	k8xC
falzifikovatelné	falzifikovatelný	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomické	ekonomický	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
této	tento	k3xDgFnSc2
školy	škola	k1gFnSc2
lze	lze	k6eAd1
vyvrátit	vyvrátit	k5eAaPmF
opět	opět	k6eAd1
jen	jen	k9
deduktivní	deduktivní	k2eAgFnSc7d1
logikou	logika	k1gFnSc7
<g/>
,	,	kIx,
nikoli	nikoli	k9
odvoláním	odvolání	k1gNnSc7
se	se	k3xPyFc4
na	na	k7c4
fakta	faktum	k1gNnPc4
(	(	kIx(
<g/>
zároveň	zároveň	k6eAd1
ale	ale	k8xC
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nejsou	být	k5eNaImIp3nP
naplněny	naplněn	k2eAgInPc1d1
její	její	k3xOp3gInPc1
výchozí	výchozí	k2eAgInPc1d1
axiomy	axiom	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
buď	buď	k8xC
najít	najít	k5eAaPmF
chybu	chyba	k1gFnSc4
ve	v	k7c6
sledu	sled	k1gInSc6
logických	logický	k2eAgInPc2d1
kroků	krok	k1gInPc2
a	a	k8xC
nebo	nebo	k8xC
omyl	omyl	k1gInSc1
v	v	k7c6
pomocné	pomocný	k2eAgFnSc6d1
konstrukci	konstrukce	k1gFnSc6
(	(	kIx(
<g/>
pomocné	pomocný	k2eAgInPc1d1
axiomy	axiom	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
platnost	platnost	k1gFnSc4
jen	jen	k9
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Apriorní	apriorní	k2eAgFnSc1d1
povaha	povaha	k1gFnSc1
ekonomie	ekonomie	k1gFnSc2
podle	podle	k7c2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
zároveň	zároveň	k6eAd1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc1
teorie	teorie	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
v	v	k7c6
situacích	situace	k1gFnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
není	být	k5eNaImIp3nS
splněn	splnit	k5eAaPmNgInS
její	její	k3xOp3gInSc1
výchozí	výchozí	k2eAgInSc1d1
axiom	axiom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Metodologický	metodologický	k2eAgInSc1d1
subjektivismus	subjektivismus	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Metodologický	metodologický	k2eAgInSc4d1
subjektivismus	subjektivismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
přístup	přístup	k1gInSc1
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgInS
do	do	k7c2
ekonomie	ekonomie	k1gFnSc2
po	po	k7c6
marginalistické	marginalistický	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
a	a	k8xC
rakouská	rakouský	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c4
dodržování	dodržování	k1gNnSc4
tohoto	tento	k3xDgInSc2
principu	princip	k1gInSc2
velmi	velmi	k6eAd1
konzistentní	konzistentní	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
je	být	k5eAaImIp3nS
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
subjektivní	subjektivní	k2eAgFnSc6d1
hodnotové	hodnotový	k2eAgFnSc6d1
škále	škála	k1gFnSc6
každého	každý	k3xTgMnSc2
jednotlivce	jednotlivec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevení	projevení	k1gNnSc1
subjektivní	subjektivní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
jednotlivců	jednotlivec	k1gMnPc2
je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
oceňuje	oceňovat	k5eAaImIp3nS
jednotlivé	jednotlivý	k2eAgInPc4d1
statky	statek	k1gInPc4
a	a	k8xC
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Objektivní	objektivní	k2eAgFnSc1d1
<g/>
“	“	k?
ohodnocení	ohodnocení	k1gNnSc2
cen	cena	k1gFnPc2
statků	statek	k1gInPc2
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Subjektivismus	subjektivismus	k1gInSc1
tak	tak	k6eAd1
v	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
vědě	věda	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
objektivní	objektivní	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
byla	být	k5eAaImAgFnS
hodnota	hodnota	k1gFnSc1
statků	statek	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
dána	dán	k2eAgFnSc1d1
jejich	jejich	k3xOp3gInPc7
náklady	náklad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
podobě	podoba	k1gFnSc6
pracovní	pracovní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
hodnoty	hodnota	k1gFnSc2
stála	stát	k5eAaImAgFnS
v	v	k7c6
pozadí	pozadí	k1gNnSc6
klasické	klasický	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princip	princip	k1gInSc1
subjektivismu	subjektivismus	k1gInSc2
pak	pak	k6eAd1
rozvinul	rozvinout	k5eAaPmAgInS
do	do	k7c2
konzistentní	konzistentní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
demonstrovaných	demonstrovaný	k2eAgFnPc2d1
preferencí	preference	k1gFnPc2
Ludwig	Ludwiga	k1gFnPc2
von	von	k1gInSc1
Mises	Mises	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
Murray	Murray	k1gInPc1
Rothbard	Rothbarda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahlížení	nahlížení	k1gNnSc1
na	na	k7c4
hodnotu	hodnota	k1gFnSc4
jako	jako	k8xS,k8xC
na	na	k7c4
subjektivní	subjektivní	k2eAgFnSc4d1
veličinu	veličina	k1gFnSc4
zároveň	zároveň	k6eAd1
znamená	znamenat	k5eAaImIp3nS
nemožnost	nemožnost	k1gFnSc1
matematizace	matematizace	k1gFnSc2
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
této	tento	k3xDgFnSc2
subjektivní	subjektivní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
nelze	lze	k6eNd1
přiřadit	přiřadit	k5eAaPmF
žádným	žádný	k3yNgInPc3
ekonomickým	ekonomický	k2eAgInPc3d1
vztahům	vztah	k1gInPc3
numerickou	numerický	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nicméně	nicméně	k8xC
neznamená	znamenat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ekonomie	ekonomie	k1gFnSc1
není	být	k5eNaImIp3nS
exaktní	exaktní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
na	na	k7c6
matematice	matematika	k1gFnSc6
exaktní	exaktní	k2eAgFnSc6d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
se	se	k3xPyFc4
aplikuje	aplikovat	k5eAaBmIp3nS
(	(	kIx(
<g/>
tedy	tedy	k8xC
konstantní	konstantní	k2eAgFnSc1d1
veličiny	veličina	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ne	ne	k9
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Metodologický	metodologický	k2eAgInSc1d1
individualismus	individualismus	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Metodologický	metodologický	k2eAgInSc4d1
individualismus	individualismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
podle	podle	k7c2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
není	být	k5eNaImIp3nS
vědou	věda	k1gFnSc7
o	o	k7c6
alokaci	alokace	k1gFnSc6
vzácných	vzácný	k2eAgInPc2d1
výrobních	výrobní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
či	či	k8xC
jakékoli	jakýkoli	k3yIgFnSc3
jiné	jiný	k2eAgFnSc3d1
alokaci	alokace	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
vědou	věda	k1gFnSc7
o	o	k7c6
lidském	lidský	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
jednotlivec	jednotlivec	k1gMnSc1
může	moct	k5eAaImIp3nS
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
středem	střed	k1gInSc7
zájmu	zájem	k1gInSc2
rakouské	rakouský	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
jednotlivec	jednotlivec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rakouská	rakouský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
dokázala	dokázat	k5eAaPmAgFnS
na	na	k7c6
základě	základ	k1gInSc6
tohoto	tento	k3xDgInSc2
principu	princip	k1gInSc2
vyvinout	vyvinout	k5eAaPmF
komplexní	komplexní	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nepřebírá	přebírat	k5eNaImIp3nS
dichotomii	dichotomie	k1gFnSc4
ekonomie	ekonomie	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
mikroekonomii	mikroekonomie	k1gFnSc4
a	a	k8xC
makroekonomii	makroekonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
také	také	k9
zdůraznit	zdůraznit	k5eAaPmF
koncept	koncept	k1gInSc4
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
založený	založený	k2eAgInSc4d1
individualismus	individualismus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
příkrém	příkrý	k2eAgInSc6d1
rozporu	rozpor	k1gInSc6
s	s	k7c7
mechanistickým	mechanistický	k2eAgInSc7d1
individualismem	individualismus	k1gInSc7
ekonomie	ekonomie	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
spočívá	spočívat	k5eAaImIp3nS
na	na	k7c6
konceptu	koncept	k1gInSc6
homo	homo	k1gMnSc1
oeconomicus	oeconomicus	k1gMnSc1
<g/>
,	,	kIx,
racionálního	racionální	k2eAgInSc2d1
stroje	stroj	k1gInSc2
alokujícího	alokující	k2eAgInSc2d1
své	svůj	k3xOyFgInPc4
zdroje	zdroj	k1gInPc4
mezi	mezi	k7c4
alternativní	alternativní	k2eAgFnPc4d1
potřeby	potřeba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
</s>
<s>
Teorie	teorie	k1gFnSc1
hodnoty	hodnota	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Teorie	teorie	k1gFnSc2
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rakušané	Rakušan	k1gMnPc1
do	do	k7c2
ekonomie	ekonomie	k1gFnSc2
vnesli	vnést	k5eAaPmAgMnP
subjektivní	subjektivní	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
mezního	mezní	k2eAgInSc2d1
užitku	užitek	k1gInSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
teorii	teorie	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
pozadí	pozadí	k1gNnSc6
všech	všecek	k3xTgFnPc2
ekonomických	ekonomický	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
už	už	k6eAd1
dávno	dávno	k6eAd1
na	na	k7c6
teorii	teorie	k1gFnSc6
hodnoty	hodnota	k1gFnSc2
nestojí	stát	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
východiskem	východisko	k1gNnSc7
je	být	k5eAaImIp3nS
teorie	teorie	k1gFnSc1
chování	chování	k1gNnSc4
spotřebitele	spotřebitel	k1gMnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
analytickým	analytický	k2eAgInSc7d1
modelem	model	k1gInSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
indiferenční	indiferenční	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
však	však	k9
nadále	nadále	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
u	u	k7c2
subjektivní	subjektivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
užitku	užitek	k1gInSc2
a	a	k8xC
jejím	její	k3xOp3gInSc7
teoretickým	teoretický	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
je	být	k5eAaImIp3nS
teorie	teorie	k1gFnSc1
demonstrovaných	demonstrovaný	k2eAgFnPc2d1
preferencí	preference	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
indiferenčních	indiferenční	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
podle	podle	k7c2
představitelů	představitel	k1gMnPc2
Rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
zásadně	zásadně	k6eAd1
popírá	popírat	k5eAaImIp3nS
kategorie	kategorie	k1gFnPc4
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
samotném	samotný	k2eAgInSc6d1
rozporu	rozpor	k1gInSc6
s	s	k7c7
konceptem	koncept	k1gInSc7
volby	volba	k1gFnSc2
jako	jako	k8xS,k8xC
takové	takový	k3xDgNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hodnoty	hodnota	k1gFnSc2
je	být	k5eAaImIp3nS
přísně	přísně	k6eAd1
ordinalistická	ordinalistický	k2eAgFnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
metodologickým	metodologický	k2eAgInSc7d1
subjektivismem	subjektivismus	k1gInSc7
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
nelze	lze	k6eNd1
hodnotám	hodnota	k1gFnPc3
přiřazovat	přiřazovat	k5eAaImF
numerické	numerický	k2eAgFnPc4d1
veličiny	veličina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
nákladů	náklad	k1gInPc2
obětované	obětovaný	k2eAgFnSc2d1
příležitosti	příležitost	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Náklady	náklad	k1gInPc1
obětované	obětovaný	k2eAgFnSc2d1
příležitosti	příležitost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tuto	tento	k3xDgFnSc4
teorii	teorie	k1gFnSc4
vnesl	vnést	k5eAaPmAgMnS
do	do	k7c2
ekonomie	ekonomie	k1gFnSc2
Friedrich	Friedrich	k1gMnSc1
von	von	k1gInSc1
Wieser	Wiesero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc7
obětované	obětovaný	k2eAgFnSc2d1
příležitosti	příležitost	k1gFnSc2
jsou	být	k5eAaImIp3nP
ztrátou	ztráta	k1gFnSc7
mezní	mezní	k2eAgFnSc2d1
užitečnosti	užitečnost	k1gFnSc2
statků	statek	k1gInPc2
či	či	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
člověk	člověk	k1gMnSc1
přišel	přijít	k5eAaPmAgMnS
z	z	k7c2
důvodu	důvod	k1gInSc2
volby	volba	k1gFnSc2
alternativního	alternativní	k2eAgNnSc2d1
využití	využití	k1gNnSc2
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Zkrátka	zkrátka	k6eAd1
<g/>
,	,	kIx,
obětovaná	obětovaný	k2eAgFnSc1d1
příležitost	příležitost	k1gFnSc1
je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc7
volbou	volba	k1gFnSc7
na	na	k7c6
škále	škála	k1gFnSc6
preferencí	preference	k1gFnPc2
jednotlivého	jednotlivý	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
však	však	k9
člověk	člověk	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
pro	pro	k7c4
něco	něco	k3yInSc4
jiného	jiný	k2eAgMnSc4d1
<g/>
,	,	kIx,
tedy	tedy	k9
preferoval	preferovat	k5eAaImAgInS
jinou	jiný	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
tato	tento	k3xDgFnSc1
druhá	druhý	k4xOgFnSc1
„	„	k?
<g/>
volba	volba	k1gFnSc1
<g/>
“	“	k?
realizována	realizovat	k5eAaBmNgFnS
a	a	k8xC
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
„	„	k?
<g/>
obětována	obětován	k2eAgMnSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
obětované	obětovaný	k2eAgInPc1d1
příležitosti	příležitost	k1gFnPc1
završily	završit	k5eAaPmAgFnP
konec	konec	k1gInSc4
objektivní	objektivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
jsou	být	k5eAaImIp3nP
právě	právě	k9
náklady	náklad	k1gInPc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
uděluje	udělovat	k5eAaImIp3nS
statkům	statek	k1gInPc3
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
obětovaná	obětovaný	k2eAgFnSc1d1
užitečnost	užitečnost	k1gFnSc1
nevyrobených	vyrobený	k2eNgInPc2d1
statků	statek	k1gInPc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
určuje	určovat	k5eAaImIp3nS
náklady	náklad	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
samotný	samotný	k2eAgInSc1d1
proces	proces	k1gInSc1
rozhodování	rozhodování	k1gNnSc2
jednotlivců	jednotlivec	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
preferenční	preferenční	k2eAgFnSc2d1
škály	škála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
jsou	být	k5eAaImIp3nP
tedy	tedy	k8xC
druhou	druhý	k4xOgFnSc7
stranou	strana	k1gFnSc7
mince	mince	k1gFnSc2
hodnotě	hodnota	k1gFnSc6
statků	statek	k1gInPc2
<g/>
,	,	kIx,
užitečností	užitečnost	k1gFnSc7
<g/>
,	,	kIx,
obětovanou	obětovaný	k2eAgFnSc7d1
a	a	k8xC
získanou	získaný	k2eAgFnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
kapitálu	kapitál	k1gInSc2
a	a	k8xC
úroku	úrok	k1gInSc2
</s>
<s>
Informace	informace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
poskytuje	poskytovat	k5eAaImIp3nS
úroková	úrokový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
podle	podle	k7c2
rakouských	rakouský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
v	v	k7c6
prostředí	prostředí	k1gNnSc6
peněz	peníze	k1gInPc2
bez	bez	k7c2
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
</s>
<s>
Kapitál	kapitál	k1gInSc1
představuje	představovat	k5eAaImIp3nS
v	v	k7c6
pojetí	pojetí	k1gNnSc6
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
statky	statek	k1gInPc1
vyšších	vysoký	k2eAgInPc2d2
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
statky	statek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
nejsou	být	k5eNaImIp3nP
určeny	určen	k2eAgInPc1d1
k	k	k7c3
okamžité	okamžitý	k2eAgFnSc3d1
spotřebě	spotřeba	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
dále	daleko	k6eAd2
ve	v	k7c6
výrobě	výroba	k1gFnSc6
jako	jako	k9
vstupy	vstup	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
statky	statek	k1gInPc1
odvozují	odvozovat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
hodnotu	hodnota	k1gFnSc4
od	od	k7c2
statků	statek	k1gInPc2
prvního	první	k4xOgInSc2
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
finálních	finální	k2eAgInPc2d1
statků	statek	k1gInPc2
metodou	metoda	k1gFnSc7
imputace	imputace	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
tvoří	tvořit	k5eAaImIp3nS
rakouskou	rakouský	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
rozdělování	rozdělování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
kapitálu	kapitál	k1gInSc2
vnesla	vnést	k5eAaPmAgFnS
do	do	k7c2
ekonomie	ekonomie	k1gFnSc2
především	především	k9
heterogennost	heterogennost	k1gFnSc4
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
kapitálové	kapitálový	k2eAgInPc1d1
statky	statek	k1gInPc1
mají	mít	k5eAaImIp3nP
jinou	jiný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
a	a	k8xC
„	„	k?
<g/>
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
“	“	k?
se	se	k3xPyFc4
do	do	k7c2
různých	různý	k2eAgNnPc2d1
stádií	stádium	k1gNnPc2
výrobního	výrobní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvíme	mluvit	k5eAaImIp1nP
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
struktuře	struktura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
stavebním	stavební	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
teorie	teorie	k1gFnSc2
kapitálu	kapitál	k1gInSc2
je	být	k5eAaImIp3nS
čas	čas	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
kategorie	kategorie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
výrazně	výrazně	k6eAd1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eugen	Eugen	k2eAgInSc1d1
Böhm	Böhm	k1gInSc1
von	von	k1gInSc1
Bawerk	Bawerk	k1gInSc4
zavedl	zavést	k5eAaPmAgInS
pojem	pojem	k1gInSc1
časové	časový	k2eAgFnSc2d1
preference	preference	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
odráží	odrážet	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
preferují	preferovat	k5eAaImIp3nP
současné	současný	k2eAgInPc4d1
statky	statek	k1gInPc4
před	před	k7c7
statky	statek	k1gInPc7
budoucími	budoucí	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
také	také	k9
vysvětlením	vysvětlení	k1gNnSc7
úroku	úrok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
samotného	samotný	k2eAgInSc2d1
faktu	fakt	k1gInSc2
preference	preference	k1gFnSc1
současných	současný	k2eAgMnPc2d1
statků	statek	k1gInPc2
před	před	k7c7
budoucími	budoucí	k2eAgMnPc7d1
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
budoucím	budoucí	k2eAgFnPc3d1
statkům	statek	k1gInPc3
přisuzují	přisuzovat	k5eAaImIp3nP
menší	malý	k2eAgFnSc4d2
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tento	tento	k3xDgInSc1
rozdíl	rozdíl	k1gInSc1
pak	pak	k6eAd1
představuje	představovat	k5eAaImIp3nS
úrok	úrok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
nižší	nízký	k2eAgMnSc1d2
přitom	přitom	k6eAd1
bude	být	k5eAaImBp3nS
společenská	společenský	k2eAgFnSc1d1
časová	časový	k2eAgFnSc1d1
preference	preference	k1gFnSc1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
bude	být	k5eAaImBp3nS
dostupných	dostupný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
pro	pro	k7c4
investice	investice	k1gFnPc4
do	do	k7c2
statků	statek	k1gInPc2
vyšších	vysoký	k2eAgInPc2d2
řádů	řád	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
spojena	spojen	k2eAgFnSc1d1
"	"	kIx"
<g/>
oklikovost	oklikovost	k1gFnSc1
výroby	výroba	k1gFnSc2
<g/>
"	"	kIx"
z	z	k7c2
pera	pero	k1gNnSc2
Böhm-Bawerka	Böhm-Bawerka	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
v	v	k7c6
pozdějším	pozdní	k2eAgNnSc6d2
Hayekově	Hayekův	k2eAgNnSc6d1
vysvětlení	vysvětlení	k1gNnSc6
rostoucí	rostoucí	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
déle	dlouho	k6eAd2
trvající	trvající	k2eAgInSc1d1
výrobní	výrobní	k2eAgInSc1d1
proces	proces	k1gInSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
produktivnější	produktivní	k2eAgNnSc1d2
<g/>
,	,	kIx,
neboť	neboť	k8xC
jinak	jinak	k6eAd1
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
proveden	proveden	k2eAgInSc1d1
proces	proces	k1gInSc1
kratší	krátký	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
poskytuje	poskytovat	k5eAaImIp3nS
úroková	úrokový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
podle	podle	k7c2
rakouských	rakouský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
v	v	k7c4
prostředí	prostředí	k1gNnSc4
peněz	peníze	k1gInPc2
ovlivněnými	ovlivněný	k2eAgInPc7d1
státními	státní	k2eAgInPc7d1
zásahy	zásah	k1gInPc7
</s>
<s>
Teorie	teorie	k1gFnSc1
peněz	peníze	k1gInPc2
a	a	k8xC
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
<g/>
Teorii	teorie	k1gFnSc4
peněz	peníze	k1gInPc2
rozpracoval	rozpracovat	k5eAaPmAgMnS
už	už	k6eAd1
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
a	a	k8xC
ukázal	ukázat	k5eAaPmAgMnS
na	na	k7c4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
peníze	peníz	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
jako	jako	k9
tržní	tržní	k2eAgInSc4d1
produkt	produkt	k1gInSc4
a	a	k8xC
až	až	k9
později	pozdě	k6eAd2
byly	být	k5eAaImAgInP
monopolizovány	monopolizovat	k5eAaBmNgInP
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
důsledně	důsledně	k6eAd1
z	z	k7c2
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
jako	jako	k9
takového	takový	k3xDgMnSc4
<g/>
,	,	kIx,
respektive	respektive	k9
z	z	k7c2
tržního	tržní	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
až	až	k6eAd1
Mises	Mises	k1gMnSc1
dokázal	dokázat	k5eAaPmAgMnS
vysvětlit	vysvětlit	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
peněz	peníze	k1gInPc2
na	na	k7c6
základě	základ	k1gInSc6
všeobecné	všeobecný	k2eAgFnSc2d1
subjektivní	subjektivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
regresního	regresní	k2eAgInSc2d1
teorému	teorém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
peněz	peníze	k1gInPc2
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
kupní	kupní	k2eAgFnSc1d1
síla	síla	k1gFnSc1
peněz	peníze	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tržní	tržní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
jako	jako	k8xS,k8xC
každá	každý	k3xTgFnSc1
jiná	jiný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
tak	tak	k6eAd1
odmítá	odmítat	k5eAaImIp3nS
veškeré	veškerý	k3xTgFnSc2
teorie	teorie	k1gFnSc2
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
spočívají	spočívat	k5eAaImIp3nP
na	na	k7c6
makroekonomických	makroekonomický	k2eAgInPc6d1
základech	základ	k1gInPc6
<g/>
,	,	kIx,
tedy	tedy	k9
zejména	zejména	k9
kvantitativní	kvantitativní	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
peněz	peníze	k1gInPc2
používající	používající	k2eAgInSc1d1
pojem	pojem	k1gInSc1
cenové	cenový	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
(	(	kIx(
<g/>
Nicméně	nicméně	k8xC
rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
kvantitativní	kvantitativní	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
shodne	shodnout	k5eAaBmIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nárůst	nárůst	k1gInSc1
peněžní	peněžní	k2eAgFnSc2d1
zásoby	zásoba	k1gFnSc2
vyvolá	vyvolat	k5eAaPmIp3nS
růst	růst	k1gInSc1
cen	cena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Z	z	k7c2
Mengerovy	Mengerův	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
spontánního	spontánní	k2eAgInSc2d1
vzniku	vznik	k1gInSc2
peněz	peníze	k1gInPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
monopolizace	monopolizace	k1gFnSc1
peněz	peníze	k1gInPc2
státem	stát	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zavedla	zavést	k5eAaPmAgFnS
do	do	k7c2
trhu	trh	k1gInSc2
s	s	k7c7
penězi	peníze	k1gInPc7
poruchy	porucha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
vznik	vznik	k1gInSc1
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInSc1d1
peněžní	peněžní	k2eAgInSc1d1
systém	systém	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c6
centrálním	centrální	k2eAgNnSc6d1
bankovnictví	bankovnictví	k1gNnSc6
a	a	k8xC
částečných	částečný	k2eAgFnPc6d1
rezervách	rezerva	k1gFnPc6
je	být	k5eAaImIp3nS
zdrojem	zdroj	k1gInSc7
vzniku	vznik	k1gInSc2
hospodářských	hospodářský	k2eAgMnPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Úvěrový	úvěrový	k2eAgInSc1d1
boom	boom	k1gInSc1
vyvolává	vyvolávat	k5eAaImIp3nS
prodlužování	prodlužování	k1gNnSc4
výrobního	výrobní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
zdroje	zdroj	k1gInPc1
však	však	k9
nejsou	být	k5eNaImIp3nP
„	„	k?
<g/>
kryty	kryt	k1gInPc1
<g/>
“	“	k?
úsporami	úspora	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledná	výsledný	k2eAgFnSc1d1
úroková	úrokový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
tak	tak	k6eAd1
neodráží	odrážet	k5eNaImIp3nS
časové	časový	k2eAgFnPc4d1
preference	preference	k1gFnPc4
a	a	k8xC
vede	vést	k5eAaImIp3nS
k	k	k7c3
chybné	chybný	k2eAgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
kalkulaci	kalkulace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemožnost	nemožnost	k1gFnSc1
financovatelnosti	financovatelnost	k1gFnSc2
všech	všecek	k3xTgFnPc2
investic	investice	k1gFnPc2
se	se	k3xPyFc4
však	však	k9
ukáže	ukázat	k5eAaPmIp3nS
až	až	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
času	čas	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vyvolává	vyvolávat	k5eAaImIp3nS
úvěrový	úvěrový	k2eAgInSc4d1
boom	boom	k1gInSc4
dojem	dojem	k1gInSc4
hospodářské	hospodářský	k2eAgFnSc2d1
prosperity	prosperita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhalení	odhalení	k1gNnSc1
chybných	chybný	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
pak	pak	k6eAd1
znamená	znamenat	k5eAaImIp3nS
fázi	fáze	k1gFnSc4
útlumu	útlum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
tržního	tržní	k2eAgInSc2d1
procesu	proces	k1gInSc2
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
vnímá	vnímat	k5eAaImIp3nS
trh	trh	k1gInSc4
jako	jako	k8xC,k8xS
proces	proces	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
jednají	jednat	k5eAaImIp3nP
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
nejistoty	nejistota	k1gFnSc2
<g/>
,	,	kIx,
neví	vědět	k5eNaImIp3nS
přesně	přesně	k6eAd1
<g/>
,	,	kIx,
jaká	jaký	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
bude	být	k5eAaImBp3nS
budoucnost	budoucnost	k1gFnSc1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
tedy	tedy	k9
odhadovat	odhadovat	k5eAaImF
<g/>
,	,	kIx,
očekávat	očekávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trh	trh	k1gInSc1
je	být	k5eAaImIp3nS
vnímán	vnímat	k5eAaImNgInS
jako	jako	k9
proces	proces	k1gInSc1
objevování	objevování	k1gNnSc1
příležitostí	příležitost	k1gFnPc2
k	k	k7c3
uspokojování	uspokojování	k1gNnSc3
lidských	lidský	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
správně	správně	k6eAd1
odhadne	odhadnout	k5eAaPmIp3nS
vývoj	vývoj	k1gInSc4
preferencí	preference	k1gFnPc2
jiných	jiný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
odměněn	odměnit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
trhu	trh	k1gInSc2
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
z	z	k7c2
principu	princip	k1gInSc2
zabudován	zabudovat	k5eAaPmNgInS
element	element	k1gInSc1
času	čas	k1gInSc2
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
plynoucí	plynoucí	k2eAgFnSc4d1
nejistota	nejistota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovým	klíčový	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
v	v	k7c6
teorii	teorie	k1gFnSc6
trhu	trh	k1gInSc2
je	být	k5eAaImIp3nS
podnikatelské	podnikatelský	k2eAgNnSc1d1
objevování	objevování	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
širší	široký	k2eAgFnSc1d2
definice	definice	k1gFnSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
dělá	dělat	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
z	z	k7c2
nás	my	k3xPp1nPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sledujeme	sledovat	k5eAaImIp1nP
vývoj	vývoj	k1gInSc4
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc4
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
poptávky	poptávka	k1gFnSc2
<g/>
,	,	kIx,
příjmy	příjem	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgNnSc2
si	se	k3xPyFc3
můžeme	moct	k5eAaImIp1nP
odvodit	odvodit	k5eAaPmF
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
trhu	trh	k1gInSc6
mezera	mezera	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
bychom	by	kYmCp1nP
mohli	moct	k5eAaImAgMnP
aktivně	aktivně	k6eAd1
vyplnit	vyplnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
klíč	klíč	k1gInSc1
k	k	k7c3
vysvětlení	vysvětlení	k1gNnSc3
<g/>
,	,	kIx,
proč	proč	k6eAd1
trhy	trh	k1gInPc1
fungují	fungovat	k5eAaImIp3nP
<g/>
,	,	kIx,
tedy	tedy	k8xC
proč	proč	k6eAd1
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
koordinaci	koordinace	k1gFnSc3
lidských	lidský	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgInPc2
zásahů	zásah	k1gInPc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
problematiku	problematika	k1gFnSc4
výrazným	výrazný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
obohatilo	obohatit	k5eAaPmAgNnS
dílo	dílo	k1gNnSc1
Israela	Israel	k1gMnSc2
Kirznera	Kirzner	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
trhu	trh	k1gInSc2
a	a	k8xC
tržního	tržní	k2eAgInSc2d1
procesu	proces	k1gInSc2
je	být	k5eAaImIp3nS
protipólem	protipól	k1gInSc7
ekonomie	ekonomie	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
modely	model	k1gInPc4
všemožných	všemožný	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
konkurence	konkurence	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
pak	pak	k6eAd1
aplikuje	aplikovat	k5eAaBmIp3nS
na	na	k7c4
realitu	realita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
zkoumání	zkoumání	k1gNnSc4
předpokladů	předpoklad	k1gInPc2
dosažení	dosažení	k1gNnSc4
rovnováhy	rovnováha	k1gFnSc2
na	na	k7c6
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
se	se	k3xPyFc4
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
zaměřuje	zaměřovat	k5eAaImIp3nS
více	hodně	k6eAd2
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
tržní	tržní	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nevidí	vidět	k5eNaImIp3nS
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
o	o	k7c4
rovnováhu	rovnováha	k1gFnSc4
usilovat	usilovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovnováha	rovnováha	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
stav	stav	k1gInSc4
klidu	klid	k1gInSc2
<g/>
,	,	kIx,
proces	proces	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
naopak	naopak	k6eAd1
nerovnováhu	nerovnováha	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
lidské	lidský	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
typická	typický	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Významní	významný	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
</s>
<s>
Eugen	Eugen	k2eAgInSc1d1
von	von	k1gInSc1
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
von	von	k1gInSc4
Wieser	Wieser	k1gInSc1
</s>
<s>
František	František	k1gMnSc1
Čuhel	Čuhel	k1gMnSc1
</s>
<s>
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k1gInSc1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Lachmann	Lachmann	k1gMnSc1
</s>
<s>
Murray	Murraa	k1gFnPc1
Rothbard	Rothbarda	k1gFnPc2
</s>
<s>
Israel	Israel	k1gMnSc1
Kirzner	Kirzner	k1gMnSc1
</s>
<s>
Současní	současný	k2eAgMnPc1d1
hlavní	hlavní	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Hans-Hermann	Hans-Hermann	k1gMnSc1
Hoppe	Hopp	k1gMnSc5
</s>
<s>
Walter	Walter	k1gMnSc1
Block	Block	k1gMnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Garrison	Garrison	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Boettke	Boettk	k1gFnSc2
</s>
<s>
Jörg	Jörg	k1gInSc1
Guido	Guido	k1gNnSc1
Hülsmann	Hülsmann	k1gInSc1
</s>
<s>
Ralph	Ralph	k1gMnSc1
Raico	Raico	k1gMnSc1
</s>
<s>
Llewellyn	Llewellyn	k1gMnSc1
Rockwell	Rockwell	k1gMnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Thornton	Thornton	k1gInSc4
</s>
<s>
David	David	k1gMnSc1
Gordon	Gordon	k1gMnSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Salerno	Salerna	k1gFnSc5
</s>
<s>
Roderick	Roderick	k1gMnSc1
T.	T.	kA
Long	Long	k1gMnSc1
</s>
<s>
Pascal	pascal	k1gInSc1
Salin	salina	k1gFnPc2
</s>
<s>
Robert	Robert	k1gMnSc1
Higgs	Higgsa	k1gFnPc2
</s>
<s>
Sanford	Sanford	k1gMnSc1
Ikeda	Ikeda	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Šíma	Šíma	k1gMnSc1
</s>
<s>
Jesús	Jesús	k6eAd1
Hueta	Hueta	k1gMnSc1
de	de	k?
Soto	Soto	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
FUCHS	Fuchs	k1gMnSc1
<g/>
,	,	kIx,
Kamil	Kamil	k1gMnSc1
<g/>
;	;	kIx,
LISÝ	LISÝ	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomicko-správní	ekonomicko-správní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.econlib.org/library/Enc/AustrianSchoolofEconomics.html	http://www.econlib.org/library/Enc/AustrianSchoolofEconomics.html	k1gInSc4
<g/>
↑	↑	k?
E-knihy	E-knih	k1gInPc4
-	-	kIx~
Liberální	liberální	k2eAgInSc4d1
institut	institut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberální	liberální	k2eAgInSc4d1
institut	institut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Produkty	produkt	k1gInPc1
Archív	archív	k1gInSc1
-	-	kIx~
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
O	o	k7c4
nás	my	k3xPp1nPc4
-	-	kIx~
Liberální	liberální	k2eAgInSc4d1
institut	institut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberální	liberální	k2eAgInSc4d1
institut	institut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LVMI	LVMI	kA
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
nás	my	k3xPp1nPc2
–	–	k?
Mises	Mises	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ludwig	Ludwiga	k1gFnPc2
von	von	k1gInSc1
Mises	Mises	k1gMnSc1
–	–	k?
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
<g/>
:	:	kIx,
Pojednání	pojednání	k1gNnSc1
o	o	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
↑	↑	k?
RYAN	RYAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Austrians	Austriansa	k1gFnPc2
Really	Realla	k1gFnSc2
Reject	Reject	k1gMnSc1
Empirical	Empirical	k1gMnSc1
Evidence	evidence	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
|	|	kIx~
C.	C.	kA
<g/>
Jay	Jay	k1gMnSc1
Engel	Engel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KANOPIADMIN	KANOPIADMIN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Philosophical	Philosophical	k1gFnSc2
Origins	Origins	k1gInSc1
of	of	k?
Austrian	Austrian	k1gInSc1
Economics	Economics	k1gInSc1
|	|	kIx~
David	David	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FUCHS	Fuchs	k1gMnSc1
<g/>
,	,	kIx,
Kamil	Kamil	k1gMnSc1
<g/>
;	;	kIx,
LISÝ	LISÝ	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomicko-správní	ekonomicko-správní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
12.4	12.4	k4
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
–	–	k?
Kapitál	kapitál	k1gInSc1
<g/>
,	,	kIx,
úrok	úrok	k1gInSc1
<g/>
,	,	kIx,
náklady	náklad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářských	hospodářský	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdravý	zdravý	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
–	–	k?
Mises	Mises	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROTHBARD	ROTHBARD	kA
<g/>
,	,	kIx,
Murray	Murray	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
v	v	k7c6
rukou	ruka	k1gFnPc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
144	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86389	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
41	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ROTHBARD	ROTHBARD	kA
<g/>
,	,	kIx,
Murray	Murray	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
v	v	k7c6
rukou	ruka	k1gFnPc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
144	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86389	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
65	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ISRAEL	ISRAEL	kA
<g/>
,	,	kIx,
Kirzner	Kirzner	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
fungují	fungovat	k5eAaImIp3nP
trhy	trh	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Higgs	Higgs	k1gInSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
:	:	kIx,
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
z	z	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Šíma	Šíma	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
společně	společně	k6eAd1
Alfa	alfa	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86851	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86389	#num#	k4
<g/>
-	-	kIx~
<g/>
43	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
intervencionismus	intervencionismus	k1gInSc1
</s>
<s>
laissez	laissez	k1gMnSc1
faire	fair	k1gInSc5
</s>
<s>
klasický	klasický	k2eAgInSc1d1
liberalismus	liberalismus	k1gInSc1
</s>
<s>
libertarianismus	libertarianismus	k1gInSc1
</s>
<s>
anarchokapitalismus	anarchokapitalismus	k1gInSc1
</s>
<s>
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc4
Institute	institut	k1gInSc5
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc4
Institut	institut	k1gInSc1
Česko	Česko	k1gNnSc1
&	&	k?
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Misesa	k1gFnPc2
Institute	institut	k1gInSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Hayek	Hayek	k1gInSc1
Center	centrum	k1gNnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
Deborah	Deborah	k1gMnSc1
L.	L.	kA
<g/>
Walker	Walker	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Brent	Brent	k?
M.	M.	kA
Johnstone	Johnston	k1gInSc5
<g/>
:	:	kIx,
Information	Information	k1gInSc1
and	and	k?
the	the	k?
Economic	Economic	k1gMnSc1
Problem	Probl	k1gInSc7
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Liberales	Liberales	k1gInSc1
Institut	institut	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Nadácia	Nadácius	k1gMnSc2
F.	F.	kA
A.	A.	kA
Hayeka	Hayeka	k1gMnSc1
</s>
<s>
Současní	současný	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Hans	Hans	k1gMnSc1
Hermann	Hermann	k1gMnSc1
Hoppe	Hopp	k1gMnSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Roderick	Roderick	k1gMnSc1
T.	T.	kA
<g/>
Long	Long	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Barry	Barr	k1gInPc1
Smith	Smitha	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Walter	Walter	k1gMnSc1
Block	Block	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
David	David	k1gMnSc1
Gordon	Gordon	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Jörg	Jörg	k1gInSc1
Guido	Guido	k1gNnSc1
Hülsmann	Hülsmann	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ralph	Ralph	k1gMnSc1
Raico	Raico	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Garrison	Garrison	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Joseph	Joseph	k1gInSc1
Salerno	Salerna	k1gFnSc5
</s>
<s>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
Jesus	Jesus	k1gInSc1
Huerta	Huert	k1gMnSc2
de	de	k?
Soto	Soto	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Lew	Lew	k1gMnSc1
Rockwell	Rockwell	k1gMnSc1
</s>
<s>
Zásadní	zásadní	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
rakouské	rakouský	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
<g/>
:	:	kIx,
Principles	Principles	k1gMnSc1
of	of	k?
Economics	Economics	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Eugen	Eugen	k2eAgInSc1d1
von	von	k1gInSc1
Boehm-Bawerk	Boehm-Bawerk	k1gInSc1
<g/>
:	:	kIx,
Capital	Capital	k1gMnSc1
and	and	k?
Interest	Interest	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
<g/>
:	:	kIx,
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
(	(	kIx(
<g/>
Human	Human	k1gInSc1
Action	Action	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
<g/>
:	:	kIx,
Theory	Theora	k1gFnPc1
of	of	k?
Money	Monea	k1gFnSc2
and	and	k?
Credit	Credit	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
<g/>
:	:	kIx,
Theory	Theora	k1gFnPc1
and	and	k?
History	Histor	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
F.	F.	kA
<g/>
A.	A.	kA
<g/>
Hayek	Hayek	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Use	usus	k1gInSc5
of	of	k?
Knowledge	Knowledg	k1gInPc4
in	in	k?
Society	societa	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Murray	Murraa	k1gFnSc2
N.	N.	kA
<g/>
Rothbard	Rothbard	k1gMnSc1
<g/>
:	:	kIx,
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
and	and	k?
State	status	k1gInSc5
+	+	kIx~
Power	Power	k1gInSc1
and	and	k?
Market	market	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ekonomové	ekonom	k1gMnPc1
Rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
•	•	k?
Eugen	Eugen	k1gInSc1
von	von	k1gInSc1
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
von	von	k1gInSc1
Wieser	Wieser	k1gInSc1
•	•	k?
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k6eAd1
Klasický	klasický	k2eAgInSc4d1
liberalismus	liberalismus	k1gInSc4
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Anderson	Anderson	k1gMnSc1
•	•	k?
Herbert	Herbert	k1gMnSc1
J.	J.	kA
Davenport	Davenport	k1gInSc1
•	•	k?
Frank	Frank	k1gMnSc1
Fetter	Fetter	k1gMnSc1
•	•	k?
Roger	Roger	k1gMnSc1
Garrison	Garrison	k1gMnSc1
•	•	k?
Gottfried	Gottfried	k1gMnSc1
Haberler	Haberler	k1gMnSc1
•	•	k?
Henry	Henry	k1gMnSc1
Hazlitt	Hazlitt	k1gMnSc1
•	•	k?
Jeffrey	Jeffrea	k1gFnSc2
Herbener	Herbener	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Harold	Harold	k1gMnSc1
Hutt	Hutt	k1gMnSc1
•	•	k?
Israel	Israel	k1gMnSc1
Kirzner	Kirzner	k1gMnSc1
•	•	k?
Ludwig	Ludwig	k1gMnSc1
Lachmann	Lachmann	k1gMnSc1
•	•	k?
Yuri	Yuri	k1gNnPc2
Maltsev	Maltsvo	k1gNnPc2
•	•	k?
Ron	Ron	k1gMnSc1
Paul	Paul	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Prychitko	Prychitka	k1gFnSc5
•	•	k?
Yaraslau	Yaraslaus	k1gInSc6
Ramanchuk	Ramanchuk	k1gInSc1
•	•	k?
George	George	k1gInSc1
Reisman	Reisman	k1gMnSc1
•	•	k?
Lionel	Lionel	k1gInSc1
Robbins	Robbins	k1gInSc1
•	•	k?
Peter	Peter	k1gMnSc1
Schiff	Schiff	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Alois	Alois	k1gMnSc1
Schumpeter	Schumpeter	k1gMnSc1
•	•	k?
George	George	k1gInSc1
Selgin	Selgin	k1gMnSc1
•	•	k?
Hans	Hans	k1gMnSc1
Sennholz	Sennholz	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Sanford	Sanford	k1gInSc1
Ikeda	Ikeda	k1gMnSc1
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
<g/>
/	/	kIx~
<g/>
Volný	volný	k2eAgInSc1d1
trh	trh	k1gInSc1
</s>
<s>
William	William	k1gInSc1
L.	L.	kA
Anderson	Anderson	k1gMnSc1
•	•	k?
Walter	Walter	k1gMnSc1
Block	Block	k1gMnSc1
•	•	k?
Gene	gen	k1gInSc5
Callahan	Callahan	k1gMnSc1
•	•	k?
Hans-Hermann	Hans-Hermann	k1gMnSc1
Hoppe	Hopp	k1gInSc5
•	•	k?
Jörg	Jörg	k1gMnSc1
Guido	Guido	k1gNnSc1
Hülsmann	Hülsmann	k1gMnSc1
•	•	k?
Jesús	Jesús	k1gInSc1
Huerta	Huert	k1gMnSc2
de	de	k?
Soto	Soto	k1gMnSc1
•	•	k?
Steven	Steven	k2eAgMnSc1d1
Horwitz	Horwitz	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Leeson	Leeson	k1gMnSc1
•	•	k?
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
P.	P.	kA
Murphy	Murpha	k1gFnPc1
•	•	k?
Joseph	Joseph	k1gMnSc1
T.	T.	kA
Salerno	Salerna	k1gFnSc5
•	•	k?
Mark	Mark	k1gMnSc1
Thornton	Thornton	k1gInSc1
•	•	k?
Peter	Peter	k1gMnSc1
Boettke	Boettk	k1gFnSc2
•	•	k?
Thomas	Thomas	k1gMnSc1
E.	E.	kA
Woods	Woods	k1gInSc1
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
•	•	k?
Peter	Peter	k1gMnSc1
Klein	Klein	k1gMnSc1
•	•	k?
Randall	Randall	k1gMnSc1
G.	G.	kA
Holcombe	Holcomb	k1gInSc5
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
směry	směr	k1gInPc1
Předmoderní	Předmoderní	k2eAgInPc1d1
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Středověká	středověký	k2eAgFnSc1d1
islámská	islámský	k2eAgFnSc1d1
•	•	k?
Scholastika	scholastika	k1gFnSc1
Moderní	moderní	k2eAgFnSc1d1
éra	éra	k1gFnSc1
</s>
<s>
Raně	raně	k6eAd1
moderní	moderní	k2eAgFnSc1d1
</s>
<s>
Salamanská	Salamanský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Merkantilismus	merkantilismus	k1gInSc1
•	•	k?
Fyziokratismus	fyziokratismus	k1gInSc1
•	•	k?
Kameralismus	Kameralismus	k1gInSc1
Pozdně	pozdně	k6eAd1
moderní	moderní	k2eAgInSc1d1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
<g/>
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Anarchistická	anarchistický	k2eAgFnSc1d1
<g/>
(	(	kIx(
<g/>
Mutualismus	mutualismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Birminghenská	Birminghenský	k2eAgFnSc1d1
•	•	k?
Klasická	klasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ricardova	Ricardův	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
historická	historický	k2eAgFnSc1d1
•	•	k?
Francouzská	francouzský	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
•	•	k?
Georgismus	Georgismus	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
historická	historický	k2eAgFnSc1d1
•	•	k?
Malthusiánství	malthusiánství	k1gNnSc6
•	•	k?
Marginalismus	Marginalismus	k1gInSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
(	(	kIx(
<g/>
Lausannská	lausannský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
Cambridgeská	cambridgeský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Křesťanský	křesťanský	k2eAgInSc4d1
socialismus	socialismus	k1gInSc4
Současná	současný	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Behavioární	Behavioární	k2eAgFnSc1d1
•	•	k?
Budhistická	Budhistický	k2eAgFnSc1d1
•	•	k?
Schopnostní	Schopnostní	k2eAgInSc1d1
přístup	přístup	k1gInSc1
•	•	k?
Carnegská	Carnegská	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Chicagská	chicagský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Konstituční	konstituční	k2eAgFnSc1d1
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
•	•	k?
Ekologická	ekologický	k2eAgFnSc1d1
•	•	k?
Enviromentální	Enviromentální	k2eAgFnSc1d1
•	•	k?
Evoluční	evoluční	k2eAgFnSc1d1
•	•	k?
Feministická	feministický	k2eAgFnSc1d1
•	•	k?
Freighburská	Freighburský	k2eAgFnSc1d1
•	•	k?
Instituciální	Instituciální	k2eAgNnSc1d1
•	•	k?
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo	Neo	k1gFnSc1
<g/>
,	,	kIx,
Neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Cirkulační	cirkulační	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
(	(	kIx(
<g/>
Tržní	tržní	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Neo-Malthusiánství	Neo-Malthusiánství	k1gNnSc6
•	•	k?
Neo-marxistická	Neo-marxistický	k2eAgFnSc1d1
•	•	k?
Neo-Ricardiánství	Neo-Ricardiánství	k1gNnSc6
•	•	k?
Neoliberalismus	neoliberalismus	k1gInSc1
•	•	k?
Nová	Nová	k1gFnSc1
klasická	klasický	k2eAgFnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Racionální	racionální	k2eAgNnPc4d1
očekávání	očekávání	k1gNnPc4
<g/>
,	,	kIx,
Teorie	teorie	k1gFnSc1
skutečného	skutečný	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Nová	nový	k2eAgFnSc1d1
institucionální	institucionální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Organizační	organizační	k2eAgFnSc1d1
•	•	k?
Teorie	teorie	k1gFnSc1
veřejné	veřejný	k2eAgFnSc2d1
volby	volba	k1gFnSc2
•	•	k?
Regulační	regulační	k2eAgNnSc1d1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc1d1
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Struktuální	Struktuální	k2eAgFnSc1d1
•	•	k?
Ekonomie	ekonomie	k1gFnSc1
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Thermoekonomie	Thermoekonomie	k1gFnSc2
•	•	k?
Virginiánská	Virginiánský	k2eAgFnSc1d1
•	•	k?
Social	Social	k1gMnSc1
Credit	Credit	k1gMnSc1
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Heterodoxní	heterodoxní	k2eAgFnPc4d1
ekonomie	ekonomie	k1gFnPc4
•	•	k?
Historie	historie	k1gFnSc1
ekonomických	ekonomický	k2eAgInPc2d1
směrů	směr	k1gInPc2
</s>
<s>
Makroekonomie	makroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
koncepty	koncept	k1gInPc4
</s>
<s>
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Agregátní	agregátní	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
•	•	k?
Deflace	deflace	k1gFnSc2
•	•	k?
Poptávkový	poptávkový	k2eAgInSc1d1
šok	šok	k1gInSc1
•	•	k?
Nabídkový	nabídkový	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
•	•	k?
Dezinflace	Dezinflace	k1gFnSc2
•	•	k?
Efektivní	efektivní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Očekávání	očekávání	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Adaptivní	adaptivní	k2eAgFnPc1d1
•	•	k?
Racionální	racionální	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
•	•	k?
Inflace	inflace	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Tahem	tah	k1gInSc7
poptávky	poptávka	k1gFnSc2
•	•	k?
Nákladová	nákladový	k2eAgFnSc1d1
)	)	kIx)
•	•	k?
Úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Investice	investice	k1gFnSc1
•	•	k?
Past	pasta	k1gFnPc2
na	na	k7c4
likviditu	likvidita	k1gFnSc4
•	•	k?
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
<g/>
(	(	kIx(
<g/>
HDP	HDP	kA
•	•	k?
HNP	HNP	kA
•	•	k?
ČND	ČND	kA
<g/>
)	)	kIx)
•	•	k?
Microfoundations	Microfoundations	k1gInSc4
•	•	k?
Peníze	peníz	k1gInPc4
<g/>
(	(	kIx(
<g/>
Endogenní	endogenní	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Tvorba	tvorba	k1gFnSc1
peněz	peníze	k1gInPc2
•	•	k?
Poptávka	poptávka	k1gFnSc1
po	po	k7c6
penězích	peníze	k1gInPc6
•	•	k?
Preference	preference	k1gFnPc1
likvidity	likvidita	k1gFnSc2
•	•	k?
Peněžní	peněžní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
•	•	k?
Národní	národní	k2eAgInPc4d1
účty	účet	k1gInPc4
<g/>
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
národních	národní	k2eAgInPc2d1
účtů	účet	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Nominal	Nominal	k1gMnSc1
rigidity	rigidita	k1gFnSc2
•	•	k?
Cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
•	•	k?
Recese	recese	k1gFnSc1
•	•	k?
Shrinkflation	Shrinkflation	k1gInSc1
•	•	k?
Stagflace	Stagflace	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
•	•	k?
Nezaměstnanost	nezaměstnanost	k1gFnSc1
Politiky	politika	k1gFnSc2
</s>
<s>
Fiskální	fiskální	k2eAgFnSc1d1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
•	•	k?
Obchodní	obchodní	k2eAgFnSc1d1
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Modely	model	k1gInPc1
</s>
<s>
IS-LM	IS-LM	k?
•	•	k?
AD	ad	k7c4
<g/>
–	–	k?
<g/>
AS	as	k1gNnSc4
•	•	k?
Keynesiánský	keynesiánský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
•	•	k?
Multiplikátor	multiplikátor	k1gInSc1
•	•	k?
Akcelerátorový	Akcelerátorový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Phillipsova	Phillipsův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Arrow	Arrow	k1gFnSc2
<g/>
–	–	k?
<g/>
Debreu	Debreus	k1gInSc2
•	•	k?
Harrod	Harroda	k1gFnPc2
<g/>
–	–	k?
<g/>
Domar	Domar	k1gMnSc1
•	•	k?
Solow	Solow	k1gMnSc1
<g/>
–	–	k?
<g/>
Swan	Swan	k1gMnSc1
•	•	k?
Ramsey	Ramsea	k1gFnSc2
<g/>
–	–	k?
<g/>
Cass	Cass	k1gInSc1
<g/>
–	–	k?
<g/>
Koopmans	Koopmans	k1gInSc1
•	•	k?
Model	modla	k1gFnPc2
překrývajících	překrývající	k2eAgFnPc2d1
se	se	k3xPyFc4
generací	generace	k1gFnPc2
•	•	k?
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
rovnováhy	rovnováha	k1gFnSc2
•	•	k?
Teorie	teorie	k1gFnSc1
endogenního	endogenní	k2eAgInSc2d1
růstu	růst	k1gInSc2
•	•	k?
Teorie	teorie	k1gFnSc1
shody	shoda	k1gFnSc2
•	•	k?
Mundell	Mundell	k1gInSc1
<g/>
–	–	k?
<g/>
Fleming	Fleming	k1gInSc1
•	•	k?
Model	model	k1gInSc1
překročení	překročení	k1gNnSc2
•	•	k?
NAIRU	NAIRU	kA
Související	související	k2eAgFnSc1d1
</s>
<s>
Ekonometrie	Ekonometrie	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Rozvojová	rozvojový	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
Školy	škola	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
proud	proud	k1gInSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo-Nová	Neo-Nová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
(	(	kIx(
<g/>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
obchdního	obchdní	k2eAgInSc2d1
cykklu	cykkl	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
Heterodox	Heterodox	k1gInSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
<g/>
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Cirkulační	cirkulační	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Tržní	tržní	k2eAgInSc1d1
monetarismus	monetarismus	k1gInSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
makroekonomové	makroekonom	k1gMnPc1
</s>
<s>
François	François	k1gFnSc1
Quesnay	Quesnaa	k1gFnSc2
•	•	k?
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Robert	Robert	k1gMnSc1
Malthus	Malthus	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
•	•	k?
Léon	Léon	k1gMnSc1
Walras	Walras	k1gMnSc1
•	•	k?
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Knapp	Knapp	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Irving	Irving	k1gInSc1
Fisher	Fishra	k1gFnPc2
•	•	k?
Wesley	Weslea	k1gFnSc2
Clair	Clair	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Maynard	Maynard	k1gMnSc1
Keynes	Keynes	k1gMnSc1
•	•	k?
Alvin	Alvin	k1gMnSc1
Hansen	Hansen	k2eAgMnSc1d1
•	•	k?
Michał	Michał	k1gMnSc1
Kalecki	Kaleck	k1gFnSc2
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Myrdal	Myrdal	k1gMnSc1
•	•	k?
Simon	Simon	k1gMnSc1
Kuznets	Kuznetsa	k1gFnPc2
•	•	k?
Joan	Joan	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
Hicks	Hicksa	k1gFnPc2
•	•	k?
Richard	Richard	k1gMnSc1
Stone	ston	k1gInSc5
•	•	k?
Hyman	Hyman	k1gMnSc1
Minsky	minsky	k6eAd1
•	•	k?
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
Klein	Klein	k1gMnSc1
•	•	k?
Edmund	Edmund	k1gMnSc1
Phelps	Phelpsa	k1gFnPc2
•	•	k?
Robert	Robert	k1gMnSc1
Lucas	Lucas	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
•	•	k?
Edward	Edward	k1gMnSc1
C.	C.	kA
Prescott	Prescott	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Diamond	Diamond	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Nordhaus	Nordhaus	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Stiglitz	Stiglitz	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
J.	J.	kA
Sargent	Sargent	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
•	•	k?
Gregory	Gregor	k1gMnPc7
Mankiw	Mankiw	k1gFnPc7
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
•	•	k?
Makroekonomický	makroekonomický	k2eAgInSc4d1
model	model	k1gInSc4
•	•	k?
Seznam	seznam	k1gInSc1
významných	významný	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
•	•	k?
Mikroekonomie	mikroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4140691-6	4140691-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85009754	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85009754	#num#	k4
</s>
