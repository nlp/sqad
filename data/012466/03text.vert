<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Maurice	Maurika	k1gFnSc3	Maurika
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc1	Talleyrand-Périgord
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1754	[number]	k4	1754
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
nejznámější	známý	k2eAgMnSc1d3	nejznámější
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
státník	státník	k1gMnSc1	státník
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
období	období	k1gNnSc2	období
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
Bourbonů	bourbon	k1gInPc2	bourbon
a	a	k8xC	a
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
obdržel	obdržet	k5eAaPmAgMnS	obdržet
několik	několik	k4yIc4	několik
vysokých	vysoký	k2eAgInPc2d1	vysoký
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
titulů	titul	k1gInPc2	titul
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
kníže	kníže	k1gMnSc1	kníže
beneventský	beneventský	k2eAgMnSc1d1	beneventský
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Dina	Dinum	k1gNnSc2	Dinum
(	(	kIx(	(
<g/>
Dino	Dino	k6eAd1	Dino
je	být	k5eAaImIp3nS	být
kalábrijský	kalábrijský	k2eAgInSc4d1	kalábrijský
ostrov	ostrov	k1gInSc4	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
činnost	činnost	k1gFnSc1	činnost
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
==	==	k?	==
</s>
</p>
<p>
<s>
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1754	[number]	k4	1754
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jako	jako	k8xC	jako
hraběcí	hraběcí	k2eAgMnSc1d1	hraběcí
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgorda	k1gFnPc2	Talleyrand-Périgorda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
plukovníkem	plukovník	k1gMnSc7	plukovník
královské	královský	k2eAgFnSc2d1	královská
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
dvorní	dvorní	k2eAgFnSc1d1	dvorní
dáma	dáma	k1gFnSc1	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
kněžském	kněžský	k2eAgInSc6d1	kněžský
semináři	seminář	k1gInSc6	seminář
Saint-Sulpice	Saint-Sulpice	k1gFnSc2	Saint-Sulpice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
absolvování	absolvování	k1gNnSc6	absolvování
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1779	[number]	k4	1779
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
první	první	k4xOgInSc4	první
post	post	k1gInSc4	post
jeho	jeho	k3xOp3gFnSc2	jeho
církevní	církevní	k2eAgFnSc2d1	církevní
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
abbé	abbé	k1gMnPc4	abbé
při	při	k7c6	při
opatství	opatství	k1gNnSc4	opatství
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
generálních	generální	k2eAgMnPc2d1	generální
inspektorů	inspektor	k1gMnPc2	inspektor
francouzského	francouzský	k2eAgInSc2d1	francouzský
katolického	katolický	k2eAgInSc2d1	katolický
kléru	klér	k1gInSc2	klér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
biskupem	biskup	k1gMnSc7	biskup
v	v	k7c6	v
provinčním	provinční	k2eAgNnSc6d1	provinční
městě	město	k1gNnSc6	město
Autun	Autuna	k1gFnPc2	Autuna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
před	před	k7c7	před
návratem	návrat	k1gInSc7	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
strávil	strávit	k5eAaPmAgMnS	strávit
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
pro	pro	k7c4	pro
vadu	vada	k1gFnSc4	vada
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
nemohl	moct	k5eNaImAgMnS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
(	(	kIx(	(
<g/>
nápadně	nápadně	k6eAd1	nápadně
kulhal	kulhat	k5eAaImAgInS	kulhat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
původu	původ	k1gInSc3	původ
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zchudlé	zchudlý	k2eAgFnSc2d1	zchudlá
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysokých	vysoký	k2eAgFnPc2d1	vysoká
funkcí	funkce	k1gFnPc2	funkce
jen	jen	k9	jen
v	v	k7c6	v
církevních	církevní	k2eAgFnPc6d1	církevní
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
charakteru	charakter	k1gInSc6	charakter
postižení	postižení	k1gNnSc2	postižení
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
panují	panovat	k5eAaImIp3nP	panovat
rozporné	rozporný	k2eAgInPc1d1	rozporný
názory	názor	k1gInPc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Pamětech	paměť	k1gFnPc6	paměť
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
malé	malý	k2eAgNnSc4d1	malé
dítě	dítě	k1gNnSc4	dítě
spadl	spadnout	k5eAaPmAgInS	spadnout
z	z	k7c2	z
komody	komoda	k1gFnSc2	komoda
<g/>
,	,	kIx,	,
zlomenina	zlomenina	k1gFnSc1	zlomenina
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
a	a	k8xC	a
léčena	léčit	k5eAaImNgFnS	léčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
zhojila	zhojit	k5eAaPmAgFnS	zhojit
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgFnSc1d2	častější
je	být	k5eAaImIp3nS	být
však	však	k9	však
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
vadu	vada	k1gFnSc4	vada
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
(	(	kIx(	(
<g/>
typu	typ	k1gInSc2	typ
pes	pes	k1gMnSc1	pes
equinovarus	equinovarus	k1gMnSc1	equinovarus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
pominout	pominout	k5eAaPmF	pominout
ani	ani	k8xC	ani
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
Marfanův	Marfanův	k2eAgInSc4d1	Marfanův
syndrom	syndrom	k1gInSc4	syndrom
(	(	kIx(	(
<g/>
systémová	systémový	k2eAgFnSc1d1	systémová
vývojová	vývojový	k2eAgFnSc1d1	vývojová
vada	vada	k1gFnSc1	vada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
klérus	klérus	k1gInSc1	klérus
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
revoluce	revoluce	k1gFnSc2	revoluce
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
přešel	přejít	k5eAaPmAgInS	přejít
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
od	od	k7c2	od
kléru	klér	k1gInSc2	klér
ke	k	k7c3	k
třetímu	třetí	k4xOgInSc3	třetí
stavu	stav	k1gInSc3	stav
a	a	k8xC	a
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
zasedání	zasedání	k1gNnSc4	zasedání
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
i	i	k9	i
v	v	k7c6	v
nově	nova	k1gFnSc6	nova
se	se	k3xPyFc4	se
tvořícím	tvořící	k2eAgNnSc6d1	tvořící
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
uznával	uznávat	k5eAaImAgInS	uznávat
nutnost	nutnost	k1gFnSc4	nutnost
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jej	on	k3xPp3gMnSc4	on
vzdalovalo	vzdalovat	k5eAaImAgNnS	vzdalovat
od	od	k7c2	od
kléru	klér	k1gInSc2	klér
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
duchovenstvu	duchovenstvo	k1gNnSc3	duchovenstvo
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
přiostřil	přiostřit	k5eAaPmAgInS	přiostřit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
pro	pro	k7c4	pro
znárodnění	znárodnění	k1gNnSc4	znárodnění
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
výtěžek	výtěžek	k1gInSc1	výtěžek
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
chtěl	chtít	k5eAaImAgMnS	chtít
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
státních	státní	k2eAgInPc2d1	státní
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
předložil	předložit	k5eAaPmAgMnS	předložit
<g/>
,	,	kIx,	,
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
konfiskaci	konfiskace	k1gFnSc4	konfiskace
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
má	mít	k5eAaImIp3nS	mít
majetek	majetek	k1gInSc1	majetek
jen	jen	k6eAd1	jen
propůjčen	propůjčit	k5eAaPmNgInS	propůjčit
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
svého	svůj	k3xOyFgNnSc2	svůj
poslání	poslání	k1gNnSc2	poslání
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
jako	jako	k9	jako
osobní	osobní	k2eAgNnSc1d1	osobní
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
hodnostářů	hodnostář	k1gMnPc2	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Talleyranda	Talleyrando	k1gNnSc2	Talleyrando
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c6	o
vyvlastnění	vyvlastnění	k1gNnSc6	vyvlastnění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
složil	složit	k5eAaPmAgMnS	složit
přísahu	přísaha	k1gFnSc4	přísaha
na	na	k7c6	na
ústavu	ústav	k1gInSc6	ústav
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
kléru	klér	k1gInSc2	klér
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jej	on	k3xPp3gMnSc4	on
podřídil	podřídit	k5eAaPmAgMnS	podřídit
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
funkce	funkce	k1gFnSc1	funkce
biskupa	biskup	k1gMnSc2	biskup
z	z	k7c2	z
Autun	Autuna	k1gFnPc2	Autuna
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Pius	Pius	k1gMnSc1	Pius
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jej	on	k3xPp3gMnSc4	on
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
činnost	činnost	k1gFnSc4	činnost
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
zbavil	zbavit	k5eAaPmAgInS	zbavit
jej	on	k3xPp3gMnSc4	on
všech	všecek	k3xTgInPc2	všecek
církevních	církevní	k2eAgInPc2d1	církevní
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
exkomunikoval	exkomunikovat	k5eAaBmAgMnS	exkomunikovat
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
hrůzovlády	hrůzovláda	k1gFnSc2	hrůzovláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
opustil	opustit	k5eAaPmAgInS	opustit
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
prozíravě	prozíravě	k6eAd1	prozíravě
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
za	za	k7c2	za
Dantonovy	Dantonův	k2eAgFnSc2d1	Dantonova
pomoci	pomoc	k1gFnSc2	pomoc
opatřil	opatřit	k5eAaPmAgInS	opatřit
výjezdní	výjezdní	k2eAgNnPc4d1	výjezdní
povolení	povolení	k1gNnPc4	povolení
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnPc3	svůj
již	již	k9	již
druhým	druhý	k4xOgMnSc7	druhý
diplomatickým	diplomatický	k2eAgInSc7d1	diplomatický
posláním	poslání	k1gNnSc7	poslání
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
mu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
hladký	hladký	k2eAgInSc4d1	hladký
návrat	návrat	k1gInSc4	návrat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
nepadlo	padnout	k5eNaPmAgNnS	padnout
odium	odium	k1gNnSc1	odium
emigranta	emigrant	k1gMnSc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
premiérem	premiér	k1gMnSc7	premiér
Williamem	William	k1gInSc7	William
Pittem	Pitt	k1gMnSc7	Pitt
mladším	mladý	k2eAgMnSc7d2	mladší
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
francouzských	francouzský	k2eAgMnPc2d1	francouzský
protirepublikánských	protirepublikánský	k2eAgMnPc2d1	protirepublikánský
exulantů	exulant	k1gMnPc2	exulant
vyhoštěn	vyhoštěn	k2eAgInSc1d1	vyhoštěn
<g/>
.	.	kIx.	.
</s>
<s>
Uchýlil	uchýlit	k5eAaPmAgInS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
mj.	mj.	kA	mj.
bankovními	bankovní	k2eAgFnPc7d1	bankovní
a	a	k8xC	a
pozemkovými	pozemkový	k2eAgFnPc7d1	pozemková
spekulacemi	spekulace	k1gFnPc7	spekulace
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Theophila	Theophil	k1gMnSc2	Theophil
Cazenove	Cazenov	k1gInSc5	Cazenov
<g/>
,	,	kIx,	,
usídleného	usídlený	k2eAgInSc2d1	usídlený
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
,	,	kIx,	,
a	a	k8xC	a
stýkal	stýkat	k5eAaImAgMnS	stýkat
se	se	k3xPyFc4	se
s	s	k7c7	s
tamějšími	tamější	k2eAgMnPc7d1	tamější
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
emigranty	emigrant	k1gMnPc7	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
přes	přes	k7c4	přes
severní	severní	k2eAgNnSc4d1	severní
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krátce	krátce	k6eAd1	krátce
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
hanzovním	hanzovní	k2eAgNnSc6d1	hanzovní
městě	město	k1gNnSc6	město
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
své	svůj	k3xOyFgFnSc2	svůj
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
paní	paní	k1gFnSc2	paní
Germaine	Germain	k1gInSc5	Germain
de	de	k?	de
Staël	Staël	k1gInSc1	Staël
vládnoucím	vládnoucí	k2eAgNnSc7d1	vládnoucí
Direktoriem	direktorium	k1gNnSc7	direktorium
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Paula	Paul	k1gMnSc2	Paul
Barrase	Barras	k1gInSc5	Barras
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
francouzským	francouzský	k2eAgMnSc7d1	francouzský
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
jako	jako	k8xC	jako
nástupce	nástupce	k1gMnSc2	nástupce
Charlese	Charles	k1gMnSc2	Charles
Delacroixe	Delacroixe	k1gFnSc2	Delacroixe
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
malíře	malíř	k1gMnSc2	malíř
Eugè	Eugè	k1gFnSc1	Eugè
Delacroixe	Delacroixe	k1gFnSc1	Delacroixe
<g/>
.	.	kIx.	.
</s>
<s>
Madame	madame	k1gFnSc1	madame
de	de	k?	de
Staël	Staël	k1gInSc1	Staël
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
zámožná	zámožný	k2eAgFnSc1d1	zámožná
manželka	manželka	k1gFnSc1	manželka
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
švédského	švédský	k2eAgMnSc2d1	švédský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
vlivná	vlivný	k2eAgFnSc1d1	vlivná
dáma	dáma	k1gFnSc1	dáma
francouzské	francouzský	k2eAgFnSc2d1	francouzská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Talleyrand	Talleyranda	k1gFnPc2	Talleyranda
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Napoleona	Napoleon	k1gMnSc2	Napoleon
===	===	k?	===
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
nechtěl	chtít	k5eNaImAgMnS	chtít
být	být	k5eAaImF	být
nadále	nadále	k6eAd1	nadále
politicky	politicky	k6eAd1	politicky
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
Direktoriem	direktorium	k1gNnSc7	direktorium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dle	dle	k7c2	dle
jeho	on	k3xPp3gNnSc2	on
mínění	mínění	k1gNnSc2	mínění
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
ke	k	k7c3	k
krachu	krach	k1gInSc3	krach
<g/>
,	,	kIx,	,
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
mladé	mladý	k2eAgFnSc3d1	mladá
síle	síla	k1gFnSc3	síla
-	-	kIx~	-
k	k	k7c3	k
Napoleonu	napoleon	k1gInSc2	napoleon
Bonapartovi	Bonapartův	k2eAgMnPc1d1	Bonapartův
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
podpořil	podpořit	k5eAaPmAgMnS	podpořit
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
státním	státní	k2eAgInSc6d1	státní
převratu	převrat	k1gInSc6	převrat
ke	k	k7c3	k
dni	den	k1gInSc3	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
brumaire	brumaire	k1gInSc1	brumaire
roku	rok	k1gInSc2	rok
VIII	VIII	kA	VIII
podle	podle	k7c2	podle
revolučního	revoluční	k2eAgInSc2d1	revoluční
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgInS	být
Napoleonem	napoleon	k1gInSc7	napoleon
coby	coby	k?	coby
prvním	první	k4xOgMnSc6	první
konzulem	konzul	k1gMnSc7	konzul
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
opět	opět	k6eAd1	opět
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
manželského	manželský	k2eAgInSc2d1	manželský
s	s	k7c7	s
Catherine	Catherin	k1gInSc5	Catherin
Worlée-Grandovou	Worlée-Grandový	k2eAgFnSc7d1	Worlée-Grandový
<g/>
,	,	kIx,	,
ženou	hnát	k5eAaImIp3nP	hnát
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
žil	žíla	k1gFnPc2	žíla
a	a	k8xC	a
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dosti	dosti	k6eAd1	dosti
pochybnou	pochybný	k2eAgFnSc4d1	pochybná
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
málo	málo	k6eAd1	málo
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
<s>
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
hlavním	hlavní	k2eAgMnSc7d1	hlavní
propagátorem	propagátor	k1gMnSc7	propagátor
myšlenky	myšlenka	k1gFnSc2	myšlenka
vytvoření	vytvoření	k1gNnSc2	vytvoření
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
postaral	postarat	k5eAaPmAgMnS	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
hladké	hladký	k2eAgNnSc4d1	hladké
diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
uznání	uznání	k1gNnSc4	uznání
francouzského	francouzský	k2eAgNnSc2d1	francouzské
císařství	císařství	k1gNnSc2	císařství
většinou	většinou	k6eAd1	většinou
světových	světový	k2eAgInPc2d1	světový
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
na	na	k7c4	na
budoucnost	budoucnost	k1gFnSc4	budoucnost
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
Napoleonových	Napoleonová	k1gFnPc2	Napoleonová
značně	značně	k6eAd1	značně
lišily	lišit	k5eAaImAgFnP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Talleyrand	Talleyrand	k1gInSc4	Talleyrand
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
mírem	mír	k1gInSc7	mír
v	v	k7c6	v
Amiensu	Amiens	k1gInSc6	Amiens
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
získala	získat	k5eAaPmAgFnS	získat
Francie	Francie	k1gFnSc1	Francie
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
Napoleona	Napoleon	k1gMnSc4	Napoleon
dále	daleko	k6eAd2	daleko
podporoval	podporovat	k5eAaImAgMnS	podporovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
protagonistů	protagonista	k1gMnPc2	protagonista
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
aféry	aféra	k1gFnSc2	aféra
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
popravou	poprava	k1gFnSc7	poprava
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
neprávem	neprávo	k1gNnSc7	neprávo
odsouzeného	odsouzený	k2eAgMnSc2d1	odsouzený
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bourbonského	bourbonský	k2eAgMnSc2d1	bourbonský
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Enghienu	Enghien	k1gInSc2	Enghien
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
podepsal	podepsat	k5eAaPmAgInS	podepsat
Bratislavský	bratislavský	k2eAgInSc1d1	bratislavský
mír	mír	k1gInSc1	mír
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
i	i	k9	i
mír	mír	k1gInSc1	mír
v	v	k7c6	v
Tylži	Tylž	k1gFnSc6	Tylž
(	(	kIx(	(
<g/>
Tilsit	Tilsit	k1gInSc1	Tilsit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
předvídavostí	předvídavost	k1gFnSc7	předvídavost
<g/>
,	,	kIx,	,
vida	vida	k?	vida
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
říši	říše	k1gFnSc4	říše
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
svých	svůj	k3xOyFgFnPc2	svůj
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
od	od	k7c2	od
Napoleona	Napoleon	k1gMnSc2	Napoleon
myšlenkově	myšlenkově	k6eAd1	myšlenkově
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
vzdaloval	vzdalovat	k5eAaImAgMnS	vzdalovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
monarchů	monarcha	k1gMnPc2	monarcha
v	v	k7c4	v
Erfurtu	Erfurta	k1gFnSc4	Erfurta
pak	pak	k6eAd1	pak
s	s	k7c7	s
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
I.	I.	kA	I.
tajně	tajně	k6eAd1	tajně
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
soustavně	soustavně	k6eAd1	soustavně
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
konspiroval	konspirovat	k5eAaImAgMnS	konspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
pádu	pád	k1gInSc6	pád
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
měl	mít	k5eAaImAgInS	mít
výrazný	výrazný	k2eAgInSc1d1	výrazný
podíl	podíl	k1gInSc1	podíl
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
trůn	trůn	k1gInSc4	trůn
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgMnS	mít
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
pokrokovějším	pokrokový	k2eAgInPc3d2	pokrokovější
názorům	názor	k1gInPc3	názor
značné	značný	k2eAgFnSc2d1	značná
výhrady	výhrada	k1gFnSc2	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
restaurace	restaurace	k1gFnSc2	restaurace
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
krátce	krátce	k6eAd1	krátce
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednou	jednou	k6eAd1	jednou
zároveň	zároveň	k6eAd1	zároveň
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
hodina	hodina	k1gFnSc1	hodina
udeřila	udeřit	k5eAaPmAgFnS	udeřit
na	na	k7c6	na
tančícím	tančící	k2eAgInSc6d1	tančící
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jako	jako	k9	jako
zástupci	zástupce	k1gMnPc1	zástupce
poražené	poražený	k2eAgFnSc2d1	poražená
země	zem	k1gFnSc2	zem
podařilo	podařit	k5eAaPmAgNnS	podařit
mistrným	mistrný	k2eAgInSc7d1	mistrný
diplomatickým	diplomatický	k2eAgInSc7d1	diplomatický
postupem	postup	k1gInSc7	postup
dojednat	dojednat	k5eAaPmF	dojednat
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
příznivé	příznivý	k2eAgFnPc4d1	příznivá
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
uhájit	uhájit	k5eAaPmF	uhájit
její	její	k3xOp3gFnPc4	její
hranice	hranice	k1gFnPc4	hranice
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyslanec	vyslanec	k1gMnSc1	vyslanec
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Červencová	červencový	k2eAgFnSc1d1	červencová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
navrhovatelů	navrhovatel	k1gMnPc2	navrhovatel
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Filipa	Filip	k1gMnSc2	Filip
jako	jako	k8xS	jako
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jej	on	k3xPp3gNnSc2	on
poté	poté	k6eAd1	poté
pověřil	pověřit	k5eAaPmAgInS	pověřit
diplomatickým	diplomatický	k2eAgNnSc7d1	diplomatické
zastupováním	zastupování	k1gNnSc7	zastupování
Francie	Francie	k1gFnSc2	Francie
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
snažil	snažit	k5eAaImAgInS	snažit
působit	působit	k5eAaImF	působit
na	na	k7c6	na
sbližování	sbližování	k1gNnSc6	sbližování
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
vztahy	vztah	k1gInPc1	vztah
byly	být	k5eAaImAgInP	být
léta	léto	k1gNnPc4	léto
těžce	těžce	k6eAd1	těžce
narušeny	narušen	k2eAgFnPc1d1	narušena
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
kousek	kousek	k1gInSc4	kousek
mistrovské	mistrovský	k2eAgFnSc2d1	mistrovská
diplomacie	diplomacie	k1gFnSc2	diplomacie
předvedl	předvést	k5eAaPmAgMnS	předvést
při	při	k7c6	při
jednáních	jednání	k1gNnPc6	jednání
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
postupu	postup	k1gInSc3	postup
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1830	[number]	k4	1830
princ	princ	k1gMnSc1	princ
Leopold	Leopold	k1gMnSc1	Leopold
Georg	Georg	k1gMnSc1	Georg
Christian	Christian	k1gMnSc1	Christian
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Sachsen-Coburg-Saalfeld	Sachsen-Coburg-Saalfeld	k1gMnSc1	Sachsen-Coburg-Saalfeld
zvolen	zvolit	k5eAaPmNgMnS	zvolit
belgickým	belgický	k2eAgMnSc7d1	belgický
králem	král	k1gMnSc7	král
Leopoldem	Leopold	k1gMnSc7	Leopold
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
proslul	proslout	k5eAaPmAgInS	proslout
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
vynikající	vynikající	k2eAgMnSc1d1	vynikající
řečník	řečník	k1gMnSc1	řečník
<g/>
,	,	kIx,	,
vtipný	vtipný	k2eAgMnSc1d1	vtipný
glosátor	glosátor	k1gMnSc1	glosátor
<g/>
,	,	kIx,	,
labužník	labužník	k1gMnSc1	labužník
a	a	k8xC	a
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
znalec	znalec	k1gMnSc1	znalec
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Charles-Maurice	Charles-Maurika	k1gFnSc3	Charles-Maurika
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
zemřel	zemřít	k5eAaPmAgMnS	zemřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1838	[number]	k4	1838
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zámku	zámek	k1gInSc6	zámek
Valençay	Valençaa	k1gFnSc2	Valençaa
<g/>
.	.	kIx.	.
</s>
<s>
Univerzální	univerzální	k2eAgFnSc7d1	univerzální
dědičkou	dědička	k1gFnSc7	dědička
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
společnice	společnice	k1gFnSc1	společnice
Dorothea	Dorothe	k1gInSc2	Dorothe
von	von	k1gInSc1	von
Biron	Biron	k1gInSc1	Biron
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Dorothée	Dorothée	k1gFnSc1	Dorothée
duchesse	duchesse	k1gFnSc2	duchesse
de	de	k?	de
Dino	Dino	k1gMnSc1	Dino
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Dorothée	Dorothée	k1gInSc1	Dorothée
de	de	k?	de
Courlande	Courland	k1gInSc5	Courland
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
rozvedená	rozvedený	k2eAgFnSc1d1	rozvedená
manželka	manželka	k1gFnSc1	manželka
jeho	on	k3xPp3gMnSc2	on
synovce	synovec	k1gMnSc2	synovec
Edmonda	Edmond	k1gMnSc2	Edmond
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gMnSc1	Talleyrand-Périgord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
neměl	mít	k5eNaImAgInS	mít
žádné	žádný	k3yNgFnPc4	žádný
manželské	manželský	k2eAgFnPc4d1	manželská
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
nemanželských	manželský	k2eNgInPc2d1	nemanželský
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Charles-Joseph	Charles-Joseph	k1gMnSc1	Charles-Joseph
de	de	k?	de
Flahaut	Flahaut	k1gMnSc1	Flahaut
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Marie-Adélaï	Marie-Adélaï	k1gFnSc2	Marie-Adélaï
Filleul	Filleul	k1gInSc1	Filleul
<g/>
,	,	kIx,	,
hraběnky	hraběnka	k1gFnPc1	hraběnka
de	de	k?	de
Flahaut	Flahaut	k1gInSc1	Flahaut
de	de	k?	de
La	la	k1gNnSc7	la
Billarderie	Billarderie	k1gFnSc2	Billarderie
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
milenec	milenec	k1gMnSc1	milenec
jeho	jeho	k3xOp3gFnSc2	jeho
vyženěné	vyženěný	k2eAgFnSc2d1	vyženěná
dcery	dcera	k1gFnSc2	dcera
Hortensie	Hortensie	k1gFnSc2	Hortensie
z	z	k7c2	z
Beauharnais	Beauharnais	k1gFnSc2	Beauharnais
(	(	kIx(	(
<g/>
královny	královna	k1gFnSc2	královna
holandské	holandský	k2eAgFnSc2d1	holandská
<g/>
)	)	kIx)	)
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Druhého	druhý	k4xOgNnSc2	druhý
císařství	císařství	k1gNnSc2	císařství
vévody	vévoda	k1gMnSc2	vévoda
de	de	k?	de
Morny	morna	k1gFnSc2	morna
<g/>
.	.	kIx.	.
</s>
<s>
Talleyrandovo	Talleyrandův	k2eAgNnSc1d1	Talleyrandův
otcovství	otcovství	k1gNnSc1	otcovství
slavného	slavný	k2eAgMnSc2d1	slavný
malíře	malíř	k1gMnSc2	malíř
Eugè	Eugè	k1gFnSc1	Eugè
Delacroixe	Delacroixe	k1gFnSc1	Delacroixe
<g/>
,	,	kIx,	,
dokládané	dokládaný	k2eAgNnSc1d1	dokládaný
údajnou	údajný	k2eAgFnSc7d1	údajná
podobou	podoba	k1gFnSc7	podoba
obou	dva	k4xCgMnPc2	dva
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
Talleyrandovou	Talleyrandový	k2eAgFnSc7d1	Talleyrandový
nápadnou	nápadný	k2eAgFnSc7d1	nápadná
podporou	podpora	k1gFnSc7	podpora
mladého	mladý	k2eAgMnSc2d1	mladý
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velmi	velmi	k6eAd1	velmi
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
TARABA	TARABA	kA	TARABA
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
intrikán	intrikán	k1gMnSc1	intrikán
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
M.	M.	kA	M.
Talleyranda	Talleyranda	k1gFnSc1	Talleyranda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
235	[number]	k4	235
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
58	[number]	k4	58
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
236	[number]	k4	236
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WILLMS	WILLMS	kA	WILLMS
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
<g/>
.	.	kIx.	.
</s>
<s>
Talleyrand	Talleyrand	k1gInSc1	Talleyrand
<g/>
.	.	kIx.	.
</s>
<s>
Virtuose	virtuos	k1gMnSc5	virtuos
der	drát	k5eAaImRp2nS	drát
Macht	Macht	k1gInSc1	Macht
1754	[number]	k4	1754
<g/>
-	-	kIx~	-
<g/>
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
:	:	kIx,	:
C.	C.	kA	C.
H.	H.	kA	H.
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
384	[number]	k4	384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9783406621451	[number]	k4	9783406621451
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WINTR	WINTR	kA	WINTR
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
''	''	k?	''
<g/>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
–	–	k?	–
jeho	jeho	k3xOp3gFnSc6	jeho
maršálové	maršál	k1gMnPc1	maršál
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
<g/>
.	.	kIx.	.
<g/>
''	''	k?	''
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
372	[number]	k4	372
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Charles	Charles	k1gMnSc1	Charles
Maurice	Maurika	k1gFnSc3	Maurika
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc4	Talleyrand-Périgord
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Charles	Charles	k1gMnSc1	Charles
Maurice	Maurika	k1gFnSc3	Maurika
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc4	Talleyrand-Périgord
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Charles-Maurice	Charles-Maurika	k1gFnSc6	Charles-Maurika
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgorda	k1gFnPc2	Talleyrand-Périgorda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
