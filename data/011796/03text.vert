<p>
<s>
Zblochan	zblochan	k1gInSc1	zblochan
vodní	vodní	k2eAgInSc1d1	vodní
(	(	kIx(	(
<g/>
Glyceria	Glycerium	k1gNnSc2	Glycerium
maxima	maximum	k1gNnSc2	maximum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
travina	travina	k1gFnSc1	travina
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
květenstvím	květenství	k1gNnSc7	květenství
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
vlhkomilný	vlhkomilný	k2eAgInSc4d1	vlhkomilný
druh	druh	k1gInSc4	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
zblochan	zblochan	k1gInSc1	zblochan
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
euroasijský	euroasijský	k2eAgInSc4d1	euroasijský
druh	druh	k1gInSc4	druh
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
obou	dva	k4xCgInPc6	dva
Amerik	Amerika	k1gFnPc2	Amerika
i	i	k8xC	i
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgNnSc1d3	nejčastější
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
rybníky	rybník	k1gInPc7	rybník
a	a	k8xC	a
na	na	k7c6	na
dolních	dolní	k2eAgInPc6d1	dolní
tocích	tok	k1gInPc6	tok
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
častý	častý	k2eAgMnSc1d1	častý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
nepřítomný	přítomný	k2eNgMnSc1d1	nepřítomný
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
mělkých	mělký	k2eAgFnPc2d1	mělká
<g/>
,	,	kIx,	,
eutrofních	eutrofní	k2eAgFnPc2d1	eutrofní
<g/>
,	,	kIx,	,
stojatých	stojatý	k2eAgFnPc2d1	stojatá
nebo	nebo	k8xC	nebo
pomalu	pomalu	k6eAd1	pomalu
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
rybnících	rybník	k1gInPc6	rybník
<g/>
,	,	kIx,	,
tůních	tůně	k1gFnPc6	tůně
<g/>
,	,	kIx,	,
slepých	slepý	k2eAgNnPc6d1	slepé
ramenech	rameno	k1gNnPc6	rameno
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
melioračních	meliorační	k2eAgInPc6d1	meliorační
kanálech	kanál	k1gInPc6	kanál
<g/>
,	,	kIx,	,
příkopech	příkop	k1gInPc6	příkop
i	i	k8xC	i
na	na	k7c6	na
zamokřených	zamokřený	k2eAgFnPc6d1	zamokřená
sníženinách	sníženina	k1gFnPc6	sníženina
nivních	nivní	k2eAgFnPc2d1	nivní
luk	louka	k1gFnPc2	louka
<g/>
.	.	kIx.	.
</s>
<s>
Rostlině	rostlina	k1gFnSc3	rostlina
nevadí	vadit	k5eNaImIp3nS	vadit
kolísání	kolísání	k1gNnSc1	kolísání
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nedobře	dobře	k6eNd1	dobře
však	však	k9	však
snáší	snášet	k5eAaImIp3nS	snášet
hluboké	hluboký	k2eAgNnSc4d1	hluboké
zaplavení	zaplavení	k1gNnSc4	zaplavení
během	během	k7c2	během
vegetační	vegetační	k2eAgFnSc2d1	vegetační
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
roste	růst	k5eAaImIp3nS	růst
při	při	k7c6	při
výšce	výška	k1gFnSc6	výška
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
0	[number]	k4	0
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
ji	on	k3xPp3gFnSc4	on
nevadí	vadit	k5eNaImIp3nS	vadit
ani	ani	k8xC	ani
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
letní	letní	k2eAgNnSc1d1	letní
vyschnutí	vyschnutí	k1gNnSc1	vyschnutí
substrátu	substrát	k1gInSc2	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Neroste	růst	k5eNaImIp3nS	růst
v	v	k7c6	v
rašeliništích	rašeliniště	k1gNnPc6	rašeliniště
a	a	k8xC	a
na	na	k7c6	na
zasolených	zasolený	k2eAgNnPc6d1	zasolené
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozlehlých	rozlehlý	k2eAgFnPc6d1	rozlehlá
vodách	voda	k1gFnPc6	voda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
netvoří	tvořit	k5eNaImIp3nS	tvořit
vlnobití	vlnobití	k1gNnSc1	vlnobití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
organický	organický	k2eAgInSc4d1	organický
sediment	sediment	k1gInSc4	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
porost	porost	k1gInSc1	porost
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
i	i	k9	i
na	na	k7c4	na
nízký	nízký	k2eAgInSc4d1	nízký
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
se	se	k3xPyFc4	se
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
vlhkých	vlhký	k2eAgFnPc2d1	vlhká
luk	louka	k1gFnPc2	louka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
k	k	k7c3	k
zastínění	zastínění	k1gNnSc3	zastínění
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
lesních	lesní	k2eAgFnPc6d1	lesní
tůních	tůně	k1gFnPc6	tůně
lužních	lužní	k2eAgInPc2d1	lužní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
stanovištích	stanoviště	k1gNnPc6	stanoviště
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
nad	nad	k7c7	nad
30	[number]	k4	30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
rostlina	rostlina	k1gFnSc1	rostlina
silně	silně	k6eAd1	silně
trsnatá	trsnatý	k2eAgFnSc1d1	trsnatá
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
stébla	stéblo	k1gNnPc4	stéblo
u	u	k7c2	u
báze	báze	k1gFnSc2	báze
poléhají	poléhat	k5eAaImIp3nP	poléhat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místech	místo	k1gNnPc6	místo
se	s	k7c7	s
stabilně	stabilně	k6eAd1	stabilně
mělkým	mělký	k2eAgNnSc7d1	mělké
zaplavením	zaplavení	k1gNnSc7	zaplavení
substrátu	substrát	k1gInSc2	substrát
mívá	mívat	k5eAaImIp3nS	mívat
rostlina	rostlina	k1gFnSc1	rostlina
stébla	stéblo	k1gNnSc2	stéblo
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
vzpřímená	vzpřímený	k2eAgFnSc1d1	vzpřímená
<g/>
,	,	kIx,	,
intenzivně	intenzivně	k6eAd1	intenzivně
odnožuje	odnožovat	k5eAaImIp3nS	odnožovat
a	a	k8xC	a
utváří	utvářet	k5eAaImIp3nS	utvářet
velké	velký	k2eAgInPc4d1	velký
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
mladá	mladý	k2eAgFnSc1d1	mladá
rostlina	rostlina	k1gFnSc1	rostlina
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
pomalý	pomalý	k2eAgInSc1d1	pomalý
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
kvete	kvést	k5eAaImIp3nS	kvést
až	až	k9	až
druhým	druhý	k4xOgInSc7	druhý
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vzrůstná	vzrůstný	k2eAgFnSc1d1	vzrůstná
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
tráva	tráva	k1gFnSc1	tráva
dorůstající	dorůstající	k2eAgFnSc1d1	dorůstající
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
80	[number]	k4	80
až	až	k9	až
200	[number]	k4	200
cm	cm	kA	cm
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgInSc1d1	šířící
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
podzemními	podzemní	k2eAgInPc7d1	podzemní
oddenky	oddenek	k1gInPc7	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Stébla	stéblo	k1gNnPc1	stéblo
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
spodu	spod	k1gInSc2	spod
až	až	k9	až
1	[number]	k4	1
cm	cm	kA	cm
hrubá	hrubá	k1gFnSc1	hrubá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc1d1	zelený
listy	list	k1gInPc1	list
vyrůstající	vyrůstající	k2eAgInPc1d1	vyrůstající
z	z	k7c2	z
oblé	oblý	k2eAgFnSc2d1	oblá
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
zmáčknuté	zmáčknutý	k2eAgFnPc4d1	zmáčknutá
pochvy	pochva	k1gFnPc4	pochva
s	s	k7c7	s
ouškem	ouško	k1gNnSc7	ouško
<g/>
.	.	kIx.	.
</s>
<s>
Listové	listový	k2eAgFnPc1d1	listová
čepele	čepel	k1gFnPc1	čepel
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
cm	cm	kA	cm
a	a	k8xC	a
široké	široký	k2eAgNnSc1d1	široké
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
uprostřed	uprostřed	k6eAd1	uprostřed
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
drážku	drážka	k1gFnSc4	drážka
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
kápovitou	kápovitý	k2eAgFnSc4d1	kápovitá
špičku	špička	k1gFnSc4	špička
<g/>
,	,	kIx,	,
na	na	k7c6	na
líci	líc	k1gFnSc6	líc
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgInPc1d1	hladký
a	a	k8xC	a
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
drsné	drsný	k2eAgNnSc1d1	drsné
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
světlu	světlo	k1gNnSc3	světlo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
patrné	patrný	k2eAgFnPc1d1	patrná
spojky	spojka	k1gFnPc1	spojka
mezi	mezi	k7c7	mezi
žilkami	žilka	k1gFnPc7	žilka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
stébla	stéblo	k1gNnSc2	stéblo
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
bohaté	bohatý	k2eAgNnSc1d1	bohaté
<g/>
,	,	kIx,	,
25	[number]	k4	25
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
květenství	květenství	k1gNnSc1	květenství
<g/>
,	,	kIx,	,
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
rozvolněná	rozvolněný	k2eAgFnSc1d1	rozvolněná
lata	lata	k1gFnSc1	lata
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
drobnými	drobné	k1gInPc7	drobné
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
mm	mm	kA	mm
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
,	,	kIx,	,
stopkatými	stopkatý	k2eAgInPc7d1	stopkatý
<g/>
,	,	kIx,	,
bočně	bočně	k6eAd1	bočně
stlačenými	stlačený	k2eAgInPc7d1	stlačený
klásky	klásek	k1gInPc7	klásek
s	s	k7c7	s
pěti	pět	k4xCc7	pět
až	až	k9	až
deseti	deset	k4xCc7	deset
kvítky	kvítko	k1gNnPc7	kvítko
<g/>
.	.	kIx.	.
</s>
<s>
Plevy	pleva	k1gFnPc1	pleva
i	i	k8xC	i
pluchy	plucha	k1gFnPc1	plucha
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
osin	osina	k1gFnPc2	osina
<g/>
.	.	kIx.	.
</s>
<s>
Kvítky	kvítek	k1gInPc1	kvítek
<g/>
,	,	kIx,	,
rozkvétající	rozkvétající	k2eAgInPc1d1	rozkvétající
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
opylovány	opylovat	k5eAaImNgFnP	opylovat
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
obilka	obilka	k1gFnSc1	obilka
velká	velká	k1gFnSc1	velká
1,5	[number]	k4	1,5
až	až	k9	až
2	[number]	k4	2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zblochan	zblochan	k1gInSc1	zblochan
vodní	vodní	k2eAgFnSc1d1	vodní
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
jak	jak	k6eAd1	jak
semeny	semeno	k1gNnPc7	semeno
(	(	kIx(	(
<g/>
obilkami	obilka	k1gFnPc7	obilka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
<g/>
)	)	kIx)	)
rozrůstáním	rozrůstání	k1gNnSc7	rozrůstání
odnoží	odnož	k1gFnPc2	odnož
oddenku	oddenek	k1gInSc2	oddenek
<g/>
,	,	kIx,	,
spolehlivě	spolehlivě	k6eAd1	spolehlivě
zakoření	zakořenit	k5eAaPmIp3nS	zakořenit
i	i	k9	i
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
odlomené	odlomený	k2eAgFnSc2d1	odlomená
a	a	k8xC	a
odplavené	odplavený	k2eAgFnSc2d1	odplavená
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
monokultury	monokultura	k1gFnPc1	monokultura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
stanovišti	stanoviště	k1gNnSc6	stanoviště
výrazně	výrazně	k6eAd1	výrazně
snižují	snižovat	k5eAaImIp3nP	snižovat
množství	množství	k1gNnSc4	množství
jiných	jiný	k2eAgInPc2d1	jiný
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Omezují	omezovat	k5eAaImIp3nP	omezovat
také	také	k9	také
přirozený	přirozený	k2eAgInSc4d1	přirozený
výskyt	výskyt	k1gInSc4	výskyt
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
vhodné	vhodný	k2eAgNnSc4d1	vhodné
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
larvy	larva	k1gFnPc4	larva
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
léta	léto	k1gNnSc2	léto
mladé	mladý	k2eAgFnSc2d1	mladá
rostliny	rostlina	k1gFnSc2	rostlina
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
výbornou	výborný	k2eAgFnSc4d1	výborná
píci	píce	k1gFnSc4	píce
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
nesnášejí	snášet	k5eNaImIp3nP	snášet
však	však	k9	však
časté	častý	k2eAgNnSc4d1	časté
kosení	kosení	k1gNnSc4	kosení
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
se	s	k7c7	s
sklizní	sklizeň	k1gFnSc7	sklizeň
ze	z	k7c2	z
zamokřených	zamokřený	k2eAgFnPc2d1	zamokřená
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
se	se	k3xPyFc4	se
zblochan	zblochan	k1gInSc1	zblochan
vodní	vodní	k2eAgInSc1d1	vodní
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
pro	pro	k7c4	pro
zpevnění	zpevnění	k1gNnSc4	zpevnění
vlhkých	vlhký	k2eAgInPc2d1	vlhký
<g/>
,	,	kIx,	,
nestálých	stálý	k2eNgInPc2d1	nestálý
břehů	břeh	k1gInPc2	břeh
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
rostlina	rostlina	k1gFnSc1	rostlina
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
parkových	parkový	k2eAgInPc2d1	parkový
rybníčků	rybníček	k1gInPc2	rybníček
<g/>
,	,	kIx,	,
pozor	pozor	k1gInSc4	pozor
však	však	k9	však
na	na	k7c4	na
rychlé	rychlý	k2eAgNnSc4d1	rychlé
rozrůstání	rozrůstání	k1gNnSc4	rozrůstání
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
vysazována	vysazován	k2eAgFnSc1d1	vysazována
jeho	jeho	k3xOp3gFnSc1	jeho
vyšlechtěná	vyšlechtěný	k2eAgFnSc1d1	vyšlechtěná
varieta	varieta	k1gFnSc1	varieta
'	'	kIx"	'
<g/>
Variegata	Variegata	k1gFnSc1	Variegata
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nižšího	nízký	k2eAgInSc2d2	nižší
vzrůstu	vzrůst	k1gInSc2	vzrůst
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
podélně	podélně	k6eAd1	podélně
zeleno-bíle	zelenoíle	k6eAd1	zeleno-bíle
pruhované	pruhovaný	k2eAgInPc4d1	pruhovaný
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zblochan	zblochan	k1gInSc1	zblochan
vodní	vodní	k2eAgFnSc1d1	vodní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Glyceria	Glycerium	k1gNnSc2	Glycerium
maxima	maximum	k1gNnSc2	maximum
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Botanický	botanický	k2eAgInSc1d1	botanický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
–	–	k?	–
rozšíření	rozšíření	k1gNnSc1	rozšíření
zblochanu	zblochan	k1gInSc2	zblochan
vodního	vodní	k2eAgInSc2d1	vodní
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
