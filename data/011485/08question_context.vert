<s>
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1917	[number]	k4	1917
–	–	k?	–
22.	[number]	k4	22.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neformálně	formálně	k6eNd1	formálně
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
Jack	Jack	k1gInSc1	Jack
Kennedy	Kenneda	k1gMnSc2	Kenneda
či	či	k8xC	či
JFK	JFK	kA	JFK
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
35.	[number]	k4	35.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
prezidenta	prezident	k1gMnSc4	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
