<s>
Světlo	světlo	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
elektromagnetickém	elektromagnetický	k2eAgNnSc6d1
záření	záření	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Světlo	světlo	k1gNnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Světelný	světelný	k2eAgInSc1d1
festival	festival	k1gInSc1
Signal	Signal	k1gFnSc2
konající	konající	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
u	u	k7c2
Rudolfina	Rudolfinum	k1gNnSc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Noční	noční	k2eAgNnSc1d1
osvětlení	osvětlení	k1gNnSc1
ve	v	k7c6
městě	město	k1gNnSc6
(	(	kIx(
<g/>
výše	vysoce	k6eAd2
<g/>
)	)	kIx)
i	i	k9
v	v	k7c6
přístavu	přístav	k1gInSc6
<g/>
,	,	kIx,
Štětín	Štětín	k1gInSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Světlo	světlo	k1gNnSc1
je	být	k5eAaImIp3nS
viditelná	viditelný	k2eAgFnSc1d1
část	část	k1gFnSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
frekvence	frekvence	k1gFnSc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
od	od	k7c2
3,9	3,9	k4
<g/>
×	×	kIx~
<g/>
1014	#num#	k4
Hz	Hz	kA
do	do	k7c2
7,9	7,9	k4
<g/>
×	×	kIx~
<g/>
1014	#num#	k4
Hz	Hz	kA
<g/>
,	,	kIx,
čemuž	což	k3yRnSc3
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
odpovídají	odpovídat	k5eAaImIp3nP
vlnové	vlnový	k2eAgFnPc1d1
délky	délka	k1gFnPc1
z	z	k7c2
intervalu	interval	k1gInSc2
390	#num#	k4
<g/>
–	–	k?
<g/>
760	#num#	k4
nm	nm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlnové	vlnový	k2eAgFnPc4d1
délky	délka	k1gFnPc4
viditelného	viditelný	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
leží	ležet	k5eAaImIp3nP
mezi	mezi	k7c7
vlnovými	vlnový	k2eAgFnPc7d1
délkami	délka	k1gFnPc7
ultrafialového	ultrafialový	k2eAgNnSc2d1
záření	záření	k1gNnSc2
a	a	k8xC
infračerveného	infračervený	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
světlem	světlo	k1gNnSc7
chápáno	chápat	k5eAaImNgNnS
i	i	k9
elektromagnetické	elektromagnetický	k2eAgNnSc1d1
záření	záření	k1gNnSc1
širšího	široký	k2eAgInSc2d2
rozsahu	rozsah	k1gInSc2
<g/>
,	,	kIx,
zasahujícího	zasahující	k2eAgInSc2d1
do	do	k7c2
infračervené	infračervený	k2eAgFnSc2d1
a	a	k8xC
ultrafialové	ultrafialový	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
lze	lze	k6eAd1
charakterizovat	charakterizovat	k5eAaBmF
pomocí	pomocí	k7c2
několika	několik	k4yIc2
hledisek	hledisko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejzákladnější	základní	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
fotometrické	fotometrický	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
svítivost	svítivost	k1gFnSc4
či	či	k8xC
světelný	světelný	k2eAgInSc4d1
tok	tok	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolorimetrické	kolorimetrický	k2eAgNnSc4d1
(	(	kIx(
<g/>
frekvenční	frekvenční	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
<g/>
,	,	kIx,
barva	barva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koherence	koherence	k1gFnSc1
a	a	k8xC
polarizace	polarizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nich	on	k3xPp3gInPc6
pak	pak	k6eAd1
závisí	záviset	k5eAaImIp3nS
i	i	k9
chování	chování	k1gNnSc1
při	při	k7c6
odrazu	odraz	k1gInSc6
<g/>
,	,	kIx,
lomu	lom	k1gInSc2
a	a	k8xC
průchodu	průchod	k1gInSc2
prostředím	prostředí	k1gNnSc7
a	a	k8xC
při	při	k7c6
skládání	skládání	k1gNnSc6
a	a	k8xC
ohybu	ohyb	k1gInSc6
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
dualitě	dualita	k1gFnSc3
částice	částice	k1gFnSc2
a	a	k8xC
vlnění	vlnění	k1gNnSc2
má	mít	k5eAaImIp3nS
světlo	světlo	k1gNnSc4
vlastnosti	vlastnost	k1gFnSc2
jak	jak	k8xS,k8xC
vlnění	vlnění	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
částice	částice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studiem	studio	k1gNnSc7
světla	světlo	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc7
interakcemi	interakce	k1gFnPc7
s	s	k7c7
hmotou	hmota	k1gFnSc7
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
optika	optika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Viditelná	viditelný	k2eAgFnSc1d1
část	část	k1gFnSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
</s>
<s>
Člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
vnímat	vnímat	k5eAaImF
část	část	k1gFnSc4
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
z	z	k7c2
rozsahu	rozsah	k1gInSc2
frekvencí	frekvence	k1gFnSc7
přibližně	přibližně	k6eAd1
3,9	3,9	k4
<g/>
×	×	k?
<g/>
1014	#num#	k4
Hz	Hz	kA
až	až	k9
7,9	7,9	k4
<g/>
×	×	k?
<g/>
1014	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
390	#num#	k4
<g/>
–	–	k?
<g/>
790	#num#	k4
THz	THz	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
odpovídá	odpovídat	k5eAaImIp3nS
vlnovým	vlnový	k2eAgFnPc3d1
délkám	délka	k1gFnPc3
v	v	k7c6
rozsahu	rozsah	k1gInSc6
přibližně	přibližně	k6eAd1
390	#num#	k4
<g/>
–	–	k?
<g/>
760	#num#	k4
nm	nm	k?
(	(	kIx(
<g/>
pro	pro	k7c4
fázovou	fázový	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
(	(	kIx(
<g/>
c	c	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
frekvenci	frekvence	k1gFnSc4
(	(	kIx(
<g/>
f	f	k?
)	)	kIx)
a	a	k8xC
vlnovou	vlnový	k2eAgFnSc4d1
délku	délka	k1gFnSc4
(	(	kIx(
<g/>
λ	λ	k?
<g/>
)	)	kIx)
platí	platit	k5eAaImIp3nS
vztah	vztah	k1gInSc4
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
f	f	k?
</s>
<s>
λ	λ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
=	=	kIx~
<g/>
f	f	k?
<g/>
\	\	kIx~
<g/>
lambda	lambda	k1gNnSc1
}	}	kIx)
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
od	od	k7c2
člověka	člověk	k1gMnSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
rozsah	rozsah	k1gInSc1
drobně	drobně	k6eAd1
liší	lišit	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
odlišný	odlišný	k2eAgInSc1d1
i	i	k9
pro	pro	k7c4
denní	denní	k2eAgInPc4d1
(	(	kIx(
<g/>
čípkové	čípkový	k2eAgInPc4d1
<g/>
)	)	kIx)
a	a	k8xC
soumrakové	soumrakový	k2eAgNnSc4d1
(	(	kIx(
<g/>
tyčinkové	tyčinkový	k2eAgNnSc4d1
<g/>
)	)	kIx)
vidění	vidění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
rozsah	rozsah	k1gInSc1
je	být	k5eAaImIp3nS
viditelným	viditelný	k2eAgNnSc7d1
světlem	světlo	k1gNnSc7
pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
živočichů	živočich	k1gMnPc2
vnímají	vnímat	k5eAaImIp3nP
rozsah	rozsah	k1gInSc4
jiný	jiný	k2eAgInSc4d1
-	-	kIx~
například	například	k6eAd1
včely	včela	k1gFnSc2
jej	on	k3xPp3gMnSc4
mají	mít	k5eAaImIp3nP
posunut	posunut	k2eAgInSc4d1
směrem	směr	k1gInSc7
ke	k	k7c3
kratším	krátký	k2eAgFnPc3d2
vlnovým	vlnový	k2eAgFnPc3d1
délkám	délka	k1gFnPc3
(	(	kIx(
<g/>
ultrafialové	ultrafialový	k2eAgNnSc1d1
záření	záření	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
někteří	některý	k3yIgMnPc1
plazi	plaz	k1gMnPc1
vnímají	vnímat	k5eAaImIp3nP
i	i	k9
infračervené	infračervený	k2eAgNnSc4d1
záření	záření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Rozsah	rozsah	k1gInSc1
vnímaných	vnímaný	k2eAgFnPc2d1
vlnových	vlnový	k2eAgFnPc2d1
délek	délka	k1gFnPc2
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgMnS
především	především	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
oblasti	oblast	k1gFnSc6
viditelného	viditelný	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
není	být	k5eNaImIp3nS
elektromagnetické	elektromagnetický	k2eAgNnSc1d1
záření	záření	k1gNnSc1
ze	z	k7c2
Slunce	slunce	k1gNnSc2
absorbováno	absorbovat	k5eAaBmNgNnS
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
a	a	k8xC
dopadá	dopadat	k5eAaImIp3nS
na	na	k7c4
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
proto	proto	k8xC
využitelné	využitelný	k2eAgNnSc1d1
pro	pro	k7c4
živé	živý	k2eAgInPc4d1
organismy	organismus	k1gInPc4
žijící	žijící	k2eAgInPc4d1
na	na	k7c6
povrchu	povrch	k1gInSc6
pro	pro	k7c4
zrakové	zrakový	k2eAgNnSc4d1
vnímání	vnímání	k1gNnSc4
polohy	poloha	k1gFnSc2
a	a	k8xC
rozprostraněnosti	rozprostraněnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evolucí	evoluce	k1gFnPc2
se	se	k3xPyFc4
k	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
vyvinuly	vyvinout	k5eAaPmAgFnP
příslušné	příslušný	k2eAgInPc4d1
světločivné	světločivný	k2eAgInPc4d1
orgány	orgán	k1gInPc4
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
je	být	k5eAaImIp3nS
i	i	k9
sítnice	sítnice	k1gFnSc1
lidského	lidský	k2eAgNnSc2d1
oka	oko	k1gNnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
nastavené	nastavený	k2eAgNnSc1d1
<g/>
"	"	kIx"
právě	právě	k9
na	na	k7c4
tento	tento	k3xDgInSc4
rozsah	rozsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
potřebě	potřeba	k1gFnSc3
objektivního	objektivní	k2eAgNnSc2d1
kvantitativního	kvantitativní	k2eAgNnSc2d1
vymezení	vymezení	k1gNnSc2
viditelných	viditelný	k2eAgInPc2d1
projevů	projev	k1gInPc2
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
byla	být	k5eAaImAgFnS
vedle	vedle	k7c2
sady	sada	k1gFnSc2
univerzálních	univerzální	k2eAgFnPc2d1
radiometrických	radiometrický	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
(	(	kIx(
<g/>
pro	pro	k7c4
libovolné	libovolný	k2eAgNnSc4d1
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
<g/>
)	)	kIx)
vytvořena	vytvořen	k2eAgFnSc1d1
sada	sada	k1gFnSc1
jednoznačně	jednoznačně	k6eAd1
definovaných	definovaný	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
fotometrických	fotometrický	k2eAgFnPc2d1
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
pro	pro	k7c4
světlo	světlo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídající	odpovídající	k2eAgFnPc4d1
veličiny	veličina	k1gFnPc4
obou	dva	k4xCgFnPc2
sad	sada	k1gFnPc2
spolu	spolu	k6eAd1
souvisejí	souviset	k5eAaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
na	na	k7c4
sebe	sebe	k3xPyFc4
převoditelné	převoditelný	k2eAgFnSc2d1
pomocí	pomoc	k1gFnSc7
tzv.	tzv.	kA
světelné	světelný	k2eAgFnSc2d1
účinnosti	účinnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
průměrnému	průměrný	k2eAgInSc3d1
lidskému	lidský	k2eAgInSc3d1
vnímání	vnímání	k1gNnSc6
světla	světlo	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
odlišná	odlišný	k2eAgFnSc1d1
pro	pro	k7c4
denní	denní	k2eAgNnSc4d1
a	a	k8xC
soumrakové	soumrakový	k2eAgNnSc4d1
vidění	vidění	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šíření	šíření	k1gNnSc1
světla	světlo	k1gNnSc2
</s>
<s>
Povahu	povaha	k1gFnSc4
světla	světlo	k1gNnSc2
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
vědci	vědec	k1gMnPc1
vystihnout	vystihnout	k5eAaPmF
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Platon	Platon	k1gMnSc1
si	se	k3xPyFc3
myslel	myslet	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
lidské	lidský	k2eAgNnSc1d1
oči	oko	k1gNnPc1
jsou	být	k5eAaImIp3nP
aktivními	aktivní	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
pojetí	pojetí	k1gNnSc1
optiky	optika	k1gFnSc2
bylo	být	k5eAaImAgNnS
přesně	přesně	k6eAd1
inverzní	inverzní	k2eAgMnSc1d1
k	k	k7c3
dnešní	dnešní	k2eAgFnSc3d1
paprskové	paprskový	k2eAgFnSc3d1
optice	optika	k1gFnSc3
(	(	kIx(
<g/>
stejné	stejný	k2eAgInPc4d1
paprsky	paprsek	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
opačný	opačný	k2eAgInSc1d1
směr	směr	k1gInSc1
pohybu	pohyb	k1gInSc2
světla	světlo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
fyziků	fyzik	k1gMnPc2
v	v	k7c6
dnešním	dnešní	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc2
byl	být	k5eAaImAgMnS
Newton	Newton	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
chápal	chápat	k5eAaImAgMnS
světlo	světlo	k1gNnSc4
jako	jako	k8xS,k8xC
proud	proud	k1gInSc1
částic	částice	k1gFnPc2
v	v	k7c6
mechanickém	mechanický	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
ale	ale	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
experimentem	experiment	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
lomu	lom	k1gInSc2
světla	světlo	k1gNnSc2
od	od	k7c2
kolmice	kolmice	k1gFnSc2
dopadu	dopad	k1gInSc2
při	při	k7c6
průchodu	průchod	k1gInSc6
světla	světlo	k1gNnSc2
z	z	k7c2
opticky	opticky	k6eAd1
řidšího	řídký	k2eAgNnSc2d2
prostředí	prostředí	k1gNnSc2
do	do	k7c2
opticky	opticky	k6eAd1
hustšího	hustý	k2eAgNnSc2d2
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
vzduch-sklo	vzduch-sknout	k5eAaPmAgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlnové	vlnový	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
světla	světlo	k1gNnSc2
zkoumal	zkoumat	k5eAaImAgInS
poprvé	poprvé	k6eAd1
Christiaan	Christiaan	k1gInSc1
Huygens	Huygensa	k1gFnPc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1678	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlnová	vlnový	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
světla	světlo	k1gNnSc2
dokázala	dokázat	k5eAaPmAgFnS
podat	podat	k5eAaPmF
vysvětlení	vysvětlení	k1gNnSc4
i	i	k9
mnoha	mnoho	k4c2
jiných	jiný	k2eAgInPc2d1
jevů	jev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částicový	částicový	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
světlo	světlo	k1gNnSc4
byl	být	k5eAaImAgInS
znovu	znovu	k6eAd1
oživen	oživen	k2eAgMnSc1d1
až	až	k9
kvantovou	kvantový	k2eAgFnSc7d1
fyzikou	fyzika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
platná	platný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
o	o	k7c6
dualitě	dualita	k1gFnSc6
částice	částice	k1gFnSc2
a	a	k8xC
vlnění	vlnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
chová	chovat	k5eAaImIp3nS
jako	jako	k9
vlna	vlna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
nese	nést	k5eAaImIp3nS
kvantované	kvantovaný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lom	lom	k1gInSc1
světla	světlo	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Lom	lom	k1gInSc1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Paprsky	paprsek	k1gInPc1
světla	světlo	k1gNnSc2
se	se	k3xPyFc4
při	při	k7c6
přechodu	přechod	k1gInSc2
z	z	k7c2
jednoho	jeden	k4xCgNnSc2
prostředí	prostředí	k1gNnSc2
do	do	k7c2
jiného	jiné	k1gNnSc2
lámou	lámat	k5eAaImIp3nP
<g/>
,	,	kIx,
například	například	k6eAd1
když	když	k8xS
světlo	světlo	k1gNnSc1
dopadá	dopadat	k5eAaImIp3nS
šikmo	šikmo	k6eAd1
na	na	k7c4
průhledný	průhledný	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
sklo	sklo	k1gNnSc1
nebo	nebo	k8xC
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
materiály	materiál	k1gInPc1
zpomalují	zpomalovat	k5eAaImIp3nP
světlo	světlo	k1gNnSc4
rozdílně	rozdílně	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
lom	lom	k1gInSc1
nastává	nastávat	k5eAaImIp3nS
vždy	vždy	k6eAd1
pod	pod	k7c7
jiným	jiný	k2eAgInSc7d1
úhlem	úhel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
</s>
<s>
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
v	v	k7c6
dokonalém	dokonalý	k2eAgNnSc6d1
vakuu	vakuum	k1gNnSc6
je	být	k5eAaImIp3nS
univerzální	univerzální	k2eAgNnSc1d1
fyzikální	fyzikální	k2eAgFnSc7d1
konstantou	konstanta	k1gFnSc7
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
c	c	k0
=	=	kIx~
299	#num#	k4
792	#num#	k4
458	#num#	k4
m	m	kA
<g/>
·	·	k?
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
definice	definice	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
byla	být	k5eAaImAgNnP
měřena	měřit	k5eAaImNgNnP
mnohokrát	mnohokrát	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
z	z	k7c2
prvních	první	k4xOgNnPc2
zdokumentovaných	zdokumentovaný	k2eAgNnPc2d1
měření	měření	k1gNnPc2
provedl	provést	k5eAaPmAgMnS
Dán	Dán	k1gMnSc1
Ole	Ola	k1gFnSc6
Rø	Rø	k1gFnPc2
roku	rok	k1gInSc2
1676	#num#	k4
při	při	k7c6
řešení	řešení	k1gNnSc6
námořní	námořní	k2eAgFnSc2d1
navigace	navigace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rø	Rø	k1gInSc1
pozoroval	pozorovat	k5eAaImAgInS
teleskopem	teleskop	k1gInSc7
pohyb	pohyb	k1gInSc1
planety	planeta	k1gFnSc2
Jupiter	Jupiter	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInPc4
měsíce	měsíc	k1gInPc4
Io	Io	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zaznamenal	zaznamenat	k5eAaPmAgInS
nepravidelnost	nepravidelnost	k1gFnSc4
v	v	k7c6
oběžné	oběžný	k2eAgFnSc6d1
době	doba	k1gFnSc6
Io	Io	k1gFnSc2
kolem	kolem	k7c2
Jupiteru	Jupiter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřicetkrát	čtyřicetkrát	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
průběhu	průběh	k1gInSc6
deseti	deset	k4xCc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
měřil	měřit	k5eAaImAgInS
dobu	doba	k1gFnSc4
oběhu	oběh	k1gInSc2
Io	Io	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
měření	měření	k1gNnSc1
však	však	k9
pochopitelně	pochopitelně	k6eAd1
probíhalo	probíhat	k5eAaImAgNnS
při	při	k7c6
současném	současný	k2eAgInSc6d1
oběžném	oběžný	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
z	z	k7c2
různých	různý	k2eAgFnPc2d1
vzdáleností	vzdálenost	k1gFnPc2
od	od	k7c2
Io	Io	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
poloviny	polovina	k1gFnSc2
měření	měření	k1gNnSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
rozdíl	rozdíl	k1gInSc4
22	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
již	již	k6eAd1
tehdy	tehdy	k6eAd1
oběžné	oběžný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
nebeských	nebeský	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
považovaly	považovat	k5eAaImAgFnP
za	za	k7c4
konstantní	konstantní	k2eAgInSc4d1
<g/>
,	,	kIx,
přemýšlel	přemýšlet	k5eAaImAgMnS
Rø	Rø	k1gMnSc1
o	o	k7c6
důvodu	důvod	k1gInSc6
této	tento	k3xDgFnSc2
disproporce	disproporce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
správně	správně	k6eAd1
vysvětlil	vysvětlit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
není	být	k5eNaImIp3nS
nekonečná	konečný	k2eNgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
tedy	tedy	k9
vliv	vliv	k1gInSc4
na	na	k7c4
přesnost	přesnost	k1gFnSc4
měření	měření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Rø	Rø	k1gMnSc1
s	s	k7c7
tímto	tento	k3xDgNnSc7
zjištěním	zjištění	k1gNnSc7
dále	daleko	k6eAd2
nepracoval	pracovat	k5eNaImAgInS
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
této	tento	k3xDgFnSc2
hodnoty	hodnota	k1gFnSc2
později	pozdě	k6eAd2
vypočítali	vypočítat	k5eAaPmAgMnP
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
byl	být	k5eAaImAgMnS
význačný	význačný	k2eAgMnSc1d1
holandský	holandský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
a	a	k8xC
astronom	astronom	k1gMnSc1
Christian	Christian	k1gMnSc1
Huygens	Huygens	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
odhadl	odhadnout	k5eAaPmAgInS
na	na	k7c4
220	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
úspěšné	úspěšný	k2eAgNnSc1d1
měření	měření	k1gNnSc1
pozemskými	pozemský	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
provedl	provést	k5eAaPmAgInS
Hippolyte	Hippolyt	k1gInSc5
Fizeau	Fizea	k1gMnSc6
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fizeau	Fizeaus	k1gInSc2
poslal	poslat	k5eAaPmAgMnS
svazek	svazek	k1gInSc4
světla	světlo	k1gNnSc2
na	na	k7c4
zrcadlo	zrcadlo	k1gNnSc4
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3,k3yIgNnSc3,k3yQgNnSc3
do	do	k7c2
cesty	cesta	k1gFnSc2
vložil	vložit	k5eAaPmAgMnS
točící	točící	k2eAgMnSc1d1
se	se	k3xPyFc4
ozubené	ozubený	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
známé	známý	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
otáčení	otáčení	k1gNnSc2
kola	kolo	k1gNnSc2
vypočetl	vypočíst	k5eAaPmAgMnS
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
na	na	k7c4
313	#num#	k4
000	#num#	k4
km	km	kA
<g/>
·	·	k?
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
měření	měření	k1gNnSc1
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
po	po	k7c6
přistání	přistání	k1gNnSc6
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
:	:	kIx,
po	po	k7c4
umístění	umístění	k1gNnSc4
zrcadla	zrcadlo	k1gNnSc2
na	na	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
povrch	povrch	k1gInSc4
se	se	k3xPyFc4
změřil	změřit	k5eAaPmAgInS
čas	čas	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
se	se	k3xPyFc4
odražený	odražený	k2eAgInSc1d1
paprsek	paprsek	k1gInSc1
laseru	laser	k1gInSc2
vrátil	vrátit	k5eAaPmAgInS
zpět	zpět	k6eAd1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
je	být	k5eAaImIp3nS
univerzální	univerzální	k2eAgFnSc7d1
konstantou	konstanta	k1gFnSc7
a	a	k8xC
čas	čas	k1gInSc4
lze	lze	k6eAd1
měřit	měřit	k5eAaImF
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
délky	délka	k1gFnSc2
metr	metr	k1gInSc1
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	si	k1gNnSc2
definována	definovat	k5eAaBmNgFnS
právě	právě	k6eAd1
pomocí	pomocí	k7c2
rychlosti	rychlost	k1gFnSc2
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
a	a	k8xC
hodnota	hodnota	k1gFnSc1
této	tento	k3xDgFnSc2
rychlosti	rychlost	k1gFnSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
zcela	zcela	k6eAd1
přesná	přesný	k2eAgFnSc1d1
a	a	k8xC
fixní	fixní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
případné	případný	k2eAgNnSc1d1
zpřesňování	zpřesňování	k1gNnSc1
měření	měření	k1gNnSc2
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
nezměnilo	změnit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
šíření	šíření	k1gNnSc2
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
prostředích	prostředí	k1gNnPc6
</s>
<s>
V	v	k7c6
jiném	jiný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
se	se	k3xPyFc4
světlo	světlo	k1gNnSc1
šíří	šířit	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
v	v	k7c6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
c.	c.	k?
Podíl	podíl	k1gInSc1
těchto	tento	k3xDgFnPc2
rychlostí	rychlost	k1gFnPc2
je	být	k5eAaImIp3nS
roven	roven	k2eAgInSc1d1
indexu	index	k1gInSc2
lomu	lom	k1gInSc2
daného	daný	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
n	n	k0
<g/>
,	,	kIx,
tj.	tj.	kA
n	n	k0
=	=	kIx~
c	c	k0
<g/>
/	/	kIx~
<g/>
v.	v.	k?
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
dochází	docházet	k5eAaImIp3nP
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
látek	látka	k1gFnPc2
s	s	k7c7
různými	různý	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
n	n	k0
k	k	k7c3
lomu	lom	k1gInSc2
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
prostředí	prostředí	k1gNnSc1
bez	bez	k7c2
disperze	disperze	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
případů	případ	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
index	index	k1gInSc1
lomu	lom	k1gInSc2
nezávisí	záviset	k5eNaImIp3nS
na	na	k7c6
vlnové	vlnový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostředí	prostředí	k1gNnSc6
s	s	k7c7
disperzí	disperze	k1gFnSc7
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
fázovou	fázový	k2eAgFnSc4d1
a	a	k8xC
grupovou	grupový	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
:	:	kIx,
fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
popisuje	popisovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
šíření	šíření	k1gNnSc4
ploch	plocha	k1gFnPc2
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
grupová	grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
obálce	obálka	k1gFnSc3
amplitudy	amplituda	k1gFnSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
k	k	k7c3
rychlosti	rychlost	k1gFnSc3
šíření	šíření	k1gNnSc2
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
závislost	závislost	k1gFnSc1
indexu	index	k1gInSc2
lomu	lom	k1gInSc2
na	na	k7c6
kruhové	kruhový	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
n	n	k0
<g/>
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pak	pak	k6eAd1
fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v	v	k7c6
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
}}}	}}}	k?
</s>
<s>
a	a	k8xC
grupová	grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
ω	ω	k?
</s>
<s>
d	d	k?
</s>
<s>
n	n	k0
</s>
<s>
d	d	k?
</s>
<s>
ω	ω	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v_	v_	k?
<g/>
{	{	kIx(
<g/>
g	g	kA
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
dn	dn	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
}}}}}	}}}}}	k?
</s>
<s>
</s>
<s>
Fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
není	být	k5eNaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
přenosem	přenos	k1gInSc7
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
téměř	téměř	k6eAd1
libovolných	libovolný	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
vyšších	vysoký	k2eAgFnPc2d2
než	než	k8xS
c	c	k0
nebo	nebo	k8xC
dokonce	dokonce	k9
záporných	záporný	k2eAgInPc2d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
též	též	k9
index	index	k1gInSc4
lomu	lom	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
grupová	grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
přenáší	přenášet	k5eAaImIp3nS
energie	energie	k1gFnSc1
<g/>
,	,	kIx,
nepřesahuje	přesahovat	k5eNaImIp3nS
hodnotu	hodnota	k1gFnSc4
c	c	k0
ve	v	k7c6
shodě	shoda	k1gFnSc6
s	s	k7c7
teorií	teorie	k1gFnSc7
relativity	relativita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šíření	šíření	k1gNnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
hmotě	hmota	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
vnímat	vnímat	k5eAaImF
jako	jako	k9
opakované	opakovaný	k2eAgInPc1d1
pohlcovaní	pohlcovaný	k2eAgMnPc1d1
a	a	k8xC
vyzařovaní	vyzařovaný	k2eAgMnPc1d1
fotonů	foton	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
ozáření	ozáření	k1gNnSc6
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
atom	atom	k1gInSc1
do	do	k7c2
excitovaného	excitovaný	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
setrvá	setrvat	k5eAaPmIp3nS
pouze	pouze	k6eAd1
zlomek	zlomek	k1gInSc4
času	čas	k1gInSc2
a	a	k8xC
následně	následně	k6eAd1
foton	foton	k1gInSc1
zpět	zpět	k6eAd1
vyzáří	vyzářit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
následně	následně	k6eAd1
pohltí	pohltit	k5eAaPmIp3nS
další	další	k2eAgInSc4d1
atom	atom	k1gInSc4
atd	atd	kA
<g/>
…	…	k?
Světlo	světlo	k1gNnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
pomaleji	pomale	k6eAd2
<g/>
,	,	kIx,
protože	protože	k8xS
atomy	atom	k1gInPc1
setrvávají	setrvávat	k5eAaImIp3nP
v	v	k7c6
excitovaném	excitovaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
ve	v	k7c6
hmotě	hmota	k1gFnSc6
šíří	šířit	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
stejnou	stejný	k2eAgFnSc7d1
jako	jako	k8xS,k8xC
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
neustále	neustále	k6eAd1
pohlcováno	pohlcovat	k5eAaImNgNnS
a	a	k8xC
vyzařováno	vyzařovat	k5eAaImNgNnS
atomy	atom	k1gInPc7
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
vhodně	vhodně	k6eAd1
připraveném	připravený	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
je	být	k5eAaImIp3nS
tak	tak	k9
možné	možný	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
dokonce	dokonce	k9
zastavit	zastavit	k5eAaPmF
a	a	k8xC
po	po	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
ho	on	k3xPp3gMnSc2
změnou	změna	k1gFnSc7
vlastností	vlastnost	k1gFnPc2
prostředí	prostředí	k1gNnSc4
uvolnit	uvolnit	k5eAaPmF
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
šíření	šíření	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Absorpce	absorpce	k1gFnSc1
světla	světlo	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Absorpce	absorpce	k1gFnSc2
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
světlo	světlo	k1gNnSc1
narazí	narazit	k5eAaPmIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
pohlcena	pohltit	k5eAaPmNgFnS
atomy	atom	k1gInPc7
povrchu	povrch	k1gInSc3
daného	daný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
povrch	povrch	k6eAd1wR
se	se	k3xPyFc4
velmi	velmi	k6eAd1
slabě	slabě	k6eAd1
zahřeje	zahřát	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
druh	druh	k1gInSc1
atomu	atom	k1gInSc2
absorbuje	absorbovat	k5eAaBmIp3nS
určité	určitý	k2eAgFnPc4d1
vlnové	vlnový	k2eAgFnPc4d1
délky	délka	k1gFnPc4
(	(	kIx(
<g/>
barvy	barva	k1gFnPc4
<g/>
)	)	kIx)
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
povrchu	povrch	k1gInSc2
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
vlnové	vlnový	k2eAgFnPc4d1
délky	délka	k1gFnPc4
vstřebává	vstřebávat	k5eAaImIp3nS
a	a	k8xC
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
odráží	odrážet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
List	lista	k1gFnPc2
tedy	tedy	k8xC
je	být	k5eAaImIp3nS
viděn	vidět	k5eAaImNgInS
jako	jako	k9
zelený	zelený	k2eAgInSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
absorbuje	absorbovat	k5eAaBmIp3nS
všechny	všechen	k3xTgFnPc4
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
zelené	zelená	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
my	my	k3xPp1nPc1
vidíme	vidět	k5eAaImIp1nP
jen	jen	k9
odražené	odražený	k2eAgNnSc4d1
zelené	zelený	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Interference	interference	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Interference	interference	k1gFnSc1
<g/>
#	#	kIx~
<g/>
Interference	interference	k1gFnSc1
vlnění	vlnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Šíří	šírý	k2eAgMnPc1d1
<g/>
-li	-li	k?
se	se	k3xPyFc4
danou	daný	k2eAgFnSc7d1
částí	část	k1gFnSc7
prostoru	prostor	k1gInSc2
více	hodně	k6eAd2
světelných	světelný	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
skládání	skládání	k1gNnSc4
(	(	kIx(
<g/>
superpozici	superpozice	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
koherentních	koherentní	k2eAgInPc2d1
světelných	světelný	k2eAgInPc2d1
svazků	svazek	k1gInPc2
tak	tak	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tzv.	tzv.	kA
interferenci	interference	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
vlny	vlna	k1gFnSc2
vzájemně	vzájemně	k6eAd1
posilují	posilovat	k5eAaImIp3nP
(	(	kIx(
<g/>
pozitivní	pozitivní	k2eAgInSc4d1
<g/>
,	,	kIx,
též	též	k9
konstruktivní	konstruktivní	k2eAgFnSc1d1
interference	interference	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
naopak	naopak	k6eAd1
zeslabují	zeslabovat	k5eAaImIp3nP
(	(	kIx(
<g/>
negativní	negativní	k2eAgFnSc1d1
<g/>
,	,	kIx,
destruktivní	destruktivní	k2eAgFnSc1d1
interference	interference	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Barva	barva	k1gFnSc1
a	a	k8xC
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
Různé	různý	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
světla	světlo	k1gNnSc2
vidíme	vidět	k5eAaImIp1nP
jako	jako	k9
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
od	od	k7c2
červeného	červený	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
s	s	k7c7
nejnižší	nízký	k2eAgFnSc7d3
frekvencí	frekvence	k1gFnSc7
a	a	k8xC
nejdelší	dlouhý	k2eAgFnSc7d3
vlnovou	vlnový	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
po	po	k7c4
fialové	fialový	k2eAgFnPc4d1
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
frekvencí	frekvence	k1gFnSc7
a	a	k8xC
nejkratší	krátký	k2eAgFnSc7d3
vlnovou	vlnový	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
vedle	vedle	k7c2
viditelného	viditelný	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
ultrafialové	ultrafialový	k2eAgInPc1d1
(	(	kIx(
<g/>
UV	UV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
do	do	k7c2
kratších	krátký	k2eAgFnPc2d2
vlnových	vlnový	k2eAgFnPc2d1
délek	délka	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
infračervené	infračervený	k2eAgNnSc1d1
záření	záření	k1gNnSc1
(	(	kIx(
<g/>
IR	Ir	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
do	do	k7c2
delších	dlouhý	k2eAgFnPc2d2
délek	délka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
lidé	člověk	k1gMnPc1
nevidí	vidět	k5eNaImIp3nP
IR	Ir	k1gMnSc1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
blízké	blízký	k2eAgFnPc1d1
IR	Ir	k1gMnSc1
cítit	cítit	k5eAaImF
jako	jako	k9
teplo	teplo	k1gNnSc4
svými	svůj	k3xOyFgInPc7
receptory	receptor	k1gInPc7
v	v	k7c6
pokožce	pokožka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ultrafialové	ultrafialový	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
se	se	k3xPyFc4
zase	zase	k9
na	na	k7c6
člověku	člověk	k1gMnSc6
projeví	projevit	k5eAaPmIp3nS
zvýšením	zvýšení	k1gNnSc7
pigmentace	pigmentace	k1gFnSc2
pokožky	pokožka	k1gFnSc2
<g/>
,	,	kIx,
známým	známý	k2eAgNnSc7d1
opálením	opálení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
světla	světlo	k1gNnSc2
</s>
<s>
Fotometrie	fotometrie	k1gFnPc1
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgMnPc1d1
se	se	k3xPyFc4
popisem	popis	k1gInSc7
a	a	k8xC
měřením	měření	k1gNnSc7
intenzity	intenzita	k1gFnSc2
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
např.	např.	kA
následující	následující	k2eAgFnSc1d1
fotometrické	fotometrický	k2eAgFnPc4d1
veličiny	veličina	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
jas	jas	k1gInSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
teplota	teplota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
intenzita	intenzita	k1gFnSc1
osvětlení	osvětlení	k1gNnSc2
(	(	kIx(
<g/>
jednotka	jednotka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
:	:	kIx,
lux	lux	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
světelný	světelný	k2eAgInSc1d1
tok	tok	k1gInSc1
(	(	kIx(
<g/>
jednotka	jednotka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
:	:	kIx,
lumen	lumen	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
svítivost	svítivost	k1gFnSc1
(	(	kIx(
<g/>
základní	základní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
:	:	kIx,
kandela	kandela	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kolorimetrie	kolorimetrie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
popisem	popis	k1gInSc7
a	a	k8xC
měřením	měření	k1gNnSc7
barevného	barevný	k2eAgInSc2d1
projevu	projev	k1gInSc2
vnímaného	vnímaný	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
založeném	založený	k2eAgNnSc6d1
(	(	kIx(
<g/>
vedle	vedle	k7c2
jasu	jas	k1gInSc2
<g/>
)	)	kIx)
zejména	zejména	k9
na	na	k7c6
spektrálním	spektrální	k2eAgNnSc6d1
složení	složení	k1gNnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
zastoupení	zastoupení	k1gNnSc4
vln	vlna	k1gFnPc2
různé	různý	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
viditelného	viditelný	k2eAgInSc2d1
rozsahu	rozsah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
světla	světlo	k1gNnSc2
se	se	k3xPyFc4
dále	daleko	k6eAd2
měří	měřit	k5eAaImIp3nS
např.	např.	kA
</s>
<s>
stupeň	stupeň	k1gInSc1
koherence	koherence	k1gFnSc2
(	(	kIx(
<g/>
fázové	fázový	k2eAgFnSc2d1
shody	shoda	k1gFnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
elektromagnetických	elektromagnetický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
ve	v	k7c6
světelném	světelný	k2eAgInSc6d1
svazku	svazek	k1gInSc6
<g/>
,	,	kIx,
díky	díky	k7c3
níž	jenž	k3xRgFnSc3
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
interferenčním	interferenční	k2eAgInPc3d1
jevům	jev	k1gInPc3
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
stupeň	stupeň	k1gInSc1
polarizace	polarizace	k1gFnSc2
(	(	kIx(
<g/>
shody	shoda	k1gFnSc2
roviny	rovina	k1gFnSc2
kmitů	kmit	k1gInPc2
intenzity	intenzita	k1gFnSc2
elektrického	elektrický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
elektromagnetických	elektromagnetický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
ve	v	k7c6
světelném	světelný	k2eAgInSc6d1
svazku	svazek	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
projevující	projevující	k2eAgFnSc4d1
se	se	k3xPyFc4
při	při	k7c6
odrazu	odraz	k1gInSc6
a	a	k8xC
lomu	lom	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
dvojlom	dvojlom	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
využívaný	využívaný	k2eAgMnSc1d1
při	při	k7c6
měření	měření	k1gNnSc6
optické	optický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
látek	látka	k1gFnPc2
v	v	k7c6
analytické	analytický	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
světla	světlo	k1gNnSc2
</s>
<s>
Žárovka	žárovka	k1gFnSc1
<g/>
,	,	kIx,
umělý	umělý	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
uvolňuje	uvolňovat	k5eAaImIp3nS
při	při	k7c6
vzniku	vznik	k1gInSc6
tepla	teplo	k1gNnSc2
</s>
<s>
sálání	sálání	k1gNnSc1
tepla	teplo	k1gNnSc2
(	(	kIx(
<g/>
záření	záření	k1gNnSc2
černého	černý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
záření	záření	k1gNnSc1
žárovky	žárovka	k1gFnSc2
</s>
<s>
sluneční	sluneční	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
</s>
<s>
záření	záření	k1gNnSc1
plazmatu	plazma	k1gNnSc2
(	(	kIx(
<g/>
oheň	oheň	k1gInSc1
<g/>
,	,	kIx,
oblouková	obloukový	k2eAgFnSc1d1
lampa	lampa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
atomová	atomový	k2eAgFnSc1d1
spektrální	spektrální	k2eAgFnSc1d1
emise	emise	k1gFnSc1
(	(	kIx(
<g/>
emise	emise	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
stimulované	stimulovaný	k2eAgNnSc4d1
nebo	nebo	k8xC
spontánní	spontánní	k2eAgNnSc4d1
<g/>
)	)	kIx)
</s>
<s>
laser	laser	k1gInSc1
a	a	k8xC
maser	maser	k1gInSc1
(	(	kIx(
<g/>
stimulovaná	stimulovaný	k2eAgFnSc1d1
emise	emise	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
světlo	světlo	k1gNnSc1
LED	LED	kA
diody	dioda	k1gFnSc2
</s>
<s>
plynové	plynový	k2eAgFnPc1d1
výbojky	výbojka	k1gFnPc1
</s>
<s>
urychlení	urychlení	k1gNnSc1
volného	volný	k2eAgInSc2d1
nosiče	nosič	k1gInSc2
proudu	proud	k1gInSc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
elektron	elektron	k1gInSc1
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
v	v	k7c6
synchrotronech	synchrotron	k1gInPc6
<g/>
)	)	kIx)
</s>
<s>
luminiscence	luminiscence	k1gFnSc1
</s>
<s>
Fotoluminiscence	fotoluminiscence	k1gFnSc1
</s>
<s>
Elektroluminiscence	elektroluminiscence	k1gFnSc1
</s>
<s>
Katodoluminiscence	Katodoluminiscence	k1gFnSc1
</s>
<s>
Chemiluminiscence	chemiluminiscence	k1gFnSc1
</s>
<s>
Radioluminiscence	radioluminiscence	k1gFnSc1
</s>
<s>
Triboluminiscence	triboluminiscence	k1gFnSc1
</s>
<s>
fluorescence	fluorescence	k1gFnSc1
</s>
<s>
fosforescence	fosforescence	k1gFnSc1
</s>
<s>
katodové	katodový	k2eAgNnSc1d1
záření	záření	k1gNnSc1
</s>
<s>
radioaktivní	radioaktivní	k2eAgInSc1d1
rozpad	rozpad	k1gInSc1
</s>
<s>
anihilace	anihilace	k1gFnSc1
páru	pár	k1gInSc2
částice-antičástice	částice-antičástika	k1gFnSc3
</s>
<s>
Využití	využití	k1gNnSc1
světla	světlo	k1gNnSc2
</s>
<s>
Světla	světlo	k1gNnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
přístrojích	přístroj	k1gInPc6
(	(	kIx(
<g/>
LCD	LCD	kA
obrazovkách	obrazovka	k1gFnPc6
<g/>
,	,	kIx,
DVD	DVD	kA
přehrávačích	přehrávač	k1gInPc6
<g/>
,	,	kIx,
mobilech	mobil	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
pomocí	pomoc	k1gFnSc7
se	se	k3xPyFc4
svařuje	svařovat	k5eAaImIp3nS
i	i	k8xC
řeže	řezat	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
třeba	třeba	k6eAd1
operuje	operovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
mezi	mezi	k7c4
ně	on	k3xPp3gInPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
,	,	kIx,
výrobní	výrobní	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
světla	světlo	k1gNnSc2
pozorují	pozorovat	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
i	i	k8xC
vzdálená	vzdálený	k2eAgNnPc1d1
vesmírná	vesmírný	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
vyzařují	vyzařovat	k5eAaImIp3nP
<g/>
,	,	kIx,
odráží	odrážet	k5eAaImIp3nP
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
jiným	jiný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
ovlivněna	ovlivnit	k5eAaPmNgFnS
světlem	světlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Přesné	přesný	k2eAgInPc4d1
vzájemné	vzájemný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
radiometrických	radiometrický	k2eAgFnPc2d1
a	a	k8xC
fotometrických	fotometrický	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
pro	pro	k7c4
fotopické	fotopický	k2eAgNnSc4d1
<g/>
,	,	kIx,
skotopické	skotopický	k2eAgNnSc4d1
i	i	k8xC
mezopické	mezopický	k2eAgNnSc4d1
vidění	vidění	k1gNnSc4
i	i	k8xC
vztah	vztah	k1gInSc4
k	k	k7c3
základním	základní	k2eAgFnPc3d1
kolorimetrickým	kolorimetrický	k2eAgFnPc3d1
veličinám	veličina	k1gFnPc3
jsou	být	k5eAaImIp3nP
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
definic	definice	k1gFnPc2
fotometrických	fotometrický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
vydány	vydat	k5eAaPmNgFnP
jako	jako	k8xC,k8xS
doplněk	doplněk	k1gInSc1
aktuální	aktuální	k2eAgFnSc2d1
Příručky	příručka	k1gFnSc2
SI	si	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
I	i	k8xC
grupová	grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
může	moct	k5eAaImIp3nS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
frekvencí	frekvence	k1gFnSc7
tzv.	tzv.	kA
anomální	anomální	k2eAgFnSc1d1
disperze	disperze	k1gFnSc1
převýšit	převýšit	k5eAaPmF
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
těchto	tento	k3xDgFnPc6
frekvencích	frekvence	k1gFnPc6
však	však	k9
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
velké	velký	k2eAgFnSc3d1
absorpci	absorpce	k1gFnSc3
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
prakticky	prakticky	k6eAd1
znemožňuje	znemožňovat	k5eAaImIp3nS
průchod	průchod	k1gInSc4
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplné	úplný	k2eAgNnSc1d1
vysvětlení	vysvětlení	k1gNnSc1
s	s	k7c7
průkazem	průkaz	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
princip	princip	k1gInSc1
maximální	maximální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
není	být	k5eNaImIp3nS
narušen	narušen	k2eAgMnSc1d1
<g/>
,	,	kIx,
podali	podat	k5eAaPmAgMnP
v	v	k7c6
r.	r.	kA
1914	#num#	k4
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
a	a	k8xC
Léon	Léon	k1gMnSc1
Brillouin	Brillouin	k1gMnSc1
<g/>
:	:	kIx,
Důležitá	důležitý	k2eAgFnSc1d1
pro	pro	k7c4
přenos	přenos	k1gInSc4
informace	informace	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
rychlost	rychlost	k1gFnSc4
čela	čelo	k1gNnSc2
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
vlnového	vlnový	k2eAgInSc2d1
balíku	balík	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tzv.	tzv.	kA
rychlost	rychlost	k1gFnSc1
signálu	signál	k1gInSc2
–	–	k?
ta	ten	k3xDgNnPc1
však	však	k9
zůstává	zůstávat	k5eAaImIp3nS
vždy	vždy	k6eAd1
menší	malý	k2eAgFnSc1d2
než	než	k8xS
rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Principles	Principles	k1gInSc1
governing	governing	k1gInSc1
photometry	photometr	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stratton	Stratton	k1gInSc1
J.	J.	kA
A.	A.	kA
<g/>
:	:	kIx,
Electromagnetic	Electromagnetice	k1gFnPc2
theory	theora	k1gFnSc2
<g/>
,	,	kIx,
McGraw-Hill	McGraw-Hill	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
Teorie	teorie	k1gFnSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
36.2	36.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brillouin	Brillouin	k1gMnSc1
<g/>
,	,	kIx,
Léon	Léon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wave	Wav	k1gInSc2
Propagation	Propagation	k1gInSc1
and	and	k?
Group	Group	k1gInSc1
Velocity	Velocit	k1gInPc1
(	(	kIx(
<g/>
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
KULHÁNEK	Kulhánek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpomalení	zpomalení	k1gNnSc1
a	a	k8xC
zastavení	zastavení	k1gNnSc1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aldebaran	Aldebaran	k1gInSc1
bulletin	bulletin	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1214	#num#	k4
<g/>
-	-	kIx~
<g/>
1674	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HABEL	HABEL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
a	a	k8xC
osvětlování	osvětlování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
FCC	FCC	kA
Public	publicum	k1gNnPc2
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
624	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86534	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Snellův	Snellův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
světlo	světlo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
světlo	světlo	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Vlnové	vlnový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
světla	světlo	k1gNnSc2
</s>
<s>
Václav	Václav	k1gMnSc1
Kaizr	Kaizr	k1gMnSc1
<g/>
:	:	kIx,
Měření	měření	k1gNnSc1
rychlosti	rychlost	k1gFnSc2
šíření	šíření	k1gNnSc2
světla	světlo	k1gNnSc2
</s>
<s>
Audiovizuální	audiovizuální	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
</s>
<s>
Light	Light	k1gMnSc1
Fantastic	Fantastice	k1gFnPc2
–	–	k?
seriál	seriál	k1gInSc1
o	o	k7c4
světlu	světlo	k1gNnSc3
z	z	k7c2
antropogenického	antropogenický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
×	×	k?
<g/>
60	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Paul	Paul	k1gMnSc1
Sen	sen	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4035596-2	4035596-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
10359	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85076871	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85076871	#num#	k4
</s>
