<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
frekvence	frekvence	k1gFnSc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
od	od	k7c2
3,9	3,9	k4
<g/>
×	×	kIx~
<g/>
1014	#num#	k4
Hz	Hz	kA
do	do	k7c2
7,9	7,9	k4
<g/>
×	×	kIx~
<g/>
1014	#num#	k4
Hz	Hz	kA
<g/>
,	,	kIx,
čemuž	což	k3yRnSc3
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
odpovídají	odpovídat	k5eAaImIp3nP
vlnové	vlnový	k2eAgFnPc1d1
délky	délka	k1gFnPc1
z	z	k7c2
intervalu	interval	k1gInSc2
390	#num#	k4
<g/>
–	–	k?
<g/>
760	#num#	k4
nm	nm	kA
<g/>
.	.	kIx.
</s>