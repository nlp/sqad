<p>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
jsou	být	k5eAaImIp3nP	být
svobodné	svobodný	k2eAgFnPc1d1	svobodná
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
se	se	k3xPyFc4	se
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
místních	místní	k2eAgNnPc2d1	místní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
a	a	k8xC	a
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Každých	každý	k3xTgMnPc2	každý
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
probíhají	probíhat	k5eAaImIp3nP	probíhat
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
poměrným	poměrný	k2eAgInSc7d1	poměrný
volebním	volební	k2eAgInSc7d1	volební
systémem	systém	k1gInSc7	systém
voleno	volen	k2eAgNnSc4d1	voleno
166	[number]	k4	166
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dominantní	dominantní	k2eAgFnSc2d1	dominantní
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
Fine	Fin	k1gMnSc5	Fin
Gael	Gael	k1gMnSc1	Gael
</s>
</p>
<p>
<s>
Labouristická	labouristický	k2eAgFnSc1d1	labouristická
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Fianna	Fianna	k1gFnSc1	Fianna
Fáil	Fáila	k1gFnPc2	Fáila
</s>
</p>
<p>
<s>
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
</s>
</p>
<p>
<s>
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
