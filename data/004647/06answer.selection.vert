<s>
EU	EU	kA	EU
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Smlouvy	smlouva	k1gFnPc1	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
jako	jako	k8xS	jako
Maastrichtská	maastrichtský	k2eAgFnSc1d1	Maastrichtská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
tak	tak	k6eAd1	tak
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
<g/>
.	.	kIx.	.
</s>
