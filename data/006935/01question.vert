<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
triatlonista	triatlonista	k1gMnSc1	triatlonista
a	a	k8xC	a
kvadriatlonista	kvadriatlonista	k1gMnSc1	kvadriatlonista
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sportovní	sportovní	k2eAgMnSc1d1	sportovní
fyzioterapeut	fyzioterapeut	k1gMnSc1	fyzioterapeut
a	a	k8xC	a
kondiční	kondiční	k2eAgMnSc1d1	kondiční
trenér	trenér	k1gMnSc1	trenér
české	český	k2eAgFnSc2d1	Česká
tenistky	tenistka	k1gFnSc2	tenistka
a	a	k8xC	a
wimbledonské	wimbledonský	k2eAgFnSc2d1	wimbledonská
vítězky	vítězka	k1gFnSc2	vítězka
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
?	?	kIx.	?
</s>
