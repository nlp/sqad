<s>
Jozef	Jozef	k1gMnSc1	Jozef
'	'	kIx"	'
<g/>
Pepe	Pepe	k1gInSc1	Pepe
<g/>
'	'	kIx"	'
Ivanko	Ivanka	k1gFnSc5	Ivanka
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Michalovce	Michalovce	k1gInPc1	Michalovce
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
triatlonista	triatlonista	k1gMnSc1	triatlonista
a	a	k8xC	a
kvadriatlonista	kvadriatlonista	k1gMnSc1	kvadriatlonista
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sportovní	sportovní	k2eAgMnSc1d1	sportovní
fyzioterapeut	fyzioterapeut	k1gMnSc1	fyzioterapeut
a	a	k8xC	a
kondiční	kondiční	k2eAgMnSc1d1	kondiční
trenér	trenér	k1gMnSc1	trenér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
kondiční	kondiční	k2eAgMnSc1d1	kondiční
trenér	trenér	k1gMnSc1	trenér
českých	český	k2eAgMnPc2d1	český
juniorských	juniorský	k2eAgMnPc2d1	juniorský
tenistů	tenista	k1gMnPc2	tenista
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
TK	TK	kA	TK
Agrofert	Agrofert	k1gInSc1	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
svěřencem	svěřenec	k1gMnSc7	svěřenec
Jiří	Jiří	k1gMnSc1	Jiří
Veselý	Veselý	k1gMnSc1	Veselý
a	a	k8xC	a
Adam	Adam	k1gMnSc1	Adam
Pavlásek	Pavlásek	k1gMnSc1	Pavlásek
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
jako	jako	k9	jako
kondiční	kondiční	k2eAgMnSc1d1	kondiční
trenér	trenér	k1gMnSc1	trenér
české	český	k2eAgFnSc2d1	Česká
tenistky	tenistka	k1gFnSc2	tenistka
a	a	k8xC	a
wimbledonské	wimbledonský	k2eAgFnSc2d1	wimbledonská
vítězky	vítězka	k1gFnSc2	vítězka
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Trenérství	trenérství	k1gNnSc4	trenérství
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
FTVŠ	FTVŠ	kA	FTVŠ
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenského	k2eAgFnSc2d1	Komenského
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
fyzioterapii	fyzioterapie	k1gFnSc6	fyzioterapie
na	na	k7c6	na
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
zdravotnické	zdravotnický	k2eAgFnSc6d1	zdravotnická
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
závodil	závodit	k5eAaImAgMnS	závodit
v	v	k7c6	v
triatlonu	triatlon	k1gInSc6	triatlon
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
i	i	k9	i
v	v	k7c6	v
kvadriatlonu	kvadriatlon	k1gInSc6	kvadriatlon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
tři	tři	k4xCgFnPc4	tři
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
však	však	k9	však
musel	muset	k5eAaImAgMnS	muset
závodní	závodní	k2eAgFnPc4d1	závodní
sportovní	sportovní	k2eAgFnPc4d1	sportovní
činnosti	činnost	k1gFnPc4	činnost
předčasně	předčasně	k6eAd1	předčasně
zanechat	zanechat	k5eAaPmF	zanechat
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
kondičnímu	kondiční	k2eAgNnSc3d1	kondiční
trénování	trénování	k1gNnSc3	trénování
a	a	k8xC	a
fyzioterapii	fyzioterapie	k1gFnSc3	fyzioterapie
nejprve	nejprve	k6eAd1	nejprve
slovenském	slovenský	k2eAgNnSc6d1	slovenské
Národním	národní	k2eAgNnSc6d1	národní
sportovním	sportovní	k2eAgNnSc6d1	sportovní
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejprve	nejprve	k6eAd1	nejprve
trénoval	trénovat	k5eAaImAgMnS	trénovat
slovenské	slovenský	k2eAgMnPc4d1	slovenský
rychlostní	rychlostní	k2eAgMnPc4d1	rychlostní
kanoisty	kanoista	k1gMnPc4	kanoista
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
kanoisty	kanoista	k1gMnPc4	kanoista
na	na	k7c6	na
divoké	divoký	k2eAgFnSc6d1	divoká
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
triatlonisty	triatlonista	k1gMnPc4	triatlonista
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tenisovým	tenisový	k2eAgMnSc7d1	tenisový
kondičním	kondiční	k2eAgMnSc7d1	kondiční
trenérem	trenér	k1gMnSc7	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
trénoval	trénovat	k5eAaImAgMnS	trénovat
nejprve	nejprve	k6eAd1	nejprve
indického	indický	k2eAgMnSc4d1	indický
tenistu	tenista	k1gMnSc4	tenista
Leandra	Leandr	k1gMnSc4	Leandr
Paese	Paes	k1gMnSc4	Paes
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
jeho	on	k3xPp3gMnSc4	on
deblového	deblový	k2eAgMnSc4d1	deblový
spoluhráče	spoluhráč	k1gMnSc4	spoluhráč
astralského	astralský	k2eAgMnSc4d1	astralský
tenistu	tenista	k1gMnSc4	tenista
Paula	Paul	k1gMnSc2	Paul
Hanleyho	Hanley	k1gMnSc2	Hanley
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
i	i	k8xC	i
českého	český	k2eAgMnSc4d1	český
tenistu	tenista	k1gMnSc4	tenista
Lukáše	Lukáš	k1gMnSc4	Lukáš
Dlouhého	Dlouhý	k1gMnSc4	Dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
českých	český	k2eAgMnPc2d1	český
tenistů	tenista	k1gMnPc2	tenista
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
kondiční	kondiční	k2eAgFnSc3d1	kondiční
přípravě	příprava	k1gFnSc3	příprava
slovenského	slovenský	k2eAgInSc2d1	slovenský
plaveckého	plavecký	k2eAgInSc2d1	plavecký
a	a	k8xC	a
triatlonového	triatlonový	k2eAgInSc2d1	triatlonový
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Sportteamu	Sportteam	k1gInSc6	Sportteam
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
osobní	osobní	k2eAgMnPc4d1	osobní
přátele	přítel	k1gMnPc4	přítel
patří	patřit	k5eAaImIp3nS	patřit
otec	otec	k1gMnSc1	otec
Caroliny	Carolina	k1gFnSc2	Carolina
Wozniacké	Wozniacké	k2eAgMnSc1d1	Wozniacké
Piotr	Piotr	k1gMnSc1	Piotr
Wozniacki	Wozniack	k1gFnSc2	Wozniack
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
nabízel	nabízet	k5eAaImAgInS	nabízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
trénoval	trénovat	k5eAaImAgMnS	trénovat
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
http://www.ivanko.sk/web/	[url]	k?	http://www.ivanko.sk/web/
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Jozefa	Jozef	k1gMnSc2	Jozef
Ivanka	Ivanka	k1gFnSc1	Ivanka
http://www.ivanko.sk/web/index.php?option=com_content&	[url]	k?	http://www.ivanko.sk/web/index.php?option=com_content&
http://www.tkagrofert.cz/index.php?novinka=231	[url]	k4	http://www.tkagrofert.cz/index.php?novinka=231
Jiří	Jiří	k1gMnSc1	Jiří
Veselý	Veselý	k1gMnSc1	Veselý
králem	král	k1gMnSc7	král
juniorky	juniorka	k1gFnSc2	juniorka
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
!	!	kIx.	!
</s>
<s>
http://hnonline.sk/sport/c1-52241060-ivanko-pre-hn-naucil-som-sa-vyrabat-vitazov	[url]	k1gInSc1	http://hnonline.sk/sport/c1-52241060-ivanko-pre-hn-naucil-som-sa-vyrabat-vitazov
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
http://sport.sme.sk/c/5964152/trener-ivanko-petra-je-sampionka-stale-sa-bude-zlepsovat.html	[url]	k5eAaPmAgMnS	http://sport.sme.sk/c/5964152/trener-ivanko-petra-je-sampionka-stale-sa-bude-zlepsovat.html
http://sportky.topky.sk/c/67959/treneri-o-kvitovej-skromne-dievca-uspech-ju-nezmeni	[url]	k2eAgMnPc1d1	http://sportky.topky.sk/c/67959/treneri-o-kvitovej-skromne-dievca-uspech-ju-nezmeni
http://www.pluska.sk/sport/tenis/wimbledon/ivanko-kvitova-je-skromna-baba-uspech-ju-nezmeni.html	[url]	k5eAaPmRp2nS	http://www.pluska.sk/sport/tenis/wimbledon/ivanko-kvitova-je-skromna-baba-uspech-ju-nezmeni.html
</s>
