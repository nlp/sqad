<s>
Zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
Mgr.	Mgr.	kA	Mgr.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgInPc4d1	umělecký
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MgA.	MgA.	k1gFnSc2	MgA.
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magister	k1gMnSc1	magister
artium	artium	k1gNnSc1	artium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
zkratky	zkratka	k1gFnPc1	zkratka
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
před	před	k7c4	před
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
