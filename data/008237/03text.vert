<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magistra	k1gFnPc2	magistra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
označující	označující	k2eAgMnSc1d1	označující
absolventa	absolvent	k1gMnSc4	absolvent
vysoké	vysoká	k1gFnSc2	vysoká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
Mgr.	Mgr.	kA	Mgr.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgInPc4d1	umělecký
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MgA.	MgA.	k1gFnSc2	MgA.
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magister	k1gMnSc1	magister
artium	artium	k1gNnSc1	artium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
zkratky	zkratka	k1gFnPc1	zkratka
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
před	před	k7c4	před
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dosažený	dosažený	k2eAgInSc1d1	dosažený
stupeň	stupeň	k1gInSc1	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
dle	dle	k7c2	dle
ISCED	ISCED	kA	ISCED
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
i	i	k8xC	i
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Získá	získat	k5eAaPmIp3nS	získat
ho	on	k3xPp3gMnSc4	on
absolvent	absolvent	k1gMnSc1	absolvent
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
případě	případ	k1gInSc6	případ
navazujícího	navazující	k2eAgNnSc2d1	navazující
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
případě	případ	k1gInSc6	případ
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
samostatného	samostatný	k2eAgMnSc2d1	samostatný
<g/>
,	,	kIx,	,
celistvého	celistvý	k2eAgInSc2d1	celistvý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
souvislého	souvislý	k2eAgNnSc2d1	souvislé
a	a	k8xC	a
nenavazujícího	navazující	k2eNgNnSc2d1	nenavazující
studia	studio	k1gNnSc2	studio
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgInPc1	tento
souvislé	souvislý	k2eAgInPc1d1	souvislý
magisterské	magisterský	k2eAgInPc1d1	magisterský
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
typické	typický	k2eAgInPc1d1	typický
např.	např.	kA	např.
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc1	umění
apod.	apod.	kA	apod.
Úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
absolvent	absolvent	k1gMnSc1	absolvent
<g/>
,	,	kIx,	,
magistr	magistr	k1gMnSc1	magistr
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
následně	následně	k6eAd1	následně
za	za	k7c4	za
úplatu	úplata	k1gFnSc4	úplata
podstoupit	podstoupit	k5eAaPmF	podstoupit
rigorózní	rigorózní	k2eAgNnSc4d1	rigorózní
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
malý	malý	k2eAgInSc1d1	malý
<g />
.	.	kIx.	.
</s>
<s>
doktorát	doktorát	k1gInSc1	doktorát
(	(	kIx(	(
<g/>
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
<g/>
,	,	kIx,	,
PharmDr.	PharmDr.	k1gFnSc1	PharmDr.
<g/>
,	,	kIx,	,
RNDr.	RNDr.	kA	RNDr.
<g/>
,	,	kIx,	,
ThDr.	ThDr.	k1gMnSc1	ThDr.
či	či	k8xC	či
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
diplomant	diplomant	k1gMnSc1	diplomant
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
student	student	k1gMnSc1	student
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
student	student	k1gMnSc1	student
pracující	pracující	k1gMnSc1	pracující
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
diplomové	diplomový	k2eAgFnSc6d1	Diplomová
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Magisterské	magisterský	k2eAgNnSc1d1	magisterské
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
řádně	řádně	k6eAd1	řádně
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
státnice	státnice	k1gFnSc1	státnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
Magistr	magistr	k1gMnSc1	magistr
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
tituly	titul	k1gInPc1	titul
této	tento	k3xDgFnSc2	tento
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
světě	svět	k1gInSc6	svět
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
běžně	běžně	k6eAd1	běžně
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
profesních	profesní	k2eAgInPc2d1	profesní
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
např.	např.	kA	např.
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
tak	tak	k9	tak
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bývají	bývat	k5eAaImIp3nP	bývat
užívány	užívat	k5eAaImNgInP	užívat
až	až	k9	až
tituly	titul	k1gInPc4	titul
od	od	k7c2	od
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gNnSc4	jejich
formální	formální	k2eAgNnSc4d1	formální
užívání	užívání	k1gNnSc4	užívání
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
častější	častý	k2eAgInSc1d2	častější
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k9	jako
formálně	formálně	k6eAd1	formálně
správné	správný	k2eAgNnSc1d1	správné
oslovování	oslovování	k1gNnSc1	oslovování
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
užívá	užívat	k5eAaImIp3nS	užívat
pane	pan	k1gMnSc5	pan
magistře	magistr	k1gMnSc5	magistr
/	/	kIx~	/
paní	paní	k1gFnSc3	paní
magistro	magistra	k1gFnSc5	magistra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magister	k1gMnSc1	magister
–	–	k?	–
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
představený	představený	k2eAgMnSc1d1	představený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
univerzitě	univerzita	k1gFnSc6	univerzita
byl	být	k5eAaImAgMnS	být
magister	magister	k1gMnSc1	magister
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mistr	mistr	k1gMnSc1	mistr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
původně	původně	k6eAd1	původně
prostě	prostě	k9	prostě
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgInS	stát
akademickým	akademický	k2eAgInSc7d1	akademický
titulem	titul	k1gInSc7	titul
<g/>
.	.	kIx.	.
</s>
<s>
Uděloval	udělovat	k5eAaImAgInS	udělovat
se	se	k3xPyFc4	se
bakalářům	bakalář	k1gMnPc3	bakalář
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
obhajobě	obhajoba	k1gFnSc3	obhajoba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
odborné	odborný	k2eAgFnSc2d1	odborná
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
these	these	k1gFnPc1	these
<g/>
)	)	kIx)	)
na	na	k7c6	na
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
(	(	kIx(	(
<g/>
fakultě	fakulta	k1gFnSc6	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gInSc1	jeho
celý	celý	k2eAgInSc1d1	celý
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
učitelského	učitelský	k2eAgNnSc2d1	učitelské
oprávnění	oprávnění	k1gNnSc2	oprávnění
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
licentia	licentia	k1gFnSc1	licentia
docendi	docend	k1gMnPc1	docend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oprávněným	oprávněný	k2eAgInSc7d1	oprávněný
tj.	tj.	kA	tj.
zpravidla	zpravidla	k6eAd1	zpravidla
jmenovaným	jmenovaný	k2eAgMnPc3d1	jmenovaný
učitelům	učitel	k1gMnPc3	učitel
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říkalo	říkat	k5eAaImAgNnS	říkat
magister	magistra	k1gFnPc2	magistra
regens	regens	k1gMnSc1	regens
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odborných	odborný	k2eAgFnPc6d1	odborná
fakultách	fakulta	k1gFnPc6	fakulta
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
teologické	teologický	k2eAgFnSc2d1	teologická
<g/>
,	,	kIx,	,
právnické	právnický	k2eAgFnSc2d1	právnická
a	a	k8xC	a
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uděloval	udělovat	k5eAaImAgMnS	udělovat
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc4	magister
nahradil	nahradit	k5eAaPmAgInS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Magistr	magistr	k1gMnSc1	magistr
zůstal	zůstat	k5eAaPmAgMnS	zůstat
pouze	pouze	k6eAd1	pouze
titulem	titul	k1gInSc7	titul
farmaceutů	farmaceut	k1gMnPc2	farmaceut
(	(	kIx(	(
<g/>
PhMr	PhMr	k1gMnSc1	PhMr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc2	magister
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
znovu	znovu	k6eAd1	znovu
zaveden	zaveden	k2eAgInSc1d1	zaveden
roku	rok	k1gInSc3	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradil	nahradit	k5eAaPmAgMnS	nahradit
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgInPc1d1	fakultativní
<g/>
)	)	kIx)	)
malé	malý	k2eAgInPc1d1	malý
doktoráty	doktorát	k1gInPc1	doktorát
(	(	kIx(	(
<g/>
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
PharmDr.	PharmDr.	k1gFnSc1	PharmDr.
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
<g/>
,	,	kIx,	,
RNDr.	RNDr.	kA	RNDr.
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
též	též	k9	též
i	i	k9	i
české	český	k2eAgInPc1d1	český
akademické	akademický	k2eAgInPc1d1	akademický
tituly	titul	k1gInPc1	titul
lépe	dobře	k6eAd2	dobře
odpovídaly	odpovídat	k5eAaImAgInP	odpovídat
titulům	titul	k1gInPc3	titul
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
č.	č.	k?	č.
172	[number]	k4	172
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
však	však	k9	však
ještě	ještě	k9	ještě
variantu	varianta	k1gFnSc4	varianta
MgA.	MgA.	k1gFnSc2	MgA.
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
neodlišoval	odlišovat	k5eNaImAgInS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
magisterského	magisterský	k2eAgInSc2d1	magisterský
gradu	grad	k1gInSc2	grad
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
označovalo	označovat	k5eAaImAgNnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
obsahově	obsahově	k6eAd1	obsahově
<g/>
)	)	kIx)	)
ucelená	ucelený	k2eAgFnSc1d1	ucelená
část	část	k1gFnSc1	část
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
univerzitní	univerzitní	k2eAgNnPc4d1	univerzitní
studia	studio	k1gNnPc4	studio
bez	bez	k7c2	bez
získání	získání	k1gNnSc2	získání
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
magistra	magistra	k1gFnSc1	magistra
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
přiznán	přiznat	k5eAaPmNgInS	přiznat
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
pak	pak	k6eAd1	pak
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
stavem	stav	k1gInSc7	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uvedené	uvedený	k2eAgInPc4d1	uvedený
tituly	titul	k1gInPc4	titul
udělovány	udělovat	k5eAaImNgFnP	udělovat
nebyly	být	k5eNaImAgFnP	být
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
opět	opět	k6eAd1	opět
umožněno	umožnit	k5eAaPmNgNnS	umožnit
takovéto	takovýto	k3xDgInPc4	takovýto
tituly	titul	k1gInPc4	titul
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
magistrům	magister	k1gMnPc3	magister
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
a	a	k8xC	a
zpoplatněné	zpoplatněný	k2eAgFnSc3d1	zpoplatněná
rigorózní	rigorózní	k2eAgFnSc3d1	rigorózní
zkoušce	zkouška	k1gFnSc3	zkouška
–	–	k?	–
udělení	udělení	k1gNnSc4	udělení
těchto	tento	k3xDgInPc2	tento
titulů	titul	k1gInPc2	titul
tak	tak	k6eAd1	tak
nepředchází	předcházet	k5eNaImIp3nS	předcházet
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
udělují	udělovat	k5eAaImIp3nP	udělovat
jak	jak	k6eAd1	jak
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgInPc4d1	fakultativní
<g/>
)	)	kIx)	)
malé	malý	k2eAgInPc4d1	malý
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
označují	označovat	k5eAaImIp3nP	označovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
degree	degree	k1gFnSc1	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
<g/>
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
primárně	primárně	k6eAd1	primárně
určenou	určený	k2eAgFnSc7d1	určená
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-1998	[number]	k4	1990-1998
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
–	–	k?	–
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obdobné	obdobný	k2eAgInPc4d1	obdobný
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Boloňská	boloňský	k2eAgFnSc1d1	Boloňská
reforma	reforma	k1gFnSc1	reforma
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
zaváděná	zaváděný	k2eAgFnSc1d1	zaváděná
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
doporučila	doporučit	k5eAaPmAgFnS	doporučit
užívání	užívání	k1gNnSc4	užívání
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
dřívější	dřívější	k2eAgInPc4d1	dřívější
tituly	titul	k1gInPc4	titul
doktorské	doktorský	k2eAgInPc4d1	doktorský
nebo	nebo	k8xC	nebo
diplomové	diplomový	k2eAgInPc4d1	diplomový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
nový	nový	k2eAgInSc4d1	nový
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
udělovaný	udělovaný	k2eAgInSc1d1	udělovaný
v	v	k7c6	v
navazujících	navazující	k2eAgInPc6d1	navazující
(	(	kIx(	(
<g/>
konsekutivních	konsekutivní	k2eAgInPc6d1	konsekutivní
<g/>
)	)	kIx)	)
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
užívá	užívat	k5eAaImIp3nS	užívat
anglické	anglický	k2eAgNnSc1d1	anglické
Master	master	k1gMnSc1	master
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
udělují	udělovat	k5eAaImIp3nP	udělovat
tyto	tento	k3xDgInPc1	tento
tituly	titul	k1gInPc1	titul
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
A.	A.	kA	A.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
společenských	společenský	k2eAgFnPc6d1	společenská
a	a	k8xC	a
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
oborech	obor	k1gInPc6	obor
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
Sc	Sc	k1gFnSc6	Sc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
informatice	informatika	k1gFnSc6	informatika
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Engineering	Engineering	k1gInSc1	Engineering
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
Eng	Eng	k1gFnSc6	Eng
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
inženýrských	inženýrský	k2eAgFnPc6d1	inženýrská
vědách	věda	k1gFnPc6	věda
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Laws	Laws	k1gInSc1	Laws
(	(	kIx(	(
<g/>
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
M.	M.	kA	M.
<g/>
)	)	kIx)	)
v	v	k7c6	v
právních	právní	k2eAgInPc6d1	právní
oborech	obor	k1gInPc6	obor
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výtvarných	výtvarný	k2eAgNnPc6d1	výtvarné
uměních	umění	k1gNnPc6	umění
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
Mus	Musa	k1gFnPc2	Musa
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
hudebních	hudební	k2eAgInPc6d1	hudební
oborech	obor	k1gInPc6	obor
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Education	Education	k1gInSc1	Education
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
pedagogických	pedagogický	k2eAgInPc6d1	pedagogický
a	a	k8xC	a
učitelských	učitelský	k2eAgInPc6d1	učitelský
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
udělují	udělovat	k5eAaImIp3nP	udělovat
různé	různý	k2eAgFnPc4d1	různá
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
i	i	k8xC	i
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
magisterských	magisterský	k2eAgInPc2d1	magisterský
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
tituly	titul	k1gInPc1	titul
také	také	k9	také
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
MA	MA	kA	MA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
MSc	MSc	k1gFnSc1	MSc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Laws	Laws	k1gInSc1	Laws
(	(	kIx(	(
<g/>
LLM	LLM	kA	LLM
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
of	of	k?	of
Business	business	k1gInSc1	business
Administration	Administration	k1gInSc1	Administration
(	(	kIx(	(
<g/>
MBA	MBA	kA	MBA
<g/>
)	)	kIx)	)
<g/>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
trvá	trvat	k5eAaImIp3nS	trvat
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
semestry	semestr	k1gInPc4	semestr
a	a	k8xC	a
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
Taught	Taught	k2eAgInSc4d1	Taught
Masters	Masters	k1gInSc4	Masters
a	a	k8xC	a
Research	Research	k1gInSc4	Research
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyššími	vysoký	k2eAgInPc7d2	vyšší
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
tituly	titul	k1gInPc7	titul
jsou	být	k5eAaImIp3nP	být
MA	MA	kA	MA
<g/>
,	,	kIx,	,
MSc	MSc	k1gFnSc1	MSc
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zavádějí	zavádět	k5eAaImIp3nP	zavádět
i	i	k9	i
čtyř-	čtyř-	k?	čtyř-
až	až	k9	až
pětileté	pětiletý	k2eAgInPc4d1	pětiletý
tzv.	tzv.	kA	tzv.
Integrated	Integrated	k1gInSc1	Integrated
Masters	Masters	k1gInSc1	Masters
s	s	k7c7	s
tituly	titul	k1gInPc7	titul
například	například	k6eAd1	například
MPhil	MPhil	k1gInSc1	MPhil
<g/>
,	,	kIx,	,
MMath	MMath	k1gInSc1	MMath
<g/>
,	,	kIx,	,
MRes	MRes	k1gInSc1	MRes
(	(	kIx(	(
<g/>
research	research	k1gInSc1	research
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MPhys	MPhys	k1gInSc1	MPhys
<g/>
,	,	kIx,	,
MEng	MEng	k1gInSc1	MEng
(	(	kIx(	(
<g/>
Engineering	Engineering	k1gInSc1	Engineering
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
MPharm	MPharm	k1gInSc4	MPharm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
udělovaných	udělovaný	k2eAgInPc2d1	udělovaný
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Magisterská	magisterský	k2eAgNnPc1d1	magisterské
studia	studio	k1gNnPc1	studio
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
mezi	mezi	k7c7	mezi
Graduate	Graduat	k1gInSc5	Graduat
studies	studies	k1gInSc4	studies
<g/>
,	,	kIx,	,
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
a	a	k8xC	a
trvají	trvat	k5eAaImIp3nP	trvat
obvykle	obvykle	k6eAd1	obvykle
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
tituly	titul	k1gInPc4	titul
jsou	být	k5eAaImIp3nP	být
MA	MA	kA	MA
<g/>
,	,	kIx,	,
MSc	MSc	k1gFnSc1	MSc
a	a	k8xC	a
MBA	MBA	kA	MBA
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
,	,	kIx,	,
orientovanému	orientovaný	k2eAgNnSc3d1	orientované
na	na	k7c4	na
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
mohou	moct	k5eAaImIp3nP	moct
hlásit	hlásit	k5eAaImF	hlásit
i	i	k9	i
bakaláři	bakalář	k1gMnPc1	bakalář
(	(	kIx(	(
<g/>
BA	ba	k9	ba
<g/>
)	)	kIx)	)
a	a	k8xC	a
magisterský	magisterský	k2eAgInSc4d1	magisterský
stupeň	stupeň	k1gInSc4	stupeň
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
en	en	k?	en
route	route	k5eAaPmIp2nP	route
(	(	kIx(	(
<g/>
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
právu	právo	k1gNnSc6	právo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bakaláři	bakalář	k1gMnPc1	bakalář
studují	studovat	k5eAaImIp3nP	studovat
nejprve	nejprve	k6eAd1	nejprve
tzv.	tzv.	kA	tzv.
professional	professionat	k5eAaPmAgInS	professionat
doctor	doctor	k1gInSc1	doctor
(	(	kIx(	(
<g/>
MD	MD	kA	MD
a	a	k8xC	a
JD	JD	kA	JD
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
věnovat	věnovat	k5eAaImF	věnovat
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
,	,	kIx,	,
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
magisterských	magisterský	k2eAgInPc6d1	magisterský
programech	program	k1gInPc6	program
(	(	kIx(	(
<g/>
LLM	LLM	kA	LLM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Master	master	k1gMnSc1	master
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
farmacie	farmacie	k1gFnSc2	farmacie
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
teologie	teologie	k1gFnSc2	teologie
</s>
</p>
<p>
<s>
Magister	magister	k1gMnSc1	magister
regens	regens	k1gMnSc1	regens
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Magister	magistra	k1gFnPc2	magistra
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
magistr	magistr	k1gMnSc1	magistr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
