<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
obdélníku	obdélník	k1gInSc2	obdélník
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vlající	vlající	k2eAgFnSc1d1	vlající
část	část	k1gFnSc1	část
listu	list	k1gInSc2	list
tvoří	tvořit	k5eAaImIp3nS	tvořit
středně	středně	k6eAd1	středně
modrý	modrý	k2eAgInSc1d1	modrý
svislý	svislý	k2eAgInSc1d1	svislý
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc4d1	žlutý
rovnoramenný	rovnoramenný	k2eAgInSc4d1	rovnoramenný
pravoúhlý	pravoúhlý	k2eAgInSc4d1	pravoúhlý
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
přiléhající	přiléhající	k2eAgInSc4d1	přiléhající
svými	svůj	k3xOyFgFnPc7	svůj
odvěsnami	odvěsna	k1gFnPc7	odvěsna
k	k	k7c3	k
modrému	modré	k1gNnSc3	modré
pruhu	pruh	k1gInSc2	pruh
a	a	k8xC	a
hornímu	horní	k2eAgInSc3d1	horní
okraji	okraj	k1gInSc3	okraj
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Žerďová	Žerďový	k2eAgFnSc1d1	Žerďový
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
modrá	modrý	k2eAgFnSc1d1	modrá
se	s	k7c7	s
sedmi	sedm	k4xCc2	sedm
celými	celý	k2eAgFnPc7d1	celá
hvězdami	hvězda	k1gFnPc7	hvězda
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
půlhvězdami	půlhvězda	k1gFnPc7	půlhvězda
podél	podél	k7c2	podél
přepony	přepona	k1gFnSc2	přepona
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
<g/>
Vrcholy	vrchol	k1gInPc1	vrchol
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
mají	mít	k5eAaImIp3nP	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
tři	tři	k4xCgInPc4	tři
národy	národ	k1gInPc4	národ
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
:	:	kIx,	:
Chorvaty	Chorvaty	k1gFnPc1	Chorvaty
<g/>
,	,	kIx,	,
Bosňáky	Bosňáky	k?	Bosňáky
a	a	k8xC	a
Srby	Srb	k1gMnPc7	Srb
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
procházejí	procházet	k5eAaImIp3nP	procházet
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
jakoby	jakoby	k8xS	jakoby
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
přes	přes	k7c4	přes
list	list	k1gInSc4	list
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
užito	užít	k5eAaPmNgNnS	užít
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgFnP	spojovat
s	s	k7c7	s
neutralitou	neutralita	k1gFnSc7	neutralita
a	a	k8xC	a
mírem	mír	k1gInSc7	mír
<g/>
:	:	kIx,	:
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
etnických	etnický	k2eAgFnPc2d1	etnická
zemí	zem	k1gFnPc2	zem
federace	federace	k1gFnSc2	federace
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
vlajek	vlajka	k1gFnPc2	vlajka
na	na	k7c6	na
území	území	k1gNnSc6	území
BiH	BiH	k1gFnSc2	BiH
==	==	k?	==
</s>
</p>
<p>
<s>
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
historické	historický	k2eAgFnPc4d1	historická
státní	státní	k2eAgFnPc4d1	státní
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dob	doba	k1gFnPc2	doba
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
nadvlády	nadvláda	k1gFnSc2	nadvláda
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
zelené	zelený	k2eAgFnPc1d1	zelená
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
vlajky	vlajka	k1gFnPc1	vlajka
s	s	k7c7	s
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
<g/>
,	,	kIx,	,
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
vojsky	vojsky	k6eAd1	vojsky
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
zemské	zemský	k2eAgFnSc2d1	zemská
barvy	barva	k1gFnSc2	barva
prohlášeny	prohlásit	k5eAaPmNgInP	prohlásit
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
SFRJ	SFRJ	kA	SFRJ
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
rudá	rudý	k2eAgFnSc1d1	rudá
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
vloženu	vložen	k2eAgFnSc4d1	vložena
malou	malý	k2eAgFnSc4d1	malá
jugoslávskou	jugoslávský	k2eAgFnSc4d1	jugoslávská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
pozadím	pozadí	k1gNnSc7	pozadí
a	a	k8xC	a
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používal	používat	k5eAaImAgMnS	používat
Tvrtko	Tvrtka	k1gFnSc5	Tvrtka
I.	I.	kA	I.
Kotromanić	Kotromanić	k1gMnPc3	Kotromanić
<g/>
.	.	kIx.	.
</s>
<s>
Chorvati	Chorvat	k1gMnPc1	Chorvat
a	a	k8xC	a
Srbové	Srb	k1gMnPc1	Srb
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc2	on
připodobnili	připodobnit	k5eAaPmAgMnP	připodobnit
k	k	k7c3	k
bosňáckému	bosňácký	k2eAgInSc3d1	bosňácký
národu	národ	k1gInSc3	národ
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
zadána	zadán	k2eAgFnSc1d1	zadána
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
bosenský	bosenský	k2eAgInSc1d1	bosenský
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
nesetkal	setkat	k5eNaPmAgInS	setkat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
představil	představit	k5eAaPmAgMnS	představit
nakonec	nakonec	k6eAd1	nakonec
vysoký	vysoký	k2eAgMnSc1d1	vysoký
představitel	představitel	k1gMnSc1	představitel
OSN	OSN	kA	OSN
Carlos	Carlos	k1gMnSc1	Carlos
Westerndorp	Westerndorp	k1gMnSc1	Westerndorp
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc4d1	neutrální
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnPc1	její
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
převzaty	převzít	k5eAaPmNgFnP	převzít
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
EU	EU	kA	EU
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
cípy	cíp	k1gInPc1	cíp
žlutého	žlutý	k2eAgInSc2d1	žlutý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
tři	tři	k4xCgNnPc1	tři
etnika	etnikum	k1gNnPc1	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
obletěla	obletět	k5eAaPmAgFnS	obletět
svět	svět	k1gInSc4	svět
při	při	k7c6	při
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
vlajek	vlajka	k1gFnPc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Návrhy	návrh	k1gInPc1	návrh
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc1	první
sled	sled	k1gInSc1	sled
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Druhý	druhý	k4xOgInSc1	druhý
sled	sled	k1gInSc1	sled
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgInSc1	třetí
sled	sled	k1gInSc1	sled
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
