<p>
<s>
Genk	Genk	k6eAd1	Genk
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
belgické	belgický	k2eAgFnSc6d1	belgická
provincii	provincie	k1gFnSc6	provincie
Limburk	Limburk	k1gInSc1	Limburk
poblíž	poblíž	k7c2	poblíž
Hasseltu	Hasselt	k1gInSc2	Hasselt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Albertově	Albertův	k2eAgInSc6d1	Albertův
kanálu	kanál	k1gInSc6	kanál
mezi	mezi	k7c7	mezi
Antverpami	Antverpy	k1gFnPc7	Antverpy
a	a	k8xC	a
Lutychem	Lutych	k1gInSc7	Lutych
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Vlámsku	Vlámsek	k1gInSc6	Vlámsek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
měl	mít	k5eAaImAgInS	mít
65	[number]	k4	65
321	[number]	k4	321
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
města	město	k1gNnSc2	město
a	a	k8xC	a
středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Genk	Genk	k1gInSc1	Genk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k9	jako
keltská	keltský	k2eAgFnSc1d1	keltská
vesnice	vesnice	k1gFnSc1	vesnice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pokřesťanštěn	pokřesťanštit	k5eAaPmNgInS	pokřesťanštit
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
města	město	k1gNnSc2	město
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
zbytky	zbytek	k1gInPc1	zbytek
malého	malý	k2eAgInSc2d1	malý
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genk	Genk	k1gInSc1	Genk
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněn	zmínit	k5eAaPmNgInS	zmínit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Geneche	Genech	k1gInSc2	Genech
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1108	[number]	k4	1108
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc4	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
oblast	oblast	k1gFnSc1	oblast
přiznána	přiznán	k2eAgFnSc1d1	přiznána
opatství	opatství	k1gNnSc4	opatství
Rolduc	Rolduc	k1gInSc1	Rolduc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Politicky	politicky	k6eAd1	politicky
Genk	Genk	k1gInSc1	Genk
patřil	patřit	k5eAaImAgInS	patřit
hrabství	hrabství	k1gNnSc4	hrabství
Loon	Loono	k1gNnPc2	Loono
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc3	rok
1365	[number]	k4	1365
připojeno	připojit	k5eAaPmNgNnS	připojit
ke	k	k7c3	k
knížectví	knížectví	k1gNnSc3	knížectví
Lutych	Lutycha	k1gFnPc2	Lutycha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Genk	Genk	k1gInSc1	Genk
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
malou	malý	k2eAgFnSc7d1	malá
a	a	k8xC	a
bezvýznamnou	bezvýznamný	k2eAgFnSc7d1	bezvýznamná
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
činil	činit	k5eAaImAgInS	činit
2537	[number]	k4	2537
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
klidné	klidný	k2eAgFnSc6d1	klidná
vesnici	vesnice	k1gFnSc6	vesnice
žili	žít	k5eAaImAgMnP	žít
krajináři	krajinář	k1gMnPc1	krajinář
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Neel	Neel	k1gMnSc1	Neel
Doff	Doff	k1gMnSc1	Doff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
objevil	objevit	k5eAaPmAgMnS	objevit
geolog	geolog	k1gMnSc1	geolog
a	a	k8xC	a
mineralog	mineralog	k1gMnSc1	mineralog
André	André	k1gMnSc1	André
Dumont	Dumont	k1gMnSc1	Dumont
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
vesnici	vesnice	k1gFnSc6	vesnice
As	as	k1gNnSc2	as
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
ložiska	ložisko	k1gNnSc2	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
nalezena	nalézt	k5eAaBmNgFnS	nalézt
i	i	k9	i
v	v	k7c6	v
Genku	Genk	k1gInSc6	Genk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začal	začít	k5eAaPmAgInS	začít
Genk	Genk	k1gInSc4	Genk
přitahovat	přitahovat	k5eAaImF	přitahovat
imigranty	imigrant	k1gMnPc4	imigrant
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
i	i	k9	i
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
Hasseltu	Hasselt	k1gInSc6	Hasselt
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Limburk	Limburk	k1gInSc4	Limburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
však	však	k9	však
zavřen	zavřen	k2eAgInSc1d1	zavřen
uhelný	uhelný	k2eAgInSc1d1	uhelný
důl	důl	k1gInSc1	důl
Zwartberg	Zwartberg	k1gInSc1	Zwartberg
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
vybudovat	vybudovat	k5eAaPmF	vybudovat
nová	nový	k2eAgNnPc4d1	nové
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Albertova	Albertův	k2eAgInSc2d1	Albertův
kanálu	kanál	k1gInSc2	kanál
a	a	k8xC	a
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
zavřeny	zavřít	k5eAaPmNgFnP	zavřít
zbývající	zbývající	k2eAgMnPc4d1	zbývající
dva	dva	k4xCgInPc4	dva
doly	dol	k1gInPc4	dol
Winterslag	Winterslaga	k1gFnPc2	Winterslaga
a	a	k8xC	a
Waterschei	Watersche	k1gFnSc2	Watersche
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genk	Genk	k1gInSc1	Genk
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
centrem	centr	k1gInSc7	centr
provincie	provincie	k1gFnSc2	provincie
Limburk	Limburk	k1gInSc1	Limburk
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
přes	přes	k7c4	přes
40	[number]	k4	40
000	[number]	k4	000
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
zejména	zejména	k9	zejména
továrna	továrna	k1gFnSc1	továrna
společnosti	společnost	k1gFnSc2	společnost
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
až	až	k6eAd1	až
10	[number]	k4	10
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
měl	mít	k5eAaImAgInS	mít
samotný	samotný	k2eAgInSc1d1	samotný
závod	závod	k1gInSc1	závod
4300	[number]	k4	4300
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
práci	práce	k1gFnSc4	práce
dalším	další	k2eAgMnPc3d1	další
tisícům	tisíc	k4xCgInPc3	tisíc
v	v	k7c6	v
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
subdodavatelských	subdodavatelský	k2eAgInPc6d1	subdodavatelský
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Genku	Genko	k1gNnSc6	Genko
žijí	žít	k5eAaImIp3nP	žít
příslušníci	příslušník	k1gMnPc1	příslušník
86	[number]	k4	86
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
cizinci	cizinec	k1gMnPc1	cizinec
tvoří	tvořit	k5eAaImIp3nP	tvořit
třetinu	třetina	k1gFnSc4	třetina
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Belgičanů	Belgičan	k1gMnPc2	Belgičan
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zejména	zejména	k9	zejména
lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Italové	Ital	k1gMnPc1	Ital
(	(	kIx(	(
<g/>
15	[number]	k4	15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turci	Turek	k1gMnPc1	Turek
(	(	kIx(	(
<g/>
4000	[number]	k4	4000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genk	Genk	k1gInSc1	Genk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
oficiálně	oficiálně	k6eAd1	oficiálně
městem	město	k1gNnSc7	město
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
je	být	k5eAaImIp3nS	být
skanzen	skanzen	k1gInSc1	skanzen
Bokrijk	Bokrijk	k1gInSc1	Bokrijk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zbývá	zbývat	k5eAaImIp3nS	zbývat
životem	život	k1gInSc7	život
na	na	k7c6	na
vlámském	vlámský	k2eAgInSc6d1	vlámský
venkově	venkov	k1gInSc6	venkov
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
bývalé	bývalý	k2eAgInPc1d1	bývalý
důlní	důlní	k2eAgInPc1d1	důlní
objekty	objekt	k1gInPc1	objekt
byly	být	k5eAaImAgInP	být
přestavěny	přestavět	k5eAaPmNgInP	přestavět
na	na	k7c4	na
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnPc4d1	obchodní
centra	centrum	k1gNnPc4	centrum
či	či	k8xC	či
multiplex	multiplex	k1gInSc4	multiplex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veřejnosti	veřejnost	k1gFnPc1	veřejnost
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgFnPc1d1	přístupná
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
důlní	důlní	k2eAgFnPc1d1	důlní
budovy	budova	k1gFnPc1	budova
bývalých	bývalý	k2eAgInPc2d1	bývalý
uhelných	uhelný	k2eAgInPc2d1	uhelný
dolů	dol	k1gInPc2	dol
Zwartberg	Zwartberg	k1gInSc1	Zwartberg
<g/>
,	,	kIx,	,
Waterschei	Waterschei	k1gNnSc1	Waterschei
a	a	k8xC	a
Winterslag	Winterslag	k1gInSc1	Winterslag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dolů	dol	k1gInPc2	dol
jsou	být	k5eAaImIp3nP	být
haldy	halda	k1gFnPc4	halda
vykopané	vykopaný	k2eAgFnSc2d1	vykopaná
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
zbytků	zbytek	k1gInPc2	zbytek
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
aktivitě	aktivita	k1gFnSc3	aktivita
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k8xC	i
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Genku	Genk	k1gInSc2	Genk
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
Zelené	Zelené	k2eAgNnSc1d1	Zelené
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
De	De	k?	De
Maten	mást	k5eAaImNgInS	mást
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekreační	rekreační	k2eAgFnSc1d1	rekreační
oblast	oblast	k1gFnSc1	oblast
Kattevennen	Kattevennna	k1gFnPc2	Kattevennna
s	s	k7c7	s
Europlanetáriem	Europlanetárium	k1gNnSc7	Europlanetárium
a	a	k8xC	a
skanzen	skanzen	k1gInSc1	skanzen
Bokrijk	Bokrijka	k1gFnPc2	Bokrijka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
slunečného	slunečný	k2eAgNnSc2d1	slunečné
počasí	počasí	k1gNnSc2	počasí
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
Zonnewijzerpark	Zonnewijzerpark	k1gInSc4	Zonnewijzerpark
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
dvanáct	dvanáct	k4xCc1	dvanáct
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Genku	Genk	k1gInSc2	Genk
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
E	E	kA	E
<g/>
314	[number]	k4	314
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Lovaň	Lovaň	k1gFnPc4	Lovaň
a	a	k8xC	a
Cáchy	Cáchy	k1gFnPc4	Cáchy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
přístupně	přístupně	k6eAd1	přístupně
také	také	k9	také
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
přímé	přímý	k2eAgNnSc1d1	přímé
vlakové	vlakový	k2eAgNnSc1d1	vlakové
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Hasseltem	Hasselt	k1gInSc7	Hasselt
<g/>
,	,	kIx,	,
Lovaní	Lovaň	k1gFnSc7	Lovaň
<g/>
,	,	kIx,	,
Bruselem	Brusel	k1gInSc7	Brusel
a	a	k8xC	a
Gentem	Gent	k1gInSc7	Gent
a	a	k8xC	a
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
také	také	k9	také
s	s	k7c7	s
Bruggami	Bruggy	k1gFnPc7	Bruggy
a	a	k8xC	a
belgickým	belgický	k2eAgNnSc7d1	Belgické
pobřežím	pobřeží	k1gNnSc7	pobřeží
(	(	kIx(	(
<g/>
Knokke	Knokke	k1gNnSc7	Knokke
a	a	k8xC	a
Blankenberge	Blankenberge	k1gNnSc7	Blankenberge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
s	s	k7c7	s
Hasseltem	Hasselt	k1gInSc7	Hasselt
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
1	[number]	k4	1
a	a	k8xC	a
45	[number]	k4	45
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Maastrichtem	Maastricht	k1gInSc7	Maastricht
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
45	[number]	k4	45
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
NIS	Nisa	k1gFnPc2	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
údaje	údaj	k1gInSc2	údaj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1806	[number]	k4	1806
až	až	k8xS	až
1970	[number]	k4	1970
včetně	včetně	k7c2	včetně
jsou	být	k5eAaImIp3nP	být
výsledky	výsledek	k1gInPc1	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
uvedený	uvedený	k2eAgInSc1d1	uvedený
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Neel	Neel	k1gMnSc1	Neel
Doff	Doff	k1gMnSc1	Doff
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Germeaux	Germeaux	k1gInSc1	Germeaux
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jo	jo	k9	jo
Vandeurzen	Vandeurzen	k2eAgMnSc1d1	Vandeurzen
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dirk	Dirk	k1gMnSc1	Dirk
Medved	Medved	k1gMnSc1	Medved
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ronny	Ronna	k1gFnPc1	Ronna
Gaspercic	Gaspercice	k1gInPc2	Gaspercice
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Geraerts	Geraertsa	k1gFnPc2	Geraertsa
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
De	De	k?	De
Ceulaer	Ceulaer	k1gMnSc1	Ceulaer
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
KRC	KRC	kA	KRC
Genk	Genk	k1gInSc1	Genk
postoupil	postoupit	k5eAaPmAgInS	postoupit
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
divize	divize	k1gFnSc2	divize
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
belgických	belgický	k2eAgMnPc2d1	belgický
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2002	[number]	k4	2002
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
Jupiler	Jupiler	k1gInSc1	Jupiler
League	Leagu	k1gInSc2	Leagu
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
a	a	k8xC	a
2000	[number]	k4	2000
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
Belgickém	belgický	k2eAgInSc6d1	belgický
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Družba	družba	k1gMnSc1	družba
==	==	k?	==
</s>
</p>
<p>
<s>
Cieszyn	Cieszyn	k1gNnSc1	Cieszyn
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Genk	Genka	k1gFnPc2	Genka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
v	v	k7c6	v
nizozemštině	nizozemština	k1gFnSc6	nizozemština
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
KRC	KRC	kA	KRC
Genk	Genk	k1gMnSc1	Genk
v	v	k7c6	v
nizozemštině	nizozemština	k1gFnSc6	nizozemština
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skanzenu	skanzen	k1gInSc2	skanzen
Bokrijk	Bokrijk	k1gInSc1	Bokrijk
v	v	k7c6	v
nizozemštině	nizozemština	k1gFnSc6	nizozemština
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
němčině	němčina	k1gFnSc6	němčina
</s>
</p>
