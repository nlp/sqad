<p>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
(	(	kIx(	(
<g/>
PS	PS	kA	PS
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
polymerace	polymerace	k1gFnSc2	polymerace
styrenu	styren	k1gInSc2	styren
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
polystyrenových	polystyrenový	k2eAgFnPc2d1	polystyrenová
hmot	hmota	k1gFnPc2	hmota
patří	patřit	k5eAaImIp3nS	patřit
standardní	standardní	k2eAgInSc4d1	standardní
(	(	kIx(	(
<g/>
krystalový	krystalový	k2eAgInSc4d1	krystalový
<g/>
,	,	kIx,	,
čirý	čirý	k2eAgInSc4d1	čirý
<g/>
)	)	kIx)	)
polystyren	polystyren	k1gInSc4	polystyren
<g/>
,	,	kIx,	,
houževnatý	houževnatý	k2eAgInSc4d1	houževnatý
PS	PS	kA	PS
<g/>
,	,	kIx,	,
zpěňovatelný	zpěňovatelný	k2eAgMnSc1d1	zpěňovatelný
PS	PS	kA	PS
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytlačovaný	vytlačovaný	k2eAgInSc4d1	vytlačovaný
pěnový	pěnový	k2eAgInSc4d1	pěnový
PS	PS	kA	PS
(	(	kIx(	(
<g/>
XPS	XPS	kA	XPS
<g/>
)	)	kIx)	)
a	a	k8xC	a
kopolymery	kopolymer	k1gInPc1	kopolymer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
standardního	standardní	k2eAgInSc2d1	standardní
polystyrénu	polystyrén	k1gInSc2	polystyrén
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
např.	např.	kA	např.
čiré	čirý	k2eAgInPc1d1	čirý
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vzhledem	vzhledem	k7c3	vzhledem
připomínají	připomínat	k5eAaImIp3nP	připomínat
plexisklo	plexisklo	k1gNnSc4	plexisklo
-	-	kIx~	-
příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různé	různý	k2eAgInPc4d1	různý
obaly	obal	k1gInPc4	obal
<g/>
,	,	kIx,	,
zkumavky	zkumavka	k1gFnPc4	zkumavka
používané	používaný	k2eAgFnPc4d1	používaná
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
a	a	k8xC	a
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
,	,	kIx,	,
Petriho	Petri	k1gMnSc2	Petri
misky	miska	k1gFnSc2	miska
<g/>
,	,	kIx,	,
spektrofotometrické	spektrofotometrický	k2eAgFnSc2d1	spektrofotometrická
kyvety	kyveta	k1gFnSc2	kyveta
atd.	atd.	kA	atd.
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
polystyrenu	polystyren	k1gInSc2	polystyren
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
křehkost	křehkost	k1gFnSc1	křehkost
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
tepelná	tepelný	k2eAgFnSc1d1	tepelná
odolnost	odolnost	k1gFnSc1	odolnost
(	(	kIx(	(
<g/>
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
polystyrenu	polystyren	k1gInSc2	polystyren
deformují	deformovat	k5eAaImIp3nP	deformovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
kratší	krátký	k2eAgNnSc1d2	kratší
než	než	k8xS	než
asi	asi	k9	asi
320	[number]	k4	320
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
lámavý	lámavý	k2eAgInSc4d1	lámavý
a	a	k8xC	a
levný	levný	k2eAgInSc4d1	levný
plast	plast	k1gInSc4	plast
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
vyložených	vyložený	k2eAgFnPc2d1	vyložená
tašek	taška	k1gFnPc2	taška
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obalů	obal	k1gInPc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
syntetický	syntetický	k2eAgInSc1d1	syntetický
aromatický	aromatický	k2eAgInSc1d1	aromatický
polymer	polymer	k1gInSc1	polymer
<g/>
,	,	kIx,	,
utvářený	utvářený	k2eAgInSc1d1	utvářený
monomerem	monomer	k1gInSc7	monomer
styrenem	styren	k1gInSc7	styren
<g/>
,	,	kIx,	,
petrochemickou	petrochemický	k2eAgFnSc7d1	petrochemická
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
pěna	pěn	k2eAgFnSc1d1	pěna
nebo	nebo	k8xC	nebo
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
odolný	odolný	k2eAgMnSc1d1	odolný
proti	proti	k7c3	proti
kyslíku	kyslík	k1gInSc3	kyslík
<g/>
,	,	kIx,	,
vodním	vodní	k2eAgFnPc3d1	vodní
parám	para	k1gFnPc3	para
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
nízký	nízký	k2eAgInSc1d1	nízký
bod	bod	k1gInSc1	bod
měknutí	měknutí	k1gNnSc2	měknutí
<g/>
.	.	kIx.	.
</s>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
nejužívanější	užívaný	k2eAgInSc1d3	nejužívanější
termoplast	termoplast	k1gInSc1	termoplast
po	po	k7c6	po
PE	PE	kA	PE
<g/>
,	,	kIx,	,
PPO	PPO	kA	PPO
a	a	k8xC	a
PVC	PVC	kA	PVC
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
12,4	[number]	k4	12,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
tun	tuna	k1gFnPc2	tuna
tuhých	tuhý	k2eAgFnPc2d1	tuhá
PS	PS	kA	PS
a	a	k8xC	a
6,8	[number]	k4	6,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
tun	tuna	k1gFnPc2	tuna
pěnového	pěnový	k2eAgInSc2d1	pěnový
PS	PS	kA	PS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
čirý	čirý	k2eAgMnSc1d1	čirý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obarven	obarven	k2eAgInSc4d1	obarven
barvivy	barvivo	k1gNnPc7	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
použití	použití	k1gNnSc4	použití
známe	znát	k5eAaImIp1nP	znát
z	z	k7c2	z
praxe	praxe	k1gFnSc2	praxe
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
obalů	obal	k1gInPc2	obal
(	(	kIx(	(
<g/>
cédéčka	cédéčka	k?	cédéčka
a	a	k8xC	a
DVDéčka	DVDéčko	k1gNnPc4	DVDéčko
<g/>
,	,	kIx,	,
přepravky	přepravka	k1gFnPc4	přepravka
<g/>
,	,	kIx,	,
víka	víko	k1gNnPc4	víko
<g/>
,	,	kIx,	,
láhve	láhev	k1gFnPc4	láhev
<g/>
,	,	kIx,	,
podnosy	podnos	k1gInPc4	podnos
<g/>
,	,	kIx,	,
štamprlata	štamprlata	k?	štamprlata
<g/>
,	,	kIx,	,
kelímky	kelímek	k1gInPc4	kelímek
<g/>
,	,	kIx,	,
nože	nůž	k1gInPc4	nůž
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
tvarovatelný	tvarovatelný	k2eAgInSc1d1	tvarovatelný
polymer	polymer	k1gInSc1	polymer
<g/>
,	,	kIx,	,
polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
pevnou	pevný	k2eAgFnSc7d1	pevná
látkou	látka	k1gFnSc7	látka
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začne	začít	k5eAaPmIp3nS	začít
měknout	měknout	k5eAaImF	měknout
jakmile	jakmile	k8xS	jakmile
ho	on	k3xPp3gNnSc4	on
začnete	začít	k5eAaPmIp2nP	začít
ohřívat	ohřívat	k5eAaImF	ohřívat
nad	nad	k7c4	nad
100	[number]	k4	100
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
ztuhne	ztuhnout	k5eAaPmIp3nS	ztuhnout
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
začnete	začít	k5eAaPmIp2nP	začít
chladit	chladit	k5eAaImF	chladit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
biodegradace	biodegradace	k1gFnSc1	biodegradace
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
pomalá	pomalý	k2eAgFnSc1d1	pomalá
<g/>
,	,	kIx,	,
veřejnosti	veřejnost	k1gFnPc4	veřejnost
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xS	jako
pěnový	pěnový	k2eAgInSc1d1	pěnový
tepelně	tepelně	k6eAd1	tepelně
izolační	izolační	k2eAgInSc1d1	izolační
polymerní	polymerní	k2eAgInSc1d1	polymerní
materiál	materiál	k1gInSc1	materiál
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
výklad	výklad	k1gInSc1	výklad
významu	význam	k1gInSc2	význam
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
polystyren	polystyren	k1gInSc1	polystyren
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
a	a	k8xC	a
neúplný	úplný	k2eNgInSc1d1	neúplný
<g/>
.	.	kIx.	.
</s>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
tepelně	tepelně	k6eAd1	tepelně
zpracovatelných	zpracovatelný	k2eAgInPc2d1	zpracovatelný
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
termoplastů	termoplast	k1gInPc2	termoplast
<g/>
.	.	kIx.	.
</s>
<s>
Objemem	objem	k1gInSc7	objem
zpracování	zpracování	k1gNnSc2	zpracování
ho	on	k3xPp3gInSc4	on
předstihují	předstihovat	k5eAaImIp3nP	předstihovat
jen	jen	k9	jen
polyethylen	polyethylen	k1gInSc4	polyethylen
<g/>
,	,	kIx,	,
polypropylen	polypropylen	k1gInSc4	polypropylen
a	a	k8xC	a
polyvinylchlorid	polyvinylchlorid	k1gInSc4	polyvinylchlorid
(	(	kIx(	(
<g/>
PVC	PVC	kA	PVC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
izolován	izolován	k2eAgInSc1d1	izolován
kapalný	kapalný	k2eAgInSc1d1	kapalný
styren	styren	k1gInSc1	styren
monomer	monomer	k1gInSc1	monomer
z	z	k7c2	z
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
borovice	borovice	k1gFnSc2	borovice
Eduardem	Eduard	k1gMnSc7	Eduard
Simonem	Simon	k1gMnSc7	Simon
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Uložený	uložený	k2eAgInSc1d1	uložený
vzorek	vzorek	k1gInSc1	vzorek
náhodně	náhodně	k6eAd1	náhodně
zpolymerizoval	zpolymerizovat	k5eAaPmAgInS	zpolymerizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
praktické	praktický	k2eAgFnPc4d1	praktická
aplikace	aplikace	k1gFnPc4	aplikace
se	se	k3xPyFc4	se
PS	PS	kA	PS
dočkal	dočkat	k5eAaPmAgMnS	dočkat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnSc4	jeho
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
zahájil	zahájit	k5eAaPmAgInS	zahájit
německý	německý	k2eAgInSc1d1	německý
chemický	chemický	k2eAgInSc1d1	chemický
koncern	koncern	k1gInSc1	koncern
IG	IG	kA	IG
Farben	Farbna	k1gFnPc2	Farbna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
polystyrenu	polystyren	k1gInSc2	polystyren
===	===	k?	===
</s>
</p>
<p>
<s>
Styren	styren	k1gInSc1	styren
monomer	monomer	k1gInSc1	monomer
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
Eduardem	Eduard	k1gMnSc7	Eduard
Simonem	Simon	k1gMnSc7	Simon
<g/>
,	,	kIx,	,
berlínským	berlínský	k2eAgMnSc7d1	berlínský
lékárníkem	lékárník	k1gMnSc7	lékárník
<g/>
.	.	kIx.	.
</s>
<s>
Izoloval	izolovat	k5eAaBmAgMnS	izolovat
ho	on	k3xPp3gNnSc4	on
ze	z	k7c2	z
storaxu	storax	k1gInSc2	storax
<g/>
,	,	kIx,	,
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
tureckého	turecký	k2eAgInSc2d1	turecký
stromu	strom	k1gInSc2	strom
Liquidambar	Liquidambara	k1gFnPc2	Liquidambara
orientalis	orientalis	k1gFnSc2	orientalis
<g/>
.	.	kIx.	.
</s>
<s>
Destiloval	destilovat	k5eAaImAgInS	destilovat
olejovitou	olejovitý	k2eAgFnSc4d1	olejovitá
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
,	,	kIx,	,
monomer	monomer	k1gInSc4	monomer
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
styrol	styrol	k1gInSc4	styrol
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
styrol	styrol	k1gInSc1	styrol
tuhnul	tuhnout	k5eAaImAgInS	tuhnout
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
vzdušnému	vzdušný	k2eAgInSc3d1	vzdušný
kyslíku	kyslík	k1gInSc3	kyslík
a	a	k8xC	a
měnil	měnit	k5eAaImAgMnS	měnit
se	se	k3xPyFc4	se
v	v	k7c4	v
"	"	kIx"	"
<g/>
puding	puding	k1gInSc4	puding
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napadlo	napadnout	k5eAaPmAgNnS	napadnout
ho	on	k3xPp3gMnSc4	on
zdvojit	zdvojit	k5eAaPmF	zdvojit
oxid	oxid	k1gInSc4	oxid
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
(	(	kIx(	(
<g/>
styroloxid	styroloxid	k1gInSc1	styroloxid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
Jamajčan	Jamajčan	k1gMnSc1	Jamajčan
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
John	John	k1gMnSc1	John
Buddle	Buddle	k1gMnSc1	Buddle
Blyth	Blyth	k1gMnSc1	Blyth
a	a	k8xC	a
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
August	August	k1gMnSc1	August
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
von	von	k1gInSc1	von
Hofmann	Hofmann	k1gMnSc1	Hofmann
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
za	za	k7c2	za
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	co	k3yInSc7	co
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
<g/>
,	,	kIx,	,
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
metastyrol	metastyrol	k1gInSc4	metastyrol
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
chemicky	chemicky	k6eAd1	chemicky
identické	identický	k2eAgNnSc1d1	identické
se	s	k7c7	s
styroloxidem	styroloxid	k1gInSc7	styroloxid
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ukázala	ukázat	k5eAaPmAgFnS	ukázat
později	pozdě	k6eAd2	pozdě
chemická	chemický	k2eAgFnSc1d1	chemická
analýza	analýza	k1gFnSc1	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrobu	výroba	k1gFnSc4	výroba
metylstyrolu	metylstyrol	k1gInSc2	metylstyrol
<g/>
/	/	kIx~	/
<g/>
oxidstyrolu	oxidstyrol	k1gInSc2	oxidstyrol
jako	jako	k8xC	jako
polymerační	polymerační	k2eAgInSc1d1	polymerační
proces	proces	k1gInSc1	proces
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
Marcelin	Marcelin	k2eAgInSc4d1	Marcelin
Berthelot	Berthelot	k1gInSc4	Berthelot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahříváním	zahřívání	k1gNnSc7	zahřívání
styrenu	styren	k1gInSc2	styren
začíná	začínat	k5eAaImIp3nS	začínat
polymerace	polymerace	k1gFnSc1	polymerace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
končí	končit	k5eAaImIp3nS	končit
zrozením	zrození	k1gNnSc7	zrození
makromolekuly	makromolekula	k1gFnSc2	makromolekula
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tak	tak	k9	tak
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
i	i	k9	i
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Hermann	Hermann	k1gMnSc1	Hermann
Staudinger	Staudinger	k1gMnSc1	Staudinger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
polystyren	polystyren	k1gInSc1	polystyren
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
pojmenováváme	pojmenovávat	k5eAaImIp1nP	pojmenovávat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
IG	IG	kA	IG
Farben	Farbna	k1gFnPc2	Farbna
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
styren	styren	k1gInSc4	styren
a	a	k8xC	a
polystyren	polystyren	k1gInSc4	polystyren
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
Ludwigshafenu	Ludwigshafena	k1gFnSc4	Ludwigshafena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
nahradí	nahradit	k5eAaPmIp3nS	nahradit
tehdy	tehdy	k6eAd1	tehdy
používaný	používaný	k2eAgInSc1d1	používaný
zinek	zinek	k1gInSc1	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
veliký	veliký	k2eAgInSc4d1	veliký
vyhřívaný	vyhřívaný	k2eAgInSc4d1	vyhřívaný
průduch	průduch	k1gInSc4	průduch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odděloval	oddělovat	k5eAaImAgInS	oddělovat
výsledný	výsledný	k2eAgInSc4d1	výsledný
polystyren	polystyren	k1gInSc4	polystyren
teplým	teplý	k2eAgInSc7d1	teplý
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
krájel	krájet	k5eAaImAgMnS	krájet
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
kousky	kousek	k1gInPc4	kousek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
je	být	k5eAaImIp3nS	být
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
polystyrenová	polystyrenový	k2eAgFnSc1d1	polystyrenová
pěna	pěna	k1gFnSc1	pěna
vytlačováním	vytlačování	k1gNnSc7	vytlačování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1949	[number]	k4	1949
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
inženýr	inženýr	k1gMnSc1	inženýr
Fritz	Fritz	k1gMnSc1	Fritz
Stastný	Stastný	k2eAgMnSc1d1	Stastný
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
polystyrenové	polystyrenový	k2eAgFnPc4d1	polystyrenová
kuličky	kulička	k1gFnPc4	kulička
suspenzní	suspenzní	k2eAgFnPc4d1	suspenzní
polymerací	polymerace	k1gFnSc7	polymerace
styrenu	styren	k1gInSc2	styren
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
nadouvadla	nadouvadlo	k1gNnSc2	nadouvadlo
typu	typ	k1gInSc2	typ
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
pentan	pentan	k1gInSc4	pentan
<g/>
.	.	kIx.	.
</s>
<s>
Perličky	perlička	k1gFnPc1	perlička
jsou	být	k5eAaImIp3nP	být
surovinou	surovina	k1gFnSc7	surovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
předpěnění	předpěnění	k1gNnSc6	předpěnění
<g/>
,	,	kIx,	,
zrání	zrání	k1gNnSc6	zrání
a	a	k8xC	a
lisování	lisování	k1gNnSc6	lisování
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pěnové	pěnový	k2eAgNnSc4d1	pěnové
PS	PS	kA	PS
výrobky	výrobek	k1gInPc7	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Stastny	Stastna	k1gFnPc1	Stastna
a	a	k8xC	a
BASF	BASF	kA	BASF
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
patentu	patent	k1gInSc2	patent
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vzniká	vznikat	k5eAaImIp3nS	vznikat
pěnový	pěnový	k2eAgInSc4d1	pěnový
polystyren	polystyren	k1gInSc4	polystyren
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
vidět	vidět	k5eAaImF	vidět
návštěvníci	návštěvník	k1gMnPc1	návštěvník
Kunstoff	Kunstoff	k1gInSc4	Kunstoff
Messe	Messe	k1gFnSc1	Messe
1952	[number]	k4	1952
v	v	k7c6	v
Dusseldorfu	Dusseldorf	k1gInSc6	Dusseldorf
<g/>
.	.	kIx.	.
</s>
<s>
Produkt	produkt	k1gInSc1	produkt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
styropor	styropor	k1gInSc1	styropor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěnový	pěnový	k2eAgInSc1d1	pěnový
polystyren	polystyren	k1gInSc1	polystyren
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
98	[number]	k4	98
<g/>
%	%	kIx~	%
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
2	[number]	k4	2
<g/>
%	%	kIx~	%
polymeru	polymer	k1gInSc2	polymer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
křehký	křehký	k2eAgInSc1d1	křehký
plast	plast	k1gInSc1	plast
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dobře	dobře	k6eAd1	dobře
odolává	odolávat	k5eAaImIp3nS	odolávat
kyselinám	kyselina	k1gFnPc3	kyselina
a	a	k8xC	a
zásadám	zásada	k1gFnPc3	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stárnutí	stárnutí	k1gNnSc6	stárnutí
křehne	křehnout	k5eAaImIp3nS	křehnout
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Neodolává	odolávat	k5eNaImIp3nS	odolávat
organickým	organický	k2eAgNnPc3d1	organické
rozpouštědlům	rozpouštědlo	k1gNnPc3	rozpouštědlo
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
benzénu	benzéna	k1gFnSc4	benzéna
<g/>
,	,	kIx,	,
aldehydům	aldehyd	k1gInPc3	aldehyd
a	a	k8xC	a
ketonům	keton	k1gInPc3	keton
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
citlivý	citlivý	k2eAgInSc1d1	citlivý
vůči	vůči	k7c3	vůči
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
málo	málo	k6eAd1	málo
odolný	odolný	k2eAgMnSc1d1	odolný
vůči	vůči	k7c3	vůči
teplotě	teplota	k1gFnSc3	teplota
(	(	kIx(	(
<g/>
jen	jen	k9	jen
asi	asi	k9	asi
do	do	k7c2	do
80	[number]	k4	80
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šuta	šuta	k1gFnSc1	šuta
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
nezreagovaný	zreagovaný	k2eNgInSc1d1	nezreagovaný
monomer	monomer	k1gInSc1	monomer
styren	styren	k1gInSc1	styren
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
a	a	k8xC	a
karcinogenní	karcinogenní	k2eAgInSc1d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
není	být	k5eNaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
styrenu	styren	k1gInSc2	styren
je	být	k5eAaImIp3nS	být
145	[number]	k4	145
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
při	při	k7c6	při
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
Množství	množství	k1gNnSc1	množství
nezreagovaného	zreagovaný	k2eNgInSc2d1	nezreagovaný
styrenu	styren	k1gInSc2	styren
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
ppm	ppm	k?	ppm
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
styren	styren	k1gInSc1	styren
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
2B	[number]	k4	2B
-	-	kIx~	-
podezřelý	podezřelý	k2eAgInSc4d1	podezřelý
karcinogen	karcinogen	k1gInSc4	karcinogen
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
chemická	chemický	k2eAgFnSc1d1	chemická
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
ECHA	echo	k1gNnSc2	echo
<g/>
)	)	kIx)	)
nezařadila	zařadit	k5eNaPmAgFnS	zařadit
styren	styren	k1gInSc4	styren
mezi	mezi	k7c4	mezi
látky	látka	k1gFnPc4	látka
vzbuzující	vzbuzující	k2eAgFnSc2d1	vzbuzující
značné	značný	k2eAgFnSc2d1	značná
obavy	obava	k1gFnSc2	obava
(	(	kIx(	(
<g/>
příloha	příloha	k1gFnSc1	příloha
XIV	XIV	kA	XIV
-	-	kIx~	-
REACH	REACH	kA	REACH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
přednostní	přednostní	k2eAgFnSc4d1	přednostní
autorizaci	autorizace	k1gFnSc4	autorizace
<g/>
.	.	kIx.	.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
výrobci	výrobce	k1gMnPc1	výrobce
polystyrenu	polystyren	k1gInSc2	polystyren
deklarují	deklarovat	k5eAaBmIp3nP	deklarovat
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
jako	jako	k8xC	jako
vyhovující	vyhovující	k2eAgFnPc4d1	vyhovující
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
,	,	kIx,	,
s	s	k7c7	s
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
pro	pro	k7c4	pro
dětské	dětský	k2eAgFnPc4d1	dětská
hračky	hračka	k1gFnPc4	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Karcinogenita	karcinogenita	k1gFnSc1	karcinogenita
styrenu	styren	k1gInSc2	styren
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
a	a	k8xC	a
styren	styren	k1gInSc1	styren
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
žádném	žádný	k3yNgInSc6	žádný
seznamu	seznam	k1gInSc6	seznam
karcinogenních	karcinogenní	k2eAgFnPc2d1	karcinogenní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
hořlavý	hořlavý	k2eAgInSc4d1	hořlavý
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
polystyren	polystyren	k1gInSc1	polystyren
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
<g/>
,	,	kIx,	,
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
náraz	náraz	k1gInSc4	náraz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
časem	čas	k1gInSc7	čas
a	a	k8xC	a
povětrnostními	povětrnostní	k2eAgInPc7d1	povětrnostní
vlivy	vliv	k1gInPc7	vliv
rychle	rychle	k6eAd1	rychle
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
(	(	kIx(	(
<g/>
křehne	křehnout	k5eAaImIp3nS	křehnout
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mnoho	mnoho	k4c4	mnoho
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
vůči	vůči	k7c3	vůči
teplotě	teplota	k1gFnSc3	teplota
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
-	-	kIx~	-
použitelný	použitelný	k2eAgMnSc1d1	použitelný
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
Měknutí	měknutí	k1gNnSc1	měknutí
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
90	[number]	k4	90
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
sklovitý	sklovitý	k2eAgInSc1d1	sklovitý
přechod	přechod	k1gInSc1	přechod
při	při	k7c6	při
95	[number]	k4	95
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
pěnového	pěnový	k2eAgInSc2d1	pěnový
polystyrenu	polystyren	k1gInSc2	polystyren
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
zpomalovač	zpomalovač	k1gInSc1	zpomalovač
hoření	hoření	k2eAgInSc4d1	hoření
hexabromcyklododekan	hexabromcyklododekan	k1gInSc4	hexabromcyklododekan
(	(	kIx(	(
<g/>
HBCDD	HBCDD	kA	HBCDD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
jako	jako	k9	jako
zpomalovač	zpomalovač	k1gInSc1	zpomalovač
hoření	hoření	k2eAgInSc1d1	hoření
především	především	k6eAd1	především
do	do	k7c2	do
izolačního	izolační	k2eAgInSc2d1	izolační
expandovaného	expandovaný	k2eAgInSc2d1	expandovaný
a	a	k8xC	a
extrudovaného	extrudovaný	k2eAgInSc2d1	extrudovaný
polystyrenu	polystyren	k1gInSc2	polystyren
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
a	a	k8xC	a
XPS	XPS	kA	XPS
<g/>
))	))	k?	))
používaného	používaný	k2eAgNnSc2d1	používané
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
persistentní	persistentní	k2eAgFnPc4d1	persistentní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
nerozkládá	rozkládat	k5eNaImIp3nS	rozkládat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
persistentní	persistentní	k2eAgFnPc4d1	persistentní
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
POP	pop	k1gMnSc1	pop
<g/>
)	)	kIx)	)
dle	dle	k7c2	dle
Stockholmské	stockholmský	k2eAgFnSc2d1	Stockholmská
úmluvy	úmluva	k1gFnSc2	úmluva
OSN	OSN	kA	OSN
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
zakázána	zakázat	k5eAaPmNgFnS	zakázat
jeho	jeho	k3xOp3gFnSc1	jeho
aplikace	aplikace	k1gFnSc1	aplikace
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
EPS	EPS	kA	EPS
suroviny	surovina	k1gFnSc2	surovina
-	-	kIx~	-
společnost	společnost	k1gFnSc1	společnost
Synthos	Synthos	k1gInSc4	Synthos
Kralupy	Kralupy	k1gInPc7	Kralupy
neaplikuje	aplikovat	k5eNaBmIp3nS	aplikovat
HBCDD	HBCDD	kA	HBCDD
od	od	k7c2	od
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
zpracovatelé	zpracovatel	k1gMnPc1	zpracovatel
-	-	kIx~	-
výrobci	výrobce	k1gMnPc1	výrobce
pěnového	pěnový	k2eAgMnSc2d1	pěnový
PS	PS	kA	PS
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
pouze	pouze	k6eAd1	pouze
typy	typa	k1gFnPc4	typa
EPS	EPS	kA	EPS
bez	bez	k7c2	bez
HBCDD	HBCDD	kA	HBCDD
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
nebylo	být	k5eNaImAgNnS	být
u	u	k7c2	u
EPS	EPS	kA	EPS
s	s	k7c7	s
HBCDD	HBCDD	kA	HBCDD
prokázáno	prokázán	k2eAgNnSc1d1	prokázáno
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
Nehrozí	hrozit	k5eNaImIp3nS	hrozit
žádné	žádný	k3yNgNnSc4	žádný
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
ani	ani	k8xC	ani
u	u	k7c2	u
zateplených	zateplený	k2eAgFnPc2d1	zateplená
budov	budova	k1gFnPc2	budova
s	s	k7c7	s
EPS	EPS	kA	EPS
s	s	k7c7	s
HBCDD	HBCDD	kA	HBCDD
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
a	a	k8xC	a
narušení	narušení	k1gNnSc2	narušení
hormonální	hormonální	k2eAgFnSc2d1	hormonální
rovnováhy	rovnováha	k1gFnSc2	rovnováha
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
nese	nést	k5eAaImIp3nS	nést
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
–	–	k?	–
když	když	k8xS	když
pracujete	pracovat	k5eAaImIp2nP	pracovat
s	s	k7c7	s
polystyrenem	polystyren	k1gInSc7	polystyren
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
HBCD	HBCD	kA	HBCD
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vdechujete	vdechovat	k5eAaImIp2nP	vdechovat
mikročástice	mikročástice	k1gFnPc1	mikročástice
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
takový	takový	k3xDgInSc1	takový
polystyren	polystyren	k1gInSc1	polystyren
likviduje	likvidovat	k5eAaBmIp3nS	likvidovat
neodborným	odborný	k2eNgInSc7d1	neodborný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
hexabromcyklododekan	hexabromcyklododekan	k1gInSc1	hexabromcyklododekan
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
potravinového	potravinový	k2eAgInSc2d1	potravinový
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Používané	používaný	k2eAgInPc1d1	používaný
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
polystyren	polystyren	k1gInSc1	polystyren
(	(	kIx(	(
<g/>
krystalový	krystalový	k2eAgMnSc1d1	krystalový
PS	PS	kA	PS
<g/>
,	,	kIx,	,
GPPS	GPPS	kA	GPPS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čirý	čirý	k2eAgMnSc1d1	čirý
a	a	k8xC	a
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Houževnatý	houževnatý	k2eAgInSc1d1	houževnatý
polystyren	polystyren	k1gInSc1	polystyren
(	(	kIx(	(
<g/>
HPS	HPS	kA	HPS
<g/>
,	,	kIx,	,
IPS	IPS	kA	IPS
<g/>
,	,	kIx,	,
HIPS	HIPS	kA	HIPS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
zakalený	zakalený	k2eAgInSc1d1	zakalený
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
přidaného	přidaný	k2eAgInSc2d1	přidaný
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
uvedené	uvedený	k2eAgInPc1d1	uvedený
druhy	druh	k1gInPc1	druh
PS	PS	kA	PS
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
granulí	granule	k1gFnPc2	granule
(	(	kIx(	(
<g/>
malých	malý	k2eAgInPc2d1	malý
válečků	váleček	k1gInPc2	váleček
nasekaných	nasekaný	k2eAgInPc2d1	nasekaný
ze	z	k7c2	z
struny	struna	k1gFnSc2	struna
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pelet	peleta	k1gFnPc2	peleta
(	(	kIx(	(
<g/>
zakalené	zakalený	k2eAgFnPc1d1	zakalená
hrany	hrana	k1gFnPc1	hrana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
baleny	balit	k5eAaImNgInP	balit
a	a	k8xC	a
expedovány	expedovat	k5eAaBmNgInP	expedovat
v	v	k7c6	v
pytlích	pytel	k1gInPc6	pytel
á	á	k0	á
25	[number]	k4	25
kg	kg	kA	kg
uložených	uložený	k2eAgInPc2d1	uložený
a	a	k8xC	a
fixovaných	fixovaný	k2eAgInPc2d1	fixovaný
na	na	k7c6	na
paletách	paleta	k1gFnPc6	paleta
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
autocisternách	autocisterna	k1gFnPc6	autocisterna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěnový	pěnový	k2eAgInSc1d1	pěnový
polystyren	polystyren	k1gInSc1	polystyren
vzniká	vznikat	k5eAaImIp3nS	vznikat
tepelným	tepelný	k2eAgNnSc7d1	tepelné
zpracováním	zpracování	k1gNnSc7	zpracování
zpěňovatelného	zpěňovatelný	k2eAgInSc2d1	zpěňovatelný
polystyrenu	polystyren	k1gInSc2	polystyren
</s>
</p>
<p>
<s>
Zpěňovatelný	Zpěňovatelný	k2eAgInSc1d1	Zpěňovatelný
polystyren	polystyren	k1gInSc1	polystyren
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
a	a	k8xC	a
dodáván	dodávat	k5eAaImNgInS	dodávat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
malých	malý	k2eAgInPc2d1	malý
<g/>
,	,	kIx,	,
mléčně	mléčně	k6eAd1	mléčně
zakalených	zakalený	k2eAgFnPc2d1	zakalená
perel	perla	k1gFnPc2	perla
nasycených	nasycený	k2eAgFnPc2d1	nasycená
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
lehkým	lehký	k2eAgInSc7d1	lehký
uhlovodíkem	uhlovodík	k1gInSc7	uhlovodík
(	(	kIx(	(
<g/>
nadouvadlem	nadouvadlo	k1gNnSc7	nadouvadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
polystyrenu	polystyren	k1gInSc2	polystyren
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
např.	např.	kA	např.
obaly	obal	k1gInPc4	obal
nebo	nebo	k8xC	nebo
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
nádobí	nádobí	k1gNnSc4	nádobí
(	(	kIx(	(
<g/>
talíře	talíř	k1gInPc4	talíř
<g/>
,	,	kIx,	,
kelímky	kelímek	k1gInPc4	kelímek
<g/>
,	,	kIx,	,
misky	miska	k1gFnPc4	miska
<g/>
,	,	kIx,	,
příbory	příbor	k1gInPc7	příbor
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
dobré	dobrý	k2eAgFnSc3d1	dobrá
barvitelnosti	barvitelnost	k1gFnSc3	barvitelnost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Pěnový	pěnový	k2eAgInSc1d1	pěnový
PS	PS	kA	PS
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
misky	miska	k1gFnPc4	miska
pod	pod	k7c4	pod
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
balení	balení	k1gNnSc4	balení
hotových	hotový	k2eAgNnPc2d1	hotové
jídel	jídlo	k1gNnPc2	jídlo
a	a	k8xC	a
jako	jako	k9	jako
přepravky	přepravka	k1gFnPc4	přepravka
pro	pro	k7c4	pro
mořské	mořský	k2eAgInPc4d1	mořský
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
v	v	k7c6	v
nábytkářství	nábytkářství	k1gNnSc6	nábytkářství
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kuliček	kulička	k1gFnPc2	kulička
se	se	k3xPyFc4	se
polystyren	polystyren	k1gInSc1	polystyren
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sedacích	sedací	k2eAgInPc2d1	sedací
pytlů	pytel	k1gInPc2	pytel
jako	jako	k8xS	jako
výplň	výplň	k1gFnSc4	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
materiálu	materiál	k1gInSc2	materiál
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
neschopnosti	neschopnost	k1gFnSc3	neschopnost
vázat	vázat	k5eAaImF	vázat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vlhkost	vlhkost	k1gFnSc4	vlhkost
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
nedrží	držet	k5eNaImIp3nP	držet
roztoči	roztoč	k1gMnPc1	roztoč
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
interiérových	interiérový	k2eAgInPc2d1	interiérový
obkladů	obklad	k1gInPc2	obklad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stropních	stropní	k2eAgFnPc2d1	stropní
desek	deska	k1gFnPc2	deska
nebo	nebo	k8xC	nebo
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Desky	deska	k1gFnPc1	deska
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stavitelství	stavitelství	k1gNnSc6	stavitelství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
polystyren	polystyren	k1gInSc1	polystyren
zejména	zejména	k9	zejména
v	v	k7c6	v
deskách	deska	k1gFnPc6	deska
o	o	k7c6	o
plošném	plošný	k2eAgInSc6d1	plošný
rozměru	rozměr	k1gInSc6	rozměr
0,5	[number]	k4	0,5
x	x	k?	x
1,0	[number]	k4	1,0
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťky	tloušťka	k1gFnPc1	tloušťka
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
-	-	kIx~	-
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
(	(	kIx(	(
<g/>
cca	cca	kA	cca
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
do	do	k7c2	do
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osvědčil	osvědčit	k5eAaPmAgMnS	osvědčit
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
například	například	k6eAd1	například
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
izolacích	izolace	k1gFnPc6	izolace
domů	domů	k6eAd1	domů
(	(	kIx(	(
<g/>
fasád	fasáda	k1gFnPc2	fasáda
<g/>
)	)	kIx)	)
nejčastěji	často	k6eAd3	často
EPS	EPS	kA	EPS
70	[number]	k4	70
F	F	kA	F
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
EPS	EPS	kA	EPS
100	[number]	k4	100
F	F	kA	F
-	-	kIx~	-
kontaktní	kontaktní	k2eAgInSc1d1	kontaktní
zateplovací	zateplovací	k2eAgInSc1d1	zateplovací
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
značí	značit	k5eAaImIp3nS	značit
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
tlaku	tlak	k1gInSc6	tlak
v	v	k7c6	v
kPa	kPa	k?	kPa
(	(	kIx(	(
<g/>
kilopascal	kilopascat	k5eAaPmAgInS	kilopascat
<g/>
)	)	kIx)	)
-	-	kIx~	-
50	[number]	k4	50
<g/>
,	,	kIx,	,
70	[number]	k4	70
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
150	[number]	k4	150
<g/>
,	,	kIx,	,
200	[number]	k4	200
až	až	k9	až
250	[number]	k4	250
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
výhody	výhoda	k1gFnPc4	výhoda
patří	patřit	k5eAaImIp3nS	patřit
dobrá	dobrý	k2eAgFnSc1d1	dobrá
zpracovatelnost	zpracovatelnost	k1gFnSc1	zpracovatelnost
a	a	k8xC	a
snadná	snadný	k2eAgFnSc1d1	snadná
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Z	z	k7c2	z
-	-	kIx~	-
základní	základní	k2eAgFnSc2d1	základní
-	-	kIx~	-
do	do	k7c2	do
podlah	podlaha	k1gFnPc2	podlaha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
vyžadovaná	vyžadovaný	k2eAgFnSc1d1	vyžadovaná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přesnost	přesnost	k1gFnSc1	přesnost
v	v	k7c6	v
rozměrech	rozměr	k1gInPc6	rozměr
desky	deska	k1gFnSc2	deska
</s>
</p>
<p>
<s>
S	s	k7c7	s
-	-	kIx~	-
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
-	-	kIx~	-
použití	použití	k1gNnSc1	použití
pro	pro	k7c4	pro
tepelné	tepelný	k2eAgFnPc4d1	tepelná
izolace	izolace	k1gFnPc4	izolace
střech	střecha	k1gFnPc2	střecha
</s>
</p>
<p>
<s>
F	F	kA	F
-	-	kIx~	-
fasádní	fasádní	k2eAgFnSc1d1	fasádní
-	-	kIx~	-
pro	pro	k7c4	pro
kontaktní	kontaktní	k2eAgNnSc4d1	kontaktní
zateplování	zateplování	k1gNnSc4	zateplování
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
požaduje	požadovat	k5eAaImIp3nS	požadovat
maximální	maximální	k2eAgFnSc1d1	maximální
přesnost	přesnost	k1gFnSc1	přesnost
rozměrů	rozměr	k1gInPc2	rozměr
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
tolerance	tolerance	k1gFnSc1	tolerance
v	v	k7c6	v
úhlopříčkách	úhlopříčka	k1gFnPc6	úhlopříčka
desky	deska	k1gFnSc2	deska
max	max	kA	max
<g/>
.	.	kIx.	.
2	[number]	k4	2
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Drcený	drcený	k2eAgMnSc1d1	drcený
===	===	k?	===
</s>
</p>
<p>
<s>
Drcený	drcený	k2eAgInSc1d1	drcený
polystyren	polystyren	k1gInSc1	polystyren
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
betonů	beton	k1gInPc2	beton
podlah	podlaha	k1gFnPc2	podlaha
(	(	kIx(	(
<g/>
polystyrenbeton	polystyrenbeton	k1gInSc1	polystyrenbeton
<g/>
)	)	kIx)	)
-	-	kIx~	-
snížení	snížení	k1gNnSc1	snížení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
zlepšení	zlepšení	k1gNnSc1	zlepšení
tepelně-izolačních	tepelnězolační	k2eAgFnPc2d1	tepelně-izolační
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Extrudovaný	Extrudovaný	k2eAgInSc1d1	Extrudovaný
polystyren	polystyren	k1gInSc1	polystyren
===	===	k?	===
</s>
</p>
<p>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
desky	deska	k1gFnPc1	deska
1250	[number]	k4	1250
x	x	k?	x
600	[number]	k4	600
mm	mm	kA	mm
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
s	s	k7c7	s
polodrážkou	polodrážka	k1gFnSc7	polodrážka
nebo	nebo	k8xC	nebo
kolmou	kolmý	k2eAgFnSc7d1	kolmá
hranou	hrana	k1gFnSc7	hrana
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c4	na
sokl	sokl	k1gInSc4	sokl
se	s	k7c7	s
zdrsněným	zdrsněný	k2eAgInSc7d1	zdrsněný
povrchem	povrch	k1gInSc7	povrch
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
přilnutí	přilnutí	k1gNnSc4	přilnutí
lepidla	lepidlo	k1gNnSc2	lepidlo
(	(	kIx(	(
<g/>
Styrodur	Styrodura	k1gFnPc2	Styrodura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nenasákavý	nasákavý	k2eNgMnSc1d1	nenasákavý
<g/>
,	,	kIx,	,
různé	různý	k2eAgNnSc1d1	různé
barevné	barevný	k2eAgNnSc1d1	barevné
provedení	provedení	k1gNnSc1	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
extruzí	extruze	k1gFnSc7	extruze
(	(	kIx(	(
<g/>
vytlačováním	vytlačování	k1gNnSc7	vytlačování
<g/>
)	)	kIx)	)
taveniny	tavenina	k1gFnSc2	tavenina
krystalového	krystalový	k2eAgInSc2d1	krystalový
polystyrenu	polystyren	k1gInSc2	polystyren
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
sycení	sycení	k1gNnSc2	sycení
vzpěňovadlem	vzpěňovadlo	k1gNnSc7	vzpěňovadlo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
tlaku	tlak	k1gInSc2	tlak
umožní	umožnit	k5eAaPmIp3nS	umožnit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vytlačovací	vytlačovací	k2eAgFnSc1d1	vytlačovací
hubice	hubice	k1gFnSc1	hubice
napěnění	napěnění	k1gNnSc2	napěnění
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Extrudovaný	Extrudovaný	k2eAgInSc1d1	Extrudovaný
polystyren	polystyren	k1gInSc1	polystyren
(	(	kIx(	(
<g/>
XPS	XPS	kA	XPS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
deskám	deska	k1gFnPc3	deska
<g/>
,	,	kIx,	,
vyrobeným	vyrobený	k2eAgFnPc3d1	vyrobená
ze	z	k7c2	z
zpěňovatelného	zpěňovatelný	k2eAgInSc2d1	zpěňovatelný
polystyrenu	polystyren	k1gInSc2	polystyren
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
výroby	výroba	k1gFnSc2	výroba
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
výrobků	výrobek	k1gInPc2	výrobek
mají	mít	k5eAaImIp3nP	mít
ale	ale	k8xC	ale
základ	základ	k1gInSc4	základ
v	v	k7c6	v
polystyrenu	polystyren	k1gInSc6	polystyren
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
hořlavosti	hořlavost	k1gFnSc3	hořlavost
EPS	EPS	kA	EPS
a	a	k8xC	a
XPS	XPS	kA	XPS
se	se	k3xPyFc4	se
do	do	k7c2	do
nch	nch	k?	nch
přidávaly	přidávat	k5eAaImAgFnP	přidávat
bromované	bromovaný	k2eAgInPc4d1	bromovaný
zpomalovače	zpomalovač	k1gInPc4	zpomalovač
hoření	hoření	k2eAgInPc4d1	hoření
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hexabromcyklododekan	hexabromcyklododekan	k1gMnSc1	hexabromcyklododekan
(	(	kIx(	(
<g/>
HBCDD	HBCDD	kA	HBCDD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
retardér	retardér	k1gInSc1	retardér
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
polymerem	polymer	k1gInSc7	polymer
bromovaným	bromovaný	k2eAgInSc7d1	bromovaný
retardérem	retardér	k1gInSc7	retardér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
polystyreny	polystyren	k1gInPc1	polystyren
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
i	i	k9	i
násobně	násobně	k6eAd1	násobně
recyklovat	recyklovat	k5eAaBmF	recyklovat
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
lze	lze	k6eAd1	lze
postupovat	postupovat	k5eAaImF	postupovat
s	s	k7c7	s
EPS	EPS	kA	EPS
bez	bez	k7c2	bez
HBCDD	HBCDD	kA	HBCDD
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
typy	typ	k1gInPc1	typ
s	s	k7c7	s
HBCDD	HBCDD	kA	HBCDD
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
energeticky	energeticky	k6eAd1	energeticky
využít	využít	k5eAaPmF	využít
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
retardér	retardér	k1gInSc1	retardér
hoření	hoření	k1gNnSc2	hoření
rozloží	rozložit	k5eAaPmIp3nS	rozložit
na	na	k7c4	na
neškodné	škodný	k2eNgInPc4d1	neškodný
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Polysterenová	Polysterenový	k2eAgNnPc1d1	Polysterenový
vlákna	vlákno	k1gNnPc1	vlákno
==	==	k?	==
</s>
</p>
<p>
<s>
Polystyrenová	polystyrenový	k2eAgNnPc1d1	polystyrenové
vlákna	vlákno	k1gNnPc1	vlákno
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
zkratka	zkratka	k1gFnSc1	zkratka
PS	PS	kA	PS
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
polystyrenu	polystyren	k1gInSc2	polystyren
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
zvlákňování	zvlákňování	k1gNnSc1	zvlákňování
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
výlučně	výlučně	k6eAd1	výlučně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
monofilu	monofil	k1gInSc2	monofil
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
izolaci	izolace	k1gFnSc4	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
vláken	vlákno	k1gNnPc2	vlákno
nejsou	být	k5eNaImIp3nP	být
veřejně	veřejně	k6eAd1	veřejně
známé	známý	k2eAgInPc1d1	známý
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
jejich	jejich	k3xOp3gFnSc2	jejich
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
ve	v	k7c6	v
statistikách	statistika	k1gFnPc6	statistika
o	o	k7c6	o
polystyrenu	polystyren	k1gInSc6	polystyren
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
publikovány	publikovat	k5eAaBmNgFnP	publikovat
obsáhlé	obsáhlý	k2eAgFnPc1d1	obsáhlá
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
možném	možný	k2eAgNnSc6d1	možné
praktickém	praktický	k2eAgNnSc6d1	praktické
použití	použití	k1gNnSc6	použití
elektrostaticky	elektrostaticky	k6eAd1	elektrostaticky
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
polystyrenových	polystyrenový	k2eAgFnPc2d1	polystyrenová
nanovláken	nanovlákna	k1gFnPc2	nanovlákna
na	na	k7c4	na
membrány	membrána	k1gFnPc4	membrána
ke	k	k7c3	k
speciálním	speciální	k2eAgInPc3d1	speciální
filtrům	filtr	k1gInPc3	filtr
<g/>
,	,	kIx,	,
obvazový	obvazový	k2eAgInSc4d1	obvazový
medicinský	medicinský	k2eAgInSc4d1	medicinský
materiál	materiál	k1gInSc4	materiál
s	s	k7c7	s
výtečnou	výtečný	k2eAgFnSc7d1	výtečná
ochranou	ochrana	k1gFnSc7	ochrana
proti	proti	k7c3	proti
infekcím	infekce	k1gFnPc3	infekce
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
standardního	standardní	k2eAgInSc2d1	standardní
(	(	kIx(	(
<g/>
krystalového	krystalový	k2eAgInSc2d1	krystalový
polystyrenu	polystyren	k1gInSc2	polystyren
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
www.epscr.cz	www.epscr.cz	k1gInSc1	www.epscr.cz
"	"	kIx"	"
<g/>
Izolační	izolační	k2eAgFnSc1d1	izolační
praxe	praxe	k1gFnSc1	praxe
č.	č.	k?	č.
<g/>
13	[number]	k4	13
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
František	František	k1gMnSc1	František
Vörös	Vörösa	k1gFnPc2	Vörösa
<g/>
:	:	kIx,	:
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
výroby	výroba	k1gFnSc2	výroba
českého	český	k2eAgInSc2d1	český
styrenu	styren	k1gInSc2	styren
a	a	k8xC	a
kompaktních	kompaktní	k2eAgInPc2d1	kompaktní
plast	plast	k1gInSc1	plast
<g/>
,	,	kIx,	,
Plasty	plast	k1gInPc1	plast
a	a	k8xC	a
kaučuk	kaučuk	k1gInSc1	kaučuk
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
František	František	k1gMnSc1	František
Vörös	Vörösa	k1gFnPc2	Vörösa
<g/>
:	:	kIx,	:
Udržitelné	udržitelný	k2eAgInPc4d1	udržitelný
plasty	plast	k1gInPc4	plast
<g/>
:	:	kIx,	:
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
<g/>
,	,	kIx,	,
Plasty	plast	k1gInPc1	plast
a	a	k8xC	a
kaučuk	kaučuk	k1gInSc1	kaučuk
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Jarmila	Jarmila	k1gFnSc1	Jarmila
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
:	:	kIx,	:
Retartdéry	Retartdér	k1gInPc1	Retartdér
hoření	hoření	k2eAgInPc1d1	hoření
v	v	k7c6	v
pěnovém	pěnový	k2eAgInSc6d1	pěnový
PS	PS	kA	PS
a	a	k8xC	a
co	co	k9	co
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
Odpady	odpad	k1gInPc1	odpad
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
.7	.7	k4	.7
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
32	[number]	k4	32
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
www.espcr.cz	www.espcr.cz	k1gInSc1	www.espcr.cz
<g/>
,	,	kIx,	,
Izolační	izolační	k2eAgFnSc1d1	izolační
praxe	praxe	k1gFnSc1	praxe
č.	č.	k?	č.
12	[number]	k4	12
"	"	kIx"	"
<g/>
Snižování	snižování	k1gNnSc2	snižování
hořlavosti	hořlavost	k1gFnSc2	hořlavost
EPS	EPS	kA	EPS
izolací	izolace	k1gFnSc7	izolace
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Nařízení	nařízení	k1gNnSc4	nařízení
evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
č.	č.	k?	č.
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
293	[number]	k4	293
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
www.epscr.cz	www.epscr.cz	k1gInSc1	www.epscr.cz
<g/>
,	,	kIx,	,
Izolační	izolační	k2eAgFnSc1d1	izolační
praxe	praxe	k1gFnSc1	praxe
č.	č.	k?	č.
13	[number]	k4	13
"	"	kIx"	"
<g/>
Využití	využití	k1gNnPc4	využití
EPS	EPS	kA	EPS
odpadů	odpad	k1gInPc2	odpad
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
systém	systém	k1gInSc4	systém
ETICS	ETICS	kA	ETICS
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
https://web.archive.org/web/20160820195740/https://www.mt-nabytek.cz/jak-udrzovat-polystyren.htm	[url]	k6eAd1	https://web.archive.org/web/20160820195740/https://www.mt-nabytek.cz/jak-udrzovat-polystyren.htm
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
Nařízení	nařízení	k1gNnSc4	nařízení
evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
č.	č.	k?	č.
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
4602	[number]	k4	4602
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
polystyren	polystyren	k1gInSc1	polystyren
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fire	Fire	k1gFnSc1	Fire
and	and	k?	and
explosion	explosion	k1gInSc1	explosion
risks	risks	k6eAd1	risks
from	from	k6eAd1	from
pentane	pentan	k1gInSc5	pentan
in	in	k?	in
expandable	expandable	k6eAd1	expandable
polystyrene	polystyren	k1gInSc5	polystyren
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
</s>
</p>
