<s>
Sơ	Sơ	k?	Sơ
Đoò	Đoò	k1gFnSc1	Đoò
je	být	k5eAaImIp3nS	být
jeskyně	jeskyně	k1gFnSc1	jeskyně
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Phong	Phonga	k1gFnPc2	Phonga
Nha-Ke	Nha-Ke	k1gNnSc1	Nha-Ke
Bang	Bang	k1gMnSc1	Bang
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Quang	Quang	k1gMnSc1	Quang
Binh	Binh	k1gMnSc1	Binh
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
50	[number]	k4	50
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
provincie	provincie	k1gFnSc2	provincie
Dong	dong	k1gInSc1	dong
Hoi	Hoi	k1gFnSc4	Hoi
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
metropole	metropol	k1gFnSc2	metropol
Hanoje	Hanoj	k1gFnSc2	Hanoj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
místním	místní	k2eAgNnSc7d1	místní
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
oznámena	oznámen	k2eAgFnSc1d1	oznámena
britskou	britský	k2eAgFnSc7d1	britská
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
jeskyni	jeskyně	k1gFnSc4	jeskyně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
