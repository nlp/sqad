<p>
<s>
Sơ	Sơ	k?	Sơ
Đoò	Đoò	k1gFnSc1	Đoò
je	být	k5eAaImIp3nS	být
jeskyně	jeskyně	k1gFnSc1	jeskyně
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Phong	Phonga	k1gFnPc2	Phonga
Nha-Ke	Nha-Ke	k1gNnSc1	Nha-Ke
Bang	Bang	k1gMnSc1	Bang
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Quang	Quang	k1gMnSc1	Quang
Binh	Binh	k1gMnSc1	Binh
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
50	[number]	k4	50
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
provincie	provincie	k1gFnSc2	provincie
Dong	dong	k1gInSc1	dong
Hoi	Hoi	k1gFnSc4	Hoi
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
metropole	metropol	k1gFnSc2	metropol
Hanoje	Hanoj	k1gFnSc2	Hanoj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
místním	místní	k2eAgNnSc7d1	místní
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
oznámena	oznámen	k2eAgFnSc1d1	oznámena
britskou	britský	k2eAgFnSc7d1	britská
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
jeskyni	jeskyně	k1gFnSc4	jeskyně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jeskyni	jeskyně	k1gFnSc4	jeskyně
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
místní	místní	k2eAgMnPc1d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
jménem	jméno	k1gNnSc7	jméno
Hồ	Hồ	k1gFnSc2	Hồ
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Hang	Hang	k1gInSc4	Hang
Son	son	k1gInSc4	son
Doong	Doonga	k1gFnPc2	Doonga
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
horské	horský	k2eAgFnSc2d1	horská
řeky	řeka	k1gFnSc2	řeka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
podrobněji	podrobně	k6eAd2	podrobně
popsána	popsat	k5eAaPmNgFnS	popsat
až	až	k9	až
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
expedice	expedice	k1gFnSc1	expedice
Britské	britský	k2eAgFnSc2d1	britská
speleologické	speleologický	k2eAgFnSc2d1	speleologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Howardem	Howard	k1gMnSc7	Howard
a	a	k8xC	a
Debem	Deb	k1gMnSc7	Deb
Limbertem	Limbert	k1gMnSc7	Limbert
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
největší	veliký	k2eAgFnSc7d3	veliký
překážkou	překážka	k1gFnSc7	překážka
byl	být	k5eAaImAgInS	být
prudký	prudký	k2eAgInSc1d1	prudký
tok	tok	k1gInSc1	tok
podzemní	podzemní	k2eAgFnSc2d1	podzemní
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
speleologové	speleolog	k1gMnPc1	speleolog
nakonec	nakonec	k6eAd1	nakonec
pronikli	proniknout	k5eAaPmAgMnP	proniknout
až	až	k9	až
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
rozměrů	rozměr	k1gInPc2	rozměr
-	-	kIx~	-
šířka	šířka	k1gFnSc1	šířka
největší	veliký	k2eAgFnSc1d3	veliký
jeskyně	jeskyně	k1gFnSc1	jeskyně
byla	být	k5eAaImAgFnS	být
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
Son	son	k1gInSc1	son
Doong	Doong	k1gInSc1	Doong
těmito	tento	k3xDgInPc7	tento
rozměry	rozměr	k1gInPc7	rozměr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
překonala	překonat	k5eAaPmAgFnS	překonat
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
Jelení	jelení	k2eAgFnSc2d1	jelení
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Deer	Deer	k1gInSc1	Deer
Cave	Cav	k1gFnSc2	Cav
<g/>
,	,	kIx,	,
malajsky	malajsky	k6eAd1	malajsky
Gua	Gua	k1gMnSc4	Gua
Rusa	Rus	k1gMnSc4	Rus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Miri	Mir	k1gFnSc2	Mir
v	v	k7c6	v
malajském	malajský	k2eAgInSc6d1	malajský
státě	stát	k1gInSc6	stát
Sarawak	Sarawak	k1gInSc1	Sarawak
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Borneo	Borneo	k1gNnSc1	Borneo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podzemní	podzemní	k2eAgFnPc1d1	podzemní
džungle	džungle	k1gFnPc1	džungle
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
strop	strop	k1gInSc1	strop
podzemního	podzemní	k2eAgInSc2d1	podzemní
dómu	dóm	k1gInSc2	dóm
propadl	propadnout	k5eAaPmAgMnS	propadnout
a	a	k8xC	a
díky	díky	k7c3	díky
přístupu	přístup	k1gInSc3	přístup
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
přiměřené	přiměřený	k2eAgFnSc2d1	přiměřená
vlhkosti	vlhkost	k1gFnSc2	vlhkost
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
partiích	partie	k1gFnPc6	partie
jeskyně	jeskyně	k1gFnSc2	jeskyně
Son	son	k1gInSc1	son
Dong	dong	k1gInSc1	dong
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc3d1	odpovídající
tropické	tropický	k2eAgFnSc3d1	tropická
džungli	džungle	k1gFnSc3	džungle
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Son	son	k1gInSc1	son
Doong	Doong	k1gInSc4	Doong
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
natočené	natočený	k2eAgNnSc1d1	natočené
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pomocí	pomocí	k7c2	pomocí
dronu	dron	k1gInSc2	dron
</s>
</p>
