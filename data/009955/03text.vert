<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc1	Dragons
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Dan	Dan	k1gMnSc1	Dan
Reynolds	Reynolds	k1gInSc1	Reynolds
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
Ben	Ben	k1gInSc4	Ben
McKee	McKe	k1gInSc2	McKe
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Wayne	Wayn	k1gInSc5	Wayn
Sermon	sermon	k1gInSc4	sermon
a	a	k8xC	a
také	také	k9	také
bubeník	bubeník	k1gMnSc1	bubeník
Dan	Dan	k1gMnSc1	Dan
Platzman	Platzman	k1gMnSc1	Platzman
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
přirovnávána	přirovnáván	k2eAgFnSc1d1	přirovnávána
k	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
kapelám	kapela	k1gFnPc3	kapela
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
The	The	k1gFnSc4	The
Killers	Killersa	k1gFnPc2	Killersa
nebo	nebo	k8xC	nebo
Arcade	Arcad	k1gInSc5	Arcad
Fire	Fire	k1gFnPc2	Fire
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
proslavili	proslavit	k5eAaPmAgMnP	proslavit
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Radioactive	Radioactiv	k1gInSc5	Radioactiv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
hlavní	hlavní	k2eAgMnPc1d1	hlavní
zpěvák	zpěvák	k1gMnSc1	zpěvák
Dan	Dan	k1gMnSc1	Dan
Reynolds	Reynolds	k1gInSc4	Reynolds
poznal	poznat	k5eAaPmAgMnS	poznat
bubeníka	bubeník	k1gMnSc4	bubeník
Andrewa	Andrewus	k1gMnSc4	Andrewus
Tolmana	Tolman	k1gMnSc4	Tolman
na	na	k7c4	na
Brigham	Brigham	k1gInSc4	Brigham
Young	Young	k1gInSc4	Young
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oba	dva	k4xCgMnPc1	dva
studovali	studovat	k5eAaImAgMnP	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Tolman	Tolman	k1gMnSc1	Tolman
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
přivedl	přivést	k5eAaPmAgMnS	přivést
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Daniela	Daniel	k1gMnSc2	Daniel
Wayna	Wayn	k1gMnSc2	Wayn
Sermona	Sermon	k1gMnSc2	Sermon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
na	na	k7c6	na
Berklee	Berklee	k1gNnSc6	Berklee
College	Colleg	k1gInSc2	Colleg
of	of	k?	of
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
Tolman	Tolman	k1gMnSc1	Tolman
poté	poté	k6eAd1	poté
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
angažoval	angažovat	k5eAaBmAgMnS	angažovat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
Brittany	Brittan	k1gInPc1	Brittan
Tolman	Tolman	k1gMnSc1	Tolman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zpívala	zpívat	k5eAaImAgFnS	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Sermon	sermon	k1gInSc4	sermon
poté	poté	k6eAd1	poté
přišel	přijít	k5eAaPmAgMnS	přijít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
spolužákem	spolužák	k1gMnSc7	spolužák
Benem	Ben	k1gInSc7	Ben
McKeem	McKeus	k1gMnSc7	McKeus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
doplnili	doplnit	k5eAaPmAgMnP	doplnit
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
v	v	k7c6	v
Provu	Prov	k1gInSc6	Prov
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
<g/>
,	,	kIx,	,
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
a	a	k8xC	a
pak	pak	k6eAd1	pak
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
Dana	Dana	k1gFnSc1	Dana
Reynoldse	Reynoldse	k1gFnSc1	Reynoldse
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahrála	nahrát	k5eAaPmAgFnS	nahrát
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
extended	extended	k1gInSc1	extended
play	play	k0	play
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xS	jako
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
a	a	k8xC	a
povznášející	povznášející	k2eAgFnSc1d1	povznášející
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
od	od	k7c2	od
používání	používání	k1gNnSc2	používání
hrubých	hrubý	k2eAgInPc2d1	hrubý
výrazů	výraz	k1gInPc2	výraz
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jejich	jejich	k3xOp3gInPc6	jejich
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
anagram	anagram	k1gInSc1	anagram
<g/>
,	,	kIx,	,
původní	původní	k2eAgNnPc1d1	původní
slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
známá	známý	k2eAgNnPc1d1	známé
pouze	pouze	k6eAd1	pouze
členům	člen	k1gMnPc3	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
dvě	dva	k4xCgFnPc4	dva
extended	extended	k1gInSc1	extended
play	play	k0	play
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc1	Dragons
a	a	k8xC	a
Hell	Hell	k1gInSc1	Hell
and	and	k?	and
Silence	silenka	k1gFnSc6	silenka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
obě	dva	k4xCgFnPc4	dva
nahrála	nahrát	k5eAaBmAgFnS	nahrát
v	v	k7c4	v
Battle	Battle	k1gFnPc4	Battle
Born	Borna	k1gFnPc2	Borna
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Time	Time	k1gFnPc7	Time
bylo	být	k5eAaImAgNnS	být
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
ještě	ještě	k9	ještě
před	před	k7c7	před
podpisem	podpis	k1gInSc7	podpis
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Continued	Continued	k1gInSc1	Continued
Silence	silenka	k1gFnSc3	silenka
<g/>
,	,	kIx,	,
Hear	Hear	k1gInSc4	Hear
Me	Me	k1gFnSc2	Me
a	a	k8xC	a
Night	Night	k2eAgInSc1d1	Night
Visions	Visions	k1gInSc1	Visions
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragonsa	k1gFnPc2	Dragonsa
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
Interscope	Interscop	k1gInSc5	Interscop
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Úzce	úzko	k6eAd1	úzko
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Alexem	Alex	k1gMnSc7	Alex
Da	Da	k1gMnSc7	Da
Kidem	Kid	k1gMnSc7	Kid
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
nahráli	nahrát	k5eAaPmAgMnP	nahrát
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
velké	velký	k2eAgFnPc4d1	velká
nahrávky	nahrávka	k1gFnPc4	nahrávka
ve	v	k7c6	v
Westlake	Westlake	k1gFnSc6	Westlake
Recording	Recording	k1gInSc1	Recording
Studios	Studios	k?	Studios
v	v	k7c6	v
Západním	západní	k2eAgInSc6d1	západní
Hollywoodu	Hollywood	k1gInSc6	Hollywood
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Extented	Extented	k1gMnSc1	Extented
play	play	k0	play
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
Continued	Continued	k1gInSc1	Continued
Silence	silenka	k1gFnSc6	silenka
bylo	být	k5eAaImAgNnS	být
digitálně	digitálně	k6eAd1	digitálně
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c4	na
Den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
40	[number]	k4	40
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
It	It	k1gFnSc4	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Time	Time	k1gNnSc7	Time
<g/>
"	"	kIx"	"
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
singl	singl	k1gInSc4	singl
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
5	[number]	k4	5
skladbách	skladba	k1gFnPc6	skladba
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
Billboard	billboard	k1gInSc4	billboard
Alternative	Alternativ	k1gInSc5	Alternativ
a	a	k8xC	a
Billboard	billboard	k1gInSc4	billboard
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
patnáctce	patnáctka	k1gFnSc6	patnáctka
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
také	také	k9	také
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
za	za	k7c4	za
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
desítce	desítka	k1gFnSc6	desítka
v	v	k7c6	v
Alternative	Alternativ	k1gInSc5	Alternativ
žebříčcích	žebříček	k1gInPc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
videoklip	videoklip	k1gInSc1	videoklip
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
stanicích	stanice	k1gFnPc6	stanice
MTV	MTV	kA	MTV
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
umělcem	umělec	k1gMnSc7	umělec
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc4	videoklip
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
nominován	nominovat	k5eAaBmNgMnS	nominovat
i	i	k9	i
na	na	k7c6	na
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
rockové	rockový	k2eAgNnSc1d1	rockové
video	video	k1gNnSc1	video
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Time	Time	k1gNnSc7	Time
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
stala	stát	k5eAaPmAgFnS	stát
platinovým	platinový	k2eAgInSc7d1	platinový
singlem	singl	k1gInSc7	singl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
Studia	studio	k1gNnSc2	studio
X	X	kA	X
ve	v	k7c4	v
Palms	Palms	k1gInSc4	Palms
Casino	Casino	k1gNnSc4	Casino
Resort	resort	k1gInSc1	resort
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dokončili	dokončit	k5eAaPmAgMnP	dokončit
nahrávání	nahrávání	k1gNnSc4	nahrávání
svého	svůj	k3xOyFgNnSc2	svůj
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
Night	Night	k2eAgInSc4d1	Night
Visions	Visions	k1gInSc4	Visions
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
a	a	k8xC	a
album	album	k1gNnSc4	album
vydali	vydat	k5eAaPmAgMnP	vydat
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
83	[number]	k4	83
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejlépe	dobře	k6eAd3	dobře
umístěným	umístěný	k2eAgNnSc7d1	umístěné
debutovým	debutový	k2eAgNnSc7d1	debutové
albem	album	k1gNnSc7	album
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
Alternative	Alternativ	k1gInSc5	Alternativ
Albums	Albums	k1gInSc1	Albums
a	a	k8xC	a
Top	topit	k5eAaImRp2nS	topit
Rock	rock	k1gInSc1	rock
Albums	Albums	k1gInSc1	Albums
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
desítce	desítka	k1gFnSc6	desítka
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
i	i	k8xC	i
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
získalo	získat	k5eAaPmAgNnS	získat
Night	Night	k2eAgInSc4d1	Night
Visions	Visions	k1gInSc4	Visions
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Radioactive	Radioactiv	k1gInSc5	Radioactiv
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Alternative	Alternativ	k1gInSc5	Alternativ
Songs	Songs	k1gInSc4	Songs
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zlatým	zlatý	k2eAgInSc7d1	zlatý
singlem	singl	k1gInSc7	singl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
magazín	magazín	k1gInSc1	magazín
Billboard	billboard	k1gInSc1	billboard
uvedl	uvést	k5eAaPmAgInS	uvést
skupinu	skupina	k1gFnSc4	skupina
jako	jako	k9	jako
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
nejzářivějších	zářivý	k2eAgFnPc2d3	nejzářivější
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
nazval	nazvat	k5eAaBmAgMnS	nazvat
skupinu	skupina	k1gFnSc4	skupina
svým	svůj	k3xOyFgMnPc3	svůj
"	"	kIx"	"
<g/>
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
rockovým	rockový	k2eAgMnSc7d1	rockový
umělcem	umělec	k1gMnSc7	umělec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc4	Dragons
živě	živě	k6eAd1	živě
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Time	Time	k1gNnSc7	Time
<g/>
"	"	kIx"	"
v	v	k7c6	v
pořadech	pořad	k1gInPc6	pořad
The	The	k1gMnSc1	The
Tonight	Tonight	k1gMnSc1	Tonight
Show	show	k1gFnPc2	show
with	with	k1gMnSc1	with
Jay	Jay	k1gMnSc1	Jay
Leno	Lena	k1gFnSc5	Lena
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jimmy	Jimma	k1gFnPc4	Jimma
Kimmel	Kimmela	k1gFnPc2	Kimmela
Live	Liv	k1gFnPc4	Liv
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Late	lat	k1gInSc5	lat
Night	Nightum	k1gNnPc2	Nightum
with	with	k1gMnSc1	with
Jimmy	Jimma	k1gFnSc2	Jimma
Fallon	Fallon	k1gInSc1	Fallon
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Conan	Conan	k1gMnSc1	Conan
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Late	lat	k1gInSc5	lat
Show	show	k1gNnSc2	show
with	with	k1gMnSc1	with
David	David	k1gMnSc1	David
Letterman	Letterman	k1gMnSc1	Letterman
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Glee	Gle	k1gFnSc2	Gle
zazněla	zaznět	k5eAaImAgFnS	zaznět
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
"	"	kIx"	"
<g/>
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Time	Time	k1gNnSc7	Time
<g/>
"	"	kIx"	"
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
zpěváka	zpěvák	k1gMnSc2	zpěvák
Darrena	Darren	k1gMnSc2	Darren
Crisse	Criss	k1gMnSc2	Criss
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
trailerech	trailer	k1gInPc6	trailer
pro	pro	k7c4	pro
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
filmy	film	k1gInPc4	film
Charlieho	Charlie	k1gMnSc2	Charlie
malá	malý	k2eAgNnPc4d1	malé
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
,	,	kIx,	,
The	The	k1gFnPc4	The
Words	Wordsa	k1gFnPc2	Wordsa
a	a	k8xC	a
Hostitel	hostitel	k1gMnSc1	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Velkému	velký	k2eAgInSc3d1	velký
úspěchu	úspěch	k1gInSc3	úspěch
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Radioactive	Radioactiv	k1gInSc5	Radioactiv
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
také	také	k9	také
pomohl	pomoct	k5eAaPmAgInS	pomoct
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
zazněl	zaznít	k5eAaPmAgInS	zaznít
v	v	k7c6	v
launch	laun	k1gFnPc6	laun
traileru	trailer	k1gInSc2	trailer
hry	hra	k1gFnSc2	hra
Assassin	Assassina	k1gFnPc2	Assassina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc1	Creed
3	[number]	k4	3
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
také	také	k9	také
podnikli	podniknout	k5eAaPmAgMnP	podniknout
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
a	a	k8xC	a
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Kanadu	Kanada	k1gFnSc4	Kanada
a	a	k8xC	a
i	i	k9	i
několik	několik	k4yIc4	několik
států	stát	k1gInPc2	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
se	se	k3xPyFc4	se
vyprodalo	vyprodat	k5eAaPmAgNnS	vyprodat
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dvou	dva	k4xCgInPc2	dva
téměř	téměř	k6eAd1	téměř
vyprodaných	vyprodaný	k2eAgInPc2d1	vyprodaný
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
jejich	jejich	k3xOp3gInSc4	jejich
producent	producent	k1gMnSc1	producent
Alex	Alex	k1gMnSc1	Alex
da	da	k?	da
Kid	Kid	k1gMnSc1	Kid
pomocí	pomocí	k7c2	pomocí
internetu	internet	k1gInSc2	internet
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragonsa	k1gFnPc2	Dragonsa
vydávají	vydávat	k5eAaPmIp3nP	vydávat
nové	nový	k2eAgInPc1d1	nový
EP	EP	kA	EP
"	"	kIx"	"
<g/>
Hear	Hear	k1gMnSc1	Hear
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vzorkem	vzorek	k1gInSc7	vzorek
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
toto	tento	k3xDgNnSc1	tento
extended	extended	k1gInSc4	extended
play	play	k0	play
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragonsa	k1gFnPc2	Dragonsa
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
s	s	k7c7	s
turné	turné	k1gNnSc7	turné
s	s	k7c7	s
názvem	název	k1gInSc7	název
Night	Night	k2eAgInSc4d1	Night
Visions	Visions	k1gInSc4	Visions
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
americké	americký	k2eAgInPc4d1	americký
koncerty	koncert	k1gInPc4	koncert
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vyprodaly	vyprodat	k5eAaPmAgFnP	vyprodat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
dalších	další	k2eAgInPc2d1	další
13	[number]	k4	13
přidaných	přidaný	k2eAgInPc2d1	přidaný
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
také	také	k9	také
vyprodaly	vyprodat	k5eAaPmAgFnP	vyprodat
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
poté	poté	k6eAd1	poté
oznámila	oznámit	k5eAaPmAgFnS	oznámit
severoamerické	severoamerický	k2eAgNnSc4d1	severoamerické
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
amfiteátrech	amfiteátr	k1gInPc6	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
také	také	k9	také
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
dělat	dělat	k5eAaImF	dělat
předskokana	předskokan	k1gMnSc4	předskokan
na	na	k7c6	na
turné	turné	k1gNnSc1	turné
skupině	skupina	k1gFnSc6	skupina
Muse	Musa	k1gFnSc6	Musa
<g/>
,	,	kIx,	,
z	z	k7c2	z
časových	časový	k2eAgInPc2d1	časový
důvodů	důvod	k1gInPc2	důvod
však	však	k9	však
museli	muset	k5eAaImAgMnP	muset
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
<g/>
Skupina	skupina	k1gFnSc1	skupina
také	také	k9	také
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
letních	letní	k2eAgInPc6d1	letní
hudebních	hudební	k2eAgInPc6d1	hudební
festivalech	festival	k1gInPc6	festival
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Sasquatch	Sasquatch	k1gInSc4	Sasquatch
<g/>
!	!	kIx.	!
</s>
<s>
Music	Music	k1gMnSc1	Music
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Isle	Isle	k1gInSc1	Isle
of	of	k?	of
Wight	Wight	k2eAgInSc1d1	Wight
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Rock	rock	k1gInSc1	rock
am	am	k?	am
Ring	ring	k1gInSc1	ring
and	and	k?	and
Rock	rock	k1gInSc1	rock
im	im	k?	im
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Summer	Summer	k1gMnSc1	Summer
Sonic	Sonic	k1gMnSc1	Sonic
<g/>
,	,	kIx,	,
Hultsfred	Hultsfred	k1gMnSc1	Hultsfred
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Kanrocksas	Kanrocksas	k1gInSc1	Kanrocksas
<g/>
,	,	kIx,	,
T	T	kA	T
in	in	k?	in
the	the	k?	the
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Lollapalooza	Lollapalooza	k1gFnSc1	Lollapalooza
<g/>
,	,	kIx,	,
Hangout	Hangout	k1gMnSc1	Hangout
Music	Musice	k1gInPc2	Musice
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Reading	Reading	k1gInSc1	Reading
and	and	k?	and
Leeds	Leeds	k1gInSc1	Leeds
Festivals	Festivals	k1gInSc1	Festivals
<g/>
,	,	kIx,	,
Summerfest	Summerfest	k1gFnSc1	Summerfest
<g/>
,	,	kIx,	,
Osheaga	Osheaga	k1gFnSc1	Osheaga
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Jay-Z	Jay-Z	k1gFnSc1	Jay-Z
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Made	Made	k1gFnSc7	Made
in	in	k?	in
America	America	k1gFnSc1	America
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gNnSc1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Road	Road	k1gInSc1	Road
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Bergenfest	Bergenfest	k1gInSc1	Bergenfest
<g/>
,	,	kIx,	,
Slotsfjell	Slotsfjell	k1gInSc1	Slotsfjell
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Pukkelpop	Pukkelpop	k1gInSc1	Pukkelpop
<g/>
,	,	kIx,	,
NorthSide	NorthSid	k1gInSc5	NorthSid
Festival	festival	k1gInSc1	festival
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
Transformers	Transformersa	k1gFnPc2	Transformersa
<g/>
:	:	kIx,	:
Zánik	zánik	k1gInSc1	zánik
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Battle	Battle	k1gFnSc1	Battle
Cry	Cry	k1gFnSc2	Cry
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
turné	turné	k1gNnSc4	turné
Into	Into	k1gMnSc1	Into
the	the	k?	the
Night	Night	k2eAgInSc4d1	Night
Tour	Tour	k1gInSc4	Tour
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začne	začít	k5eAaPmIp3nS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
vydali	vydat	k5eAaPmAgMnP	vydat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Riot	Riot	k1gMnSc1	Riot
Games	Games	k1gMnSc1	Games
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Warriors	Warriors	k1gInSc1	Warriors
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
League	League	k1gFnSc6	League
of	of	k?	of
Legends	Legends	k1gInSc4	Legends
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vyšel	vyjít	k5eAaPmAgMnS	vyjít
singl	singl	k1gInSc4	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
I	i	k9	i
Bet	Bet	k1gFnSc3	Bet
My	my	k3xPp1nPc1	my
Life	Life	k1gNnPc2	Life
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
"	"	kIx"	"
<g/>
Shots	Shots	k1gInSc1	Shots
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Smoke	Smok	k1gFnSc2	Smok
+	+	kIx~	+
Mirrors	Mirrors	k1gInSc1	Mirrors
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evolve	Evolev	k1gFnSc2	Evolev
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vydali	vydat	k5eAaPmAgMnP	vydat
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragons	k1gInSc4	Dragons
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
Evolve	Evolev	k1gFnSc2	Evolev
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
Evolve	Evolev	k1gFnSc2	Evolev
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Origins	Originsa	k1gFnPc2	Originsa
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Origins	Originsa	k1gFnPc2	Originsa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
singly	singl	k1gInPc7	singl
jsou	být	k5eAaImIp3nP	být
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Natural	Natural	k?	Natural
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zero	Zero	k6eAd1	Zero
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Machine	Machin	k1gMnSc5	Machin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
celého	celý	k2eAgNnSc2d1	celé
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc4	Dragons
zavítali	zavítat	k5eAaPmAgMnP	zavítat
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
již	již	k6eAd1	již
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
skupina	skupina	k1gFnSc1	skupina
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
zavítala	zavítat	k5eAaPmAgFnS	zavítat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Sasazu	Sasaz	k1gInSc2	Sasaz
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
podívala	podívat	k5eAaImAgFnS	podívat
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
alba	album	k1gNnSc2	album
Smoke	Smoke	k1gFnSc1	Smoke
+	+	kIx~	+
Mirrors	Mirrors	k1gInSc1	Mirrors
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragons	k1gInSc1	Dragons
na	na	k7c6	na
ostravském	ostravský	k2eAgInSc6d1	ostravský
multižánrovém	multižánrový	k2eAgInSc6d1	multižánrový
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
hudebním	hudební	k2eAgInSc6d1	hudební
festivalu	festival	k1gInSc6	festival
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Web	web	k1gInSc1	web
iReport	iReport	k1gInSc1	iReport
označil	označit	k5eAaPmAgInS	označit
jejich	jejich	k3xOp3gNnSc4	jejich
vystoupení	vystoupení	k1gNnSc4	vystoupení
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
další	další	k2eAgNnSc1d1	další
vystoupení	vystoupení	k1gNnSc1	vystoupení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podruhé	podruhé	k6eAd1	podruhé
vyprodané	vyprodaný	k2eAgFnSc6d1	vyprodaná
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
.	.	kIx.	.
</s>
<s>
Předkapelou	předkapela	k1gFnSc7	předkapela
byla	být	k5eAaImAgFnS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
K.	K.	kA	K.
<g/>
Flay	Flaa	k1gFnSc2	Flaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charitativní	charitativní	k2eAgFnSc4d1	charitativní
činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
skupina	skupina	k1gFnSc1	skupina
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Tylera	Tyler	k1gMnSc2	Tyler
Robinsona	Robinson	k1gMnSc2	Robinson
založila	založit	k5eAaPmAgFnS	založit
charitativní	charitativní	k2eAgFnSc4d1	charitativní
organizaci	organizace	k1gFnSc4	organizace
The	The	k1gFnSc2	The
Tyler	Tyler	k1gInSc1	Tyler
Robinson	Robinson	k1gMnSc1	Robinson
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lidem	lid	k1gInSc7	lid
bojujícím	bojující	k2eAgFnPc3d1	bojující
s	s	k7c7	s
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
nadace	nadace	k1gFnSc2	nadace
jsou	být	k5eAaImIp3nP	být
tylerrobinsonfoundation	tylerrobinsonfoundation	k1gInSc4	tylerrobinsonfoundation
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragons	k1gInSc1	Dragons
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
kampaně	kampaň	k1gFnPc4	kampaň
pro	pro	k7c4	pro
zastavení	zastavení	k1gNnSc4	zastavení
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
Do	do	k7c2	do
The	The	k1gFnSc2	The
Write	Writ	k1gInSc5	Writ
Thing	Thing	k1gMnSc1	Thing
<g/>
:	:	kIx,	:
National	National	k1gMnSc1	National
Campaign	Campaign	k1gMnSc1	Campaign
to	ten	k3xDgNnSc4	ten
Stop	stop	k2eAgFnSc1d1	stop
Violence	Violence	k1gFnSc1	Violence
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Bringing	Bringing	k1gInSc1	Bringing
Human	Human	k1gInSc1	Human
Rights	Rights	k1gInSc1	Rights
Home	Home	k1gFnSc1	Home
(	(	kIx(	(
<g/>
Přinášení	přinášení	k1gNnSc1	přinášení
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
domů	dům	k1gInPc2	dům
<g/>
)	)	kIx)	)
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnPc2	International
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlivy	vliv	k1gInPc4	vliv
==	==	k?	==
</s>
</p>
<p>
<s>
Dan	Dan	k1gMnSc1	Dan
Reynolds	Reynoldsa	k1gFnPc2	Reynoldsa
jako	jako	k9	jako
zdroje	zdroj	k1gInPc4	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
citoval	citovat	k5eAaBmAgInS	citovat
Arcade	Arcad	k1gInSc5	Arcad
Fire	Fire	k1gFnSc6	Fire
<g/>
,	,	kIx,	,
Muse	Musa	k1gFnSc6	Musa
<g/>
,	,	kIx,	,
The	The	k1gMnSc2	The
Beatles	beatles	k1gMnSc2	beatles
<g/>
,	,	kIx,	,
Paula	Paul	k1gMnSc2	Paul
Simona	Simon	k1gMnSc2	Simon
a	a	k8xC	a
Harryho	Harry	k1gMnSc2	Harry
Nilssona	Nilsson	k1gMnSc2	Nilsson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
Reynolds	Reynoldsa	k1gFnPc2	Reynoldsa
také	také	k9	také
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
skupiny	skupina	k1gFnSc2	skupina
Foster	Foster	k1gMnSc1	Foster
the	the	k?	the
People	People	k1gMnSc1	People
a	a	k8xC	a
Mumford	Mumford	k1gMnSc1	Mumford
&	&	k?	&
Sons	Sons	k1gInSc1	Sons
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nynějších	nynější	k2eAgNnPc6d1	nynější
letech	léto	k1gNnPc6	léto
přinesli	přinést	k5eAaPmAgMnP	přinést
alternativní	alternativní	k2eAgInSc4d1	alternativní
popovou	popový	k2eAgFnSc4d1	popová
hudbu	hudba	k1gFnSc4	hudba
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
úrovně	úroveň	k1gFnSc2	úroveň
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Dan	Dan	k1gMnSc1	Dan
Reynolds	Reynolds	k1gInSc1	Reynolds
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
basový	basový	k2eAgInSc1d1	basový
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
vířivý	vířivý	k2eAgInSc1d1	vířivý
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ben	Ben	k1gInSc1	Ben
McKee	McKee	k1gInSc1	McKee
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
basový	basový	k2eAgInSc1d1	basový
buben	buben	k1gInSc1	buben
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Wayne	Wayn	k1gInSc5	Wayn
Sermon	sermon	k1gInSc1	sermon
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
a	a	k8xC	a
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
cello	cello	k1gNnSc1	cello
<g/>
,	,	kIx,	,
basový	basový	k2eAgInSc1d1	basový
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
James	James	k1gMnSc1	James
Platzman	Platzman	k1gMnSc1	Platzman
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
cajón	cajón	k1gInSc1	cajón
<g/>
,	,	kIx,	,
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vířivý	vířivý	k2eAgInSc1d1	vířivý
buben	buben	k1gInSc1	buben
<g/>
,	,	kIx,	,
tamburína	tamburína	k1gFnSc1	tamburína
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Doprovodní	doprovodní	k2eAgMnPc1d1	doprovodní
hudebníci	hudebník	k1gMnPc1	hudebník
na	na	k7c6	na
skladbách	skladba	k1gFnPc6	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Andrew	Andrew	k?	Andrew
Tolman	Tolman	k1gMnSc1	Tolman
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brittany	Brittan	k1gInPc4	Brittan
Tolman	Tolman	k1gMnSc1	Tolman
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Theresa	Theresa	k1gFnSc1	Theresa
Flaminio	Flaminio	k1gMnSc1	Flaminio
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dave	Dav	k1gInSc5	Dav
Lemke	Lemke	k1gFnSc7	Lemke
–	–	k?	–
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aurora	Aurora	k1gFnSc1	Aurora
Florence	Florenc	k1gFnSc2	Florenc
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc1	housle
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Andrew	Andrew	k?	Andrew
Beck	Beck	k1gInSc1	Beck
–	–	k?	–
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
===	===	k?	===
</s>
</p>
<p>
<s>
Ryan	Ryan	k1gMnSc1	Ryan
Walker	Walker	k1gMnSc1	Walker
-	-	kIx~	-
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Night	Night	k2eAgInSc1d1	Night
Visions	Visions	k1gInSc1	Visions
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smoke	Smoke	k1gFnSc1	Smoke
+	+	kIx~	+
Mirrors	Mirrors	k1gInSc1	Mirrors
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evolve	Evolev	k1gFnPc1	Evolev
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Origins	Origins	k1gInSc1	Origins
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Koncertní	koncertní	k2eAgNnPc4d1	koncertní
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Live	Live	k6eAd1	Live
at	at	k?	at
Independent	independent	k1gMnSc1	independent
Records	Records	k1gInSc1	Records
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Extended	Extended	k1gInSc1	Extended
Play	play	k0	play
===	===	k?	===
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc1	Dragons
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hell	Helnout	k5eAaPmAgInS	Helnout
and	and	k?	and
Silence	silenka	k1gFnSc3	silenka
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
It	It	k?	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Time	Time	k1gFnSc7	Time
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Continued	Continued	k1gInSc1	Continued
Silence	silenka	k1gFnSc3	silenka
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hear	Hear	k1gMnSc1	Hear
Me	Me	k1gMnSc1	Me
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Archive	archiv	k1gInSc5	archiv
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
iTunes	iTunes	k1gInSc1	iTunes
Session	Session	k1gInSc1	Session
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
==	==	k?	==
</s>
</p>
<p>
<s>
Night	Night	k2eAgInSc1d1	Night
Visions	Visions	k1gInSc1	Visions
Tour	Toura	k1gFnPc2	Toura
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
–	–	k?	–
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragons	k1gInSc4	Dragons
on	on	k3xPp3gMnSc1	on
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fall	Fall	k1gInSc1	Fall
Tour	Tour	k1gInSc1	Tour
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Europe	Europ	k1gInSc5	Europ
Tour	Tour	k1gInSc1	Tour
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Into	Into	k1gMnSc1	Into
The	The	k1gFnSc2	The
Night	Night	k1gMnSc1	Night
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smoke	Smoke	k1gFnSc1	Smoke
+	+	kIx~	+
Mirrors	Mirrors	k1gInSc1	Mirrors
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evolve	Evolvat	k5eAaPmIp3nS	Evolvat
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragonsa	k1gFnPc2	Dragonsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Imagine	Imagin	k1gInSc5	Imagin
Dragons	Dragons	k1gInSc4	Dragons
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc4	Dragons
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc1	Dragons
na	na	k7c4	na
Myspace	Myspace	k1gFnPc4	Myspace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Imagine	Imaginout	k5eAaPmIp3nS	Imaginout
Dragons	Dragons	k1gInSc4	Dragons
na	na	k7c4	na
Interscope	Interscop	k1gInSc5	Interscop
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
