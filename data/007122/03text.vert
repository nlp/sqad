<s>
Špagety	špagety	k1gFnPc1	špagety
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
spaghetti	spaghetti	k1gInPc2	spaghetti
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
spaˈ	spaˈ	k?	spaˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
italské	italský	k2eAgFnPc1d1	italská
těstoviny	těstovina	k1gFnPc1	těstovina
tenkého	tenký	k2eAgInSc2d1	tenký
a	a	k8xC	a
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
mm	mm	kA	mm
tlusté	tlustý	k2eAgFnSc2d1	tlustá
a	a	k8xC	a
30	[number]	k4	30
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
v	v	k7c6	v
syrovém	syrový	k2eAgInSc6d1	syrový
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Špagety	špagety	k1gFnPc1	špagety
jsou	být	k5eAaImIp3nP	být
italské	italský	k2eAgNnSc4d1	italské
národní	národní	k2eAgNnSc4d1	národní
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
v	v	k7c6	v
prodejnách	prodejna	k1gFnPc6	prodejna
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
v	v	k7c6	v
baleních	balení	k1gNnPc6	balení
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
g.	g.	k?	g.
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
vařením	vaření	k1gNnSc7	vaření
v	v	k7c6	v
osolené	osolený	k2eAgFnSc6d1	osolená
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
varu	var	k1gInSc2	var
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Špaget	špagety	k1gFnPc2	špagety
se	se	k3xPyFc4	se
též	též	k9	též
někdy	někdy	k6eAd1	někdy
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
soutěže	soutěž	k1gFnPc1	soutěž
ve	v	k7c6	v
stavění	stavění	k1gNnSc6	stavění
mostů	most	k1gInPc2	most
ze	z	k7c2	z
špaget	špagety	k1gFnPc2	špagety
(	(	kIx(	(
<g/>
nevařených	vařený	k2eNgNnPc2d1	nevařené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Špagety	špagety	k1gFnPc4	špagety
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
doma	doma	k6eAd1	doma
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
smícháním	smíchání	k1gNnSc7	smíchání
jednoho	jeden	k4xCgInSc2	jeden
žloutku	žloutek	k1gInSc2	žloutek
s	s	k7c7	s
cca	cca	kA	cca
90	[number]	k4	90
g	g	kA	g
hrubé	hrubý	k2eAgFnSc2d1	hrubá
mouky	mouka	k1gFnSc2	mouka
a	a	k8xC	a
špetkou	špetka	k1gFnSc7	špetka
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
vyválet	vyválet	k5eAaPmF	vyválet
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
mm	mm	kA	mm
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
uschnout	uschnout	k5eAaPmF	uschnout
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
nakrájet	nakrájet	k5eAaPmF	nakrájet
nožem	nůž	k1gInSc7	nůž
nebo	nebo	k8xC	nebo
strojkem	strojek	k1gInSc7	strojek
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
udělat	udělat	k5eAaPmF	udělat
i	i	k9	i
lasagne	lasagnout	k5eAaImIp3nS	lasagnout
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
boloňské	boloňský	k2eAgFnPc1d1	Boloňská
špagety	špagety	k1gFnPc1	špagety
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
neexistují	existovat	k5eNaImIp3nP	existovat
a	a	k8xC	a
boloňská	boloňský	k2eAgFnSc1d1	Boloňská
omáčka	omáčka	k1gFnSc1	omáčka
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tradičně	tradičně	k6eAd1	tradičně
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
lasagní	lasagnit	k5eAaImIp3nS	lasagnit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
špaget	špagety	k1gFnPc2	špagety
tuto	tento	k3xDgFnSc4	tento
omáčku	omáčka	k1gFnSc4	omáčka
využila	využít	k5eAaPmAgFnS	využít
až	až	k9	až
komerční	komerční	k2eAgFnSc1d1	komerční
světová	světový	k2eAgFnSc1d1	světová
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
převzala	převzít	k5eAaPmAgFnS	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgFnPc1d1	originální
italské	italský	k2eAgFnPc1d1	italská
špagety	špagety	k1gFnPc1	špagety
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
alla	allus	k1gMnSc2	allus
carbonara	carbonar	k1gMnSc2	carbonar
(	(	kIx(	(
<g/>
s	s	k7c7	s
vajíčky	vajíčko	k1gNnPc7	vajíčko
a	a	k8xC	a
slaninou	slanina	k1gFnSc7	slanina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
s	s	k7c7	s
rajčatovou	rajčatový	k2eAgFnSc7d1	rajčatová
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aglio	aglio	k6eAd1	aglio
<g/>
,	,	kIx,	,
olio	olio	k6eAd1	olio
e	e	k0	e
peperoncino	peperoncin	k2eAgNnSc1d1	peperoncin
(	(	kIx(	(
<g/>
s	s	k7c7	s
česnekem	česnek	k1gInSc7	česnek
<g/>
,	,	kIx,	,
olivovým	olivový	k2eAgInSc7d1	olivový
olejem	olej	k1gInSc7	olej
a	a	k8xC	a
pálivou	pálivý	k2eAgFnSc7d1	pálivá
paprikou	paprika	k1gFnSc7	paprika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Makaróny	makarón	k1gInPc1	makarón
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
špagety	špagety	k1gFnPc1	špagety
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
špageta	špageta	k1gFnSc1	špageta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
