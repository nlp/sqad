<s>
Salmiak	salmiak	k1gInSc1
(	(	kIx(
<g/>
minerál	minerál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Salmiak	salmiak	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Minerál	minerál	k1gInSc1
</s>
<s>
Chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
NH	NH	kA
<g/>
4	#num#	kA
<g/>
Cl	Cl	kA
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
bezbarvý	bezbarvý	k2eAgInSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
načervenalá	načervenalý	k2eAgFnSc1d1
</s>
<s>
Vzhled	vzhled	k1gInSc1
krystalu	krystal	k1gInSc2
</s>
<s>
mnohoploché	mnohoplochý	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
<g/>
,	,	kIx,
<g/>
agregáty	agregát	k1gInPc1
</s>
<s>
Soustava	soustava	k1gFnSc1
</s>
<s>
krychlová	krychlový	k2eAgFnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Lesk	lesk	k1gInSc1
</s>
<s>
skelný	skelný	k2eAgInSc1d1
</s>
<s>
Štěpnost	štěpnost	k1gFnSc1
</s>
<s>
dokonalá	dokonalý	k2eAgFnSc1d1
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
<g/>
=	=	kIx~
<g/>
1,639	1,639	k4
</s>
<s>
Vryp	vryp	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc1d1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1,5	1,5	k4
g	g	kA
⋅	⋅	k?
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
</s>
<s>
lehce	lehko	k6eAd1
ve	v	k7c6
vodě	voda	k1gFnSc6
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
slaná	slaný	k2eAgFnSc1d1
chuť	chuť	k1gFnSc1
</s>
<s>
Salmiak	salmiak	k1gInSc1
(	(	kIx(
<g/>
Agricola	Agricola	k1gFnSc1
<g/>
,	,	kIx,
1546	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
NH	NH	kA
<g/>
4	#num#	k4
<g/>
Cl	Cl	k1gFnPc1
(	(	kIx(
<g/>
chlorid	chlorid	k1gInSc1
amonný	amonný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
krychlový	krychlový	k2eAgInSc4d1
minerál	minerál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
ά	ά	k?
ά	ά	k?
<g/>
,	,	kIx,
alas	alas	k1gInSc1
ammóniakos	ammóniakos	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
sůl	sůl	k1gFnSc1
Ammónova	Ammónův	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
totiž	totiž	k9
znám	znám	k2eAgMnSc1d1
již	již	k6eAd1
ze	z	k7c2
starého	starý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nS
vulkanickou	vulkanický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
guánová	guánový	k2eAgNnPc4d1
ložiska	ložisko	k1gNnPc4
<g/>
,	,	kIx,
hořící	hořící	k2eAgFnPc4d1
uhelné	uhelný	k2eAgFnPc4d1
haldy	halda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Morfologie	morfologie	k1gFnSc1
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
zaoblené	zaoblený	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
buď	buď	k8xC
samostatně	samostatně	k6eAd1
nebo	nebo	k8xC
v	v	k7c6
drúzách	drúza	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krystaly	krystal	k1gInPc1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
umělé	umělý	k2eAgFnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
jednosměrně	jednosměrně	k6eAd1
vyvinuté	vyvinutý	k2eAgInPc1d1
(	(	kIx(
<g/>
monstrózní	monstrózní	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
mají	mít	k5eAaImIp3nP
pak	pak	k6eAd1
vzhled	vzhled	k1gInSc4
šesterečný	šesterečný	k2eAgInSc4d1
<g/>
,	,	kIx,
čtverečný	čtverečný	k2eAgInSc4d1
<g/>
,	,	kIx,
kosočtverečný	kosočtverečný	k2eAgInSc4d1
až	až	k8xS
asymetrický	asymetrický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
mohou	moct	k5eAaImIp3nP
dosáhnout	dosáhnout	k5eAaPmF
v	v	k7c6
přírodě	příroda	k1gFnSc6
velikosti	velikost	k1gFnSc2
až	až	k9
5	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
zrnité	zrnitý	k2eAgFnPc1d1
kůry	kůra	k1gFnPc1
a	a	k8xC
povlaky	povlak	k1gInPc1
<g/>
,	,	kIx,
nálety	nálet	k1gInPc1
složené	složený	k2eAgInPc1d1
z	z	k7c2
drobných	drobný	k2eAgInPc2d1
stalaktitů	stalaktit	k1gInPc2
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
zemitý	zemitý	k2eAgMnSc1d1
a	a	k8xC
práškovitý	práškovitý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hořících	hořící	k2eAgFnPc6d1
uhelných	uhelný	k2eAgFnPc6d1
haldách	halda	k1gFnPc6
často	často	k6eAd1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
rovnoběžně	rovnoběžně	k6eAd1
vláknité	vláknitý	k2eAgInPc4d1
agregáty	agregát	k1gInPc4
a	a	k8xC
shluky	shluk	k1gInPc4
rovnoběžných	rovnoběžný	k2eAgFnPc2d1
vláken	vlákna	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gInPc6
koncích	konec	k1gInPc6
narůstají	narůstat	k5eAaImIp3nP
izometrické	izometrický	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Tvrdost	tvrdost	k1gFnSc1
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
hustota	hustota	k1gFnSc1
1,5	1,5	k4
<g/>
-	-	kIx~
<g/>
1,6	1,6	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
plastický	plastický	k2eAgInSc1d1
<g/>
,	,	kIx,
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
krájet	krájet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lom	lom	k1gInSc1
lasturnatý	lasturnatý	k2eAgInSc1d1
<g/>
,	,	kIx,
štěpnost	štěpnost	k1gFnSc1
dokonalá	dokonalý	k2eAgFnSc1d1
podle	podle	k7c2
{	{	kIx(
<g/>
100	#num#	k4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
nedokonale	dokonale	k6eNd1
podle	podle	k7c2
{	{	kIx(
<g/>
111	#num#	k4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Složení	složení	k1gNnSc1
<g/>
:	:	kIx,
NH4	NH4	k1gFnSc1
33,72	33,72	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
Cl	Cl	k1gFnSc1
66,28	66,28	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
dmuchavkou	dmuchavka	k1gFnSc7
snadno	snadno	k6eAd1
a	a	k8xC
úplně	úplně	k6eAd1
vyprchá	vyprchat	k5eAaPmIp3nS
bez	bez	k7c2
tavení	tavení	k1gNnSc2
za	za	k7c2
tvorby	tvorba	k1gFnSc2
dýmů	dým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpouští	rozpouštět	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
při	při	k7c6
zahřátí	zahřátí	k1gNnSc6
se	s	k7c7
sodou	soda	k1gFnSc7
a	a	k8xC
kyselinami	kyselina	k1gFnPc7
vzniká	vznikat	k5eAaImIp3nS
amoniak	amoniak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Optické	optický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Barva	barva	k1gFnSc1
<g/>
:	:	kIx,
bezbarvý	bezbarvý	k2eAgMnSc1d1
<g/>
,	,	kIx,
vlivem	vliv	k1gInSc7
příměsí	příměs	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
zbarven	zbarvit	k5eAaPmNgInS
do	do	k7c2
žluta	žluto	k1gNnSc2
<g/>
,	,	kIx,
červena	červeno	k1gNnSc2
až	až	k8xS
hněda	hnědo	k1gNnSc2
<g/>
,	,	kIx,
vryp	vryp	k1gInSc1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
lesk	lesk	k1gInSc1
skelný	skelný	k2eAgInSc1d1
<g/>
,	,	kIx,
průhledný	průhledný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Má	mít	k5eAaImIp3nS
palčivě	palčivě	k6eAd1
slanou	slaný	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mechanické	mechanický	k2eAgFnSc6d1
deformaci	deformace	k1gFnSc6
se	se	k3xPyFc4
někdy	někdy	k6eAd1
objeví	objevit	k5eAaPmIp3nS
slabý	slabý	k2eAgInSc4d1
dvojlom	dvojlom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Parageneze	parageneze	k1gFnSc1
</s>
<s>
síra	síra	k1gFnSc1
(	(	kIx(
<g/>
fumaroly	fumarola	k1gFnPc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
realgar	realgar	k1gInSc1
<g/>
,	,	kIx,
auripigment	auripigment	k1gInSc1
<g/>
,	,	kIx,
síra	síra	k1gFnSc1
<g/>
,	,	kIx,
mascagnit	mascagnit	k1gInSc1
<g/>
,	,	kIx,
čermíkit	čermíkit	k1gInSc1
(	(	kIx(
<g/>
hořící	hořící	k2eAgFnSc2d1
uhelné	uhelný	k2eAgFnSc2d1
haldy	halda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
minerál	minerál	k1gInSc1
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Průmyslově	průmyslově	k6eAd1
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
uměle	uměle	k6eAd1
připravený	připravený	k2eAgInSc1d1
chlorid	chlorid	k1gInSc1
amonný	amonný	k2eAgInSc1d1
<g/>
,	,	kIx,
například	například	k6eAd1
jako	jako	k9
tzv.	tzv.	kA
salmiak	salmiak	k1gInSc1
na	na	k7c4
letování	letování	k1gNnSc4
na	na	k7c4
čištění	čištění	k1gNnSc4
hrotů	hrot	k1gInPc2
pájičky	pájička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplem	teplo	k1gNnSc7
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
chlorovodík	chlorovodík	k1gInSc4
a	a	k8xC
čpavek	čpavek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
hrot	hrot	k1gInSc1
pájičky	pájička	k1gFnSc2
čistí	čistit	k5eAaImIp3nS
nejen	nejen	k6eAd1
mechanicky	mechanicky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
chemicky	chemicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Naleziště	naleziště	k1gNnSc1
</s>
<s>
Lokální	lokální	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
<g/>
:	:	kIx,
hořící	hořící	k2eAgFnPc1d1
haldy	halda	k1gFnPc1
uhelných	uhelný	k2eAgInPc2d1
revírů	revír	k1gInPc2
-	-	kIx~
Bečkov	Bečkov	k1gInSc1
u	u	k7c2
Trutnova	Trutnov	k1gInSc2
<g/>
,	,	kIx,
Kladno	Kladno	k1gNnSc1
<g/>
,	,	kIx,
Lampertice	Lampertice	k1gFnSc1
u	u	k7c2
Žacléře	Žacléř	k1gInSc2
z	z	k7c2
haldy	halda	k1gFnSc2
dolu	dol	k1gInSc2
Eliška	Eliška	k1gFnSc1
<g/>
,	,	kIx,
Radvanice	Radvanice	k1gFnSc1
u	u	k7c2
Trutnova	Trutnov	k1gInSc2
krystalické	krystalický	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
i	i	k8xC
drúzy	drúza	k1gFnSc2
dobře	dobře	k6eAd1
vyvinutých	vyvinutý	k2eAgInPc2d1
krystalů	krystal	k1gInPc2
až	až	k9
15	#num#	k4
mm	mm	kA
velkých	velký	k2eAgInPc2d1
z	z	k7c2
haldy	halda	k1gFnSc2
dolu	dol	k1gInSc2
Kateřina	Kateřina	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
<g/>
:	:	kIx,
Bzenec	Bzenec	k1gInSc1
<g/>
,	,	kIx,
Oslavany	Oslavany	k1gInPc1
–	–	k?
býval	bývat	k5eAaImAgMnS
hojný	hojný	k2eAgMnSc1d1
až	až	k9
v	v	k7c4
1	#num#	k4
cm	cm	kA
velkých	velký	k2eAgInPc6d1
krystalech	krystal	k1gInPc6
<g/>
,	,	kIx,
Zastávka	zastávka	k1gFnSc1
–	–	k?
v	v	k7c6
bílých	bílý	k2eAgFnPc6d1
drúzách	drúza	k1gFnPc6
v	v	k7c6
lokalitě	lokalita	k1gFnSc6
U	u	k7c2
bítešské	bítešský	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
<g/>
:	:	kIx,
na	na	k7c6
Ostravsku	Ostravsko	k1gNnSc6
z	z	k7c2
Doubravy	Doubrava	k1gFnSc2
<g/>
,	,	kIx,
Karviné	Karviná	k1gFnSc2
<g/>
,	,	kIx,
Lazů	Lazů	k?
<g/>
,	,	kIx,
Petřkovic	Petřkovice	k1gFnPc2
<g/>
,	,	kIx,
Poruby	porub	k1gInPc1
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
Orlová	Orlová	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
–	–	k?
zejména	zejména	k9
ze	z	k7c2
sopek	sopka	k1gFnPc2
(	(	kIx(
<g/>
Etna	Etna	k1gFnSc1
<g/>
,	,	kIx,
Stromboli	Strombole	k1gFnSc4
<g/>
,	,	kIx,
Vesuv	Vesuv	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Rusko	Rusko	k1gNnSc1
–	–	k?
hořící	hořící	k2eAgFnSc2d1
haldy	halda	k1gFnSc2
v	v	k7c6
Čeljabinsku	Čeljabinsk	k1gInSc6
(	(	kIx(
<g/>
Ural	Ural	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Palache	Palache	k1gFnSc1
<g/>
,	,	kIx,
C.	C.	kA
<g/>
,	,	kIx,
H.	H.	kA
Berman	Berman	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
C.	C.	kA
Frondel	Frondlo	k1gNnPc2
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
Dana	Dana	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
system	syst	k1gInSc7
of	of	k?
mineralogy	mineralog	k1gMnPc7
<g/>
,	,	kIx,
(	(	kIx(
<g/>
7	#num#	k4
<g/>
th	th	k?
edition	edition	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
volume	volum	k1gInSc5
II	II	kA
<g/>
,	,	kIx,
15	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
</s>
<s>
DUĎA	DUĎA	kA
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
;	;	kIx,
REJL	REJL	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minerály	minerál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografie	k1gFnSc1
Dušan	Dušan	k1gMnSc1
Slivka	slivka	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
české	český	k2eAgFnSc2d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
AVENTINUM	AVENTINUM	kA
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
520	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Velký	velký	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7151	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
salmiak	salmiak	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Salmiak	salmiak	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
mindat	mindat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Salmiak	salmiak	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Webmineral	Webmineral	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Salmiak	salmiak	k1gInSc1
v	v	k7c6
Atlasu	Atlas	k1gInSc6
minerálů	minerál	k1gInPc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mineral	Minerat	k5eAaImAgMnS,k5eAaPmAgMnS
data	datum	k1gNnSc2
publishing	publishing	k1gInSc1
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
</s>
