<s>
Hala	hala	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
účelové	účelový	k2eAgFnSc6d1
stavbě	stavba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
příjmení	příjmení	k1gNnSc4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Hála	Hála	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hala	hala	k1gFnSc1
nového	nový	k2eAgNnSc2d1
výstaviště	výstaviště	k1gNnSc2
v	v	k7c6
Lipsku	Lipsko	k1gNnSc6
</s>
<s>
Slovem	slovem	k6eAd1
hala	hala	k1gFnSc1
obvykle	obvykle	k6eAd1
označujeme	označovat	k5eAaImIp1nP
zastřešený	zastřešený	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
místnost	místnost	k1gFnSc1
<g/>
,	,	kIx,
síň	síň	k1gFnSc1
či	či	k8xC
sál	sál	k1gInSc1
větších	veliký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
neslouží	sloužit	k5eNaImIp3nS
pro	pro	k7c4
ubytování	ubytování	k1gNnSc4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
i	i	k9
o	o	k7c4
označení	označení	k1gNnSc4
pro	pro	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
takovouto	takovýto	k3xDgFnSc4
velkou	velká	k1gFnSc4
místnost	místnost	k1gFnSc4
obklopuje	obklopovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Haly	hala	k1gFnPc1
v	v	k7c6
dopravě	doprava	k1gFnSc6
</s>
<s>
Velké	velký	k2eAgFnPc1d1
haly	hala	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
součástí	součást	k1gFnSc7
velkých	velký	k2eAgInPc2d1
dopravních	dopravní	k2eAgInPc2d1
uzlů	uzel	k1gInPc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
cestuje	cestovat	k5eAaImIp3nS
větší	veliký	k2eAgInSc1d2
počet	počet	k1gInSc1
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
o	o	k7c4
haly	hala	k1gFnPc4
u	u	k7c2
velkých	velký	k2eAgNnPc2d1
nádraží	nádraží	k1gNnPc2
(	(	kIx(
<g/>
nádražní	nádražní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
na	na	k7c6
letištích	letiště	k1gNnPc6
(	(	kIx(
<g/>
letištní	letištní	k2eAgFnSc4d1
<g/>
,	,	kIx,
resp.	resp.	kA
odbavovací	odbavovací	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tovární	tovární	k2eAgFnPc1d1
a	a	k8xC
obchodní	obchodní	k2eAgFnPc1d1
haly	hala	k1gFnPc1
</s>
<s>
Haly	hala	k1gFnSc2
určené	určený	k2eAgFnSc2d1
pro	pro	k7c4
průmyslovou	průmyslový	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
či	či	k8xC
průmyslovou	průmyslový	k2eAgFnSc4d1
montáž	montáž	k1gFnSc4
označujeme	označovat	k5eAaImIp1nP
souslovím	sousloví	k1gNnSc7
tovární	tovární	k2eAgNnSc4d1
hala	halo	k1gNnPc4
případně	případně	k6eAd1
montážní	montážní	k2eAgNnPc4d1
hala	halo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
velkých	velký	k2eAgFnPc2d1
hal	hala	k1gFnPc2
jsou	být	k5eAaImIp3nP
také	také	k9
velmi	velmi	k6eAd1
často	často	k6eAd1
umístěna	umístit	k5eAaPmNgFnS
velká	velký	k2eAgFnSc1d1
nákupní	nákupní	k2eAgFnSc1d1
a	a	k8xC
obchodní	obchodní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
v	v	k7c6
hypermarketech	hypermarket	k1gInPc6
a	a	k8xC
supermaketech	supermaket	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
výstavní	výstavní	k2eAgFnPc1d1
haly	hala	k1gFnPc1
můžeme	moct	k5eAaImIp1nP
spatřit	spatřit	k5eAaPmF
také	také	k9
na	na	k7c6
výstavištích	výstaviště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
haly	hala	k1gFnPc1
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
Velké	velký	k2eAgFnPc1d1
sportovní	sportovní	k2eAgFnPc1d1
haly	hala	k1gFnPc1
mívají	mívat	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
podobu	podoba	k1gFnSc4
samostatných	samostatný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
často	často	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
(	(	kIx(
<g/>
po	po	k7c6
původním	původní	k2eAgInSc6d1
starořímském	starořímský	k2eAgInSc6d1
vzoru	vzor	k1gInSc6
<g/>
)	)	kIx)
arény	aréna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgFnSc2
moderní	moderní	k2eAgFnSc2d1
arény	aréna	k1gFnSc2
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
konstruovány	konstruovat	k5eAaImNgInP
víceúčelově	víceúčelově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
jako	jako	k8xC,k8xS
krytá	krytý	k2eAgNnPc1d1
sportoviště	sportoviště	k1gNnPc1
pro	pro	k7c4
provozování	provozování	k1gNnSc4
mnoha	mnoho	k4c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
sportu	sport	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pro	pro	k7c4
účely	účel	k1gInPc4
kulturní	kulturní	k2eAgFnSc2d1
a	a	k8xC
společenské	společenský	k2eAgFnSc2d1
(	(	kIx(
<g/>
haly	hala	k1gFnSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
kulturní	kulturní	k2eAgInSc1d1
či	či	k8xC
společenský	společenský	k2eAgInSc1d1
sál	sál	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Haly	hala	k1gFnPc1
obvykle	obvykle	k6eAd1
slouží	sloužit	k5eAaImIp3nP
pro	pro	k7c4
provozování	provozování	k1gNnSc4
kolektivních	kolektivní	k2eAgInPc2d1
<g/>
,	,	kIx,
resp.	resp.	kA
halových	halový	k2eAgInPc2d1
sportů	sport	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
košíková	košíková	k1gFnSc1
<g/>
,	,	kIx,
odbíjená	odbíjená	k1gFnSc1
<g/>
,	,	kIx,
házená	házená	k1gFnSc1
<g/>
,	,	kIx,
florbal	florbal	k1gInSc1
<g/>
,	,	kIx,
korfbal	korfbal	k1gInSc1
<g/>
,	,	kIx,
lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
,	,	kIx,
nohejbal	nohejbal	k1gInSc1
<g/>
,	,	kIx,
kolová	kolová	k1gFnSc1
<g/>
,	,	kIx,
sálová	sálový	k2eAgFnSc1d1
kopaná	kopaná	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
specializované	specializovaný	k2eAgFnPc4d1
haly	hala	k1gFnPc4
i	i	k9
pro	pro	k7c4
soutěže	soutěž	k1gFnPc4
individuální	individuální	k2eAgFnSc2d1
<g/>
,	,	kIx,
např.	např.	kA
gymnastické	gymnastický	k2eAgFnSc2d1
<g/>
,	,	kIx,
vzpěračské	vzpěračský	k2eAgFnSc2d1
<g/>
,	,	kIx,
zápasnické	zápasnický	k2eAgFnSc2d1
<g/>
,	,	kIx,
badmintonové	badmintonový	k2eAgFnSc2d1
<g/>
,	,	kIx,
pingpongové	pingpongový	k2eAgFnSc2d1
<g/>
,	,	kIx,
tenisové	tenisový	k2eAgFnSc2d1
<g/>
,	,	kIx,
šermířské	šermířský	k2eAgFnSc2d1
či	či	k8xC
atletické	atletický	k2eAgFnSc2d1
a	a	k8xC
další	další	k2eAgFnSc2d1
haly	hala	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgInP
jak	jak	k6eAd1
pro	pro	k7c4
trénink	trénink	k1gInSc4
(	(	kIx(
<g/>
třeba	třeba	k6eAd1
nafukovací	nafukovací	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
pro	pro	k7c4
provoz	provoz	k1gInSc4
původně	původně	k6eAd1
letních	letní	k2eAgInPc2d1
sportů	sport	k1gInPc2
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
halu	hala	k1gFnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
považována	považován	k2eAgFnSc1d1
každá	každý	k3xTgFnSc1
větší	veliký	k2eAgFnSc1d2
víceúčelová	víceúčelový	k2eAgFnSc1d1
tělocvična	tělocvična	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
sportovních	sportovní	k2eAgFnPc6d1
halách	hala	k1gFnPc6
ale	ale	k8xC
obvykle	obvykle	k6eAd1
nemluvíme	mluvit	k5eNaImIp1nP
u	u	k7c2
úzce	úzko	k6eAd1
specializovaných	specializovaný	k2eAgNnPc2d1
sportovišť	sportoviště	k1gNnPc2
–	–	k?
např.	např.	kA
plavecké	plavecký	k2eAgInPc4d1
stadiony	stadion	k1gInPc4
a	a	k8xC
bazény	bazén	k1gInPc4
<g/>
,	,	kIx,
cyklistické	cyklistický	k2eAgInPc4d1
velodromy	velodrom	k1gInPc4
<g/>
,	,	kIx,
jízdárny	jízdárna	k1gFnPc4
<g/>
,	,	kIx,
kryté	krytý	k2eAgFnPc4d1
střelnice	střelnice	k1gFnPc4
apod.	apod.	kA
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
volby	volba	k1gFnSc2
mezi	mezi	k7c7
materiály	materiál	k1gInPc7
a	a	k8xC
typu	typ	k1gInSc2
stavby	stavba	k1gFnSc2
v	v	k7c6
drtivé	drtivý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
vládne	vládnout	k5eAaImIp3nS
ocelová	ocelový	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocelové	ocelový	k2eAgFnPc1d1
sportovní	sportovní	k2eAgFnPc1d1
haly	hala	k1gFnPc1
mají	mít	k5eAaImIp3nP
řadu	řada	k1gFnSc4
výhod	výhoda	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Prostor	prostor	k1gInSc1
bez	bez	k7c2
vnitřních	vnitřní	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
</s>
<s>
Rychlost	rychlost	k1gFnSc1
výstavby	výstavba	k1gFnSc2
</s>
<s>
Flexibilita	flexibilita	k1gFnSc1
</s>
<s>
Snadná	snadný	k2eAgFnSc1d1
údržba	údržba	k1gFnSc1
</s>
<s>
Místnosti	místnost	k1gFnPc1
v	v	k7c6
domech	dům	k1gInPc6
</s>
<s>
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
větší	veliký	k2eAgFnSc4d2
vstupní	vstupní	k2eAgFnSc4d1
místnost	místnost	k1gFnSc4
(	(	kIx(
<g/>
může	moct	k5eAaImIp3nS
to	ten	k3xDgNnSc1
být	být	k5eAaImF
i	i	k9
velká	velký	k2eAgFnSc1d1
předsíň	předsíň	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
menší	malý	k2eAgInSc1d2
sál	sál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
vchodu	vchod	k1gInSc2
do	do	k7c2
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
budov	budova	k1gFnPc2
či	či	k8xC
jiných	jiný	k2eAgFnPc2d1
specializovaných	specializovaný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
(	(	kIx(
<g/>
metro	metro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
mají	mít	k5eAaImIp3nP
nějaký	nějaký	k3yIgInSc4
širší	široký	k2eAgInSc4d2
společenský	společenský	k2eAgInSc4d1
účel	účel	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
takováto	takovýto	k3xDgFnSc1
vstupní	vstupní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
někdy	někdy	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
slovem	slovo	k1gNnSc7
vestibul	vestibulum	k1gNnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
dvorana	dvorana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
halách	hala	k1gFnPc6
většinou	většinou	k6eAd1
nemluvíme	mluvit	k5eNaImIp1nP
u	u	k7c2
převážné	převážný	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
církevních	církevní	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
kláštery	klášter	k1gInPc1
<g/>
,	,	kIx,
chrámy	chrám	k1gInPc1
<g/>
,	,	kIx,
kostely	kostel	k1gInPc1
<g/>
,	,	kIx,
mešity	mešita	k1gFnPc1
<g/>
,	,	kIx,
svatyně	svatyně	k1gFnPc1
apod.	apod.	kA
Za	za	k7c4
haly	hala	k1gFnPc4
také	také	k9
obvykle	obvykle	k6eAd1
nejsou	být	k5eNaImIp3nP
považovány	považován	k2eAgFnPc1d1
ani	ani	k8xC
velké	velký	k2eAgFnPc1d1
školní	školní	k2eAgFnPc1d1
učebny	učebna	k1gFnPc1
či	či	k8xC
posluchárny	posluchárna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
ocelové	ocelový	k2eAgFnSc2d1
haly	hala	k1gFnSc2
</s>
<s>
Ocelové	ocelový	k2eAgFnPc1d1
konstrukce	konstrukce	k1gFnPc1
pro	pro	k7c4
montované	montovaný	k2eAgFnPc4d1
haly	hala	k1gFnPc4
jsou	být	k5eAaImIp3nP
navrhovány	navrhovat	k5eAaImNgFnP
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
velikosti	velikost	k1gFnPc4
rozponů	rozpon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
šířky	šířka	k1gFnPc4
haly	hala	k1gFnSc2
od	od	k7c2
6	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
m	m	kA
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
větších	veliký	k2eAgInPc2d2
rozponů	rozpon	k1gInPc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
přidávat	přidávat	k5eAaImF
podpěry	podpěra	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
odlehčí	odlehčit	k5eAaPmIp3nP
zatížení	zatížení	k1gNnSc4
rámů	rám	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rámy	rám	k1gInPc7
ocelových	ocelový	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
se	s	k7c7
šroubovanými	šroubovaný	k2eAgInPc7d1
spoji	spoj	k1gInPc7
bývají	bývat	k5eAaImIp3nP
většinou	většinou	k6eAd1
navržené	navržený	k2eAgInPc4d1
z	z	k7c2
tzv.	tzv.	kA
IPE	IPE	kA
profilů	profil	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
střešní	střešní	k2eAgFnSc6d1
konstrukci	konstrukce	k1gFnSc6
jsou	být	k5eAaImIp3nP
navzájem	navzájem	k6eAd1
propojeny	propojit	k5eAaPmNgFnP
vaznicemi	vaznice	k1gFnPc7
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
pokládá	pokládat	k5eAaImIp3nS
střešní	střešní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
buď	buď	k8xC
zateplený	zateplený	k2eAgInSc1d1
nebo	nebo	k8xC
nezateplený	zateplený	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
také	také	k6eAd1
navrhují	navrhovat	k5eAaImIp3nP
typy	typ	k1gInPc1
a	a	k8xC
hustota	hustota	k1gFnSc1
vaznic	vaznice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bocích	bok	k1gInPc6
jsou	být	k5eAaImIp3nP
rámy	rám	k1gInPc1
propojené	propojený	k2eAgInPc1d1
tzv.	tzv.	kA
paždíky	paždík	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
poté	poté	k6eAd1
připevňuje	připevňovat	k5eAaImIp3nS
boční	boční	k2eAgMnPc1d1
opláštění	opláštěný	k2eAgMnPc1d1
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rovněž	rovněž	k9
zateplené	zateplený	k2eAgInPc4d1
i	i	k8xC
nezateplené	zateplený	k2eNgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
Rámy	rám	k1gInPc1
ocelových	ocelový	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
od	od	k7c2
sebe	se	k3xPyFc2
vzdáleny	vzdálit	k5eAaPmNgInP
zpravidla	zpravidla	k6eAd1
od	od	k7c2
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
m	m	kA
–	–	k?
tuto	tento	k3xDgFnSc4
vzájemnou	vzájemný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
nazýváme	nazývat	k5eAaImIp1nP
tzv.	tzv.	kA
modulem	modul	k1gInSc7
haly	hala	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projektanti	projektant	k1gMnPc1
navrhují	navrhovat	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
moduly	modul	k1gInPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
celková	celkový	k2eAgFnSc1d1
ocelová	ocelový	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
byla	být	k5eAaImAgFnS
stabilní	stabilní	k2eAgFnSc1d1
a	a	k8xC
současně	současně	k6eAd1
i	i	k9
ekonomická	ekonomický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Každá	každý	k3xTgFnSc1
ocelová	ocelový	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
je	být	k5eAaImIp3nS
posuzována	posuzovat	k5eAaImNgFnS
individuálně	individuálně	k6eAd1
<g/>
,	,	kIx,
složení	složení	k1gNnSc1
profilů	profil	k1gInPc2
je	být	k5eAaImIp3nS
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
mnoha	mnoho	k4c6
faktorech	faktor	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
rozpon	rozpon	k1gInSc1
haly	hala	k1gFnSc2
</s>
<s>
výška	výška	k1gFnSc1
haly	hala	k1gFnSc2
</s>
<s>
zatížení	zatížení	k1gNnSc1
sněhem	sníh	k1gInSc7
</s>
<s>
povětrnostní	povětrnostní	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
</s>
<s>
požadavky	požadavek	k1gInPc1
investora	investor	k1gMnSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
nafukovací	nafukovací	k2eAgFnSc1d1
hala	hala	k1gFnSc1
</s>
<s>
letištní	letištní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
resp.	resp.	kA
odbavovací	odbavovací	k2eAgFnSc1d1
hala	hala	k1gFnSc1
</s>
<s>
nádražní	nádražní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
</s>
<s>
tělocvična	tělocvična	k1gFnSc1
</s>
<s>
posluchárna	posluchárna	k1gFnSc1
</s>
<s>
sál	sál	k1gInSc1
</s>
<s>
aula	aula	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
hala	halo	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4023023-5	4023023-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
227	#num#	k4
</s>
