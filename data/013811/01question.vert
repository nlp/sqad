<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
zastřešený	zastřešený	k2eAgInSc1d1
prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
místnost	místnost	k1gFnSc1
či	či	k8xC
sál	sál	k1gInSc1
větších	veliký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
neslouží	sloužit	k5eNaImIp3nS
pro	pro	k7c4
ubytování	ubytování	k1gNnSc4
osob	osoba	k1gFnPc2
<g/>
?	?	kIx.
</s>