<s>
Slovem	slovem	k6eAd1
hala	hala	k1gFnSc1
obvykle	obvykle	k6eAd1
označujeme	označovat	k5eAaImIp1nP
zastřešený	zastřešený	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
místnost	místnost	k1gFnSc1
<g/>
,	,	kIx,
síň	síň	k1gFnSc1
či	či	k8xC
sál	sál	k1gInSc1
větších	veliký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
neslouží	sloužit	k5eNaImIp3nS
pro	pro	k7c4
ubytování	ubytování	k1gNnSc4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>