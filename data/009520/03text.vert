<p>
<s>
Květináč	květináč	k1gInSc1	květináč
nebo	nebo	k8xC	nebo
kořenáč	kořenáč	k1gInSc1	kořenáč
(	(	kIx(	(
<g/>
na	na	k7c6	na
Chodsku	Chodsko	k1gNnSc6	Chodsko
vrhlík	vrhlík	k?	vrhlík
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nádoba	nádoba	k1gFnSc1	nádoba
nejrůznější	různý	k2eAgFnSc2d3	nejrůznější
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
cizorodém	cizorodý	k2eAgNnSc6d1	cizorodé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nádoba	nádoba	k1gFnSc1	nádoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgInPc1d1	používaný
patří	patřit	k5eAaImIp3nP	patřit
plasty	plast	k1gInPc1	plast
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
či	či	k8xC	či
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nádoba	nádoba	k1gFnSc1	nádoba
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
dnu	den	k1gInSc6	den
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
otvory	otvor	k1gInPc4	otvor
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
odtéká	odtékat	k5eAaImIp3nS	odtékat
po	po	k7c6	po
zalévání	zalévání	k1gNnSc6	zalévání
přebytečná	přebytečný	k2eAgFnSc1d1	přebytečná
voda	voda	k1gFnSc1	voda
na	na	k7c4	na
misku	miska	k1gFnSc4	miska
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
zásoby	zásoba	k1gFnSc2	zásoba
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tak	tak	k6eAd1	tak
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
trvalé	trvalý	k2eAgNnSc4d1	trvalé
podmáčení	podmáčení	k1gNnSc4	podmáčení
kořenů	kořen	k1gInPc2	kořen
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
následně	následně	k6eAd1	následně
vést	vést	k5eAaImF	vést
k	k	k7c3	k
jejích	její	k3xOp3gNnPc2	její
hnilobě	hniloba	k1gFnSc6	hniloba
či	či	k8xC	či
plesnivění	plesnivění	k1gNnSc6	plesnivění
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
má	mít	k5eAaImIp3nS	mít
zde	zde	k6eAd1	zde
půdu	půda	k1gFnSc4	půda
relativně	relativně	k6eAd1	relativně
vlhkou	vlhký	k2eAgFnSc4d1	vlhká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřesycenou	přesycený	k2eNgFnSc7d1	přesycený
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kořenovým	kořenový	k2eAgInSc7d1	kořenový
systémem	systém	k1gInSc7	systém
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
sama	sám	k3xTgFnSc1	sám
odebírá	odebírat	k5eAaImIp3nS	odebírat
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
jí	on	k3xPp3gFnSc3	on
využije	využít	k5eAaPmIp3nS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
květináč	květináč	k1gInSc1	květináč
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
nádoba	nádoba	k1gFnSc1	nádoba
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
veškerou	veškerý	k3xTgFnSc4	veškerý
dodávanou	dodávaný	k2eAgFnSc4d1	dodávaná
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
tedy	tedy	k9	tedy
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
rostlině	rostlina	k1gFnSc6	rostlina
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
hospodařit	hospodařit	k5eAaImF	hospodařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
květináče	květináč	k1gInSc2	květináč
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
zpravidla	zpravidla	k6eAd1	zpravidla
substrát	substrát	k1gInSc1	substrát
s	s	k7c7	s
pěstovanou	pěstovaný	k2eAgFnSc7d1	pěstovaná
rostlinou	rostlina	k1gFnSc7	rostlina
<g/>
,	,	kIx,	,
dodávanou	dodávaný	k2eAgFnSc7d1	dodávaná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
případně	případně	k6eAd1	případně
také	také	k9	také
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Hnojivo	hnojivo	k1gNnSc1	hnojivo
se	se	k3xPyFc4	se
dodává	dodávat	k5eAaImIp3nS	dodávat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zvolna	zvolna	k6eAd1	zvolna
vodou	voda	k1gFnSc7	voda
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
do	do	k7c2	do
zeminy	zemina	k1gFnSc2	zemina
dostávají	dostávat	k5eAaImIp3nP	dostávat
živiny	živina	k1gFnPc1	živina
potřebné	potřebné	k1gNnSc4	potřebné
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
pěstované	pěstovaný	k2eAgFnSc2d1	pěstovaná
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ozdobné	ozdobný	k2eAgInPc1d1	ozdobný
květináče	květináč	k1gInPc1	květináč
<g/>
,	,	kIx,	,
Bobee	Bobee	k1gNnPc1	Bobee
Needhamová	Needhamový	k2eAgNnPc1d1	Needhamový
<g/>
,	,	kIx,	,
Vydání	vydání	k1gNnSc1	vydání
<g/>
:	:	kIx,	:
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
Název	název	k1gInSc1	název
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
Fantastic	Fantastice	k1gFnPc2	Fantastice
flowerpots	flowerpots	k6eAd1	flowerpots
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jiřina	Jiřina	k1gFnSc1	Jiřina
Stárková	Stárková	k1gFnSc1	Stárková
<g/>
,	,	kIx,	,
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7341-872-X	[number]	k4	80-7341-872-X
</s>
</p>
<p>
<s>
Komposty	kompost	k1gInPc1	kompost
<g/>
,	,	kIx,	,
pařeniště	pařeniště	k1gNnPc1	pařeniště
<g/>
,	,	kIx,	,
truhlíky	truhlík	k1gInPc1	truhlík
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Himmelhuber	Himmelhuber	k1gMnSc1	Himmelhuber
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Název	název	k1gInSc1	název
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
Selbst	Selbst	k1gMnSc1	Selbst
Pflanz	Pflanz	k1gMnSc1	Pflanz
<g/>
-	-	kIx~	-
und	und	k?	und
Kompostsysteme	Kompostsysteme	k1gFnSc3	Kompostsysteme
bauen	bauen	k1gInSc4	bauen
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Nora	Nora	k1gFnSc1	Nora
Martišková	Martišková	k1gFnSc1	Martišková
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-247-0754-3	[number]	k4	80-247-0754-3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
květináč	květináč	k1gInSc1	květináč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
