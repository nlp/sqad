<s>
Květináč	květináč	k1gInSc1	květináč
nebo	nebo	k8xC	nebo
kořenáč	kořenáč	k1gInSc1	kořenáč
(	(	kIx(	(
<g/>
na	na	k7c6	na
Chodsku	Chodsko	k1gNnSc6	Chodsko
vrhlík	vrhlík	k?	vrhlík
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nádoba	nádoba	k1gFnSc1	nádoba
nejrůznější	různý	k2eAgFnSc2d3	nejrůznější
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
cizorodém	cizorodý	k2eAgNnSc6d1	cizorodé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
