<s>
Britské	britský	k2eAgInPc1d1	britský
ostrovy	ostrov	k1gInPc1	ostrov
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc4	souostroví
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
při	při	k7c6	při
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
British	British	k1gMnSc1	British
Isles	Isles	k1gMnSc1	Isles
<g/>
,	,	kIx,	,
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
souostroví	souostroví	k1gNnSc4	souostroví
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
Oileáin	Oileáin	k2eAgInSc1d1	Oileáin
Iarthair	Iarthair	k1gInSc1	Iarthair
Eorpa	Eorpa	k1gFnSc1	Eorpa
(	(	kIx(	(
<g/>
Západoevropské	západoevropský	k2eAgInPc1d1	západoevropský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Éire	Éire	k1gInSc1	Éire
agus	agus	k1gInSc1	agus
an	an	k?	an
Bhreatain	Bhreatain	k2eAgInSc1d1	Bhreatain
Mhór	Mhór	k1gInSc1	Mhór
(	(	kIx(	(
<g/>
Irsko	Irsko	k1gNnSc1	Irsko
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
64	[number]	k4	64
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
315	[number]	k4	315
134	[number]	k4	134
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
ostrovy	ostrov	k1gInPc1	ostrov
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
ostrov	ostrov	k1gInSc1	ostrov
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
součástmi	součást	k1gFnPc7	součást
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
jsou	být	k5eAaImIp3nP	být
Ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
uprostřed	uprostřed	k7c2	uprostřed
Irského	irský	k2eAgNnSc2d1	irské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
souostroví	souostroví	k1gNnSc1	souostroví
Hebridy	Hebridy	k1gFnPc1	Hebridy
<g/>
,	,	kIx,	,
Shetlandy	Shetlanda	k1gFnPc1	Shetlanda
a	a	k8xC	a
Orkneje	Orkneje	k1gFnPc1	Orkneje
u	u	k7c2	u
severního	severní	k2eAgNnSc2d1	severní
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Anglesey	Anglesea	k1gFnSc2	Anglesea
při	při	k7c6	při
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Wight	Wight	k2eAgInSc1d1	Wight
u	u	k7c2	u
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Arran	Arran	k1gInSc1	Arran
a	a	k8xC	a
Bute	Bute	k1gFnSc1	Bute
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Clyde	Clyd	k1gInSc5	Clyd
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc4	ostrov
Achill	Achilla	k1gFnPc2	Achilla
a	a	k8xC	a
Aranské	Aranský	k2eAgInPc4d1	Aranský
ostrovy	ostrov	k1gInPc4	ostrov
u	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Scilly	Scilla	k1gFnSc2	Scilla
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
anglického	anglický	k2eAgInSc2d1	anglický
poloostrova	poloostrov	k1gInSc2	poloostrov
Cornwall	Cornwalla	k1gFnPc2	Cornwalla
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
jsou	být	k5eAaImIp3nP	být
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc1	ostrov
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Irskou	irský	k2eAgFnSc4d1	irská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
Ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
britskou	britský	k2eAgFnSc7d1	britská
korunní	korunní	k2eAgFnSc7d1	korunní
dependencí	dependence	k1gFnSc7	dependence
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
příslušnosti	příslušnost	k1gFnSc3	příslušnost
k	k	k7c3	k
britské	britský	k2eAgFnSc3d1	britská
koruně	koruna	k1gFnSc3	koruna
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
k	k	k7c3	k
Britským	britský	k2eAgInPc3d1	britský
ostrovům	ostrov	k1gInPc3	ostrov
přiřazovány	přiřazovat	k5eAaImNgInP	přiřazovat
také	také	k9	také
Normanské	normanský	k2eAgInPc1d1	normanský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
britská	britský	k2eAgNnPc1d1	Britské
závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
Guernsey	Guernsea	k1gFnSc2	Guernsea
a	a	k8xC	a
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
zeměpisně	zeměpisně	k6eAd1	zeměpisně
náležejí	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
francouzské	francouzský	k2eAgFnSc3d1	francouzská
pevnině	pevnina	k1gFnSc3	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
existuje	existovat	k5eAaImIp3nS	existovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
běžného	běžný	k2eAgInSc2d1	běžný
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
pojmu	pojem	k1gInSc2	pojem
British	British	k1gMnSc1	British
Isles	Isles	k1gMnSc1	Isles
ještě	ještě	k9	ještě
příležitostně	příležitostně	k6eAd1	příležitostně
užívané	užívaný	k2eAgNnSc1d1	užívané
oficiální	oficiální	k2eAgNnSc1d1	oficiální
politické	politický	k2eAgNnSc1d1	politické
označení	označení	k1gNnSc1	označení
British	Britisha	k1gFnPc2	Britisha
Islands	Islandsa	k1gFnPc2	Islandsa
<g/>
,	,	kIx,	,
zastřešující	zastřešující	k2eAgNnSc1d1	zastřešující
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
Guernsey	Guernse	k1gMnPc4	Guernse
a	a	k8xC	a
Jersey	Jerse	k1gMnPc4	Jerse
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Britské	britský	k2eAgInPc1d1	britský
ostrovy	ostrov	k1gInPc1	ostrov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
