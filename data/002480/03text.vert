<s>
Stephen	Stephen	k2eAgMnSc1d1	Stephen
John	John	k1gMnSc1	John
Fry	Fry	k1gMnSc1	Fry
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgMnSc1d1	televizní
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
kulturně	kulturně	k6eAd1	kulturně
zviditelnil	zviditelnit	k5eAaPmAgMnS	zviditelnit
televizní	televizní	k2eAgFnSc7d1	televizní
produkcí	produkce	k1gFnSc7	produkce
herecké	herecký	k2eAgFnSc2d1	herecká
skupiny	skupina	k1gFnSc2	skupina
Cambridge	Cambridge	k1gFnPc1	Cambridge
Footlights	Footlights	k1gInSc4	Footlights
Review	Review	k1gFnSc2	Review
jménem	jméno	k1gNnSc7	jméno
The	The	k1gFnSc2	The
Cellar	Cellar	k1gMnSc1	Cellar
Tapes	Tapes	k1gMnSc1	Tapes
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k1gMnSc7	známý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
převážně	převážně	k6eAd1	převážně
svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
s	s	k7c7	s
Hughem	Hugh	k1gInSc7	Hugh
Lauriem	Laurium	k1gNnSc7	Laurium
v	v	k7c6	v
komickém	komický	k2eAgNnSc6d1	komické
duu	duo	k1gNnSc6	duo
známém	známý	k2eAgNnSc6d1	známé
jako	jako	k9	jako
Fry	Fry	k1gMnSc2	Fry
and	and	k?	and
Laurie	Laurie	k1gFnSc2	Laurie
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
televizním	televizní	k2eAgInSc7d1	televizní
počinem	počin	k1gInSc7	počin
této	tento	k3xDgFnSc2	tento
dvojice	dvojice	k1gFnSc2	dvojice
byl	být	k5eAaImAgInS	být
šestadvacetiepizodový	šestadvacetiepizodový	k2eAgInSc1d1	šestadvacetiepizodový
televizní	televizní	k2eAgInSc1d1	televizní
komediální	komediální	k2eAgInSc1d1	komediální
seriál	seriál	k1gInSc1	seriál
A	a	k8xC	a
Bit	bit	k1gInSc1	bit
of	of	k?	of
Fry	Fry	k1gFnSc2	Fry
and	and	k?	and
Laurie	Laurie	k1gFnSc2	Laurie
sestávající	sestávající	k2eAgFnSc2d1	sestávající
z	z	k7c2	z
krátkých	krátký	k2eAgInPc2d1	krátký
skečů	skeč	k1gInPc2	skeč
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Hughem	Hugh	k1gInSc7	Hugh
Lauriem	Laurium	k1gNnSc7	Laurium
také	také	k6eAd1	také
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Jeeves	Jeeves	k1gMnSc1	Jeeves
and	and	k?	and
Wooster	Wooster	k1gMnSc1	Wooster
inspirovaném	inspirovaný	k2eAgInSc6d1	inspirovaný
knihami	kniha	k1gFnPc7	kniha
P.	P.	kA	P.
G.	G.	kA	G.
Wodehouse	Wodehouse	k1gFnSc1	Wodehouse
<g/>
.	.	kIx.	.
</s>
<s>
Fry	Fry	k?	Fry
je	být	k5eAaImIp3nS	být
také	také	k9	také
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
poli	pole	k1gNnSc6	pole
debutoval	debutovat	k5eAaBmAgInS	debutovat
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
románem	román	k1gInSc7	román
The	The	k1gFnPc2	The
Liar	Liara	k1gFnPc2	Liara
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
populárních	populární	k2eAgFnPc2d1	populární
knih	kniha	k1gFnPc2	kniha
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
autobiografie	autobiografie	k1gFnSc1	autobiografie
pokrývající	pokrývající	k2eAgFnSc1d1	pokrývající
jeho	jeho	k3xOp3gFnSc2	jeho
prvních	první	k4xOgNnPc6	první
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
Moab	Moab	k1gInSc1	Moab
is	is	k?	is
my	my	k3xPp1nPc1	my
Washpot	Washpot	k1gInSc1	Washpot
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
rozešel	rozejít	k5eAaPmAgInS	rozejít
po	po	k7c6	po
14	[number]	k4	14
<g/>
letém	letý	k2eAgInSc6d1	letý
partnerském	partnerský	k2eAgInSc6d1	partnerský
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Danielem	Daniel	k1gMnSc7	Daniel
Cohenem	Cohen	k1gMnSc7	Cohen
<g/>
.	.	kIx.	.
</s>
<s>
Fry	Fry	k?	Fry
trpí	trpět	k5eAaImIp3nS	trpět
bipolární	bipolární	k2eAgFnSc7d1	bipolární
poruchou	porucha	k1gFnSc7	porucha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
s	s	k7c7	s
27	[number]	k4	27
<g/>
letým	letý	k2eAgMnSc7d1	letý
partnerem	partner	k1gMnSc7	partner
Elliottem	Elliott	k1gMnSc7	Elliott
Spencerem	Spencer	k1gMnSc7	Spencer
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stephen	Stephna	k1gFnPc2	Stephna
Fry	Fry	k1gFnSc2	Fry
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Stephen	Stephen	k1gInSc1	Stephen
Fry	Fry	k1gFnSc4	Fry
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Stephen	Stephna	k1gFnPc2	Stephna
Fry	Fry	k1gFnSc7	Fry
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Fry	Fry	k1gMnSc1	Fry
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
