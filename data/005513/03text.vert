<s>
Císař	Císař	k1gMnSc1	Císař
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
panovnická	panovnický	k2eAgFnSc1d1	panovnická
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
titulu	titul	k1gInSc2	titul
caesar	caesara	k1gFnPc2	caesara
<g/>
,	,	kIx,	,
odvozeného	odvozený	k2eAgMnSc2d1	odvozený
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
Gaia	Gaius	k1gMnSc2	Gaius
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
obdobou	obdoba	k1gFnSc7	obdoba
je	být	k5eAaImIp3nS	být
hodnost	hodnost	k1gFnSc4	hodnost
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
slovanských	slovanský	k2eAgFnPc6d1	Slovanská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
či	či	k8xC	či
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
popisuje	popisovat	k5eAaImIp3nS	popisovat
panovníka	panovník	k1gMnSc4	panovník
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Císaři	císař	k1gMnPc1	císař
přísluší	příslušet	k5eAaImIp3nP	příslušet
oslovení	oslovení	k1gNnSc4	oslovení
císařské	císařský	k2eAgNnSc4d1	císařské
Veličenstvo	veličenstvo	k1gNnSc4	veličenstvo
<g/>
,	,	kIx,	,
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
císařského	císařský	k2eAgInSc2d1	císařský
rodu	rod	k1gInSc2	rod
přísluší	příslušet	k5eAaImIp3nS	příslušet
oslovení	oslovení	k1gNnPc1	oslovení
císařská	císařský	k2eAgFnSc1d1	císařská
Výsost	výsost	k1gFnSc1	výsost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
císař	císař	k1gMnSc1	císař
zároveň	zároveň	k6eAd1	zároveň
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
přísluší	příslušet	k5eAaImIp3nS	příslušet
mu	on	k3xPp3gMnSc3	on
titul	titul	k1gInSc4	titul
císařské	císařský	k2eAgNnSc4d1	císařské
a	a	k8xC	a
královské	královský	k2eAgNnSc4d1	královské
Veličenstvo	veličenstvo	k1gNnSc4	veličenstvo
<g/>
,	,	kIx,	,
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
císařského	císařský	k2eAgInSc2d1	císařský
rodu	rod	k1gInSc2	rod
pak	pak	k6eAd1	pak
císařská	císařský	k2eAgFnSc1d1	císařská
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediné	k1gNnSc7	jediné
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
panujícím	panující	k2eAgMnSc7d1	panující
císařem	císař	k1gMnSc7	císař
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgMnSc1d1	japonský
císař	císař	k1gMnSc1	císař
Akihito	Akihit	k2eAgNnSc4d1	Akihito
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
císařů	císař	k1gMnPc2	císař
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Římskou	římský	k2eAgFnSc7d1	římská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
císařem	císař	k1gMnSc7	císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Oktavián	Oktavián	k1gMnSc1	Oktavián
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
roku	rok	k1gInSc2	rok
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
augustus	augustus	k1gMnSc1	augustus
(	(	kIx(	(
<g/>
vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tituly	titul	k1gInPc1	titul
augustus	augustus	k1gInSc1	augustus
a	a	k8xC	a
caesar	caesar	k1gInSc1	caesar
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
používaly	používat	k5eAaImAgFnP	používat
římskými	římský	k2eAgInPc7d1	římský
císaři	císař	k1gMnSc3	císař
v	v	k7c6	v
období	období	k1gNnSc6	období
principátu	principát	k1gInSc2	principát
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
dominátu	dominát	k1gInSc2	dominát
byl	být	k5eAaImAgMnS	být
titul	titul	k1gInSc4	titul
caesar	caesar	k1gMnSc1	caesar
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
mladšímu	mladý	k2eAgMnSc3d2	mladší
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
,	,	kIx,	,
spoluvládci	spoluvládce	k1gMnPc1	spoluvládce
augusta	august	k1gMnSc4	august
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
roku	rok	k1gInSc2	rok
476	[number]	k4	476
se	se	k3xPyFc4	se
titul	titul	k1gInSc1	titul
císaře	císař	k1gMnSc2	císař
užíval	užívat	k5eAaImAgInS	užívat
výhradně	výhradně	k6eAd1	výhradně
jen	jen	k9	jen
v	v	k7c6	v
přeživší	přeživší	k2eAgFnSc6d1	přeživší
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
–	–	k?	–
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
obnoven	obnovit	k5eAaPmNgInS	obnovit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
800	[number]	k4	800
korunován	korunován	k2eAgInSc1d1	korunován
papežem	papež	k1gMnSc7	papež
Lvem	Lev	k1gMnSc7	Lev
III	III	kA	III
<g/>
.	.	kIx.	.
jakožto	jakožto	k8xS	jakožto
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
a	a	k8xC	a
znovuobnovitel	znovuobnovitel	k1gMnSc1	znovuobnovitel
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
jeho	jeho	k3xOp3gFnSc2	jeho
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
užívání	užívání	k1gNnSc1	užívání
titulu	titul	k1gInSc2	titul
nakonec	nakonec	k6eAd1	nakonec
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
ve	v	k7c6	v
Východofranské	východofranský	k2eAgFnSc6d1	Východofranská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
962	[number]	k4	962
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
titul	titul	k1gInSc1	titul
císaře	císař	k1gMnSc2	císař
byl	být	k5eAaImAgInS	být
doposud	doposud	k6eAd1	doposud
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
ideou	idea	k1gFnSc7	idea
obnovy	obnova	k1gFnSc2	obnova
či	či	k8xC	či
pokračování	pokračování	k1gNnSc4	pokračování
Říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
mimo	mimo	k7c4	mimo
tento	tento	k3xDgInSc4	tento
rámec	rámec	k1gInSc4	rámec
jako	jako	k8xS	jako
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
prestiže	prestiž	k1gFnSc2	prestiž
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
a	a	k8xC	a
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
ve	v	k7c6	v
východoevropských	východoevropský	k2eAgInPc6d1	východoevropský
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
hodnost	hodnost	k1gFnSc1	hodnost
císaře	císař	k1gMnSc2	císař
např.	např.	kA	např.
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
(	(	kIx(	(
<g/>
913	[number]	k4	913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
(	(	kIx(	(
<g/>
1345	[number]	k4	1345
<g/>
)	)	kIx)	)
či	či	k8xC	či
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Imperator	Imperator	k1gMnSc1	Imperator
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
přízvisek	přízvisko	k1gNnPc2	přízvisko
vládců	vládce	k1gMnPc2	vládce
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Seznam	seznam	k1gInSc4	seznam
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
-	-	kIx~	-
používající	používající	k2eAgInSc1d1	používající
řecký	řecký	k2eAgInSc1d1	řecký
termín	termín	k1gInSc1	termín
basileos	basileosa	k1gFnPc2	basileosa
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
vládl	vládnout	k5eAaImAgMnS	vládnout
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
–	–	k?	–
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
historicky	historicky	k6eAd1	historicky
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
zaniklé	zaniklý	k2eAgNnSc4d1	zaniklé
antické	antický	k2eAgNnSc4d1	antické
císařství	císařství	k1gNnSc4	císařství
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
řídící	řídící	k2eAgNnSc1d1	řídící
římské	římský	k2eAgNnSc1d1	římské
impérium	impérium	k1gNnSc1	impérium
(	(	kIx(	(
<g/>
Romanum	Romanum	k?	Romanum
gubernans	gubernans	k1gInSc1	gubernans
imperium	imperium	k1gNnSc1	imperium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
o	o	k7c4	o
římské	římský	k2eAgNnSc4d1	římské
císařství	císařství	k1gNnSc4	císařství
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
císařství	císařství	k1gNnSc1	císařství
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
byl	být	k5eAaImAgInS	být
především	především	k6eAd1	především
ochráncem	ochránce	k1gMnSc7	ochránce
západoevropského	západoevropský	k2eAgNnSc2d1	západoevropské
křesťanstva	křesťanstvo	k1gNnSc2	křesťanstvo
a	a	k8xC	a
sídelního	sídelní	k2eAgNnSc2d1	sídelní
města	město	k1gNnSc2	město
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nesídlil	sídlit	k5eNaImAgMnS	sídlit
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
císařem	císař	k1gMnSc7	císař
říše	říš	k1gFnSc2	říš
Římské	římský	k2eAgFnPc1d1	římská
a	a	k8xC	a
obnovitelem	obnovitel	k1gMnSc7	obnovitel
dlouho	dlouho	k6eAd1	dlouho
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
západořímského	západořímský	k2eAgInSc2d1	západořímský
císařského	císařský	k2eAgInSc2d1	císařský
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
800	[number]	k4	800
n.	n.	k?	n.
l.	l.	k?	l.
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
nebyl	být	k5eNaImAgInS	být
dědičný	dědičný	k2eAgMnSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgMnSc1d1	latinský
císař	císař	k1gMnSc1	císař
-	-	kIx~	-
vládce	vládce	k1gMnSc1	vládce
křižáckého	křižácký	k2eAgInSc2d1	křižácký
státu	stát	k1gInSc2	stát
Latinské	latinský	k2eAgNnSc1d1	latinské
císařství	císařství	k1gNnSc1	císařství
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
latinsky	latinsky	k6eAd1	latinsky
Imperator	Imperator	k1gMnSc1	Imperator
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
Císař	Císař	k1gMnSc1	Císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
-	-	kIx~	-
(	(	kIx(	(
<g/>
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
historických	historický	k2eAgInPc6d1	historický
textech	text	k1gInPc6	text
používán	používat	k5eAaImNgInS	používat
i	i	k8xC	i
titul	titul	k1gInSc1	titul
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
–	–	k?	–
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
I.	I.	kA	I.
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
začala	začít	k5eAaPmAgFnS	začít
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
titul	titul	k1gInSc1	titul
císaře	císař	k1gMnSc2	císař
opakovaně	opakovaně	k6eAd1	opakovaně
ocital	ocitat	k5eAaImAgMnS	ocitat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
německých	německý	k2eAgMnPc2d1	německý
králů	král	k1gMnPc2	král
a	a	k8xC	a
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nové	nový	k2eAgNnSc4d1	nové
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
říši	říše	k1gFnSc4	říše
i	i	k9	i
pro	pro	k7c4	pro
císaře	císař	k1gMnSc4	císař
–	–	k?	–
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgMnSc2d1	německý
a	a	k8xC	a
Císař	Císař	k1gMnSc1	Císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgMnSc2d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
císařský	císařský	k2eAgInSc1d1	císařský
titul	titul	k1gInSc1	titul
rovněž	rovněž	k9	rovněž
nebyl	být	k5eNaImAgInS	být
dědičný	dědičný	k2eAgInSc1d1	dědičný
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
císařem	císař	k1gMnSc7	císař
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaPmF	stát
jen	jen	k9	jen
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
právo	právo	k1gNnSc1	právo
korunovat	korunovat	k5eAaBmF	korunovat
jen	jen	k9	jen
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Samotného	samotný	k2eAgMnSc2d1	samotný
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
měli	mít	k5eAaImAgMnP	mít
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
jen	jen	k9	jen
volitelé	volitel	k1gMnPc1	volitel
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
římské	římský	k2eAgMnPc4d1	římský
krále	král	k1gMnPc4	král
voleni	volit	k5eAaImNgMnP	volit
jen	jen	k9	jen
členové	člen	k1gMnPc1	člen
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
automaticky	automaticky	k6eAd1	automaticky
získávali	získávat	k5eAaImAgMnP	získávat
i	i	k8xC	i
titul	titul	k1gInSc4	titul
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
korunovace	korunovace	k1gFnSc2	korunovace
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Posledním	poslední	k2eAgMnSc7d1	poslední
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
korunoval	korunovat	k5eAaBmAgMnS	korunovat
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Fridrich	Fridrich	k1gMnSc1	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgMnSc7d1	poslední
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
korunoval	korunovat	k5eAaBmAgMnS	korunovat
sám	sám	k3xTgMnSc1	sám
papež	papež	k1gMnSc1	papež
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
)	)	kIx)	)
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgInSc2d1	německý
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
titul	titul	k1gInSc4	titul
jejího	její	k3xOp3gMnSc2	její
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
rakouský	rakouský	k2eAgMnSc1d1	rakouský
–	–	k?	–
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
přijal	přijmout	k5eAaPmAgMnS	přijmout
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgMnSc2d1	německý
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
další	další	k2eAgInSc4d1	další
císařský	císařský	k2eAgInSc4d1	císařský
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
císaře	císař	k1gMnSc2	císař
rakouského	rakouský	k2eAgMnSc2d1	rakouský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgInSc2d1	německý
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgInS	ponechat
pouze	pouze	k6eAd1	pouze
titul	titul	k1gInSc1	titul
císaře	císař	k1gMnSc2	císař
rakouského	rakouský	k2eAgMnSc2d1	rakouský
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
František	Františka	k1gFnPc2	Františka
I.	I.	kA	I.
Titul	titul	k1gInSc1	titul
císaře	císař	k1gMnSc2	císař
rakouského	rakouský	k2eAgMnSc2d1	rakouský
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
rodu	rod	k1gInSc6	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
dědil	dědit	k5eAaImAgMnS	dědit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
–	–	k?	–
panovníci	panovník	k1gMnPc1	panovník
ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
do	do	k7c2	do
r.	r.	kA	r.
1918	[number]	k4	1918
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
car	car	k1gMnSc1	car
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
císařským	císařský	k2eAgInSc7d1	císařský
titulem	titul	k1gInSc7	titul
imperátor	imperátor	k1gMnSc1	imperátor
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
Německý	německý	k2eAgMnSc1d1	německý
císař	císař	k1gMnSc1	císař
–	–	k?	–
titul	titul	k1gInSc4	titul
panovníků	panovník	k1gMnPc2	panovník
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Fridrich	Fridrich	k1gMnSc1	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Císař	Císař	k1gMnSc1	Císař
Francouzů	Francouz	k1gMnPc2	Francouz
–	–	k?	–
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
používal	používat	k5eAaImAgMnS	používat
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
Napoleon	napoleon	k1gInSc4	napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Bulharský	bulharský	k2eAgMnSc1d1	bulharský
car	car	k1gMnSc1	car
–	–	k?	–
titul	titul	k1gInSc1	titul
rovný	rovný	k2eAgMnSc1d1	rovný
císaři	císař	k1gMnSc3	císař
používaný	používaný	k2eAgInSc4d1	používaný
za	za	k7c2	za
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
bulharské	bulharský	k2eAgFnSc2d1	bulharská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
i	i	k9	i
za	za	k7c2	za
třetí	třetí	k4xOgFnSc2	třetí
bulharské	bulharský	k2eAgFnSc2d1	bulharská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
panovník	panovník	k1gMnSc1	panovník
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznáván	uznáván	k2eAgMnSc1d1	uznáván
jako	jako	k8xC	jako
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Srbský	srbský	k2eAgMnSc1d1	srbský
car	car	k1gMnSc1	car
–	–	k?	–
za	za	k7c4	za
srbského	srbský	k2eAgMnSc4d1	srbský
císaře	císař	k1gMnSc4	císař
(	(	kIx(	(
<g/>
cara	car	k1gMnSc4	car
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
Štěpán	Štěpán	k1gMnSc1	Štěpán
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
pak	pak	k6eAd1	pak
používal	používat	k5eAaImAgInS	používat
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Štěpán	Štěpán	k1gMnSc1	Štěpán
Uroš	Uroš	k1gMnSc1	Uroš
V.	V.	kA	V.
Císař	Císař	k1gMnSc1	Císař
celého	celý	k2eAgNnSc2d1	celé
Španělska	Španělsko	k1gNnSc2	Španělsko
–	–	k?	–
titul	titul	k1gInSc4	titul
používaný	používaný	k2eAgInSc4d1	používaný
u	u	k7c2	u
různých	různý	k2eAgFnPc6d1	různá
příležitostech	příležitost	k1gFnPc6	příležitost
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Imperator	Imperator	k1gMnSc1	Imperator
totius	totius	k1gMnSc1	totius
Hispaniae	Hispaniae	k1gInSc1	Hispaniae
<g/>
)	)	kIx)	)
Trapezuntský	trapezuntský	k2eAgMnSc1d1	trapezuntský
císař	císař	k1gMnSc1	císař
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
panovník	panovník	k1gMnSc1	panovník
Trapezuntského	trapezuntský	k2eAgNnSc2d1	Trapezuntské
císařství	císařství	k1gNnSc2	císařství
(	(	kIx(	(
<g/>
nástupnického	nástupnický	k2eAgInSc2d1	nástupnický
státu	stát	k1gInSc2	stát
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
císařský	císařský	k2eAgInSc1d1	císařský
titul	titul	k1gInSc1	titul
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1204	[number]	k4	1204
<g/>
–	–	k?	–
<g/>
1461	[number]	k4	1461
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
zval	zvát	k5eAaImAgMnS	zvát
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
samovládce	samovládce	k1gMnSc1	samovládce
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
po	po	k7c6	po
r.	r.	kA	r.
1282	[number]	k4	1282
se	se	k3xPyFc4	se
zval	zvát	k5eAaImAgMnS	zvát
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
samovládce	samovládce	k1gMnSc1	samovládce
celého	celý	k2eAgInSc2d1	celý
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
Iberů	Iber	k1gMnPc2	Iber
a	a	k8xC	a
zámořských	zámořský	k2eAgFnPc2d1	zámořská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1	čínský
císař	císař	k1gMnSc1	císař
–	–	k?	–
titul	titul	k1gInSc1	titul
čínského	čínský	k2eAgMnSc2d1	čínský
panovníka	panovník	k1gMnSc2	panovník
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2100	[number]	k4	2100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
do	do	k7c2	do
r.	r.	kA	r.
1911	[number]	k4	1911
Japonský	japonský	k2eAgMnSc1d1	japonský
císař	císař	k1gMnSc1	císař
–	–	k?	–
panovníci	panovník	k1gMnPc1	panovník
nejdéle	dlouho	k6eAd3	dlouho
nepřetržitě	přetržitě	k6eNd1	přetržitě
panující	panující	k2eAgFnSc2d1	panující
dynastie	dynastie	k1gFnSc2	dynastie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
660	[number]	k4	660
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Indický	indický	k2eAgMnSc1d1	indický
císař	císař	k1gMnSc1	císař
–	–	k?	–
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
titulů	titul	k1gInPc2	titul
panovníků	panovník	k1gMnPc2	panovník
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Korejský	korejský	k2eAgMnSc1d1	korejský
císař	císař	k1gMnSc1	císař
–	–	k?	–
panovník	panovník	k1gMnSc1	panovník
Korejského	korejský	k2eAgNnSc2d1	korejské
císařství	císařství	k1gNnSc2	císařství
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
Vietnamský	vietnamský	k2eAgMnSc1d1	vietnamský
císař	císař	k1gMnSc1	císař
–	–	k?	–
panovník	panovník	k1gMnSc1	panovník
Vietnamského	vietnamský	k2eAgNnSc2d1	vietnamské
císařství	císařství	k1gNnSc2	císařství
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Dynastie	dynastie	k1gFnSc2	dynastie
Nguyễ	Nguyễ	k1gFnSc2	Nguyễ
<g/>
.	.	kIx.	.
</s>
<s>
Brazilský	brazilský	k2eAgMnSc1d1	brazilský
císař	císař	k1gMnSc1	císař
–	–	k?	–
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
užíval	užívat	k5eAaImAgInS	užívat
Pedro	Pedra	k1gFnSc5	Pedra
I.	I.	kA	I.
Brazilský	brazilský	k2eAgMnSc1d1	brazilský
a	a	k8xC	a
Pedro	Pedro	k1gNnSc1	Pedro
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Brazilský	brazilský	k2eAgInSc1d1	brazilský
<g/>
.	.	kIx.	.
</s>
<s>
Mexický	mexický	k2eAgMnSc1d1	mexický
císař	císař	k1gMnSc1	císař
–	–	k?	–
panovníci	panovník	k1gMnPc1	panovník
Mexika	Mexiko	k1gNnSc2	Mexiko
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
krátkých	krátký	k2eAgNnPc6d1	krátké
obdobích	období	k1gNnPc6	období
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
císařem	císař	k1gMnSc7	císař
Mexika	Mexiko	k1gNnSc2	Mexiko
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1822	[number]	k4	1822
<g/>
–	–	k?	–
<g/>
1823	[number]	k4	1823
Agustín	Agustín	k1gInSc1	Agustín
I.	I.	kA	I.
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
císařem	císař	k1gMnSc7	císař
pak	pak	k6eAd1	pak
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
v	v	k7c6	v
letech	let	k1gInPc6	let
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Haitský	haitský	k2eAgMnSc1d1	haitský
císař	císař	k1gMnSc1	císař
–	–	k?	–
titul	titul	k1gInSc4	titul
používali	používat	k5eAaImAgMnP	používat
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Dessalines	Dessalines	k1gMnSc1	Dessalines
a	a	k8xC	a
Faustin-Élie	Faustin-Élie	k1gFnSc1	Faustin-Élie
Soulouque	Soulouque	k1gFnSc1	Soulouque
<g/>
.	.	kIx.	.
</s>
<s>
Habešský	habešský	k2eAgMnSc1d1	habešský
císař	císař	k1gMnSc1	císař
–	–	k?	–
panovník	panovník	k1gMnSc1	panovník
dnešní	dnešní	k2eAgMnSc1d1	dnešní
Etiopie	Etiopie	k1gFnPc4	Etiopie
(	(	kIx(	(
<g/>
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Jean-Bédel	Jean-Bédlo	k1gNnPc2	Jean-Bédlo
Bokassa	Bokass	k1gMnSc2	Bokass
–	–	k?	–
jediný	jediný	k2eAgMnSc1d1	jediný
císař	císař	k1gMnSc1	císař
Středoafrického	středoafrický	k2eAgNnSc2d1	středoafrické
císařství	císařství	k1gNnSc2	císařství
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
</s>
