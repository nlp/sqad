<p>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Grover	Grover	k1gInSc1	Grover
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1837	[number]	k4	1837
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
až	až	k9	až
1889	[number]	k4	1889
a	a	k8xC	a
1893	[number]	k4	1893
až	až	k9	až
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
jehož	jehož	k3xOyRp3gFnSc2	jehož
funkční	funkční	k2eAgFnSc2d1	funkční
období	období	k1gNnSc4	období
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenavazovala	navazovat	k5eNaImAgNnP	navazovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sendvičové	sendvičový	k2eAgNnSc1d1	sendvičové
prezidentství	prezidentství	k1gNnSc1	prezidentství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
demokratem	demokrat	k1gMnSc7	demokrat
zastávajícím	zastávající	k2eAgFnPc3d1	zastávající
úřad	úřad	k1gInSc1	úřad
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
manželka	manželka	k1gFnSc1	manželka
Frances	Francesa	k1gFnPc2	Francesa
Clevelandová	Clevelandový	k2eAgFnSc1d1	Clevelandová
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
21	[number]	k4	21
letech	léto	k1gNnPc6	léto
stala	stát	k5eAaPmAgFnS	stát
nejmladší	mladý	k2eAgInSc4d3	nejmladší
první	první	k4xOgFnSc7	první
dámou	dáma	k1gFnSc7	dáma
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
slavnostně	slavnostně	k6eAd1	slavnostně
odhalil	odhalit	k5eAaPmAgMnS	odhalit
Sochu	socha	k1gFnSc4	socha
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
pronesl	pronést	k5eAaPmAgMnS	pronést
při	při	k7c6	při
onom	onen	k3xDgInSc6	onen
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nezapomeneme	zapomnět	k5eNaImIp1nP	zapomnět
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
Svoboda	svoboda	k1gFnSc1	svoboda
našla	najít	k5eAaPmAgFnS	najít
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
oltář	oltář	k1gInSc1	oltář
nebude	být	k5eNaImBp3nS	být
zanedbáván	zanedbávat	k5eAaImNgInS	zanedbávat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlády	vláda	k1gFnSc2	vláda
Grovera	Grover	k1gMnSc4	Grover
Clevelenda	Clevelend	k1gMnSc4	Clevelend
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Grover	Grovra	k1gFnPc2	Grovra
Cleveland	Clevelando	k1gNnPc2	Clevelando
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Grover	Grovero	k1gNnPc2	Grovero
Cleveland	Clevelando	k1gNnPc2	Clevelando
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Biografie	biografie	k1gFnSc1	biografie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
</s>
</p>
