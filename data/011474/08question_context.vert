<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Grover	Grover	k1gInSc1	Grover
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1837	[number]	k4	1837
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
až	až	k9	až
1889	[number]	k4	1889
a	a	k8xC	a
1893	[number]	k4	1893
až	až	k9	až
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
jehož	jehož	k3xOyRp3gFnSc2	jehož
funkční	funkční	k2eAgFnSc2d1	funkční
období	období	k1gNnSc4	období
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenavazovala	navazovat	k5eNaImAgNnP	navazovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sendvičové	sendvičový	k2eAgNnSc1d1	sendvičové
prezidentství	prezidentství	k1gNnSc1	prezidentství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
demokratem	demokrat	k1gMnSc7	demokrat
zastávajícím	zastávající	k2eAgFnPc3d1	zastávající
úřad	úřad	k1gInSc1	úřad
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
