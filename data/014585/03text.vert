<s>
Přídavné	přídavný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
jsou	být	k5eAaImIp3nP
vlastnosti	vlastnost	k1gFnPc1
nebo	nebo	k8xC
vztahy	vztah	k1gInPc1
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
rozlišují	rozlišovat	k5eAaImIp3nP
rody	rod	k1gInPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
přídavné	přídavný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
zpravidla	zpravidla	k6eAd1
shoduje	shodovat	k5eAaImIp3nS
s	s	k7c7
rodem	rod	k1gInSc7
podstatného	podstatný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
rozvíjí	rozvíjet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
větě	věta	k1gFnSc6
mají	mít	k5eAaImIp3nP
nejčastěji	často	k6eAd3
funkci	funkce	k1gFnSc4
přívlastku	přívlastek	k1gInSc2
shodného	shodný	k2eAgInSc2d1
nebo	nebo	k8xC
doplňku	doplněk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stupňování	stupňování	k1gNnSc1
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Stupňování	stupňování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
lze	lze	k6eAd1
stupňovat	stupňovat	k5eAaImF
ve	v	k7c6
třech	tři	k4xCgInPc6
stupních	stupeň	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
stupeň	stupeň	k1gInSc1
–	–	k?
pozitiv	pozitiv	k1gInSc1
–	–	k?
základní	základní	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
(	(	kIx(
<g/>
Pavel	Pavel	k1gMnSc1
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
stupeň	stupeň	k1gInSc1
–	–	k?
komparativ	komparativ	k1gInSc1
–	–	k?
porovnává	porovnávat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
subjekty	subjekt	k1gInPc4
(	(	kIx(
<g/>
Pavel	Pavel	k1gMnSc1
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgMnSc1d2
než	než	k8xS
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
stupeň	stupeň	k1gInSc1
–	–	k?
superlativ	superlativ	k1gInSc1
–	–	k?
vymezuje	vymezovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
subjekt	subjekt	k1gInSc4
z	z	k7c2
celku	celek	k1gInSc2
(	(	kIx(
<g/>
Pavel	Pavel	k1gMnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgMnSc1d3
ze	z	k7c2
třídy	třída	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
se	se	k3xPyFc4
stupňuje	stupňovat	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
<g/>
,	,	kIx,
některá	některý	k3yIgFnSc1
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
ta	ten	k3xDgFnSc1
nejběžněji	běžně	k6eAd3
užívaná	užívaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
nepravidelně	pravidelně	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Nepravidelné	pravidelný	k2eNgNnSc1d1
stupňování	stupňování	k1gNnSc1
přídavného	přídavný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
dobrý	dobrý	k2eAgInSc1d1
v	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
</s>
<s>
Čeština	čeština	k1gFnSc1
<g/>
:	:	kIx,
dobrý	dobrý	k2eAgInSc1d1
–	–	k?
lepší	dobrý	k2eAgInSc1d2
–	–	k?
nejlepší	dobrý	k2eAgInSc4d3
</s>
<s>
Angličtina	angličtina	k1gFnSc1
<g/>
:	:	kIx,
good	good	k1gInSc1
–	–	k?
better	better	k1gInSc1
–	–	k?
the	the	k?
best	best	k1gInSc1
</s>
<s>
Němčina	němčina	k1gFnSc1
<g/>
:	:	kIx,
gut	gut	k?
–	–	k?
besser	besser	k1gInSc1
–	–	k?
der	drát	k5eAaImRp2nS
<g/>
,	,	kIx,
die	die	k?
<g/>
,	,	kIx,
das	das	k?
beste	beste	k5eAaPmIp2nP
</s>
<s>
Latina	latina	k1gFnSc1
<g/>
:	:	kIx,
bonus	bonus	k1gInSc1
–	–	k?
melior	melior	k1gInSc1
–	–	k?
optimus	optimus	k1gInSc1
</s>
<s>
Španělština	španělština	k1gFnSc1
<g/>
:	:	kIx,
bueno	bueno	k1gNnSc1
–	–	k?
mejor	mejora	k1gFnPc2
–	–	k?
el	ela	k1gFnPc2
mejor	mejor	k1gMnSc1
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Česká	český	k2eAgNnPc1d1
přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
v	v	k7c6
češtině	čeština	k1gFnSc6
dělíme	dělit	k5eAaImIp1nP
na	na	k7c4
tvrdá	tvrdé	k1gNnPc4
(	(	kIx(
<g/>
skloňujeme	skloňovat	k5eAaImIp1nP
podle	podle	k7c2
vzoru	vzor	k1gInSc2
mladý	mladý	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měkká	měkký	k2eAgNnPc4d1
(	(	kIx(
<g/>
jarní	jarní	k2eAgNnPc4d1
<g/>
)	)	kIx)
a	a	k8xC
přivlastňovací	přivlastňovací	k2eAgInSc4d1
(	(	kIx(
<g/>
otcův	otcův	k2eAgInSc4d1
<g/>
,	,	kIx,
matčin	matčin	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgNnPc2
tvrdých	tvrdý	k2eAgNnPc2d1
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
můžeme	moct	k5eAaImIp1nP
tvořit	tvořit	k5eAaImF
i	i	k9
krátké	krátký	k2eAgInPc4d1
(	(	kIx(
<g/>
jmenné	jmenný	k2eAgInPc4d1
<g/>
)	)	kIx)
tvary	tvar	k1gInPc4
<g/>
:	:	kIx,
hladový	hladový	k2eAgInSc4d1
–	–	k?
hladov	hladov	k1gInSc4
<g/>
,	,	kIx,
šťastný	šťastný	k2eAgMnSc1d1
–	–	k?
šťasten	šťasten	k2eAgInSc1d1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
však	však	k9
užívají	užívat	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
výjimečně	výjimečně	k6eAd1
4	#num#	k4
<g/>
.	.	kIx.
pádě	pád	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
jsou	být	k5eAaImIp3nP
utvořena	utvořit	k5eAaPmNgNnP
ze	z	k7c2
sloves	sloveso	k1gNnPc2
(	(	kIx(
<g/>
běžet	běžet	k5eAaImF
–	–	k?
běžící	běžící	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
tvořit	tvořit	k5eAaImF
příslovce	příslovce	k1gNnSc4
(	(	kIx(
<g/>
chytrý	chytrý	k2eAgMnSc1d1
–	–	k?
chytře	chytro	k6eAd1
<g/>
)	)	kIx)
nebo	nebo	k8xC
podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
(	(	kIx(
<g/>
barevný	barevný	k2eAgInSc1d1
–	–	k?
barevnost	barevnost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
jsou	být	k5eAaImIp3nP
zpodstatnělá	zpodstatnělý	k2eAgNnPc1d1
(	(	kIx(
<g/>
ve	v	k7c6
větě	věta	k1gFnSc6
mají	mít	k5eAaImIp3nP
funkci	funkce	k1gFnSc4
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
)	)	kIx)
–	–	k?
např.	např.	kA
hajný	hajný	k1gMnSc1
<g/>
,	,	kIx,
průvodčí	průvodčí	k1gMnSc1
<g/>
,	,	kIx,
stříbrný	stříbrný	k2eAgMnSc1d1
<g/>
,	,	kIx,
bytná	bytná	k1gFnSc1
<g/>
,	,	kIx,
vstupné	vstupné	k1gNnSc1
<g/>
,	,	kIx,
hovězí	hovězí	k2eAgNnSc1d1
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
v	v	k7c6
latině	latina	k1gFnSc6
a	a	k8xC
románských	románský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
</s>
<s>
V	v	k7c6
latině	latina	k1gFnSc6
se	se	k3xPyFc4
adjektiva	adjektivum	k1gNnPc1
skloňují	skloňovat	k5eAaImIp3nP
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
substantiva	substantivum	k1gNnPc1
(	(	kIx(
<g/>
až	až	k9
na	na	k7c4
malé	malý	k2eAgFnPc4d1
odchylky	odchylka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
proto	proto	k8xC
může	moct	k5eAaImIp3nS
bez	bez	k7c2
větších	veliký	k2eAgInPc2d2
problémů	problém	k1gInPc2
docházet	docházet	k5eAaImF
k	k	k7c3
zpodstatňování	zpodstatňování	k1gNnSc3
adjektiv	adjektivum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srov.	srov.	kA
např.	např.	kA
latinský	latinský	k2eAgInSc4d1
superlativ	superlativ	k1gInSc4
amicissimus	amicissimus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
nejpřátelštější	přátelský	k2eAgFnSc1d3
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
již	již	k6eAd1
ustálené	ustálený	k2eAgNnSc1d1
substantivum	substantivum	k1gNnSc1
amicus	amicus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
přítel	přítel	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
kolísání	kolísání	k1gNnSc1
mezi	mezi	k7c7
adjektivy	adjektivum	k1gNnPc7
a	a	k8xC
substantivy	substantivum	k1gNnPc7
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
i	i	k9
v	v	k7c6
románských	románský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeden	jeden	k4xCgInSc1
výraz	výraz	k1gInSc1
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
jednou	jednou	k6eAd1
jako	jako	k8xC,k8xS
adjektivum	adjektivum	k1gNnSc4
<g/>
,	,	kIx,
podruhé	podruhé	k6eAd1
jako	jako	k9
substantivum	substantivum	k1gNnSc1
<g/>
;	;	kIx,
srov.	srov.	kA
italské	italský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
paziente	pazient	k1gInSc5
–	–	k?
„	„	k?
<g/>
trpělivý	trpělivý	k2eAgInSc4d1
<g/>
“	“	k?
(	(	kIx(
<g/>
adjektivum	adjektivum	k1gNnSc1
<g/>
)	)	kIx)
i	i	k9
„	„	k?
<g/>
pacient	pacient	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
substantivum	substantivum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týká	týkat	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
zvláště	zvláště	k6eAd1
původních	původní	k2eAgNnPc2d1
latinských	latinský	k2eAgNnPc2d1
participií	participium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Sémantická	sémantický	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
</s>
<s>
Sémantická	sémantický	k2eAgNnPc4d1
adjektiva	adjektivum	k1gNnPc4
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgMnPc4
kromě	kromě	k7c2
tradičních	tradiční	k2eAgNnPc2d1
adjektiv	adjektivum	k1gNnPc2
patří	patřit	k5eAaImIp3nS
též	též	k9
některé	některý	k3yIgNnSc1
příslovce	příslovce	k1gNnSc1
<g/>
,	,	kIx,
zájmena	zájmeno	k1gNnSc2
a	a	k8xC
číslovky	číslovka	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
z	z	k7c2
tektogramatického	tektogramatický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pojmenovací	pojmenovací	k2eAgNnPc1d1
sémantická	sémantický	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
–	–	k?
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
patří	patřit	k5eAaImIp3nP
všechna	všechen	k3xTgNnPc1
tradiční	tradiční	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
přivlastňovacích	přivlastňovací	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
tradiční	tradiční	k2eAgNnSc1d1
příslovce	příslovce	k1gNnSc1
deadjektivní	deadjektivní	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
„	„	k?
<g/>
hezky	hezky	k6eAd1
<g/>
“	“	k?
odvozené	odvozený	k2eAgFnSc2d1
od	od	k7c2
„	„	k?
<g/>
hezký	hezký	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
chladno	chladno	k1gNnSc4
<g/>
“	“	k?
od	od	k7c2
„	„	k?
<g/>
chladný	chladný	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
určitá	určitý	k2eAgFnSc1d1
pronominální	pronominální	k2eAgFnSc1d1
sémantická	sémantický	k2eAgFnSc1d1
adjektiva	adjektivum	k1gNnSc2
ukazovací	ukazovací	k2eAgInSc4d1
–	–	k?
ukazovací	ukazovací	k2eAgNnSc1d1
a	a	k8xC
identifikační	identifikační	k2eAgNnSc1d1
zájmena	zájmeno	k1gNnPc1
v	v	k7c6
pozici	pozice	k1gFnSc6
syntaktického	syntaktický	k2eAgNnSc2d1
adjektiva	adjektivum	k1gNnSc2
(	(	kIx(
<g/>
ten	ten	k3xDgInSc1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
takový	takový	k3xDgInSc4
přístup	přístup	k1gInSc4
<g/>
,	,	kIx,
tentýž	týž	k3xTgInSc1
problém	problém	k1gInSc1
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
už	už	k6eAd1
je	být	k5eAaImIp3nS
takový	takový	k3xDgInSc4
<g/>
)	)	kIx)
</s>
<s>
neurčitá	určitý	k2eNgNnPc1d1
pronominální	pronominální	k2eAgNnPc1d1
sémantická	sémantický	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
–	–	k?
zájmena	zájmeno	k1gNnSc2
se	s	k7c7
sémantickým	sémantický	k2eAgInSc7d1
rysem	rys	k1gInSc7
neurčitosti	neurčitost	k1gFnSc2
a	a	k8xC
s	s	k7c7
adjektivní	adjektivní	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
<g/>
,	,	kIx,
t.	t.	k?
j.	j.	k?
zájmena	zájmeno	k1gNnSc2
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
a	a	k8xC
jaký	jaký	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnSc2
neurčité	určitý	k2eNgFnSc2d1
<g/>
,	,	kIx,
tázací	tázací	k2eAgFnSc2d1
<g/>
,	,	kIx,
záporné	záporný	k2eAgFnSc2d1
a	a	k8xC
totalizační	totalizační	k2eAgFnSc2d1
odvozeniny	odvozenina	k1gFnSc2
(	(	kIx(
<g/>
některý	některý	k3yIgMnSc1
<g/>
,	,	kIx,
nijaký	nijaký	k3yNgInSc4
<g/>
,	,	kIx,
každý	každý	k3xTgInSc4
<g/>
,	,	kIx,
málokterý	málokterý	k3yIgInSc4
<g/>
,	,	kIx,
jakýsi	jakýsi	k3yIgInSc4
<g/>
,	,	kIx,
žádný	žádný	k3yNgInSc4
<g/>
,	,	kIx,
veškerý	veškerý	k3xTgInSc4
<g/>
,	,	kIx,
kdejaký	kdejaký	k3yIgInSc1
<g/>
,	,	kIx,
kterýpak	kterýpak	k3yQgMnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
v	v	k7c6
pozici	pozice	k1gFnSc6
syntaktického	syntaktický	k2eAgNnSc2d1
adjektiva	adjektivum	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
určitá	určitý	k2eAgNnPc1d1
kvantifikační	kvantifikační	k2eAgNnPc1d1
sémantická	sémantický	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
–	–	k?
základní	základní	k2eAgFnPc4d1
řadové	řadový	k2eAgFnPc4d1
číslovky	číslovka	k1gFnPc4
v	v	k7c6
pozici	pozice	k1gFnSc6
syntaktického	syntaktický	k2eAgNnSc2d1
adjektiva	adjektivum	k1gNnSc2
<g/>
,	,	kIx,
určité	určitý	k2eAgFnSc2d1
číslovky	číslovka	k1gFnSc2
řadové	řadový	k2eAgNnSc1d1
(	(	kIx(
<g/>
druhý	druhý	k4xOgMnSc1
<g/>
,	,	kIx,
stý	stý	k4xOgMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
souborové	souborový	k2eAgNnSc4d1
(	(	kIx(
<g/>
dvoje	dvoje	k4xRgFnSc1
<g/>
,	,	kIx,
stery	stero	k1gNnPc7
<g/>
)	)	kIx)
a	a	k8xC
druhové	druhový	k2eAgNnSc1d1
(	(	kIx(
<g/>
dvojí	dvojí	k4xRgFnSc1
<g/>
,	,	kIx,
sterý	sterý	k4xRgMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odvozeniny	odvozenina	k1gFnSc2
základních	základní	k2eAgFnPc2d1
číslovek	číslovka	k1gFnPc2
<g/>
,	,	kIx,
příslovce	příslovce	k1gNnSc1
typu	typ	k1gInSc2
dvakrát	dvakrát	k6eAd1
nebo	nebo	k8xC
podruhé	podruhé	k6eAd1
<g/>
,	,	kIx,
číslovka	číslovka	k1gFnSc1
tolik	tolik	k6eAd1
a	a	k8xC
její	její	k3xOp3gInPc1
deriváty	derivát	k1gInPc1
(	(	kIx(
<g/>
tolikátý	tolikátý	k4xOgInSc1
<g/>
,	,	kIx,
tolikery	toliker	k1gMnPc7
<g/>
,	,	kIx,
tolikerý	tolikerý	k4xRgMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příslovce	příslovce	k1gNnSc1
tolikrát	tolikrát	k6eAd1
a	a	k8xC
potolikáté	potolikátý	k2eAgFnPc1d1
</s>
<s>
neurčitá	určitý	k2eNgNnPc1d1
kvantifikační	kvantifikační	k2eAgNnPc1d1
sémantická	sémantický	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
–	–	k?
základní	základní	k2eAgFnSc1d1
neurčitá	určitý	k2eNgFnSc1d1
číslovka	číslovka	k1gFnSc1
kolik	kolika	k1gFnPc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc2
neurčité	určitý	k2eNgFnSc2d1
<g/>
,	,	kIx,
tázací	tázací	k2eAgFnSc2d1
<g/>
,	,	kIx,
řadové	řadový	k2eAgFnSc2d1
<g/>
,	,	kIx,
souborové	souborový	k2eAgFnSc2d1
a	a	k8xC
druhové	druhový	k2eAgFnSc2d1
odvozeniny	odvozenina	k1gFnSc2
(	(	kIx(
<g/>
několik	několik	k4yIc4
<g/>
,	,	kIx,
kolikátý	kolikátý	k4xOyRgInSc1,k4xOyQgInSc1,k4xOyIgInSc1
<g/>
,	,	kIx,
kolikery	kolikero	k4xRyIgFnPc4
<g/>
…	…	k?
<g/>
)	)	kIx)
a	a	k8xC
adverbiální	adverbiální	k2eAgFnPc4d1
odvozeniny	odvozenina	k1gFnPc4
(	(	kIx(
<g/>
kolikrát	kolikrát	k6eAd1
<g/>
,	,	kIx,
poněkolikáté	poněkolikáté	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
kvantifikační	kvantifikační	k2eAgNnPc1d1
sémantická	sémantický	k2eAgNnPc1d1
adjektiva	adjektivum	k1gNnPc1
stupňovatelná	stupňovatelný	k2eAgNnPc1d1
–	–	k?
základní	základní	k2eAgFnPc4d1
neurčité	určitý	k2eNgFnPc4d1
číslovky	číslovka	k1gFnPc4
v	v	k7c6
pozici	pozice	k1gFnSc6
syntaktického	syntaktický	k2eAgNnSc2d1
adjektiva	adjektivum	k1gNnSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
<g/>
-li	-li	k?
je	být	k5eAaImIp3nS
stupňovat	stupňovat	k5eAaImF
(	(	kIx(
<g/>
málo	málo	k1gNnSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
chyb	chyba	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
příslovce	příslovce	k1gNnSc1
typu	typ	k1gInSc2
málokrát	málokrát	k6eAd1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kapitola	kapitola	k1gFnSc1
4.6	4.6	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podskupiny	podskupina	k1gFnSc2
sémantických	sémantický	k2eAgInPc2d1
slovních	slovní	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
jim	on	k3xPp3gInPc3
přináležející	přináležející	k2eAgInSc1d1
gramatémy	gramatém	k1gInPc1
4.6	4.6	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sémantická	sémantický	k2eAgNnPc4d1
adjektiva	adjektivum	k1gNnPc4
<g/>
,	,	kIx,
Anotace	anotace	k1gFnPc4
na	na	k7c6
tektogramatické	tektogramatický	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
Pražského	pražský	k2eAgInSc2d1
závislostního	závislostní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
–	–	k?
Anotátorská	Anotátorský	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Mikulová	Mikulová	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
skloňování	skloňování	k1gNnSc1
</s>
<s>
Příslovce	příslovce	k1gNnSc1
</s>
<s>
Přívlastek	přívlastek	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
přídavné	přídavný	k2eAgFnSc2d1
jméno	jméno	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Adjektiva	adjektivum	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Adjektivum	adjektivum	k1gNnSc4
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4000497-1	4000497-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6853	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85000888	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85000888	#num#	k4
</s>
