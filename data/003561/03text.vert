<s>
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
μ	μ	k?	μ
či	či	k8xC	či
uP	uP	k?	uP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
centrální	centrální	k2eAgFnSc4d1	centrální
procesorovou	procesorový	k2eAgFnSc4d1	procesorová
jednotku	jednotka	k1gFnSc4	jednotka
(	(	kIx(	(
<g/>
CPU	CPU	kA	CPU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
celek	celek	k1gInSc1	celek
integrována	integrovat	k5eAaBmNgFnS	integrovat
do	do	k7c2	do
pouzdra	pouzdro	k1gNnSc2	pouzdro
jediného	jediný	k2eAgInSc2d1	jediný
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
nebo	nebo	k8xC	nebo
nejvýše	nejvýše	k6eAd1	nejvýše
několika	několik	k4yIc7	několik
mála	málo	k4c2	málo
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
je	být	k5eAaImIp3nS	být
víceúčelové	víceúčelový	k2eAgNnSc4d1	víceúčelové
programovatelné	programovatelný	k2eAgNnSc4d1	programovatelné
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
digitální	digitální	k2eAgNnPc4d1	digitální
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
zpracuje	zpracovat	k5eAaPmIp3nS	zpracovat
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
instrukcí	instrukce	k1gFnPc2	instrukce
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
a	a	k8xC	a
jako	jako	k9	jako
výstup	výstup	k1gInSc4	výstup
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
výsledek	výsledek	k1gInSc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
představuje	představovat	k5eAaImIp3nS	představovat
příklad	příklad	k1gInSc4	příklad
sekvenčního	sekvenční	k2eAgInSc2d1	sekvenční
logického	logický	k2eAgInSc2d1	logický
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
dat	datum	k1gNnPc2	datum
používá	používat	k5eAaImIp3nS	používat
dvojkovou	dvojkový	k2eAgFnSc4d1	dvojková
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
bývá	bývat	k5eAaImIp3nS	bývat
součástí	součást	k1gFnSc7	součást
mnoha	mnoho	k4c2	mnoho
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
softwarem	software	k1gInSc7	software
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jednoduše	jednoduše	k6eAd1	jednoduše
realizovat	realizovat	k5eAaBmF	realizovat
i	i	k9	i
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgInPc4d1	složitý
požadavky	požadavek	k1gInPc4	požadavek
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neustále	neustále	k6eAd1	neustále
klesající	klesající	k2eAgFnSc3d1	klesající
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
rostoucím	rostoucí	k2eAgFnPc3d1	rostoucí
možnostem	možnost	k1gFnPc3	možnost
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
složitějším	složitý	k2eAgNnSc6d2	složitější
elektronickém	elektronický	k2eAgNnSc6d1	elektronické
zařízení	zařízení	k1gNnSc6	zařízení
(	(	kIx(	(
<g/>
rádia	rádio	k1gNnSc2	rádio
<g/>
,	,	kIx,	,
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
tiskárny	tiskárna	k1gFnPc1	tiskárna
<g/>
,	,	kIx,	,
pračky	pračka	k1gFnPc1	pračka
<g/>
,	,	kIx,	,
chladničky	chladnička	k1gFnPc1	chladnička
<g/>
,	,	kIx,	,
televizory	televizor	k1gInPc1	televizor
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CPU	CPU	kA	CPU
(	(	kIx(	(
<g/>
Central	Central	k1gMnSc2	Central
Processing	Processing	k1gInSc1	Processing
Unit	Unit	k2eAgInSc1d1	Unit
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlavní	hlavní	k2eAgNnSc4d1	hlavní
(	(	kIx(	(
<g/>
mikro	mikro	k1gNnSc4	mikro
<g/>
)	)	kIx)	)
<g/>
procesor	procesor	k1gInSc1	procesor
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
však	však	k9	však
CPU	cpát	k5eAaImIp1nS	cpát
neměl	mít	k5eNaImAgMnS	mít
vždy	vždy	k6eAd1	vždy
podobu	podoba	k1gFnSc4	podoba
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
<g/>
)	)	kIx)	)
GPU	GPU	kA	GPU
(	(	kIx(	(
<g/>
grafický	grafický	k2eAgInSc1d1	grafický
procesor	procesor	k1gInSc1	procesor
<g/>
)	)	kIx)	)
–	–	k?	–
hlavní	hlavní	k2eAgInSc4d1	hlavní
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
grafické	grafický	k2eAgFnSc2d1	grafická
karty	karta	k1gFnSc2	karta
APU	APU	kA	APU
(	(	kIx(	(
<g/>
Accelerated	Accelerated	k1gInSc1	Accelerated
Processing	Processing	k1gInSc1	Processing
Unit	Unita	k1gFnPc2	Unita
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
CPU	CPU	kA	CPU
a	a	k8xC	a
GPU	GPU	kA	GPU
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
pouzdře	pouzdro	k1gNnSc6	pouzdro
matematický	matematický	k2eAgInSc1d1	matematický
procesor	procesor	k1gInSc1	procesor
(	(	kIx(	(
<g/>
FPU	FPU	kA	FPU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k8xS	jako
matematický	matematický	k2eAgInSc1d1	matematický
koprocesor	koprocesor	k1gInSc1	koprocesor
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
s	s	k7c7	s
CPU	CPU	kA	CPU
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
pouzdře	pouzdro	k1gNnSc6	pouzdro
<g/>
.	.	kIx.	.
zvukový	zvukový	k2eAgInSc1d1	zvukový
procesor	procesor	k1gInSc1	procesor
signálový	signálový	k2eAgInSc1d1	signálový
procesor	procesor	k1gInSc1	procesor
(	(	kIx(	(
<g/>
DSP	DSP	kA	DSP
procesor	procesor	k1gInSc1	procesor
<g/>
)	)	kIx)	)
jiné	jiný	k2eAgInPc1d1	jiný
specializované	specializovaný	k2eAgInPc1d1	specializovaný
procesory	procesor	k1gInPc1	procesor
Před	před	k7c7	před
vynálezem	vynález	k1gInSc7	vynález
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
byly	být	k5eAaImAgFnP	být
elektronické	elektronický	k2eAgFnPc1d1	elektronická
CPU	CPU	kA	CPU
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
z	z	k7c2	z
oddělených	oddělený	k2eAgInPc2d1	oddělený
TTL	TTL	kA	TTL
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
<g/>
;	;	kIx,	;
předtím	předtím	k6eAd1	předtím
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
;	;	kIx,	;
a	a	k8xC	a
ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
elektronkách	elektronka	k1gFnPc6	elektronka
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
návrhy	návrh	k1gInPc4	návrh
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
počítacích	počítací	k2eAgInPc2d1	počítací
strojů	stroj	k1gInPc2	stroj
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
mechanických	mechanický	k2eAgFnPc6d1	mechanická
součástkách	součástka	k1gFnPc6	součástka
jako	jako	k8xS	jako
ozubené	ozubený	k2eAgNnSc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
hřídele	hřídel	k1gFnSc2	hřídel
<g/>
,	,	kIx,	,
páky	páka	k1gFnSc2	páka
atd.	atd.	kA	atd.
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
bohužel	bohužel	k6eAd1	bohužel
ho	on	k3xPp3gMnSc4	on
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
sestavit	sestavit	k5eAaPmF	sestavit
výrobními	výrobní	k2eAgInPc7d1	výrobní
postupy	postup	k1gInPc7	postup
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
dosud	dosud	k6eAd1	dosud
sleduje	sledovat	k5eAaImIp3nS	sledovat
Mooreův	Mooreův	k2eAgInSc4d1	Mooreův
zákon	zákon	k1gInSc4	zákon
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
stálého	stálý	k2eAgInSc2d1	stálý
zvyšovaní	zvyšovaný	k2eAgMnPc1d1	zvyšovaný
výkonu	výkon	k1gInSc3	výkon
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
nám	my	k3xPp1nPc3	my
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
komplexnost	komplexnost	k1gFnSc4	komplexnost
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
minimální	minimální	k2eAgFnSc4d1	minimální
cenu	cena	k1gFnSc4	cena
komponent	komponenta	k1gFnPc2	komponenta
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS	zdvojnásobit
každých	každý	k3xTgFnPc2	každý
přibližně	přibližně	k6eAd1	přibližně
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
ve	v	k7c6	v
všeobecnosti	všeobecnost	k1gFnSc6	všeobecnost
i	i	k9	i
překvapivě	překvapivě	k6eAd1	překvapivě
dělo	dít	k5eAaImAgNnS	dít
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
samotných	samotný	k2eAgInPc2d1	samotný
začátků	začátek	k1gInPc2	začátek
jako	jako	k8xC	jako
mozek	mozek	k1gInSc4	mozek
kalkulaček	kalkulačka	k1gFnPc2	kalkulačka
viděl	vidět	k5eAaImAgMnS	vidět
zvyšující	zvyšující	k2eAgMnSc1d1	zvyšující
se	se	k3xPyFc4	se
výkon	výkon	k1gInSc1	výkon
k	k	k7c3	k
dominanci	dominance	k1gFnSc3	dominance
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
jiné	jiný	k2eAgFnSc6d1	jiná
formě	forma	k1gFnSc6	forma
počítače	počítač	k1gInSc2	počítač
<g/>
;	;	kIx,	;
každý	každý	k3xTgInSc1	každý
systém	systém	k1gInSc1	systém
od	od	k7c2	od
největšího	veliký	k2eAgMnSc2d3	veliký
mainframe	mainframe	k1gInSc1	mainframe
(	(	kIx(	(
<g/>
sálový	sálový	k2eAgInSc1d1	sálový
počítač	počítač	k1gInSc1	počítač
<g/>
)	)	kIx)	)
po	po	k7c4	po
nejmenší	malý	k2eAgInSc4d3	nejmenší
handheld	handheld	k1gInSc4	handheld
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
používá	používat	k5eAaImIp3nS	používat
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc2	historie
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
výroby	výroba	k1gFnSc2	výroba
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
pokročila	pokročit	k5eAaPmAgFnS	pokročit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vyrobit	vyrobit	k5eAaPmF	vyrobit
procesor	procesor	k1gInSc4	procesor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
jedním	jeden	k4xCgInSc7	jeden
integrovaným	integrovaný	k2eAgInSc7d1	integrovaný
obvodem	obvod	k1gInSc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc4	výsledek
tří	tři	k4xCgInPc2	tři
nezávislých	závislý	k2eNgInPc2d1	nezávislý
projektů	projekt	k1gInPc2	projekt
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
stejném	stejný	k2eAgInSc6d1	stejný
čase	čas	k1gInSc6	čas
<g/>
:	:	kIx,	:
Intel	Intel	kA	Intel
4004	[number]	k4	4004
<g/>
,	,	kIx,	,
Texas	Texas	kA	Texas
Instruments	Instruments	kA	Instruments
TMS1000	TMS1000	k1gMnSc1	TMS1000
a	a	k8xC	a
Garrett	Garrett	k2eAgMnSc1d1	Garrett
AiResearch	AiResearch	k1gMnSc1	AiResearch
Central	Central	k1gFnSc2	Central
Air	Air	k1gMnSc1	Air
Data	datum	k1gNnSc2	datum
Computer	computer	k1gInSc1	computer
MP	MP	kA	MP
<g/>
944	[number]	k4	944
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
společnosti	společnost	k1gFnSc2	společnost
Garrett	Garrett	k1gInSc4	Garrett
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
počítač	počítač	k1gInSc1	počítač
schopný	schopný	k2eAgInSc1d1	schopný
soupeřit	soupeřit	k5eAaImF	soupeřit
s	s	k7c7	s
elektromechanickými	elektromechanický	k2eAgInPc7d1	elektromechanický
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
tehdy	tehdy	k6eAd1	tehdy
vyvíjeny	vyvíjet	k5eAaImNgInP	vyvíjet
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
letové	letový	k2eAgFnSc2d1	letová
kontroly	kontrola	k1gFnSc2	kontrola
nové	nový	k2eAgFnSc2d1	nová
stíhačky	stíhačka	k1gFnSc2	stíhačka
F-14	F-14	k1gMnSc2	F-14
Tomcat	Tomcat	k1gMnSc2	Tomcat
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
hotový	hotový	k2eAgInSc1d1	hotový
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
jako	jako	k9	jako
jádro	jádro	k1gNnSc4	jádro
používal	používat	k5eAaImAgMnS	používat
čipovou	čipový	k2eAgFnSc4d1	čipová
sadu	sada	k1gFnSc4	sada
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
MOS	MOS	kA	MOS
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
menší	malý	k2eAgMnSc1d2	menší
a	a	k8xC	a
spolehlivější	spolehlivý	k2eAgMnSc1d2	spolehlivější
než	než	k8xS	než
mechanické	mechanický	k2eAgInPc1d1	mechanický
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgMnPc7	který
soupeřil	soupeřit	k5eAaImAgMnS	soupeřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
raných	raný	k2eAgInPc6d1	raný
modelech	model	k1gInPc6	model
letadel	letadlo	k1gNnPc2	letadlo
Tomcat	Tomcat	k1gMnPc3	Tomcat
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
však	však	k9	však
považovalo	považovat	k5eAaImAgNnS	považovat
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
za	za	k7c4	za
tak	tak	k9	tak
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
tajný	tajný	k2eAgInSc4d1	tajný
<g/>
,	,	kIx,	,
a	a	k8xC	a
utajování	utajování	k1gNnSc1	utajování
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
CADC	CADC	kA	CADC
a	a	k8xC	a
čipová	čipový	k2eAgFnSc1d1	čipová
sada	sada	k1gFnSc1	sada
MP944	MP944	k1gMnPc2	MP944
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
neznámou	známý	k2eNgFnSc4d1	neznámá
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Texas	Texas	k1gInSc1	Texas
Instruments	Instruments	kA	Instruments
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
4	[number]	k4	4
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
čip	čip	k1gInSc1	čip
Texas	Texas	k1gInSc1	Texas
Instruments	Instruments	kA	Instruments
TMS	TMS	kA	TMS
<g/>
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
testovala	testovat	k5eAaImAgFnS	testovat
hranice	hranice	k1gFnSc1	hranice
předprogramovaných	předprogramovaný	k2eAgFnPc2d1	předprogramovaná
vložených	vložený	k2eAgFnPc2d1	vložená
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
1971	[number]	k4	1971
představila	představit	k5eAaPmAgFnS	představit
verzi	verze	k1gFnSc4	verze
zvanou	zvaný	k2eAgFnSc4d1	zvaná
TMS1802NC	TMS1802NC	k1gFnSc4	TMS1802NC
17	[number]	k4	17
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
implementovala	implementovat	k5eAaImAgFnS	implementovat
kalkulačku	kalkulačka	k1gFnSc4	kalkulačka
na	na	k7c6	na
čipu	čip	k1gInSc6	čip
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Intel	Intel	kA	Intel
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
Federico	Federico	k1gMnSc1	Federico
Faggin	Faggin	k2eAgMnSc1d1	Faggin
4	[number]	k4	4
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
čip	čip	k1gInSc1	čip
Intel	Intel	kA	Intel
4004	[number]	k4	4004
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Texas	Texas	k1gInSc1	Texas
Instruments	Instruments	kA	Instruments
se	se	k3xPyFc4	se
ucházela	ucházet	k5eAaImAgFnS	ucházet
o	o	k7c4	o
patent	patent	k1gInSc4	patent
na	na	k7c4	na
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
a	a	k8xC	a
Gary	Gary	k1gInPc4	Gary
Boone	Boon	k1gInSc5	Boon
dostal	dostat	k5eAaPmAgMnS	dostat
patent	patent	k1gInSc4	patent
v	v	k7c6	v
USA	USA	kA	USA
3,757,306	[number]	k4	3,757,306
za	za	k7c4	za
architekturu	architektura	k1gFnSc4	architektura
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
čipu	čip	k1gInSc6	čip
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nikdy	nikdy	k6eAd1	nikdy
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
společnost	společnost	k1gFnSc1	společnost
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
první	první	k4xOgInSc4	první
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
pracující	pracující	k2eAgInSc4d1	pracující
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
a	a	k8xC	a
1976	[number]	k4	1976
firmy	firma	k1gFnSc2	firma
Intel	Intel	kA	Intel
a	a	k8xC	a
Texas	Texas	kA	Texas
Instruments	Instruments	kA	Instruments
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
mnoho	mnoho	k4c4	mnoho
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
licenčních	licenční	k2eAgFnPc2d1	licenční
patentových	patentový	k2eAgFnPc2d1	patentová
dohod	dohoda	k1gFnPc2	dohoda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgFnPc2	který
firma	firma	k1gFnSc1	firma
Intel	Intel	kA	Intel
platila	platit	k5eAaImAgFnS	platit
firmě	firma	k1gFnSc3	firma
Texas	Texas	k1gInSc1	Texas
Instruments	Instruments	kA	Instruments
za	za	k7c4	za
patent	patent	k1gInSc4	patent
na	na	k7c4	na
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Jednočipový	jednočipový	k2eAgInSc1d1	jednočipový
počítač	počítač	k1gInSc1	počítač
je	být	k5eAaImIp3nS	být
variantou	varianta	k1gFnSc7	varianta
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
jádro	jádro	k1gNnSc4	jádro
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
(	(	kIx(	(
<g/>
CPU	CPU	kA	CPU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc4	paměť
a	a	k8xC	a
vstupně	vstupně	k6eAd1	vstupně
<g/>
/	/	kIx~	/
<g/>
výstupné	výstupný	k2eAgFnPc4d1	výstupná
linky	linka	k1gFnPc4	linka
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
čipu	čip	k1gInSc6	čip
<g/>
.	.	kIx.	.
</s>
<s>
Patent	patent	k1gInSc1	patent
USA	USA	kA	USA
4,074,351	[number]	k4	4,074,351
na	na	k7c4	na
počítač	počítač	k1gInSc4	počítač
na	na	k7c6	na
čipu	čip	k1gInSc6	čip
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
patent	patent	k1gInSc1	patent
na	na	k7c4	na
mikropočítač	mikropočítač	k1gInSc4	mikropočítač
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
Garymu	Garym	k1gInSc6	Garym
Booneovi	Booneův	k2eAgMnPc1d1	Booneův
a	a	k8xC	a
Michaelovi	Michaelův	k2eAgMnPc1d1	Michaelův
J.	J.	kA	J.
Cochranovi	Cochran	k1gMnSc3	Cochran
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Texas	Texas	k1gInSc1	Texas
Instruments	Instruments	kA	Instruments
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
tohoto	tento	k3xDgInSc2	tento
patentu	patent	k1gInSc2	patent
je	být	k5eAaImIp3nS	být
správný	správný	k2eAgInSc4d1	správný
význam	význam	k1gInSc4	význam
mikropočítače	mikropočítač	k1gInSc2	mikropočítač
počítač	počítač	k1gInSc1	počítač
používající	používající	k2eAgInSc1d1	používající
(	(	kIx(	(
<g/>
několik	několik	k4yIc1	několik
<g/>
)	)	kIx)	)
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
<g/>
(	(	kIx(	(
<g/>
ů	ů	k?	ů
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
svůj	svůj	k3xOyFgInSc4	svůj
CPU	cpát	k5eAaImIp1nS	cpát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
koncept	koncept	k1gInSc1	koncept
patentu	patent	k1gInSc2	patent
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
připomíná	připomínat	k5eAaImIp3nS	připomínat
mikrokontrolér	mikrokontrolér	k1gInSc1	mikrokontrolér
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Computer	computer	k1gInSc1	computer
Terminals	Terminals	k1gInSc1	Terminals
Corporation	Corporation	k1gInSc4	Corporation
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Datapoint	Datapointa	k1gFnPc2	Datapointa
<g/>
,	,	kIx,	,
ze	z	k7c2	z
San	San	k1gFnPc2	San
Antonio	Antonio	k1gMnSc1	Antonio
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
na	na	k7c4	na
čip	čip	k1gInSc4	čip
pro	pro	k7c4	pro
terminál	terminál	k1gInSc4	terminál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
<g/>
.	.	kIx.	.
</s>
<s>
Datapoint	Datapoint	k1gMnSc1	Datapoint
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
čip	čip	k1gInSc4	čip
nepoužít	použít	k5eNaPmF	použít
a	a	k8xC	a
Intel	Intel	kA	Intel
jej	on	k3xPp3gInSc4	on
prodával	prodávat	k5eAaImAgInS	prodávat
jako	jako	k9	jako
8008	[number]	k4	8008
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc6	první
8	[number]	k4	8
<g/>
bitovým	bitový	k2eAgInSc7d1	bitový
mikroprocesorem	mikroprocesor	k1gInSc7	mikroprocesor
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
základem	základ	k1gInSc7	základ
slavné	slavný	k2eAgFnSc2d1	slavná
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sady	sada	k1gFnSc2	sada
Mark-	Mark-	k1gFnSc2	Mark-
<g/>
8	[number]	k4	8
propagované	propagovaný	k2eAgFnSc2d1	propagovaná
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Radio-Electronics	Radio-Electronicsa	k1gFnPc2	Radio-Electronicsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
Intel	Intel	kA	Intel
8008	[number]	k4	8008
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
následovník	následovník	k1gMnSc1	následovník
Intel	Intel	kA	Intel
8080	[number]	k4	8080
otevřely	otevřít	k5eAaPmAgFnP	otevřít
trh	trh	k1gInSc4	trh
s	s	k7c7	s
mikroprocesorovými	mikroprocesorový	k2eAgFnPc7d1	mikroprocesorová
komponentami	komponenta	k1gFnPc7	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Intel	Intel	kA	Intel
4004	[number]	k4	4004
později	pozdě	k6eAd2	pozdě
následoval	následovat	k5eAaImAgInS	následovat
Intel	Intel	kA	Intel
8008	[number]	k4	8008
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
první	první	k4xOgInPc4	první
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
procesory	procesor	k1gInPc1	procesor
jsou	být	k5eAaImIp3nP	být
předchůdci	předchůdce	k1gMnSc3	předchůdce
tržně	tržně	k6eAd1	tržně
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
Intel	Intel	kA	Intel
8080	[number]	k4	8080
<g/>
,	,	kIx,	,
Zilog	Zilog	k1gInSc1	Zilog
Z80	Z80	k1gFnSc2	Z80
a	a	k8xC	a
odvozené	odvozený	k2eAgInPc1d1	odvozený
8	[number]	k4	8
<g/>
-bitové	itový	k2eAgInPc4d1	-bitový
procesory	procesor	k1gInPc4	procesor
Intel	Intel	kA	Intel
8	[number]	k4	8
<g/>
-bit	ita	k1gFnPc2	-bita
<g/>
.	.	kIx.	.
</s>
<s>
Konkurenční	konkurenční	k2eAgFnSc1d1	konkurenční
architektura	architektura	k1gFnSc1	architektura
Motorola	Motorola	kA	Motorola
6800	[number]	k4	6800
byla	být	k5eAaImAgFnS	být
klonovaná	klonovaný	k2eAgFnSc1d1	klonovaná
a	a	k8xC	a
zlepšená	zlepšený	k2eAgFnSc1d1	zlepšená
firmou	firma	k1gFnSc7	firma
MOS	MOS	kA	MOS
Technology	technolog	k1gMnPc4	technolog
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
MOS	MOS	kA	MOS
Technology	technolog	k1gMnPc7	technolog
6502	[number]	k4	6502
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
soupeřila	soupeřit	k5eAaImAgFnS	soupeřit
s	s	k7c7	s
popularitou	popularita	k1gFnSc7	popularita
Z80	Z80	k1gFnSc2	Z80
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z80	Z80	k4	Z80
i	i	k8xC	i
6502	[number]	k4	6502
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgFnP	soustředit
na	na	k7c4	na
nízkou	nízký	k2eAgFnSc4d1	nízká
celkovou	celkový	k2eAgFnSc4d1	celková
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
dosaženou	dosažený	k2eAgFnSc4d1	dosažená
díky	díky	k7c3	díky
kombinaci	kombinace	k1gFnSc3	kombinace
malého	malý	k2eAgNnSc2d1	malé
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
požadavky	požadavek	k1gInPc7	požadavek
na	na	k7c4	na
sběrnici	sběrnice	k1gFnSc4	sběrnice
a	a	k8xC	a
integrované	integrovaný	k2eAgInPc4d1	integrovaný
obvody	obvod	k1gInPc4	obvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
běžně	běžně	k6eAd1	běžně
byly	být	k5eAaImAgInP	být
poskytované	poskytovaný	k2eAgInPc1d1	poskytovaný
jako	jako	k8xS	jako
oddělený	oddělený	k2eAgInSc1d1	oddělený
čip	čip	k1gInSc1	čip
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Z80	Z80	k1gMnPc2	Z80
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
paměťový	paměťový	k2eAgInSc1d1	paměťový
kontrolér	kontrolér	k1gInSc1	kontrolér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožnily	umožnit	k5eAaPmAgFnP	umožnit
start	start	k1gInSc4	start
"	"	kIx"	"
<g/>
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
"	"	kIx"	"
domácích	domácí	k2eAgMnPc2d1	domácí
počítačů	počítač	k1gMnPc2	počítač
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nakonec	nakonec	k6eAd1	nakonec
produkovala	produkovat	k5eAaImAgFnS	produkovat
polo-užitečné	položitečný	k2eAgInPc4d1	polo-užitečný
stroje	stroj	k1gInPc4	stroj
za	za	k7c4	za
99	[number]	k4	99
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Motorola	Motorola	kA	Motorola
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
8	[number]	k4	8
<g/>
bitovém	bitový	k2eAgInSc6d1	bitový
světě	svět	k1gInSc6	svět
představením	představení	k1gNnSc7	představení
MC	MC	kA	MC
<g/>
6809	[number]	k4	6809
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
nejsilnějšího	silný	k2eAgInSc2d3	nejsilnější
ortogonálního	ortogonální	k2eAgInSc2d1	ortogonální
a	a	k8xC	a
čistě	čistě	k6eAd1	čistě
8	[number]	k4	8
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
mikroprocesorového	mikroprocesorový	k2eAgInSc2d1	mikroprocesorový
návrhu	návrh	k1gInSc2	návrh
<g/>
;	;	kIx,	;
a	a	k8xC	a
též	též	k9	též
nejkomplexnější	komplexní	k2eAgNnSc1d3	nejkomplexnější
napevno	napevno	k6eAd1	napevno
zadrátovanou	zadrátovaný	k2eAgFnSc7d1	zadrátovaná
logikou	logika	k1gFnSc7	logika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
pro	pro	k7c4	pro
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Mikrokódovaní	Mikrokódovaný	k2eAgMnPc1d1	Mikrokódovaný
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
napevno	napevno	k6eAd1	napevno
zadrátovanou	zadrátovaný	k2eAgFnSc4d1	zadrátovaná
logiku	logika	k1gFnSc4	logika
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
návrzích	návrh	k1gInPc6	návrh
silnějších	silný	k2eAgInPc6d2	silnější
než	než	k8xS	než
MC	MC	kA	MC
<g/>
6809	[number]	k4	6809
<g/>
,	,	kIx,	,
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
pro	pro	k7c4	pro
pevné	pevný	k2eAgNnSc4d1	pevné
drátování	drátování	k1gNnSc4	drátování
příliš	příliš	k6eAd1	příliš
komplexní	komplexní	k2eAgFnPc1d1	komplexní
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
8	[number]	k4	8
<g/>
-bitových	itův	k2eAgInPc2d1	-bitův
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
byl	být	k5eAaImAgInS	být
Signetics	Signetics	k1gInSc1	Signetics
2650	[number]	k4	2650
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
krátkému	krátký	k2eAgInSc3d1	krátký
návalu	nával	k1gInSc3	nával
zájmu	zájem	k1gInSc2	zájem
díky	díky	k7c3	díky
inovativní	inovativní	k2eAgFnSc3d1	inovativní
a	a	k8xC	a
obsáhlé	obsáhlý	k2eAgFnSc3d1	obsáhlá
architektuře	architektura	k1gFnSc3	architektura
instrukční	instrukční	k2eAgFnSc2d1	instrukční
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgInSc1d1	výchozí
návrh	návrh	k1gInSc1	návrh
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
letu	let	k1gInSc2	let
byl	být	k5eAaImAgMnS	být
RCA	RCA	kA	RCA
1802	[number]	k4	1802
od	od	k7c2	od
Radio	radio	k1gNnSc1	radio
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
America	Americ	k1gInSc2	Americ
(	(	kIx(	(
<g/>
též	též	k9	též
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k8xS	jako
CDP	CDP	kA	CDP
<g/>
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
RCA	RCA	kA	RCA
COSMAC	COSMAC	kA	COSMAC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
NASA	NASA	kA	NASA
použila	použít	k5eAaPmAgFnS	použít
v	v	k7c6	v
programu	program	k1gInSc6	program
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
sond	sonda	k1gFnPc2	sonda
Voyager	Voyager	k1gMnSc1	Voyager
a	a	k8xC	a
Viking	Viking	k1gMnSc1	Viking
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
na	na	k7c4	na
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CDP1802	CDP1802	k4	CDP1802
byl	být	k5eAaImAgMnS	být
použit	použít	k5eAaPmNgMnS	použít
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
spotřebu	spotřeba	k1gFnSc4	spotřeba
a	a	k8xC	a
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
výrobní	výrobní	k2eAgInSc1d1	výrobní
proces	proces	k1gInSc1	proces
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
lepší	dobrý	k2eAgFnSc4d2	lepší
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
kosmickému	kosmický	k2eAgNnSc3d1	kosmické
záření	záření	k1gNnSc3	záření
a	a	k8xC	a
elektrostatickým	elektrostatický	k2eAgInPc3d1	elektrostatický
výbojům	výboj	k1gInPc3	výboj
než	než	k8xS	než
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
jiný	jiný	k2eAgInSc1d1	jiný
procesor	procesor	k1gInSc1	procesor
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
o	o	k7c6	o
1802	[number]	k4	1802
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
prvním	první	k4xOgInSc6	první
procesoru	procesor	k1gInSc6	procesor
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
radiaci	radiace	k1gFnSc3	radiace
<g/>
.	.	kIx.	.
</s>
<s>
CPU	cpát	k5eAaImIp1nS	cpát
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
instrukční	instrukční	k2eAgFnSc2d1	instrukční
sady	sada	k1gFnSc2	sada
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
zrodu	zrod	k1gInSc2	zrod
<g/>
)	)	kIx)	)
CISC	CISC	kA	CISC
<g/>
,	,	kIx,	,
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
VLIW	VLIW	kA	VLIW
a	a	k8xC	a
EPIC.	EPIC.	k1gFnSc1	EPIC.
Mezi	mezi	k7c4	mezi
představitele	představitel	k1gMnPc4	představitel
CPU	CPU	kA	CPU
s	s	k7c7	s
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
CISC	CISC	kA	CISC
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zejména	zejména	k9	zejména
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
řady	řada	k1gFnSc2	řada
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
představitele	představitel	k1gMnPc4	představitel
CPU	CPU	kA	CPU
s	s	k7c7	s
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
RISC	RISC	kA	RISC
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
SPARC	SPARC	kA	SPARC
<g/>
,	,	kIx,	,
MIPS	MIPS	kA	MIPS
<g/>
,	,	kIx,	,
PowerPC	PowerPC	k1gFnSc1	PowerPC
a	a	k8xC	a
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
představitele	představitel	k1gMnPc4	představitel
VLIW	VLIW	kA	VLIW
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
Tilera	Tiler	k1gMnSc4	Tiler
a	a	k8xC	a
některé	některý	k3yIgNnSc4	některý
GPU	GPU	kA	GPU
fy	fy	kA	fy
AMD	AMD	kA	AMD
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
představitelé	představitel	k1gMnPc1	představitel
instrukční	instrukční	k2eAgFnSc2d1	instrukční
sady	sada	k1gFnSc2	sada
typu	typ	k1gInSc2	typ
EPIC	EPIC	kA	EPIC
jsou	být	k5eAaImIp3nP	být
mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
IA-64	IA-64	k1gFnSc2	IA-64
a	a	k8xC	a
ev.	ev.	k?	ev.
mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
Elbrus	Elbrus	k1gInSc1	Elbrus
<g/>
.	.	kIx.	.
</s>
<s>
CPU	CPU	kA	CPU
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
mikroprocesorech	mikroprocesor	k1gInPc6	mikroprocesor
byly	být	k5eAaImAgFnP	být
4	[number]	k4	4
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
počítaly	počítat	k5eAaImAgFnP	počítat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
v	v	k7c6	v
desítkové	desítkový	k2eAgFnSc6d1	desítková
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
přišly	přijít	k5eAaPmAgFnP	přijít
8	[number]	k4	8
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
procesory	procesor	k1gInPc1	procesor
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
umí	umět	k5eAaImIp3nS	umět
přímo	přímo	k6eAd1	přímo
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
255	[number]	k4	255
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
16	[number]	k4	16
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
procesor	procesor	k1gInSc4	procesor
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
65535	[number]	k4	65535
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
0	[number]	k4	0
až	až	k9	až
216	[number]	k4	216
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
<s>
Operace	operace	k1gFnSc1	operace
s	s	k7c7	s
většími	veliký	k2eAgNnPc7d2	veliký
čísly	číslo	k1gNnPc7	číslo
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
kroků	krok	k1gInPc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
32	[number]	k4	32
<g/>
bitů	bit	k1gInPc2	bit
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
dostačující	dostačující	k2eAgInPc1d1	dostačující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
kvůli	kvůli	k7c3	kvůli
návrhu	návrh	k1gInSc3	návrh
dokázaly	dokázat	k5eAaPmAgInP	dokázat
přímo	přímo	k6eAd1	přímo
adresovat	adresovat	k5eAaBmF	adresovat
jen	jen	k9	jen
4	[number]	k4	4
GB	GB	kA	GB
virtuální	virtuální	k2eAgFnSc2d1	virtuální
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
procesory	procesor	k1gInPc1	procesor
64	[number]	k4	64
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgInP	existovat
i	i	k9	i
procesory	procesor	k1gInPc1	procesor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
exotické	exotický	k2eAgFnPc1d1	exotická
šířky	šířka	k1gFnPc1	šířka
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
10	[number]	k4	10
nebo	nebo	k8xC	nebo
24	[number]	k4	24
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
DSP	DSP	kA	DSP
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Motorola	Motorola	kA	Motorola
56000	[number]	k4	56000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
se	se	k3xPyFc4	se
však	však	k9	však
prosadily	prosadit	k5eAaPmAgInP	prosadit
procesory	procesor	k1gInPc1	procesor
s	s	k7c7	s
šířkou	šířka	k1gFnSc7	šířka
slova	slovo	k1gNnSc2	slovo
danou	daný	k2eAgFnSc7d1	daná
mocninou	mocnina	k1gFnSc7	mocnina
2	[number]	k4	2
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
jednodušší	jednoduchý	k2eAgFnSc3d2	jednodušší
manipulaci	manipulace	k1gFnSc3	manipulace
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
zaměnitelnosti	zaměnitelnost	k1gFnSc3	zaměnitelnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
operandů	operand	k1gInPc2	operand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
RISC	RISC	kA	RISC
s	s	k7c7	s
redukovanou	redukovaný	k2eAgFnSc7d1	redukovaná
sadou	sada	k1gFnSc7	sada
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
a	a	k8xC	a
CISC	CISC	kA	CISC
procesory	procesor	k1gInPc1	procesor
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
se	se	k3xPyFc4	se
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
instrukční	instrukční	k2eAgFnPc1d1	instrukční
sady	sada	k1gFnPc1	sada
typu	typ	k1gInSc2	typ
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
některé	některý	k3yIgFnSc2	některý
architektury	architektura	k1gFnSc2	architektura
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zachování	zachování	k1gNnSc4	zachování
zpětné	zpětný	k2eAgFnSc2d1	zpětná
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
pracují	pracovat	k5eAaImIp3nP	pracovat
i	i	k9	i
se	s	k7c7	s
strojovým	strojový	k2eAgInSc7d1	strojový
kódem	kód	k1gInSc7	kód
typu	typ	k1gInSc2	typ
CISC	CISC	kA	CISC
(	(	kIx(	(
<g/>
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
RISC	RISC	kA	RISC
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
např.	např.	kA	např.
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
nebo	nebo	k8xC	nebo
v	v	k7c6	v
superpočítačích	superpočítač	k1gInPc6	superpočítač
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
architektura	architektura	k1gFnSc1	architektura
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nižší	nízký	k2eAgFnSc7d2	nižší
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
aplikace	aplikace	k1gFnPc4	aplikace
nemusí	muset	k5eNaImIp3nS	muset
procesor	procesor	k1gInSc1	procesor
integrovat	integrovat	k5eAaBmF	integrovat
jednotku	jednotka	k1gFnSc4	jednotka
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
MMU	MMU	kA	MMU
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
memory	memora	k1gFnSc2	memora
management	management	k1gInSc1	management
unit	unit	k1gInSc1	unit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
podporovat	podporovat	k5eAaImF	podporovat
ochranu	ochrana	k1gFnSc4	ochrana
paměti	paměť	k1gFnSc2	paměť
nebo	nebo	k8xC	nebo
privilegovaný	privilegovaný	k2eAgInSc4d1	privilegovaný
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
plnohodnotných	plnohodnotný	k2eAgInPc2d1	plnohodnotný
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Windows	Windows	kA	Windows
NT	NT	kA	NT
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc4d2	vyšší
tj.	tj.	kA	tj.
XP	XP	kA	XP
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
...	...	k?	...
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
paměti	paměť	k1gFnSc2	paměť
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
procesoru	procesor	k1gInSc6	procesor
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnPc1d1	omezená
možnosti	možnost	k1gFnPc1	možnost
preemptivního	preemptivní	k2eAgInSc2d1	preemptivní
multitaskingu	multitasking	k1gInSc2	multitasking
<g/>
,	,	kIx,	,
současné	současný	k2eAgFnSc2d1	současná
práce	práce	k1gFnSc2	práce
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
počítači	počítač	k1gInSc6	počítač
nebo	nebo	k8xC	nebo
virtualizace	virtualizace	k1gFnSc2	virtualizace
<g/>
.	.	kIx.	.
</s>
<s>
Jednočipový	jednočipový	k2eAgInSc1d1	jednočipový
mikropočítač	mikropočítač	k1gInSc1	mikropočítač
nebo	nebo	k8xC	nebo
také	také	k9	také
mikrokontrolér	mikrokontrolér	k1gInSc1	mikrokontrolér
(	(	kIx(	(
<g/>
MCU	MCU	kA	MCU
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
procesoru	procesor	k1gInSc2	procesor
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
obvody	obvod	k1gInPc4	obvod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
časovače	časovač	k1gInPc4	časovač
(	(	kIx(	(
<g/>
timery	timera	k1gFnPc4	timera
a	a	k8xC	a
watchdog	watchdog	k1gInSc4	watchdog
timery	timera	k1gFnSc2	timera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nevolatilní	volatilní	k2eNgFnSc1d1	volatilní
paměť	paměť	k1gFnSc1	paměť
(	(	kIx(	(
<g/>
EEPROM	EEPROM	kA	EEPROM
<g/>
,	,	kIx,	,
FLASH	FLASH	kA	FLASH
nebo	nebo	k8xC	nebo
ROM	ROM	kA	ROM
<g/>
)	)	kIx)	)
a	a	k8xC	a
volatilní	volatilní	k2eAgFnSc4d1	volatilní
paměť	paměť	k1gFnSc4	paměť
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
SRAM	SRAM	kA	SRAM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vstupně	vstupně	k6eAd1	vstupně
výstupní	výstupní	k2eAgInPc1d1	výstupní
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
samostatné	samostatný	k2eAgFnPc4d1	samostatná
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
průkopníky	průkopník	k1gMnPc4	průkopník
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kategorii	kategorie	k1gFnSc6	kategorie
můžeme	moct	k5eAaImIp1nP	moct
považovat	považovat	k5eAaImF	považovat
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
procesor	procesor	k1gInSc4	procesor
Intel	Intel	kA	Intel
i	i	k8xC	i
<g/>
8051	[number]	k4	8051
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poprvé	poprvé	k6eAd1	poprvé
integroval	integrovat	k5eAaBmAgInS	integrovat
všechny	všechen	k3xTgFnPc4	všechen
základní	základní	k2eAgFnPc4d1	základní
periferie	periferie	k1gFnPc4	periferie
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc4	paměť
RAM	RAM	kA	RAM
<g/>
,	,	kIx,	,
EEPROM	EEPROM	kA	EEPROM
<g/>
,	,	kIx,	,
čítače	čítač	k1gInPc1	čítač
a	a	k8xC	a
časovače	časovač	k1gInPc1	časovač
<g/>
)	)	kIx)	)
na	na	k7c6	na
jediném	jediné	k1gNnSc6	jediné
čipu	čip	k1gInSc2	čip
a	a	k8xC	a
16	[number]	k4	16
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
technologický	technologický	k2eAgInSc4d1	technologický
procesor	procesor	k1gInSc4	procesor
Siemens	siemens	k1gInSc1	siemens
SAB	SAB	kA	SAB
80	[number]	k4	80
<g/>
C	C	kA	C
<g/>
166	[number]	k4	166
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poprvé	poprvé	k6eAd1	poprvé
integroval	integrovat	k5eAaBmAgMnS	integrovat
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
D	D	kA	D
převodníky	převodník	k1gInPc1	převodník
<g/>
,	,	kIx,	,
komunikační	komunikační	k2eAgFnPc1d1	komunikační
linky	linka	k1gFnPc1	linka
a	a	k8xC	a
masivní	masivní	k2eAgInSc1d1	masivní
systém	systém	k1gInSc1	systém
čítačů	čítač	k1gInPc2	čítač
<g/>
/	/	kIx~	/
<g/>
časovačů	časovač	k1gInPc2	časovač
<g/>
/	/	kIx~	/
<g/>
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Následníky	následník	k1gMnPc4	následník
řady	řada	k1gFnSc2	řada
80166	[number]	k4	80166
dnes	dnes	k6eAd1	dnes
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Infineon	Infineon	k1gMnSc1	Infineon
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
C167	C167	k1gFnSc1	C167
a	a	k8xC	a
C166	C166	k1gFnSc1	C166
SV	sv	kA	sv
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
SGS	SGS	kA	SGS
Thomson	Thomson	k1gInSc1	Thomson
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
ST	St	kA	St
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jednočipových	jednočipový	k2eAgInPc2d1	jednočipový
mikropočítačů	mikropočítač	k1gInPc2	mikropočítač
a	a	k8xC	a
mikrokontrolérů	mikrokontrolér	k1gInPc2	mikrokontrolér
převažují	převažovat	k5eAaImIp3nP	převažovat
obvody	obvod	k1gInPc1	obvod
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
ARM	ARM	kA	ARM
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgInSc1d1	digitální
signálový	signálový	k2eAgInSc1d1	signálový
procesor	procesor	k1gInSc1	procesor
(	(	kIx(	(
<g/>
DSP	DSP	kA	DSP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
DSP	DSP	kA	DSP
jsou	být	k5eAaImIp3nP	být
optimalizovány	optimalizovat	k5eAaBmNgFnP	optimalizovat
na	na	k7c4	na
co	co	k3yRnSc4	co
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
opakování	opakování	k1gNnSc4	opakování
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
matematických	matematický	k2eAgInPc2d1	matematický
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
aplikací	aplikace	k1gFnSc7	aplikace
DSP	DSP	kA	DSP
je	být	k5eAaImIp3nS	být
filtrace	filtrace	k1gFnSc1	filtrace
signálu	signál	k1gInSc2	signál
pomocí	pomocí	k7c2	pomocí
filtrů	filtr	k1gInPc2	filtr
FIR	FIR	kA	FIR
a	a	k8xC	a
IIR	IIR	kA	IIR
nebo	nebo	k8xC	nebo
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
analýza	analýza	k1gFnSc1	analýza
<g/>
.	.	kIx.	.
</s>
<s>
DSP	DSP	kA	DSP
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používají	používat	k5eAaImIp3nP	používat
především	především	k9	především
ve	v	k7c6	v
spotřební	spotřební	k2eAgFnSc6d1	spotřební
elektronice	elektronika	k1gFnSc6	elektronika
a	a	k8xC	a
v	v	k7c6	v
telekomunikační	telekomunikační	k2eAgFnSc6d1	telekomunikační
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
DSP	DSP	kA	DSP
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
předchůdcům	předchůdce	k1gMnPc3	předchůdce
navíc	navíc	k6eAd1	navíc
také	také	k9	také
rychlé	rychlý	k2eAgFnPc4d1	rychlá
komunikační	komunikační	k2eAgFnPc4d1	komunikační
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přenášet	přenášet	k5eAaImF	přenášet
velký	velký	k2eAgInSc4d1	velký
datový	datový	k2eAgInSc4d1	datový
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
protékající	protékající	k2eAgFnSc1d1	protékající
těmito	tento	k3xDgInPc7	tento
procesory	procesor	k1gInPc7	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
rovněž	rovněž	k9	rovněž
pozorovat	pozorovat	k5eAaImF	pozorovat
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
výhod	výhoda	k1gFnPc2	výhoda
DSP	DSP	kA	DSP
a	a	k8xC	a
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
cestou	cestou	k7c2	cestou
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
DSP	DSP	kA	DSP
o	o	k7c4	o
periferie	periferie	k1gFnPc4	periferie
nebo	nebo	k8xC	nebo
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
mikrokontrolérů	mikrokontrolér	k1gMnPc2	mikrokontrolér
o	o	k7c4	o
DSP	DSP	kA	DSP
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jde	jít	k5eAaImIp3nS	jít
vývoj	vývoj	k1gInSc1	vývoj
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
více	hodně	k6eAd2	hodně
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
procesorů	procesor	k1gInPc2	procesor
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
čipu	čip	k1gInSc2	čip
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
trend	trend	k1gInSc4	trend
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
u	u	k7c2	u
procesorů	procesor	k1gInPc2	procesor
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
jednojádrové	jednojádrový	k2eAgFnSc6d1	jednojádrová
a	a	k8xC	a
vícejádrové	vícejádrový	k2eAgFnSc6d1	vícejádrový
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšování	zvyšování	k1gNnSc4	zvyšování
počtu	počet	k1gInSc2	počet
jader	jádro	k1gNnPc2	jádro
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vynuceno	vynutit	k5eAaPmNgNnS	vynutit
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
omezeními	omezení	k1gNnPc7	omezení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rychlostí	rychlost	k1gFnSc7	rychlost
šíření	šíření	k1gNnSc2	šíření
elektrického	elektrický	k2eAgInSc2d1	elektrický
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
technologií	technologie	k1gFnSc7	technologie
daným	daný	k2eAgInSc7d1	daný
ztrátovým	ztrátový	k2eAgInSc7d1	ztrátový
výkonem	výkon	k1gInSc7	výkon
čipu	čip	k1gInSc2	čip
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Integrací	integrace	k1gFnSc7	integrace
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
jednodušších	jednoduchý	k2eAgNnPc2d2	jednodušší
jader	jádro	k1gNnPc2	jádro
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
výrobní	výrobní	k2eAgFnSc6d1	výrobní
technologii	technologie	k1gFnSc6	technologie
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
ploše	plocha	k1gFnSc6	plocha
křemíku	křemík	k1gInSc2	křemík
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
aplikace	aplikace	k1gFnPc4	aplikace
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
náročných	náročný	k2eAgInPc2d1	náročný
paralelních	paralelní	k2eAgInPc2d1	paralelní
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
)	)	kIx)	)
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgInSc4d2	vyšší
výpočetní	výpočetní	k2eAgInSc4d1	výpočetní
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
než	než	k8xS	než
použitím	použití	k1gNnSc7	použití
jediného	jediný	k2eAgNnSc2d1	jediné
složitého	složitý	k2eAgNnSc2d1	složité
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
jako	jako	k8xS	jako
jednoúčelový	jednoúčelový	k2eAgInSc1d1	jednoúčelový
obvod	obvod	k1gInSc1	obvod
určený	určený	k2eAgInSc1d1	určený
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
velkosériové	velkosériový	k2eAgFnSc3d1	velkosériová
výrobě	výroba	k1gFnSc3	výroba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
implementován	implementován	k2eAgInSc1d1	implementován
také	také	k9	také
jako	jako	k9	jako
firmware	firmware	k1gInSc4	firmware
běžící	běžící	k2eAgInSc4d1	běžící
v	v	k7c6	v
programovatelném	programovatelný	k2eAgNnSc6d1	programovatelné
hradlovém	hradlový	k2eAgNnSc6d1	hradlové
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc4	jádro
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
8	[number]	k4	8
<g/>
bitových	bitový	k2eAgInPc2d1	bitový
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
hradel	hradlo	k1gNnPc2	hradlo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
složitosti	složitost	k1gFnSc3	složitost
procesorů	procesor	k1gInPc2	procesor
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stanovit	stanovit	k5eAaPmF	stanovit
několik	několik	k4yIc4	několik
jednoduše	jednoduše	k6eAd1	jednoduše
srozumitelných	srozumitelný	k2eAgInPc2d1	srozumitelný
obecných	obecný	k2eAgInPc2d1	obecný
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
umožnily	umožnit	k5eAaPmAgFnP	umožnit
objektivní	objektivní	k2eAgNnSc4d1	objektivní
srovnání	srovnání	k1gNnSc4	srovnání
různých	různý	k2eAgInPc2d1	různý
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zhruba	zhruba	k6eAd1	zhruba
srovnat	srovnat	k5eAaPmF	srovnat
hlavní	hlavní	k2eAgInPc4d1	hlavní
rysy	rys	k1gInPc4	rys
současných	současný	k2eAgInPc2d1	současný
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Váňa	Váňa	k1gMnSc1	Váňa
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
:	:	kIx,	:
ARM	ARM	kA	ARM
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7300-246-6	[number]	k4	978-80-7300-246-6
Skalický	Skalický	k1gMnSc1	Skalický
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
řady	řada	k1gFnSc2	řada
8051	[number]	k4	8051
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86056-39-2	[number]	k4	80-86056-39-2
Pinker	Pinker	k1gMnSc1	Pinker
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
a	a	k8xC	a
mikropočítače	mikropočítač	k1gInPc1	mikropočítač
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7300-110-1	[number]	k4	80-7300-110-1
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
–	–	k?	–
Michel	Michel	k1gMnSc1	Michel
<g/>
,	,	kIx,	,
Hugon	Hugon	k1gMnSc1	Hugon
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
Corvec	Corvec	k1gMnSc1	Corvec
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Le	Le	k1gMnSc1	Le
<g/>
:	:	kIx,	:
Od	od	k7c2	od
logických	logický	k2eAgInPc2d1	logický
obvodů	obvod	k1gInPc2	obvod
k	k	k7c3	k
mikroprocesorům	mikroprocesor	k1gInPc3	mikroprocesor
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
Centrální	centrální	k2eAgFnSc1d1	centrální
procesorová	procesorový	k2eAgFnSc1d1	procesorová
jednotka	jednotka	k1gFnSc1	jednotka
Architektura	architektura	k1gFnSc1	architektura
procesoru	procesor	k1gInSc2	procesor
Vektorový	vektorový	k2eAgInSc1d1	vektorový
procesor	procesor	k1gInSc1	procesor
</s>
