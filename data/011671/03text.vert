<p>
<s>
Meč	meč	k1gInSc1	meč
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
jako	jako	k9	jako
zbraň	zbraň	k1gFnSc1	zbraň
sečná	sečný	k2eAgFnSc1d1	sečná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bodná	bodný	k2eAgFnSc1d1	bodná
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
osobních	osobní	k2eAgFnPc2d1	osobní
zbraní	zbraň	k1gFnPc2	zbraň
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
starověk	starověk	k1gInSc4	starověk
a	a	k8xC	a
středověk	středověk	k1gInSc4	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc2	novověk
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
vytlačován	vytlačovat	k5eAaImNgInS	vytlačovat
palnými	palný	k2eAgFnPc7d1	palná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
se	se	k3xPyFc4	se
z	z	k7c2	z
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
bronzu	bronz	k1gInSc2	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
byl	být	k5eAaImAgInS	být
mnoho	mnoho	k6eAd1	mnoho
set	set	k1gInSc1	set
let	let	k1gInSc1	let
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
prestižní	prestižní	k2eAgFnSc4d1	prestižní
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
výsadu	výsada	k1gFnSc4	výsada
urozeného	urozený	k2eAgInSc2d1	urozený
stavu	stav	k1gInSc2	stav
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
odznak	odznak	k1gInSc1	odznak
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
–	–	k?	–
čepele	čepel	k1gInSc2	čepel
a	a	k8xC	a
jílce	jílec	k1gInSc2	jílec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
přímá	přímý	k2eAgFnSc1d1	přímá
a	a	k8xC	a
dvojbřitá	dvojbřitý	k2eAgFnSc1d1	dvojbřitá
-	-	kIx~	-
broušená	broušený	k2eAgFnSc1d1	broušená
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zakončena	zakončit	k5eAaPmNgFnS	zakončit
hrotem	hrot	k1gInSc7	hrot
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zakulaceným	zakulacený	k2eAgMnPc3d1	zakulacený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc4d1	velké
rozpětí	rozpětí	k1gNnSc4	rozpětí
délky	délka	k1gFnSc2	délka
až	až	k9	až
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
10	[number]	k4	10
centimetrů	centimetr	k1gInPc2	centimetr
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
:	:	kIx,	:
Čepel	čepel	k1gInSc1	čepel
obvykle	obvykle	k6eAd1	obvykle
nebývá	bývat	k5eNaImIp3nS	bývat
zdobena	zdoben	k2eAgFnSc1d1	zdobena
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
rytina	rytina	k1gFnSc1	rytina
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
kříž	kříž	k1gInSc1	kříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ornament	ornament	k1gInSc1	ornament
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
bojový	bojový	k2eAgInSc1d1	bojový
pokřik	pokřik	k1gInSc1	pokřik
nebo	nebo	k8xC	nebo
osobní	osobní	k2eAgNnSc1d1	osobní
heslo	heslo	k1gNnSc1	heslo
(	(	kIx(	(
<g/>
devisa	devisa	k1gFnSc1	devisa
<g/>
)	)	kIx)	)
majitele	majitel	k1gMnSc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Rytina	rytina	k1gFnSc1	rytina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvýrazněna	zvýraznit	k5eAaPmNgFnS	zvýraznit
měkčím	měkký	k2eAgInSc7d2	měkčí
kovem	kov	k1gInSc7	kov
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
)	)	kIx)	)
zatloukaným	zatloukaný	k2eAgInSc7d1	zatloukaný
do	do	k7c2	do
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
<g/>
,	,	kIx,	,
zbroušeným	zbroušený	k2eAgInSc7d1	zbroušený
a	a	k8xC	a
leštěným	leštěný	k2eAgInSc7d1	leštěný
<g/>
,	,	kIx,	,
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
taušírování	taušírování	k1gNnSc1	taušírování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
středověkých	středověký	k2eAgInPc2d1	středověký
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
mečů	meč	k1gInPc2	meč
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
záštity	záštita	k1gFnSc2	záštita
proražený	proražený	k2eAgInSc1d1	proražený
malý	malý	k2eAgInSc1d1	malý
otvor	otvor	k1gInSc1	otvor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čepelí	čepel	k1gFnPc2	čepel
z	z	k7c2	z
damaskované	damaskovaný	k2eAgFnSc2d1	damaskovaný
oceli	ocel	k1gFnSc2	ocel
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
dvoubarevné	dvoubarevný	k2eAgFnPc4d1	dvoubarevná
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
evropských	evropský	k2eAgInPc2d1	evropský
mečů	meč	k1gInPc2	meč
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
podélně	podélně	k6eAd1	podélně
jí	on	k3xPp3gFnSc3	on
prochází	procházet	k5eAaImIp3nS	procházet
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
"	"	kIx"	"
<g/>
žlábků	žlábek	k1gInPc2	žlábek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
"	"	kIx"	"
<g/>
žlábku	žlábek	k1gInSc6	žlábek
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
lépe	dobře	k6eAd2	dobře
odvádět	odvádět	k5eAaImF	odvádět
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
novodobý	novodobý	k2eAgInSc1d1	novodobý
mýtus	mýtus	k1gInSc1	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žlábek	žlábek	k1gInSc1	žlábek
sloužil	sloužit	k5eAaImAgInS	sloužit
ke	k	k7c3	k
zpevnění	zpevnění	k1gNnSc3	zpevnění
čepele	čepel	k1gFnSc2	čepel
v	v	k7c6	v
ohybu	ohyb	k1gInSc6	ohyb
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
odlehčení	odlehčení	k1gNnSc3	odlehčení
<g/>
,	,	kIx,	,
mečíř	mečíř	k1gMnSc1	mečíř
jej	on	k3xPp3gMnSc4	on
koval	kovat	k5eAaImAgMnS	kovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomocí	pomocí	k7c2	pomocí
něj	on	k3xPp3gNnSc2	on
čepel	čepel	k1gInSc1	čepel
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
staletích	staletí	k1gNnPc6	staletí
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
čepele	čepel	k1gFnPc1	čepel
s	s	k7c7	s
šestiúhelníkovým	šestiúhelníkový	k2eAgInSc7d1	šestiúhelníkový
průřezem	průřez	k1gInSc7	průřez
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
žlábku	žlábek	k1gInSc2	žlábek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
bývala	bývat	k5eAaImAgFnS	bývat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
kulturách	kultura	k1gFnPc6	kultura
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
nejkvalitnější	kvalitní	k2eAgFnSc2d3	nejkvalitnější
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
nákladná	nákladný	k2eAgFnSc1d1	nákladná
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zejména	zejména	k9	zejména
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
existovaly	existovat	k5eAaImAgInP	existovat
i	i	k9	i
sice	sice	k8xC	sice
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
levnější	levný	k2eAgInPc1d2	levnější
meče	meč	k1gInPc1	meč
pro	pro	k7c4	pro
hromadnější	hromadní	k2eAgNnSc4d2	hromadní
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
např.	např.	kA	např.
běžné	běžný	k2eAgNnSc1d1	běžné
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgInPc4d2	lepší
meče	meč	k1gInPc4	meč
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
čepel	čepel	k1gFnSc1	čepel
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
několika	několik	k4yIc6	několik
střediscích	středisko	k1gNnPc6	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Mečíři	mečíř	k1gMnPc1	mečíř
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
umění	umění	k1gNnSc4	umění
předávali	předávat	k5eAaImAgMnP	předávat
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c6	na
generaci	generace	k1gFnSc6	generace
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
technologie	technologie	k1gFnSc2	technologie
tajili	tajit	k5eAaImAgMnP	tajit
před	před	k7c7	před
konkurencí	konkurence	k1gFnSc7	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Povolání	povolání	k1gNnSc4	povolání
mečíře	mečíř	k1gMnSc2	mečíř
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
ceněné	ceněný	k2eAgNnSc1d1	ceněné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jílec	jílec	k1gInSc1	jílec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zpravidla	zpravidla	k6eAd1	zpravidla
z	z	k7c2	z
křížové	křížový	k2eAgFnSc2d1	křížová
záštity	záštita	k1gFnSc2	záštita
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
mečů	meč	k1gInPc2	meč
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rukojeti	rukojeť	k1gFnSc2	rukojeť
a	a	k8xC	a
hlavice	hlavice	k1gFnSc2	hlavice
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
mečů	meč	k1gInPc2	meč
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
jílce	jílec	k1gInSc2	jílec
bývaly	bývat	k5eAaImAgFnP	bývat
často	často	k6eAd1	často
nákladně	nákladně	k6eAd1	nákladně
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
jejich	jejich	k3xOp3gFnSc2	jejich
velikosti	velikost	k1gFnSc2	velikost
je	být	k5eAaImIp3nS	být
také	také	k9	také
značné	značný	k2eAgNnSc1d1	značné
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
jílce	jílec	k1gInPc1	jílec
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
15	[number]	k4	15
centimetrů	centimetr	k1gInPc2	centimetr
až	až	k6eAd1	až
do	do	k7c2	do
jílců	jílec	k1gInPc2	jílec
přesahující	přesahující	k2eAgInSc4d1	přesahující
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
záštita	záštita	k1gFnSc1	záštita
tvoří	tvořit	k5eAaImIp3nS	tvořit
předěl	předěl	k1gInSc4	předěl
mezi	mezi	k7c7	mezi
čepelí	čepel	k1gFnSc7	čepel
a	a	k8xC	a
rukojetí	rukojeť	k1gFnSc7	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
rovný	rovný	k2eAgInSc4d1	rovný
prut	prut	k1gInSc4	prut
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
delší	dlouhý	k2eAgFnSc4d2	delší
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
zdobený	zdobený	k2eAgInSc1d1	zdobený
a	a	k8xC	a
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
funkce	funkce	k1gFnSc2	funkce
vytvarovaný	vytvarovaný	k2eAgInSc1d1	vytvarovaný
<g/>
.	.	kIx.	.
</s>
<s>
Záštita	záštita	k1gFnSc1	záštita
se	se	k3xPyFc4	se
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
ohnula	ohnout	k5eAaPmAgFnS	ohnout
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
hlavici	hlavice	k1gFnSc3	hlavice
rukojeti	rukojeť	k1gFnSc2	rukojeť
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tak	tak	k6eAd1	tak
zvanou	zvaný	k2eAgFnSc4d1	zvaná
protizáštitu	protizáštit	k1gInSc3	protizáštit
<g/>
.	.	kIx.	.
</s>
<s>
Záštita	záštita	k1gFnSc1	záštita
znemožňovala	znemožňovat	k5eAaImAgFnS	znemožňovat
ztrátu	ztráta	k1gFnSc4	ztráta
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
rukojetí	rukojeť	k1gFnSc7	rukojeť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
chránila	chránit	k5eAaImAgFnS	chránit
ruku	ruka	k1gFnSc4	ruka
před	před	k7c7	před
zraněním	zranění	k1gNnSc7	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Rukojeť	rukojeť	k1gFnSc1	rukojeť
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
záštitou	záštita	k1gFnSc7	záštita
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnSc2d1	určená
k	k	k7c3	k
držení	držení	k1gNnSc3	držení
meče	meč	k1gInSc2	meč
<g/>
,	,	kIx,	,
rukojeti	rukojeť	k1gFnPc1	rukojeť
bývaly	bývat	k5eAaImAgFnP	bývat
často	často	k6eAd1	často
hodně	hodně	k6eAd1	hodně
a	a	k8xC	a
draze	draha	k1gFnSc3	draha
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
různé	různý	k2eAgInPc1d1	různý
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
je	být	k5eAaImIp3nS	být
zakončení	zakončení	k1gNnSc4	zakončení
jílce	jílec	k1gInSc2	jílec
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
tvar	tvar	k1gInSc4	tvar
plochého	plochý	k2eAgInSc2d1	plochý
medailonu	medailon	k1gInSc2	medailon
<g/>
,	,	kIx,	,
ořechu	ořech	k1gInSc2	ořech
nebo	nebo	k8xC	nebo
para	para	k2eAgInSc2d1	para
ořechu	ořech	k1gInSc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
primární	primární	k2eAgFnSc1d1	primární
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
vyvažovací	vyvažovací	k2eAgFnSc1d1	vyvažovací
<g/>
.	.	kIx.	.
</s>
<s>
Bývala	bývat	k5eAaImAgFnS	bývat
zdobena	zdobit	k5eAaImNgFnS	zdobit
znaky	znak	k1gInPc4	znak
majitele	majitel	k1gMnSc2	majitel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Meč	meč	k1gInSc1	meč
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
dal	dát	k5eAaPmAgInS	dát
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ozdobit	ozdobit	k5eAaPmF	ozdobit
znaky	znak	k1gInPc4	znak
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Bronzové	bronzový	k2eAgInPc1d1	bronzový
meče	meč	k1gInPc1	meč
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
meče	meč	k1gInSc2	meč
byl	být	k5eAaImAgInS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
hlavně	hlavně	k9	hlavně
pokrokem	pokrok	k1gInSc7	pokrok
lidstva	lidstvo	k1gNnSc2	lidstvo
v	v	k7c6	v
technologiích	technologie	k1gFnPc6	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
technologie	technologie	k1gFnSc2	technologie
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
3500-3000	[number]	k4	3500-3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tyto	tento	k3xDgFnPc1	tento
zbraně	zbraň	k1gFnPc1	zbraň
tvarově	tvarově	k6eAd1	tvarově
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
pazourkové	pazourkový	k2eAgFnSc2d1	Pazourková
dýky	dýka	k1gFnSc2	dýka
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
měděné	měděný	k2eAgFnPc4d1	měděná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
meč	meč	k1gInSc4	meč
sice	sice	k8xC	sice
připomínaly	připomínat	k5eAaImAgFnP	připomínat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
dýkou	dýka	k1gFnSc7	dýka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
lidstvo	lidstvo	k1gNnSc1	lidstvo
začalo	začít	k5eAaPmAgNnS	začít
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
bronzu	bronz	k1gInSc2	bronz
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
možnostmi	možnost	k1gFnPc7	možnost
odlévání	odlévání	k1gNnSc2	odlévání
a	a	k8xC	a
metalurgickými	metalurgický	k2eAgFnPc7d1	metalurgická
možnostmi	možnost	k1gFnPc7	možnost
zkvalitnění	zkvalitnění	k1gNnPc2	zkvalitnění
kovu	kov	k1gInSc2	kov
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
různých	různý	k2eAgInPc2d1	různý
poměrů	poměr	k1gInPc2	poměr
příměsí	příměs	k1gFnPc2	příměs
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
daly	dát	k5eAaPmAgFnP	dát
zhotovit	zhotovit	k5eAaPmF	zhotovit
silné	silný	k2eAgFnPc1d1	silná
čepele	čepel	k1gFnPc1	čepel
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgFnPc6	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zbraň	zbraň	k1gFnSc4	zbraň
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
první	první	k4xOgInPc4	první
meče	meč	k1gInPc4	meč
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
převážně	převážně	k6eAd1	převážně
bodnou	bodný	k2eAgFnSc4d1	bodná
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
jako	jako	k9	jako
takové	takový	k3xDgFnPc1	takový
byly	být	k5eAaImAgFnP	být
i	i	k9	i
koncipovány	koncipovat	k5eAaBmNgFnP	koncipovat
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
listová	listový	k2eAgFnSc1d1	listová
čepel	čepel	k1gFnSc1	čepel
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
jílec	jílec	k1gInSc1	jílec
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
těchto	tento	k3xDgInPc2	tento
mečů	meč	k1gInPc2	meč
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
vkládal	vkládat	k5eAaImAgMnS	vkládat
centrální	centrální	k2eAgInSc4d1	centrální
pás	pás	k1gInSc4	pás
kvůli	kvůli	k7c3	kvůli
zvýšení	zvýšení	k1gNnSc3	zvýšení
kompaktnosti	kompaktnost	k1gFnSc2	kompaktnost
při	při	k7c6	při
bodání	bodání	k1gNnSc6	bodání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
meče	meč	k1gInPc4	meč
nalezené	nalezený	k2eAgInPc4d1	nalezený
na	na	k7c6	na
řeckém	řecký	k2eAgNnSc6d1	řecké
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
oboustranně	oboustranně	k6eAd1	oboustranně
broušenou	broušený	k2eAgFnSc4d1	broušená
čepel	čepel	k1gFnSc4	čepel
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gInPc1	jejich
rozměry	rozměr	k1gInPc1	rozměr
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
mezi	mezi	k7c4	mezi
60	[number]	k4	60
a	a	k8xC	a
90	[number]	k4	90
centimetry	centimetr	k1gInPc7	centimetr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
třetího	třetí	k4xOgNnSc2	třetí
a	a	k8xC	a
druhého	druhý	k4xOgMnSc4	druhý
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jejich	jejich	k3xOp3gFnSc4	jejich
výrobci	výrobce	k1gMnPc1	výrobce
byli	být	k5eAaImAgMnP	být
patrně	patrně	k6eAd1	patrně
nájezdníci	nájezdník	k1gMnPc1	nájezdník
z	z	k7c2	z
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
na	na	k7c6	na
řeckém	řecký	k2eAgNnSc6d1	řecké
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
meče	meč	k1gInSc2	meč
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tito	tento	k3xDgMnPc1	tento
nájezdníci	nájezdník	k1gMnPc1	nájezdník
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
bojovou	bojový	k2eAgFnSc7d1	bojová
taktikou	taktika	k1gFnSc7	taktika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
nejen	nejen	k6eAd1	nejen
bodání	bodání	k1gNnSc4	bodání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sekání	sekání	k1gNnSc1	sekání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bronzové	bronzový	k2eAgInPc1d1	bronzový
meče	meč	k1gInPc1	meč
byly	být	k5eAaImAgInP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
drahé	drahý	k2eAgInPc1d1	drahý
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojena	spojit	k5eAaPmNgFnS	spojit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgFnPc3d1	silná
zbraním	zbraň	k1gFnPc3	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
zbraní	zbraň	k1gFnPc2	zbraň
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgNnPc1d1	obtížné
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
bylo	být	k5eAaImAgNnS	být
připojení	připojení	k1gNnSc1	připojení
trnu	trn	k1gInSc2	trn
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
zadního	zadní	k2eAgInSc2d1	zadní
konce	konec	k1gInSc2	konec
čepele	čepel	k1gFnSc2	čepel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
rukojetí	rukojeť	k1gFnPc2	rukojeť
<g/>
)	)	kIx)	)
k	k	k7c3	k
prstenci	prstenec	k1gInSc3	prstenec
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Konstrukce	konstrukce	k1gFnPc4	konstrukce
bronzových	bronzový	k2eAgInPc2d1	bronzový
mečů	meč	k1gInPc2	meč
====	====	k?	====
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnPc1d1	původní
měděné	měděný	k2eAgFnPc1d1	měděná
dýky	dýka	k1gFnPc1	dýka
měly	mít	k5eAaImAgFnP	mít
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
čepele	čepel	k1gFnSc2	čepel
vloženou	vložený	k2eAgFnSc4d1	vložená
do	do	k7c2	do
rukojeti	rukojeť	k1gFnSc2	rukojeť
a	a	k8xC	a
spojenu	spojen	k2eAgFnSc4d1	spojena
několika	několik	k4yIc7	několik
malými	malý	k2eAgInPc7d1	malý
nýty	nýt	k1gInPc7	nýt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
zbraní	zbraň	k1gFnPc2	zbraň
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
pnutí	pnutí	k1gNnSc3	pnutí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sloužily	sloužit	k5eAaImAgFnP	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
bodání	bodání	k1gNnSc3	bodání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
má	mít	k5eAaImIp3nS	mít
všestrannější	všestranný	k2eAgNnSc4d2	všestrannější
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
výrazně	výrazně	k6eAd1	výrazně
komplikovanější	komplikovaný	k2eAgFnSc3d2	komplikovanější
konstrukci	konstrukce	k1gFnSc3	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
takovýmto	takovýto	k3xDgInSc7	takovýto
mečem	meč	k1gInSc7	meč
lze	lze	k6eAd1	lze
dělat	dělat	k5eAaImF	dělat
více	hodně	k6eAd2	hodně
pohybů	pohyb	k1gInPc2	pohyb
a	a	k8xC	a
původní	původní	k2eAgNnSc4d1	původní
řešení	řešení	k1gNnSc4	řešení
je	být	k5eAaImIp3nS	být
nedostačující	dostačující	k2eNgNnSc1d1	nedostačující
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
dospěly	dochvít	k5eAaPmAgFnP	dochvít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
trnu	trn	k1gInSc6	trn
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
speciální	speciální	k2eAgFnSc1d1	speciální
drážka	drážka	k1gFnSc1	drážka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
horní	horní	k2eAgInSc1d1	horní
konec	konec	k1gInSc1	konec
čepele	čepel	k1gInSc2	čepel
(	(	kIx(	(
<g/>
rameno	rameno	k1gNnSc4	rameno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
držely	držet	k5eAaImAgFnP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
pomocí	pomocí	k7c2	pomocí
silnějších	silný	k2eAgInPc2d2	silnější
nýtů	nýt	k1gInPc2	nýt
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
řešením	řešení	k1gNnSc7	řešení
bylo	být	k5eAaImAgNnS	být
odlévat	odlévat	k5eAaImF	odlévat
čepel	čepel	k1gFnSc4	čepel
společně	společně	k6eAd1	společně
s	s	k7c7	s
trnem	trn	k1gInSc7	trn
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
mělo	mít	k5eAaImAgNnS	mít
několik	několik	k4yIc4	několik
nevýhod	nevýhoda	k1gFnPc2	nevýhoda
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc7d3	veliký
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
vytvořit	vytvořit	k5eAaPmF	vytvořit
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
či	či	k8xC	či
kostěné	kostěný	k2eAgNnSc4d1	kostěné
držadlo	držadlo	k1gNnSc4	držadlo
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
řešení	řešení	k1gNnSc1	řešení
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nestabilní	stabilní	k2eNgNnSc1d1	nestabilní
a	a	k8xC	a
držadlo	držadlo	k1gNnSc1	držadlo
rukojeti	rukojeť	k1gFnSc2	rukojeť
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvolňovalo	uvolňovat	k5eAaImAgNnS	uvolňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ideálním	ideální	k2eAgNnSc7d1	ideální
řešením	řešení	k1gNnSc7	řešení
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
odlévat	odlévat	k5eAaImF	odlévat
celou	celý	k2eAgFnSc4d1	celá
zbraň	zbraň	k1gFnSc4	zbraň
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
takové	takový	k3xDgNnSc1	takový
odlévání	odlévání	k1gNnSc1	odlévání
přesahovalo	přesahovat	k5eAaImAgNnS	přesahovat
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
technologické	technologický	k2eAgFnPc4d1	technologická
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Halštatu	Halštat	k1gInSc6	Halštat
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
využívat	využívat	k5eAaPmF	využívat
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgNnSc1d1	drahé
a	a	k8xC	a
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mečů	meč	k1gInPc2	meč
využívat	využívat	k5eAaImF	využívat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
byly	být	k5eAaImAgInP	být
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
bronzové	bronzový	k2eAgInPc1d1	bronzový
meče	meč	k1gInPc1	meč
kvalitnější	kvalitní	k2eAgFnSc2d2	kvalitnější
než	než	k8xS	než
železné	železný	k2eAgFnSc2d1	železná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
kovářství	kovářství	k1gNnSc2	kovářství
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
prosadily	prosadit	k5eAaPmAgFnP	prosadit
výhody	výhoda	k1gFnPc4	výhoda
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přechodovém	přechodový	k2eAgNnSc6d1	přechodové
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
objevují	objevovat	k5eAaImIp3nP	objevovat
železné	železný	k2eAgFnPc1d1	železná
čepele	čepel	k1gFnPc1	čepel
s	s	k7c7	s
bronzovými	bronzový	k2eAgInPc7d1	bronzový
jílci	jílec	k1gInPc7	jílec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
mečů	meč	k1gInPc2	meč
byly	být	k5eAaImAgFnP	být
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
používány	používat	k5eAaImNgInP	používat
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
mezi	mezi	k7c7	mezi
litím	lití	k1gNnSc7	lití
a	a	k8xC	a
kováním	kování	k1gNnSc7	kování
si	se	k3xPyFc3	se
vynutila	vynutit	k5eAaPmAgFnS	vynutit
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
využitím	využití	k1gNnSc7	využití
železa	železo	k1gNnSc2	železo
přišli	přijít	k5eAaPmAgMnP	přijít
Chetité	Chetita	k1gMnPc1	Chetita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ty	k3xPp2nSc3	ty
ho	on	k3xPp3gNnSc4	on
ještě	ještě	k9	ještě
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
promítnout	promítnout	k5eAaPmF	promítnout
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začali	začít	k5eAaPmAgMnP	začít
železné	železný	k2eAgInPc4d1	železný
meče	meč	k1gInPc4	meč
systematicky	systematicky	k6eAd1	systematicky
využívat	využívat	k5eAaPmF	využívat
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Asyřany	Asyřan	k1gMnPc4	Asyřan
bylo	být	k5eAaImAgNnS	být
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
nalezišť	naleziště	k1gNnPc2	naleziště
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohli	moct	k5eAaImAgMnP	moct
vyzbrojit	vyzbrojit	k5eAaPmF	vyzbrojit
značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
bojovníků	bojovník	k1gMnPc2	bojovník
relativně	relativně	k6eAd1	relativně
levně	levně	k6eAd1	levně
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
vojenské	vojenský	k2eAgInPc1d1	vojenský
úspěchy	úspěch	k1gInPc1	úspěch
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
rozšíření	rozšíření	k1gNnSc3	rozšíření
železných	železný	k2eAgInPc2d1	železný
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
rozvoji	rozvoj	k1gInSc3	rozvoj
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
mečů	meč	k1gInPc2	meč
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
meče	meč	k1gInPc4	meč
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
přesahující	přesahující	k2eAgInSc4d1	přesahující
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
touto	tento	k3xDgFnSc7	tento
dobou	doba	k1gFnSc7	doba
pronikl	proniknout	k5eAaPmAgInS	proniknout
meč	meč	k1gInSc1	meč
i	i	k9	i
do	do	k7c2	do
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
symboliky	symbolika	k1gFnSc2	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
základní	základní	k2eAgFnPc1d1	základní
techniky	technika	k1gFnPc1	technika
výroby	výroba	k1gFnSc2	výroba
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
ocel	ocel	k1gFnSc1	ocel
byla	být	k5eAaImAgFnS	být
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
meče	meč	k1gInPc1	meč
neměly	mít	k5eNaImAgInP	mít
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
často	často	k6eAd1	často
měnil	měnit	k5eAaImAgInS	měnit
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
proporce	proporce	k1gFnSc1	proporce
a	a	k8xC	a
vyváženost	vyváženost	k1gFnSc1	vyváženost
meče	meč	k1gInSc2	meč
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
meče	meč	k1gInPc1	meč
zdokonalovaly	zdokonalovat	k5eAaImAgInP	zdokonalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znalosti	znalost	k1gFnPc1	znalost
o	o	k7c6	o
problémech	problém	k1gInPc6	problém
byly	být	k5eAaImAgFnP	být
objevovány	objevovat	k5eAaImNgFnP	objevovat
lokálně	lokálně	k6eAd1	lokálně
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
nálezů	nález	k1gInPc2	nález
mečů	meč	k1gInPc2	meč
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Keltské	keltský	k2eAgInPc4d1	keltský
a	a	k8xC	a
germánské	germánský	k2eAgInPc4d1	germánský
meče	meč	k1gInPc4	meč
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
laténské	laténský	k2eAgFnSc2d1	laténská
zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
Keltové	Kelt	k1gMnPc1	Kelt
kovářské	kovářský	k2eAgFnSc2d1	Kovářská
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
první	první	k4xOgFnSc1	první
dělba	dělba	k1gFnSc1	dělba
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zvýšit	zvýšit	k5eAaPmF	zvýšit
jejich	jejich	k3xOp3gFnSc4	jejich
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
výroby	výroba	k1gFnSc2	výroba
mečů	meč	k1gInPc2	meč
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zbraně	zbraň	k1gFnPc1	zbraň
byly	být	k5eAaImAgFnP	být
sice	sice	k8xC	sice
relativně	relativně	k6eAd1	relativně
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
byla	být	k5eAaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
měkkost	měkkost	k1gFnSc1	měkkost
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
se	se	k3xPyFc4	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
ohýbaly	ohýbat	k5eAaImAgFnP	ohýbat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
rovnat	rovnat	k5eAaImF	rovnat
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
využívali	využívat	k5eAaImAgMnP	využívat
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
výraznou	výrazný	k2eAgFnSc7d1	výrazná
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
opakovaných	opakovaný	k2eAgInPc6d1	opakovaný
úderech	úder	k1gInPc6	úder
nekvalitně	kvalitně	k6eNd1	kvalitně
přinýtované	přinýtovaný	k2eAgFnPc1d1	přinýtovaná
rukojeti	rukojeť	k1gFnPc1	rukojeť
uvolňovaly	uvolňovat	k5eAaImAgFnP	uvolňovat
a	a	k8xC	a
na	na	k7c6	na
trnu	trn	k1gInSc6	trn
protáčely	protáčet	k5eAaImAgFnP	protáčet
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
keltských	keltský	k2eAgInPc2d1	keltský
mečů	meč	k1gInPc2	meč
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
70	[number]	k4	70
a	a	k8xC	a
90	[number]	k4	90
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
první	první	k4xOgFnPc1	první
výrobní	výrobní	k2eAgFnPc1d1	výrobní
značky	značka	k1gFnPc1	značka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejen	nejen	k6eAd1	nejen
sloužily	sloužit	k5eAaImAgInP	sloužit
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
meč	meč	k1gInSc1	meč
patřil	patřit	k5eAaImAgInS	patřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
upustilo	upustit	k5eAaPmAgNnS	upustit
od	od	k7c2	od
zdobení	zdobení	k1gNnSc2	zdobení
a	a	k8xC	a
meč	meč	k1gInSc1	meč
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
podřízen	podřídit	k5eAaPmNgInS	podřídit
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
jako	jako	k8xC	jako
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Keltští	keltský	k2eAgMnPc1d1	keltský
a	a	k8xC	a
germánští	germánský	k2eAgMnPc1d1	germánský
válečníci	válečník	k1gMnPc1	válečník
byli	být	k5eAaImAgMnP	být
pohřbíváni	pohřbívat	k5eAaImNgMnP	pohřbívat
s	s	k7c7	s
meči	meč	k1gInPc7	meč
se	s	k7c7	s
stočenou	stočený	k2eAgFnSc7d1	stočená
čepelí	čepel	k1gFnSc7	čepel
<g/>
,	,	kIx,	,
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
meče	meč	k1gInPc4	meč
speciálně	speciálně	k6eAd1	speciálně
určené	určený	k2eAgInPc4d1	určený
do	do	k7c2	do
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
běžný	běžný	k2eAgInSc4d1	běžný
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
meče	meč	k1gInPc1	meč
v	v	k7c6	v
hrobech	hrob	k1gInPc6	hrob
měly	mít	k5eAaImAgFnP	mít
bojovníkovi	bojovník	k1gMnSc3	bojovník
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xC	jako
zbraň	zbraň	k1gFnSc4	zbraň
na	na	k7c6	na
onom	onen	k3xDgInSc6	onen
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
laténské	laténský	k2eAgFnSc6d1	laténská
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
upustilo	upustit	k5eAaPmAgNnS	upustit
od	od	k7c2	od
výroby	výroba	k1gFnSc2	výroba
bronzových	bronzový	k2eAgInPc2d1	bronzový
mečů	meč	k1gInPc2	meč
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
bronz	bronz	k1gInSc1	bronz
vzácně	vzácně	k6eAd1	vzácně
používán	používat	k5eAaImNgInS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
jílec	jílec	k1gInSc4	jílec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
našich	náš	k3xOp1gFnPc2	náš
vědomostí	vědomost	k1gFnPc2	vědomost
o	o	k7c6	o
výzbroji	výzbroj	k1gFnSc6	výzbroj
z	z	k7c2	z
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
naleziště	naleziště	k1gNnSc4	naleziště
železných	železný	k2eAgFnPc2d1	železná
zbraní	zbraň	k1gFnPc2	zbraň
ve	v	k7c4	v
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
La	la	k1gNnSc6	la
Tene	Ten	k1gFnSc2	Ten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Římské	římský	k2eAgInPc1d1	římský
meče	meč	k1gInPc1	meč
===	===	k?	===
</s>
</p>
<p>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
a	a	k8xC	a
v	v	k7c6	v
systému	systém	k1gInSc6	systém
využívání	využívání	k1gNnSc2	využívání
mečů	meč	k1gInPc2	meč
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
nejen	nejen	k6eAd1	nejen
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
rozlišováno	rozlišován	k2eAgNnSc4d1	rozlišováno
i	i	k9	i
jejich	jejich	k3xOp3gNnSc4	jejich
užití	užití	k1gNnSc4	užití
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vyrábět	vyrábět	k5eAaImF	vyrábět
jiné	jiný	k2eAgInPc4d1	jiný
meče	meč	k1gInPc4	meč
pro	pro	k7c4	pro
pěchotu	pěchota	k1gFnSc4	pěchota
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
pro	pro	k7c4	pro
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
pěchota	pěchota	k1gFnSc1	pěchota
využívala	využívat	k5eAaPmAgFnS	využívat
meče	meč	k1gInSc2	meč
gladius	gladius	k1gInSc1	gladius
<g/>
.	.	kIx.	.
</s>
<s>
Jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
mívalo	mívat	k5eAaImAgNnS	mívat
meče	meč	k1gInPc4	meč
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
od	od	k7c2	od
pasu	pas	k1gInSc2	pas
dolů	dolů	k6eAd1	dolů
zavěšenou	zavěšený	k2eAgFnSc4d1	zavěšená
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
spathu	spatha	k1gFnSc4	spatha
a	a	k8xC	a
vodorovně	vodorovně	k6eAd1	vodorovně
před	před	k7c7	před
tělem	tělo	k1gNnSc7	tělo
krátký	krátký	k2eAgInSc1d1	krátký
scramasax	scramasax	k1gInSc1	scramasax
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
použití	použití	k1gNnSc3	použití
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
dýky	dýka	k1gFnSc2	dýka
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
přizpůsobené	přizpůsobený	k2eAgInPc1d1	přizpůsobený
meče	meč	k1gInPc1	meč
tvořily	tvořit	k5eAaImAgFnP	tvořit
z	z	k7c2	z
římských	římský	k2eAgNnPc2d1	římské
vojsk	vojsko	k1gNnPc2	vojsko
nejlépe	dobře	k6eAd3	dobře
vyzbrojené	vyzbrojený	k2eAgInPc1d1	vyzbrojený
vojenské	vojenský	k2eAgInPc1d1	vojenský
oddíly	oddíl	k1gInPc1	oddíl
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
i	i	k8xC	i
způsob	způsob	k1gInSc1	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
dělení	dělení	k1gNnSc2	dělení
<g/>
,	,	kIx,	,
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
ovládání	ovládání	k1gNnSc2	ovládání
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
Frankové	Frank	k1gMnPc1	Frank
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
Germáni	Germán	k1gMnPc1	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
používání	používání	k1gNnSc1	používání
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
dominantní	dominantní	k2eAgNnSc1d1	dominantní
až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
Říma	Řím	k1gInSc2	Řím
dokázali	dokázat	k5eAaPmAgMnP	dokázat
navíc	navíc	k6eAd1	navíc
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
meče	meč	k1gInPc4	meč
obouruční	obouruční	k2eAgInPc4d1	obouruční
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svoji	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
projevily	projevit	k5eAaPmAgFnP	projevit
především	především	k9	především
v	v	k7c6	v
jezdectvu	jezdectvo	k1gNnSc6	jezdectvo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgFnSc1d1	efektivní
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
taktika	taktika	k1gFnSc1	taktika
sekání	sekání	k1gNnSc2	sekání
namísto	namísto	k7c2	namísto
bodání	bodání	k1gNnSc2	bodání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vikingské	vikingský	k2eAgInPc1d1	vikingský
meče	meč	k1gInPc1	meč
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
do	do	k7c2	do
cca	cca	kA	cca
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začal	začít	k5eAaPmAgMnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
nový	nový	k2eAgMnSc1d1	nový
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
boje	boj	k1gInSc2	boj
přelomový	přelomový	k2eAgInSc4d1	přelomový
typ	typ	k1gInSc4	typ
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
novým	nový	k2eAgNnSc7d1	nové
pojetím	pojetí	k1gNnSc7	pojetí
mečů	meč	k1gInPc2	meč
přišli	přijít	k5eAaPmAgMnP	přijít
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hrotu	hrot	k1gInSc3	hrot
zužovaly	zužovat	k5eAaImAgInP	zužovat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
snížilo	snížit	k5eAaPmAgNnS	snížit
hmotnost	hmotnost	k1gFnSc4	hmotnost
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
byla	být	k5eAaImAgFnS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
obloukovitě	obloukovitě	k6eAd1	obloukovitě
zkosenou	zkosený	k2eAgFnSc7d1	zkosená
špicí	špice	k1gFnSc7	špice
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
byly	být	k5eAaImAgInP	být
převážně	převážně	k6eAd1	převážně
sečnou	sečna	k1gFnSc7	sečna
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnější	vhodný	k2eAgInSc1d2	vhodnější
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
snížená	snížený	k2eAgFnSc1d1	snížená
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1,5	[number]	k4	1,5
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
usnadnily	usnadnit	k5eAaPmAgFnP	usnadnit
ovládání	ovládání	k1gNnSc4	ovládání
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
tvaru	tvar	k1gInSc6	tvar
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
víceméně	víceméně	k9	víceméně
ustálil	ustálit	k5eAaPmAgInS	ustálit
<g/>
,	,	kIx,	,
čepel	čepel	k1gFnSc1	čepel
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
měnila	měnit	k5eAaImAgFnS	měnit
pouze	pouze	k6eAd1	pouze
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
mohutnost	mohutnost	k1gFnSc4	mohutnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
tvaru	tvar	k1gInSc2	tvar
docházelo	docházet	k5eAaImAgNnS	docházet
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
takové	takový	k3xDgInPc1	takový
meče	meč	k1gInPc1	meč
neujaly	ujmout	k5eNaPmAgInP	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
meče	meč	k1gInSc2	meč
však	však	k9	však
nadále	nadále	k6eAd1	nadále
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
se	se	k3xPyFc4	se
jílec	jílec	k1gInSc1	jílec
<g/>
,	,	kIx,	,
záštita	záštita	k1gFnSc1	záštita
a	a	k8xC	a
hlavice	hlavice	k1gFnSc1	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
záštity	záštita	k1gFnSc2	záštita
a	a	k8xC	a
hlavice	hlavice	k1gFnSc2	hlavice
lze	lze	k6eAd1	lze
meče	meč	k1gInSc2	meč
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
24	[number]	k4	24
–	–	k?	–
26	[number]	k4	26
skupin	skupina	k1gFnPc2	skupina
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výroba	výroba	k1gFnSc1	výroba
meče	meč	k1gInSc2	meč
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
drahou	drahý	k2eAgFnSc7d1	drahá
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
do	do	k7c2	do
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
převažovaly	převažovat	k5eAaImAgInP	převažovat
jízdní	jízdní	k2eAgInPc1d1	jízdní
meče	meč	k1gInPc1	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meče	meč	k1gInPc1	meč
vikingské	vikingský	k2eAgFnSc2d1	vikingská
éry	éra	k1gFnSc2	éra
(	(	kIx(	(
<g/>
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
čepele	čepel	k1gFnSc2	čepel
klasifikují	klasifikovat	k5eAaImIp3nP	klasifikovat
většinou	většina	k1gFnSc7	většina
podle	podle	k7c2	podle
Petersenovy	Petersenův	k2eAgFnSc2d1	Petersenova
typologie	typologie	k1gFnSc2	typologie
(	(	kIx(	(
<g/>
26	[number]	k4	26
typů	typ	k1gInPc2	typ
v	v	k7c6	v
9	[number]	k4	9
hlavních	hlavní	k2eAgFnPc6d1	hlavní
kategoriích	kategorie	k1gFnPc6	kategorie
I	i	k9	i
až	až	k9	až
IX	IX	kA	IX
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klasifikaci	klasifikace	k1gFnSc4	klasifikace
mečů	meč	k1gInPc2	meč
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
většinou	většina	k1gFnSc7	většina
Oakeshottova	Oakeshottův	k2eAgFnSc1d1	Oakeshottův
typologie	typologie	k1gFnSc1	typologie
(	(	kIx(	(
<g/>
24	[number]	k4	24
typů	typ	k1gInPc2	typ
čepelí	čepel	k1gFnPc2	čepel
v	v	k7c6	v
13	[number]	k4	13
kategoriích	kategorie	k1gFnPc6	kategorie
X	X	kA	X
až	až	k8xS	až
XXII	XXII	kA	XXII
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
klasifikace	klasifikace	k1gFnSc2	klasifikace
mečů	meč	k1gInPc2	meč
je	být	k5eAaImIp3nS	být
klasifikace	klasifikace	k1gFnSc1	klasifikace
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
jílce	jílec	k1gInSc2	jílec
<g/>
,	,	kIx,	,
záštity	záštita	k1gFnSc2	záštita
a	a	k8xC	a
hlavice	hlavice	k1gFnSc2	hlavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
záštita	záštita	k1gFnSc1	záštita
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
exempláře	exemplář	k1gInPc1	exemplář
se	s	k7c7	s
záštitou	záštita	k1gFnSc7	záštita
přes	přes	k7c4	přes
25	[number]	k4	25
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
začala	začít	k5eAaPmAgFnS	začít
chránit	chránit	k5eAaImF	chránit
celé	celý	k2eAgNnSc4d1	celé
předloktí	předloktí	k1gNnSc4	předloktí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
další	další	k2eAgNnSc1d1	další
vylepšení	vylepšení	k1gNnSc1	vylepšení
a	a	k8xC	a
záštita	záštita	k1gFnSc1	záštita
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hrotu	hrot	k1gInSc3	hrot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgNnSc4d2	lepší
krytí	krytí	k1gNnSc4	krytí
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
první	první	k4xOgInSc4	první
náznak	náznak	k1gInSc4	náznak
snahy	snaha	k1gFnSc2	snaha
chránit	chránit	k5eAaImF	chránit
prst	prst	k1gInSc4	prst
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
naznačila	naznačit	k5eAaPmAgFnS	naznačit
pozdější	pozdní	k2eAgInSc4d2	pozdější
vývoj	vývoj	k1gInSc4	vývoj
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
koši	koš	k1gInSc3	koš
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
našel	najít	k5eAaPmAgInS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
především	především	k6eAd1	především
u	u	k7c2	u
kordu	kord	k1gInSc2	kord
<g/>
.	.	kIx.	.
</s>
<s>
Meče	meč	k1gInPc1	meč
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
v	v	k7c4	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
románské	románský	k2eAgInPc4d1	románský
meče	meč	k1gInPc4	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zlepšení	zlepšení	k1gNnSc3	zlepšení
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
nutně	nutně	k6eAd1	nutně
musely	muset	k5eAaImAgFnP	muset
reagovat	reagovat	k5eAaBmF	reagovat
i	i	k9	i
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
proražení	proražení	k1gNnSc6	proražení
zlepšené	zlepšený	k2eAgFnSc2d1	zlepšená
zbroje	zbroj	k1gFnSc2	zbroj
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
vynaložit	vynaložit	k5eAaPmF	vynaložit
více	hodně	k6eAd2	hodně
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používaly	používat	k5eAaImAgFnP	používat
oběma	dva	k4xCgFnPc7	dva
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
dvouruční	dvouruční	k2eAgInPc4d1	dvouruční
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
protože	protože	k8xS	protože
je	on	k3xPp3gFnPc4	on
používala	používat	k5eAaImAgFnS	používat
jízda	jízda	k1gFnSc1	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
už	už	k6eAd1	už
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
lze	lze	k6eAd1	lze
na	na	k7c6	na
iluminacích	iluminace	k1gFnPc6	iluminace
najít	najít	k5eAaPmF	najít
u	u	k7c2	u
jednoručních	jednoruční	k2eAgInPc2d1	jednoruční
mečů	meč	k1gInPc2	meč
i	i	k8xC	i
(	(	kIx(	(
<g/>
riskantní	riskantní	k2eAgMnSc1d1	riskantní
<g/>
)	)	kIx)	)
držení	držení	k1gNnSc1	držení
s	s	k7c7	s
ukazovákem	ukazovák	k1gInSc7	ukazovák
před	před	k7c7	před
záštitou	záštita	k1gFnSc7	záštita
umožňující	umožňující	k2eAgNnSc1d1	umožňující
lepší	dobrý	k2eAgNnSc1d2	lepší
ovládání	ovládání	k1gNnSc1	ovládání
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
kordu	kord	k1gInSc2	kord
nebo	nebo	k8xC	nebo
rapíru	rapír	k1gInSc2	rapír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
čepel	čepel	k1gFnSc1	čepel
meče	meč	k1gInSc2	meč
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
a	a	k8xC	a
zúžila	zúžit	k5eAaPmAgFnS	zúžit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
meč	meč	k1gInSc1	meč
dostal	dostat	k5eAaPmAgInS	dostat
charakter	charakter	k1gInSc4	charakter
bodné	bodný	k2eAgFnSc2d1	bodná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
delšími	dlouhý	k2eAgInPc7d2	delší
meči	meč	k1gInPc7	meč
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
problém	problém	k1gInSc1	problém
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
vyváženost	vyváženost	k1gFnSc1	vyváženost
meče	meč	k1gInSc2	meč
<g/>
,	,	kIx,	,
posun	posun	k1gInSc1	posun
těžiště	těžiště	k1gNnSc2	těžiště
se	se	k3xPyFc4	se
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
pomocí	pomocí	k7c2	pomocí
hlavice	hlavice	k1gFnSc2	hlavice
umístěné	umístěný	k2eAgFnSc2d1	umístěná
na	na	k7c4	na
jílce	jílec	k1gInPc4	jílec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
měla	mít	k5eAaImAgNnP	mít
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejčastěji	často	k6eAd3	často
tvar	tvar	k1gInSc4	tvar
paraořechu	paraořech	k1gInSc2	paraořech
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
století	století	k1gNnSc6	století
převažovala	převažovat	k5eAaImAgFnS	převažovat
zpravidla	zpravidla	k6eAd1	zpravidla
hlavice	hlavice	k1gFnSc1	hlavice
mincového	mincový	k2eAgInSc2d1	mincový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
bodné	bodný	k2eAgInPc1d1	bodný
a	a	k8xC	a
sečné	sečný	k2eAgInPc1d1	sečný
meče	meč	k1gInPc1	meč
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oddělily	oddělit	k5eAaPmAgFnP	oddělit
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
mečů	meč	k1gInPc2	meč
vypadal	vypadat	k5eAaImAgInS	vypadat
diametrálně	diametrálně	k6eAd1	diametrálně
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
rytíř	rytíř	k1gMnSc1	rytíř
sebou	se	k3xPyFc7	se
vozil	vozit	k5eAaImAgInS	vozit
oba	dva	k4xCgInPc4	dva
meče	meč	k1gInPc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vývoje	vývoj	k1gInSc2	vývoj
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
i	i	k8xC	i
poměrně	poměrně	k6eAd1	poměrně
rychlý	rychlý	k2eAgInSc4d1	rychlý
vývoj	vývoj	k1gInSc4	vývoj
bodných	bodný	k2eAgInPc2d1	bodný
mečů	meč	k1gInPc2	meč
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
kordu	kord	k1gInSc3	kord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
bastard	bastard	k1gMnSc1	bastard
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
vlastně	vlastně	k9	vlastně
mečem	meč	k1gInSc7	meč
určeným	určený	k2eAgInSc7d1	určený
pro	pro	k7c4	pro
pěší	pěší	k2eAgNnSc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
mečů	meč	k1gInPc2	meč
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
od	od	k7c2	od
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hovoříme	hovořit	k5eAaImIp1nP	hovořit
jako	jako	k9	jako
o	o	k7c6	o
gotických	gotický	k2eAgInPc6d1	gotický
mečích	meč	k1gInPc6	meč
</s>
</p>
<p>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
pěší	pěší	k2eAgInPc1d1	pěší
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
širokou	široký	k2eAgFnSc4d1	široká
čepel	čepel	k1gFnSc4	čepel
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
jílec	jílec	k1gInSc4	jílec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
meče	meč	k1gInSc2	meč
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
dvouruční	dvouruční	k2eAgInPc1d1	dvouruční
meče	meč	k1gInPc1	meč
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jízdních	jízdní	k2eAgInPc2d1	jízdní
mečů	meč	k1gInPc2	meč
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
extrémnímu	extrémní	k2eAgNnSc3d1	extrémní
prodloužení	prodloužení	k1gNnSc3	prodloužení
jílce	jílec	k1gInSc2	jílec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
meče	meč	k1gInSc2	meč
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
palné	palný	k2eAgFnPc1d1	palná
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yRgMnPc3	který
byl	být	k5eAaImAgInS	být
meč	meč	k1gInSc4	meč
neúčinnou	účinný	k2eNgFnSc7d1	neúčinná
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
meč	meč	k1gInSc1	meč
ztratil	ztratit	k5eAaPmAgInS	ztratit
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
svoji	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc4d1	poslední
zbraň	zbraň	k1gFnSc4	zbraň
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
muže	muž	k1gMnSc2	muž
proti	proti	k7c3	proti
muži	muž	k1gMnSc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existovaly	existovat	k5eAaImAgFnP	existovat
různé	různý	k2eAgInPc4d1	různý
ceremoniální	ceremoniální	k2eAgInPc4d1	ceremoniální
<g/>
,	,	kIx,	,
popravčí	popravčí	k2eAgInPc4d1	popravčí
nebo	nebo	k8xC	nebo
soubojové	soubojový	k2eAgInPc4d1	soubojový
meče	meč	k1gInPc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
nošeným	nošený	k2eAgInSc7d1	nošený
mečem	meč	k1gInSc7	meč
byl	být	k5eAaImAgInS	být
rapír	rapír	k1gInSc1	rapír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nosili	nosit	k5eAaImAgMnP	nosit
španělští	španělský	k2eAgMnPc1d1	španělský
šlechtici	šlechtic	k1gMnPc1	šlechtic
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
nahrazen	nahradit	k5eAaPmNgInS	nahradit
kordem	kord	k1gInSc7	kord
nebo	nebo	k8xC	nebo
šavlí	šavle	k1gFnSc7	šavle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
převažuje	převažovat	k5eAaImIp3nS	převažovat
symbolická	symbolický	k2eAgFnSc1d1	symbolická
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
soubojová	soubojový	k2eAgFnSc1d1	soubojová
funkce	funkce	k1gFnSc1	funkce
meče	meč	k1gInSc2	meč
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
už	už	k6eAd1	už
existují	existovat	k5eAaImIp3nP	existovat
a	a	k8xC	a
někde	někde	k6eAd1	někde
i	i	k9	i
převažují	převažovat	k5eAaImIp3nP	převažovat
souboje	souboj	k1gInPc1	souboj
s	s	k7c7	s
pistolí	pistol	k1gFnSc7	pistol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
meč	meč	k1gInSc1	meč
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
takto	takto	k6eAd1	takto
vystavených	vystavený	k2eAgInPc2d1	vystavený
mečů	meč	k1gInPc2	meč
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
mečů	meč	k1gInPc2	meč
vystavovaných	vystavovaný	k2eAgInPc2d1	vystavovaný
na	na	k7c6	na
hradech	hrad	k1gInPc6	hrad
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nefunkční	funkční	k2eNgFnPc1d1	nefunkční
kopie	kopie	k1gFnPc1	kopie
dvouručních	dvouruční	k2eAgInPc2d1	dvouruční
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
často	často	k6eAd1	často
využíval	využívat	k5eAaImAgInS	využívat
k	k	k7c3	k
ceremoniálním	ceremoniální	k2eAgInPc3d1	ceremoniální
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgInS	být
odznakem	odznak	k1gInSc7	odznak
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kruzích	kruh	k1gInPc6	kruh
stále	stále	k6eAd1	stále
existovaly	existovat	k5eAaImAgInP	existovat
souboje	souboj	k1gInPc1	souboj
kordem	kord	k1gInSc7	kord
nebo	nebo	k8xC	nebo
šavlí	šavle	k1gFnSc7	šavle
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
meče	meč	k1gInPc4	meč
postupně	postupně	k6eAd1	postupně
oslabována	oslabovat	k5eAaImNgFnS	oslabovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
a	a	k8xC	a
odvozené	odvozený	k2eAgFnPc1d1	odvozená
zbraně	zbraň	k1gFnPc1	zbraň
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
šermířských	šermířský	k2eAgInPc6d1	šermířský
kroužcích	kroužek	k1gInPc6	kroužek
či	či	k8xC	či
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
larpů	larp	k1gInPc2	larp
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
virtuální	virtuální	k2eAgFnSc6d1	virtuální
podobě	podoba	k1gFnSc6	podoba
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
počítačových	počítačový	k2eAgFnPc6d1	počítačová
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
si	se	k3xPyFc3	se
meč	meč	k1gInSc4	meč
(	(	kIx(	(
<g/>
kord	kord	k1gInSc1	kord
<g/>
,	,	kIx,	,
šavle	šavle	k1gFnSc1	šavle
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
podržely	podržet	k5eAaPmAgFnP	podržet
svoji	svůj	k3xOyFgFnSc4	svůj
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ceremoniální	ceremoniální	k2eAgFnSc4d1	ceremoniální
nebo	nebo	k8xC	nebo
rituální	rituální	k2eAgFnSc4d1	rituální
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
mečů	meč	k1gInPc2	meč
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
začaly	začít	k5eAaPmAgInP	začít
meč	meč	k1gInSc4	meč
používat	používat	k5eAaImF	používat
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
ojedinělé	ojedinělý	k2eAgNnSc4d1	ojedinělé
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
těžké	těžký	k2eAgInPc4d1	těžký
dvousečné	dvousečný	k2eAgInPc4d1	dvousečný
meče	meč	k1gInPc4	meč
západního	západní	k2eAgInSc2d1	západní
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
měly	mít	k5eAaImAgInP	mít
záštitu	záštita	k1gFnSc4	záštita
a	a	k8xC	a
vícedílnou	vícedílný	k2eAgFnSc7d1	vícedílná
hlavicí	hlavice	k1gFnSc7	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
byly	být	k5eAaImAgInP	být
dováženy	dovážen	k2eAgInPc4d1	dovážen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
na	na	k7c4	na
území	území	k1gNnSc4	území
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
opravdu	opravdu	k6eAd1	opravdu
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
objevuje	objevovat	k5eAaImIp3nS	objevovat
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
dominantní	dominantní	k2eAgFnSc7d1	dominantní
zbraní	zbraň	k1gFnSc7	zbraň
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
válečná	válečný	k2eAgFnSc1d1	válečná
sekera	sekera	k1gFnSc1	sekera
<g/>
.	.	kIx.	.
</s>
<s>
Meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
záštitu	záštita	k1gFnSc4	záštita
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
90	[number]	k4	90
až	až	k9	až
105	[number]	k4	105
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
mezi	mezi	k7c7	mezi
5,5	[number]	k4	5,5
až	až	k9	až
6	[number]	k4	6
centimetry	centimetr	k1gInPc4	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Jílec	jílec	k1gInSc1	jílec
těchto	tento	k3xDgInPc2	tento
mečů	meč	k1gInPc2	meč
má	mít	k5eAaImIp3nS	mít
hlavici	hlavice	k1gFnSc4	hlavice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vyvažovat	vyvažovat	k5eAaImF	vyvažovat
meč	meč	k1gInSc4	meč
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
nebyly	být	k5eNaImAgFnP	být
dobře	dobře	k6eAd1	dobře
vyváženy	vyvážen	k2eAgFnPc1d1	vyvážena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přestože	přestože	k8xS	přestože
měly	mít	k5eAaImAgInP	mít
hlavici	hlavice	k1gFnSc4	hlavice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
čepel	čepel	k1gFnSc1	čepel
příliš	příliš	k6eAd1	příliš
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čepeli	čepel	k1gFnSc6	čepel
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
žlábek	žlábek	k1gInSc1	žlábek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
ostrým	ostrý	k2eAgInSc7d1	ostrý
špičatým	špičatý	k2eAgInSc7d1	špičatý
hrotem	hrot	k1gInSc7	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
zapomenutou	zapomenutý	k2eAgFnSc7d1	zapomenutá
technologií	technologie	k1gFnSc7	technologie
svářkový	svářkový	k2eAgInSc1d1	svářkový
damask	damask	k1gInSc1	damask
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
čeští	český	k2eAgMnPc1d1	český
kováři	kovář	k1gMnPc1	kovář
neovládali	ovládat	k5eNaImAgMnP	ovládat
ani	ani	k8xC	ani
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
meče	meč	k1gInPc4	meč
mimo	mimo	k7c4	mimo
vší	všecek	k3xTgFnSc6	všecek
pochybnost	pochybnost	k1gFnSc1	pochybnost
dovážené	dovážený	k2eAgFnSc2d1	dovážená
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
meče	meč	k1gInPc4	meč
vyrobené	vyrobený	k2eAgInPc4d1	vyrobený
českými	český	k2eAgMnPc7d1	český
kováři	kovář	k1gMnPc7	kovář
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
měly	mít	k5eAaImAgInP	mít
horší	zlý	k2eAgFnSc4d2	horší
kvalitu	kvalita	k1gFnSc4	kvalita
než	než	k8xS	než
dovážené	dovážený	k2eAgInPc4d1	dovážený
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
levnější	levný	k2eAgFnPc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
meče	meč	k1gInSc2	meč
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
až	až	k6eAd1	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nahradil	nahradit	k5eAaPmAgInS	nahradit
meč	meč	k1gInSc1	meč
vymyšlený	vymyšlený	k2eAgInSc1d1	vymyšlený
na	na	k7c4	na
území	území	k1gNnSc4	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
záštitu	záštita	k1gFnSc4	záštita
ohnutou	ohnutý	k2eAgFnSc4d1	ohnutá
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hrotu	hrot	k1gInSc3	hrot
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
délka	délka	k1gFnSc1	délka
přesahovala	přesahovat	k5eAaImAgFnS	přesahovat
20	[number]	k4	20
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
románské	románský	k2eAgInPc1d1	románský
meče	meč	k1gInPc1	meč
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
používaly	používat	k5eAaImAgFnP	používat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ačkoli	ačkoli	k8xS	ačkoli
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikal	pronikat	k5eAaImAgMnS	pronikat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
i	i	k9	i
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
nový	nový	k2eAgInSc4d1	nový
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
gotický	gotický	k2eAgInSc4d1	gotický
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
vůbec	vůbec	k9	vůbec
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
běžně	běžně	k6eAd1	běžně
používaný	používaný	k2eAgInSc4d1	používaný
krátký	krátký	k2eAgInSc4d1	krátký
meč	meč	k1gInSc4	meč
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
špicí	špice	k1gFnSc7	špice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
gotické	gotický	k2eAgInPc1d1	gotický
meče	meč	k1gInPc1	meč
používané	používaný	k2eAgInPc1d1	používaný
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
neměly	mít	k5eNaImAgFnP	mít
žádná	žádný	k3yNgNnPc4	žádný
česká	český	k2eAgNnPc4d1	české
specifika	specifikon	k1gNnPc4	specifikon
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
o	o	k7c4	o
meče	meč	k1gInPc4	meč
dovážené	dovážený	k2eAgInPc4d1	dovážený
komplet	komplet	k1gInSc4	komplet
<g/>
,	,	kIx,	,
tak	tak	k9	tak
o	o	k7c4	o
meče	meč	k1gInPc4	meč
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
zde	zde	k6eAd1	zde
z	z	k7c2	z
dovezené	dovezený	k2eAgFnSc2d1	dovezená
čepele	čepel	k1gFnSc2	čepel
(	(	kIx(	(
<g/>
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
případ	případ	k1gInSc1	případ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
meče	meč	k1gInPc4	meč
zcela	zcela	k6eAd1	zcela
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
i	i	k9	i
výroba	výroba	k1gFnSc1	výroba
mečů	meč	k1gInPc2	meč
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
replik	replika	k1gFnPc2	replika
dvouručních	dvouruční	k2eAgInPc2d1	dvouruční
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k8xC	jako
výzdoba	výzdoba	k1gFnSc1	výzdoba
mnoha	mnoho	k4c2	mnoho
domů	dům	k1gInPc2	dům
či	či	k8xC	či
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
řada	řada	k1gFnSc1	řada
ceremoniálních	ceremoniální	k2eAgInPc2d1	ceremoniální
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
spolky	spolek	k1gInPc4	spolek
atp.	atp.	kA	atp.
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
vynikaly	vynikat	k5eAaImAgInP	vynikat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
německými	německý	k2eAgInPc7d1	německý
meči	meč	k1gInPc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
nebyly	být	k5eNaImAgInP	být
zhotovovány	zhotovovat	k5eAaImNgInP	zhotovovat
jako	jako	k8xS	jako
funkční	funkční	k2eAgInPc1d1	funkční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
ozdobné	ozdobný	k2eAgInPc1d1	ozdobný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
mečů	meč	k1gInPc2	meč
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Železné	železný	k2eAgInPc1d1	železný
meče	meč	k1gInPc1	meč
===	===	k?	===
</s>
</p>
<p>
<s>
Železo	železo	k1gNnSc1	železo
a	a	k8xC	a
ocel	ocel	k1gFnSc1	ocel
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
mečů	meč	k1gInPc2	meč
se	se	k3xPyFc4	se
před	před	k7c7	před
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
získávaly	získávat	k5eAaImAgFnP	získávat
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
přímou	přímý	k2eAgFnSc7d1	přímá
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vytavením	vytavení	k1gNnSc7	vytavení
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
stálého	stálý	k2eAgNnSc2d1	stálé
dmýchání	dmýchání	k1gNnSc2	dmýchání
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
tavenina	tavenina	k1gFnSc1	tavenina
tzv.	tzv.	kA	tzv.
koláč	koláč	k1gInSc4	koláč
byla	být	k5eAaImAgFnS	být
co	co	k9	co
do	do	k7c2	do
obsahu	obsah	k1gInSc2	obsah
uhlíku	uhlík	k1gInSc2	uhlík
značně	značně	k6eAd1	značně
nehomogenní	homogenní	k2eNgFnSc1d1	nehomogenní
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
strusky	struska	k1gFnSc2	struska
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ji	on	k3xPp3gFnSc4	on
potřeba	potřeba	k6eAd1	potřeba
vyčistit	vyčistit	k5eAaPmF	vyčistit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
štípala	štípat	k5eAaImAgFnS	štípat
na	na	k7c4	na
menší	malý	k2eAgInPc4d2	menší
kusy	kus	k1gInPc4	kus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
kovářsky	kovářsky	k6eAd1	kovářsky
svařily	svařit	k5eAaPmAgFnP	svařit
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
ingot	ingot	k1gInSc1	ingot
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
rozštípal	rozštípat	k5eAaPmAgMnS	rozštípat
na	na	k7c4	na
menší	malý	k2eAgInPc4d2	menší
kusy	kus	k1gInPc4	kus
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
kovářsky	kovářsky	k6eAd1	kovářsky
svařily	svařit	k5eAaPmAgFnP	svařit
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
vrstvy	vrstva	k1gFnPc4	vrstva
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
materiál	materiál	k1gInSc4	materiál
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
jako	jako	k9	jako
svářkové	svářkový	k2eAgNnSc1d1	svářkové
železo	železo	k1gNnSc1	železo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
toto	tento	k3xDgNnSc4	tento
zpracování	zpracování	k1gNnSc4	zpracování
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
řezu	řez	k1gInSc6	řez
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
starých	starý	k2eAgInPc2d1	starý
železných	železný	k2eAgInPc2d1	železný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
ocelí	ocel	k1gFnSc7	ocel
a	a	k8xC	a
železem	železo	k1gNnSc7	železo
určovala	určovat	k5eAaImAgFnS	určovat
schopnost	schopnost	k1gFnSc4	schopnost
kalení	kalení	k1gNnSc2	kalení
<g/>
.	.	kIx.	.
</s>
<s>
Kalitelný	kalitelný	k2eAgInSc1d1	kalitelný
materiál	materiál	k1gInSc1	materiál
byl	být	k5eAaImAgInS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ocel	ocel	k1gFnSc4	ocel
<g/>
,	,	kIx,	,
nekalitelný	kalitelný	k2eNgInSc4d1	kalitelný
za	za	k7c4	za
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svářková	svářkový	k2eAgFnSc1d1	svářková
ocel	ocel	k1gFnSc1	ocel
se	se	k3xPyFc4	se
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
zopakováním	zopakování	k1gNnSc7	zopakování
postupu	postup	k1gInSc2	postup
přímé	přímý	k2eAgFnSc2d1	přímá
tavby	tavba	k1gFnSc2	tavba
na	na	k7c4	na
již	již	k6eAd1	již
zpracované	zpracovaný	k2eAgNnSc4d1	zpracované
svářkové	svářkový	k2eAgNnSc4d1	svářkové
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kalitelný	kalitelný	k2eAgInSc4d1	kalitelný
materiál	materiál	k1gInSc4	materiál
-	-	kIx~	-
ocel	ocel	k1gFnSc4	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
výhoda	výhoda	k1gFnSc1	výhoda
tavby	tavba	k1gFnSc2	tavba
přímou	přímý	k2eAgFnSc7d1	přímá
metodou	metoda	k1gFnSc7	metoda
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
možnosti	možnost	k1gFnSc6	možnost
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
–	–	k?	–
i	i	k8xC	i
nekvalitní	kvalitní	k2eNgFnSc4d1	nekvalitní
–	–	k?	–
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čepel	čepel	k1gFnSc1	čepel
meče	meč	k1gInSc2	meč
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
kovala	kovat	k5eAaImAgFnS	kovat
z	z	k7c2	z
kombinace	kombinace	k1gFnSc2	kombinace
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavním	hlavní	k2eAgNnSc7d1	hlavní
kritériem	kritérion	k1gNnSc7	kritérion
byla	být	k5eAaImAgFnS	být
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
dostupnost	dostupnost	k1gFnSc1	dostupnost
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
celoocelové	celoocelový	k2eAgInPc1d1	celoocelový
homogenní	homogenní	k2eAgInPc1d1	homogenní
meče	meč	k1gInPc1	meč
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	jenž	k3xRgFnSc4	jenž
ocel	ocel	k1gFnSc4	ocel
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nálezů	nález	k1gInPc2	nález
máme	mít	k5eAaImIp1nP	mít
doložené	doložená	k1gFnSc6	doložená
tyto	tento	k3xDgFnPc4	tento
konstrukce	konstrukce	k1gFnPc4	konstrukce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čepel	čepel	k1gInSc1	čepel
s	s	k7c7	s
navařeným	navařený	k2eAgNnSc7d1	navařené
ostřím	ostří	k1gNnSc7	ostří
</s>
</p>
<p>
<s>
Čepel	čepel	k1gFnSc1	čepel
byla	být	k5eAaImAgFnS	být
vykována	vykovat	k5eAaPmNgFnS	vykovat
z	z	k7c2	z
měkkého	měkký	k2eAgNnSc2d1	měkké
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
kovářsky	kovářsky	k6eAd1	kovářsky
navařeno	navařen	k2eAgNnSc1d1	navařeno
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
ocelové	ocelový	k2eAgNnSc1d1	ocelové
ostří	ostří	k1gNnSc1	ostří
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
čepele	čepel	k1gFnSc2	čepel
byl	být	k5eAaImAgInS	být
vůbec	vůbec	k9	vůbec
nejoblíbenější	oblíbený	k2eAgNnSc1d3	nejoblíbenější
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čepel	čepel	k1gInSc1	čepel
s	s	k7c7	s
měkkým	měkký	k2eAgNnSc7d1	měkké
jádrem	jádro	k1gNnSc7	jádro
</s>
</p>
<p>
<s>
Čepel	čepel	k1gFnSc1	čepel
byla	být	k5eAaImAgFnS	být
vykována	vykovat	k5eAaPmNgFnS	vykovat
z	z	k7c2	z
měkkého	měkký	k2eAgNnSc2d1	měkké
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
zakováno	zakovat	k5eAaPmNgNnS	zakovat
do	do	k7c2	do
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sendvičová	sendvičový	k2eAgFnSc1d1	sendvičová
čepel	čepel	k1gFnSc1	čepel
</s>
</p>
<p>
<s>
Čepel	čepel	k1gInSc1	čepel
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ocelové	ocelový	k2eAgNnSc4d1	ocelové
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
plochých	plochý	k2eAgFnPc6d1	plochá
stranách	strana	k1gFnPc6	strana
zesíleno	zesílit	k5eAaPmNgNnS	zesílit
navařenými	navařený	k2eAgInPc7d1	navařený
železnými	železný	k2eAgInPc7d1	železný
pásy	pás	k1gInPc7	pás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protodamask	Protodamask	k1gInSc1	Protodamask
</s>
</p>
<p>
<s>
Čepel	čepel	k1gInSc1	čepel
má	mít	k5eAaImIp3nS	mít
jádro	jádro	k1gNnSc1	jádro
vykováno	vykovat	k5eAaPmNgNnS	vykovat
ze	z	k7c2	z
svařených	svařený	k2eAgInPc2d1	svařený
železných	železný	k2eAgInPc2d1	železný
a	a	k8xC	a
ocelových	ocelový	k2eAgInPc2d1	ocelový
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jádře	jádro	k1gNnSc6	jádro
jsou	být	k5eAaImIp3nP	být
navařeny	navařit	k5eAaBmNgInP	navařit
ocelové	ocelový	k2eAgInPc1d1	ocelový
břity	břit	k1gInPc1	břit
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
lehce	lehko	k6eAd1	lehko
naleptávalo	naleptávat	k5eAaImAgNnS	naleptávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
struktura	struktura	k1gFnSc1	struktura
protodamasku	protodamasek	k1gInSc2	protodamasek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svářkový	svářkový	k2eAgInSc1d1	svářkový
damask	damask	k1gInSc1	damask
</s>
</p>
<p>
<s>
Jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
vykováno	vykovat	k5eAaPmNgNnS	vykovat
z	z	k7c2	z
damascénské	damascénský	k2eAgFnSc2d1	damascénská
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svařených	svařený	k2eAgFnPc2d1	svařená
a	a	k8xC	a
mnohokrát	mnohokrát	k6eAd1	mnohokrát
překládaných	překládaný	k2eAgInPc2d1	překládaný
pásů	pás	k1gInPc2	pás
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
složitější	složitý	k2eAgFnSc4d2	složitější
strukturu	struktura	k1gFnSc4	struktura
než	než	k8xS	než
u	u	k7c2	u
protodamasku	protodamasek	k1gInSc2	protodamasek
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
lehce	lehko	k6eAd1	lehko
naleptáno	naleptat	k5eAaPmNgNnS	naleptat
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
struktura	struktura	k1gFnSc1	struktura
damasku	damasek	k1gInSc2	damasek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
čepelí	čepel	k1gFnSc7	čepel
byl	být	k5eAaImAgInS	být
oblíben	oblíbit	k5eAaPmNgInS	oblíbit
zejména	zejména	k9	zejména
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
upouští	upouštět	k5eAaImIp3nS	upouštět
<g/>
,	,	kIx,	,
snad	snad	k9	snad
protože	protože	k8xS	protože
mechanické	mechanický	k2eAgFnPc1d1	mechanická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
byly	být	k5eAaImAgFnP	být
srovnatelné	srovnatelný	k2eAgInPc4d1	srovnatelný
z	z	k7c2	z
konstrukčně	konstrukčně	k6eAd1	konstrukčně
jednodušší	jednoduchý	k2eAgFnSc7d2	jednodušší
čepelí	čepel	k1gFnSc7	čepel
s	s	k7c7	s
navařeným	navařený	k2eAgNnSc7d1	navařené
ostřím	ostří	k1gNnSc7	ostří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdobení	zdobení	k1gNnPc2	zdobení
mečů	meč	k1gInPc2	meč
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
vikinských	vikinský	k2eAgInPc2d1	vikinský
a	a	k8xC	a
franských	franský	k2eAgInPc2d1	franský
mečů	meč	k1gInPc2	meč
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
hlavicemi	hlavice	k1gFnPc7	hlavice
a	a	k8xC	a
záštitami	záštita	k1gFnPc7	záštita
zdobenými	zdobený	k2eAgInPc7d1	zdobený
rytím	rytí	k1gNnSc7	rytí
<g/>
,	,	kIx,	,
rýhováním	rýhování	k1gNnSc7	rýhování
<g/>
,	,	kIx,	,
či	či	k8xC	či
taušírováním	taušírování	k1gNnSc7	taušírování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevují	objevovat	k5eAaImIp3nP	objevovat
velmi	velmi	k6eAd1	velmi
zdobené	zdobený	k2eAgInPc1d1	zdobený
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
válečná	válečný	k2eAgFnSc1d1	válečná
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
diskutabilní	diskutabilní	k2eAgInSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
zdobených	zdobený	k2eAgInPc2d1	zdobený
funkčních	funkční	k2eAgInPc2d1	funkční
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Rukojeti	rukojeť	k1gFnPc1	rukojeť
se	se	k3xPyFc4	se
zdobily	zdobit	k5eAaImAgFnP	zdobit
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
ornamentů	ornament	k1gInPc2	ornament
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vykládaly	vykládat	k5eAaImAgInP	vykládat
vzácnými	vzácný	k2eAgInPc7d1	vzácný
kovy	kov	k1gInPc7	kov
nebo	nebo	k8xC	nebo
perlami	perla	k1gFnPc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavici	hlavice	k1gFnSc6	hlavice
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
zákruty	zákruta	k1gFnPc4	zákruta
a	a	k8xC	a
zlacené	zlacený	k2eAgNnSc4d1	zlacené
zdobení	zdobení	k1gNnSc4	zdobení
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
bývala	bývat	k5eAaImAgFnS	bývat
zdobena	zdobit	k5eAaImNgFnS	zdobit
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
rytí	rytí	k1gNnSc1	rytí
nemělo	mít	k5eNaImAgNnS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věcný	věcný	k2eAgMnSc1d1	věcný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
název	název	k1gInSc4	název
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
erb	erb	k1gInSc1	erb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
výsekem	výsek	k1gInSc7	výsek
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
osazen	osadit	k5eAaPmNgInS	osadit
zlatě	zlatě	k6eAd1	zlatě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ceny	cena	k1gFnPc4	cena
mečů	meč	k1gInPc2	meč
==	==	k?	==
</s>
</p>
<p>
<s>
Ceny	cena	k1gFnPc1	cena
nově	nově	k6eAd1	nově
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
mečů	meč	k1gInPc2	meč
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
individuální	individuální	k2eAgInPc1d1	individuální
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
především	především	k9	především
na	na	k7c4	na
určení	určení	k1gNnSc4	určení
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
dekorativními	dekorativní	k2eAgInPc7d1	dekorativní
meči	meč	k1gInPc7	meč
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
kopiemi	kopie	k1gFnPc7	kopie
kopií	kopie	k1gFnSc7	kopie
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
repliky	replika	k1gFnPc4	replika
dělané	dělaný	k2eAgFnPc4d1	dělaná
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
zpravidla	zpravidla	k6eAd1	zpravidla
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
věrnou	věrný	k2eAgFnSc4d1	věrná
repliku	replika	k1gFnSc4	replika
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
mečů	meč	k1gInPc2	meč
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
unikátních	unikátní	k2eAgInPc2d1	unikátní
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
neprodejné	prodejný	k2eNgInPc1d1	neprodejný
<g/>
,	,	kIx,	,
nejvzácnější	vzácný	k2eAgInPc1d3	nejvzácnější
japonské	japonský	k2eAgInPc1d1	japonský
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
prakticky	prakticky	k6eAd1	prakticky
neobjevují	objevovat	k5eNaImIp3nP	objevovat
a	a	k8xC	a
lze	lze	k6eAd1	lze
sehnat	sehnat	k5eAaPmF	sehnat
pouze	pouze	k6eAd1	pouze
novodobé	novodobý	k2eAgFnPc4d1	novodobá
kopie	kopie	k1gFnPc4	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
druhem	druh	k1gInSc7	druh
nyní	nyní	k6eAd1	nyní
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
meče	meč	k1gInPc4	meč
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
členy	člen	k1gInPc4	člen
šermířských	šermířský	k2eAgFnPc2d1	šermířská
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
historický	historický	k2eAgInSc1d1	historický
šerm	šerm	k1gInSc1	šerm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
různé	různý	k2eAgFnPc1d1	různá
larpy	larpa	k1gFnPc1	larpa
atp.	atp.	kA	atp.
Cena	cena	k1gFnSc1	cena
takovýchto	takovýto	k3xDgInPc2	takovýto
mečů	meč	k1gInPc2	meč
je	být	k5eAaImIp3nS	být
určována	určovat	k5eAaImNgFnS	určovat
kvalitou	kvalita	k1gFnSc7	kvalita
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
deset	deset	k4xCc1	deset
tisíc	tisíc	k4xCgInPc2	tisíc
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ceny	cena	k1gFnPc4	cena
nejběžnějších	běžný	k2eAgInPc2d3	nejběžnější
sběratelských	sběratelský	k2eAgInPc2d1	sběratelský
mečů	meč	k1gInPc2	meč
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Meče	meč	k1gInPc1	meč
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
sběratelské	sběratelský	k2eAgFnSc2d1	sběratelská
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
jako	jako	k8xC	jako
takové	takový	k3xDgInPc4	takový
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
pravidel	pravidlo	k1gNnPc2	pravidlo
oceňovány	oceňován	k2eAgFnPc1d1	oceňována
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
jednotlivého	jednotlivý	k2eAgInSc2d1	jednotlivý
typu	typ	k1gInSc2	typ
meče	meč	k1gInSc2	meč
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnSc6	jeho
sběratelské	sběratelský	k2eAgFnSc6d1	sběratelská
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstaršími	starý	k2eAgNnPc7d3	nejstarší
<g/>
,	,	kIx,	,
víceméně	víceméně	k9	víceméně
běžně	běžně	k6eAd1	běžně
sehnatelnými	sehnatelný	k2eAgInPc7d1	sehnatelný
typy	typ	k1gInPc7	typ
mečů	meč	k1gInPc2	meč
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
gotických	gotický	k2eAgInPc2d1	gotický
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
takovýchto	takovýto	k3xDgInPc2	takovýto
kompletních	kompletní	k2eAgInPc2d1	kompletní
mečů	meč	k1gInPc2	meč
začíná	začínat	k5eAaImIp3nS	začínat
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
30	[number]	k4	30
a	a	k8xC	a
40	[number]	k4	40
tisíci	tisíc	k4xCgInPc7	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dostávají	dostávat	k5eAaImIp3nP	dostávat
pouze	pouze	k6eAd1	pouze
meče	meč	k1gInPc1	meč
poškozené	poškozený	k2eAgInPc1d1	poškozený
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
velmi	velmi	k6eAd1	velmi
zkorodované	zkorodovaný	k2eAgNnSc1d1	zkorodované
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nepoškozených	poškozený	k2eNgInPc2d1	nepoškozený
exemplářů	exemplář	k1gInPc2	exemplář
bývá	bývat	k5eAaImIp3nS	bývat
cena	cena	k1gFnSc1	cena
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sběratelsky	sběratelsky	k6eAd1	sběratelsky
běžně	běžně	k6eAd1	běžně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
ceremoniální	ceremoniální	k2eAgInPc1d1	ceremoniální
meče	meč	k1gInPc1	meč
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
těchto	tento	k3xDgInPc2	tento
mečů	meč	k1gInPc2	meč
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
běžně	běžně	k6eAd1	běžně
koupit	koupit	k5eAaPmF	koupit
ve	v	k7c6	v
starožitnictví	starožitnictví	k1gNnSc6	starožitnictví
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
takovýchto	takovýto	k3xDgInPc2	takovýto
mečů	meč	k1gInPc2	meč
začíná	začínat	k5eAaImIp3nS	začínat
kolem	kolem	k7c2	kolem
čtyř	čtyři	k4xCgInPc2	čtyři
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
cenu	cena	k1gFnSc4	cena
deseti	deset	k4xCc2	deset
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
lze	lze	k6eAd1	lze
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
sehnat	sehnat	k5eAaPmF	sehnat
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgFnPc4d1	přesná
kopie	kopie	k1gFnPc4	kopie
zpravidla	zpravidla	k6eAd1	zpravidla
dvouručních	dvouruční	k2eAgInPc2d1	dvouruční
mečů	meč	k1gInPc2	meč
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k8xC	jako
výzdoba	výzdoba	k1gFnSc1	výzdoba
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
jako	jako	k9	jako
nefunkční	funkční	k2eNgFnPc1d1	nefunkční
kopie	kopie	k1gFnPc1	kopie
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
dekorativní	dekorativní	k2eAgFnSc4d1	dekorativní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nejsou	být	k5eNaImIp3nP	být
z	z	k7c2	z
dostatečně	dostatečně	k6eAd1	dostatečně
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
provedení	provedení	k1gNnSc1	provedení
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
používány	používat	k5eAaImNgFnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
laika	laik	k1gMnSc4	laik
jsou	být	k5eAaImIp3nP	být
nerozeznatelné	rozeznatelný	k2eNgInPc1d1	nerozeznatelný
od	od	k7c2	od
pravých	pravý	k2eAgInPc2d1	pravý
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
mnoha	mnoho	k4c2	mnoho
exemplářů	exemplář	k1gInPc2	exemplář
několikrát	několikrát	k6eAd1	několikrát
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
těchto	tento	k3xDgFnPc2	tento
kopií	kopie	k1gFnPc2	kopie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
prodávány	prodávat	k5eAaImNgInP	prodávat
jako	jako	k8xS	jako
pravé	pravý	k2eAgInPc1d1	pravý
meče	meč	k1gInPc1	meč
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k8xS	až
cen	cena	k1gFnPc2	cena
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
reálná	reálný	k2eAgFnSc1d1	reálná
cena	cena	k1gFnSc1	cena
však	však	k9	však
výjimečně	výjimečně	k6eAd1	výjimečně
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvzácnějšími	vzácný	k2eAgNnPc7d3	nejvzácnější
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
relativně	relativně	k6eAd1	relativně
sehnatelnými	sehnatelný	k2eAgInPc7d1	sehnatelný
meči	meč	k1gInPc7	meč
jsou	být	k5eAaImIp3nP	být
meče	meč	k1gInSc2	meč
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
kompletního	kompletní	k2eAgInSc2d1	kompletní
nepoškozeného	poškozený	k2eNgInSc2d1	nepoškozený
meče	meč	k1gInSc2	meč
s	s	k7c7	s
pochvou	pochva	k1gFnSc7	pochva
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
typy	typ	k1gInPc1	typ
mečů	meč	k1gInPc2	meč
lze	lze	k6eAd1	lze
koupit	koupit	k5eAaPmF	koupit
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
v	v	k7c6	v
dražbě	dražba	k1gFnSc6	dražba
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
individuální	individuální	k2eAgInPc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
prodat	prodat	k5eAaPmF	prodat
tyto	tento	k3xDgInPc1	tento
vzácné	vzácný	k2eAgInPc1d1	vzácný
exempláře	exemplář	k1gInPc1	exemplář
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavné	slavný	k2eAgInPc1d1	slavný
meče	meč	k1gInPc1	meč
==	==	k?	==
</s>
</p>
<p>
<s>
Především	především	k9	především
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
meč	meč	k1gInSc1	meč
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
pro	pro	k7c4	pro
rytíře	rytíř	k1gMnSc4	rytíř
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
cenný	cenný	k2eAgInSc4d1	cenný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
býval	bývat	k5eAaImAgMnS	bývat
knězem	kněz	k1gMnSc7	kněz
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
-	-	kIx~	-
zasvěcen	zasvětit	k5eAaPmNgMnS	zasvětit
patronovi	patron	k1gMnSc3	patron
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc1	jméno
dali	dát	k5eAaPmAgMnP	dát
sami	sám	k3xTgMnPc1	sám
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jednak	jednak	k8xC	jednak
skutečné	skutečný	k2eAgInPc1d1	skutečný
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
činy	čin	k1gInPc1	čin
se	se	k3xPyFc4	se
zveličily	zveličit	k5eAaPmAgInP	zveličit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
smyšlené	smyšlený	k2eAgInPc1d1	smyšlený
meče	meč	k1gInPc1	meč
mýtických	mýtický	k2eAgFnPc2d1	mýtická
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neexistovaly	existovat	k5eNaImAgFnP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obojí	obojí	k4xRgInPc1	obojí
meče	meč	k1gInPc1	meč
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
legendami	legenda	k1gFnPc7	legenda
pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
bojového	bojový	k2eAgNnSc2d1	bojové
odhodlání	odhodlání	k1gNnSc2	odhodlání
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Lgendární	Lgendární	k2eAgInPc1d1	Lgendární
meče	meč	k1gInPc1	meč
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
o	o	k7c6	o
hrdinských	hrdinský	k2eAgInPc6d1	hrdinský
činech	čin	k1gInPc6	čin
(	(	kIx(	(
<g/>
chansons	chansons	k1gInSc1	chansons
des	des	k1gNnSc1	des
gestes	gestes	k1gInSc1	gestes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eposech	epos	k1gInPc6	epos
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
literatuře	literatura	k1gFnSc6	literatura
objevují	objevovat	k5eAaImIp3nP	objevovat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
měl	mít	k5eAaImAgInS	mít
rytířský	rytířský	k2eAgInSc1d1	rytířský
román	román	k1gInSc1	román
již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Almace	Almace	k1gFnSc1	Almace
<g/>
,	,	kIx,	,
Almice	Almice	k1gFnSc1	Almice
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Jana	Jana	k1gFnSc1	Jana
Turpina	Turpina	k1gFnSc1	Turpina
</s>
</p>
<p>
<s>
Andúril	Andúril	k1gInSc1	Andúril
<g/>
,	,	kIx,	,
smyšlený	smyšlený	k2eAgInSc1d1	smyšlený
meč	meč	k1gInSc1	meč
z	z	k7c2	z
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
Aragorn	Aragorn	k1gInSc4	Aragorn
</s>
</p>
<p>
<s>
Anglachel	Anglachel	k1gMnSc1	Anglachel
<g/>
,	,	kIx,	,
Gurthang	Gurthang	k1gMnSc1	Gurthang
-	-	kIx~	-
černý	černý	k2eAgInSc1d1	černý
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Túrina	Túrin	k2eAgMnSc2d1	Túrin
Turambara	Turambar	k1gMnSc2	Turambar
</s>
</p>
<p>
<s>
Balmung	Balmung	k1gMnSc1	Balmung
<g/>
,	,	kIx,	,
meč	mečet	k5eAaImRp2nS	mečet
Siegfrieda	Siegfrieda	k1gMnSc1	Siegfrieda
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Nibelunzích	Nibelung	k1gMnPc6	Nibelung
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Courechouse	Courechouse	k6eAd1	Courechouse
<g/>
,	,	kIx,	,
meč	meč	k1gInSc4	meč
krále	král	k1gMnSc2	král
Bana	Banus	k1gMnSc2	Banus
</s>
</p>
<p>
<s>
Curetana	Curetana	k1gFnSc1	Curetana
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Odiera	Odiera	k1gFnSc1	Odiera
(	(	kIx(	(
<g/>
Hoger	Hoger	k1gInSc1	Hoger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Damoklův	Damoklův	k2eAgInSc1d1	Damoklův
meč	meč	k1gInSc1	meč
</s>
</p>
<p>
<s>
Davidův	Davidův	k2eAgInSc1d1	Davidův
meč	meč	k1gInSc1	meč
</s>
</p>
<p>
<s>
Durandal	Durandat	k5eAaPmAgMnS	Durandat
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Rolanda	Rolanda	k1gFnSc1	Rolanda
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Rolandovi	Roland	k1gMnSc6	Roland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dyrnywyn	Dyrnywyn	k1gInSc1	Dyrnywyn
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Haela	Haela	k1gFnSc1	Haela
(	(	kIx(	(
<g/>
Rhyderch	Rhyderch	k1gInSc1	Rhyderch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eckesachs	Eckesachs	k6eAd1	Eckesachs
<g/>
,	,	kIx,	,
meč	meč	k1gInSc4	meč
Dietricha	Dietrich	k1gMnSc2	Dietrich
z	z	k7c2	z
Bernu	Bern	k1gInSc2	Bern
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Nibelunzích	Nibelung	k1gMnPc6	Nibelung
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Excalibur	Excalibur	k1gMnSc1	Excalibur
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
</s>
</p>
<p>
<s>
Eyelander	Eyelander	k1gInSc1	Eyelander
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Tavishe	Tavish	k1gFnSc2	Tavish
DeGroota	DeGroot	k1gMnSc2	DeGroot
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Team	team	k1gInSc1	team
Fortress	Fortress	k1gInSc1	Fortress
2	[number]	k4	2
</s>
</p>
<p>
<s>
Flamberg	Flamberg	k1gInSc1	Flamberg
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Renoulda	Renoulda	k1gFnSc1	Renoulda
de	de	k?	de
Monteban	Monteban	k1gInSc1	Monteban
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
označení	označení	k1gNnSc1	označení
dvouručních	dvouruční	k2eAgInPc2d1	dvouruční
mečů	meč	k1gInPc2	meč
</s>
</p>
<p>
<s>
Glamdring	Glamdring	k1gInSc1	Glamdring
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
krále	král	k1gMnSc4	král
Gondolinu	Gondolin	k1gInSc2	Gondolin
</s>
</p>
<p>
<s>
Halteclere	Haltecler	k1gMnSc5	Haltecler
<g/>
,	,	kIx,	,
meč	mečet	k5eAaImRp2nS	mečet
Oliviera	Oliviera	k1gFnSc1	Oliviera
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Rolandovi	Roland	k1gMnSc6	Roland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Honoree	Honoree	k1gFnSc1	Honoree
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
z	z	k7c2	z
artušovských	artušovský	k2eAgFnPc2d1	artušovská
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
účel	účel	k1gInSc1	účel
i	i	k8xC	i
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
složité	složitý	k2eAgNnSc1d1	složité
</s>
</p>
<p>
<s>
Hruting	Hruting	k1gInSc1	Hruting
<g/>
,	,	kIx,	,
Beowulfův	Beowulfův	k2eAgInSc1d1	Beowulfův
meč	meč	k1gInSc1	meč
</s>
</p>
<p>
<s>
Chastiefol	Chastiefol	k1gInSc1	Chastiefol
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
</s>
</p>
<p>
<s>
Joyeuse	Joyeuse	k1gFnSc1	Joyeuse
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
královský	královský	k2eAgInSc1d1	královský
obřadní	obřadní	k2eAgInSc1d1	obřadní
meč	meč	k1gInSc1	meč
</s>
</p>
<p>
<s>
Marmyadose	Marmyadosa	k1gFnSc3	Marmyadosa
<g/>
,	,	kIx,	,
meč	meč	k1gInSc4	meč
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
grálu	grál	k1gInSc2	grál
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
z	z	k7c2	z
artušovských	artušovský	k2eAgFnPc2d1	artušovská
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
obřadní	obřadní	k2eAgFnSc7d1	obřadní
meč	meč	k1gInSc4	meč
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
mečů	meč	k1gInPc2	meč
z	z	k7c2	z
období	období	k1gNnSc2	období
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
s	s	k7c7	s
podivným	podivný	k2eAgInSc7d1	podivný
pásem	pás	k1gInSc7	pás
<g/>
,	,	kIx,	,
meč	meč	k1gInSc4	meč
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
David	David	k1gMnSc1	David
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Šalamoun	Šalamoun	k1gMnSc1	Šalamoun
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
jej	on	k3xPp3gMnSc4	on
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
čepel	čepel	k1gFnSc4	čepel
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
rytými	rytý	k2eAgFnPc7d1	rytá
značkami	značka	k1gFnPc7	značka
<g/>
;	;	kIx,	;
Habsburkové	Habsburk	k1gMnPc1	Habsburk
jej	on	k3xPp3gMnSc4	on
používali	používat	k5eAaImAgMnP	používat
jako	jako	k8xC	jako
obřadní	obřadní	k2eAgInSc1d1	obřadní
meč	meč	k1gInSc1	meč
<g/>
;	;	kIx,	;
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
narodoweho	narodoweze	k6eAd1	narodoweze
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
svatého	svatý	k2eAgMnSc2d1	svatý
Mauricia	Mauricius	k1gMnSc2	Mauricius
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc4	meč
velmistra	velmistr	k1gMnSc2	velmistr
Řádu	řád	k1gInSc2	řád
Křižovníků	křižovník	k1gMnPc2	křižovník
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
s	s	k7c7	s
devisou	devisa	k1gFnSc7	devisa
kardinála	kardinál	k1gMnSc2	kardinál
Arnošta	Arnošt	k1gMnSc2	Arnošt
Harracha	Harrach	k1gMnSc2	Harrach
–	–	k?	–
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Murgleis	Murgleis	k1gFnSc1	Murgleis
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
Gueleuna	Gueleuna	k1gFnSc1	Gueleuna
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Rolandovi	Roland	k1gMnSc6	Roland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatoštěpánský	svatoštěpánský	k2eAgInSc1d1	svatoštěpánský
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
údajný	údajný	k2eAgInSc1d1	údajný
meč	meč	k1gInSc1	meč
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
</s>
</p>
<p>
<s>
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
posledních	poslední	k2eAgFnPc2d1	poslední
metalografických	metalografický	k2eAgFnPc2d1	metalografická
analýz	analýza	k1gFnPc2	analýza
čepel	čepel	k1gFnSc1	čepel
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
autentická	autentický	k2eAgFnSc1d1	autentická
</s>
</p>
<p>
<s>
Sequence	Sequence	k1gFnSc1	Sequence
<g/>
,	,	kIx,	,
mýtický	mýtický	k2eAgInSc1d1	mýtický
meč	meč	k1gInSc1	meč
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
</s>
</p>
<p>
<s>
Szczerbiec	Szczerbiec	k1gInSc1	Szczerbiec
<g/>
,	,	kIx,	,
korunovační	korunovační	k2eAgInSc1d1	korunovační
meč	meč	k1gInSc1	meč
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
</s>
</p>
<p>
<s>
Tizon	Tizon	k1gInSc1	Tizon
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
El	Ela	k1gFnPc2	Ela
Cida	Cid	k1gInSc2	Cid
</s>
</p>
<p>
<s>
Tyrving	Tyrving	k1gInSc1	Tyrving
<g/>
,	,	kIx,	,
Angantyrsův	Angantyrsův	k2eAgInSc1d1	Angantyrsův
meč	meč	k1gInSc1	meč
</s>
</p>
<p>
<s>
Zerwikaptur	Zerwikaptura	k1gFnPc2	Zerwikaptura
<g/>
,	,	kIx,	,
meč	meč	k1gInSc1	meč
z	z	k7c2	z
románu	román	k1gInSc2	román
Henryka	Henryko	k1gNnSc2	Henryko
Sienkiewicze	Sienkiewicze	k1gFnSc2	Sienkiewicze
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečemV	mečemV	k?	mečemV
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
legendární	legendární	k2eAgInPc1d1	legendární
meče	meč	k1gInPc1	meč
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
literatuře	literatura	k1gFnSc3	literatura
<g/>
,	,	kIx,	,
pohádkách	pohádka	k1gFnPc6	pohádka
či	či	k8xC	či
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
legendu	legenda	k1gFnSc4	legenda
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnSc2	kniha
či	či	k8xC	či
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc4d3	nejznámější
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
světelné	světelný	k2eAgInPc1d1	světelný
meče	meč	k1gInPc1	meč
z	z	k7c2	z
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
či	či	k8xC	či
Aragornův	Aragornův	k2eAgInSc4d1	Aragornův
(	(	kIx(	(
<g/>
Elendilův	Elendilův	k2eAgInSc4d1	Elendilův
<g/>
)	)	kIx)	)
meč	meč	k1gInSc4	meč
Narsil	Narsil	k1gFnPc2	Narsil
z	z	k7c2	z
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
mečů	meč	k1gInPc2	meč
==	==	k?	==
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgFnSc2d1	široká
skupiny	skupina	k1gFnSc2	skupina
zbraní	zbraň	k1gFnPc2	zbraň
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
určitá	určitý	k2eAgNnPc4d1	určité
specifika	specifikon	k1gNnPc4	specifikon
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnPc1d1	základní
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
vzniku	vznik	k1gInSc2	vznik
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
vzniku	vznik	k1gInSc2	vznik
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
užití	užití	k1gNnSc1	užití
(	(	kIx(	(
<g/>
bojový	bojový	k2eAgMnSc1d1	bojový
<g/>
,	,	kIx,	,
cvičný	cvičný	k2eAgMnSc1d1	cvičný
<g/>
,	,	kIx,	,
exekutivní	exekutivní	k2eAgMnSc1d1	exekutivní
<g/>
/	/	kIx~	/
<g/>
popravčí	popravčí	k1gMnSc1	popravčí
<g/>
,	,	kIx,	,
symbolický	symbolický	k2eAgMnSc1d1	symbolický
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
technologieV	technologieV	k?	technologieV
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
evropské	evropský	k2eAgInPc4d1	evropský
typy	typ	k1gInPc4	typ
mečů	meč	k1gInPc2	meč
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
vymezovány	vymezován	k2eAgFnPc1d1	vymezována
oblastí	oblast	k1gFnSc7	oblast
vzniku	vznik	k1gInSc2	vznik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
japonské	japonský	k2eAgInPc1d1	japonský
meče	meč	k1gInPc1	meč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
hesle	heslo	k1gNnSc6	heslo
je	být	k5eAaImIp3nS	být
pojednáno	pojednat	k5eAaPmNgNnS	pojednat
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
evropských	evropský	k2eAgInPc2d1	evropský
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbolické	symbolický	k2eAgInPc4d1	symbolický
významy	význam	k1gInPc4	význam
meče	meč	k1gInSc2	meč
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
náboženský	náboženský	k2eAgMnSc1d1	náboženský
2	[number]	k4	2
<g/>
)	)	kIx)	)
vlastenecký	vlastenecký	k2eAgMnSc1d1	vlastenecký
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Meč	meč	k1gInSc1	meč
českého	český	k2eAgMnSc2d1	český
patrona	patron	k1gMnSc2	patron
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
dodává	dodávat	k5eAaImIp3nS	dodávat
sílu	síla	k1gFnSc4	síla
celému	celý	k2eAgInSc3d1	celý
českému	český	k2eAgInSc3d1	český
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
)	)	kIx)	)
ceremoniální	ceremoniální	k2eAgMnPc1d1	ceremoniální
<g/>
:	:	kIx,	:
slouží	sloužit	k5eAaImIp3nP	sloužit
panovníkovi	panovník	k1gMnSc3	panovník
nebo	nebo	k8xC	nebo
velmistrovi	velmistr	k1gMnSc3	velmistr
řádu	řád	k1gInSc2	řád
k	k	k7c3	k
pasování	pasování	k1gNnSc3	pasování
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
)	)	kIx)	)
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
kříže	kříž	k1gInSc2	kříž
<g/>
:	:	kIx,	:
rytíři	rytíř	k1gMnPc1	rytíř
se	se	k3xPyFc4	se
před	před	k7c7	před
bojem	boj	k1gInSc7	boj
modlí	modlit	k5eAaImIp3nS	modlit
k	k	k7c3	k
meči	meč	k1gInSc3	meč
zabodnutému	zabodnutý	k2eAgInSc3d1	zabodnutý
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
od	od	k7c2	od
meče	meč	k1gInSc2	meč
(	(	kIx(	(
<g/>
Mečový	mečový	k2eAgInSc1d1	mečový
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
aktérem	aktér	k1gMnSc7	aktér
reconquisty	reconquista	k1gMnSc2	reconquista
na	na	k7c6	na
Iberském	iberský	k2eAgInSc6d1	iberský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
;	;	kIx,	;
5	[number]	k4	5
<g/>
)	)	kIx)	)
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
:	:	kIx,	:
např.	např.	kA	např.
artistické	artistický	k2eAgInPc1d1	artistický
(	(	kIx(	(
<g/>
polykání	polykání	k1gNnSc2	polykání
meče	meč	k1gInSc2	meč
<g/>
)	)	kIx)	)
či	či	k8xC	či
zábavné	zábavný	k2eAgNnSc1d1	zábavné
(	(	kIx(	(
<g/>
mečový	mečový	k2eAgInSc1d1	mečový
tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mimoevropské	mimoevropský	k2eAgInPc1d1	mimoevropský
meče	meč	k1gInPc1	meč
===	===	k?	===
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Evropy	Evropa	k1gFnSc2	Evropa
měl	mít	k5eAaImAgInS	mít
meč	meč	k1gInSc1	meč
stejnou	stejný	k2eAgFnSc4d1	stejná
primární	primární	k2eAgFnSc4d1	primární
funkci	funkce	k1gFnSc4	funkce
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
technikách	technika	k1gFnPc6	technika
a	a	k8xC	a
v	v	k7c6	v
symbolickém	symbolický	k2eAgNnSc6d1	symbolické
využití	využití	k1gNnSc6	využití
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
tvarování	tvarování	k1gNnSc1	tvarování
mečů	meč	k1gInPc2	meč
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
posuzovat	posuzovat	k5eAaImF	posuzovat
týmiž	týž	k3xTgNnPc7	týž
měřítky	měřítko	k1gNnPc7	měřítko
jako	jako	k8xC	jako
meče	meč	k1gInPc4	meč
evropské	evropský	k2eAgInPc4d1	evropský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Japonské	japonský	k2eAgInPc1d1	japonský
meče	meč	k1gInPc1	meč
====	====	k?	====
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
a	a	k8xC	a
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
nejvíce	hodně	k6eAd3	hodně
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
meči	meč	k1gInSc6	meč
prokazována	prokazován	k2eAgFnSc1d1	prokazována
větší	veliký	k2eAgFnSc1d2	veliký
úcta	úcta	k1gFnSc1	úcta
než	než	k8xS	než
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
meč	meč	k1gInSc1	meč
mnoho	mnoho	k6eAd1	mnoho
symbolických	symbolický	k2eAgInPc2d1	symbolický
významů	význam	k1gInPc2	význam
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
velmi	velmi	k6eAd1	velmi
složitá	složitý	k2eAgNnPc1d1	složité
pravidla	pravidlo	k1gNnPc1	pravidlo
nejen	nejen	k6eAd1	nejen
jak	jak	k8xS	jak
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
zacházet	zacházet	k5eAaImF	zacházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
nosí	nosit	k5eAaImIp3nS	nosit
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
<g/>
,	,	kIx,	,
prohlíží	prohlížet	k5eAaImIp3nS	prohlížet
či	či	k8xC	či
jak	jak	k6eAd1	jak
a	a	k8xC	a
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
doma	doma	k6eAd1	doma
umístěn	umístit	k5eAaPmNgInS	umístit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
odznakem	odznak	k1gInSc7	odznak
válečníka	válečník	k1gMnSc2	válečník
–	–	k?	–
samuraje	samuraj	k1gMnSc2	samuraj
a	a	k8xC	a
jen	jen	k9	jen
on	on	k3xPp3gMnSc1	on
ho	on	k3xPp3gNnSc4	on
měl	mít	k5eAaImAgMnS	mít
právo	právo	k1gNnSc4	právo
nosit	nosit	k5eAaImF	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
samurajů	samuraj	k1gMnPc2	samuraj
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
udržela	udržet	k5eAaPmAgFnS	udržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zákonem	zákon	k1gInSc7	zákon
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
samurajů	samuraj	k1gMnPc2	samuraj
jako	jako	k8xS	jako
kasty	kasta	k1gFnSc2	kasta
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
nosit	nosit	k5eAaImF	nosit
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jsou	být	k5eAaImIp3nP	být
meče	meč	k1gInPc1	meč
rozděleny	rozdělen	k2eAgInPc1d1	rozdělen
podle	podle	k7c2	podle
základních	základní	k2eAgNnPc2d1	základní
období	období	k1gNnPc2	období
japonských	japonský	k2eAgFnPc2d1	japonská
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
zrovna	zrovna	k6eAd1	zrovna
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
epochou	epocha	k1gFnSc7	epocha
spojeno	spojit	k5eAaPmNgNnS	spojit
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
společné	společný	k2eAgInPc4d1	společný
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
meče	meč	k1gInPc1	meč
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
vazbu	vazba	k1gFnSc4	vazba
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
historii	historie	k1gFnSc3	historie
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
k	k	k7c3	k
nějakému	nějaký	k3yIgMnSc3	nějaký
slavnému	slavný	k2eAgMnSc3d1	slavný
válečníkovi	válečník	k1gMnSc3	válečník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
používal	používat	k5eAaImAgMnS	používat
<g/>
,	,	kIx,	,
či	či	k8xC	či
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
byl	být	k5eAaImAgInS	být
meč	meč	k1gInSc1	meč
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jím	jíst	k5eAaImIp1nS	jíst
byla	být	k5eAaImAgFnS	být
zabita	zabit	k2eAgFnSc1d1	zabita
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc1	meč
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výraz	výraz	k1gInSc1	výraz
obrovské	obrovský	k2eAgFnSc2d1	obrovská
úcty	úcta	k1gFnSc2	úcta
a	a	k8xC	a
pocty	pocta	k1gFnSc2	pocta
k	k	k7c3	k
danému	daný	k2eAgMnSc3d1	daný
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
tři	tři	k4xCgFnPc4	tři
japonské	japonský	k2eAgFnPc4d1	japonská
insignie	insignie	k1gFnPc4	insignie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
laický	laický	k2eAgInSc1d1	laický
názor	názor	k1gInSc1	názor
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
základě	základ	k1gInSc6	základ
asijských	asijský	k2eAgInPc2d1	asijský
akčních	akční	k2eAgInPc2d1	akční
filmů	film	k1gInPc2	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonské	japonský	k2eAgInPc1d1	japonský
meče	meč	k1gInPc1	meč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
katana	katan	k1gMnSc2	katan
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
mnohem	mnohem	k6eAd1	mnohem
kvalitnější	kvalitní	k2eAgInPc1d2	kvalitnější
než	než	k8xS	než
meče	meč	k1gInPc1	meč
evropské	evropský	k2eAgInPc1d1	evropský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bylo	být	k5eAaImAgNnS	být
evropské	evropský	k2eAgNnSc1d1	Evropské
zpracování	zpracování	k1gNnSc1	zpracování
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
úrovni	úroveň	k1gFnSc6	úroveň
než	než	k8xS	než
japonské	japonský	k2eAgInPc1d1	japonský
a	a	k8xC	a
evropské	evropský	k2eAgInPc1d1	evropský
meče	meč	k1gInPc1	meč
si	se	k3xPyFc3	se
s	s	k7c7	s
japonskými	japonský	k2eAgFnPc7d1	japonská
v	v	k7c6	v
ničem	nic	k3yNnSc6	nic
nezadaly	zadat	k5eNaPmAgFnP	zadat
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
o	o	k7c6	o
bojovém	bojový	k2eAgNnSc6d1	bojové
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
evropský	evropský	k2eAgInSc1d1	evropský
šerm	šerm	k1gInSc1	šerm
je	být	k5eAaImIp3nS	být
neméně	málo	k6eNd2	málo
tradiční	tradiční	k2eAgInSc1d1	tradiční
a	a	k8xC	a
efektivní	efektivní	k2eAgInSc1d1	efektivní
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
japonská	japonský	k2eAgFnSc1d1	japonská
obdoba	obdoba	k1gFnSc1	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
přizpůsoben	přizpůsoben	k2eAgMnSc1d1	přizpůsoben
daným	daný	k2eAgFnPc3d1	daná
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
určení	určení	k1gNnSc4	určení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
(	(	kIx(	(
<g/>
či	či	k8xC	či
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
odlišné	odlišný	k2eAgFnPc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Orientální	orientální	k2eAgInPc1d1	orientální
meče	meč	k1gInPc1	meč
====	====	k?	====
</s>
</p>
<p>
<s>
Orient	Orient	k1gInSc1	Orient
používal	používat	k5eAaImAgInS	používat
hlavně	hlavně	k9	hlavně
zahnuté	zahnutý	k2eAgInPc4d1	zahnutý
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
arabský	arabský	k2eAgInSc1d1	arabský
scimitar	scimitar	k1gInSc1	scimitar
<g/>
,	,	kIx,	,
perský	perský	k2eAgInSc1d1	perský
shamshir	shamshir	k1gInSc1	shamshir
<g/>
,	,	kIx,	,
turecký	turecký	k2eAgInSc1d1	turecký
yatagan	yatagan	k1gInSc1	yatagan
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Předislámské	předislámský	k2eAgInPc1d1	předislámský
íránské	íránský	k2eAgInPc1d1	íránský
meče	meč	k1gInPc1	meč
bývaly	bývat	k5eAaImAgInP	bývat
zřejmě	zřejmě	k6eAd1	zřejmě
rovné	rovný	k2eAgInPc1d1	rovný
<g/>
.	.	kIx.	.
</s>
<s>
Zahnuté	zahnutý	k2eAgFnPc1d1	zahnutá
čepele	čepel	k1gFnPc1	čepel
se	se	k3xPyFc4	se
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
turko-mongolských	turkoongolský	k2eAgFnPc2d1	turko-mongolský
šavlí	šavle	k1gFnPc2	šavle
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
zde	zde	k6eAd1	zde
však	však	k9	však
neprobíhal	probíhat	k5eNaImAgMnS	probíhat
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
zůstávaly	zůstávat	k5eAaImAgFnP	zůstávat
dlouho	dlouho	k6eAd1	dlouho
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
mírně	mírně	k6eAd1	mírně
změněné	změněný	k2eAgFnSc6d1	změněná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
velmi	velmi	k6eAd1	velmi
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
datování	datování	k1gNnSc1	datování
takovýchto	takovýto	k3xDgInPc2	takovýto
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gudara	Gudara	k1gFnSc1	Gudara
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc1	Persie
</s>
</p>
<p>
<s>
Kadara	Kadara	k1gFnSc1	Kadara
<g/>
,	,	kIx,	,
perská	perský	k2eAgFnSc1d1	perská
pěchota	pěchota	k1gFnSc1	pěchota
</s>
</p>
<p>
<s>
====	====	k?	====
Meče	meč	k1gInPc4	meč
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
regionů	region	k1gInPc2	region
Asie	Asie	k1gFnSc2	Asie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
meč	meč	k1gInSc1	meč
nestal	stát	k5eNaPmAgInS	stát
tak	tak	k6eAd1	tak
populární	populární	k2eAgFnSc7d1	populární
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
existuje	existovat	k5eAaImIp3nS	existovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
typy	typ	k1gInPc1	typ
se	se	k3xPyFc4	se
neodlišují	odlišovat	k5eNaImIp3nP	odlišovat
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
z	z	k7c2	z
evropského	evropský	k2eAgInSc2d1	evropský
pohledu	pohled	k1gInSc2	pohled
pro	pro	k7c4	pro
meč	meč	k1gInSc4	meč
netradičních	tradiční	k2eNgMnPc2d1	netradiční
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dhar	Dhar	k1gInSc1	Dhar
<g/>
,	,	kIx,	,
Barma	Barma	k1gFnSc1	Barma
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
</s>
</p>
<p>
<s>
Kampilan	Kampilan	k1gInSc1	Kampilan
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
domorodci	domorodec	k1gMnPc1	domorodec
na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
</s>
</p>
<p>
<s>
Klevang	Klevang	k1gInSc1	Klevang
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
</s>
</p>
<p>
<s>
Mandau	Mandau	k6eAd1	Mandau
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
Dajákové	Dajákové	k2eAgFnSc1d1	Dajákové
</s>
</p>
<p>
<s>
Motýlí	motýlí	k2eAgInPc4d1	motýlí
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
původ	původ	k1gInSc4	původ
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
</s>
</p>
<p>
<s>
====	====	k?	====
Indické	indický	k2eAgInPc1d1	indický
meče	meč	k1gInPc1	meč
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
indické	indický	k2eAgInPc4d1	indický
meče	meč	k1gInPc4	meč
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
kovový	kovový	k2eAgInSc4d1	kovový
jílec	jílec	k1gInSc4	jílec
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
i	i	k9	i
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c4	mnoho
tvarů	tvar	k1gInPc2	tvar
mečů	meč	k1gInPc2	meč
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivě	jednotlivě	k6eAd1	jednotlivě
typy	typ	k1gInPc1	typ
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
téměř	téměř	k6eAd1	téměř
neměnily	měnit	k5eNaImAgFnP	měnit
a	a	k8xC	a
přesnější	přesný	k2eAgFnPc1d2	přesnější
datace	datace	k1gFnPc1	datace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kusů	kus	k1gInPc2	kus
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nemožná	možný	k2eNgFnSc1d1	nemožná
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
zkombinování	zkombinování	k1gNnSc1	zkombinování
meče	meč	k1gInSc2	meč
s	s	k7c7	s
dýkou	dýka	k1gFnSc7	dýka
nebo	nebo	k8xC	nebo
s	s	k7c7	s
obrněnou	obrněný	k2eAgFnSc7d1	obrněná
rukavicí	rukavice	k1gFnSc7	rukavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dhoup	Dhoup	k1gMnSc1	Dhoup
</s>
</p>
<p>
<s>
Firandž	Firandž	k6eAd1	Firandž
</s>
</p>
<p>
<s>
Kastane	Kastat	k5eAaPmIp3nS	Kastat
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
</s>
</p>
<p>
<s>
Khanda	Khanda	k1gFnSc1	Khanda
</s>
</p>
<p>
<s>
Moplah	Moplah	k1gMnSc1	Moplah
</s>
</p>
<p>
<s>
Navaz-chani	Navazhan	k1gMnPc1	Navaz-chan
</s>
</p>
<p>
<s>
Noklang	Noklang	k1gMnSc1	Noklang
</s>
</p>
<p>
<s>
Pata	pata	k1gFnSc1	pata
</s>
</p>
<p>
<s>
Pattisa	Pattisa	k1gFnSc1	Pattisa
</s>
</p>
<p>
<s>
Ram	Ram	k?	Ram
Da	Da	k1gFnSc1	Da
<g/>
'	'	kIx"	'
<g/>
o	o	k7c6	o
</s>
</p>
<p>
<s>
Sosun	Sosun	k1gMnSc1	Sosun
pattah	pattah	k1gMnSc1	pattah
</s>
</p>
<p>
<s>
Zafar	Zafar	k1gMnSc1	Zafar
takieh	takieh	k1gMnSc1	takieh
</s>
</p>
<p>
<s>
Zulfikar	Zulfikar	k1gMnSc1	Zulfikar
</s>
</p>
<p>
<s>
====	====	k?	====
Africké	africký	k2eAgInPc1d1	africký
meče	meč	k1gInPc1	meč
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Afriku	Afrika	k1gFnSc4	Afrika
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc1	meč
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
velmi	velmi	k6eAd1	velmi
netypickou	typický	k2eNgFnSc7d1	netypická
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
meče	meč	k1gInPc1	meč
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
afrických	africký	k2eAgInPc2d1	africký
mečů	meč	k1gInPc2	meč
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopeš	Chopat	k5eAaImIp2nS	Chopat
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
moci	moc	k1gFnSc2	moc
faraa	farao	k1gNnSc2	farao
</s>
</p>
<p>
<s>
Kaskara	Kaskara	k1gFnSc1	Kaskara
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
celé	celý	k2eAgFnSc2d1	celá
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
Súdánu	Súdán	k1gInSc2	Súdán
</s>
</p>
<p>
<s>
Takouba	Takouba	k1gFnSc1	Takouba
<g/>
,	,	kIx,	,
Tuaregové	Tuareg	k1gMnPc1	Tuareg
</s>
</p>
<p>
<s>
Šotel	Šotel	k1gInSc1	Šotel
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
netradiční	tradiční	k2eNgInSc1d1	netradiční
meč	meč	k1gInSc1	meč
tvarem	tvar	k1gInSc7	tvar
podobný	podobný	k2eAgInSc1d1	podobný
kose	kose	k6eAd1	kose
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgInSc1d1	ostatní
svět	svět	k1gInSc1	svět
====	====	k?	====
</s>
</p>
<p>
<s>
Kora	Kora	k1gFnSc1	Kora
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
</s>
</p>
<p>
<s>
Paramerioun	Paramerioun	k1gInSc1	Paramerioun
<g/>
,	,	kIx,	,
Byzance	Byzanc	k1gFnPc1	Byzanc
</s>
</p>
<p>
<s>
Tao	Tao	k?	Tao
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Dao	Dao	k1gFnSc1	Dao
<g/>
,	,	kIx,	,
čínská	čínský	k2eAgFnSc1d1	čínská
šavle	šavle	k1gFnSc1	šavle
</s>
</p>
<p>
<s>
Tau-kien	Tauien	k1gInSc1	Tau-kien
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
</s>
</p>
<p>
<s>
Jian	Jian	k1gNnSc1	Jian
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
</s>
</p>
<p>
<s>
Makana	Makana	k1gFnSc1	Makana
<g/>
,	,	kIx,	,
Maquahuitl	Maquahuitl	k1gFnSc1	Maquahuitl
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
mečům	meč	k1gInPc3	meč
podobné	podobný	k2eAgFnSc2d1	podobná
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
či	či	k8xC	či
obsidiánové	obsidiánový	k2eAgFnSc2d1	obsidiánová
zbraně	zbraň	k1gFnSc2	zbraň
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Evženie	Evženie	k1gFnSc1	Evženie
Šnajdrová	Šnajdrová	k1gFnSc1	Šnajdrová
<g/>
:	:	kIx,	:
Arma	Arm	k2eAgFnSc1d1	Arma
ofensiva	ofensiva	k1gFnSc1	ofensiva
et	et	k?	et
defensiva	defensiva	k1gFnSc1	defensiva
<g/>
:	:	kIx,	:
historické	historický	k2eAgFnPc1d1	historická
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
zbroj	zbroj	k1gFnSc1	zbroj
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
NM	NM	kA	NM
Praha	Praha	k1gFnSc1	Praha
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
:	:	kIx,	:
Zbraně	zbraň	k1gFnPc1	zbraň
sečné	sečný	k2eAgFnPc1d1	sečná
a	a	k8xC	a
bodné	bodný	k2eAgFnPc1d1	bodná
<g/>
.	.	kIx.	.
</s>
<s>
Aventinum	Aventinum	k1gInSc1	Aventinum
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
vydání	vydání	k1gNnSc1	vydání
<g/>
:	:	kIx,	:
Swords	Swords	k1gInSc1	Swords
and	and	k?	and
daggers	daggers	k1gInSc1	daggers
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Wagner	Wagner	k1gMnSc1	Wagner
-	-	kIx~	-
Zoroslava	Zoroslava	k1gFnSc1	Zoroslava
Drobná	drobný	k2eAgFnSc1d1	drobná
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
:	:	kIx,	:
Kroje	kroj	k1gInPc1	kroj
<g/>
,	,	kIx,	,
zbroj	zbroj	k1gFnSc4	zbroj
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
doby	doba	k1gFnSc2	doba
předhusitské	předhusitský	k2eAgFnSc2d1	předhusitská
a	a	k8xC	a
husitské	husitský	k2eAgFnSc2d1	husitská
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1450	[number]	k4	1450
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
z	z	k7c2	z
obrazových	obrazový	k2eAgInPc2d1	obrazový
pramenů	pramen	k1gInPc2	pramen
sebral	sebrat	k5eAaPmAgMnS	sebrat
a	a	k8xC	a
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Eduard	Eduard	k1gMnSc1	Eduard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
;	;	kIx,	;
Textem	text	k1gInSc7	text
doprovodili	doprovodit	k5eAaPmAgMnP	doprovodit
Zoroslava	Zoroslav	k1gMnSc2	Zoroslav
Drobná	drobná	k1gFnSc1	drobná
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Wagner	Wagner	k1gMnSc1	Wagner
-	-	kIx~	-
Zoroslava	Zoroslava	k1gFnSc1	Zoroslava
Drobná	drobný	k2eAgFnSc1d1	drobná
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Durdík	Durdík	k1gMnSc1	Durdík
upravené	upravený	k2eAgNnSc4d1	upravené
vydání	vydání	k1gNnSc4	vydání
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
<g/>
Medieval	medieval	k1gInSc1	medieval
Costume	Costum	k1gInSc5	Costum
<g/>
,	,	kIx,	,
Armour	Armour	k1gMnSc1	Armour
and	and	k?	and
Weapons	Weapons	k1gInSc1	Weapons
(	(	kIx(	(
<g/>
Dover	Dover	k1gInSc1	Dover
Fashion	Fashion	k1gInSc1	Fashion
and	and	k?	and
Costumes	Costumes	k1gInSc1	Costumes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dýka	dýka	k1gFnSc1	dýka
</s>
</p>
<p>
<s>
Kindžál	kindžál	k1gInSc1	kindžál
</s>
</p>
<p>
<s>
Fleret	fleret	k1gInSc1	fleret
</s>
</p>
<p>
<s>
Katana	katan	k1gMnSc4	katan
</s>
</p>
<p>
<s>
Kord	kord	k1gInSc1	kord
</s>
</p>
<p>
<s>
Palaš	palaš	k1gInSc1	palaš
</s>
</p>
<p>
<s>
Šavle	šavle	k1gFnSc1	šavle
</s>
</p>
<p>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
meč	meč	k1gInSc1	meč
</s>
</p>
<p>
<s>
Mečíř	mečíř	k1gMnSc1	mečíř
</s>
</p>
<p>
<s>
Mečoun	mečoun	k1gMnSc1	mečoun
</s>
</p>
<p>
<s>
Mačeta	mačeta	k1gFnSc1	mačeta
</s>
</p>
<p>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
</s>
</p>
<p>
<s>
Šerm	šerm	k1gInSc1	šerm
</s>
</p>
<p>
<s>
Šermíř	šermíř	k1gMnSc1	šermíř
</s>
</p>
<p>
<s>
Bajonet	bajonet	k1gInSc1	bajonet
<g/>
,	,	kIx,	,
Bodák	bodák	k1gInSc1	bodák
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
meč	meč	k1gInSc1	meč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
meč	meč	k1gInSc1	meč
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
