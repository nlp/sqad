<p>
<s>
Borderlands	Borderlands	k1gInSc1	Borderlands
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
akční	akční	k2eAgFnSc1d1	akční
dynamická	dynamický	k2eAgFnSc1d1	dynamická
střílečka	střílečka	k1gFnSc1	střílečka
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
firmou	firma	k1gFnSc7	firma
Gearbox	Gearbox	k1gInSc4	Gearbox
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
hodně	hodně	k6eAd1	hodně
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
RPG	RPG	kA	RPG
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
svou	svůj	k3xOyFgFnSc4	svůj
postavu	postava	k1gFnSc4	postava
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
vkusu	vkus	k1gInSc2	vkus
a	a	k8xC	a
herního	herní	k2eAgInSc2d1	herní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
rovněž	rovněž	k9	rovněž
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jednička	jednička	k1gFnSc1	jednička
využívá	využívat	k5eAaImIp3nS	využívat
možnost	možnost	k1gFnSc4	možnost
kooperativního	kooperativní	k2eAgInSc2d1	kooperativní
multiplayeru	multiplayer	k1gInSc2	multiplayer
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
přizvat	přizvat	k5eAaPmF	přizvat
kamaráda	kamarád	k1gMnSc4	kamarád
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
zrovna	zrovna	k6eAd1	zrovna
rozpracované	rozpracovaný	k2eAgFnSc2d1	rozpracovaná
mise	mise	k1gFnSc2	mise
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
pomoct	pomoct	k5eAaPmF	pomoct
například	například	k6eAd1	například
při	při	k7c6	při
obtížných	obtížný	k2eAgInPc6d1	obtížný
bojích	boj	k1gInPc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
fiktivní	fiktivní	k2eAgFnSc6d1	fiktivní
planetě	planeta	k1gFnSc6	planeta
Pandora	Pandora	k1gFnSc1	Pandora
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
oblíbenost	oblíbenost	k1gFnSc4	oblíbenost
převážně	převážně	k6eAd1	převážně
díky	díky	k7c3	díky
zajímavému	zajímavý	k2eAgNnSc3d1	zajímavé
grafickému	grafický	k2eAgNnSc3d1	grafické
zpracování	zpracování	k1gNnSc3	zpracování
připomínající	připomínající	k2eAgInSc4d1	připomínající
komiks	komiks	k1gInSc4	komiks
a	a	k8xC	a
neobvyklému	obvyklý	k2eNgInSc3d1	neobvyklý
hernímu	herní	k2eAgInSc3d1	herní
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gameplay	Gamepla	k2eAgMnPc4d1	Gamepla
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
převážně	převážně	k6eAd1	převážně
jako	jako	k9	jako
střílečka	střílečka	k1gFnSc1	střílečka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
postavu	postava	k1gFnSc4	postava
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
styl	styl	k1gInSc4	styl
boje	boj	k1gInSc2	boj
mu	on	k3xPp3gMnSc3	on
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
ze	z	k7c2	z
4	[number]	k4	4
základních	základní	k2eAgFnPc2d1	základní
postav	postava	k1gFnPc2	postava
Vault	Vaulta	k1gFnPc2	Vaulta
hunterů	hunter	k1gMnPc2	hunter
a	a	k8xC	a
poté	poté	k6eAd1	poté
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
DLC	DLC	kA	DLC
stažitelných	stažitelný	k2eAgInPc2d1	stažitelný
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
výhody	výhoda	k1gFnPc4	výhoda
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
bonusy	bonus	k1gInPc4	bonus
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
stylu	styl	k1gInSc3	styl
boje	boj	k1gInSc2	boj
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
speciální	speciální	k2eAgFnSc4d1	speciální
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
fungování	fungování	k1gNnSc2	fungování
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
nějaký	nějaký	k3yIgInSc1	nějaký
čas	čas	k1gInSc1	čas
musí	muset	k5eAaImIp3nS	muset
nabíjet	nabíjet	k5eAaImF	nabíjet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
cestování	cestování	k1gNnSc3	cestování
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgFnPc7d1	velká
lokacemi	lokace	k1gFnPc7	lokace
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
použít	použít	k5eAaPmF	použít
Fast	Fast	k2eAgInSc4d1	Fast
Travel	Travel	k1gInSc4	Travel
Stations	Stationsa	k1gFnPc2	Stationsa
<g/>
,	,	kIx,	,
sérii	série	k1gFnSc4	série
teleportů	teleport	k1gInPc2	teleport
mezi	mezi	k7c7	mezi
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jakkoliv	jakkoliv	k6eAd1	jakkoliv
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
nebo	nebo	k8xC	nebo
Catch-a-ride	Catchid	k1gInSc5	Catch-a-rid
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hráči	hráč	k1gMnPc1	hráč
generují	generovat	k5eAaImIp3nP	generovat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
celá	celý	k2eAgFnSc1d1	celá
hra	hra	k1gFnSc1	hra
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
upraveném	upravený	k2eAgInSc6d1	upravený
Unreal	Unreal	k1gInSc1	Unreal
Enginu	Engin	k1gMnSc3	Engin
<g/>
,	,	kIx,	,
ovladatelnost	ovladatelnost	k1gFnSc1	ovladatelnost
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Pandora	Pandora	k1gFnSc1	Pandora
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
gravitaci	gravitace	k1gFnSc4	gravitace
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
vyšší	vysoký	k2eAgInSc4d2	vyšší
skoky	skok	k1gInPc4	skok
<g/>
,	,	kIx,	,
pomalejší	pomalý	k2eAgInSc4d2	pomalejší
dopad	dopad	k1gInSc4	dopad
a	a	k8xC	a
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
trochu	trochu	k6eAd1	trochu
nezraníte	zranit	k5eNaPmIp2nP	zranit
<g/>
,	,	kIx,	,
když	když	k8xS	když
skočíte	skočit	k5eAaPmIp2nP	skočit
z	z	k7c2	z
hodně	hodně	k6eAd1	hodně
vysokého	vysoký	k2eAgNnSc2d1	vysoké
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
svému	svůj	k3xOyFgMnSc3	svůj
hrdinovi	hrdinův	k2eAgMnPc1d1	hrdinův
můžete	moct	k5eAaImIp2nP	moct
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
vyměnit	vyměnit	k5eAaPmF	vyměnit
"	"	kIx"	"
<g/>
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k9	což
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc4d1	různá
čepice	čepice	k1gFnPc4	čepice
<g/>
,	,	kIx,	,
účesy	účes	k1gInPc4	účes
<g/>
,	,	kIx,	,
helmy	helma	k1gFnPc4	helma
<g/>
,	,	kIx,	,
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
otevíráte	otevírat	k5eAaImIp2nP	otevírat
a	a	k8xC	a
dostáváte	dostávat	k5eAaImIp2nP	dostávat
je	on	k3xPp3gInPc4	on
jako	jako	k8xS	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
určité	určitý	k2eAgInPc4d1	určitý
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
změna	změna	k1gFnSc1	změna
vzhledu	vzhled	k1gInSc2	vzhled
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výměna	výměna	k1gFnSc1	výměna
"	"	kIx"	"
<g/>
skinu	skin	k1gMnSc3	skin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
jiná	jiný	k2eAgFnSc1d1	jiná
barva	barva	k1gFnSc1	barva
základního	základní	k2eAgNnSc2d1	základní
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Skiny	skin	k1gMnPc4	skin
rovněž	rovněž	k9	rovněž
dostáváte	dostávat	k5eAaImIp2nP	dostávat
za	za	k7c4	za
splněné	splněný	k2eAgFnPc4d1	splněná
mise	mise	k1gFnPc4	mise
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
vypadnout	vypadnout	k5eAaPmF	vypadnout
z	z	k7c2	z
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
příběhu	příběh	k1gInSc2	příběh
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
hru	hra	k1gFnSc4	hra
se	s	k7c7	s
stejným	stejný	k2eAgMnSc7d1	stejný
hrdinou	hrdina	k1gMnSc7	hrdina
rozjet	rozjet	k5eAaPmF	rozjet
znova	znova	k6eAd1	znova
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
však	však	k9	však
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
skončením	skončení	k1gNnSc7	skončení
předchozí	předchozí	k2eAgFnSc2d1	předchozí
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
také	také	k6eAd1	také
bude	být	k5eAaImBp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
stejnými	stejný	k2eAgFnPc7d1	stejná
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
zbrojí	zbroj	k1gFnSc7	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
modu	modus	k1gInSc3	modus
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
True	True	k1gFnSc1	True
vaulthunter	vaulthuntra	k1gFnPc2	vaulthuntra
mod.	mod.	k?	mod.
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
jsou	být	k5eAaImIp3nP	být
tužší	tuhý	k2eAgMnPc1d2	tužší
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zajímavější	zajímavý	k2eAgMnSc1d2	zajímavější
a	a	k8xC	a
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
True	Tru	k1gFnSc2	Tru
vaulthunter	vaulthunter	k1gInSc4	vaulthunter
modu	modus	k1gInSc2	modus
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
rozjet	rozjet	k5eAaPmF	rozjet
i	i	k9	i
třetí	třetí	k4xOgFnSc4	třetí
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
hru	hra	k1gFnSc4	hra
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
je	on	k3xPp3gMnPc4	on
Ultimate	Ultimat	k1gInSc5	Ultimat
vaulthunter	vaulthunter	k1gMnSc1	vaulthunter
mod.	mod.	k?	mod.
Hra	hra	k1gFnSc1	hra
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
začátečníka	začátečník	k1gMnSc4	začátečník
prakticky	prakticky	k6eAd1	prakticky
nehratelná	hratelný	k2eNgFnSc1d1	nehratelná
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
rozdělenými	rozdělený	k2eAgInPc7d1	rozdělený
body	bod	k1gInPc7	bod
v	v	k7c6	v
úrovních	úroveň	k1gFnPc6	úroveň
postavy	postava	k1gFnSc2	postava
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
mod	mod	k?	mod
stává	stávat	k5eAaImIp3nS	stávat
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
hromada	hromada	k1gFnSc1	hromada
lidí	člověk	k1gMnPc2	člověk
tuhle	tenhle	k3xDgFnSc4	tenhle
hru	hra	k1gFnSc4	hra
teprve	teprve	k6eAd1	teprve
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Projet	projet	k2eAgInSc1d1	projet
Ulitimate	Ulitimat	k1gInSc5	Ulitimat
vautlhunter	vautlhunter	k1gInSc1	vautlhunter
mod	mod	k?	mod
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
příjemná	příjemný	k2eAgFnSc1d1	příjemná
zábava	zábava	k1gFnSc1	zábava
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
výzvy	výzva	k1gFnSc2	výzva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrdinové	Hrdinové	k2eAgNnPc6d1	Hrdinové
==	==	k?	==
</s>
</p>
<p>
<s>
Axton	Axton	k1gInSc1	Axton
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
útočná	útočný	k2eAgNnPc4d1	útočné
komanda	komando	k1gNnPc4	komando
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Roland	Roland	k1gInSc1	Roland
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
speciální	speciální	k2eAgFnSc7d1	speciální
schopností	schopnost	k1gFnSc7	schopnost
vyhození	vyhození	k1gNnSc2	vyhození
turretu	turret	k1gInSc2	turret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
střílí	střílet	k5eAaImIp3nS	střílet
a	a	k8xC	a
mate	mást	k5eAaImIp3nS	mást
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Axton	Axton	k1gInSc1	Axton
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
mezi	mezi	k7c7	mezi
tím	ten	k3xDgInSc7	ten
schovat	schovat	k5eAaPmF	schovat
či	či	k8xC	či
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
věnovat	věnovat	k5eAaPmF	věnovat
nepřátelům	nepřítel	k1gMnPc3	nepřítel
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Střílna	střílna	k1gFnSc1	střílna
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
asi	asi	k9	asi
minutu	minuta	k1gFnSc4	minuta
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylepšení	vylepšení	k1gNnSc6	vylepšení
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
tato	tento	k3xDgFnSc1	tento
prodleva	prodleva	k1gFnSc1	prodleva
zkrátit	zkrátit	k5eAaPmF	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
silná	silný	k2eAgFnSc1d1	silná
pro	pro	k7c4	pro
začátek	začátek	k1gInSc4	začátek
a	a	k8xC	a
pro	pro	k7c4	pro
naprostého	naprostý	k2eAgMnSc4d1	naprostý
začátečníka	začátečník	k1gMnSc4	začátečník
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
částech	část	k1gFnPc6	část
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
na	na	k7c4	na
dynamický	dynamický	k2eAgInSc4d1	dynamický
boj	boj	k1gInSc4	boj
a	a	k8xC	a
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
střílečkách	střílečka	k1gFnPc6	střílečka
preferuje	preferovat	k5eAaImIp3nS	preferovat
boj	boj	k1gInSc1	boj
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maya	Maya	k?	Maya
je	být	k5eAaImIp3nS	být
siréna	siréna	k1gFnSc1	siréna
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Lillith	Lillith	k1gInSc1	Lillith
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
schopnost	schopnost	k1gFnSc4	schopnost
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
si	se	k3xPyFc3	se
nepřítele	nepřítel	k1gMnSc4	nepřítel
chytit	chytit	k5eAaPmF	chytit
do	do	k7c2	do
Phase	Phase	k1gFnSc2	Phase
locku	lock	k1gInSc2	lock
<g/>
,	,	kIx,	,
jakési	jakýsi	k3yIgFnSc2	jakýsi
bubliny	bublina	k1gFnSc2	bublina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vylepšení	vylepšení	k1gNnSc4	vylepšení
tato	tento	k3xDgFnSc1	tento
bublina	bublina	k1gFnSc1	bublina
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
okolní	okolní	k2eAgMnPc4d1	okolní
nepřátele	nepřítel	k1gMnPc4	nepřítel
stříkat	stříkat	k5eAaImF	stříkat
kyselinu	kyselina	k1gFnSc4	kyselina
či	či	k8xC	či
oheň	oheň	k1gInSc4	oheň
nebo	nebo	k8xC	nebo
po	po	k7c6	po
aktivaci	aktivace	k1gFnSc6	aktivace
nahromadit	nahromadit	k5eAaPmF	nahromadit
všechny	všechen	k3xTgMnPc4	všechen
nepřátele	nepřítel	k1gMnPc4	nepřítel
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schpnost	schpnost	k1gFnSc1	schpnost
má	mít	k5eAaImIp3nS	mít
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Maya	Maya	k?	Maya
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc4d2	nižší
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
preferuje	preferovat	k5eAaImIp3nS	preferovat
boj	boj	k1gInSc1	boj
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
při	při	k7c6	při
boji	boj	k1gInSc6	boj
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
vylepšení	vylepšení	k1gNnPc2	vylepšení
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
jí	on	k3xPp3gFnSc7	on
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
dává	dávat	k5eAaImIp3nS	dávat
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
damage	damage	k1gFnSc1	damage
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
při	při	k7c6	při
Ultimate	Ultimat	k1gInSc5	Ultimat
vaulthunter	vaulthunter	k1gInSc1	vaulthunter
modu	modus	k1gInSc6	modus
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
slabých	slabý	k2eAgFnPc2d1	slabá
a	a	k8xC	a
středně	středně	k6eAd1	středně
silných	silný	k2eAgMnPc2d1	silný
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Salvador	Salvador	k1gMnSc1	Salvador
je	být	k5eAaImIp3nS	být
gunzerker	gunzerker	k1gMnSc1	gunzerker
<g/>
,	,	kIx,	,
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
pistolník	pistolník	k1gMnSc1	pistolník
jehož	jehož	k3xOyRp3gFnSc4	jehož
speciální	speciální	k2eAgFnSc4d1	speciální
schopnost	schopnost	k1gFnSc4	schopnost
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
síle	síla	k1gFnSc6	síla
připomínající	připomínající	k2eAgNnSc1d1	připomínající
berserka	berserk	k1gMnSc4	berserk
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
používat	používat	k5eAaImF	používat
dvě	dva	k4xCgFnPc4	dva
obouruční	obouruční	k2eAgFnPc4d1	obouruční
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
klidně	klidně	k6eAd1	klidně
raketomet	raketomet	k1gInSc4	raketomet
a	a	k8xC	a
útočnou	útočný	k2eAgFnSc4d1	útočná
pušku	puška	k1gFnSc4	puška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
této	tento	k3xDgFnSc2	tento
schopnosti	schopnost	k1gFnSc2	schopnost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
obnovují	obnovovat	k5eAaImIp3nP	obnovovat
zásobníky	zásobník	k1gInPc1	zásobník
v	v	k7c6	v
batohu	batoh	k1gInSc6	batoh
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
odolnost	odolnost	k1gFnSc4	odolnost
a	a	k8xC	a
dobíjí	dobíjet	k5eAaImIp3nS	dobíjet
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
boj	boj	k1gInSc4	boj
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
proti	proti	k7c3	proti
menšímu	malý	k2eAgInSc3d2	menší
počtu	počet	k1gInSc3	počet
silnějších	silný	k2eAgMnPc2d2	silnější
nepřátel	nepřítel	k1gMnPc2	nepřítel
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
bossům	boss	k1gMnPc3	boss
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zero	Zero	k6eAd1	Zero
je	být	k5eAaImIp3nS	být
android	android	k1gInSc1	android
připomínající	připomínající	k2eAgInSc1d1	připomínající
ninju	ninju	k5eAaPmIp1nS	ninju
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
schopností	schopnost	k1gFnSc7	schopnost
je	být	k5eAaImIp3nS	být
vypuštění	vypuštění	k1gNnSc2	vypuštění
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
více	hodně	k6eAd2	hodně
klonů	klon	k1gInPc2	klon
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pravý	pravý	k2eAgMnSc1d1	pravý
Zero	Zero	k1gMnSc1	Zero
se	se	k3xPyFc4	se
neviditelně	viditelně	k6eNd1	viditelně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
krátkého	krátký	k2eAgNnSc2d1	krátké
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
na	na	k7c4	na
boj	boj	k1gInSc4	boj
na	na	k7c4	na
blízko	blízko	k1gNnSc4	blízko
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
jeho	jeho	k3xOp3gInSc2	jeho
útočného	útočný	k2eAgInSc2d1	útočný
meče	meč	k1gInSc2	meč
nebo	nebo	k8xC	nebo
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
Zero	Zero	k6eAd1	Zero
bonusy	bonus	k1gInPc4	bonus
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
odstřelovací	odstřelovací	k2eAgFnSc2d1	odstřelovací
pušky	puška	k1gFnSc2	puška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gaige	Gaige	k6eAd1	Gaige
je	být	k5eAaImIp3nS	být
mechromancerka	mechromancerka	k1gFnSc1	mechromancerka
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
holka	holka	k1gFnSc1	holka
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
ještě	ještě	k9	ještě
dítě	dítě	k1gNnSc1	dítě
školou	škola	k1gFnSc7	škola
povinné	povinný	k2eAgInPc1d1	povinný
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
staví	stavit	k5eAaPmIp3nS	stavit
vlastního	vlastní	k2eAgMnSc4d1	vlastní
robota	robot	k1gMnSc4	robot
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
mapě	mapa	k1gFnSc6	mapa
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
za	za	k7c4	za
Gaige	Gaige	k1gFnSc4	Gaige
případně	případně	k6eAd1	případně
pomůže	pomoct	k5eAaPmIp3nS	pomoct
rozdělení	rozdělení	k1gNnSc1	rozdělení
nepřátel	nepřítel	k1gMnPc2	nepřítel
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
jedni	jeden	k4xCgMnPc1	jeden
honí	honit	k5eAaImIp3nP	honit
za	za	k7c4	za
Gaige	Gaige	k1gFnSc4	Gaige
a	a	k8xC	a
druzí	druhý	k4xOgMnPc1	druhý
za	za	k7c4	za
robotem	robot	k1gMnSc7	robot
<g/>
.	.	kIx.	.
</s>
<s>
Robot	robot	k1gMnSc1	robot
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
silným	silný	k2eAgMnSc7d1	silný
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
pomocníkem	pomocník	k1gMnSc7	pomocník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Gaige	Gaige	k1gInSc1	Gaige
rychle	rychle	k6eAd1	rychle
dobíjí	dobíjet	k5eAaImIp3nS	dobíjet
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
další	další	k2eAgFnSc7d1	další
schopností	schopnost	k1gFnSc7	schopnost
je	být	k5eAaImIp3nS	být
hromadění	hromadění	k1gNnSc1	hromadění
Anarchy	Anarcha	k1gFnSc2	Anarcha
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
každého	každý	k3xTgMnSc2	každý
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
číslem	číslo	k1gNnSc7	číslo
násobí	násobit	k5eAaImIp3nS	násobit
její	její	k3xOp3gInPc4	její
damage	damag	k1gInPc4	damag
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Mechromancerka	Mechromancerka	k1gFnSc1	Mechromancerka
hodí	hodit	k5eAaImIp3nS	hodit
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
si	se	k3xPyFc3	se
všímají	všímat	k5eAaImIp3nP	všímat
jejího	její	k3xOp3gMnSc4	její
robota	robot	k1gMnSc4	robot
<g/>
.	.	kIx.	.
</s>
<s>
Gaige	Gaige	k1gFnSc1	Gaige
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
klasické	klasický	k2eAgFnSc2d1	klasická
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
DLC	DLC	kA	DLC
<g/>
.	.	kIx.	.
hráč	hráč	k1gMnSc1	hráč
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
velice	velice	k6eAd1	velice
se	se	k3xPyFc4	se
probojovat	probojovat	k5eAaPmF	probojovat
Normal	Normal	k1gInSc1	Normal
i	i	k8xC	i
True	True	k1gInSc1	True
vaulthunter	vaulthunter	k1gMnSc1	vaulthunter
modem	modem	k1gInSc1	modem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krieg	Krieg	k1gInSc1	Krieg
je	být	k5eAaImIp3nS	být
psycho	psycha	k1gFnSc5	psycha
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
normálně	normálně	k6eAd1	normálně
fungují	fungovat	k5eAaImIp3nP	fungovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
jako	jako	k9	jako
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Psycho	psycha	k1gFnSc5	psycha
má	mít	k5eAaImIp3nS	mít
rozdvojenou	rozdvojený	k2eAgFnSc4d1	rozdvojená
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
mysl	mysl	k1gFnSc1	mysl
je	být	k5eAaImIp3nS	být
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
a	a	k8xC	a
čistá	čistý	k2eAgFnSc1d1	čistá
jako	jako	k8xS	jako
napadnutý	napadnutý	k2eAgInSc1d1	napadnutý
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vylézá	vylézat	k5eAaImIp3nS	vylézat
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
sprostá	sprostý	k2eAgNnPc1d1	sprosté
slova	slovo	k1gNnPc1	slovo
prokládána	prokládat	k5eAaImNgFnS	prokládat
morbidním	morbidní	k2eAgNnSc7d1	morbidní
povzbuzováním	povzbuzování	k1gNnSc7	povzbuzování
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
kůži	kůže	k1gFnSc4	kůže
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
schopností	schopnost	k1gFnSc7	schopnost
používání	používání	k1gNnSc2	používání
jeho	jeho	k3xOp3gFnSc2	jeho
rotující	rotující	k2eAgFnSc2d1	rotující
sekyrky	sekyrka	k1gFnSc2	sekyrka
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
uzdravuje	uzdravovat	k5eAaImIp3nS	uzdravovat
si	se	k3xPyFc3	se
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
sekyrku	sekyrka	k1gFnSc4	sekyrka
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
házet	házet	k5eAaImF	házet
či	či	k8xC	či
případně	případně	k6eAd1	případně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pouze	pouze	k6eAd1	pouze
velkou	velký	k2eAgFnSc7d1	velká
rychlostí	rychlost	k1gFnSc7	rychlost
sekat	sekat	k5eAaImF	sekat
do	do	k7c2	do
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
jeho	jeho	k3xOp3gInPc2	jeho
skillů	skill	k1gInPc2	skill
je	být	k5eAaImIp3nS	být
nespočetné	spočetný	k2eNgNnSc1d1	nespočetné
a	a	k8xC	a
velice	velice	k6eAd1	velice
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
funkce	funkce	k1gFnPc4	funkce
jakési	jakýsi	k3yIgFnSc2	jakýsi
autodestrukce	autodestrukce	k1gFnSc2	autodestrukce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
umírání	umírání	k1gNnSc6	umírání
vybouchne	vybouchnout	k5eAaPmIp3nS	vybouchnout
a	a	k8xC	a
zapálí	zapálit	k5eAaPmIp3nS	zapálit
okolní	okolní	k2eAgMnPc4d1	okolní
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
Kriega	Krieg	k1gMnSc4	Krieg
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
zábavné	zábavný	k2eAgNnSc1d1	zábavné
<g/>
,	,	kIx,	,
hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
zkušenější	zkušený	k2eAgMnPc4d2	zkušenější
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
již	již	k9	již
Borderlands	Borderlands	k1gInSc4	Borderlands
2	[number]	k4	2
projeli	projet	k5eAaPmAgMnP	projet
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Gaige	Gaige	k1gFnSc1	Gaige
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
klasické	klasický	k2eAgFnSc2d1	klasická
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
musíte	muset	k5eAaImIp2nP	muset
pořídit	pořídit	k5eAaPmF	pořídit
jako	jako	k9	jako
DLC	DLC	kA	DLC
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
postavy	postava	k1gFnPc4	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
Vault	Vaultum	k1gNnPc2	Vaultum
hunterů	hunter	k1gMnPc2	hunter
a	a	k8xC	a
charismatický	charismatický	k2eAgMnSc1d1	charismatický
velitel	velitel	k1gMnSc1	velitel
Crimson	Crimsona	k1gFnPc2	Crimsona
Raiders	Raidersa	k1gFnPc2	Raidersa
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgInSc2d1	hlavní
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
tyranské	tyranský	k2eAgFnSc3d1	tyranská
společnosti	společnost	k1gFnSc3	společnost
Hyperion	Hyperion	k1gInSc4	Hyperion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lillith	Lillith	k1gInSc1	Lillith
-	-	kIx~	-
siréna	siréna	k1gFnSc1	siréna
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
také	také	k9	také
mohli	moct	k5eAaImAgMnP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Firehawk	Firehawk	k1gInSc1	Firehawk
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc1d1	aktivní
bojovnice	bojovnice	k1gFnSc1	bojovnice
za	za	k7c4	za
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
Pandoru	Pandora	k1gFnSc4	Pandora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mordecai	Mordecai	k1gNnSc1	Mordecai
-	-	kIx~	-
věčně	věčně	k6eAd1	věčně
opilý	opilý	k2eAgMnSc1d1	opilý
lovec	lovec	k1gMnSc1	lovec
vybavený	vybavený	k2eAgMnSc1d1	vybavený
svou	svůj	k3xOyFgFnSc7	svůj
odstřelovací	odstřelovací	k2eAgFnSc7d1	odstřelovací
puškou	puška	k1gFnSc7	puška
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
Bloodwing	Bloodwing	k1gInSc1	Bloodwing
připomínající	připomínající	k2eAgFnSc4d1	připomínající
kombinaci	kombinace	k1gFnSc4	kombinace
orlosupa	orlosup	k1gMnSc2	orlosup
a	a	k8xC	a
netopýra	netopýr	k1gMnSc2	netopýr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
důležitější	důležitý	k2eAgNnSc4d2	důležitější
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
cokoliv	cokoliv	k3yInSc4	cokoliv
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brick	Brick	k1gInSc1	Brick
-	-	kIx~	-
natvrdlý	natvrdlý	k2eAgMnSc1d1	natvrdlý
velitel	velitel	k1gMnSc1	velitel
místních	místní	k2eAgMnPc2d1	místní
tupých	tupý	k2eAgMnPc2d1	tupý
nájezdníků	nájezdník	k1gMnPc2	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bojuje	bojovat	k5eAaImIp3nS	bojovat
za	za	k7c7	za
svobodou	svoboda	k1gFnSc7	svoboda
Pandoru	Pandora	k1gFnSc4	Pandora
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
původní	původní	k2eAgInSc4d1	původní
Vault	Vault	k1gInSc4	Vault
huntery	hunter	k1gInPc7	hunter
jako	jako	k8xS	jako
Roland	Rolanda	k1gFnPc2	Rolanda
<g/>
,	,	kIx,	,
Lillith	Lillitha	k1gFnPc2	Lillitha
a	a	k8xC	a
Mordecai	Mordeca	k1gFnSc2	Mordeca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Handsome	Handsom	k1gInSc5	Handsom
Jack	Jack	k1gMnSc1	Jack
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
společnosti	společnost	k1gFnSc2	společnost
Hyperion	Hyperion	k1gInSc1	Hyperion
a	a	k8xC	a
oportunistický	oportunistický	k2eAgInSc1d1	oportunistický
sociopat	sociopat	k5eAaPmF	sociopat
s	s	k7c7	s
narcistní	narcistní	k2eAgFnSc7d1	narcistní
poruchou	porucha	k1gFnSc7	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Velí	velet	k5eAaImIp3nS	velet
celé	celý	k2eAgFnSc3d1	celá
Pandoře	Pandora	k1gFnSc3	Pandora
a	a	k8xC	a
jako	jako	k8xS	jako
správný	správný	k2eAgInSc1d1	správný
černobílý	černobílý	k2eAgInSc1d1	černobílý
záporák	záporák	k1gInSc1	záporák
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k8xC	ani
kousku	kousek	k1gInSc2	kousek
dobré	dobrý	k2eAgFnSc2d1	dobrá
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
masku	maska	k1gFnSc4	maska
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadá	vypadat	k5eAaPmIp3nS	vypadat
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nějak	nějak	k6eAd1	nějak
zohyzdněný	zohyzdněný	k2eAgInSc1d1	zohyzdněný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
mít	mít	k5eAaImF	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	jeho	k3xOp3gFnSc4	jeho
prohnilou	prohnilý	k2eAgFnSc4d1	prohnilá
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
tvář	tvář	k1gFnSc4	tvář
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
úplně	úplně	k6eAd1	úplně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angel	angel	k1gMnSc1	angel
-	-	kIx~	-
siréna	siréna	k1gFnSc1	siréna
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
průvodce	průvodce	k1gMnSc2	průvodce
většinou	většina	k1gFnSc7	většina
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
ti	ty	k3xPp2nSc3	ty
dostat	dostat	k5eAaPmF	dostat
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacek	k1gMnSc4	Jacek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Claptrap	Claptrap	k1gMnSc1	Claptrap
-	-	kIx~	-
robot	robot	k1gMnSc1	robot
ze	z	k7c2	z
série	série	k1gFnSc2	série
CL	CL	kA	CL
<g/>
4	[number]	k4	4
<g/>
P-TP	P-TP	k1gMnPc2	P-TP
<g/>
,	,	kIx,	,
užvaněný	užvaněný	k2eAgInSc4d1	užvaněný
<g/>
,	,	kIx,	,
optimistický	optimistický	k2eAgInSc4d1	optimistický
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
nebojácný	bojácný	k2eNgInSc1d1	nebojácný
robůtek	robůtek	k1gInSc1	robůtek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
sebemenší	sebemenší	k2eAgFnSc6d1	sebemenší
akci	akce	k1gFnSc6	akce
sesype	sesypat	k5eAaPmIp3nS	sesypat
a	a	k8xC	a
schová	schovat	k5eAaPmIp3nS	schovat
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
končetiny	končetina	k1gFnPc4	končetina
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Patricia	Patricius	k1gMnSc2	Patricius
Tannis	Tannis	k1gFnSc1	Tannis
-	-	kIx~	-
androfobní	androfobní	k2eAgFnSc1d1	androfobní
archeoložka	archeoložka	k1gFnSc1	archeoložka
pracující	pracující	k2eAgFnSc1d1	pracující
pro	pro	k7c4	pro
Crimson	Crimson	k1gNnSc4	Crimson
Raiders	Raidersa	k1gFnPc2	Raidersa
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
zvracet	zvracet	k5eAaImF	zvracet
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
přinucena	přinucen	k2eAgFnSc1d1	přinucena
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
s	s	k7c7	s
cizími	cizí	k2eAgFnPc7d1	cizí
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
-	-	kIx~	-
místní	místní	k2eAgMnSc1d1	místní
obchodník	obchodník	k1gMnSc1	obchodník
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
zisk	zisk	k1gInSc4	zisk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
prodává	prodávat	k5eAaImIp3nS	prodávat
zbraně	zbraň	k1gFnPc4	zbraň
oběma	dva	k4xCgFnPc3	dva
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
ZED	ZED	kA	ZED
-	-	kIx~	-
nevystudovaný	vystudovaný	k2eNgMnSc1d1	nevystudovaný
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prodává	prodávat	k5eAaImIp3nS	prodávat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Pandoře	Pandora	k1gFnSc6	Pandora
léčivé	léčivý	k2eAgFnSc2d1	léčivá
injekce	injekce	k1gFnSc2	injekce
a	a	k8xC	a
štíty	štít	k1gInPc4	štít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moxxi	Moxxe	k1gFnSc4	Moxxe
-	-	kIx~	-
prostitutka	prostitutka	k1gFnSc1	prostitutka
a	a	k8xC	a
hospodská	hospodská	k1gFnSc1	hospodská
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
baru	bar	k1gInSc6	bar
v	v	k7c6	v
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
polovinou	polovina	k1gFnSc7	polovina
Pandory	Pandora	k1gFnSc2	Pandora
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Mordecaie	Mordecaie	k1gFnSc2	Mordecaie
<g/>
,	,	kIx,	,
Marcuse	Marcuse	k1gFnSc2	Marcuse
nebo	nebo	k8xC	nebo
samotného	samotný	k2eAgInSc2d1	samotný
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacko	k1gNnPc1	Jacko
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
časem	čas	k1gInSc7	čas
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
sérii	série	k1gFnSc4	série
zabijáckých	zabijácký	k2eAgFnPc2d1	zabijácká
arén	aréna	k1gFnPc2	aréna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
nacházíš	nacházet	k5eAaImIp2nS	nacházet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
můžeš	moct	k5eAaImIp2nS	moct
zabojovat	zabojovat	k5eAaPmF	zabojovat
a	a	k8xC	a
vyhrát	vyhrát	k5eAaPmF	vyhrát
praktické	praktický	k2eAgFnPc4d1	praktická
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jí	on	k3xPp3gFnSc3	on
podplatíš	podplatit	k5eAaPmIp2nS	podplatit
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
ti	ty	k3xPp2nSc3	ty
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
elementární	elementární	k2eAgInSc4d1	elementární
SMG	SMG	kA	SMG
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Maliwan	Maliwany	k1gInPc2	Maliwany
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ti	ten	k3xDgMnPc1	ten
dobíjí	dobíjet	k5eAaImIp3nP	dobíjet
zdraví	zdravý	k2eAgMnPc1d1	zdravý
<g/>
,	,	kIx,	,
když	když	k8xS	když
střílíš	střílet	k5eAaImIp2nS	střílet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Scooter	Scooter	k1gMnSc1	Scooter
-	-	kIx~	-
syn	syn	k1gMnSc1	syn
Moxxi	Moxxe	k1gFnSc4	Moxxe
a	a	k8xC	a
buranský	buranský	k2eAgMnSc1d1	buranský
majitel	majitel	k1gMnSc1	majitel
Catch-a-Ride	Catch-Rid	k1gInSc5	Catch-a-Rid
vlastnící	vlastnící	k2eAgInSc4d1	vlastnící
autoopravnu	autoopravna	k1gFnSc4	autoopravna
v	v	k7c4	v
Sanctuary	Sanctuar	k1gMnPc4	Sanctuar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
naprosté	naprostý	k2eAgFnPc4d1	naprostá
piliny	pilina	k1gFnPc4	pilina
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c4	na
auta	auto	k1gNnPc4	auto
je	být	k5eAaImIp3nS	být
odborník	odborník	k1gMnSc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
ženská	ženská	k1gFnSc1	ženská
ho	on	k3xPp3gMnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
páchají	páchat	k5eAaImIp3nP	páchat
sebevraždy	sebevražda	k1gFnPc1	sebevražda
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
něco	něco	k3yInSc1	něco
měly	mít	k5eAaImAgFnP	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ellie	Ellie	k1gFnSc1	Ellie
-	-	kIx~	-
dcera	dcera	k1gFnSc1	dcera
Moxxi	Moxxe	k1gFnSc4	Moxxe
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
kyprá	kyprý	k2eAgFnSc1d1	kyprá
autoopravářka	autoopravářka	k1gFnSc1	autoopravářka
<g/>
,	,	kIx,	,
vlastnící	vlastnící	k2eAgInSc4d1	vlastnící
svůj	svůj	k3xOyFgInSc4	svůj
nezávislý	závislý	k2eNgInSc4d1	nezávislý
salon	salon	k1gInSc4	salon
v	v	k7c6	v
Dustu	Dust	k1gInSc6	Dust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tiny	Tina	k1gFnPc4	Tina
Tina	Tina	k1gFnSc1	Tina
-	-	kIx~	-
naprosto	naprosto	k6eAd1	naprosto
střelená	střelený	k2eAgFnSc1d1	střelená
13	[number]	k4	13
<g/>
letá	letý	k2eAgFnSc1d1	letá
holka	holka	k1gFnSc1	holka
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
v	v	k7c6	v
Tundra	tundra	k1gFnSc1	tundra
Expres	expres	k2eAgMnSc7d1	expres
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jí	jíst	k5eAaImIp3nS	jíst
přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
zabili	zabít	k5eAaPmAgMnP	zabít
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Odbornice	odbornice	k1gFnSc1	odbornice
na	na	k7c4	na
výbušniny	výbušnina	k1gFnPc4	výbušnina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ráda	rád	k2eAgFnSc1d1	ráda
mučí	mučet	k5eAaImIp3nP	mučet
nájezdníky	nájezdník	k1gMnPc4	nájezdník
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
křesle	křeslo	k1gNnSc6	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Vděčná	vděčný	k2eAgFnSc1d1	vděčná
Rolandovi	Rolandův	k2eAgMnPc1d1	Rolandův
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
polovina	polovina	k1gFnSc1	polovina
Pandory	Pandora	k1gFnSc2	Pandora
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
každou	každý	k3xTgFnSc4	každý
botu	bota	k1gFnSc4	bota
jinou	jiný	k2eAgFnSc4d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sir	sir	k1gMnSc1	sir
Hammerlock	Hammerlock	k1gMnSc1	Hammerlock
-	-	kIx~	-
hrdý	hrdý	k2eAgMnSc1d1	hrdý
Brit	Brit	k1gMnSc1	Brit
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
obří	obří	k2eAgMnSc1d1	obří
mutantní	mutantní	k2eAgMnSc1d1	mutantní
červ	červ	k1gMnSc1	červ
urval	urvat	k5eAaPmAgMnS	urvat
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
místa	místo	k1gNnPc1	místo
a	a	k8xC	a
lokace	lokace	k1gFnPc1	lokace
==	==	k?	==
</s>
</p>
<p>
<s>
Sanctuary	Sanctuara	k1gFnPc1	Sanctuara
(	(	kIx(	(
<g/>
Azyl	azyl	k1gInSc1	azyl
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
enkláva	enkláva	k1gFnSc1	enkláva
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
Crimson	Crimsona	k1gFnPc2	Crimsona
Raiders	Raidersa	k1gFnPc2	Raidersa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
Hyperionu	Hyperion	k1gInSc3	Hyperion
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
resetovat	resetovat	k5eAaImF	resetovat
své	svůj	k3xOyFgInPc4	svůj
dovednostní	dovednostní	k2eAgInPc4d1	dovednostní
body	bod	k1gInPc4	bod
případně	případně	k6eAd1	případně
si	se	k3xPyFc3	se
změnit	změnit	k5eAaPmF	změnit
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eridiové	Eridiový	k2eAgInPc1d1	Eridiový
doly	dol	k1gInPc1	dol
-	-	kIx~	-
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Handsome	Handsom	k1gInSc5	Handsom
Jack	Jack	k1gMnSc1	Jack
těží	těžet	k5eAaImIp3nS	těžet
místní	místní	k2eAgInSc4d1	místní
vzácný	vzácný	k2eAgInSc4d1	vzácný
fialový	fialový	k2eAgInSc4d1	fialový
prvek	prvek	k1gInSc4	prvek
eridium	eridium	k1gNnSc1	eridium
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
sirénám	siréna	k1gFnPc3	siréna
jejich	jejich	k3xOp3gFnSc4	jejich
speciální	speciální	k2eAgFnSc4d1	speciální
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Pandoře	Pandora	k1gFnSc6	Pandora
sbírat	sbírat	k5eAaImF	sbírat
a	a	k8xC	a
pak	pak	k6eAd1	pak
v	v	k7c4	v
Sanctuary	Sanctuara	k1gFnPc4	Sanctuara
u	u	k7c2	u
Crazy	Craza	k1gFnSc2	Craza
Earla	earl	k1gMnSc2	earl
prodávat	prodávat	k5eAaImF	prodávat
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
kupovat	kupovat	k5eAaImF	kupovat
si	se	k3xPyFc3	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
různá	různý	k2eAgNnPc1d1	různé
další	další	k1gNnPc1	další
vylepšení	vylepšení	k1gNnSc2	vylepšení
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hyperionská	Hyperionský	k2eAgNnPc4d1	Hyperionský
sídla	sídlo	k1gNnPc4	sídlo
-	-	kIx~	-
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
mapě	mapa	k1gFnSc6	mapa
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
nespočet	nespočet	k1gInSc4	nespočet
a	a	k8xC	a
všechna	všechen	k3xTgNnPc4	všechen
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
podobná	podobný	k2eAgNnPc4d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
Loaderů	Loader	k1gMnPc2	Loader
-	-	kIx~	-
robotů	robot	k1gMnPc2	robot
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
společností	společnost	k1gFnPc2	společnost
Hyperion	Hyperion	k1gInSc1	Hyperion
<g/>
,	,	kIx,	,
útočných	útočný	k2eAgInPc2d1	útočný
tupých	tupý	k2eAgInPc2d1	tupý
strojů	stroj	k1gInPc2	stroj
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
sebedestrukci	sebedestrukce	k1gFnSc3	sebedestrukce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
zákonech	zákon	k1gInPc6	zákon
robotiky	robotika	k1gFnSc2	robotika
ani	ani	k8xC	ani
neslyšeli	slyšet	k5eNaImAgMnP	slyšet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nájezdnická	nájezdnický	k2eAgNnPc4d1	nájezdnický
sídla	sídlo	k1gNnPc4	sídlo
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
hyperionských	hyperionský	k2eAgNnPc2d1	hyperionský
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
nájezdnických	nájezdnický	k2eAgNnPc2d1	nájezdnický
sídel	sídlo	k1gNnPc2	sídlo
plno	plno	k6eAd1	plno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
nomádi	nomád	k1gMnPc1	nomád
obdaření	obdaření	k1gNnSc2	obdaření
fyzickým	fyzický	k2eAgInSc7d1	fyzický
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
goliášové	goliáš	k1gMnPc1	goliáš
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
ustřelení	ustřelení	k1gNnSc6	ustřelení
helmy	helma	k1gFnSc2	helma
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
vymutovaní	vymutovaný	k2eAgMnPc1d1	vymutovaný
krysolidé	krysolidý	k2eAgFnSc6d1	krysolidý
<g/>
,	,	kIx,	,
psycha	psycha	k1gFnSc1	psycha
<g/>
,	,	kIx,	,
skrčci	skrček	k1gMnPc1	skrček
či	či	k8xC	či
sebevražední	sebevražedný	k2eAgMnPc1d1	sebevražedný
nájezdníci	nájezdník	k1gMnPc1	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tito	tento	k3xDgMnPc1	tento
nepřátelé	nepřítel	k1gMnPc1	nepřítel
jsou	být	k5eAaImIp3nP	být
naprosto	naprosto	k6eAd1	naprosto
tupí	tupý	k2eAgMnPc1d1	tupý
a	a	k8xC	a
neschopní	schopný	k2eNgMnPc1d1	neschopný
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
řízenosti	řízenost	k1gFnPc1	řízenost
jejich	jejich	k3xOp3gInPc2	jejich
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divočina	divočina	k1gFnSc1	divočina
-	-	kIx~	-
příroda	příroda	k1gFnSc1	příroda
Pandory	Pandora	k1gFnSc2	Pandora
je	být	k5eAaImIp3nS	být
obývaná	obývaný	k2eAgFnSc1d1	obývaná
tamními	tamní	k2eAgNnPc7d1	tamní
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zvyklá	zvyklý	k2eAgNnPc1d1	zvyklé
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgMnSc7d3	nejčastější
nepřítelem	nepřítel	k1gMnSc7	nepřítel
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Skag	Skag	k1gInSc4	Skag
-	-	kIx~	-
agresivní	agresivní	k2eAgMnSc1d1	agresivní
predátor	predátor	k1gMnSc1	predátor
nejvíce	nejvíce	k6eAd1	nejvíce
připomínající	připomínající	k2eAgMnPc4d1	připomínající
psy	pes	k1gMnPc4	pes
<g/>
,	,	kIx,	,
Bullymong	Bullymong	k1gMnSc1	Bullymong
-	-	kIx~	-
gorilo-medvěd	goriloedvěd	k1gMnSc1	gorilo-medvěd
s	s	k7c7	s
šesti	šest	k4xCc7	šest
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
Stalker	Stalker	k1gMnSc1	Stalker
-	-	kIx~	-
ještěr	ještěr	k1gMnSc1	ještěr
vypadající	vypadající	k2eAgMnSc1d1	vypadající
trochu	trochu	k6eAd1	trochu
jako	jako	k8xC	jako
malý	malý	k2eAgInSc1d1	malý
drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
nejotravnější	otravný	k2eAgFnSc1d3	nejotravnější
vlastnost	vlastnost	k1gFnSc1	vlastnost
je	být	k5eAaImIp3nS	být
občasná	občasný	k2eAgFnSc1d1	občasná
neviditelnost	neviditelnost	k1gFnSc1	neviditelnost
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Invisible	Invisible	k1gFnSc1	Invisible
Asshole	Asshole	k1gFnSc1	Asshole
<g/>
,	,	kIx,	,
Rakk	Rakk	k1gInSc1	Rakk
-	-	kIx~	-
malý	malý	k2eAgInSc1d1	malý
drakonetopýr	drakonetopýr	k1gInSc1	drakonetopýr
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nP	vařit
nekvalitní	kvalitní	k2eNgMnPc1d1	nekvalitní
halucinogení	halucinogený	k1gMnPc1	halucinogený
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
Thresher	Threshra	k1gFnPc2	Threshra
-	-	kIx~	-
červorybochobotnice	červorybochobotnice	k1gFnSc1	červorybochobotnice
<g/>
,	,	kIx,	,
zalezlá	zalezlý	k2eAgFnSc1d1	zalezlá
v	v	k7c6	v
dírách	díra	k1gFnPc6	díra
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nepřátele	nepřítel	k1gMnPc4	nepřítel
stahuje	stahovat	k5eAaImIp3nS	stahovat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
vaultu	vault	k1gInSc2	vault
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	díl	k1gInSc6	díl
převzal	převzít	k5eAaPmAgMnS	převzít
Pandoru	Pandora	k1gFnSc4	Pandora
tyran	tyran	k1gMnSc1	tyran
Handsome	Handsom	k1gInSc5	Handsom
Jack	Jack	k1gMnSc1	Jack
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
posledních	poslední	k2eAgNnPc2d1	poslední
5	[number]	k4	5
let	léto	k1gNnPc2	léto
ovládá	ovládat	k5eAaImIp3nS	ovládat
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
Klíče	klíč	k1gInSc2	klíč
vaultu	vault	k1gInSc2	vault
probudit	probudit	k5eAaPmF	probudit
bájného	bájný	k2eAgMnSc4d1	bájný
Válečníka	válečník	k1gMnSc4	válečník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
využívá	využívat	k5eAaImIp3nS	využívat
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Angel	angel	k1gMnSc1	angel
<g/>
,	,	kIx,	,
sirénu	siréna	k1gFnSc4	siréna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vězněná	vězněný	k2eAgFnSc1d1	vězněná
a	a	k8xC	a
pumpována	pumpován	k2eAgFnSc1d1	pumpována
eridiem	eridium	k1gNnSc7	eridium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
znásobila	znásobit	k5eAaPmAgFnS	znásobit
její	její	k3xOp3gFnSc1	její
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Pandoru	Pandora	k1gFnSc4	Pandora
proto	proto	k8xC	proto
přijíždí	přijíždět	k5eAaImIp3nP	přijíždět
čtyři	čtyři	k4xCgMnPc1	čtyři
noví	nový	k2eAgMnPc1d1	nový
Vault	Vault	k2eAgMnSc1d1	Vault
hunteři	hunter	k1gMnPc1	hunter
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
Jacka	Jacko	k1gNnSc2	Jacko
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přežije	přežít	k5eAaPmIp3nS	přežít
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
hry	hra	k1gFnSc2	hra
dle	dle	k7c2	dle
hráčova	hráčův	k2eAgInSc2d1	hráčův
výběhu	výběh	k1gInSc2	výběh
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgMnSc4	tento
hrdinu	hrdina	k1gMnSc4	hrdina
nalezne	nalézt	k5eAaBmIp3nS	nalézt
Claptrap	Claptrap	k1gMnSc1	Claptrap
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Angel	Angela	k1gFnPc2	Angela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
umí	umět	k5eAaImIp3nS	umět
datově	datově	k6eAd1	datově
nabourat	nabourat	k5eAaPmF	nabourat
do	do	k7c2	do
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
dovede	dovést	k5eAaPmIp3nS	dovést
do	do	k7c2	do
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
mezitím	mezitím	k6eAd1	mezitím
unese	unést	k5eAaPmIp3nS	unést
Rolanda	Rolanda	k1gFnSc1	Rolanda
Firehawk	Firehawka	k1gFnPc2	Firehawka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dalšího	další	k2eAgMnSc4d1	další
bývalého	bývalý	k2eAgMnSc4d1	bývalý
Vault	Vaultum	k1gNnPc2	Vaultum
huntera	hunter	k1gMnSc2	hunter
Lillith	Lillitha	k1gFnPc2	Lillitha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nalézt	nalézt	k5eAaPmF	nalézt
Rolanda	Rolanda	k1gFnSc1	Rolanda
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Společnými	společný	k2eAgFnPc7d1	společná
silami	síla	k1gFnPc7	síla
vypátrají	vypátrat	k5eAaPmIp3nP	vypátrat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
unesli	unést	k5eAaPmAgMnP	unést
nájezdníci	nájezdník	k1gMnPc1	nájezdník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
prodali	prodat	k5eAaPmAgMnP	prodat
Hyperionu	Hyperion	k1gInSc3	Hyperion
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Hyperion	Hyperion	k1gInSc1	Hyperion
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
s	s	k7c7	s
nájezdníky	nájezdník	k1gMnPc7	nájezdník
o	o	k7c4	o
Rolanda	Rolando	k1gNnPc4	Rolando
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
a	a	k8xC	a
jednoduše	jednoduše	k6eAd1	jednoduše
nájezdnickou	nájezdnický	k2eAgFnSc4d1	nájezdnická
základnu	základna	k1gFnSc4	základna
napadnou	napadnout	k5eAaPmIp3nP	napadnout
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
Rolanda	Rolando	k1gNnPc1	Rolando
unesou	unést	k5eAaPmIp3nP	unést
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ho	on	k3xPp3gMnSc4	on
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
musí	muset	k5eAaImIp3nS	muset
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lillith	Lillith	k1gInSc1	Lillith
hledají	hledat	k5eAaImIp3nP	hledat
Jackem	Jacko	k1gNnSc7	Jacko
ukradený	ukradený	k2eAgInSc1d1	ukradený
Klíč	klíč	k1gInSc1	klíč
vaultu	vault	k1gInSc2	vault
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
navrátili	navrátit	k5eAaPmAgMnP	navrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
původní	původní	k2eAgNnSc4d1	původní
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
porazili	porazit	k5eAaPmAgMnP	porazit
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacko	k1gNnPc4	Jacko
a	a	k8xC	a
proto	proto	k8xC	proto
hrdinu	hrdina	k1gMnSc4	hrdina
příběhu	příběh	k1gInSc3	příběh
vyšlou	vyslat	k5eAaPmIp3nP	vyslat
do	do	k7c2	do
Tundra	tundra	k1gFnSc1	tundra
Expressu	express	k1gInSc2	express
najít	najít	k5eAaPmF	najít
dalšího	další	k2eAgMnSc4d1	další
původního	původní	k2eAgMnSc4d1	původní
Vault	Vault	k2eAgMnSc1d1	Vault
huntera	hunter	k1gMnSc4	hunter
Mordecaie	Mordecaie	k1gFnSc2	Mordecaie
a	a	k8xC	a
Tiny	Tina	k1gFnSc2	Tina
Tinu	Tina	k1gFnSc4	Tina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
přepade	přepad	k1gInSc5	přepad
hrdina	hrdina	k1gMnSc1	hrdina
vlak	vlak	k1gInSc1	vlak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
ukrytý	ukrytý	k2eAgInSc1d1	ukrytý
Klíč	klíč	k1gInSc1	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
najde	najít	k5eAaPmIp3nS	najít
nové	nový	k2eAgNnSc1d1	nové
ochranné	ochranný	k2eAgNnSc1d1	ochranné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
vylepšit	vylepšit	k5eAaPmF	vylepšit
ochranu	ochrana	k1gFnSc4	ochrana
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
proti	proti	k7c3	proti
velkým	velký	k2eAgInPc3d1	velký
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
aktivaci	aktivace	k1gFnSc6	aktivace
se	se	k3xPyFc4	se
ale	ale	k9	ale
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Angel	Angela	k1gFnPc2	Angela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
na	na	k7c4	na
Jackův	Jackův	k2eAgInSc4d1	Jackův
příkaz	příkaz	k1gInSc4	příkaz
ochranu	ochrana	k1gFnSc4	ochrana
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
úplně	úplně	k6eAd1	úplně
vypnout	vypnout	k5eAaPmF	vypnout
a	a	k8xC	a
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Sanctuary	Sanctuara	k1gFnPc4	Sanctuara
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nuceno	nucen	k2eAgNnSc1d1	nuceno
využít	využít	k5eAaPmF	využít
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
útokům	útok	k1gInPc3	útok
ubránilo	ubránit	k5eAaPmAgNnS	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
útoku	útok	k1gInSc2	útok
je	být	k5eAaImIp3nS	být
hrdina	hrdina	k1gMnSc1	hrdina
omylem	omylem	k6eAd1	omylem
Lillith	Lillith	k1gInSc4	Lillith
přenesen	přenést	k5eAaPmNgInS	přenést
úplně	úplně	k6eAd1	úplně
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
lokace	lokace	k1gFnSc2	lokace
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
najít	najít	k5eAaPmF	najít
Fast	Fast	k2eAgInSc1d1	Fast
Travel	Travel	k1gInSc1	Travel
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
znova	znova	k6eAd1	znova
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
se	se	k3xPyFc4	se
nejde	jít	k5eNaImIp3nS	jít
do	do	k7c2	do
města	město	k1gNnSc2	město
dostat	dostat	k5eAaPmF	dostat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
teleportem	teleport	k1gInSc7	teleport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
opět	opět	k6eAd1	opět
zkontaktuje	zkontaktovat	k5eAaPmIp3nS	zkontaktovat
Angel	angel	k1gMnSc1	angel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
snaží	snažit	k5eAaImIp3nS	snažit
odčinit	odčinit	k5eAaPmF	odčinit
Jackovy	Jackův	k2eAgInPc4d1	Jackův
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
musí	muset	k5eAaImIp3nP	muset
poslouchat	poslouchat	k5eAaImF	poslouchat
a	a	k8xC	a
sdělí	sdělit	k5eAaPmIp3nP	sdělit
Rolandovi	Rolandův	k2eAgMnPc1d1	Rolandův
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zavřený	zavřený	k2eAgInSc1d1	zavřený
v	v	k7c6	v
obřím	obří	k2eAgInSc6d1	obří
Bunkru	bunkr	k1gInSc6	bunkr
napájen	napájen	k2eAgMnSc1d1	napájen
její	její	k3xOp3gFnSc7	její
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
dostat	dostat	k5eAaPmF	dostat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
si	se	k3xPyFc3	se
sehnat	sehnat	k5eAaPmF	sehnat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Jackových	Jackův	k2eAgMnPc2d1	Jackův
dvojníků	dvojník	k1gMnPc2	dvojník
a	a	k8xC	a
modulátor	modulátor	k1gInSc1	modulátor
Jackova	Jackův	k2eAgInSc2d1	Jackův
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Bunkr	bunkr	k1gInSc1	bunkr
otevře	otevřít	k5eAaPmIp3nS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
Oportunity	oportunita	k1gFnSc2	oportunita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c2	za
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
pomoci	pomoc	k1gFnSc2	pomoc
dočasně	dočasně	k6eAd1	dočasně
začne	začít	k5eAaPmIp3nS	začít
mluvit	mluvit	k5eAaImF	mluvit
jako	jako	k9	jako
Handsome	Handsom	k1gInSc5	Handsom
Jack	Jack	k1gMnSc1	Jack
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
otevření	otevření	k1gNnSc1	otevření
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
k	k	k7c3	k
Bunkru	bunkr	k1gInSc3	bunkr
vedou	vést	k5eAaImIp3nP	vést
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
jdou	jít	k5eAaImIp3nP	jít
otevřít	otevřít	k5eAaPmF	otevřít
pouze	pouze	k6eAd1	pouze
zevnitř	zevnitř	k6eAd1	zevnitř
hyperionským	hyperionský	k2eAgInSc7d1	hyperionský
robotem	robot	k1gInSc7	robot
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
obstarat	obstarat	k5eAaPmF	obstarat
Claptrap	Claptrap	k1gMnSc1	Claptrap
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
bohužel	bohužel	k9	bohužel
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
vylepšení	vylepšení	k1gNnSc1	vylepšení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
Mordecai	Mordecai	k1gNnPc7	Mordecai
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
bohužel	bohužel	k6eAd1	bohužel
byla	být	k5eAaImAgFnS	být
Handsome	Handsom	k1gInSc5	Handsom
Jackem	Jacek	k1gMnSc7	Jacek
unesena	unesen	k2eAgFnSc1d1	unesena
Bloodwing	Bloodwing	k1gInSc4	Bloodwing
do	do	k7c2	do
Hyperionské	Hyperionský	k2eAgFnSc2d1	Hyperionský
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dělají	dělat	k5eAaImIp3nP	dělat
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
eridiové	eridiový	k2eAgInPc4d1	eridiový
pokusy	pokus	k1gInPc1	pokus
a	a	k8xC	a
hrdina	hrdina	k1gMnSc1	hrdina
jí	on	k3xPp3gFnSc2	on
první	první	k4xOgMnSc1	první
musí	muset	k5eAaImIp3nS	muset
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
eridium	eridium	k1gNnSc1	eridium
udělalo	udělat	k5eAaPmAgNnS	udělat
z	z	k7c2	z
Bloodwing	Bloodwing	k1gInSc1	Bloodwing
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgFnSc4d2	veliký
vraždící	vraždící	k2eAgFnSc4d1	vraždící
stvůru	stvůra	k1gFnSc4	stvůra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
přestává	přestávat	k5eAaImIp3nS	přestávat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
nepřáteli	nepřítel	k1gMnPc7	nepřítel
a	a	k8xC	a
Mordecai	Mordeca	k1gFnSc2	Mordeca
hrdinu	hrdina	k1gMnSc4	hrdina
prosí	prosit	k5eAaImIp3nS	prosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
nezabíjel	zabíjet	k5eNaImAgMnS	zabíjet
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
jí	on	k3xPp3gFnSc3	on
uspal	uspat	k5eAaPmAgMnS	uspat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
Jack	Jack	k1gMnSc1	Jack
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
nepovedený	povedený	k2eNgInSc4d1	nepovedený
pokus	pokus	k1gInSc4	pokus
mutace	mutace	k1gFnSc1	mutace
a	a	k8xC	a
Bloodwing	Bloodwing	k1gInSc1	Bloodwing
nechá	nechat	k5eAaPmIp3nS	nechat
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
hrdina	hrdina	k1gMnSc1	hrdina
získá	získat	k5eAaPmIp3nS	získat
vylepšení	vylepšení	k1gNnSc4	vylepšení
pro	pro	k7c4	pro
Claptrapa	Claptrap	k1gMnSc4	Claptrap
a	a	k8xC	a
chybí	chybět	k5eAaImIp3nS	chybět
poslední	poslední	k2eAgInSc4d1	poslední
krok	krok	k1gInSc4	krok
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
Bunkru	bunkr	k1gInSc2	bunkr
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
získání	získání	k1gNnSc4	získání
pomoci	pomoc	k1gFnSc2	pomoc
od	od	k7c2	od
místního	místní	k2eAgMnSc2d1	místní
velitele	velitel	k1gMnSc2	velitel
nájezdníků	nájezdník	k1gMnPc2	nájezdník
Bricka	Bricko	k1gNnSc2	Bricko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
vrtulníky	vrtulník	k1gInPc1	vrtulník
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
při	při	k7c6	při
boji	boj	k1gInSc6	boj
hodit	hodit	k5eAaPmF	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
hrdina	hrdina	k1gMnSc1	hrdina
zabije	zabít	k5eAaPmIp3nS	zabít
polovinu	polovina	k1gFnSc4	polovina
jeho	jeho	k3xOp3gMnPc2	jeho
tupých	tupý	k2eAgMnPc2d1	tupý
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
získá	získat	k5eAaPmIp3nS	získat
Brickův	Brickův	k2eAgInSc4d1	Brickův
respekt	respekt	k1gInSc4	respekt
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
už	už	k6eAd1	už
ho	on	k3xPp3gMnSc4	on
neoslovuje	oslovovat	k5eNaImIp3nS	oslovovat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
Slab	slabit	k5eAaImRp2nS	slabit
(	(	kIx(	(
<g/>
Sekáč	sekáč	k1gInSc1	sekáč
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
pomoct	pomoct	k5eAaPmF	pomoct
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Bunkr	bunkr	k1gInSc4	bunkr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
do	do	k7c2	do
Bunkru	bunkr	k1gInSc2	bunkr
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
,	,	kIx,	,
poprosí	poprosit	k5eAaPmIp3nS	poprosit
ho	on	k3xPp3gMnSc4	on
Angel	angel	k1gMnSc1	angel
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c4	za
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc3	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacek	k1gInSc2	Jacek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
s	s	k7c7	s
klidem	klid	k1gInSc7	klid
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
svobodná	svobodný	k2eAgFnSc1d1	svobodná
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zjeví	zjevit	k5eAaPmIp3nS	zjevit
Jack	Jack	k1gInSc1	Jack
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zabije	zabít	k5eAaPmIp3nS	zabít
Rolanda	Rolanda	k1gFnSc1	Rolanda
a	a	k8xC	a
protože	protože	k8xS	protože
stále	stále	k6eAd1	stále
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
sirénu	siréna	k1gFnSc4	siréna
na	na	k7c4	na
probuzení	probuzení	k1gNnSc4	probuzení
válečníka	válečník	k1gMnSc2	válečník
a	a	k8xC	a
Angel	Angela	k1gFnPc2	Angela
je	být	k5eAaImIp3nS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
unese	unést	k5eAaPmIp3nS	unést
Lillith	Lillith	k1gInSc4	Lillith
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
jí	on	k3xPp3gFnSc3	on
věznit	věznit	k5eAaImF	věznit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
předtím	předtím	k6eAd1	předtím
Angel	angel	k1gMnSc1	angel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
nabourá	nabourat	k5eAaPmIp3nS	nabourat
do	do	k7c2	do
Hyperionské	Hyperionský	k2eAgFnSc2d1	Hyperionský
databanky	databanka	k1gFnSc2	databanka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
eridium	eridium	k1gNnSc1	eridium
posíláno	posílat	k5eAaImNgNnS	posílat
Hrdinského	hrdinský	k2eAgInSc2d1	hrdinský
průsmyku	průsmyk	k1gInSc2	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
nacházet	nacházet	k5eAaImF	nacházet
spící	spící	k2eAgMnSc1d1	spící
Válečník	válečník	k1gMnSc1	válečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tedy	tedy	k9	tedy
vydá	vydat	k5eAaPmIp3nS	vydat
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
a	a	k8xC	a
poté	poté	k6eAd1	poté
s	s	k7c7	s
Válečníkem	válečník	k1gMnSc7	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
poražení	poražení	k1gNnSc6	poražení
může	moct	k5eAaImIp3nS	moct
hrdina	hrdina	k1gMnSc1	hrdina
konečně	konečně	k6eAd1	konečně
zabít	zabít	k5eAaPmF	zabít
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacko	k1gNnPc4	Jacko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
že	že	k8xS	že
Jacka	Jacka	k1gFnSc1	Jacka
nezabije	zabít	k5eNaPmIp3nS	zabít
udělá	udělat	k5eAaPmIp3nS	udělat
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
hráče	hráč	k1gMnSc4	hráč
Lilith	Lilith	k1gInSc4	Lilith
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
hru	hra	k1gFnSc4	hra
navazuje	navazovat	k5eAaImIp3nS	navazovat
několik	několik	k4yIc1	několik
datadisků	datadisk	k1gInPc2	datadisk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dějově	dějově	k6eAd1	dějově
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
po	po	k7c6	po
poražení	poražení	k1gNnSc3	poražení
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacko	k1gNnPc1	Jacko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Easter	Easter	k1gInSc1	Easter
Eggy	Egga	k1gFnSc2	Egga
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
mnoho	mnoho	k4c1	mnoho
Easter	Eastra	k1gFnPc2	Eastra
Eggů	Egg	k1gMnPc2	Egg
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
drobnosti	drobnost	k1gFnPc1	drobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pobavit	pobavit	k5eAaPmF	pobavit
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc4d1	jiné
vám	vy	k3xPp2nPc3	vy
můžou	můžou	k?	můžou
přinést	přinést	k5eAaPmF	přinést
nový	nový	k2eAgMnSc1d1	nový
skin	skin	k1gMnSc1	skin
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
nebo	nebo	k8xC	nebo
i	i	k9	i
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
-	-	kIx~	-
Jakmile	jakmile	k8xS	jakmile
vstoupíte	vstoupit	k5eAaPmIp2nP	vstoupit
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
lokace	lokace	k1gFnSc2	lokace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Eridium	Eridium	k1gNnSc4	Eridium
Blight	Blighta	k1gFnPc2	Blighta
(	(	kIx(	(
<g/>
Eridiová	Eridiový	k2eAgFnSc1d1	Eridiový
zhouba	zhouba	k1gFnSc1	zhouba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrátíte	vrátit	k5eAaPmIp2nP	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Claptrapovy	Claptrapův	k2eAgFnSc2d1	Claptrapův
skrýše	skrýš	k1gFnSc2	skrýš
<g/>
,	,	kIx,	,
z	z	k7c2	z
krbu	krb	k1gInSc2	krb
vezmete	vzít	k5eAaPmIp2nP	vzít
Prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
poté	poté	k6eAd1	poté
musíte	muset	k5eAaImIp2nP	muset
pěšky	pěšky	k6eAd1	pěšky
donést	donést	k5eAaPmF	donést
až	až	k9	až
do	do	k7c2	do
Eridium	Eridium	k1gNnSc1	Eridium
Blight	Blight	k1gInSc4	Blight
<g/>
,	,	kIx,	,
nesmíte	smět	k5eNaImIp2nP	smět
použít	použít	k5eAaPmF	použít
Fast	Fast	k2eAgInSc4d1	Fast
travel	travel	k1gInSc4	travel
station	station	k1gInSc1	station
ani	ani	k8xC	ani
Catch-a-ride	Catchid	k1gInSc5	Catch-a-rid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Eridium	Eridium	k1gNnSc4	Eridium
Blight	Blighta	k1gFnPc2	Blighta
projdete	projít	k5eAaPmIp2nP	projít
na	na	k7c6	na
SZ	SZ	kA	SZ
až	až	k9	až
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nájezdník	nájezdník	k1gMnSc1	nájezdník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
Gluma	Gluma	k1gFnSc1	Gluma
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vás	vy	k3xPp2nPc4	vy
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
,	,	kIx,	,
vám	vy	k3xPp2nPc3	vy
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
Prsten	prsten	k1gInSc1	prsten
a	a	k8xC	a
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skok	k1gInSc7	skok
do	do	k7c2	do
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Načež	načež	k6eAd1	načež
přiletí	přiletět	k5eAaPmIp3nP	přiletět
tři	tři	k4xCgMnPc1	tři
obří	obří	k2eAgMnPc1d1	obří
rakkové	rakek	k1gMnPc1	rakek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vám	vy	k3xPp2nPc3	vy
hodí	hodit	k5eAaPmIp3nS	hodit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
bedny	bedna	k1gFnSc2	bedna
s	s	k7c7	s
odměnou	odměna	k1gFnSc7	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Gluma	Glumum	k1gNnPc1	Glumum
můžete	moct	k5eAaImIp2nP	moct
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
vám	vy	k3xPp2nPc3	vy
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
nový	nový	k2eAgMnSc1d1	nový
skin	skin	k1gMnSc1	skin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Double	double	k1gInSc1	double
rainbow	rainbow	k?	rainbow
-	-	kIx~	-
V	v	k7c4	v
Highlands	Highlands	k1gInSc4	Highlands
(	(	kIx(	(
<g/>
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
podívat	podívat	k5eAaPmF	podívat
na	na	k7c4	na
dvojtou	dvojtý	k2eAgFnSc4d1	dvojtá
duhu	duha	k1gFnSc4	duha
a	a	k8xC	a
poslechnout	poslechnout	k5eAaPmF	poslechnout
si	se	k3xPyFc3	se
srdceryvný	srdceryvný	k2eAgInSc4d1	srdceryvný
komentář	komentář	k1gInSc4	komentář
Handsome	Handsom	k1gInSc5	Handsom
Jacka	Jacek	k1gMnSc4	Jacek
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
přírodní	přírodní	k2eAgInSc4d1	přírodní
úkaz	úkaz	k1gInSc4	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
když	když	k8xS	když
půjdete	jít	k5eAaImIp2nP	jít
do	do	k7c2	do
Hyperionského	Hyperionský	k2eAgNnSc2d1	Hyperionský
sídla	sídlo	k1gNnSc2	sídlo
s	s	k7c7	s
kánonem	kánon	k1gInSc7	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
dělem	dělo	k1gNnSc7	dělo
se	se	k3xPyFc4	se
vydáte	vydat	k5eAaPmIp2nP	vydat
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
skočíte	skočit	k5eAaPmIp2nP	skočit
k	k	k7c3	k
malému	malý	k2eAgInSc3d1	malý
stanu	stan	k1gInSc3	stan
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
objeví	objevit	k5eAaPmIp3nS	objevit
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
duha	duha	k1gFnSc1	duha
za	za	k7c4	za
Jackova	Jackův	k2eAgNnPc4d1	Jackovo
povzbuzování	povzbuzování	k1gNnPc4	povzbuzování
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
scéna	scéna	k1gFnSc1	scéna
je	být	k5eAaImIp3nS	být
parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
známé	známý	k2eAgNnSc4d1	známé
virální	virální	k2eAgNnSc4d1	virální
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nějaký	nějaký	k3yIgMnSc1	nějaký
muž	muž	k1gMnSc1	muž
ztrapňuje	ztrapňovat	k5eAaImIp3nS	ztrapňovat
rozplýváním	rozplývání	k1gNnSc7	rozplývání
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
duhou	duha	k1gFnSc7	duha
<g/>
.	.	kIx.	.
</s>
<s>
Stejnému	stejný	k2eAgNnSc3d1	stejné
videu	video	k1gNnSc3	video
s	s	k7c7	s
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
duhou	duha	k1gFnSc7	duha
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
komediální	komediální	k2eAgInSc4d1	komediální
pořad	pořad	k1gInSc4	pořad
Kids	Kids	k1gInSc4	Kids
react	react	k5eAaPmF	react
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
facebook	facebook	k1gInSc4	facebook
-	-	kIx~	-
během	během	k7c2	během
zadání	zadání	k1gNnSc2	zadání
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jdete	jít	k5eAaImIp2nP	jít
zachránit	zachránit	k5eAaPmF	zachránit
Bloodwing	Bloodwing	k1gInSc4	Bloodwing
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
v	v	k7c4	v
Crimson	Crimson	k1gInSc4	Crimson
raiders	raiders	k6eAd1	raiders
v	v	k7c4	v
Sanctuary	Sanctuara	k1gFnPc4	Sanctuara
podívat	podívat	k5eAaPmF	podívat
na	na	k7c4	na
obrazovky	obrazovka	k1gFnPc4	obrazovka
u	u	k7c2	u
stropu	strop	k1gInSc2	strop
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vždycky	vždycky	k6eAd1	vždycky
promítají	promítat	k5eAaImIp3nP	promítat
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
kamer	kamera	k1gFnPc2	kamera
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Tentokráte	Tentokráte	k?	Tentokráte
se	se	k3xPyFc4	se
však	však	k9	však
bude	být	k5eAaImBp3nS	být
místo	místo	k7c2	místo
nudných	nudný	k2eAgInPc2d1	nudný
záběrů	záběr	k1gInPc2	záběr
na	na	k7c4	na
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
ulici	ulice	k1gFnSc4	ulice
ukázán	ukázán	k2eAgInSc4d1	ukázán
Rolandův	Rolandův	k2eAgInSc4d1	Rolandův
facebook	facebook	k1gInSc4	facebook
se	s	k7c7	s
zprávami	zpráva	k1gFnPc7	zpráva
od	od	k7c2	od
Lillith	Lillitha	k1gFnPc2	Lillitha
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
vault	vaultum	k1gNnPc2	vaultum
huntery	hunter	k1gInPc7	hunter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
má	mít	k5eAaImIp3nS	mít
Roland	Roland	k1gInSc1	Roland
v	v	k7c6	v
přátelích	přítel	k1gMnPc6	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krásná	krásný	k2eAgFnSc1d1	krásná
kočka	kočka	k1gFnSc1	kočka
s	s	k7c7	s
errorovou	errorův	k2eAgFnSc7d1	errorův
hláškou	hláška	k1gFnSc7	hláška
-	-	kIx~	-
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
stejné	stejný	k2eAgFnSc2d1	stejná
mise	mise	k1gFnSc2	mise
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
taky	taky	k6eAd1	taky
pokochat	pokochat	k5eAaPmF	pokochat
obrazovkami	obrazovka	k1gFnPc7	obrazovka
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
sexy	sex	k1gInPc7	sex
<g/>
"	"	kIx"	"
kočka	kočka	k1gFnSc1	kočka
v	v	k7c6	v
plavkách	plavka	k1gFnPc6	plavka
a	a	k8xC	a
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
errorová	errorová	k1gFnSc1	errorová
hláška	hláška	k1gFnSc1	hláška
o	o	k7c6	o
nefunkčních	funkční	k2eNgFnPc6d1	nefunkční
kamerách	kamera	k1gFnPc6	kamera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
<g/>
7	[number]	k4	7
<g/>
en	en	k?	en
-	-	kIx~	-
V	v	k7c6	v
Dustu	Dust	k1gInSc6	Dust
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
přehrát	přehrát	k5eAaPmF	přehrát
svou	svůj	k3xOyFgFnSc4	svůj
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
scénu	scéna	k1gFnSc4	scéna
z	z	k7c2	z
filmu	film	k1gInSc2	film
Se	s	k7c7	s
<g/>
7	[number]	k4	7
<g/>
en	en	k?	en
s	s	k7c7	s
názvem	název	k1gInSc7	název
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
in	in	k?	in
the	the	k?	the
box	box	k1gInSc1	box
<g/>
?!!!	?!!!	k?	?!!!
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
zamířit	zamířit	k5eAaPmF	zamířit
do	do	k7c2	do
sídla	sídlo	k1gNnSc2	sídlo
nájezdnických	nájezdnický	k2eAgFnPc2d1	nájezdnická
poletuch	poletucha	k1gFnPc2	poletucha
po	po	k7c6	po
skočení	skočení	k1gNnSc6	skočení
autem	aut	k1gInSc7	aut
přes	přes	k7c4	přes
kamenný	kamenný	k2eAgInSc4d1	kamenný
hopík	hopík	k1gInSc4	hopík
se	se	k3xPyFc4	se
vydáte	vydat	k5eAaPmIp2nP	vydat
vpravo	vpravo	k6eAd1	vpravo
až	až	k9	až
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
útesu	útes	k1gInSc2	útes
a	a	k8xC	a
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
malou	malý	k2eAgFnSc4d1	malá
chaloupku	chaloupka	k1gFnSc4	chaloupka
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
můžete	moct	k5eAaImIp2nP	moct
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bude	být	k5eAaImBp3nS	být
nebo	nebo	k8xC	nebo
nebude	být	k5eNaImBp3nS	být
v	v	k7c6	v
krabici	krabice	k1gFnSc6	krabice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
-	-	kIx~	-
Během	během	k7c2	během
hry	hra	k1gFnSc2	hra
vás	vy	k3xPp2nPc2	vy
Scooter	Scooter	k1gMnSc1	Scooter
poprosí	poprosit	k5eAaPmIp3nS	poprosit
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
mu	on	k3xPp3gMnSc3	on
přivedli	přivést	k5eAaPmAgMnP	přivést
zpátky	zpátky	k6eAd1	zpátky
jeho	jeho	k3xOp3gFnSc4	jeho
holku	holka	k1gFnSc4	holka
Laney	Lanea	k1gFnSc2	Lanea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
jako	jako	k9	jako
kanibal	kanibal	k1gMnSc1	kanibal
s	s	k7c7	s
krysami	krysa	k1gFnPc7	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jí	on	k3xPp3gFnSc3	on
nalákáte	nalákat	k5eAaPmIp2nP	nalákat
na	na	k7c4	na
květiny	květina	k1gFnPc4	květina
a	a	k8xC	a
pizzu	pizza	k1gFnSc4	pizza
vyleze	vylézt	k5eAaPmIp3nS	vylézt
Laney	Lane	k1gMnPc4	Lane
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vás	vy	k3xPp2nPc4	vy
zabila	zabít	k5eAaPmAgFnS	zabít
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
7	[number]	k4	7
malých	malý	k2eAgInPc2d1	malý
krysích	krysí	k2eAgInPc2d1	krysí
liliputů	liliput	k1gInPc2	liliput
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
domácnost	domácnost	k1gFnSc1	domácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Želví	želví	k2eAgMnSc1d1	želví
nindža	nindža	k1gMnSc1	nindža
-	-	kIx~	-
v	v	k7c4	v
Bloodshot	Bloodshot	k1gInSc4	Bloodshot
stronghold	stronghold	k6eAd1	stronghold
(	(	kIx(	(
<g/>
Pevnost	pevnost	k1gFnSc1	pevnost
Krvounů	Krvoun	k1gInPc2	Krvoun
<g/>
)	)	kIx)	)
můžete	moct	k5eAaImIp2nP	moct
nalákat	nalákat	k5eAaPmF	nalákat
na	na	k7c4	na
pizzu	pizza	k1gFnSc4	pizza
4	[number]	k4	4
krysy	krysa	k1gFnSc2	krysa
<g/>
:	:	kIx,	:
Dona	Don	k1gMnSc4	Don
<g/>
,	,	kIx,	,
Mikyho	Miky	k1gMnSc4	Miky
<g/>
,	,	kIx,	,
Rapha	Raph	k1gMnSc4	Raph
a	a	k8xC	a
Lea	Leo	k1gMnSc4	Leo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minecraft	Minecraft	k1gInSc1	Minecraft
-	-	kIx~	-
v	v	k7c6	v
Caustic	Caustice	k1gFnPc2	Caustice
caverns	cavernsa	k1gFnPc2	cavernsa
(	(	kIx(	(
<g/>
Leptavé	leptavý	k2eAgFnSc2d1	leptavá
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
)	)	kIx)	)
půjdete	jít	k5eAaImIp2nP	jít
až	až	k9	až
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
začínáte	začínat	k5eAaImIp2nP	začínat
s	s	k7c7	s
misí	mise	k1gFnSc7	mise
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
tlačíte	tlačit	k5eAaImIp2nP	tlačit
důlní	důlní	k2eAgInPc4d1	důlní
vozík	vozík	k1gInSc4	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
podíváte	podívat	k5eAaPmIp2nP	podívat
na	na	k7c4	na
vozík	vozík	k1gInSc4	vozík
<g/>
,	,	kIx,	,
celé	celá	k1gFnSc3	celá
Caustic	Caustice	k1gFnPc2	Caustice
caverns	caverns	k6eAd1	caverns
za	za	k7c7	za
sebou	se	k3xPyFc7	se
a	a	k8xC	a
půjdete	jít	k5eAaImIp2nP	jít
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
od	od	k7c2	od
pohledu	pohled	k1gInSc2	pohled
tam	tam	k6eAd1	tam
vypadají	vypadat	k5eAaPmIp3nP	vypadat
kameny	kámen	k1gInPc1	kámen
podivně	podivně	k6eAd1	podivně
hranatě	hranatě	k6eAd1	hranatě
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	on	k3xPp3gNnSc4	on
přelezete	přelézt	k5eAaPmIp2nP	přelézt
<g/>
,	,	kIx,	,
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
v	v	k7c6	v
klasicky	klasicky	k6eAd1	klasicky
minecraftovém	minecraftový	k2eAgInSc6d1	minecraftový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
vymlátíte	vymlátit	k5eAaPmIp2nP	vymlátit
ručně	ručně	k6eAd1	ručně
kameny	kámen	k1gInPc1	kámen
a	a	k8xC	a
dostanete	dostat	k5eAaPmIp2nP	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zabití	zabití	k1gNnSc6	zabití
minecraftových	minecraftův	k2eAgMnPc2d1	minecraftův
nepřátel	nepřítel	k1gMnPc2	nepřítel
dostanete	dostat	k5eAaPmIp2nP	dostat
krásnou	krásný	k2eAgFnSc4d1	krásná
hranatou	hranatý	k2eAgFnSc4d1	hranatá
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
kostkovaný	kostkovaný	k2eAgMnSc1d1	kostkovaný
skin	skin	k1gMnSc1	skin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dabing	dabing	k1gInSc4	dabing
==	==	k?	==
</s>
</p>
<p>
<s>
Dameon	Dameon	k1gInSc1	Dameon
Clarke	Clark	k1gInSc2	Clark
-	-	kIx~	-
Handsome	Handsom	k1gInSc5	Handsom
Jack	Jack	k1gMnSc1	Jack
</s>
</p>
<p>
<s>
Jennifer	Jennifer	k1gMnSc1	Jennifer
Green	Green	k2eAgMnSc1d1	Green
-	-	kIx~	-
Angel	angel	k1gMnSc1	angel
</s>
</p>
<p>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Lloyd	Lloyd	k1gMnSc1	Lloyd
-	-	kIx~	-
Roland	Roland	k1gInSc1	Roland
</s>
</p>
<p>
<s>
Colleen	Colleen	k2eAgInSc1d1	Colleen
Clinkenbeard	Clinkenbeard	k1gInSc1	Clinkenbeard
-	-	kIx~	-
Lillith	Lillith	k1gInSc1	Lillith
</s>
</p>
<p>
<s>
Jason	Jason	k1gMnSc1	Jason
Liebrecht	Liebrecht	k1gMnSc1	Liebrecht
-	-	kIx~	-
Mordecai	Mordecai	k1gNnSc1	Mordecai
</s>
</p>
<p>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Mauldin	Mauldin	k2eAgMnSc1d1	Mauldin
-	-	kIx~	-
Brick	Brick	k1gMnSc1	Brick
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Eddings	Eddings	k1gInSc1	Eddings
-	-	kIx~	-
Claptrap	Claptrap	k1gInSc1	Claptrap
</s>
</p>
<p>
<s>
Michaael	Michaael	k1gMnSc1	Michaael
Turner	turner	k1gMnSc1	turner
-	-	kIx~	-
Zero	Zero	k1gMnSc1	Zero
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Swasey	Swasea	k1gFnSc2	Swasea
-	-	kIx~	-
Salvador	Salvador	k1gInSc1	Salvador
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
McCollum	McCollum	k1gInSc1	McCollum
-	-	kIx~	-
Axton	Axton	k1gInSc1	Axton
</s>
</p>
<p>
<s>
Martha	Martha	k1gFnSc1	Martha
Harms	Harmsa	k1gFnPc2	Harmsa
-	-	kIx~	-
Maya	Maya	k?	Maya
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Borderlands	Borderlands	k6eAd1	Borderlands
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
Borderlands	Borderlands	k1gInSc1	Borderlands
2	[number]	k4	2
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
na	na	k7c4	na
Borderlands	Borderlands	k1gInSc4	Borderlands
2	[number]	k4	2
</s>
</p>
<p>
<s>
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
fanoušků	fanoušek	k1gMnPc2	fanoušek
</s>
</p>
