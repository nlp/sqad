<s>
Páv	páv	k1gMnSc1	páv
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
ptáků	pták	k1gMnPc2	pták
řazený	řazený	k2eAgMnSc1d1	řazený
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
bažantovitých	bažantovitý	k2eAgMnPc2d1	bažantovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
druhy	druh	k1gMnPc4	druh
páv	páv	k1gMnSc1	páv
korunkatý	korunkatý	k2eAgMnSc1d1	korunkatý
a	a	k8xC	a
páv	páv	k1gMnSc1	páv
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
páv	páv	k1gMnSc1	páv
se	se	k3xPyFc4	se
česky	česky	k6eAd1	česky
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
rodu	rod	k1gInSc2	rod
Afropavo	Afropava	k1gFnSc5	Afropava
<g/>
,	,	kIx,	,
páv	páv	k1gMnSc1	páv
konžský	konžský	k2eAgMnSc1d1	konžský
(	(	kIx(	(
<g/>
Afropavo	Afropava	k1gFnSc5	Afropava
congensis	congensis	k1gFnSc3	congensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Páv	páv	k1gMnSc1	páv
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
páv	páv	k1gMnSc1	páv
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Pavo	Pavo	k6eAd1	Pavo
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
