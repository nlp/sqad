<s>
Člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
Homo	Homo	k1gMnSc1	Homo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hominidi	hominid	k1gMnPc1	hominid
(	(	kIx(	(
<g/>
Hominidae	Hominidae	k1gInSc1	Hominidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
jediný	jediný	k2eAgInSc1d1	jediný
současně	současně	k6eAd1	současně
žijící	žijící	k2eAgInSc1d1	žijící
druh	druh	k1gInSc1	druh
člověka	člověk	k1gMnSc2	člověk
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
moudrý	moudrý	k2eAgMnSc1d1	moudrý
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
sapiens	sapiens	k6eAd1	sapiens
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
blízcí	blízký	k2eAgMnPc1d1	blízký
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
mládě	mládě	k1gNnSc4	mládě
člověka	člověk	k1gMnSc2	člověk
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgInPc1	první
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
nejstarší	starý	k2eAgInSc1d3	nejstarší
paleolit	paleolit	k1gInSc1	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
člověka	člověk	k1gMnSc2	člověk
moudrého	moudrý	k2eAgMnSc2d1	moudrý
<g/>
,	,	kIx,	,
vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
vzpřímený	vzpřímený	k2eAgMnSc1d1	vzpřímený
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
před	před	k7c7	před
50	[number]	k4	50
000	[number]	k4	000
až	až	k9	až
35	[number]	k4	35
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
Homo	Homo	k6eAd1	Homo
neanderthalensis	neanderthalensis	k1gFnSc4	neanderthalensis
asi	asi	k9	asi
před	před	k7c7	před
30	[number]	k4	30
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
Homo	Homo	k6eAd1	Homo
floresiensis	floresiensis	k1gInSc1	floresiensis
však	však	k9	však
až	až	k9	až
před	před	k7c7	před
12	[number]	k4	12
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iwo	Iwo	k1gFnSc6	Iwo
Eleru	Eler	k1gInSc2	Eler
(	(	kIx(	(
<g/>
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
také	také	k9	také
archaická	archaický	k2eAgFnSc1d1	archaická
forma	forma	k1gFnSc1	forma
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
stará	starat	k5eAaImIp3nS	starat
pouze	pouze	k6eAd1	pouze
13	[number]	k4	13
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Evoluce	evoluce	k1gFnSc2	evoluce
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
:	:	kIx,	:
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
naledi	naled	k1gMnPc1	naled
†	†	k?	†
<g/>
Homo	Homo	k6eAd1	Homo
habilis	habilis	k1gFnSc2	habilis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
zručný	zručný	k2eAgMnSc1d1	zručný
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc1	Homo
rudolfensis	rudolfensis	k1gFnSc1	rudolfensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
východoafrický	východoafrický	k2eAgMnSc1d1	východoafrický
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
ergaster	ergaster	k1gMnSc1	ergaster
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
dělný	dělný	k2eAgMnSc1d1	dělný
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
<g />
.	.	kIx.	.
</s>
<s>
vzpřímený	vzpřímený	k2eAgInSc1d1	vzpřímený
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc1	Homo
floresiensis	floresiensis	k1gFnSc1	floresiensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
floreský	floreský	k2eAgMnSc1d1	floreský
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
antecessor	antecessor	k1gMnSc1	antecessor
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc4	Homo
heidelbergensis	heidelbergensis	k1gFnSc2	heidelbergensis
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
Homo	Homo	k6eAd1	Homo
erectus	erectus	k1gMnSc1	erectus
heidelbergensis	heidelbergensis	k1gFnSc2	heidelbergensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
heidelberský	heidelberský	k2eAgMnSc1d1	heidelberský
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc1	Homo
neanderthalensis	neanderthalensis	k1gFnSc1	neanderthalensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
neandrtálský	neandrtálský	k2eAgMnSc1d1	neandrtálský
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
neanderthalensis	neanderthalensis	k1gFnPc1	neanderthalensis
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc1	Homo
rhodesiensis	rhodesiensis	k1gFnSc1	rhodesiensis
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
rhodéský	rhodéský	k1gMnSc1	rhodéský
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
georgicus	georgicus	k1gMnSc1	georgicus
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
<g/>
)	)	kIx)	)
Homo	Homo	k1gNnSc1	Homo
sapiens	sapiensa	k1gFnPc2	sapiensa
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
)	)	kIx)	)
Některé	některý	k3yIgInPc1	některý
nálezy	nález	k1gInPc1	nález
popsané	popsaný	k2eAgInPc1d1	popsaný
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgInPc1d1	samostatný
druhy	druh	k1gInPc1	druh
zatím	zatím	k6eAd1	zatím
nedosáhly	dosáhnout	k5eNaPmAgInP	dosáhnout
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
uznání	uznání	k1gNnSc2	uznání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
cepranensis	cepranensis	k1gFnSc2	cepranensis
či	či	k8xC	či
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc1	Homo
gautengensis	gautengensis	k1gFnSc1	gautengensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
byly	být	k5eAaImAgInP	být
včleněny	včlenit	k5eAaPmNgInP	včlenit
do	do	k7c2	do
druhů	druh	k1gInPc2	druh
jiných	jiný	k1gMnPc2	jiný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
†	†	k?	†
<g/>
Homo	Homo	k1gMnSc1	Homo
pekinensis	pekinensis	k1gFnSc2	pekinensis
a	a	k8xC	a
†	†	k?	†
<g/>
Homo	Homo	k1gNnSc1	Homo
soloensis	soloensis	k1gFnSc2	soloensis
do	do	k7c2	do
H.	H.	kA	H.
erectus	erectus	k1gInSc1	erectus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
uznáni	uznán	k2eAgMnPc1d1	uznán
ani	ani	k8xC	ani
neandrtálcům	neandrtálec	k1gMnPc3	neandrtálec
blízcí	blízký	k2eAgMnPc1d1	blízký
denisované	denisovaný	k2eAgFnPc1d1	denisovaný
<g/>
,	,	kIx,	,
současníci	současník	k1gMnPc1	současník
Homo	Homo	k6eAd1	Homo
neanderthalensis	neanderthalensis	k1gFnSc4	neanderthalensis
i	i	k9	i
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
(	(	kIx(	(
<g/>
genetické	genetický	k2eAgFnPc4d1	genetická
analýzy	analýza	k1gFnPc4	analýza
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
křížili	křížit	k5eAaImAgMnP	křížit
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
moudrým	moudrý	k2eAgMnSc7d1	moudrý
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
s	s	k7c7	s
Melanésany	Melanésan	k1gMnPc7	Melanésan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgInPc4d1	molekulární
rozbory	rozbor	k1gInPc4	rozbor
identifikovaly	identifikovat	k5eAaBmAgFnP	identifikovat
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
vymřelé	vymřelý	k2eAgFnPc1d1	vymřelá
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
geny	gen	k1gInPc1	gen
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
dávného	dávný	k2eAgNnSc2d1	dávné
křížení	křížení	k1gNnSc2	křížení
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
lidské	lidský	k2eAgFnSc6d1	lidská
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
linie	linie	k1gFnPc1	linie
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
dalšího	další	k2eAgInSc2d1	další
druhu	druh	k1gInSc2	druh
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
označeni	označit	k5eAaPmNgMnP	označit
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
archaičtí	archaický	k2eAgMnPc1d1	archaický
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
geneticky	geneticky	k6eAd1	geneticky
identifikovaní	identifikovaný	k2eAgMnPc1d1	identifikovaný
v	v	k7c6	v
genomu	genom	k1gInSc6	genom
Pygmejů	Pygmej	k1gMnPc2	Pygmej
<g/>
,	,	kIx,	,
Hadzů	Hadz	k1gMnPc2	Hadz
a	a	k8xC	a
Sandawů	Sandaw	k1gMnPc2	Sandaw
<g/>
.	.	kIx.	.
</s>
<s>
Systematika	systematika	k1gFnSc1	systematika
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
paleontologickými	paleontologický	k2eAgInPc7d1	paleontologický
nálezy	nález	k1gInPc7	nález
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
poddruhy	poddruh	k1gInPc1	poddruh
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Homo	Homo	k1gNnSc4	Homo
neanderthalensis	neanderthalensis	k1gInSc1	neanderthalensis
jakožto	jakožto	k8xS	jakožto
H.	H.	kA	H.
sapiens	sapiens	k1gInSc1	sapiens
neanderthalensis	neanderthalensis	k1gFnSc1	neanderthalensis
–	–	k?	–
genetické	genetický	k2eAgFnPc4d1	genetická
analýzy	analýza	k1gFnPc4	analýza
totiž	totiž	k9	totiž
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
značnou	značný	k2eAgFnSc4d1	značná
míru	míra	k1gFnSc4	míra
křížení	křížení	k1gNnSc2	křížení
s	s	k7c7	s
H.	H.	kA	H.
sapiens	sapiens	k6eAd1	sapiens
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
některé	některý	k3yIgInPc1	některý
nové	nový	k2eAgInPc1d1	nový
fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
Dmanisi	Dmanise	k1gFnSc4	Dmanise
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zdají	zdát	k5eAaPmIp3nP	zdát
podporovat	podporovat	k5eAaImF	podporovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kmenové	kmenový	k2eAgFnSc6d1	kmenová
linii	linie	k1gFnSc6	linie
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
linie	linie	k1gFnPc1	linie
tradičně	tradičně	k6eAd1	tradičně
pokládáné	pokládáný	k2eAgFnPc1d1	pokládáná
za	za	k7c4	za
odlišné	odlišný	k2eAgInPc4d1	odlišný
druhy	druh	k1gInPc4	druh
by	by	kYmCp3nP	by
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
mohly	moct	k5eAaImAgFnP	moct
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
navazovat	navazovat	k5eAaImF	navazovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jedinci	jedinec	k1gMnPc1	jedinec
různých	různý	k2eAgInPc2d1	různý
tradičních	tradiční	k2eAgInPc2d1	tradiční
druhů	druh	k1gInPc2	druh
mohli	moct	k5eAaImAgMnP	moct
spolužít	spolužít	k5eAaImF	spolužít
ve	v	k7c6	v
stejných	stejný	k2eAgInPc6d1	stejný
sídlištích	sídliště	k1gNnPc6	sídliště
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
pouhé	pouhý	k2eAgFnPc4d1	pouhá
morfologické	morfologický	k2eAgFnPc4d1	morfologická
formy	forma	k1gFnPc4	forma
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
"	"	kIx"	"
<g/>
slučovací	slučovací	k2eAgFnSc1d1	slučovací
<g/>
"	"	kIx"	"
systematika	systematika	k1gFnSc1	systematika
pak	pak	k6eAd1	pak
nově	nově	k6eAd1	nově
řadí	řadit	k5eAaImIp3nS	řadit
popsané	popsaný	k2eAgFnPc4d1	popsaná
skupiny	skupina	k1gFnPc4	skupina
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
jakožto	jakožto	k8xS	jakožto
poddruhy	poddruh	k1gInPc1	poddruh
do	do	k7c2	do
pouhých	pouhý	k2eAgInPc2d1	pouhý
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
–	–	k?	–
Homo	Homo	k6eAd1	Homo
erectus	erectus	k1gMnSc1	erectus
sensu	sens	k1gInSc2	sens
lato	lata	k1gFnSc5	lata
a	a	k8xC	a
Homo	Homo	k1gNnSc1	Homo
sapiens	sapiensa	k1gFnPc2	sapiensa
sensu	sens	k1gInSc2	sens
lato	lata	k1gFnSc5	lata
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Homo	Homo	k6eAd1	Homo
erectus	erectus	k1gMnSc1	erectus
sensu	sens	k1gInSc2	sens
lato	lata	k1gFnSc5	lata
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
H.	H.	kA	H.
habilis	habilis	k1gInSc1	habilis
<g/>
,	,	kIx,	,
H.	H.	kA	H.
rudolfensis	rudolfensis	k1gInSc1	rudolfensis
<g/>
,	,	kIx,	,
H.	H.	kA	H.
naledi	naled	k1gMnPc1	naled
<g/>
,	,	kIx,	,
H.	H.	kA	H.
ergaster	ergaster	k1gInSc1	ergaster
<g/>
,	,	kIx,	,
H.	H.	kA	H.
erectus	erectus	k1gInSc4	erectus
sensu	sens	k1gInSc2	sens
stricto	strict	k2eAgNnSc1d1	stricto
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
H.	H.	kA	H.
pekinensis	pekinensis	k1gFnSc2	pekinensis
a	a	k8xC	a
H.	H.	kA	H.
soloensis	soloensis	k1gInSc1	soloensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
H.	H.	kA	H.
antecessor	antecessor	k1gInSc1	antecessor
<g/>
,	,	kIx,	,
H.	H.	kA	H.
georgicus	georgicus	k1gInSc1	georgicus
a	a	k8xC	a
H.	H.	kA	H.
floresiensis	floresiensis	k1gInSc1	floresiensis
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
sensu	sensa	k1gFnSc4	sensa
lato	lata	k1gFnSc5	lata
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
řazeni	řazen	k2eAgMnPc1d1	řazen
H.	H.	kA	H.
helmei	helmei	k1gNnSc7	helmei
<g/>
,	,	kIx,	,
H.	H.	kA	H.
rhodesiensis	rhodesiensis	k1gInSc1	rhodesiensis
<g/>
,	,	kIx,	,
H.	H.	kA	H.
heidelbergensis	heidelbergensis	k1gInSc1	heidelbergensis
<g/>
,	,	kIx,	,
H.	H.	kA	H.
neanderthalensis	neanderthalensis	k1gInSc1	neanderthalensis
a	a	k8xC	a
H.	H.	kA	H.
sapiens	sapiens	k1gInSc4	sapiens
sensu	sens	k1gInSc2	sens
stricto	strict	k2eAgNnSc1d1	stricto
(	(	kIx(	(
<g/>
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
pochopitelně	pochopitelně	k6eAd1	pochopitelně
i	i	k9	i
denisované	denisovaný	k2eAgFnSc3d1	denisovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
používaly	používat	k5eAaImAgInP	používat
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
anatomických	anatomický	k2eAgInPc2d1	anatomický
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
především	především	k9	především
bipedie	bipedie	k1gFnSc1	bipedie
<g/>
,	,	kIx,	,
zvětšení	zvětšení	k1gNnSc1	zvětšení
objemu	objem	k1gInSc2	objem
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c4	vyjma
Homo	Homo	k1gNnSc4	Homo
habilis	habilis	k1gFnSc2	habilis
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
ml	ml	kA	ml
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čelo	čelo	k1gNnSc4	čelo
od	od	k7c2	od
očí	oko	k1gNnPc2	oko
stoupá	stoupat	k5eAaImIp3nS	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
lebka	lebka	k1gFnSc1	lebka
se	se	k3xPyFc4	se
zakulacuje	zakulacovat	k5eAaImIp3nS	zakulacovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
redukci	redukce	k1gFnSc3	redukce
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
zkracování	zkracování	k1gNnSc3	zkracování
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
prodlužování	prodlužování	k1gNnSc1	prodlužování
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
současní	současný	k2eAgMnPc1d1	současný
hominini	hominin	k2eAgMnPc1d1	hominin
<g/>
)	)	kIx)	)
živí	živit	k5eAaImIp3nS	živit
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
i	i	k8xC	i
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgFnSc1	obojí
potrava	potrava	k1gFnSc1	potrava
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
i	i	k9	i
v	v	k7c6	v
paleontologických	paleontologický	k2eAgInPc6d1	paleontologický
nálezech	nález	k1gInPc6	nález
(	(	kIx(	(
<g/>
v	v	k7c6	v
sídlištích	sídliště	k1gNnPc6	sídliště
shromážděné	shromážděný	k2eAgFnSc2d1	shromážděná
pevné	pevný	k2eAgFnSc2d1	pevná
části	část	k1gFnSc2	část
rostlin	rostlina	k1gFnPc2	rostlina
-	-	kIx~	-
pecky	pecka	k1gFnSc2	pecka
<g/>
,	,	kIx,	,
skořápky	skořápka	k1gFnSc2	skořápka
-	-	kIx~	-
a	a	k8xC	a
rozbité	rozbitý	k2eAgFnPc1d1	rozbitá
kosti	kost	k1gFnPc1	kost
ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
a	a	k8xC	a
nově	nově	k6eAd1	nově
prokázána	prokázat	k5eAaPmNgFnS	prokázat
i	i	k9	i
analyticky	analyticky	k6eAd1	analyticky
pomocí	pomocí	k7c2	pomocí
zastoupení	zastoupení	k1gNnSc2	zastoupení
izotopů	izotop	k1gInPc2	izotop
v	v	k7c6	v
kosterních	kosterní	k2eAgInPc6d1	kosterní
pozůstatcích	pozůstatek	k1gInPc6	pozůstatek
či	či	k8xC	či
analýzou	analýza	k1gFnSc7	analýza
zubního	zubní	k2eAgInSc2d1	zubní
povlaku	povlak	k1gInSc2	povlak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
trávit	trávit	k5eAaImF	trávit
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc4	bílkovina
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
sacharidů	sacharid	k1gInPc2	sacharid
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
celulózu	celulóza	k1gFnSc4	celulóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
živočišné	živočišný	k2eAgFnSc2d1	živočišná
potravy	potrava	k1gFnSc2	potrava
přijímá	přijímat	k5eAaImIp3nS	přijímat
zejména	zejména	k9	zejména
bílkoviny	bílkovina	k1gFnPc4	bílkovina
a	a	k8xC	a
tuky	tuk	k1gInPc4	tuk
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
se	se	k3xPyFc4	se
u	u	k7c2	u
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
schopnost	schopnost	k1gFnSc4	schopnost
i	i	k9	i
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
trávit	trávit	k5eAaImF	trávit
mléko	mléko	k1gNnSc4	mléko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modernějším	moderní	k2eAgInPc3d2	modernější
druhům	druh	k1gInPc3	druh
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jediného	jediné	k1gNnSc2	jediné
současně	současně	k6eAd1	současně
žijícího	žijící	k2eAgMnSc2d1	žijící
člověka	člověk	k1gMnSc2	člověk
moudrého	moudrý	k2eAgMnSc2d1	moudrý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgNnSc1d1	vlastní
užívání	užívání	k1gNnSc1	užívání
oděvů	oděv	k1gInPc2	oděv
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
klimatickými	klimatický	k2eAgInPc7d1	klimatický
vlivy	vliv	k1gInPc7	vliv
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
mechanickými	mechanický	k2eAgInPc7d1	mechanický
a	a	k8xC	a
tepelnými	tepelný	k2eAgInPc7d1	tepelný
účinky	účinek	k1gInPc7	účinek
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
společenského	společenský	k2eAgNnSc2d1	společenské
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
určení	určení	k1gNnSc4	určení
role	role	k1gFnSc2	role
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Evoluce	evoluce	k1gFnSc2	evoluce
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
z	z	k7c2	z
jaké	jaký	k3yRgFnSc2	jaký
linie	linie	k1gFnSc2	linie
hominidů	hominid	k1gMnPc2	hominid
se	se	k3xPyFc4	se
rod	rod	k1gInSc1	rod
Homo	Homo	k6eAd1	Homo
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předkem	předek	k1gMnSc7	předek
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
linie	linie	k1gFnSc1	linie
australopitéků	australopitéek	k1gMnPc2	australopitéek
(	(	kIx(	(
<g/>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
gracilní	gracilní	k2eAgNnSc1d1	gracilní
australopitéci	australopitéct	k5eAaPmF	australopitéct
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
A.	A.	kA	A.
africanus	africanus	k1gInSc4	africanus
či	či	k8xC	či
A.	A.	kA	A.
afarensis	afarensis	k1gInSc1	afarensis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
linie	linie	k1gFnSc1	linie
australopitéků	australopiték	k1gInPc2	australopiték
měla	mít	k5eAaImAgFnS	mít
menší	malý	k2eAgFnSc1d2	menší
čelist	čelist	k1gFnSc1	čelist
i	i	k8xC	i
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
živila	živit	k5eAaImAgFnS	živit
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jemnější	jemný	k2eAgFnSc7d2	jemnější
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
paleontologickým	paleontologický	k2eAgInSc7d1	paleontologický
nálezem	nález	k1gInSc7	nález
<g/>
,	,	kIx,	,
zařazovaným	zařazovaný	k2eAgInSc7d1	zařazovaný
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
africký	africký	k2eAgMnSc1d1	africký
Homo	Homo	k1gMnSc1	Homo
habilis	habilis	k1gFnSc2	habilis
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc4d1	starý
až	až	k6eAd1	až
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
tento	tento	k3xDgMnSc1	tento
primitivní	primitivní	k2eAgMnSc1d1	primitivní
člověk	člověk	k1gMnSc1	člověk
měl	mít	k5eAaImAgMnS	mít
proti	proti	k7c3	proti
australopitékům	australopitéek	k1gMnPc3	australopitéek
větší	veliký	k2eAgInSc1d2	veliký
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
asi	asi	k9	asi
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
750	[number]	k4	750
ml	ml	kA	ml
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
ustupující	ustupující	k2eAgFnPc1d1	ustupující
čelisti	čelist	k1gFnPc1	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
H.	H.	kA	H.
habilis	habilis	k1gInSc1	habilis
nikdy	nikdy	k6eAd1	nikdy
neopustil	opustit	k5eNaPmAgInS	opustit
africký	africký	k2eAgInSc1d1	africký
kontinent	kontinent	k1gInSc1	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Opustit	opustit	k5eAaPmF	opustit
Afriku	Afrika	k1gFnSc4	Afrika
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
člověku	člověk	k1gMnSc3	člověk
vzpřímenému	vzpřímený	k2eAgMnSc3d1	vzpřímený
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
druhu	druh	k1gInSc2	druh
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
mezi	mezi	k7c7	mezi
1,8	[number]	k4	1,8
–	–	k?	–
0,5	[number]	k4	0,5
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
erectus	erectus	k1gMnSc1	erectus
osídlil	osídlit	k5eAaPmAgMnS	osídlit
například	například	k6eAd1	například
Indonésii	Indonésie	k1gFnSc4	Indonésie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
objevů	objev	k1gInPc2	objev
relativně	relativně	k6eAd1	relativně
brzy	brzy	k6eAd1	brzy
i	i	k8xC	i
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgFnPc1d3	nejmladší
jsou	být	k5eAaImIp3nP	být
fosílie	fosílie	k1gFnPc1	fosílie
druhů	druh	k1gInPc2	druh
Homo	Homo	k1gNnSc4	Homo
neanderthalensis	neanderthalensis	k1gFnSc2	neanderthalensis
a	a	k8xC	a
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
člověk	člověk	k1gMnSc1	člověk
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
recentním	recentní	k2eAgMnSc7d1	recentní
zástupcem	zástupce	k1gMnSc7	zástupce
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
světě	svět	k1gInSc6	svět
7,3	[number]	k4	7,3
miliard	miliarda	k4xCgFnPc2	miliarda
a	a	k8xC	a
počet	počet	k1gInSc1	počet
neustále	neustále	k6eAd1	neustále
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
již	již	k6eAd1	již
osídlili	osídlit	k5eAaPmAgMnP	osídlit
všechny	všechen	k3xTgInPc4	všechen
kontinenty	kontinent	k1gInPc4	kontinent
a	a	k8xC	a
dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
zemskou	zemský	k2eAgFnSc4d1	zemská
orbitu	orbita	k1gFnSc4	orbita
<g/>
.	.	kIx.	.
</s>
<s>
DUNBAR	DUNBAR	kA	DUNBAR
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
rodu	rod	k1gInSc2	rod
homo	homo	k6eAd1	homo
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jana	Jan	k1gMnSc2	Jan
Enderlová	Enderlová	k1gFnSc1	Enderlová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1715	[number]	k4	1715
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
J.	J.	kA	J.
-	-	kIx~	-
MACHLÁŇ	MACHLÁŇ	kA	MACHLÁŇ
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Předkové	předek	k1gMnPc1	předek
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
2015	[number]	k4	2015
Ekumena	ekumena	k1gFnSc1	ekumena
Homo	Homo	k6eAd1	Homo
Lid	lid	k1gInSc4	lid
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Homo	Homo	k6eAd1	Homo
genus	genus	k1gInSc1	genus
–	–	k?	–
rod	rod	k1gInSc1	rod
Homo	Homo	k6eAd1	Homo
</s>
