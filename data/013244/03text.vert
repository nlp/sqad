<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
chrám	chrám	k1gInSc1	chrám
v	v	k7c4	v
Lipov	Lipov	k1gInSc4	Lipov
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
lipovské	lipovský	k2eAgFnSc2d1	lipovská
farnosti	farnost	k1gFnSc2	farnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
Lipově	lipově	k6eAd1	lipově
vybudován	vybudován	k2eAgInSc4d1	vybudován
jednolodní	jednolodní	k2eAgInSc4d1	jednolodní
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
poškozen	poškodit	k5eAaPmNgInS	poškodit
za	za	k7c2	za
tureckých	turecký	k2eAgMnPc2d1	turecký
vpádů	vpád	k1gInPc2	vpád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1663	[number]	k4	1663
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
provizorně	provizorně	k6eAd1	provizorně
opraven	opravna	k1gFnPc2	opravna
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
chátrající	chátrající	k2eAgFnSc1d1	chátrající
budova	budova	k1gFnSc1	budova
hrozila	hrozit	k5eAaImAgFnS	hrozit
zřícením	zřícení	k1gNnSc7	zřícení
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
stavbě	stavba	k1gFnSc6	stavba
a	a	k8xC	a
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
strženého	stržený	k2eAgInSc2d1	stržený
gotického	gotický	k2eAgInSc2d1	gotický
kostela	kostel	k1gInSc2	kostel
postaven	postavit	k5eAaPmNgInS	postavit
kostel	kostel	k1gInSc1	kostel
současný	současný	k2eAgInSc1d1	současný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednolodní	jednolodní	k2eAgFnSc4d1	jednolodní
stavbu	stavba	k1gFnSc4	stavba
s	s	k7c7	s
vestavěnou	vestavěný	k2eAgFnSc7d1	vestavěná
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
emporou	empora	k1gFnSc7	empora
<g/>
.	.	kIx.	.
</s>
<s>
Čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
postavěna	postavět	k5eAaPmNgFnS	postavět
nad	nad	k7c7	nad
předsíní	předsíň	k1gFnSc7	předsíň
<g/>
,	,	kIx,	,
přístupná	přístupný	k2eAgFnSc1d1	přístupná
je	být	k5eAaImIp3nS	být
dvěma	dva	k4xCgFnPc7	dva
bočními	boční	k2eAgNnPc7d1	boční
schodišti	schodiště	k1gNnPc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
má	mít	k5eAaImIp3nS	mít
klenutý	klenutý	k2eAgInSc1d1	klenutý
portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
zastropena	zastropit	k5eAaPmNgFnS	zastropit
plochým	plochý	k2eAgInSc7d1	plochý
stropem	strop	k1gInSc7	strop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
