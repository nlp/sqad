<s desamb="1">
Od	od	k7c2
chvíle	chvíle	k1gFnSc2
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
první	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
dodán	dodat	k5eAaPmNgInS
americkému	americký	k2eAgInSc3d1
námořnictvu	námořnictvo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
dodávky	dodávka	k1gFnSc2
roku	rok	k1gInSc2
1953	#num#	k4
Francouzům	Francouzi	k1gInPc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
vyrobeno	vyrobit	k5eAaPmNgNnS
12	#num#	k4
571	#num#	k4
letounů	letoun	k1gInPc2
F4U	F4U	kA
Corsair	Corsair	k1gInSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
16	#num#	k4
různých	různý	k2eAgInPc6d1
modelech	model	k1gInPc6
<g/>
.	.	kIx.
</s>