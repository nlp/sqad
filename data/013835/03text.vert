<s>
Vought	Vought	k2eAgMnSc1d1
F4U	F4U	k1gMnSc1
Corsair	Corsair	k1gMnSc1
</s>
<s>
F4U	F4U	k4
Corsair	Corsair	k1gInSc1
Určení	určení	k1gNnSc2
</s>
<s>
palubní	palubní	k2eAgMnSc1d1
stíhací	stíhací	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Vought	Vought	k1gMnSc1
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Rex	Rex	k?
BeiselIgor	BeiselIgor	k1gMnSc1
Sikorsky	Sikorsky	k1gMnSc1
První	první	k4xOgFnSc4
let	léto	k1gNnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1940	#num#	k4
Zařazeno	zařazen	k2eAgNnSc4d1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1942	#num#	k4
Vyřazeno	vyřadit	k5eAaPmNgNnS
</s>
<s>
1953	#num#	k4
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
1979	#num#	k4
(	(	kIx(
<g/>
Honduras	Honduras	k1gInSc1
<g/>
)	)	kIx)
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
a	a	k8xC
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
0	#num#	k4
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
FAA	FAA	kA
(	(	kIx(
<g/>
2	#num#	k4
0	#num#	k4
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
RNZAF	RNZAF	kA
(	(	kIx(
<g/>
424	#num#	k4
<g/>
)	)	kIx)
<g/>
Aéronavale	Aéronavala	k1gFnSc6
(	(	kIx(
<g/>
119	#num#	k4
<g/>
)	)	kIx)
Výroba	výroba	k1gFnSc1
</s>
<s>
1942	#num#	k4
<g/>
–	–	k?
<g/>
53	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
12	#num#	k4
571	#num#	k4
ks	ks	kA
Cena	cena	k1gFnSc1
za	za	k7c4
kus	kus	k1gInSc4
</s>
<s>
1	#num#	k4
500	#num#	k4
000	#num#	k4
$	$	kIx~
Varianty	varianta	k1gFnSc2
</s>
<s>
F2G	F2G	k4
Corsair	Corsair	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chance	Chanka	k1gFnSc3
Vought	Vought	k1gMnSc1
F4U	F4U	k1gMnSc1
Corsair	Corsair	k1gMnSc1
byl	být	k5eAaImAgMnS
americký	americký	k2eAgInSc4d1
stíhací	stíhací	k2eAgInSc4d1
letoun	letoun	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
sloužil	sloužit	k5eAaImAgInS
především	především	k6eAd1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
a	a	k8xC
v	v	k7c6
korejské	korejský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
vyráběny	vyrábět	k5eAaImNgFnP
v	v	k7c6
letech	let	k1gInPc6
1940	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgNnPc6
letectvech	letectvo	k1gNnPc6
sloužily	sloužit	k5eAaImAgFnP
tyto	tento	k3xDgInPc1
stroje	stroj	k1gInPc4
až	až	k9
do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
12	#num#	k4
571	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Navržen	navržen	k2eAgMnSc1d1
a	a	k8xC
zpočátku	zpočátku	k6eAd1
vyráběn	vyráběn	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
ve	v	k7c6
firmě	firma	k1gFnSc6
Chance	Chanec	k1gInSc2
Vought	Vought	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
žádán	žádán	k2eAgInSc1d1
-	-	kIx~
výrobní	výrobní	k2eAgFnPc1d1
smlouvy	smlouva	k1gFnPc1
byly	být	k5eAaImAgFnP
dodatečné	dodatečný	k2eAgFnPc1d1
poskytnuty	poskytnut	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
Goodyear	Goodyeara	k1gFnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInPc1
Corsairy	Corsair	k1gInPc1
byly	být	k5eAaImAgInP
označeny	označit	k5eAaPmNgInP
jako	jako	k8xC,k8xS
FG	FG	kA
a	a	k8xC
firmě	firma	k1gFnSc6
Brewster	Brewstra	k1gFnPc2
Aeronautical	Aeronautical	k1gFnSc1
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
značeny	značit	k5eAaImNgInP
jako	jako	k8xC,k8xS
F	F	kA
<g/>
3	#num#	k4
<g/>
A.	A.	kA
</s>
<s>
Corsair	Corsair	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
a	a	k8xC
provozován	provozovat	k5eAaImNgInS
jako	jako	k9
palubní	palubní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
a	a	k8xC
ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
se	se	k3xPyFc4
do	do	k7c2
služby	služba	k1gFnSc2
u	u	k7c2
amerického	americký	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
dostal	dostat	k5eAaPmAgMnS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
a	a	k8xC
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychle	rychle	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejúspěšnějších	úspěšný	k2eAgInPc2d3
stíhacích	stíhací	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
japonští	japonský	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
jej	on	k3xPp3gMnSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
nejimpozantnější	impozantní	k2eAgMnPc4d3
americký	americký	k2eAgMnSc1d1
druhoválečný	druhoválečný	k2eAgMnSc1d1
stíhač	stíhač	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
piloti	pilot	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
poměru	poměr	k1gInSc6
sestřelení	sestřelení	k1gNnPc2
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
..	..	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
měl	mít	k5eAaImAgMnS
zpočátku	zpočátku	k6eAd1
problémy	problém	k1gInPc4
s	s	k7c7
přistáváním	přistávání	k1gNnSc7
na	na	k7c6
letadlových	letadlový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
a	a	k8xC
logistikou	logistika	k1gFnSc7
čímž	což	k3yQnSc7,k3yRnSc7
byl	být	k5eAaImAgInS
u	u	k7c2
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
částečně	částečně	k6eAd1
zastíněn	zastínit	k5eAaPmNgMnS
letounem	letoun	k1gMnSc7
Grumman	Grumman	k1gMnSc1
F6F	F6F	k1gMnSc1
Hellcat	Hellcat	k1gMnSc1
poháněným	poháněný	k2eAgInSc7d1
stejným	stejný	k2eAgInSc7d1
motorem	motor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
si	se	k3xPyFc3
Corsair	Corsair	k1gInSc1
udržel	udržet	k5eAaPmAgInS
významou	významý	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
na	na	k7c6
pozemních	pozemní	k2eAgFnPc6d1
základnách	základna	k1gFnPc6
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
celé	celý	k2eAgFnSc2d1
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
Corsair	Corsair	k1gMnSc1
sloužil	sloužit	k5eAaImAgMnS
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
jako	jako	k8xS,k8xC
stíhací	stíhací	k2eAgInSc1d1
bombardér	bombardér	k1gInSc1
<g/>
;	;	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
během	během	k7c2
francouzských	francouzský	k2eAgFnPc2d1
koloniálních	koloniální	k2eAgFnPc2d1
válek	válka	k1gFnPc2
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
Alžírské	alžírský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedle	vedle	k7c2
nasazení	nasazení	k1gNnSc2
v	v	k7c6
amerických	americký	k2eAgFnPc6d1
či	či	k8xC
britských	britský	k2eAgFnPc6d1
službách	služba	k1gFnPc6
byl	být	k5eAaImAgInS
Corsair	Corsair	k1gInSc1
používán	používán	k2eAgInSc1d1
královským	královský	k2eAgNnSc7d1
novozélandským	novozélandský	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
<g/>
,	,	kIx,
francouzským	francouzský	k2eAgNnSc7d1
námořním	námořní	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
leteckými	letecký	k2eAgFnPc7d1
silami	síla	k1gFnPc7
až	až	k8xS
do	do	k7c2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
chvíle	chvíle	k1gFnSc2
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
první	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
dodán	dodat	k5eAaPmNgInS
americkému	americký	k2eAgInSc3d1
námořnictvu	námořnictvo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
dodávky	dodávka	k1gFnSc2
roku	rok	k1gInSc2
1953	#num#	k4
Francouzům	Francouzi	k1gInPc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
vyrobeno	vyrobit	k5eAaPmNgNnS
12	#num#	k4
571	#num#	k4
letounů	letoun	k1gInPc2
F4U	F4U	kA
Corsair	Corsair	k1gInSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
16	#num#	k4
různých	různý	k2eAgInPc6d1
modelech	model	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nejdéle	dlouho	k6eAd3
vyráběným	vyráběný	k2eAgMnSc7d1
americkým	americký	k2eAgMnSc7d1
stíhačem	stíhač	k1gMnSc7
s	s	k7c7
pístovým	pístový	k2eAgInSc7d1
motorem	motor	k1gInSc7
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Prototyp	prototyp	k1gInSc1
XF4U-1	XF4U-1	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
</s>
<s>
Corsair	Corsair	k1gMnSc1
pálí	pálit	k5eAaImIp3nS
rakety	raketa	k1gFnPc4
na	na	k7c4
japonské	japonský	k2eAgNnSc4d1
opevnění	opevnění	k1gNnSc4
na	na	k7c6
Okinawě	Okinawa	k1gFnSc6
</s>
<s>
Vought	Vought	k2eAgMnSc1d1
F4U-1A	F4U-1A	k1gMnSc1
Corsair	Corsair	k1gMnSc1
<g/>
,	,	kIx,
BuNo	buna	k1gFnSc5
17883	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1943	#num#	k4
podnikal	podnikat	k5eAaImAgMnS
bojové	bojový	k2eAgFnPc4d1
akce	akce	k1gFnPc4
z	z	k7c2
Vella	Vello	k1gNnSc2
Lavella	Lavella	k1gMnSc1
velitel	velitel	k1gMnSc1
VMF-214	VMF-214	k1gMnSc3
Gregory	Gregor	k1gMnPc4
„	„	k?
<g/>
Pappy	Papp	k1gInPc1
<g/>
“	“	k?
Boyington	Boyington	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Corsair	Corsair	k1gInSc1
na	na	k7c4
lodi	loď	k1gFnPc4
HMS	HMS	kA
Glory	Glora	k1gFnSc2
v	v	k7c6
Rabaulu	Rabaul	k1gInSc6
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
letounu	letoun	k1gInSc2
Vought	Vought	k2eAgMnSc1d1
F4U	F4U	k1gMnSc1
Corsair	Corsair	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
dle	dle	k7c2
zadání	zadání	k1gNnSc2
námořní	námořní	k2eAgFnSc7d1
stíhačkou	stíhačka	k1gFnSc7
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
„	„	k?
<g/>
kolem	kolem	k7c2
<g/>
“	“	k?
motoru	motor	k1gInSc2
Pratt	Pratta	k1gFnPc2
&	&	k?
Whitney	Whitnea	k1gMnSc2
R-2800	R-2800	k1gMnSc2
Double	double	k2eAgInSc4d1
Wasp	Wasp	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
maximální	maximální	k2eAgNnSc4d1
využití	využití	k1gNnSc4
výkonu	výkon	k1gInSc2
motoru	motor	k1gInSc2
bylo	být	k5eAaImAgNnS
nezbytné	zbytný	k2eNgNnSc1d1,k2eAgNnSc1d1
instalovat	instalovat	k5eAaBmF
vrtuli	vrtule	k1gFnSc3
Hamilton	Hamilton	k1gInSc4
Standard	standard	k1gInSc1
Hydromatic	Hydromatice	k1gFnPc2
o	o	k7c6
průměru	průměr	k1gInSc6
4	#num#	k4
m	m	kA
a	a	k8xC
také	také	k9
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
zachovat	zachovat	k5eAaPmF
podvozkovou	podvozkový	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
typickému	typický	k2eAgInSc3d1
designovému	designový	k2eAgInSc3d1
znaku	znak	k1gInSc3
Corsairu	Corsair	k1gInSc2
–	–	k?
zalomenému	zalomený	k2eAgNnSc3d1
křídlu	křídlo	k1gNnSc3
do	do	k7c2
tvaru	tvar	k1gInSc2
písmene	písmeno	k1gNnSc2
„	„	k?
<g/>
W	W	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Prototyp	prototyp	k1gInSc1
nového	nový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
s	s	k7c7
označením	označení	k1gNnSc7
XF4U-1	XF4U-1	k1gMnSc2
poprvé	poprvé	k6eAd1
vzlétl	vzlétnout	k5eAaPmAgMnS
s	s	k7c7
šéfpilotem	šéfpilot	k1gMnSc7
Lymanem	Lyman	k1gMnSc7
A.	A.	kA
Bullardem	Bullard	k1gMnSc7
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1940	#num#	k4
z	z	k7c2
letiště	letiště	k1gNnSc2
Bridgeport	Bridgeport	k1gInSc1
Municipal	Municipal	k1gFnSc4
ve	v	k7c6
Stratfortu	Stratfort	k1gInSc6
(	(	kIx(
<g/>
stát	stát	k1gInSc1
Connecticut	Connecticut	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
brzy	brzy	k6eAd1
vykázal	vykázat	k5eAaPmAgMnS
rychlost	rychlost	k1gFnSc4
651	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Námořnictvo	námořnictvo	k1gNnSc1
poznalo	poznat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
stroj	stroj	k1gInSc4
mimořádných	mimořádný	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
po	po	k7c6
úpravách	úprava	k1gFnPc6
objednalo	objednat	k5eAaPmAgNnS
první	první	k4xOgFnSc3
sérii	série	k1gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
sériový	sériový	k2eAgInSc4d1
letoun	letoun	k1gInSc4
F4U-1	F4U-1	k1gFnSc2
s	s	k7c7
motorem	motor	k1gInSc7
R-2800-8	R-2800-8	k1gFnSc2
o	o	k7c6
výkonu	výkon	k1gInSc6
1	#num#	k4
449	#num#	k4
kW	kW	kA
provedl	provést	k5eAaPmAgMnS
zálet	zálet	k1gInSc4
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
problémem	problém	k1gInSc7
byly	být	k5eAaImAgFnP
nevhodné	vhodný	k2eNgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
letadla	letadlo	k1gNnSc2
při	při	k7c6
přistání	přistání	k1gNnSc6
na	na	k7c4
palubu	paluba	k1gFnSc4
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
–	–	k?
omezený	omezený	k2eAgInSc4d1
výhled	výhled	k1gInSc4
vpřed	vpřed	k6eAd1
při	při	k7c6
závěrečném	závěrečný	k2eAgNnSc6d1
přiblížení	přiblížení	k1gNnSc6
(	(	kIx(
<g/>
přes	přes	k7c4
dlouhý	dlouhý	k2eAgInSc4d1
mohutný	mohutný	k2eAgInSc4d1
překryt	překryt	k2eAgMnSc1d1
motoru	motor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byly	být	k5eAaImAgInP
tyto	tento	k3xDgInPc1
stroje	stroj	k1gInPc1
od	od	k7c2
dubna	duben	k1gInSc2
1943	#num#	k4
přiděleny	přidělit	k5eAaPmNgInP
k	k	k7c3
leteckým	letecký	k2eAgFnPc3d1
silám	síla	k1gFnPc3
námořní	námořní	k2eAgFnSc2d1
letky	letka	k1gFnSc2
VF-	VF-	k1gFnSc2
<g/>
12	#num#	k4
<g/>
,	,	kIx,
startující	startující	k2eAgMnSc1d1
z	z	k7c2
pozemních	pozemní	k2eAgFnPc2d1
základen	základna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
jednotka	jednotka	k1gFnSc1
letectva	letectvo	k1gNnSc2
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
s	s	k7c7
letouny	letoun	k1gMnPc7
F4U-1	F4U-1	k1gFnSc2
byla	být	k5eAaImAgFnS
zformována	zformovat	k5eAaPmNgFnS
v	v	k7c4
Camp	camp	k1gInSc4
Kearney	Kearnea	k1gFnSc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1942	#num#	k4
jako	jako	k8xC,k8xS
VMF-	VMF-	k1gMnSc1
<g/>
124	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Prvních	první	k4xOgFnPc2
bojových	bojový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
se	se	k3xPyFc4
letadla	letadlo	k1gNnPc1
účastnila	účastnit	k5eAaImAgNnP
na	na	k7c6
jaře	jaro	k1gNnSc6
1943	#num#	k4
z	z	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
VMF-124	VMF-124	k1gFnPc2
zde	zde	k6eAd1
své	svůj	k3xOyFgFnSc2
F4U-1	F4U-1	k1gFnSc2
poprvé	poprvé	k6eAd1
nasadila	nasadit	k5eAaPmAgFnS
v	v	k7c6
boji	boj	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Měsíc	měsíc	k1gInSc4
poté	poté	k6eAd1
začala	začít	k5eAaPmAgFnS
působit	působit	k5eAaImF
z	z	k7c2
letiště	letiště	k1gNnSc2
na	na	k7c6
Nové	Nové	k2eAgFnSc6d1
Georgii	Georgie	k1gFnSc6
také	také	k9
námořní	námořní	k2eAgFnSc1d1
letka	letka	k1gFnSc1
VF-	VF-	k1gFnSc2
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
problémy	problém	k1gInPc1
s	s	k7c7
technikou	technika	k1gFnSc7
přistání	přistání	k1gNnSc2
na	na	k7c4
palubu	paluba	k1gFnSc4
řešili	řešit	k5eAaImAgMnP
Američané	Američan	k1gMnPc1
dost	dost	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
startovaly	startovat	k5eAaBmAgFnP
první	první	k4xOgFnPc1
Corsairy	Corsaira	k1gFnPc1
I	I	kA
(	(	kIx(
<g/>
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
II	II	kA
(	(	kIx(
<g/>
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
III	III	kA
(	(	kIx(
<g/>
F	F	kA
<g/>
3	#num#	k4
<g/>
A-	A-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
IV	IV	kA
(	(	kIx(
<g/>
FG-	FG-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
britských	britský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
neměli	mít	k5eNaImAgMnP
dostatek	dostatek	k1gInSc4
vlastních	vlastní	k2eAgMnPc2d1
stíhačů	stíhač	k1gMnPc2
srovnatelných	srovnatelný	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
a	a	k8xC
tak	tak	k9
piloti	pilot	k1gMnPc1
pro	pro	k7c4
přiblížení	přiblížení	k1gNnSc4
používali	používat	k5eAaImAgMnP
manévr	manévr	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přilétali	přilétat	k5eAaImAgMnP
k	k	k7c3
palubě	paluba	k1gFnSc3
šikmo	šikmo	k6eAd1
a	a	k8xC
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
srovnali	srovnat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novozélandské	novozélandský	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
operovalo	operovat	k5eAaImAgNnS
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
s	s	k7c7
Corsairy	Corsair	k1gMnPc7
od	od	k7c2
května	květen	k1gInSc2
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
amerických	americký	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
se	se	k3xPyFc4
první	první	k4xOgFnPc1
F4U	F4U	k1gFnPc1
objevily	objevit	k5eAaPmAgFnP
až	až	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stroje	stroj	k1gInPc4
vyrábělo	vyrábět	k5eAaImAgNnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
licenčně	licenčně	k6eAd1
již	již	k6eAd1
několik	několik	k4yIc4
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
Brewster	Brewster	k1gMnSc1
Aeronautical	Aeronautical	k1gMnSc1
Corp	Corp	k1gMnSc1
<g/>
.	.	kIx.
jako	jako	k8xS,k8xC
F	F	kA
<g/>
3	#num#	k4
<g/>
A-	A-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Goodyear	Goodyear	k1gMnSc1
Aeronautical	Aeronautical	k1gMnSc1
Corp	Corp	k1gMnSc1
<g/>
.	.	kIx.
s	s	k7c7
označením	označení	k1gNnSc7
FG-	FG-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevovaly	objevovat	k5eAaImAgFnP
se	se	k3xPyFc4
nové	nový	k2eAgFnPc1d1
zdokonalené	zdokonalený	k2eAgFnPc1d1
verze	verze	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
689	#num#	k4
<g/>
.	.	kIx.
stroje	stroj	k1gInSc2
byla	být	k5eAaImAgFnS
do	do	k7c2
výroby	výroba	k1gFnSc2
zavedena	zaveden	k2eAgFnSc1d1
verze	verze	k1gFnSc1
F4U-1A	F4U-1A	k1gFnSc2
s	s	k7c7
vypouklým	vypouklý	k2eAgInSc7d1
překrytem	překryt	k1gInSc7
pilotního	pilotní	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
i	i	k9
na	na	k7c6
analogickém	analogický	k2eAgInSc6d1
typu	typ	k1gInSc6
FG-	FG-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
A.	A.	kA
Následující	následující	k2eAgInSc4d1
typ	typ	k1gInSc4
F4U-1C	F4U-1C	k1gFnSc2
vyrobený	vyrobený	k2eAgInSc4d1
ve	v	k7c6
200	#num#	k4
kusové	kusový	k2eAgFnSc6d1
sérii	série	k1gFnSc6
byl	být	k5eAaImAgInS
osazen	osadit	k5eAaPmNgInS
čtyřmi	čtyři	k4xCgInPc7
kanony	kanon	k1gInPc7
M2	M2	k1gFnSc4
ráže	ráže	k1gFnSc2
20	#num#	k4
mm	mm	kA
a	a	k8xC
F4U-1D	F4U-1D	k1gFnSc1
(	(	kIx(
<g/>
FG-	FG-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
D	D	kA
a	a	k8xC
F	F	kA
<g/>
3	#num#	k4
<g/>
A-	A-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
opět	opět	k6eAd1
se	s	k7c7
šesti	šest	k4xCc7
kulomety	kulomet	k1gInPc7
<g/>
,	,	kIx,
pumovými	pumový	k2eAgInPc7d1
závěsníky	závěsník	k1gInPc7
a	a	k8xC
centrálním	centrální	k2eAgInSc7d1
závěsníkem	závěsník	k1gInSc7
pro	pro	k7c4
přídavnou	přídavný	k2eAgFnSc4d1
palivovou	palivový	k2eAgFnSc4d1
nádrž	nádrž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
F4U-2	F4U-2	k1gFnSc1
byla	být	k5eAaImAgFnS
noční	noční	k2eAgFnSc1d1
stíhací	stíhací	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
s	s	k7c7
radiolokátorem	radiolokátor	k1gInSc7
v	v	k7c6
pouzdře	pouzdro	k1gNnSc6
u	u	k7c2
konce	konec	k1gInSc2
pravého	pravý	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
a	a	k8xC
F4U-1P	F4U-1P	k1gFnSc2
s	s	k7c7
kamerou	kamera	k1gFnSc7
K-21	K-21	k1gFnSc2
na	na	k7c6
zádi	záď	k1gFnSc6
trupu	trup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýkonnější	výkonný	k2eAgInSc1d3
byla	být	k5eAaImAgFnS
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
prototyp	prototyp	k1gInSc4
označený	označený	k2eAgMnSc1d1
F4U-4XA	F4U-4XA	k1gMnSc1
vzlétl	vzlétnout	k5eAaPmAgMnS
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
F4U-4XB	F4U-4XB	k1gMnSc1
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
a	a	k8xC
první	první	k4xOgInSc4
sériový	sériový	k2eAgInSc4d1
stroj	stroj	k1gInSc4
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uplatněno	uplatněn	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
000	#num#	k4
změn	změna	k1gFnPc2
nicméně	nicméně	k8xC
nejdůležitější	důležitý	k2eAgFnSc1d3
byla	být	k5eAaImAgFnS
zástavba	zástavba	k1gFnSc1
motoru	motor	k1gInSc2
R-2800-18W	R-2800-18W	k1gMnPc2
série	série	k1gFnSc2
C	C	kA
se	s	k7c7
čtyřlistou	čtyřlistý	k2eAgFnSc7d1
vrtulí	vrtule	k1gFnSc7
Hamilton	Hamilton	k1gInSc1
Standard	standard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgInSc1d1
vzletový	vzletový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
dosahoval	dosahovat	k5eAaImAgInS
1	#num#	k4
552	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
F4U-4B	F4U-4B	k1gFnPc2
a	a	k8xC
C	C	kA
obdržely	obdržet	k5eAaPmAgFnP
opět	opět	k6eAd1
kanónovou	kanónový	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
čtyř	čtyři	k4xCgMnPc2
M3	M3	k1gMnPc2
ráže	ráže	k1gFnSc2
20	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřeba	potřeba	k1gFnSc1
nočních	noční	k2eAgInPc2d1
stíhacích	stíhací	k2eAgInPc2d1
letounů	letoun	k1gInPc2
vybavených	vybavený	k2eAgInPc2d1
radiolokátory	radiolokátor	k1gInPc4
vyústila	vyústit	k5eAaPmAgNnP
v	v	k7c4
obměny	obměna	k1gFnPc4
F4U-4E	F4U-4E	k1gFnSc2
a	a	k8xC
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
N.	N.	kA
První	první	k4xOgInSc4
vybavená	vybavený	k2eAgFnSc1d1
radiolokátorem	radiolokátor	k1gInSc7
APS-	APS-	k1gFnSc2
<g/>
4	#num#	k4
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
APS-	APS-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vlekání	vlekání	k1gNnSc3
střeleckých	střelecký	k2eAgInPc2d1
terčů	terč	k1gInPc2
vznikla	vzniknout	k5eAaPmAgFnS
verze	verze	k1gFnSc1
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
K.	K.	kA
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1946	#num#	k4
vzlétl	vzlétnout	k5eAaPmAgMnS
do	do	k7c2
vzduchu	vzduch	k1gInSc2
prototyp	prototyp	k1gInSc1
výškového	výškový	k2eAgInSc2d1
stíhacího	stíhací	k2eAgInSc2d1
letounu	letoun	k1gInSc2
XF4U-5	XF4U-5	k1gMnPc2
s	s	k7c7
novým	nový	k2eAgInSc7d1
motorem	motor	k1gInSc7
R-2800-32W	R-2800-32W	k1gFnSc2
série	série	k1gFnSc2
E	E	kA
s	s	k7c7
dvoustupňovým	dvoustupňový	k2eAgInSc7d1
kompresorem	kompresor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavňová	hlavňový	k2eAgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
sestávala	sestávat	k5eAaImAgFnS
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
kanonů	kanon	k1gInPc2
M3	M3	k1gMnSc4
T-31	T-31	k1gFnSc2
ráže	ráže	k1gFnSc2
20	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
pilotní	pilotní	k2eAgInSc1d1
prostor	prostor	k1gInSc1
byl	být	k5eAaImAgInS
již	již	k6eAd1
plně	plně	k6eAd1
vyhřívaný	vyhřívaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sériové	sériový	k2eAgInPc1d1
stroje	stroj	k1gInPc1
byly	být	k5eAaImAgInP
produkovány	produkovat	k5eAaImNgInP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1947	#num#	k4
do	do	k7c2
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1951	#num#	k4
<g/>
,	,	kIx,
včetně	včetně	k7c2
nočních	noční	k2eAgFnPc2d1
stíhacích	stíhací	k2eAgFnPc2d1
F4U-5N	F4U-5N	k1gFnPc2
a	a	k8xC
F4U-5NL	F4U-5NL	k1gFnPc2
s	s	k7c7
vyhříváním	vyhřívání	k1gNnSc7
náběžných	náběžný	k2eAgFnPc2d1
hran	hrana	k1gFnPc2
a	a	k8xC
ocasních	ocasní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
a	a	k8xC
F4U-5P	F4U-5P	k1gFnSc4
určenou	určený	k2eAgFnSc4d1
k	k	k7c3
průzkumným	průzkumný	k2eAgInPc3d1
letům	let	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciálně	speciálně	k6eAd1
pro	pro	k7c4
válku	válka	k1gFnSc4
v	v	k7c6
Koreji	Korea	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
bitevní	bitevní	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
F4U-6	F4U-6	k1gFnSc1
zalétaná	zalétaný	k2eAgFnSc1d1
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
bitevníků	bitevník	k1gInPc2
se	se	k3xPyFc4
montovaly	montovat	k5eAaImAgInP
motory	motor	k1gInPc1
R-2800-83W	R-2800-83W	k1gFnSc2
s	s	k7c7
jednostupňovým	jednostupňový	k2eAgInSc7d1
dvourychlostním	dvourychlostní	k2eAgInSc7d1
kompresorem	kompresor	k1gInSc7
a	a	k8xC
přibylo	přibýt	k5eAaPmAgNnS
dodatečné	dodatečný	k2eAgNnSc1d1
pancéřování	pancéřování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
sériovou	sériový	k2eAgFnSc7d1
variantou	varianta	k1gFnSc7
byl	být	k5eAaImAgInS
typ	typ	k1gInSc1
F4U-7	F4U-7	k1gMnPc2
pro	pro	k7c4
francouzské	francouzský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
převzalo	převzít	k5eAaPmAgNnS
poslední	poslední	k2eAgNnSc4d1
94	#num#	k4
<g/>
.	.	kIx.
kus	kus	k1gInSc1
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgInPc1d1
Corsairy	Corsair	k1gInPc1
působily	působit	k5eAaImAgInP
do	do	k7c2
května	květen	k1gInSc2
1954	#num#	k4
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
v	v	k7c6
Suezu	Suez	k1gInSc6
při	při	k7c6
operaci	operace	k1gFnSc6
„	„	k?
<g/>
Mušketýr	mušketýr	k1gMnSc1
<g/>
“	“	k?
z	z	k7c2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Arromanches	Arromanchesa	k1gFnPc2
a	a	k8xC
La	la	k1gNnPc2
Fayette	Fayett	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
července	červenec	k1gInSc2
1962	#num#	k4
operovaly	operovat	k5eAaImAgInP
nad	nad	k7c7
Alžírskem	Alžírsko	k1gNnSc7
a	a	k8xC
ještě	ještě	k9
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
nad	nad	k7c7
tuniskou	tuniský	k2eAgFnSc7d1
Bizertou	Bizerta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
F4U-7	F4U-7	k1gFnSc7
vyřadila	vyřadit	k5eAaPmAgFnS
letka	letka	k1gFnSc1
14F	14F	k4
francouzského	francouzský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
náleželo	náležet	k5eAaImAgNnS
také	také	k6eAd1
salvadorské	salvadorský	k2eAgNnSc1d1
a	a	k8xC
honduraské	honduraský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argentinské	argentinský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
používalo	používat	k5eAaImAgNnS
F4U-5	F4U-5	k1gMnPc3
a	a	k8xC
F4U-5N	F4U-5N	k1gMnPc3
na	na	k7c6
letadlové	letadlový	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Independencia	Independencium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
Corsair	Corsaira	k1gFnPc2
přezdívali	přezdívat	k5eAaImAgMnP
„	„	k?
<g/>
whistling	whistling	k1gInSc1
death	death	k1gInSc1
<g/>
“	“	k?
–	–	k?
pištící	pištící	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
–	–	k?
kvůli	kvůli	k7c3
charakteristickému	charakteristický	k2eAgInSc3d1
zvuku	zvuk	k1gInSc3
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
mu	on	k3xPp3gMnSc3
přezdívali	přezdívat	k5eAaImAgMnP
například	například	k6eAd1
„	„	k?
<g/>
bent	bent	k1gMnSc1
wing	wing	k1gMnSc1
widow-maker	widow-maker	k1gMnSc1
<g/>
“	“	k?
–	–	k?
ovdovovač	ovdovovač	k1gInSc1
se	s	k7c7
zalomeným	zalomený	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
„	„	k?
<g/>
Ensign	Ensign	k1gMnSc1
eliminator	eliminator	k1gMnSc1
<g/>
“	“	k?
–	–	k?
eliminánor	eliminánor	k1gInSc4
nováčků	nováček	k1gMnPc2
(	(	kIx(
<g/>
právě	právě	k9
kvůli	kvůli	k7c3
špatnému	špatný	k2eAgInSc3d1
výhledu	výhled	k1gInSc3
při	při	k7c6
přistání	přistání	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
AU-1	AU-1	k4
Corsair	Corsair	k1gInSc1
</s>
<s>
F4U	F4U	k4
Corsair	Corsaira	k1gFnPc2
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Osádka	osádka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
(	(	kIx(
<g/>
pilot	pilot	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
12,50	12,50	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
10,26	10,26	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
4,5	4,5	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
29,17	29,17	k4
m²	m²	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
:	:	kIx,
4	#num#	k4
175	#num#	k4
kg	kg	kA
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
6	#num#	k4
350	#num#	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
×	×	k?
hvězdicový	hvězdicový	k2eAgInSc1d1
motor	motor	k1gInSc1
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gMnSc2
R-2800-18W	R-2800-18W	k1gMnSc2
o	o	k7c6
výkonu	výkon	k1gInSc6
2	#num#	k4
380	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
1	#num#	k4
770	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
718	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
446	#num#	k4
mph	mph	k?
<g/>
,	,	kIx,
388	#num#	k4
kn	kn	k?
<g/>
)	)	kIx)
</s>
<s>
Pádová	pádový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
143	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
89	#num#	k4
mph	mph	k?
<g/>
,	,	kIx,
77	#num#	k4
kn	kn	k?
<g/>
)	)	kIx)
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
12	#num#	k4
600	#num#	k4
m	m	kA
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
617	#num#	k4
km	km	kA
</s>
<s>
Bojový	bojový	k2eAgInSc1d1
dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
528	#num#	k4
km	km	kA
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1
stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
22,1	22,1	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
6	#num#	k4
×	×	k?
kulomet	kulomet	k1gInSc4
M2	M2	k1gFnSc2
Browning	browning	k1gInSc4
ráže	ráže	k1gFnSc2
0,50	0,50	k4
in	in	k?
(	(	kIx(
<g/>
12,7	12,7	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
400	#num#	k4
střel	střela	k1gFnPc2
na	na	k7c4
zbraň	zbraň	k1gFnSc4
</s>
<s>
4	#num#	k4
×	×	k?
kanon	kanon	k1gInSc1
AN	AN	kA
<g/>
/	/	kIx~
<g/>
M	M	kA
<g/>
3	#num#	k4
ráže	ráže	k1gFnSc2
0,79	0,79	k4
in	in	k?
(	(	kIx(
<g/>
20	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
231	#num#	k4
střel	střela	k1gFnPc2
na	na	k7c4
zbraň	zbraň	k1gFnSc4
</s>
<s>
8	#num#	k4
×	×	k?
rakety	raketa	k1gFnSc2
ráže	ráže	k1gFnSc2
12,7	12,7	k4
cm	cm	kA
</s>
<s>
1	#num#	k4
800	#num#	k4
kg	kg	kA
pum	puma	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Green	Green	k1gInSc1
1975	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
137	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jablonski	Jablonski	k1gNnSc1
1979	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
171	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Donald	Donald	k1gMnSc1
1995	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
246	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PIKE	PIKE	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
F4U	F4U	k1gMnSc1
Corsair	Corsair	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pilot	pilot	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Manual	Manual	k1gInSc1
1979	#num#	k4
<g/>
,	,	kIx,
Prologue	Prologue	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Shettle	Shettle	k1gFnSc1
2001	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
107	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Leary	Leara	k1gFnSc2
1980	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Donald	Donald	k1gMnSc1
1995	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
244	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wilson	Wilson	k1gInSc1
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POLANSKÝ	Polanský	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontroverzní	kontroverzní	k2eAgFnSc1d1
letecké	letecký	k2eAgNnSc4d1
eso	eso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NV	NV	kA
Military	Militara	k1gFnSc2
revue	revue	k1gFnSc2
<g/>
.	.	kIx.
28.4	28.4	k4
<g/>
.2016	.2016	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vought	Voughta	k1gFnPc2
F4U	F4U	k1gMnPc2
Corsair	Corsaira	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Vought	Voughta	k1gFnPc2
F4U	F4U	k1gMnPc2
Corsair	Corsaira	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
VBF-85	VBF-85	k1gFnSc1
Historical	Historical	k1gFnSc1
web	web	k1gInSc4
site	site	k1gInSc1
<g/>
;	;	kIx,
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
F	F	kA
<g/>
4	#num#	k4
<g/>
U-	U-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
FG-1D	FG-1D	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Americká	americký	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
letadla	letadlo	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
FR	fr	k0
•	•	k?
F2A	F2A	k1gMnSc3
•	•	k?
F3F	F3F	k1gMnSc1
•	•	k?
F4F	F4F	k1gMnSc1
•	•	k?
F4U	F4U	k1gMnSc1
•	•	k?
F6F	F6F	k1gMnSc1
•	•	k?
F7F	F7F	k1gMnSc1
•	•	k?
F8F	F8F	k1gMnSc1
•	•	k?
P-26	P-26	k1gMnSc1
•	•	k?
P-35	P-35	k1gMnSc1
•	•	k?
P-36	P-36	k1gMnSc1
•	•	k?
P-38	P-38	k1gMnSc1
•	•	k?
P-39	P-39	k1gMnSc1
•	•	k?
P-40	P-40	k1gMnSc1
•	•	k?
P-43	P-43	k1gMnSc1
•	•	k?
P-47	P-47	k1gMnSc1
•	•	k?
P-51	P-51	k1gMnSc1
•	•	k?
P-61	P-61	k1gMnSc1
•	•	k?
P-63	P-63	k1gMnSc1
•	•	k?
P-66	P-66	k1gMnSc1
•	•	k?
P-70	P-70	k1gMnSc1
•	•	k?
P-80	P-80	k1gMnSc1
Bombardovací	bombardovací	k2eAgInPc4d1
a	a	k8xC
torpédové	torpédový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
A-20	A-20	k4
•	•	k?
A-26	A-26	k1gMnSc1
•	•	k?
A-27	A-27	k1gMnSc1
•	•	k?
A-	A-	k1gMnSc1
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
A-	A-	k1gFnSc1
<g/>
29	#num#	k4
•	•	k?
A-30	A-30	k1gMnSc1
•	•	k?
A-	A-	k1gMnSc1
<g/>
31	#num#	k4
<g/>
/	/	kIx~
<g/>
A-	A-	k1gFnSc1
<g/>
35	#num#	k4
•	•	k?
A-36	A-36	k1gMnSc1
•	•	k?
B-17	B-17	k1gMnSc1
•	•	k?
B-18	B-18	k1gMnSc1
•	•	k?
B-23	B-23	k1gMnSc1
•	•	k?
B-24	B-24	k1gMnSc1
•	•	k?
B-25	B-25	k1gMnSc1
•	•	k?
B-26	B-26	k1gMnSc1
•	•	k?
B-29	B-29	k1gMnSc1
•	•	k?
B-32	B-32	k1gMnSc1
•	•	k?
B-34	B-34	k1gMnSc1
•	•	k?
B-37	B-37	k1gMnSc1
•	•	k?
BT	BT	kA
•	•	k?
TBD	TBD	kA
•	•	k?
TBF	TBF	kA
•	•	k?
SBC	SBC	kA
•	•	k?
SB2A	SB2A	k1gMnSc1
•	•	k?
SB2C	SB2C	k1gMnSc1
•	•	k?
SB2U	SB2U	k1gMnSc1
•	•	k?
SBD	SBD	kA
Průzkumné	průzkumný	k2eAgInPc1d1
a	a	k8xC
spojovací	spojovací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
F-	F-	k?
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
F-	F-	k1gFnSc1
<g/>
5	#num#	k4
•	•	k?
F-6	F-6	k1gMnSc1
•	•	k?
F-7	F-7	k1gMnSc1
•	•	k?
F-9	F-9	k1gMnSc1
•	•	k?
F-13	F-13	k1gMnSc1
•	•	k?
F-15	F-15	k1gMnSc1
•	•	k?
L-3	L-3	k1gMnSc1
•	•	k?
L-4	L-4	k1gMnSc1
•	•	k?
L-10	L-10	k1gMnSc1
•	•	k?
O-47	O-47	k1gMnSc1
•	•	k?
PBM	PBM	kA
•	•	k?
PBO	PBO	kA
•	•	k?
PBY	PBY	kA
•	•	k?
PB2Y	PB2Y	k1gMnSc1
•	•	k?
PB4Y	PB4Y	k1gMnSc1
•	•	k?
PB4Y-2	PB4Y-2	k1gMnSc1
•	•	k?
PV-1	PV-1	k1gMnSc1
•	•	k?
PV-2	PV-2	k1gMnSc1
•	•	k?
JF	JF	kA
•	•	k?
J2F	J2F	k1gFnSc2
•	•	k?
J	J	kA
<g/>
4	#num#	k4
<g/>
F	F	kA
<g/>
/	/	kIx~
<g/>
OA-	OA-	k1gFnSc1
<g/>
14	#num#	k4
•	•	k?
JRF	JRF	kA
•	•	k?
OS2U	OS2U	k1gFnSc2
•	•	k?
SC	SC	kA
•	•	k?
SOC	soc	kA
•	•	k?
SO3C	SO3C	k1gFnSc2
Cvičná	cvičný	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
PT-17	PT-17	k4
•	•	k?
PT-19	PT-19	k1gMnSc1
•	•	k?
AT-6	AT-6	k1gMnSc1
•	•	k?
AT-7	AT-7	k1gMnSc1
•	•	k?
AT-9	AT-9	k1gMnSc1
•	•	k?
BT-8	BT-8	k1gMnSc1
•	•	k?
BT-12	BT-12	k1gMnSc1
•	•	k?
BT-13	BT-13	k1gMnSc1
•	•	k?
AT-17	AT-17	k1gMnSc1
•	•	k?
AT-18	AT-18	k1gMnSc1
•	•	k?
SBN	SBN	kA
•	•	k?
TG-1	TG-1	k1gMnSc1
Transportní	transportní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
kluzáky	kluzák	k1gInPc4
</s>
<s>
C-28	C-28	k4
•	•	k?
C-33	C-33	k1gMnSc1
•	•	k?
UC-	UC-	k1gMnSc1
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
R	R	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c6
•	•	k?
C-40	C-40	k1gMnSc1
•	•	k?
UC-43	UC-43	k1gMnSc1
•	•	k?
C-45	C-45	k1gMnSc1
•	•	k?
C-46	C-46	k1gMnSc1
•	•	k?
C-47	C-47	k1gMnSc1
•	•	k?
C-	C-	k1gMnSc1
<g/>
48	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
C-54	C-54	k1gMnSc1
•	•	k?
C-58	C-58	k1gMnSc1
•	•	k?
UC-61	UC-61	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
C-63	C-63	k1gMnSc1
•	•	k?
C-64	C-64	k1gMnSc1
•	•	k?
UC-67	UC-67	k1gMnSc1
•	•	k?
C-69	C-69	k1gMnSc1
•	•	k?
C-73	C-73	k1gMnSc1
•	•	k?
C-75	C-75	k1gMnSc1
•	•	k?
UC-78	UC-78	k1gMnSc1
•	•	k?
UC-81	UC-81	k1gMnSc1
•	•	k?
UC-85	UC-85	k1gMnSc1
•	•	k?
C-87	C-87	k1gMnSc1
•	•	k?
C-91	C-91	k1gMnSc1
•	•	k?
C-98	C-98	k1gMnSc1
•	•	k?
UC-100	UC-100	k1gMnSc1
•	•	k?
UC-101	UC-101	k1gMnSc1
•	•	k?
C-105	C-105	k1gMnSc1
•	•	k?
C-	C-	k1gMnSc1
<g/>
110	#num#	k4
<g/>
/	/	kIx~
<g/>
R	R	kA
<g/>
3	#num#	k4
<g/>
D	D	kA
•	•	k?
CG-3	CG-3	k1gMnSc1
•	•	k?
CG-4	CG-4	k1gMnSc1
•	•	k?
CG-13	CG-13	k1gMnSc1
•	•	k?
CG-15	CG-15	k1gMnSc1
•	•	k?
AE	AE	kA
•	•	k?
R5O	R5O	k1gFnSc2
•	•	k?
JRS	JRS	kA
•	•	k?
JR2S	JR2S	k1gFnSc2
Vírníky	vírník	k1gInPc1
a	a	k8xC
vrtulníky	vrtulník	k1gInPc1
</s>
<s>
R-4	R-4	k4
•	•	k?
R-	R-	k1gMnSc1
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
H-	H-	k1gFnSc1
<g/>
5	#num#	k4
•	•	k?
R-	R-	k1gMnSc1
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
HOS-	HOS-	k1gFnSc1
<g/>
1	#num#	k4
Experimentální	experimentální	k2eAgInPc1d1
letouny	letoun	k1gInPc1
a	a	k8xC
prototypy	prototyp	k1gInPc1
</s>
<s>
XA-21	XA-21	k4
•	•	k?
XA-22	XA-22	k1gMnSc1
•	•	k?
XA-23	XA-23	k1gMnSc1
•	•	k?
XA-38	XA-38	k1gMnSc1
•	•	k?
XFL	XFL	kA
•	•	k?
XF-11	XF-11	k1gMnSc1
•	•	k?
XF8B	XF8B	k1gMnSc1
•	•	k?
TB2D	TB2D	k1gMnSc1
•	•	k?
XF5F	XF5F	k1gMnSc1
•	•	k?
XF5U	XF5U	k1gMnSc1
•	•	k?
XFD-1	XFD-1	k1gMnSc1
•	•	k?
XF14C	XF14C	k1gMnSc1
•	•	k?
XF15C	XF15C	k1gMnSc1
•	•	k?
XP-41	XP-41	k1gMnSc1
•	•	k?
XP-46	XP-46	k1gMnSc1
•	•	k?
XP-49	XP-49	k1gMnSc1
•	•	k?
XP-50	XP-50	k1gMnSc1
•	•	k?
XP-52	XP-52	k1gMnSc1
•	•	k?
XP-54	XP-54	k1gMnSc1
•	•	k?
XP-55	XP-55	k1gMnSc1
•	•	k?
XP-56	XP-56	k1gMnSc1
•	•	k?
XP-57	XP-57	k1gMnSc1
•	•	k?
XP-58	XP-58	k1gMnSc1
•	•	k?
P-59	P-59	k1gMnSc1
•	•	k?
XP-60	XP-60	k1gMnSc1
•	•	k?
XP-62	XP-62	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
XP-67	XP-67	k1gMnSc1
•	•	k?
XP-68	XP-68	k1gMnSc1
•	•	k?
XP-71	XP-71	k1gMnSc1
•	•	k?
XP-72	XP-72	k1gMnSc1
•	•	k?
XP-73	XP-73	k1gMnSc1
•	•	k?
P-75	P-75	k1gMnSc1
•	•	k?
XP-76	XP-76	k1gMnSc1
•	•	k?
XP-77	XP-77	k1gMnSc1
•	•	k?
XP-79	XP-79	k1gMnSc1
•	•	k?
XP-81	XP-81	k1gMnSc1
•	•	k?
XP-82	XP-82	k1gMnSc1
•	•	k?
XP-83	XP-83	k1gMnSc1
•	•	k?
AM	AM	kA
•	•	k?
XB-19	XB-19	k1gMnSc1
•	•	k?
XB-27	XB-27	k1gMnSc1
•	•	k?
XB-28	XB-28	k1gMnSc1
•	•	k?
XB-30	XB-30	k1gMnSc1
•	•	k?
XB-31	XB-31	k1gMnSc1
•	•	k?
XB-33	XB-33	k1gMnSc1
•	•	k?
YB-35	YB-35	k1gMnSc1
•	•	k?
XB-38	XB-38	k1gMnSc1
•	•	k?
XB-39	XB-39	k1gMnSc1
•	•	k?
YB-40	YB-40	k1gMnSc1
•	•	k?
XB-41	XB-41	k1gMnSc1
•	•	k?
XB-42	XB-42	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
XB-43	XB-43	k1gMnSc1
•	•	k?
XB-45	XB-45	k1gMnSc1
•	•	k?
XB-46	XB-46	k1gMnSc1
•	•	k?
XB-48	XB-48	k1gMnSc1
•	•	k?
XB-50	XB-50	k1gMnSc1
•	•	k?
XC-74	XC-74	k1gMnSc1
•	•	k?
YC-76	YC-76	k1gMnSc1
•	•	k?
XC-82	XC-82	k1gMnSc1
•	•	k?
XC-97	XC-97	k1gMnSc1
•	•	k?
XP2V	XP2V	k1gMnSc1
•	•	k?
XPB2M	XPB2M	k1gMnSc1
•	•	k?
XP4Y	XP4Y	k1gMnSc1
•	•	k?
R2Y	R2Y	k1gMnSc1
•	•	k?
RB	RB	kA
•	•	k?
TB2F	TB2F	k1gMnSc1
•	•	k?
XSB2D	XSB2D	k1gMnSc1
•	•	k?
XBTC	XBTC	kA
•	•	k?
XBT2C	XBT2C	k1gMnSc1
•	•	k?
XBT2D	XBT2D	k1gMnSc1
•	•	k?
XBTK	XBTK	kA
•	•	k?
TBY	TBY	kA
•	•	k?
HK-1	HK-1	k1gMnSc1
•	•	k?
YFM-1	YFM-1	k1gMnSc1
•	•	k?
XO-61	XO-61	k1gMnSc1
•	•	k?
XR-1	XR-1	k1gMnSc1
•	•	k?
XR-	XR-	k1gMnSc1
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
•	•	k?
XR-8	XR-8	k1gMnSc1
•	•	k?
N-1M	N-1M	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4243459-2	4243459-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85033064	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85033064	#num#	k4
</s>
