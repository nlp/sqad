<s>
Nespoutaný	spoutaný	k2eNgMnSc1d1	nespoutaný
Django	Django	k1gMnSc1	Django
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Django	Django	k1gMnSc1	Django
Unchained	Unchained	k1gMnSc1	Unchained
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
western	western	k1gInSc1	western
režiséra	režisér	k1gMnSc2	režisér
Quentina	Quentin	k1gMnSc2	Quentin
Tarantina	Tarantin	k1gMnSc2	Tarantin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Djanga	Djang	k1gMnSc4	Djang
<g/>
,	,	kIx,	,
osvobozeného	osvobozený	k2eAgMnSc4d1	osvobozený
otroka	otrok	k1gMnSc4	otrok
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
cestuje	cestovat	k5eAaImIp3nS	cestovat
napříč	napříč	k7c7	napříč
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
doktora	doktor	k1gMnSc2	doktor
Kinga	King	k1gMnSc2	King
Schultze	Schultze	k1gFnSc2	Schultze
<g/>
,	,	kIx,	,
nájemného	nájemný	k2eAgMnSc2d1	nájemný
lovce	lovec	k1gMnSc2	lovec
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
hrají	hrát	k5eAaImIp3nP	hrát
Jamie	Jamie	k1gFnPc1	Jamie
Foxx	Foxx	k1gInSc1	Foxx
<g/>
,	,	kIx,	,
Christoph	Christoph	k1gInSc1	Christoph
Waltz	waltz	k1gInSc1	waltz
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
,	,	kIx,	,
Kerry	Kerra	k1gFnSc2	Kerra
Washingtonová	Washingtonový	k2eAgNnPc1d1	Washingtonové
a	a	k8xC	a
Samuel	Samuel	k1gMnSc1	Samuel
L.	L.	kA	L.
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
získal	získat	k5eAaPmAgMnS	získat
dva	dva	k4xCgMnPc4	dva
Zlaté	zlatý	k2eAgInPc4d1	zlatý
glóby	glóbus	k1gInPc7	glóbus
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Christoph	Christoph	k1gInSc1	Christoph
Waltz	waltz	k1gInSc1	waltz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
(	(	kIx(	(
<g/>
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
nominován	nominován	k2eAgMnSc1d1	nominován
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
drama	drama	k1gNnSc4	drama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
režie	režie	k1gFnPc4	režie
(	(	kIx(	(
<g/>
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
dva	dva	k4xCgInPc4	dva
Oscary	Oscar	k1gInPc4	Oscar
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Christoph	Christoph	k1gInSc1	Christoph
Waltz	waltz	k1gInSc1	waltz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
původní	původní	k2eAgInSc4d1	původní
scénář	scénář	k1gInSc4	scénář
(	(	kIx(	(
<g/>
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
byl	být	k5eAaImAgMnS	být
také	také	k9	také
nominován	nominován	k2eAgMnSc1d1	nominován
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Richardson	Richardson	k1gMnSc1	Richardson
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
zvuku	zvuk	k1gInSc2	zvuk
(	(	kIx(	(
<g/>
Wylie	Wylie	k1gFnSc1	Wylie
Stateman	Stateman	k1gMnSc1	Stateman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
