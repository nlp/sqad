<p>
<s>
Den	den	k1gInSc1	den
upálení	upálení	k1gNnSc2	upálení
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
každoročně	každoročně	k6eAd1	každoročně
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
daný	daný	k2eAgMnSc1d1	daný
§	§	k?	§
1	[number]	k4	1
zákona	zákon	k1gInSc2	zákon
číslo	číslo	k1gNnSc4	číslo
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
jako	jako	k9	jako
připomenutí	připomenutí	k1gNnSc2	připomenutí
upálení	upálení	k1gNnSc2	upálení
kněze	kněz	k1gMnSc2	kněz
a	a	k8xC	a
reformátora	reformátor	k1gMnSc2	reformátor
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
během	během	k7c2	během
kostnického	kostnický	k2eAgInSc2d1	kostnický
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
středověký	středověký	k2eAgMnSc1d1	středověký
náboženský	náboženský	k2eAgMnSc1d1	náboženský
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
reformátor	reformátor	k1gMnSc1	reformátor
a	a	k8xC	a
kazatel	kazatel	k1gMnSc1	kazatel
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
Johnu	John	k1gMnSc6	John
Wycliffovi	Wycliff	k1gMnSc6	Wycliff
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc7	jehož
myšlenkami	myšlenka	k1gFnPc7	myšlenka
a	a	k8xC	a
argumentací	argumentace	k1gFnSc7	argumentace
byl	být	k5eAaImAgMnS	být
inspirován	inspirovat	k5eAaBmNgMnS	inspirovat
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
reformátorů	reformátor	k1gMnPc2	reformátor
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
své	svůj	k3xOyFgMnPc4	svůj
následníky	následník	k1gMnPc4	následník
–	–	k?	–
reformátory	reformátor	k1gMnPc4	reformátor
Luthera	Luther	k1gMnSc2	Luther
<g/>
,	,	kIx,	,
Kalvína	Kalvín	k1gMnSc2	Kalvín
a	a	k8xC	a
Zwingliho	Zwingli	k1gMnSc2	Zwingli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1409	[number]	k4	1409
<g/>
–	–	k?	–
<g/>
1410	[number]	k4	1410
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
náboženských	náboženský	k2eAgFnPc6d1	náboženská
pracích	práce	k1gFnPc6	práce
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
mravní	mravní	k2eAgInSc4d1	mravní
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
ho	on	k3xPp3gMnSc4	on
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c2	za
kacíře	kacíř	k1gMnSc2	kacíř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
učení	učení	k1gNnSc1	učení
za	za	k7c4	za
herezi	hereze	k1gFnSc4	hereze
a	a	k8xC	a
exkomunikovala	exkomunikovat	k5eAaBmAgFnS	exkomunikovat
jej	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
1411	[number]	k4	1411
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
mu	on	k3xPp3gMnSc3	on
zaručil	zaručit	k5eAaPmAgMnS	zaručit
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
příchod	příchod	k1gInSc4	příchod
na	na	k7c4	na
kostnický	kostnický	k2eAgInSc4d1	kostnický
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
jako	jako	k8xC	jako
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
světské	světský	k2eAgFnSc3d1	světská
moci	moc	k1gFnSc3	moc
k	k	k7c3	k
upálení	upálení	k1gNnSc3	upálení
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
když	když	k8xS	když
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odvolat	odvolat	k5eAaPmF	odvolat
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
odkazu	odkaz	k1gInSc3	odkaz
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
husité	husita	k1gMnPc1	husita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
další	další	k2eAgFnPc1d1	další
církve	církev	k1gFnPc1	církev
a	a	k8xC	a
společnosti	společnost	k1gFnPc1	společnost
vzešlé	vzešlý	k2eAgFnPc1d1	vzešlá
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
i	i	k8xC	i
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
zahrnovali	zahrnovat	k5eAaImAgMnP	zahrnovat
většinu	většina	k1gFnSc4	většina
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
Husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
porazili	porazit	k5eAaPmAgMnP	porazit
několik	několik	k4yIc1	několik
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
bylo	být	k5eAaImAgNnS	být
nekatolické	katolický	k2eNgNnSc1d1	nekatolické
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
stále	stále	k6eAd1	stále
následovali	následovat	k5eAaImAgMnP	následovat
učení	učení	k1gNnSc4	učení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavení	slavení	k1gNnSc2	slavení
svátku	svátek	k1gInSc2	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
Datum	datum	k1gNnSc1	datum
jeho	on	k3xPp3gNnSc2	on
upálení	upálení	k1gNnSc2	upálení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
památným	památný	k2eAgInSc7d1	památný
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pak	pak	k6eAd1	pak
českým	český	k2eAgInSc7d1	český
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
svatého	svatý	k2eAgMnSc2d1	svatý
mučedníka	mučedník	k1gMnSc2	mučedník
jej	on	k3xPp3gNnSc4	on
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
mučedníkem	mučedník	k1gMnSc7	mučedník
Jeronýmem	Jeroným	k1gMnSc7	Jeroným
Pražským	pražský	k2eAgInPc3d1	pražský
<g/>
)	)	kIx)	)
uctívá	uctívat	k5eAaImIp3nS	uctívat
česká	český	k2eAgFnSc1d1	Česká
starokatolická	starokatolický	k2eAgFnSc1d1	Starokatolická
církev	církev	k1gFnSc1	církev
</s>
</p>
<p>
<s>
a	a	k8xC	a
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
metropolity	metropolita	k1gMnSc2	metropolita
Kryštofa	Kryštof	k1gMnSc2	Kryštof
<g/>
,	,	kIx,	,
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Církev	církev	k1gFnSc4	církev
československou	československý	k2eAgFnSc7d1	Československá
husitskou	husitský	k2eAgFnSc7d1	husitská
je	být	k5eAaImIp3nS	být
výročí	výročí	k1gNnSc3	výročí
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
fakticky	fakticky	k6eAd1	fakticky
největším	veliký	k2eAgInSc7d3	veliký
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
bývá	bývat	k5eAaImIp3nS	bývat
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k9	jako
předchůdce	předchůdce	k1gMnSc1	předchůdce
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
díky	díky	k7c3	díky
národnímu	národní	k2eAgInSc3d1	národní
obrození	obrození	k1gNnSc4	obrození
vnímán	vnímat	k5eAaImNgMnS	vnímat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
postav	postava	k1gFnPc2	postava
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Organizovány	organizován	k2eAgFnPc1d1	organizována
byly	být	k5eAaImAgFnP	být
poutě	pouť	k1gFnPc1	pouť
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
i	i	k8xC	i
husitstvím	husitství	k1gNnSc7	husitství
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgNnPc2	dva
velkých	velký	k2eAgNnPc2d1	velké
poutí	poutí	k1gNnPc2	poutí
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
a	a	k8xC	a
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
byly	být	k5eAaImAgFnP	být
též	též	k9	též
budovány	budován	k2eAgInPc4d1	budován
pomníky	pomník	k1gInPc4	pomník
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
<g/>
Tradice	tradice	k1gFnSc1	tradice
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
i	i	k9	i
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
husitstvím	husitství	k1gNnSc7	husitství
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
vyzdvihována	vyzdvihovat	k5eAaImNgFnS	vyzdvihovat
také	také	k9	také
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jako	jako	k9	jako
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
bylo	být	k5eAaImAgNnS	být
výročí	výročí	k1gNnSc1	výročí
upálení	upálení	k1gNnSc2	upálení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
reformace	reformace	k1gFnSc1	reformace
</s>
</p>
<p>
<s>
Husitství	husitství	k1gNnSc1	husitství
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
</s>
</p>
<p>
<s>
Kostnický	kostnický	k2eAgInSc1d1	kostnický
koncil	koncil	k1gInSc1	koncil
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Upálení	upálení	k1gNnSc2	upálení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgInPc6d1	významný
dnech	den	k1gInPc6	den
a	a	k8xC	a
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KRUŠINA	krušina	k1gFnSc1	krušina
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
upálen	upálit	k5eAaPmNgMnS	upálit
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
-	-	kIx~	-
Slavné	slavný	k2eAgInPc4d1	slavný
dny	den	k1gInPc4	den
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-11-17	[number]	k4	2010-11-17
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
