<s>
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Karl	Karl	k1gMnSc1	Karl
Otto	Otto	k1gMnSc1	Otto
Lagerfeldt	Lagerfeldt	k1gMnSc1	Lagerfeldt
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
světových	světový	k2eAgMnPc2d1	světový
módních	módní	k2eAgMnPc2d1	módní
návrhářů	návrhář	k1gMnPc2	návrhář
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Hamburku	Hamburk	k1gInSc6	Hamburk
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
Otto	Otto	k1gMnSc1	Otto
a	a	k8xC	a
Elizabethy	Elizabetha	k1gFnPc1	Elizabetha
Lagerfeldtových	Lagerfeldtových	k2eAgFnPc1d1	Lagerfeldtových
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
švédské	švédský	k2eAgFnSc2d1	švédská
rodiny	rodina	k1gFnSc2	rodina
působící	působící	k2eAgFnSc2d1	působící
v	v	k7c6	v
investičním	investiční	k2eAgNnSc6d1	investiční
bankovnictví	bankovnictví	k1gNnSc6	bankovnictví
a	a	k8xC	a
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jmění	jmění	k1gNnPc4	jmění
vydělal	vydělat	k5eAaPmAgMnS	vydělat
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
odkud	odkud	k6eAd1	odkud
pocházela	pocházet	k5eAaImAgFnS	pocházet
Lagerfeldova	Lagerfeldův	k2eAgFnSc1d1	Lagerfeldova
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
vyrábět	vyrábět	k5eAaImF	vyrábět
kondenzované	kondenzovaný	k2eAgNnSc1d1	kondenzované
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
sňatku	sňatek	k1gInSc6	sňatek
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
42	[number]	k4	42
a	a	k8xC	a
Christianovi	Christianův	k2eAgMnPc1d1	Christianův
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
předchozích	předchozí	k2eAgInPc2d1	předchozí
svazků	svazek	k1gInPc2	svazek
měl	mít	k5eAaImAgMnS	mít
Karl	Karl	k1gMnSc1	Karl
několik	několik	k4yIc4	několik
nevlastních	vlastní	k2eNgMnPc2d1	nevlastní
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rodného	rodný	k2eAgInSc2d1	rodný
listu	list	k1gInSc2	list
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
narodit	narodit	k5eAaPmF	narodit
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
matriky	matrika	k1gFnPc1	matrika
nejsou	být	k5eNaImIp3nP	být
veřejně	veřejně	k6eAd1	veřejně
přístupné	přístupný	k2eAgFnPc1d1	přístupná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
německý	německý	k2eAgInSc1d1	německý
deník	deník	k1gInSc1	deník
Bild	Bild	k1gInSc1	Bild
am	am	k?	am
Sonntag	Sonntag	k1gInSc1	Sonntag
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
třídním	třídní	k2eAgMnSc7d1	třídní
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
dřívější	dřívější	k2eAgInSc4d1	dřívější
datum	datum	k1gInSc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
módními	módní	k2eAgFnPc7d1	módní
značkami	značka	k1gFnPc7	značka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Chloé	Chloá	k1gFnSc2	Chloá
<g/>
,	,	kIx,	,
Fendi	Fend	k1gMnPc1	Fend
a	a	k8xC	a
Chanelu	Chanela	k1gFnSc4	Chanela
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
značku	značka	k1gFnSc4	značka
Lagerfeld	Lagerfelda	k1gFnPc2	Lagerfelda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
parfémů	parfém	k1gInPc2	parfém
a	a	k8xC	a
kolekčního	kolekční	k2eAgNnSc2d1	kolekční
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
sehrála	sehrát	k5eAaPmAgFnS	sehrát
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	s	k7c7	s
známými	známý	k2eAgMnPc7d1	známý
umělci	umělec	k1gMnPc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
výhrou	výhra	k1gFnSc7	výhra
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
o	o	k7c4	o
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
návrh	návrh	k1gInSc4	návrh
kabátu	kabát	k1gInSc2	kabát
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
práci	práce	k1gFnSc4	práce
u	u	k7c2	u
Pierra	Pierr	k1gInSc2	Pierr
Balmaina	Balmaino	k1gNnSc2	Balmaino
<g/>
.	.	kIx.	.
</s>
<s>
Soutěže	soutěž	k1gFnPc1	soutěž
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
účastnil	účastnit	k5eAaImAgInS	účastnit
i	i	k9	i
Yves	Yves	k1gInSc1	Yves
Saint-Laurent	Saint-Laurent	k1gInSc1	Saint-Laurent
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgInS	odnést
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
šatů	šat	k1gInPc2	šat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Yves	Yves	k1gInSc1	Yves
tehdy	tehdy	k6eAd1	tehdy
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
Diora	Dior	k1gMnSc4	Dior
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
jsem	být	k5eAaImIp1nS	být
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
pracovali	pracovat	k5eAaImAgMnP	pracovat
pro	pro	k7c4	pro
Balenciagu	Balenciaga	k1gFnSc4	Balenciaga
a	a	k8xC	a
mysleli	myslet	k5eAaImAgMnP	myslet
si	se	k3xPyFc3	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
žádný	žádný	k3yNgInSc4	žádný
velký	velký	k2eAgInSc4d1	velký
dojem	dojem	k1gInSc4	dojem
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
k	k	k7c3	k
Jean	Jean	k1gMnSc1	Jean
Patouovi	Patouův	k2eAgMnPc1d1	Patouův
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tam	tam	k6eAd1	tam
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
taky	taky	k6eAd1	taky
přestalo	přestat	k5eAaPmAgNnS	přestat
bavit	bavit	k5eAaImF	bavit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jsem	být	k5eAaImIp1nS	být
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
zkusil	zkusit	k5eAaPmAgMnS	zkusit
zase	zase	k9	zase
studovat	studovat	k5eAaImF	studovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
ono	onen	k3xDgNnSc1	onen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsem	být	k5eAaImIp1nS	být
následující	následující	k2eAgInPc4d1	následující
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
strávil	strávit	k5eAaPmAgMnS	strávit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
–	–	k?	–
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
studoval	studovat	k5eAaImAgMnS	studovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
jeho	jeho	k3xOp3gFnSc1	jeho
majetná	majetný	k2eAgFnSc1d1	majetná
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zařídil	zařídit	k5eAaPmAgMnS	zařídit
malý	malý	k2eAgInSc4d1	malý
obchod	obchod	k1gInSc4	obchod
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
často	často	k6eAd1	často
chodil	chodit	k5eAaImAgMnS	chodit
k	k	k7c3	k
Madame	madame	k1gFnSc3	madame
Zereakian	Zereakiana	k1gFnPc2	Zereakiana
<g/>
,	,	kIx,	,
turecké	turecký	k2eAgFnSc3d1	turecká
věštkyni	věštkyně	k1gFnSc3	věštkyně
Christiana	Christian	k1gMnSc2	Christian
Diora	Dior	k1gMnSc2	Dior
<g/>
.	.	kIx.	.
</s>
<s>
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Předpověděla	předpovědět	k5eAaPmAgFnS	předpovědět
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
uspěju	uspět	k5eAaPmIp1nS	uspět
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
parfémů	parfém	k1gInPc2	parfém
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
návrhář	návrhář	k1gMnSc1	návrhář
firmy	firma	k1gFnSc2	firma
Chanel	Chanlo	k1gNnPc2	Chanlo
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
dělá	dělat	k5eAaImIp3nS	dělat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
však	však	k9	však
nic	nic	k3yNnSc1	nic
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
kontroverznosti	kontroverznost	k1gFnSc6	kontroverznost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
oslovil	oslovit	k5eAaPmAgMnS	oslovit
několik	několik	k4yIc4	několik
striptérek	striptérka	k1gFnPc2	striptérka
a	a	k8xC	a
bývalou	bývalý	k2eAgFnSc4d1	bývalá
italskou	italský	k2eAgFnSc4d1	italská
hvězdu	hvězda	k1gFnSc4	hvězda
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
Moanu	Moaen	k2eAgFnSc4d1	Moaen
Pozzi	Pozze	k1gFnSc4	Pozze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
modelky	modelek	k1gInPc1	modelek
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
přehlídky	přehlídka	k1gFnPc1	přehlídka
jeho	jeho	k3xOp3gFnSc2	jeho
černobílé	černobílý	k2eAgFnSc2d1	černobílá
kolekce	kolekce	k1gFnSc2	kolekce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Anna	Anna	k1gFnSc1	Anna
Wintour	Wintoura	k1gFnPc2	Wintoura
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
šéfredaktorka	šéfredaktorka	k1gFnSc1	šéfredaktorka
časopisu	časopis	k1gInSc2	časopis
Vogue	Vogu	k1gFnSc2	Vogu
<g/>
,	,	kIx,	,
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
přehlídky	přehlídka	k1gFnSc2	přehlídka
vstala	vstát	k5eAaPmAgFnS	vstát
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
také	také	k9	také
několik	několik	k4yIc4	několik
legendárních	legendární	k2eAgInPc2d1	legendární
módních	módní	k2eAgInPc2d1	módní
kousků	kousek	k1gInPc2	kousek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
jeho	jeho	k3xOp3gInPc4	jeho
"	"	kIx"	"
<g/>
sprchové	sprchový	k2eAgInPc4d1	sprchový
šaty	šat	k1gInPc4	šat
<g/>
"	"	kIx"	"
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
tryskající	tryskající	k2eAgFnSc2d1	tryskající
vody	voda	k1gFnSc2	voda
vyšitým	vyšitý	k2eAgMnSc7d1	vyšitý
z	z	k7c2	z
korálků	korálek	k1gInPc2	korálek
<g/>
,	,	kIx,	,
netradiční	tradiční	k2eNgInPc1d1	netradiční
vyjížďkové	vyjížďkový	k2eAgInPc1d1	vyjížďkový
šaty	šat	k1gInPc1	šat
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
množství	množství	k1gNnSc4	množství
výjimečně	výjimečně	k6eAd1	výjimečně
výstředních	výstřední	k2eAgInPc2d1	výstřední
klobouků	klobouk	k1gInPc2	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
módní	módní	k2eAgFnSc6d1	módní
akci	akce	k1gFnSc6	akce
v	v	k7c6	v
Lincolnově	Lincolnův	k2eAgNnSc6d1	Lincolnovo
Centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
stal	stát	k5eAaPmAgInS	stát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Calvinem	Calvin	k1gMnSc7	Calvin
Kleinem	Klein	k1gMnSc7	Klein
terčem	terč	k1gInSc7	terč
dortového	dortový	k2eAgInSc2d1	dortový
útoku	útok	k1gInSc2	útok
členů	člen	k1gMnPc2	člen
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
PETA	PETA	kA	PETA
<g/>
,	,	kIx,	,
protestujícím	protestující	k2eAgInSc6d1	protestující
proti	proti	k7c3	proti
nošení	nošení	k1gNnSc3	nošení
pravých	pravý	k2eAgInPc2d1	pravý
kožichů	kožich	k1gInPc2	kožich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
některé	některý	k3yIgInPc4	některý
modely	model	k1gInPc4	model
pro	pro	k7c4	pro
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůru	šňůra	k1gFnSc4	šňůra
Madonny	Madonna	k1gFnSc2	Madonna
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
pak	pak	k6eAd1	pak
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
i	i	k9	i
na	na	k7c6	na
modelech	model	k1gInPc6	model
pro	pro	k7c4	pro
koncertní	koncertní	k2eAgInSc4d1	koncertní
tour	tour	k1gInSc4	tour
Showgirls	Showgirlsa	k1gFnPc2	Showgirlsa
australské	australský	k2eAgFnSc2d1	australská
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Kylie	Kylie	k1gFnSc2	Kylie
Minogue	Minogu	k1gFnSc2	Minogu
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dámské	dámský	k2eAgNnSc4d1	dámské
i	i	k8xC	i
pánské	pánský	k2eAgInPc4d1	pánský
modely	model	k1gInPc4	model
pro	pro	k7c4	pro
švédskou	švédský	k2eAgFnSc4d1	švédská
oděvní	oděvní	k2eAgFnSc4d1	oděvní
značku	značka	k1gFnSc4	značka
H	H	kA	H
<g/>
&	&	k?	&
<g/>
M.	M.	kA	M.
Tato	tento	k3xDgFnSc1	tento
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vybraných	vybraný	k2eAgInPc6d1	vybraný
obchodech	obchod	k1gInPc6	obchod
uvedena	uveden	k2eAgFnSc1d1	uvedena
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
a	a	k8xC	a
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
musela	muset	k5eAaImAgFnS	muset
H	H	kA	H
<g/>
&	&	k?	&
<g/>
M	M	kA	M
oznámit	oznámit	k5eAaPmF	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
všechno	všechen	k3xTgNnSc1	všechen
toto	tento	k3xDgNnSc1	tento
zboží	zboží	k1gNnSc1	zboží
bylo	být	k5eAaImAgNnS	být
vyprodáno	vyprodat	k5eAaPmNgNnS	vyprodat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
také	také	k9	také
jako	jako	k9	jako
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Nafotil	nafotit	k5eAaPmAgInS	nafotit
sérii	série	k1gFnSc4	série
aktů	akt	k1gInPc2	akt
jihoafrického	jihoafrický	k2eAgMnSc2d1	jihoafrický
modela	model	k1gMnSc2	model
Davida	David	k1gMnSc2	David
Millera	Miller	k1gMnSc2	Miller
<g/>
,	,	kIx,	,
vydanou	vydaný	k2eAgFnSc4d1	vydaná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Visionaire	Visionair	k1gInSc5	Visionair
23	[number]	k4	23
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Emperor	Emperor	k1gMnSc1	Emperor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
Clothes	Clothesa	k1gFnPc2	Clothesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc1	show
Signé	Signé	k2eAgNnPc2d1	Signé
Chanel	Chanlo	k1gNnPc2	Chanlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dokumentovala	dokumentovat	k5eAaBmAgFnS	dokumentovat
proces	proces	k1gInSc4	proces
vytváření	vytváření	k1gNnSc2	vytváření
podzimní	podzimní	k2eAgFnSc2d1	podzimní
a	a	k8xC	a
zimní	zimní	k2eAgFnSc2d1	zimní
dámské	dámský	k2eAgFnSc2d1	dámská
kolekce	kolekce	k1gFnSc2	kolekce
značky	značka	k1gFnSc2	značka
Chanel	Chanela	k1gFnPc2	Chanela
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílat	k5eAaPmNgFnS	odvysílat
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Sundance	Sundance	k1gFnSc2	Sundance
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
během	během	k7c2	během
podzimu	podzim	k1gInSc2	podzim
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
pak	pak	k6eAd1	pak
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
novou	nový	k2eAgFnSc4d1	nová
kolekci	kolekce	k1gFnSc4	kolekce
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
K	K	kA	K
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
<g/>
.	.	kIx.	.
</s>
<s>
Kolekce	kolekce	k1gFnSc1	kolekce
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
trička	tričko	k1gNnPc4	tričko
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
výběr	výběr	k1gInSc4	výběr
džínů	džíny	k1gInPc2	džíny
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
známým	známý	k1gMnSc7	známý
také	také	k9	také
svojí	svůj	k3xOyFgFnSc7	svůj
neuvěřitelnou	uvěřitelný	k2eNgFnSc7d1	neuvěřitelná
změnou	změna	k1gFnSc7	změna
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
–	–	k?	–
během	během	k7c2	během
13	[number]	k4	13
měsíců	měsíc	k1gInPc2	měsíc
shodil	shodit	k5eAaPmAgInS	shodit
42	[number]	k4	42
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Prostě	prostě	k9	prostě
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
chtěl	chtít	k5eAaImAgMnS	chtít
oblékat	oblékat	k5eAaImF	oblékat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
nosit	nosit	k5eAaImF	nosit
věci	věc	k1gFnPc4	věc
od	od	k7c2	od
Hedi	Hed	k1gMnSc5	Hed
Slimane	Sliman	k1gMnSc5	Sliman
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
tyhle	tenhle	k3xDgInPc1	tenhle
střihy	střih	k1gInPc1	střih
<g/>
,	,	kIx,	,
předváděné	předváděný	k2eAgFnPc1d1	předváděná
velmi	velmi	k6eAd1	velmi
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
hubenými	hubený	k2eAgInPc7d1	hubený
mladými	mladý	k2eAgInPc7d1	mladý
modely	model	k1gInPc7	model
a	a	k8xC	a
ne	ne	k9	ne
muži	muž	k1gMnPc1	muž
mého	můj	k3xOp1gInSc2	můj
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
mě	já	k3xPp1nSc4	já
donutily	donutit	k5eAaPmAgFnP	donutit
shodit	shodit	k5eAaPmF	shodit
40	[number]	k4	40
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
přesně	přesně	k6eAd1	přesně
třináct	třináct	k4xCc1	třináct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dietu	dieta	k1gFnSc4	dieta
Lagerfeldovi	Lagerfeld	k1gMnSc3	Lagerfeld
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Houdret	Houdret	k1gInSc4	Houdret
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
spolupráce	spolupráce	k1gFnSc2	spolupráce
pak	pak	k6eAd1	pak
vzešla	vzejít	k5eAaPmAgFnS	vzejít
kniha	kniha	k1gFnSc1	kniha
The	The	k1gMnSc1	The
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
Diet	dieta	k1gFnPc2	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
původní	původní	k2eAgNnSc1d1	původní
příjmení	příjmení	k1gNnSc1	příjmení
bylo	být	k5eAaImAgNnS	být
Lagerfeldt	Lagerfeldt	k1gInSc4	Lagerfeldt
(	(	kIx(	(
<g/>
s	s	k7c7	s
"	"	kIx"	"
<g/>
t	t	k?	t
<g/>
"	"	kIx"	"
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
Lagerfeld	Lagerfeld	k1gInSc4	Lagerfeld
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
zní	znět	k5eAaImIp3nS	znět
víc	hodně	k6eAd2	hodně
obchodně	obchodně	k6eAd1	obchodně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
kolekci	kolekce	k1gFnSc4	kolekce
60000	[number]	k4	60000
hudebních	hudební	k2eAgMnPc2d1	hudební
CD	CD	kA	CD
převedl	převést	k5eAaPmAgMnS	převést
na	na	k7c4	na
přehrávače	přehrávač	k1gInPc4	přehrávač
iPod	iPoda	k1gFnPc2	iPoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
těchto	tento	k3xDgInPc2	tento
přehrávačů	přehrávač	k1gInPc2	přehrávač
<g/>
,	,	kIx,	,
rozmístěných	rozmístěný	k2eAgMnPc2d1	rozmístěný
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
rezidencích	rezidence	k1gFnPc6	rezidence
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbil	oblíbit	k5eAaPmAgInS	oblíbit
Chana	Chaen	k2eAgNnPc4d1	Chaen
Marshalla	Marshallo	k1gNnPc4	Marshallo
<g/>
,	,	kIx,	,
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Cat	Cat	k1gFnSc2	Cat
Power	Power	k1gInSc1	Power
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
několika	několik	k4yIc2	několik
evropských	evropský	k2eAgFnPc2d1	Evropská
přehlídek	přehlídka	k1gFnPc2	přehlídka
Chanelu	Chanel	k1gInSc2	Chanel
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Lagerfeld	Lagerfelda	k1gFnPc2	Lagerfelda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvícíh	mluvícíh	k1gInSc1	mluvícíh
zemích	zem	k1gFnPc6	zem
někdy	někdy	k6eAd1	někdy
zaměňováno	zaměňovat	k5eAaImNgNnS	zaměňovat
s	s	k7c7	s
názvem	název	k1gInSc7	název
pro	pro	k7c4	pro
proces	proces	k1gInSc4	proces
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
lager	lager	k1gInSc1	lager
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
"	"	kIx"	"
<g/>
ležák	ležák	k1gInSc1	ležák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
autora	autor	k1gMnSc4	autor
hudebního	hudební	k2eAgInSc2d1	hudební
výběru	výběr	k1gInSc2	výběr
s	s	k7c7	s
názvem	název	k1gInSc7	název
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
:	:	kIx,	:
Les	les	k1gInSc1	les
Musiques	Musiques	k1gInSc1	Musiques
que	que	k?	que
j	j	k?	j
<g/>
'	'	kIx"	'
<g/>
aime	aim	k1gMnSc2	aim
<g/>
.	.	kIx.	.
</s>
<s>
Carolina	Carolin	k2eAgFnSc1d1	Carolina
Herrera	Herrera	k1gFnSc1	Herrera
jej	on	k3xPp3gNnSc4	on
označila	označit	k5eAaPmAgFnS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejvíc	hodně	k6eAd3	hodně
k	k	k7c3	k
zulíbání	zulíbání	k1gNnSc3	zulíbání
<g/>
"	"	kIx"	"
člena	člen	k1gMnSc4	člen
módního	módní	k2eAgInSc2d1	módní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
portálu	portál	k1gInSc2	portál
YouTube	YouTub	k1gInSc5	YouTub
hovoří	hovořit	k5eAaImIp3nS	hovořit
doložitelně	doložitelně	k6eAd1	doložitelně
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Americká	americký	k2eAgNnPc1d1	americké
auta	auto	k1gNnPc1	auto
už	už	k6eAd1	už
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neprodávají	prodávat	k5eNaImIp3nP	prodávat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
designéři	designér	k1gMnPc1	designér
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
americký	americký	k2eAgInSc1d1	americký
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
si	se	k3xPyFc3	se
koupíte	koupit	k5eAaPmIp2nP	koupit
auto	auto	k1gNnSc4	auto
dnes	dnes	k6eAd1	dnes
nebo	nebo	k8xC	nebo
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
–	–	k?	–
auta	auto	k1gNnSc2	auto
už	už	k6eAd1	už
nejsou	být	k5eNaImIp3nP	být
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
americký	americký	k2eAgInSc1d1	americký
automobilový	automobilový	k2eAgInSc1d1	automobilový
průmysl	průmysl	k1gInSc1	průmysl
takové	takový	k3xDgInPc4	takový
problémy	problém	k1gInPc4	problém
–	–	k?	–
žádný	žádný	k3yNgInSc4	žádný
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
touha	touha	k1gFnSc1	touha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
pro	pro	k7c4	pro
Vanity	Vanita	k1gFnPc4	Vanita
Fair	fair	k2eAgFnPc4d1	fair
<g/>
,	,	kIx,	,
únor	únor	k1gInSc1	únor
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
módu	móda	k1gFnSc4	móda
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
jen	jen	k9	jen
minuta	minuta	k1gFnSc1	minuta
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
–	–	k?	–
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zničeny	zničit	k5eAaPmNgFnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	k9	kdyby
všichni	všechen	k3xTgMnPc1	všechen
dělali	dělat	k5eAaImAgMnP	dělat
všechno	všechen	k3xTgNnSc1	všechen
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
<g/>
,	,	kIx,	,
nikam	nikam	k6eAd1	nikam
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nemám	mít	k5eNaImIp1nS	mít
rád	rád	k6eAd1	rád
standardizovanou	standardizovaný	k2eAgFnSc4d1	standardizovaná
krásu	krása	k1gFnSc4	krása
–	–	k?	–
bez	bez	k7c2	bez
zvláštností	zvláštnost	k1gFnPc2	zvláštnost
žádná	žádný	k3yNgFnSc1	žádný
krása	krása	k1gFnSc1	krása
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
iPod	iPod	k1gInSc1	iPod
zcela	zcela	k6eAd1	zcela
změnil	změnit	k5eAaPmAgInS	změnit
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
lidé	člověk	k1gMnPc1	člověk
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Držení	držení	k1gNnSc1	držení
diety	dieta	k1gFnSc2	dieta
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyhrajete	vyhrát	k5eAaPmIp2nP	vyhrát
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c4	o
něco	něco	k3yInSc4	něco
přijdete	přijít	k5eAaPmIp2nP	přijít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
včerejšek	včerejšek	k1gInSc4	včerejšek
byl	být	k5eAaImAgInS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
dnešek	dnešek	k1gInSc1	dnešek
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
znehodnocují	znehodnocovat	k5eAaImIp3nP	znehodnocovat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
kulturu	kultura	k1gFnSc4	kultura
Německé	německý	k2eAgFnSc2d1	německá
fotografické	fotografický	k2eAgFnSc2d1	fotografická
společnosti	společnost	k1gFnSc2	společnost
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karl	Karla	k1gFnPc2	Karla
Lagerfeld	Lagerfeldo	k1gNnPc2	Lagerfeldo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Karl	Karl	k1gInSc1	Karl
Lagerfeld	Lagerfeld	k1gInSc1	Lagerfeld
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
článek	článek	k1gInSc1	článek
o	o	k7c4	o
Karlu	Karla	k1gFnSc4	Karla
Lagerfeldovi	Lagerfeldův	k2eAgMnPc1d1	Lagerfeldův
</s>
