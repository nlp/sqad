<s>
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Karl	Karl	k1gMnSc1	Karl
Otto	Otto	k1gMnSc1	Otto
Lagerfeldt	Lagerfeldt	k1gMnSc1	Lagerfeldt
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
světových	světový	k2eAgMnPc2d1	světový
módních	módní	k2eAgMnPc2d1	módní
návrhářů	návrhář	k1gMnPc2	návrhář
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
