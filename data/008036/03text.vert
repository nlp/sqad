<s>
Amnestie	amnestie	k1gFnSc1	amnestie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
amnéstia	amnéstium	k1gNnSc2	amnéstium
<g/>
,	,	kIx,	,
zapomenutí	zapomenutí	k1gNnSc1	zapomenutí
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c6	o
hromadném	hromadný	k2eAgInSc6d1	hromadný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
úplném	úplný	k2eAgNnSc6d1	úplné
nebo	nebo	k8xC	nebo
částečném	částečný	k2eAgNnSc6d1	částečné
prominutí	prominutí	k1gNnSc6	prominutí
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
o	o	k7c4	o
zahlazení	zahlazení	k1gNnSc4	zahlazení
odsouzení	odsouzení	k1gNnSc2	odsouzení
pro	pro	k7c4	pro
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Neruší	rušit	k5eNaImIp3nS	rušit
tedy	tedy	k9	tedy
rozsudek	rozsudek	k1gInSc4	rozsudek
ani	ani	k8xC	ani
občanskoprávní	občanskoprávní	k2eAgFnSc4d1	občanskoprávní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
odsouzeného	odsouzený	k1gMnSc2	odsouzený
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
povinnost	povinnost	k1gFnSc4	povinnost
nahradit	nahradit	k5eAaPmF	nahradit
škodu	škoda	k1gFnSc4	škoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bývá	bývat	k5eAaImIp3nS	bývat
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
abolicí	abolice	k1gFnSc7	abolice
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
o	o	k7c4	o
zastavení	zastavení	k1gNnSc4	zastavení
nebo	nebo	k8xC	nebo
nezahájení	nezahájení	k1gNnSc4	nezahájení
trestního	trestní	k2eAgNnSc2d1	trestní
řízení	řízení	k1gNnSc2	řízení
pro	pro	k7c4	pro
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
amnestie	amnestie	k1gFnSc1	amnestie
výsadní	výsadní	k2eAgNnSc4d1	výsadní
právo	právo	k1gNnSc4	právo
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
uděloval	udělovat	k5eAaImAgMnS	udělovat
obvykle	obvykle	k6eAd1	obvykle
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
často	často	k6eAd1	často
týkala	týkat	k5eAaImAgFnS	týkat
odpuštění	odpuštění	k1gNnSc4	odpuštění
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
oddlužení	oddlužení	k1gNnSc3	oddlužení
zemědělců	zemědělec	k1gMnPc2	zemědělec
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
jí	jíst	k5eAaImIp3nS	jíst
neomezeně	omezeně	k6eNd1	omezeně
disponoval	disponovat	k5eAaBmAgMnS	disponovat
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
milosti	milost	k1gFnSc2	milost
<g/>
,	,	kIx,	,
udělované	udělovaný	k2eAgFnPc1d1	udělovaná
individuálně	individuálně	k6eAd1	individuálně
<g/>
,	,	kIx,	,
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
amnestie	amnestie	k1gFnSc1	amnestie
všech	všecek	k3xTgInPc2	všecek
anebo	anebo	k8xC	anebo
jen	jen	k9	jen
určitých	určitý	k2eAgFnPc2d1	určitá
skupin	skupina	k1gFnPc2	skupina
odsouzených	odsouzený	k2eAgMnPc2d1	odsouzený
a	a	k8xC	a
trestaných	trestaný	k2eAgMnPc2d1	trestaný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jen	jen	k9	jen
za	za	k7c4	za
určité	určitý	k2eAgInPc4d1	určitý
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
omezena	omezit	k5eAaPmNgFnS	omezit
například	například	k6eAd1	například
výší	výše	k1gFnSc7	výše
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
spojena	spojit	k5eAaPmNgFnS	spojit
i	i	k9	i
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
týkat	týkat	k5eAaImF	týkat
jen	jen	k6eAd1	jen
trestaných	trestaný	k2eAgMnPc2d1	trestaný
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neměli	mít	k5eNaImAgMnP	mít
kázeňské	kázeňský	k2eAgInPc4d1	kázeňský
přestupky	přestupek	k1gInPc4	přestupek
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
naopak	naopak	k6eAd1	naopak
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
amnestovaný	amnestovaný	k2eAgInSc1d1	amnestovaný
po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
nebude	být	k5eNaImBp3nS	být
pravomocně	pravomocně	k6eAd1	pravomocně
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc1	výkon
amnestovaného	amnestovaný	k2eAgInSc2d1	amnestovaný
trestu	trest	k1gInSc2	trest
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
obnoví	obnovit	k5eAaPmIp3nS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
63	[number]	k4	63
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
písm	písma	k1gFnPc2	písma
<g/>
.	.	kIx.	.
k	k	k7c3	k
<g/>
)	)	kIx)	)
Ústavy	ústava	k1gFnSc2	ústava
o	o	k7c4	o
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
ústavně	ústavně	k6eAd1	ústavně
neodpovědného	odpovědný	k2eNgMnSc2d1	neodpovědný
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pouze	pouze	k6eAd1	pouze
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
platnosti	platnost	k1gFnSc3	platnost
kontrasignaci	kontrasignace	k1gFnSc3	kontrasignace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřeného	pověřený	k2eAgMnSc4d1	pověřený
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
spolupodpisem	spolupodpis	k1gInSc7	spolupodpis
ale	ale	k8xC	ale
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
ústavně	ústavně	k6eAd1	ústavně
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
,	,	kIx,	,
za	za	k7c4	za
amnestii	amnestie	k1gFnSc4	amnestie
přebírá	přebírat	k5eAaImIp3nS	přebírat
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
platná	platný	k2eAgFnSc1d1	platná
až	až	k9	až
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
výhradní	výhradní	k2eAgFnSc4d1	výhradní
pravomoc	pravomoc	k1gFnSc4	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
přitom	přitom	k6eAd1	přitom
nemá	mít	k5eNaImIp3nS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
udělovat	udělovat	k5eAaImF	udělovat
amnestii	amnestie	k1gFnSc4	amnestie
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
trestů	trest	k1gInPc2	trest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
i	i	k9	i
přestupků	přestupek	k1gInPc2	přestupek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
spolkových	spolkový	k2eAgFnPc6d1	spolková
zemích	zem	k1gFnPc6	zem
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
udělovat	udělovat	k5eAaImF	udělovat
amnestie	amnestie	k1gFnSc1	amnestie
pravidelně	pravidelně	k6eAd1	pravidelně
před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
vězňům	vězeň	k1gMnPc3	vězeň
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
může	moct	k5eAaImIp3nS	moct
amnestii	amnestie	k1gFnSc3	amnestie
udělit	udělit	k5eAaPmF	udělit
Spolkový	spolkový	k2eAgInSc4d1	spolkový
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
prezident	prezident	k1gMnSc1	prezident
nebo	nebo	k8xC	nebo
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Státní	státní	k2eAgFnSc1d1	státní
duma	duma	k1gFnSc1	duma
<g/>
,	,	kIx,	,
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
ji	on	k3xPp3gFnSc4	on
uděluje	udělovat	k5eAaImIp3nS	udělovat
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
omezené	omezený	k2eAgFnSc2d1	omezená
amnestie	amnestie	k1gFnSc2	amnestie
dána	dát	k5eAaPmNgFnS	dát
prezidentovi	prezident	k1gMnSc3	prezident
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
prezidenti	prezident	k1gMnPc1	prezident
dobrovolně	dobrovolně	k6eAd1	dobrovolně
zavazují	zavazovat	k5eAaImIp3nP	zavazovat
amnestii	amnestie	k1gFnSc4	amnestie
neudělovat	udělovat	k5eNaImF	udělovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
amnestii	amnestie	k1gFnSc4	amnestie
prezident	prezident	k1gMnSc1	prezident
na	na	k7c6	na
základě	základ	k1gInSc6	základ
doporučení	doporučení	k1gNnSc2	doporučení
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Udílet	udílet	k5eAaImF	udílet
amnestie	amnestie	k1gFnSc2	amnestie
byl	být	k5eAaImAgInS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
prezident	prezident	k1gMnSc1	prezident
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
byly	být	k5eAaImAgInP	být
prominuty	prominut	k2eAgInPc1d1	prominut
tresty	trest	k1gInPc1	trest
uložené	uložený	k2eAgInPc1d1	uložený
za	za	k7c2	za
předchozího	předchozí	k2eAgInSc2d1	předchozí
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Amnestie	amnestie	k1gFnSc1	amnestie
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
zhruba	zhruba	k6eAd1	zhruba
23	[number]	k4	23
tisíc	tisíc	k4xCgInSc4	tisíc
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
na	na	k7c4	na
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
31	[number]	k4	31
tisíc	tisíc	k4xCgInPc2	tisíc
uvězněných	uvězněný	k2eAgInPc2d1	uvězněný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
amnestovanými	amnestovaný	k2eAgMnPc7d1	amnestovaný
byl	být	k5eAaImAgInS	být
i	i	k9	i
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
Jozef	Jozef	k1gMnSc1	Jozef
Slovák	Slovák	k1gMnSc1	Slovák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
spáchal	spáchat	k5eAaPmAgInS	spáchat
nejméně	málo	k6eAd3	málo
čtyři	čtyři	k4xCgFnPc1	čtyři
další	další	k2eAgFnPc1d1	další
vraždy	vražda	k1gFnPc1	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
posílení	posílení	k1gNnSc2	posílení
záruk	záruka	k1gFnPc2	záruka
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
veřejného	veřejný	k2eAgInSc2d1	veřejný
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
pořádku	pořádek	k1gInSc2	pořádek
<g/>
"	"	kIx"	"
a	a	k8xC	a
týkala	týkat	k5eAaImAgFnS	týkat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
abolice	abolice	k1gFnPc1	abolice
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
nedovoleného	dovolený	k2eNgNnSc2d1	nedovolené
ozbrojování	ozbrojování	k1gNnSc2	ozbrojování
<g/>
,	,	kIx,	,
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
dobrovolného	dobrovolný	k2eAgNnSc2d1	dobrovolné
odevzdání	odevzdání	k1gNnSc2	odevzdání
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
výbušnin	výbušnina	k1gFnPc2	výbušnina
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Udílet	udílet	k5eAaImF	udílet
amnestie	amnestie	k1gFnPc4	amnestie
je	být	k5eAaImIp3nS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ale	ale	k8xC	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
platnosti	platnost	k1gFnSc3	platnost
spolupodpis	spolupodpis	k1gInSc4	spolupodpis
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřeného	pověřený	k2eAgMnSc4d1	pověřený
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
amnestii	amnestie	k1gFnSc4	amnestie
pak	pak	k6eAd1	pak
nese	nést	k5eAaImIp3nS	nést
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
ji	on	k3xPp3gFnSc4	on
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
tisíc	tisíc	k4xCgInSc4	tisíc
vězňů	vězeň	k1gMnPc2	vězeň
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
jen	jen	k9	jen
asi	asi	k9	asi
130	[number]	k4	130
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
prominutí	prominutí	k1gNnSc4	prominutí
trestů	trest	k1gInPc2	trest
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
u	u	k7c2	u
příležitostí	příležitost	k1gFnPc2	příležitost
svého	své	k1gNnSc2	své
znovuzvolení	znovuzvolení	k1gNnSc2	znovuzvolení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
tisíc	tisíc	k4xCgInSc4	tisíc
vězňů	vězeň	k1gMnPc2	vězeň
bylo	být	k5eAaImAgNnS	být
propuštěno	propustit	k5eAaPmNgNnS	propustit
přes	přes	k7c4	přes
930	[number]	k4	930
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
dalších	další	k2eAgNnPc2d1	další
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
trestní	trestní	k2eAgNnSc1d1	trestní
stíhání	stíhání	k1gNnSc1	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Amnestie	amnestie	k1gFnSc2	amnestie
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
amnestii	amnestie	k1gFnSc4	amnestie
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
svého	svůj	k3xOyFgInSc2	svůj
druhého	druhý	k4xOgNnSc2	druhý
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
osamostatnění	osamostatnění	k1gNnPc2	osamostatnění
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
amnestie	amnestie	k1gFnSc1	amnestie
týká	týkat	k5eAaImIp3nS	týkat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
32	[number]	k4	32
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vězeňské	vězeňský	k2eAgFnPc1d1	vězeňská
brány	brána	k1gFnPc1	brána
opustilo	opustit	k5eAaPmAgNnS	opustit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
145	[number]	k4	145
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
odhad	odhad	k1gInSc1	odhad
činil	činit	k5eAaImAgInS	činit
7	[number]	k4	7
416	[number]	k4	416
pravomocně	pravomocně	k6eAd1	pravomocně
odsouzených	odsouzený	k2eAgFnPc2d1	odsouzená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
však	však	k9	však
dotkne	dotknout	k5eAaPmIp3nS	dotknout
i	i	k9	i
dalších	další	k2eAgMnPc2d1	další
zhruba	zhruba	k6eAd1	zhruba
14	[number]	k4	14
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
podmíněně	podmíněně	k6eAd1	podmíněně
odsouzených	odsouzený	k2eAgMnPc2d1	odsouzený
s	s	k7c7	s
probačním	probační	k2eAgInSc7d1	probační
dohledem	dohled	k1gInSc7	dohled
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
