<s>
Zámek	zámek	k1gInSc1	zámek
Sychrov	Sychrov	k1gInSc1	Sychrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
obci	obec	k1gFnSc6	obec
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
16	[number]	k4	16
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Liberce	Liberec	k1gInSc2	Liberec
a	a	k8xC	a
6	[number]	k4	6
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Turnova	Turnov	k1gInSc2	Turnov
<g/>
.	.	kIx.	.
</s>
