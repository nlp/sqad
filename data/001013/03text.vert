<s>
Zámek	zámek	k1gInSc1	zámek
Sychrov	Sychrov	k1gInSc1	Sychrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
obci	obec	k1gFnSc6	obec
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
16	[number]	k4	16
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Liberce	Liberec	k1gInSc2	Liberec
a	a	k8xC	a
6	[number]	k4	6
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Turnova	Turnov	k1gInSc2	Turnov
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
státní	státní	k2eAgInSc4d1	státní
zámek	zámek	k1gInSc4	zámek
zpřístupněný	zpřístupněný	k2eAgInSc4d1	zpřístupněný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc4	jeho
správu	správa	k1gFnSc4	správa
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
unikátním	unikátní	k2eAgInSc7d1	unikátní
příkladem	příklad	k1gInSc7	příklad
novogotického	novogotický	k2eAgNnSc2d1	novogotické
šlechtického	šlechtický	k2eAgNnSc2d1	šlechtické
sídla	sídlo	k1gNnSc2	sídlo
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
zámecké	zámecký	k2eAgFnSc2d1	zámecká
budovy	budova	k1gFnSc2	budova
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
mobiliářem	mobiliář	k1gInSc7	mobiliář
k	k	k7c3	k
areálu	areál	k1gInSc3	areál
zámku	zámek	k1gInSc2	zámek
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
stávala	stávat	k5eAaImAgFnS	stávat
původně	původně	k6eAd1	původně
pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
zničená	zničený	k2eAgFnSc1d1	zničená
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1690	[number]	k4	1690
<g/>
-	-	kIx~	-
<g/>
1693	[number]	k4	1693
si	se	k3xPyFc3	se
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
majitelé	majitel	k1gMnPc1	majitel
Sychrova	Sychrův	k2eAgInSc2d1	Sychrův
Lamottové	Lamottové	k2eAgInSc2d1	Lamottové
z	z	k7c2	z
Frintropu	Frintrop	k1gInSc2	Frintrop
postavili	postavit	k5eAaPmAgMnP	postavit
dvoupatrový	dvoupatrový	k2eAgInSc4d1	dvoupatrový
barokní	barokní	k2eAgInSc4d1	barokní
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
koupila	koupit	k5eAaPmAgFnS	koupit
sychrovský	sychrovský	k2eAgInSc4d1	sychrovský
zámek	zámek	k1gInSc4	zámek
původem	původ	k1gInSc7	původ
francouzská	francouzský	k2eAgFnSc1d1	francouzská
knížecí	knížecí	k2eAgFnSc1d1	knížecí
rodina	rodina	k1gFnSc1	rodina
Rohanů	Rohan	k1gMnPc2	Rohan
a	a	k8xC	a
Sychrov	Sychrov	k1gInSc1	Sychrov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
hlavních	hlavní	k2eAgNnPc2d1	hlavní
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
starý	starý	k2eAgInSc1d1	starý
barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
nesplňoval	splňovat	k5eNaImAgInS	splňovat
nároky	nárok	k1gInPc4	nárok
Rohanů	Rohan	k1gMnPc2	Rohan
na	na	k7c4	na
reprezentaci	reprezentace	k1gFnSc4	reprezentace
a	a	k8xC	a
pohodlné	pohodlný	k2eAgNnSc4d1	pohodlné
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
jej	on	k3xPp3gMnSc4	on
kníže	kníže	k1gMnSc1	kníže
Karel	Karel	k1gMnSc1	Karel
Alain	Alain	k1gMnSc1	Alain
Gabriel	Gabriel	k1gMnSc1	Gabriel
Rohan	Rohan	k1gMnSc1	Rohan
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozšířit	rozšířit	k5eAaPmF	rozšířit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
pozdního	pozdní	k2eAgInSc2d1	pozdní
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Kamil	Kamil	k1gMnSc1	Kamil
Filip	Filip	k1gMnSc1	Filip
Josef	Josef	k1gMnSc1	Josef
Idesbald	Idesbald	k1gMnSc1	Idesbald
Rohan	Rohan	k1gMnSc1	Rohan
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
provedl	provést	k5eAaPmAgInS	provést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
zámeckých	zámecký	k2eAgFnPc2d1	zámecká
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
romantické	romantický	k2eAgFnSc2d1	romantická
gotiky	gotika	k1gFnSc2	gotika
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
byly	být	k5eAaImAgInP	být
novogoticky	novogoticky	k6eAd1	novogoticky
upraveny	upravit	k5eAaPmNgInP	upravit
i	i	k9	i
veškeré	veškerý	k3xTgInPc4	veškerý
zámecké	zámecký	k2eAgInPc4d1	zámecký
interiéry	interiér	k1gInPc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížete	kníže	k1gMnSc2	kníže
Kamila	Kamil	k1gMnSc2	Kamil
<g/>
,	,	kIx,	,
vášnivého	vášnivý	k2eAgMnSc2d1	vášnivý
botanika	botanik	k1gMnSc2	botanik
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
přízni	přízeň	k1gFnSc6	přízeň
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
zámecký	zámecký	k2eAgInSc1d1	zámecký
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
majitel	majitel	k1gMnSc1	majitel
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Rohanů	Rohan	k1gMnPc2	Rohan
<g/>
,	,	kIx,	,
Alain	Alain	k1gMnSc1	Alain
Rohan	Rohan	k1gMnSc1	Rohan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
přijal	přijmout	k5eAaPmAgMnS	přijmout
německé	německý	k2eAgNnSc4d1	německé
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
majetek	majetek	k1gInSc1	majetek
Rohanů	Rohan	k1gMnPc2	Rohan
konfiskován	konfiskovat	k5eAaBmNgInS	konfiskovat
československým	československý	k2eAgInSc7d1	československý
státem	stát	k1gInSc7	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dekretu	dekret	k1gInSc2	dekret
č.	č.	k?	č.
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
postupně	postupně	k6eAd1	postupně
zpřístupňován	zpřístupňovat	k5eAaImNgInS	zpřístupňovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nařízením	nařízení	k1gNnSc7	nařízení
vlády	vláda	k1gFnSc2	vláda
č.	č.	k?	č.
262	[number]	k4	262
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1690	[number]	k4	1690
až	až	k9	až
1693	[number]	k4	1693
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
zdi	zeď	k1gFnPc1	zeď
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
zachovány	zachován	k2eAgInPc1d1	zachován
v	v	k7c6	v
parkovém	parkový	k2eAgNnSc6d1	parkové
křídle	křídlo	k1gNnSc6	křídlo
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
několika	několik	k4yIc2	několik
stavebních	stavební	k2eAgFnPc2d1	stavební
úprav	úprava	k1gFnPc2	úprava
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přistavěny	přistavěn	k2eAgFnPc4d1	přistavěna
další	další	k2eAgFnPc4d1	další
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
čestný	čestný	k2eAgInSc4d1	čestný
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
zámecké	zámecký	k2eAgNnSc4d1	zámecké
nádvoří	nádvoří	k1gNnSc4	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
významná	významný	k2eAgFnSc1d1	významná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
zámku	zámek	k1gInSc2	zámek
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
půli	půle	k1gFnSc6	půle
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
empírovém	empírový	k2eAgInSc6d1	empírový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1847	[number]	k4	1847
až	až	k9	až
1862	[number]	k4	1862
následovala	následovat	k5eAaImAgFnS	následovat
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
<g/>
,	,	kIx,	,
romantická	romantický	k2eAgFnSc1d1	romantická
novogotická	novogotický	k2eAgFnSc1d1	novogotická
přestavba	přestavba	k1gFnSc1	přestavba
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Josefa	Josef	k1gMnSc2	Josef
Pruvota	Pruvot	k1gMnSc2	Pruvot
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dala	dát	k5eAaPmAgFnS	dát
zámku	zámek	k1gInSc2	zámek
jeho	jeho	k3xOp3gFnSc4	jeho
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Neopakovatelný	opakovatelný	k2eNgInSc4d1	neopakovatelný
ráz	ráz	k1gInSc4	ráz
vtiskl	vtisknout	k5eAaPmAgMnS	vtisknout
Sychrovu	Sychrův	k2eAgFnSc4d1	Sychrova
řezbář	řezbář	k1gMnSc1	řezbář
Petr	Petr	k1gMnSc1	Petr
Bušek	Bušek	k1gMnSc1	Bušek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
během	během	k7c2	během
řady	řada	k1gFnSc2	řada
let	léto	k1gNnPc2	léto
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dílně	dílna	k1gFnSc6	dílna
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
obložení	obložení	k1gNnSc4	obložení
stěn	stěna	k1gFnPc2	stěna
a	a	k8xC	a
stropů	strop	k1gInPc2	strop
řady	řada	k1gFnSc2	řada
zámeckých	zámecký	k2eAgInPc2d1	zámecký
salónů	salón	k1gInPc2	salón
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
veškerý	veškerý	k3xTgInSc4	veškerý
nábytek	nábytek	k1gInSc4	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
modernizaci	modernizace	k1gFnSc3	modernizace
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
zastarale	zastarale	k6eAd1	zastarale
zdobného	zdobný	k2eAgNnSc2d1	zdobné
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
některé	některý	k3yIgFnPc1	některý
Buškovy	Buškův	k2eAgFnPc1d1	Buškova
řezby	řezba	k1gFnPc1	řezba
z	z	k7c2	z
interiérů	interiér	k1gInPc2	interiér
a	a	k8xC	a
z	z	k7c2	z
exteriérů	exteriér	k1gInPc2	exteriér
novogotické	novogotický	k2eAgInPc4d1	novogotický
arkýře	arkýř	k1gInPc4	arkýř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
odstraněno	odstraněn	k2eAgNnSc1d1	odstraněno
omítané	omítaný	k2eAgNnSc1d1	omítané
zdivo	zdivo	k1gNnSc1	zdivo
na	na	k7c6	na
věžích	věž	k1gFnPc6	věž
a	a	k8xC	a
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
lomovým	lomový	k2eAgNnSc7d1	lomové
zdivem	zdivo	k1gNnSc7	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnPc1	rekonstrukce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
započaly	započnout	k5eAaPmAgFnP	započnout
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
navrácení	navrácení	k1gNnSc3	navrácení
podoby	podoba	k1gFnSc2	podoba
zámku	zámek	k1gInSc2	zámek
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
měl	mít	k5eAaImAgInS	mít
Sychrov	Sychrov	k1gInSc1	Sychrov
po	po	k7c6	po
zámku	zámek	k1gInSc6	zámek
Konopiště	Konopiště	k1gNnSc2	Konopiště
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
sbírkový	sbírkový	k2eAgInSc1d1	sbírkový
fond	fond	k1gInSc1	fond
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
48	[number]	k4	48
750	[number]	k4	750
evidovaných	evidovaný	k2eAgMnPc2d1	evidovaný
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
jak	jak	k6eAd1	jak
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ze	z	k7c2	z
svozů	svoz	k1gInPc2	svoz
(	(	kIx(	(
<g/>
Sychrov	Sychrov	k1gInSc1	Sychrov
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
zestátnění	zestátnění	k1gNnSc6	zestátnění
velkým	velký	k2eAgInSc7d1	velký
skladem	sklad	k1gInSc7	sklad
svozů	svoz	k1gInPc2	svoz
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
90	[number]	k4	90
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
mobiliáře	mobiliář	k1gInSc2	mobiliář
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
rozvezen	rozvezen	k2eAgMnSc1d1	rozvezen
a	a	k8xC	a
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
částečně	částečně	k6eAd1	částečně
vrácen	vrácen	k2eAgInSc4d1	vrácen
v	v	k7c6	v
restitucích	restituce	k1gFnPc6	restituce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
evidováno	evidovat	k5eAaImNgNnS	evidovat
7	[number]	k4	7
209	[number]	k4	209
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
naopak	naopak	k6eAd1	naopak
vedle	vedle	k7c2	vedle
svozů	svoz	k1gInPc2	svoz
byly	být	k5eAaImAgFnP	být
nejcennější	cenný	k2eAgFnPc1d3	nejcennější
předměty	předmět	k1gInPc4	předmět
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
odvezeny	odvezen	k2eAgFnPc1d1	odvezena
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
Rohanské	Rohanský	k2eAgFnPc4d1	Rohanská
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
,	,	kIx,	,
iluminovaný	iluminovaný	k2eAgInSc4d1	iluminovaný
rukopis	rukopis	k1gInSc4	rukopis
hodinek	hodinka	k1gFnPc2	hodinka
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
kolekci	kolekce	k1gFnSc6	kolekce
rohanských	rohanský	k2eAgInPc2d1	rohanský
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
portrétů	portrét	k1gInPc2	portrét
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
příklady	příklad	k1gInPc1	příklad
italského	italský	k2eAgNnSc2d1	italské
a	a	k8xC	a
nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
malířství	malířství	k1gNnSc2	malířství
získala	získat	k5eAaPmAgFnS	získat
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
důležité	důležitý	k2eAgFnPc1d1	důležitá
písemnosti	písemnost	k1gFnPc1	písemnost
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Státního	státní	k2eAgInSc2d1	státní
oblastního	oblastní	k2eAgInSc2d1	oblastní
archivu	archiv	k1gInSc2	archiv
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zbývajících	zbývající	k2eAgInPc2d1	zbývající
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
zejména	zejména	k9	zejména
kolekce	kolekce	k1gFnSc1	kolekce
francouzské	francouzský	k2eAgFnSc2d1	francouzská
portrétní	portrétní	k2eAgFnSc2d1	portrétní
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Rohanská	Rohanský	k2eAgFnSc1d1	Rohanská
portrétní	portrétní	k2eAgFnSc1d1	portrétní
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
francouzského	francouzský	k2eAgNnSc2d1	francouzské
portrétního	portrétní	k2eAgNnSc2d1	portrétní
malířství	malířství	k1gNnSc2	malířství
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Čítá	čítat	k5eAaImIp3nS	čítat
přesně	přesně	k6eAd1	přesně
243	[number]	k4	243
portrétů	portrét	k1gInPc2	portrét
členů	člen	k1gInPc2	člen
rohanského	rohanský	k2eAgInSc2d1	rohanský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
spřízněných	spřízněný	k2eAgInPc2d1	spřízněný
rodů	rod	k1gInPc2	rod
i	i	k8xC	i
členů	člen	k1gMnPc2	člen
francouzské	francouzský	k2eAgFnSc2d1	francouzská
královské	královský	k2eAgFnSc2d1	královská
dynastie	dynastie	k1gFnSc2	dynastie
Bourbonů	bourbon	k1gInPc2	bourbon
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
portrétů	portrét	k1gInPc2	portrét
králů	král	k1gMnPc2	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
cenný	cenný	k2eAgInSc4d1	cenný
je	být	k5eAaImIp3nS	být
Jezdecký	jezdecký	k2eAgInSc4d1	jezdecký
portrét	portrét	k1gInSc4	portrét
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
přes	přes	k7c4	přes
Rýn	Rýn	k1gInSc4	Rýn
od	od	k7c2	od
Charlese	Charles	k1gMnSc2	Charles
Le	Le	k1gMnSc2	Le
Bruna	Bruna	k1gMnSc1	Bruna
a	a	k8xC	a
dílenská	dílenský	k2eAgFnSc1d1	dílenská
malba	malba	k1gFnSc1	malba
portrétu	portrét	k1gInSc2	portrét
stejného	stejný	k2eAgMnSc4d1	stejný
krále	král	k1gMnSc4	král
od	od	k7c2	od
Hyacinthe	Hyacinth	k1gFnSc2	Hyacinth
Rigauda	Rigauda	k1gFnSc1	Rigauda
<g/>
,	,	kIx,	,
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
hlavně	hlavně	k9	hlavně
svým	svůj	k3xOyFgInSc7	svůj
osudem	osud	k1gInSc7	osud
je	být	k5eAaImIp3nS	být
portrét	portrét	k1gInSc1	portrét
Marie	Maria	k1gFnSc2	Maria
de	de	k?	de
Rohan	Rohan	k1gMnSc1	Rohan
(	(	kIx(	(
<g/>
okruh	okruh	k1gInSc1	okruh
Henriho	Henri	k1gMnSc2	Henri
Gascara	Gascar	k1gMnSc2	Gascar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c4	na
Sychrov	Sychrov	k1gInSc4	Sychrov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
francouzské	francouzský	k2eAgInPc1d1	francouzský
importy	import	k1gInPc1	import
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
restaurovány	restaurovat	k5eAaBmNgFnP	restaurovat
Karlem	Karel	k1gMnSc7	Karel
Javůrkem	Javůrek	k1gMnSc7	Javůrek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
portrétní	portrétní	k2eAgFnSc4d1	portrétní
galerii	galerie	k1gFnSc4	galerie
o	o	k7c4	o
několik	několik	k4yIc4	několik
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
podobizen	podobizna	k1gFnPc2	podobizna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
doplnění	doplnění	k1gNnSc6	doplnění
se	se	k3xPyFc4	se
postarali	postarat	k5eAaPmAgMnP	postarat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
čeští	český	k2eAgMnPc1d1	český
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Brandeis	Brandeis	k1gFnSc1	Brandeis
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hellich	Hellich	k1gMnSc1	Hellich
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Mašek	Mašek	k1gMnSc1	Mašek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
obrazů	obraz	k1gInPc2	obraz
sychrovských	sychrovský	k2eAgFnPc2d1	sychrovská
sbírek	sbírka	k1gFnPc2	sbírka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
dvě	dva	k4xCgFnPc1	dva
nejstarší	starý	k2eAgFnPc1d3	nejstarší
italské	italský	k2eAgFnPc1d1	italská
desky	deska	k1gFnPc1	deska
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Trůnící	trůnící	k2eAgFnSc1d1	trůnící
madona	madona	k1gFnSc1	madona
s	s	k7c7	s
anděly	anděl	k1gMnPc7	anděl
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Petrem	Petr	k1gMnSc7	Petr
a	a	k8xC	a
Pavlem	Pavel	k1gMnSc7	Pavel
<g/>
)	)	kIx)	)
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Sv.	sv.	kA	sv.
Rodina	rodina	k1gFnSc1	rodina
se	s	k7c7	s
sv.	sv.	kA	sv.
Kateřinou	Kateřina	k1gFnSc7	Kateřina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkoryse	velkoryse	k6eAd1	velkoryse
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupena	zastoupen	k2eAgNnPc1d1	zastoupeno
díla	dílo	k1gNnPc1	dílo
malířů	malíř	k1gMnPc2	malíř
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Engelmüller	Engelmüller	k1gMnSc1	Engelmüller
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Charlemont	Charlemont	k1gMnSc1	Charlemont
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Mařák	Mařák	k1gMnSc1	Mařák
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Ullik	Ullik	k1gInSc1	Ullik
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sochařských	sochařský	k2eAgNnPc2d1	sochařské
děl	dělo	k1gNnPc2	dělo
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
jak	jak	k8xC	jak
importy	import	k1gInPc1	import
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
mramorová	mramorový	k2eAgFnSc1d1	mramorová
busta	busta	k1gFnSc1	busta
Markéty	Markéta	k1gFnSc2	Markéta
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
sochy	socha	k1gFnPc1	socha
ze	z	k7c2	z
svozů	svoz	k1gInPc2	svoz
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
sbírku	sbírka	k1gFnSc4	sbírka
doplnil	doplnit	k5eAaPmAgMnS	doplnit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Max	Max	k1gMnSc1	Max
sochami	socha	k1gFnPc7	socha
Henriho	Henri	k1gMnSc2	Henri
II	II	kA	II
<g/>
.	.	kIx.	.
de	de	k?	de
Rohan	Rohan	k1gMnSc1	Rohan
a	a	k8xC	a
Godefroye	Godefroye	k1gFnSc1	Godefroye
z	z	k7c2	z
Bouillonu	Bouillon	k1gInSc2	Bouillon
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
sbírka	sbírka	k1gFnSc1	sbírka
leptaného	leptaný	k2eAgNnSc2d1	leptané
a	a	k8xC	a
malovaného	malovaný	k2eAgNnSc2d1	malované
českého	český	k2eAgNnSc2d1	české
skla	sklo	k1gNnSc2	sklo
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
severočeského	severočeský	k2eAgNnSc2d1	Severočeské
skla	sklo	k1gNnSc2	sklo
obsahujícího	obsahující	k2eAgNnSc2d1	obsahující
invence	invence	k1gFnSc1	invence
od	od	k7c2	od
Friedricha	Friedrich	k1gMnSc2	Friedrich
Egermanna	Egermann	k1gMnSc2	Egermann
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
sbírka	sbírka	k1gFnSc1	sbírka
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
odlitcích	odlitek	k1gInPc6	odlitek
i	i	k8xC	i
míšeňského	míšeňský	k2eAgInSc2d1	míšeňský
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
mobiliář	mobiliář	k1gInSc4	mobiliář
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
Petrem	Petr	k1gMnSc7	Petr
Buškem	Bušek	k1gMnSc7	Bušek
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
podle	podle	k7c2	podle
předloh	předloha	k1gFnPc2	předloha
Augusta	August	k1gMnSc2	August
Pugina	Pugin	k1gMnSc2	Pugin
a	a	k8xC	a
Georga	Georg	k1gMnSc2	Georg
Gottloba	Gottloba	k1gFnSc1	Gottloba
Ungewittera	Ungewitter	k1gMnSc2	Ungewitter
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
starší	starý	k2eAgInPc1d2	starší
kusy	kus	k1gInPc1	kus
empirové	empirový	k2eAgInPc1d1	empirový
<g/>
,	,	kIx,	,
novoklasicistní	novoklasicistní	k2eAgInPc1d1	novoklasicistní
a	a	k8xC	a
barokní	barokní	k2eAgInPc1d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porohanské	porohanský	k2eAgFnSc6d1	porohanský
době	doba	k1gFnSc6	doba
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
sbírky	sbírka	k1gFnPc4	sbírka
svozy	svoz	k1gInPc4	svoz
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
konfiskáty	konfiskát	k1gInPc1	konfiskát
ze	z	k7c2	z
severočeských	severočeský	k2eAgInPc2d1	severočeský
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Lázně	lázeň	k1gFnPc1	lázeň
Bělohrad	Bělohrad	k1gInSc1	Bělohrad
(	(	kIx(	(
<g/>
merveldtské	merveldtský	k2eAgInPc1d1	merveldtský
portréty	portrét	k1gInPc1	portrét
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kopidlno	Kopidlno	k1gNnSc1	Kopidlno
(	(	kIx(	(
<g/>
schlickovská	schlickovský	k2eAgFnSc1d1	schlickovský
a	a	k8xC	a
waldsteinská	waldsteinský	k2eAgFnSc1d1	waldsteinský
galerie	galerie	k1gFnSc1	galerie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Skála	skála	k1gFnSc1	skála
(	(	kIx(	(
<g/>
Aerenthalové	Aerenthalové	k2eAgFnSc1d1	Aerenthalové
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Maršov	Maršov	k1gInSc1	Maršov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
místnosti	místnost	k1gFnSc6	místnost
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
zlatníka	zlatník	k1gMnSc2	zlatník
a	a	k8xC	a
klenotníka	klenotník	k1gMnSc2	klenotník
Františka	František	k1gMnSc2	František
Khynla	Khynl	k1gMnSc2	Khynl
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
dochoval	dochovat	k5eAaPmAgInS	dochovat
v	v	k7c6	v
rozloze	rozloha	k1gFnSc6	rozloha
23	[number]	k4	23
ha	ha	kA	ha
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
anglickém	anglický	k2eAgMnSc6d1	anglický
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zejména	zejména	k9	zejména
zásluhou	zásluha	k1gFnSc7	zásluha
knížete	kníže	k1gMnSc2	kníže
Kamila	Kamil	k1gMnSc2	Kamil
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zahradníků	zahradník	k1gMnPc2	zahradník
Vincenta	Vincent	k1gMnSc2	Vincent
Zakouřila	zakouřit	k5eAaPmAgFnS	zakouřit
a	a	k8xC	a
poté	poté	k6eAd1	poté
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Maška	Mašek	k1gMnSc2	Mašek
a	a	k8xC	a
stavitele	stavitel	k1gMnSc2	stavitel
Josefa	Josef	k1gMnSc2	Josef
Pruvota	Pruvot	k1gMnSc2	Pruvot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
zahradní	zahradní	k2eAgFnSc4d1	zahradní
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Výraznější	výrazný	k2eAgFnPc1d2	výraznější
přestavby	přestavba	k1gFnPc1	přestavba
zahrady	zahrada	k1gFnSc2	zahrada
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
ještě	ještě	k9	ještě
za	za	k7c4	za
Kamilova	Kamilův	k2eAgMnSc4d1	Kamilův
předchůdce	předchůdce	k1gMnSc4	předchůdce
Karla	Karel	k1gMnSc2	Karel
Alaina	Alain	k1gMnSc2	Alain
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
rozložen	rozložit	k5eAaPmNgInS	rozložit
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
osách	osa	k1gFnPc6	osa
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
kompozice	kompozice	k1gFnSc1	kompozice
přetrvávající	přetrvávající	k2eAgFnSc1d1	přetrvávající
od	od	k7c2	od
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
přestavby	přestavba	k1gFnSc2	přestavba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
patte	patit	k5eAaBmRp2nP	patit
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oie	oie	k?	oie
(	(	kIx(	(
<g/>
husí	husí	k2eAgFnSc1d1	husí
nožka	nožka	k1gFnSc1	nožka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
centrální	centrální	k2eAgFnSc1d1	centrální
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
průhledová	průhledový	k2eAgFnSc1d1	průhledová
a	a	k8xC	a
zakončena	zakončit	k5eAaPmNgFnS	zakončit
stavbou	stavba	k1gFnSc7	stavba
oranžérie	oranžérie	k1gFnSc2	oranžérie
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
osa	osa	k1gFnSc1	osa
(	(	kIx(	(
<g/>
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
pravá	pravá	k1gFnSc1	pravá
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
příjezdová	příjezdový	k2eAgFnSc1d1	příjezdová
cesta	cesta	k1gFnSc1	cesta
lemována	lemován	k2eAgFnSc1d1	lemována
alejí	alej	k1gFnSc7	alej
pyramidálních	pyramidální	k2eAgInPc2d1	pyramidální
dubů	dub	k1gInPc2	dub
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
lipovou	lipový	k2eAgFnSc7d1	Lipová
alejí	alej	k1gFnSc7	alej
(	(	kIx(	(
<g/>
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xC	jako
Rohanka	Rohanka	k1gFnSc1	Rohanka
<g/>
)	)	kIx)	)
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
krajiny	krajina	k1gFnSc2	krajina
s	s	k7c7	s
průhledem	průhled	k1gInSc7	průhled
na	na	k7c4	na
jenišovický	jenišovický	k2eAgInSc4d1	jenišovický
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Levá	levý	k2eAgFnSc1d1	levá
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
osa	osa	k1gFnSc1	osa
prochází	procházet	k5eAaImIp3nS	procházet
romantickou	romantický	k2eAgFnSc7d1	romantická
zříceninou	zřícenina	k1gFnSc7	zřícenina
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Arthurův	Arthurův	k2eAgInSc1d1	Arthurův
hrad	hrad	k1gInSc4	hrad
postavený	postavený	k2eAgInSc4d1	postavený
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
louky	louka	k1gFnPc4	louka
mohelského	mohelský	k2eAgNnSc2d1	mohelský
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Oranžérie	oranžérie	k1gFnSc1	oranžérie
je	být	k5eAaImIp3nS	být
nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
dominantou	dominanta	k1gFnSc7	dominanta
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kompoziční	kompoziční	k2eAgFnSc4d1	kompoziční
protiváhu	protiváha	k1gFnSc4	protiváha
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
první	první	k4xOgFnSc2	první
oranžérie	oranžérie	k1gFnSc2	oranžérie
zahájil	zahájit	k5eAaPmAgMnS	zahájit
kníže	kníže	k1gMnSc1	kníže
Karel	Karel	k1gMnSc1	Karel
Alain	Alain	k1gMnSc1	Alain
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
podle	podle	k7c2	podle
Pruvotova	Pruvotův	k2eAgInSc2d1	Pruvotův
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
stylem	styl	k1gInSc7	styl
o	o	k7c4	o
poněkud	poněkud	k6eAd1	poněkud
vybočující	vybočující	k2eAgFnSc4d1	vybočující
budovu	budova	k1gFnSc4	budova
postavenou	postavený	k2eAgFnSc4d1	postavená
v	v	k7c6	v
novorenesančním	novorenesanční	k2eAgInSc6d1	novorenesanční
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
průčelím	průčelí	k1gNnSc7	průčelí
do	do	k7c2	do
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
arkádová	arkádový	k2eAgFnSc1d1	arkádová
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc1	vstup
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
kruhových	kruhový	k2eAgNnPc6d1	kruhové
schodištích	schodiště	k1gNnPc6	schodiště
vedoucí	vedoucí	k1gFnSc2	vedoucí
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
portiků	portikus	k1gInPc2	portikus
<g/>
.	.	kIx.	.
</s>
<s>
Oranžérie	oranžérie	k1gFnSc1	oranžérie
sloužila	sloužit	k5eAaImAgFnS	sloužit
také	také	k9	také
jako	jako	k9	jako
květinová	květinový	k2eAgFnSc1d1	květinová
galérie	galérie	k1gFnSc1	galérie
<g/>
,	,	kIx,	,
letní	letní	k2eAgInSc4d1	letní
zámeček	zámeček	k1gInSc4	zámeček
a	a	k8xC	a
konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
různé	různý	k2eAgFnPc1d1	různá
společenské	společenský	k2eAgFnPc1d1	společenská
akce	akce	k1gFnPc1	akce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Kamilově	Kamilův	k2eAgFnSc6d1	Kamilova
smrti	smrt	k1gFnSc6	smrt
oranžérie	oranžérie	k1gFnSc1	oranžérie
chátrala	chátrat	k5eAaImAgFnS	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1	rekonstruovaná
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
stavby	stavba	k1gFnPc4	stavba
v	v	k7c6	v
parku	park	k1gInSc6	park
patří	patřit	k5eAaImIp3nS	patřit
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Rudolfa	Rudolf	k1gMnSc2	Rudolf
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
mohelského	mohelský	k2eAgNnSc2d1	mohelský
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
průhled	průhled	k1gInSc1	průhled
až	až	k9	až
k	k	k7c3	k
Ještědu	Ještěd	k1gInSc3	Ještěd
a	a	k8xC	a
Jizerským	jizerský	k2eAgFnPc3d1	Jizerská
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
oranžérie	oranžérie	k1gFnSc2	oranžérie
stojí	stát	k5eAaImIp3nS	stát
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
dva	dva	k4xCgInPc1	dva
objektu	objekt	k1gInSc2	objekt
navozující	navozující	k2eAgFnSc4d1	navozující
rustikální	rustikální	k2eAgFnSc4d1	rustikální
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
chaloupka	chaloupka	k1gFnSc1	chaloupka
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bauernhaus	Bauernhaus	k1gInSc1	Bauernhaus
a	a	k8xC	a
holubník	holubník	k1gInSc1	holubník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
bazénů	bazén	k1gInPc2	bazén
a	a	k8xC	a
čínské	čínský	k2eAgNnSc1d1	čínské
jezírko	jezírko	k1gNnSc1	jezírko
s	s	k7c7	s
mostkem	mostek	k1gInSc7	mostek
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
areál	areál	k1gInSc4	areál
parku	park	k1gInSc2	park
vymezený	vymezený	k2eAgInSc4d1	vymezený
litinovým	litinový	k2eAgInSc7d1	litinový
plotem	plot	k1gInSc7	plot
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kompozičně	kompozičně	k6eAd1	kompozičně
do	do	k7c2	do
parku	park	k1gInSc2	park
včleněný	včleněný	k2eAgInSc1d1	včleněný
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
Arturův	Arturův	k2eAgInSc4d1	Arturův
hrad	hrad	k1gInSc4	hrad
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k6eAd1	také
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
Artušův	Artušův	k2eAgInSc4d1	Artušův
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
či	či	k8xC	či
Alte	alt	k1gInSc5	alt
Burg	Burg	k1gMnSc1	Burg
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
romantické	romantický	k2eAgFnSc2d1	romantická
zříceniny	zřícenina	k1gFnSc2	zřícenina
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
brány	brána	k1gFnPc4	brána
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Radostínu	Radostín	k1gInSc3	Radostín
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
chodba	chodba	k1gFnSc1	chodba
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
hradu	hrad	k1gInSc3	hrad
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
atrakcím	atrakce	k1gFnPc3	atrakce
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
hlubokému	hluboký	k2eAgInSc3d1	hluboký
zájmu	zájem	k1gInSc3	zájem
knížete	kníže	k1gMnSc2	kníže
Kamila	Kamil	k1gMnSc2	Kamil
o	o	k7c4	o
botaniku	botanika	k1gFnSc4	botanika
a	a	k8xC	a
dendrologii	dendrologie	k1gFnSc4	dendrologie
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
vybavit	vybavit	k5eAaPmF	vybavit
řadou	řada	k1gFnSc7	řada
vzácných	vzácný	k2eAgInPc2d1	vzácný
a	a	k8xC	a
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
dřevin	dřevina	k1gFnPc2	dřevina
i	i	k8xC	i
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nalézá	nalézat	k5eAaImIp3nS	nalézat
unikátní	unikátní	k2eAgInSc1d1	unikátní
červenolistý	červenolistý	k2eAgInSc1d1	červenolistý
buk	buk	k1gInSc1	buk
Rohanův	Rohanův	k2eAgInSc1d1	Rohanův
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc1	Fagus
silvatica	silvatica	k6eAd1	silvatica
Rohani	rohan	k1gMnPc1	rohan
<g/>
)	)	kIx)	)
vyšlechtěný	vyšlechtěný	k2eAgMnSc1d1	vyšlechtěný
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Sychrově	Sychrův	k2eAgFnSc6d1	Sychrova
<g/>
,	,	kIx,	,
z	z	k7c2	z
dřevin	dřevina	k1gFnPc2	dřevina
zde	zde	k6eAd1	zde
dále	daleko	k6eAd2	daleko
roste	růst	k5eAaImIp3nS	růst
například	například	k6eAd1	například
platan	platan	k1gInSc1	platan
východní	východní	k2eAgInSc1d1	východní
<g/>
,	,	kIx,	,
jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgInSc1d1	dvoulaločný
<g/>
,	,	kIx,	,
záznamy	záznam	k1gInPc1	záznam
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
největší	veliký	k2eAgFnSc2d3	veliký
slávy	sláva	k1gFnSc2	sláva
dokonce	dokonce	k9	dokonce
uvádí	uvádět	k5eAaImIp3nS	uvádět
až	až	k9	až
1872	[number]	k4	1872
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
Rohanem	Rohan	k1gMnSc7	Rohan
pěstěné	pěstěný	k2eAgInPc4d1	pěstěný
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
v	v	k7c6	v
sychrovských	sychrovský	k2eAgFnPc6d1	sychrovská
podmínkách	podmínka	k1gFnPc6	podmínka
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Pěstovalo	pěstovat	k5eAaImAgNnS	pěstovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
ve	v	k7c6	v
sklenících	skleník	k1gInPc6	skleník
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
oranžérii	oranžérie	k1gFnSc4	oranžérie
<g/>
,	,	kIx,	,
záznamy	záznam	k1gInPc4	záznam
uvádí	uvádět	k5eAaImIp3nS	uvádět
1119	[number]	k4	1119
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sychrovským	sychrovský	k2eAgInSc7d1	sychrovský
zámkem	zámek	k1gInSc7	zámek
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
i	i	k9	i
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
obec	obec	k1gFnSc4	obec
sedmkrát	sedmkrát	k6eAd1	sedmkrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Dojížděl	dojíždět	k5eAaImAgMnS	dojíždět
sem	sem	k6eAd1	sem
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
sychrovským	sychrovský	k2eAgMnSc7d1	sychrovský
správcem	správce	k1gMnSc7	správce
Aloisem	Alois	k1gMnSc7	Alois
Göbelem	Göbel	k1gMnSc7	Göbel
<g/>
.	.	kIx.	.
</s>
<s>
Krása	krása	k1gFnSc1	krása
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
procházel	procházet	k5eAaImAgInS	procházet
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
k	k	k7c3	k
houslovému	houslový	k2eAgInSc3d1	houslový
koncertu	koncert	k1gInSc3	koncert
A	a	k8xC	a
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
opus	opus	k1gInSc4	opus
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
září	září	k1gNnSc6	září
1834	[number]	k4	1834
navštívil	navštívit	k5eAaPmAgMnS	navštívit
dokonce	dokonce	k9	dokonce
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
X.	X.	kA	X.
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
byl	být	k5eAaImAgMnS	být
mimochodem	mimochodem	k6eAd1	mimochodem
i	i	k9	i
Joachim	Joachim	k1gMnSc1	Joachim
Barrande	Barrand	k1gInSc5	Barrand
a	a	k8xC	a
vévodkyně	vévodkyně	k1gFnPc4	vévodkyně
z	z	k7c2	z
Angoulemu	Angoulem	k1gInSc2	Angoulem
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
popravená	popravený	k2eAgFnSc1d1	popravená
francouzská	francouzský	k2eAgFnSc1d1	francouzská
královna	královna	k1gFnSc1	královna
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Sychrov	Sychrov	k1gInSc1	Sychrov
těšil	těšit	k5eAaImAgInS	těšit
několikrát	několikrát	k6eAd1	několikrát
z	z	k7c2	z
návštěvy	návštěva	k1gFnSc2	návštěva
členů	člen	k1gMnPc2	člen
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1866	[number]	k4	1866
navštívil	navštívit	k5eAaPmAgMnS	navštívit
zámek	zámek	k1gInSc4	zámek
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
nazvána	nazván	k2eAgFnSc1d1	nazvána
cesta	cesta	k1gFnSc1	cesta
skrz	skrz	k7c4	skrz
oboru	obora	k1gFnSc4	obora
do	do	k7c2	do
Radostína	Radostín	k1gInSc2	Radostín
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
malá	malý	k2eAgFnSc1d1	malá
žulová	žulový	k2eAgFnSc1d1	Žulová
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1871	[number]	k4	1871
poté	poté	k6eAd1	poté
navštívil	navštívit	k5eAaPmAgMnS	navštívit
zámek	zámek	k1gInSc4	zámek
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
této	tento	k3xDgFnSc2	tento
návštěvy	návštěva	k1gFnSc2	návštěva
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Rudolfa	Rudolf	k1gMnSc2	Rudolf
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
také	také	k9	také
navštívila	navštívit	k5eAaPmAgFnS	navštívit
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
a	a	k8xC	a
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Carl	Carl	k1gMnSc1	Carl
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
,	,	kIx,	,
data	datum	k1gNnSc2	datum
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
návštěv	návštěva	k1gFnPc2	návštěva
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
pyramidu	pyramida	k1gFnSc4	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Desky	deska	k1gFnPc1	deska
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
údaji	údaj	k1gInPc7	údaj
posléze	posléze	k6eAd1	posléze
zmizely	zmizet	k5eAaPmAgInP	zmizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vytvoření	vytvoření	k1gNnSc4	vytvoření
naučné	naučný	k2eAgFnSc2d1	naučná
stezky	stezka	k1gFnSc2	stezka
Lesní	lesní	k2eAgNnSc1d1	lesní
putování	putování	k1gNnSc1	putování
s	s	k7c7	s
Kamilem	Kamil	k1gMnSc7	Kamil
Rohanem	Rohan	k1gMnSc7	Rohan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
prostory	prostor	k1gInPc7	prostor
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
zejména	zejména	k9	zejména
parku	park	k1gInSc2	park
využívají	využívat	k5eAaPmIp3nP	využívat
vedle	vedle	k7c2	vedle
klasické	klasický	k2eAgFnSc2d1	klasická
expozice	expozice	k1gFnSc2	expozice
ke	k	k7c3	k
konání	konání	k1gNnSc3	konání
dalších	další	k2eAgFnPc2d1	další
periodických	periodický	k2eAgFnPc2d1	periodická
i	i	k8xC	i
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Rohanů	Rohan	k1gMnPc2	Rohan
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
Sychrov	Sychrov	k1gInSc4	Sychrov
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
určité	určitý	k2eAgNnSc4d1	určité
kulturní	kulturní	k2eAgNnSc4d1	kulturní
obohacení	obohacení	k1gNnSc4	obohacení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
slaven	slavit	k5eAaImNgInS	slavit
zejména	zejména	k9	zejména
svátek	svátek	k1gInSc1	svátek
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
prostému	prostý	k2eAgNnSc3d1	prosté
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
povoleno	povolit	k5eAaPmNgNnS	povolit
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
konfiskaci	konfiskace	k1gFnSc6	konfiskace
se	se	k3xPyFc4	se
svátek	svátek	k1gInSc1	svátek
přestal	přestat	k5eAaPmAgInS	přestat
slavit	slavit	k5eAaImF	slavit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pevnější	pevný	k2eAgMnSc1d2	pevnější
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
slavení	slavení	k1gNnSc3	slavení
pouti	pouť	k1gFnSc2	pouť
k	k	k7c3	k
sychrovské	sychrovský	k2eAgFnSc3d1	sychrovská
kapli	kaple	k1gFnSc3	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
je	být	k5eAaImIp3nS	být
tradice	tradice	k1gFnSc1	tradice
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
Dvořákův	Dvořákův	k2eAgInSc1d1	Dvořákův
Sychrov	Sychrov	k1gInSc1	Sychrov
a	a	k8xC	a
Turnov	Turnov	k1gInSc1	Turnov
pořádaného	pořádaný	k2eAgInSc2d1	pořádaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zhuštění	zhuštění	k1gNnSc3	zhuštění
doprovodných	doprovodný	k2eAgFnPc2d1	doprovodná
akcí	akce	k1gFnPc2	akce
ale	ale	k9	ale
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
Sychrově	Sychrův	k2eAgMnSc6d1	Sychrův
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
konalo	konat	k5eAaImAgNnS	konat
sympozium	sympozium	k1gNnSc4	sympozium
Romantický	romantický	k2eAgInSc1d1	romantický
historismus	historismus	k1gInSc1	historismus
<g/>
:	:	kIx,	:
Novogotika	novogotika	k1gFnSc1	novogotika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
českou	český	k2eAgFnSc4d1	Česká
premiéru	premiéra	k1gFnSc4	premiéra
opera	opera	k1gFnSc1	opera
Maria	Maria	k1gFnSc1	Maria
di	di	k?	di
Rohan	Rohan	k1gMnSc1	Rohan
italského	italský	k2eAgMnSc2d1	italský
romantického	romantický	k2eAgMnSc2d1	romantický
skladatele	skladatel	k1gMnSc2	skladatel
Gaetana	Gaetan	k1gMnSc2	Gaetan
Donizetti	Donizetť	k1gFnSc2	Donizetť
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
konají	konat	k5eAaImIp3nP	konat
skotské	skotský	k2eAgFnPc1d1	skotská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
sítě	síť	k1gFnSc2	síť
podobných	podobný	k2eAgFnPc2d1	podobná
akcí	akce	k1gFnPc2	akce
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
přibližující	přibližující	k2eAgFnSc4d1	přibližující
skotskou	skotský	k2eAgFnSc4d1	skotská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
si	se	k3xPyFc3	se
buduje	budovat	k5eAaImIp3nS	budovat
pozici	pozice	k1gFnSc4	pozice
nová	nový	k2eAgFnSc1d1	nová
hudební	hudební	k2eAgFnSc1d1	hudební
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
letní	letní	k2eAgInPc4d1	letní
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
vystoupit	vystoupit	k5eAaPmF	vystoupit
světový	světový	k2eAgMnSc1d1	světový
interpret	interpret	k1gMnSc1	interpret
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Night	k1gInSc1	Night
<g/>
,	,	kIx,	,
Sinéad	Sinéad	k1gInSc1	Sinéad
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
a	a	k8xC	a
Apocalyptica	Apocalyptica	k1gMnSc1	Apocalyptica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
