<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
ze	z	k7c2	z
Stageiry	Stageira	k1gFnSc2	Stageira
byl	být	k5eAaImAgMnS	být
filosof	filosof	k1gMnSc1	filosof
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
řecké	řecký	k2eAgFnSc2d1	řecká
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
žák	žák	k1gMnSc1	žák
Platonův	Platonův	k2eAgMnSc1d1	Platonův
a	a	k8xC	a
vychovatel	vychovatel	k1gMnSc1	vychovatel
Alexandra	Alexandr	k1gMnSc2	Alexandr
Makedonského	makedonský	k2eAgMnSc2d1	makedonský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
dílo	dílo	k1gNnSc1	dílo
položilo	položit	k5eAaPmAgNnS	položit
základy	základ	k1gInPc4	základ
mnoha	mnoho	k4c2	mnoho
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
384	[number]	k4	384
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Stageira	Stageir	k1gInSc2	Stageir
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Chalkidiki	Chalkidik	k1gFnSc2	Chalkidik
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
řecké	řecký	k2eAgFnSc6d1	řecká
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Soluně	Soluň	k1gFnSc2	Soluň
-	-	kIx~	-
Thessaloniki	Thessalonik	k1gFnSc2	Thessalonik
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
Stagirita	Stagirita	k1gFnSc1	Stagirita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
322	[number]	k4	322
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Chalkidě	Chalkida	k1gFnSc6	Chalkida
<g/>
,	,	kIx,	,
asi	asi	k9	asi
60	[number]	k4	60
km	km	kA	km
SV	sv	kA	sv
od	od	k7c2	od
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelův	Aristotelův	k2eAgMnSc1d1	Aristotelův
otec	otec	k1gMnSc1	otec
Nikomachos	Nikomachos	k1gMnSc1	Nikomachos
byl	být	k5eAaImAgMnS	být
osobním	osobní	k2eAgMnSc7d1	osobní
lékařem	lékař	k1gMnSc7	lékař
makedonského	makedonský	k2eAgNnSc2d1	makedonské
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
367	[number]	k4	367
až	až	k9	až
347	[number]	k4	347
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
v	v	k7c6	v
Platónově	Platónův	k2eAgFnSc6d1	Platónova
Akadémii	Akadémie	k1gFnSc6	Akadémie
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Platónově	Platónův	k2eAgFnSc6d1	Platónova
smrti	smrt	k1gFnSc6	smrt
odešel	odejít	k5eAaPmAgMnS	odejít
vyučovat	vyučovat	k5eAaImF	vyučovat
do	do	k7c2	do
Assu	Assus	k1gInSc2	Assus
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
na	na	k7c4	na
Lesbos	Lesbos	k1gInSc4	Lesbos
do	do	k7c2	do
Mytilény	Mytiléna	k1gFnSc2	Mytiléna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
343	[number]	k4	343
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jej	on	k3xPp3gNnSc4	on
povolal	povolat	k5eAaPmAgMnS	povolat
makedonský	makedonský	k2eAgMnSc1d1	makedonský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
jako	jako	k8xC	jako
vychovatele	vychovatel	k1gMnSc4	vychovatel
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Alexandra	Alexandra	k1gFnSc1	Alexandra
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgMnSc1d1	budoucí
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Filipově	Filipův	k2eAgFnSc6d1	Filipova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
vědě	věda	k1gFnSc3	věda
a	a	k8xC	a
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
335	[number]	k4	335
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
školu	škola	k1gFnSc4	škola
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Lykeion	Lykeion	k1gInSc4	Lykeion
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
peripatetická	peripatetický	k2eAgFnSc1d1	peripatetická
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
z	z	k7c2	z
názvu	název	k1gInSc2	název
krytého	krytý	k2eAgNnSc2d1	kryté
sloupořadí	sloupořadí	k1gNnSc2	sloupořadí
peripatos	peripatosa	k1gFnPc2	peripatosa
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
učili	učit	k5eAaImAgMnP	učit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
peripatein	peripatein	k1gInSc1	peripatein
(	(	kIx(	(
<g/>
procházet	procházet	k5eAaImF	procházet
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
se	se	k3xPyFc4	se
prý	prý	k9	prý
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
procházel	procházet	k5eAaImAgMnS	procházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
spíše	spíše	k9	spíše
poznámek	poznámka	k1gFnPc2	poznámka
nebo	nebo	k8xC	nebo
záznamů	záznam	k1gInPc2	záznam
z	z	k7c2	z
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
322	[number]	k4	322
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reakce	reakce	k1gFnSc2	reakce
proti	proti	k7c3	proti
Alexandrovi	Alexandr	k1gMnSc3	Alexandr
obviněn	obviněn	k2eAgMnSc1d1	obviněn
z	z	k7c2	z
rouhání	rouhání	k1gNnSc2	rouhání
bohům	bůh	k1gMnPc3	bůh
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
do	do	k7c2	do
Chalkidy	Chalkida	k1gFnSc2	Chalkida
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pocházela	pocházet	k5eAaImAgFnS	pocházet
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
kde	kde	k6eAd1	kde
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Zachovala	Zachoval	k1gMnSc2	Zachoval
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
závěť	závěť	k1gFnSc1	závěť
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
Platóna	Platón	k1gMnSc4	Platón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
převážně	převážně	k6eAd1	převážně
otázkám	otázka	k1gFnPc3	otázka
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
a	a	k8xC	a
uspořádat	uspořádat	k5eAaPmF	uspořádat
i	i	k9	i
všechno	všechen	k3xTgNnSc4	všechen
předmětné	předmětný	k2eAgNnSc4d1	předmětné
vědění	vědění	k1gNnSc4	vědění
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
vlastních	vlastní	k2eAgNnPc2d1	vlastní
pozorování	pozorování	k1gNnPc2	pozorování
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
sice	sice	k8xC	sice
psal	psát	k5eAaImAgInS	psát
také	také	k9	také
dialogy	dialog	k1gInPc4	dialog
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
zachovaly	zachovat	k5eAaPmAgInP	zachovat
jen	jen	k9	jen
zlomky	zlomek	k1gInPc1	zlomek
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
Aristotelovo	Aristotelův	k2eAgNnSc4d1	Aristotelovo
dílo	dílo	k1gNnSc4	dílo
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
soustavných	soustavný	k2eAgNnPc2d1	soustavné
pojednání	pojednání	k1gNnPc2	pojednání
<g/>
,	,	kIx,	,
traktátů	traktát	k1gInPc2	traktát
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Platónův	Platónův	k2eAgMnSc1d1	Platónův
učitel	učitel	k1gMnSc1	učitel
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
partnery	partner	k1gMnPc7	partner
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
a	a	k8xC	a
hleděl	hledět	k5eAaImAgMnS	hledět
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jejich	jejich	k3xOp3gInSc2	jejich
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
,	,	kIx,	,
Aristotelovy	Aristotelův	k2eAgInPc4d1	Aristotelův
spisy	spis	k1gInPc4	spis
už	už	k6eAd1	už
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přesnou	přesný	k2eAgFnSc4d1	přesná
a	a	k8xC	a
závaznou	závazný	k2eAgFnSc4d1	závazná
argumentaci	argumentace	k1gFnSc4	argumentace
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
svým	svůj	k3xOyFgMnPc3	svůj
žákům	žák	k1gMnPc3	žák
přednášet	přednášet	k5eAaImF	přednášet
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
v	v	k7c6	v
Sedmém	sedmý	k4xOgInSc6	sedmý
listě	list	k1gInSc6	list
výslovně	výslovně	k6eAd1	výslovně
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádnou	žádný	k3yNgFnSc4	žádný
svoji	svůj	k3xOyFgFnSc4	svůj
nauku	nauka	k1gFnSc4	nauka
nikdy	nikdy	k6eAd1	nikdy
nenapsal	napsat	k5eNaPmAgMnS	napsat
a	a	k8xC	a
nenapíše	napsat	k5eNaPmIp3nS	napsat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
přemýšlení	přemýšlení	k1gNnSc2	přemýšlení
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
úsilí	úsilí	k1gNnSc4	úsilí
a	a	k8xC	a
konfrontaci	konfrontace	k1gFnSc4	konfrontace
různých	různý	k2eAgInPc2d1	různý
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
teprve	teprve	k6eAd1	teprve
jako	jako	k9	jako
třením	tření	k1gNnSc7	tření
dřev	dřevo	k1gNnPc2	dřevo
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
"	"	kIx"	"
<g/>
vyšlehne	vyšlehnout	k5eAaPmIp3nS	vyšlehnout
oheň	oheň	k1gInSc1	oheň
poznání	poznání	k1gNnSc2	poznání
a	a	k8xC	a
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Aristotelově	Aristotelův	k2eAgNnSc6d1	Aristotelovo
myšlení	myšlení	k1gNnSc6	myšlení
hraje	hrát	k5eAaImIp3nS	hrát
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc3d2	veliký
roli	role	k1gFnSc3	role
věcná	věcný	k2eAgFnSc1d1	věcná
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
všem	všecek	k3xTgMnPc3	všecek
společná	společný	k2eAgNnPc4d1	společné
a	a	k8xC	a
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
lze	lze	k6eAd1	lze
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
filosofa	filosof	k1gMnSc2	filosof
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
přesně	přesně	k6eAd1	přesně
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
uspořádat	uspořádat	k5eAaPmF	uspořádat
a	a	k8xC	a
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
správně	správně	k6eAd1	správně
myslet	myslet	k5eAaImF	myslet
i	i	k8xC	i
argumentovat	argumentovat	k5eAaImF	argumentovat
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
novým	nový	k2eAgNnSc7d1	nové
vymezením	vymezení	k1gNnSc7	vymezení
filosofie	filosofie	k1gFnSc2	filosofie
položil	položit	k5eAaPmAgInS	položit
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
základy	základ	k1gInPc4	základ
soustavné	soustavný	k2eAgFnSc2d1	soustavná
filosofie	filosofie	k1gFnSc2	filosofie
jako	jako	k8xC	jako
snahy	snaha	k1gFnSc2	snaha
"	"	kIx"	"
<g/>
porozumět	porozumět	k5eAaPmF	porozumět
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jest	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různých	různý	k2eAgFnPc2d1	různá
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
embryologie	embryologie	k1gFnSc2	embryologie
<g/>
,	,	kIx,	,
etnografie	etnografie	k1gFnSc2	etnografie
<g/>
,	,	kIx,	,
geografie	geografie	k1gFnSc2	geografie
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
meteorologie	meteorologie	k1gFnSc2	meteorologie
nebo	nebo	k8xC	nebo
politologie	politologie	k1gFnSc2	politologie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
filosofické	filosofický	k2eAgFnSc6d1	filosofická
oblasti	oblast	k1gFnSc6	oblast
psal	psát	k5eAaImAgMnS	psát
o	o	k7c6	o
estetice	estetika	k1gFnSc6	estetika
<g/>
,	,	kIx,	,
etice	etika	k1gFnSc6	etika
<g/>
,	,	kIx,	,
metafyzice	metafyzika	k1gFnSc6	metafyzika
<g/>
,	,	kIx,	,
rétorice	rétorika	k1gFnSc6	rétorika
a	a	k8xC	a
teologii	teologie	k1gFnSc6	teologie
<g/>
,	,	kIx,	,
zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
výchovou	výchova	k1gFnSc7	výchova
a	a	k8xC	a
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
<g/>
,	,	kIx,	,
i	i	k8xC	i
literaturou	literatura	k1gFnSc7	literatura
a	a	k8xC	a
poezií	poezie	k1gFnSc7	poezie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc1	jeho
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
názory	názor	k1gInPc1	názor
většinou	většinou	k6eAd1	většinou
překonané	překonaný	k2eAgInPc1d1	překonaný
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
způsobu	způsob	k1gInSc2	způsob
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
,	,	kIx,	,
třídění	třídění	k1gNnSc2	třídění
a	a	k8xC	a
argumentace	argumentace	k1gFnSc2	argumentace
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
živé	živý	k2eAgNnSc1d1	živé
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
aristotelského	aristotelský	k2eAgNnSc2d1	aristotelské
třídění	třídění	k1gNnSc2	třídění
věcí	věc	k1gFnPc2	věc
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
genos	genos	k1gInSc1	genos
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
genus	genus	k1gInSc1	genus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
eidos	eidos	k1gInSc1	eidos
<g/>
,	,	kIx,	,
species	species	k1gFnSc1	species
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nP	lišit
"	"	kIx"	"
<g/>
specifickou	specifický	k2eAgFnSc7d1	specifická
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
druhovou	druhový	k2eAgFnSc7d1	druhová
diferencí	diference	k1gFnSc7	diference
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
tak	tak	k9	tak
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
určení	určení	k1gNnSc6	určení
(	(	kIx(	(
<g/>
společného	společný	k2eAgInSc2d1	společný
<g/>
)	)	kIx)	)
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
tohoto	tento	k3xDgInSc2	tento
rozdílu	rozdíl	k1gInSc2	rozdíl
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgInPc3d1	ostatní
druhům	druh	k1gInPc3	druh
v	v	k7c4	v
něm.	něm.	k?	něm.
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
předměty	předmět	k1gInPc4	předmět
se	se	k3xPyFc4	se
tak	tak	k9	tak
dají	dát	k5eAaPmIp3nP	dát
uspořádat	uspořádat	k5eAaPmF	uspořádat
do	do	k7c2	do
"	"	kIx"	"
<g/>
stromu	strom	k1gInSc2	strom
<g/>
"	"	kIx"	"
nadřazených	nadřazený	k2eAgFnPc2d1	nadřazená
a	a	k8xC	a
podřazených	podřazený	k2eAgFnPc2d1	podřazená
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
spočívá	spočívat	k5eAaImIp3nS	spočívat
tzv.	tzv.	kA	tzv.
binomická	binomický	k2eAgFnSc1d1	binomická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
v	v	k7c6	v
botanice	botanika	k1gFnSc6	botanika
<g/>
,	,	kIx,	,
zoologii	zoologie	k1gFnSc6	zoologie
atd.	atd.	kA	atd.
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc3	první
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
metafyzice	metafyzika	k1gFnSc3	metafyzika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
jsoucnech	jsoucno	k1gNnPc6	jsoucno
a	a	k8xC	a
bytí	bytí	k1gNnSc6	bytí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
snaží	snažit	k5eAaImIp3nS	snažit
překonat	překonat	k5eAaPmF	překonat
Parmenidovo	Parmenidův	k2eAgNnSc4d1	Parmenidovo
ostré	ostrý	k2eAgNnSc4d1	ostré
rozlišení	rozlišení	k1gNnSc4	rozlišení
"	"	kIx"	"
<g/>
jsoucího	jsoucí	k2eAgInSc2d1	jsoucí
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nejsoucího	nejsoucí	k2eAgNnSc2d1	nejsoucí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vylučovalo	vylučovat	k5eAaImAgNnS	vylučovat
každý	každý	k3xTgInSc4	každý
vznik	vznik	k1gInSc4	vznik
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
a	a	k8xC	a
zánik	zánik	k1gInSc4	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
rozlišení	rozlišení	k1gNnSc3	rozlišení
mezi	mezi	k7c7	mezi
možností	možnost	k1gFnSc7	možnost
(	(	kIx(	(
<g/>
dynamis	dynamis	k1gFnSc1	dynamis
<g/>
,	,	kIx,	,
potentia	potentia	k1gFnSc1	potentia
<g/>
)	)	kIx)	)
a	a	k8xC	a
uskutečněním	uskutečnění	k1gNnSc7	uskutečnění
(	(	kIx(	(
<g/>
energeia	energeium	k1gNnPc1	energeium
<g/>
,	,	kIx,	,
actus	actus	k1gInSc1	actus
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vznikající	vznikající	k2eAgFnSc1d1	vznikající
věc	věc	k1gFnSc1	věc
už	už	k6eAd1	už
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
možnosti	možnost	k1gFnSc2	možnost
k	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
změna	změna	k1gFnSc1	změna
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
svoji	svůj	k3xOyFgFnSc4	svůj
příčinu	příčina	k1gFnSc4	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
následující	následující	k2eAgFnPc4d1	následující
příčiny	příčina	k1gFnPc4	příčina
látková	látkový	k2eAgFnSc1d1	látková
(	(	kIx(	(
<g/>
hyletiké	hyletiký	k2eAgInPc1d1	hyletiký
<g/>
,	,	kIx,	,
materialis	materialis	k1gInSc1	materialis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
materiál	materiál	k1gInSc1	materiál
či	či	k8xC	či
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
tvarová	tvarový	k2eAgFnSc1d1	tvarová
(	(	kIx(	(
<g/>
eidetiké	eidetiký	k2eAgInPc1d1	eidetiký
<g/>
,	,	kIx,	,
formalis	formalis	k1gInSc1	formalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
představa	představa	k1gFnSc1	představa
či	či	k8xC	či
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
účinná	účinný	k2eAgFnSc1d1	účinná
(	(	kIx(	(
<g/>
hothen	hothno	k1gNnPc2	hothno
hé	hé	k0	hé
arché	arché	k1gFnSc1	arché
<g/>
,	,	kIx,	,
efficiens	efficiens	k1gInSc1	efficiens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ji	on	k3xPp3gFnSc4	on
působí	působit	k5eAaImIp3nP	působit
<g/>
,	,	kIx,	,
a	a	k8xC	a
účelová	účelový	k2eAgFnSc1d1	účelová
(	(	kIx(	(
<g/>
hú	hú	k0	hú
heneka	heneka	k6eAd1	heneka
<g/>
,	,	kIx,	,
finalis	finalis	k1gFnSc1	finalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
když	když	k8xS	když
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
přičinit	přičinit	k5eAaPmF	přičinit
<g/>
"	"	kIx"	"
jak	jak	k8xS	jak
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
představa	představa	k1gFnSc1	představa
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
účel	účel	k1gInSc4	účel
<g/>
:	:	kIx,	:
někdo	někdo	k3yInSc1	někdo
chce	chtít	k5eAaImIp3nS	chtít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
chyběla	chybět	k5eAaImAgFnS	chybět
<g/>
,	,	kIx,	,
nemohlo	moct	k5eNaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
příkladě	příklad	k1gInSc6	příklad
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aristotelova	Aristotelův	k2eAgFnSc1d1	Aristotelova
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
jsoucím	jsoucí	k2eAgMnSc6d1	jsoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
novověké	novověký	k2eAgFnSc2d1	novověká
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
vzorové	vzorový	k2eAgNnSc1d1	vzorové
jsoucno	jsoucno	k1gNnSc1	jsoucno
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
živá	živá	k1gFnSc1	živá
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
"	"	kIx"	"
<g/>
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
věc	věc	k1gFnSc1	věc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Jsoucno	jsoucno	k1gNnSc1	jsoucno
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
za	za	k7c7	za
nějakým	nějaký	k3yIgInSc7	nějaký
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
přirozenosti	přirozenost	k1gFnSc3	přirozenost
(	(	kIx(	(
<g/>
entelechie	entelechie	k1gFnSc1	entelechie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelova	Aristotelův	k2eAgFnSc1d1	Aristotelova
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
teleologická	teleologický	k2eAgFnSc1d1	teleologická
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
je	být	k5eAaImIp3nS	být
Aristotelovi	Aristoteles	k1gMnSc3	Aristoteles
cizí	cizí	k2eAgFnSc1d1	cizí
představa	představa	k1gFnSc1	představa
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ani	ani	k8xC	ani
řetězec	řetězec	k1gInSc1	řetězec
příčin	příčina	k1gFnPc2	příčina
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
musí	muset	k5eAaImIp3nS	muset
skončit	skončit	k5eAaPmF	skončit
u	u	k7c2	u
první	první	k4xOgFnSc2	první
nutné	nutný	k2eAgFnSc2d1	nutná
příčiny	příčina	k1gFnSc2	příčina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
prvního	první	k4xOgMnSc2	první
nehybného	hybný	k2eNgMnSc2d1	nehybný
hybatele	hybatel	k1gMnSc2	hybatel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
pak	pak	k6eAd1	pak
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
teologie	teologie	k1gFnSc1	teologie
ztotožnila	ztotožnit	k5eAaPmAgFnS	ztotožnit
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlům	pravidlo	k1gNnPc3	pravidlo
správného	správný	k2eAgNnSc2d1	správné
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
usuzování	usuzování	k1gNnSc2	usuzování
a	a	k8xC	a
argumentace	argumentace	k1gFnSc2	argumentace
je	být	k5eAaImIp3nS	být
věnována	věnován	k2eAgFnSc1d1	věnována
řada	řada	k1gFnSc1	řada
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xS	jako
Organon	organon	k1gNnSc4	organon
čili	čili	k8xC	čili
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
právě	právě	k9	právě
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
tvrzení	tvrzení	k1gNnPc4	tvrzení
spojujeme	spojovat	k5eAaImIp1nP	spojovat
v	v	k7c4	v
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
z	z	k7c2	z
předpokladů	předpoklad	k1gInPc2	předpoklad
(	(	kIx(	(
<g/>
premis	premisa	k1gFnPc2	premisa
<g/>
)	)	kIx)	)
vyvozujeme	vyvozovat	k5eAaImIp1nP	vyvozovat
závěry	závěr	k1gInPc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
formou	forma	k1gFnSc7	forma
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
sylogismus	sylogismus	k1gInSc1	sylogismus
<g/>
:	:	kIx,	:
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
tvrzení	tvrzení	k1gNnPc2	tvrzení
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
členem	člen	k1gInSc7	člen
plyne	plynout	k5eAaImIp3nS	plynout
třetí	třetí	k4xOgNnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
výchozí	výchozí	k2eAgNnPc1d1	výchozí
tvrzení	tvrzení	k1gNnPc1	tvrzení
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
záporná	záporný	k2eAgFnSc1d1	záporná
(	(	kIx(	(
<g/>
žádný	žádný	k1gMnSc1	žádný
a	a	k8xC	a
jen	jen	k9	jen
některý	některý	k3yIgInSc1	některý
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
pak	pak	k6eAd1	pak
některé	některý	k3yIgInPc1	některý
závěry	závěr	k1gInPc1	závěr
platí	platit	k5eAaImIp3nP	platit
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Formalizováním	formalizování	k1gNnSc7	formalizování
aristotelské	aristotelský	k2eAgFnSc2d1	aristotelská
logiky	logika	k1gFnSc2	logika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
včetně	včetně	k7c2	včetně
kvantifikátorů	kvantifikátor	k1gInPc2	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelova	Aristotelův	k2eAgFnSc1d1	Aristotelova
"	"	kIx"	"
<g/>
analytika	analytika	k1gFnSc1	analytika
<g/>
"	"	kIx"	"
či	či	k8xC	či
logika	logika	k1gFnSc1	logika
jakožto	jakožto	k8xS	jakožto
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
pravidlech	pravidlo	k1gNnPc6	pravidlo
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
řeči	řeč	k1gFnSc2	řeč
také	také	k9	také
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
forem	forma	k1gFnPc2	forma
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
:	:	kIx,	:
podstata	podstata	k1gFnSc1	podstata
<g/>
,	,	kIx,	,
případek	případek	k1gInSc1	případek
(	(	kIx(	(
<g/>
akcidens	akcidens	k1gNnSc1	akcidens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atribut	atribut	k1gInSc1	atribut
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
kategorie	kategorie	k1gFnPc4	kategorie
myšlení	myšlení	k1gNnSc2	myšlení
i	i	k8xC	i
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Aubenque	Aubenque	k1gNnSc1	Aubenque
<g/>
,	,	kIx,	,
Rozumnost	rozumnost	k1gFnSc1	rozumnost
podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
K.	K.	kA	K.
Berka	Berka	k1gMnSc1	Berka
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
J.	J.	kA	J.
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
J.	J.	kA	J.
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
a	a	k8xC	a
dědicové	dědic	k1gMnPc1	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
R.	R.	kA	R.
Sorabji	Sorabje	k1gFnSc4	Sorabje
<g/>
,	,	kIx,	,
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
o	o	k7c6	o
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
Aristotelových	Aristotelův	k2eAgInPc2d1	Aristotelův
spisů	spis	k1gInPc2	spis
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
zachované	zachovaný	k2eAgNnSc1d1	zachované
dílo	dílo	k1gNnSc1	dílo
nesmírně	smírně	k6eNd1	smírně
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
spisy	spis	k1gInPc1	spis
i	i	k8xC	i
poznámky	poznámka	k1gFnPc1	poznámka
se	se	k3xPyFc4	se
výborně	výborně	k6eAd1	výborně
hodily	hodit	k5eAaPmAgFnP	hodit
pro	pro	k7c4	pro
filosofické	filosofický	k2eAgFnPc4d1	filosofická
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
když	když	k8xS	když
ty	ten	k3xDgFnPc4	ten
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
,	,	kIx,	,
na	na	k7c4	na
čas	čas	k1gInSc4	čas
zmizely	zmizet	k5eAaPmAgInP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Objevili	objevit	k5eAaPmAgMnP	objevit
je	on	k3xPp3gNnSc4	on
řecky	řecky	k6eAd1	řecky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
křesťané	křesťan	k1gMnPc1	křesťan
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
Libanonu	Libanon	k1gInSc2	Libanon
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
do	do	k7c2	do
arabštiny	arabština	k1gFnSc2	arabština
a	a	k8xC	a
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
je	on	k3xPp3gNnPc4	on
tak	tak	k9	tak
arabským	arabský	k2eAgMnPc3d1	arabský
islámským	islámský	k2eAgMnPc3d1	islámský
vzdělancům	vzdělanec	k1gMnPc3	vzdělanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gMnPc4	on
nejen	nejen	k6eAd1	nejen
četli	číst	k5eAaImAgMnP	číst
a	a	k8xC	a
komentovali	komentovat	k5eAaBmAgMnP	komentovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
zprostředkovali	zprostředkovat	k5eAaPmAgMnP	zprostředkovat
západní	západní	k2eAgFnSc3d1	západní
středověké	středověký	k2eAgFnSc3d1	středověká
kultuře	kultura	k1gFnSc3	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
(	(	kIx(	(
<g/>
v	v	k7c6	v
latinských	latinský	k2eAgInPc6d1	latinský
překladech	překlad	k1gInPc6	překlad
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgMnS	stát
učitelem	učitel	k1gMnSc7	učitel
celé	celý	k2eAgFnSc2d1	celá
scholastiky	scholastika	k1gFnSc2	scholastika
a	a	k8xC	a
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
vzoru	vzor	k1gInSc2	vzor
se	se	k3xPyFc4	se
učilo	učít	k5eAaPmAgNnS	učít
na	na	k7c6	na
evropských	evropský	k2eAgFnPc6d1	Evropská
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
názvy	název	k1gInPc1	název
Aristotelových	Aristotelův	k2eAgInPc2d1	Aristotelův
spisů	spis	k1gInPc2	spis
nejčastěji	často	k6eAd3	často
uvádějí	uvádět	k5eAaImIp3nP	uvádět
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
aristotelských	aristotelský	k2eAgInPc2d1	aristotelský
spisů	spis	k1gInPc2	spis
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
uspořádán	uspořádán	k2eAgInSc1d1	uspořádán
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
už	už	k6eAd1	už
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
do	do	k7c2	do
několika	několik	k4yIc2	několik
tematických	tematický	k2eAgInPc2d1	tematický
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
kritické	kritický	k2eAgNnSc1d1	kritické
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
německý	německý	k2eAgMnSc1d1	německý
filolog	filolog	k1gMnSc1	filolog
August	August	k1gMnSc1	August
Immanuel	Immanuel	k1gMnSc1	Immanuel
Bekker	Bekker	k1gMnSc1	Bekker
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
akademie	akademie	k1gFnSc1	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Aristotelovy	Aristotelův	k2eAgInPc1d1	Aristotelův
spisy	spis	k1gInPc1	spis
citují	citovat	k5eAaBmIp3nP	citovat
podle	podle	k7c2	podle
stránek	stránka	k1gFnPc2	stránka
a	a	k8xC	a
sloupců	sloupec	k1gInPc2	sloupec
(	(	kIx(	(
<g/>
a	a	k8xC	a
nebo	nebo	k8xC	nebo
b	b	k?	b
<g/>
)	)	kIx)	)
případně	případně	k6eAd1	případně
i	i	k9	i
řádek	řádek	k1gInSc4	řádek
tohoto	tento	k3xDgNnSc2	tento
souborného	souborný	k2eAgNnSc2d1	souborné
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zvaného	zvaný	k2eAgNnSc2d1	zvané
"	"	kIx"	"
<g/>
Bekkerova	Bekkerův	k2eAgNnSc2d1	Bekkerův
číslování	číslování	k1gNnSc2	číslování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
<g/>
:	:	kIx,	:
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
různých	různý	k2eAgNnPc2d1	různé
vydání	vydání	k1gNnPc2	vydání
a	a	k8xC	a
překladů	překlad	k1gInPc2	překlad
se	se	k3xPyFc4	se
vždycky	vždycky	k6eAd1	vždycky
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
právě	právě	k9	právě
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
a	a	k8xC	a
citát	citát	k1gInSc1	citát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
snadno	snadno	k6eAd1	snadno
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Etika	etika	k1gFnSc1	etika
Nikomachova	Nikomachův	k2eAgFnSc1d1	Nikomachova
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
řádku	řádek	k1gInSc6	řádek
a	a	k8xC	a
sloupci	sloupec	k1gInSc6	sloupec
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
1094	[number]	k4	1094
<g/>
,	,	kIx,	,
údaj	údaj	k1gInSc1	údaj
tedy	tedy	k9	tedy
bude	být	k5eAaImBp3nS	být
1094	[number]	k4	1094
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Andronika	Andronik	k1gMnSc2	Andronik
Rhodského	rhodský	k2eAgNnSc2d1	rhodský
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
člení	členit	k5eAaImIp3nS	členit
korpus	korpus	k1gInSc1	korpus
Aristotelových	Aristotelův	k2eAgInPc2d1	Aristotelův
spisů	spis	k1gInPc2	spis
na	na	k7c4	na
logické	logický	k2eAgNnSc4d1	logické
(	(	kIx(	(
<g/>
Organon	organon	k1gNnSc4	organon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Kategorie	kategorie	k1gFnPc1	kategorie
<g/>
,	,	kIx,	,
O	o	k7c6	o
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
O	o	k7c6	o
interpretaci	interpretace	k1gFnSc6	interpretace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
První	první	k4xOgMnSc1	první
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Druhé	druhý	k4xOgFnPc1	druhý
analytiky	analytika	k1gFnPc1	analytika
<g/>
,	,	kIx,	,
Topiky	topika	k1gFnPc1	topika
a	a	k8xC	a
O	o	k7c6	o
sofistických	sofistický	k2eAgInPc6d1	sofistický
důkazech	důkaz	k1gInPc6	důkaz
<g/>
;	;	kIx,	;
přírodovědecké	přírodovědecký	k2eAgInPc1d1	přírodovědecký
<g/>
:	:	kIx,	:
Fysika	fysika	k1gFnSc1	fysika
<g/>
,	,	kIx,	,
O	o	k7c6	o
nebi	nebe	k1gNnSc6	nebe
<g/>
,	,	kIx,	,
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
zániku	zánik	k1gInSc6	zánik
<g/>
,	,	kIx,	,
Meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
,	,	kIx,	,
O	o	k7c6	o
duši	duše	k1gFnSc6	duše
a	a	k8xC	a
17	[number]	k4	17
menších	malý	k2eAgInPc2d2	menší
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
poloviny	polovina	k1gFnSc2	polovina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
autorství	autorství	k1gNnSc6	autorství
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
<g/>
,	,	kIx,	,
Metafysiku	metafysik	k1gMnSc6	metafysik
<g/>
,	,	kIx,	,
etické	etický	k2eAgNnSc1d1	etické
a	a	k8xC	a
politické	politický	k2eAgNnSc1d1	politické
<g/>
:	:	kIx,	:
Etika	etika	k1gFnSc1	etika
Nikomachova	Nikomachův	k2eAgNnSc2d1	Nikomachův
a	a	k8xC	a
Eudemova	Eudemův	k2eAgNnSc2d1	Eudemův
<g/>
,	,	kIx,	,
Politika	politikum	k1gNnSc2	politikum
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
3	[number]	k4	3
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
rétoriku	rétorika	k1gFnSc4	rétorika
a	a	k8xC	a
poetiku	poetika	k1gFnSc4	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tradiční	tradiční	k2eAgInSc4d1	tradiční
korpus	korpus	k1gInSc4	korpus
je	být	k5eAaImIp3nS	být
Athénská	athénský	k2eAgFnSc1d1	Athénská
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
nalezená	nalezený	k2eAgFnSc1d1	nalezená
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
<g/>
:	:	kIx,	:
Kategorie	kategorie	k1gFnPc1	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Organon	organon	k1gInSc1	organon
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
O	o	k7c6	o
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
Organon	organon	k1gInSc1	organon
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
První	první	k4xOgFnSc2	první
analytiky	analytika	k1gFnSc2	analytika
<g/>
.	.	kIx.	.
</s>
<s>
Organon	organon	k1gInSc1	organon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
Druhé	druhý	k4xOgFnSc2	druhý
analytiky	analytika	k1gFnSc2	analytika
<g/>
.	.	kIx.	.
</s>
<s>
Organon	organon	k1gNnSc1	organon
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
Topiky	topikum	k1gNnPc7	topikum
<g/>
.	.	kIx.	.
</s>
<s>
Organon	organon	k1gInSc1	organon
V.	V.	kA	V.
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
O	o	k7c6	o
sofistických	sofistický	k2eAgInPc6d1	sofistický
důkazech	důkaz	k1gInPc6	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Organon	organon	k1gInSc1	organon
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
Fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
O	o	k7c6	o
nebi	nebe	k1gNnSc6	nebe
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
zániku	zánik	k1gInSc6	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
1985	[number]	k4	1985
O	o	k7c6	o
duši	duše	k1gFnSc6	duše
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
Metafyzika	metafyzika	k1gFnSc1	metafyzika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Etika	etika	k1gFnSc1	etika
Nikomachova	Nikomachův	k2eAgFnSc1d1	Nikomachova
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Politika	politikum	k1gNnSc2	politikum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Politika	politikum	k1gNnSc2	politikum
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
1988	[number]	k4	1988
Poetika	poetika	k1gFnSc1	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
Poetika	poetika	k1gFnSc1	poetika
;	;	kIx,	;
Rétorika	rétorika	k1gFnSc1	rétorika
;	;	kIx,	;
Politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
1980	[number]	k4	1980
Rétorika	rétorika	k1gFnSc1	rétorika
<g/>
.	.	kIx.	.
</s>
<s>
Poetika	poetika	k1gFnSc1	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
Athénská	athénský	k2eAgFnSc1d1	Athénská
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
Výbory	výbor	k1gInPc4	výbor
<g/>
:	:	kIx,	:
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1984	[number]	k4	1984
Epagogé	Epagogý	k2eAgInPc1d1	Epagogý
a	a	k8xC	a
epistémé	epistémý	k2eAgInPc1d1	epistémý
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
Logos	logos	k1gInSc1	logos
apofantikos	apofantikos	k1gInSc4	apofantikos
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
Spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
jako	jako	k8xS	jako
zdatnost	zdatnost	k1gFnSc4	zdatnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
Připisováno	připisovat	k5eAaImNgNnS	připisovat
Aristotelovi	Aristoteles	k1gMnSc3	Aristoteles
<g/>
:	:	kIx,	:
Liber	libra	k1gFnPc2	libra
de	de	k?	de
causis	causis	k1gFnPc2	causis
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
Magna	Magna	k1gFnSc1	Magna
moralia	moralia	k1gFnSc1	moralia
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
Většinu	většina	k1gFnSc4	většina
Aristotelových	Aristotelův	k2eAgInPc2d1	Aristotelův
spisů	spis	k1gInPc2	spis
přeložili	přeložit	k5eAaPmAgMnP	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Antonín	Antonín	k1gMnSc1	Antonín
Kříž	Kříž	k1gMnSc1	Kříž
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
fysiky	fysika	k1gFnSc2	fysika
jako	jako	k8xS	jako
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
fysis	fysis	k1gFnSc6	fysis
(	(	kIx(	(
<g/>
přirozenosti	přirozenost	k1gFnSc6	přirozenost
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
-	-	kIx~	-
jako	jako	k9	jako
ostatně	ostatně	k6eAd1	ostatně
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
antice	antika	k1gFnSc6	antika
-	-	kIx~	-
podstatně	podstatně	k6eAd1	podstatně
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
se	se	k3xPyFc4	se
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slovesa	sloveso	k1gNnSc2	sloveso
fyó	fyó	k?	fyó
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
vznikat	vznikat	k5eAaImF	vznikat
<g/>
,	,	kIx,	,
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
příroda	příroda	k1gFnSc1	příroda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
Aristotela	Aristoteles	k1gMnSc4	Aristoteles
především	především	k6eAd1	především
živá	živý	k2eAgFnSc1d1	živá
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
všechno	všechen	k3xTgNnSc1	všechen
živé	živá	k1gFnPc4	živá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
zrození	zrození	k1gNnSc1	zrození
<g/>
,	,	kIx,	,
růst	růst	k1gInSc1	růst
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
vnímavost	vnímavost	k1gFnSc1	vnímavost
a	a	k8xC	a
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednodušších	jednoduchý	k2eAgMnPc2d2	jednodušší
živočichů	živočich	k1gMnPc2	živočich
patrně	patrně	k6eAd1	patrně
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
samoplození	samoplození	k1gNnSc1	samoplození
<g/>
,	,	kIx,	,
správně	správně	k6eAd1	správně
však	však	k9	však
vykládá	vykládat	k5eAaImIp3nS	vykládat
význam	význam	k1gInSc4	význam
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
živé	živé	k1gNnSc1	živé
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
duši	duše	k1gFnSc4	duše
(	(	kIx(	(
<g/>
psyché	psyché	k1gFnSc2	psyché
<g/>
,	,	kIx,	,
anima	animo	k1gNnSc2	animo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
trojího	trojí	k4xRgMnSc2	trojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
vegetativní	vegetativní	k2eAgMnSc1d1	vegetativní
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
;	;	kIx,	;
vnímavá	vnímavý	k2eAgFnSc1d1	vnímavá
(	(	kIx(	(
<g/>
sensitiva	sensitiva	k1gFnSc1	sensitiva
<g/>
)	)	kIx)	)
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
a	a	k8xC	a
vnímají	vnímat	k5eAaImIp3nP	vnímat
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozumná	rozumný	k2eAgFnSc1d1	rozumná
(	(	kIx(	(
<g/>
rationalis	rationalis	k1gFnSc1	rationalis
<g/>
)	)	kIx)	)
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
věnuje	věnovat	k5eAaPmIp3nS	věnovat
popisu	popis	k1gInSc2	popis
a	a	k8xC	a
anatomii	anatomie	k1gFnSc4	anatomie
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
všímá	všímat	k5eAaImIp3nS	všímat
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gFnPc2	jejich
podobností	podobnost	k1gFnPc2	podobnost
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
účelnost	účelnost	k1gFnSc4	účelnost
jejich	jejich	k3xOp3gInPc2	jejich
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
nepomýšlí	pomýšlet	k5eNaImIp3nS	pomýšlet
ale	ale	k8xC	ale
na	na	k7c4	na
žádný	žádný	k3yNgInSc4	žádný
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
o	o	k7c6	o
geografických	geografický	k2eAgInPc6d1	geografický
jevech	jev	k1gInPc6	jev
<g/>
,	,	kIx,	,
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
shrnul	shrnout	k5eAaPmAgInS	shrnout
a	a	k8xC	a
zobecnil	zobecnit	k5eAaPmAgInS	zobecnit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Meteorologika	Meteorologika	k1gFnSc1	Meteorologika
(	(	kIx(	(
<g/>
Čtyři	čtyři	k4xCgFnPc1	čtyři
knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
jevech	jev	k1gInPc6	jev
meteorických	meteorický	k2eAgInPc2d1	meteorický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
přírodních	přírodní	k2eAgInPc6d1	přírodní
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
probíhajících	probíhající	k2eAgInPc2d1	probíhající
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
sublunární	sublunární	k2eAgFnSc6d1	sublunární
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
elementární	elementární	k2eAgInPc4d1	elementární
poznatky	poznatek	k1gInPc4	poznatek
z	z	k7c2	z
obecné	obecný	k2eAgFnSc2d1	obecná
fyzické	fyzický	k2eAgFnSc2d1	fyzická
geografie	geografie	k1gFnSc2	geografie
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyčleněné	vyčleněný	k2eAgInPc1d1	vyčleněný
z	z	k7c2	z
jednotné	jednotný	k2eAgFnSc2d1	jednotná
starořecké	starořecký	k2eAgFnSc2d1	starořecká
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
podle	podle	k7c2	podle
sfér	sféra	k1gFnPc2	sféra
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
důsledně	důsledně	k6eAd1	důsledně
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
chápe	chápat	k5eAaImIp3nS	chápat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
jako	jako	k9	jako
sféru	sféra	k1gFnSc4	sféra
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
,	,	kIx,	,
sráží	srážet	k5eAaImIp3nS	srážet
se	se	k3xPyFc4	se
a	a	k8xC	a
padá	padat	k5eAaImIp3nS	padat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Koloběh	koloběh	k1gInSc1	koloběh
vody	voda	k1gFnSc2	voda
připomíná	připomínat	k5eAaImIp3nS	připomínat
řeku	řeka	k1gFnSc4	řeka
tekoucí	tekoucí	k2eAgFnSc4d1	tekoucí
po	po	k7c6	po
kruhu	kruh	k1gInSc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vypařování	vypařování	k1gNnSc3	vypařování
dochází	docházet	k5eAaImIp3nS	docházet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc4	srážka
padají	padat	k5eAaImIp3nP	padat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
a	a	k8xC	a
v	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
prší	pršet	k5eAaImIp3nS	pršet
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
;	;	kIx,	;
příčinu	příčina	k1gFnSc4	příčina
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
mraky	mrak	k1gInPc1	mrak
-	-	kIx~	-
následkem	následkem	k7c2	následkem
velkého	velký	k2eAgNnSc2d1	velké
horka	horko	k1gNnSc2	horko
-	-	kIx~	-
rychle	rychle	k6eAd1	rychle
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
Platónem	Platón	k1gMnSc7	Platón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
napájeny	napájet	k5eAaImNgFnP	napájet
z	z	k7c2	z
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
podzemních	podzemní	k2eAgInPc2d1	podzemní
rezervoárů	rezervoár	k1gInPc2	rezervoár
<g/>
,	,	kIx,	,
doplňovaných	doplňovaný	k2eAgFnPc2d1	doplňovaná
atmosferickými	atmosferický	k2eAgFnPc7d1	atmosferická
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
starověký	starověký	k2eAgInSc4d1	starověký
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
vytékat	vytékat	k5eAaImF	vytékat
z	z	k7c2	z
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zejména	zejména	k9	zejména
veletoky	veletok	k1gInPc1	veletok
pramení	pramenit	k5eAaImIp3nP	pramenit
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývá	bývat	k5eAaImIp3nS	bývat
nejvíce	nejvíce	k6eAd1	nejvíce
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chladu	chlad	k1gInSc3	chlad
a	a	k8xC	a
příznivějším	příznivý	k2eAgFnPc3d2	příznivější
podmínkám	podmínka	k1gFnPc3	podmínka
kondenzace	kondenzace	k1gFnSc2	kondenzace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
kapitolu	kapitola	k1gFnSc4	kapitola
věnuje	věnovat	k5eAaImIp3nS	věnovat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Souše	souš	k1gFnPc1	souš
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
může	moct	k5eAaImIp3nS	moct
proměňovat	proměňovat	k5eAaImF	proměňovat
v	v	k7c4	v
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
má	mít	k5eAaImIp3nS	mít
cyklický	cyklický	k2eAgInSc4d1	cyklický
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
činnost	činnost	k1gFnSc1	činnost
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
nánosů	nános	k1gInPc2	nános
Nilu	Nil	k1gInSc2	Nil
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
úroveň	úroveň	k1gFnSc1	úroveň
leží	ležet	k5eAaImIp3nS	ležet
údajně	údajně	k6eAd1	údajně
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
hladina	hladina	k1gFnSc1	hladina
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgInPc1d1	říční
nánosy	nános	k1gInPc1	nános
postupně	postupně	k6eAd1	postupně
zaplňují	zaplňovat	k5eAaImIp3nP	zaplňovat
například	například	k6eAd1	například
i	i	k9	i
Maiotis	Maiotis	k1gFnSc1	Maiotis
(	(	kIx(	(
<g/>
Azovské	azovský	k2eAgNnSc1d1	Azovské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
řecké	řecký	k2eAgFnPc1d1	řecká
roviny	rovina	k1gFnPc1	rovina
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
mořských	mořský	k2eAgMnPc2d1	mořský
zálivů	záliv	k1gInPc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
změny	změna	k1gFnPc1	změna
probíhají	probíhat	k5eAaImIp3nP	probíhat
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
Země	zem	k1gFnSc2	zem
mají	mít	k5eAaImIp3nP	mít
různý	různý	k2eAgInSc4d1	různý
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
přibývá	přibývat	k5eAaImIp3nS	přibývat
vlhkosti	vlhkost	k1gFnPc4	vlhkost
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
vysychají	vysychat	k5eAaImIp3nP	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
doby	doba	k1gFnPc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neexistoval	existovat	k5eNaImAgInS	existovat
Nil	Nil	k1gInSc1	Nil
ani	ani	k8xC	ani
Tanais	Tanais	k1gFnSc1	Tanais
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
časem	časem	k6eAd1	časem
obě	dva	k4xCgFnPc1	dva
řeky	řeka	k1gFnPc1	řeka
zřejmě	zřejmě	k6eAd1	zřejmě
zase	zase	k9	zase
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
má	mít	k5eAaImIp3nS	mít
počátek	počátek	k1gInSc4	počátek
a	a	k8xC	a
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc4	moře
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
řek	řeka	k1gFnPc2	řeka
<g/>
)	)	kIx)	)
stabilní	stabilní	k2eAgInPc1d1	stabilní
toky	tok	k1gInPc1	tok
<g/>
,	,	kIx,	,
proudění	proudění	k1gNnSc1	proudění
vzniká	vznikat	k5eAaImIp3nS	vznikat
pouze	pouze	k6eAd1	pouze
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
úzkých	úzký	k2eAgInPc6d1	úzký
průlivech	průliv	k1gInPc6	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
vliv	vliv	k1gInSc4	vliv
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
hloubka	hloubka	k1gFnSc1	hloubka
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
prý	prý	k9	prý
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
proudění	proudění	k1gNnSc3	proudění
z	z	k7c2	z
Maiotisu	Maiotis	k1gInSc2	Maiotis
do	do	k7c2	do
Pontos	Pontos	k1gInSc4	Pontos
Euxinos	Euxinos	k1gInSc4	Euxinos
(	(	kIx(	(
<g/>
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Egejského	egejský	k2eAgMnSc2d1	egejský
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Sicilského	sicilský	k2eAgNnSc2d1	sicilské
<g/>
,	,	kIx,	,
Sardinského	sardinský	k2eAgNnSc2d1	Sardinské
a	a	k8xC	a
Tyrhénského	tyrhénský	k2eAgNnSc2d1	Tyrhénské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
údajně	údajně	k6eAd1	údajně
nejhlubší	hluboký	k2eAgFnPc1d3	nejhlubší
<g/>
.	.	kIx.	.
</s>
<s>
Výpary	výpar	k1gInPc1	výpar
a	a	k8xC	a
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
moří	moře	k1gNnPc2	moře
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Démokritovu	Démokritův	k2eAgFnSc4d1	Démokritova
myšlenku	myšlenka	k1gFnSc4	myšlenka
o	o	k7c6	o
postupném	postupný	k2eAgNnSc6d1	postupné
vysychání	vysychání	k1gNnSc6	vysychání
moří	moře	k1gNnPc2	moře
označuje	označovat	k5eAaImIp3nS	označovat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
za	za	k7c4	za
Ezopovu	Ezopův	k2eAgFnSc4d1	Ezopova
bajku	bajka	k1gFnSc4	bajka
<g/>
.	.	kIx.	.
</s>
<s>
Salinitu	salinita	k1gFnSc4	salinita
prý	prý	k9	prý
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
dostává	dostávat	k5eAaImIp3nS	dostávat
zemní	zemní	k2eAgFnSc1d1	zemní
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vlivem	vlivem	k7c2	vlivem
suchého	suchý	k2eAgNnSc2d1	suché
vypařování	vypařování	k1gNnSc2	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nS	mísit
s	s	k7c7	s
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
obyčejným	obyčejný	k2eAgNnSc7d1	obyčejné
<g/>
)	)	kIx)	)
vypařováním	vypařování	k1gNnSc7	vypařování
<g/>
,	,	kIx,	,
následkem	následek	k1gInSc7	následek
jsou	být	k5eAaImIp3nP	být
příměsi	příměs	k1gFnPc1	příměs
solí	sůl	k1gFnPc2	sůl
v	v	k7c6	v
atmosferických	atmosferický	k2eAgFnPc6d1	atmosferická
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgInPc1d1	Suché
páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
větrů	vítr	k1gInPc2	vítr
<g/>
;	;	kIx,	;
nejde	jít	k5eNaImIp3nS	jít
prý	prý	k9	prý
o	o	k7c4	o
pohyb	pohyb	k1gInSc4	pohyb
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Převažují	převažovat	k5eAaImIp3nP	převažovat
<g/>
-li	i	k?	-li
suché	suchý	k2eAgInPc1d1	suchý
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
suché	suchý	k2eAgInPc1d1	suchý
větrné	větrný	k2eAgInPc1d1	větrný
roky	rok	k1gInPc1	rok
<g/>
;	;	kIx,	;
vlhké	vlhký	k2eAgNnSc4d1	vlhké
vypařování	vypařování	k1gNnSc4	vypařování
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vlhká	vlhký	k2eAgFnSc1d1	vlhká
a	a	k8xC	a
deštivá	deštivý	k2eAgNnPc1d1	deštivé
léta	léto	k1gNnPc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
prý	prý	k9	prý
vanou	vanout	k5eAaImIp3nP	vanout
od	od	k7c2	od
severu	sever	k1gInSc2	sever
nebo	nebo	k8xC	nebo
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Chlad	chlad	k1gInSc1	chlad
a	a	k8xC	a
horko	horko	k1gNnSc1	horko
brání	bránit	k5eAaImIp3nS	bránit
jejich	jejich	k3xOp3gInSc3	jejich
vzniku	vznik	k1gInSc3	vznik
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
především	především	k9	především
při	při	k7c6	při
střídání	střídání	k1gNnSc6	střídání
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
substance	substance	k1gFnSc1	substance
větru	vítr	k1gInSc2	vítr
pozemský	pozemský	k2eAgInSc4d1	pozemský
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
jeho	jeho	k3xOp3gInSc1	jeho
směr	směr	k1gInSc1	směr
určován	určován	k2eAgInSc1d1	určován
pohybem	pohyb	k1gInSc7	pohyb
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
oblohy	obloha	k1gFnSc2	obloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
větry	vítr	k1gInPc4	vítr
(	(	kIx(	(
<g/>
suché	suchý	k2eAgInPc1d1	suchý
páry	pár	k1gInPc1	pár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
dutin	dutina	k1gFnPc2	dutina
(	(	kIx(	(
<g/>
pastí	past	k1gFnPc2	past
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
prý	prý	k9	prý
nejčastěji	často	k6eAd3	často
za	za	k7c2	za
tichého	tichý	k2eAgNnSc2d1	tiché
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
sníženinách	sníženina	k1gFnPc6	sníženina
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
pórovitých	pórovitý	k2eAgFnPc2d1	pórovitá
hornin	hornina	k1gFnPc2	hornina
<g/>
;	;	kIx,	;
nezřídka	nezřídka	k6eAd1	nezřídka
prý	prý	k9	prý
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
zatmění	zatmění	k1gNnSc6	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
blesky	blesk	k1gInPc1	blesk
<g/>
,	,	kIx,	,
hromy	hrom	k1gInPc1	hrom
a	a	k8xC	a
orkány	orkán	k1gInPc1	orkán
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc1	produkt
suchého	suchý	k2eAgNnSc2d1	suché
vypařování	vypařování	k1gNnSc2	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
nitro	nitro	k1gNnSc1	nitro
se	se	k3xPyFc4	se
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
obou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
vypařování	vypařování	k1gNnSc2	vypařování
-	-	kIx~	-
suché	suchý	k2eAgNnSc1d1	suché
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nerudných	rudný	k2eNgFnPc2d1	nerudná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgNnSc4d1	vlhké
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
věnoval	věnovat	k5eAaImAgMnS	věnovat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
živočišstvu	živočišstvo	k1gNnSc6	živočišstvo
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
asi	asi	k9	asi
500	[number]	k4	500
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
základy	základ	k1gInPc1	základ
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
anatomie	anatomie	k1gFnSc2	anatomie
a	a	k8xC	a
embryologie	embryologie	k1gFnSc2	embryologie
<g/>
.	.	kIx.	.
</s>
<s>
Vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
elementární	elementární	k2eAgFnSc4d1	elementární
klasifikaci	klasifikace	k1gFnSc4	klasifikace
živočišstva	živočišstvo	k1gNnSc2	živočišstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
tezi	teze	k1gFnSc4	teze
o	o	k7c6	o
postupném	postupný	k2eAgInSc6d1	postupný
přechodu	přechod	k1gInSc6	přechod
od	od	k7c2	od
neživé	živý	k2eNgFnSc2d1	neživá
přírody	příroda	k1gFnSc2	příroda
k	k	k7c3	k
rostlinstvu	rostlinstvo	k1gNnSc3	rostlinstvo
a	a	k8xC	a
od	od	k7c2	od
rostlinstva	rostlinstvo	k1gNnSc2	rostlinstvo
k	k	k7c3	k
živočišstvu	živočišstvo	k1gNnSc3	živočišstvo
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
ovšem	ovšem	k9	ovšem
neměla	mít	k5eNaImAgFnS	mít
evoluční	evoluční	k2eAgInSc4d1	evoluční
obsah	obsah	k1gInSc4	obsah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgInPc1d1	živý
objekty	objekt	k1gInPc1	objekt
zůstaly	zůstat	k5eAaPmAgInP	zůstat
pro	pro	k7c4	pro
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
předmětem	předmět	k1gInSc7	předmět
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
pracích	práce	k1gFnPc6	práce
je	být	k5eAaImIp3nS	být
systematizován	systematizován	k2eAgInSc1d1	systematizován
a	a	k8xC	a
zobecněn	zobecněn	k2eAgInSc1d1	zobecněn
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
empirický	empirický	k2eAgInSc1d1	empirický
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pět	pět	k4xCc4	pět
zemských	zemský	k2eAgInPc2d1	zemský
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
dva	dva	k4xCgInPc1	dva
jsou	být	k5eAaImIp3nP	být
obyvatelné	obyvatelný	k2eAgFnPc1d1	obyvatelná
<g/>
.	.	kIx.	.
</s>
<s>
Souše	souš	k1gFnPc1	souš
zabírají	zabírat	k5eAaImIp3nP	zabírat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
severního	severní	k2eAgInSc2d1	severní
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
protáhlost	protáhlost	k1gFnSc1	protáhlost
<g/>
)	)	kIx)	)
ekumeny	ekumena	k1gFnPc1	ekumena
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
(	(	kIx(	(
<g/>
od	od	k7c2	od
Heraklových	Heraklův	k2eAgInPc2d1	Heraklův
sloupů	sloup	k1gInPc2	sloup
-	-	kIx~	-
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
po	po	k7c6	po
Indii	Indie	k1gFnSc6	Indie
<g/>
)	)	kIx)	)
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
prý	prý	k9	prý
její	její	k3xOp3gFnSc4	její
šířku	šířka	k1gFnSc4	šířka
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
(	(	kIx(	(
<g/>
od	od	k7c2	od
Skýtie	Skýtie	k1gFnSc2	Skýtie
po	po	k7c6	po
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
)	)	kIx)	)
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Aristotelových	Aristotelův	k2eAgInPc6d1	Aristotelův
prostorových	prostorový	k2eAgInPc6d1	prostorový
obzorech	obzor	k1gInPc6	obzor
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
představu	představa	k1gFnSc4	představa
zejména	zejména	k9	zejména
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
kapitoly	kapitola	k1gFnPc1	kapitola
druhé	druhý	k4xOgFnSc2	druhý
knihy	kniha	k1gFnSc2	kniha
Meteorologiky	Meteorologika	k1gFnSc2	Meteorologika
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hlavní	hlavní	k2eAgInPc1d1	hlavní
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
řeky	řeka	k1gFnPc4	řeka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zná	znát	k5eAaImIp3nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
Tigridu	Tigris	k1gInSc6	Tigris
<g/>
,	,	kIx,	,
Eufratu	Eufrat	k1gInSc6	Eufrat
ani	ani	k8xC	ani
o	o	k7c6	o
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc7d1	východní
hranicí	hranice	k1gFnSc7	hranice
ekumeny	ekumena	k1gFnSc2	ekumena
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Parnas	Parnas	k1gInSc1	Parnas
(	(	kIx(	(
<g/>
Paropamíz	Paropamíz	k1gInSc1	Paropamíz
a	a	k8xC	a
Hindúkuš	Hindúkuš	k1gInSc1	Hindúkuš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
tekou	teka	k1gFnSc7	teka
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
Baktra	Baktra	k1gFnSc1	Baktra
(	(	kIx(	(
<g/>
Amudarja	Amudarja	k1gFnSc1	Amudarja
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hoasp	Hoasp	k1gMnSc1	Hoasp
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nil	Nil	k1gInSc1	Nil
prý	prý	k9	prý
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
"	"	kIx"	"
<g/>
Stříbrných	stříbrný	k2eAgFnPc6d1	stříbrná
horách	hora	k1gFnPc6	hora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Hremet	Hremet	k1gInSc1	Hremet
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlévající	vlévající	k2eAgFnSc2d1	vlévající
se	se	k3xPyFc4	se
do	do	k7c2	do
"	"	kIx"	"
<g/>
Vnějšího	vnější	k2eAgInSc2d1	vnější
oceánu	oceán	k1gInSc2	oceán
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
sdílel	sdílet	k5eAaImAgInS	sdílet
i	i	k9	i
mnohé	mnohý	k2eAgInPc4d1	mnohý
omyly	omyl	k1gInPc4	omyl
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
i	i	k8xC	i
současníků	současník	k1gMnPc2	současník
-	-	kIx~	-
za	za	k7c4	za
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
a	a	k8xC	a
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
pohoří	pohoří	k1gNnSc4	pohoří
pokládal	pokládat	k5eAaImAgMnS	pokládat
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
,	,	kIx,	,
za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
řeku	řeka	k1gFnSc4	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
podzemní	podzemní	k2eAgNnSc4d1	podzemní
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Pontem	Pont	k1gInSc7	Pont
<g/>
.	.	kIx.	.
</s>
<s>
Tanais	Tanais	k1gFnSc1	Tanais
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odvětvuje	odvětvovat	k5eAaImIp3nS	odvětvovat
od	od	k7c2	od
Araksu	Araks	k1gInSc2	Araks
(	(	kIx(	(
<g/>
Syrdarji	Syrdarja	k1gFnSc6	Syrdarja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Istros	Istrosa	k1gFnPc2	Istrosa
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgInSc1d1	dolní
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tartes	Tartes	k1gMnSc1	Tartes
(	(	kIx(	(
<g/>
Guadalquivir	Guadalquivir	k1gMnSc1	Guadalquivir
<g/>
)	)	kIx)	)
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
prý	prý	k9	prý
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
zakladatelem	zakladatel	k1gMnSc7	zakladatel
hydrologie	hydrologie	k1gFnSc2	hydrologie
<g/>
,	,	kIx,	,
oceánologie	oceánologie	k1gFnSc2	oceánologie
a	a	k8xC	a
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
historický	historický	k2eAgInSc4d1	historický
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
interakce	interakce	k1gFnSc2	interakce
souše	souš	k1gFnSc2	souš
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
zoologickým	zoologický	k2eAgFnPc3d1	zoologická
otázkám	otázka	k1gFnPc3	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
o	o	k7c6	o
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
reálné	reálný	k2eAgFnSc2d1	reálná
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
příčinnosti	příčinnost	k1gFnSc2	příčinnost
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
procesů	proces	k1gInPc2	proces
vývoje	vývoj	k1gInSc2	vývoj
matematickými	matematický	k2eAgInPc7d1	matematický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
Peri	peri	k1gFnSc2	peri
uranú	uranú	k?	uranú
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
De	De	k?	De
coelo	coelo	k1gNnSc4	coelo
-	-	kIx~	-
O	o	k7c6	o
nebi	nebe	k1gNnSc6	nebe
<g/>
)	)	kIx)	)
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
argumenty	argument	k1gInPc4	argument
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
učení	učení	k1gNnSc2	učení
pythágorejské	pythágorejský	k2eAgFnSc2d1	pythágorejský
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Sókrata	Sókrat	k1gMnSc2	Sókrat
<g/>
,	,	kIx,	,
Platóna	Platón	k1gMnSc2	Platón
(	(	kIx(	(
<g/>
dialog	dialog	k1gInSc1	dialog
Faidon	Faidon	k1gInSc1	Faidon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
.	.	kIx.	.
</s>
<s>
Otázkami	otázka	k1gFnPc7	otázka
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc4	hledání
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
života	život	k1gInSc2	život
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
zabýval	zabývat	k5eAaImAgInS	zabývat
v	v	k7c6	v
několika	několik	k4yIc6	několik
spisech	spis	k1gInPc6	spis
<g/>
:	:	kIx,	:
v	v	k7c6	v
Etice	etika	k1gFnSc6	etika
Eudemově	Eudemův	k2eAgFnSc6d1	Eudemův
<g/>
,	,	kIx,	,
v	v	k7c6	v
Politice	politika	k1gFnSc6	politika
a	a	k8xC	a
v	v	k7c6	v
nejznámější	známý	k2eAgFnSc6d3	nejznámější
Etice	etika	k1gFnSc6	etika
Nikomachově	Nikomachův	k2eAgFnSc6d1	Nikomachova
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
následující	následující	k2eAgInPc4d1	následující
citáty	citát	k1gInPc4	citát
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
z	z	k7c2	z
věd	věda	k1gFnPc2	věda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Aristotela	Aristoteles	k1gMnSc4	Aristoteles
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
(	(	kIx(	(
<g/>
politiké	politiká	k1gFnSc6	politiká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
musí	muset	k5eAaImIp3nS	muset
naučit	naučit	k5eAaPmF	naučit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
o	o	k7c6	o
poznání	poznání	k1gNnSc6	poznání
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c6	o
jednání	jednání	k1gNnSc6	jednání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ušlechtilé	ušlechtilý	k2eAgNnSc1d1	ušlechtilé
a	a	k8xC	a
spravedlivé	spravedlivý	k2eAgNnSc1d1	spravedlivé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
jednání	jednání	k1gNnSc2	jednání
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
zdraví	zdraví	k1gNnSc2	zdraví
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
přesnost	přesnost	k1gFnSc1	přesnost
možná	možný	k2eAgFnSc1d1	možná
a	a	k8xC	a
"	"	kIx"	"
<g/>
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
oblasti	oblast	k1gFnSc6	oblast
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
jen	jen	k9	jen
tolik	tolik	k4xDc4	tolik
přesnosti	přesnost	k1gFnSc2	přesnost
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
povaha	povaha	k1gFnSc1	povaha
předmětu	předmět	k1gInSc2	předmět
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1095	[number]	k4	1095
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
jednání	jednání	k1gNnSc1	jednání
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
blaženost	blaženost	k1gFnSc1	blaženost
(	(	kIx(	(
<g/>
eudaimonia	eudaimonium	k1gNnSc2	eudaimonium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
jako	jako	k8xS	jako
pocit	pocit	k1gInSc4	pocit
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
Blaženost	blaženost	k1gFnSc1	blaženost
je	být	k5eAaImIp3nS	být
nejspíš	nejspíš	k9	nejspíš
darem	dar	k1gInSc7	dar
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
něco	něco	k3yInSc4	něco
také	také	k9	také
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
:	:	kIx,	:
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Blažený	blažený	k2eAgMnSc1d1	blažený
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
jednání	jednání	k1gNnSc6	jednání
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nutné	nutný	k2eAgFnPc1d1	nutná
podmínky	podmínka	k1gFnPc1	podmínka
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1101	[number]	k4	1101
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
blaženosti	blaženost	k1gFnSc3	blaženost
je	být	k5eAaImIp3nS	být
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
učením	učení	k1gNnSc7	učení
<g/>
,	,	kIx,	,
zkušeností	zkušenost	k1gFnSc7	zkušenost
a	a	k8xC	a
návykem	návyk	k1gInSc7	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Přirozenost	přirozenost	k1gFnSc1	přirozenost
dává	dávat	k5eAaImIp3nS	dávat
člověku	člověk	k1gMnSc3	člověk
jen	jen	k6eAd1	jen
schopnost	schopnost	k1gFnSc4	schopnost
učení	učení	k1gNnSc2	učení
jako	jako	k8xC	jako
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
konáním	konání	k1gNnSc7	konání
(	(	kIx(	(
<g/>
dobrým	dobré	k1gNnSc7	dobré
nebo	nebo	k8xC	nebo
špatným	špatný	k2eAgInSc7d1	špatný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
smysl	smysl	k1gInSc4	smysl
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Neřestí	neřest	k1gFnPc2	neřest
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ctnost	ctnost	k1gFnSc1	ctnost
je	být	k5eAaImIp3nS	být
vždycky	vždycky	k6eAd1	vždycky
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
znamená	znamenat	k5eAaImIp3nS	znamenat
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
krajnostem	krajnost	k1gFnPc3	krajnost
<g/>
,	,	kIx,	,
nedostatku	nedostatek	k1gInSc3	nedostatek
i	i	k8xC	i
přebytku	přebytek	k1gInSc3	přebytek
<g/>
;	;	kIx,	;
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
štědrost	štědrost	k1gFnSc1	štědrost
mezi	mezi	k7c7	mezi
rozhazovačností	rozhazovačnost	k1gFnSc7	rozhazovačnost
a	a	k8xC	a
lakotou	lakota	k1gFnSc7	lakota
<g/>
,	,	kIx,	,
statečnost	statečnost	k1gFnSc1	statečnost
mezi	mezi	k7c7	mezi
zbrklostí	zbrklost	k1gFnSc7	zbrklost
a	a	k8xC	a
zbabělostí	zbabělost	k1gFnSc7	zbabělost
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
žádný	žádný	k3yNgInSc1	žádný
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
dokonalost	dokonalost	k1gFnSc1	dokonalost
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
by	by	kYmCp3nS	by
každé	každý	k3xTgNnSc1	každý
přidání	přidání	k1gNnSc1	přidání
i	i	k8xC	i
ubrání	ubrání	k1gNnSc1	ubrání
pokazilo	pokazit	k5eAaPmAgNnS	pokazit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1107	[number]	k4	1107
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Ctnost	ctnost	k1gFnSc1	ctnost
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
jen	jen	k9	jen
jednání	jednání	k1gNnPc4	jednání
chtěných	chtěný	k2eAgInPc2d1	chtěný
a	a	k8xC	a
vědomých	vědomý	k2eAgInPc2d1	vědomý
<g/>
,	,	kIx,	,
volba	volba	k1gFnSc1	volba
je	být	k5eAaImIp3nS	být
předem	předem	k6eAd1	předem
rozvážené	rozvážený	k2eAgNnSc1d1	rozvážené
chtěné	chtěný	k2eAgNnSc1d1	chtěné
jednání	jednání	k1gNnSc1	jednání
(	(	kIx(	(
<g/>
PRO-AIRESIS	PRO-AIRESIS	k1gFnSc1	PRO-AIRESIS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
ctnosti	ctnost	k1gFnPc1	ctnost
jsou	být	k5eAaImIp3nP	být
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
,	,	kIx,	,
umírněnost	umírněnost	k1gFnSc4	umírněnost
<g/>
,	,	kIx,	,
štědrost	štědrost	k1gFnSc4	štědrost
<g/>
,	,	kIx,	,
laskavost	laskavost	k1gFnSc4	laskavost
jako	jako	k8xC	jako
střed	střed	k1gInSc4	střed
vůči	vůči	k7c3	vůči
hněvivosti	hněvivost	k1gFnSc3	hněvivost
i	i	k8xC	i
slabošství	slabošství	k1gNnSc3	slabošství
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
zejména	zejména	k9	zejména
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výkon	výkon	k1gInSc4	výkon
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
ctnosti	ctnost	k1gFnSc2	ctnost
vůči	vůči	k7c3	vůči
druhým	druhý	k4xOgMnPc3	druhý
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dobré	dobrý	k2eAgNnSc4d1	dobré
těch	ten	k3xDgMnPc2	ten
druhých	druhý	k4xOgMnPc2	druhý
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
souhrn	souhrn	k1gInSc1	souhrn
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
ctností	ctnost	k1gFnPc2	ctnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1130	[number]	k4	1130
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
pak	pak	k6eAd1	pak
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
spravedlností	spravedlnost	k1gFnSc7	spravedlnost
retributivní	retributivní	k2eAgFnSc7d1	retributivní
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
napravuje	napravovat	k5eAaImIp3nS	napravovat
nebo	nebo	k8xC	nebo
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
distributivní	distributivní	k2eAgFnSc1d1	distributivní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
trestů	trest	k1gInPc2	trest
a	a	k8xC	a
odměn	odměna	k1gFnPc2	odměna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
směny	směna	k1gFnSc2	směna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zprostředkují	zprostředkovat	k5eAaPmIp3nP	zprostředkovat
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc4	peníz
činí	činit	k5eAaImIp3nS	činit
věci	věc	k1gFnSc3	věc
souměřitelnými	souměřitelný	k2eAgInPc7d1	souměřitelný
a	a	k8xC	a
zaručují	zaručovat	k5eAaImIp3nP	zaručovat
budoucí	budoucí	k2eAgFnSc4d1	budoucí
směnu	směna	k1gFnSc4	směna
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
teď	teď	k6eAd1	teď
nic	nic	k3yNnSc1	nic
nepotřebujeme	potřebovat	k5eNaImIp1nP	potřebovat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1134	[number]	k4	1134
<g/>
)	)	kIx)	)
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
řízena	řízen	k2eAgFnSc1d1	řízena
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
proto	proto	k8xC	proto
nenecháváme	nechávat	k5eNaImIp1nP	nechávat
vládnout	vládnout	k5eAaImF	vládnout
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1134	[number]	k4	1134
<g/>
a	a	k8xC	a
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
Náležitost	náležitost	k1gFnSc1	náležitost
<g/>
,	,	kIx,	,
slušnost	slušnost	k1gFnSc1	slušnost
(	(	kIx(	(
<g/>
EPIEIKEIA	EPIEIKEIA	kA	EPIEIKEIA
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
fairness	fairness	k1gInSc1	fairness
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
totéž	týž	k3xTgNnSc1	týž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spravedlivá	spravedlivý	k2eAgFnSc1d1	spravedlivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
náprava	náprava	k1gFnSc1	náprava
zákonné	zákonný	k2eAgFnSc2d1	zákonná
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
totiž	totiž	k9	totiž
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
jednání	jednání	k1gNnSc1	jednání
je	být	k5eAaImIp3nS	být
nepravidelné	pravidelný	k2eNgNnSc1d1	nepravidelné
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
některé	některý	k3yIgInPc1	některý
případy	případ	k1gInPc1	případ
nelze	lze	k6eNd1	lze
spravedlivě	spravedlivě	k6eAd1	spravedlivě
posoudit	posoudit	k5eAaPmF	posoudit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1137	[number]	k4	1137
<g/>
)	)	kIx)	)
A.	A.	kA	A.
MacIntyre	MacIntyr	k1gInSc5	MacIntyr
<g/>
,	,	kIx,	,
Ztráta	ztráta	k1gFnSc1	ztráta
ctnosti	ctnost	k1gFnSc2	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
(	(	kIx(	(
<g/>
zóon	zóon	k1gNnSc1	zóon
politikon	politikona	k1gFnPc2	politikona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
tedy	tedy	k8xC	tedy
žít	žít	k5eAaImF	žít
sám	sám	k3xTgMnSc1	sám
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
soběstačnosti	soběstačnost	k1gFnSc6	soběstačnost
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
částí	část	k1gFnSc7	část
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
buď	buď	k8xC	buď
divoké	divoký	k2eAgNnSc1d1	divoké
zvíře	zvíře	k1gNnSc1	zvíře
nebo	nebo	k8xC	nebo
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pol	pola	k1gFnPc2	pola
1253	[number]	k4	1253
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Aristotelovo	Aristotelův	k2eAgNnSc1d1	Aristotelovo
myšlení	myšlení	k1gNnSc1	myšlení
není	být	k5eNaImIp3nS	být
evoluční	evoluční	k2eAgNnSc1d1	evoluční
ani	ani	k8xC	ani
historické	historický	k2eAgNnSc1d1	historické
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
systematické	systematický	k2eAgNnSc1d1	systematické
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
(	(	kIx(	(
<g/>
logicky	logicky	k6eAd1	logicky
<g/>
)	)	kIx)	)
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
obec	obec	k1gFnSc1	obec
sdružením	sdružení	k1gNnSc7	sdružení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
naopak	naopak	k6eAd1	naopak
člověk	člověk	k1gMnSc1	člověk
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
spolčují	spolčovat	k5eAaImIp3nP	spolčovat
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
:	:	kIx,	:
rodina	rodina	k1gFnSc1	rodina
nebo	nebo	k8xC	nebo
lépe	dobře	k6eAd2	dobře
hospodářství	hospodářství	k1gNnSc1	hospodářství
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
oikia	oikia	k1gFnSc1	oikia
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
obživu	obživa	k1gFnSc4	obživa
a	a	k8xC	a
reprodukci	reprodukce	k1gFnSc4	reprodukce
<g/>
;	;	kIx,	;
obec	obec	k1gFnSc1	obec
nebo	nebo	k8xC	nebo
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
polis	polis	k1gFnSc1	polis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soběstačný	soběstačný	k2eAgMnSc1d1	soběstačný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
účelem	účel	k1gInSc7	účel
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
lidského	lidský	k2eAgNnSc2d1	lidské
spolužití	spolužití	k1gNnSc2	spolužití
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
soběstačné	soběstačný	k2eAgNnSc4d1	soběstačné
společenství	společenství	k1gNnSc4	společenství
rovných	rovný	k2eAgInPc2d1	rovný
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
co	co	k8xS	co
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1328	[number]	k4	1328
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
společenství	společenství	k1gNnSc4	společenství
dobrého	dobrý	k2eAgInSc2d1	dobrý
života	život	k1gInSc2	život
v	v	k7c6	v
domech	dům	k1gInPc6	dům
a	a	k8xC	a
rodinách	rodina	k1gFnPc6	rodina
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
a	a	k8xC	a
soběstačný	soběstačný	k2eAgInSc1d1	soběstačný
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1280	[number]	k4	1280
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vládne	vládnout	k5eAaImIp3nS	vládnout
nad	nad	k7c7	nad
sobě	se	k3xPyFc3	se
rovnými	rovný	k2eAgInPc7d1	rovný
a	a	k8xC	a
svobodnými	svobodný	k2eAgInPc7d1	svobodný
<g/>
;	;	kIx,	;
nazýváme	nazývat	k5eAaImIp1nP	nazývat
ji	on	k3xPp3gFnSc4	on
politickou	politický	k2eAgFnSc4d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
dobře	dobře	k6eAd1	dobře
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
nenaučil	naučit	k5eNaPmAgMnS	naučit
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1277	[number]	k4	1277
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Základem	základ	k1gInSc7	základ
života	život	k1gInSc2	život
v	v	k7c6	v
míru	mír	k1gInSc6	mír
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
majetku	majetek	k1gInSc2	majetek
<g/>
;	;	kIx,	;
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
odmítá	odmítat	k5eAaImIp3nS	odmítat
Platónův	Platónův	k2eAgInSc1d1	Platónův
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
lidé	člověk	k1gMnPc1	člověk
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
všechno	všechen	k3xTgNnSc4	všechen
společné	společný	k2eAgNnSc4d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
peníze	peníz	k1gInPc1	peníz
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
takříkajíc	takříkajíc	k6eAd1	takříkajíc
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
nějak	nějak	k6eAd1	nějak
rádi	rád	k2eAgMnPc1d1	rád
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1263	[number]	k4	1263
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varuje	varovat	k5eAaImIp3nS	varovat
však	však	k9	však
před	před	k7c7	před
každou	každý	k3xTgFnSc7	každý
hamižností	hamižnost	k1gFnSc7	hamižnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obec	obec	k1gFnSc1	obec
ničí	ničit	k5eAaImIp3nS	ničit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Výsady	výsada	k1gFnPc1	výsada
zámožných	zámožný	k2eAgFnPc2d1	zámožná
ničí	ničit	k5eAaImIp3nS	ničit
ústavu	ústav	k1gInSc2	ústav
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
výsady	výsada	k1gFnPc4	výsada
lidu	lid	k1gInSc2	lid
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1297	[number]	k4	1297
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Otroky	otrok	k1gMnPc4	otrok
pokládá	pokládat	k5eAaImIp3nS	pokládat
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
za	za	k7c4	za
nutnou	nutný	k2eAgFnSc4d1	nutná
součást	součást	k1gFnSc4	součást
každého	každý	k3xTgNnSc2	každý
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
kdyby	kdyby	k9	kdyby
tkalcovský	tkalcovský	k2eAgInSc4d1	tkalcovský
člunek	člunek	k1gInSc4	člunek
sám	sám	k3xTgInSc4	sám
tkal	tkát	k5eAaImAgInS	tkát
<g/>
,	,	kIx,	,
...	...	k?	...
nebyli	být	k5eNaImAgMnP	být
by	by	kYmCp3nP	by
třeba	třeba	k6eAd1	třeba
otroci	otrok	k1gMnPc1	otrok
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1253	[number]	k4	1253
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
O	o	k7c6	o
oprávněnosti	oprávněnost	k1gFnSc6	oprávněnost
otroctví	otroctví	k1gNnSc2	otroctví
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
různá	různý	k2eAgNnPc1d1	různé
mínění	mínění	k1gNnPc1	mínění
a	a	k8xC	a
podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišit	odlišit	k5eAaPmF	odlišit
otroky	otrok	k1gMnPc4	otrok
"	"	kIx"	"
<g/>
přirozené	přirozený	k2eAgFnPc4d1	přirozená
<g/>
"	"	kIx"	"
a	a	k8xC	a
zotročené	zotročený	k2eAgFnPc1d1	zotročená
násilně	násilně	k6eAd1	násilně
<g/>
.	.	kIx.	.
</s>
<s>
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dovede	dovést	k5eAaPmIp3nS	dovést
rozkazovat	rozkazovat	k5eAaImF	rozkazovat
i	i	k8xC	i
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
otrok	otrok	k1gMnSc1	otrok
umí	umět	k5eAaImIp3nS	umět
jen	jen	k6eAd1	jen
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
dává	dávat	k5eAaImIp3nS	dávat
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
svobodou	svoboda	k1gFnSc7	svoboda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
právem	právem	k6eAd1	právem
otrok	otrok	k1gMnSc1	otrok
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vláda	vláda	k1gFnSc1	vláda
hospodáře	hospodář	k1gMnSc2	hospodář
nad	nad	k7c4	nad
otroky	otrok	k1gMnPc4	otrok
je	být	k5eAaImIp3nS	být
však	však	k9	však
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
sobě	se	k3xPyFc3	se
rovnými	rovný	k2eAgFnPc7d1	rovná
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
svobodní	svobodný	k2eAgMnPc1d1	svobodný
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jednou	jednou	k6eAd1	jednou
rozkazují	rozkazovat	k5eAaImIp3nP	rozkazovat
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vládne	vládnout	k5eAaImIp3nS	vládnout
<g/>
,	,	kIx,	,
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
tři	tři	k4xCgFnPc1	tři
právní	právní	k2eAgFnPc1d1	právní
formy	forma	k1gFnPc1	forma
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládne	vládnout	k5eAaImIp3nS	vládnout
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
všech	všecek	k3xTgMnPc2	všecek
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
aristokracie	aristokracie	k1gFnPc1	aristokracie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládnou	vládnout	k5eAaImIp3nP	vládnout
ti	ten	k3xDgMnPc1	ten
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
aristoi	aristoi	k1gNnSc1	aristoi
<g/>
)	)	kIx)	)
politeia	politeia	k1gFnSc1	politeia
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
podílejí	podílet	k5eAaImIp3nP	podílet
všichni	všechen	k3xTgMnPc1	všechen
svobodní	svobodný	k2eAgMnPc1d1	svobodný
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
pokleslé	pokleslý	k2eAgFnPc1d1	pokleslá
formy	forma	k1gFnPc1	forma
<g/>
:	:	kIx,	:
tyranie	tyranie	k1gFnSc1	tyranie
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
samozvaného	samozvaný	k2eAgMnSc2d1	samozvaný
vládce	vládce	k1gMnSc2	vládce
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgNnSc2	samý
<g/>
,	,	kIx,	,
oligarchie	oligarchie	k1gFnSc1	oligarchie
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
bohatých	bohatý	k2eAgMnPc2d1	bohatý
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Demokracie	demokracie	k1gFnSc1	demokracie
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládne	vládnout	k5eAaImIp3nS	vládnout
většina	většina	k1gFnSc1	většina
nemajetných	majetný	k2eNgFnPc2d1	nemajetná
a	a	k8xC	a
svobodných	svobodný	k2eAgFnPc2d1	svobodná
<g/>
,	,	kIx,	,
oligarchie	oligarchie	k1gFnPc1	oligarchie
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládne	vládnout	k5eAaImIp3nS	vládnout
menšina	menšina	k1gFnSc1	menšina
bohatých	bohatý	k2eAgMnPc2d1	bohatý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Demokracie	demokracie	k1gFnSc1	demokracie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
domněnky	domněnka	k1gFnSc2	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
rovní	roveň	k1gFnSc7	roveň
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
svobodou	svoboda	k1gFnSc7	svoboda
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovni	roveň	k1gMnPc1	roveň
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
oligarchie	oligarchie	k1gFnSc2	oligarchie
naopak	naopak	k6eAd1	naopak
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jsou	být	k5eAaImIp3nP	být
nerovní	rovný	k2eNgMnPc1d1	nerovný
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
nejsou	být	k5eNaImIp3nP	být
rovní	rovný	k2eAgMnPc1d1	rovný
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1301	[number]	k4	1301
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
právní	právní	k2eAgFnSc4d1	právní
formu	forma	k1gFnSc4	forma
státu	stát	k1gInSc2	stát
považoval	považovat	k5eAaImAgInS	považovat
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vládu	vláda	k1gFnSc4	vláda
spravedlivého	spravedlivý	k2eAgMnSc2d1	spravedlivý
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
dále	daleko	k6eAd2	daleko
považoval	považovat	k5eAaImAgMnS	považovat
aristokracii	aristokracie	k1gFnSc4	aristokracie
(	(	kIx(	(
<g/>
vládu	vláda	k1gFnSc4	vláda
"	"	kIx"	"
<g/>
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
méně	málo	k6eAd2	málo
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
považoval	považovat	k5eAaImAgMnS	považovat
politeu	politea	k1gFnSc4	politea
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
mnoha	mnoho	k4c3	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Politeia	Politeia	k1gFnSc1	Politeia
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spíš	spíš	k9	spíš
oligarchická	oligarchický	k2eAgFnSc1d1	oligarchická
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
trestá	trestat	k5eAaImIp3nS	trestat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
demokratická	demokratický	k2eAgFnSc1d1	demokratická
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
chudým	chudý	k1gMnSc7	chudý
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
zasedání	zasedání	k1gNnSc2	zasedání
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1293	[number]	k4	1293
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ústavě	ústava	k1gFnSc6	ústava
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zákony	zákon	k1gInPc1	zákon
zaručily	zaručit	k5eAaPmAgInP	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
nemůže	moct	k5eNaImIp3nS	moct
obohatit	obohatit	k5eAaPmF	obohatit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1308	[number]	k4	1308
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
Za	za	k7c4	za
naprosto	naprosto	k6eAd1	naprosto
nepřijatelnou	přijatelný	k2eNgFnSc4d1	nepřijatelná
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
považoval	považovat	k5eAaImAgMnS	považovat
tyranii	tyranie	k1gFnSc4	tyranie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vládu	vláda	k1gFnSc4	vláda
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
oligarchii	oligarchie	k1gFnSc4	oligarchie
(	(	kIx(	(
<g/>
vládu	vláda	k1gFnSc4	vláda
bohatých	bohatý	k2eAgMnPc2d1	bohatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
tyranie	tyranie	k1gFnSc1	tyranie
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vládu	vláda	k1gFnSc4	vláda
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgNnSc4d3	nejmenší
zlo	zlo	k1gNnSc4	zlo
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
tří	tři	k4xCgMnPc2	tři
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
odděloval	oddělovat	k5eAaImAgMnS	oddělovat
politiku	politika	k1gFnSc4	politika
od	od	k7c2	od
etiky	etika	k1gFnSc2	etika
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
hledat	hledat	k5eAaImF	hledat
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgFnPc2d1	různá
ctností	ctnost	k1gFnPc2	ctnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
čili	čili	k1gNnSc2	čili
"	"	kIx"	"
<g/>
dobro	dobro	k1gNnSc4	dobro
těch	ten	k3xDgInPc2	ten
druhých	druhý	k4xOgInPc2	druhý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
určitými	určitý	k2eAgNnPc7d1	určité
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
formou	forma	k1gFnSc7	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
politickém	politický	k2eAgNnSc6d1	politické
zřízení	zřízení	k1gNnSc6	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
G.	G.	kA	G.
Mulgan	Mulgan	k1gMnSc1	Mulgan
<g/>
,	,	kIx,	,
Aristotelova	Aristotelův	k2eAgFnSc1d1	Aristotelova
politická	politický	k2eAgFnSc1d1	politická
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
Tóth	Tóth	k1gMnSc1	Tóth
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
Krno	Krno	k1gNnSc1	Krno
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
Kulašik	Kulašik	k1gInSc1	Kulašik
<g/>
:	:	kIx,	:
Stručný	stručný	k2eAgInSc1d1	stručný
politologický	politologický	k2eAgInSc1d1	politologický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
UNIAPRESS	UNIAPRESS	kA	UNIAPRESS
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85313-18-9	[number]	k4	80-85313-18-9
Filosofie	filosofie	k1gFnSc1	filosofie
Politologie	politologie	k1gFnSc2	politologie
Aristotelovo	Aristotelův	k2eAgNnSc4d1	Aristotelovo
pojetí	pojetí	k1gNnSc4	pojetí
demokracie	demokracie	k1gFnSc2	demokracie
Demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
Njája	Njája	k1gMnSc1	Njája
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
(	(	kIx(	(
<g/>
kráter	kráter	k1gInSc1	kráter
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Autor	autor	k1gMnSc1	autor
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Osoba	osoba	k1gFnSc1	osoba
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc1	galerie
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Aristotelovy	Aristotelův	k2eAgFnPc4d1	Aristotelova
<g />
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
-	-	kIx~	-
O	o	k7c6	o
duši	duše	k1gFnSc6	duše
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Etika	etika	k1gFnSc1	etika
Nikomachova	Nikomachův	k2eAgFnSc1d1	Nikomachova
(	(	kIx(	(
<g/>
celá	celý	k2eAgFnSc1d1	celá
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stanford	Stanford	k1gInSc1	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Philosophy	Philosopha	k1gMnSc2	Philosopha
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Heslo	heslo	k1gNnSc1	heslo
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
v	v	k7c6	v
Philosophen-Lexikon	Philosophen-Lexikon	k1gMnSc1	Philosophen-Lexikon
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Eisler	Eisler	k1gMnSc1	Eisler
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
Aristotelovi	Aristoteles	k1gMnSc6	Aristoteles
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aristotelovy	Aristotelův	k2eAgInPc4d1	Aristotelův
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Nikomachische	Nikomachische	k1gFnSc1	Nikomachische
Ethik	Ethik	k1gInSc4	Ethik
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
</s>
