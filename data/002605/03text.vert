<s>
Aphex	Aphex	k1gInSc1	Aphex
Twin	Twina	k1gFnPc2	Twina
<g/>
,	,	kIx,	,
pravým	pravý	k2eAgNnSc7d1	pravé
jménem	jméno	k1gNnSc7	jméno
Richard	Richard	k1gMnSc1	Richard
David	David	k1gMnSc1	David
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Limerick	Limerick	k1gInSc1	Limerick
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
průkopník	průkopník	k1gMnSc1	průkopník
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc2d1	tvořící
především	především	k9	především
v	v	k7c6	v
žánrech	žánr	k1gInPc6	žánr
techno	techno	k6eAd1	techno
<g/>
,	,	kIx,	,
ambient	ambient	k1gMnSc1	ambient
<g/>
,	,	kIx,	,
acid	acid	k1gMnSc1	acid
<g/>
,	,	kIx,	,
IDM	IDM	kA	IDM
a	a	k8xC	a
drill	drill	k1gMnSc1	drill
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
bass	bass	k6eAd1	bass
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
Limericku	Limerick	k1gInSc6	Limerick
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
Lorna	Lorna	k1gFnSc1	Lorna
a	a	k8xC	a
Derek	Derek	k6eAd1	Derek
Jamesovi	Jamesův	k2eAgMnPc1d1	Jamesův
<g/>
,	,	kIx,	,
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Welsu	Wels	k1gInSc2	Wels
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Cornwall	Cornwall	k1gInSc1	Cornwall
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožíval	prožívat	k5eAaImAgMnS	prožívat
šťastné	šťastný	k2eAgNnSc4d1	šťastné
dětství	dětství	k1gNnSc4	dětství
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dvěma	dva	k4xCgFnPc7	dva
sestrami	sestra	k1gFnPc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
provozovat	provozovat	k5eAaImF	provozovat
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
teenager	teenager	k1gMnSc1	teenager
dělal	dělat	k5eAaImAgMnS	dělat
DJe	DJe	k1gFnSc4	DJe
v	v	k7c6	v
St.	st.	kA	st.
<g/>
Ives	Ives	k1gInSc1	Ives
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
nahrávka	nahrávka	k1gFnSc1	nahrávka
měla	mít	k5eAaImAgFnS	mít
12	[number]	k4	12
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
se	se	k3xPyFc4	se
EP	EP	kA	EP
Analogue	Analogue	k1gNnSc4	Analogue
Bubblebath	Bubblebath	k1gInSc1	Bubblebath
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
James	Jamesa	k1gFnPc2	Jamesa
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Rephlex	Rephlex	k1gInSc1	Rephlex
Records	Records	k1gInSc1	Records
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
kamarádem	kamarád	k1gMnSc7	kamarád
Grantem	grant	k1gInSc7	grant
Wilsonem	Wilson	k1gInSc7	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Rephlex	Rephlex	k1gInSc1	Rephlex
Records	Recordsa	k1gFnPc2	Recordsa
tak	tak	k9	tak
začalo	začít	k5eAaPmAgNnS	začít
podporovat	podporovat	k5eAaImF	podporovat
britský	britský	k2eAgInSc4d1	britský
Acid	Acid	k1gInSc4	Acid
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Selected	Selected	k1gInSc1	Selected
Ambient	Ambient	k1gInSc1	Ambient
Works	Works	kA	Works
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
na	na	k7c6	na
R	R	kA	R
<g/>
&	&	k?	&
<g/>
S	s	k7c7	s
Records	Records	k1gInSc1	Records
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
kritiky	kritika	k1gFnSc2	kritika
označováno	označován	k2eAgNnSc4d1	označováno
jako	jako	k8xS	jako
průlomové	průlomový	k2eAgNnSc4d1	průlomové
ambientové	ambientový	k2eAgNnSc4d1	ambientový
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaImF	vydávat
svá	svůj	k3xOyFgNnPc4	svůj
alba	album	k1gNnPc4	album
na	na	k7c6	na
kultovním	kultovní	k2eAgInSc6d1	kultovní
Warp	Warp	k1gInSc1	Warp
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
album	album	k1gNnSc1	album
Selected	Selected	k1gInSc1	Selected
Ambient	Ambient	k1gMnSc1	Ambient
Works	Works	kA	Works
Volume	volum	k1gInSc5	volum
II	II	kA	II
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vydané	vydaný	k2eAgFnPc4d1	vydaná
na	na	k7c6	na
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
Warp	Warp	k1gInSc1	Warp
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
I	i	k9	i
Care	car	k1gMnSc5	car
Because	Becaus	k1gInSc6	Becaus
You	You	k1gFnPc1	You
Do	do	k7c2	do
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
Richard	Richard	k1gMnSc1	Richard
D.	D.	kA	D.
James	James	k1gMnSc1	James
svůj	svůj	k3xOyFgInSc4	svůj
obličej	obličej	k1gInSc4	obličej
na	na	k7c4	na
obal	obal	k1gInSc4	obal
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Care	car	k1gMnSc5	car
Because	Becaus	k1gInSc6	Becaus
You	You	k1gMnSc1	You
Do	do	k7c2	do
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
analogový	analogový	k2eAgInSc1d1	analogový
syntetizér	syntetizér	k1gInSc1	syntetizér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
stává	stávat	k5eAaImIp3nS	stávat
populárnější	populární	k2eAgFnSc1d2	populárnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
dvěma	dva	k4xCgInPc3	dva
singlům	singl	k1gInPc3	singl
-	-	kIx~	-
Come	Com	k1gFnSc2	Com
to	ten	k3xDgNnSc1	ten
Daddy	Dadda	k1gFnPc4	Dadda
a	a	k8xC	a
Windowlicker	Windowlicker	k1gInSc4	Windowlicker
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
pouštěny	pouštět	k5eAaImNgInP	pouštět
na	na	k7c4	na
MTV	MTV	kA	MTV
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
covery	cover	k1gInPc1	cover
se	se	k3xPyFc4	se
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Videa	video	k1gNnPc1	video
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
singly	singl	k1gInPc4	singl
natočil	natočit	k5eAaBmAgMnS	natočit
kultovní	kultovní	k2eAgMnSc1d1	kultovní
režisér	režisér	k1gMnSc1	režisér
Chris	Chris	k1gFnSc2	Chris
Cunningham	Cunningham	k1gInSc1	Cunningham
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vydal	vydat	k5eAaPmAgInS	vydat
Aphex	Aphex	k1gInSc1	Aphex
Twin	Twin	k1gMnSc1	Twin
desku	deska	k1gFnSc4	deska
drukqs	drukqsa	k1gFnPc2	drukqsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
disky	disk	k1gInPc4	disk
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
pianovými	pianový	k2eAgFnPc7d1	pianová
skladbami	skladba	k1gFnPc7	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
Richard	Richard	k1gMnSc1	Richard
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
acid	acid	k6eAd1	acid
technu	technout	k5eAaPmIp1nS	technout
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Analord	Analorda	k1gFnPc2	Analorda
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
,	,	kIx,	,
výběrem	výběr	k1gInSc7	výběr
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
deska	deska	k1gFnSc1	deska
Chosen	Chosna	k1gFnPc2	Chosna
lords	lords	k6eAd1	lords
<g/>
.	.	kIx.	.
</s>
<s>
Selected	Selected	k1gMnSc1	Selected
Ambient	Ambient	k1gMnSc1	Ambient
Works	Works	kA	Works
85-92	[number]	k4	85-92
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Selected	Selected	k1gMnSc1	Selected
Ambient	Ambient	k1gMnSc1	Ambient
Works	Works	kA	Works
Volume	volum	k1gInSc5	volum
II	II	kA	II
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
...	...	k?	...
<g/>
I	i	k9	i
Care	car	k1gMnSc5	car
Because	Becaus	k1gInSc6	Becaus
You	You	k1gFnPc1	You
Do	do	k7c2	do
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Melodies	Melodies	k1gMnSc1	Melodies
from	from	k1gMnSc1	from
Mars	Mars	k1gMnSc1	Mars
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
nevydáno	vydat	k5eNaPmNgNnS	vydat
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
D.	D.	kA	D.
James	James	k1gInSc1	James
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Drukqs	Drukqs	k1gInSc1	Drukqs
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Syro	syro	k6eAd1	syro
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Digeridoo	Digeridoo	k6eAd1	Digeridoo
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Xylem	Xylem	k1gInSc1	Xylem
Tube	tubus	k1gInSc5	tubus
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
On	on	k3xPp3gMnSc1	on
<g/>
/	/	kIx~	/
<g/>
On	on	k3xPp3gMnSc1	on
Remixes	Remixes	k1gMnSc1	Remixes
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Ventolin	Ventolin	k1gInSc1	Ventolin
<g/>
/	/	kIx~	/
<g/>
Ventolin	Ventolin	k2eAgInSc1d1	Ventolin
Remixes	Remixes	k1gInSc1	Remixes
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Donkey	Donkea	k1gFnSc2	Donkea
Rhubarb	Rhubarb	k1gMnSc1	Rhubarb
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Girl	girl	k1gFnSc2	girl
<g/>
/	/	kIx~	/
<g/>
Boy	boa	k1gFnSc2	boa
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Come	Come	k1gFnSc4	Come
to	ten	k3xDgNnSc1	ten
Daddy	Dadda	k1gMnSc2	Dadda
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Windowlicker	Windowlickra	k1gFnPc2	Windowlickra
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Analord	Analord	k1gMnSc1	Analord
série	série	k1gFnSc2	série
Cheetah	Cheetah	k1gMnSc1	Cheetah
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
En	En	k1gFnSc1	En
Trance	Tranec	k1gInSc2	Tranec
To	to	k9	to
Exit	exit	k1gInSc1	exit
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Words	Words	k1gInSc1	Words
&	&	k?	&
<g />
.	.	kIx.	.
</s>
<s>
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Aphex	Aphex	k1gInSc1	Aphex
Airlines	Airlines	k1gInSc1	Airlines
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Classics	Classicsa	k1gFnPc2	Classicsa
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
51	[number]	k4	51
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
Aphex	Aphex	k1gInSc1	Aphex
Singles	Singles	k1gMnSc1	Singles
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Cock	Cock	k1gInSc1	Cock
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
Cymru	Cymr	k1gInSc2	Cymr
beats	beats	k1gInSc1	beats
(	(	kIx(	(
<g/>
drukqs	drukqs	k6eAd1	drukqs
promo	promo	k6eAd1	promo
<g/>
)	)	kIx)	)
26	[number]	k4	26
Mixes	Mixes	k1gInSc1	Mixes
for	forum	k1gNnPc2	forum
Cash	cash	k1gFnSc2	cash
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
2	[number]	k4	2
Mixes	Mixes	k1gMnSc1	Mixes
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
12	[number]	k4	12
<g/>
"	"	kIx"	"
for	forum	k1gNnPc2	forum
Cash	cash	k1gFnPc2	cash
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Falling	Falling	k1gInSc1	Falling
Free	Fre	k1gInSc2	Fre
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Analogue	Analogu	k1gInSc2	Analogu
Bubblebath	Bubblebath	k1gInSc1	Bubblebath
Analogue	Analogue	k1gInSc1	Analogue
Bubblebath	Bubblebath	k1gInSc1	Bubblebath
2	[number]	k4	2
Analogue	Analogue	k1gNnSc7	Analogue
Bubblebath	Bubblebatha	k1gFnPc2	Bubblebatha
3	[number]	k4	3
Analogue	Analogue	k1gFnSc4	Analogue
Bubblebath	Bubblebatha	k1gFnPc2	Bubblebatha
4	[number]	k4	4
Analogue	Analogue	k1gFnSc4	Analogue
Bubblebath	Bubblebatha	k1gFnPc2	Bubblebatha
5	[number]	k4	5
Analogue	Analogue	k1gFnSc4	Analogue
Bubblebath	Bubblebatha	k1gFnPc2	Bubblebatha
3.1	[number]	k4	3.1
Hangable	Hangable	k1gFnSc6	Hangable
Auto	auto	k1gNnSc4	auto
Bulb	Bulba	k1gFnPc2	Bulba
2	[number]	k4	2
Remixes	Remixesa	k1gFnPc2	Remixesa
By	by	k9	by
AFX	AFX	kA	AFX
<g />
.	.	kIx.	.
</s>
<s>
Smojphace	Smojphace	k1gFnSc1	Smojphace
Analord	Analord	k1gInSc1	Analord
AFX	AFX	kA	AFX
<g/>
/	/	kIx~	/
<g/>
LFO	LFO	kA	LFO
Chosen	Chosna	k1gFnPc2	Chosna
Lords	Lordsa	k1gFnPc2	Lordsa
Confederation	Confederation	k1gInSc1	Confederation
Trough	Trough	k1gMnSc1	Trough
EP	EP	kA	EP
Rushup	Rushup	k1gMnSc1	Rushup
Edge	Edg	k1gMnSc2	Edg
LP	LP	kA	LP
Bradley	Bradlea	k1gMnSc2	Bradlea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Beat	beat	k1gInSc1	beat
Bradley	Bradley	k1gInPc1	Bradley
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Robot	robot	k1gMnSc1	robot
Joyrex	Joyrex	k1gInSc4	Joyrex
J4	J4	k1gMnSc3	J4
EP	EP	kA	EP
Joyrex	Joyrex	k1gInSc1	Joyrex
J5	J5	k1gMnPc2	J5
EP	EP	kA	EP
Joyrex	Joyrex	k1gInSc4	Joyrex
J	J	kA	J
<g/>
9	[number]	k4	9
<g/>
i	i	k8xC	i
Joyrex	Joyrex	k1gInSc1	Joyrex
J	J	kA	J
<g/>
9	[number]	k4	9
<g/>
ii	ii	k?	ii
(	(	kIx(	(
<g/>
untitled	untitled	k1gMnSc1	untitled
<g/>
)	)	kIx)	)
Compilation	Compilation	k1gInSc1	Compilation
Gak	Gak	k1gFnSc2	Gak
Single	singl	k1gInSc5	singl
Red	Red	k1gMnSc7	Red
Green	Grena	k1gFnPc2	Grena
Innovation	Innovation	k1gInSc1	Innovation
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Dynamics	Dynamicsa	k1gFnPc2	Dynamicsa
Of	Of	k1gMnSc1	Of
Acid	Acid	k1gMnSc1	Acid
Surfing	surfing	k1gInSc4	surfing
on	on	k3xPp3gMnSc1	on
Sine	sinus	k1gInSc5	sinus
Waves	Wavesa	k1gFnPc2	Wavesa
Quoth	Quoth	k1gInSc1	Quoth
Pac-Man	Pac-Man	k1gMnSc1	Pac-Man
Q-Chastic	Q-Chastice	k1gFnPc2	Q-Chastice
EP	EP	kA	EP
Expert	expert	k1gMnSc1	expert
Knob	Knob	k1gMnSc1	Knob
Twiddlers	Twiddlersa	k1gFnPc2	Twiddlersa
Richard	Richard	k1gMnSc1	Richard
vlastní	vlastní	k2eAgInSc4d1	vlastní
tank	tank	k1gInSc4	tank
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc4d1	ruská
ponorku	ponorka	k1gFnSc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
přestavěné	přestavěný	k2eAgFnSc6d1	přestavěná
bance	banka	k1gFnSc6	banka
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
je	být	k5eAaImIp3nS	být
očividně	očividně	k6eAd1	očividně
zkušený	zkušený	k2eAgMnSc1d1	zkušený
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
blízcí	blízký	k2eAgMnPc1d1	blízký
přátelé	přítel	k1gMnPc1	přítel
vypověděli	vypovědět	k5eAaPmAgMnP	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svoje	svůj	k3xOyFgMnPc4	svůj
syntetizéry	syntetizér	k1gMnPc4	syntetizér
a	a	k8xC	a
samplery	sampler	k1gMnPc4	sampler
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
dělal	dělat	k5eAaImAgMnS	dělat
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
ještě	ještě	k6eAd1	ještě
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
si	se	k3xPyFc3	se
upravil	upravit	k5eAaPmAgMnS	upravit
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
syntetizuje	syntetizovat	k5eAaImIp3nS	syntetizovat
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
skladeb	skladba	k1gFnPc2	skladba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvuky	zvuk	k1gInPc4	zvuk
z	z	k7c2	z
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
<g/>
.	.	kIx.	.
</s>
<s>
Třeba	třeba	k6eAd1	třeba
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Carn	Carn	k1gMnSc1	Carn
Marth	Marth	k1gMnSc1	Marth
<g/>
"	"	kIx"	"
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvuky	zvuk	k1gInPc4	zvuk
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Sabre	Sabr	k1gInSc5	Sabr
Wulf	Wulf	k1gInSc1	Wulf
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
něco	něco	k3yInSc4	něco
kolem	kolem	k7c2	kolem
100	[number]	k4	100
hodin	hodina	k1gFnPc2	hodina
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
hudby	hudba	k1gFnSc2	hudba
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
software	software	k1gInSc4	software
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
umí	umět	k5eAaImIp3nP	umět
algoritmické	algoritmický	k2eAgInPc1d1	algoritmický
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
automaticky	automaticky	k6eAd1	automaticky
generují	generovat	k5eAaImIp3nP	generovat
beaty	beat	k1gInPc4	beat
a	a	k8xC	a
melodie	melodie	k1gFnPc4	melodie
<g/>
)	)	kIx)	)
Má	mít	k5eAaImIp3nS	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
se	s	k7c7	s
Synestézií	synestézie	k1gFnSc7	synestézie
a	a	k8xC	a
lucidním	lucidní	k2eAgNnSc7d1	lucidní
sněním	snění	k1gNnSc7	snění
<g/>
.	.	kIx.	.
</s>
