<s>
Aphex	Aphex	k1gInSc1	Aphex
Twin	Twina	k1gFnPc2	Twina
<g/>
,	,	kIx,	,
pravým	pravý	k2eAgNnSc7d1	pravé
jménem	jméno	k1gNnSc7	jméno
Richard	Richard	k1gMnSc1	Richard
David	David	k1gMnSc1	David
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Limerick	Limerick	k1gInSc1	Limerick
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
průkopník	průkopník	k1gMnSc1	průkopník
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc2d1	tvořící
především	především	k9	především
v	v	k7c6	v
žánrech	žánr	k1gInPc6	žánr
techno	techno	k6eAd1	techno
<g/>
,	,	kIx,	,
ambient	ambient	k1gMnSc1	ambient
<g/>
,	,	kIx,	,
acid	acid	k1gMnSc1	acid
<g/>
,	,	kIx,	,
IDM	IDM	kA	IDM
a	a	k8xC	a
drill	drill	k1gMnSc1	drill
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
bass	bass	k6eAd1	bass
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
Limericku	Limerick	k1gInSc6	Limerick
<g/>
.	.	kIx.	.
</s>
