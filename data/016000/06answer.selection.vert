<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
mikroregionu	mikroregion	k1gInSc2
Záhoran	Záhorana	k1gFnPc2
je	být	k5eAaImIp3nS
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
okresu	okres	k1gInSc6
Přerov	Přerov	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Rouské	Rouské	k1gNnSc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
rozvoje	rozvoj	k1gInSc2
obcí	obec	k1gFnPc2
/	/	kIx~
<g/>
cestovní	cestovní	k2eAgInSc4d1
ruch	ruch	k1gInSc4
<g/>
,	,	kIx,
územní	územní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
<g/>
,	,	kIx,
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
</s>