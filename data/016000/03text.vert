<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
mikroregionu	mikroregion	k1gInSc2
Záhoran	Záhorana	k1gFnPc2
</s>
<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
mikroregionu	mikroregion	k1gInSc2
ZáhoranForma	ZáhoranFormum	k1gNnSc2
</s>
<s>
Svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Stržínková	Stržínkový	k2eAgFnSc1d1
Miluše	Miluše	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Rouské	Rouský	k2eAgNnSc1d1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2001	#num#	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Přerov	Přerov	k1gInSc1
Kontakt	kontakt	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
ourouske@tiscali.czpavla.kr	ourouske@tiscali.czpavla.kr	k1gMnSc1
<g/>
balkova@seznam.cz	balkova@seznam.cz	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
www.zahoran.cz	www.zahoran.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
mikroregionu	mikroregion	k1gInSc2
Záhoran	Záhorana	k1gFnPc2
je	být	k5eAaImIp3nS
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
okresu	okres	k1gInSc6
Přerov	Přerov	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Rouské	Rouské	k1gNnSc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
rozvoje	rozvoj	k1gInSc2
obcí	obec	k1gFnPc2
/	/	kIx~
<g/>
cestovní	cestovní	k2eAgInSc4d1
ruch	ruch	k1gInSc4
<g/>
,	,	kIx,
územní	územní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
<g/>
,	,	kIx,
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružuje	sdružovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
9	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
sdružené	sdružený	k2eAgFnPc1d1
v	v	k7c6
mikroregionu	mikroregion	k1gInSc6
</s>
<s>
Býškovice	Býškovice	k1gFnSc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
</s>
<s>
Malhotice	Malhotice	k1gFnSc1
</s>
<s>
Opatovice	Opatovice	k1gFnSc1
</s>
<s>
Paršovice	Paršovice	k1gFnSc1
</s>
<s>
Provodovice	Provodovice	k1gFnSc1
</s>
<s>
Rakov	Rakov	k1gInSc1
</s>
<s>
Rouské	Rouské	k2eAgFnSc1d1
</s>
<s>
Všechovice	Všechovice	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
mikroregionu	mikroregion	k1gInSc2
Záhoran	Záhorana	k1gFnPc2
na	na	k7c6
Regionálním	regionální	k2eAgInSc6d1
informačním	informační	k2eAgInSc6d1
servisu	servis	k1gInSc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc4d1
zdroj	zdroj	k1gInSc4
<g/>
]	]	kIx)
</s>
<s>
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
154095618	#num#	k4
</s>
