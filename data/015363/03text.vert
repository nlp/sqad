<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Japonska	Japonsko	k1gNnSc2
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
prefektury	prefektura	k1gFnPc1
očíslované	očíslovaný	k2eAgFnPc1d1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
normou	norma	k1gFnSc7
ISO	ISO	kA
3166	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
-JP	-JP	k?
a	a	k8xC
japonské	japonský	k2eAgInPc1d1
regiony	region	k1gInPc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
se	se	k3xPyFc4
administrativně	administrativně	k6eAd1
člení	členit	k5eAaImIp3nS
na	na	k7c4
47	#num#	k4
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
čtyř	čtyři	k4xCgInPc2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
nazývaných	nazývaný	k2eAgFnPc6d1
souhrnně	souhrnně	k6eAd1
todófuken	todófuken	k2eAgMnSc1d1
(	(	kIx(
<g/>
都	都	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
je	být	k5eAaImIp3nS
zvykem	zvyk	k1gInSc7
označovat	označovat	k5eAaImF
tyto	tento	k3xDgFnPc4
jednotky	jednotka	k1gFnPc4
jako	jako	k8xS,k8xC
prefektury	prefektura	k1gFnPc4
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
přesné	přesný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kořeny	kořen	k1gInPc1
současného	současný	k2eAgInSc2d1
systému	systém	k1gInSc2
prefektur	prefektura	k1gFnPc2
položila	položit	k5eAaPmAgFnS
někdejší	někdejší	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
období	období	k1gNnSc2
Meidži	Meidž	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
výnosem	výnos	k1gInSc7
zanikly	zaniknout	k5eAaPmAgInP
feudální	feudální	k2eAgInPc1d1
územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
han	hana	k1gFnPc2
(	(	kIx(
<g/>
藩	藩	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
transformovaly	transformovat	k5eAaBmAgFnP
na	na	k7c4
prefektury	prefektura	k1gFnPc4
<g/>
,	,	kIx,
často	často	k6eAd1
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prefektur	prefektura	k1gFnPc2
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
přes	přes	k7c4
300	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
koncem	koncem	k7c2
roku	rok	k1gInSc2
1871	#num#	k4
byl	být	k5eAaImAgInS
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
zredukován	zredukovat	k5eAaPmNgInS
na	na	k7c4
72	#num#	k4
a	a	k8xC
později	pozdě	k6eAd2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1888	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
dnešních	dnešní	k2eAgFnPc2d1
47	#num#	k4
prefektur	prefektura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Současný	současný	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Současný	současný	k2eAgInSc1d1
stav	stav	k1gInSc1
má	mít	k5eAaImIp3nS
právní	právní	k2eAgInSc1d1
základ	základ	k1gInSc1
v	v	k7c6
zákoně	zákon	k1gInSc6
o	o	k7c4
místní	místní	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dal	dát	k5eAaPmAgInS
prefekturám	prefektura	k1gFnPc3
více	hodně	k6eAd2
politické	politický	k2eAgFnSc3d1
moci	moc	k1gFnSc3
a	a	k8xC
zavedl	zavést	k5eAaPmAgMnS
prefekturní	prefekturní	k2eAgFnPc4d1
vlády	vláda	k1gFnPc4
a	a	k8xC
parlamenty	parlament	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
47	#num#	k4
administrativních	administrativní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
nejvyšší	vysoký	k2eAgFnSc2d3
úrovně	úroveň	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
metropolitní	metropolitní	k2eAgInSc4d1
město	město	k1gNnSc1
to	ten	k3xDgNnSc4
(	(	kIx(
<g/>
都	都	k?
<g/>
)	)	kIx)
…	…	k?
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
1	#num#	k4
správní	správní	k2eAgInSc1d1
okruh	okruh	k1gInSc1
dó	dó	k?
(	(	kIx(
<g/>
道	道	k?
<g/>
)	)	kIx)
…	…	k?
Hokkaidó	Hokkaidó	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
2	#num#	k4
městské	městský	k2eAgFnPc4d1
prefektury	prefektura	k1gFnPc4
fu	fu	k0
(	(	kIx(
<g/>
府	府	k?
<g/>
)	)	kIx)
…	…	k?
Ósaka	Ósaka	k1gFnSc1
a	a	k8xC
Kjóto	Kjóto	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
43	#num#	k4
prefektur	prefektura	k1gFnPc2
ken	ken	k?
(	(	kIx(
<g/>
県	県	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prefektury	prefektura	k1gFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
člení	členit	k5eAaImIp3nS
na	na	k7c4
města	město	k1gNnPc4
ši	ši	k?
(	(	kIx(
<g/>
市	市	k?
<g/>
)	)	kIx)
a	a	k8xC
okresy	okres	k1gInPc1
gun	gun	k?
(	(	kIx(
<g/>
郡	郡	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
prefektura	prefektura	k1gFnSc1
Hokkaidó	Hokkaidó	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
velikosti	velikost	k1gFnSc3
území	území	k1gNnSc2
<g/>
,	,	kIx,
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
14	#num#	k4
subprefektur	subprefektura	k1gFnPc2
šičó	šičó	k?
(	(	kIx(
<g/>
支	支	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresy	okres	k1gInPc7
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
města	město	k1gNnPc4
čó	čó	k?
(	(	kIx(
<g/>
町	町	k?
<g/>
)	)	kIx)
a	a	k8xC
obce	obec	k1gFnPc1
son	son	k1gInSc1
<g/>
(	(	kIx(
<g/>
村	村	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
</s>
<s>
Prefektury	prefektura	k1gFnPc1
se	se	k3xPyFc4
také	také	k9
často	často	k6eAd1
seskupují	seskupovat	k5eAaImIp3nP
do	do	k7c2
regionů	region	k1gInPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
takové	takový	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
nemá	mít	k5eNaImIp3nS
zatím	zatím	k6eAd1
právní	právní	k2eAgInSc4d1
podklad	podklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regionů	region	k1gInPc2
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
udáváno	udávat	k5eAaImNgNnS
8	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
počtem	počet	k1gInSc7
10	#num#	k4
regionů	region	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
získáme	získat	k5eAaPmIp1nP
<g/>
,	,	kIx,
budeme	být	k5eAaImBp1nP
<g/>
-li	-li	k?
uvažovat	uvažovat	k5eAaImF
jednotlivé	jednotlivý	k2eAgInPc1d1
podregiony	podregion	k1gInPc1
regionu	region	k1gInSc2
Čúbu	Čúbus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
navíc	navíc	k6eAd1
nejsou	být	k5eNaImIp3nP
exaktně	exaktně	k6eAd1
definovány	definovat	k5eAaBmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Hokkaidó	Hokkaidó	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hokkaidó	Hokkaidó	k1gFnSc1
</s>
<s>
Tóhoku	Tóhok	k1gInSc3
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aomori	Aomori	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iwate	Iwat	k1gMnSc5
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mijagi	Mijagi	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akita	Akit	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamagata	Jamagata	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fukušima	Fukušima	k1gNnSc1
</s>
<s>
Kantó	Kantó	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ibaraki	Ibaraki	k1gNnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Točigi	Točigi	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gunma	Gunma	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saitama	Saitama	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čiba	Čib	k1gInSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tokio	Tokio	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanagawa	Kanagaw	k1gInSc2
</s>
<s>
Čúbu	Čúbu	k6eAd1
</s>
<s>
Kóšinecu	Kóšinecu	k6eAd1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Niigata	Niigata	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamanaši	Jamanaš	k1gMnPc7
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nagano	Nagano	k1gNnSc1
</s>
<s>
Hokuriku	Hokurika	k1gFnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tojama	Tojama	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Išikawa	Išikaw	k1gInSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fukui	Fukui	k1gNnSc1
</s>
<s>
Tókai	Tókai	k6eAd1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gifu	Gifus	k1gInSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aiči	Aiči	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šizuoka	Šizuoek	k1gInSc2
</s>
<s>
Kansai	Kansai	k6eAd1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mie	Mie	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šiga	Šig	k1gInSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kjóto	Kjóto	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ósaka	Ósaka	k1gFnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hjógo	Hjógo	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nara	Nar	k1gInSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wakajama	Wakajama	k1gNnSc1
</s>
<s>
Čúgoku	Čúgok	k1gInSc3
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tottori	Tottori	k1gNnSc1
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šimane	Šiman	k1gMnSc5
</s>
<s>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okajama	Okajama	k1gNnSc1
</s>
<s>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hirošima	Hirošima	k1gFnSc1
</s>
<s>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamaguči	Jamaguč	k1gFnSc6
</s>
<s>
Šikoku	Šikok	k1gInSc3
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tokušima	Tokušima	k1gNnSc1
</s>
<s>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kagawa	Kagaw	k1gInSc2
</s>
<s>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ehime	Ehim	k1gMnSc5
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kóči	Kóči	k1gNnSc1
</s>
<s>
Kjúšú	Kjúšú	k?
a	a	k8xC
Okinawa	Okinawa	k1gFnSc1
</s>
<s>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fukuoka	Fukuoek	k1gInSc2
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saga	Sag	k1gInSc2
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nagasaki	Nagasaki	k1gNnSc1
</s>
<s>
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kumamoto	Kumamota	k1gFnSc5
</s>
<s>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Óita	Óit	k1gInSc2
</s>
<s>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mijazaki	Mijazaki	k1gNnSc1
</s>
<s>
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kagošima	Kagošima	k1gNnSc1
</s>
<s>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okinawa	Okinawa	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Prefectures	Prefectures	k1gInSc4
of	of	k?
Japan	japan	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
日	日	k?
na	na	k7c6
japonské	japonský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Prefektúra	Prefektúra	k1gFnSc1
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Japonsko	Japonsko	k1gNnSc1
–	–	k?
日	日	k?
(	(	kIx(
<g/>
Nihon	Nihon	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
Prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
都	都	k?
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Aiči	Aič	k1gFnSc2
(	(	kIx(
<g/>
Nagoja	Nagoja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Akita	Akita	k1gFnSc1
(	(	kIx(
<g/>
Akita	Akita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Aomori	Aomor	k1gFnSc2
(	(	kIx(
<g/>
Aomori	Aomor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Čiba	Čiba	k1gFnSc1
(	(	kIx(
<g/>
Čiba	Čiba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ehime	Ehim	k1gInSc5
(	(	kIx(
<g/>
Macujama	Macujamum	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukui	Fuku	k1gFnSc2
(	(	kIx(
<g/>
Fukui	Fuku	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukuoka	Fukuoka	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Fukuoka	Fukuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukušima	Fukušima	k1gFnSc1
(	(	kIx(
<g/>
Fukušima	Fukušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gifu	Gifus	k1gInSc2
(	(	kIx(
<g/>
Gifu	Gifus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gunma	Gunma	k1gFnSc1
(	(	kIx(
<g/>
Maebaši	Maebaše	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hirošima	Hirošima	k1gFnSc1
(	(	kIx(
<g/>
Hirošima	Hirošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hokkaidó	Hokkaidó	k1gFnSc1
(	(	kIx(
<g/>
Sapporo	Sappora	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hjógo	Hjógo	k1gMnSc1
(	(	kIx(
<g/>
Kóbe	Kóbe	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ibaraki	Ibarak	k1gFnSc2
(	(	kIx(
<g/>
Mito	Mito	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Išikawa	Išikawa	k1gFnSc1
(	(	kIx(
<g/>
Kanazawa	Kanazawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Iwate	Iwat	k1gInSc5
(	(	kIx(
<g/>
Morioka	Morioko	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamagata	Jamagata	k1gFnSc1
(	(	kIx(
<g/>
Jamagata	Jamagata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamaguči	Jamaguč	k1gInPc7
(	(	kIx(
<g/>
Jamaguči	Jamaguč	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamanaši	Jamanaše	k1gFnSc4
(	(	kIx(
<g/>
Kófu	Kófa	k1gFnSc4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagawa	Kagawa	k1gFnSc1
(	(	kIx(
<g/>
Takamacu	Takamacus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagošima	Kagošima	k1gFnSc1
(	(	kIx(
<g/>
Kagošima	Kagošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kanagawa	Kanagawa	k1gFnSc1
(	(	kIx(
<g/>
Jokohama	Jokohama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kjóto	Kjóto	k1gNnSc4
(	(	kIx(
<g/>
Kjóto	Kjóto	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kóči	Kóč	k1gFnSc2
(	(	kIx(
<g/>
Kóči	Kóč	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kumamoto	Kumamota	k1gFnSc5
(	(	kIx(
<g/>
Kumamoto	Kumamota	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Mie	Mie	k1gFnSc1
(	(	kIx(
<g/>
Cu	Cu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijagi	Mijag	k1gFnSc2
(	(	kIx(
<g/>
Sendai	Senda	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijazaki	Mijazak	k1gFnSc2
(	(	kIx(
<g/>
Mijazaki	Mijazak	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagano	Nagano	k1gNnSc4
(	(	kIx(
<g/>
Nagano	Nagano	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagasaki	Nagasaki	k1gNnSc2
(	(	kIx(
<g/>
Nagasaki	Nagasaki	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nara	Nara	k1gFnSc1
(	(	kIx(
<g/>
Nara	Nara	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Niigata	Niigata	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Niigata	Niigata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Óita	Óita	k1gFnSc1
(	(	kIx(
<g/>
Óita	Óita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okajama	Okajama	k1gFnSc1
(	(	kIx(
<g/>
Okajama	Okajama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okinawa	Okinawa	k1gFnSc1
(	(	kIx(
<g/>
Naha	naho	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ósaka	Ósaka	k1gFnSc1
(	(	kIx(
<g/>
Ósaka	Ósaka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saga	Saga	k1gFnSc1
(	(	kIx(
<g/>
Saga	Saga	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saitama	Saitama	k1gFnSc1
(	(	kIx(
<g/>
Saitama	Saitama	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šiga	Šiga	k1gFnSc1
(	(	kIx(
<g/>
Ócu	Ócu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šimane	Šiman	k1gMnSc5
(	(	kIx(
<g/>
Macue	Macue	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šizuoka	Šizuoka	k1gFnSc1
(	(	kIx(
<g/>
Šizuoka	Šizuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Točigi	Točig	k1gFnSc2
(	(	kIx(
<g/>
Ucunomija	Ucunomija	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokušima	Tokušima	k1gFnSc1
(	(	kIx(
<g/>
Tokušima	Tokušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokio	Tokio	k1gNnSc4
(	(	kIx(
<g/>
Šindžuku	Šindžuk	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
(	(	kIx(
<g/>
Tottori	Tottor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tojama	Tojama	k1gFnSc1
(	(	kIx(
<g/>
Tojama	Tojama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Wakajama	Wakajama	k1gFnSc1
(	(	kIx(
<g/>
Wakajama	Wakajama	k1gFnSc1
<g/>
)	)	kIx)
Japonské	japonský	k2eAgInPc1d1
regiony	region	k1gInPc1
(	(	kIx(
<g/>
地	地	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Hokkaidó	Hokkaidó	k1gFnSc1
•	•	k?
Tóhoku	Tóhok	k1gInSc2
•	•	k?
Kantó	Kantó	k1gMnSc1
•	•	k?
Čúbu	Čúba	k1gFnSc4
•	•	k?
Kansai	Kansa	k1gFnSc3
•	•	k?
Čúgoku	Čúgok	k1gInSc6
•	•	k?
Šikoku	Šikok	k1gInSc2
•	•	k?
Kjúšú	Kjúšú	k1gMnSc1
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Asie	Asie	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Sýrie	Sýrie	k1gFnSc2
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc4
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Vánoční	vánoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Japonsko	Japonsko	k1gNnSc1
</s>
