<s>
Gustave	Gustav	k1gMnSc5	Gustav
Flaubert	Flaubert	k1gMnSc1	Flaubert
[	[	kIx(	[
<g/>
flobér	flobér	k1gMnSc1	flobér
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1821	[number]	k4	1821
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
romantismu	romantismus	k1gInSc2	romantismus
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
a	a	k8xC	a
naturalismu	naturalismus	k1gInSc3	naturalismus
<g/>
.	.	kIx.	.
</s>
