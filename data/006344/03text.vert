<s>
Gustave	Gustav	k1gMnSc5	Gustav
Flaubert	Flaubert	k1gMnSc1	Flaubert
[	[	kIx(	[
<g/>
flobér	flobér	k1gMnSc1	flobér
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1821	[number]	k4	1821
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
romantismu	romantismus	k1gInSc2	romantismus
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
a	a	k8xC	a
naturalismu	naturalismus	k1gInSc3	naturalismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
plno	plno	k1gNnSc4	plno
dvojznačnosti	dvojznačnost	k1gFnSc2	dvojznačnost
tohoto	tento	k3xDgInSc2	tento
přechodu	přechod	k1gInSc2	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
do	do	k7c2	do
lékařské	lékařský	k2eAgFnSc2d1	lékařská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navázal	navázat	k5eAaPmAgMnS	navázat
literární	literární	k2eAgFnSc4d1	literární
známost	známost	k1gFnSc4	známost
s	s	k7c7	s
Victorem	Victor	k1gMnSc7	Victor
Hugem	Hugo	k1gMnSc7	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nervové	nervový	k2eAgInPc4d1	nervový
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
,	,	kIx,	,
epilepsie	epilepsie	k1gFnPc4	epilepsie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
přání	přání	k1gNnSc4	přání
r.	r.	kA	r.
1844	[number]	k4	1844
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
dalších	další	k2eAgNnPc2d1	další
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
finančně	finančně	k6eAd1	finančně
zabezpečen	zabezpečit	k5eAaPmNgMnS	zabezpečit
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
si	se	k3xPyFc3	se
dovolit	dovolit	k5eAaPmF	dovolit
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
každé	každý	k3xTgNnSc4	každý
dílo	dílo	k1gNnSc4	dílo
několikrát	několikrát	k6eAd1	několikrát
přepracovávat	přepracovávat	k5eAaImF	přepracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Obdiv	obdiv	k1gInSc4	obdiv
a	a	k8xC	a
přátelství	přátelství	k1gNnSc4	přátelství
choval	chovat	k5eAaImAgMnS	chovat
k	k	k7c3	k
I.	I.	kA	I.
<g/>
S.	S.	kA	S.
Turgeněvovi	Turgeněva	k1gMnSc3	Turgeněva
a	a	k8xC	a
k	k	k7c3	k
synovci	synovec	k1gMnSc3	synovec
svého	svůj	k3xOyFgMnSc4	svůj
předčasně	předčasně	k6eAd1	předčasně
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
přítele	přítel	k1gMnSc4	přítel
Guy	Guy	k1gMnSc4	Guy
de	de	k?	de
Maupassantovi	Maupassant	k1gMnSc3	Maupassant
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
svými	svůj	k3xOyFgFnPc7	svůj
radami	rada	k1gFnPc7	rada
i	i	k8xC	i
pomocí	pomoc	k1gFnSc7	pomoc
prakticky	prakticky	k6eAd1	prakticky
uváděl	uvádět	k5eAaImAgMnS	uvádět
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
manželství	manželství	k1gNnSc4	manželství
jako	jako	k8xS	jako
projev	projev	k1gInSc4	projev
měšťáctví	měšťáctví	k1gNnSc2	měšťáctví
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
každodenní	každodenní	k2eAgInPc4d1	každodenní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
platonickou	platonický	k2eAgFnSc4d1	platonická
lásku	láska	k1gFnSc4	láska
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
rádkyně	rádkyně	k1gFnSc1	rádkyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
starší	starý	k2eAgFnSc1d2	starší
o	o	k7c4	o
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
s	s	k7c7	s
naturalisty	naturalista	k1gMnPc7	naturalista
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
původem	původ	k1gInSc7	původ
romantik	romantika	k1gFnPc2	romantika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
možného	možný	k2eAgNnSc2d1	možné
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
toužil	toužit	k5eAaImAgInS	toužit
po	po	k7c6	po
objektivním	objektivní	k2eAgInSc6d1	objektivní
vědeckém	vědecký	k2eAgInSc6d1	vědecký
pohledu	pohled	k1gInSc6	pohled
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
filozofické	filozofický	k2eAgFnSc2d1	filozofická
a	a	k8xC	a
psychologické	psychologický	k2eAgFnSc2d1	psychologická
analýzy	analýza	k1gFnSc2	analýza
–	–	k?	–
vynikal	vynikat	k5eAaImAgMnS	vynikat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
–	–	k?	–
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
<g/>
Tunis	Tunis	k1gInSc4	Tunis
a	a	k8xC	a
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
-	-	kIx~	-
Cařihrad	Cařihrad	k1gInSc1	Cařihrad
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc1d1	horní
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
soudně	soudně	k6eAd1	soudně
stíhán	stíhat	k5eAaImNgMnS	stíhat
za	za	k7c4	za
román	román	k1gInSc4	román
"	"	kIx"	"
<g/>
Paní	paní	k1gFnSc1	paní
Bovaryová	Bovaryová	k1gFnSc1	Bovaryová
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
jako	jako	k8xC	jako
Baudelaire	Baudelair	k1gInSc5	Baudelair
za	za	k7c7	za
"	"	kIx"	"
<g/>
Květy	květ	k1gInPc1	květ
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vytržených	vytržený	k2eAgInPc2d1	vytržený
úryvků	úryvek	k1gInPc2	úryvek
"	"	kIx"	"
<g/>
lascivních	lascivní	k2eAgNnPc2d1	lascivní
líčení	líčení	k1gNnPc2	líčení
<g/>
"	"	kIx"	"
milostných	milostný	k2eAgNnPc2d1	milostné
dobrodružství	dobrodružství	k1gNnPc2	dobrodružství
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
byl	být	k5eAaImAgInS	být
stíhán	stíhat	k5eAaImNgMnS	stíhat
pro	pro	k7c4	pro
urážku	urážka	k1gFnSc4	urážka
veřejné	veřejný	k2eAgFnSc2d1	veřejná
mravnosti	mravnost	k1gFnSc2	mravnost
a	a	k8xC	a
manželské	manželský	k2eAgFnSc2d1	manželská
instituce	instituce	k1gFnSc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
soudním	soudní	k2eAgNnSc6d1	soudní
líčení	líčení	k1gNnSc6	líčení
v	v	k7c6	v
r.	r.	kA	r.
1857	[number]	k4	1857
byl	být	k5eAaImAgInS	být
zproštěn	zprostit	k5eAaPmNgMnS	zprostit
všech	všecek	k3xTgNnPc2	všecek
obvinění	obvinění	k1gNnSc2	obvinění
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
však	však	k9	však
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1864	[number]	k4	1864
zařadila	zařadit	k5eAaPmAgFnS	zařadit
Flaubertovy	Flaubertův	k2eAgInPc4d1	Flaubertův
romány	román	k1gInPc4	román
"	"	kIx"	"
<g/>
Paní	paní	k1gFnSc1	paní
Bovaryová	Bovaryová	k1gFnSc1	Bovaryová
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Salambo	Salamba	k1gFnSc5	Salamba
<g/>
"	"	kIx"	"
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Bovaryová	Bovaryová	k1gFnSc1	Bovaryová
<g/>
,	,	kIx,	,
1857	[number]	k4	1857
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
citového	citový	k2eAgNnSc2d1	citové
odcizení	odcizení	k1gNnSc2	odcizení
a	a	k8xC	a
manželské	manželský	k2eAgFnSc2d1	manželská
nevěry	nevěra	k1gFnSc2	nevěra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
uznáváno	uznáván	k2eAgNnSc4d1	uznáváno
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
realistického	realistický	k2eAgInSc2d1	realistický
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
původně	původně	k6eAd1	původně
časopisecky	časopisecky	k6eAd1	časopisecky
v	v	k7c4	v
Revue	revue	k1gFnSc4	revue
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
v	v	k7c6	v
r.	r.	kA	r.
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
knižní	knižní	k2eAgNnPc4d1	knižní
vydání	vydání	k1gNnPc4	vydání
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc4	dílo
proti	proti	k7c3	proti
původnímu	původní	k2eAgInSc3d1	původní
rukopisu	rukopis	k1gInSc3	rukopis
radikálně	radikálně	k6eAd1	radikálně
zkráceno	zkrácen	k2eAgNnSc1d1	zkráceno
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Emy	Ema	k1gFnSc2	Ema
Bovaryové	Bovaryová	k1gFnSc2	Bovaryová
<g/>
,	,	kIx,	,
ženy	žena	k1gFnSc2	žena
venkovského	venkovský	k2eAgMnSc4d1	venkovský
lékaře	lékař	k1gMnSc4	lékař
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
něčem	něco	k3yInSc6	něco
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
život	život	k1gInSc4	život
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
městě	město	k1gNnSc6	město
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
východisko	východisko	k1gNnSc1	východisko
hledá	hledat	k5eAaImIp3nS	hledat
v	v	k7c6	v
milostných	milostný	k2eAgFnPc6d1	milostná
pletkách	pletka	k1gFnPc6	pletka
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ty	ten	k3xDgFnPc1	ten
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
časem	časem	k6eAd1	časem
neuspokojují	uspokojovat	k5eNaImIp3nP	uspokojovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
stále	stále	k6eAd1	stále
čerstvé	čerstvý	k2eAgFnSc3d1	čerstvá
lásce	láska	k1gFnSc3	láska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
životě	život	k1gInSc6	život
možná	možná	k9	možná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgInSc6	první
nárazu	náraz	k1gInSc6	náraz
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
konfrontována	konfrontovat	k5eAaBmNgFnS	konfrontovat
s	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
svých	svůj	k3xOyFgInPc2	svůj
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
Bovaryů	Bovary	k1gMnPc2	Bovary
propadá	propadat	k5eAaPmIp3nS	propadat
exekuci	exekuce	k1gFnSc4	exekuce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zadlužila	zadlužit	k5eAaPmAgFnS	zadlužit
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
finančně	finančně	k6eAd1	finančně
vydržovala	vydržovat	k5eAaImAgFnS	vydržovat
milence	milenec	k1gMnSc4	milenec
<g/>
)	)	kIx)	)
situaci	situace	k1gFnSc4	situace
neunese	unést	k5eNaPmIp3nS	unést
a	a	k8xC	a
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
smrtí	smrt	k1gFnSc7	smrt
zlomeného	zlomený	k2eAgMnSc4d1	zlomený
manžela	manžel	k1gMnSc4	manžel
Bovaryho	Bovary	k1gMnSc4	Bovary
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgFnSc1d1	vynikající
psychologie	psychologie	k1gFnSc1	psychologie
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
do	do	k7c2	do
Emy	Ema	k1gFnSc2	Ema
(	(	kIx(	(
<g/>
Bovaryové	Bovaryová	k1gFnSc2	Bovaryová
<g/>
)	)	kIx)	)
vložil	vložit	k5eAaPmAgMnS	vložit
hodně	hodně	k6eAd1	hodně
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
bovarysmus	bovarysmus	k1gInSc1	bovarysmus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc1d1	znamenající
útěk	útěk	k1gInSc4	útěk
z	z	k7c2	z
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
k	k	k7c3	k
iluzím	iluze	k1gFnPc3	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Citová	citový	k2eAgFnSc1d1	citová
výchova	výchova	k1gFnSc1	výchova
<g/>
,	,	kIx,	,
1869	[number]	k4	1869
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejpesimističtějších	pesimistický	k2eAgNnPc2d3	nejpesimističtější
děl	dělo	k1gNnPc2	dělo
francouzské	francouzský	k2eAgFnSc2d1	francouzská
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
zpověď	zpověď	k1gFnSc1	zpověď
Flauberta	Flaubert	k1gMnSc2	Flaubert
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
události	událost	k1gFnPc4	událost
okolo	okolo	k7c2	okolo
revolučního	revoluční	k2eAgInSc2d1	revoluční
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
Frederic	Frederic	k1gMnSc1	Frederic
<g/>
)	)	kIx)	)
–	–	k?	–
postava	postava	k1gFnSc1	postava
silně	silně	k6eAd1	silně
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
skutečného	skutečný	k2eAgInSc2d1	skutečný
života	život	k1gInSc2	život
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
své	svůj	k3xOyFgFnPc4	svůj
iluze	iluze	k1gFnPc4	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Propadá	propadat	k5eAaPmIp3nS	propadat
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgInPc1d1	hlavní
citové	citový	k2eAgInPc1d1	citový
konflikty	konflikt	k1gInPc1	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
pesimistický	pesimistický	k2eAgInSc1d1	pesimistický
obraz	obraz	k1gInSc1	obraz
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Fréderic	Fréderic	k1gMnSc1	Fréderic
marně	marně	k6eAd1	marně
<g/>
,	,	kIx,	,
platonicky	platonicky	k6eAd1	platonicky
miluje	milovat	k5eAaImIp3nS	milovat
paní	paní	k1gFnSc7	paní
Arnouxovou	Arnouxový	k2eAgFnSc7d1	Arnouxový
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
přitahován	přitahovat	k5eAaImNgInS	přitahovat
veselou	veselý	k2eAgFnSc7d1	veselá
kurtizánou	kurtizána	k1gFnSc7	kurtizána
Rosanettou	Rosanettý	k2eAgFnSc7d1	Rosanettý
<g/>
,	,	kIx,	,
zmítá	zmítat	k5eAaImIp3nS	zmítat
se	se	k3xPyFc4	se
v	v	k7c6	v
citových	citový	k2eAgInPc6d1	citový
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
<s>
Flaubert	Flaubert	k1gMnSc1	Flaubert
s	s	k7c7	s
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
psychologickou	psychologický	k2eAgFnSc7d1	psychologická
znalostí	znalost	k1gFnSc7	znalost
a	a	k8xC	a
porozuměním	porozumění	k1gNnSc7	porozumění
sleduje	sledovat	k5eAaImIp3nS	sledovat
Frédericův	Frédericův	k2eAgInSc1d1	Frédericův
citový	citový	k2eAgInSc1d1	citový
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgInSc1d1	probíhající
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
polohách	poloha	k1gFnPc6	poloha
<g/>
:	:	kIx,	:
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
nadšení	nadšení	k1gNnSc1	nadšení
<g/>
,	,	kIx,	,
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
,	,	kIx,	,
sebedůvěra	sebedůvěra	k1gFnSc1	sebedůvěra
vzbuzená	vzbuzený	k2eAgFnSc1d1	vzbuzená
bezvýznamnou	bezvýznamný	k2eAgFnSc7d1	bezvýznamná
událostí	událost	k1gFnSc7	událost
či	či	k8xC	či
slovem	slovo	k1gNnSc7	slovo
obdivované	obdivovaný	k2eAgFnSc2d1	obdivovaná
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
nesmělost	nesmělost	k1gFnSc4	nesmělost
<g/>
,	,	kIx,	,
váhavost	váhavost	k1gFnSc4	váhavost
<g/>
,	,	kIx,	,
zoufalství	zoufalství	k1gNnSc4	zoufalství
z	z	k7c2	z
nepatrného	nepatrný	k2eAgMnSc2d1	nepatrný
či	či	k8xC	či
jen	jen	k9	jen
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
neúspěchu	neúspěch	k1gInSc2	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
citovým	citový	k2eAgInSc7d1	citový
životem	život	k1gInSc7	život
Frédericovým	Frédericův	k2eAgInSc7d1	Frédericův
probíhá	probíhat	k5eAaImIp3nS	probíhat
onen	onen	k3xDgInSc4	onen
"	"	kIx"	"
<g/>
román	román	k1gInSc4	román
o	o	k7c6	o
politické	politický	k2eAgFnSc6d1	politická
moci	moc	k1gFnSc6	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Záporně	záporně	k6eAd1	záporně
jsou	být	k5eAaImIp3nP	být
zobrazeni	zobrazen	k2eAgMnPc1d1	zobrazen
různí	různý	k2eAgMnPc1d1	různý
demokraté	demokrat	k1gMnPc1	demokrat
a	a	k8xC	a
socialisté	socialist	k1gMnPc1	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Flaubertových	Flaubertových	k2eAgFnPc2d1	Flaubertových
pozic	pozice	k1gFnPc2	pozice
si	se	k3xPyFc3	se
katolictví	katolictví	k1gNnSc4	katolictví
a	a	k8xC	a
socialismus	socialismus	k1gInSc4	socialismus
podávají	podávat	k5eAaImIp3nP	podávat
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obojí	obojí	k4xRgFnSc1	obojí
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
davu	dav	k1gInSc3	dav
a	a	k8xC	a
ne	ne	k9	ne
k	k	k7c3	k
elitě	elita	k1gFnSc3	elita
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
i	i	k8xC	i
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
císařství	císařství	k1gNnSc1	císařství
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
vůbec	vůbec	k9	vůbec
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Flauberta	Flaubert	k1gMnSc4	Flaubert
projevem	projev	k1gInSc7	projev
téhož	týž	k3xTgInSc2	týž
"	"	kIx"	"
<g/>
davového	davový	k2eAgInSc2d1	davový
<g/>
"	"	kIx"	"
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
představujícího	představující	k2eAgMnSc2d1	představující
pro	pro	k7c4	pro
myslící	myslící	k2eAgFnSc4d1	myslící
a	a	k8xC	a
povznesenou	povznesený	k2eAgFnSc4d1	povznesená
elitu	elita	k1gFnSc4	elita
nesnesitelnou	snesitelný	k2eNgFnSc4d1	nesnesitelná
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Salambo	Salamba	k1gMnSc5	Salamba
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
–	–	k?	–
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Vzpoura	vzpoura	k1gFnSc1	vzpoura
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
proti	proti	k7c3	proti
Kartágu	Kartágo	k1gNnSc3	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
–	–	k?	–
dcery	dcera	k1gFnSc2	dcera
hlavního	hlavní	k2eAgMnSc2d1	hlavní
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
Zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vůdce	vůdce	k1gMnSc2	vůdce
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
<g/>
.	.	kIx.	.
</s>
<s>
Líčí	líčit	k5eAaImIp3nP	líčit
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
davové	davový	k2eAgFnPc4d1	davová
scény	scéna	k1gFnPc4	scéna
a	a	k8xC	a
otrokářství	otrokářství	k1gNnPc4	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Pokušení	pokušení	k1gNnSc1	pokušení
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
-	-	kIx~	-
dramatický	dramatický	k2eAgInSc4d1	dramatický
text	text	k1gInSc4	text
popisující	popisující	k2eAgInSc4d1	popisující
v	v	k7c6	v
groteskním	groteskní	k2eAgNnSc6d1	groteskní
světle	světlo	k1gNnSc6	světlo
pokušení	pokušení	k1gNnPc2	pokušení
provázející	provázející	k2eAgFnSc4d1	provázející
askezi	askeze	k1gFnSc4	askeze
raně	raně	k6eAd1	raně
křesťanského	křesťanský	k2eAgMnSc2d1	křesťanský
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
konfrontován	konfrontovat	k5eAaBmNgInS	konfrontovat
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
"	"	kIx"	"
<g/>
nepravými	pravý	k2eNgInPc7d1	nepravý
<g/>
"	"	kIx"	"
kulty	kult	k1gInPc7	kult
<g/>
,	,	kIx,	,
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
sektami	sekta	k1gFnPc7	sekta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
diskusí	diskuse	k1gFnSc7	diskuse
kolem	kolem	k7c2	kolem
výkladu	výklad	k1gInSc2	výklad
Písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
života	život	k1gInSc2	život
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
s	s	k7c7	s
pohanskými	pohanský	k2eAgMnPc7d1	pohanský
bohy	bůh	k1gMnPc7	bůh
<g/>
,	,	kIx,	,
s	s	k7c7	s
gnostiky	gnostik	k1gMnPc7	gnostik
<g/>
,	,	kIx,	,
ariány	arián	k1gMnPc7	arián
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
i	i	k9	i
s	s	k7c7	s
bájnými	bájný	k2eAgNnPc7d1	bájné
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
alegorickými	alegorický	k2eAgFnPc7d1	alegorická
postavami	postava	k1gFnPc7	postava
Ďábla	ďábel	k1gMnSc2	ďábel
<g/>
,	,	kIx,	,
Vilnosti	vilnost	k1gFnSc2	vilnost
a	a	k8xC	a
Smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
fiktivně	fiktivně	k6eAd1	fiktivně
odehraje	odehrát	k5eAaPmIp3nS	odehrát
během	během	k7c2	během
jediné	jediný	k2eAgFnSc2d1	jediná
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
pochybami	pochyba	k1gFnPc7	pochyba
zkoušený	zkoušený	k2eAgMnSc1d1	zkoušený
Antonín	Antonín	k1gMnSc1	Antonín
vystaven	vystaven	k2eAgMnSc1d1	vystaven
mnoha	mnoho	k4c2	mnoho
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
obrazně	obrazně	k6eAd1	obrazně
propracovaným	propracovaný	k2eAgInSc7d1	propracovaný
<g/>
,	,	kIx,	,
halucinacím	halucinace	k1gFnPc3	halucinace
a	a	k8xC	a
vizím	vize	k1gFnPc3	vize
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
(	(	kIx(	(
<g/>
Prosté	prostý	k2eAgNnSc1d1	prosté
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
Julián	Julián	k1gMnSc1	Julián
Pohostinný	pohostinný	k2eAgMnSc1d1	pohostinný
<g/>
,	,	kIx,	,
Herodias	Herodias	k1gFnSc1	Herodias
<g/>
)	)	kIx)	)
Bouvard	Bouvard	k1gMnSc1	Bouvard
a	a	k8xC	a
Pécuchet	Pécuchet	k1gMnSc1	Pécuchet
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
–	–	k?	–
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
vysloužilých	vysloužilý	k2eAgMnPc6d1	vysloužilý
písařích	písař	k1gMnPc6	písař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
důchodu	důchod	k1gInSc6	důchod
<g/>
,	,	kIx,	,
věnují	věnovat	k5eAaImIp3nP	věnovat
usilovnému	usilovný	k2eAgNnSc3d1	usilovné
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ale	ale	k9	ale
nedaří	dařit	k5eNaImIp3nS	dařit
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
dočkají	dočkat	k5eAaPmIp3nP	dočkat
pouze	pouze	k6eAd1	pouze
posměchu	posměch	k1gInSc2	posměch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
jako	jako	k8xS	jako
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Byli	být	k5eAaImAgMnP	být
jednou	jednou	k6eAd1	jednou
dva	dva	k4xCgMnPc1	dva
písaři	písař	k1gMnPc1	písař
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Ján	Ján	k1gMnSc1	Ján
Roháč	roháč	k1gMnSc1	roháč
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Jiří	Jiří	k1gMnSc1	Jiří
Sovák	Sovák	k1gMnSc1	Sovák
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
