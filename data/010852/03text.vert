<p>
<s>
Podvazek	podvazek	k1gInSc1	podvazek
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
dámského	dámský	k2eAgNnSc2d1	dámské
spodního	spodní	k2eAgNnSc2d1	spodní
prádla	prádlo	k1gNnSc2	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
punčochy	punčocha	k1gFnSc2	punčocha
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
punčocha	punčocha	k1gFnSc1	punčocha
sjela	sjet	k5eAaPmAgFnS	sjet
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
gumový	gumový	k2eAgInSc4d1	gumový
kroužek	kroužek	k1gInSc4	kroužek
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
gumový	gumový	k2eAgInSc4d1	gumový
kroužek	kroužek	k1gInSc4	kroužek
s	s	k7c7	s
krajkou	krajka	k1gFnSc7	krajka
<g/>
,	,	kIx,	,
obepínající	obepínající	k2eAgFnSc4d1	obepínající
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
přetažen	přetáhnout	k5eAaPmNgInS	přetáhnout
přes	přes	k7c4	přes
punčochu	punčocha	k1gFnSc4	punčocha
a	a	k8xC	a
drží	držet	k5eAaImIp3nP	držet
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
stehně	stehno	k1gNnSc6	stehno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
dámského	dámský	k2eAgNnSc2d1	dámské
spodního	spodní	k2eAgNnSc2d1	spodní
prádla	prádlo	k1gNnSc2	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
4	[number]	k4	4
podvazkové	podvazkový	k2eAgInPc4d1	podvazkový
tahy	tah	k1gInPc4	tah
se	s	k7c7	s
sponkami	sponka	k1gFnPc7	sponka
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
upínají	upínat	k5eAaImIp3nP	upínat
lemy	lem	k1gInPc1	lem
punčoch	punčocha	k1gFnPc2	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
také	také	k9	také
podvazkové	podvazkový	k2eAgInPc1d1	podvazkový
pásy	pás	k1gInPc1	pás
se	s	k7c7	s
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
podvazkovými	podvazkový	k2eAgInPc7d1	podvazkový
tahy	tah	k1gInPc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Tahy	tah	k1gInPc1	tah
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
gumou	guma	k1gFnSc7	guma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
a	a	k8xC	a
přezkou	přezka	k1gFnSc7	přezka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
upne	upnout	k5eAaPmIp3nS	upnout
do	do	k7c2	do
lemu	lem	k1gInSc2	lem
punčochy	punčocha	k1gFnSc2	punčocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
podvazku	podvazek	k1gInSc2	podvazek
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
nazval	nazvat	k5eAaBmAgInS	nazvat
Podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
řád	řád	k1gInSc1	řád
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nejprestižnější	prestižní	k2eAgInSc1d3	nejprestižnější
anglický	anglický	k2eAgInSc4d1	anglický
rytířský	rytířský	k2eAgInSc4d1	rytířský
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc7	jeho
založením	založení	k1gNnSc7	založení
král	král	k1gMnSc1	král
záchránil	záchránit	k5eAaImAgMnS	záchránit
čest	čest	k1gFnSc4	čest
hraběnce	hraběnka	k1gFnSc3	hraběnka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
terčem	terč	k1gInSc7	terč
posměchu	posměch	k1gInSc2	posměch
pro	pro	k7c4	pro
nehodu	nehoda	k1gFnSc4	nehoda
s	s	k7c7	s
podvazkem	podvazek	k1gInSc7	podvazek
na	na	k7c6	na
dvorním	dvorní	k2eAgInSc6d1	dvorní
plese	ples	k1gInSc6	ples
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podvazek	podvazek	k1gInSc1	podvazek
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
masově	masově	k6eAd1	masově
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
především	především	k9	především
jako	jako	k8xC	jako
erotický	erotický	k2eAgInSc4d1	erotický
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Nemalou	malý	k2eNgFnSc4d1	nemalá
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
měly	mít	k5eAaImAgFnP	mít
tanečnice	tanečnice	k1gFnPc1	tanečnice
kankánu	kankán	k1gInSc2	kankán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
ženy	žena	k1gFnPc1	žena
punčochy	punčocha	k1gFnSc2	punčocha
pevně	pevně	k6eAd1	pevně
uchycovat	uchycovat	k5eAaImF	uchycovat
podvázkovými	podvázkový	k2eAgInPc7d1	podvázkový
pásy	pás	k1gInPc7	pás
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
vnímání	vnímání	k1gNnSc1	vnímání
podvazků	podvazek	k1gInPc2	podvazek
a	a	k8xC	a
podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Erotické	erotický	k2eAgFnPc1d1	erotická
konotace	konotace	k1gFnPc1	konotace
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sice	sice	k8xC	sice
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
ustoupily	ustoupit	k5eAaPmAgInP	ustoupit
výhodám	výhoda	k1gFnPc3	výhoda
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgNnSc4d1	každodenní
používání	používání	k1gNnSc4	používání
podvazků	podvazek	k1gInPc2	podvazek
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
pevného	pevný	k2eAgNnSc2d1	pevné
uchycení	uchycení	k1gNnSc2	uchycení
punčoch	punčocha	k1gFnPc2	punčocha
jsou	být	k5eAaImIp3nP	být
podvazky	podvazek	k1gInPc1	podvazek
hygieničtější	hygienický	k2eAgInPc1d2	hygieničtější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podvazcích	podvazek	k1gInPc6	podvazek
se	se	k3xPyFc4	se
ženy	žena	k1gFnPc1	žena
potí	potit	k5eAaImIp3nP	potit
méně	málo	k6eAd2	málo
než	než	k8xS	než
při	při	k7c6	při
nošení	nošení	k1gNnSc6	nošení
punčochových	punčochový	k2eAgFnPc2d1	punčochová
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
.	.	kIx.	.
<g/>
Podvazek	podvazek	k1gInSc1	podvazek
je	být	k5eAaImIp3nS	být
také	také	k9	také
častý	častý	k2eAgInSc1d1	častý
doplněk	doplněk	k1gInSc1	doplněk
svatebních	svatební	k2eAgInPc2d1	svatební
šatů	šat	k1gInPc2	šat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
modrý	modrý	k2eAgInSc1d1	modrý
podvazek	podvazek	k1gInSc1	podvazek
věrnost	věrnost	k1gFnSc4	věrnost
a	a	k8xC	a
čistotu	čistota	k1gFnSc4	čistota
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
darem	dar	k1gInSc7	dar
dostat	dostat	k5eAaPmF	dostat
podvazků	podvazek	k1gInPc2	podvazek
několik	několik	k4yIc4	několik
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
pak	pak	k6eAd1	pak
draží	dražit	k5eAaImIp3nP	dražit
mezi	mezi	k7c7	mezi
svobodnými	svobodný	k2eAgMnPc7d1	svobodný
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
