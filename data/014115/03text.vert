<s>
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Hegel	Hegela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
filosofovi	filosof	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Hegel	Hegel	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
</s>
<s>
Jakob	Jakob	k1gMnSc1
Schlesinger	Schlesinger	k1gMnSc1
<g/>
:	:	kIx,
Portrét	portrét	k1gInSc1
G.	G.	kA
W.	W.	kA
F.	F.	kA
Hegela	Hegel	k1gMnSc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
(	(	kIx(
<g/>
1831	#num#	k4
<g/>
)	)	kIx)
<g/>
Celé	celý	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
Region	region	k1gInSc4
</s>
<s>
západní	západní	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Období	období	k1gNnSc1
</s>
<s>
filosofie	filosofie	k1gFnSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Narození	narození	k1gNnPc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1770	#num#	k4
Stuttgart	Stuttgart	k1gInSc1
<g/>
,	,	kIx,
Württembersko	Württembersko	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1831	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
61	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Pruské	pruský	k2eAgNnSc1d1
království	království	k1gNnSc1
Škola	škola	k1gFnSc1
<g/>
/	/	kIx~
<g/>
tradice	tradice	k1gFnSc1
</s>
<s>
Německý	německý	k2eAgInSc1d1
idealismus	idealismus	k1gInSc1
Oblasti	oblast	k1gFnSc2
zájmu	zájem	k1gInSc2
</s>
<s>
logika	logika	k1gFnSc1
<g/>
,	,	kIx,
estetika	estetika	k1gFnSc1
<g/>
,	,	kIx,
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
metafyzika	metafyzika	k1gFnSc1
<g/>
,	,	kIx,
gnozeologie	gnozeologie	k1gFnSc1
<g/>
,	,	kIx,
politická	politický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Význačné	význačný	k2eAgFnSc2d1
ideje	idea	k1gFnSc2
</s>
<s>
dialektika	dialektika	k1gFnSc1
Vlivy	vliv	k1gInPc1
</s>
<s>
Aristotelés	Aristotelés	k1gInSc1
<g/>
,	,	kIx,
Platón	platón	k1gInSc1
<g/>
,	,	kIx,
Hérakleitos	Hérakleitos	k1gInSc1
<g/>
,	,	kIx,
novoplatonismus	novoplatonismus	k1gInSc1
<g/>
,	,	kIx,
Descartes	Descartes	k1gMnSc1
<g/>
,	,	kIx,
Goethe	Goethe	k1gFnSc1
<g/>
,	,	kIx,
Spinoza	Spinoza	k1gFnSc1
<g/>
,	,	kIx,
Leibniz	Leibniz	k1gMnSc1
<g/>
,	,	kIx,
Rousseau	Rousseau	k1gMnSc1
<g/>
,	,	kIx,
Böhme	Böhm	k1gMnSc5
<g/>
,	,	kIx,
Kant	Kant	k1gMnSc1
<g/>
,	,	kIx,
Fichte	Ficht	k1gMnSc5
<g/>
,	,	kIx,
Hölderlin	Hölderlin	k1gInSc1
<g/>
,	,	kIx,
Schelling	Schelling	k1gInSc1
<g/>
,	,	kIx,
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Schiller	Schiller	k1gMnSc1
Vliv	vliv	k1gInSc1
na	na	k7c6
</s>
<s>
Adorno	Adorno	k1gNnSc1
<g/>
,	,	kIx,
Barth	Barth	k1gMnSc1
<g/>
,	,	kIx,
Bauer	Bauer	k1gMnSc1
<g/>
,	,	kIx,
Bosanquet	Bosanquet	k1gMnSc1
<g/>
,	,	kIx,
Bradley	Bradlea	k1gFnPc1
<g/>
,	,	kIx,
Brandom	Brandom	k1gInSc1
<g/>
,	,	kIx,
Beauvoir	Beauvoir	k1gInSc1
<g/>
,	,	kIx,
Butler	Butler	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Carové	carová	k1gFnPc1
<g/>
,	,	kIx,
Croce	Croec	k1gInPc1
<g/>
,	,	kIx,
Danto	Danto	k1gNnSc1
<g/>
,	,	kIx,
Derrida	Derrida	k1gFnSc1
<g/>
,	,	kIx,
Dilthey	Dilthea	k1gFnPc1
<g/>
,	,	kIx,
Doull	Doull	k1gMnSc1
<g/>
,	,	kIx,
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Erdmann	Erdmann	k1gMnSc1
<g/>
,	,	kIx,
Fackenheim	Fackenheim	k1gMnSc1
<g/>
,	,	kIx,
Feuerbach	Feuerbach	k1gMnSc1
<g/>
,	,	kIx,
Fischer	Fischer	k1gMnSc1
<g/>
,	,	kIx,
Fukuyama	Fukuyama	k1gFnSc1
<g/>
,	,	kIx,
Heidegger	Heidegger	k1gInSc1
<g/>
,	,	kIx,
Gans	Gans	k1gInSc1
<g/>
,	,	kIx,
Gentile	Gentil	k1gMnSc5
<g/>
,	,	kIx,
Hinrichs	Hinrichsa	k1gFnPc2
<g/>
,	,	kIx,
Heine	Hein	k1gMnSc5
<g/>
,	,	kIx,
Hyppolite	Hyppolit	k1gInSc5
<g/>
,	,	kIx,
Kaufmann	Kaufmanna	k1gFnPc2
<g/>
,	,	kIx,
Kierkegaard	Kierkegaarda	k1gFnPc2
<g/>
,	,	kIx,
Kojè	Kojè	k1gFnPc1
<g/>
,	,	kIx,
Küng	Küng	k1gInSc1
<g/>
,	,	kIx,
Lukács	Lukács	k1gInSc1
<g/>
,	,	kIx,
Marcuse	Marcuse	k1gFnSc1
<g/>
,	,	kIx,
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
Michelet	Michelet	k1gInSc1
<g/>
,	,	kIx,
Nietzsche	Nietzsche	k1gInSc1
<g/>
,	,	kIx,
Oppenheim	Oppenheim	k1gMnSc1
<g/>
,	,	kIx,
Pippin	Pippin	k1gMnSc1
<g/>
,	,	kIx,
Rose	Rose	k1gMnSc1
<g/>
,	,	kIx,
Rosenkranz	Rosenkranz	k1gMnSc1
<g/>
,	,	kIx,
Russon	Russon	k1gMnSc1
<g/>
,	,	kIx,
Sartre	Sartr	k1gMnSc5
<g/>
,	,	kIx,
Singer	Singra	k1gFnPc2
<g/>
,	,	kIx,
Strauss	Straussa	k1gFnPc2
<g/>
,	,	kIx,
Taylor	Taylora	k1gFnPc2
<g/>
,	,	kIx,
Žižek	Žižek	k6eAd1
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1770	#num#	k4
<g/>
,	,	kIx,
Stuttgart	Stuttgart	k1gInSc1
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1831	#num#	k4
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
filosof	filosof	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
německého	německý	k2eAgInSc2d1
idealismu	idealismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hegelovým	Hegelův	k2eAgInSc7d1
hlavním	hlavní	k2eAgInSc7d1
přínosem	přínos	k1gInSc7
je	být	k5eAaImIp3nS
objev	objev	k1gInSc1
dějinnosti	dějinnost	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
svět	svět	k1gInSc1
není	být	k5eNaImIp3nS
neměnné	měnný	k2eNgNnSc4d1
<g/>
,	,	kIx,
stálé	stálý	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jedno	jeden	k4xCgNnSc1
nesmírné	smírný	k2eNgNnSc1d1
dějství	dějství	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
světový	světový	k2eAgMnSc1d1
duch	duch	k1gMnSc1
hledá	hledat	k5eAaImIp3nS
cestu	cesta	k1gFnSc4
sám	sám	k3xTgMnSc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lidstvu	lidstvo	k1gNnSc6
se	se	k3xPyFc4
uskutečňuje	uskutečňovat	k5eAaImIp3nS
svoboda	svoboda	k1gFnSc1
a	a	k8xC
rozum	rozum	k1gInSc1
vesmíru	vesmír	k1gInSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
roste	růst	k5eAaImIp3nS
lidské	lidský	k2eAgNnSc4d1
uvědomění	uvědomění	k1gNnSc4
a	a	k8xC
schopnost	schopnost	k1gFnSc4
přetvářet	přetvářet	k5eAaImF
svět	svět	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Tomu	ten	k3xDgMnSc3
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
jest	být	k5eAaImIp3nS
<g/>
,	,	kIx,
nelze	lze	k6eNd1
rozumět	rozumět	k5eAaImF
jinak	jinak	k6eAd1
než	než	k8xS
jako	jako	k8xC,k8xS
procesu	proces	k1gInSc6
<g/>
,	,	kIx,
změně	změna	k1gFnSc6
<g/>
,	,	kIx,
vývoji	vývoj	k1gInSc6
<g/>
,	,	kIx,
pokroku	pokrok	k1gInSc6
<g/>
,	,	kIx,
cestě	cesta	k1gFnSc6
k	k	k7c3
dokonalosti	dokonalost	k1gFnSc3
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k8xC
zápasu	zápas	k1gInSc2
protikladů	protiklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
myšlenku	myšlenka	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
v	v	k7c6
průběhu	průběh	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nejen	nejen	k6eAd1
filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
postupně	postupně	k6eAd1
i	i	k9
jednotlivé	jednotlivý	k2eAgFnPc1d1
vědy	věda	k1gFnPc1
<g/>
:	:	kIx,
jazykověda	jazykověda	k1gFnSc1
<g/>
,	,	kIx,
právo	právo	k1gNnSc1
<g/>
,	,	kIx,
sociologie	sociologie	k1gFnSc1
a	a	k8xC
v	v	k7c6
podobě	podoba	k1gFnSc6
evoluce	evoluce	k1gFnSc2
také	také	k9
např.	např.	kA
biologie	biologie	k1gFnSc1
<g/>
,	,	kIx,
antropologie	antropologie	k1gFnSc1
a	a	k8xC
kosmologie	kosmologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
ducha	duch	k1gMnSc2
a	a	k8xC
zákonitá	zákonitý	k2eAgFnSc1d1
dějinnost	dějinnost	k1gFnSc1
světa	svět	k1gInSc2
začíná	začínat	k5eAaImIp3nS
uzavřeným	uzavřený	k2eAgNnSc7d1
absolutnem	absolutno	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
totožné	totožný	k2eAgNnSc1d1
s	s	k7c7
nicotou	nicota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kosmologického	kosmologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
hovořit	hovořit	k5eAaImF
o	o	k7c6
singularitě	singularita	k1gFnSc6
v	v	k7c6
období	období	k1gNnSc6
před	před	k7c7
velkým	velký	k2eAgInSc7d1
třeskem	třesk	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Absolutno	absolutno	k1gNnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
začíná	začínat	k5eAaImIp3nS
realizovat	realizovat	k5eAaBmF
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nakonec	nakonec	k6eAd1
po	po	k7c6
mnoha	mnoho	k4c6
miliardách	miliarda	k4xCgFnPc6
let	léto	k1gNnPc2
dalo	dát	k5eAaPmAgNnS
vzniknout	vzniknout	k5eAaPmF
vědomí	vědomí	k1gNnSc1
a	a	k8xC
poznávalo	poznávat	k5eAaImAgNnS
samo	sám	k3xTgNnSc1
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
ve	v	k7c6
Stuttgartu	Stuttgart	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svých	svůj	k3xOyFgNnPc2
pěti	pět	k4xCc2
let	léto	k1gNnPc2
navštěvoval	navštěvovat	k5eAaImAgInS
tamější	tamější	k2eAgFnSc4d1
latinskou	latinský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
a	a	k8xC
brzy	brzy	k6eAd1
si	se	k3xPyFc3
vynikajícím	vynikající	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
osvojil	osvojit	k5eAaPmAgInS
řecké	řecký	k2eAgMnPc4d1
i	i	k8xC
římské	římský	k2eAgMnPc4d1
klasiky	klasik	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1788	#num#	k4
studoval	studovat	k5eAaImAgMnS
filosofii	filosofie	k1gFnSc4
a	a	k8xC
teologii	teologie	k1gFnSc4
v	v	k7c6
Tübingenu	Tübingen	k1gInSc6
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
přáteli	přítel	k1gMnPc7
<g/>
,	,	kIx,
filosofem	filosof	k1gMnSc7
Schellingem	Schelling	k1gInSc7
a	a	k8xC
básníkem	básník	k1gMnSc7
Hölderlinem	Hölderlin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1801	#num#	k4
se	se	k3xPyFc4
habilitoval	habilitovat	k5eAaBmAgInS
spisem	spis	k1gInSc7
o	o	k7c4
astronomii	astronomie	k1gFnSc4
v	v	k7c6
Jeně	Jena	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pak	pak	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
filosofii	filosofie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
roku	rok	k1gInSc2
1806	#num#	k4
porazil	porazit	k5eAaPmAgInS
Napoleon	napoleon	k1gInSc1
pruskou	pruský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
u	u	k7c2
Jeny	jen	k1gInPc1
<g/>
,	,	kIx,
Hegel	Hegel	k1gInSc1
právě	právě	k6eAd1
dokončoval	dokončovat	k5eAaImAgInS
své	svůj	k3xOyFgNnSc4
nejslavnější	slavný	k2eAgNnSc4d3
dílo	dílo	k1gNnSc4
–	–	k?
Fenomenologii	fenomenologie	k1gFnSc4
ducha	duch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přítelem	přítel	k1gMnSc7
Schellingem	Schelling	k1gInSc7
vydával	vydávat	k5eAaPmAgInS,k5eAaImAgInS
„	„	k?
<g/>
Kritický	kritický	k2eAgInSc1d1
časopis	časopis	k1gInSc1
pro	pro	k7c4
filosofii	filosofie	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
věnovaný	věnovaný	k2eAgInSc1d1
hlavně	hlavně	k6eAd1
dědictví	dědictví	k1gNnSc4
Kantovy	Kantův	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
rektorem	rektor	k1gMnSc7
norimberského	norimberský	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
dokončil	dokončit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
druhé	druhý	k4xOgNnSc4
velké	velký	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
třísvazkovou	třísvazkový	k2eAgFnSc4d1
Vědu	věda	k1gFnSc4
o	o	k7c6
logice	logika	k1gFnSc6
(	(	kIx(
<g/>
1812	#num#	k4
<g/>
–	–	k?
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
vynesla	vynést	k5eAaPmAgFnS
povolání	povolání	k1gNnPc4
na	na	k7c4
katedru	katedra	k1gFnSc4
filosofie	filosofie	k1gFnSc2
v	v	k7c6
Heidelbergu	Heidelberg	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
Encyklopedii	encyklopedie	k1gFnSc4
filosofických	filosofický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
1817	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1811	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Marií	Maria	k1gFnSc7
von	von	k1gInSc4
Tücher	Tüchra	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
měl	mít	k5eAaImAgMnS
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1818	#num#	k4
byl	být	k5eAaImAgInS
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
těšil	těšit	k5eAaImAgMnS
nesmírné	smírný	k2eNgFnSc3d1
vážnosti	vážnost	k1gFnSc3
a	a	k8xC
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
jeho	jeho	k3xOp3gInSc1
přednes	přednes	k1gInSc1
nebyl	být	k5eNaImAgInS
dokonalý	dokonalý	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
řeči	řeč	k1gFnSc6
často	často	k6eAd1
zadrhoval	zadrhovat	k5eAaImAgMnS
<g/>
,	,	kIx,
navštěvovali	navštěvovat	k5eAaImAgMnP
jeho	jeho	k3xOp3gFnPc4
přednášky	přednáška	k1gFnPc4
i	i	k9
přední	přední	k2eAgMnPc1d1
činitelé	činitel	k1gMnPc1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
1821	#num#	k4
vyšly	vyjít	k5eAaPmAgInP
jeho	jeho	k3xOp3gInPc1
Základy	základ	k1gInPc1
filosofie	filosofie	k1gFnSc2
práva	právo	k1gNnSc2
a	a	k8xC
potom	potom	k6eAd1
ještě	ještě	k6eAd1
přepracovaná	přepracovaný	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
Encyklopedie	encyklopedie	k1gFnSc2
a	a	k8xC
Logiky	logika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadu	řad	k1gInSc2
přednášek	přednáška	k1gFnPc2
o	o	k7c4
filosofii	filosofie	k1gFnSc4
dějin	dějiny	k1gFnPc2
a	a	k8xC
náboženství	náboženství	k1gNnPc2
<g/>
,	,	kIx,
o	o	k7c6
estetice	estetika	k1gFnSc6
a	a	k8xC
dějinách	dějiny	k1gFnPc6
filosofie	filosofie	k1gFnSc2
vydali	vydat	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc1
žáci	žák	k1gMnPc1
až	až	k9
po	po	k7c6
Hegelově	Hegelův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
sebraných	sebraný	k2eAgInPc2d1
spisů	spis	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
má	mít	k5eAaImIp3nS
20	#num#	k4
svazků	svazek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
choleru	cholera	k1gFnSc4
při	při	k7c6
epidemii	epidemie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vypukla	vypuknout	k5eAaPmAgFnS
v	v	k7c6
Berlíně	Berlín	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Myšlení	myšlení	k1gNnSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Georg	Georg	k1gMnSc1
W.	W.	kA
F.	F.	kA
Hegel	Hegel	k1gMnSc1
</s>
<s>
Hegelovo	Hegelův	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
Kanta	Kant	k1gMnSc4
–	–	k?
jehož	jehož	k3xOyRp3gFnPc7
si	se	k3xPyFc3
upřímně	upřímně	k6eAd1
vážil	vážit	k5eAaImAgMnS
–	–	k?
a	a	k8xC
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
začínající	začínající	k2eAgInSc4d1
romantismus	romantismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gInSc6
vrcholí	vrcholit	k5eAaImIp3nS
německý	německý	k2eAgInSc1d1
idealismus	idealismus	k1gInSc1
<g/>
,	,	kIx,
systematická	systematický	k2eAgFnSc1d1
snaha	snaha	k1gFnSc1
o	o	k7c4
racionální	racionální	k2eAgNnSc4d1
vyjádření	vyjádření	k1gNnSc4
i	i	k9
těch	ten	k3xDgFnPc2
nejméně	málo	k6eAd3
přístupných	přístupný	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
skutečnosti	skutečnost	k1gFnSc2
jakožto	jakožto	k8xS
projevu	projev	k1gInSc2
ducha	duch	k1gMnSc2
a	a	k8xC
o	o	k7c4
jejich	jejich	k3xOp3gInSc4
výklad	výklad	k1gInSc4
„	„	k?
<g/>
z	z	k7c2
pojmu	pojem	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hegelovy	Hegelův	k2eAgInPc1d1
spisy	spis	k1gInPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
<g/>
,	,	kIx,
také	také	k9
kvůli	kvůli	k7c3
poněkud	poněkud	k6eAd1
svévolné	svévolný	k2eAgFnSc3d1
terminologii	terminologie	k1gFnSc3
<g/>
,	,	kIx,
přitahovaly	přitahovat	k5eAaImAgFnP
však	však	k9
především	především	k6eAd1
hloubkou	hloubka	k1gFnSc7
myšlenek	myšlenka	k1gFnPc2
i	i	k8xC
brilantními	brilantní	k2eAgFnPc7d1
formulacemi	formulace	k1gFnPc7
<g/>
,	,	kIx,
takže	takže	k8xS
na	na	k7c4
něj	on	k3xPp3gMnSc4
mnozí	mnohý	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
19	#num#	k4
<g/>
.	.	kIx.
i	i	k9
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
tak	tak	k6eAd1
či	či	k8xC
onak	onak	k6eAd1
navazovali	navazovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrážejí	odrážet	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
převratné	převratný	k2eAgFnPc4d1
události	událost	k1gFnPc4
jeho	jeho	k3xOp3gFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
Francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
a	a	k8xC
napoleonské	napoleonský	k2eAgInPc1d1
války	válek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
Hegel	Hegel	k1gMnSc1
(	(	kIx(
<g/>
aspoň	aspoň	k9
zpočátku	zpočátku	k6eAd1
<g/>
)	)	kIx)
s	s	k7c7
obdivem	obdiv	k1gInSc7
vítal	vítat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Ani	ani	k8xC
pro	pro	k7c4
Hegela	Hegel	k1gMnSc4
není	být	k5eNaImIp3nS
filosofie	filosofie	k1gFnSc1
jen	jen	k6eAd1
poznáním	poznání	k1gNnSc7
či	či	k8xC
vědou	věda	k1gFnSc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
„	„	k?
<g/>
pravá	pravá	k1gFnSc1
potřeba	potřeba	k1gFnSc1
filosofie	filosofie	k1gFnSc1
nemíří	mířit	k5eNaImIp3nS
patrně	patrně	k6eAd1
k	k	k7c3
ničemu	nic	k3yNnSc3
jinému	jiný	k2eAgNnSc3d1
než	než	k8xS
naučit	naučit	k5eAaPmF
se	se	k3xPyFc4
žít	žít	k5eAaImF
<g/>
“	“	k?
<g/>
;	;	kIx,
tato	tento	k3xDgFnSc1
potřeba	potřeba	k1gFnSc1
vzniká	vznikat	k5eAaImIp3nS
„	„	k?
<g/>
když	když	k8xS
z	z	k7c2
lidských	lidský	k2eAgInPc2d1
životů	život	k1gInPc2
mizí	mizet	k5eAaImIp3nS
síla	síla	k1gFnSc1
sjednocení	sjednocení	k1gNnSc1
a	a	k8xC
protiklady	protiklad	k1gInPc1
ztratily	ztratit	k5eAaPmAgFnP
svoji	svůj	k3xOyFgFnSc4
životnost	životnost	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
myšlení	myšlení	k1gNnSc6
člověk	člověk	k1gMnSc1
„	„	k?
<g/>
hledá	hledat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
základ	základ	k1gInSc4
mravnosti	mravnost	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
„	„	k?
<g/>
pochopit	pochopit	k5eAaPmF
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
jest	být	k5eAaImIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rozum	rozum	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
„	„	k?
<g/>
synem	syn	k1gMnSc7
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
filosofie	filosofie	k1gFnSc1
„	„	k?
<g/>
svojí	svojit	k5eAaImIp3nS
dobou	doba	k1gFnSc7
uchopenou	uchopený	k2eAgFnSc7d1
v	v	k7c6
myšlenkách	myšlenka	k1gFnPc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
se	se	k3xPyFc4
bohužel	bohužel	k9
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
myšlení	myšlení	k1gNnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
svobodné	svobodný	k2eAgNnSc4d1
<g/>
,	,	kIx,
jen	jen	k9
když	když	k8xS
se	se	k3xPyFc4
odchyluje	odchylovat	k5eAaImIp3nS
od	od	k7c2
obecně	obecně	k6eAd1
uznávaného	uznávaný	k2eAgInSc2d1
a	a	k8xC
platného	platný	k2eAgInSc2d1
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
dovede	dovést	k5eAaPmIp3nS
vynalézt	vynalézt	k5eAaPmF
něco	něco	k3yInSc4
zvláštního	zvláštní	k2eAgMnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
„	„	k?
<g/>
neklidné	klidný	k2eNgFnSc2d1
samolibosti	samolibost	k1gFnSc2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
kdekdo	kdekdo	k3yInSc1
„	„	k?
<g/>
přesvědčen	přesvědčit	k5eAaPmNgInS
<g/>
,	,	kIx,
že	že	k8xS
rozumí	rozumět	k5eAaImIp3nS
filosofii	filosofie	k1gFnSc4
vůbec	vůbec	k9
a	a	k8xC
je	být	k5eAaImIp3nS
s	s	k7c7
to	ten	k3xDgNnSc1
o	o	k7c6
ní	on	k3xPp3gFnSc6
hovořit	hovořit	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
„	„	k?
<g/>
Žádné	žádný	k3yNgFnSc3
jiné	jiný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
ani	ani	k8xC
věda	věda	k1gFnSc1
není	být	k5eNaImIp3nS
v	v	k7c6
takovém	takový	k3xDgNnSc6
opovržení	opovržení	k1gNnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
každý	každý	k3xTgMnSc1
myslel	myslet	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
přímo	přímo	k6eAd1
vlastní	vlastnit	k5eAaImIp3nS
<g/>
,	,	kIx,
ovládá	ovládat	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
stálého	stálý	k2eAgInSc2d1
světa	svět	k1gInSc2
více	hodně	k6eAd2
méně	málo	k6eAd2
samostatných	samostatný	k2eAgFnPc2d1
podstat	podstata	k1gFnPc2
<g/>
,	,	kIx,
Kantových	Kantových	k2eAgFnPc2d1
„	„	k?
<g/>
věcí	věc	k1gFnPc2
o	o	k7c4
sobě	se	k3xPyFc3
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Hegelův	Hegelův	k2eAgInSc4d1
svět	svět	k1gInSc4
nekonečná	konečný	k2eNgFnSc1d1
souvislost	souvislost	k1gFnSc1
<g/>
,	,	kIx,
plná	plný	k2eAgFnSc1d1
napětí	napětí	k1gNnSc2
<g/>
,	,	kIx,
protikladů	protiklad	k1gInPc2
a	a	k8xC
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Pravdivý	pravdivý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
celek	celek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celek	celek	k1gInSc1
se	se	k3xPyFc4
však	však	k9
dovršuje	dovršovat	k5eAaImIp3nS
jen	jen	k9
ve	v	k7c6
vývoji	vývoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
absolutnu	absolutno	k1gNnSc6
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
bytostně	bytostně	k6eAd1
výsledkem	výsledek	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
teprve	teprve	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
jest	být	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
I	i	k9
lidské	lidský	k2eAgNnSc4d1
vědomí	vědomí	k1gNnSc4
vzniká	vznikat	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
naivní	naivní	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
dítěte	dítě	k1gNnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
bytí	bytí	k1gNnSc2
o	o	k7c4
sobě	se	k3xPyFc3
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
čili	čili	k8xC
these	these	k1gFnPc1
<g/>
)	)	kIx)
nejprve	nejprve	k6eAd1
musí	muset	k5eAaImIp3nS
obrátit	obrátit	k5eAaPmF
ke	k	k7c3
druhému	druhý	k4xOgMnSc3
(	(	kIx(
<g/>
„	„	k?
<g/>
bytí	bytí	k1gNnSc2
pro	pro	k7c4
druhé	druhý	k4xOgFnPc4
<g/>
“	“	k?
čili	čili	k8xC
antithese	antithese	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
teprve	teprve	k6eAd1
v	v	k7c6
(	(	kIx(
<g/>
reflexivním	reflexivní	k2eAgInSc6d1
<g/>
)	)	kIx)
návratu	návrat	k1gInSc6
k	k	k7c3
sobě	se	k3xPyFc3
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nejenom	nejenom	k6eAd1
„	„	k?
<g/>
o	o	k7c4
sobě	se	k3xPyFc3
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k6eAd1
„	„	k?
<g/>
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
“	“	k?
<g/>
,	,	kIx,
lidskou	lidský	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
jádro	jádro	k1gNnSc1
Hegelovy	Hegelův	k2eAgFnSc2d1
dialektiky	dialektika	k1gFnSc2
i	i	k8xC
dalších	další	k2eAgInPc2d1
klíčových	klíčový	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
„	„	k?
<g/>
Ducha	duch	k1gMnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
svobody	svoboda	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůbec	vůbec	k9
pro	pro	k7c4
lidskou	lidský	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gInPc1
vztahy	vztah	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
rodině	rodina	k1gFnSc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
Hegel	Hegel	k1gMnSc1
daleko	daleko	k6eAd1
větší	veliký	k2eAgNnSc4d2
pochopení	pochopení	k1gNnSc4
než	než	k8xS
kdokoli	kdokoli	k3yInSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
předchůdců	předchůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
navázání	navázání	k1gNnSc6
na	na	k7c4
křesťanskou	křesťanský	k2eAgFnSc4d1
myšlenku	myšlenka	k1gFnSc4
dějin	dějiny	k1gFnPc2
spásy	spása	k1gFnSc2
chápe	chápat	k5eAaImIp3nS
Hegel	Hegel	k1gMnSc1
svět	svět	k1gInSc1
jako	jako	k8xC,k8xS
jedno	jeden	k4xCgNnSc1
nesmírné	smírný	k2eNgNnSc1d1
a	a	k8xC
dramatické	dramatický	k2eAgNnSc1d1
dějství	dějství	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
Duch	duch	k1gMnSc1
–	–	k?
oklikou	oklika	k1gFnSc7
a	a	k8xC
díky	díky	k7c3
„	„	k?
<g/>
práci	práce	k1gFnSc3
záporna	záporno	k1gNnSc2
<g/>
“	“	k?
–	–	k?
nakonec	nakonec	k6eAd1
vrací	vracet	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téma	téma	k1gNnSc1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
neustálé	neustálý	k2eAgFnPc1d1
a	a	k8xC
nevratné	vratný	k2eNgFnPc1d1
proměny	proměna	k1gFnPc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
a	a	k8xC
trvalých	trvalý	k2eAgInPc2d1
přínosů	přínos	k1gInPc2
Hegelovy	Hegelův	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
postupně	postupně	k6eAd1
proměnil	proměnit	k5eAaPmAgMnS
i	i	k9
všechny	všechen	k3xTgFnPc1
vědy	věda	k1gFnPc1
–	–	k?
od	od	k7c2
historie	historie	k1gFnSc2
přes	přes	k7c4
biologii	biologie	k1gFnSc4
(	(	kIx(
<g/>
Charles	Charles	k1gMnSc1
Darwin	Darwin	k1gMnSc1
<g/>
)	)	kIx)
až	až	k9
po	po	k7c4
astronomii	astronomie	k1gFnSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
velký	velký	k2eAgInSc4d1
třesk	třesk	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Základem	základ	k1gInSc7
Hegelovy	Hegelův	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
myšlenka	myšlenka	k1gFnSc1
svobody	svoboda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
pro	pro	k7c4
člověka	člověk	k1gMnSc4
možná	možná	k9
právě	právě	k9
jen	jen	k9
v	v	k7c6
dobře	dobře	k6eAd1
uspořádané	uspořádaný	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
vztahu	vztah	k1gInSc6
občana	občan	k1gMnSc2
a	a	k8xC
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hegel	Hegel	k1gInSc1
je	být	k5eAaImIp3nS
tak	tak	k9
jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
myslitelů	myslitel	k1gMnPc2
důsledně	důsledně	k6eAd1
„	„	k?
<g/>
občanské	občanský	k2eAgFnSc2d1
<g/>
“	“	k?
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
neopírá	opírat	k5eNaImIp3nS
o	o	k7c4
posvátné	posvátný	k2eAgFnPc4d1
autority	autorita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
Hegel	Hegel	k1gMnSc1
myslitel	myslitel	k1gMnSc1
spíš	spíš	k9
konzervativní	konzervativní	k2eAgMnPc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
Filosofii	filosofie	k1gFnSc6
práva	právo	k1gNnSc2
<g/>
:	:	kIx,
základem	základ	k1gInSc7
občanské	občanský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
vlastnictví	vlastnictví	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
si	se	k3xPyFc3
osoba	osoba	k1gFnSc1
dává	dávat	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
skutečnost	skutečnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatnou	podstatný	k2eAgFnSc7d1
institucí	instituce	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
rodina	rodina	k1gFnSc1
s	s	k7c7
jasně	jasně	k6eAd1
vymezenými	vymezený	k2eAgFnPc7d1
rolemi	role	k1gFnPc7
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
<g/>
:	:	kIx,
veřejný	veřejný	k2eAgInSc4d1
život	život	k1gInSc4
je	být	k5eAaImIp3nS
záležitost	záležitost	k1gFnSc1
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoké	vysoká	k1gFnSc6
hodnocení	hodnocení	k1gNnSc1
občanské	občanský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
státu	stát	k1gInSc2
Hegel	Hegela	k1gFnPc2
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
přenáší	přenášet	k5eAaImIp3nS
na	na	k7c4
soudobé	soudobý	k2eAgNnSc4d1
Prusko	Prusko	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mu	on	k3xPp3gMnSc3
lze	lze	k6eAd1
právem	právem	k6eAd1
vytýkat	vytýkat	k5eAaImF
<g/>
;	;	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
bylo	být	k5eAaImAgNnS
tehdejší	tehdejší	k2eAgNnSc1d1
Prusko	Prusko	k1gNnSc1
zemí	zem	k1gFnPc2
reforem	reforma	k1gFnPc2
Steinových	Steinových	k2eAgFnPc2d1
<g/>
,	,	kIx,
Hardenbergových	Hardenbergův	k2eAgFnPc2d1
a	a	k8xC
Humboldtových	Humboldtův	k2eAgFnPc2d1
a	a	k8xC
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejliberálnějších	liberální	k2eAgInPc2d3
států	stát	k1gInPc2
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Hegelovým	Hegelův	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
je	být	k5eAaImIp3nS
spojen	spojen	k2eAgInSc1d1
pojem	pojem	k1gInSc1
spirála	spirála	k1gFnSc1
poznání	poznání	k1gNnSc1
či	či	k8xC
vývojová	vývojový	k2eAgFnSc1d1
spirála	spirála	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
vývoj	vývoj	k1gInSc1
či	či	k8xC
poznání	poznání	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
po	po	k7c6
spirále	spirála	k1gFnSc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
v	v	k7c6
soustředných	soustředný	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
určitých	určitý	k2eAgInPc6d1
fázích	fág	k1gInPc6
života	život	k1gInSc2
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
kruh	kruh	k1gInSc1
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
a	a	k8xC
člověk	člověk	k1gMnSc1
postupuje	postupovat	k5eAaImIp3nS
na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
na	na	k7c4
vyšší	vysoký	k2eAgFnSc4d2
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
vzdálenější	vzdálený	k2eAgInSc4d2
soustředný	soustředný	k2eAgInSc4d1
kruh	kruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
spirály	spirála	k1gFnSc2
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
bod	bod	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
spojnicí	spojnice	k1gFnSc7
se	s	k7c7
středem	střed	k1gInSc7
spirály	spirála	k1gFnSc2
vytyčuje	vytyčovat	k5eAaImIp3nS
předchozí	předchozí	k2eAgInPc4d1
vývojové	vývojový	k2eAgInPc4d1
body	bod	k1gInPc4
vztahující	vztahující	k2eAgInPc4d1
se	se	k3xPyFc4
k	k	k7c3
dané	daný	k2eAgFnSc3d1
problematice	problematika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
tak	tak	k9
může	moct	k5eAaImIp3nS
dál	daleko	k6eAd2
stavět	stavět	k5eAaImF
na	na	k7c6
již	již	k6eAd1
poznaném	poznaný	k2eAgInSc6d1
–	–	k?
dialektická	dialektický	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
<g/>
,	,	kIx,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1
bytím	bytí	k1gNnSc7
o	o	k7c4
sobě	se	k3xPyFc3
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
objevuje	objevovat	k5eAaImIp3nS
zvnějšnění	zvnějšnění	k1gNnSc4
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
vně	vně	k7c2
nás	my	k3xPp1nPc2
a	a	k8xC
je	být	k5eAaImIp3nS
obsahem	obsah	k1gInSc7
filosofie	filosofie	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
navrací	navracet	k5eAaBmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
sobě	se	k3xPyFc3
samému	samý	k3xTgMnSc3
<g/>
,	,	kIx,
poznávajícímu	poznávající	k2eAgNnSc3d1
vědomí	vědomí	k1gNnSc3
(	(	kIx(
<g/>
zde	zde	k6eAd1
je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
ducha	duch	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dialektika	dialektika	k1gFnSc1
poznání	poznání	k1gNnSc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
rozdílu	rozdíl	k1gInSc6
identity	identita	k1gFnSc2
a	a	k8xC
ne-identity	ne-identit	k1gInPc4
(	(	kIx(
<g/>
shodné	shodný	k2eAgInPc4d1
a	a	k8xC
rozdílné	rozdílný	k2eAgInPc4d1
znaky	znak	k1gInPc4
poznávaného	poznávaný	k2eAgNnSc2d1
jsoucna	jsoucno	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vliv	vliv	k1gInSc1
</s>
<s>
Ve	v	k7c6
Fenomenologii	fenomenologie	k1gFnSc6
ducha	duch	k1gMnSc2
znázorňuje	znázorňovat	k5eAaImIp3nS
Hegel	Hegel	k1gInSc1
dialektický	dialektický	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
ducha	duch	k1gMnSc2
na	na	k7c6
slavném	slavný	k2eAgInSc6d1
příkladu	příklad	k1gInSc6
„	„	k?
<g/>
pána	pán	k1gMnSc4
a	a	k8xC
raba	rab	k1gMnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
inspirovaném	inspirovaný	k2eAgInSc6d1
Hérakleitem	Hérakleitum	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
střetu	střet	k1gInSc6
na	na	k7c4
život	život	k1gInSc4
a	a	k8xC
na	na	k7c4
smrt	smrt	k1gFnSc4
se	se	k3xPyFc4
rabem	rab	k1gMnSc7
stává	stávat	k5eAaImIp3nS
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
bojí	bát	k5eAaImIp3nS
smrti	smrt	k1gFnSc2
a	a	k8xC
uzná	uznat	k5eAaPmIp3nS
druhého	druhý	k4xOgMnSc4
za	za	k7c2
svého	svůj	k3xOyFgMnSc4
pána	pán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
však	však	k9
postupně	postupně	k6eAd1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
závislosti	závislost	k1gFnSc2
na	na	k7c6
rabově	rabův	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
rab	rab	k1gMnSc1
se	se	k3xPyFc4
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
dovednosti	dovednost	k1gFnSc3
stává	stávat	k5eAaImIp3nS
svobodným	svobodný	k2eAgNnSc7d1
<g/>
,	,	kIx,
tj.	tj.	kA
tvořivým	tvořivý	k2eAgMnSc7d1
člověkem	člověk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
motiv	motiv	k1gInSc4
dialektiky	dialektika	k1gFnSc2
a	a	k8xC
dějinného	dějinný	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
mohla	moct	k5eAaImAgFnS
využít	využít	k5eAaPmF
Marxova	Marxův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
opřená	opřený	k2eAgFnSc1d1
spíše	spíše	k9
o	o	k7c4
Adama	Adam	k1gMnSc4
Smithe	Smith	k1gMnSc4
a	a	k8xC
D.	D.	kA
Ricarda	Ricarda	k1gFnSc1
<g/>
,	,	kIx,
Hegela	Hegela	k1gFnSc1
však	však	k9
musela	muset	k5eAaImAgFnS
obrátit	obrátit	k5eAaPmF
<g/>
,	,	kIx,
„	„	k?
<g/>
postavit	postavit	k5eAaPmF
z	z	k7c2
hlavy	hlava	k1gFnSc2
na	na	k7c4
nohy	noha	k1gFnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
říká	říkat	k5eAaImIp3nS
Marx	Marx	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tedy	tedy	k9
paradoxní	paradoxní	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Hegel	Hegel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vždy	vždy	k6eAd1
platil	platit	k5eAaImAgMnS
i	i	k8xC
působil	působit	k5eAaImAgMnS
jako	jako	k9
filosof	filosof	k1gMnSc1
svobody	svoboda	k1gFnSc2
(	(	kIx(
<g/>
proto	proto	k8xC
např.	např.	kA
český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
Filosofie	filosofie	k1gFnSc1
práva	právo	k1gNnSc2
mohl	moct	k5eAaImAgInS
vyjít	vyjít	k5eAaPmF
až	až	k9
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pro	pro	k7c4
K.	K.	kA
R.	R.	kA
Poppera	Popper	k1gMnSc2
(	(	kIx(
<g/>
vedle	vedle	k7c2
Platóna	Platón	k1gMnSc2
<g/>
)	)	kIx)
myslitelem	myslitel	k1gMnSc7
totality	totalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hegelův	Hegelův	k2eAgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c6
pozdější	pozdní	k2eAgFnSc6d2
filosofii	filosofie	k1gFnSc6
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
mnohotvárný	mnohotvárný	k2eAgInSc1d1
a	a	k8xC
většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gMnPc2
následovníků	následovník	k1gMnPc2
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
stavěla	stavět	k5eAaImAgFnS
kriticky	kriticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
platí	platit	k5eAaImIp3nS
jak	jak	k8xC,k8xS
o	o	k7c6
Bruno	Bruno	k1gMnSc1
Bauerovi	Bauerův	k2eAgMnPc1d1
nebo	nebo	k8xC
S.	S.	kA
Kierkegaardovi	Kierkegaardův	k2eAgMnPc1d1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
o	o	k7c6
Feuerbachovi	Feuerbach	k1gMnSc6
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgNnSc2
hegelovské	hegelovský	k2eAgInPc1d1
motivy	motiv	k1gInPc1
převzal	převzít	k5eAaPmAgMnS
Marx	Marx	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
významná	významný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
hegelovců	hegelovec	k1gMnPc2
(	(	kIx(
<g/>
František	František	k1gMnSc1
Matouš	Matouš	k1gMnSc1
Klácel	Klácel	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Bratránek	bratránek	k1gMnSc1
<g/>
,	,	kIx,
I.	I.	kA
Hanuš	Hanuš	k1gMnSc1
<g/>
,	,	kIx,
Augustin	Augustin	k1gMnSc1
Smetana	Smetana	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Rusku	Rusko	k1gNnSc6
na	na	k7c4
Hegela	Hegel	k1gMnSc4
navázal	navázat	k5eAaPmAgMnS
V.	V.	kA
G.	G.	kA
Bělinskij	Bělinskij	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
hegelovskou	hegelovský	k2eAgFnSc7d1
filosofií	filosofie	k1gFnSc7
inspirovali	inspirovat	k5eAaBmAgMnP
např.	např.	kA
W.	W.	kA
Dilthey	Dilthea	k1gMnSc2
<g/>
,	,	kIx,
A.	A.	kA
N.	N.	kA
Whitehead	Whitehead	k1gInSc1
<g/>
,	,	kIx,
G.	G.	kA
Lukács	Lukács	k1gInSc1
nebo	nebo	k8xC
Frankfurtská	frankfurtský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
A.	A.	kA
Kojè	Kojè	k1gFnSc2
a	a	k8xC
J.	J.	kA
<g/>
-	-	kIx~
<g/>
P.	P.	kA
Sartre	Sartr	k1gInSc5
a	a	k8xC
v	v	k7c6
Itálii	Itálie	k1gFnSc6
B.	B.	kA
Croce	Croce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Dějiny	dějiny	k1gFnPc1
filosofie	filosofie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1961	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1
filozofických	filozofický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
I.	I.	kA
Malá	Malá	k1gFnSc1
logika	logika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1992	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-205-0153-3	80-205-0153-3	k4
</s>
<s>
Estetika	estetika	k1gFnSc1
I.	I.	kA
<g/>
/	/	kIx~
<g/>
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fenomenologie	fenomenologie	k1gFnSc1
ducha	duch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1960	#num#	k4
</s>
<s>
Filosofie	filosofie	k1gFnSc1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-86559-19-X	80-86559-19-X	k4
</s>
<s>
Filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
umění	umění	k1gNnSc1
a	a	k8xC
náboženství	náboženství	k1gNnSc1
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
vztah	vztah	k1gInSc1
k	k	k7c3
mravnosti	mravnost	k1gFnSc3
a	a	k8xC
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1943	#num#	k4
</s>
<s>
Úvod	úvod	k1gInSc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1952	#num#	k4
</s>
<s>
Základy	základ	k1gInPc1
filosofie	filosofie	k1gFnSc2
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1992	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-200-0296-0	80-200-0296-0	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BUTLER	BUTLER	kA
<g/>
,	,	kIx,
Judith	Judith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Subjects	Subjects	k1gInSc1
of	of	k?
desire	desir	k1gInSc5
<g/>
:	:	kIx,
Hegelian	Hegelian	k1gInSc1
reflections	reflections	k1gInSc1
in	in	k?
twentieth-century	twentieth-centura	k1gFnSc2
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Filosofický	filosofický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
FIN.	FIN.	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
163	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Základy	základ	k1gInPc1
filosofie	filosofie	k1gFnSc1
práva	právo	k1gNnPc1
<g/>
,	,	kIx,
Úvod	úvod	k1gInSc1
<g/>
↑	↑	k?
Hérakleitos	Hérakleitos	k1gInSc1
<g/>
,	,	kIx,
zl	zl	k?
<g/>
.	.	kIx.
22	#num#	k4
B	B	kA
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Krejčí	Krejčí	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
:	:	kIx,
Hegelova	Hegelův	k2eAgFnSc1d1
filozofie	filozofie	k1gFnSc1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1933	#num#	k4
</s>
<s>
Major	major	k1gMnSc1
–	–	k?
Sobotka	Sobotka	k1gMnSc1
<g/>
:	:	kIx,
G.	G.	kA
W.	W.	kA
F.	F.	kA
Hegel	Hegel	k1gInSc4
<g/>
,	,	kIx,
život	život	k1gInSc4
a	a	k8xC
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1975	#num#	k4
</s>
<s>
Popper	Popper	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
R.	R.	kA
<g/>
:	:	kIx,
Otevřená	otevřený	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
nepřátelé	nepřítel	k1gMnPc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1994	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85241-54-4	80-85241-54-4	k4
</s>
<s>
Singer	Singer	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
:	:	kIx,
Hegel	Hegel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1995	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85794-46-2	80-85794-46-2	k4
</s>
<s>
Benyovszky	Benyovszek	k1gInPc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
:	:	kIx,
Problém	problém	k1gInSc1
bytí	bytí	k1gNnSc2
v	v	k7c6
Hegelově	Hegelův	k2eAgFnSc6d1
filosofii	filosofie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7184-770-4	80-7184-770-4	k4
</s>
<s>
SCRUTON	SCRUTON	kA
<g/>
,	,	kIx,
Roger	Roger	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzervativní	konzervativní	k2eAgMnPc1d1
myslitelé	myslitel	k1gMnPc1
:	:	kIx,
výbor	výbor	k1gInSc1
esejů	esej	k1gInPc2
z	z	k7c2
britského	britský	k2eAgInSc2d1
konzervativního	konzervativní	k2eAgInSc2d1
čtvrtletníku	čtvrtletník	k1gInSc2
The	The	k1gFnSc2
Salisbury	Salisbura	k1gFnSc2
Review	Review	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
196	#num#	k4
s.	s.	k?
</s>
<s>
Schaefer	Schaefer	k1gMnSc1
<g/>
,	,	kIx,
Rainer	Rainer	k1gMnSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Dialektik	dialektik	k1gMnSc1
und	und	k?
ihre	ihr	k1gFnSc2
besonderen	besonderna	k1gFnPc2
Formen	Formen	k1gInSc1
in	in	k?
Hegels	Hegels	k1gInSc1
Logik	logika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hegel-Studien	Hegel-Studien	k2eAgInSc1d1
Beiheft	Beiheft	k1gInSc1
45	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
<g/>
/	/	kIx~
<g/>
Meiner	Meiner	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-7873-1585-3	3-7873-1585-3	k4
</s>
<s>
Iljin	Iljin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
<g/>
,	,	kIx,
Hegelova	Hegelův	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
jako	jako	k8xS,k8xC
nauka	nauka	k1gFnSc1
o	o	k7c6
konkrétnosti	konkrétnost	k1gFnSc6
Boha	bůh	k1gMnSc2
a	a	k8xC
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Refugium	refugium	k1gNnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7412-000-8	978-80-7412-000-8	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Georg	Georg	k1gInSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegela	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegela	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Internationale	Internationale	k1gMnSc1
Hegel-Gesellschaft	Hegel-Gesellschaft	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
německá	německý	k2eAgFnSc1d1
Hegel	Hegel	k1gInSc1
Wiki	Wik	k1gMnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stanford	Stanford	k1gInSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosoph	k1gInPc1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Hegel	Hegel	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Hegelův	Hegelův	k2eAgInSc1d1
životopis	životopis	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Přednášky	přednáška	k1gFnPc1
o	o	k7c6
estetice	estetika	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc1
Hegel	Hegela	k1gFnPc2
Society	societa	k1gFnSc2
of	of	k?
America	America	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990003303	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118547739	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2282	#num#	k4
8149	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79021767	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500221954	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
89774942	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79021767	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
