<s desamb="1">
Hegelovým	Hegelův	k2eAgInSc7d1
hlavním	hlavní	k2eAgInSc7d1
přínosem	přínos	k1gInSc7
je	být	k5eAaImIp3nS
objev	objev	k1gInSc1
dějinnosti	dějinnost	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
svět	svět	k1gInSc1
není	být	k5eNaImIp3nS
neměnné	měnný	k2eNgNnSc4d1
<g/>
,	,	kIx,
stálé	stálý	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jedno	jeden	k4xCgNnSc1
nesmírné	smírný	k2eNgNnSc1d1
dějství	dějství	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
světový	světový	k2eAgMnSc1d1
duch	duch	k1gMnSc1
hledá	hledat	k5eAaImIp3nS
cestu	cesta	k1gFnSc4
sám	sám	k3xTgMnSc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>