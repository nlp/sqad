<s>
Sudety	Sudety	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
Sudetsko	Sudetsko	k1gNnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Sudetenland	Sudetenland	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Kraj	kraj	k1gInSc1
Sudetów	Sudetów	k1gFnSc2
nebo	nebo	k8xC
Kraj	kraj	k1gInSc1
Sudecki	Sudecki	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
češtině	čeština	k1gFnSc6
nesprávné	správný	k2eNgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
ty	ten	k3xDgFnPc4
pohraniční	pohraniční	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgFnPc6
od	od	k7c2
středověku	středověk	k1gInSc2
až	až	k6eAd1
do	do	k7c2
let	léto	k1gNnPc2
1945-1947	1945-1947	k4
převažovalo	převažovat	k5eAaImAgNnS
německé	německý	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
<g/>
.	.	kIx.
</s>