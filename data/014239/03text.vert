<s>
Sudety	Sudety	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
pohraniční	pohraniční	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Sudety	Sudety	k1gFnPc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Oblasti	oblast	k1gFnPc1
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
osídlené	osídlený	k2eAgNnSc1d1
podle	podle	k7c2
rakouského	rakouský	k2eAgNnSc2d1
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
převážně	převážně	k6eAd1
německojazyčným	německojazyčný	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
(	(	kIx(
<g/>
národnost	národnost	k1gFnSc1
se	se	k3xPyFc4
určovala	určovat	k5eAaImAgFnS
dle	dle	k7c2
tzv.	tzv.	kA
obcovací	obcovací	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
německých	německý	k2eAgNnPc2d1
nářečí	nářečí	k1gNnPc2
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
zobrazuje	zobrazovat	k5eAaImIp3nS
regionální	regionální	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
v	v	k7c6
pohraničí	pohraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihočeské	jihočeský	k2eAgInPc1d1
a	a	k8xC
jihomoravské	jihomoravský	k2eAgInPc1d1
byly	být	k5eAaImAgFnP
spřízněny	spříznit	k5eAaPmNgFnP
s	s	k7c7
hornoněmeckým	hornoněmecký	k2eAgNnSc7d1
nářečím	nářečí	k1gNnSc7
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Bavorsko	Bavorsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severočeské	severočeský	k2eAgInPc1d1
a	a	k8xC
severomoravské	severomoravský	k2eAgInPc1d1
dialekty	dialekt	k1gInPc1
pak	pak	k9
se	s	k7c7
středovýchodním	středovýchodní	k2eAgNnSc7d1
nářečím	nářečí	k1gNnSc7
(	(	kIx(
<g/>
Sasko	Sasko	k1gNnSc1
<g/>
,	,	kIx,
Slezsko	Slezsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Oblasti	oblast	k1gFnPc1
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
osídlené	osídlený	k2eAgNnSc1d1
podle	podle	k7c2
československého	československý	k2eAgNnSc2d1
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
převážně	převážně	k6eAd1
sudetoněmeckým	sudetoněmecký	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
(	(	kIx(
<g/>
národnost	národnost	k1gFnSc1
se	se	k3xPyFc4
určovala	určovat	k5eAaImAgFnS
podle	podle	k7c2
národnostní	národnostní	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
a	a	k8xC
mateřské	mateřský	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Sudety	Sudety	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
Sudetsko	Sudetsko	k1gNnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Sudetenland	Sudetenland	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Kraj	kraj	k1gInSc1
Sudetów	Sudetów	k1gFnSc2
nebo	nebo	k8xC
Kraj	kraj	k1gInSc1
Sudecki	Sudecki	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
češtině	čeština	k1gFnSc6
nesprávné	správný	k2eNgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
ty	ten	k3xDgFnPc4
pohraniční	pohraniční	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgFnPc6
od	od	k7c2
středověku	středověk	k1gInSc2
až	až	k6eAd1
do	do	k7c2
let	léto	k1gNnPc2
1945-1947	1945-1947	k4
převažovalo	převažovat	k5eAaImAgNnS
německé	německý	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československé	československý	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
používání	používání	k1gNnSc4
slova	slovo	k1gNnSc2
„	„	k?
<g/>
Sudety	Sudety	k1gInPc1
<g/>
“	“	k?
v	v	k7c6
úředním	úřední	k2eAgInSc6d1
styku	styk	k1gInSc6
vyhláškou	vyhláška	k1gFnSc7
zakázalo	zakázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
jako	jako	k8xC,k8xS
historicko-geografický	historicko-geografický	k2eAgInSc1d1
pojem	pojem	k1gInSc1
částečně	částečně	k6eAd1
rehabilitováno	rehabilitován	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Sudety	Sudety	k1gFnPc4
je	být	k5eAaImIp3nS
původně	původně	k6eAd1
označení	označení	k1gNnSc1
Krkonošsko-jesenické	krkonošsko-jesenický	k2eAgFnSc2d1
subprovincie	subprovincie	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
němčině	němčina	k1gFnSc6
(	(	kIx(
<g/>
Sudeten	Sudeten	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
polštině	polština	k1gFnSc3
(	(	kIx(
<g/>
Sudety	Sudety	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
Sudety	Sudety	k1gInPc7
vlivem	vlivem	k7c2
nesprávného	správný	k2eNgNnSc2d1
užívání	užívání	k1gNnSc2
pro	pro	k7c4
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
pohraniční	pohraniční	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
již	již	k6eAd1
používá	používat	k5eAaImIp3nS
méně	málo	k6eAd2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
české	český	k2eAgFnSc6d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc6d1
geomorfologii	geomorfologie	k1gFnSc6
nahrazen	nahradit	k5eAaPmNgInS
novotvarem	novotvar	k1gInSc7
Krkonošsko-jesenická	krkonošsko-jesenický	k2eAgFnSc1d1
subprovincie	subprovincie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Toponomastika	toponomastika	k1gFnSc1
pojmu	pojmout	k5eAaPmIp1nS
Sudety	Sudety	k1gInPc4
</s>
<s>
Původ	původ	k1gInSc1
názvu	název	k1gInSc2
Sudety	Sudety	k1gInPc4
není	být	k5eNaImIp3nS
jednoznačný	jednoznačný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
uváděný	uváděný	k2eAgInSc1d1
výklad	výklad	k1gInSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
původní	původní	k2eAgInSc1d1
fyzickogeografický	fyzickogeografický	k2eAgInSc1d1
termín	termín	k1gInSc1
Sudéta	Sudét	k1gInSc2
je	být	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
keltského	keltský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
složené	složený	k2eAgInPc1d1
z	z	k7c2
gramatického	gramatický	k2eAgInSc2d1
základu	základ	k1gInSc2
sud-	sud-	k?
(	(	kIx(
<g/>
kanec	kanec	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
přípony	přípona	k1gFnSc2
-éta	-ét	k1gInSc2
(	(	kIx(
<g/>
les	les	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dohromady	dohromady	k6eAd1
tedy	tedy	k9
„	„	k?
<g/>
Les	les	k1gInSc1
kanců	kanec	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jiný	jiný	k2eAgInSc1d1
výklad	výklad	k1gInSc1
<g/>
,	,	kIx,
například	například	k6eAd1
pomocí	pomocí	k7c2
starogermánského	starogermánský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
Sudtha	Sudtha	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
překládané	překládaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
les	les	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
je	být	k5eAaImIp3nS
doložený	doložený	k2eAgInSc1d1
již	již	k6eAd1
ve	v	k7c6
starověku	starověk	k1gInSc6
u	u	k7c2
Klaudia	Klaudium	k1gNnSc2
Ptolemaia	Ptolemaios	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
však	však	k9
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
jestli	jestli	k8xS
skutečně	skutečně	k6eAd1
toto	tento	k3xDgNnSc4
slovo	slovo	k1gNnSc4
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
mapě	mapa	k1gFnSc6
označovalo	označovat	k5eAaImAgNnS
stejné	stejný	k2eAgNnSc1d1
pohoří	pohoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Bohaté	bohatý	k2eAgInPc1d1
Sudety	Sudety	k1gInPc1
a	a	k8xC
Chudé	Chudé	k2eAgInPc1d1
Sudety	Sudety	k1gInPc1
</s>
<s>
Sociální	sociální	k2eAgMnSc1d1
geograf	geograf	k1gMnSc1
Radim	Radim	k1gMnSc1
Perlín	perlín	k1gMnSc1
dělí	dělit	k5eAaImIp3nS
Sudety	Sudety	k1gInPc4
na	na	k7c6
tzv.	tzv.	kA
Bohaté	bohatý	k2eAgFnPc4d1
Sudety	Sudety	k1gFnPc4
a	a	k8xC
tzv.	tzv.	kA
Chudé	Chudé	k2eAgInPc1d1
Sudety	Sudety	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
Bohaté	bohatý	k2eAgInPc4d1
Sudety	Sudety	k1gInPc4
je	být	k5eAaImIp3nS
vymezena	vymezit	k5eAaPmNgFnS
na	na	k7c6
základě	základ	k1gInSc6
bývalé	bývalý	k2eAgFnSc2d1
česko-německé	česko-německý	k2eAgFnSc2d1
etnické	etnický	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
rozšíření	rozšíření	k1gNnSc1
původního	původní	k2eAgNnSc2d1
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
pásu	pás	k1gInSc6
osídlení	osídlení	k1gNnSc2
podél	podél	k7c2
severozápadní	severozápadní	k2eAgFnSc2d1
a	a	k8xC
severovýchodní	severovýchodní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohaté	bohatý	k2eAgInPc1d1
Sudety	Sudety	k1gInPc1
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
v	v	k7c6
prostoru	prostor	k1gInSc6
od	od	k7c2
ašského	ašský	k2eAgInSc2d1
výběžku	výběžek	k1gInSc2
přes	přes	k7c4
Karlovarsko	Karlovarsko	k1gNnSc4
<g/>
,	,	kIx,
severočeskou	severočeský	k2eAgFnSc4d1
konurbaci	konurbace	k1gFnSc4
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc4
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
,	,	kIx,
Liberecko	Liberecko	k1gNnSc1
<g/>
,	,	kIx,
krkonošské	krkonošský	k2eAgNnSc1d1
a	a	k8xC
orlické	orlický	k2eAgNnSc1d1
podhůří	podhůří	k1gNnSc1
až	až	k9
na	na	k7c4
Jesenicko	Jesenicko	k1gNnSc4
a	a	k8xC
Opavsko	Opavsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Bohaté	bohatý	k2eAgInPc4d1
Sudety	Sudety	k1gInPc4
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
industrializace	industrializace	k1gFnSc1
a	a	k8xC
urbanizace	urbanizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
Chudé	Chudé	k2eAgFnPc1d1
Sudety	Sudety	k1gFnPc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
podél	podél	k7c2
jihozápadní	jihozápadní	k2eAgFnSc2d1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chudé	Chudé	k2eAgInPc1d1
Sudety	Sudety	k1gInPc1
jsou	být	k5eAaImIp3nP
vymezeny	vymezit	k5eAaPmNgInP
na	na	k7c6
bázi	báze	k1gFnSc6
původní	původní	k2eAgFnSc2d1
etnické	etnický	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Bohatých	bohatý	k2eAgFnPc2d1
Sudet	Sudety	k1gFnPc2
v	v	k7c6
rurálním	rurální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Sudet	Sudety	k1gInPc2
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Již	již	k6eAd1
před	před	k7c7
první	první	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
Sudetech	Sudety	k1gInPc6
pangermánské	pangermánský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
DAP	DAP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
nepřímo	přímo	k6eNd1
<g/>
)	)	kIx)
vznikla	vzniknout	k5eAaPmAgFnS
NSDAP	NSDAP	kA
a	a	k8xC
SdP	SdP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
spory	spor	k1gInPc4
o	o	k7c4
přesnou	přesný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
hranic	hranice	k1gFnPc2
nově	nově	k6eAd1
vzniklého	vzniklý	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
vedly	vést	k5eAaImAgFnP
k	k	k7c3
několika	několik	k4yIc3
ozbrojeným	ozbrojený	k2eAgInPc3d1
konfliktům	konflikt	k1gInPc3
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
nejdramatičtější	dramatický	k2eAgMnSc1d3
byl	být	k5eAaImAgMnS
s	s	k7c7
Polskem	Polsko	k1gNnSc7
o	o	k7c4
jižní	jižní	k2eAgFnSc4d1
část	část	k1gFnSc4
Slezska	Slezsko	k1gNnSc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
nebyly	být	k5eNaImAgFnP
Sudety	Sudety	k1gFnPc1
nijak	nijak	k6eAd1
přesněji	přesně	k6eAd2
vymezeny	vymezen	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1
autonomní	autonomní	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
</s>
<s>
Po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
vyhlásili	vyhlásit	k5eAaPmAgMnP
Němci	Němec	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
pohraničních	pohraniční	k2eAgFnPc6d1
částech	část	k1gFnPc6
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
a	a	k8xC
bývalého	bývalý	k2eAgNnSc2d1
Rakouského	rakouský	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
ve	v	k7c6
dnech	den	k1gInPc6
29	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
následující	následující	k2eAgFnPc1d1
čtyři	čtyři	k4xCgFnPc1
autonomní	autonomní	k2eAgFnPc1d1
provincie	provincie	k1gFnPc1
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgFnPc1d1
se	se	k3xPyFc4
podél	podél	k7c2
hranic	hranice	k1gFnPc2
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
a	a	k8xC
Německem	Německo	k1gNnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
požadovaly	požadovat	k5eAaImAgFnP
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
přičlenění	přičlenění	k1gNnSc4
k	k	k7c3
Německému	německý	k2eAgNnSc3d1
Rakousku	Rakousko	k1gNnSc3
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
k	k	k7c3
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc3d1
Německé	německý	k2eAgFnSc3d1
republice	republika	k1gFnSc3
<g/>
:	:	kIx,
</s>
<s>
Deutschböhmen	Deutschböhmen	k1gInSc1
(	(	kIx(
<g/>
Německé	německý	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInSc1d1
západ	západ	k1gInSc1
<g/>
,	,	kIx,
severozápad	severozápad	k1gInSc1
a	a	k8xC
sever	sever	k1gInSc1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Liberci	Liberec	k1gInSc6
(	(	kIx(
<g/>
německy	německy	k6eAd1
Reichenberg	Reichenberg	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sudetenland	Sudetenland	k1gInSc1
(	(	kIx(
<g/>
Sudetsko	Sudetsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInSc1d1
severovýchod	severovýchod	k1gInSc1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
severozápad	severozápad	k1gInSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
bývalého	bývalý	k2eAgNnSc2d1
Rakouského	rakouský	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Opavě	Opava	k1gFnSc6
(	(	kIx(
<g/>
Troppau	Troppaus	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mělo	mít	k5eAaImAgNnS
rozlohu	rozloha	k1gFnSc4
přibližně	přibližně	k6eAd1
6	#num#	k4
543	#num#	k4
km²	km²	k?
a	a	k8xC
asi	asi	k9
650	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Böhmerwaldgau	Böhmerwaldgau	k5eAaPmIp1nS
(	(	kIx(
<g/>
Šumavská	šumavský	k2eAgFnSc1d1
župa	župa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInSc1d1
jihozápad	jihozápad	k1gInSc1
<g/>
,	,	kIx,
jih	jih	k1gInSc1
a	a	k8xC
jihovýchod	jihovýchod	k1gInSc1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
(	(	kIx(
<g/>
Böhmisch	Böhmisch	k1gInSc1
Krummau	Krummaus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelé	představitel	k1gMnPc1
tohoto	tento	k3xDgInSc2
celku	celek	k1gInSc2
deklarovali	deklarovat	k5eAaBmAgMnP
spojení	spojení	k1gNnSc4
s	s	k7c7
Horními	horní	k2eAgInPc7d1
Rakousy	Rakousy	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Deutschsüdmähren	Deutschsüdmährna	k1gFnPc2
(	(	kIx(
<g/>
Německá	německý	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInSc1d1
jih	jih	k1gInSc1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
centrem	centrum	k1gNnSc7
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
(	(	kIx(
<g/>
Znaim	Znaim	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelé	představitel	k1gMnPc1
tohoto	tento	k3xDgInSc2
celku	celek	k1gInSc2
deklarovali	deklarovat	k5eAaBmAgMnP
spojení	spojení	k1gNnSc4
s	s	k7c7
Dolními	dolní	k2eAgInPc7d1
Rakousy	Rakousy	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínala	začínat	k5eAaImAgFnS
u	u	k7c2
Břeclavi	Břeclav	k1gFnSc2
(	(	kIx(
<g/>
Poštorná	Poštorný	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
zahrnovala	zahrnovat	k5eAaImAgFnS
Mikulovsko	Mikulovsko	k1gNnSc4
<g/>
,	,	kIx,
Znojemsko	Znojemsko	k1gNnSc1
a	a	k8xC
Slavonicko	Slavonicko	k1gNnSc1
(	(	kIx(
<g/>
Mikulov	Mikulov	k1gInSc1
byl	být	k5eAaImAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
okresním	okresní	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
okresním	okresní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Břeclav	Břeclav	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
také	také	k9
došlo	dojít	k5eAaPmAgNnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
obcích	obec	k1gFnPc6
k	k	k7c3
přečíslování	přečíslování	k1gNnSc3
všech	všecek	k3xTgNnPc2
popisných	popisný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
a	a	k8xC
všechny	všechen	k3xTgInPc1
domy	dům	k1gInPc1
dostaly	dostat	k5eAaPmAgInP
nová	nový	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
popisná	popisný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvníci	návštěvník	k1gMnPc1
z	z	k7c2
Německa	Německo	k1gNnSc2
nebo	nebo	k8xC
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
obraceli	obracet	k5eAaImAgMnP
na	na	k7c4
místní	místní	k2eAgMnPc4d1
kronikáře	kronikář	k1gMnPc4
se	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
při	při	k7c6
hledání	hledání	k1gNnSc6
domu	dům	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
kdysi	kdysi	k6eAd1
bydleli	bydlet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
těchto	tento	k3xDgFnPc2
čtyřech	čtyři	k4xCgFnPc2
oblastí	oblast	k1gFnPc2
stálo	stát	k5eAaImAgNnS
Novobystřicko	Novobystřicko	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
připojené	připojený	k2eAgFnSc2d1
rovněž	rovněž	k6eAd1
k	k	k7c3
Dolním	dolní	k2eAgInPc3d1
Rakousům	Rakousy	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
ještě	ještě	k6eAd1
jazykový	jazykový	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Jihlava	Jihlava	k1gFnSc1
s	s	k7c7
22	#num#	k4
okolními	okolní	k2eAgFnPc7d1
vesnicemi	vesnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
oblastmi	oblast	k1gFnPc7
obývanými	obývaný	k2eAgFnPc7d1
převážně	převážně	k6eAd1
německou	německý	k2eAgFnSc7d1
populací	populace	k1gFnSc7
<g/>
,	,	kIx,
respektive	respektive	k9
mezi	mezi	k7c7
jejich	jejich	k3xOp3gMnPc7
zástupci	zástupce	k1gMnPc7
<g/>
,	,	kIx,
však	však	k9
neexistovala	existovat	k5eNaImAgFnS
politická	politický	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československo	Československo	k1gNnSc1
obnovilo	obnovit	k5eAaPmAgNnS
svoji	svůj	k3xOyFgFnSc4
územní	územní	k2eAgFnSc4d1
integritu	integrita	k1gFnSc4
se	s	k7c7
všemi	všecek	k3xTgNnPc7
odtrženými	odtržený	k2eAgNnPc7d1
územími	území	k1gNnPc7
jejich	jejich	k3xOp3gNnSc7
vojenským	vojenský	k2eAgNnSc7d1
obsazením	obsazení	k1gNnSc7
prakticky	prakticky	k6eAd1
bez	bez	k7c2
větších	veliký	k2eAgFnPc2d2
vojenských	vojenský	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
mezi	mezi	k7c7
1	#num#	k4
<g/>
.	.	kIx.
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
prosincem	prosinec	k1gInSc7
1918	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
pokus	pokus	k1gInSc1
o	o	k7c4
odtržení	odtržení	k1gNnSc4
nezdařil	zdařit	k5eNaPmAgMnS
a	a	k8xC
tyto	tento	k3xDgInPc1
čtyři	čtyři	k4xCgInPc1
celky	celek	k1gInPc1
přestaly	přestat	k5eAaPmAgInP
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německé	německý	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
protestovat	protestovat	k5eAaBmF
a	a	k8xC
demonstrovat	demonstrovat	k5eAaBmF
za	za	k7c4
tzv.	tzv.	kA
práva	právo	k1gNnPc4
národů	národ	k1gInPc2
na	na	k7c4
sebeurčení	sebeurčení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
demonstrace	demonstrace	k1gFnPc1
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
Moravské	moravský	k2eAgFnPc1d1
Třebová	Třebová	k1gFnSc1
<g/>
,	,	kIx,
Šternberku	Šternberk	k1gInSc6
nebo	nebo	k8xC
Kadani	Kadaň	k1gFnSc6
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
podpořeny	podpořit	k5eAaPmNgFnP
jednodenní	jednodenní	k2eAgFnSc7d1
generální	generální	k2eAgFnSc7d1
stávkou	stávka	k1gFnSc7
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protesty	protest	k1gInPc7
byly	být	k5eAaImAgInP
v	v	k7c6
březnu	březen	k1gInSc6
1919	#num#	k4
násilně	násilně	k6eAd1
potlačeny	potlačen	k2eAgInPc1d1
československou	československý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
54	#num#	k4
osob	osoba	k1gFnPc2
při	při	k7c6
tom	ten	k3xDgInSc6
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
a	a	k8xC
750	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
<g/>
,	,	kIx,
mezi	mezi	k7c7
oběťmi	oběť	k1gFnPc7
byly	být	k5eAaImAgFnP
i	i	k9
ženy	žena	k1gFnPc1
a	a	k8xC
děti	dítě	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
oběti	oběť	k1gFnPc1
coby	coby	k?
„	„	k?
<g/>
březnoví	březnový	k2eAgMnPc1d1
padlí	padlý	k1gMnPc1
<g/>
“	“	k?
propagandisticky	propagandisticky	k6eAd1
využívány	využíván	k2eAgMnPc4d1
nacionalisty	nacionalista	k1gMnPc4
k	k	k7c3
eskalaci	eskalace	k1gFnSc3
konfliktů	konflikt	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
pohraničních	pohraniční	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
samy	sám	k3xTgFnPc1
začaly	začít	k5eAaPmAgFnP
označovat	označovat	k5eAaImF
jako	jako	k9
Sudety	Sudety	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
přestal	přestat	k5eAaPmAgInS
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
odporu	odpor	k1gInSc2
ztrácet	ztrácet	k5eAaImF
na	na	k7c6
intenzitě	intenzita	k1gFnSc6
<g/>
,	,	kIx,
pojem	pojem	k1gInSc1
„	„	k?
<g/>
sudetský	sudetský	k2eAgInSc1d1
<g/>
“	“	k?
přetrval	přetrvat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
identifikace	identifikace	k1gFnSc1
se	se	k3xPyFc4
zbytkem	zbytek	k1gInSc7
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
Němců	Němec	k1gMnPc2
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
až	až	k6eAd1
do	do	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
nebylo	být	k5eNaImAgNnS
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
používáno	používán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Hranice	hranice	k1gFnSc1
Československa	Československo	k1gNnSc2
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byly	být	k5eAaImAgInP
určovány	určovat	k5eAaImNgInP
mírovými	mírový	k2eAgFnPc7d1
smlouvami	smlouva	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
potvrzovaly	potvrzovat	k5eAaImAgFnP
vítězné	vítězný	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
východě	východ	k1gInSc6
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byly	být	k5eAaImAgFnP
hranice	hranice	k1gFnPc1
potvrzeny	potvrzen	k2eAgFnPc1d1
tzv.	tzv.	kA
Trianonskou	trianonský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
se	s	k7c7
sousedním	sousední	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
a	a	k8xC
Rakouskem	Rakousko	k1gNnSc7
byly	být	k5eAaImAgFnP
potvrzeny	potvrzen	k2eAgFnPc1d1
na	na	k7c6
Pařížské	pařížský	k2eAgFnSc6d1
mírové	mírový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
už	už	k6eAd1
o	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
také	také	k9
potvrzena	potvrzen	k2eAgFnSc1d1
územní	územní	k2eAgFnSc1d1
celistvost	celistvost	k1gFnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
s	s	k7c7
Německem	Německo	k1gNnSc7
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
a	a	k8xC
v	v	k7c6
Saint	Sainto	k1gNnPc2
<g/>
–	–	k?
<g/>
Germain	Germain	k1gMnSc1
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
bylo	být	k5eAaImAgNnS
k	k	k7c3
ČSR	ČSR	kA
připojeno	připojen	k2eAgNnSc1d1
i	i	k8xC
území	území	k1gNnSc1
Hlučínska	Hlučínsko	k1gNnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
část	část	k1gFnSc1
Vitorazska	Vitorazska	k1gFnSc1
a	a	k8xC
Valticko	Valticko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bojových	bojový	k2eAgNnPc6d1
střetnutích	střetnutí	k1gNnPc6
a	a	k8xC
následném	následný	k2eAgNnSc6d1
arbitrážním	arbitrážní	k2eAgNnSc6d1
rozhodnutí	rozhodnutí	k1gNnSc6
Spojenců	spojenec	k1gMnPc2
se	se	k3xPyFc4
rozdělila	rozdělit	k5eAaPmAgFnS
oblast	oblast	k1gFnSc1
Těšínska	Těšínsko	k1gNnSc2
mezi	mezi	k7c7
ČSR	ČSR	kA
a	a	k8xC
obnovené	obnovený	k2eAgNnSc1d1
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
hranicí	hranice	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
řeka	řeka	k1gFnSc1
Olše	olše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
šlo	jít	k5eAaImAgNnS
především	především	k6eAd1
o	o	k7c4
velké	velký	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
kamenného	kamenný	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
a	a	k8xC
vysokou	vysoký	k2eAgFnSc4d1
koncentraci	koncentrace	k1gFnSc4
průmyslových	průmyslový	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sudety	Sudety	k1gFnPc1
zpočátku	zpočátku	k6eAd1
zřejmě	zřejmě	k6eAd1
zahrnovaly	zahrnovat	k5eAaImAgInP
pouze	pouze	k6eAd1
pohoří	pohoří	k1gNnPc2
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
záhy	záhy	k6eAd1
tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
označoval	označovat	k5eAaImAgInS
celou	celý	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
pohoří	pohoří	k1gNnSc2
(	(	kIx(
<g/>
Sudety	Sudety	k1gFnPc1
=	=	kIx~
Sudetská	sudetský	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
/	/	kIx~
<g/>
subprovincie	subprovincie	k1gFnSc1
=	=	kIx~
Krkonošsko-jesenická	krkonošsko-jesenický	k2eAgFnSc1d1
subprovincie	subprovincie	k1gFnSc1
<g/>
)	)	kIx)
od	od	k7c2
dolního	dolní	k2eAgInSc2d1
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Ohře	Ohře	k1gFnSc2
až	až	k9
po	po	k7c4
řeku	řeka	k1gFnSc4
Odru	odr	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
Lužické	lužický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
Jizerské	jizerský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
Krkonoše	Krkonoše	k1gFnPc1
<g/>
,	,	kIx,
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
a	a	k8xC
Jeseníky	Jeseník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polštině	polština	k1gFnSc6
termín	termín	k1gInSc1
Sudety	Sudety	k1gInPc1
(	(	kIx(
<g/>
čes.	čes.	k?
Krkonošsko-jesenická	krkonošsko-jesenický	k2eAgFnSc1d1
subprovincie	subprovincie	k1gFnSc1
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
horské	horský	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
od	od	k7c2
Labského	labský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
až	až	k9
po	po	k7c4
Moravskou	moravský	k2eAgFnSc4d1
bránu	brána	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Sudetské	sudetský	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
,	,	kIx,
sudetští	sudetský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1903	#num#	k4
použil	použít	k5eAaPmAgMnS
politický	politický	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
Franz	Franz	k1gMnSc1
Jesser	Jesser	k1gMnSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
studií	studie	k1gFnPc2
poprvé	poprvé	k6eAd1
termín	termín	k1gInSc1
Sudetendeutsche	Sudetendeutsche	k1gInSc1
(	(	kIx(
<g/>
sudetští	sudetský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
horstva	horstvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
i	i	k8xC
v	v	k7c6
rakouském	rakouský	k2eAgNnSc6d1
Slezsku	Slezsko	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
označení	označení	k1gNnSc1
pro	pro	k7c4
německojazyčné	německojazyčný	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
Českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Alpendeutsche	Alpendeutsch	k1gFnSc2
–	–	k?
tedy	tedy	k8xC
Němců	Němec	k1gMnPc2
ze	z	k7c2
zemí	zem	k1gFnPc2
dnešního	dnešní	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
Slovinska	Slovinsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujících	následující	k2eAgFnPc2d1
dekád	dekáda	k1gFnPc2
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
slovo	slovo	k1gNnSc1
prosadilo	prosadit	k5eAaPmAgNnS
a	a	k8xC
bylo	být	k5eAaImAgNnS
široce	široko	k6eAd1
užíváno	užíván	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
začali	začít	k5eAaPmAgMnP
rakouští	rakouský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
označovat	označovat	k5eAaImF
termínem	termín	k1gInSc7
Sudetské	sudetský	k2eAgFnSc2d1
země	zem	k1gFnSc2
(	(	kIx(
<g/>
něm.	něm.	k?
Sudetische	Sudetisch	k1gInSc2
Länder	Ländero	k1gNnPc2
nebo	nebo	k8xC
Sudetenländer	Sudetenländra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
vydal	vydat	k5eAaPmAgMnS
geograf	geograf	k1gMnSc1
Fritz	Fritz	k1gMnSc1
Machatschek	Machatschek	k1gMnSc1
geografii	geografie	k1gFnSc4
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
ČSR	ČSR	kA
<g/>
)	)	kIx)
pod	pod	k7c7
názvem	název	k1gInSc7
Landeskunde	Landeskund	k1gInSc5
der	drát	k5eAaImRp2nS
Sudeten-	Sudeten-	k1gFnSc2
und	und	k?
Westkarpatenländer	Westkarpatenländer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgFnSc1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
neoznačovala	označovat	k5eNaImAgFnS
za	za	k7c4
sudetoněmeckou	sudetoněmecký	k2eAgFnSc4d1
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
s	s	k7c7
tímto	tento	k3xDgInSc7
pojmem	pojem	k1gInSc7
začal	začít	k5eAaPmAgInS
operovat	operovat	k5eAaImF
Henlein	Henlein	k2eAgInSc1d1
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
termín	termín	k1gInSc1
Sudety	Sudety	k1gInPc1
(	(	kIx(
<g/>
Sudetenland	Sudetenland	k1gInSc1
<g/>
)	)	kIx)
začal	začít	k5eAaPmAgInS
užívat	užívat	k5eAaImF
pro	pro	k7c4
většinu	většina	k1gFnSc4
pohraničních	pohraniční	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
žila	žít	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
početná	početný	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
až	až	k9
3,5	3,5	k4
milionů	milion	k4xCgInPc2
občanů	občan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sudetské	sudetský	k2eAgNnSc4d1
pohraničí	pohraničí	k1gNnSc4
živil	živit	k5eAaImAgInS
především	především	k9
lehký	lehký	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
(	(	kIx(
<g/>
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
produkce	produkce	k1gFnSc1
se	se	k3xPyFc4
vyvážela	vyvážet	k5eAaImAgFnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
velkou	velký	k2eAgFnSc7d1
hospodářskou	hospodářský	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
z	z	k7c2
let	léto	k1gNnPc2
1929	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
bylo	být	k5eAaImAgNnS
obzvláště	obzvláště	k6eAd1
silně	silně	k6eAd1
postiženo	postihnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
v	v	k7c6
Sudetech	Sudety	k1gInPc6
nijak	nijak	k6eAd1
neřešila	řešit	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
součást	součást	k1gFnSc1
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sudetoněmecké	sudetoněmecký	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Říšská	říšský	k2eAgFnSc1d1
župa	župa	k1gFnSc1
Sudety	Sudety	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sudetoněmecký	sudetoněmecký	k2eAgInSc4d1
odboj	odboj	k1gInSc4
proti	proti	k7c3
nacismu	nacismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Čeští	český	k2eAgMnPc1d1
uprchlíci	uprchlík	k1gMnPc1
ze	z	k7c2
Sudet	Sudety	k1gFnPc2
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
Krásné	krásný	k2eAgFnSc6d1
Lípě	lípa	k1gFnSc6
-	-	kIx~
říjen	říjen	k1gInSc1
1938	#num#	k4
</s>
<s>
Termín	termín	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nabyl	nabýt	k5eAaPmAgInS
územně	územně	k6eAd1
politického	politický	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
využit	využít	k5eAaPmNgInS
německými	německý	k2eAgMnPc7d1
nacisty	nacista	k1gMnPc7
za	za	k7c2
podpory	podpora	k1gFnSc2
části	část	k1gFnSc2
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
Sudet	Sudety	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
SdP	SdP	k1gFnSc2
<g/>
)	)	kIx)
k	k	k7c3
rozbití	rozbití	k1gNnSc3
demokratického	demokratický	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
totiž	totiž	k9
od	od	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
nikdy	nikdy	k6eAd1
nezpochybněnou	zpochybněný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
zemí	zem	k1gFnSc7
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
(	(	kIx(
<g/>
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
součástí	součást	k1gFnSc7
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc2d1
Svaté	svatá	k1gFnSc2
říše	říš	k1gFnSc2
římské	římský	k2eAgFnPc4d1
<g/>
,	,	kIx,
respektive	respektive	k9
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
tu	tu	k6eAd1
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
žilo	žít	k5eAaImAgNnS
německé	německý	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
zde	zde	k6eAd1
mělo	mít	k5eAaImAgNnS
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
dohodě	dohoda	k1gFnSc6
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
jako	jako	k8xS,k8xC
Sudetenland	Sudetenland	k1gInSc4
označováno	označován	k2eAgNnSc4d1
veškeré	veškerý	k3xTgNnSc4
území	území	k1gNnSc4
odstoupené	odstoupený	k2eAgNnSc4d1
pod	pod	k7c7
nátlakem	nátlak	k1gInSc7
velmocí	velmoc	k1gFnPc2
Československem	Československo	k1gNnSc7
hitlerovskému	hitlerovský	k2eAgNnSc3d1
Německu	Německo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
české	český	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
přihlásit	přihlásit	k5eAaPmF
k	k	k7c3
obnovenému	obnovený	k2eAgNnSc3d1
občanství	občanství	k1gNnSc3
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
přestěhovat	přestěhovat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	Čech	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
v	v	k7c6
pohraničí	pohraničí	k1gNnSc6
zůstali	zůstat	k5eAaPmAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
diskriminováni	diskriminován	k2eAgMnPc1d1
v	v	k7c6
zaměstnání	zaměstnání	k1gNnSc6
i	i	k8xC
ve	v	k7c6
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgMnPc1d1
němečtí	německý	k2eAgMnPc1d1
antifašisté	antifašista	k1gMnPc1
<g/>
,	,	kIx,
respektive	respektive	k9
jedinci	jedinec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
nepřipojili	připojit	k5eNaPmAgMnP
k	k	k7c3
masové	masový	k2eAgFnSc3d1
podpoře	podpora	k1gFnSc3
nacistické	nacistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
zároveň	zároveň	k6eAd1
nemohli	moct	k5eNaImAgMnP
odejít	odejít	k5eAaPmF
do	do	k7c2
českého	český	k2eAgNnSc2d1
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
okamžitě	okamžitě	k6eAd1
zatýkáni	zatýkán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
nicméně	nicméně	k8xC
začalo	začít	k5eAaPmAgNnS
nyní	nyní	k6eAd1
zjednodušenou	zjednodušený	k2eAgFnSc7d1
optikou	optika	k1gFnSc7
ve	v	k7c6
všech	všecek	k3xTgInPc6
českých	český	k2eAgInPc6d1
Němcích	Němec	k1gMnPc6
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
v	v	k7c6
duchu	duch	k1gMnSc6
nacistické	nacistický	k2eAgNnSc1d1
propagandy	propaganda	k1gFnPc4
označovaných	označovaný	k2eAgInPc2d1
jako	jako	k8xC,k8xS
sudetskými	sudetský	k2eAgMnPc7d1
Němci	Němec	k1gMnPc7
<g/>
,	,	kIx,
své	svůj	k3xOyFgMnPc4
úhlavní	úhlavní	k2eAgMnPc4d1
nepřátele	nepřítel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
zabrané	zabraný	k2eAgNnSc4d1
pohraniční	pohraniční	k2eAgNnSc4d1
území	území	k1gNnSc4
byla	být	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
výnosu	výnos	k1gInSc2
vůdce	vůdce	k1gMnSc2
a	a	k8xC
říšského	říšský	k2eAgMnSc2d1
kancléře	kancléř	k1gMnSc2
o	o	k7c6
správě	správa	k1gFnSc6
sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
Erlaß	Erlaß	k1gFnSc1
des	des	k1gNnSc2
Führers	Führersa	k1gFnPc2
und	und	k?
Reichskanzlers	Reichskanzlersa	k1gFnPc2
über	über	k1gMnSc1
die	die	k?
Verwaltung	Verwaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
sudetendeutschen	sudetendeutschen	k1gInSc1
Gebiete	Gebie	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
ustavena	ustaven	k2eAgFnSc1d1
správní	správní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
podle	podle	k7c2
zákona	zákon	k1gInSc2
„	„	k?
<g/>
O	o	k7c6
opětovném	opětovný	k2eAgNnSc6d1
sjednocení	sjednocení	k1gNnSc6
sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
s	s	k7c7
Německou	německý	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
Gesetz	Gesetz	k1gMnSc1
über	über	k1gMnSc1
die	die	k?
Wiedervereinigung	Wiedervereinigung	k1gMnSc1
der	drát	k5eAaImRp2nS
sudetendeutschen	sudetendeutschno	k1gNnPc2
Gebiete	Gebie	k1gNnSc2
mit	mit	k?
dem	dem	k?
Deutschen	Deutschen	k1gInSc1
Reich	Reich	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
oficiálně	oficiálně	k6eAd1
stala	stát	k5eAaPmAgFnS
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1938	#num#	k4
integrální	integrální	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říšská	říšský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Sudetoněmeckých	sudetoněmecký	k2eAgNnPc6d1
územích	území	k1gNnPc6
zavedena	zavést	k5eAaPmNgFnS
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
teprve	teprve	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1938	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
souběžná	souběžný	k2eAgFnSc1d1
platnost	platnost	k1gFnSc1
československé	československý	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
Protektorátu	protektorát	k1gInSc6
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
se	se	k3xPyFc4
podle	podle	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
Hitlera	Hitler	k1gMnSc2
z	z	k7c2
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
Sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
přeměnila	přeměnit	k5eAaPmAgFnS
k	k	k7c3
15	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc6
1939	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
„	„	k?
<g/>
O	o	k7c6
rozdělení	rozdělení	k1gNnSc6
sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Gesetz	Gesetz	k1gMnSc1
über	über	k1gMnSc1
die	die	k?
Gliederung	Gliederung	k1gMnSc1
der	drát	k5eAaImRp2nS
sudetendeutschen	sudetendeutschen	k1gInSc1
Gebiete	Gebie	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
14	#num#	k4
<g/>
]	]	kIx)
v	v	k7c4
Sudetskou	sudetský	k2eAgFnSc4d1
župu	župa	k1gFnSc4
(	(	kIx(
<g/>
Gau-Sudetenland	Gau-Sudetenland	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
od	od	k7c2
Domažlicka	Domažlicko	k1gNnSc2
po	po	k7c4
Moravskou	moravský	k2eAgFnSc4d1
Ostravu	Ostrava	k1gFnSc4
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Liberci	Liberec	k1gInSc6
(	(	kIx(
<g/>
Reichenberg	Reichenberg	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgMnS
sídlo	sídlo	k1gNnSc4
říšský	říšský	k2eAgMnSc1d1
župní	župní	k2eAgMnSc1d1
správce	správce	k1gMnSc1
Konrad	Konrada	k1gFnPc2
Henlein	Henlein	k1gMnSc1
<g/>
;	;	kIx,
území	území	k1gNnSc1
se	se	k3xPyFc4
dělilo	dělit	k5eAaImAgNnS
na	na	k7c4
tři	tři	k4xCgInPc4
vládní	vládní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g/>
:	:	kIx,
Cheb	Cheb	k1gInSc1
(	(	kIx(
<g/>
Eger	Eger	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
Aussig	Aussig	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Opavu	Opava	k1gFnSc4
(	(	kIx(
<g/>
Troppau	Troppaa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Čech	Čechy	k1gFnPc2
bylo	být	k5eAaImAgNnS
připojeno	připojit	k5eAaPmNgNnS
k	k	k7c3
Bavorsku	Bavorsko	k1gNnSc3
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
součástí	součást	k1gFnSc7
župy	župa	k1gFnSc2
Bavorská	bavorský	k2eAgFnSc1d1
Východní	východní	k2eAgFnSc1d1
marka	marka	k1gFnSc1
(	(	kIx(
<g/>
Reichsgau	Reichsgaus	k1gInSc2
Bayerische	Bayerische	k1gInSc1
Ostmark	Ostmark	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
říšskou	říšský	k2eAgFnSc4d1
župu	župa	k1gFnSc4
Bayreuth	Bayreutha	k1gFnPc2
(	(	kIx(
<g/>
Reichsgau	Reichsgaa	k1gFnSc4
Bayreuth	Bayreutha	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlučínsko	Hlučínsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
znovupřipojeno	znovupřipojit	k5eAaPmNgNnS
k	k	k7c3
Prusku	Prusko	k1gNnSc3
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
rámci	rámec	k1gInSc6
začleněno	začlenit	k5eAaPmNgNnS
do	do	k7c2
provincie	provincie	k1gFnSc2
Slezsko	Slezsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
území	území	k1gNnSc2
na	na	k7c6
jihu	jih	k1gInSc6
byl	být	k5eAaImAgInS
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
bývalému	bývalý	k2eAgInSc3d1
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
Ostmark	Ostmark	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
rámci	rámec	k1gInSc6
rozdělen	rozdělit	k5eAaPmNgInS
mezi	mezi	k7c4
Zemské	zemský	k2eAgNnSc4d1
hejtmanství	hejtmanství	k1gNnSc4
Dolní	dolní	k2eAgNnSc1d1
Podunají	Podunají	k1gNnSc1
(	(	kIx(
<g/>
bývalé	bývalý	k2eAgInPc1d1
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Zemské	zemský	k2eAgNnSc4d1
hejtmanství	hejtmanství	k1gNnSc4
Horní	horní	k2eAgNnSc1d1
Podunají	Podunají	k1gNnSc1
(	(	kIx(
<g/>
bývalé	bývalý	k2eAgInPc1d1
Horní	horní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hitler	Hitler	k1gMnSc1
vjíždí	vjíždět	k5eAaImIp3nS
triumfálně	triumfálně	k6eAd1
do	do	k7c2
Kraslic	Kraslice	k1gFnPc2
(	(	kIx(
<g/>
Sudety	Sudety	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
nadšeně	nadšeně	k6eAd1
odstraňují	odstraňovat	k5eAaImIp3nP
československý	československý	k2eAgInSc4d1
hraniční	hraniční	k2eAgInSc4d1
orientační	orientační	k2eAgInSc4d1
sloup	sloup	k1gInSc4
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
vítají	vítat	k5eAaImIp3nP
vojáky	voják	k1gMnPc7
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
v	v	k7c6
Žatci	Žatec	k1gInSc6
</s>
<s>
Sudetské	sudetský	k2eAgFnPc4d1
Němky	Němka	k1gFnPc4
při	při	k7c6
návštěvě	návštěva	k1gFnSc6
Hitlera	Hitler	k1gMnSc2
v	v	k7c6
Chebu	Cheb	k1gInSc6
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
</s>
<s>
Německý	německý	k2eAgInSc1d1
tank	tank	k1gInSc1
projíždí	projíždět	k5eAaImIp3nS
slavnostně	slavnostně	k6eAd1
vyzdobenými	vyzdobený	k2eAgFnPc7d1
ulicemi	ulice	k1gFnPc7
Chomutova	Chomutov	k1gInSc2
</s>
<s>
Jednotky	jednotka	k1gFnPc1
sudetoněmeckých	sudetoněmecký	k2eAgFnPc2d1
Freikorps	Freikorpsa	k1gFnPc2
</s>
<s>
Obnovené	obnovený	k2eAgNnSc1d1
Československo	Československo	k1gNnSc1
</s>
<s>
Odsun	odsun	k1gInSc1
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
bylo	být	k5eAaImAgNnS
veškeré	veškerý	k3xTgNnSc4
odstoupené	odstoupený	k2eAgNnSc4d1
pohraniční	pohraniční	k2eAgNnSc4d1
území	území	k1gNnSc4
připojeno	připojit	k5eAaPmNgNnS
zpět	zpět	k6eAd1
k	k	k7c3
ČSR	ČSR	kA
a	a	k8xC
zároveň	zároveň	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
článku	článek	k1gInSc2
XI	XI	kA
<g/>
.	.	kIx.
až	až	k9
XII	XII	kA
<g/>
.	.	kIx.
závěrů	závěr	k1gInPc2
Postupimské	postupimský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
vítězných	vítězný	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
československého	československý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
hlásícího	hlásící	k2eAgInSc2d1
se	se	k3xPyFc4
k	k	k7c3
německé	německý	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
„	„	k?
<g/>
odsunuta	odsunut	k2eAgMnSc4d1
<g/>
“	“	k?
z	z	k7c2
Československa	Československo	k1gNnSc2
do	do	k7c2
americké	americký	k2eAgFnSc2d1
a	a	k8xC
sovětské	sovětský	k2eAgFnSc2d1
okupační	okupační	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
a	a	k8xC
připravena	připravit	k5eAaPmNgFnS
prakticky	prakticky	k6eAd1
o	o	k7c4
veškerý	veškerý	k3xTgInSc4
majetek	majetek	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
odsunu	odsun	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
angl.	angl.	k?
orig	orig	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupimské	postupimský	k2eAgFnPc4d1
dohody	dohoda	k1gFnPc4
je	být	k5eAaImIp3nS
uvedeno	uveden	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
„	„	k?
<g/>
transfer	transfer	k1gInSc4
<g/>
“	“	k?
=	=	kIx~
přemístění	přemístění	k1gNnSc1
<g/>
,	,	kIx,
odsun	odsun	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
docházelo	docházet	k5eAaImAgNnS
i	i	k9
k	k	k7c3
zabíjení	zabíjení	k1gNnSc3
německých	německý	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
amnestijní	amnestijní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
č.	č.	k?
115	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
toto	tento	k3xDgNnSc4
zabíjení	zabíjení	k1gNnSc4
neexkulpoval	exkulpovat	k5eNaPmAgMnS
–	–	k?
vztahoval	vztahovat	k5eAaImAgInS
se	se	k3xPyFc4
jen	jen	k9
na	na	k7c6
„	„	k?
<g/>
jednání	jednání	k1gNnSc6
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
došlo	dojít	k5eAaPmAgNnS
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1938	#num#	k4
do	do	k7c2
28	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1945	#num#	k4
a	a	k8xC
…	…	k?
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
směřovalo	směřovat	k5eAaImAgNnS
ke	k	k7c3
spravedlivé	spravedlivý	k2eAgFnSc3d1
odplatě	odplata	k1gFnSc3
za	za	k7c4
činy	čin	k1gInPc4
okupantů	okupant	k1gMnPc2
nebo	nebo	k8xC
jejich	jejich	k3xOp3gMnPc2
pomahačů	pomahač	k1gMnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
na	na	k7c4
činy	čin	k1gInPc4
spáchané	spáchaný	k2eAgInPc4d1
z	z	k7c2
„	„	k?
<g/>
pohnutek	pohnutka	k1gFnPc2
nízkých	nízký	k2eAgFnPc2d1
a	a	k8xC
nečestných	čestný	k2eNgFnPc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Masivní	masivní	k2eAgInSc1d1
úbytek	úbytek	k1gInSc1
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
Sudet	Sudety	k1gFnPc2
byl	být	k5eAaImAgMnS
alespoň	alespoň	k9
z	z	k7c2
části	část	k1gFnSc2
nahrazen	nahradit	k5eAaPmNgInS
lidmi	člověk	k1gMnPc7
z	z	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
Českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
Slovenska	Slovensko	k1gNnSc2
a	a	k8xC
také	také	k9
přistěhovalci	přistěhovalec	k1gMnPc1
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Řeky	Řek	k1gMnPc7
<g/>
,	,	kIx,
Rumuny	Rumun	k1gMnPc7
a	a	k8xC
Volyňskými	volyňský	k2eAgMnPc7d1
Čechy	Čech	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Imigrantů	imigrant	k1gMnPc2
z	z	k7c2
ciziny	cizina	k1gFnSc2
bylo	být	k5eAaImAgNnS
úhrnem	úhrnem	k6eAd1
asi	asi	k9
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Sudet	Sudety	k1gInPc2
později	pozdě	k6eAd2
za	za	k7c4
podpory	podpora	k1gFnPc4
komunistické	komunistický	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
přesídlilo	přesídlit	k5eAaPmAgNnS
i	i	k9
mnoho	mnoho	k4c1
slovenských	slovenský	k2eAgMnPc2d1
Cikánů	cikán	k1gMnPc2
a	a	k8xC
Maďarů	Maďar	k1gMnPc2
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgInSc1d1
tisk	tisk	k1gInSc1
psal	psát	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
zdrojem	zdroj	k1gInSc7
pracovní	pracovní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
v	v	k7c6
pohraničí	pohraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovala	existovat	k5eAaImAgFnS
zde	zde	k6eAd1
totiž	totiž	k9
iniciativa	iniciativa	k1gFnSc1
československé	československý	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
na	na	k7c4
osídlení	osídlení	k1gNnSc4
prázdného	prázdný	k2eAgNnSc2d1
českého	český	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
soustředěn	soustředěn	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
let	léto	k1gNnPc2
čtyřicátých	čtyřicátý	k4xOgNnPc2
až	až	k9
asi	asi	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
let	léto	k1gNnPc2
osmdesátých	osmdesátý	k4xOgNnPc2
československý	československý	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
,	,	kIx,
rozhlas	rozhlas	k1gInSc1
a	a	k8xC
televize	televize	k1gFnSc1
nazývali	nazývat	k5eAaImAgMnP
odsunuté	odsunutý	k2eAgInPc4d1
sudetské	sudetský	k2eAgInPc4d1
Němce	Němec	k1gMnSc4
revanšisty	revanšista	k1gMnSc2
(	(	kIx(
<g/>
údajně	údajně	k6eAd1
mysleli	myslet	k5eAaImAgMnP
na	na	k7c4
odvetu	odveta	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
revanš	revanš	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
vlivu	vliv	k1gInSc3
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
migrace	migrace	k1gFnSc2
v	v	k7c6
Sudetech	Sudety	k1gInPc6
zde	zde	k6eAd1
na	na	k7c4
několik	několik	k4yIc4
desetiletí	desetiletí	k1gNnPc2
prakticky	prakticky	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
veřejný	veřejný	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
často	často	k6eAd1
cítili	cítit	k5eAaImAgMnP
a	a	k8xC
stále	stále	k6eAd1
ještě	ještě	k9
cítí	cítit	k5eAaImIp3nP
vykořeněni	vykořeněn	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
výrazně	výrazně	k6eAd1
lepšit	lepšit	k5eAaImF
až	až	k9
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
o	o	k7c6
problému	problém	k1gInSc6
vůbec	vůbec	k9
mohlo	moct	k5eAaImAgNnS
otevřeně	otevřeně	k6eAd1
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Zatímco	zatímco	k8xS
československo-rakouskou	československo-rakouský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
byla	být	k5eAaImAgFnS
otázka	otázka	k1gFnSc1
odškodnění	odškodnění	k1gNnSc2
odsunutého	odsunutý	k2eAgNnSc2d1
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
fakticky	fakticky	k6eAd1
vyřešena	vyřešit	k5eAaPmNgFnS
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
sudetští	sudetský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
vzdor	vzdor	k6eAd1
uzavřené	uzavřený	k2eAgFnSc6d1
česko-německé	česko-německý	k2eAgFnSc6d1
deklaraci	deklarace	k1gFnSc6
(	(	kIx(
<g/>
ratifikovaná	ratifikovaný	k2eAgFnSc1d1
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nepovažují	považovat	k5eNaImIp3nP
problém	problém	k1gInSc4
svého	svůj	k3xOyFgInSc2
odsunu	odsun	k1gInSc2
a	a	k8xC
zabavení	zabavení	k1gNnSc2
majetku	majetek	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
„	„	k?
<g/>
kolektivní	kolektivní	k2eAgFnSc2d1
viny	vina	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
podle	podle	k7c2
Benešových	Benešových	k2eAgInPc2d1
dekretů	dekret	k1gInPc2
<g/>
)	)	kIx)
za	za	k7c4
uzavřený	uzavřený	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
zákazu	zákaz	k1gInSc2
používání	používání	k1gNnSc2
pojmu	pojmout	k5eAaPmIp1nS
„	„	k?
<g/>
Sudety	Sudety	k1gInPc1
<g/>
“	“	k?
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Československý	československý	k2eAgMnSc1d1
exilový	exilový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Edvard	Edvard	k1gMnSc1
Beneš	Beneš	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
v	v	k7c6
červnu	červen	k1gInSc6
1942	#num#	k4
Wenzelu	Wenzel	k1gMnSc3
Jakschovi	Jaksch	k1gMnSc3
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
A	a	k9
<g/>
:	:	kIx,
vědecky	vědecky	k6eAd1
<g/>
,	,	kIx,
geograficky	geograficky	k6eAd1
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
slovo	slovo	k1gNnSc1
”	”	k?
<g/>
sudetský	sudetský	k2eAgInSc1d1
<g/>
”	”	k?
znamená	znamenat	k5eAaImIp3nS
zcela	zcela	k6eAd1
něco	něco	k3yInSc1
jiného	jiný	k2eAgNnSc2d1
<g/>
,	,	kIx,
nežli	nežli	k8xS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
ho	on	k3xPp3gInSc2
nyní	nyní	k6eAd1
používá	používat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
B	B	kA
<g/>
:	:	kIx,
slovo	slovo	k1gNnSc1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
výrazem	výraz	k1gInSc7
politického	politický	k2eAgInSc2d1
podvodu	podvod	k1gInSc2
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
z	z	k7c2
německé	německý	k2eAgFnSc2d1
otázky	otázka	k1gFnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
učinil	učinit	k5eAaImAgInS,k5eAaPmAgInS
Henlein	Henlein	k2eAgInSc1d1
a	a	k8xC
německý	německý	k2eAgInSc1d1
fašismus	fašismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěl	chtít	k5eAaImAgMnS
zvláštní	zvláštní	k2eAgFnSc7d1
terminologií	terminologie	k1gFnSc7
vytvořit	vytvořit	k5eAaPmF
politické	politický	k2eAgFnSc2d1
skutečnosti	skutečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
od	od	k7c2
něho	on	k3xPp3gInSc2
podvod	podvod	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
podvod	podvod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snad	snad	k9
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
chápat	chápat	k5eAaImF
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
ze	z	k7c2
stran	strana	k1gFnPc2
jiných	jiný	k1gMnPc2
chtělo	chtít	k5eAaImAgNnS
u	u	k7c2
nás	my	k3xPp1nPc2
proti	proti	k7c3
podvodné	podvodný	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
agitaci	agitace	k1gFnSc3
Henleinově	Henleinův	k2eAgFnSc3d1
působit	působit	k5eAaImF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
reagovalo	reagovat	k5eAaBmAgNnS
a	a	k8xC
termínu	termín	k1gInSc2
toho	ten	k3xDgInSc2
z	z	k7c2
důvodů	důvod	k1gInPc2
taktických	taktický	k2eAgInPc2d1
také	také	k9
užívalo	užívat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
politická	politický	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
:	:	kIx,
obtíže	obtíž	k1gFnPc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nevyřeší	vyřešit	k5eNaPmIp3nS
následováním	následování	k1gNnSc7
chyb	chyba	k1gFnPc2
nebo	nebo	k8xC
demagogie	demagogie	k1gFnSc2
odpůrcovy	odpůrcův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chceme	chtít	k5eAaImIp1nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
henleinismu	henleinismus	k1gInSc2
a	a	k8xC
nacismu	nacismus	k1gInSc2
definitivně	definitivně	k6eAd1
emancipovat	emancipovat	k5eAaBmF
<g/>
,	,	kIx,
dělejme	dělat	k5eAaImRp1nP
to	ten	k3xDgNnSc4
důsledně	důsledně	k6eAd1
a	a	k8xC
nepodléhejme	podléhat	k5eNaImRp1nP
jeho	jeho	k3xOp3gFnPc3
metodám	metoda	k1gFnPc3
<g/>
!	!	kIx.
</s>
<s desamb="1">
Vraťme	vrátit	k5eAaPmRp1nP
se	se	k3xPyFc4
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
dobré	dobrý	k2eAgFnSc3d1
dřívější	dřívější	k2eAgFnSc3d1
tradici	tradice	k1gFnSc3
<g/>
!	!	kIx.
</s>
<s>
C	C	kA
<g/>
:	:	kIx,
Slovo	slovo	k1gNnSc1
”	”	k?
<g/>
sudetský	sudetský	k2eAgInSc4d1
<g/>
”	”	k?
<g/>
,	,	kIx,
”	”	k?
<g/>
Sudetenland	Sudetenland	k1gInSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
”	”	k?
<g/>
sudeťák	sudeťák	k1gMnSc1
<g/>
”	”	k?
bude	být	k5eAaImBp3nS
navždy	navždy	k6eAd1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
spojeno	spojen	k2eAgNnSc1d1
s	s	k7c7
nacistickým	nacistický	k2eAgNnSc7d1
zvířectvím	zvířectví	k1gNnSc7
na	na	k7c6
nás	my	k3xPp1nPc2
Češích	Čech	k1gMnPc6
i	i	k8xC
na	na	k7c6
demokratických	demokratický	k2eAgInPc6d1
Němcích	Němec	k1gMnPc6
prováděným	prováděný	k2eAgInSc7d1
v	v	k7c6
osudné	osudný	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
před	před	k7c7
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musíme	muset	k5eAaImIp1nP
se	se	k3xPyFc4
snažit	snažit	k5eAaImF
o	o	k7c4
novou	nový	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
zbavení	zbavení	k1gNnSc2
se	se	k3xPyFc4
i	i	k9
těch	ten	k3xDgFnPc2
smutných	smutná	k1gFnPc2
<g/>
,	,	kIx,
politicky	politicky	k6eAd1
osudných	osudný	k2eAgMnPc2d1
a	a	k8xC
pro	pro	k7c4
nás	my	k3xPp1nPc4
nepřijatelných	přijatelný	k2eNgMnPc2d1
rekvizit	rekvizit	k1gInSc4
nacistické	nacistický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
D	D	kA
<g/>
:	:	kIx,
Češi	česat	k5eAaImIp1nS
po	po	k7c6
dnešní	dnešní	k2eAgFnSc3d1
válce	válka	k1gFnSc3
toto	tento	k3xDgNnSc4
slovo	slovo	k1gNnSc4
nepřijmou	přijmout	k5eNaPmIp3nP
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
je	on	k3xPp3gInPc4
perhoreskovat	perhoreskovat	k5eAaImF
(	(	kIx(
<g/>
tj.	tj.	kA
rozhodně	rozhodně	k6eAd1
odmítat	odmítat	k5eAaImF
<g/>
)	)	kIx)
<g/>
;	;	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
v	v	k7c6
našem	náš	k3xOp1gInSc6
zájmu	zájem	k1gInSc6
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
se	se	k3xPyFc4
včas	včas	k6eAd1
shodli	shodnout	k5eAaBmAgMnP,k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
naší	náš	k3xOp1gFnSc2
politiky	politika	k1gFnSc2
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
zmizí	zmizet	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
v	v	k7c6
zájmu	zájem	k1gInSc6
dobré	dobrý	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
nemuselo	muset	k5eNaImAgNnS
dělat	dělat	k5eAaImF
zákony	zákon	k1gInPc7
a	a	k8xC
nařízeními	nařízení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
by	by	kYmCp3nS
směšné	směšný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
i	i	k9
českoslovenští	československý	k2eAgMnPc1d1
antihenleinovci	antihenleinovec	k1gMnPc1
a	a	k8xC
antinacističtí	antinacistický	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
se	se	k3xPyFc4
po	po	k7c6
válce	válka	k1gFnSc6
mezi	mezi	k7c7
sebou	se	k3xPyFc7
hádali	hádat	k5eAaImAgMnP
o	o	k7c4
slova	slovo	k1gNnPc4
a	a	k8xC
dělali	dělat	k5eAaImAgMnP
z	z	k7c2
henleinovského	henleinovský	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
a	a	k8xC
z	z	k7c2
terminologie	terminologie	k1gFnSc2
politické	politický	k2eAgFnSc2d1
aféry	aféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudíme	soudit	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
naši	náš	k3xOp1gMnPc1
němečtí	německý	k2eAgMnPc1d1
občané	občan	k1gMnPc1
dobře	dobře	k6eAd1
to	ten	k3xDgNnSc4
uváží	uvážet	k5eAaImIp3nS,k5eAaPmIp3nS
a	a	k8xC
vše	všechen	k3xTgNnSc4
potřebné	potřebné	k1gNnSc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
připraví	připravit	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1942	#num#	k4
přijalo	přijmout	k5eAaPmAgNnS
exilové	exilový	k2eAgNnSc1d1
Prezidium	prezidium	k1gNnSc1
čs	čs	kA
<g/>
.	.	kIx.
Ministerské	ministerský	k2eAgFnPc1d1
rady	rada	k1gFnPc1
(	(	kIx(
<g/>
na	na	k7c4
návrh	návrh	k1gInSc4
čs	čs	kA
<g/>
.	.	kIx.
exilového	exilový	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
<g/>
)	)	kIx)
vyhlášku	vyhláška	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
užívání	užívání	k1gNnSc1
pojmů	pojem	k1gInPc2
spojených	spojený	k2eAgInPc2d1
se	s	k7c7
slovem	slovo	k1gNnSc7
sudetský	sudetský	k2eAgMnSc1d1
Němec	Němec	k1gMnSc1
stává	stávat	k5eAaImIp3nS
protiústavním	protiústavní	k2eAgMnSc7d1
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
trestné	trestný	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Deska	deska	k1gFnSc1
v	v	k7c6
Andělské	andělský	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
na	na	k7c4
památku	památka	k1gFnSc4
premiéry	premiéra	k1gFnSc2
i	i	k8xC
derniéry	derniéra	k1gFnSc2
Cimrmanovy	Cimrmanův	k2eAgFnSc2d1
vlastenecké	vlastenecký	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Sudety	Sudety	k1gFnPc4
krásné	krásný	k2eAgFnPc4d1
<g/>
,	,	kIx,
Sudety	Sudety	k1gFnPc4
mé	můj	k3xOp1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
údajně	údajně	k6eAd1
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
původním	původní	k2eAgNnSc6d1
českém	český	k2eAgNnSc6d1
znění	znění	k1gNnSc6
s	s	k7c7
německými	německý	k2eAgInPc7d1
titulky	titulek	k1gInPc7
roku	rok	k1gInSc2
1912	#num#	k4
v	v	k7c6
místním	místní	k2eAgInSc6d1
hostinci	hostinec	k1gInSc6
Gasthaus	Gasthaus	k1gInSc1
zum	zum	k?
Goldenen	Goldenen	k2eAgInSc1d1
Kreuz	Kreuz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
vešla	vejít	k5eAaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
vyhláška	vyhláška	k1gFnSc1
<g/>
,	,	kIx,
hovořící	hovořící	k2eAgInSc1d1
o	o	k7c6
nepřípustnosti	nepřípustnost	k1gFnSc6
užívání	užívání	k1gNnSc2
pojmu	pojem	k1gInSc2
Sudety	Sudety	k1gInPc4
v	v	k7c6
úředním	úřední	k2eAgInSc6d1
styku	styk	k1gInSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Název	název	k1gInSc1
”	”	k?
<g/>
Sudety	Sudety	k1gInPc1
<g/>
”	”	k?
nepřípustný	přípustný	k2eNgMnSc1d1
<g/>
:	:	kIx,
ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
upozorňuje	upozorňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
používání	používání	k1gNnSc1
názvu	název	k1gInSc2
”	”	k?
<g/>
Sudety	Sudety	k1gInPc1
<g/>
”	”	k?
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc2
odvozenin	odvozenina	k1gFnPc2
a	a	k8xC
podobných	podobný	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
<g/>
,	,	kIx,
obvyklých	obvyklý	k2eAgInPc2d1
v	v	k7c6
době	doba	k1gFnSc6
okupace	okupace	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nepřípustné	přípustný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
označení	označení	k1gNnSc4
příslušného	příslušný	k2eAgNnSc2d1
území	území	k1gNnSc2
buď	buď	k8xC
užíváno	užíván	k2eAgNnSc4d1
názvu	název	k1gInSc2
”	”	k?
<g/>
pohraniční	pohraniční	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opatření	opatření	k1gNnSc1
tohoto	tento	k3xDgMnSc2
dlužno	dlužno	k6eAd1
dbáti	dbát	k5eAaImF
zejména	zejména	k9
v	v	k7c6
úředním	úřední	k2eAgInSc6d1
styku	styk	k1gInSc6
veškeré	veškerý	k3xTgFnSc2
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
problematice	problematika	k1gFnSc6
pohraničí	pohraničí	k1gNnSc2
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
v	v	k7c6
českých	český	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
od	od	k7c2
konce	konec	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždy	vždy	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
území	území	k1gNnPc4
výrazně	výrazně	k6eAd1
diferencovaná	diferencovaný	k2eAgNnPc4d1
ekonomickým	ekonomický	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
i	i	k8xC
národnostním	národnostní	k2eAgNnSc7d1
složením	složení	k1gNnSc7
(	(	kIx(
<g/>
s	s	k7c7
výrazným	výrazný	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
Němců	Němec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
pojmem	pojem	k1gInSc7
pohraničí	pohraničí	k1gNnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
vymezení	vymezení	k1gNnPc2
bylo	být	k5eAaImAgNnS
ztotožněno	ztotožněn	k2eAgNnSc1d1
s	s	k7c7
územím	území	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
okupovala	okupovat	k5eAaBmAgFnS
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
dosídlovací	dosídlovací	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
a	a	k8xC
uplatňování	uplatňování	k1gNnSc2
dalších	další	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
byly	být	k5eAaImAgInP
v	v	k7c6
letech	léto	k1gNnPc6
1949	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
a	a	k8xC
1969	#num#	k4
vzhledem	vzhledem	k7c3
k	k	k7c3
administrativně	administrativně	k6eAd1
správnímu	správní	k2eAgNnSc3d1
členění	členění	k1gNnSc3
provedeny	proveden	k2eAgFnPc1d1
dílčí	dílčí	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dle	dle	k7c2
Stanoviska	stanovisko	k1gNnSc2
Odboru	odbor	k1gInSc2
legislativy	legislativa	k1gFnSc2
a	a	k8xC
koordinace	koordinace	k1gFnSc2
předpisů	předpis	k1gInPc2
a	a	k8xC
kompatibility	kompatibilita	k1gFnSc2
s	s	k7c7
právem	právo	k1gNnSc7
Evropských	evropský	k2eAgNnPc2d1
společenství	společenství	k1gNnPc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
k	k	k7c3
používání	používání	k1gNnSc6
názvu	název	k1gInSc6
Sudety	Sudety	k1gFnPc1
ze	z	k7c2
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2004	#num#	k4
však	však	k9
nejsou	být	k5eNaImIp3nP
vyhlášky	vyhláška	k1gFnPc1
z	z	k7c2
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
a	a	k8xC
z	z	k7c2
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1942	#num#	k4
(	(	kIx(
<g/>
navržená	navržený	k2eAgFnSc1d1
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
<g/>
)	)	kIx)
součástí	součást	k1gFnSc7
platného	platný	k2eAgInSc2d1
právního	právní	k2eAgInSc2d1
řádu	řád	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
psal	psát	k5eAaImAgInS
o	o	k7c4
rehabilitaci	rehabilitace	k1gFnSc4
historicko-geografického	historicko-geografický	k2eAgInSc2d1
pojmu	pojem	k1gInSc2
Sudety	Sudety	k1gFnPc4
Václav	Václav	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
použili	použít	k5eAaPmAgMnP
Ivan	Ivan	k1gMnSc1
Bičík	bičík	k1gInSc1
a	a	k8xC
Vít	Vít	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
v	v	k7c6
názvu	název	k1gInSc6
své	svůj	k3xOyFgFnSc2
studie	studie	k1gFnSc2
Case	Cas	k1gFnSc2
Study	stud	k1gInPc4
of	of	k?
the	the	k?
Prague	Pragu	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Environs	Environs	k1gInSc1
and	and	k?
the	the	k?
Czech	Czech	k1gInSc1
Sudetenland	Sudetenlanda	k1gFnPc2
termínu	termín	k1gInSc2
Czech	Czech	k1gInSc4
Sudetenland	Sudetenland	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
české	český	k2eAgFnPc4d1
Sudety	Sudety	k1gFnPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
již	již	k6eAd1
média	médium	k1gNnSc2
výraz	výraz	k1gInSc4
Sudety	Sudety	k1gInPc1
běžně	běžně	k6eAd1
používají	používat	k5eAaImIp3nP
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
dalšího	další	k2eAgNnSc2d1
vysvětlování	vysvětlování	k1gNnSc2
či	či	k8xC
obhajování	obhajování	k1gNnSc2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
užití	užití	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Radim	Radim	k1gMnSc1
Perlín	perlín	k1gMnSc1
<g/>
:	:	kIx,
Venkov	venkov	k1gInSc1
<g/>
,	,	kIx,
typologie	typologie	k1gFnSc1
venkovského	venkovský	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
URL	URL	kA
<g/>
:	:	kIx,
https://web.archive.org/web/20110928135236/http://aplikace.mvcr.cz/archiv2008/odbor/reforma/perlin.pdf	https://web.archive.org/web/20110928135236/http://aplikace.mvcr.cz/archiv2008/odbor/reforma/perlin.pdf	k1gInSc1
<g/>
↑	↑	k?
http://is.muni.cz/th/63542/prif_d_b1/Jen_text-finIV.txt1	http://is.muni.cz/th/63542/prif_d_b1/Jen_text-finIV.txt1	k4
2	#num#	k4
Arburg	Arburg	k1gMnSc1
<g/>
,	,	kIx,
Adrian	Adrian	k1gMnSc1
von	von	k1gInSc1
<g/>
,	,	kIx,
Staněk	Staněk	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysídlení	vysídlení	k1gNnPc4
Němců	Němec	k1gMnPc2
a	a	k8xC
proměny	proměna	k1gFnSc2
českého	český	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
1945	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
:	:	kIx,
dokumenty	dokument	k1gInPc4
z	z	k7c2
českých	český	k2eAgInPc2d1
archivů	archiv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
Středokluky	Středokluk	k1gInPc4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Susa	Susa	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
28	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Suppan	Suppan	k1gMnSc1
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
:	:	kIx,
Austrians	Austrians	k1gInSc1
<g/>
,	,	kIx,
Czechs	Czechs	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Sudeten	Sudeten	k2eAgInSc1d1
Germans	Germans	k1gInSc1
as	as	k1gNnSc2
a	a	k8xC
Community	Communita	k1gFnSc2
of	of	k?
Conflict	Conflict	k1gMnSc1
in	in	k?
the	the	k?
Twentieth	Twentieth	k1gInSc4
Century	Centura	k1gFnSc2
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
9	#num#	k4
<g/>
↑	↑	k?
http://www.ceskatelevize.cz/ct24/domaci/1281191-pri-vzniku-ceskoslovenska-tekla-nemecka-krev	http://www.ceskatelevize.cz/ct24/domaci/1281191-pri-vzniku-ceskoslovenska-tekla-nemecka-krva	k1gFnPc2
<g/>
↑	↑	k?
http://nassmer.blogspot.cz/2009/03/krvave-uvitani-v-novem-state-4-brezen.html	http://nassmer.blogspot.cz/2009/03/krvave-uvitani-v-novem-state-4-brezen.html	k1gMnSc1
<g/>
↑	↑	k?
Hahn	Hahn	k1gInSc1
<g/>
,	,	kIx,
Hans-Henning	Hans-Henning	k1gInSc1
<g/>
:	:	kIx,
Hundert	Hundert	k1gInSc1
Jahre	Jahr	k1gMnSc5
sudeten-deutsche	sudeten-deutschus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
<g/>
:	:	kIx,
eine	einat	k5eAaPmIp3nS
völkische	völkische	k6eAd1
Bewegung	Bewegung	k1gInSc1
in	in	k?
drei	dre	k1gFnSc2
...	...	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
Books	Booksa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
BRÁNA	brána	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
720	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
597	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
139	#num#	k4
<g/>
-	-	kIx~
<g/>
146	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Slavný	slavný	k2eAgMnSc1d1
německý	německý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
vypráví	vyprávět	k5eAaImIp3nS
příběh	příběh	k1gInSc1
antifašistů	antifašista	k1gMnPc2
v	v	k7c6
Sudetech	Sudety	k1gInPc6
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Slavný	slavný	k2eAgMnSc1d1
německý	německý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
vypráví	vyprávět	k5eAaImIp3nS
příběh	příběh	k1gInSc1
antifašistů	antifašista	k1gMnPc2
v	v	k7c6
Sudetech	Sudety	k1gInPc6
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
výnos	výnos	k1gInSc1
vůdce	vůdce	k1gMnSc2
a	a	k8xC
říšského	říšský	k2eAgMnSc2d1
kancléře	kancléř	k1gMnSc2
o	o	k7c6
správě	správa	k1gFnSc6
sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
↑	↑	k?
zákon	zákon	k1gInSc1
„	„	k?
<g/>
O	o	k7c6
opětovném	opětovný	k2eAgNnSc6d1
sjednocení	sjednocení	k1gNnSc6
sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
s	s	k7c7
Německou	německý	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
“	“	k?
<g/>
↑	↑	k?
Kronika	kronika	k1gFnSc1
Větřkovic	Větřkovice	k1gFnPc2
z	z	k7c2
let	léto	k1gNnPc2
1298-1945	1298-1945	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
http://www.verfassungen.de/de/de33-45/sudetenland-verwaltung39.htm	http://www.verfassungen.de/de/de33-45/sudetenland-verwaltung39.htm	k1gInSc1
zákon	zákon	k1gInSc1
„	„	k?
<g/>
O	o	k7c6
rozdělení	rozdělení	k1gNnSc6
sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
“	“	k?
<g/>
↑	↑	k?
Pořad	pořad	k1gInSc1
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cs	cs	k?
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
o	o	k7c6
amnestii	amnestie	k1gFnSc6
nejednalo	jednat	k5eNaImAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
za	za	k7c4
tyto	tento	k3xDgInPc4
skutky	skutek	k1gInPc4
nikdo	nikdo	k3yNnSc1
nebyl	být	k5eNaImAgMnS
souzen	soudit	k5eAaImNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problematické	problematický	k2eAgNnSc1d1
také	také	k9
je	být	k5eAaImIp3nS
datum	datum	k1gNnSc4
<g/>
,	,	kIx,
do	do	k7c2
kdy	kdy	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
vztahoval	vztahovat	k5eAaImAgInS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgInS
i	i	k9
činů	čin	k1gInPc2
spáchaných	spáchaný	k2eAgInPc2d1
po	po	k7c6
oficiálním	oficiální	k2eAgInSc6d1
konci	konec	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Čapka	Čapka	k1gMnSc1
<g/>
,	,	kIx,
Fr.	Fr.	k1gMnSc1
<g/>
;	;	kIx,
Slezák	Slezák	k1gMnSc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
;	;	kIx,
Vaculík	Vaculík	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Nové	Nové	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
pohraničí	pohraničí	k1gNnPc2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Prezentace	prezentace	k1gFnPc1
o	o	k7c6
novoosídlencích	novoosídlenec	k1gMnPc6
Problematika	problematika	k1gFnSc1
nového	nový	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
pohraničí	pohraničí	k1gNnSc2
je	být	k5eAaImIp3nS
už	už	k6eAd1
jinou	jiný	k2eAgFnSc7d1
kapitolou	kapitola	k1gFnSc7
dějin	dějiny	k1gFnPc2
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BENEŠ	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Edvard	Edvard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásadní	zásadní	k2eAgInSc4d1
stanovisko	stanovisko	k1gNnSc1
k	k	k7c3
usnesení	usnesení	k1gNnSc3
představenstva	představenstvo	k1gNnSc2
SdS	SdS	k1gFnSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
odevzdané	odevzdaný	k2eAgFnSc2d1
W.	W.	kA
Jakschovi	Jakschův	k2eAgMnPc1d1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XII	XII	kA
<g/>
.	.	kIx.
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
VI	VI	kA
<g/>
.	.	kIx.
1942	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
↑	↑	k?
JAKSCH	JAKSCH	kA
<g/>
,	,	kIx,
Wenzel	Wenzel	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopis	dopis	k1gInSc1
R.	R.	kA
Bechyněmu	Bechyněm	k1gInSc2
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
X.	X.	kA
1940	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
↑	↑	k?
Úřední	úřední	k2eAgInSc4d1
list	list	k1gInSc4
Republiky	republika	k1gFnSc2
československé	československý	k2eAgFnSc2d1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
1945	#num#	k4
<g/>
..	..	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
↑	↑	k?
JEŘÁBEK	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohraničí	pohraničí	k1gNnPc1
v	v	k7c6
regionálním	regionální	k2eAgInSc6d1
rozvoji	rozvoj	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
výzkum	výzkum	k1gInSc1
(	(	kIx(
<g/>
Geografie	geografie	k1gFnSc1
-	-	kIx~
Sborník	sborník	k1gInSc1
ČGS	ČGS	kA
<g/>
,	,	kIx,
105	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geografická	geografický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KAUCKÝ	KAUCKÝ	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanovisko	stanovisko	k1gNnSc1
Odboru	odbor	k1gInSc2
legislativy	legislativa	k1gFnSc2
a	a	k8xC
koordinace	koordinace	k1gFnSc2
předpisů	předpis	k1gInPc2
a	a	k8xC
kompatibility	kompatibilita	k1gFnSc2
s	s	k7c7
právem	právo	k1gNnSc7
Evropských	evropský	k2eAgNnPc2d1
společenství	společenství	k1gNnPc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2004	#num#	k4
k	k	k7c3
používání	používání	k1gNnSc3
názvu	název	k1gInSc2
Sudety	Sudety	k1gFnPc1
<g/>
,	,	kIx,
Č.	Č.	kA
j.	j.	k?
LG-	LG-	k1gMnSc1
<g/>
1242	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIII	VIII	kA
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
↑	↑	k?
KRÁL	Král	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rehabilitujme	rehabilitovat	k5eAaBmRp1nP
název	název	k1gInSc4
Sudety	Sudety	k1gFnPc4
(	(	kIx(
<g/>
Geografické	geografický	k2eAgInPc4d1
rozhledy	rozhled	k1gInPc4
<g/>
,	,	kIx,
ročník	ročník	k1gInSc4
2	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Terra	Terr	k1gMnSc2
klub	klub	k1gInSc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BIČÍK	bičík	k1gInSc4
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Long-Term	Long-Term	k1gInSc1
and	and	k?
the	the	k?
Current	Current	k1gMnSc1
Tendendies	Tendendies	k1gMnSc1
in	in	k?
Land-Use	Land-Use	k1gFnSc1
<g/>
:	:	kIx,
Case	Case	k1gInSc1
Study	stud	k1gInPc1
of	of	k?
the	the	k?
Prague	Pragu	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Environs	Environs	k1gInSc1
and	and	k?
the	the	k?
Czech	Czech	k1gInSc1
Sudetenland	Sudetenland	k1gInSc1
(	(	kIx(
<g/>
Acta	Act	k2eAgFnSc1d1
Universitatis	Universitatis	k1gFnSc1
Carolinae	Carolinae	k1gFnSc1
<g/>
,	,	kIx,
Geographica	Geographica	k1gFnSc1
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Např.	např.	kA
ŠPIČÁKOVÁ	Špičáková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupy	zástup	k1gInPc1
Čechů	Čech	k1gMnPc2
opustily	opustit	k5eAaPmAgInP
po	po	k7c6
Mnichovu	Mnichov	k1gInSc6
Sudety	Sudety	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Glassheim	Glassheim	k1gInSc1
<g/>
,	,	kIx,
Eagle	Eagle	k1gFnSc1
<g/>
:	:	kIx,
Očista	očista	k1gFnSc1
československého	československý	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
<g/>
:	:	kIx,
Migrace	migrace	k1gFnPc4
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
zdraví	zdraví	k1gNnSc4
v	v	k7c6
bývalých	bývalý	k2eAgInPc6d1
Sudetech	Sudety	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-3045-0	978-80-200-3045-0	k4
</s>
<s>
Arburg	Arburg	k1gMnSc1
<g/>
,	,	kIx,
Adrian	Adrian	k1gMnSc1
von	von	k1gInSc1
<g/>
,	,	kIx,
Staněk	Staněk	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysídlení	vysídlení	k1gNnPc4
Němců	Němec	k1gMnPc2
a	a	k8xC
proměny	proměna	k1gFnSc2
českého	český	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
1945	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
:	:	kIx,
dokumenty	dokument	k1gInPc4
z	z	k7c2
českých	český	k2eAgInPc2d1
archivů	archiv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
Středokluky	Středokluk	k1gInPc4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Susa	Susa	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Brügel	Brügel	k1gMnSc1
<g/>
,	,	kIx,
Johann	Johann	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
<g/>
:	:	kIx,
Češi	Čech	k1gMnPc1
a	a	k8xC
Němci	Němec	k1gMnPc1
1918	#num#	k4
–	–	k?
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zykmund	Zykmund	k1gInSc1
<g/>
,	,	kIx,
Neuvirt	Neuvirt	k1gInSc1
<g/>
,	,	kIx,
Kuranda	Kuranda	k1gFnSc1
<g/>
,	,	kIx,
Špaček	Špaček	k1gMnSc1
<g/>
:	:	kIx,
Turn	Turn	k1gMnSc1
<g/>
,	,	kIx,
100	#num#	k4
let	léto	k1gNnPc2
města	město	k1gNnSc2
Trnovany-Teplice	Trnovany-Teplice	k1gFnSc2
<g/>
,	,	kIx,
Europrinty	Europrinta	k1gFnSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zmizelé	zmizelý	k2eAgFnPc1d1
Sudety	Sudety	k1gFnPc1
/	/	kIx~
4	#num#	k4
<g/>
.	.	kIx.
upr	upr	k?
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domažlice	Domažlice	k1gFnPc1
:	:	kIx,
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
656	#num#	k4
s.	s.	k?
:	:	kIx,
il	il	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
mapy	mapa	k1gFnPc1
;	;	kIx,
21	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
ISBN	ISBN	kA
80-86125-73-4	80-86125-73-4	k4
</s>
<s>
Mikšíček	Mikšíček	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Sudetská	sudetský	k2eAgFnSc1d1
pouť	pouť	k1gFnSc1
<g/>
,	,	kIx,
aneb	aneb	k?
<g/>
,	,	kIx,
Waldgang	Waldgang	k1gMnSc1
/	/	kIx~
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Dokořán	dokořán	k6eAd1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
165	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
:	:	kIx,
il	il	k?
<g/>
.	.	kIx.
<g/>
ISBN	ISBN	kA
80-7363-009-5	80-7363-009-5	k4
</s>
<s>
Proměny	proměna	k1gFnPc1
sudetské	sudetský	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
/	/	kIx~
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
]	]	kIx)
:	:	kIx,
Antikomplex	Antikomplex	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
238	#num#	k4
s.	s.	k?
:	:	kIx,
il	il	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86125-75-0	80-86125-75-0	k4
</s>
<s>
Glotz	Glotz	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
,	,	kIx,
Vyhnání	vyhnání	k1gNnSc1
<g/>
:	:	kIx,
České	český	k2eAgFnPc1d1
země	zem	k1gFnPc1
jako	jako	k8xC,k8xS
poučný	poučný	k2eAgInSc1d1
případ	případ	k1gInSc1
/	/	kIx~
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
222	#num#	k4
s.	s.	k?
:	:	kIx,
il	il	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
portréty	portrét	k1gInPc1
;	;	kIx,
21	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
ISBN	ISBN	kA
80-7185-705-X	80-7185-705-X	k4
(	(	kIx(
<g/>
váz	váza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
SUPPAN	SUPPAN	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Österreicher	Österreichra	k1gFnPc2
<g/>
,	,	kIx,
Tschechen	Tschechen	k2eAgInSc1d1
und	und	k?
Sudetendeutschen	Sudetendeutschen	k1gInSc1
als	als	k?
Konfliktgeleinschaft	Konfliktgeleinschaft	k1gInSc1
im	im	k?
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jahrhundert	Jahrhundert	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
157	#num#	k4
<g/>
-	-	kIx~
<g/>
209	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Werner	Werner	k1gMnSc1
Flach	Flach	k1gMnSc1
<g/>
,	,	kIx,
Christa	Christa	k1gMnSc1
Kouschilová	Kouschilová	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Organizovaný	organizovaný	k2eAgInSc1d1
revanšismus	revanšismus	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
strůjci	strůjce	k1gMnPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Svoboda	svoboda	k1gFnSc1
Praha	Praha	k1gFnSc1
1986	#num#	k4
</s>
<s>
SLEZÁK	Slezák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělské	zemědělský	k2eAgInPc1d1
osídlování	osídlování	k1gNnSc2
pohraničí	pohraničí	k1gNnSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Blok	blok	k1gInSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sudetenland	Sudetenland	k1gInSc1
(	(	kIx(
<g/>
provincie	provincie	k1gFnSc2
<g/>
,	,	kIx,
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Říšská	říšský	k2eAgFnSc1d1
župa	župa	k1gFnSc1
Sudety	Sudety	k1gInPc4
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
</s>
<s>
Obsazení	obsazení	k1gNnSc1
Sudet	Sudety	k1gFnPc2
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
program	program	k1gInSc1
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
</s>
<s>
Vyhnání	vyhnání	k1gNnSc1
Čechů	Čech	k1gMnPc2
ze	z	k7c2
Sudet	Sudety	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
</s>
<s>
Druhá	druhý	k4xOgFnSc1
republika	republika	k1gFnSc1
</s>
<s>
Vysídlení	vysídlení	k1gNnSc1
Němců	Němec	k1gMnPc2
z	z	k7c2
Československa	Československo	k1gNnSc2
</s>
<s>
Sudetoněmecké	sudetoněmecký	k2eAgNnSc1d1
krajanské	krajanský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
</s>
<s>
Krkonošsko-jesenická	krkonošsko-jesenický	k2eAgFnSc1d1
subprovincie	subprovincie	k1gFnSc1
</s>
<s>
Pohraničí	pohraničí	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sudety	Sudety	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
Sudety	Sudety	k1gFnPc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Detailní	detailní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
sudetoněmeckých	sudetoněmecký	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
<g/>
,	,	kIx,
vytvořených	vytvořený	k2eAgInPc2d1
na	na	k7c4
podzim	podzim	k1gInSc4
1918	#num#	k4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
:	:	kIx,
Bilaterální	bilaterální	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
–	–	k?
stránky	stránka	k1gFnSc2
Německého	německý	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
zahraničí	zahraničí	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
přestavbě	přestavba	k1gFnSc6
správy	správa	k1gFnSc2
Sudet	Sudety	k1gFnPc2
po	po	k7c6
Mnichovu	Mnichov	k1gInSc6
</s>
<s>
Edice	edice	k1gFnSc1
Vysídlení	vysídlení	k1gNnSc2
Němců	Němec	k1gMnPc2
a	a	k8xC
proměny	proměna	k1gFnSc2
českého	český	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
1945	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
Dokumenty	dokument	k1gInPc7
z	z	k7c2
českých	český	k2eAgInPc2d1
archivů	archiv	k1gInPc2
</s>
<s>
Sudetoněmecké	sudetoněmecký	k2eAgNnSc1d1
krajanské	krajanský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
(	(	kIx(
<g/>
SL	SL	kA
<g/>
)	)	kIx)
</s>
<s>
Sudetoněmecké	sudetoněmecký	k2eAgNnSc1d1
krajanské	krajanský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
SLÖ	SLÖ	kA
<g/>
)	)	kIx)
</s>
<s>
Svaz	svaz	k1gInSc1
vyhnanců	vyhnanec	k1gMnPc2
(	(	kIx(
<g/>
BdV	BdV	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Svaz	svaz	k1gInSc1
landsmanšaftů	landsmanšaft	k1gInPc2
etnických	etnický	k2eAgMnPc2d1
Němců	Němec	k1gMnPc2
Rakouska	Rakousko	k1gNnSc2
(	(	kIx(
<g/>
VLÖ	VLÖ	kA
<g/>
)	)	kIx)
</s>
<s>
Podrobné	podrobný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
slovech	slovo	k1gNnPc6
Sudety	Sudety	k1gFnPc1
<g/>
,	,	kIx,
Sudeten	Sudeten	k2eAgInSc1d1
<g/>
,	,	kIx,
Sudetenland	Sudetenland	k1gInSc1
<g/>
,	,	kIx,
Sudetendeutschland	Sudetendeutschland	k1gInSc1
<g/>
,	,	kIx,
sudeťák	sudeťák	k1gMnSc1
<g/>
,	,	kIx,
sudetoněmecký	sudetoněmecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
…	…	k?
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
–	–	k?
Emil	Emil	k1gMnSc1
Hruška	Hruška	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Mapa	mapa	k1gFnSc1
sudetských	sudetský	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128482	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4058390-9	4058390-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85129624	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
248530085	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85129624	#num#	k4
</s>
