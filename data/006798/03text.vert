<s>
Jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
byla	být	k5eAaImAgFnS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
setkání	setkání	k1gNnSc2	setkání
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
Churchilla	Churchill	k1gMnSc2	Churchill
a	a	k8xC	a
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
)	)	kIx)	)
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
mělo	mít	k5eAaImAgNnS	mít
krycí	krycí	k2eAgInSc4d1	krycí
název	název	k1gInSc4	název
Argonaut	argonaut	k1gMnSc1	argonaut
a	a	k8xC	a
hlavními	hlavní	k2eAgFnPc7d1	hlavní
otázkami	otázka	k1gFnPc7	otázka
projednávanými	projednávaný	k2eAgFnPc7d1	projednávaná
byl	být	k5eAaImAgInS	být
vztah	vztah	k1gInSc4	vztah
Spojenců	spojenec	k1gMnPc2	spojenec
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
projednávala	projednávat	k5eAaImAgFnS	projednávat
polská	polský	k2eAgFnSc1d1	polská
otázka	otázka	k1gFnSc1	otázka
a	a	k8xC	a
také	také	k9	také
vznik	vznik	k1gInSc1	vznik
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
setkání	setkání	k1gNnSc1	setkání
zástupců	zástupce	k1gMnPc2	zástupce
tří	tři	k4xCgFnPc2	tři
mocností	mocnost	k1gFnPc2	mocnost
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
carském	carský	k2eAgInSc6d1	carský
Livadijském	Livadijský	k2eAgInSc6d1	Livadijský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Jaltě	Jalt	k1gInSc6	Jalt
<g/>
,	,	kIx,	,
nejznámějším	známý	k2eAgNnSc6d3	nejznámější
letovisku	letovisko	k1gNnSc6	letovisko
Krymu	Krym	k1gInSc2	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
tzv.	tzv.	kA	tzv.
Deklarace	deklarace	k1gFnSc1	deklarace
o	o	k7c6	o
svobodné	svobodný	k2eAgFnSc6d1	svobodná
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
USA	USA	kA	USA
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
zavázaly	zavázat	k5eAaPmAgInP	zavázat
nechat	nechat	k5eAaPmF	nechat
na	na	k7c6	na
osvobozených	osvobozený	k2eAgNnPc6d1	osvobozené
evropských	evropský	k2eAgNnPc6d1	Evropské
územích	území	k1gNnPc6	území
proběhnout	proběhnout	k5eAaPmF	proběhnout
demokratické	demokratický	k2eAgFnPc4d1	demokratická
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
mocnosti	mocnost	k1gFnPc1	mocnost
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
pomáhat	pomáhat	k5eAaImF	pomáhat
národům	národ	k1gInPc3	národ
osvobozeným	osvobozený	k2eAgInPc3d1	osvobozený
od	od	k7c2	od
nacistů	nacista	k1gMnPc2	nacista
řešit	řešit	k5eAaImF	řešit
demokraticky	demokraticky	k6eAd1	demokraticky
jejich	jejich	k3xOp3gInPc4	jejich
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
místo	místo	k7c2	místo
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
San	San	k1gFnPc4	San
Francisco	Francisco	k1gNnSc1	Francisco
a	a	k8xC	a
konference	konference	k1gFnSc1	konference
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
Jaltě	Jalt	k1gInSc6	Jalt
se	se	k3xPyFc4	se
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
navrhované	navrhovaný	k2eAgFnSc2d1	navrhovaná
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
dohodlo	dohodnout	k5eAaPmAgNnS	dohodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
existenci	existence	k1gFnSc6	existence
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgInSc3d1	původní
Stalinovu	Stalinův	k2eAgInSc3d1	Stalinův
požadavku	požadavek	k1gInSc3	požadavek
na	na	k7c4	na
samostatný	samostatný	k2eAgInSc4d1	samostatný
hlas	hlas	k1gInSc4	hlas
každé	každý	k3xTgFnSc2	každý
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
shodly	shodnout	k5eAaPmAgFnP	shodnout
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
samostatného	samostatný	k2eAgInSc2d1	samostatný
hlasu	hlas	k1gInSc2	hlas
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dohodnuta	dohodnut	k2eAgFnSc1d1	dohodnuta
demilitarizace	demilitarizace	k1gFnSc1	demilitarizace
a	a	k8xC	a
odzbrojení	odzbrojení	k1gNnSc1	odzbrojení
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
země	zem	k1gFnSc2	zem
na	na	k7c4	na
několik	několik	k4yIc4	několik
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
spravovány	spravovat	k5eAaImNgInP	spravovat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
zóna	zóna	k1gFnSc1	zóna
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
britský	britský	k2eAgInSc4d1	britský
nátlak	nátlak	k1gInSc4	nátlak
vymezena	vymezit	k5eAaPmNgFnS	vymezit
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tím	ten	k3xDgNnSc7	ten
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Kontrolní	kontrolní	k2eAgFnSc6d1	kontrolní
komisi	komise	k1gFnSc6	komise
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Německa	Německo	k1gNnSc2	Německo
měla	mít	k5eAaImAgFnS	mít
připadnout	připadnout	k5eAaPmF	připadnout
Polsku	Polsko	k1gNnSc3	Polsko
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
cca	cca	kA	cca
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
odsunuto	odsunout	k5eAaPmNgNnS	odsunout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
státníci	státník	k1gMnPc1	státník
shodli	shodnout	k5eAaBmAgMnP	shodnout
na	na	k7c6	na
reparacích	reparace	k1gFnPc6	reparace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mělo	mít	k5eAaImAgNnS	mít
Německo	Německo	k1gNnSc1	Německo
platit	platit	k5eAaImF	platit
jako	jako	k9	jako
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
ztráty	ztráta	k1gFnPc4	ztráta
způsobené	způsobený	k2eAgFnPc4d1	způsobená
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
reparací	reparace	k1gFnPc2	reparace
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
uhrazena	uhradit	k5eAaPmNgFnS	uhradit
formou	forma	k1gFnSc7	forma
zboží	zboží	k1gNnSc4	zboží
(	(	kIx(	(
<g/>
také	také	k9	také
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
společnostech	společnost	k1gFnPc6	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
sumě	suma	k1gFnSc6	suma
22	[number]	k4	22
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
mělo	mít	k5eAaImAgNnS	mít
50	[number]	k4	50
%	%	kIx~	%
připadnout	připadnout	k5eAaPmF	připadnout
Sovětům	Sovět	k1gMnPc3	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
konečnou	konečný	k2eAgFnSc4d1	konečná
sumu	suma	k1gFnSc4	suma
bude	být	k5eAaImBp3nS	být
možno	možno	k6eAd1	možno
stanovit	stanovit	k5eAaPmF	stanovit
až	až	k9	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
sovětsko-americký	sovětskomerický	k2eAgInSc1d1	sovětsko-americký
návrh	návrh	k1gInSc1	návrh
předložen	předložen	k2eAgInSc1d1	předložen
Reparační	reparační	k2eAgFnSc3d1	reparační
komisi	komise	k1gFnSc3	komise
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
shodě	shoda	k1gFnSc3	shoda
o	o	k7c6	o
válečných	válečný	k2eAgInPc6d1	válečný
zločinech	zločin	k1gInPc6	zločin
a	a	k8xC	a
řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
bylo	být	k5eAaImAgNnS	být
odloženo	odložit	k5eAaPmNgNnS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
měla	mít	k5eAaImAgFnS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
demokratická	demokratický	k2eAgFnSc1d1	demokratická
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zastoupení	zastoupení	k1gNnSc3	zastoupení
i	i	k9	i
nekomunistický	komunistický	k2eNgInSc4d1	nekomunistický
odboj	odboj	k1gInSc4	odboj
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
londýnské	londýnský	k2eAgFnSc2d1	londýnská
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
porážkou	porážka	k1gFnSc7	porážka
Britů	Brit	k1gMnPc2	Brit
a	a	k8xC	a
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prakticky	prakticky	k6eAd1	prakticky
znamenalo	znamenat	k5eAaImAgNnS	znamenat
jen	jen	k9	jen
doplnění	doplnění	k1gNnSc4	doplnění
loutkové	loutkový	k2eAgFnSc2d1	loutková
lublinské	lublinský	k2eAgFnSc2d1	Lublinská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
řízené	řízený	k2eAgNnSc1d1	řízené
z	z	k7c2	z
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
požadavek	požadavek	k1gInSc1	požadavek
o	o	k7c4	o
odvolání	odvolání	k1gNnSc4	odvolání
této	tento	k3xDgFnSc2	tento
lublinské	lublinský	k2eAgFnSc2d1	Lublinská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
Stalina	Stalin	k1gMnSc2	Stalin
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
byla	být	k5eAaImAgFnS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Titem	Tit	k1gInSc7	Tit
a	a	k8xC	a
Šubašićem	Šubašić	k1gInSc7	Šubašić
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přiblížit	přiblížit	k5eAaPmF	přiblížit
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
a	a	k8xC	a
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
válku	válka	k1gFnSc4	válka
Japonsku	Japonsko	k1gNnSc3	Japonsko
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
získat	získat	k5eAaPmF	získat
Sachalin	Sachalin	k1gInSc4	Sachalin
a	a	k8xC	a
ostrovy	ostrov	k1gInPc4	ostrov
jemu	on	k3xPp3gMnSc3	on
přilehlé	přilehlý	k2eAgNnSc1d1	přilehlé
<g/>
,	,	kIx,	,
Kurilské	kurilský	k2eAgInPc1d1	kurilský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
nájem	nájem	k1gInSc1	nájem
přístavu	přístav	k1gInSc2	přístav
Port	porta	k1gFnPc2	porta
Arthur	Arthura	k1gFnPc2	Arthura
<g/>
,	,	kIx,	,
výsadní	výsadní	k2eAgNnPc4d1	výsadní
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Dairen	Dairno	k1gNnPc2	Dairno
a	a	k8xC	a
mimořádná	mimořádný	k2eAgNnPc4d1	mimořádné
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
Východočínské	východočínský	k2eAgFnSc2d1	východočínský
a	a	k8xC	a
Jihomandžuské	Jihomandžuský	k2eAgFnSc2d1	Jihomandžuský
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
italsko-jugoslávské	italskougoslávský	k2eAgFnSc6d1	italsko-jugoslávský
a	a	k8xC	a
italsko-rakouské	italskoakouský	k2eAgFnSc6d1	italsko-rakouská
hranici	hranice	k1gFnSc6	hranice
bylo	být	k5eAaImAgNnS	být
odloženo	odložit	k5eAaPmNgNnS	odložit
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
ohledně	ohledně	k7c2	ohledně
Jugoslávsko-bulharských	jugoslávskoulharský	k2eAgInPc2d1	jugoslávsko-bulharský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
montreuxské	montreuxské	k2eAgFnSc4d1	montreuxské
konvenci	konvence	k1gFnSc4	konvence
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
dohod	dohoda	k1gFnPc2	dohoda
byla	být	k5eAaImAgFnS	být
tajná	tajný	k2eAgFnSc1d1	tajná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
se	se	k3xPyFc4	se
samostatným	samostatný	k2eAgNnSc7d1	samostatné
zastoupením	zastoupení	k1gNnSc7	zastoupení
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
v	v	k7c6	v
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
veřejnost	veřejnost	k1gFnSc1	veřejnost
seznámila	seznámit	k5eAaPmAgFnS	seznámit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Dohoda	dohoda	k1gFnSc1	dohoda
tří	tři	k4xCgFnPc2	tři
velmocí	velmoc	k1gFnPc2	velmoc
o	o	k7c6	o
otázkách	otázka	k1gFnPc6	otázka
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
byla	být	k5eAaImAgNnP	být
zveřejněna	zveřejnit	k5eAaPmNgNnP	zveřejnit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
Truman	Truman	k1gMnSc1	Truman
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Jalty	Jalta	k1gFnSc2	Jalta
Rooseveltův	Rooseveltův	k2eAgMnSc1d1	Rooseveltův
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
existenci	existence	k1gFnSc6	existence
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
až	až	k9	až
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
jaltské	jaltský	k2eAgFnSc2d1	Jaltská
konference	konference	k1gFnSc2	konference
Protokol	protokol	k1gInSc1	protokol
z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
Jaltě	Jalta	k1gFnSc6	Jalta
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Závěry	závěr	k1gInPc1	závěr
jaltské	jaltský	k2eAgFnSc2d1	Jaltská
konference	konference	k1gFnSc2	konference
tří	tři	k4xCgFnPc2	tři
mocností	mocnost	k1gFnPc2	mocnost
</s>
