<s>
Jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
byla	být	k5eAaImAgFnS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
setkání	setkání	k1gNnSc2	setkání
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
Churchilla	Churchill	k1gMnSc2	Churchill
a	a	k8xC	a
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
)	)	kIx)	)
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
