<s>
Symphonic	Symphonice	k1gFnPc2	Symphonice
black	black	k6eAd1	black
metal	metal	k1gInSc1	metal
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
black	black	k6eAd1	black
metal	metal	k1gInSc4	metal
a	a	k8xC	a
orchestrální	orchestrální	k2eAgInPc4d1	orchestrální
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
klasické	klasický	k2eAgInPc4d1	klasický
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
,	,	kIx,	,
žesťové	žesťový	k2eAgInPc4d1	žesťový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
podtrhující	podtrhující	k2eAgFnSc4d1	podtrhující
strašidelnou	strašidelný	k2eAgFnSc4d1	strašidelná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
v	v	k7c6	v
symphonic	symphonice	k1gFnPc2	symphonice
black	black	k1gInSc1	black
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
čistý	čistý	k2eAgInSc4d1	čistý
ženský	ženský	k2eAgInSc4d1	ženský
vokál	vokál	k1gInSc4	vokál
<g/>
.	.	kIx.	.
</s>
