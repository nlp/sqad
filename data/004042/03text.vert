<s>
Axiom	axiom	k1gInSc1	axiom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
axióma	axióma	k1gNnSc1	axióma
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
co	co	k9	co
se	se	k3xPyFc4	se
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
předem	předem	k6eAd1	předem
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
platné	platný	k2eAgNnSc4d1	platné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
nedokazuje	dokazovat	k5eNaImIp3nS	dokazovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
slovo	slovo	k1gNnSc4	slovo
postulát	postulát	k1gInSc1	postulát
<g/>
.	.	kIx.	.
</s>
<s>
Matematické	matematický	k2eAgFnPc1d1	matematická
teorie	teorie	k1gFnPc1	teorie
lze	lze	k6eAd1	lze
založit	založit	k5eAaPmF	založit
na	na	k7c6	na
soustavách	soustava	k1gFnPc6	soustava
axiomů	axiom	k1gInPc2	axiom
(	(	kIx(	(
<g/>
od	od	k7c2	od
nichž	jenž	k3xRgFnPc2	jenž
požadujeme	požadovat	k5eAaImIp1nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
vnitřně	vnitřně	k6eAd1	vnitřně
bezesporné	bezesporný	k2eAgFnPc1d1	bezesporná
a	a	k8xC	a
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
aby	aby	kYmCp3nS	aby
daná	daný	k2eAgFnSc1d1	daná
skupina	skupina	k1gFnSc1	skupina
axiomů	axiom	k1gInPc2	axiom
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
dva	dva	k4xCgInPc4	dva
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
protiřečící	protiřečící	k2eAgInPc4d1	protiřečící
axiomy	axiom	k1gInPc4	axiom
a	a	k8xC	a
současně	současně	k6eAd1	současně
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
odvodit	odvodit	k5eAaPmF	odvodit
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
axiomů	axiom	k1gInPc2	axiom
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
vytváření	vytváření	k1gNnSc2	vytváření
matematických	matematický	k2eAgFnPc2d1	matematická
teorií	teorie	k1gFnPc2	teorie
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
axiomatickou	axiomatický	k2eAgFnSc4d1	axiomatická
a	a	k8xC	a
takto	takto	k6eAd1	takto
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
teorii	teorie	k1gFnSc4	teorie
za	za	k7c4	za
teorii	teorie	k1gFnSc4	teorie
formální	formální	k2eAgFnSc4d1	formální
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prokazování	prokazování	k1gNnSc4	prokazování
tvrzení	tvrzení	k1gNnSc2	tvrzení
ve	v	k7c6	v
formálních	formální	k2eAgFnPc6d1	formální
teoriích	teorie	k1gFnPc6	teorie
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
formální	formální	k2eAgInSc1d1	formální
důkaz	důkaz	k1gInSc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
formálních	formální	k2eAgInPc2d1	formální
důkazů	důkaz	k1gInPc2	důkaz
lišících	lišící	k2eAgInPc2d1	lišící
se	se	k3xPyFc4	se
systémy	systém	k1gInPc7	systém
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
dokazování	dokazování	k1gNnSc4	dokazování
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kalkuly	kalkul	k1gInPc1	kalkul
–	–	k?	–
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
hilbertovský	hilbertovský	k2eAgInSc4d1	hilbertovský
a	a	k8xC	a
gentzenovský	gentzenovský	k2eAgInSc4d1	gentzenovský
kalkulus	kalkulus	k1gInSc4	kalkulus
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
logický	logický	k2eAgInSc4d1	logický
kalkulus	kalkulus	k1gInSc4	kalkulus
celé	celý	k2eAgFnSc2d1	celá
matematiky	matematika	k1gFnSc2	matematika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
axiomatických	axiomatický	k2eAgFnPc2d1	axiomatická
teorií	teorie	k1gFnPc2	teorie
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
co	co	k3yRnSc4	co
největší	veliký	k2eAgNnSc4d3	veliký
zpřesnění	zpřesnění	k1gNnSc4	zpřesnění
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
axiomatické	axiomatický	k2eAgFnSc3d1	axiomatická
metodě	metoda	k1gFnSc3	metoda
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
matematika	matematika	k1gFnSc1	matematika
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
geometrickém	geometrický	k2eAgInSc6d1	geometrický
(	(	kIx(	(
<g/>
či	či	k8xC	či
jiném	jiný	k2eAgMnSc6d1	jiný
<g/>
)	)	kIx)	)
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
intuici	intuice	k1gFnSc4	intuice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pojetí	pojetí	k1gNnSc6	pojetí
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc1	některý
tvrzení	tvrzení	k1gNnPc1	tvrzení
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
natolik	natolik	k6eAd1	natolik
intuitivně	intuitivně	k6eAd1	intuitivně
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
a	a	k8xC	a
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
blíže	blízce	k6eAd2	blízce
zdůvodňovat	zdůvodňovat	k5eAaImF	zdůvodňovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvrzení	tvrzení	k1gNnSc1	tvrzení
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
Bolzanova	Bolzanův	k2eAgFnSc1d1	Bolzanova
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spojitá	spojitý	k2eAgFnSc1d1	spojitá
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nabývá	nabývat	k5eAaImIp3nS	nabývat
alespoň	alespoň	k9	alespoň
jedné	jeden	k4xCgFnSc2	jeden
kladné	kladný	k2eAgFnSc2d1	kladná
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
záporné	záporný	k2eAgFnSc2d1	záporná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
musí	muset	k5eAaImIp3nS	muset
nabývat	nabývat	k5eAaImF	nabývat
i	i	k9	i
hodnotu	hodnota	k1gFnSc4	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
pojetí	pojetí	k1gNnSc6	pojetí
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jen	jen	k9	jen
návodem	návod	k1gInSc7	návod
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
intuitivně	intuitivně	k6eAd1	intuitivně
zřejmých	zřejmý	k2eAgNnPc2d1	zřejmé
pozorování	pozorování	k1gNnPc2	pozorování
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
platnost	platnost	k1gFnSc4	platnost
daného	daný	k2eAgNnSc2d1	dané
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
pojetí	pojetí	k1gNnSc4	pojetí
s	s	k7c7	s
sebou	se	k3xPyFc7	se
ovšem	ovšem	k9	ovšem
nese	nést	k5eAaImIp3nS	nést
řadu	řada	k1gFnSc4	řada
rizik	riziko	k1gNnPc2	riziko
–	–	k?	–
například	například	k6eAd1	například
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
někomu	někdo	k3yInSc3	někdo
přijde	přijít	k5eAaPmIp3nS	přijít
intuitivně	intuitivně	k6eAd1	intuitivně
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
rozrůstajících	rozrůstající	k2eAgInPc6d1	rozrůstající
se	se	k3xPyFc4	se
matematických	matematický	k2eAgFnPc6d1	matematická
teoriích	teorie	k1gFnPc6	teorie
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jediný	jediný	k2eAgMnSc1d1	jediný
člověk	člověk	k1gMnSc1	člověk
přečetl	přečíst	k5eAaPmAgMnS	přečíst
všechny	všechen	k3xTgInPc4	všechen
existující	existující	k2eAgInPc4d1	existující
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pojetí	pojetí	k1gNnSc6	pojetí
nucen	nucen	k2eAgMnSc1d1	nucen
přijímat	přijímat	k5eAaImF	přijímat
za	za	k7c4	za
pravdivá	pravdivý	k2eAgNnPc4d1	pravdivé
i	i	k8xC	i
taková	takový	k3xDgNnPc4	takový
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
důkazech	důkaz	k1gInPc6	důkaz
použité	použitý	k2eAgFnSc2d1	použitá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
opravdu	opravdu	k6eAd1	opravdu
intuitivně	intuitivně	k6eAd1	intuitivně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
axiomatická	axiomatický	k2eAgFnSc1d1	axiomatická
metoda	metoda	k1gFnSc1	metoda
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
základní	základní	k2eAgNnSc1d1	základní
tvrzení	tvrzení	k1gNnPc1	tvrzení
nazývaná	nazývaný	k2eAgNnPc1d1	nazývané
axiomy	axiom	k1gInPc7	axiom
lze	lze	k6eAd1	lze
připustit	připustit	k5eAaPmF	připustit
bez	bez	k7c2	bez
důkazu	důkaz	k1gInSc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
axiomů	axiom	k1gInPc2	axiom
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
axiomatické	axiomatický	k2eAgFnSc2d1	axiomatická
metody	metoda	k1gFnSc2	metoda
jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
matematické	matematický	k2eAgFnSc6d1	matematická
teorii	teorie	k1gFnSc6	teorie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
důkazu	důkaz	k1gInSc3	důkaz
tvrzení	tvrzení	k1gNnSc2	tvrzení
postačí	postačit	k5eAaPmIp3nS	postačit
názor	názor	k1gInSc1	názor
a	a	k8xC	a
intuice	intuice	k1gFnSc1	intuice
<g/>
.	.	kIx.	.
</s>
<s>
Kdokoli	kdokoli	k3yInSc1	kdokoli
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
uznat	uznat	k5eAaPmF	uznat
výběr	výběr	k1gInSc4	výběr
axiomů	axiom	k1gInPc2	axiom
a	a	k8xC	a
odvozovacích	odvozovací	k2eAgNnPc2d1	odvozovací
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
zkoumané	zkoumaný	k2eAgFnSc6d1	zkoumaná
teorii	teorie	k1gFnSc6	teorie
za	za	k7c4	za
správný	správný	k2eAgInSc4d1	správný
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
si	se	k3xPyFc3	se
již	již	k9	již
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jistý	jistý	k2eAgInSc4d1	jistý
platností	platnost	k1gFnSc7	platnost
každého	každý	k3xTgNnSc2	každý
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
formálně	formálně	k6eAd1	formálně
odvozené	odvozený	k2eAgInPc4d1	odvozený
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
používání	používání	k1gNnSc1	používání
axiomů	axiom	k1gInPc2	axiom
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
matematik	matematik	k1gMnSc1	matematik
Eukleidés	Eukleidésa	k1gFnPc2	Eukleidésa
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Základy	základ	k1gInPc1	základ
zavedl	zavést	k5eAaPmAgInS	zavést
pět	pět	k4xCc4	pět
geometrických	geometrický	k2eAgInPc2d1	geometrický
axiomů	axiom	k1gInPc2	axiom
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgMnPc2	jenž
byl	být	k5eAaImAgInS	být
schopen	schopit	k5eAaPmNgInS	schopit
logicky	logicky	k6eAd1	logicky
odvozovat	odvozovat	k5eAaImF	odvozovat
všechny	všechen	k3xTgMnPc4	všechen
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známé	známý	k2eAgFnSc2d1	známá
geometrické	geometrický	k2eAgFnSc2d1	geometrická
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
axiomy	axiom	k1gInPc1	axiom
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
jako	jako	k8xC	jako
Euklidovy	Euklidův	k2eAgInPc1d1	Euklidův
postuláty	postulát	k1gInPc1	postulát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
rozvoji	rozvoj	k1gInSc3	rozvoj
axiomatické	axiomatický	k2eAgFnSc2d1	axiomatická
metody	metoda	k1gFnSc2	metoda
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
století	století	k1gNnSc2	století
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
axiomatizována	axiomatizovat	k5eAaImNgFnS	axiomatizovat
i	i	k9	i
logika	logika	k1gFnSc1	logika
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
axiomatické	axiomatický	k2eAgFnSc2d1	axiomatická
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
teorií	teorie	k1gFnSc7	teorie
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
celou	celý	k2eAgFnSc4d1	celá
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
změně	změna	k1gFnSc6	změna
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
podíleli	podílet	k5eAaImAgMnP	podílet
David	David	k1gMnSc1	David
Hilbert	Hilbert	k1gMnSc1	Hilbert
<g/>
,	,	kIx,	,
Bertrand	Bertrand	k1gInSc1	Bertrand
Russell	Russell	k1gInSc1	Russell
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Gödel	Gödel	k1gMnSc1	Gödel
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Zermelo	Zermela	k1gFnSc5	Zermela
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
Gentzen	Gentzen	k2eAgMnSc1d1	Gentzen
ale	ale	k8xC	ale
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
logice	logika	k1gFnSc6	logika
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
axiomů	axiom	k1gInPc2	axiom
–	–	k?	–
axiomy	axiom	k1gInPc1	axiom
logické	logický	k2eAgInPc1d1	logický
a	a	k8xC	a
vlastní	vlastní	k2eAgInPc1d1	vlastní
axiomy	axiom	k1gInPc1	axiom
nějaké	nějaký	k3yIgFnSc2	nějaký
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Axiom	axiom	k1gInSc1	axiom
teorie	teorie	k1gFnSc2	teorie
T	T	kA	T
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
L	L	kA	L
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
formule	formule	k1gFnSc1	formule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc3	varphi
}	}	kIx)	}
jazyka	jazyk	k1gInSc2	jazyk
L	L	kA	L
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
∈	∈	k?	∈
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc7	varphi
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
z	z	k7c2	z
formálního	formální	k2eAgNnSc2d1	formální
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
teorie	teorie	k1gFnSc1	teorie
množina	množina	k1gFnSc1	množina
svých	svůj	k3xOyFgInPc2	svůj
(	(	kIx(	(
<g/>
vlastních	vlastní	k2eAgInPc2d1	vlastní
<g/>
)	)	kIx)	)
axiomů	axiom	k1gInPc2	axiom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnSc3	takový
formuli	formule	k1gFnSc3	formule
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
vlastní	vlastní	k2eAgInSc4d1	vlastní
axiom	axiom	k1gInSc4	axiom
T.	T.	kA	T.
Na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
axiomy	axiom	k1gInPc4	axiom
teorií	teorie	k1gFnPc2	teorie
tedy	tedy	k8xC	tedy
nejsou	být	k5eNaImIp3nP	být
kladeny	klást	k5eAaImNgInP	klást
žádné	žádný	k3yNgInPc1	žádný
jiné	jiný	k2eAgInPc1d1	jiný
požadavky	požadavek	k1gInPc1	požadavek
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
jít	jít	k5eAaImF	jít
o	o	k7c4	o
správně	správně	k6eAd1	správně
utvořené	utvořený	k2eAgFnPc4d1	utvořená
formule	formule	k1gFnPc4	formule
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
axiomatické	axiomatický	k2eAgFnPc1d1	axiomatická
teorie	teorie	k1gFnPc1	teorie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
také	také	k9	také
zcela	zcela	k6eAd1	zcela
libovolné	libovolný	k2eAgFnPc4d1	libovolná
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k9	zvlášť
poznamenejme	poznamenat	k5eAaPmRp1nP	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
také	také	k9	také
teorií	teorie	k1gFnSc7	teorie
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prázdná	prázdný	k2eAgFnSc1d1	prázdná
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hilbertovský	Hilbertovský	k2eAgInSc4d1	Hilbertovský
kalkulus	kalkulus	k1gInSc4	kalkulus
<g/>
.	.	kIx.	.
</s>
<s>
Logické	logický	k2eAgInPc1d1	logický
axiomy	axiom	k1gInPc1	axiom
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
základní	základní	k2eAgNnPc4d1	základní
pravidla	pravidlo	k1gNnPc4	pravidlo
rozumového	rozumový	k2eAgNnSc2d1	rozumové
odvozování	odvozování	k1gNnSc2	odvozování
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
formulovány	formulovat	k5eAaImNgInP	formulovat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
bez	bez	k7c2	bez
mimologických	mimologický	k2eAgInPc2d1	mimologický
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
nevztahují	vztahovat	k5eNaImIp3nP	vztahovat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
konkrétní	konkrétní	k2eAgFnSc3d1	konkrétní
teorii	teorie	k1gFnSc3	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
systému	systém	k1gInSc6	systém
<g/>
)	)	kIx)	)
pevně	pevně	k6eAd1	pevně
dány	dán	k2eAgFnPc1d1	dána
a	a	k8xC	a
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
k	k	k7c3	k
vlastním	vlastní	k2eAgInPc3d1	vlastní
axiomům	axiom	k1gInPc3	axiom
každé	každý	k3xTgFnSc2	každý
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
logických	logický	k2eAgInPc2d1	logický
axiomů	axiom	k1gInPc2	axiom
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
logické	logický	k2eAgInPc4d1	logický
kalkuly	kalkul	k1gInPc4	kalkul
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
tentýž	týž	k3xTgInSc4	týž
kalkulus	kalkulus	k1gInSc4	kalkulus
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgFnPc2d3	nejběžnější
definicí	definice	k1gFnPc2	definice
logických	logický	k2eAgInPc2d1	logický
axiomů	axiom	k1gInPc2	axiom
pro	pro	k7c4	pro
výrokovou	výrokový	k2eAgFnSc4d1	výroková
logiku	logika	k1gFnSc4	logika
resp.	resp.	kA	resp.
predikátovou	predikátový	k2eAgFnSc4d1	predikátová
logiku	logika	k1gFnSc4	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
hilbertovský	hilbertovský	k2eAgInSc4d1	hilbertovský
klasický	klasický	k2eAgInSc4d1	klasický
kalkulus	kalkulus	k1gInSc4	kalkulus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kalkulus	kalkulus	k1gInSc1	kalkulus
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
logickým	logický	k2eAgInSc7d1	logický
kalkulem	kalkul	k1gInSc7	kalkul
používaným	používaný	k2eAgInSc7d1	používaný
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
matematice	matematika	k1gFnSc6	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
jak	jak	k8xC	jak
zvolit	zvolit	k5eAaPmF	zvolit
logické	logický	k2eAgInPc4d1	logický
axiomy	axiom	k1gInPc4	axiom
je	být	k5eAaImIp3nS	být
gentzenovský	gentzenovský	k2eAgInSc1d1	gentzenovský
kalkulus	kalkulus	k1gInSc1	kalkulus
<g/>
.	.	kIx.	.
</s>
