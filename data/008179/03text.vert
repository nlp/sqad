<p>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
DPO	DPO	kA	DPO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc4	společnost
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
provoz	provoz	k1gInSc4	provoz
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
MHD	MHD	kA	MHD
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
síť	síť	k1gFnSc1	síť
autobusových	autobusový	k2eAgFnPc2d1	autobusová
<g/>
,	,	kIx,	,
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
a	a	k8xC	a
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
do	do	k7c2	do
Ostravského	ostravský	k2eAgInSc2d1	ostravský
dopravního	dopravní	k2eAgInSc2d1	dopravní
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgInPc1d1	dopravní
podniky	podnik	k1gInPc1	podnik
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
spojením	spojení	k1gNnSc7	spojení
původní	původní	k2eAgFnSc2d1	původní
Společnosti	společnost	k1gFnSc2	společnost
moravských	moravský	k2eAgFnPc2d1	Moravská
místních	místní	k2eAgFnPc2d1	místní
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
tramvaje	tramvaj	k1gFnPc1	tramvaj
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zemských	zemský	k2eAgFnPc2d1	zemská
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
úzkorozchodné	úzkorozchodný	k2eAgFnPc1d1	úzkorozchodná
dráhy	dráha	k1gFnPc1	dráha
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
,	,	kIx,	,
Karvinsku	Karvinsko	k1gNnSc6	Karvinsko
a	a	k8xC	a
Bohumínsku	Bohumínsko	k1gNnSc6	Bohumínsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Místní	místní	k2eAgFnSc2d1	místní
dráhy	dráha	k1gFnSc2	dráha
Ostrava	Ostrava	k1gFnSc1	Ostrava
–	–	k?	–
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
připojily	připojit	k5eAaPmAgFnP	připojit
ještě	ještě	k9	ještě
Vítkovické	vítkovický	k2eAgFnPc1d1	Vítkovická
závodní	závodní	k2eAgFnPc1d1	závodní
dráhy	dráha	k1gFnPc1	dráha
(	(	kIx(	(
<g/>
provoz	provoz	k1gInSc1	provoz
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
Vítkovicích	Vítkovice	k1gInPc6	Vítkovice
a	a	k8xC	a
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
DPO	DPO	kA	DPO
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
100	[number]	k4	100
<g/>
%	%	kIx~	%
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
statutárním	statutární	k2eAgInPc3d1	statutární
městem	město	k1gNnSc7	město
Ostravou	Ostrava	k1gFnSc7	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
ostravské	ostravský	k2eAgFnSc2d1	Ostravská
MHD	MHD	kA	MHD
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tramvaje	tramvaj	k1gFnSc2	tramvaj
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
DP	DP	kA	DP
====	====	k?	====
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
projekt	projekt	k1gInSc1	projekt
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
realizace	realizace	k1gFnSc1	realizace
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
až	až	k9	až
projekt	projekt	k1gInSc4	projekt
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1894	[number]	k4	1894
z	z	k7c2	z
Přívozu	přívoz	k1gInSc2	přívoz
(	(	kIx(	(
<g/>
od	od	k7c2	od
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
do	do	k7c2	do
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
ještě	ještě	k9	ještě
druhá	druhý	k4xOgFnSc1	druhý
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
síť	síť	k1gFnSc1	síť
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
elektrifikována	elektrifikován	k2eAgFnSc1d1	elektrifikována
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1901	[number]	k4	1901
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
elektrických	elektrický	k2eAgFnPc2d1	elektrická
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
páteřní	páteřní	k2eAgFnPc1d1	páteřní
tratě	trať	k1gFnPc1	trať
(	(	kIx(	(
<g/>
Přívoz	přívoz	k1gInSc1	přívoz
nádraží	nádraží	k1gNnSc2	nádraží
–	–	k?	–
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
město	město	k1gNnSc1	město
–	–	k?	–
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
a	a	k8xC	a
Ostravický	ostravický	k2eAgInSc1d1	ostravický
most	most	k1gInSc1	most
–	–	k?	–
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
město	město	k1gNnSc1	město
–	–	k?	–
Svinov	Svinovo	k1gNnPc2	Svinovo
<g/>
)	)	kIx)	)
ostravské	ostravský	k2eAgFnSc2d1	Ostravská
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
síť	síť	k1gFnSc1	síť
připojena	připojen	k2eAgFnSc1d1	připojena
původně	původně	k6eAd1	původně
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Svinov	Svinov	k1gInSc1	Svinov
–	–	k?	–
Klimkovice	Klimkovice	k1gFnSc1	Klimkovice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
zdvoukolejnění	zdvoukolejnění	k1gNnSc1	zdvoukolejnění
nejvytíženějších	vytížený	k2eAgInPc2d3	nejvytíženější
úseků	úsek	k1gInPc2	úsek
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
DP	DP	kA	DP
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Poruby	Poruba	k1gFnSc2	Poruba
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
do	do	k7c2	do
Martinova	Martinův	k2eAgMnSc2d1	Martinův
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
ústředním	ústřední	k2eAgFnPc3d1	ústřední
dílnám	dílna	k1gFnPc3	dílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
probíhala	probíhat	k5eAaImAgFnS	probíhat
výstavba	výstavba	k1gFnSc1	výstavba
do	do	k7c2	do
nových	nový	k2eAgNnPc2d1	nové
sídlišť	sídliště	k1gNnPc2	sídliště
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovějším	nový	k2eAgInSc7d3	nejnovější
úsekem	úsek	k1gInSc7	úsek
je	být	k5eAaImIp3nS	být
trať	trať	k1gFnSc1	trať
podél	podél	k7c2	podél
Místecké	místecký	k2eAgFnSc2d1	Místecká
ulice	ulice	k1gFnSc2	ulice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Autobusy	autobus	k1gInPc1	autobus
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Autobusy	autobus	k1gInPc1	autobus
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
DP	DP	kA	DP
====	====	k?	====
</s>
</p>
<p>
<s>
Městské	městský	k2eAgInPc1d1	městský
autobusy	autobus	k1gInPc1	autobus
se	se	k3xPyFc4	se
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
první	první	k4xOgFnSc6	první
lince	linka	k1gFnSc6	linka
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
–	–	k?	–
Přívoz	přívoz	k1gInSc1	přívoz
chemická	chemický	k2eAgFnSc1d1	chemická
továrna	továrna	k1gFnSc1	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
autobusy	autobus	k1gInPc1	autobus
nikdy	nikdy	k6eAd1	nikdy
nezískaly	získat	k5eNaPmAgFnP	získat
žádný	žádný	k3yNgInSc4	žádný
velký	velký	k2eAgInSc4d1	velký
přepravní	přepravní	k2eAgInSc4d1	přepravní
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
provozováno	provozovat	k5eAaImNgNnS	provozovat
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
DP	DP	kA	DP
====	====	k?	====
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
díky	díky	k7c3	díky
levné	levný	k2eAgFnSc3d1	levná
ropě	ropa	k1gFnSc3	ropa
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
masově	masově	k6eAd1	masově
vyrábět	vyrábět	k5eAaImF	vyrábět
autobusy	autobus	k1gInPc4	autobus
v	v	k7c6	v
Karose	karosa	k1gFnSc6	karosa
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
Mýto	mýto	k1gNnSc1	mýto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Trolejbusy	trolejbus	k1gInPc4	trolejbus
===	===	k?	===
</s>
</p>
<p>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
trolejbusů	trolejbus	k1gInPc2	trolejbus
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
je	být	k5eAaImIp3nS	být
datováno	datovat	k5eAaImNgNnS	datovat
k	k	k7c3	k
9	[number]	k4	9
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
okružní	okružní	k2eAgFnSc1d1	okružní
trať	trať	k1gFnSc1	trať
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
rozvoj	rozvoj	k1gInSc4	rozvoj
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Fifejdy	Fifejd	k1gMnPc4	Fifejd
<g/>
,	,	kIx,	,
k	k	k7c3	k
dolu	dol	k1gInSc3	dol
Heřmanice	Heřmanice	k1gFnSc2	Heřmanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
se	se	k3xPyFc4	se
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
nerozšiřovala	rozšiřovat	k5eNaImAgFnS	rozšiřovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Koblova	Koblův	k2eAgNnSc2d1	Koblův
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
prodloužení	prodloužení	k1gNnSc2	prodloužení
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
sítě	síť	k1gFnSc2	síť
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zprovoznění	zprovoznění	k1gNnSc4	zprovoznění
nové	nový	k2eAgFnSc2d1	nová
linky	linka	k1gFnSc2	linka
k	k	k7c3	k
obchodnímu	obchodní	k2eAgInSc3d1	obchodní
centru	centr	k1gInSc3	centr
Karolina	Karolinum	k1gNnSc2	Karolinum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
terminál	terminál	k1gInSc1	terminál
Hranečník	hranečník	k1gInSc1	hranečník
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
vede	vést	k5eAaImIp3nS	vést
nová	nový	k2eAgFnSc1d1	nová
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
trať	trať	k1gFnSc1	trať
po	po	k7c6	po
Těšínské	Těšínské	k2eAgFnSc6d1	Těšínské
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
tratí	trať	k1gFnPc2	trať
přes	přes	k7c4	přes
Františkov	Františkov	k1gInSc4	Františkov
a	a	k8xC	a
podél	podél	k7c2	podél
Havlíčkova	Havlíčkův	k2eAgNnSc2d1	Havlíčkovo
nábřeží	nábřeží	k1gNnSc2	nábřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
vozidel	vozidlo	k1gNnPc2	vozidlo
MHD	MHD	kA	MHD
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dopravní	dopravní	k2eAgInSc4d1	dopravní
podnik	podnik	k1gInSc4	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
Koordinátor	koordinátor	k1gMnSc1	koordinátor
ODIS	ODIS	kA	ODIS
</s>
</p>
