<s>
Slavnosti	slavnost	k1gFnSc2	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
režisérem	režisér	k1gMnSc7	režisér
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
i	i	k8xC	i
sám	sám	k3xTgInSc1	sám
hraje	hrát	k5eAaImIp3nS	hrát
malou	malý	k2eAgFnSc4d1	malá
epizodní	epizodní	k2eAgFnSc4d1	epizodní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
