<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ф	Ф	k?	Ф
М	М	k?	М
Д	Д	k?	Д
<g/>
;	;	kIx,	;
30	[number]	k4	30
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1821	[number]	k4	1821
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
lednajul	lednajout	k5eAaPmAgInS	lednajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1881	[number]	k4	1881
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
světových	světový	k2eAgMnPc2d1	světový
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgMnSc1d1	vrcholný
představitel	představitel	k1gMnSc1	představitel
ruského	ruský	k2eAgInSc2d1	ruský
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
současně	současně	k6eAd1	současně
předchůdce	předchůdce	k1gMnSc1	předchůdce
moderní	moderní	k2eAgFnSc2d1	moderní
psychologické	psychologický	k2eAgFnSc2d1	psychologická
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
lékaře	lékař	k1gMnSc2	lékař
jako	jako	k8xS	jako
druhé	druhý	k4xOgFnSc2	druhý
z	z	k7c2	z
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
vojenské	vojenský	k2eAgNnSc4d1	vojenské
technické	technický	k2eAgNnSc4d1	technické
učiliště	učiliště	k1gNnSc4	učiliště
(	(	kIx(	(
<g/>
obor	obor	k1gInSc1	obor
konstrukce	konstrukce	k1gFnSc2	konstrukce
mostů	most	k1gInPc2	most
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
utopického	utopický	k2eAgMnSc2d1	utopický
socialisty	socialista	k1gMnSc2	socialista
Michaila	Michail	k1gMnSc2	Michail
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
Petraševského	Petraševský	k2eAgMnSc2d1	Petraševský
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Petraševci	Petraševce	k1gMnSc3	Petraševce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
proti	proti	k7c3	proti
carovi	car	k1gMnSc3	car
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
I	i	k8xC	i
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
byl	být	k5eAaImAgMnS	být
Dostojevskij	Dostojevskij	k1gFnSc4	Dostojevskij
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
členy	člen	k1gInPc7	člen
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
vojenským	vojenský	k2eAgInSc7d1	vojenský
soudem	soud	k1gInSc7	soud
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
jim	on	k3xPp3gInPc3	on
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
změněn	změnit	k5eAaPmNgInS	změnit
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
v	v	k7c6	v
káznici	káznice	k1gFnSc6	káznice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
strávil	strávit	k5eAaPmAgMnS	strávit
Dostojevskij	Dostojevskij	k1gFnSc4	Dostojevskij
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
první	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
na	na	k7c6	na
nucených	nucený	k2eAgFnPc6d1	nucená
pracích	práce	k1gFnPc6	práce
a	a	k8xC	a
poté	poté	k6eAd1	poté
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
psychicky	psychicky	k6eAd1	psychicky
a	a	k8xC	a
fyzicky	fyzicky	k6eAd1	fyzicky
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
trpět	trpět	k5eAaImF	trpět
epilepsií	epilepsie	k1gFnSc7	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Stráž	stráž	k1gFnSc1	stráž
však	však	k9	však
považovala	považovat	k5eAaImAgFnS	považovat
jeho	jeho	k3xOp3gFnSc1	jeho
záchvaty	záchvat	k1gInPc4	záchvat
za	za	k7c4	za
předstírání	předstírání	k1gNnSc4	předstírání
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
prošel	projít	k5eAaPmAgMnS	projít
peklem	peklo	k1gNnSc7	peklo
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
se	se	k3xPyFc4	se
vcítit	vcítit	k5eAaPmF	vcítit
do	do	k7c2	do
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
narušenou	narušený	k2eAgFnSc7d1	narušená
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Dostojevskij	Dostojevskij	k1gFnPc4	Dostojevskij
dva	dva	k4xCgInPc4	dva
delší	dlouhý	k2eAgInPc4d2	delší
pobyty	pobyt	k1gInPc4	pobyt
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
morální	morální	k2eAgFnSc4d1	morální
úroveň	úroveň	k1gFnSc4	úroveň
zdejší	zdejší	k2eAgFnSc2d1	zdejší
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
.	.	kIx.	.
</s>
<s>
Vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaImF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
přehnané	přehnaný	k2eAgFnSc3d1	přehnaná
hráčské	hráčský	k2eAgFnSc3d1	hráčská
vášni	vášeň	k1gFnSc3	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Neuznával	uznávat	k5eNaImAgInS	uznávat
racionalismus	racionalismus	k1gInSc1	racionalismus
<g/>
,	,	kIx,	,
bezohledné	bezohledný	k2eAgNnSc1d1	bezohledné
jednání	jednání	k1gNnSc1	jednání
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc3	jejich
sobectví	sobectví	k1gNnSc3	sobectví
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc1d1	velké
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c4	za
proměnu	proměna	k1gFnSc4	proměna
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc4d1	zásadní
potřebu	potřeba	k1gFnSc4	potřeba
změny	změna	k1gFnSc2	změna
viděl	vidět	k5eAaImAgMnS	vidět
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
proměně	proměna	k1gFnSc6	proměna
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
návratu	návrat	k1gInSc6	návrat
k	k	k7c3	k
pokoře	pokora	k1gFnSc3	pokora
<g/>
,	,	kIx,	,
v	v	k7c6	v
respektování	respektování	k1gNnSc6	respektování
ruských	ruský	k2eAgFnPc2d1	ruská
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
názory	názor	k1gInPc7	názor
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
carovu	carův	k2eAgFnSc4d1	carova
přízeň	přízeň	k1gFnSc4	přízeň
a	a	k8xC	a
úctu	úcta	k1gFnSc4	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
carská	carský	k2eAgFnSc1d1	carská
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sebevraždám	sebevražda	k1gFnPc3	sebevražda
a	a	k8xC	a
k	k	k7c3	k
despotismu	despotismus	k1gInSc3	despotismus
docházelo	docházet	k5eAaImAgNnS	docházet
podle	podle	k7c2	podle
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
zejména	zejména	k9	zejména
vinou	vinou	k7c2	vinou
ztráty	ztráta	k1gFnSc2	ztráta
náboženské	náboženský	k2eAgFnSc2d1	náboženská
opory	opora	k1gFnSc2	opora
a	a	k8xC	a
přehnaného	přehnaný	k2eAgInSc2d1	přehnaný
individualismu	individualismus	k1gInSc2	individualismus
<g/>
.	.	kIx.	.
</s>
<s>
Přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
změnit	změnit	k5eAaPmF	změnit
společnost	společnost	k1gFnSc4	společnost
podle	podle	k7c2	podle
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
ideálů	ideál	k1gInPc2	ideál
<g/>
,	,	kIx,	,
radikálního	radikální	k2eAgInSc2d1	radikální
převratu	převrat	k1gInSc2	převrat
se	se	k3xPyFc4	se
ale	ale	k9	ale
bál	bát	k5eAaImAgMnS	bát
<g/>
.	.	kIx.	.
</s>
<s>
Dostojevskij	Dostojevskít	k5eAaPmRp2nS	Dostojevskít
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Michail	Michail	k1gMnSc1	Michail
vydávali	vydávat	k5eAaPmAgMnP	vydávat
v	v	k7c6	v
letech	let	k1gInPc6	let
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
časopis	časopis	k1gInSc1	časopis
Čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
Vremja	Vremj	k2eAgNnPc4d1	Vremj
<g/>
)	)	kIx)	)
a	a	k8xC	a
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
časopis	časopis	k1gInSc1	časopis
Epocha	epocha	k1gFnSc1	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Michail	Michail	k1gMnSc1	Michail
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
splácel	splácet	k5eAaImAgMnS	splácet
jeho	jeho	k3xOp3gInPc4	jeho
dluhy	dluh	k1gInPc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
existenčních	existenční	k2eAgInPc2d1	existenční
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
psychologicky	psychologicky	k6eAd1	psychologicky
rozebíral	rozebírat	k5eAaImAgMnS	rozebírat
zločince	zločinec	k1gMnPc4	zločinec
<g/>
,	,	kIx,	,
revolucionáře	revolucionář	k1gMnPc4	revolucionář
<g/>
,	,	kIx,	,
prostitutky	prostitutka	k1gFnPc4	prostitutka
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
duševními	duševní	k2eAgFnPc7d1	duševní
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
byl	být	k5eAaImAgMnS	být
rozporuplnou	rozporuplný	k2eAgFnSc7d1	rozporuplná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Odmítal	odmítat	k5eAaImAgMnS	odmítat
se	se	k3xPyFc4	se
sžít	sžít	k5eAaPmF	sžít
se	s	k7c7	s
soudobým	soudobý	k2eAgNnSc7d1	soudobé
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Usiloval	usilovat	k5eAaImAgInS	usilovat
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
bezmyšlenkovitě	bezmyšlenkovitě	k6eAd1	bezmyšlenkovitě
zříkal	zříkat	k5eAaImAgMnS	zříkat
i	i	k9	i
vymožeností	vymoženost	k1gFnPc2	vymoženost
vyspělé	vyspělý	k2eAgFnSc2d1	vyspělá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lidi	člověk	k1gMnPc4	člověk
ho	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
psychologa	psycholog	k1gMnSc4	psycholog
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
on	on	k3xPp3gMnSc1	on
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zakladatelem	zakladatel	k1gMnSc7	zakladatel
psychologické	psychologický	k2eAgFnSc2d1	psychologická
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
filozofii	filozofie	k1gFnSc4	filozofie
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dostojevskij	Dostojevskít	k5eAaPmRp2nS	Dostojevskít
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
na	na	k7c4	na
plicní	plicní	k2eAgNnSc4d1	plicní
krvácení	krvácení	k1gNnSc4	krvácení
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
rozedmou	rozedma	k1gFnSc7	rozedma
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
epileptickým	epileptický	k2eAgInSc7d1	epileptický
záchvatem	záchvat	k1gInSc7	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Chudí	chudý	k2eAgMnPc1d1	chudý
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
prvotinou	prvotina	k1gFnSc7	prvotina
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
gogolovského	gogolovský	k2eAgInSc2d1	gogolovský
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
už	už	k6eAd1	už
zde	zde	k6eAd1	zde
soustřeďoval	soustřeďovat	k5eAaImAgMnS	soustřeďovat
především	především	k6eAd1	především
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyložit	vyložit	k5eAaPmF	vyložit
jejich	jejich	k3xOp3gInSc4	jejich
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
proces	proces	k1gInSc1	proces
deformace	deformace	k1gFnSc2	deformace
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
lidé	člověk	k1gMnPc1	člověk
podléhají	podléhat	k5eAaImIp3nP	podléhat
v	v	k7c6	v
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
krutým	krutý	k2eAgNnSc7d1	kruté
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
noci	noc	k1gFnPc1	noc
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
–	–	k?	–
lyrická	lyrický	k2eAgFnSc1d1	lyrická
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
vymyká	vymykat	k5eAaImIp3nS	vymykat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
další	další	k2eAgFnSc3d1	další
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dívce	dívka	k1gFnSc6	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
každý	každý	k3xTgInSc4	každý
večer	večer	k1gInSc1	večer
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
milého	milý	k1gMnSc4	milý
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Nesmělý	smělý	k2eNgMnSc1d1	nesmělý
mladík	mladík	k1gMnSc1	mladík
jí	on	k3xPp3gFnSc2	on
začne	začít	k5eAaPmIp3nS	začít
dělat	dělat	k5eAaImF	dělat
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dívka	dívka	k1gFnSc1	dívka
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zůstali	zůstat	k5eAaPmAgMnP	zůstat
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jí	on	k3xPp3gFnSc3	on
vyzná	vyznat	k5eAaPmIp3nS	vyznat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
dívka	dívka	k1gFnSc1	dívka
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přijmout	přijmout	k5eAaPmF	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
její	její	k3xOp3gInSc1	její
milý	milý	k2eAgInSc1d1	milý
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
jde	jít	k5eAaImIp3nS	jít
bez	bez	k7c2	bez
váhání	váhání	k1gNnPc2	váhání
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Dvojník	dvojník	k1gMnSc1	dvojník
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
–	–	k?	–
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
z	z	k7c2	z
hlubinných	hlubinný	k2eAgFnPc2d1	hlubinná
analýz	analýza	k1gFnPc2	analýza
rozdvojené	rozdvojený	k2eAgFnSc2d1	rozdvojená
psychiky	psychika	k1gFnSc2	psychika
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nětočka	Nětočka	k1gFnSc1	Nětočka
Nězvanovová	Nězvanovová	k1gFnSc1	Nězvanovová
/	/	kIx~	/
Strýčkův	strýčkův	k2eAgInSc1d1	strýčkův
sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
–	–	k?	–
Dostojevskij	Dostojevskij	k1gFnSc1	Dostojevskij
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
literatuře	literatura	k1gFnSc6	literatura
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
osud	osud	k1gInSc4	osud
bezprávného	bezprávný	k2eAgNnSc2d1	bezprávné
"	"	kIx"	"
<g/>
mladého	mladý	k2eAgMnSc2d1	mladý
<g/>
"	"	kIx"	"
člověka	člověk	k1gMnSc2	člověk
jako	jako	k8xC	jako
sociální	sociální	k2eAgFnSc3d1	sociální
tragédii	tragédie	k1gFnSc3	tragédie
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
lidskost	lidskost	k1gFnSc1	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tragiku	tragika	k1gFnSc4	tragika
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
reagoval	reagovat	k5eAaBmAgMnS	reagovat
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
černým	černý	k2eAgInSc7d1	černý
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
jeho	jeho	k3xOp3gNnPc2	jeho
prací	práce	k1gFnPc2	práce
tohoto	tento	k3xDgNnSc2	tento
gogolovského	gogolovský	k2eAgNnSc2d1	gogolovský
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
satirické	satirický	k2eAgFnPc1d1	satirická
novely	novela	k1gFnPc1	novela
<g/>
:	:	kIx,	:
Nětočka	Nětočka	k1gFnSc1	Nětočka
Nězvanovová	Nězvanovová	k1gFnSc1	Nězvanovová
/	/	kIx~	/
Strýčkův	strýčkův	k2eAgInSc1d1	strýčkův
sen	sen	k1gInSc1	sen
a	a	k8xC	a
Ves	ves	k1gFnSc1	ves
Štěpančikovo	Štěpančikův	k2eAgNnSc1d1	Štěpančikovo
–	–	k?	–
charakterová	charakterový	k2eAgFnSc1d1	charakterová
studie	studie	k1gFnSc1	studie
ruského	ruský	k2eAgMnSc2d1	ruský
Tartuffa	Tartuffe	k1gMnSc2	Tartuffe
<g/>
.	.	kIx.	.
</s>
<s>
Uražení	uražení	k1gNnSc1	uražení
a	a	k8xC	a
ponížení	ponížení	k1gNnSc1	ponížení
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
se	s	k7c7	s
sentimentální	sentimentální	k2eAgFnSc7d1	sentimentální
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Dostojevskij	Dostojevskij	k1gFnSc1	Dostojevskij
opět	opět	k6eAd1	opět
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
nutnost	nutnost	k1gFnSc4	nutnost
zastat	zastat	k5eAaPmF	zastat
se	se	k3xPyFc4	se
chudých	chudý	k2eAgFnPc2d1	chudá
a	a	k8xC	a
bezmocných	bezmocný	k2eAgFnPc2d1	bezmocná
proti	proti	k7c3	proti
krutosti	krutost	k1gFnSc3	krutost
mocných	mocní	k1gMnPc2	mocní
<g/>
.	.	kIx.	.
</s>
<s>
Zápisky	zápiska	k1gFnPc1	zápiska
z	z	k7c2	z
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
–	–	k?	–
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
omské	omský	k2eAgFnSc6d1	Omská
káznici	káznice	k1gFnSc6	káznice
a	a	k8xC	a
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc4	jenž
tam	tam	k6eAd1	tam
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
stane	stanout	k5eAaPmIp3nS	stanout
zločincem	zločinec	k1gMnSc7	zločinec
a	a	k8xC	a
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
vůbec	vůbec	k9	vůbec
následovat	následovat	k5eAaImF	následovat
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
motivů	motiv	k1gInPc2	motiv
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgMnS	složit
operu	opera	k1gFnSc4	opera
Z	z	k7c2	z
Mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
domu	dům	k1gInSc2	dům
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Zápisky	zápisek	k1gInPc1	zápisek
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
–	–	k?	–
novela	novela	k1gFnSc1	novela
předjímající	předjímající	k2eAgFnSc1d1	předjímající
existencialismus	existencialismus	k1gInSc1	existencialismus
<g/>
,	,	kIx,	,
drásavý	drásavý	k2eAgInSc1d1	drásavý
portrét	portrét	k1gInSc1	portrét
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
je	být	k5eAaImIp3nS	být
nenechat	nechat	k5eNaPmF	nechat
sebou	se	k3xPyFc7	se
nikdy	nikdy	k6eAd1	nikdy
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ospravedlňováno	ospravedlňován	k2eAgNnSc1d1	ospravedlňováno
"	"	kIx"	"
<g/>
prospěchem	prospěch	k1gInSc7	prospěch
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
pokrokem	pokrok	k1gInSc7	pokrok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
trest	trest	k1gInSc1	trest
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
–	–	k?	–
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
chudý	chudý	k2eAgMnSc1d1	chudý
petrohradský	petrohradský	k2eAgMnSc1d1	petrohradský
student	student	k1gMnSc1	student
Rodion	Rodion	k1gInSc4	Rodion
Romanovič	Romanovič	k1gInSc1	Romanovič
Raskolnikov	Raskolnikov	k1gInSc1	Raskolnikov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
být	být	k5eAaImF	být
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
některých	některý	k3yIgMnPc2	některý
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
bezcenný	bezcenný	k2eAgInSc4d1	bezcenný
a	a	k8xC	a
že	že	k8xS	že
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
hříchem	hřích	k1gInSc7	hřích
jim	on	k3xPp3gMnPc3	on
ho	on	k3xPp3gNnSc4	on
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
k	k	k7c3	k
lichvářce	lichvářka	k1gFnSc3	lichvářka
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
za	za	k7c7	za
lichvářkou	lichvářka	k1gFnSc7	lichvářka
přijde	přijít	k5eAaPmIp3nS	přijít
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vše	všechen	k3xTgNnSc4	všechen
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
musí	muset	k5eAaImIp3nS	muset
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
ji	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
sekerou	sekera	k1gFnSc7	sekera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
činu	čin	k1gInSc6	čin
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
,	,	kIx,	,
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
alkoholika	alkoholik	k1gMnSc2	alkoholik
Marmeladova	Marmeladův	k2eAgMnSc2d1	Marmeladův
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Soňa	Soňa	k1gFnSc1	Soňa
<g/>
.	.	kIx.	.
</s>
<s>
Raskolnikov	Raskolnikov	k1gInSc1	Raskolnikov
se	se	k3xPyFc4	se
do	do	k7c2	do
Soni	Soňa	k1gFnSc2	Soňa
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
a	a	k8xC	a
svěří	svěřit	k5eAaPmIp3nS	svěřit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Soňa	Soňa	k1gFnSc1	Soňa
ho	on	k3xPp3gMnSc4	on
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
šel	jít	k5eAaImAgMnS	jít
přiznat	přiznat	k5eAaPmF	přiznat
<g/>
,	,	kIx,	,
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
je	být	k5eAaImIp3nS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
8	[number]	k4	8
let	léto	k1gNnPc2	léto
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
Soňa	Soňa	k1gFnSc1	Soňa
oporou	opora	k1gFnSc7	opora
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgMnSc6	který
použil	použít	k5eAaPmAgMnS	použít
autor	autor	k1gMnSc1	autor
své	svůj	k3xOyFgFnSc2	svůj
zkušenosti	zkušenost	k1gFnSc2	zkušenost
s	s	k7c7	s
hráčskou	hráčský	k2eAgFnSc7d1	hráčská
vášní	vášeň	k1gFnSc7	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Idiot	idiot	k1gMnSc1	idiot
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
–	–	k?	–
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
kníže	kníže	k1gMnSc1	kníže
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Myškin	Myškin	k1gMnSc1	Myškin
<g/>
,	,	kIx,	,
nedoléčený	doléčený	k2eNgMnSc1d1	nedoléčený
epileptik	epileptik	k1gMnSc1	epileptik
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
léčení	léčení	k1gNnSc2	léčení
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
zprvu	zprvu	k6eAd1	zprvu
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
nemoci	nemoc	k1gFnSc3	nemoc
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
idiota	idiot	k1gMnSc4	idiot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
dovede	dovést	k5eAaPmIp3nS	dovést
předsudky	předsudek	k1gInPc4	předsudek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
dokonce	dokonce	k9	dokonce
zcela	zcela	k6eAd1	zcela
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
lidskostí	lidskost	k1gFnSc7	lidskost
<g/>
,	,	kIx,	,
nesobeckou	sobecký	k2eNgFnSc7d1	nesobecká
ušlechtilostí	ušlechtilost	k1gFnSc7	ušlechtilost
a	a	k8xC	a
zdánlivě	zdánlivě	k6eAd1	zdánlivě
až	až	k9	až
naivní	naivní	k2eAgFnSc7d1	naivní
dobrotivostí	dobrotivost	k1gFnSc7	dobrotivost
dokonce	dokonce	k9	dokonce
nad	nad	k7c7	nad
ostatní	ostatní	k2eAgFnSc7d1	ostatní
ruskou	ruský	k2eAgFnSc7d1	ruská
společností	společnost	k1gFnSc7	společnost
výrazně	výrazně	k6eAd1	výrazně
vyniká	vynikat	k5eAaImIp3nS	vynikat
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g/>
.	.	kIx.	.
</s>
<s>
Běsi	běs	k1gMnPc1	běs
(	(	kIx(	(
<g/>
též	též	k9	též
Běsové	běs	k1gMnPc1	běs
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
myšlenka	myšlenka	k1gFnSc1	myšlenka
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
vyjevena	vyjevit	k5eAaPmNgFnS	vyjevit
v	v	k7c6	v
soudobém	soudobý	k2eAgNnSc6d1	soudobé
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
hrdinovi	hrdina	k1gMnSc3	hrdina
Štěpánu	Štěpán	k1gMnSc3	Štěpán
Trofimovičovi	Trofimovič	k1gMnSc3	Trofimovič
přijde	přijít	k5eAaPmIp3nS	přijít
nemocné	nemocný	k2eAgMnPc4d1	nemocný
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc1d1	plná
neduhů	neduh	k1gInPc2	neduh
a	a	k8xC	a
morových	morový	k2eAgFnPc2d1	morová
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc4d1	plná
běsů	běs	k1gMnPc2	běs
a	a	k8xC	a
běsíků	běsík	k1gMnPc2	běsík
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
milém	milý	k2eAgNnSc6d1	milé
Rusku	Rusko	k1gNnSc6	Rusko
nashromáždilo	nashromáždit	k5eAaPmAgNnS	nashromáždit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
(	(	kIx(	(
<g/>
Režie	režie	k1gFnSc2	režie
<g/>
:	:	kIx,	:
Andrzej	Andrzej	k1gMnSc2	Andrzej
Wajda	Wajd	k1gMnSc4	Wajd
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Isabelle	Isabelle	k1gFnSc1	Isabelle
Huppert	Huppert	k1gInSc1	Huppert
<g/>
,	,	kIx,	,
Philippine	Philippin	k1gInSc5	Philippin
Leroy-Beaulieu	Leroy-Beaulieus	k1gInSc2	Leroy-Beaulieus
<g/>
)	)	kIx)	)
Deník	deník	k1gInSc1	deník
spisovatele	spisovatel	k1gMnSc2	spisovatel
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
–	–	k?	–
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
komentář	komentář	k1gInSc4	komentář
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Výrostek	výrostek	k1gInSc1	výrostek
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
morální	morální	k2eAgInSc1d1	morální
rozpor	rozpor	k1gInSc1	rozpor
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
společenské	společenský	k2eAgFnSc2d1	společenská
skutečnosti	skutečnost	k1gFnSc2	skutečnost
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc1	peníz
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
,	,	kIx,	,
touží	toužit	k5eAaImIp3nP	toužit
vlastnit	vlastnit	k5eAaImF	vlastnit
milion	milion	k4xCgInSc4	milion
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
netroufl	troufnout	k5eNaPmAgMnS	troufnout
urážet	urážet	k5eAaImF	urážet
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
vydáno	vydat	k5eAaPmNgNnS	vydat
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
psychologický	psychologický	k2eAgInSc4d1	psychologický
a	a	k8xC	a
filosofický	filosofický	k2eAgInSc4d1	filosofický
román	román	k1gInSc4	román
s	s	k7c7	s
kriminální	kriminální	k2eAgFnSc7d1	kriminální
zápletkou	zápletka	k1gFnSc7	zápletka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Fjodor	Fjodor	k1gMnSc1	Fjodor
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Karamazov	Karamazov	k1gInSc4	Karamazov
je	být	k5eAaImIp3nS	být
zhýralec	zhýralec	k1gMnSc1	zhýralec
a	a	k8xC	a
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
nad	nad	k7c7	nad
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
(	(	kIx(	(
<g/>
Dmitrij	Dmitrij	k1gMnSc7	Dmitrij
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
,	,	kIx,	,
Aljoša	Aljoša	k1gMnSc1	Aljoša
<g/>
,	,	kIx,	,
nemanželský	manželský	k2eNgInSc1d1	nemanželský
Smerďakov	Smerďakov	k1gInSc1	Smerďakov
<g/>
)	)	kIx)	)
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zachce	zachtít	k5eAaPmIp3nS	zachtít
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
syn	syn	k1gMnSc1	syn
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
je	být	k5eAaImIp3nS	být
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
je	být	k5eAaImIp3nS	být
nadaný	nadaný	k2eAgMnSc1d1	nadaný
<g/>
,	,	kIx,	,
racionálně	racionálně	k6eAd1	racionálně
uvažující	uvažující	k2eAgMnSc1d1	uvažující
ateista	ateista	k1gMnSc1	ateista
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
Aljoša	Aljoša	k1gMnSc1	Aljoša
žije	žít	k5eAaImIp3nS	žít
zasvěceným	zasvěcený	k2eAgInSc7d1	zasvěcený
životem	život	k1gInSc7	život
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
Smerďakov	Smerďakov	k1gInSc4	Smerďakov
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
starý	starý	k2eAgInSc1d1	starý
Karamazov	Karamazov	k1gInSc1	Karamazov
nepřijal	přijmout	k5eNaPmAgInS	přijmout
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
otce	otec	k1gMnSc4	otec
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
obviněn	obviněn	k2eAgMnSc1d1	obviněn
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
<g/>
.	.	kIx.	.
</s>
<s>
Smerďakov	Smerďakov	k1gInSc1	Smerďakov
pak	pak	k6eAd1	pak
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
je	být	k5eAaImIp3nS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
důležitých	důležitý	k2eAgFnPc2d1	důležitá
dějových	dějový	k2eAgFnPc2d1	dějová
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lásky	láska	k1gFnSc2	láska
Dmitrije	Dmitrije	k1gFnSc2	Dmitrije
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
mezi	mezi	k7c7	mezi
Kateřinou	Kateřina	k1gFnSc7	Kateřina
a	a	k8xC	a
Agrafenou	Agrafena	k1gFnSc7	Agrafena
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
má	mít	k5eAaImIp3nS	mít
zálusk	zálusk	k1gInSc1	zálusk
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Fjodor	Fjodor	k1gMnSc1	Fjodor
<g/>
.	.	kIx.	.
</s>
<s>
Věčný	věčný	k2eAgMnSc1d1	věčný
manžel	manžel	k1gMnSc1	manžel
–	–	k?	–
krátký	krátký	k2eAgInSc4d1	krátký
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
Balzaca	Balzac	k1gMnSc2	Balzac
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
překlady	překlad	k1gInPc4	překlad
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
<g/>
.	.	kIx.	.
</s>
