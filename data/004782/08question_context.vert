<s>
Fjodor	Fjodor	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Dostojevskij	Dostojevskij	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Ф	Ф	k?
М	М	k?
Д	Д	k?
<g/>
;	;	kIx,
30	[number]	k4
<g/>
.	.	kIx.
říjnajul	říjnajout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
11	[number]	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1821	[number]	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
Moskva	Moskva	k1gFnSc1
–	–	k?
28	[number]	k4
<g/>
.	.	kIx.
lednajul	lednajout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
9	[number]	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1881	[number]	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
světových	světový	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
,	,	kIx,
vrcholný	vrcholný	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
ruského	ruský	k2eAgInSc2d1
realismu	realismus	k1gInSc2
a	a	k8xC
současně	současně	k6eAd1
předchůdce	předchůdce	k1gMnSc1
moderní	moderní	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
prózy	próza	k1gFnSc2
<g/>
.	.	kIx.
</s>