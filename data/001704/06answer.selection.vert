<s>
Giosuè	Giosuè	k?	Giosuè
Carducci	Carducce	k1gMnPc1	Carducce
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
Valdicastello	Valdicastello	k1gNnSc1	Valdicastello
<g/>
,	,	kIx,	,
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1860	[number]	k4	1860
až	až	k9	až
1904	[number]	k4	1904
profesor	profesor	k1gMnSc1	profesor
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
