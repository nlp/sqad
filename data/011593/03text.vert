<p>
<s>
Babička	babička	k1gFnSc1	babička
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Obrazy	obraz	k1gInPc4	obraz
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgNnSc1d3	nejznámější
dílo	dílo	k1gNnSc1	dílo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Uniká	unikat	k5eAaImIp3nS	unikat
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
žánrovým	žánrový	k2eAgInSc7d1	žánrový
<g/>
,	,	kIx,	,
směrovým	směrový	k2eAgFnPc3d1	směrová
a	a	k8xC	a
tematickým	tematický	k2eAgFnPc3d1	tematická
interpretacím	interpretace	k1gFnPc3	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Žánrově	žánrově	k6eAd1	žánrově
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
idylu	idyla	k1gFnSc4	idyla
<g/>
,	,	kIx,	,
novelu	novela	k1gFnSc4	novela
<g/>
,,	,,	k?	,,
povídkový	povídkový	k2eAgInSc1d1	povídkový
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
črt	črta	k1gFnPc2	črta
nebo	nebo	k8xC	nebo
za	za	k7c4	za
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
literární	literární	k2eAgMnPc1d1	literární
historikové	historik	k1gMnPc1	historik
ji	on	k3xPp3gFnSc4	on
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
k	k	k7c3	k
romantismu	romantismus	k1gInSc3	romantismus
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
k	k	k7c3	k
biedermeieru	biedermeier	k1gInSc2	biedermeier
nebo	nebo	k8xC	nebo
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
idealizovaného	idealizovaný	k2eAgNnSc2d1	idealizované
vzpomínání	vzpomínání	k1gNnSc2	vzpomínání
na	na	k7c6	na
dětství	dětství	k1gNnSc6	dětství
v	v	k7c6	v
Ratibořickém	ratibořický	k2eAgNnSc6d1	Ratibořické
údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
lidové	lidový	k2eAgInPc4d1	lidový
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
vesnický	vesnický	k2eAgInSc4d1	vesnický
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
obyčejné	obyčejný	k2eAgFnPc1d1	obyčejná
i	i	k8xC	i
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
epizody	epizoda	k1gFnPc4	epizoda
spojuje	spojovat	k5eAaImIp3nS	spojovat
postava	postava	k1gFnSc1	postava
babičky	babička	k1gFnSc2	babička
<g/>
,	,	kIx,	,
laskavé	laskavý	k2eAgFnPc1d1	laskavá
<g/>
,	,	kIx,	,
moudré	moudrý	k2eAgFnPc1d1	moudrá
a	a	k8xC	a
zbožné	zbožný	k2eAgFnPc1d1	zbožná
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
váží	vážit	k5eAaImIp3nP	vážit
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nářečních	nářeční	k2eAgInPc2d1	nářeční
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
úsloví	úsloví	k1gNnPc2	úsloví
a	a	k8xC	a
zastaralých	zastaralý	k2eAgNnPc2d1	zastaralé
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
rysů	rys	k1gInPc2	rys
mluveného	mluvený	k2eAgInSc2d1	mluvený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
oživuje	oživovat	k5eAaImIp3nS	oživovat
a	a	k8xC	a
dělá	dělat	k5eAaImIp3nS	dělat
poměrně	poměrně	k6eAd1	poměrně
srozumitelným	srozumitelný	k2eAgInSc7d1	srozumitelný
i	i	k9	i
pro	pro	k7c4	pro
moderního	moderní	k2eAgMnSc4d1	moderní
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
.	.	kIx.	.
<g/>
Dílem	dílem	k6eAd1	dílem
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
autorka	autorka	k1gFnSc1	autorka
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
Babičky	babička	k1gFnSc2	babička
ideově	ideově	k6eAd1	ideově
nejsilněji	silně	k6eAd3	silně
čerpala	čerpat	k5eAaImAgFnS	čerpat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Erbenova	Erbenův	k2eAgFnSc1d1	Erbenova
Kytice	kytice	k1gFnSc1	kytice
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
dílem	díl	k1gInSc7	díl
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
univerzitního	univerzitní	k2eAgMnSc2d1	univerzitní
profesora	profesor	k1gMnSc2	profesor
a	a	k8xC	a
lékaře	lékař	k1gMnSc2	lékař
Františka	František	k1gMnSc2	František
Jana	Jan	k1gMnSc2	Jan
Mošnera	Mošner	k1gMnSc2	Mošner
Pěstounka	pěstounka	k1gFnSc1	pěstounka
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
a	a	k8xC	a
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
též	též	k9	též
o	o	k7c6	o
ovlivnění	ovlivnění	k1gNnSc6	ovlivnění
Adalbertem	Adalbert	k1gMnSc7	Adalbert
Stifterem	Stifter	k1gMnSc7	Stifter
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Němcová	Němcová	k1gFnSc1	Němcová
viděla	vidět	k5eAaImAgFnS	vidět
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
na	na	k7c6	na
mládí	mládí	k1gNnSc6	mládí
v	v	k7c6	v
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
<g/>
.	.	kIx.	.
</s>
<s>
Využila	využít	k5eAaPmAgFnS	využít
ovšem	ovšem	k9	ovšem
také	také	k9	také
své	svůj	k3xOyFgFnPc4	svůj
pozdější	pozdní	k2eAgFnPc4d2	pozdější
bohaté	bohatý	k2eAgFnPc4d1	bohatá
etnografické	etnografický	k2eAgFnPc4d1	etnografická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
do	do	k7c2	do
Babičky	babička	k1gFnSc2	babička
umístila	umístit	k5eAaPmAgFnS	umístit
popisy	popis	k1gInPc1	popis
zvyků	zvyk	k1gInPc2	zvyk
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
včetně	včetně	k7c2	včetně
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
<g/>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
Babičku	babička	k1gFnSc4	babička
psala	psát	k5eAaImAgFnS	psát
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
syna	syn	k1gMnSc2	syn
Hynka	Hynek	k1gMnSc2	Hynek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
potýkat	potýkat	k5eAaImF	potýkat
s	s	k7c7	s
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
chudobou	chudoba	k1gFnSc7	chudoba
a	a	k8xC	a
policejním	policejní	k2eAgNnSc7d1	policejní
sledováním	sledování	k1gNnSc7	sledování
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
ji	on	k3xPp3gFnSc4	on
psát	psát	k5eAaImF	psát
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Ječné	ječný	k2eAgFnSc6d1	Ječná
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
p.	p.	k?	p.
516	[number]	k4	516
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
a	a	k8xC	a
dokončila	dokončit	k5eAaPmAgFnS	dokončit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
ve	v	k7c6	v
Vyšehradské	vyšehradský	k2eAgFnSc6d1	Vyšehradská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
č.	č.	k?	č.
p.	p.	k?	p.
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
sešitech	sešit	k1gInPc6	sešit
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
českých	český	k2eAgFnPc2d1	Česká
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgFnPc4	tři
stovky	stovka	k1gFnPc1	stovka
českých	český	k2eAgMnPc2d1	český
vydání	vydání	k1gNnSc6	vydání
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc2	dvacet
cizích	cizí	k2eAgMnPc2d1	cizí
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručné	stručný	k2eAgNnSc4d1	stručné
shrnutí	shrnutí	k1gNnSc4	shrnutí
děje	děj	k1gInSc2	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vychovávat	vychovávat	k5eAaImF	vychovávat
vnoučata	vnouče	k1gNnPc1	vnouče
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
rádkyní	rádkyně	k1gFnSc7	rádkyně
sousedům	soused	k1gMnPc3	soused
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
paní	paní	k1gFnSc4	paní
kněžně	kněžna	k1gFnSc3	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
popisuje	popisovat	k5eAaImIp3nS	popisovat
i	i	k9	i
příběh	příběh	k1gInSc1	příběh
tragické	tragický	k2eAgFnSc2d1	tragická
lásky	láska	k1gFnSc2	láska
Viktorky	Viktorka	k1gFnSc2	Viktorka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejdramatičtějším	dramatický	k2eAgInPc3d3	nejdramatičtější
momentům	moment	k1gInPc3	moment
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Podstatné	podstatný	k2eAgNnSc1d1	podstatné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
má	mít	k5eAaImIp3nS	mít
líčení	líčení	k1gNnSc1	líčení
života	život	k1gInSc2	život
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
i	i	k9	i
jeho	jeho	k3xOp3gFnPc2	jeho
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristika	k1gFnSc1	charakteristika
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Babička	babička	k1gFnSc1	babička
===	===	k?	===
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Madlenka	Madlenka	k1gFnSc1	Madlenka
<g/>
)	)	kIx)	)
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
bělidlo	bělidlo	k1gNnSc4	bělidlo
<g/>
.	.	kIx.	.
</s>
<s>
Určuje	určovat	k5eAaImIp3nS	určovat
řád	řád	k1gInSc4	řád
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
a	a	k8xC	a
radí	radit	k5eAaImIp3nS	radit
a	a	k8xC	a
těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
přirozené	přirozený	k2eAgFnSc3d1	přirozená
autoritě	autorita	k1gFnSc3	autorita
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
osob	osoba	k1gFnPc2	osoba
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
od	od	k7c2	od
dětí	dítě	k1gFnPc2	dítě
až	až	k9	až
po	po	k7c4	po
paní	paní	k1gFnSc4	paní
kněžnu	kněžna	k1gFnSc4	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
se	se	k3xPyFc4	se
prochází	procházet	k5eAaImIp3nS	procházet
po	po	k7c6	po
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
a	a	k8xC	a
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
návštěvy	návštěva	k1gFnPc4	návštěva
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
v	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
myslivně	myslivna	k1gFnSc6	myslivna
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
staročeský	staročeský	k2eAgInSc4d1	staročeský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
zidealizované	zidealizovaný	k2eAgFnSc2d1	zidealizovaná
postavy	postava	k1gFnSc2	postava
Marie	Maria	k1gFnSc2	Maria
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Novotné-Čudové	Novotné-Čudová	k1gFnSc2	Novotné-Čudová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
dohadů	dohad	k1gInPc2	dohad
pobývala	pobývat	k5eAaImAgFnS	pobývat
v	v	k7c6	v
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1825	[number]	k4	1825
až	až	k9	až
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgMnPc2d1	jiný
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1820	[number]	k4	1820
a	a	k8xC	a
1830	[number]	k4	1830
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
zajížděla	zajíždět	k5eAaImAgFnS	zajíždět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barunka	Barunka	k1gFnSc1	Barunka
===	===	k?	===
</s>
</p>
<p>
<s>
Barunka	Barunka	k1gFnSc1	Barunka
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
babiččina	babiččin	k2eAgFnSc1d1	babiččina
vnučka	vnučka	k1gFnSc1	vnučka
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
částečně	částečně	k6eAd1	částečně
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
obraz	obraz	k1gInSc1	obraz
mladé	mladý	k2eAgFnSc2d1	mladá
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Barunka	Barunka	k1gFnSc1	Barunka
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
zpodobňovat	zpodobňovat	k5eAaImF	zpodobňovat
také	také	k9	také
mládí	mládí	k1gNnSc4	mládí
samo	sám	k3xTgNnSc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
a	a	k8xC	a
vzmáhající	vzmáhající	k2eAgMnSc1d1	vzmáhající
se	se	k3xPyFc4	se
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
děje	děj	k1gInSc2	děj
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
dětstvím	dětství	k1gNnSc7	dětství
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
dosud	dosud	k6eAd1	dosud
spadají	spadat	k5eAaImIp3nP	spadat
její	její	k3xOp3gMnPc1	její
mladší	mladý	k2eAgMnPc1d2	mladší
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
vážným	vážný	k2eAgInSc7d1	vážný
světem	svět	k1gInSc7	svět
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
===	===	k?	===
</s>
</p>
<p>
<s>
Paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
je	být	k5eAaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
a	a	k8xC	a
hodná	hodný	k2eAgFnSc1d1	hodná
šlechtična	šlechtična	k1gFnSc1	šlechtična
<g/>
,	,	kIx,	,
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
schovanku	schovanka	k1gFnSc4	schovanka
Hortensii	Hortensie	k1gFnSc4	Hortensie
<g/>
.	.	kIx.	.
</s>
<s>
Babičky	babička	k1gFnPc4	babička
si	se	k3xPyFc3	se
váží	vážit	k5eAaImIp3nS	vážit
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
jako	jako	k9	jako
k	k	k7c3	k
sobě	se	k3xPyFc3	se
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
má	mít	k5eAaImIp3nS	mít
pozici	pozice	k1gFnSc4	pozice
absolutního	absolutní	k2eAgMnSc2d1	absolutní
vládce	vládce	k1gMnSc2	vládce
<g/>
,	,	kIx,	,
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
babička	babička	k1gFnSc1	babička
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
jako	jako	k8xS	jako
Panna	Panna	k1gFnSc1	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
laskavá	laskavý	k2eAgFnSc1d1	laskavá
orodovnice	orodovnice	k1gFnSc1	orodovnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
vrchnosti	vrchnost	k1gFnSc2	vrchnost
přimluvit	přimluvit	k5eAaPmF	přimluvit
<g/>
.	.	kIx.	.
</s>
<s>
Předobrazem	předobraz	k1gInSc7	předobraz
pro	pro	k7c4	pro
paní	paní	k1gFnSc4	paní
kněžnu	kněžna	k1gFnSc4	kněžna
byla	být	k5eAaImAgFnS	být
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Kateřina	Kateřina	k1gFnSc1	Kateřina
Vilemína	Vilemína	k1gFnSc1	Vilemína
Zaháňská	zaháňský	k2eAgFnSc1d1	Zaháňská
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
spíše	spíše	k9	spíše
ženou	hnát	k5eAaImIp3nP	hnát
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
blízkou	blízký	k2eAgFnSc7d1	blízká
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
mnoha	mnoho	k4c2	mnoho
dobových	dobový	k2eAgFnPc2d1	dobová
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
kancléře	kancléř	k1gMnSc2	kancléř
Metternicha	Metternich	k1gMnSc2	Metternich
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
Esterházyho	Esterházy	k1gMnSc2	Esterházy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
romantizovanou	romantizovaný	k2eAgFnSc7d1	romantizovaná
a	a	k8xC	a
idealizovanou	idealizovaný	k2eAgFnSc7d1	idealizovaná
kněžnou	kněžna	k1gFnSc7	kněžna
starající	starající	k2eAgFnSc7d1	starající
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
poddané	poddaná	k1gFnSc6	poddaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Viktorka	Viktorka	k1gFnSc1	Viktorka
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
smutný	smutný	k2eAgInSc1d1	smutný
příběh	příběh	k1gInSc1	příběh
bláznivé	bláznivý	k2eAgFnSc2d1	bláznivá
Viktorky	Viktorka	k1gFnSc2	Viktorka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
vojáka	voják	k1gMnSc2	voják
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
černého	černý	k2eAgMnSc4d1	černý
myslivce	myslivec	k1gMnSc4	myslivec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
tragické	tragický	k2eAgFnSc2d1	tragická
lásky	láska	k1gFnSc2	láska
se	se	k3xPyFc4	se
pomátla	pomást	k5eAaPmAgFnS	pomást
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
postav	postava	k1gFnPc2	postava
téměř	téměř	k6eAd1	téměř
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
lese	les	k1gInSc6	les
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
živlům	živel	k1gInPc3	živel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spojena	spojit	k5eAaPmNgFnS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
pudové	pudový	k2eAgFnSc2d1	pudová
a	a	k8xC	a
smyslné	smyslný	k2eAgFnSc2d1	smyslná
(	(	kIx(	(
<g/>
ženské	ženský	k2eAgFnSc2d1	ženská
<g/>
)	)	kIx)	)
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
přemáhána	přemáhat	k5eAaImNgFnS	přemáhat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
ritualizovaně	ritualizovaně	k6eAd1	ritualizovaně
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Viktorka	Viktorka	k1gFnSc1	Viktorka
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
hrdinka	hrdinka	k1gFnSc1	hrdinka
příběhu	příběh	k1gInSc2	příběh
vyprávěného	vyprávěný	k2eAgInSc2d1	vyprávěný
myslivcem	myslivec	k1gMnSc7	myslivec
(	(	kIx(	(
<g/>
vložené	vložený	k2eAgFnSc2d1	vložená
povídky	povídka	k1gFnSc2	povídka
či	či	k8xC	či
novely	novela	k1gFnSc2	novela
<g/>
)	)	kIx)	)
a	a	k8xC	a
morální	morální	k2eAgInSc1d1	morální
a	a	k8xC	a
didaktický	didaktický	k2eAgInSc1d1	didaktický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
současně	současně	k6eAd1	současně
jako	jako	k9	jako
prvek	prvek	k1gInSc1	prvek
estetický	estetický	k2eAgInSc1d1	estetický
<g/>
,	,	kIx,	,
obohacující	obohacující	k2eAgInSc1d1	obohacující
text	text	k1gInSc1	text
o	o	k7c4	o
romantickou	romantický	k2eAgFnSc4d1	romantická
poetiku	poetika	k1gFnSc4	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Šmahelová	Šmahelová	k1gFnSc1	Šmahelová
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Viktorčina	Viktorčin	k2eAgInSc2d1	Viktorčin
příběhu	příběh	k1gInSc2	příběh
nejsilněji	silně	k6eAd3	silně
proniká	pronikat	k5eAaImIp3nS	pronikat
pohádkovost	pohádkovost	k1gFnSc1	pohádkovost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
jejího	její	k3xOp3gInSc2	její
dramatického	dramatický	k2eAgInSc2d1	dramatický
osudu	osud	k1gInSc2	osud
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
dějové	dějový	k2eAgNnSc1d1	dějové
schéma	schéma	k1gNnSc1	schéma
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
Vladimir	Vladimir	k1gMnSc1	Vladimir
Jakovlevič	Jakovlevič	k1gMnSc1	Jakovlevič
Propp	Propp	k1gMnSc1	Propp
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
náznacích	náznak	k1gInPc6	náznak
přichází	přicházet	k5eAaImIp3nS	přicházet
náhlé	náhlý	k2eAgNnSc4d1	náhlé
neštěstí	neštěstí	k1gNnSc4	neštěstí
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
záchranu	záchrana	k1gFnSc4	záchrana
nevinné	vinný	k2eNgFnPc4d1	nevinná
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgFnPc4d1	trpící
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
hledají	hledat	k5eAaImIp3nP	hledat
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
škůdci	škůdce	k1gMnSc3	škůdce
<g/>
,	,	kIx,	,
černému	černý	k1gMnSc3	černý
myslivci	myslivec	k1gMnSc3	myslivec
<g/>
,	,	kIx,	,
a	a	k8xC	a
slibují	slibovat	k5eAaImIp3nP	slibovat
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
postava	postava	k1gFnSc1	postava
pomocníka	pomocník	k1gMnSc2	pomocník
<g/>
,	,	kIx,	,
kovářky	kovářka	k1gFnSc2	kovářka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
daruje	darovat	k5eAaPmIp3nS	darovat
Viktorce	Viktorka	k1gFnSc3	Viktorka
magický	magický	k2eAgInSc4d1	magický
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
škapulíř	škapulíř	k1gInSc4	škapulíř
<g/>
.	.	kIx.	.
</s>
<s>
Ženich	ženich	k1gMnSc1	ženich
je	být	k5eAaImIp3nS	být
však	však	k9	však
jako	jako	k9	jako
zachránce	zachránce	k1gMnSc1	zachránce
nepoužitelný	použitelný	k2eNgMnSc1d1	nepoužitelný
a	a	k8xC	a
také	také	k9	také
zlo	zlo	k1gNnSc1	zlo
černého	černý	k2eAgMnSc2d1	černý
myslivce	myslivec	k1gMnSc2	myslivec
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
Viktorku	Viktorka	k1gFnSc4	Viktorka
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
Šmahelová	Šmahelová	k1gFnSc1	Šmahelová
si	se	k3xPyFc3	se
povšimla	povšimnout	k5eAaPmAgFnS	povšimnout
také	také	k6eAd1	také
propojení	propojení	k1gNnSc4	propojení
postavy	postava	k1gFnSc2	postava
Viktorky	Viktorka	k1gFnSc2	Viktorka
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
autorky	autorka	k1gFnSc2	autorka
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
scéně	scéna	k1gFnSc6	scéna
její	její	k3xOp3gFnSc2	její
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
když	když	k8xS	když
leží	ležet	k5eAaImIp3nS	ležet
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
v	v	k7c6	v
rakvi	rakev	k1gFnSc6	rakev
a	a	k8xC	a
na	na	k7c6	na
rtech	ret	k1gInPc6	ret
jí	on	k3xPp3gFnSc2	on
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
trpký	trpký	k2eAgInSc1d1	trpký
úsměšek	úsměšek	k1gInSc1	úsměšek
<g/>
.	.	kIx.	.
<g/>
Symbolický	symbolický	k2eAgInSc1d1	symbolický
hrob	hrob	k1gInSc1	hrob
bláznivé	bláznivý	k2eAgFnSc2d1	bláznivá
Viktorky	Viktorka	k1gFnSc2	Viktorka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
Kostelci	Kostelec	k1gInSc6	Kostelec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
žila	žít	k5eAaImAgFnS	žít
od	od	k7c2	od
září	září	k1gNnSc2	září
1837	[number]	k4	1837
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Předobrazem	předobraz	k1gInSc7	předobraz
postavy	postava	k1gFnSc2	postava
byla	být	k5eAaImAgFnS	být
reálně	reálně	k6eAd1	reálně
žijící	žijící	k2eAgFnSc1d1	žijící
Viktorie	Viktorie	k1gFnSc1	Viktorie
Židová	židová	k1gFnSc1	židová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nakonec	nakonec	k6eAd1	nakonec
zemřela	zemřít	k5eAaPmAgFnS	zemřít
sešlostí	sešlost	k1gFnSc7	sešlost
věkem	věk	k1gInSc7	věk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodišti	rodiště	k1gNnSc6	rodiště
<g/>
,	,	kIx,	,
nedaleké	daleký	k2eNgFnSc3d1	nedaleká
Červené	Červené	k2eAgFnSc3d1	Červené
Hoře	hora	k1gFnSc3	hora
(	(	kIx(	(
<g/>
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
kostelecké	kostelecký	k2eAgFnSc2d1	kostelecká
farnosti	farnost	k1gFnSc2	farnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
na	na	k7c6	na
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
kosteleckém	kostelecký	k2eAgInSc6d1	kostelecký
hřbitově	hřbitov	k1gInSc6	hřbitov
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
hrobu	hrob	k1gInSc2	hrob
chudých	chudý	k1gMnPc2	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zpodobnění	zpodobnění	k1gNnSc4	zpodobnění
Viktorky	Viktorka	k1gFnSc2	Viktorka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považuje	považovat	k5eAaImIp3nS	považovat
motiv	motiv	k1gInSc4	motiv
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
české	český	k2eAgNnSc1d1	české
500	[number]	k4	500
<g/>
korunové	korunový	k2eAgFnPc4d1	korunová
bankovky	bankovka	k1gFnPc4	bankovka
<g/>
;	;	kIx,	;
oficiálně	oficiálně	k6eAd1	oficiálně
jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
o	o	k7c4	o
obecnější	obecní	k2eAgMnPc4d2	obecní
"	"	kIx"	"
<g/>
symbol	symbol	k1gInSc1	symbol
ženských	ženský	k2eAgFnPc2d1	ženská
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úvodní	úvodní	k2eAgFnSc4d1	úvodní
pasáž	pasáž	k1gFnSc4	pasáž
===	===	k?	===
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
začíná	začínat	k5eAaImIp3nS	začínat
německým	německý	k2eAgNnSc7d1	německé
mottem	motto	k1gNnSc7	motto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vidíš	vidět	k5eAaImIp2nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
chudí	chudý	k2eAgMnPc1d1	chudý
nejsou	být	k5eNaImIp3nP	být
tak	tak	k9	tak
docela	docela	k6eAd1	docela
ubozí	ubohý	k2eAgMnPc1d1	ubohý
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
myslíme	myslet	k5eAaImIp1nP	myslet
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
opravdu	opravdu	k6eAd1	opravdu
blaženější	blažený	k2eAgFnPc1d2	blaženější
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
představujeme	představovat	k5eAaImIp1nP	představovat
a	a	k8xC	a
než	než	k8xS	než
my	my	k3xPp1nPc1	my
sami	sám	k3xTgMnPc1	sám
jsme	být	k5eAaImIp1nP	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Úryvek	úryvek	k1gInSc1	úryvek
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
románu	román	k1gInSc2	román
Rytíři	Rytíř	k1gMnPc7	Rytíř
ducha	duch	k1gMnSc2	duch
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Ritter	Ritter	k1gMnSc1	Ritter
vom	vom	k?	vom
Geiste	Geist	k1gMnSc5	Geist
<g/>
,	,	kIx,	,
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
mladoněmecký	mladoněmecký	k2eAgMnSc1d1	mladoněmecký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karl	Karl	k1gMnSc1	Karl
Gutzkow	Gutzkow	k1gMnSc1	Gutzkow
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
motta	motto	k1gNnSc2	motto
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
originálu	originál	k1gInSc3	originál
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
oslovení	oslovení	k1gNnSc1	oslovení
"	"	kIx"	"
<g/>
teuer	teurat	k5eAaPmRp2nS	teurat
Communist	Communist	k1gInSc1	Communist
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
drahý	drahý	k1gMnSc5	drahý
komunisto	komunista	k1gMnSc5	komunista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gutzkowově	Gutzkowov	k1gInSc6	Gutzkowov
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
pasáž	pasáž	k1gFnSc1	pasáž
součástí	součást	k1gFnPc2	součást
polemiky	polemika	k1gFnSc2	polemika
mířené	mířený	k2eAgFnPc1d1	mířená
na	na	k7c4	na
prosazovatele	prosazovatel	k1gMnPc4	prosazovatel
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
utopického	utopický	k2eAgInSc2d1	utopický
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
postavené	postavený	k2eAgFnPc4d1	postavená
kolem	kolem	k7c2	kolem
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
třídního	třídní	k2eAgInSc2d1	třídní
boje	boj	k1gInSc2	boj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
zrovnoprávněním	zrovnoprávnění	k1gNnSc7	zrovnoprávnění
neprivilegovaných	privilegovaný	k2eNgFnPc2d1	neprivilegovaná
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
Němcovou	Němcová	k1gFnSc7	Němcová
oslovil	oslovit	k5eAaPmAgInS	oslovit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
souzněl	souznít	k5eAaImAgMnS	souznít
s	s	k7c7	s
jejími	její	k3xOp3gFnPc7	její
vlastními	vlastní	k2eAgFnPc7d1	vlastní
představami	představa	k1gFnPc7	představa
o	o	k7c6	o
bezproblémovém	bezproblémový	k2eAgNnSc6d1	bezproblémové
soužití	soužití	k1gNnSc6	soužití
různých	různý	k2eAgFnPc2d1	různá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chudí	chudý	k1gMnPc1	chudý
bohatým	bohatý	k2eAgMnPc3d1	bohatý
nezávidí	závidět	k5eNaImIp3nS	závidět
a	a	k8xC	a
bohatí	bohatit	k5eAaImIp3nP	bohatit
chudými	chudý	k1gMnPc7	chudý
nepohrdají	pohrdat	k5eNaImIp3nP	pohrdat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
popsala	popsat	k5eAaPmAgFnS	popsat
i	i	k9	i
v	v	k7c6	v
Babičce	babička	k1gFnSc6	babička
<g/>
.	.	kIx.	.
<g/>
Následuje	následovat	k5eAaImIp3nS	následovat
věnování	věnování	k1gNnSc1	věnování
hraběnce	hraběnka	k1gFnSc3	hraběnka
Eleonoře	Eleonora	k1gFnSc3	Eleonora
z	z	k7c2	z
Kounic	Kounice	k1gFnPc2	Kounice
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc3d1	rozená
hraběnce	hraběnka	k1gFnSc3	hraběnka
Voračické	Voračická	k1gFnSc2	Voračická
z	z	k7c2	z
Paběnic	Paběnice	k1gFnPc2	Paběnice
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnSc3d1	významná
dobové	dobový	k2eAgFnSc3d1	dobová
filantropce	filantropka	k1gFnSc3	filantropka
a	a	k8xC	a
vlastenecké	vlastenecký	k2eAgFnSc3d1	vlastenecká
podporovatelce	podporovatelka	k1gFnSc3	podporovatelka
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
šlechtična	šlechtična	k1gFnSc1	šlechtična
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
překládala	překládat	k5eAaImAgFnS	překládat
Babičku	babička	k1gFnSc4	babička
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
výsledek	výsledek	k1gInSc1	výsledek
její	její	k3xOp3gFnSc2	její
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prologu	prolog	k1gInSc6	prolog
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
obtížnosti	obtížnost	k1gFnSc6	obtížnost
zachycení	zachycení	k1gNnSc2	zachycení
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
dávno	dávno	k6eAd1	dávno
zemřelou	zemřelý	k2eAgFnSc4d1	zemřelá
babičku	babička	k1gFnSc4	babička
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
psaného	psaný	k2eAgInSc2d1	psaný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
se	se	k3xPyFc4	se
představuje	představovat	k5eAaImIp3nS	představovat
jako	jako	k9	jako
účastnice	účastnice	k1gFnSc1	účastnice
kulturního	kulturní	k2eAgInSc2d1	kulturní
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
starobylé	starobylý	k2eAgFnSc2d1	starobylá
orální	orální	k2eAgFnSc2d1	orální
kultury	kultura	k1gFnSc2	kultura
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
kultuře	kultura	k1gFnSc3	kultura
psaného	psaný	k2eAgNnSc2d1	psané
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
chaloupce	chaloupka	k1gFnSc6	chaloupka
v	v	k7c6	v
pohorské	pohorský	k2eAgFnSc6d1	Pohorská
vesnici	vesnice	k1gFnSc6	vesnice
Olešnici	Olešnice	k1gFnSc4	Olešnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nejstarší	starý	k2eAgFnSc2d3	nejstarší
dcery	dcera	k1gFnSc2	dcera
Terezky	Terezka	k1gFnSc2	Terezka
přichází	přicházet	k5eAaImIp3nS	přicházet
babičce	babička	k1gFnSc3	babička
dopis	dopis	k1gInSc1	dopis
<g/>
;	;	kIx,	;
dcera	dcera	k1gFnSc1	dcera
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
a	a	k8xC	a
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
přijal	přijmout	k5eAaPmAgMnS	přijmout
službu	služba	k1gFnSc4	služba
u	u	k7c2	u
kněžny	kněžna	k1gFnSc2	kněžna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
panství	panství	k1gNnSc4	panství
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
babiččina	babiččin	k2eAgNnSc2d1	Babiččino
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
babičku	babička	k1gFnSc4	babička
prosí	prosit	k5eAaImIp3nS	prosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přišla	přijít	k5eAaPmAgFnS	přijít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Kterou	který	k3yIgFnSc4	který
Olešnici	Olešnice	k1gFnSc4	Olešnice
měla	mít	k5eAaImAgFnS	mít
Němcová	Němcová	k1gFnSc1	Němcová
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgNnSc1d2	starší
bádání	bádání	k1gNnSc1	bádání
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marie	Marie	k1gFnSc1	Marie
Magdalena	Magdalena	k1gFnSc1	Magdalena
Čudová-Novotná	Čudová-Novotný	k2eAgFnSc1d1	Čudová-Novotný
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
přistěhovala	přistěhovat	k5eAaPmAgFnS	přistěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
z	z	k7c2	z
Rovného	rovný	k2eAgMnSc2d1	rovný
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Kašparem	Kašpar	k1gMnSc7	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
její	její	k3xOp3gInSc4	její
příjezd	příjezd	k1gInSc4	příjezd
do	do	k7c2	do
Ratibořic	Ratibořice	k1gFnPc2	Ratibořice
datovat	datovat	k5eAaImF	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skončil	skončit	k5eAaPmAgInS	skončit
její	její	k3xOp3gInSc4	její
pobyt	pobyt	k1gInSc4	pobyt
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
možná	možná	k9	možná
také	také	k9	také
v	v	k7c6	v
Zaháni	Zaháň	k1gFnSc6	Zaháň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Babička	babička	k1gFnSc1	babička
dceřino	dceřin	k2eAgNnSc4d1	dceřino
pozvání	pozvání	k1gNnSc4	pozvání
přijme	přijmout	k5eAaPmIp3nS	přijmout
a	a	k8xC	a
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
bělidlo	bělidlo	k1gNnSc4	bělidlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
Proškovi	Proškův	k2eAgMnPc1d1	Proškův
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
Barunka	Barunka	k1gFnSc1	Barunka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Vilímek	Vilímek	k1gMnSc1	Vilímek
a	a	k8xC	a
Adélka	Adélka	k1gFnSc1	Adélka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
velkých	velká	k1gFnPc2	velká
psů	pes	k1gMnPc2	pes
(	(	kIx(	(
<g/>
Sultán	sultán	k1gMnSc1	sultán
a	a	k8xC	a
Tyrl	Tyrl	k1gMnSc1	Tyrl
<g/>
)	)	kIx)	)
ji	on	k3xPp3gFnSc4	on
netrpělivě	trpělivě	k6eNd1	trpělivě
očekávají	očekávat	k5eAaImIp3nP	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijede	přijet	k5eAaPmIp3nS	přijet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
jejím	její	k3xOp3gNnSc7	její
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
bílými	bílý	k2eAgInPc7d1	bílý
vlasy	vlas	k1gInPc7	vlas
<g/>
,	,	kIx,	,
scvrklýma	scvrklý	k2eAgFnPc7d1	scvrklá
rukama	ruka	k1gFnPc7	ruka
a	a	k8xC	a
pouhými	pouhý	k2eAgInPc7d1	pouhý
čtyřmi	čtyři	k4xCgInPc7	čtyři
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
si	se	k3xPyFc3	se
okamžitě	okamžitě	k6eAd1	okamžitě
vezme	vzít	k5eAaPmIp3nS	vzít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
pečením	pečení	k1gNnSc7	pečení
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
měli	mít	k5eAaImAgMnP	mít
úctu	úcta	k1gFnSc4	úcta
k	k	k7c3	k
božímu	boží	k2eAgInSc3d1	boží
daru	dar	k1gInSc3	dar
a	a	k8xC	a
žehnali	žehnat	k5eAaImAgMnP	žehnat
chléb	chléb	k1gInSc4	chléb
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
vezme	vzít	k5eAaPmIp3nS	vzít
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
těsto	těsto	k1gNnSc1	těsto
nebo	nebo	k8xC	nebo
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Barunce	Barunka	k1gFnSc3	Barunka
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobrá	dobrý	k2eAgFnSc1d1	dobrá
hospodyně	hospodyně	k1gFnSc1	hospodyně
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
šetrná	šetrný	k2eAgFnSc1d1	šetrná
a	a	k8xC	a
pro	pro	k7c4	pro
pírko	pírko	k1gNnSc4	pírko
i	i	k9	i
přes	přes	k7c4	přes
plot	plot	k1gInSc4	plot
skočit	skočit	k5eAaPmF	skočit
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
je	být	k5eAaImIp3nS	být
také	také	k9	také
překvapena	překvapit	k5eAaPmNgFnS	překvapit
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
novotami	novota	k1gFnPc7	novota
<g/>
,	,	kIx,	,
čalouněným	čalouněný	k2eAgInSc7d1	čalouněný
a	a	k8xC	a
lesklým	lesklý	k2eAgInSc7d1	lesklý
nábytkem	nábytek	k1gInSc7	nábytek
nebo	nebo	k8xC	nebo
klavírem	klavír	k1gInSc7	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sedničce	sednička	k1gFnSc6	sednička
<g/>
.	.	kIx.	.
</s>
<s>
Dětem	dítě	k1gFnPc3	dítě
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
své	svůj	k3xOyFgInPc4	svůj
poklady	poklad	k1gInPc4	poklad
<g/>
,	,	kIx,	,
dárky	dárek	k1gInPc4	dárek
z	z	k7c2	z
poutí	pouť	k1gFnPc2	pouť
<g/>
,	,	kIx,	,
rodinné	rodinný	k2eAgInPc4d1	rodinný
spisy	spis	k1gInPc4	spis
a	a	k8xC	a
drobnosti	drobnost	k1gFnPc4	drobnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tolaru	tolar	k1gInSc2	tolar
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Líčí	líčit	k5eAaImIp3nS	líčit
babiččin	babiččin	k2eAgInSc4d1	babiččin
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
<g/>
;	;	kIx,	;
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
vstává	vstávat	k5eAaImIp3nS	vstávat
ve	v	k7c4	v
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
řekne	říct	k5eAaPmIp3nS	říct
požehnání	požehnání	k1gNnSc4	požehnání
<g/>
,	,	kIx,	,
políbí	políbit	k5eAaPmIp3nP	políbit
křížek	křížek	k1gInSc4	křížek
a	a	k8xC	a
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
přede	příst	k5eAaImIp3nS	příst
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
popis	popis	k1gInSc1	popis
jejího	její	k3xOp3gInSc2	její
něžného	něžný	k2eAgInSc2d1	něžný
vztahu	vztah	k1gInSc2	vztah
ke	k	k7c3	k
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
:	:	kIx,	:
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
hovězí	hovězí	k2eAgInSc1d1	hovězí
dobytek	dobytek	k1gInSc1	dobytek
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc1	drůbež
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
ji	on	k3xPp3gFnSc4	on
mají	mít	k5eAaImIp3nP	mít
ráda	rád	k2eAgFnSc1d1	ráda
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
je	on	k3xPp3gInPc4	on
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
týrání	týrání	k1gNnSc3	týrání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
psi	pes	k1gMnPc1	pes
roztrhali	roztrhat	k5eAaPmAgMnP	roztrhat
kačátka	kačátka	k1gFnSc1	kačátka
<g/>
,	,	kIx,	,
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Prošek	Prošek	k1gMnSc1	Prošek
je	on	k3xPp3gNnSc4	on
seřeže	seřezat	k5eAaPmIp3nS	seřezat
<g/>
,	,	kIx,	,
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
babička	babička	k1gFnSc1	babička
obstará	obstarat	k5eAaPmIp3nS	obstarat
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
probudit	probudit	k5eAaPmF	probudit
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
poté	poté	k6eAd1	poté
buď	buď	k8xC	buď
dál	daleko	k6eAd2	daleko
přede	příst	k5eAaImIp3nS	příst
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c4	na
procházku	procházka	k1gFnSc4	procházka
a	a	k8xC	a
sbírá	sbírat	k5eAaImIp3nS	sbírat
léčivé	léčivý	k2eAgFnPc4d1	léčivá
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lese	les	k1gInSc6	les
potkávají	potkávat	k5eAaImIp3nP	potkávat
bledou	bledý	k2eAgFnSc4d1	bledá
<g/>
,	,	kIx,	,
rozcuchanou	rozcuchaný	k2eAgFnSc4d1	rozcuchaná
<g/>
,	,	kIx,	,
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
Viktorku	Viktorka	k1gFnSc4	Viktorka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
a	a	k8xC	a
večer	večer	k6eAd1	večer
zpívá	zpívat	k5eAaImIp3nS	zpívat
u	u	k7c2	u
splavu	splav	k1gInSc2	splav
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
tu	tu	k6eAd1	tu
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
,	,	kIx,	,
boží	božit	k5eAaImIp3nP	božit
vyvolení	vyvolený	k1gMnPc1	vyvolený
jasnou	jasný	k2eAgFnSc4d1	jasná
a	a	k8xC	a
krásnou	krásný	k2eAgFnSc4d1	krásná
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xS	jako
Viktorka	Viktorka	k1gFnSc1	Viktorka
zakalenou	zakalený	k2eAgFnSc7d1	zakalená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
domov	domov	k1gInSc4	domov
Proškových	Prošková	k1gFnPc2	Prošková
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
bělidlo	bělidlo	k1gNnSc1	bělidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
malé	malý	k2eAgNnSc1d1	malé
stavení	stavení	k1gNnSc1	stavení
uprostřed	uprostřed	k7c2	uprostřed
květinových	květinový	k2eAgFnPc2d1	květinová
a	a	k8xC	a
zeleninových	zeleninový	k2eAgFnPc2d1	zeleninová
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
je	být	k5eAaImIp3nS	být
také	také	k9	také
ovocný	ovocný	k2eAgInSc1d1	ovocný
sad	sad	k1gInSc1	sad
<g/>
,	,	kIx,	,
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
stavení	stavení	k1gNnPc1	stavení
a	a	k8xC	a
protékající	protékající	k2eAgFnSc1d1	protékající
řeka	řeka	k1gFnSc1	řeka
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
chodívá	chodívat	k5eAaImIp3nS	chodívat
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
koupat	koupat	k5eAaImF	koupat
ve	v	k7c6	v
strouze	strouha	k1gFnSc6	strouha
<g/>
,	,	kIx,	,
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
teče	téct	k5eAaImIp3nS	téct
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
pak	pak	k6eAd1	pak
až	až	k9	až
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
rýzmburském	rýzmburský	k2eAgMnSc6d1	rýzmburský
myslivci	myslivec	k1gMnSc6	myslivec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stavuje	stavovat	k5eAaImIp3nS	stavovat
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
Hektorem	Hektor	k1gMnSc7	Hektor
na	na	k7c4	na
skleničku	sklenička	k1gFnSc4	sklenička
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
chléb	chléb	k1gInSc1	chléb
se	se	k3xPyFc4	se
solí	solit	k5eAaImIp3nS	solit
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
panský	panský	k2eAgMnSc1d1	panský
hlídač	hlídač	k1gMnSc1	hlídač
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
bojí	bát	k5eAaImIp3nP	bát
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
panstvo	panstvo	k1gNnSc1	panstvo
<g/>
,	,	kIx,	,
projíždějící	projíždějící	k2eAgFnSc1d1	projíždějící
kolem	kolem	k6eAd1	kolem
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
však	však	k8xC	však
děti	dítě	k1gFnPc1	dítě
obdivují	obdivovat	k5eAaImIp3nP	obdivovat
a	a	k8xC	a
také	také	k9	také
babička	babička	k1gFnSc1	babička
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlechta	šlechta	k1gFnSc1	šlechta
je	být	k5eAaImIp3nS	být
lidem	lid	k1gInSc7	lid
k	k	k7c3	k
radosti	radost	k1gFnSc3	radost
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
přichází	přicházet	k5eAaImIp3nS	přicházet
babičku	babička	k1gFnSc4	babička
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
navštívit	navštívit	k5eAaPmF	navštívit
Kristla	Kristla	k1gFnSc1	Kristla
<g/>
,	,	kIx,	,
čiperná	čiperný	k2eAgFnSc1d1	čiperná
a	a	k8xC	a
usměvavá	usměvavý	k2eAgFnSc1d1	usměvavá
dcera	dcera	k1gFnSc1	dcera
hospodského	hospodský	k2eAgInSc2d1	hospodský
pojatá	pojatý	k2eAgFnSc1d1	pojatá
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
dcery	dcera	k1gFnSc2	dcera
hostinského	hostinský	k1gMnSc2	hostinský
Františka	František	k1gMnSc2	František
Celby	Celba	k1gMnSc2	Celba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc4	její
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
slavnou	slavný	k2eAgFnSc4d1	slavná
babičku	babička	k1gFnSc4	babička
zachytil	zachytit	k5eAaPmAgMnS	zachytit
také	také	k9	také
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
ve	v	k7c6	v
stati	stať	k1gFnSc6	stať
"	"	kIx"	"
<g/>
Paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
se	se	k3xPyFc4	se
i	i	k9	i
mlynář	mlynář	k1gMnSc1	mlynář
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
paní	paní	k1gFnSc1	paní
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
paní	paní	k1gFnSc1	paní
správcová	správcová	k1gFnSc1	správcová
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
babička	babička	k1gFnSc1	babička
nemá	mít	k5eNaImIp3nS	mít
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
pýchu	pýcha	k1gFnSc4	pýcha
ráda	rád	k2eAgFnSc1d1	ráda
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
odchází	odcházet	k5eAaImIp3nS	odcházet
i	i	k9	i
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nemuseli	muset	k5eNaImAgMnP	muset
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
však	však	k9	však
babička	babička	k1gFnSc1	babička
ráda	rád	k2eAgFnSc1d1	ráda
vítá	vítat	k5eAaImIp3nS	vítat
prosté	prostý	k2eAgMnPc4d1	prostý
a	a	k8xC	a
nuzné	nuzný	k2eAgMnPc4d1	nuzný
lidi	člověk	k1gMnPc4	člověk
<g/>
:	:	kIx,	:
olejkáře	olejkář	k1gMnSc4	olejkář
<g/>
,	,	kIx,	,
dráteníka	dráteník	k1gMnSc4	dráteník
nebo	nebo	k8xC	nebo
žida	žid	k1gMnSc4	žid
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
pana	pan	k1gMnSc2	pan
Beyera	Beyer	k1gMnSc2	Beyer
<g/>
,	,	kIx,	,
krkonošského	krkonošský	k2eAgMnSc2d1	krkonošský
myslivce	myslivec	k1gMnSc2	myslivec
z	z	k7c2	z
Horního	horní	k2eAgInSc2d1	horní
Maršova	Maršův	k2eAgInSc2d1	Maršův
<g/>
,	,	kIx,	,
vysokého	vysoký	k2eAgMnSc2d1	vysoký
<g/>
,	,	kIx,	,
svalnatého	svalnatý	k2eAgNnSc2d1	svalnaté
a	a	k8xC	a
hubeného	hubený	k2eAgNnSc2d1	hubené
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
vlasy	vlas	k1gInPc7	vlas
<g/>
,	,	kIx,	,
velkým	velký	k2eAgInSc7d1	velký
knírem	knír	k1gInSc7	knír
a	a	k8xC	a
ptačími	ptačí	k2eAgNnPc7d1	ptačí
péry	péro	k1gNnPc7	péro
za	za	k7c7	za
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
;	;	kIx,	;
dětem	dítě	k1gFnPc3	dítě
přináší	přinášet	k5eAaImIp3nS	přinášet
krystaly	krystal	k1gInPc1	krystal
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
jim	on	k3xPp3gMnPc3	on
o	o	k7c6	o
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
Rýbrcoulovi	Rýbrcoul	k1gMnSc6	Rýbrcoul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
4	[number]	k4	4
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
obzvláště	obzvláště	k6eAd1	obzvláště
těší	těšit	k5eAaImIp3nS	těšit
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
babička	babička	k1gFnSc1	babička
nebudí	budit	k5eNaImIp3nS	budit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
ranní	ranní	k2eAgFnSc2d1	ranní
mše	mše	k1gFnSc2	mše
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
jí	on	k3xPp3gFnSc3	on
pak	pak	k6eAd1	pak
chodí	chodit	k5eAaImIp3nS	chodit
naproti	naproti	k6eAd1	naproti
<g/>
,	,	kIx,	,
když	když	k8xS	když
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
hrubou	hrubý	k2eAgFnSc4d1	hrubá
mši	mše	k1gFnSc4	mše
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nedělních	nedělní	k2eAgNnPc2d1	nedělní
odpolední	odpoledne	k1gNnPc2	odpoledne
chodí	chodit	k5eAaImIp3nS	chodit
babička	babička	k1gFnSc1	babička
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
Mančinka	Mančinka	k1gFnSc1	Mančinka
<g/>
,	,	kIx,	,
děvče	děvče	k1gNnSc1	děvče
Barunčina	Barunčin	k2eAgInSc2d1	Barunčin
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
mlžně	mlžně	k6eAd1	mlžně
dostávají	dostávat	k5eAaImIp3nP	dostávat
děti	dítě	k1gFnPc1	dítě
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
křížaly	křížala	k1gFnSc2	křížala
a	a	k8xC	a
buchty	buchta	k1gFnSc2	buchta
<g/>
.	.	kIx.	.
</s>
<s>
Děvčata	děvče	k1gNnPc1	děvče
žertují	žertovat	k5eAaImIp3nP	žertovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
místo	místo	k7c2	místo
zajíců	zajíc	k1gMnPc2	zajíc
jedí	jíst	k5eAaImIp3nP	jíst
kočky	kočka	k1gFnPc1	kočka
a	a	k8xC	a
veverky	veverka	k1gFnPc1	veverka
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
také	také	k9	také
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
dovádějí	dovádět	k5eAaImIp3nP	dovádět
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
si	se	k3xPyFc3	se
s	s	k7c7	s
mlynářovou	mlynářův	k2eAgFnSc7d1	mlynářova
rodinou	rodina	k1gFnSc7	rodina
mezitím	mezitím	k6eAd1	mezitím
povídá	povídat	k5eAaImIp3nS	povídat
o	o	k7c6	o
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
ruském	ruský	k2eAgNnSc6d1	ruské
tažení	tažení	k1gNnSc6	tažení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zná	znát	k5eAaImIp3nS	znát
vojenský	vojenský	k2eAgInSc4d1	vojenský
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Olešnice	Olešnice	k1gFnSc2	Olešnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
vdovy	vdova	k1gFnSc2	vdova
Novotné	Novotná	k1gFnSc2	Novotná
naučila	naučit	k5eAaPmAgFnS	naučit
výrobě	výroba	k1gFnSc3	výroba
vlněných	vlněný	k2eAgFnPc2d1	vlněná
houní	houně	k1gFnPc2	houně
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Čudová	Čudová	k1gFnSc1	Čudová
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Křovicích	Křovice	k1gFnPc6	Křovice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poprvé	poprvé	k6eAd1	poprvé
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kulíř	Kulíř	k1gMnSc1	Kulíř
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
městečka	městečko	k1gNnSc2	městečko
Olešnice	Olešnice	k1gFnSc2	Olešnice
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
také	také	k9	také
o	o	k7c4	o
Olešnici	Olešnice	k1gFnSc4	Olešnice
u	u	k7c2	u
Červeného	Červeného	k2eAgInSc2d1	Červeného
Kostelce	Kostelec	k1gInSc2	Kostelec
nebo	nebo	k8xC	nebo
o	o	k7c6	o
Osečnici	Osečnice	k1gFnSc6	Osečnice
<g/>
.	.	kIx.	.
<g/>
Babička	babička	k1gFnSc1	babička
dále	daleko	k6eAd2	daleko
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
stavěla	stavět	k5eAaImAgFnS	stavět
se	se	k3xPyFc4	se
pevnost	pevnost	k1gFnSc1	pevnost
Josefov	Josefov	k1gInSc1	Josefov
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
tam	tam	k6eAd1	tam
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
odnášely	odnášet	k5eAaImAgFnP	odnášet
houně	houně	k1gFnPc4	houně
<g/>
,	,	kIx,	,
potkaly	potkat	k5eAaPmAgInP	potkat
pána	pán	k1gMnSc4	pán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
babičku	babička	k1gFnSc4	babička
nechal	nechat	k5eAaPmAgInS	nechat
hledět	hledět	k5eAaImF	hledět
dalekohledem	dalekohled	k1gInSc7	dalekohled
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
tolar	tolar	k1gInSc4	tolar
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
sice	sice	k8xC	sice
skutečně	skutečně	k6eAd1	skutečně
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
do	do	k7c2	do
budoucího	budoucí	k2eAgInSc2d1	budoucí
Josefova	Josefov	k1gInSc2	Josefov
zajížděl	zajíždět	k5eAaImAgInS	zajíždět
<g/>
,	,	kIx,	,
vyprávění	vyprávěný	k2eAgMnPc1d1	vyprávěný
o	o	k7c6	o
tolaru	tolar	k1gInSc6	tolar
je	být	k5eAaImIp3nS	být
však	však	k9	však
téměř	téměř	k6eAd1	téměř
jistě	jistě	k6eAd1	jistě
smyšlené	smyšlený	k2eAgNnSc1d1	smyšlené
a	a	k8xC	a
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
podobným	podobný	k2eAgInSc7d1	podobný
příběhem	příběh	k1gInSc7	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zachytil	zachytit	k5eAaPmAgInS	zachytit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
moravský	moravský	k2eAgMnSc1d1	moravský
buditel	buditel	k1gMnSc1	buditel
Tomáš	Tomáš	k1gMnSc1	Tomáš
Fryčaj	Fryčaj	k1gMnSc1	Fryčaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
5	[number]	k4	5
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
bere	brát	k5eAaImIp3nS	brát
babička	babička	k1gFnSc1	babička
děti	dítě	k1gFnPc4	dítě
k	k	k7c3	k
myslivcům	myslivec	k1gMnPc3	myslivec
do	do	k7c2	do
Rýzmburku	Rýzmburk	k1gInSc2	Rýzmburk
poblíž	poblíž	k7c2	poblíž
zříceniny	zřícenina	k1gFnSc2	zřícenina
hradu	hrad	k1gInSc2	hrad
Rýzmburk	Rýzmburk	k1gInSc1	Rýzmburk
a	a	k8xC	a
vrchnostenského	vrchnostenský	k2eAgInSc2d1	vrchnostenský
altánku	altánek	k1gInSc2	altánek
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
posedí	posedit	k5eAaPmIp3nS	posedit
a	a	k8xC	a
Barunčin	Barunčin	k2eAgMnSc1d1	Barunčin
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
si	se	k3xPyFc3	se
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
silném	silný	k2eAgMnSc6d1	silný
Ctiborovi	Ctibor	k1gMnSc6	Ctibor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
heraldická	heraldický	k2eAgFnSc1d1	heraldická
pověst	pověst	k1gFnSc1	pověst
původně	původně	k6eAd1	původně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kolem	kolem	k7c2	kolem
náhrobku	náhrobek	k1gInSc2	náhrobek
Bernarda	Bernard	k1gMnSc2	Bernard
Žehušického	Žehušický	k2eAgMnSc2d1	Žehušický
z	z	k7c2	z
Nestajova	Nestajův	k2eAgInSc2d1	Nestajův
a	a	k8xC	a
Němcová	Němcová	k1gFnSc1	Němcová
ji	on	k3xPp3gFnSc4	on
podrobněji	podrobně	k6eAd2	podrobně
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
rovněž	rovněž	k9	rovněž
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
boušínský	boušínský	k2eAgInSc4d1	boušínský
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
Babička	babička	k1gFnSc1	babička
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
zázraku	zázrak	k1gInSc2	zázrak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
udál	udát	k5eAaPmAgMnS	udát
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgFnS	ztratit
němá	němý	k2eAgFnSc1d1	němá
a	a	k8xC	a
hluchá	hluchý	k2eAgFnSc1d1	hluchá
dceruška	dceruška	k1gFnSc1	dceruška
místních	místní	k2eAgMnPc2d1	místní
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
ovčák	ovčák	k1gMnSc1	ovčák
Bárta	Bárta	k1gMnSc1	Bárta
nalezl	nalézt	k5eAaBmAgMnS	nalézt
uzdravenou	uzdravený	k2eAgFnSc4d1	uzdravená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jednou	k6eAd1	jednou
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přijede	přijet	k5eAaPmIp3nS	přijet
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
a	a	k8xC	a
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
ji	on	k3xPp3gFnSc4	on
jahody	jahoda	k1gFnPc1	jahoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
děti	dítě	k1gFnPc1	dítě
nasbíraly	nasbírat	k5eAaPmAgFnP	nasbírat
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
svého	svůj	k3xOyFgNnSc2	svůj
dítěte	dítě	k1gNnSc2	dítě
jahody	jahoda	k1gFnSc2	jahoda
nejí	jíst	k5eNaImIp3nS	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
přijme	přijmout	k5eAaPmIp3nS	přijmout
košíček	košíček	k1gInSc4	košíček
s	s	k7c7	s
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
pozve	pozvat	k5eAaPmIp3nS	pozvat
děti	dítě	k1gFnPc4	dítě
i	i	k9	i
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
na	na	k7c4	na
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
konečně	konečně	k6eAd1	konečně
přijdou	přijít	k5eAaPmIp3nP	přijít
k	k	k7c3	k
myslivně	myslivna	k1gFnSc3	myslivna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
trávě	tráva	k1gFnSc6	tráva
procházejí	procházet	k5eAaImIp3nP	procházet
pávi	páv	k1gMnPc1	páv
<g/>
,	,	kIx,	,
perličky	perlička	k1gFnPc1	perlička
<g/>
,	,	kIx,	,
bílí	bílý	k2eAgMnPc1d1	bílý
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
srna	srna	k1gFnSc1	srna
s	s	k7c7	s
obojkem	obojek	k1gInSc7	obojek
a	a	k8xC	a
několik	několik	k4yIc1	několik
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
pozve	pozvat	k5eAaPmIp3nS	pozvat
babičku	babička	k1gFnSc4	babička
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
hospodyně	hospodyně	k1gFnSc1	hospodyně
ji	on	k3xPp3gFnSc4	on
pohostí	pohostit	k5eAaPmIp3nS	pohostit
jídlem	jídlo	k1gNnSc7	jídlo
a	a	k8xC	a
pivem	pivo	k1gNnSc7	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
mezitím	mezitím	k6eAd1	mezitím
hrají	hrát	k5eAaImIp3nP	hrát
s	s	k7c7	s
myslivcovými	myslivcův	k2eAgMnPc7d1	myslivcův
chlapci	chlapec	k1gMnPc7	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
babičce	babička	k1gFnSc3	babička
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
potkal	potkat	k5eAaPmAgMnS	potkat
panstvo	panstvo	k1gNnSc4	panstvo
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
paní	paní	k1gFnSc4	paní
kněžně	kněžna	k1gFnSc3	kněžna
vyprávět	vyprávět	k5eAaImF	vyprávět
o	o	k7c6	o
bláznivé	bláznivý	k2eAgFnSc6d1	bláznivá
Viktorce	Viktorka	k1gFnSc6	Viktorka
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
ho	on	k3xPp3gMnSc4	on
požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
také	také	k9	také
o	o	k7c4	o
Viktorce	Viktorka	k1gFnSc3	Viktorka
všechno	všechen	k3xTgNnSc4	všechen
podrobně	podrobně	k6eAd1	podrobně
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
si	se	k3xPyFc3	se
zapálí	zapálit	k5eAaPmIp3nS	zapálit
dýmku	dýmka	k1gFnSc4	dýmka
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
do	do	k7c2	do
vypravování	vypravování	k1gNnSc2	vypravování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
6	[number]	k4	6
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
(	(	kIx(	(
<g/>
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
Viktorce	Viktorka	k1gFnSc6	Viktorka
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Viktorčin	Viktorčin	k2eAgInSc4d1	Viktorčin
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
sedláka	sedlák	k1gMnSc2	sedlák
z	z	k7c2	z
Žernova	Žernov	k1gInSc2	Žernov
<g/>
,	,	kIx,	,
před	před	k7c7	před
patnácti	patnáct	k4xCc7	patnáct
lety	let	k1gInPc7	let
velmi	velmi	k6eAd1	velmi
krásnou	krásný	k2eAgFnSc7d1	krásná
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
ucházelo	ucházet	k5eAaImAgNnS	ucházet
mnoho	mnoho	k4c1	mnoho
mládenců	mládenec	k1gMnPc2	mládenec
<g/>
.	.	kIx.	.
</s>
<s>
Viktorka	Viktorka	k1gFnSc1	Viktorka
si	se	k3xPyFc3	se
však	však	k9	však
nemohla	moct	k5eNaImAgFnS	moct
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
myslivec	myslivec	k1gMnSc1	myslivec
(	(	kIx(	(
<g/>
voják	voják	k1gMnSc1	voják
<g/>
)	)	kIx)	)
z	z	k7c2	z
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ležela	ležet	k5eAaImAgFnS	ležet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
černé	černá	k1gFnPc4	černá
<g/>
,	,	kIx,	,
uhrančivé	uhrančivý	k2eAgFnPc4d1	uhrančivá
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
srostlé	srostlý	k2eAgNnSc4d1	srostlé
obočí	obočí	k1gNnSc4	obočí
a	a	k8xC	a
Viktorka	Viktorka	k1gFnSc1	Viktorka
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
bála	bát	k5eAaImAgFnS	bát
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
snad	snad	k9	snad
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
morous	morous	k1gMnSc1	morous
(	(	kIx(	(
<g/>
upír	upír	k1gMnSc1	upír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Viktorka	Viktorka	k1gFnSc1	Viktorka
někam	někam	k6eAd1	někam
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
venku	venku	k6eAd1	venku
u	u	k7c2	u
okna	okno	k1gNnSc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
vdá	vdát	k5eAaPmIp3nS	vdát
za	za	k7c4	za
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zbavila	zbavit	k5eAaPmAgFnS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
selský	selský	k2eAgMnSc1d1	selský
synek	synek	k1gMnSc1	synek
Tonda	Tonda	k1gMnSc1	Tonda
<g/>
.	.	kIx.	.
</s>
<s>
Viktorka	Viktorka	k1gFnSc1	Viktorka
však	však	k9	však
stále	stále	k6eAd1	stále
vídala	vídat	k5eAaImAgFnS	vídat
vojákovy	vojákův	k2eAgFnPc4d1	vojákova
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
ani	ani	k8xC	ani
pomocí	pomocí	k7c2	pomocí
modlitby	modlitba	k1gFnSc2	modlitba
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nedařilo	dařit	k5eNaImAgNnS	dařit
se	se	k3xPyFc4	se
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
moci	moc	k1gFnSc2	moc
vymanit	vymanit	k5eAaPmF	vymanit
<g/>
.	.	kIx.	.
</s>
<s>
Šla	jít	k5eAaImAgFnS	jít
se	se	k3xPyFc4	se
poradit	poradit	k5eAaPmF	poradit
s	s	k7c7	s
kovářkou	kovářka	k1gFnSc7	kovářka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jí	on	k3xPp3gFnSc3	on
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
modlit	modlit	k5eAaImF	modlit
k	k	k7c3	k
andělu	anděl	k1gMnSc3	anděl
strážci	strážce	k1gMnSc3	strážce
a	a	k8xC	a
za	za	k7c2	za
duše	duše	k1gFnSc2	duše
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Viktorka	Viktorka	k1gFnSc1	Viktorka
to	ten	k3xDgNnSc4	ten
dokázala	dokázat	k5eAaPmAgFnS	dokázat
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
pole	pole	k1gNnSc4	pole
na	na	k7c4	na
jetelinu	jetelina	k1gFnSc4	jetelina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našla	najít	k5eAaPmAgFnS	najít
čtyřlístek	čtyřlístek	k1gInSc4	čtyřlístek
a	a	k8xC	a
zajistila	zajistit	k5eAaPmAgFnS	zajistit
si	se	k3xPyFc3	se
štěstí	štěstí	k1gNnSc4	štěstí
s	s	k7c7	s
Tondou	Tonda	k1gFnSc7	Tonda
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
zabodla	zabodnout	k5eAaPmAgFnS	zabodnout
do	do	k7c2	do
nohy	noha	k1gFnSc2	noha
trn	trn	k1gInSc1	trn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
přinesli	přinést	k5eAaPmAgMnP	přinést
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
nohu	noha	k1gFnSc4	noha
obvázanou	obvázaný	k2eAgFnSc4d1	obvázaná
bílým	bílý	k2eAgInSc7d1	bílý
<g/>
,	,	kIx,	,
hebkým	hebký	k2eAgInSc7d1	hebký
šátkem	šátek	k1gInSc7	šátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kovářce	kovářka	k1gFnSc3	kovářka
pak	pak	k6eAd1	pak
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlápla	šlápnout	k5eAaPmAgFnS	šlápnout
na	na	k7c6	na
trní	trní	k1gNnSc6	trní
u	u	k7c2	u
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
když	když	k8xS	když
utíkala	utíkat	k5eAaImAgFnS	utíkat
před	před	k7c7	před
vojákem	voják	k1gMnSc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
omdlela	omdlet	k5eAaPmAgFnS	omdlet
<g/>
,	,	kIx,	,
odnesl	odnést	k5eAaPmAgMnS	odnést
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
potoku	potok	k1gInSc3	potok
a	a	k8xC	a
ošetřil	ošetřit	k5eAaPmAgMnS	ošetřit
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
pak	pak	k6eAd1	pak
jí	on	k3xPp3gFnSc3	on
vyznal	vyznat	k5eAaPmAgInS	vyznat
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Viktorka	Viktorka	k1gFnSc1	Viktorka
mu	on	k3xPp3gMnSc3	on
uvěřila	uvěřit	k5eAaPmAgFnS	uvěřit
a	a	k8xC	a
věnovala	věnovat	k5eAaImAgFnS	věnovat
mu	on	k3xPp3gMnSc3	on
svůj	svůj	k3xOyFgInSc4	svůj
škapulíř	škapulíř	k1gInSc4	škapulíř
<g/>
.	.	kIx.	.
</s>
<s>
Ležela	ležet	k5eAaImAgFnS	ležet
pak	pak	k6eAd1	pak
dlouho	dlouho	k6eAd1	dlouho
jako	jako	k8xS	jako
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
a	a	k8xC	a
kovářka	kovářka	k1gFnSc1	kovářka
vídala	vídat	k5eAaImAgFnS	vídat
vojáka	voják	k1gMnSc4	voják
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
ovšem	ovšem	k9	ovšem
myslivci	myslivec	k1gMnPc1	myslivec
odtáhli	odtáhnout	k5eAaPmAgMnP	odtáhnout
a	a	k8xC	a
Viktorka	Viktorka	k1gFnSc1	Viktorka
se	se	k3xPyFc4	se
uzdravila	uzdravit	k5eAaPmAgFnS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
ale	ale	k9	ale
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Mařenka	Mařenka	k1gFnSc1	Mařenka
vyřídila	vyřídit	k5eAaPmAgFnS	vyřídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voják	voják	k1gMnSc1	voják
vzkazuje	vzkazovat	k5eAaImIp3nS	vzkazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Viktorka	Viktorka	k1gFnSc1	Viktorka
nezapomněla	zapomenout	k5eNaPmAgFnS	zapomenout
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
utekla	utéct	k5eAaPmAgFnS	utéct
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Viktorčin	Viktorčin	k2eAgMnSc1d1	Viktorčin
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgMnS	vypravit
hledat	hledat	k5eAaImF	hledat
vojenský	vojenský	k2eAgInSc4d1	vojenský
oddíl	oddíl	k1gInSc4	oddíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Viktorku	Viktorka	k1gFnSc4	Viktorka
nikde	nikde	k6eAd1	nikde
nenalezl	nalézt	k5eNaBmAgMnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
zahlédli	zahlédnout	k5eAaPmAgMnP	zahlédnout
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
ale	ale	k8xC	ale
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
domů	dům	k1gInPc2	dům
a	a	k8xC	a
přespávala	přespávat	k5eAaImAgFnS	přespávat
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Myslivec	Myslivec	k1gMnSc1	Myslivec
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
něco	něco	k3yInSc1	něco
háže	hážat	k5eAaPmIp3nS	hážat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
směje	smát	k5eAaImIp3nS	smát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
seděla	sedět	k5eAaImAgFnS	sedět
na	na	k7c6	na
pařezu	pařez	k1gInSc6	pařez
a	a	k8xC	a
zpívala	zpívat	k5eAaImAgFnS	zpívat
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
ukolébavku	ukolébavka	k1gFnSc4	ukolébavka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
hodila	hodit	k5eAaImAgFnS	hodit
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
Toníka	Toník	k1gMnSc2	Toník
a	a	k8xC	a
Mařenky	Mařenka	k1gFnSc2	Mařenka
přinesla	přinést	k5eAaPmAgFnS	přinést
pak	pak	k6eAd1	pak
Viktorka	Viktorka	k1gFnSc1	Viktorka
květiny	květina	k1gFnSc2	květina
do	do	k7c2	do
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
napadla	napadnout	k5eAaPmAgFnS	napadnout
světlovlasého	světlovlasý	k2eAgMnSc4d1	světlovlasý
Němce	Němec	k1gMnSc4	Němec
přezdívaného	přezdívaný	k2eAgMnSc2d1	přezdívaný
Zlatohlávek	zlatohlávek	k1gMnSc1	zlatohlávek
<g/>
,	,	kIx,	,
kousala	kousat	k5eAaImAgFnS	kousat
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
sípala	sípat	k5eAaImAgFnS	sípat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
až	až	k9	až
ho	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
zachránili	zachránit	k5eAaPmAgMnP	zachránit
pacholci	pacholek	k1gMnPc1	pacholek
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
Viktorky	Viktorka	k1gFnSc2	Viktorka
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
po	po	k7c6	po
vyslechnutí	vyslechnutí	k1gNnSc6	vyslechnutí
Viktorčina	Viktorčin	k2eAgInSc2d1	Viktorčin
příběhu	příběh	k1gInSc2	příběh
rozpláče	rozplakat	k5eAaPmIp3nS	rozplakat
a	a	k8xC	a
také	také	k9	také
Barunka	Barunka	k1gFnSc1	Barunka
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
myslí	myslet	k5eAaImIp3nS	myslet
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
ubohé	ubohý	k2eAgNnSc4d1	ubohé
děvče	děvče	k1gNnSc4	děvče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
7	[number]	k4	7
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
společně	společně	k6eAd1	společně
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
musejí	muset	k5eAaImIp3nP	muset
odhánět	odhánět	k5eAaImF	odhánět
Adélčinu	Adélčin	k2eAgFnSc4d1	Adélčina
slepici	slepice	k1gFnSc4	slepice
Černou	černý	k2eAgFnSc4d1	černá
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přivezla	přivézt	k5eAaPmAgFnS	přivézt
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
pohorské	pohorský	k2eAgFnSc2d1	Pohorská
vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
je	být	k5eAaImIp3nS	být
čistota	čistota	k1gFnSc1	čistota
jejich	jejich	k3xOp3gNnSc2	jejich
odění	odění	k1gNnSc2	odění
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
psy	pes	k1gMnPc7	pes
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterými	který	k3yIgFnPc7	který
je	on	k3xPp3gInPc4	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
otec	otec	k1gMnSc1	otec
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
.	.	kIx.	.
</s>
<s>
Projdou	projít	k5eAaPmIp3nP	projít
kolem	kolem	k7c2	kolem
hospody	hospody	k?	hospody
a	a	k8xC	a
zámeckým	zámecký	k2eAgInSc7d1	zámecký
parkem	park	k1gInSc7	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakrmí	nakrmit	k5eAaPmIp3nP	nakrmit
ryby	ryba	k1gFnPc4	ryba
v	v	k7c6	v
tůni	tůně	k1gFnSc6	tůně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
sloužící	sloužící	k2eAgNnSc1d1	sloužící
pozdraví	pozdravit	k5eAaPmIp3nS	pozdravit
po	po	k7c4	po
německu	německa	k1gFnSc4	německa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uvedení	uvedený	k2eAgMnPc1d1	uvedený
do	do	k7c2	do
předsíně	předsíň	k1gFnSc2	předsíň
<g/>
.	.	kIx.	.
</s>
<s>
Babičce	babička	k1gFnSc3	babička
je	být	k5eAaImIp3nS	být
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
že	že	k8xS	že
musejí	muset	k5eAaImIp3nP	muset
šlapat	šlapat	k5eAaImF	šlapat
po	po	k7c6	po
vzácných	vzácný	k2eAgInPc6d1	vzácný
kobercích	koberec	k1gInPc6	koberec
<g/>
,	,	kIx,	,
když	když	k8xS	když
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
kněžnina	kněžnin	k2eAgInSc2d1	kněžnin
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
milostivá	milostivý	k2eAgFnSc1d1	milostivá
paní	paní	k1gFnSc1	paní
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Políbí	políbit	k5eAaPmIp3nS	políbit
paní	paní	k1gFnSc1	paní
kněžně	kněžna	k1gFnSc3	kněžna
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
je	on	k3xPp3gInPc4	on
políbí	políbit	k5eAaPmIp3nS	políbit
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
ale	ale	k8xC	ale
babička	babička	k1gFnSc1	babička
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
bydlet	bydlet	k5eAaImF	bydlet
nechtěla	chtít	k5eNaImAgFnS	chtít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tam	tam	k6eAd1	tam
jako	jako	k8xS	jako
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tu	tu	k6eAd1	tu
není	být	k5eNaImIp3nS	být
hospodářství	hospodářství	k1gNnSc4	hospodářství
a	a	k8xC	a
nemohla	moct	k5eNaImAgFnS	moct
by	by	kYmCp3nS	by
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přichází	přicházet	k5eAaImIp3nS	přicházet
komtesa	komtesa	k1gFnSc1	komtesa
Hortensie	Hortensie	k1gFnSc1	Hortensie
<g/>
,	,	kIx,	,
kněžnina	kněžnin	k2eAgFnSc1d1	kněžnina
chráněnka	chráněnka	k1gFnSc1	chráněnka
<g/>
,	,	kIx,	,
a	a	k8xC	a
odvede	odvést	k5eAaPmIp3nS	odvést
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
postavu	postava	k1gFnSc4	postava
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Wilson	Wilson	k1gInSc4	Wilson
von	von	k1gInSc1	von
Steinach	Steinach	k1gInSc1	Steinach
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
kněžniny	kněžnin	k2eAgFnSc2d1	kněžnina
sestry	sestra	k1gFnSc2	sestra
Marie	Maria	k1gFnSc2	Maria
Pavlíny	Pavlína	k1gFnSc2	Pavlína
z	z	k7c2	z
Hohenzollern-Hechingenu	Hohenzollern-Hechingen	k1gInSc2	Hohenzollern-Hechingen
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
její	její	k3xOp3gInPc1	její
životní	životní	k2eAgInPc1d1	životní
osudy	osud	k1gInPc1	osud
byly	být	k5eAaImAgInP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
<g/>
Kněžna	kněžna	k1gFnSc1	kněžna
Zaháňská	zaháňský	k2eAgFnSc1d1	Zaháňská
si	se	k3xPyFc3	se
zatím	zatím	k6eAd1	zatím
povídá	povídat	k5eAaImIp3nS	povídat
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
a	a	k8xC	a
prohlížejí	prohlížet	k5eAaImIp3nP	prohlížet
si	se	k3xPyFc3	se
portréty	portrét	k1gInPc1	portrét
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
visí	viset	k5eAaImIp3nS	viset
kromě	kromě	k7c2	kromě
paniny	panin	k2eAgFnSc2d1	panina
rodiny	rodina	k1gFnSc2	rodina
také	také	k6eAd1	také
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Jiří	Jiří	k1gMnSc1	Jiří
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
pruském	pruský	k2eAgInSc6d1	pruský
pluku	pluk	k1gInSc6	pluk
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dělová	dělový	k2eAgFnSc1d1	dělová
koule	koule	k1gFnSc1	koule
mu	on	k3xPp3gMnSc3	on
utrhla	utrhnout	k5eAaPmAgFnS	utrhnout
nohu	noha	k1gFnSc4	noha
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
živila	živit	k5eAaImAgFnS	živit
výrobou	výroba	k1gFnSc7	výroba
houní	houně	k1gFnPc2	houně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vadila	vadit	k5eAaImAgFnS	vadit
jí	on	k3xPp3gFnSc3	on
německá	německý	k2eAgFnSc1d1	německá
řeč	řeč	k1gFnSc1	řeč
v	v	k7c6	v
Nise	Nisa	k1gFnSc6	Nisa
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
musela	muset	k5eAaImAgFnS	muset
uprchnout	uprchnout	k5eAaPmF	uprchnout
před	před	k7c7	před
povodní	povodeň	k1gFnSc7	povodeň
<g/>
,	,	kIx,	,
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
penzi	penze	k1gFnSc4	penze
z	z	k7c2	z
královské	královský	k2eAgFnSc2d1	královská
milosti	milost	k1gFnSc2	milost
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
romantická	romantický	k2eAgFnSc1d1	romantická
příhoda	příhoda	k1gFnSc1	příhoda
s	s	k7c7	s
válečným	válečný	k2eAgNnSc7d1	válečné
zraněním	zranění	k1gNnSc7	zranění
byla	být	k5eAaImAgFnS	být
literárními	literární	k2eAgMnPc7d1	literární
historiky	historik	k1gMnPc7	historik
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Václava	Václav	k1gMnSc2	Václav
Černého	Černý	k1gMnSc2	Černý
<g/>
,	,	kIx,	,
přijímána	přijímán	k2eAgFnSc1d1	přijímána
jako	jako	k8xC	jako
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
babiččin	babiččin	k2eAgMnSc1d1	babiččin
manžel	manžel	k1gMnSc1	manžel
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zmrzačen	zmrzačit	k5eAaPmNgInS	zmrzačit
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
rebelie	rebelie	k1gFnSc2	rebelie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
sloužil	sloužit	k5eAaImAgMnS	sloužit
ještě	ještě	k6eAd1	ještě
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc6	vojsko
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
sám	sám	k3xTgMnSc1	sám
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
niský	niský	k2eAgMnSc1d1	niský
houňař	houňař	k1gMnSc1	houňař
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
souchotiny	souchotiny	k1gFnPc4	souchotiny
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
domovské	domovský	k2eAgFnSc2d1	domovská
chaloupky	chaloupka	k1gFnSc2	chaloupka
se	se	k3xPyFc4	se
babička	babička	k1gFnSc1	babička
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
vyprávění	vyprávění	k1gNnSc2	vyprávění
shledala	shledat	k5eAaPmAgFnS	shledat
se	s	k7c7	s
starým	starý	k1gMnSc7	starý
<g/>
,	,	kIx,	,
slepým	slepý	k2eAgMnSc7d1	slepý
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
neviděla	vidět	k5eNaImAgFnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domovském	domovský	k2eAgNnSc6d1	domovské
stavení	stavení	k1gNnSc6	stavení
ji	on	k3xPp3gFnSc4	on
pohostili	pohostit	k5eAaPmAgMnP	pohostit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrovou	bratrův	k2eAgFnSc7d1	bratrova
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
opět	opět	k6eAd1	opět
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
houních	houně	k1gFnPc6	houně
<g/>
.	.	kIx.	.
</s>
<s>
Babiččin	babiččin	k2eAgMnSc1d1	babiččin
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
dobrotivý	dobrotivý	k2eAgMnSc1d1	dobrotivý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
půjčoval	půjčovat	k5eAaImAgMnS	půjčovat
svým	svůj	k3xOyFgMnPc3	svůj
sousedům	soused	k1gMnPc3	soused
a	a	k8xC	a
obdarovával	obdarovávat	k5eAaImAgInS	obdarovávat
žebráky	žebrák	k1gMnPc4	žebrák
<g/>
,	,	kIx,	,
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
je	on	k3xPp3gMnPc4	on
měl	mít	k5eAaImAgMnS	mít
sám	sám	k3xTgMnSc1	sám
jít	jít	k5eAaImF	jít
hledat	hledat	k5eAaImF	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
babiččiny	babiččin	k2eAgFnPc1d1	babiččina
děti	dítě	k1gFnPc1	dítě
rozlétly	rozlétnout	k5eAaPmAgFnP	rozlétnout
do	do	k7c2	do
světa	svět	k1gInSc2	svět
za	za	k7c7	za
učením	učení	k1gNnSc7	učení
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
děkuje	děkovat	k5eAaImIp3nS	děkovat
babičce	babička	k1gFnSc3	babička
za	za	k7c4	za
příběh	příběh	k1gInSc4	příběh
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
a	a	k8xC	a
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
odcházejí	odcházet	k5eAaImIp3nP	odcházet
na	na	k7c4	na
snídani	snídaně	k1gFnSc4	snídaně
i	i	k9	i
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zatím	zatím	k6eAd1	zatím
obdarovány	obdarovat	k5eAaPmNgFnP	obdarovat
komtesou	komtesa	k1gFnSc7	komtesa
Hortensií	Hortensie	k1gFnPc2	Hortensie
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
odmítá	odmítat	k5eAaImIp3nS	odmítat
čokoládu	čokoláda	k1gFnSc4	čokoláda
a	a	k8xC	a
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
snídat	snídat	k5eAaImF	snídat
kyselo	kyselo	k1gNnSc4	kyselo
a	a	k8xC	a
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
přijme	přijmout	k5eAaPmIp3nS	přijmout
trochu	trocha	k1gFnSc4	trocha
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Barunka	Barunka	k1gFnSc1	Barunka
dostane	dostat	k5eAaPmIp3nS	dostat
peníze	peníz	k1gInPc4	peníz
pro	pro	k7c4	pro
Kudrnovic	Kudrnovice	k1gFnPc2	Kudrnovice
děti	dítě	k1gFnPc4	dítě
na	na	k7c4	na
ošacení	ošacení	k1gNnSc4	ošacení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
babička	babička	k1gFnSc1	babička
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
šlechtičnu	šlechtična	k1gFnSc4	šlechtična
<g/>
,	,	kIx,	,
že	že	k8xS	že
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
almužna	almužna	k1gFnSc1	almužna
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
placená	placený	k2eAgFnSc1d1	placená
práce	práce	k1gFnSc1	práce
hlídače	hlídač	k1gMnSc2	hlídač
nebo	nebo	k8xC	nebo
poklasného	poklasný	k1gMnSc2	poklasný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
babička	babička	k1gFnSc1	babička
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
šeptá	šeptat	k5eAaImIp3nS	šeptat
si	se	k3xPyFc3	se
paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
to	ten	k3xDgNnSc1	ten
žena	žena	k1gFnSc1	žena
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
8	[number]	k4	8
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
jede	jet	k5eAaImIp3nS	jet
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
komtesa	komtesa	k1gFnSc1	komtesa
Hortensie	Hortensie	k1gFnSc1	Hortensie
a	a	k8xC	a
sesedne	sesednout	k5eAaPmIp3nS	sesednout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
pozdravila	pozdravit	k5eAaPmAgFnS	pozdravit
<g/>
.	.	kIx.	.
</s>
<s>
Povídají	povídat	k5eAaImIp3nP	povídat
si	se	k3xPyFc3	se
o	o	k7c6	o
beruškách	beruška	k1gFnPc6	beruška
a	a	k8xC	a
mravencích	mravenec	k1gMnPc6	mravenec
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zvířecí	zvířecí	k2eAgNnSc1d1	zvířecí
živobytí	živobytí	k1gNnSc1	živobytí
podobá	podobat	k5eAaImIp3nS	podobat
tomu	ten	k3xDgNnSc3	ten
lidskému	lidský	k2eAgNnSc3d1	lidské
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přicházejí	přicházet	k5eAaImIp3nP	přicházet
babička	babička	k1gFnSc1	babička
s	s	k7c7	s
Barunkou	Barunka	k1gFnSc7	Barunka
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
komtese	komtesa	k1gFnSc3	komtesa
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yInSc3	co
slouží	sloužit	k5eAaImIp3nS	sloužit
všechny	všechen	k3xTgFnPc4	všechen
různé	různý	k2eAgFnPc4d1	různá
byliny	bylina	k1gFnPc4	bylina
a	a	k8xC	a
koření	koření	k1gNnPc4	koření
<g/>
.	.	kIx.	.
</s>
<s>
Hortensie	Hortensie	k1gFnSc1	Hortensie
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
Florencie	Florencie	k1gFnSc2	Florencie
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
touží	toužit	k5eAaImIp3nP	toužit
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
vystrojené	vystrojený	k2eAgFnPc4d1	vystrojená
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Proškem	Prošek	k1gMnSc7	Prošek
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
slavnost	slavnost	k1gFnSc4	slavnost
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
procesím	procesí	k1gNnSc7	procesí
a	a	k8xC	a
mší	mše	k1gFnSc7	mše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
slavnosti	slavnost	k1gFnSc6	slavnost
jde	jít	k5eAaImIp3nS	jít
babička	babička	k1gFnSc1	babička
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
čeká	čekat	k5eAaImIp3nS	čekat
kočár	kočár	k1gInSc1	kočár
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kristla	Kristla	k1gFnSc1	Kristla
se	se	k3xPyFc4	se
namlouvá	namlouvat	k5eAaImIp3nS	namlouvat
s	s	k7c7	s
mládencem	mládenec	k1gMnSc7	mládenec
Jakubem	Jakub	k1gMnSc7	Jakub
Mílou	Míla	k1gMnSc7	Míla
–	–	k?	–
jeho	jeho	k3xOp3gInPc4	jeho
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
žernovským	žernovský	k2eAgInSc7d1	žernovský
sedlákem	sedlák	k1gInSc7	sedlák
jménem	jméno	k1gNnSc7	jméno
Jakub	Jakub	k1gMnSc1	Jakub
Míl	Míla	k1gFnPc2	Míla
<g/>
.	.	kIx.	.
</s>
<s>
Míla	Míla	k1gFnSc1	Míla
řídí	řídit	k5eAaImIp3nS	řídit
kočár	kočár	k1gInSc4	kočár
a	a	k8xC	a
hospodského	hospodský	k1gMnSc2	hospodský
dcera	dcera	k1gFnSc1	dcera
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
preceptora	preceptor	k1gMnSc2	preceptor
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
dostala	dostat	k5eAaPmAgFnS	dostat
veršované	veršovaný	k2eAgNnSc4d1	veršované
přáníčko	přáníčko	k1gNnSc4	přáníčko
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
příliš	příliš	k6eAd1	příliš
nerozumí	rozumět	k5eNaImIp3nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Verše	verš	k1gInPc1	verš
jsou	být	k5eAaImIp3nP	být
obměněnou	obměněný	k2eAgFnSc4d1	obměněná
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
ironizovanou	ironizovaný	k2eAgFnSc7d1	ironizovaná
básní	báseň	k1gFnSc7	báseň
Františka	František	k1gMnSc2	František
Matouše	Matouš	k1gMnSc2	Matouš
Klácela	Klácela	k1gMnSc2	Klácela
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k6eAd1	také
znala	znát	k5eAaImAgFnS	znát
studovaného	studovaný	k2eAgMnSc4d1	studovaný
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
podivína	podivín	k1gMnSc4	podivín
<g/>
,	,	kIx,	,
když	když	k8xS	když
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Kladsku	Kladsko	k1gNnSc6	Kladsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
humornou	humorný	k2eAgFnSc4d1	humorná
příhodu	příhoda	k1gFnSc4	příhoda
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyprávění	vyprávění	k1gNnSc1	vyprávění
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
anekdoty	anekdota	k1gFnSc2	anekdota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kolovala	kolovat	k5eAaImAgFnS	kolovat
o	o	k7c4	o
Janu	Jana	k1gFnSc4	Jana
Evangelistu	evangelista	k1gMnSc4	evangelista
Purkyňovi	Purkyňův	k2eAgMnPc5d1	Purkyňův
<g/>
.	.	kIx.	.
</s>
<s>
Kristla	Kristla	k1gFnSc1	Kristla
pak	pak	k6eAd1	pak
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
učenosti	učenost	k1gFnSc6	učenost
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
babiččině	babiččin	k2eAgNnSc6d1	Babiččino
vyprávění	vyprávění	k1gNnSc6	vyprávění
a	a	k8xC	a
na	na	k7c6	na
písničkách	písnička	k1gFnPc6	písnička
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
písni	píseň	k1gFnSc6	píseň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
složily	složit	k5eAaPmAgFnP	složit
s	s	k7c7	s
dívkami	dívka	k1gFnPc7	dívka
při	při	k7c6	při
robotě	robota	k1gFnSc6	robota
na	na	k7c6	na
panském	panské	k1gNnSc6	panské
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
kočár	kočár	k1gInSc1	kočár
projíždí	projíždět	k5eAaImIp3nS	projíždět
kolem	kolem	k7c2	kolem
vrat	vrata	k1gNnPc2	vrata
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
usměje	usmát	k5eAaPmIp3nS	usmát
se	se	k3xPyFc4	se
na	na	k7c4	na
Kristlu	Kristla	k1gFnSc4	Kristla
černě	černě	k6eAd1	černě
oděný	oděný	k2eAgMnSc1d1	oděný
italský	italský	k2eAgMnSc1d1	italský
komorník	komorník	k1gMnSc1	komorník
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
vousem	vous	k1gInSc7	vous
a	a	k8xC	a
třpytícími	třpytící	k2eAgFnPc7d1	třpytící
se	se	k3xPyFc4	se
prsteny	prsten	k1gInPc7	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Kristla	Kristla	k1gFnSc1	Kristla
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nerada	nerad	k2eAgMnSc4d1	nerad
vidí	vidět	k5eAaImIp3nP	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
snaží	snažit	k5eAaImIp3nS	snažit
začít	začít	k5eAaPmF	začít
si	se	k3xPyFc3	se
něco	něco	k6eAd1	něco
s	s	k7c7	s
českými	český	k2eAgNnPc7d1	české
děvčaty	děvče	k1gNnPc7	děvče
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
samotnou	samotný	k2eAgFnSc7d1	samotná
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
neustále	neustále	k6eAd1	neustále
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
9	[number]	k4	9
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
svatojánský	svatojánský	k2eAgInSc4d1	svatojánský
předvečer	předvečer	k1gInSc4	předvečer
svolá	svolat	k5eAaPmIp3nS	svolat
pan	pan	k1gMnSc1	pan
Prošek	Prošek	k1gMnSc1	Prošek
podle	podle	k7c2	podle
obyčeje	obyčej	k1gInSc2	obyčej
své	svůj	k3xOyFgMnPc4	svůj
nejmilejší	milý	k2eAgMnPc4d3	nejmilejší
přátele	přítel	k1gMnPc4	přítel
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Pečou	péct	k5eAaImIp3nP	péct
se	se	k3xPyFc4	se
koláče	koláč	k1gInSc2	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
sleduje	sledovat	k5eAaImIp3nS	sledovat
Kristlu	Kristla	k1gFnSc4	Kristla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sbírá	sbírat	k5eAaImIp3nS	sbírat
věnečky	věneček	k1gInPc4	věneček
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udělala	udělat	k5eAaPmAgFnS	udělat
svatojánský	svatojánský	k2eAgInSc4d1	svatojánský
věneček	věneček	k1gInSc4	věneček
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
věnečku	věneček	k1gInSc2	věneček
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
věštit	věštit	k5eAaImF	věštit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bude	být	k5eAaImBp3nS	být
vypadat	vypadat	k5eAaImF	vypadat
její	její	k3xOp3gInSc1	její
milý	milý	k2eAgInSc1d1	milý
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
setká	setkat	k5eAaPmIp3nS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nyní	nyní	k6eAd1	nyní
babička	babička	k1gFnSc1	babička
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zesnulým	zesnulý	k2eAgMnSc7d1	zesnulý
chotěm	choť	k1gMnSc7	choť
znovu	znovu	k6eAd1	znovu
setká	setkat	k5eAaPmIp3nS	setkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
hovoří	hovořit	k5eAaImIp3nS	hovořit
babička	babička	k1gFnSc1	babička
s	s	k7c7	s
Kristlou	Kristla	k1gFnSc7	Kristla
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
zámecký	zámecký	k2eAgInSc4d1	zámecký
Talián	talián	k1gInSc4	talián
dívku	dívka	k1gFnSc4	dívka
neustále	neustále	k6eAd1	neustále
obtěžuje	obtěžovat	k5eAaImIp3nS	obtěžovat
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
a	a	k8xC	a
ťuká	ťukat	k5eAaImIp3nS	ťukat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
okénko	okénko	k1gNnSc4	okénko
<g/>
.	.	kIx.	.
</s>
<s>
Vesničtí	vesnický	k2eAgMnPc1d1	vesnický
chlapci	chlapec	k1gMnPc1	chlapec
vedení	vedený	k2eAgMnPc1d1	vedený
Mílou	Míla	k1gFnSc7	Míla
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
včerejšího	včerejší	k2eAgInSc2d1	včerejší
večera	večer	k1gInSc2	večer
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
<g/>
,	,	kIx,	,
namazali	namazat	k5eAaPmAgMnP	namazat
mu	on	k3xPp3gMnSc3	on
kolomazí	kolomaz	k1gFnPc2	kolomaz
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
za	za	k7c4	za
knoflík	knoflík	k1gInSc4	knoflík
vetkli	vetknout	k5eAaPmAgMnP	vetknout
kopřivu	kopřiva	k1gFnSc4	kopřiva
<g/>
.	.	kIx.	.
</s>
<s>
Kristla	Kristla	k1gFnSc1	Kristla
však	však	k9	však
není	být	k5eNaImIp3nS	být
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mílu	Míla	k1gFnSc4	Míla
kvůli	kvůli	k7c3	kvůli
Taliánovi	Taliánův	k2eAgMnPc1d1	Taliánův
nevezmou	vzít	k5eNaPmIp3nP	vzít
za	za	k7c4	za
pacholka	pacholek	k1gMnSc4	pacholek
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
zatím	zatím	k6eAd1	zatím
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
zvířata	zvíře	k1gNnPc1	zvíře
v	v	k7c6	v
obrázkové	obrázkový	k2eAgFnSc6d1	obrázková
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
komtesy	komtesa	k1gFnSc2	komtesa
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
přichází	přicházet	k5eAaImIp3nS	přicházet
paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
pana	pan	k1gMnSc2	pan
Proška	Prošek	k1gMnSc2	Prošek
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
v	v	k7c6	v
lesích	les	k1gInPc6	les
krade	krást	k5eAaImIp3nS	krást
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
napadl	napadnout	k5eAaPmAgMnS	napadnout
jejího	její	k3xOp3gMnSc4	její
komorníka	komorník	k1gMnSc4	komorník
Piccola	Piccola	k1gFnSc1	Piccola
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
však	však	k9	však
paní	paní	k1gFnSc4	paní
kněžně	kněžna	k1gFnSc3	kněžna
rázně	rázně	k6eAd1	rázně
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
šlechtična	šlechtična	k1gFnSc1	šlechtična
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věci	věc	k1gFnPc4	věc
urovná	urovnat	k5eAaPmIp3nS	urovnat
a	a	k8xC	a
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Talián	talián	k1gInSc1	talián
nemstil	mstít	k5eNaImAgInS	mstít
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Prošková	Prošková	k1gFnSc1	Prošková
pak	pak	k6eAd1	pak
babičku	babička	k1gFnSc4	babička
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
takto	takto	k6eAd1	takto
odvážně	odvážně	k6eAd1	odvážně
mluvit	mluvit	k5eAaImF	mluvit
i	i	k9	i
s	s	k7c7	s
panstvem	panstvo	k1gNnSc7	panstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
10	[number]	k4	10
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
panímáma	panímáma	k1gFnSc1	panímáma
Prošková	Prošková	k1gFnSc1	Prošková
<g/>
,	,	kIx,	,
Kristla	Kristla	k1gFnSc1	Kristla
<g/>
,	,	kIx,	,
Mančinka	Mančinka	k1gFnSc1	Mančinka
a	a	k8xC	a
Barunka	Barunka	k1gFnSc1	Barunka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žernovské	žernovský	k2eAgFnSc2d1	žernovská
kapličky	kaplička	k1gFnSc2	kaplička
se	se	k3xPyFc4	se
přidají	přidat	k5eAaPmIp3nP	přidat
k	k	k7c3	k
zástupu	zástup	k1gInSc3	zástup
poutníků	poutník	k1gMnPc2	poutník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
Martincem	Martinec	k1gMnSc7	Martinec
<g/>
.	.	kIx.	.
</s>
<s>
Pochodují	pochodovat	k5eAaImIp3nP	pochodovat
pak	pak	k6eAd1	pak
ke	k	k7c3	k
Svatoňovicím	Svatoňovice	k1gFnPc3	Svatoňovice
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
zpívají	zpívat	k5eAaImIp3nP	zpívat
zbožné	zbožný	k2eAgFnPc1d1	zbožná
písně	píseň	k1gFnPc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
napomíná	napomínat	k5eAaImIp3nS	napomínat
Barunku	Barunka	k1gFnSc4	Barunka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
pouti	pouť	k1gFnSc6	pouť
zpívala	zpívat	k5eAaImAgFnS	zpívat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
tiše	tiš	k1gFnSc2	tiš
modlila	modlit	k5eAaImAgFnS	modlit
a	a	k8xC	a
neobracela	obracet	k5eNaImAgFnS	obracet
mysl	mysl	k1gFnSc4	mysl
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
věcem	věc	k1gFnPc3	věc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Svatoňovicích	Svatoňovice	k1gFnPc6	Svatoňovice
se	se	k3xPyFc4	se
poutníci	poutník	k1gMnPc1	poutník
napijí	napít	k5eAaPmIp3nP	napít
ze	z	k7c2	z
zázračné	zázračný	k2eAgFnSc2d1	zázračná
studánky	studánka	k1gFnSc2	studánka
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
prameny	pramen	k1gInPc7	pramen
pod	pod	k7c7	pod
stromem	strom	k1gInSc7	strom
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
osvětleného	osvětlený	k2eAgInSc2d1	osvětlený
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panímáma	panímáma	k1gFnSc1	panímáma
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
přespávají	přespávat	k5eAaImIp3nP	přespávat
u	u	k7c2	u
rodiny	rodina	k1gFnSc2	rodina
správce	správce	k1gMnSc2	správce
uhelných	uhelný	k2eAgInPc2d1	uhelný
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
Kristla	Kristla	k1gFnSc1	Kristla
s	s	k7c7	s
Ančou	Anča	k1gFnSc7	Anča
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
u	u	k7c2	u
vdovy	vdova	k1gFnSc2	vdova
domkářky	domkářka	k1gFnSc2	domkářka
<g/>
.	.	kIx.	.
</s>
<s>
Děvčata	děvče	k1gNnPc1	děvče
se	se	k3xPyFc4	se
domlouvají	domlouvat	k5eAaImIp3nP	domlouvat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
mít	mít	k5eAaImF	mít
svatbu	svatba	k1gFnSc4	svatba
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Kristla	Kristla	k1gFnSc1	Kristla
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
věštby	věštba	k1gFnSc2	věštba
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
s	s	k7c7	s
Mílou	Míla	k1gFnSc7	Míla
vyjít	vyjít	k5eAaPmF	vyjít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
babička	babička	k1gFnSc1	babička
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sen	sen	k1gInSc1	sen
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
sen	sen	k1gInSc4	sen
a	a	k8xC	a
pověrám	pověra	k1gFnPc3	pověra
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
přikládat	přikládat	k5eAaImF	přikládat
víry	vír	k1gInPc4	vír
<g/>
;	;	kIx,	;
a	a	k8xC	a
babičce	babička	k1gFnSc3	babička
věří	věřit	k5eAaImIp3nS	věřit
jako	jako	k8xC	jako
evangeliu	evangelium	k1gNnSc6	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
po	po	k7c6	po
mši	mše	k1gFnSc6	mše
se	se	k3xPyFc4	se
jdou	jít	k5eAaImIp3nP	jít
poutníci	poutník	k1gMnPc1	poutník
do	do	k7c2	do
lázní	lázeň	k1gFnPc2	lázeň
vykoupat	vykoupat	k5eAaPmF	vykoupat
a	a	k8xC	a
potom	potom	k6eAd1	potom
nakoupit	nakoupit	k5eAaPmF	nakoupit
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
růžence	růženec	k1gInPc4	růženec
<g/>
,	,	kIx,	,
figurky	figurka	k1gFnPc4	figurka
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
poutní	poutní	k2eAgInPc4d1	poutní
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
kupců	kupec	k1gMnPc2	kupec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
staré	starý	k2eAgFnPc1d1	stará
Fouskové	Fousková	k1gFnPc1	Fousková
prodal	prodat	k5eAaPmAgInS	prodat
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
levněji	levně	k6eAd2	levně
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
pak	pak	k6eAd1	pak
Barunce	Barunka	k1gFnSc3	Barunka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dobročinný	dobročinný	k2eAgInSc4d1	dobročinný
skutek	skutek	k1gInSc4	skutek
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
nemluvila	mluvit	k5eNaImAgFnS	mluvit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
levice	levice	k1gFnSc1	levice
nesmí	smět	k5eNaImIp3nS	smět
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
dává	dávat	k5eAaImIp3nS	dávat
pravice	pravice	k1gFnSc1	pravice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
ve	v	k7c6	v
rtyňském	rtyňský	k2eAgInSc6d1	rtyňský
lese	les	k1gInSc6	les
u	u	k7c2	u
hrobu	hrob	k1gInSc2	hrob
s	s	k7c7	s
devíti	devět	k4xCc7	devět
kříži	kříž	k1gInPc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Ančinka	Ančinka	k1gFnSc1	Ančinka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
panoši	panoš	k1gMnSc6	panoš
Heřmanovi	Heřman	k1gMnSc6	Heřman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
hradu	hrad	k1gInSc2	hrad
Vízmburku	Vízmburk	k1gInSc2	Vízmburk
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
sokem	sok	k1gMnSc7	sok
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
svatební	svatební	k2eAgInSc4d1	svatební
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Fousková	Fousková	k1gFnSc1	Fousková
ale	ale	k9	ale
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
panoš	panoš	k1gMnSc1	panoš
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
u	u	k7c2	u
Litoboře	Litoboř	k1gFnSc2	Litoboř
<g/>
.	.	kIx.	.
</s>
<s>
Balada	balada	k1gFnSc1	balada
o	o	k7c6	o
ženichovi	ženich	k1gMnSc6	ženich
zabitém	zabitý	k1gMnSc6	zabitý
u	u	k7c2	u
Krákorky	krákorka	k1gFnSc2	krákorka
se	se	k3xPyFc4	se
zpívávala	zpívávat	k5eAaImAgFnS	zpívávat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
i	i	k8xC	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
11	[number]	k4	11
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
panstvo	panstvo	k1gNnSc4	panstvo
i	i	k8xC	i
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnPc3	svůj
kočím	kočí	k1gMnPc3	kočí
a	a	k8xC	a
paní	paní	k1gFnSc2	paní
Prošková	Prošková	k1gFnSc1	Prošková
pláče	plakat	k5eAaImIp3nS	plakat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
manžela	manžel	k1gMnSc4	manžel
opět	opět	k6eAd1	opět
uvidí	uvidět	k5eAaPmIp3nS	uvidět
až	až	k9	až
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Chlapci	chlapec	k1gMnPc1	chlapec
pouštějí	pouštět	k5eAaImIp3nP	pouštět
draky	drak	k1gInPc4	drak
a	a	k8xC	a
Barunka	Barunka	k1gFnSc1	Barunka
sbírá	sbírat	k5eAaImIp3nS	sbírat
pro	pro	k7c4	pro
babičku	babička	k1gFnSc4	babička
plody	plod	k1gInPc1	plod
kaliny	kalina	k1gFnSc2	kalina
a	a	k8xC	a
šípky	šípka	k1gFnSc2	šípka
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sibyla	Sibyla	k1gFnSc1	Sibyla
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
mramorovém	mramorový	k2eAgMnSc6d1	mramorový
koni	kůň	k1gMnSc6	kůň
v	v	k7c6	v
kopci	kopec	k1gInSc6	kopec
a	a	k8xC	a
podle	podle	k7c2	podle
jejího	její	k3xOp3gNnSc2	její
proroctví	proroctví	k1gNnSc2	proroctví
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
zemi	zem	k1gFnSc4	zem
bída	bída	k1gFnSc1	bída
<g/>
,	,	kIx,	,
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
hlad	hlad	k1gInSc1	hlad
a	a	k8xC	a
mor	mor	k1gInSc1	mor
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
bude	být	k5eAaImBp3nS	být
roznesena	roznést	k5eAaPmNgFnS	roznést
na	na	k7c6	na
kopytech	kopyto	k1gNnPc6	kopyto
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
pak	pak	k9	pak
dětem	dítě	k1gFnPc3	dítě
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
milovat	milovat	k5eAaImF	milovat
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
proroctví	proroctví	k1gNnSc1	proroctví
nevyplnilo	vyplnit	k5eNaPmAgNnS	vyplnit
<g/>
.	.	kIx.	.
</s>
<s>
Adélka	Adélka	k1gFnSc1	Adélka
chce	chtít	k5eAaImIp3nS	chtít
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
babička	babička	k1gFnSc1	babička
neumře	umřít	k5eNaPmIp3nS	umřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
babička	babička	k1gFnSc1	babička
jí	on	k3xPp3gFnSc3	on
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všeho	všecek	k3xTgNnSc2	všecek
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
do	do	k7c2	do
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
přinese	přinést	k5eAaPmIp3nS	přinést
babička	babička	k1gFnSc1	babička
svíčičky	svíčička	k1gFnSc2	svíčička
a	a	k8xC	a
rozsvítí	rozsvítit	k5eAaPmIp3nS	rozsvítit
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
zesnulých	zesnulý	k2eAgMnPc2d1	zesnulý
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
poté	poté	k6eAd1	poté
napadne	napadnout	k5eAaPmIp3nS	napadnout
sníh	sníh	k1gInSc1	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
a	a	k8xC	a
v	v	k7c6	v
domech	dům	k1gInPc6	dům
se	se	k3xPyFc4	se
dere	drát	k5eAaImIp3nS	drát
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
zlodějích	zloděj	k1gMnPc6	zloděj
a	a	k8xC	a
loupežnících	loupežník	k1gMnPc6	loupežník
<g/>
.	.	kIx.	.
</s>
<s>
Adélka	Adélka	k1gFnSc1	Adélka
chodí	chodit	k5eAaImIp3nS	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
učí	učit	k5eAaImIp3nP	učit
se	se	k3xPyFc4	se
písmena	písmeno	k1gNnPc1	písmeno
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
babičce	babička	k1gFnSc3	babička
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
stará	starý	k2eAgFnSc1d1	stará
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ještě	ještě	k6eAd1	ještě
abecedu	abeceda	k1gFnSc4	abeceda
naučí	naučit	k5eAaPmIp3nS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
snese	snést	k5eAaPmIp3nS	snést
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
kolovrat	kolovrat	k1gInSc1	kolovrat
<g/>
,	,	kIx,	,
do	do	k7c2	do
sednice	sednice	k?	sednice
přijdou	přijít	k5eAaPmIp3nP	přijít
přástevnice	přástevnice	k1gFnPc4	přástevnice
<g/>
,	,	kIx,	,
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
se	se	k3xPyFc4	se
pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláš	mikuláš	k1gInSc4	mikuláš
dostanou	dostat	k5eAaPmIp3nP	dostat
děti	dítě	k1gFnPc1	dítě
nadílku	nadílka	k1gFnSc4	nadílka
do	do	k7c2	do
punčošky	punčoška	k1gFnSc2	punčoška
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
strachem	strach	k1gInSc7	strach
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
bílé	bílý	k2eAgFnPc1d1	bílá
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
neposlušné	poslušný	k2eNgFnPc4d1	neposlušná
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
není	být	k5eNaImIp3nS	být
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
strašeny	strašen	k2eAgFnPc1d1	strašena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pověry	pověra	k1gFnPc1	pověra
jim	on	k3xPp3gFnPc3	on
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
nevymlouvá	vymlouvat	k5eNaImIp3nS	vymlouvat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c4	v
ně	on	k3xPp3gMnPc4	on
sama	sám	k3xTgFnSc1	sám
věří	věřit	k5eAaImIp3nS	věřit
–	–	k?	–
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
nebojí	bát	k5eNaImIp3nP	bát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pevně	pevně	k6eAd1	pevně
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
Mílovi	Míla	k1gMnSc6	Míla
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
bude	být	k5eAaImBp3nS	být
možná	možná	k9	možná
muset	muset	k5eAaImF	muset
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
12	[number]	k4	12
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
a	a	k8xC	a
Boží	boží	k2eAgInSc4d1	boží
hod	hod	k1gInSc4	hod
dostávají	dostávat	k5eAaImIp3nP	dostávat
všichni	všechen	k3xTgMnPc1	všechen
příchozí	příchozí	k1gMnPc1	příchozí
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
bělidle	bělidlo	k1gNnSc6	bělidlo
i	i	k8xC	i
ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
jíst	jíst	k5eAaImF	jíst
a	a	k8xC	a
pít	pít	k5eAaImF	pít
do	do	k7c2	do
sytosti	sytost	k1gFnSc2	sytost
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
radost	radost	k1gFnSc4	radost
má	mít	k5eAaImIp3nS	mít
babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
když	když	k8xS	když
znenadání	znenadání	k6eAd1	znenadání
přijde	přijít	k5eAaPmIp3nS	přijít
z	z	k7c2	z
Olešnice	Olešnice	k1gFnSc2	Olešnice
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
postit	postit	k5eAaImF	postit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
viděly	vidět	k5eAaImAgFnP	vidět
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
prasátko	prasátko	k1gNnSc4	prasátko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
slabou	slabý	k2eAgFnSc4d1	slabá
vůli	vůle	k1gFnSc4	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Různými	různý	k2eAgInPc7d1	různý
rituály	rituál	k1gInPc7	rituál
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
věštit	věštit	k5eAaImF	věštit
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
:	:	kIx,	:
pouštění	pouštění	k1gNnSc1	pouštění
lodiček	lodička	k1gFnPc2	lodička
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
Barunka	Barunka	k1gFnSc1	Barunka
dostanou	dostat	k5eAaPmIp3nP	dostat
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozkrojení	rozkrojení	k1gNnSc1	rozkrojení
jablka	jablko	k1gNnSc2	jablko
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudou	být	k5eNaImBp3nP	být
šťastní	šťastný	k2eAgMnPc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
pasáž	pasáž	k1gFnSc4	pasáž
napsala	napsat	k5eAaBmAgFnS	napsat
Němcová	Němcová	k1gFnSc1	Němcová
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
budoucích	budoucí	k2eAgFnPc2d1	budoucí
událostí	událost	k1gFnPc2	událost
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
a	a	k8xC	a
Adélka	Adélka	k1gFnSc1	Adélka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
napsání	napsání	k1gNnPc2	napsání
Babičky	babička	k1gFnSc2	babička
ještě	ještě	k6eAd1	ještě
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
přichází	přicházet	k5eAaImIp3nS	přicházet
nadílka	nadílka	k1gFnSc1	nadílka
od	od	k7c2	od
Jezulátka	Jezulátko	k1gNnSc2	Jezulátko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
osvětleným	osvětlený	k2eAgInSc7d1	osvětlený
<g/>
,	,	kIx,	,
okrášleným	okrášlený	k2eAgInSc7d1	okrášlený
stromečkem	stromeček	k1gInSc7	stromeček
objeví	objevit	k5eAaPmIp3nP	objevit
děti	dítě	k1gFnPc1	dítě
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
tento	tento	k3xDgInSc4	tento
zvyk	zvyk	k1gInSc4	zvyk
sice	sice	k8xC	sice
předtím	předtím	k6eAd1	předtím
neznala	znát	k5eNaImAgFnS	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
líbí	líbit	k5eAaImIp3nS	líbit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Venku	venku	k6eAd1	venku
zpívá	zpívat	k5eAaImIp3nS	zpívat
pastýř	pastýř	k1gMnSc1	pastýř
koledy	koleda	k1gFnSc2	koleda
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
výslužku	výslužka	k1gFnSc4	výslužka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
jdou	jít	k5eAaImIp3nP	jít
chlapci	chlapec	k1gMnPc1	chlapec
na	na	k7c4	na
koledu	koleda	k1gFnSc4	koleda
do	do	k7c2	do
mlýna	mlýn	k1gInSc2	mlýn
a	a	k8xC	a
myslivny	myslivna	k1gFnSc2	myslivna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
se	se	k3xPyFc4	se
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
bělidle	bělidlo	k1gNnSc6	bělidlo
tančí	tančit	k5eAaImIp3nS	tančit
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
nechce	chtít	k5eNaImIp3nS	chtít
připojit	připojit	k5eAaPmF	připojit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zamlada	zamlada	k6eAd1	zamlada
tančila	tančit	k5eAaImAgFnS	tančit
neustále	neustále	k6eAd1	neustále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
svaté	svatý	k2eAgFnSc2d1	svatá
Doroty	Dorota	k1gFnSc2	Dorota
se	se	k3xPyFc4	se
předvádí	předvádět	k5eAaImIp3nS	předvádět
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
Dorotině	Dorotin	k2eAgInSc6d1	Dorotin
umučení	umučení	k1gNnSc2	umučení
králem	král	k1gMnSc7	král
Diokleciánem	Dioklecián	k1gMnSc7	Dioklecián
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc4d1	zimní
radovánky	radovánka	k1gFnPc4	radovánka
skončí	skončit	k5eAaPmIp3nP	skončit
pochováním	pochování	k1gNnSc7	pochování
masopustu	masopust	k1gInSc2	masopust
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
postní	postní	k2eAgFnSc1d1	postní
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
sedí	sedit	k5eAaImIp3nS	sedit
u	u	k7c2	u
kolovratu	kolovrat	k1gInSc2	kolovrat
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
postní	postní	k2eAgFnPc4d1	postní
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
životě	život	k1gInSc6	život
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pátou	pátý	k4xOgFnSc4	pátý
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
Smrtnou	smrtný	k2eAgFnSc4d1	Smrtná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vynáší	vynášet	k5eAaImIp3nS	vynášet
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
děvčata	děvče	k1gNnPc4	děvče
koledují	koledovat	k5eAaImIp3nP	koledovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgFnP	popsat
také	také	k9	také
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Barunka	Barunka	k1gFnSc1	Barunka
je	být	k5eAaImIp3nS	být
dojatá	dojatý	k2eAgFnSc1d1	dojatá
ze	z	k7c2	z
slavnosti	slavnost	k1gFnSc2	slavnost
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
a	a	k8xC	a
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
rozpláče	rozplakat	k5eAaPmIp3nS	rozplakat
radostí	radost	k1gFnSc7	radost
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
na	na	k7c4	na
Boží	boží	k2eAgInSc4d1	boží
hod	hod	k1gInSc4	hod
velikonoční	velikonoční	k2eAgInSc4d1	velikonoční
odnese	odnést	k5eAaPmIp3nS	odnést
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
mazanec	mazanec	k1gInSc1	mazanec
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
posvětit	posvětit	k5eAaPmF	posvětit
<g/>
.	.	kIx.	.
</s>
<s>
Příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
dynovačka	dynovačka	k1gFnSc1	dynovačka
<g/>
,	,	kIx,	,
chlapci	chlapec	k1gMnPc1	chlapec
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
šlehají	šlehat	k5eAaImIp3nP	šlehat
proutky	proutek	k1gInPc1	proutek
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
babičky	babička	k1gFnSc2	babička
a	a	k8xC	a
malé	malý	k2eAgFnSc2d1	malá
Adélky	Adélka	k1gFnSc2	Adélka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
13	[number]	k4	13
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
se	se	k3xPyFc4	se
příchod	příchod	k1gInSc1	příchod
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
sbírají	sbírat	k5eAaImIp3nP	sbírat
květiny	květina	k1gFnPc4	květina
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
dne	den	k1gInSc2	den
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
není	být	k5eNaImIp3nS	být
žádné	žádný	k3yNgNnSc1	žádný
zvíře	zvíře	k1gNnSc1	zvíře
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
,	,	kIx,	,
jed	jed	k1gInSc1	jed
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
až	až	k9	až
potom	potom	k6eAd1	potom
<g/>
.	.	kIx.	.
</s>
<s>
Chlapci	chlapec	k1gMnPc1	chlapec
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
píšťalky	píšťalka	k1gFnPc4	píšťalka
<g/>
,	,	kIx,	,
Barunka	Barunka	k1gFnSc1	Barunka
klobouček	klobouček	k1gInSc1	klobouček
z	z	k7c2	z
olšových	olšový	k2eAgInPc2d1	olšový
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
však	však	k9	však
povodeň	povodeň	k1gFnSc1	povodeň
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
koryto	koryto	k1gNnSc1	koryto
strouhy	strouha	k1gFnSc2	strouha
přeteče	přetéct	k5eAaPmIp3nS	přetéct
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
stavení	stavení	k1gNnSc4	stavení
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
údolí	údolí	k1gNnSc1	údolí
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
jezero	jezero	k1gNnSc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
voda	voda	k1gFnSc1	voda
odnáší	odnášet	k5eAaImIp3nS	odnášet
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
lávky	lávka	k1gFnPc4	lávka
i	i	k8xC	i
některá	některý	k3yIgNnPc4	některý
stavení	stavení	k1gNnPc4	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
voda	voda	k1gFnSc1	voda
opadne	opadnout	k5eAaPmIp3nS	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
filipojakubské	filipojakubský	k2eAgFnSc2d1	Filipojakubská
noci	noc	k1gFnSc2	noc
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
pálení	pálení	k1gNnSc1	pálení
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
a	a	k8xC	a
děvčata	děvče	k1gNnPc1	děvče
skáčou	skákat	k5eAaImIp3nP	skákat
přes	přes	k7c4	přes
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
s	s	k7c7	s
pochodněmi	pochodeň	k1gFnPc7	pochodeň
a	a	k8xC	a
vyhazují	vyhazovat	k5eAaImIp3nP	vyhazovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
zapálená	zapálený	k2eAgNnPc1d1	zapálené
pometla	pometlo	k1gNnPc1	pometlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
třešně	třešeň	k1gFnPc1	třešeň
a	a	k8xC	a
jahody	jahoda	k1gFnPc1	jahoda
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
začínají	začínat	k5eAaImIp3nP	začínat
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
daruje	darovat	k5eAaPmIp3nS	darovat
každému	každý	k3xTgNnSc3	každý
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
kudličku	kudlička	k1gFnSc4	kudlička
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohly	moct	k5eAaImAgInP	moct
krájet	krájet	k5eAaImF	krájet
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
a	a	k8xC	a
napomíná	napomínat	k5eAaImIp3nS	napomínat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
vyučování	vyučování	k1gNnSc6	vyučování
chovaly	chovat	k5eAaImAgFnP	chovat
slušně	slušně	k6eAd1	slušně
a	a	k8xC	a
dávaly	dávat	k5eAaImAgFnP	dávat
si	se	k3xPyFc3	se
pozor	pozor	k1gInSc4	pozor
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
pak	pak	k9	pak
babičce	babička	k1gFnSc3	babička
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
a	a	k8xC	a
jak	jak	k6eAd1	jak
dovádějí	dovádět	k5eAaImIp3nP	dovádět
o	o	k7c6	o
přestávkách	přestávka	k1gFnPc6	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
napomíná	napomínat	k5eAaImIp3nS	napomínat
Barunku	Barunka	k1gFnSc4	Barunka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepsala	psát	k5eNaImAgFnS	psát
ostatním	ostatní	k2eAgFnPc3d1	ostatní
za	za	k7c2	za
úplatu	úplat	k1gInSc2	úplat
úlohy	úloha	k1gFnSc2	úloha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
tím	ten	k3xDgNnSc7	ten
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zadanou	zadaný	k2eAgFnSc4d1	zadaná
věc	věc	k1gFnSc4	věc
naučili	naučit	k5eAaPmAgMnP	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Míla	Míla	k1gFnSc1	Míla
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
půjde	jít	k5eAaImIp3nS	jít
opravdu	opravdu	k6eAd1	opravdu
k	k	k7c3	k
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vykoupil	vykoupit	k5eAaPmAgInS	vykoupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
ženění	ženění	k1gNnSc6	ženění
s	s	k7c7	s
Kristlou	Kristla	k1gFnSc7	Kristla
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
čekat	čekat	k5eAaImF	čekat
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
14	[number]	k4	14
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
bělidle	bělidlo	k1gNnSc6	bělidlo
smutno	smutno	k6eAd1	smutno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
přišel	přijít	k5eAaPmAgMnS	přijít
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
komtesa	komtesa	k1gFnSc1	komtesa
Hortensie	Hortensie	k1gFnSc1	Hortensie
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
a	a	k8xC	a
kněžna	kněžna	k1gFnSc1	kněžna
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Proškem	Prošek	k1gMnSc7	Prošek
nepřijedou	přijet	k5eNaPmIp3nP	přijet
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
plánovalo	plánovat	k5eAaImAgNnS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Kristla	Kristla	k1gFnSc1	Kristla
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
Míla	Míla	k1gMnSc1	Míla
odveden	odvést	k5eAaPmNgMnS	odvést
<g/>
,	,	kIx,	,
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
jí	on	k3xPp3gFnSc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
ona	onen	k3xDgFnSc1	onen
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
vojnou	vojna	k1gFnSc7	vojna
soužení	soužení	k1gNnSc2	soužení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
Rakouska	Rakousko	k1gNnSc2	Rakousko
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
ji	on	k3xPp3gFnSc4	on
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
lehkomyslný	lehkomyslný	k2eAgMnSc1d1	lehkomyslný
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
Kladska	Kladsko	k1gNnSc2	Kladsko
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
staré	starý	k2eAgFnSc2d1	stará
Novotné	Novotná	k1gFnSc2	Novotná
<g/>
,	,	kIx,	,
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
verbíři	verbíř	k1gMnPc7	verbíř
<g/>
.	.	kIx.	.
</s>
<s>
Skrýval	skrývat	k5eAaImAgInS	skrývat
se	se	k3xPyFc4	se
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
chalupě	chalupa	k1gFnSc6	chalupa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
babičku	babička	k1gFnSc4	babička
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
večer	večer	k6eAd1	večer
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
prát	prát	k5eAaImF	prát
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
seskočil	seskočit	k5eAaPmAgMnS	seskočit
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
jí	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
musel	muset	k5eAaImAgInS	muset
opět	opět	k6eAd1	opět
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k6eAd1	jen
ho	on	k3xPp3gMnSc4	on
naverbovali	naverbovat	k5eAaPmAgMnP	naverbovat
Prusové	Prus	k1gMnPc1	Prus
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
chytili	chytit	k5eAaPmAgMnP	chytit
opilého	opilý	k2eAgMnSc4d1	opilý
v	v	k7c4	v
hospodě	hospodě	k?	hospodě
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
na	na	k7c6	na
vambeřické	vambeřický	k2eAgFnSc6d1	vambeřický
pouti	pouť	k1gFnSc6	pouť
vdala	vdát	k5eAaPmAgFnS	vdát
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
opustit	opustit	k5eAaPmF	opustit
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
k	k	k7c3	k
pluku	pluk	k1gInSc3	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
babička	babička	k1gFnSc1	babička
dovypráví	dovyprávět	k5eAaPmIp3nS	dovyprávět
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
Míla	Míla	k1gFnSc1	Míla
a	a	k8xC	a
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vojákem	voják	k1gMnSc7	voják
a	a	k8xC	a
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
15	[number]	k4	15
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
bělidlo	bělidlo	k1gNnSc4	bělidlo
přichází	přicházet	k5eAaImIp3nS	přicházet
myslivec	myslivec	k1gMnSc1	myslivec
Beyer	Beyer	k1gMnSc1	Beyer
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
Aurel	Aurel	k1gInSc1	Aurel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
Orel	Orel	k1gMnSc1	Orel
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dětem	dítě	k1gFnPc3	dítě
svou	svůj	k3xOyFgFnSc4	svůj
ručnici	ručnice	k1gFnSc4	ručnice
a	a	k8xC	a
myslivec	myslivec	k1gMnSc1	myslivec
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
snaze	snaha	k1gFnSc6	snaha
polapit	polapit	k5eAaPmF	polapit
škodnou	škodný	k2eAgFnSc4d1	škodná
lišku	liška	k1gFnSc4	liška
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
přichází	přicházet	k5eAaImIp3nS	přicházet
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
kněžna	kněžna	k1gFnSc1	kněžna
přijede	přijet	k5eAaPmIp3nS	přijet
na	na	k7c4	na
dožínky	dožínek	k1gInPc4	dožínek
a	a	k8xC	a
potom	potom	k6eAd1	potom
odjede	odjet	k5eAaPmIp3nS	odjet
bez	bez	k7c2	bez
koní	kůň	k1gMnPc2	kůň
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Prošek	Prošek	k1gMnSc1	Prošek
tedy	tedy	k9	tedy
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
zůstat	zůstat	k5eAaPmF	zůstat
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Povídají	povídat	k5eAaImIp3nP	povídat
si	se	k3xPyFc3	se
o	o	k7c6	o
Viktorce	Viktorka	k1gFnSc6	Viktorka
<g/>
,	,	kIx,	,
prý	prý	k9	prý
již	již	k6eAd1	již
stárne	stárnout	k5eAaImIp3nS	stárnout
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
čim	čim	k0	čim
dál	daleko	k6eAd2	daleko
méně	málo	k6eAd2	málo
a	a	k8xC	a
háze	háze	k6eAd1	háze
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
vrbové	vrbový	k2eAgInPc4d1	vrbový
proutky	proutek	k1gInPc4	proutek
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Beyer	Beyer	k1gMnSc1	Beyer
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
dýmce	dýmka	k1gFnSc6	dýmka
a	a	k8xC	a
o	o	k7c6	o
kouření	kouření	k1gNnSc6	kouření
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
kouří	kouřit	k5eAaImIp3nS	kouřit
často	často	k6eAd1	často
také	také	k9	také
staré	starý	k2eAgFnPc1d1	stará
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
tabáku	tabák	k1gInSc2	tabák
ale	ale	k8xC	ale
bramborovou	bramborový	k2eAgFnSc4d1	bramborová
nať	nať	k1gFnSc4	nať
nebo	nebo	k8xC	nebo
višňový	višňový	k2eAgInSc4d1	višňový
list	list	k1gInSc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
bolesti	bolest	k1gFnSc3	bolest
objal	obejmout	k5eAaPmAgInS	obejmout
kdysi	kdysi	k6eAd1	kdysi
strom	strom	k1gInSc1	strom
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
také	také	k9	také
rýznburský	rýznburský	k2eAgMnSc1d1	rýznburský
myslivec	myslivec	k1gMnSc1	myslivec
<g/>
.	.	kIx.	.
</s>
<s>
Beyer	Beyer	k1gMnSc1	Beyer
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
dvou	dva	k4xCgMnPc2	dva
vrahů	vrah	k1gMnPc2	vrah
<g/>
,	,	kIx,	,
pohledného	pohledný	k2eAgMnSc4d1	pohledný
a	a	k8xC	a
ošklivého	ošklivý	k2eAgMnSc4d1	ošklivý
<g/>
,	,	kIx,	,
za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
nehezkým	hezký	k2eNgNnSc7d1	nehezké
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
cely	cela	k1gFnSc2	cela
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc4d1	dobré
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
na	na	k7c4	na
scestí	scestí	k1gNnSc4	scestí
ho	on	k3xPp3gMnSc2	on
přivedl	přivést	k5eAaPmAgInS	přivést
těžký	těžký	k2eAgInSc1d1	těžký
život	život	k1gInSc1	život
a	a	k8xC	a
nenávist	nenávist	k1gFnSc1	nenávist
a	a	k8xC	a
ústrky	ústrk	k1gInPc1	ústrk
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
ale	ale	k9	ale
více	hodně	k6eAd2	hodně
litovali	litovat	k5eAaImAgMnP	litovat
krásného	krásný	k2eAgMnSc4d1	krásný
zločince	zločinec	k1gMnSc4	zločinec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vraždil	vraždit	k5eAaImAgMnS	vraždit
ze	z	k7c2	z
žárlivosti	žárlivost	k1gFnSc2	žárlivost
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
odcházejí	odcházet	k5eAaImIp3nP	odcházet
babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
Barunka	Barunka	k1gFnSc1	Barunka
a	a	k8xC	a
Adélka	Adélka	k1gFnSc1	Adélka
do	do	k7c2	do
hospody	hospody	k?	hospody
dát	dát	k5eAaPmF	dát
sbohem	sbohem	k0	sbohem
Mílovi	Mílův	k2eAgMnPc1d1	Mílův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
16	[number]	k4	16
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svatodušních	svatodušní	k2eAgInPc6d1	svatodušní
svátcích	svátek	k1gInPc6	svátek
panují	panovat	k5eAaImIp3nP	panovat
velká	velký	k2eAgNnPc4d1	velké
parna	parno	k1gNnPc4	parno
a	a	k8xC	a
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
zrají	zrát	k5eAaImIp3nP	zrát
klasy	klasa	k1gFnPc1	klasa
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Prošek	Prošek	k1gMnSc1	Prošek
přiváží	přivážet	k5eAaImIp3nS	přivážet
babičce	babička	k1gFnSc3	babička
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
dopis	dopis	k1gInSc1	dopis
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
Johanky	Johanka	k1gFnSc2	Johanka
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
nastávajícím	nastávající	k1gMnSc6	nastávající
<g/>
,	,	kIx,	,
Chorvatu	Chorvat	k1gMnSc6	Chorvat
Jurovi	Jura	k1gMnSc6	Jura
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
také	také	k9	také
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
komtesa	komtesa	k1gFnSc1	komtesa
Hortensie	Hortensie	k1gFnSc1	Hortensie
si	se	k3xPyFc3	se
má	mít	k5eAaImIp3nS	mít
brát	brát	k5eAaImF	brát
jednoho	jeden	k4xCgMnSc4	jeden
bohatého	bohatý	k2eAgMnSc4d1	bohatý
hraběte	hrabě	k1gMnSc4	hrabě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
zdravá	zdravý	k2eAgFnSc1d1	zdravá
<g/>
.	.	kIx.	.
</s>
<s>
Kristle	Kristla	k1gFnSc3	Kristla
přichází	přicházet	k5eAaImIp3nS	přicházet
psaní	psaní	k1gNnSc1	psaní
od	od	k7c2	od
milého	milý	k1gMnSc2	milý
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hortensie	Hortensie	k1gFnSc1	Hortensie
pak	pak	k6eAd1	pak
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
navštívit	navštívit	k5eAaPmF	navštívit
babičku	babička	k1gFnSc4	babička
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jí	on	k3xPp3gFnSc2	on
své	svůj	k3xOyFgFnSc2	svůj
kresby	kresba	k1gFnSc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
údolíčka	údolíčko	k1gNnSc2	údolíčko
a	a	k8xC	a
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
i	i	k9	i
Viktorka	Viktorka	k1gFnSc1	Viktorka
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgInSc1d1	jiný
obrázek	obrázek	k1gInSc1	obrázek
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
Staré	Staré	k2eAgNnSc4d1	Staré
bělidlo	bělidlo	k1gNnSc4	bělidlo
i	i	k9	i
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Komtesa	komtesa	k1gFnSc1	komtesa
požádá	požádat	k5eAaPmIp3nS	požádat
babičku	babička	k1gFnSc4	babička
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
směla	smět	k5eAaImAgFnS	smět
namalovat	namalovat	k5eAaPmF	namalovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
domluví	domluvit	k5eAaPmIp3nS	domluvit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
babička	babička	k1gFnSc1	babička
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
portrét	portrét	k1gInSc4	portrét
tam	tam	k6eAd1	tam
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
stařenka	stařenka	k1gFnSc1	stařenka
nezemře	zemřít	k5eNaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
babičku	babička	k1gFnSc4	babička
namaluje	namalovat	k5eAaPmIp3nS	namalovat
její	její	k3xOp3gNnPc4	její
vnoučata	vnouče	k1gNnPc4	vnouče
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měla	mít	k5eAaImAgFnS	mít
památku	památka	k1gFnSc4	památka
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
odrostou	odrůst	k5eAaPmIp3nP	odrůst
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
také	také	k9	také
přimlouvá	přimlouvat	k5eAaImIp3nS	přimlouvat
za	za	k7c4	za
Mílu	Míla	k1gFnSc4	Míla
a	a	k8xC	a
Kristlu	Kristla	k1gFnSc4	Kristla
a	a	k8xC	a
kněžna	kněžna	k1gFnSc1	kněžna
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomůže	pomoct	k5eAaPmIp3nS	pomoct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
17	[number]	k4	17
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Staří	starý	k2eAgMnPc1d1	starý
i	i	k9	i
mladí	mladý	k2eAgMnPc1d1	mladý
pracují	pracovat	k5eAaImIp3nP	pracovat
za	za	k7c2	za
dusného	dusný	k2eAgNnSc2d1	dusné
léta	léto	k1gNnSc2	léto
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odvezli	odvézt	k5eAaPmAgMnP	odvézt
sžaté	sžatý	k2eAgNnSc4d1	sžatý
obilí	obilí	k1gNnSc4	obilí
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
se	se	k3xPyFc4	se
kupí	kupit	k5eAaImIp3nP	kupit
hrozivé	hrozivý	k2eAgInPc1d1	hrozivý
černé	černý	k2eAgInPc1d1	černý
mraky	mrak	k1gInPc1	mrak
<g/>
,	,	kIx,	,
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
bouřka	bouřka	k1gFnSc1	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Prošek	Prošek	k1gMnSc1	Prošek
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
lese	les	k1gInSc6	les
viděl	vidět	k5eAaImAgMnS	vidět
po	po	k7c6	po
stromem	strom	k1gInSc7	strom
stát	stát	k5eAaPmF	stát
Viktorku	Viktorka	k1gFnSc4	Viktorka
<g/>
,	,	kIx,	,
smála	smát	k5eAaImAgFnS	smát
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
a	a	k8xC	a
tleskala	tleskat	k5eAaImAgFnS	tleskat
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
rozžehne	rozžehnout	k5eAaPmIp3nS	rozžehnout
hromničnou	hromničný	k2eAgFnSc7d1	hromničný
svící	svíce	k1gFnSc7	svíce
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
se	se	k3xPyFc4	se
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
venku	venku	k6eAd1	venku
šlehají	šlehat	k5eAaImIp3nP	šlehat
blesky	blesk	k1gInPc4	blesk
a	a	k8xC	a
zuří	zuřit	k5eAaImIp3nS	zuřit
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bouře	bouře	k1gFnSc1	bouře
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
,	,	kIx,	,
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
blesk	blesk	k1gInSc1	blesk
roztříštil	roztříštit	k5eAaPmAgInS	roztříštit
starou	starý	k2eAgFnSc4d1	stará
hrušku	hruška	k1gFnSc4	hruška
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měli	mít	k5eAaImAgMnP	mít
rádi	rád	k2eAgMnPc1d1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Uhodla	uhodnout	k5eAaPmAgFnS	uhodnout
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hortensie	Hortensie	k1gFnSc1	Hortensie
je	být	k5eAaImIp3nS	být
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
do	do	k7c2	do
italského	italský	k2eAgMnSc2d1	italský
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
učil	učít	k5eAaPmAgMnS	učít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kněžna	kněžna	k1gFnSc1	kněžna
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
chce	chtít	k5eAaImIp3nS	chtít
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
bouřky	bouřka	k1gFnPc4	bouřka
zahynula	zahynout	k5eAaPmAgFnS	zahynout
Viktorka	Viktorka	k1gFnSc1	Viktorka
<g/>
,	,	kIx,	,
když	když	k8xS	když
blesk	blesk	k1gInSc1	blesk
udeřil	udeřit	k5eAaPmAgInS	udeřit
do	do	k7c2	do
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgNnSc7	který
stála	stát	k5eAaImAgFnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
zahradním	zahradní	k2eAgInSc6d1	zahradní
domku	domek	k1gInSc6	domek
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
zesnulou	zesnulá	k1gFnSc4	zesnulá
podívat	podívat	k5eAaPmF	podívat
a	a	k8xC	a
pomodlit	pomodlit	k5eAaPmF	pomodlit
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Pochovají	pochovat	k5eAaPmIp3nP	pochovat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
romantickém	romantický	k2eAgNnSc6d1	romantické
údolí	údolí	k1gNnSc6	údolí
u	u	k7c2	u
boušínského	boušínský	k2eAgInSc2d1	boušínský
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
na	na	k7c4	na
hrob	hrob	k1gInSc4	hrob
jí	on	k3xPp3gFnSc3	on
zasadí	zasadit	k5eAaPmIp3nP	zasadit
jedli	jedle	k1gFnSc4	jedle
<g/>
.	.	kIx.	.
</s>
<s>
Bára	Bára	k1gFnSc1	Bára
z	z	k7c2	z
Žernova	Žernov	k1gInSc2	Žernov
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
složí	složit	k5eAaPmIp3nS	složit
smutnou	smutný	k2eAgFnSc4d1	smutná
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Žebračka	žebračka	k1gFnSc1	žebračka
Viktorka	Viktorka	k1gFnSc1	Viktorka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
literární	literární	k2eAgFnSc4d1	literární
postavu	postava	k1gFnSc4	postava
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
<g/>
,	,	kIx,	,
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zemřela	zemřít	k5eAaPmAgFnS	zemřít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
18	[number]	k4	18
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
===	===	k?	===
</s>
</p>
<p>
<s>
Komtesa	komtesa	k1gFnSc1	komtesa
předá	předat	k5eAaPmIp3nS	předat
babičce	babička	k1gFnSc3	babička
obrazy	obraz	k1gInPc4	obraz
vnoučat	vnouče	k1gNnPc2	vnouče
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
někdejší	někdejší	k2eAgFnPc1d1	někdejší
děti	dítě	k1gFnPc1	dítě
rozběhnou	rozběhnout	k5eAaPmIp3nP	rozběhnout
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dožínkách	dožínky	k1gFnPc6	dožínky
předá	předat	k5eAaPmIp3nS	předat
Kristla	Kristla	k1gFnSc1	Kristla
Hortensii	Hortensie	k1gFnSc3	Hortensie
věnec	věnec	k1gInSc4	věnec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Míla	Míla	k1gFnSc1	Míla
a	a	k8xC	a
ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
vykoupila	vykoupit	k5eAaPmAgFnS	vykoupit
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
odjedou	odjet	k5eAaPmIp3nP	odjet
kněžna	kněžna	k1gFnSc1	kněžna
a	a	k8xC	a
komtesa	komtesa	k1gFnSc1	komtesa
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
pak	pak	k6eAd1	pak
nadiktuje	nadiktovat	k5eAaPmIp3nS	nadiktovat
Barunce	Barunka	k1gFnSc3	Barunka
dopis	dopis	k1gInSc4	dopis
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Johanku	Johanka	k1gFnSc4	Johanka
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
jí	on	k3xPp3gFnSc3	on
svolení	svolení	k1gNnSc4	svolení
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
popis	popis	k1gInSc1	popis
svatebního	svatební	k2eAgNnSc2d1	svatební
veselí	veselí	k1gNnSc2	veselí
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Jakuba	Jakub	k1gMnSc2	Jakub
Míly	Míla	k1gFnSc2	Míla
a	a	k8xC	a
Kristly	Kristla	k1gFnSc2	Kristla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hostině	hostina	k1gFnSc6	hostina
následuje	následovat	k5eAaImIp3nS	následovat
tanec	tanec	k1gInSc4	tanec
a	a	k8xC	a
čepení	čepení	k1gNnSc4	čepení
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
trvá	trvat	k5eAaImIp3nS	trvat
celý	celý	k2eAgInSc4d1	celý
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
smyšlenou	smyšlený	k2eAgFnSc4d1	smyšlená
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
Kristla	Kristla	k1gFnSc1	Kristla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
předlohou	předloha	k1gFnSc7	předloha
literární	literární	k2eAgFnSc3d1	literární
postavě	postava	k1gFnSc3	postava
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
nevzala	vzít	k5eNaPmAgFnS	vzít
Jakuba	Jakub	k1gMnSc2	Jakub
Mílu	Míla	k1gMnSc4	Míla
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
učitele	učitel	k1gMnPc4	učitel
Václava	Václav	k1gMnSc2	Václav
Nemastu	Nemast	k1gInSc2	Nemast
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
pak	pak	k6eAd1	pak
přichází	přicházet	k5eAaImIp3nS	přicházet
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hortensie	Hortensie	k1gFnSc1	Hortensie
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vdávat	vdávat	k5eAaImF	vdávat
za	za	k7c2	za
mladého	mladý	k2eAgMnSc2d1	mladý
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dříve	dříve	k6eAd2	dříve
učil	učít	k5eAaPmAgInS	učít
<g/>
,	,	kIx,	,
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chvála	chvála	k1gFnSc1	chvála
Pánubohu	Pánbůh	k1gMnSc3	Pánbůh
<g/>
,	,	kIx,	,
všecko	všecek	k3xTgNnSc4	všecek
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dobře	dobře	k6eAd1	dobře
spořádalo	spořádat	k5eAaPmAgNnS	spořádat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
babička	babička	k1gFnSc1	babička
dál	daleko	k6eAd2	daleko
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
údolíčku	údolíčko	k1gNnSc6	údolíčko
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
vše	všechen	k3xTgNnSc4	všechen
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vnoučata	vnouče	k1gNnPc1	vnouče
ji	on	k3xPp3gFnSc4	on
jedno	jeden	k4xCgNnSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgMnSc6	druhý
opouštějí	opouštět	k5eAaImIp3nP	opouštět
a	a	k8xC	a
odcházejí	odcházet	k5eAaImIp3nP	odcházet
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
vracejí	vracet	k5eAaImIp3nP	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
kněžna	kněžna	k1gFnSc1	kněžna
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komtesa	komtesa	k1gFnSc1	komtesa
Hortensie	Hortensie	k1gFnSc2	Hortensie
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
nemoc	nemoc	k1gFnSc4	nemoc
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
zanechala	zanechat	k5eAaPmAgFnS	zanechat
však	však	k9	však
po	po	k7c6	po
sobě	se	k3xPyFc3	se
chlapečka	chlapeček	k1gMnSc4	chlapeček
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
pláče	plakat	k5eAaImIp3nS	plakat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
komtesu	komtesa	k1gFnSc4	komtesa
svět	svět	k1gInSc1	svět
nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
ji	on	k3xPp3gFnSc4	on
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
sobě	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
nejšťastnější	šťastný	k2eAgFnSc1d3	nejšťastnější
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
stárne	stárnout	k5eAaImIp3nS	stárnout
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemře	zemřít	k5eAaPmIp3nS	zemřít
společně	společně	k6eAd1	společně
s	s	k7c7	s
jabloní	jabloň	k1gFnSc7	jabloň
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
jabloni	jabloň	k1gFnSc6	jabloň
nevyraší	vyrašit	k5eNaPmIp3nP	vyrašit
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
babička	babička	k1gFnSc1	babička
kašlat	kašlat	k5eAaImF	kašlat
a	a	k8xC	a
potom	potom	k6eAd1	potom
ulehne	ulehnout	k5eAaPmIp3nS	ulehnout
<g/>
.	.	kIx.	.
</s>
<s>
Proškovi	Prošek	k1gMnSc3	Prošek
rozešlou	rozeslat	k5eAaPmIp3nP	rozeslat
zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
sjedou	sjet	k5eAaPmIp3nP	sjet
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc1	všechen
vnoučata	vnouče	k1gNnPc1	vnouče
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rozloučila	rozloučit	k5eAaPmAgFnS	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Barunkou	Barunka	k1gFnSc7	Barunka
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
pohřební	pohřební	k2eAgInSc4d1	pohřební
průvod	průvod	k1gInSc4	průvod
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
zašeptá	zašeptat	k5eAaPmIp3nS	zašeptat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
to	ten	k3xDgNnSc1	ten
žena	žena	k1gFnSc1	žena
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Čudová-Novotná	Čudová-Novotný	k2eAgFnSc1d1	Čudová-Novotný
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
babičku	babička	k1gFnSc4	babička
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
v	v	k7c6	v
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc4	Čechy
opustila	opustit	k5eAaPmAgFnS	opustit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
a	a	k8xC	a
následujících	následující	k2eAgNnPc2d1	následující
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
prožila	prožít	k5eAaPmAgFnS	prožít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
u	u	k7c2	u
dcery	dcera	k1gFnSc2	dcera
Johany	Johana	k1gFnSc2	Johana
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
zetě	zeť	k1gMnSc4	zeť
Šimona	Šimon	k1gMnSc4	Šimon
Frenzela	Frenzela	k1gMnSc4	Frenzela
<g/>
,	,	kIx,	,
městského	městský	k2eAgMnSc4d1	městský
slanečkáře	slanečkář	k1gMnSc4	slanečkář
<g/>
.	.	kIx.	.
</s>
<s>
Zesnula	zesnout	k5eAaPmAgFnS	zesnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
době	doba	k1gFnSc6	doba
úmrtí	úmrtí	k1gNnSc2	úmrtí
nevlastnila	vlastnit	k5eNaImAgFnS	vlastnit
nic	nic	k3yNnSc4	nic
kromě	kromě	k7c2	kromě
vlastního	vlastní	k2eAgNnSc2d1	vlastní
skromného	skromný	k2eAgNnSc2d1	skromné
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
pohřbu	pohřeb	k1gInSc2	pohřeb
je	být	k5eAaImIp3nS	být
smyšlená	smyšlený	k2eAgFnSc1d1	smyšlená
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Zaháňská	zaháňský	k2eAgFnSc1d1	Zaháňská
zemřela	zemřít	k5eAaPmAgFnS	zemřít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
před	před	k7c7	před
babičkou	babička	k1gFnSc7	babička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Žánr	žánr	k1gInSc1	žánr
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Sama	sám	k3xTgFnSc1	sám
autorka	autorka	k1gFnSc1	autorka
o	o	k7c6	o
textu	text	k1gInSc6	text
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
psaní	psaní	k1gNnSc2	psaní
mluvila	mluvit	k5eAaImAgFnS	mluvit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
jako	jako	k8xC	jako
o	o	k7c6	o
novele	novela	k1gFnSc6	novela
<g/>
,	,	kIx,	,
povídce	povídka	k1gFnSc6	povídka
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
práci	práce	k1gFnSc4	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Janáčková	Janáčková	k1gFnSc1	Janáčková
ve	v	k7c4	v
studii	studie	k1gFnSc4	studie
"	"	kIx"	"
<g/>
Obrazy	obraz	k1gInPc1	obraz
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
žánrové	žánrový	k2eAgNnSc1d1	žánrové
určení	určení	k1gNnSc1	určení
Babičky	babička	k1gFnSc2	babička
<g/>
"	"	kIx"	"
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
Babičky	babička	k1gFnSc2	babička
přibližně	přibližně	k6eAd1	přibližně
třetinový	třetinový	k2eAgInSc1d1	třetinový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
žánr	žánr	k1gInSc4	žánr
"	"	kIx"	"
<g/>
obrazu	obraz	k1gInSc2	obraz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
byl	být	k5eAaImAgInS	být
nicméně	nicméně	k8xC	nicméně
rozšiřován	rozšiřován	k2eAgInSc1d1	rozšiřován
dalšími	další	k2eAgInPc7d1	další
"	"	kIx"	"
<g/>
obrazy	obraz	k1gInPc7	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
autorku	autorka	k1gFnSc4	autorka
přitom	přitom	k6eAd1	přitom
stál	stát	k5eAaImAgInS	stát
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgMnSc1d1	umožňující
čtenáři	čtenář	k1gMnPc1	čtenář
projevit	projevit	k5eAaPmF	projevit
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
popisovanému	popisovaný	k2eAgInSc3d1	popisovaný
<g/>
,	,	kIx,	,
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
"	"	kIx"	"
<g/>
objektivního	objektivní	k2eAgInSc2d1	objektivní
<g/>
"	"	kIx"	"
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Vymezovala	vymezovat	k5eAaImAgFnS	vymezovat
se	se	k3xPyFc4	se
také	také	k9	také
proti	proti	k7c3	proti
žánru	žánr	k1gInSc3	žánr
vesnické	vesnický	k2eAgFnSc2d1	vesnická
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
vnímala	vnímat	k5eAaImAgFnS	vnímat
jako	jako	k8xS	jako
příliš	příliš	k6eAd1	příliš
prestižní	prestižní	k2eAgNnSc1d1	prestižní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
tendenční	tendenční	k2eAgFnSc1d1	tendenční
<g/>
.	.	kIx.	.
</s>
<s>
Janáčková	Janáčková	k1gFnSc1	Janáčková
také	také	k9	také
napsala	napsat	k5eAaBmAgFnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němcová	Němcová	k1gFnSc1	Němcová
mohla	moct	k5eAaImAgFnS	moct
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc4d1	velký
obraz	obraz	k1gInSc4	obraz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skromně	skromně	k6eAd1	skromně
však	však	k9	však
zvolila	zvolit	k5eAaPmAgFnS	zvolit
plurál	plurál	k1gInSc1	plurál
"	"	kIx"	"
<g/>
obrazy	obraz	k1gInPc1	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dobový	dobový	k2eAgInSc1d1	dobový
tisk	tisk	k1gInSc1	tisk
pak	pak	k6eAd1	pak
dílo	dílo	k1gNnSc4	dílo
někdy	někdy	k6eAd1	někdy
označoval	označovat	k5eAaImAgInS	označovat
za	za	k7c4	za
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ho	on	k3xPp3gMnSc4	on
řadil	řadit	k5eAaImAgInS	řadit
k	k	k7c3	k
"	"	kIx"	"
<g/>
národním	národní	k2eAgFnPc3d1	národní
povídkám	povídka	k1gFnPc3	povídka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
povídkám	povídka	k1gFnPc3	povídka
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
Františka	František	k1gMnSc2	František
Matouše	Matouš	k1gMnSc2	Matouš
Klácela	Klácela	k1gMnSc2	Klácela
<g/>
)	)	kIx)	)
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babička	babička	k1gFnSc1	babička
není	být	k5eNaImIp3nS	být
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
ani	ani	k8xC	ani
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
něco	něco	k3yInSc4	něco
nového	nový	k2eAgMnSc4d1	nový
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
galerie	galerie	k1gFnSc1	galerie
milých	milý	k2eAgFnPc2d1	Milá
podobizen	podobizna	k1gFnPc2	podobizna
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
galerie	galerie	k1gFnSc1	galerie
obrazů	obraz	k1gInPc2	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgNnSc1d1	dobové
vnímání	vnímání	k1gNnSc1	vnímání
žánrů	žánr	k1gInPc2	žánr
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
odlišné	odlišný	k2eAgNnSc1d1	odlišné
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
román	román	k1gInSc1	román
Klácel	Klácel	k1gFnSc2	Klácel
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
článku	článek	k1gInSc6	článek
vymezil	vymezit	k5eAaPmAgInS	vymezit
jako	jako	k9	jako
dobrodružné	dobrodružný	k2eAgNnSc4d1	dobrodružné
dílo	dílo	k1gNnSc4	dílo
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Boleslav	Boleslav	k1gMnSc1	Boleslav
Štorch	Štorch	k1gMnSc1	Štorch
také	také	k9	také
psal	psát	k5eAaImAgMnS	psát
o	o	k7c6	o
galerii	galerie	k1gFnSc6	galerie
obrazů	obraz	k1gInPc2	obraz
nebo	nebo	k8xC	nebo
o	o	k7c4	o
"	"	kIx"	"
<g/>
galerii	galerie	k1gFnSc4	galerie
obrazů	obraz	k1gInPc2	obraz
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
o	o	k7c6	o
"	"	kIx"	"
<g/>
neúplné	úplný	k2eNgFnSc6d1	neúplná
povídce	povídka	k1gFnSc6	povídka
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Janáčkovová	Janáčkovová	k1gFnSc1	Janáčkovová
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
syntetické	syntetický	k2eAgFnSc6d1	syntetická
učebnici	učebnice	k1gFnSc6	učebnice
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
označila	označit	k5eAaPmAgFnS	označit
nekonfliktním	konfliktní	k2eNgInSc7d1	nekonfliktní
výrazem	výraz	k1gInSc7	výraz
"	"	kIx"	"
<g/>
próza	próza	k1gFnSc1	próza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nezavrhla	zavrhnout	k5eNaPmAgFnS	zavrhnout
však	však	k9	však
ani	ani	k9	ani
možnost	možnost	k1gFnSc4	možnost
označovat	označovat	k5eAaImF	označovat
text	text	k1gInSc4	text
za	za	k7c4	za
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
značné	značný	k2eAgFnPc4d1	značná
mnohotvárnosti	mnohotvárnost	k1gFnPc4	mnohotvárnost
románový	románový	k2eAgInSc4d1	románový
žánr	žánr	k1gInSc4	žánr
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nabyl	nabýt	k5eAaPmAgMnS	nabýt
<g/>
.	.	kIx.	.
<g/>
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Babičky	babička	k1gFnSc2	babička
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
o	o	k7c4	o
idylu	idyla	k1gFnSc4	idyla
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
biedermeierovskou	biedermeierovský	k2eAgFnSc4d1	biedermeierovská
idylu	idyla	k1gFnSc4	idyla
označila	označit	k5eAaPmAgFnS	označit
Babičku	babička	k1gFnSc4	babička
například	například	k6eAd1	například
Olga	Olga	k1gFnSc1	Olga
Poštulková	Poštulkový	k2eAgFnSc1d1	Poštulková
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
Božena	Božena	k1gFnSc1	Božena
Němcovás	Němcovása	k1gFnPc2	Němcovása
Babička	babička	k1gFnSc1	babička
als	als	k?	als
biedermeierliche	biedermeierlichat	k5eAaPmIp3nS	biedermeierlichat
Idylle	Idylle	k1gFnSc1	Idylle
(	(	kIx(	(
<g/>
Babička	babička	k1gFnSc1	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgFnSc1d1	Němcové
jako	jako	k8xC	jako
biedermeierovská	biedermeierovský	k2eAgFnSc1d1	biedermeierovská
idyla	idyla	k1gFnSc1	idyla
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Miloš	Miloš	k1gMnSc1	Miloš
Sedmidubský	sedmidubský	k2eAgMnSc1d1	sedmidubský
v	v	k7c6	v
textu	text	k1gInSc6	text
"	"	kIx"	"
<g/>
Das	Das	k1gFnSc1	Das
Idyllische	Idyllisch	k1gFnSc2	Idyllisch
im	im	k?	im
Spannungsfeld	Spannungsfeld	k1gInSc1	Spannungsfeld
zwischen	zwischna	k1gFnPc2	zwischna
Kultur	kultura	k1gFnPc2	kultura
und	und	k?	und
Natur	Natur	k1gMnSc1	Natur
<g/>
:	:	kIx,	:
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
"	"	kIx"	"
<g/>
Babička	babička	k1gFnSc1	babička
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Idyličnost	idyličnost	k1gFnSc1	idyličnost
v	v	k7c6	v
napětí	napětí	k1gNnSc6	napětí
mezi	mezi	k7c7	mezi
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
přírodou	příroda	k1gFnSc7	příroda
<g/>
:	:	kIx,	:
Babička	babička	k1gFnSc1	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Babičku	babička	k1gFnSc4	babička
přijímá	přijímat	k5eAaImIp3nS	přijímat
jako	jako	k9	jako
pastorální	pastorální	k2eAgFnSc4d1	pastorální
idylu	idyla	k1gFnSc4	idyla
<g/>
,	,	kIx,	,
přenesenou	přenesený	k2eAgFnSc4d1	přenesená
z	z	k7c2	z
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
idylizované	idylizovaný	k2eAgFnSc2d1	idylizovaný
a	a	k8xC	a
abstraktní	abstraktní	k2eAgFnSc2d1	abstraktní
polohy	poloha	k1gFnSc2	poloha
do	do	k7c2	do
empirického	empirický	k2eAgInSc2d1	empirický
světa	svět	k1gInSc2	svět
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Aleš	Aleš	k1gMnSc1	Aleš
Haman	Haman	k1gMnSc1	Haman
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zpochybňováno	zpochybňovat	k5eAaImNgNnS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Haman	Haman	k1gMnSc1	Haman
také	také	k9	také
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babička	babička	k1gFnSc1	babička
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
modernizovaná	modernizovaný	k2eAgFnSc1d1	modernizovaná
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hrbata	Hrbat	k1gMnSc4	Hrbat
použil	použít	k5eAaPmAgMnS	použít
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
idyla	idyla	k1gFnSc1	idyla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Pelán	Pelán	k1gMnSc1	Pelán
pojmenování	pojmenování	k1gNnSc2	pojmenování
"	"	kIx"	"
<g/>
příkladná	příkladný	k2eAgFnSc1d1	příkladná
biedermeierovská	biedermeierovský	k2eAgFnSc1d1	biedermeierovská
idyla	idyla	k1gFnSc1	idyla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Šalda	Šalda	k1gMnSc1	Šalda
spojení	spojení	k1gNnSc2	spojení
"	"	kIx"	"
<g/>
idylická	idylický	k2eAgFnSc1d1	idylická
báseň	báseň	k1gFnSc1	báseň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Andreas	Andreas	k1gInSc1	Andreas
Ohme	ohm	k1gInSc5	ohm
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
označování	označování	k1gNnSc1	označování
Babičky	babička	k1gFnSc2	babička
za	za	k7c4	za
idylu	idyla	k1gFnSc4	idyla
nesedí	sedit	k5eNaImIp3nS	sedit
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
rozsahu	rozsah	k1gInSc3	rozsah
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žánr	žánr	k1gInSc1	žánr
idyly	idyla	k1gFnSc2	idyla
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
tradici	tradice	k1gFnSc6	tradice
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
až	až	k9	až
po	po	k7c4	po
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
psán	psát	k5eAaImNgInS	psát
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
krátkých	krátký	k2eAgInPc2d1	krátký
lyrických	lyrický	k2eAgInPc2d1	lyrický
a	a	k8xC	a
epických	epický	k2eAgFnPc2d1	epická
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
podle	podle	k7c2	podle
Ohmeho	Ohme	k1gMnSc2	Ohme
Babička	babička	k1gFnSc1	babička
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
idylu	idyla	k1gFnSc4	idyla
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
žánrové	žánrový	k2eAgFnPc4d1	žánrová
konvence	konvence	k1gFnPc4	konvence
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
k	k	k7c3	k
žánru	žánr	k1gInSc3	žánr
vesnického	vesnický	k2eAgInSc2d1	vesnický
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babičku	babička	k1gFnSc4	babička
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
Gotthelfovou	Gotthelfův	k2eAgFnSc7d1	Gotthelfův
prózou	próza	k1gFnSc7	próza
Käthie	Käthie	k1gFnSc2	Käthie
die	die	k?	die
Großmutter	Großmutter	k1gInSc1	Großmutter
(	(	kIx(	(
<g/>
Babička	babička	k1gFnSc1	babička
Käthi	Käth	k1gFnSc2	Käth
<g/>
,	,	kIx,	,
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
román	román	k1gInSc4	román
označit	označit	k5eAaPmF	označit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
dáno	dát	k5eAaPmNgNnS	dát
pouze	pouze	k6eAd1	pouze
poetikou	poetika	k1gFnSc7	poetika
žánru	žánr	k1gInSc2	žánr
románu	román	k1gInSc2	román
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
obecně	obecně	k6eAd1	obecně
závaznými	závazný	k2eAgInPc7d1	závazný
pravidly	pravidlo	k1gNnPc7	pravidlo
románového	románový	k2eAgInSc2d1	románový
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
udal	udat	k5eAaPmAgMnS	udat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babička	babička	k1gFnSc1	babička
je	být	k5eAaImIp3nS	být
zkonstruována	zkonstruovat	k5eAaPmNgFnS	zkonstruovat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dílčích	dílčí	k2eAgInPc2d1	dílčí
textů	text	k1gInPc2	text
napsaných	napsaný	k2eAgInPc2d1	napsaný
jako	jako	k8xC	jako
žánrové	žánrový	k2eAgInPc1d1	žánrový
obrázky	obrázek	k1gInPc1	obrázek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Genrebild	Genrebild	k1gInSc1	Genrebild
<g/>
"	"	kIx"	"
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
fyziologické	fyziologický	k2eAgFnPc1d1	fyziologická
skici	skica	k1gFnPc1	skica
ruské	ruský	k2eAgFnSc2d1	ruská
naturální	naturální	k2eAgFnSc2d1	naturální
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
následně	následně	k6eAd1	následně
seřazených	seřazený	k2eAgFnPc2d1	seřazená
do	do	k7c2	do
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
epického	epický	k2eAgNnSc2d1	epické
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
strukturovaného	strukturovaný	k2eAgNnSc2d1	strukturované
jako	jako	k8xC	jako
idyla	idyla	k1gFnSc1	idyla
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
Turgeněvovými	Turgeněvův	k2eAgInPc7d1	Turgeněvův
Lovcovými	lovcův	k2eAgInPc7d1	lovcův
zápisky	zápisek	k1gInPc7	zápisek
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Lenka	Lenka	k1gFnSc1	Lenka
Kusáková	Kusáková	k1gFnSc1	Kusáková
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němcová	Němcová	k1gFnSc1	Němcová
se	se	k3xPyFc4	se
poučila	poučit	k5eAaPmAgFnS	poučit
ze	z	k7c2	z
zkušeností	zkušenost	k1gFnPc2	zkušenost
romanticko-folkloristické	romantickoolkloristický	k2eAgFnSc2d1	romanticko-folkloristický
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
využila	využít	k5eAaPmAgFnS	využít
některé	některý	k3yIgInPc4	některý
tradiční	tradiční	k2eAgInPc4d1	tradiční
vyprávěcí	vyprávěcí	k2eAgInPc4d1	vyprávěcí
postupy	postup	k1gInPc4	postup
z	z	k7c2	z
žánru	žánr	k1gInSc2	žánr
venkovské	venkovský	k2eAgFnSc2d1	venkovská
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
nevyužít	využít	k5eNaPmF	využít
omezujícího	omezující	k2eAgInSc2d1	omezující
tvaru	tvar	k1gInSc2	tvar
syžetové	syžetový	k2eAgFnSc2d1	syžetová
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc1	přednost
podobě	podoba	k1gFnSc6	podoba
líčení	líčený	k2eAgMnPc1d1	líčený
s	s	k7c7	s
několika	několik	k4yIc7	několik
vloženými	vložený	k2eAgFnPc7d1	vložená
dějovými	dějový	k2eAgFnPc7d1	dějová
epizodami	epizoda	k1gFnPc7	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
použil	použít	k5eAaPmAgMnS	použít
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
volná	volný	k2eAgFnSc1d1	volná
řada	řada	k1gFnSc1	řada
výjevů	výjev	k1gInPc2	výjev
<g/>
"	"	kIx"	"
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ač	ač	k8xS	ač
není	být	k5eNaImIp3nS	být
Babička	babička	k1gFnSc1	babička
ucelenou	ucelený	k2eAgFnSc7d1	ucelená
skladbou	skladba	k1gFnSc7	skladba
románovou	románový	k2eAgFnSc7d1	románová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
dílem	díl	k1gInSc7	díl
české	český	k2eAgFnSc2d1	Česká
novelistické	novelistický	k2eAgFnSc2d1	novelistická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Opelíkově	Opelíkův	k2eAgInSc6d1	Opelíkův
Lexikonu	lexikon	k1gInSc6	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
volně	volně	k6eAd1	volně
koncipovaný	koncipovaný	k2eAgInSc4d1	koncipovaný
povídkový	povídkový	k2eAgInSc4d1	povídkový
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
<g/>
Felix	Felix	k1gMnSc1	Felix
Vodička	Vodička	k1gMnSc1	Vodička
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babička	babička	k1gFnSc1	babička
je	být	k5eAaImIp3nS	být
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
nesyžetové	syžetový	k2eNgNnSc4d1	syžetový
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
stojí	stát	k5eAaImIp3nS	stát
mimo	mimo	k7c4	mimo
syžet	syžet	k1gInSc4	syžet
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
narativní	narativní	k2eAgInSc1d1	narativní
pouze	pouze	k6eAd1	pouze
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
syžetovost	syžetovost	k1gFnSc1	syžetovost
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
epizodní	epizodní	k2eAgFnPc4d1	epizodní
události	událost	k1gFnPc4	událost
(	(	kIx(	(
<g/>
každodenní	každodenní	k2eAgFnPc4d1	každodenní
příhody	příhoda	k1gFnPc4	příhoda
<g/>
,	,	kIx,	,
milostné	milostný	k2eAgInPc4d1	milostný
příběhy	příběh	k1gInPc4	příběh
Kristly	Kristla	k1gFnSc2	Kristla
<g/>
,	,	kIx,	,
Hortensie	Hortensie	k1gFnSc2	Hortensie
a	a	k8xC	a
Viktorky	Viktorka	k1gFnSc2	Viktorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
atomizovanosti	atomizovanost	k1gFnSc3	atomizovanost
odmítal	odmítat	k5eAaImAgMnS	odmítat
Vodičky	vodička	k1gFnSc2	vodička
veškeré	veškerý	k3xTgInPc1	veškerý
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
žánrové	žánrový	k2eAgInPc1d1	žánrový
termíny	termín	k1gInPc1	termín
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
žánrové	žánrový	k2eAgNnSc1d1	žánrové
určení	určení	k1gNnSc1	určení
nutné	nutný	k2eAgNnSc1d1	nutné
používat	používat	k5eAaImF	používat
právě	právě	k9	právě
jen	jen	k9	jen
"	"	kIx"	"
<g/>
obrazy	obraz	k1gInPc4	obraz
z	z	k7c2	z
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
v	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
<g/>
,	,	kIx,	,
rozšířeném	rozšířený	k2eAgNnSc6d1	rozšířené
vydání	vydání	k1gNnSc6	vydání
své	svůj	k3xOyFgFnSc2	svůj
Knížky	knížka	k1gFnSc2	knížka
o	o	k7c6	o
Babičce	babička	k1gFnSc6	babička
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Vodičkovy	Vodičkův	k2eAgInPc4d1	Vodičkův
názory	názor	k1gInPc4	názor
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
román	román	k1gInSc4	román
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
dějovou	dějový	k2eAgFnSc7d1	dějová
složkou	složka	k1gFnSc7	složka
<g/>
,	,	kIx,	,
vztaženou	vztažený	k2eAgFnSc4d1	vztažená
na	na	k7c4	na
kompoziční	kompoziční	k2eAgInSc4d1	kompoziční
model	model	k1gInSc4	model
idylického	idylický	k2eAgInSc2d1	idylický
eposu	epos	k1gInSc2	epos
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
není	být	k5eNaImIp3nS	být
nesyžetový	syžetový	k2eNgInSc1d1	syžetový
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
syžetů	syžet	k1gInPc2	syžet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
celistvý	celistvý	k2eAgInSc1d1	celistvý
a	a	k8xC	a
provázaný	provázaný	k2eAgInSc1d1	provázaný
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vlček	Vlček	k1gMnSc1	Vlček
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
Babičku	babička	k1gFnSc4	babička
k	k	k7c3	k
vesnickým	vesnický	k2eAgInPc3d1	vesnický
románům	román	k1gInPc3	román
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
český	český	k2eAgInSc1d1	český
román	román	k1gInSc1	román
nové	nový	k2eAgFnSc2d1	nová
doby	doba	k1gFnSc2	doba
popsala	popsat	k5eAaPmAgFnS	popsat
Babičku	babička	k1gFnSc4	babička
Světlana	Světlana	k1gFnSc1	Světlana
Šerlaimova	Šerlaimův	k2eAgFnSc1d1	Šerlaimův
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
"	"	kIx"	"
<g/>
Český	český	k2eAgInSc1d1	český
román	román	k1gInSc1	román
a	a	k8xC	a
evropský	evropský	k2eAgInSc1d1	evropský
kánon	kánon	k1gInSc1	kánon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
za	za	k7c4	za
román	román	k1gInSc4	román
nekanonický	kanonický	k2eNgInSc4d1	nekanonický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
jí	jíst	k5eAaImIp3nS	jíst
románový	románový	k2eAgInSc1d1	románový
status	status	k1gInSc1	status
často	často	k6eAd1	často
upírán	upírat	k5eAaImNgInS	upírat
<g/>
.	.	kIx.	.
</s>
<s>
Tamáš	Tamat	k5eAaBmIp2nS	Tamat
Berkes	Berkes	k1gInSc1	Berkes
chápe	chápat	k5eAaImIp3nS	chápat
Babičku	babička	k1gFnSc4	babička
jako	jako	k8xC	jako
biedermeierovský	biedermeierovský	k2eAgInSc4d1	biedermeierovský
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
literárnímu	literární	k2eAgInSc3d1	literární
směru	směr	k1gInSc3	směr
==	==	k?	==
</s>
</p>
<p>
<s>
Otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
číst	číst	k5eAaImF	číst
Babičku	babička	k1gFnSc4	babička
jako	jako	k8xC	jako
romantické	romantický	k2eAgFnPc4d1	romantická
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
realistické	realistický	k2eAgNnSc1d1	realistické
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
provázána	provázat	k5eAaPmNgFnS	provázat
s	s	k7c7	s
diskusí	diskuse	k1gFnSc7	diskuse
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
přísluší	příslušet	k5eAaImIp3nS	příslušet
dílo	dílo	k1gNnSc1	dílo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
vyostřil	vyostřit	k5eAaPmAgInS	vyostřit
právě	právě	k6eAd1	právě
u	u	k7c2	u
Babičky	babička	k1gFnSc2	babička
<g/>
,	,	kIx,	,
o	o	k7c4	o
niž	jenž	k3xRgFnSc4	jenž
svedli	svést	k5eAaPmAgMnP	svést
akademickou	akademický	k2eAgFnSc4d1	akademická
půtku	půtka	k1gFnSc4	půtka
Felix	Felix	k1gMnSc1	Felix
Vodička	Vodička	k1gMnSc1	Vodička
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
<g/>
Felix	Felix	k1gMnSc1	Felix
Vodička	Vodička	k1gMnSc1	Vodička
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babička	babička	k1gFnSc1	babička
je	být	k5eAaImIp3nS	být
realistickým	realistický	k2eAgInSc7d1	realistický
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
působivost	působivost	k1gFnSc1	působivost
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
především	především	k9	především
historickým	historický	k2eAgInSc7d1	historický
pravdivým	pravdivý	k2eAgInSc7d1	pravdivý
koloritem	kolorit	k1gInSc7	kolorit
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
jednání	jednání	k1gNnSc1	jednání
babičky	babička	k1gFnSc2	babička
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
stal	stát	k5eAaPmAgMnS	stát
odpůrcem	odpůrce	k1gMnSc7	odpůrce
přiřazování	přiřazování	k1gNnSc2	přiřazování
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgFnSc2d1	Němcové
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přednášce	přednáška	k1gFnSc6	přednáška
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
pronesl	pronést	k5eAaPmAgInS	pronést
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
realistkou	realistka	k1gFnSc7	realistka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k9	ani
romantičkou	romantička	k1gFnSc7	romantička
<g/>
,	,	kIx,	,
v	v	k7c6	v
Knížce	knížka	k1gFnSc6	knížka
o	o	k7c6	o
Babičce	babička	k1gFnSc6	babička
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
nicméně	nicméně	k8xC	nicméně
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
Vodičkovi	Vodička	k1gMnSc3	Vodička
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němcová	Němcová	k1gFnSc1	Němcová
čerpala	čerpat	k5eAaImAgFnS	čerpat
z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
preromantismu	preromantismus	k1gInSc2	preromantismus
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
rousseauovského	rousseauovský	k2eAgNnSc2d1	rousseauovský
a	a	k8xC	a
herderovského	herderovský	k2eAgNnSc2d1	herderovský
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tak	tak	k9	tak
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
českého	český	k2eAgInSc2d1	český
romantismu	romantismus	k1gInSc2	romantismus
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
romantismus	romantismus	k1gInSc1	romantismus
Máchův	Máchův	k2eAgInSc1d1	Máchův
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInSc1d1	vycházející
ze	z	k7c2	z
světobolného	světobolný	k2eAgInSc2d1	světobolný
preromantismu	preromantismus	k1gInSc2	preromantismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
psána	psán	k2eAgFnSc1d1	psána
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
<g/>
,	,	kIx,	,
barvitý	barvitý	k2eAgInSc4d1	barvitý
a	a	k8xC	a
plastický	plastický	k2eAgInSc4d1	plastický
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
naléhavý	naléhavý	k2eAgInSc4d1	naléhavý
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
probouzí	probouzet	k5eAaImIp3nS	probouzet
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
pojmů	pojem	k1gInPc2	pojem
sochařských	sochařský	k2eAgNnPc2d1	sochařské
a	a	k8xC	a
malířských	malířský	k2eAgNnPc2d1	malířské
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ovšem	ovšem	k9	ovšem
Němcová	Němcová	k1gFnSc1	Němcová
nepoužívá	používat	k5eNaImIp3nS	používat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
nezvyklých	zvyklý	k2eNgFnPc2d1	nezvyklá
metafor	metafora	k1gFnPc2	metafora
nebo	nebo	k8xC	nebo
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
či	či	k8xC	či
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
Babičky	babička	k1gFnSc2	babička
je	být	k5eAaImIp3nS	být
stylově	stylově	k6eAd1	stylově
velmi	velmi	k6eAd1	velmi
uměřený	uměřený	k2eAgInSc1d1	uměřený
<g/>
,	,	kIx,	,
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
a	a	k8xC	a
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
sugestivnost	sugestivnost	k1gFnSc1	sugestivnost
jazyka	jazyk	k1gInSc2	jazyk
není	být	k5eNaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
hýřením	hýření	k1gNnSc7	hýření
bohatství	bohatství	k1gNnSc2	bohatství
jazyka	jazyk	k1gMnSc2	jazyk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
vrstvením	vrstvení	k1gNnSc7	vrstvení
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
přítomnost	přítomnost	k1gFnSc1	přítomnost
pak	pak	k6eAd1	pak
sugeruje	sugerovat	k5eAaBmIp3nS	sugerovat
i	i	k9	i
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přímo	přímo	k6eAd1	přímo
popsány	popsán	k2eAgFnPc1d1	popsána
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
připojil	připojit	k5eAaPmAgInS	připojit
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc4	množství
drobných	drobný	k2eAgInPc2d1	drobný
<g/>
,	,	kIx,	,
uplývajících	uplývající	k2eAgInPc2d1	uplývající
detailů	detail	k1gInPc2	detail
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
také	také	k9	také
syntax	syntax	k1gFnSc1	syntax
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Němcová	Němcová	k1gFnSc1	Němcová
často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
souřadné	souřadný	k2eAgNnSc4d1	souřadné
spojení	spojení	k1gNnSc4	spojení
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
spojek	spojka	k1gFnPc2	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
a	a	k8xC	a
melodický	melodický	k2eAgInSc1d1	melodický
dojem	dojem	k1gInSc1	dojem
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
posilován	posilován	k2eAgInSc1d1	posilován
ještě	ještě	k9	ještě
i	i	k9	i
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
stránkou	stránka	k1gFnSc7	stránka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
na	na	k7c4	na
klíčové	klíčový	k2eAgNnSc4d1	klíčové
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
vět	věta	k1gFnPc2	věta
jsou	být	k5eAaImIp3nP	být
umisťovány	umisťovat	k5eAaImNgInP	umisťovat
slova	slovo	k1gNnPc1	slovo
nižší	nízký	k2eAgFnPc1d2	nižší
tónem	tón	k1gInSc7	tón
a	a	k8xC	a
slabší	slabý	k2eAgMnPc1d2	slabší
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
.	.	kIx.	.
<g/>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Havránek	Havránek	k1gMnSc1	Havránek
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Babičce	babička	k1gFnSc6	babička
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
spisovným	spisovný	k2eAgInSc7d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
prvky	prvek	k1gInPc1	prvek
mluveného	mluvený	k2eAgInSc2d1	mluvený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
severovýchodočeského	severovýchodočeský	k2eAgNnSc2d1	severovýchodočeský
nářečí	nářečí	k1gNnSc2	nářečí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
výmoky	výmok	k1gInPc1	výmok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chodského	chodský	k2eAgInSc2d1	chodský
dialektu	dialekt	k1gInSc2	dialekt
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tuten	tuten	k1gInSc1	tuten
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
slovenštiny	slovenština	k1gFnSc2	slovenština
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zasmutit	zasmutit	k5eAaPmF	zasmutit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
systematické	systematický	k2eAgNnSc4d1	systematické
a	a	k8xC	a
vědomé	vědomý	k2eAgNnSc4d1	vědomé
využívání	využívání	k1gNnSc4	využívání
nářečí	nářečí	k1gNnSc2	nářečí
v	v	k7c6	v
dialozích	dialog	k1gInPc6	dialog
jako	jako	k8xS	jako
v	v	k7c6	v
Karle	Karla	k1gFnSc6	Karla
nebo	nebo	k8xC	nebo
Pohorské	pohorský	k2eAgFnSc3d1	Pohorská
vesnici	vesnice	k1gFnSc3	vesnice
<g/>
.	.	kIx.	.
<g/>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
stylistickým	stylistický	k2eAgInSc7d1	stylistický
prostředkem	prostředek	k1gInSc7	prostředek
v	v	k7c6	v
textu	text	k1gInSc6	text
Babičky	babička	k1gFnSc2	babička
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
frazémů	frazém	k1gInPc2	frazém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
rčení	rčení	k1gNnPc2	rčení
a	a	k8xC	a
přísloví	přísloví	k1gNnSc2	přísloví
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
jsou	být	k5eAaImIp3nP	být
živá	živý	k2eAgFnSc1d1	živá
i	i	k8xC	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
češtině	čeština	k1gFnSc6	čeština
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chytal	chytat	k5eAaImAgMnS	chytat
lelky	lelek	k1gInPc4	lelek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
chytrý	chytrý	k2eAgInSc1d1	chytrý
jako	jako	k8xS	jako
liška	liška	k1gFnSc1	liška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
čiň	činit	k5eAaImRp2nS	činit
čertu	čert	k1gMnSc3	čert
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
peklem	peklo	k1gNnSc7	peklo
se	se	k3xPyFc4	se
ti	ten	k3xDgMnPc1	ten
odmění	odměnit	k5eAaPmIp3nP	odměnit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgNnPc1d1	jiné
jsou	být	k5eAaImIp3nP	být
archaická	archaický	k2eAgNnPc1d1	archaické
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
slyšet	slyšet	k5eAaImF	slyšet
za	za	k7c7	za
devátou	devátý	k4xOgFnSc7	devátý
stěnou	stěna	k1gFnSc7	stěna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pinka	pinka	k1gFnSc1	pinka
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
pinka	pinka	k1gFnSc1	pinka
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
uletěla	uletět	k5eAaPmAgFnS	uletět
Pánubohu	Pánbůh	k1gMnSc3	Pánbůh
do	do	k7c2	do
okýnka	okýnko	k1gNnSc2	okýnko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slovní	slovní	k2eAgNnPc1d1	slovní
spojení	spojení	k1gNnPc1	spojení
zároveň	zároveň	k6eAd1	zároveň
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gMnPc7	jejich
jedinými	jediný	k2eAgMnPc7d1	jediný
uživateli	uživatel	k1gMnPc7	uživatel
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
nic	nic	k3yNnSc1	nic
platno	platno	k6eAd1	platno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
babička	babička	k1gFnSc1	babička
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
proti	proti	k7c3	proti
gustu	gusto	k1gNnSc3	gusto
žádný	žádný	k3yNgMnSc1	žádný
dišputát	dišputát	k?	dišputát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraznými	výrazný	k2eAgInPc7d1	výrazný
prvky	prvek	k1gInPc7	prvek
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
přechodníky	přechodník	k1gInPc1	přechodník
<g/>
,	,	kIx,	,
v	v	k7c6	v
textu	text	k1gInSc6	text
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
celkem	celkem	k6eAd1	celkem
550	[number]	k4	550
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
ve	v	k7c6	v
jmenném	jmenný	k2eAgInSc6d1	jmenný
tvaru	tvar	k1gInSc6	tvar
(	(	kIx(	(
<g/>
churav	churat	k5eAaPmDgInS	churat
<g/>
,	,	kIx,	,
zabit	zabít	k5eAaPmNgInS	zabít
<g/>
,	,	kIx,	,
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
65	[number]	k4	65
různých	různý	k2eAgNnPc2d1	různé
sloves	sloveso	k1gNnPc2	sloveso
uvozujících	uvozující	k2eAgMnPc2d1	uvozující
přímou	přímý	k2eAgFnSc4d1	přímá
řeč	řeč	k1gFnSc4	řeč
(	(	kIx(	(
<g/>
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
mínit	mínit	k5eAaImF	mínit
<g/>
,	,	kIx,	,
pravit	pravit	k5eAaBmF	pravit
<g/>
,	,	kIx,	,
prohodit	prohodit	k5eAaPmF	prohodit
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Inspirační	inspirační	k2eAgInPc1d1	inspirační
zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Babičkou	babička	k1gFnSc7	babička
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
starší	starý	k2eAgInSc1d2	starší
didaktický	didaktický	k2eAgInSc1d1	didaktický
spis	spis	k1gInSc1	spis
Pěstounka	pěstounka	k1gFnSc1	pěstounka
lékaře	lékař	k1gMnSc2	lékař
Františka	František	k1gMnSc2	František
Jana	Jan	k1gMnSc2	Jan
Mošnera	Mošner	k1gMnSc2	Mošner
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Tille	Tille	k1gFnSc2	Tille
a	a	k8xC	a
Miloslav	Miloslav	k1gMnSc1	Miloslav
Hýsek	Hýsek	k1gMnSc1	Hýsek
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
Pěstounce	pěstounka	k1gFnSc3	pěstounka
blíží	blížit	k5eAaImIp3nS	blížit
některými	některý	k3yIgInPc7	některý
rysy	rys	k1gInPc7	rys
a	a	k8xC	a
že	že	k8xS	že
původní	původní	k2eAgInSc4d1	původní
plán	plán	k1gInSc4	plán
Babičky	babička	k1gFnSc2	babička
si	se	k3xPyFc3	se
Němcová	Němcová	k1gFnSc1	Němcová
rozvrhla	rozvrhnout	k5eAaPmAgFnS	rozvrhnout
právě	právě	k6eAd1	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
četby	četba	k1gFnSc2	četba
Mošnerova	Mošnerův	k2eAgInSc2d1	Mošnerův
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
však	však	k9	však
spíše	spíše	k9	spíše
jen	jen	k9	jen
o	o	k7c4	o
tvůrčí	tvůrčí	k2eAgInSc4d1	tvůrčí
impuls	impuls	k1gInSc4	impuls
a	a	k8xC	a
vypůjčení	vypůjčení	k1gNnSc4	vypůjčení
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Pěstounka	pěstounka	k1gFnSc1	pěstounka
byla	být	k5eAaImAgFnS	být
koneckonců	koneckonců	k9	koneckonců
primárně	primárně	k6eAd1	primárně
dílem	dílo	k1gNnSc7	dílo
odborným	odborný	k2eAgNnSc7d1	odborné
a	a	k8xC	a
pedagogickým	pedagogický	k2eAgNnSc7d1	pedagogické
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Babička	babička	k1gFnSc1	babička
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ideovým	ideový	k2eAgFnPc3d1	ideová
inspiracím	inspirace	k1gFnPc3	inspirace
Němcové	Němcová	k1gFnSc2	Němcová
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Čelakovského	Čelakovský	k2eAgNnSc2d1	Čelakovský
sběratelství	sběratelství	k1gNnSc2	sběratelství
lidového	lidový	k2eAgNnSc2d1	lidové
mudrosloví	mudrosloví	k1gNnSc2	mudrosloví
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc1	myšlenka
Františka	František	k1gMnSc2	František
Matouše	Matouš	k1gMnSc2	Matouš
Klácela	Klácela	k1gFnSc1	Klácela
a	a	k8xC	a
teze	teze	k1gFnSc1	teze
Jeana	Jean	k1gMnSc2	Jean
Jacquesa	Jacques	k1gMnSc2	Jacques
Rousseaua	Rousseau	k1gMnSc2	Rousseau
o	o	k7c6	o
zušlechťujícím	zušlechťující	k2eAgInSc6d1	zušlechťující
vlivu	vliv	k1gInSc6	vliv
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
možný	možný	k2eAgInSc1d1	možný
zdroj	zdroj	k1gInSc1	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
také	také	k9	také
rakouský	rakouský	k2eAgMnSc1d1	rakouský
autor	autor	k1gMnSc1	autor
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
verzi	verze	k1gFnSc4	verze
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
Paměti	paměť	k1gFnSc2	paměť
mého	můj	k1gMnSc2	můj
pradědečka	pradědeček	k1gMnSc2	pradědeček
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
mohla	moct	k5eAaImAgFnS	moct
Němcová	Němcová	k1gFnSc1	Němcová
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mohla	moct	k5eAaImAgFnS	moct
přinejmenším	přinejmenším	k6eAd1	přinejmenším
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Stifterovy	Stifterův	k2eAgFnSc2d1	Stifterova
knihy	kniha	k1gFnSc2	kniha
si	se	k3xPyFc3	se
vepsala	vepsat	k5eAaPmAgFnS	vepsat
do	do	k7c2	do
zápisníku	zápisník	k1gInSc2	zápisník
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zachycování	zachycování	k1gNnSc2	zachycování
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
a	a	k8xC	a
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pracují	pracovat	k5eAaImIp3nP	pracovat
Paměti	paměť	k1gFnPc1	paměť
mého	můj	k1gMnSc2	můj
pradědečka	pradědeček	k1gMnSc2	pradědeček
s	s	k7c7	s
přepisováním	přepisování	k1gNnSc7	přepisování
zpřeházených	zpřeházený	k2eAgInPc2d1	zpřeházený
papírů	papír	k1gInPc2	papír
a	a	k8xC	a
zápisků	zápisek	k1gInPc2	zápisek
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
nalezne	naleznout	k5eAaPmIp3nS	naleznout
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
kufru	kufr	k1gInSc6	kufr
svého	svůj	k3xOyFgMnSc2	svůj
praděda	praděd	k1gMnSc2	praděd
<g/>
,	,	kIx,	,
východiska	východisko	k1gNnSc2	východisko
obou	dva	k4xCgNnPc2	dva
děl	dělo	k1gNnPc2	dělo
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
značně	značně	k6eAd1	značně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
obě	dva	k4xCgFnPc1	dva
tematizují	tematizovat	k5eAaImIp3nP	tematizovat
moudré	moudrý	k2eAgNnSc4d1	moudré
stáří	stáří	k1gNnSc4	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc4d1	jisté
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
německým	německý	k2eAgMnSc7d1	německý
romanopiscem	romanopisec	k1gMnSc7	romanopisec
a	a	k8xC	a
teoretikem	teoretik	k1gMnSc7	teoretik
Karlem	Karel	k1gMnSc7	Karel
Gutzkowem	Gutzkow	k1gMnSc7	Gutzkow
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
již	již	k6eAd1	již
z	z	k7c2	z
použitého	použitý	k2eAgNnSc2d1	Použité
motta	motto	k1gNnSc2	motto
<g/>
.	.	kIx.	.
</s>
<s>
Němcová	Němcová	k1gFnSc1	Němcová
autora	autor	k1gMnSc2	autor
doporučila	doporučit	k5eAaPmAgFnS	doporučit
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
synu	syn	k1gMnSc3	syn
Karlovi	Karel	k1gMnSc3	Karel
a	a	k8xC	a
užívala	užívat	k5eAaImAgFnS	užívat
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
rytíř	rytíř	k1gMnSc1	rytíř
ducha	duch	k1gMnSc2	duch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
Gutzkowova	Gutzkowův	k2eAgInSc2d1	Gutzkowův
románu	román	k1gInSc2	román
Rytíři	Rytíř	k1gMnSc3	Rytíř
ducha	duch	k1gMnSc2	duch
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Ritter	Ritter	k1gMnSc1	Ritter
vom	vom	k?	vom
Geiste	Geist	k1gMnSc5	Geist
<g/>
,	,	kIx,	,
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rytíři	rytíř	k1gMnPc1	rytíř
ducha	duch	k1gMnSc2	duch
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
Babičky	babička	k1gFnSc2	babička
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
konverzační	konverzační	k2eAgInSc4d1	konverzační
román	román	k1gInSc4	román
plný	plný	k2eAgInSc4d1	plný
dějových	dějový	k2eAgInPc2d1	dějový
zvratů	zvrat	k1gInPc2	zvrat
<g/>
;	;	kIx,	;
Němcová	Němcová	k1gFnSc1	Němcová
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
neinspirovala	inspirovat	k5eNaBmAgFnS	inspirovat
umělecky	umělecky	k6eAd1	umělecky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
myšlenkou	myšlenka	k1gFnSc7	myšlenka
sociálního	sociální	k2eAgInSc2d1	sociální
smíru	smír	k1gInSc2	smír
různých	různý	k2eAgFnPc2d1	různá
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
–	–	k?	–
samotné	samotný	k2eAgNnSc1d1	samotné
motto	motto	k1gNnSc1	motto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Gutzkowově	Gutzkowov	k1gInSc6	Gutzkowov
textu	text	k1gInSc2	text
výrokem	výrok	k1gInSc7	výrok
adresovaným	adresovaný	k2eAgInSc7d1	adresovaný
radikálnímu	radikální	k2eAgMnSc3d1	radikální
komunistovi	komunista	k1gMnSc3	komunista
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepodněcoval	podněcovat	k5eNaImAgInS	podněcovat
třídní	třídní	k2eAgInSc1d1	třídní
boj	boj	k1gInSc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Němcovou	Němcová	k1gFnSc7	Němcová
mohly	moct	k5eAaImAgFnP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
také	také	k9	také
starší	starý	k2eAgFnPc4d2	starší
osvícenské	osvícenský	k2eAgFnPc4d1	osvícenská
úvahy	úvaha	k1gFnPc4	úvaha
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
vzorně	vzorně	k6eAd1	vzorně
řízené	řízený	k2eAgFnSc3d1	řízená
obci	obec	k1gFnSc3	obec
<g/>
,	,	kIx,	,
když	když	k8xS	když
umístila	umístit	k5eAaPmAgFnS	umístit
Babičku	babička	k1gFnSc4	babička
do	do	k7c2	do
středu	střed	k1gInSc2	střed
osady	osada	k1gFnSc2	osada
jako	jako	k8xS	jako
její	její	k3xOp3gFnSc4	její
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgNnSc1d1	moderní
vnímání	vnímání	k1gNnSc1	vnímání
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
čítankách	čítanka	k1gFnPc6	čítanka
se	se	k3xPyFc4	se
úryvky	úryvek	k1gInPc7	úryvek
z	z	k7c2	z
Babičky	babička	k1gFnSc2	babička
objevovaly	objevovat	k5eAaImAgInP	objevovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
tradičně	tradičně	k6eAd1	tradičně
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
povinná	povinný	k2eAgFnSc1d1	povinná
či	či	k8xC	či
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
dětská	dětský	k2eAgFnSc1d1	dětská
četba	četba	k1gFnSc1	četba
pro	pro	k7c4	pro
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
budilo	budit	k5eAaImAgNnS	budit
jisté	jistý	k2eAgInPc4d1	jistý
rozpaky	rozpak	k1gInPc4	rozpak
a	a	k8xC	a
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
text	text	k1gInSc1	text
doporučován	doporučovat	k5eAaImNgInS	doporučovat
spíše	spíše	k9	spíše
středoškolským	středoškolský	k2eAgMnPc3d1	středoškolský
studentům	student	k1gMnPc3	student
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
Ke	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
Babičky	babička	k1gFnSc2	babička
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
matiné	matiné	k1gNnSc7	matiné
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1955	[number]	k4	1955
v	v	k7c6	v
Tylově	Tylův	k2eAgNnSc6d1	Tylovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
čtenářské	čtenářský	k2eAgFnSc6d1	čtenářská
anketě	anketa	k1gFnSc6	anketa
Kniha	kniha	k1gFnSc1	kniha
mého	můj	k3xOp1gNnSc2	můj
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
organizovala	organizovat	k5eAaBmAgFnS	organizovat
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Babička	babička	k1gFnSc1	babička
umístila	umístit	k5eAaPmAgFnS	umístit
s	s	k7c7	s
29	[number]	k4	29
935	[number]	k4	935
hlasy	hlas	k1gInPc7	hlas
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
–	–	k?	–
černobílý	černobílý	k2eAgInSc1d1	černobílý
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Thea	Thea	k1gFnSc1	Thea
Červenková	Červenková	k1gFnSc1	Červenková
<g/>
,	,	kIx,	,
v	v	k7c4	v
roli	role	k1gFnSc4	role
babičky	babička	k1gFnSc2	babička
Ludmila	Ludmila	k1gFnSc1	Ludmila
Lvová-Innemannová	Lvová-Innemannová	k1gFnSc1	Lvová-Innemannová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Barunky	Barunka	k1gFnSc2	Barunka
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
Liduška	Liduška	k1gFnSc1	Liduška
Innemannová	Innemannová	k1gFnSc1	Innemannová
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
–	–	k?	–
černobílý	černobílý	k2eAgInSc4d1	černobílý
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Čáp	Čáp	k1gMnSc1	Čáp
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
babičky	babička	k1gFnSc2	babička
Terezie	Terezie	k1gFnSc2	Terezie
Brzková	Brzková	k1gFnSc1	Brzková
<g/>
,	,	kIx,	,
v	v	k7c4	v
roli	role	k1gFnSc4	role
Barunky	Barunka	k1gFnSc2	Barunka
Nataša	Nataša	k1gFnSc1	Nataša
Tanská	Tanská	k1gFnSc1	Tanská
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
–	–	k?	–
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
barevný	barevný	k2eAgInSc1d1	barevný
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Moskalyk	Moskalyk	k1gMnSc1	Moskalyk
<g/>
,	,	kIx,	,
v	v	k7c4	v
roli	role	k1gFnSc4	role
babičky	babička	k1gFnSc2	babička
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kurandová	Kurandový	k2eAgFnSc1d1	Kurandová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Barunky	Barunka	k1gFnSc2	Barunka
Libuše	Libuše	k1gFnSc2	Libuše
Šafránková	Šafránková	k1gFnSc1	Šafránková
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
–	–	k?	–
opera	opera	k1gFnSc1	opera
Antonína	Antonín	k1gMnSc4	Antonín
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
Horáka	Horák	k1gMnSc4	Horák
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Adolfa	Adolf	k1gMnSc2	Adolf
Weniga	Wenig	k1gMnSc2	Wenig
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
11	[number]	k4	11
<g/>
×	×	k?	×
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
–	–	k?	–
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
letech	let	k1gInPc6	let
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
v	v	k7c4	v
roli	role	k1gFnSc4	role
babičky	babička	k1gFnSc2	babička
Vlasta	Vlasta	k1gFnSc1	Vlasta
Chramostová	Chramostová	k1gFnSc1	Chramostová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Barunky	Barunka	k1gFnSc2	Barunka
alternovaly	alternovat	k5eAaImAgFnP	alternovat
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Borová	borový	k2eAgFnSc1d1	Borová
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Josefíková	Josefíková	k1gFnSc1	Josefíková
<g/>
;	;	kIx,	;
zdramatizovali	zdramatizovat	k5eAaPmAgMnP	zdramatizovat
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
Kolihová	Kolihová	k1gFnSc1	Kolihová
Havlíková	Havlíková	k1gFnSc1	Havlíková
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
scénáře	scénář	k1gInSc2	scénář
Růženy	Růžena	k1gFnSc2	Růžena
Pohorské	pohorský	k2eAgFnSc2d1	Pohorská
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
–	–	k?	–
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
Fidlovačce	Fidlovačka	k1gFnSc6	Fidlovačka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
v	v	k7c4	v
roli	role	k1gFnSc4	role
babičky	babička	k1gFnSc2	babička
Eliška	Eliška	k1gFnSc1	Eliška
Balzerová	Balzerová	k1gFnSc1	Balzerová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Barunky	Barunka	k1gFnSc2	Barunka
alternují	alternovat	k5eAaImIp3nP	alternovat
Anika	Anika	k1gFnSc1	Anika
Menclová	Menclová	k1gFnSc1	Menclová
a	a	k8xC	a
Rebeka	Rebeka	k1gFnSc1	Rebeka
Chudobová	Chudobová	k1gFnSc1	Chudobová
</s>
</p>
<p>
<s>
===	===	k?	===
Zpracování	zpracování	k1gNnSc1	zpracování
knihou	kniha	k1gFnSc7	kniha
pouze	pouze	k6eAd1	pouze
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
bělidle	bělidlo	k1gNnSc6	bělidlo
–	–	k?	–
opera	opera	k1gFnSc1	opera
Karla	Karla	k1gFnSc1	Karla
Kovařovice	Kovařovice	k1gFnSc1	Kovařovice
(	(	kIx(	(
<g/>
starším	starý	k2eAgInSc7d2	starší
pravopisem	pravopis	k1gInSc7	pravopis
Na	na	k7c6	na
starém	starý	k2eAgNnSc6d1	staré
bělidle	bělidlo	k1gNnSc6	bělidlo
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
obrazech	obraz	k1gInPc6	obraz
dle	dle	k7c2	dle
Babičky	babička	k1gFnSc2	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viktorka	Viktorka	k1gFnSc1	Viktorka
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Svobody	Svoboda	k1gMnSc2	Svoboda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
povídky	povídka	k1gFnSc2	povídka
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
–	–	k?	–
fetišistická	fetišistický	k2eAgFnSc1d1	fetišistická
revue	revue	k1gFnSc1	revue
–	–	k?	–
inscenace	inscenace	k1gFnSc2	inscenace
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
babičky	babička	k1gFnSc2	babička
Jiří	Jiří	k1gMnSc1	Jiří
Pecha	Pecha	k1gMnSc1	Pecha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
obdržel	obdržet	k5eAaPmAgMnS	obdržet
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
speciální	speciální	k2eAgFnSc4d1	speciální
Cenu	cena	k1gFnSc4	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoko	k1gNnSc2	Radoko
za	za	k7c4	za
"	"	kIx"	"
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
aneb	aneb	k?	aneb
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
doopravdy	doopravdy	k6eAd1	doopravdy
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
parodie	parodie	k1gFnSc1	parodie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
==	==	k?	==
Překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
Muzeum	muzeum	k1gNnSc1	muzeum
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
</s>
</p>
<p>
<s>
Do	do	k7c2	do
hornolužické	hornolužický	k2eAgFnSc2d1	Hornolužická
srbštiny	srbština	k1gFnSc2	srbština
knihu	kniha	k1gFnSc4	kniha
poprvé	poprvé	k6eAd1	poprvé
přeložil	přeložit	k5eAaPmAgMnS	přeložit
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
farář	farář	k1gMnSc1	farář
Filip	Filip	k1gMnSc1	Filip
Rězak	Rězak	k1gMnSc1	Rězak
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Naša	Naša	k?	Naša
wowka	wowka	k1gFnSc1	wowka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
ji	on	k3xPp3gFnSc4	on
přeložili	přeložit	k5eAaPmAgMnP	přeložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
Hanaróža	Hanaróža	k1gMnSc1	Hanaróža
Völkelowa	Völkelowa	k1gMnSc1	Völkelowa
a	a	k8xC	a
Pawoł	Pawoł	k1gMnSc1	Pawoł
Völkel	Völkel	k1gMnSc1	Völkel
(	(	kIx(	(
<g/>
Wowka	Wowka	k1gMnSc1	Wowka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
rumunštiny	rumunština	k1gFnSc2	rumunština
Jan	Jan	k1gMnSc1	Jan
Urban	Urban	k1gMnSc1	Urban
Jarník	jarník	k1gInSc1	jarník
<g/>
:	:	kIx,	:
Bunica	Bunica	k1gFnSc1	Bunica
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
katalánštiny	katalánština	k1gFnSc2	katalánština
a	a	k8xC	a
španělštiny	španělština	k1gFnSc2	španělština
Rudolf	Rudolf	k1gMnSc1	Rudolf
Jan	Jan	k1gMnSc1	Jan
Slabý	Slabý	k1gMnSc1	Slabý
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
à	à	k?	à
a	a	k8xC	a
La	la	k1gNnSc3	la
abuela	abuela	k1gFnSc1	abuela
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŠŮLA	Šůla	k1gMnSc1	Šůla
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Novotná-Čudová	Novotná-Čudová	k1gFnSc1	Novotná-Čudová
:	:	kIx,	:
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
:	:	kIx,	:
OFTIS	OFTIS	kA	OFTIS
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7405	[number]	k4	7405
<g/>
-	-	kIx~	-
<g/>
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NĚMCOVÁ	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
:	:	kIx,	:
obrazy	obraz	k1gInPc1	obraz
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Adolf	Adolf	k1gMnSc1	Adolf
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
grafická	grafický	k2eAgFnSc1d1	grafická
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠÍPEK	Šípek	k1gMnSc1	Šípek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
bělidle	bělidlo	k1gNnSc6	bělidlo
:	:	kIx,	:
zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
obrazech	obraz	k1gInPc6	obraz
dle	dle	k7c2	dle
Babičky	babička	k1gFnSc2	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
libreta	libreto	k1gNnSc2	libreto
ke	k	k7c3	k
Kovařovicově	Kovařovicově	k1gFnSc3	Kovařovicově
5	[number]	k4	5
<g/>
.	.	kIx.	.
opeře	opera	k1gFnSc3	opera
podle	podle	k7c2	podle
známé	známý	k2eAgFnSc2d1	známá
knihy	kniha	k1gFnSc2	kniha
B.	B.	kA	B.
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
libreta	libreto	k1gNnSc2	libreto
líčí	líčit	k5eAaImIp3nS	líčit
prostředí	prostředí	k1gNnSc4	prostředí
i	i	k8xC	i
zvyky	zvyk	k1gInPc4	zvyk
vesničanů	vesničan	k1gMnPc2	vesničan
a	a	k8xC	a
babiččinu	babiččin	k2eAgFnSc4d1	babiččina
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
moudrost	moudrost	k1gFnSc4	moudrost
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
prokáže	prokázat	k5eAaPmIp3nS	prokázat
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
lásky	láska	k1gFnSc2	láska
Kristly	Kristla	k1gFnSc2	Kristla
a	a	k8xC	a
Míly	Míla	k1gFnSc2	Míla
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
tajném	tajný	k2eAgNnSc6d1	tajné
soužení	soužení	k1gNnSc6	soužení
komtesy	komtesa	k1gFnSc2	komtesa
Hortensie	Hortensie	k1gFnSc2	Hortensie
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
zazněla	zaznět	k5eAaImAgFnS	zaznět
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
ND	ND	kA	ND
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1901	[number]	k4	1901
za	za	k7c4	za
řízení	řízení	k1gNnSc4	řízení
autora	autor	k1gMnSc2	autor
hudby	hudba	k1gFnSc2	hudba
K.	K.	kA	K.
Kovařovice	Kovařovice	k1gFnSc2	Kovařovice
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Babička	babička	k1gFnSc1	babička
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Babička	babička	k1gFnSc1	babička
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Babička	babička	k1gFnSc1	babička
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Babička	babička	k1gFnSc1	babička
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
Babičky	babička	k1gFnSc2	babička
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
Josef	Josef	k1gMnSc1	Josef
Vojvodík	Vojvodík	k1gMnSc1	Vojvodík
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Před	před	k7c7	před
150	[number]	k4	150
lety	léto	k1gNnPc7	léto
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
Babička	babička	k1gFnSc1	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
na	na	k7c6	na
webu	web	k1gInSc6	web
Muzea	muzeum	k1gNnSc2	muzeum
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Babičky	babička	k1gFnSc2	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
tamtéž	tamtéž	k6eAd1	tamtéž
</s>
</p>
<p>
<s>
Ediční	ediční	k2eAgInPc1d1	ediční
problémy	problém	k1gInPc1	problém
v	v	k7c6	v
Babičce	babička	k1gFnSc6	babička
–	–	k?	–
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
ročník	ročník	k1gInSc1	ročník
35	[number]	k4	35
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
–	–	k?	–
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
on-line	onin	k1gInSc5	on-lin
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Marie	Maria	k1gFnSc2	Maria
Rendlové	Rendlová	k1gFnSc2	Rendlová
<g/>
,	,	kIx,	,
sběratelky	sběratelka	k1gFnSc2	sběratelka
různých	různý	k2eAgFnPc2d1	různá
vydání	vydání	k1gNnPc2	vydání
Babičky	babička	k1gFnSc2	babička
</s>
</p>
<p>
<s>
Publikace	publikace	k1gFnSc1	publikace
o	o	k7c6	o
povídce	povídka	k1gFnSc6	povídka
Babička	babička	k1gFnSc1	babička
</s>
</p>
