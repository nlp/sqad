<s>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
Acquired	Acquired	k1gInSc1	Acquired
Immune	Immun	k1gInSc5	Immun
Deficiency	Deficiency	k1gInPc1	Deficiency
Syndrome	syndrom	k1gInSc5	syndrom
nebo	nebo	k8xC	nebo
též	též	k9	též
Acquired	Acquired	k1gInSc1	Acquired
Immunodeficiency	Immunodeficienca	k1gMnSc2	Immunodeficienca
Syndrome	syndrom	k1gInSc5	syndrom
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Syndrom	syndrom	k1gInSc1	syndrom
získaného	získaný	k2eAgNnSc2d1	získané
selhání	selhání	k1gNnSc2	selhání
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Syndrome	syndrom	k1gInSc5	syndrom
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
immunodéficience	immunodéficience	k1gFnSc1	immunodéficience
acquise	acquise	k1gFnSc1	acquise
<g/>
,	,	kIx,	,
SIDA	SIDA	kA	SIDA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
následkem	následkem	k7c2	následkem
poškození	poškození	k1gNnSc2	poškození
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
člověka	člověk	k1gMnSc2	člověk
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
