<p>
<s>
Vänern	Vänern	k1gNnSc1	Vänern
/	/	kIx~	/
<g/>
ˈ	ˈ	k?	ˈ
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Vänernské	Vänernský	k2eAgNnSc1d1	Vänernský
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
krajích	kraj	k1gInPc6	kraj
Västra	Västra	k1gFnSc1	Västra
Götaland	Götaland	k1gInSc1	Götaland
<g/>
,	,	kIx,	,
Värmland	Värmland	k1gInSc1	Värmland
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
svého	svůj	k3xOyFgInSc2	svůj
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
a	a	k8xC	a
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Topografie	topografie	k1gFnSc1	topografie
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
zvednutí	zvednutí	k1gNnSc2	zvednutí
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
po	po	k7c6	po
roztopení	roztopení	k1gNnSc6	roztopení
ledovce	ledovec	k1gInSc2	ledovec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
tektonickou	tektonický	k2eAgFnSc4d1	tektonická
propadlinu	propadlina	k1gFnSc4	propadlina
prohloubenou	prohloubený	k2eAgFnSc4d1	prohloubená
ledovci	ledovec	k1gInSc6	ledovec
a	a	k8xC	a
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
místech	místo	k1gNnPc6	místo
zlomy	zlom	k1gInPc1	zlom
a	a	k8xC	a
poklesy	pokles	k1gInPc1	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
5650	[number]	k4	5650
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
140	[number]	k4	140
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
je	být	k5eAaImIp3nS	být
hluboké	hluboký	k2eAgFnPc1d1	hluboká
27	[number]	k4	27
m	m	kA	m
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc2d1	maximální
hloubky	hloubka	k1gFnSc2	hloubka
106	[number]	k4	106
m.	m.	k?	m.
Jeho	jeho	k3xOp3gInSc4	jeho
objem	objem	k1gInSc4	objem
je	být	k5eAaImIp3nS	být
153	[number]	k4	153
km3	km3	k4	km3
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
největší	veliký	k2eAgFnSc7d3	veliký
zásobárnou	zásobárna	k1gFnSc7	zásobárna
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
41	[number]	k4	41
182	[number]	k4	182
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
44	[number]	k4	44
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
členité	členitý	k2eAgNnSc1d1	členité
<g/>
.	.	kIx.	.
</s>
<s>
Břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostrovy	ostrov	k1gInPc1	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Torsö	Torsö	k1gFnSc2	Torsö
(	(	kIx(	(
<g/>
62,03	[number]	k4	62,03
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kå	Kå	k1gMnSc1	Kå
(	(	kIx(	(
<g/>
56,78	[number]	k4	56,78
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Hammarö	Hammarö	k1gFnSc2	Hammarö
(	(	kIx(	(
<g/>
48,8	[number]	k4	48,8
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
jezera	jezero	k1gNnSc2	jezero
mezi	mezi	k7c7	mezi
ostrovem	ostrov	k1gInSc7	ostrov
Kå	Kå	k1gFnSc2	Kå
a	a	k8xC	a
Värmlandským	Värmlandský	k2eAgInSc7d1	Värmlandský
poloostrovem	poloostrov	k1gInSc7	poloostrov
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
Husön	Husön	k1gInSc1	Husön
nachází	nacházet	k5eAaImIp3nS	nacházet
klášter	klášter	k1gInSc4	klášter
Lurö	Lurö	k1gFnSc2	Lurö
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
ostrova	ostrov	k1gInSc2	ostrov
Djurö	Djurö	k1gFnSc2	Djurö
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
souostroví	souostroví	k1gNnSc6	souostroví
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Djurö	Djurö	k1gFnSc2	Djurö
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Kolísání	kolísání	k1gNnSc1	kolísání
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
je	být	k5eAaImIp3nS	být
nepatrné	nepatrný	k2eAgNnSc1d1	nepatrný
a	a	k8xC	a
plynulé	plynulý	k2eAgNnSc1d1	plynulé
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgInPc1d1	velký
vodní	vodní	k2eAgInPc1d1	vodní
stavy	stav	k1gInPc1	stav
na	na	k7c6	na
přítocích	přítok	k1gInPc6	přítok
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
nastávají	nastávat	k5eAaImIp3nP	nastávat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
regulace	regulace	k1gFnSc2	regulace
přehradní	přehradní	k2eAgInSc4d1	přehradní
hrází	hráz	k1gFnSc7	hráz
na	na	k7c6	na
odtoku	odtok	k1gInSc6	odtok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
ústí	ústit	k5eAaImIp3nS	ústit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
je	být	k5eAaImIp3nS	být
Klarälven	Klarälven	k2eAgInSc1d1	Klarälven
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
vlévá	vlévat	k5eAaImIp3nS	vlévat
v	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
<g/>
.	.	kIx.	.
</s>
<s>
Odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
Göta	Göta	k1gFnSc1	Göta
Älv	Älv	k1gFnSc4	Älv
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Kattegatu	Kattegat	k1gInSc2	Kattegat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vänern	Vänern	k1gInSc1	Vänern
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
vedoucí	vedoucí	k1gFnSc2	vedoucí
napříč	napříč	k7c7	napříč
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Göteborgem	Göteborg	k1gInSc7	Göteborg
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
kanálem	kanál	k1gInSc7	kanál
Trollhätte	Trollhätt	k1gInSc5	Trollhätt
a	a	k8xC	a
se	s	k7c7	s
Stockholmem	Stockholm	k1gInSc7	Stockholm
kanálem	kanál	k1gInSc7	kanál
Göta	Götum	k1gNnSc2	Götum
přes	přes	k7c4	přes
jezero	jezero	k1gNnSc4	jezero
Vättern	Vätterna	k1gFnPc2	Vätterna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
až	až	k6eAd1	až
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
nestálý	stálý	k2eNgInSc1d1	nestálý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
bylo	být	k5eAaImAgNnS	být
jezero	jezero	k1gNnSc1	jezero
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
poznat	poznat	k5eAaPmF	poznat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
flóře	flóra	k1gFnSc6	flóra
a	a	k8xC	a
fauně	fauna	k1gFnSc6	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Výskytují	Výskytovat	k5eAaImIp3nP	Výskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mořští	mořský	k2eAgMnPc1d1	mořský
živočichové	živočich	k1gMnPc1	živočich
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
34	[number]	k4	34
rozličných	rozličný	k2eAgInPc2d1	rozličný
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Nejhojnější	hojný	k2eAgFnSc1d3	nejhojnější
je	být	k5eAaImIp3nS	být
koruška	koruška	k1gFnSc1	koruška
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
až	až	k9	až
2600	[number]	k4	2600
exemplářů	exemplář	k1gInPc2	exemplář
na	na	k7c4	na
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
síh	síh	k1gMnSc1	síh
malý	malý	k1gMnSc1	malý
s	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
200	[number]	k4	200
až	až	k9	až
300	[number]	k4	300
exemplářů	exemplář	k1gInPc2	exemplář
na	na	k7c6	na
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Nalzezneme	Nalzeznout	k5eAaPmIp1nP	Nalzeznout
zde	zde	k6eAd1	zde
většinu	většina	k1gFnSc4	většina
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
druhů	druh	k1gInPc2	druh
žijících	žijící	k2eAgInPc2d1	žijící
ve	v	k7c6	v
švédských	švédský	k2eAgFnPc6d1	švédská
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
síh	síh	k1gMnSc1	síh
severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
síh	síh	k1gMnSc1	síh
peleď	peledit	k5eAaPmRp2nS	peledit
<g/>
,	,	kIx,	,
síh	síh	k1gMnSc1	síh
(	(	kIx(	(
<g/>
nilssoni	nilssoň	k1gFnSc6	nilssoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pstruh	pstruh	k1gMnSc1	pstruh
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
cejn	cejn	k1gMnSc1	cejn
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
cejn	cejn	k1gMnSc1	cejn
siný	siný	k2eAgMnSc1d1	siný
<g/>
,	,	kIx,	,
cejnek	cejnek	k1gMnSc1	cejnek
malý	malý	k1gMnSc1	malý
<g/>
,	,	kIx,	,
jelec	jelec	k1gMnSc1	jelec
tloušť	tloušť	k1gMnSc1	tloušť
<g/>
,	,	kIx,	,
jelec	jelec	k1gMnSc1	jelec
jesen	jesen	k1gMnSc1	jesen
<g/>
,	,	kIx,	,
bolen	bolen	k2eAgMnSc1d1	bolen
dravý	dravý	k2eAgMnSc1d1	dravý
<g/>
,	,	kIx,	,
ouklej	ouklej	k1gFnSc1	ouklej
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
lín	lín	k1gMnSc1	lín
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
plotice	plotice	k1gFnSc1	plotice
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
mník	mník	k1gMnSc1	mník
jednovousý	jednovousý	k2eAgMnSc1d1	jednovousý
<g/>
,	,	kIx,	,
karas	karas	k1gMnSc1	karas
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
candát	candát	k1gMnSc1	candát
obecný	obecný	k2eAgMnSc1d1	obecný
a	a	k8xC	a
okoun	okoun	k1gMnSc1	okoun
říční	říční	k2eAgMnSc1d1	říční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
rozvinuté	rozvinutý	k2eAgNnSc1d1	rozvinuté
rybářství	rybářství	k1gNnSc1	rybářství
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
výlov	výlov	k1gInSc1	výlov
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
činil	činit	k5eAaImAgInS	činit
165	[number]	k4	165
tun	tuna	k1gFnPc2	tuna
síhů	síh	k1gMnPc2	síh
malých	malý	k2eAgMnPc2d1	malý
<g/>
,	,	kIx,	,
100	[number]	k4	100
tun	tuna	k1gFnPc2	tuna
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
síhů	síh	k1gMnPc2	síh
a	a	k8xC	a
25	[number]	k4	25
tun	tuna	k1gFnPc2	tuna
úhořů	úhoř	k1gMnPc2	úhoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
sladkovodní	sladkovodní	k2eAgMnSc1d1	sladkovodní
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
9	[number]	k4	9
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
oddělil	oddělit	k5eAaPmAgMnS	oddělit
od	od	k7c2	od
lososů	losos	k1gMnPc2	losos
z	z	k7c2	z
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ryby	ryba	k1gFnPc1	ryba
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
20	[number]	k4	20
kg	kg	kA	kg
a	a	k8xC	a
kromě	kromě	k7c2	kromě
Vänern	Vänerna	k1gFnPc2	Vänerna
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ještě	ještě	k9	ještě
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
Saimaa	Saima	k1gInSc2	Saima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
kormoráni	kormorán	k1gMnPc1	kormorán
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
zase	zase	k9	zase
vracejí	vracet	k5eAaImIp3nP	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
mořští	mořský	k2eAgMnPc1d1	mořský
orli	orel	k1gMnPc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rybáků	rybák	k1gMnPc2	rybák
a	a	k8xC	a
racků	racek	k1gMnPc2	racek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
potáplice	potáplice	k1gFnPc4	potáplice
severní	severní	k2eAgFnPc4d1	severní
<g/>
,	,	kIx,	,
kameňáčky	kameňáček	k1gInPc7	kameňáček
pestré	pestrý	k2eAgFnSc2d1	pestrá
a	a	k8xC	a
rybáky	rybák	k1gMnPc4	rybák
velkozobé	velkozobý	k2eAgFnSc2d1	velkozobý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
místní	místní	k2eAgFnSc1d1	místní
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osídlení	osídlení	k1gNnSc3	osídlení
pobřeží	pobřeží	k1gNnSc3	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
patří	patřit	k5eAaImIp3nS	patřit
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
podporovala	podporovat	k5eAaImAgFnS	podporovat
v	v	k7c6	v
rybolovu	rybolov	k1gInSc6	rybolov
a	a	k8xC	a
distribuci	distribuce	k1gFnSc6	distribuce
ulovených	ulovený	k2eAgFnPc2d1	ulovená
ryb	ryba	k1gFnPc2	ryba
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ta	ten	k3xDgNnPc4	ten
největší	veliký	k2eAgMnSc1d3	veliký
patří	patřit	k5eAaImIp3nS	patřit
Karlstad	Karlstad	k1gInSc1	Karlstad
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kristinehamn	Kristinehamn	k1gMnSc1	Kristinehamn
(	(	kIx(	(
<g/>
1642	[number]	k4	1642
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Mariestad	Mariestad	k1gInSc1	Mariestad
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lidköping	Lidköping	k1gInSc1	Lidköping
(	(	kIx(	(
<g/>
1446	[number]	k4	1446
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Vänersborg	Vänersborg	k1gMnSc1	Vänersborg
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Å	Å	k?	Å
(	(	kIx(	(
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Säffle	Säffla	k1gFnSc6	Säffla
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
В	В	k?	В
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seppälä	Seppälä	k?	Seppälä
<g/>
,	,	kIx,	,
Matti	Matť	k1gFnPc1	Matť
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Physical	Physical	k1gFnSc2	Physical
Geography	Geographa	k1gFnSc2	Geographa
of	of	k?	of
Fennoscandia	Fennoscandium	k1gNnSc2	Fennoscandium
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
p.	p.	k?	p.
145	[number]	k4	145
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0-19-924590-1	[number]	k4	978-0-19-924590-1
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vättern	Vättern	k1gMnSc1	Vättern
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vänern	Vänerna	k1gFnPc2	Vänerna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.vastsverige.com/bat-kust-och-hav/Vanern/	[url]	k?	http://www.vastsverige.com/bat-kust-och-hav/Vanern/
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Jezero	jezero	k1gNnSc1	jezero
Vänern	Vänerna	k1gFnPc2	Vänerna
<g/>
:	:	kIx,	:
Průvodce	průvodce	k1gMnSc1	průvodce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
jezera	jezero	k1gNnSc2	jezero
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lake	Lake	k1gFnSc1	Lake
Vänern	Vänerna	k1gFnPc2	Vänerna
guide	guide	k6eAd1	guide
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Väner	Väner	k1gInSc1	Väner
museum	museum	k1gNnSc1	museum
i	i	k8xC	i
Lidköping	Lidköping	k1gInSc1	Lidköping
</s>
</p>
