<s>
Orogeneze	orogeneze	k1gFnSc1	orogeneze
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xS	jako
horotvorba	horotvorba	k1gFnSc1	horotvorba
či	či	k8xC	či
horotvoření	horotvoření	k1gNnSc1	horotvoření
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
oros	orosit	k5eAaPmRp2nS	orosit
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
a	a	k8xC	a
genesis	genesis	k1gFnSc1	genesis
"	"	kIx"	"
<g/>
vznik	vznik	k1gInSc1	vznik
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horotvorný	horotvorný	k2eAgInSc1d1	horotvorný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
pásemných	pásemný	k2eAgFnPc2d1	pásemná
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
vznikajících	vznikající	k2eAgInPc2d1	vznikající
většinou	většinou	k6eAd1	většinou
vlivem	vlivem	k7c2	vlivem
procesů	proces	k1gInPc2	proces
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
