<s>
José	Josý	k2eAgInPc1d1	Josý
Echegaray	Echegaray	k1gInPc1	Echegaray
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
básníkem	básník	k1gMnSc7	básník
Frédéricem	Frédéric	k1gMnSc7	Frédéric
Mistralem	mistral	k1gInSc7	mistral
<g/>
.	.	kIx.	.
</s>
