<s>
Watchmen	Watchmen	k1gInSc1	Watchmen
-	-	kIx~	-
Strážci	strážce	k1gMnPc1	strážce
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Watchmen	Watchmen	k1gInSc1	Watchmen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
komiksová	komiksový	k2eAgFnSc1d1	komiksová
minisérie	minisérie	k1gFnSc1	minisérie
o	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
sešitech	sešit	k1gInPc6	sešit
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ji	on	k3xPp3gFnSc4	on
scenárista	scenárista	k1gMnSc1	scenárista
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
Dave	Dav	k1gInSc5	Dav
Gibbons	Gibbons	k1gInSc1	Gibbons
a	a	k8xC	a
kolorista	kolorista	k1gMnSc1	kolorista
John	John	k1gMnSc1	John
Higgins	Higgins	k1gInSc4	Higgins
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
vyšla	vyjít	k5eAaPmAgFnS	vyjít
u	u	k7c2	u
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1986	[number]	k4	1986
a	a	k8xC	a
1987	[number]	k4	1987
a	a	k8xC	a
následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Strážci	strážce	k1gMnPc1	strážce
původně	původně	k6eAd1	původně
měli	mít	k5eAaImAgMnP	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
jako	jako	k9	jako
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
superhrdinové	superhrdinový	k2eAgFnPc4d1	superhrdinový
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
Charlton	Charlton	k1gInSc1	Charlton
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ale	ale	k8xC	ale
Moore	Moor	k1gInSc5	Moor
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
příběhů	příběh	k1gInPc2	příběh
nepočítal	počítat	k5eNaImAgMnS	počítat
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
Dick	Dick	k1gMnSc1	Dick
Giordano	Giordana	k1gFnSc5	Giordana
ho	on	k3xPp3gMnSc4	on
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
postavy	postava	k1gFnPc4	postava
vlastní	vlastní	k2eAgFnPc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zápletku	zápletka	k1gFnSc4	zápletka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ohrožení	ohrožení	k1gNnSc2	ohrožení
možnou	možný	k2eAgFnSc7d1	možná
jadernou	jaderný	k2eAgFnSc7d1	jaderná
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
současně	současně	k6eAd1	současně
dekonstruoval	dekonstruovat	k5eAaPmAgInS	dekonstruovat
superhrdinský	superhrdinský	k2eAgInSc1d1	superhrdinský
mýtus	mýtus	k1gInSc1	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
Strážců	strážce	k1gMnPc2	strážce
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
alternativní	alternativní	k2eAgFnSc2d1	alternativní
historie	historie	k1gFnSc2	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
superhrdinové	superhrdinový	k2eAgMnPc4d1	superhrdinový
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pomohli	pomoct	k5eAaPmAgMnP	pomoct
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
vyhrát	vyhrát	k5eAaPmF	vyhrát
válku	válka	k1gFnSc4	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
nukleární	nukleární	k2eAgFnSc2d1	nukleární
války	válka	k1gFnSc2	válka
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
maskovaní	maskovaný	k2eAgMnPc1d1	maskovaný
strážci	strážce	k1gMnPc1	strážce
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
postaveni	postavit	k5eAaPmNgMnP	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
na	na	k7c6	na
odpočinku	odpočinek	k1gInSc6	odpočinek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pracují	pracovat	k5eAaImIp3nP	pracovat
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zápletka	zápletka	k1gFnSc1	zápletka
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
příběhy	příběh	k1gInPc4	příběh
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
vraždy	vražda	k1gFnSc2	vražda
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
agent	agent	k1gMnSc1	agent
vlády	vláda	k1gFnSc2	vláda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
devítipolíčkovým	devítipolíčkův	k2eAgNnSc7d1	devítipolíčkův
rozložením	rozložení	k1gNnSc7	rozložení
a	a	k8xC	a
využil	využít	k5eAaPmAgMnS	využít
několik	několik	k4yIc4	několik
opakujících	opakující	k2eAgInPc2d1	opakující
se	se	k3xPyFc4	se
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zakrvácený	zakrvácený	k2eAgInSc1d1	zakrvácený
smajlík	smajlík	k?	smajlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
části	část	k1gFnSc6	část
kromě	kromě	k7c2	kromě
poslední	poslední	k2eAgFnSc2d1	poslední
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
fiktivní	fiktivní	k2eAgInPc1d1	fiktivní
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
příběh	příběh	k1gInSc1	příběh
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
protkán	protkán	k2eAgInSc1d1	protkán
dalším	další	k2eAgInSc7d1	další
příběhem	příběh	k1gInSc7	příběh
<g/>
,	,	kIx,	,
fiktivním	fiktivní	k2eAgInSc7d1	fiktivní
pirátským	pirátský	k2eAgInSc7d1	pirátský
komiksem	komiks	k1gInSc7	komiks
Příběhy	příběh	k1gInPc4	příběh
Černé	Černá	k1gFnSc2	Černá
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
čte	číst	k5eAaImIp3nS	číst
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Strážcům	strážce	k1gMnPc3	strážce
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
ohlasu	ohlas	k1gInSc2	ohlas
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
ho	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zásadní	zásadní	k2eAgNnSc4d1	zásadní
dílo	dílo	k1gNnSc4	dílo
komiksu	komiks	k1gInSc2	komiks
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
pokusech	pokus	k1gInPc6	pokus
se	se	k3xPyFc4	se
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
dostal	dostat	k5eAaPmAgInS	dostat
film	film	k1gInSc1	film
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
komiksu	komiks	k1gInSc2	komiks
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Zackem	Zacek	k1gMnSc7	Zacek
Snyderem	Snyder	k1gMnSc7	Snyder
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
získalo	získat	k5eAaPmAgNnS	získat
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
od	od	k7c2	od
Charlton	Charlton	k1gInSc1	Charlton
Comics	comics	k1gInSc1	comics
několik	několik	k4yIc1	několik
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
o	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
by	by	kYmCp3nS	by
oživil	oživit	k5eAaPmAgMnS	oživit
některé	některý	k3yIgMnPc4	některý
nevyužité	využitý	k2eNgMnPc4d1	nevyužitý
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
to	ten	k3xDgNnSc1	ten
udělal	udělat	k5eAaPmAgInS	udělat
s	s	k7c7	s
Miraclemanem	Miracleman	k1gInSc7	Miracleman
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
vybral	vybrat	k5eAaPmAgMnS	vybrat
Mighty	Might	k1gInPc4	Might
Crusaders	Crusadersa	k1gFnPc2	Crusadersa
od	od	k7c2	od
MLJ	MLJ	kA	MLJ
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
zápletku	zápletka	k1gFnSc4	zápletka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
začínala	začínat	k5eAaImAgFnS	začínat
objevením	objevení	k1gNnSc7	objevení
těla	tělo	k1gNnSc2	tělo
The	The	k1gMnSc2	The
Shielda	Shield	k1gMnSc2	Shield
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
postavy	postava	k1gFnPc4	postava
využije	využít	k5eAaPmIp3nS	využít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
čtenáři	čtenář	k1gMnPc1	čtenář
poznají	poznat	k5eAaPmIp3nP	poznat
a	a	k8xC	a
"	"	kIx"	"
<g/>
budou	být	k5eAaImBp3nP	být
šokováni	šokovat	k5eAaBmNgMnP	šokovat
a	a	k8xC	a
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
jejich	jejich	k3xOp3gFnSc7	jejich
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
povahou	povaha	k1gFnSc7	povaha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
použil	použít	k5eAaPmAgInS	použít
tuto	tento	k3xDgFnSc4	tento
premisu	premisa	k1gFnSc4	premisa
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
postavy	postav	k1gInPc1	postav
od	od	k7c2	od
Charlton	Charlton	k1gInSc4	Charlton
<g/>
,	,	kIx,	,
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gMnSc4	on
Who	Who	k1gMnSc1	Who
Killed	Killed	k1gMnSc1	Killed
the	the	k?	the
Peacemaker	Peacemaker	k1gMnSc1	Peacemaker
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
předal	předat	k5eAaPmAgInS	předat
Dickovi	Dicek	k1gMnSc3	Dicek
Giordanovi	Giordan	k1gMnSc3	Giordan
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
návrh	návrh	k1gInSc1	návrh
líbil	líbit	k5eAaImAgInS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
myšlence	myšlenka	k1gFnSc3	myšlenka
použit	použit	k2eAgInSc4d1	použit
postavy	postav	k1gInPc4	postav
od	od	k7c2	od
Charlton	Charlton	k1gInSc1	Charlton
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
DC	DC	kA	DC
si	se	k3xPyFc3	se
uvědomilo	uvědomit	k5eAaPmAgNnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyhle	tenhle	k3xDgInPc4	tenhle
draze	draha	k1gFnSc6	draha
koupené	koupený	k2eAgFnPc4d1	koupená
postavy	postava	k1gFnPc4	postava
skončí	skončit	k5eAaPmIp3nS	skončit
buď	buď	k8xC	buď
jako	jako	k9	jako
mrtvoly	mrtvola	k1gFnPc4	mrtvola
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
blázni	blázen	k1gMnPc1	blázen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
Giordano	Giordana	k1gFnSc5	Giordana
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
Moorea	Mooreus	k1gMnSc4	Mooreus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
a	a	k8xC	a
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
originální	originální	k2eAgFnPc4d1	originální
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
si	se	k3xPyFc3	se
nejdříve	dříve	k6eAd3	dříve
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgFnPc1d1	nová
postavy	postava	k1gFnPc1	postava
nebudou	být	k5eNaImBp3nP	být
mít	mít	k5eAaImF	mít
správný	správný	k2eAgInSc4d1	správný
emocionální	emocionální	k2eAgInSc4d1	emocionální
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
vytvořím	vytvořit	k5eAaPmIp1nS	vytvořit
nové	nový	k2eAgFnPc4d1	nová
postavy	postava	k1gFnPc4	postava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lidem	člověk	k1gMnPc3	člověk
připadaly	připadat	k5eAaPmAgInP	připadat
povědomé	povědomý	k2eAgInPc4d1	povědomý
<g/>
,	,	kIx,	,
aby	aby	k9	aby
jejich	jejich	k3xOp3gFnPc4	jejich
určité	určitý	k2eAgFnPc4d1	určitá
superhrdinské	superhrdinský	k2eAgFnPc4d1	superhrdinská
stránky	stránka	k1gFnPc4	stránka
lidé	člověk	k1gMnPc1	člověk
znali	znát	k5eAaImAgMnP	znát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dave	Dav	k1gInSc5	Dav
Gibbons	Gibbons	k1gInSc4	Gibbons
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
s	s	k7c7	s
Mooorem	Mooor	k1gMnSc7	Mooor
pracoval	pracovat	k5eAaImAgMnS	pracovat
již	již	k6eAd1	již
na	na	k7c6	na
předešlých	předešlý	k2eAgInPc6d1	předešlý
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
Moore	Moor	k1gInSc5	Moor
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
minisérii	minisérie	k1gFnSc6	minisérie
<g/>
.	.	kIx.	.
</s>
<s>
Sdělil	sdělit	k5eAaPmAgMnS	sdělit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
podílet	podílet	k5eAaImF	podílet
<g/>
,	,	kIx,	,
a	a	k8xC	a
Moore	Moor	k1gInSc5	Moor
mu	on	k3xPp3gMnSc3	on
poslal	poslat	k5eAaPmAgMnS	poslat
nástin	nástin	k1gInSc4	nástin
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k1gInSc1	Gibbons
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivedl	přivést	k5eAaPmAgMnS	přivést
Johna	John	k1gMnSc4	John
Higginse	Higgins	k1gMnSc4	Higgins
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbil	líbit	k5eAaImAgInS	líbit
jeho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
<g/>
"	"	kIx"	"
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Bydleli	bydlet	k5eAaImAgMnP	bydlet
blízko	blízko	k7c2	blízko
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
komunikace	komunikace	k1gFnSc1	komunikace
byla	být	k5eAaImAgFnS	být
snazší	snadný	k2eAgFnSc1d2	snazší
<g/>
.	.	kIx.	.
</s>
<s>
Len	Lena	k1gFnPc2	Lena
Wein	Wein	k1gMnSc1	Wein
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
jako	jako	k9	jako
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Giordano	Giordana	k1gFnSc5	Giordana
pouze	pouze	k6eAd1	pouze
dohlížel	dohlížet	k5eAaImAgInS	dohlížet
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Wein	Wein	k1gMnSc1	Wein
i	i	k9	i
Giordano	Giordana	k1gFnSc5	Giordana
se	se	k3xPyFc4	se
drželi	držet	k5eAaImAgMnP	držet
zpátky	zpátky	k6eAd1	zpátky
a	a	k8xC	a
"	"	kIx"	"
<g/>
nepřekáželi	překážet	k5eNaImAgMnP	překážet
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dostal	dostat	k5eAaPmAgInS	dostat
projekt	projekt	k1gInSc1	projekt
zelenou	zelená	k1gFnSc4	zelená
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
Moore	Moor	k1gInSc5	Moor
a	a	k8xC	a
Gibbons	Gibbons	k1gInSc4	Gibbons
vymýšlet	vymýšlet	k5eAaImF	vymýšlet
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
dotvářet	dotvářet	k5eAaImF	dotvářet
detaily	detail	k1gInPc4	detail
zápletky	zápletka	k1gFnSc2	zápletka
a	a	k8xC	a
probírat	probírat	k5eAaImF	probírat
různé	různý	k2eAgInPc4d1	různý
vlivy	vliv	k1gInPc4	vliv
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
je	on	k3xPp3gMnPc4	on
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
supermanská	supermanský	k2eAgFnSc1d1	supermanská
parodie	parodie	k1gFnSc1	parodie
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
Mad	Mad	k1gFnSc2	Mad
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Superduperman	Superduperman	k1gMnSc1	Superduperman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
Supermana	superman	k1gMnSc4	superman
obrátit	obrátit	k5eAaPmF	obrátit
o	o	k7c4	o
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
komedii	komedie	k1gFnSc4	komedie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Moore	Moor	k1gMnSc5	Moor
s	s	k7c7	s
Gibbonsem	Gibbons	k1gMnSc7	Gibbons
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
převést	převést	k5eAaPmF	převést
známé	známý	k2eAgFnPc4d1	známá
staromódní	staromódní	k2eAgFnPc4d1	staromódní
superhrdiny	superhrdina	k1gFnPc4	superhrdina
do	do	k7c2	do
úplně	úplně	k6eAd1	úplně
nového	nový	k2eAgInSc2d1	nový
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
superhrdinského	superhrdinský	k2eAgMnSc2d1	superhrdinský
Moby	Moba	k1gMnSc2	Moba
Dicka	Dicko	k1gNnSc2	Dicko
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
hloubku	hloubka	k1gFnSc4	hloubka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
povahu	povaha	k1gFnSc4	povaha
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc4	jejich
vzhled	vzhled	k1gInSc4	vzhled
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
Gibbonsovi	Gibbons	k1gMnSc6	Gibbons
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
postavy	postava	k1gFnPc4	postava
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
strávil	strávit	k5eAaPmAgMnS	strávit
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
děláním	dělání	k1gNnSc7	dělání
náčrtků	náčrtek	k1gInPc2	náčrtek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k1gInSc1	Gibbons
postavy	postava	k1gFnSc2	postava
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
kreslily	kreslit	k5eAaImAgFnP	kreslit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
byl	být	k5eAaImAgMnS	být
Rorschach	Rorschach	k1gMnSc1	Rorschach
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
musíte	muset	k5eAaImIp2nP	muset
namalovat	namalovat	k5eAaPmF	namalovat
klobouk	klobouk	k1gInSc4	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
umíte	umět	k5eAaImIp2nP	umět
namalovat	namalovat	k5eAaPmF	namalovat
klobouk	klobouk	k1gInSc4	klobouk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jste	být	k5eAaImIp2nP	být
namalovali	namalovat	k5eAaPmAgMnP	namalovat
Rorschacha	Rorschach	k1gMnSc4	Rorschach
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přimalujete	přimalovat	k5eAaPmIp2nP	přimalovat
nějaký	nějaký	k3yIgInSc4	nějaký
tvar	tvar	k1gInSc4	tvar
jako	jako	k8xS	jako
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dáte	dát	k5eAaPmIp2nP	dát
pár	pár	k4xCyI	pár
černých	černé	k1gNnPc2	černé
bublin	bublina	k1gFnPc2	bublina
a	a	k8xC	a
jste	být	k5eAaImIp2nP	být
hotovi	hotov	k2eAgMnPc1d1	hotov
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
Moore	Moor	k1gInSc5	Moor
psal	psát	k5eAaImAgInS	psát
scénář	scénář	k1gInSc1	scénář
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
sešit	sešit	k1gInSc4	sešit
<g/>
,	,	kIx,	,
uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
materiál	materiál	k1gInSc4	materiál
jenom	jenom	k9	jenom
na	na	k7c4	na
šest	šest	k4xCc4	šest
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nasmlouváno	nasmlouvat	k5eAaPmNgNnS	nasmlouvat
jich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
.	.	kIx.	.
</s>
<s>
Vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyprávění	vyprávění	k1gNnSc1	vyprávění
hlavní	hlavní	k2eAgFnSc2d1	hlavní
příběhové	příběhový	k2eAgFnSc2d1	příběhová
linie	linie	k1gFnSc2	linie
přerušoval	přerušovat	k5eAaImAgInS	přerušovat
vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c6	o
původu	původ	k1gInSc6	původ
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
napsal	napsat	k5eAaBmAgMnS	napsat
Gibbonsovi	Gibbons	k1gMnSc3	Gibbons
velmi	velmi	k6eAd1	velmi
podrobný	podrobný	k2eAgInSc4d1	podrobný
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
si	se	k3xPyFc3	se
vybavuje	vybavovat	k5eAaImIp3nS	vybavovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Scénář	scénář	k1gInSc1	scénář
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
sešit	sešit	k1gInSc4	sešit
měl	mít	k5eAaImAgMnS	mít
tuším	tušit	k5eAaImIp1nS	tušit
101	[number]	k4	101
stran	strana	k1gFnPc2	strana
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
mezer	mezera	k1gFnPc2	mezera
mezi	mezi	k7c7	mezi
popisy	popis	k1gInPc7	popis
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
políček	políček	k1gInSc4	políček
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
scénář	scénář	k1gInSc1	scénář
obdržel	obdržet	k5eAaPmAgInS	obdržet
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
si	se	k3xPyFc3	se
očíslovat	očíslovat	k5eAaPmF	očíslovat
každou	každý	k3xTgFnSc4	každý
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
to	ten	k3xDgNnSc4	ten
upustil	upustit	k5eAaPmAgMnS	upustit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
by	by	kYmCp3nS	by
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
než	než	k8xS	než
bych	by	kYmCp1nS	by
to	ten	k3xDgNnSc1	ten
dal	dát	k5eAaPmAgInS	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Chce	chtít	k5eAaImIp3nS	chtít
to	ten	k3xDgNnSc1	ten
trochu	trochu	k6eAd1	trochu
organizování	organizování	k1gNnSc3	organizování
<g/>
,	,	kIx,	,
než	než	k8xS	než
můžete	moct	k5eAaImIp2nP	moct
začít	začít	k5eAaPmF	začít
kreslit	kreslit	k5eAaImF	kreslit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
podotkl	podotknout	k5eAaPmAgMnS	podotknout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
velmi	velmi	k6eAd1	velmi
podrobný	podrobný	k2eAgInSc4d1	podrobný
scénář	scénář	k1gInSc4	scénář
některé	některý	k3yIgFnSc2	některý
Mooreovy	Mooreův	k2eAgFnSc2d1	Mooreova
popisky	popiska	k1gFnSc2	popiska
končily	končit	k5eAaImAgInP	končit
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
ti	ten	k3xDgMnPc1	ten
to	ten	k3xDgNnSc4	ten
nebude	být	k5eNaImBp3nS	být
zapadat	zapadat	k5eAaImF	zapadat
<g/>
,	,	kIx,	,
udělej	udělat	k5eAaPmRp2nS	udělat
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
fungovalo	fungovat	k5eAaImAgNnS	fungovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gibbons	Gibbons	k1gInSc1	Gibbons
se	se	k3xPyFc4	se
přesto	přesto	k6eAd1	přesto
držel	držet	k5eAaImAgInS	držet
Mooreových	Mooreův	k2eAgInPc2d1	Mooreův
pokynů	pokyn	k1gInPc2	pokyn
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k1gInSc1	Gibbons
měl	mít	k5eAaImAgInS	mít
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
vizuálního	vizuální	k2eAgNnSc2d1	vizuální
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
Strážců	strážce	k1gMnPc2	strážce
velkou	velký	k2eAgFnSc4d1	velká
volnost	volnost	k1gFnSc4	volnost
a	a	k8xC	a
často	často	k6eAd1	často
přidával	přidávat	k5eAaImAgMnS	přidávat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
drobnosti	drobnost	k1gFnSc2	drobnost
<g/>
,	,	kIx,	,
kterých	který	k3yQgNnPc6	který
si	se	k3xPyFc3	se
Moore	Moor	k1gInSc5	Moor
všiml	všimnout	k5eAaPmAgMnS	všimnout
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
také	také	k9	také
občas	občas	k6eAd1	občas
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
spřízněného	spřízněný	k2eAgMnSc4d1	spřízněný
autora	autor	k1gMnSc4	autor
komiksů	komiks	k1gInPc2	komiks
Neila	Neil	k1gMnSc4	Neil
Gaimana	Gaiman	k1gMnSc4	Gaiman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
poradil	poradit	k5eAaPmAgInS	poradit
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
vybrali	vybrat	k5eAaPmAgMnP	vybrat
citáty	citát	k1gInPc4	citát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
sešitech	sešit	k1gInPc6	sešit
objevují	objevovat	k5eAaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
očekáváním	očekávání	k1gNnPc3	očekávání
Moore	Moor	k1gInSc5	Moor
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1986	[number]	k4	1986
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
objeví	objevit	k5eAaPmIp3nP	objevit
prodlevy	prodleva	k1gFnPc1	prodleva
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
byl	být	k5eAaImAgInS	být
sešit	sešit	k1gInSc1	sešit
číslo	číslo	k1gNnSc1	číslo
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
teprve	teprve	k6eAd1	teprve
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
deváté	devátý	k4xOgNnSc4	devátý
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
scénáře	scénář	k1gInPc4	scénář
dostával	dostávat	k5eAaImAgInS	dostávat
po	po	k7c6	po
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
zpomalilo	zpomalit	k5eAaPmAgNnS	zpomalit
kolem	kolem	k7c2	kolem
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
blížily	blížit	k5eAaImAgInP	blížit
termíny	termín	k1gInPc1	termín
<g/>
,	,	kIx,	,
Moore	Moor	k1gInSc5	Moor
si	se	k3xPyFc3	se
najal	najmout	k5eAaPmAgInS	najmout
taxikáře	taxikář	k1gMnSc4	taxikář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jezdil	jezdit	k5eAaImAgMnS	jezdit
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
a	a	k8xC	a
vozil	vozit	k5eAaImAgMnS	vozit
scénáře	scénář	k1gInSc2	scénář
Gibbonsovi	Gibbons	k1gMnSc3	Gibbons
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Gibbons	Gibbons	k1gInSc1	Gibbons
dokonce	dokonce	k9	dokonce
nechal	nechat	k5eAaPmAgInS	nechat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
kreslit	kreslit	k5eAaImF	kreslit
mřížky	mřížka	k1gFnSc2	mřížka
políček	políček	k1gInSc4	políček
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ušetřil	ušetřit	k5eAaPmAgInS	ušetřit
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
také	také	k9	také
musel	muset	k5eAaImAgMnS	muset
zkrátit	zkrátit	k5eAaPmF	zkrátit
část	část	k1gFnSc4	část
Veidtova	Veidtův	k2eAgInSc2d1	Veidtův
proslovu	proslov	k1gInSc2	proslov
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
pronáší	pronášet	k5eAaImIp3nS	pronášet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
odvrací	odvracet	k5eAaImIp3nS	odvracet
Rorschachovy	Rorschachův	k2eAgInPc4d1	Rorschachův
útoky	útok	k1gInPc4	útok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gInSc4	on
Gibbons	Gibbons	k1gInSc4	Gibbons
nedokázal	dokázat	k5eNaPmAgInS	dokázat
vtěsnat	vtěsnat	k5eAaPmF	vtěsnat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
projekt	projekt	k1gInSc1	projekt
blížil	blížit	k5eAaImAgInS	blížit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
,	,	kIx,	,
Moore	Moor	k1gInSc5	Moor
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zápletka	zápletka	k1gFnSc1	zápletka
trochu	trochu	k6eAd1	trochu
podobá	podobat	k5eAaImIp3nS	podobat
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
epizod	epizoda	k1gFnPc2	epizoda
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Krajní	krajní	k2eAgFnSc2d1	krajní
meze	mez	k1gFnSc2	mez
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
a	a	k8xC	a
Wein	Weina	k1gFnPc2	Weina
měli	mít	k5eAaImAgMnP	mít
spor	spor	k1gInSc4	spor
ohledně	ohledně	k7c2	ohledně
změny	změna	k1gFnSc2	změna
závěru	závěr	k1gInSc2	závěr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Moore	Moor	k1gInSc5	Moor
si	se	k3xPyFc3	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
svou	svůj	k3xOyFgFnSc7	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
epizodu	epizoda	k1gFnSc4	epizoda
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
sešitu	sešit	k1gInSc6	sešit
<g/>
.	.	kIx.	.
</s>
<s>
Strážci	strážce	k1gMnPc1	strážce
jsou	být	k5eAaImIp3nP	být
zasazeni	zasadit	k5eAaPmNgMnP	zasadit
do	do	k7c2	do
alternativní	alternativní	k2eAgFnSc2d1	alternativní
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
odrážejí	odrážet	k5eAaImIp3nP	odrážet
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
superhrdinové	superhrdinový	k2eAgFnPc1d1	superhrdinový
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
zásadní	zásadní	k2eAgFnPc4d1	zásadní
události	událost	k1gFnPc4	událost
minulosti	minulost	k1gFnSc2	minulost
jako	jako	k8xC	jako
válku	válka	k1gFnSc4	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
nebo	nebo	k8xC	nebo
prezidentství	prezidentství	k1gNnSc6	prezidentství
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
bojovníci	bojovník	k1gMnPc1	bojovník
se	s	k7c7	s
zločinem	zločin	k1gInSc7	zločin
nazýváni	nazývat	k5eAaImNgMnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
superhrdinové	superhrdinový	k2eAgInPc4d1	superhrdinový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jedinou	jediný	k2eAgFnSc7d1	jediná
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nadlidské	nadlidský	k2eAgFnPc4d1	nadlidská
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattana	k1gFnSc1	Manhattana
dává	dávat	k5eAaImIp3nS	dávat
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
strategickou	strategický	k2eAgFnSc4d1	strategická
výhodu	výhoda	k1gFnSc4	výhoda
nad	nad	k7c7	nad
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
však	však	k9	však
stali	stát	k5eAaPmAgMnP	stát
superhrdinové	superhrdinový	k2eAgFnPc4d1	superhrdinový
nepopulárními	populární	k2eNgMnPc7d1	nepopulární
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
postaveni	postavit	k5eAaPmNgMnP	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1985	[number]	k4	1985
policie	policie	k1gFnSc2	policie
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
vraždu	vražda	k1gFnSc4	vražda
Edwarda	Edward	k1gMnSc2	Edward
Blakea	Blakeus	k1gMnSc2	Blakeus
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
policie	policie	k1gFnSc1	policie
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
stopy	stopa	k1gFnPc4	stopa
<g/>
,	,	kIx,	,
Rorschach	Rorschach	k1gInSc1	Rorschach
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zjistit	zjistit	k5eAaPmF	zjistit
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Edward	Edward	k1gMnSc1	Edward
Blake	Blak	k1gFnSc2	Blak
je	být	k5eAaImIp3nS	být
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
Komediantem	komediant	k1gMnSc7	komediant
<g/>
,	,	kIx,	,
hrdinou	hrdina	k1gMnSc7	hrdina
v	v	k7c6	v
kostýmu	kostým	k1gInSc6	kostým
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
vláda	vláda	k1gFnSc1	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rorschach	Rorschach	k1gMnSc1	Rorschach
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgMnS	objevit
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
tyto	tento	k3xDgMnPc4	tento
hrdiny	hrdina	k1gMnPc4	hrdina
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
a	a	k8xC	a
chystá	chystat	k5eAaImIp3nS	chystat
se	se	k3xPyFc4	se
varovat	varovat	k5eAaImF	varovat
své	svůj	k3xOyFgMnPc4	svůj
druhy	druh	k1gMnPc4	druh
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
<g/>
:	:	kIx,	:
Dana	Dana	k1gFnSc1	Dana
Dreiberga	Dreiberga	k1gFnSc1	Dreiberga
(	(	kIx(	(
<g/>
býval	bývat	k5eAaImAgMnS	bývat
druhým	druhý	k4xOgMnSc7	druhý
Sůvou	sůva	k1gFnSc7	sůva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattan	k1gMnSc2	Manhattan
<g/>
,	,	kIx,	,
s	s	k7c7	s
nadlidskými	nadlidský	k2eAgFnPc7d1	nadlidská
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
emocionálně	emocionálně	k6eAd1	emocionálně
odtrženého	odtržený	k2eAgMnSc2d1	odtržený
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
milenku	milenka	k1gFnSc4	milenka
Laurie	Laurie	k1gFnSc1	Laurie
Juspeczykovou	Juspeczyková	k1gFnSc7	Juspeczyková
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc4	druhý
Hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
přízrak	přízrak	k1gInSc4	přízrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adriana	Adriana	k1gFnSc1	Adriana
Veidta	Veidta	k1gFnSc1	Veidta
(	(	kIx(	(
<g/>
kdysi	kdysi	k6eAd1	kdysi
hrdina	hrdina	k1gMnSc1	hrdina
jménem	jméno	k1gNnSc7	jméno
Ozymandias	Ozymandiasa	k1gFnPc2	Ozymandiasa
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
nejchytřejšího	chytrý	k2eAgMnSc4d3	nejchytřejší
člověka	člověk	k1gMnSc4	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Blakeově	Blakeův	k2eAgInSc6d1	Blakeův
pohřbu	pohřeb	k1gInSc6	pohřeb
je	být	k5eAaImIp3nS	být
Doktor	doktor	k1gMnSc1	doktor
Manhattan	Manhattan	k1gInSc1	Manhattan
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
obviněn	obviněn	k2eAgMnSc1d1	obviněn
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobil	způsobit	k5eAaPmAgMnS	způsobit
rakovinu	rakovina	k1gFnSc4	rakovina
svých	svůj	k3xOyFgMnPc2	svůj
bývalých	bývalý	k2eAgMnPc2d1	bývalý
kolegů	kolega	k1gMnPc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tato	tento	k3xDgNnPc4	tento
obvinění	obvinění	k1gNnPc4	obvinění
vezme	vzít	k5eAaPmIp3nS	vzít
vláda	vláda	k1gFnSc1	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
odejde	odejít	k5eAaPmIp3nS	odejít
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
na	na	k7c4	na
toho	ten	k3xDgMnSc4	ten
oslabení	oslabení	k1gNnSc4	oslabení
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
reaguje	reagovat	k5eAaBmIp3nS	reagovat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
invazí	invaze	k1gFnSc7	invaze
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Rorschachova	Rorschachův	k2eAgFnSc1d1	Rorschachova
paranoidní	paranoidní	k2eAgFnSc1d1	paranoidní
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Adrian	Adrian	k1gMnSc1	Adrian
Veidt	Veidt	k1gMnSc1	Veidt
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
unikne	uniknout	k5eAaPmIp3nS	uniknout
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
a	a	k8xC	a
Rorschach	Rorschach	k1gInSc1	Rorschach
samotný	samotný	k2eAgInSc1d1	samotný
je	být	k5eAaImIp3nS	být
neprávem	neprávo	k1gNnSc7	neprávo
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Juspeczyková	Juspeczyková	k1gFnSc1	Juspeczyková
<g/>
,	,	kIx,	,
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Doktorem	doktor	k1gMnSc7	doktor
Manhattanem	Manhattan	k1gInSc7	Manhattan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
k	k	k7c3	k
Dreibergovi	Dreiberg	k1gMnSc3	Dreiberg
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
sílí	sílet	k5eAaImIp3nS	sílet
citové	citový	k2eAgNnSc1d1	citové
pouto	pouto	k1gNnSc1	pouto
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
si	se	k3xPyFc3	se
oblečou	obléct	k5eAaPmIp3nP	obléct
své	svůj	k3xOyFgInPc4	svůj
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
strážců	strážce	k1gMnPc2	strážce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Dreiberg	Dreiberg	k1gMnSc1	Dreiberg
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
věcech	věc	k1gFnPc6	věc
dává	dávat	k5eAaImIp3nS	dávat
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
Rorschachovi	Rorschachův	k2eAgMnPc1d1	Rorschachův
<g/>
,	,	kIx,	,
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc1	Manhattan
teleportuje	teleportovat	k5eAaPmIp3nS	teleportovat
Laurie	Laurie	k1gFnSc1	Laurie
Juspeczykovou	Juspeczyková	k1gFnSc4	Juspeczyková
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
během	během	k7c2	během
hádky	hádka	k1gFnSc2	hádka
je	být	k5eAaImIp3nS	být
nucena	nucen	k2eAgFnSc1d1	nucena
se	se	k3xPyFc4	se
smířit	smířit	k5eAaPmF	smířit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Blake	Blake	k1gInSc1	Blake
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
pokusil	pokusit	k5eAaPmAgMnS	pokusit
znásilnit	znásilnit	k5eAaPmF	znásilnit
její	její	k3xOp3gFnSc4	její
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
přiměje	přimět	k5eAaPmIp3nS	přimět
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattana	k1gFnSc1	Manhattana
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
Rorschach	Rorschacha	k1gFnPc2	Rorschacha
a	a	k8xC	a
Sůva	sůva	k1gFnSc1	sůva
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c4	v
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
spiknutí	spiknutí	k1gNnSc2	spiknutí
kolem	kolem	k7c2	kolem
smrti	smrt	k1gFnSc2	smrt
Komedianta	komediant	k1gMnSc2	komediant
a	a	k8xC	a
obvinění	obvinění	k1gNnSc1	obvinění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
donutilo	donutit	k5eAaPmAgNnS	donutit
Manhattana	Manhattan	k1gMnSc4	Manhattan
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
celým	celý	k2eAgInSc7d1	celý
plánem	plán	k1gInSc7	plán
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
Adrian	Adrian	k1gMnSc1	Adrian
Veidt	Veidt	k2eAgMnSc1d1	Veidt
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
si	se	k3xPyFc3	se
Rorschach	Rorschach	k1gMnSc1	Rorschach
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pošle	poslat	k5eAaPmIp3nS	poslat
poštou	pošta	k1gFnSc7	pošta
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
ultrapravicového	ultrapravicový	k2eAgInSc2d1	ultrapravicový
newyorského	newyorský	k2eAgInSc2d1	newyorský
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
poté	poté	k6eAd1	poté
konfrontuje	konfrontovat	k5eAaBmIp3nS	konfrontovat
Veidta	Veidta	k1gFnSc1	Veidta
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
útočišti	útočiště	k1gNnSc6	útočiště
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Veidt	Veidt	k1gMnSc1	Veidt
objasní	objasnit	k5eAaPmIp3nS	objasnit
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
chce	chtít	k5eAaImIp3nS	chtít
zachránit	zachránit	k5eAaPmF	zachránit
lidstvo	lidstvo	k1gNnSc4	lidstvo
před	před	k7c7	před
hrozící	hrozící	k2eAgFnSc7d1	hrozící
nukleární	nukleární	k2eAgFnSc7d1	nukleární
válkou	válka	k1gFnSc7	válka
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
předstírat	předstírat	k5eAaImF	předstírat
útok	útok	k1gInSc4	útok
mimozemšťanů	mimozemšťan	k1gMnPc2	mimozemšťan
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabije	zabít	k5eAaPmIp3nS	zabít
polovinu	polovina	k1gFnSc4	polovina
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
spojí	spojit	k5eAaPmIp3nP	spojit
oba	dva	k4xCgInPc1	dva
národy	národ	k1gInPc1	národ
proti	proti	k7c3	proti
společnému	společný	k2eAgMnSc3d1	společný
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
přizná	přiznat	k5eAaPmIp3nS	přiznat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabil	zabít	k5eAaPmAgMnS	zabít
Komedianta	komediant	k1gMnSc4	komediant
<g/>
,	,	kIx,	,
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rakovinu	rakovina	k1gFnSc4	rakovina
u	u	k7c2	u
bývalých	bývalý	k2eAgMnPc2d1	bývalý
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattan	k1gMnSc2	Manhattan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
vystavil	vystavit	k5eAaPmAgMnS	vystavit
obviněním	obvinění	k1gNnSc7	obvinění
<g/>
,	,	kIx,	,
a	a	k8xC	a
inscenoval	inscenovat	k5eAaBmAgMnS	inscenovat
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
sebe	sebe	k3xPyFc4	sebe
samotného	samotný	k2eAgMnSc4d1	samotný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgMnS	zbavit
podezření	podezření	k1gNnSc4	podezření
<g/>
.	.	kIx.	.
</s>
<s>
Dreiberg	Dreiberg	k1gInSc1	Dreiberg
a	a	k8xC	a
Rorschach	Rorschach	k1gInSc1	Rorschach
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pokusí	pokusit	k5eAaPmIp3nS	pokusit
plán	plán	k1gInSc4	plán
překazit	překazit	k5eAaPmF	překazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Veidt	Veidt	k1gMnSc1	Veidt
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
již	již	k6eAd1	již
provedl	provést	k5eAaPmAgMnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
Juspeczyková	Juspeczyková	k1gFnSc1	Juspeczyková
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
svědky	svědek	k1gMnPc7	svědek
zkázy	zkáza	k1gFnSc2	zkáza
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
si	se	k3xPyFc3	se
povšimne	povšimnout	k5eAaPmIp3nS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
schopnosti	schopnost	k1gFnPc1	schopnost
jsou	být	k5eAaImIp3nP	být
brzděny	brzděn	k2eAgFnPc1d1	brzděna
tachyony	tachyona	k1gFnPc1	tachyona
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
zdroj	zdroj	k1gInSc1	zdroj
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
teleportují	teleportovat	k5eAaPmIp3nP	teleportovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dozvídají	dozvídat	k5eAaImIp3nP	dozvídat
se	se	k3xPyFc4	se
o	o	k7c6	o
plánu	plán	k1gInSc6	plán
Adriana	Adrian	k1gMnSc2	Adrian
Veidta	Veidt	k1gMnSc2	Veidt
<g/>
.	.	kIx.	.
</s>
<s>
Veidt	Veidt	k1gMnSc1	Veidt
jim	on	k3xPp3gFnPc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
televizní	televizní	k2eAgMnSc1d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
konec	konec	k1gInSc1	konec
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
mezi	mezi	k7c7	mezi
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zjištění	zjištění	k1gNnSc1	zjištění
přiměje	přimět	k5eAaPmIp3nS	přimět
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
skryli	skrýt	k5eAaPmAgMnP	skrýt
pravdu	pravda	k1gFnSc4	pravda
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Rorschach	Rorschach	k1gInSc1	Rorschach
ale	ale	k9	ale
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
dělat	dělat	k5eAaImF	dělat
kompromisy	kompromis	k1gInPc4	kompromis
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odhalil	odhalit	k5eAaPmAgMnS	odhalit
světu	svět	k1gInSc3	svět
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
postaví	postavit	k5eAaPmIp3nS	postavit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
Rorschach	Rorschach	k1gInSc1	Rorschach
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
musí	muset	k5eAaImIp3nS	muset
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
zabránil	zabránit	k5eAaPmAgInS	zabránit
v	v	k7c6	v
odhalení	odhalení	k1gNnSc6	odhalení
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
Manhattan	Manhattan	k1gInSc1	Manhattan
reaguje	reagovat	k5eAaBmIp3nS	reagovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
strážce	strážce	k1gMnSc1	strážce
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Veidt	Veidta	k1gFnPc2	Veidta
se	se	k3xPyFc4	se
Manhattana	Manhattana	k1gFnSc1	Manhattana
zeptá	zeptat	k5eAaPmIp3nS	zeptat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
tedy	tedy	k9	tedy
nakonec	nakonec	k6eAd1	nakonec
udělal	udělat	k5eAaPmAgMnS	udělat
správné	správný	k2eAgNnSc4d1	správné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
a	a	k8xC	a
Manhattan	Manhattan	k1gInSc1	Manhattan
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc1	nic
nekončí	končit	k5eNaImIp3nS	končit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
odejde	odejít	k5eAaPmIp3nS	odejít
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Dreiberg	Dreiberg	k1gInSc1	Dreiberg
a	a	k8xC	a
Juspeczyková	Juspeczyková	k1gFnSc1	Juspeczyková
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
pod	pod	k7c7	pod
novou	nový	k2eAgFnSc7d1	nová
identitou	identita	k1gFnSc7	identita
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
si	se	k3xPyFc3	se
vydavatel	vydavatel	k1gMnSc1	vydavatel
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
článků	článek	k1gInPc2	článek
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
požádá	požádat	k5eAaPmIp3nS	požádat
svého	svůj	k3xOyFgMnSc4	svůj
asistenta	asistent	k1gMnSc4	asistent
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našel	najít	k5eAaPmAgMnS	najít
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
místo	místo	k1gNnSc4	místo
vyplnit	vyplnit	k5eAaPmF	vyplnit
<g/>
.	.	kIx.	.
</s>
<s>
Komiks	komiks	k1gInSc1	komiks
končí	končit	k5eAaImIp3nS	končit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladík	mladík	k1gMnSc1	mladík
směřuje	směřovat	k5eAaImIp3nS	směřovat
ke	k	k7c3	k
stohu	stoh	k1gInSc3	stoh
vyřazeného	vyřazený	k2eAgInSc2d1	vyřazený
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vrcholu	vrchol	k1gInSc6	vrchol
leží	ležet	k5eAaImIp3nS	ležet
Rorschachův	Rorschachův	k2eAgInSc1d1	Rorschachův
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
Alana	Alan	k1gMnSc2	Alan
Moorea	Mooreus	k1gMnSc2	Mooreus
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
čtyři	čtyři	k4xCgFnPc4	čtyři
nebo	nebo	k8xC	nebo
pět	pět	k4xCc4	pět
úplně	úplně	k6eAd1	úplně
odlišných	odlišný	k2eAgInPc2d1	odlišný
pojetí	pojetí	k1gNnSc3	pojetí
vnímání	vnímání	k1gNnSc1	vnímání
světa	svět	k1gInSc2	svět
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
na	na	k7c6	na
čtenáři	čtenář	k1gMnSc6	čtenář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
morálně	morálně	k6eAd1	morálně
nejpřijatelnější	přijatelný	k2eAgFnSc1d3	nejpřijatelnější
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
nechtěl	chtít	k5eNaImAgMnS	chtít
do	do	k7c2	do
čtenáře	čtenář	k1gMnSc2	čtenář
"	"	kIx"	"
<g/>
tlačit	tlačit	k5eAaImF	tlačit
<g/>
"	"	kIx"	"
morální	morální	k2eAgNnSc4d1	morální
ponaučení	ponaučení	k1gNnSc4	ponaučení
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
chtěl	chtít	k5eAaImAgInS	chtít
ukázat	ukázat	k5eAaPmF	ukázat
rozpolcenost	rozpolcenost	k1gFnSc4	rozpolcenost
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
všechny	všechen	k3xTgMnPc4	všechen
ty	ten	k3xDgMnPc4	ten
lidi	člověk	k1gMnPc4	člověk
ukázat	ukázat	k5eAaPmF	ukázat
bez	bez	k7c2	bez
příkras	příkrasa	k1gFnPc2	příkrasa
<g/>
.	.	kIx.	.
</s>
<s>
Říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
ten	ten	k3xDgMnSc1	ten
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
a	a	k8xC	a
i	i	k9	i
ten	ten	k3xDgMnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
chyby	chyba	k1gFnPc4	chyba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Komediant	komediant	k1gMnSc1	komediant
/	/	kIx~	/
Edward	Edward	k1gMnSc1	Edward
Blake	Blak	k1gFnSc2	Blak
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
začátkem	začátkem	k7c2	začátkem
celého	celý	k2eAgInSc2d1	celý
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
retrospektivách	retrospektiva	k1gFnPc6	retrospektiva
a	a	k8xC	a
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
ostatních	ostatní	k2eAgFnPc2d1	ostatní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
jeho	jeho	k3xOp3gInPc4	jeho
povahové	povahový	k2eAgInPc4d1	povahový
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Komediant	komediant	k1gMnSc1	komediant
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
Peacemakera	Peacemakera	k1gFnSc1	Peacemakera
od	od	k7c2	od
Charlton	Charlton	k1gInSc4	Charlton
Comics	comics	k1gInSc1	comics
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
určité	určitý	k2eAgInPc4d1	určitý
rysy	rys	k1gInPc4	rys
špióna	špión	k1gMnSc2	špión
jménem	jméno	k1gNnSc7	jméno
Nick	Nicka	k1gFnPc2	Nicka
Fury	Fura	k1gFnSc2	Fura
od	od	k7c2	od
Marvel	Marvela	k1gFnPc2	Marvela
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
a	a	k8xC	a
Gibbons	Gibbonsa	k1gFnPc2	Gibbonsa
ho	on	k3xPp3gMnSc4	on
viděli	vidět	k5eAaImAgMnP	vidět
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
Gordona	Gordon	k1gMnSc2	Gordon
Liddyho	Liddy	k1gMnSc2	Liddy
[	[	kIx(	[
<g/>
měl	mít	k5eAaImAgInS	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vloupání	vloupání	k1gNnSc6	vloupání
do	do	k7c2	do
Watergate	Watergat	k1gMnSc5	Watergat
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
většího	veliký	k2eAgInSc2d2	veliký
a	a	k8xC	a
drsnějšího	drsný	k2eAgInSc2d2	drsnější
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc4	Manhattan
jsou	být	k5eAaImIp3nP	být
vládou	vláda	k1gFnSc7	vláda
tolerováni	tolerován	k2eAgMnPc1d1	tolerován
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vešel	vejít	k5eAaPmAgInS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Keenův	Keenův	k2eAgInSc4d1	Keenův
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
superhrdiny	superhrdina	k1gFnPc4	superhrdina
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
ilegality	ilegalita	k1gFnSc2	ilegalita
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
znásilnit	znásilnit	k5eAaPmF	znásilnit
Hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
přízrak	přízrak	k1gInSc4	přízrak
<g/>
,	,	kIx,	,
v	v	k7c6	v
sešitu	sešit	k1gInSc6	sešit
číslo	číslo	k1gNnSc1	číslo
9	[number]	k4	9
je	být	k5eAaImIp3nS	být
odhalen	odhalit	k5eAaPmNgMnS	odhalit
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
Laurie	Laurie	k1gFnSc2	Laurie
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc1	Manhattan
/	/	kIx~	/
Jonathan	Jonathan	k1gMnSc1	Jonathan
Osterman	Osterman	k1gMnSc1	Osterman
<g/>
:	:	kIx,	:
Bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
nadlidskými	nadlidský	k2eAgFnPc7d1	nadlidská
schopnostmi	schopnost	k1gFnPc7	schopnost
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vědec	vědec	k1gMnSc1	vědec
Jon	Jon	k1gMnSc1	Jon
Osterman	Osterman	k1gMnSc1	Osterman
získal	získat	k5eAaPmAgMnS	získat
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
nedopatřením	nedopatření	k1gNnSc7	nedopatření
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
na	na	k7c4	na
extrahování	extrahování	k1gNnSc4	extrahování
inherentního	inherentní	k2eAgNnSc2d1	inherentní
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattan	Manhattan	k1gInSc1	Manhattan
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
Captain	Captain	k2eAgInSc1d1	Captain
Atom	atom	k1gInSc1	atom
od	od	k7c2	od
Charlton	Charlton	k1gInSc1	Charlton
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Manhattan	Manhattan	k1gInSc1	Manhattan
jako	jako	k8xC	jako
jakýsi	jakýsi	k3yIgInSc1	jakýsi
"	"	kIx"	"
<g/>
kvantový	kvantový	k2eAgInSc1d1	kvantový
superhrdina	superhrdin	k2eAgNnSc2d1	superhrdin
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgInSc4d2	veliký
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
postrádají	postrádat	k5eAaImIp3nP	postrádat
vědecké	vědecký	k2eAgNnSc4d1	vědecké
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
svých	svůj	k3xOyFgInPc2	svůj
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Moore	Moor	k1gInSc5	Moor
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
postavy	postava	k1gFnSc2	postava
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattan	k1gMnSc4	Manhattan
ponořil	ponořit	k5eAaPmAgMnS	ponořit
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
jaderné	jaderný	k2eAgFnSc2d1	jaderná
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
kvantovém	kvantový	k2eAgInSc6d1	kvantový
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
nevnímal	vnímat	k5eNaImAgInS	vnímat
čas	čas	k1gInSc4	čas
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
jeho	on	k3xPp3gInSc4	on
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
záležitosti	záležitost	k1gFnPc4	záležitost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
se	se	k3xPyFc4	se
také	také	k9	také
chtěl	chtít	k5eAaImAgMnS	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
vytvoření	vytvoření	k1gNnSc4	vytvoření
postavy	postava	k1gFnSc2	postava
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
Spock	Spock	k1gInSc4	Spock
ze	z	k7c2	z
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ponechal	ponechat	k5eAaPmAgMnS	ponechat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattanovi	Manhattan	k1gMnSc3	Manhattan
lidské	lidský	k2eAgFnPc1d1	lidská
návyky	návyk	k1gInPc4	návyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkově	celkově	k6eAd1	celkově
ho	on	k3xPp3gMnSc4	on
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
odcizil	odcizit	k5eAaPmAgMnS	odcizit
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
si	se	k3xPyFc3	se
vybavuje	vybavovat	k5eAaImIp3nS	vybavovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nebyl	být	k5eNaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
mu	on	k3xPp3gMnSc3	on
DC	DC	kA	DC
dovolí	dovolit	k5eAaPmIp3nS	dovolit
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
nahá	nahý	k2eAgFnSc1d1	nahá
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
chtěl	chtít	k5eAaImAgMnS	chtít
zachovat	zachovat	k5eAaPmF	zachovat
vkus	vkus	k1gInSc4	vkus
při	při	k7c6	při
zobrazení	zobrazení	k1gNnSc6	zobrazení
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
vybíral	vybírat	k5eAaImAgMnS	vybírat
momenty	moment	k1gInPc4	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnSc4	jeho
nahotu	nahota	k1gFnSc4	nahota
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
anatomicky	anatomicky	k6eAd1	anatomicky
jej	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
obdařil	obdařit	k5eAaPmAgMnS	obdařit
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
klasické	klasický	k2eAgFnPc1d1	klasická
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
čtenářova	čtenářův	k2eAgFnSc1d1	čtenářova
pozornost	pozornost	k1gFnSc1	pozornost
nebyla	být	k5eNaImAgFnS	být
rušena	rušit	k5eAaImNgFnS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Sůva	sůva	k1gFnSc1	sůva
/	/	kIx~	/
Dan	Dan	k1gMnSc1	Dan
Dreiberg	Dreiberg	k1gMnSc1	Dreiberg
<g/>
:	:	kIx,	:
Superhrdina	Superhrdina	k1gFnSc1	Superhrdina
na	na	k7c6	na
odpočinku	odpočinek	k1gInSc6	odpočinek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
pomůcky	pomůcka	k1gFnPc4	pomůcka
odkazující	odkazující	k2eAgFnPc1d1	odkazující
k	k	k7c3	k
sovám	sova	k1gFnPc3	sova
<g/>
.	.	kIx.	.
</s>
<s>
Sůva	sůva	k1gFnSc1	sůva
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
Teda	Ted	k1gMnSc2	Ted
Korda	Korda	k1gMnSc1	Korda
z	z	k7c2	z
Blue	Blu	k1gInSc2	Blu
Beetlea	Beetle	k1gInSc2	Beetle
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ted	Ted	k1gMnSc1	Ted
Kord	kord	k1gInSc1	kord
i	i	k8xC	i
Sůva	sůva	k1gFnSc1	sůva
měl	mít	k5eAaImAgInS	mít
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
,	,	kIx,	,
bojovníka	bojovník	k1gMnSc2	bojovník
se	s	k7c7	s
zločinem	zločin	k1gInSc7	zločin
v	v	k7c6	v
důchodu	důchod	k1gInSc6	důchod
<g/>
,	,	kIx,	,
Hollise	Hollise	k1gFnSc1	Hollise
Masona	mason	k1gMnSc2	mason
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Moore	Moor	k1gInSc5	Moor
sestavil	sestavit	k5eAaPmAgInS	sestavit
pro	pro	k7c4	pro
Gibbonse	Gibbons	k1gMnPc4	Gibbons
základní	základní	k2eAgInSc4d1	základní
popis	popis	k1gInSc4	popis
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
Gibbons	Gibbons	k1gInSc4	Gibbons
jí	jíst	k5eAaImIp3nS	jíst
dal	dát	k5eAaPmAgMnS	dát
kostým	kostým	k1gInSc4	kostým
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
si	se	k3xPyFc3	se
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
bylo	být	k5eAaImAgNnS	být
dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Reynolds	Reynolds	k1gInSc4	Reynolds
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Super	super	k2eAgFnPc2d1	super
Heroes	Heroesa	k1gFnPc2	Heroesa
<g/>
:	:	kIx,	:
A	a	k8xC	a
Modern	Modern	k1gInSc4	Modern
Mythology	mytholog	k1gMnPc7	mytholog
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
původ	původ	k1gInSc4	původ
postavy	postava	k1gFnSc2	postava
mají	mít	k5eAaImIp3nP	mít
motivy	motiv	k1gInPc4	motiv
Sůvy	sůva	k1gFnPc1	sůva
více	hodně	k6eAd2	hodně
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
postavou	postava	k1gFnSc7	postava
Batmana	Batman	k1gMnSc2	Batman
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Klocka	Klocko	k1gNnSc2	Klocko
jeho	jeho	k3xOp3gInSc1	jeho
civilní	civilní	k2eAgInSc1d1	civilní
vzhled	vzhled	k1gInSc1	vzhled
"	"	kIx"	"
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
impotentního	impotentní	k2eAgInSc2d1	impotentní
Clarka	Clark	k1gInSc2	Clark
Kenta	Kento	k1gNnSc2	Kento
ve	v	k7c6	v
středních	střední	k2eAgNnPc6d1	střední
letech	léto	k1gNnPc6	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ozymandias	Ozymandias	k1gMnSc1	Ozymandias
/	/	kIx~	/
Adrian	Adrian	k1gMnSc1	Adrian
Veidt	Veidt	k1gMnSc1	Veidt
<g/>
:	:	kIx,	:
Veidt	Veidt	k1gMnSc1	Veidt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kresba	kresba	k1gFnSc1	kresba
je	být	k5eAaImIp3nS	být
inspirována	inspirován	k2eAgFnSc1d1	inspirována
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
superhrdina	superhrdin	k2eAgNnPc1d1	superhrdin
jménem	jméno	k1gNnSc7	jméno
Ozymandias	Ozymandiasa	k1gFnPc2	Ozymandiasa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
podnikáním	podnikání	k1gNnSc7	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Veidtovi	Veidt	k1gMnSc6	Veidt
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejchytřejším	chytrý	k2eAgMnSc7d3	nejchytřejší
člověkem	člověk	k1gMnSc7	člověk
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ozymandias	Ozymandias	k1gInSc1	Ozymandias
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
inspirován	inspirován	k2eAgInSc1d1	inspirován
Peterem	Peter	k1gMnSc7	Peter
Cannonem	Cannon	k1gMnSc7	Cannon
-	-	kIx~	-
Thunderboltem	Thunderbolt	k1gInSc7	Thunderbolt
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
Moore	Moor	k1gInSc5	Moor
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
kvůli	kvůli	k7c3	kvůli
plnému	plný	k2eAgNnSc3d1	plné
využití	využití	k1gNnSc3	využití
jeho	jeho	k3xOp3gFnSc2	jeho
mozkové	mozkový	k2eAgFnSc2d1	mozková
kapacity	kapacita	k1gFnSc2	kapacita
a	a	k8xC	a
absolutní	absolutní	k2eAgFnSc6d1	absolutní
fyzické	fyzický	k2eAgFnSc3d1	fyzická
a	a	k8xC	a
duševní	duševní	k2eAgFnSc3d1	duševní
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Reynolds	Reynoldsa	k1gFnPc2	Reynoldsa
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
záměr	záměr	k1gInSc1	záměr
"	"	kIx"	"
<g/>
pomoci	pomoct	k5eAaPmF	pomoct
světu	svět	k1gInSc3	svět
<g/>
"	"	kIx"	"
staví	stavit	k5eAaPmIp3nS	stavit
Veidta	Veidta	k1gMnSc1	Veidta
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
normálně	normálně	k6eAd1	normálně
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
komiksovým	komiksový	k2eAgMnPc3d1	komiksový
padouchům	padouch	k1gMnPc3	padouch
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
hlavním	hlavní	k2eAgMnSc7d1	hlavní
"	"	kIx"	"
<g/>
padouchem	padouch	k1gMnSc7	padouch
<g/>
"	"	kIx"	"
série	série	k1gFnSc1	série
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejhorší	zlý	k2eAgFnSc7d3	nejhorší
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
povyšování	povyšování	k1gNnSc1	povyšování
nad	nad	k7c4	nad
ostatní	ostatní	k2eAgMnPc4d1	ostatní
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
pohrdání	pohrdání	k1gNnSc4	pohrdání
lidstvem	lidstvo	k1gNnSc7	lidstvo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rorschach	Rorschach	k1gMnSc1	Rorschach
/	/	kIx~	/
Walter	Walter	k1gMnSc1	Walter
Kovacs	Kovacsa	k1gFnPc2	Kovacsa
<g/>
:	:	kIx,	:
Strážce	strážce	k1gMnSc1	strážce
nosící	nosící	k2eAgMnSc1d1	nosící
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
bílou	bílý	k2eAgFnSc4d1	bílá
masku	maska	k1gFnSc4	maska
obsahující	obsahující	k2eAgFnSc2d1	obsahující
symetrické	symetrický	k2eAgFnSc2d1	symetrická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
měnící	měnící	k2eAgFnPc1d1	měnící
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
stíhán	stíhat	k5eAaImNgMnS	stíhat
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
charakter	charakter	k1gInSc1	charakter
Moore	Moor	k1gInSc5	Moor
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
podle	podle	k7c2	podle
postav	postava	k1gFnPc2	postava
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nebo	nebo	k8xC	nebo
The	The	k1gFnSc1	The
Question	Question	k1gInSc1	Question
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
komiksový	komiksový	k2eAgMnSc1d1	komiksový
spisovatel	spisovatel	k1gMnSc1	spisovatel
Steve	Steve	k1gMnSc1	Steve
Ditko	Ditko	k1gNnSc4	Ditko
<g/>
.	.	kIx.	.
</s>
<s>
Komiksový	komiksový	k2eAgMnSc1d1	komiksový
historik	historik	k1gMnSc1	historik
Bradford	Bradford	k1gMnSc1	Bradford
W.	W.	kA	W.
Wright	Wright	k1gMnSc1	Wright
popsal	popsat	k5eAaPmAgMnS	popsat
Rorschachovo	Rorschachův	k2eAgNnSc4d1	Rorschachův
vidění	vidění	k1gNnSc4	vidění
světa	svět	k1gInSc2	svět
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
černobílé	černobílý	k2eAgFnPc1d1	černobílá
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
bere	brát	k5eAaImIp3nS	brát
různé	různý	k2eAgFnSc2d1	různá
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
neslije	slít	k5eNaPmIp3nS	slít
do	do	k7c2	do
šedivé	šedivý	k2eAgFnSc2d1	šedivá
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
skvrny	skvrna	k1gFnPc1	skvrna
v	v	k7c6	v
testech	test	k1gInPc6	test
jeho	jeho	k3xOp3gFnSc3	jeho
jmenovce	jmenovka	k1gFnSc3	jmenovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
dílu	díl	k1gInSc2	díl
neuvažoval	uvažovat	k5eNaImAgMnS	uvažovat
o	o	k7c6	o
Rorschachově	Rorschachův	k2eAgFnSc6d1	Rorschachova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
ale	ale	k9	ale
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Rorschach	Rorschach	k1gMnSc1	Rorschach
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
udělat	udělat	k5eAaPmF	udělat
kompromis	kompromis	k1gInSc4	kompromis
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
přežít	přežít	k5eAaPmF	přežít
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Laurie	Laurie	k1gFnSc1	Laurie
Juspeczyková	Juspeczyková	k1gFnSc1	Juspeczyková
/	/	kIx~	/
Hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
přízrak	přízrak	k1gInSc1	přízrak
<g/>
:	:	kIx,	:
Dcera	dcera	k1gFnSc1	dcera
prvního	první	k4xOgInSc2	první
Hedvábného	hedvábný	k2eAgInSc2d1	hedvábný
přízraku	přízrak	k1gInSc2	přízrak
a	a	k8xC	a
Komedianta	komediant	k1gMnSc2	komediant
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gInPc1	jejich
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
napjaté	napjatý	k2eAgInPc1d1	napjatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
milenkou	milenka	k1gFnSc7	milenka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattan	k1gMnSc2	Manhattan
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
sblížila	sblížit	k5eAaPmAgFnS	sblížit
se	s	k7c7	s
Sůvou	sůva	k1gFnSc7	sůva
(	(	kIx(	(
<g/>
Danem	Dan	k1gMnSc7	Dan
Dreibergem	Dreiberg	k1gMnSc7	Dreiberg
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
také	také	k6eAd1	také
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
přízrak	přízrak	k1gInSc1	přízrak
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
Nightshade	Nightshad	k1gInSc5	Nightshad
od	od	k7c2	od
Charlton	Charlton	k1gInSc1	Charlton
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
postavou	postava	k1gFnSc7	postava
spokojený	spokojený	k2eAgMnSc1d1	spokojený
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
jí	jíst	k5eAaImIp3nS	jíst
něco	něco	k3yInSc4	něco
z	z	k7c2	z
hrdinek	hrdinka	k1gFnPc2	hrdinka
jako	jako	k8xS	jako
Black	Blacka	k1gFnPc2	Blacka
Canary	Canara	k1gFnSc2	Canara
nebo	nebo	k8xC	nebo
Phantom	Phantom	k1gInSc4	Phantom
Lady	Lada	k1gFnSc2	Lada
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
a	a	k8xC	a
Gibbons	Gibbonsa	k1gFnPc2	Gibbonsa
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Strážce	strážce	k1gMnSc1	strážce
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ukázali	ukázat	k5eAaPmAgMnP	ukázat
unikátní	unikátní	k2eAgFnPc4d1	unikátní
možnosti	možnost	k1gFnPc4	možnost
komiksu	komiks	k1gInSc2	komiks
a	a	k8xC	a
vyzdvihli	vyzdvihnout	k5eAaPmAgMnP	vyzdvihnout
jeho	jeho	k3xOp3gFnPc4	jeho
silné	silný	k2eAgFnPc4d1	silná
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Moore	Moor	k1gInSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
ty	ten	k3xDgFnPc4	ten
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
je	být	k5eAaImIp3nS	být
komiks	komiks	k1gInSc1	komiks
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
a	a	k8xC	a
kde	kde	k9	kde
žádná	žádný	k3yNgNnPc1	žádný
jiná	jiný	k2eAgNnPc1d1	jiné
média	médium	k1gNnPc1	médium
nemohou	moct	k5eNaImIp3nP	moct
fungovat	fungovat	k5eAaImF	fungovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
filmem	film	k1gInSc7	film
a	a	k8xC	a
komiksem	komiks	k1gInSc7	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Strážci	strážce	k1gMnPc1	strážce
jsou	být	k5eAaImIp3nP	být
napsáni	napsat	k5eAaBmNgMnP	napsat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
přečíst	přečíst	k5eAaPmF	přečíst
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
nebo	nebo	k8xC	nebo
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
až	až	k9	až
tehdy	tehdy	k6eAd1	tehdy
čtenář	čtenář	k1gMnSc1	čtenář
pochopí	pochopit	k5eAaPmIp3nS	pochopit
některé	některý	k3yIgFnPc4	některý
souvislosti	souvislost	k1gFnPc4	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Gibbons	Gibbons	k1gInSc4	Gibbons
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jak	jak	k6eAd1	jak
jsme	být	k5eAaImIp1nP	být
postupovali	postupovat	k5eAaImAgMnP	postupovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Strážci	strážce	k1gMnPc1	strážce
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
vypravování	vypravování	k1gNnSc2	vypravování
příběhu	příběh	k1gInSc2	příběh
než	než	k8xS	než
o	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
samotném	samotný	k2eAgInSc6d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Zápletka	zápletka	k1gFnSc1	zápletka
nemá	mít	k5eNaImIp3nS	mít
až	až	k9	až
takový	takový	k3xDgInSc4	takový
význam	význam	k1gInSc4	význam
<g/>
...	...	k?	...
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ta	ten	k3xDgFnSc1	ten
nejzajímavější	zajímavý	k2eAgFnSc1d3	nejzajímavější
věc	věc	k1gFnSc1	věc
na	na	k7c6	na
Strážcích	strážce	k1gMnPc6	strážce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gibbons	Gibbons	k1gInSc1	Gibbons
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úmyslně	úmyslně	k6eAd1	úmyslně
vzhled	vzhled	k1gInSc4	vzhled
Strážců	strážce	k1gMnPc2	strážce
stvořil	stvořit	k5eAaPmAgMnS	stvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každá	každý	k3xTgFnSc1	každý
stránka	stránka	k1gFnSc1	stránka
byla	být	k5eAaImAgFnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
identifikovatelná	identifikovatelný	k2eAgFnSc1d1	identifikovatelná
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
určité	určitý	k2eAgFnSc2d1	určitá
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
"	"	kIx"	"
<g/>
jen	jen	k9	jen
nějakého	nějaký	k3yIgInSc2	nějaký
komiksu	komiks	k1gInSc2	komiks
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
kreslit	kreslit	k5eAaImF	kreslit
postavy	postava	k1gFnPc4	postava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
odlišovaly	odlišovat	k5eAaImAgFnP	odlišovat
od	od	k7c2	od
běžných	běžný	k2eAgMnPc2d1	běžný
komiksových	komiksový	k2eAgMnPc2d1	komiksový
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
využil	využít	k5eAaPmAgInS	využít
Gibbonsovu	Gibbonsův	k2eAgFnSc4d1	Gibbonsův
původní	původní	k2eAgFnSc4d1	původní
profesi	profese	k1gFnSc4	profese
geodeta	geodet	k1gMnSc4	geodet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
každého	každý	k3xTgNnSc2	každý
okénka	okénko	k1gNnSc2	okénko
i	i	k9	i
ty	ten	k3xDgInPc1	ten
nejmenší	malý	k2eAgInPc1d3	nejmenší
detaily	detail	k1gInPc1	detail
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tak	tak	k6eAd1	tak
přesně	přesně	k6eAd1	přesně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vyznění	vyznění	k1gNnSc3	vyznění
každého	každý	k3xTgNnSc2	každý
políčka	políčko	k1gNnSc2	políčko
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
popsal	popsat	k5eAaPmAgMnS	popsat
sérii	série	k1gFnSc4	série
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
komiks	komiks	k1gInSc1	komiks
o	o	k7c6	o
komiksu	komiks	k1gInSc6	komiks
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Alan	Alan	k1gMnSc1	Alan
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
zabývá	zabývat	k5eAaImIp3nS	zabývat
sociální	sociální	k2eAgFnSc7d1	sociální
problematikou	problematika	k1gFnSc7	problematika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
technickou	technický	k2eAgFnSc7d1	technická
stránkou	stránka	k1gFnSc7	stránka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
alternativní	alternativní	k2eAgFnSc6d1	alternativní
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Gibbonsovi	Gibbonsův	k2eAgMnPc1d1	Gibbonsův
některé	některý	k3yIgFnPc4	některý
součásti	součást	k1gFnPc4	součást
amerického	americký	k2eAgInSc2d1	americký
života	život	k1gInSc2	život
pozměnit	pozměnit	k5eAaPmF	pozměnit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dodat	dodat	k5eAaPmF	dodat
auta	auto	k1gNnPc4	auto
poháněná	poháněný	k2eAgFnSc1d1	poháněná
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
,	,	kIx,	,
odlišné	odlišný	k2eAgFnPc4d1	odlišná
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgInPc4d1	elektrický
hydranty	hydrant	k1gInPc4	hydrant
namísto	namísto	k7c2	namísto
vodních	vodní	k2eAgInPc2d1	vodní
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
Moore	Moor	k1gInSc5	Moor
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
americkému	americký	k2eAgMnSc3d1	americký
čtenáři	čtenář	k1gMnSc3	čtenář
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kulturu	kultura	k1gFnSc4	kultura
očima	oko	k1gNnPc7	oko
cizince	cizinec	k1gMnSc2	cizinec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgInS	mít
tak	tak	k6eAd1	tak
svázané	svázaný	k2eAgFnPc4d1	svázaná
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
obvyklé	obvyklý	k2eAgFnSc6d1	obvyklá
práci	práce	k1gFnSc6	práce
musí	muset	k5eAaImIp3nS	muset
nastudovat	nastudovat	k5eAaBmF	nastudovat
hodně	hodně	k6eAd1	hodně
faktů	fakt	k1gInPc2	fakt
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Higgins	Higgins	k1gInSc1	Higgins
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
komiks	komiks	k1gInSc1	komiks
barvil	barvit	k5eAaImAgInS	barvit
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgInS	použít
temnější	temný	k2eAgFnSc4d2	temnější
paletu	paleta	k1gFnSc4	paleta
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
sekundárním	sekundární	k2eAgFnPc3d1	sekundární
barvám	barva	k1gFnPc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
vždycky	vždycky	k6eAd1	vždycky
"	"	kIx"	"
<g/>
miloval	milovat	k5eAaImAgMnS	milovat
Johnovy	Johnův	k2eAgFnPc4d1	Johnova
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
ho	on	k3xPp3gMnSc4	on
zařazeného	zařazený	k2eAgMnSc4d1	zařazený
jako	jako	k8xS	jako
airbrushového	airbrushový	k2eAgMnSc4d1	airbrushový
koloristu	kolorista	k1gMnSc4	kolorista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
Mooreovi	Mooreus	k1gMnSc6	Mooreus
moc	moc	k6eAd1	moc
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Higgins	Higgins	k6eAd1	Higgins
věnoval	věnovat	k5eAaPmAgMnS	věnovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
světlu	světlo	k1gNnSc3	světlo
a	a	k8xC	a
jemným	jemný	k2eAgFnPc3d1	jemná
barevným	barevný	k2eAgFnPc3d1	barevná
změnám	změna	k1gFnPc3	změna
<g/>
;	;	kIx,	;
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
čísle	číslo	k1gNnSc6	číslo
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
používal	používat	k5eAaImAgMnS	používat
teplé	teplý	k2eAgNnSc4d1	teplé
a	a	k8xC	a
radostné	radostný	k2eAgNnSc4d1	radostné
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
potemňoval	potemňovat	k5eAaImAgMnS	potemňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dal	dát	k5eAaPmAgInS	dát
příběhu	příběh	k1gInSc3	příběh
ponurý	ponurý	k2eAgInSc1d1	ponurý
a	a	k8xC	a
temný	temný	k2eAgInSc1d1	temný
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Strážci	strážce	k1gMnPc1	strážce
se	se	k3xPyFc4	se
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
aspektech	aspekt	k1gInPc6	aspekt
lišili	lišit	k5eAaImAgMnP	lišit
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
komiksů	komiks	k1gInPc2	komiks
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
políčkovém	políčkový	k2eAgNnSc6d1	políčkový
rozložení	rozložení	k1gNnSc6	rozložení
a	a	k8xC	a
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
políček	políčko	k1gNnPc2	políčko
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
tvůrci	tvůrce	k1gMnPc1	tvůrce
každou	každý	k3xTgFnSc4	každý
stranu	strana	k1gFnSc4	strana
na	na	k7c4	na
mřížku	mřížka	k1gFnSc4	mřížka
o	o	k7c6	o
devíti	devět	k4xCc6	devět
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
toto	tento	k3xDgNnSc4	tento
rozložení	rozložení	k1gNnSc4	rozložení
preferoval	preferovat	k5eAaImAgInS	preferovat
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
vážnosti	vážnost	k1gFnSc3	vážnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
evokuje	evokovat	k5eAaBmIp3nS	evokovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Gibbonse	Gibbonse	k1gFnSc2	Gibbonse
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
rozložením	rozložení	k1gNnSc7	rozložení
Moore	Moor	k1gInSc5	Moor
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mohl	moct	k5eAaImAgInS	moct
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
takovou	takový	k3xDgFnSc4	takový
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
vyprávěním	vyprávění	k1gNnSc7	vyprávění
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
neměl	mít	k5eNaImAgMnS	mít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obálka	obálka	k1gFnSc1	obálka
každého	každý	k3xTgInSc2	každý
sešitu	sešit	k1gInSc2	sešit
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
první	první	k4xOgNnSc4	první
políčko	políčko	k1gNnSc4	políčko
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Obálka	obálka	k1gFnSc1	obálka
Strážců	strážce	k1gMnPc2	strážce
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vypadá	vypadat	k5eAaPmIp3nS	vypadat
skutečně	skutečně	k6eAd1	skutečně
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
v	v	k7c4	v
komiks	komiks	k1gInSc4	komiks
<g/>
,	,	kIx,	,
portál	portál	k1gInSc4	portál
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Obálky	obálka	k1gFnPc1	obálka
byly	být	k5eAaImAgFnP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
jako	jako	k8xS	jako
detail	detail	k1gInSc1	detail
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
věc	věc	k1gFnSc4	věc
bez	bez	k7c2	bez
lidské	lidský	k2eAgFnSc2d1	lidská
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
občas	občas	k6eAd1	občas
s	s	k7c7	s
rozvržením	rozvržení	k1gNnSc7	rozvržení
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
páté	pátý	k4xOgNnSc4	pátý
číslo	číslo	k1gNnSc4	číslo
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Strašná	strašný	k2eAgFnSc1d1	strašná
souměrnost	souměrnost	k1gFnSc1	souměrnost
<g/>
"	"	kIx"	"
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
stránka	stránka	k1gFnSc1	stránka
zrcadlově	zrcadlově	k6eAd1	zrcadlově
odráží	odrážet	k5eAaImIp3nS	odrážet
poslední	poslední	k2eAgNnSc1d1	poslední
(	(	kIx(	(
<g/>
myšleno	myšlen	k2eAgNnSc1d1	myšleno
rozmístění	rozmístění	k1gNnSc1	rozmístění
polí	pole	k1gFnPc2	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
postupně	postupně	k6eAd1	postupně
až	až	k9	až
ke	k	k7c3	k
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
každého	každý	k3xTgInSc2	každý
sešitu	sešit	k1gInSc2	sešit
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
poslední	poslední	k2eAgFnSc2d1	poslední
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
doplňující	doplňující	k2eAgInSc1d1	doplňující
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
Moore	Moor	k1gMnSc5	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
kapitoly	kapitola	k1gFnPc1	kapitola
fiktivní	fiktivní	k2eAgFnPc4d1	fiktivní
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc4	dopis
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc4	zpráva
a	a	k8xC	a
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
napsaly	napsat	k5eAaPmAgFnP	napsat
různé	různý	k2eAgFnPc4d1	různá
postavy	postava	k1gFnPc4	postava
ze	z	k7c2	z
Strážců	strážce	k1gMnPc2	strážce
<g/>
.	.	kIx.	.
</s>
<s>
DC	DC	kA	DC
mělo	mít	k5eAaImAgNnS	mít
potíže	potíž	k1gFnSc2	potíž
při	při	k7c6	při
prodávání	prodávání	k1gNnSc6	prodávání
reklamního	reklamní	k2eAgInSc2d1	reklamní
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
zhruba	zhruba	k6eAd1	zhruba
osm	osm	k4xCc4	osm
až	až	k9	až
devět	devět	k4xCc4	devět
stran	strana	k1gFnPc2	strana
na	na	k7c4	na
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
DC	DC	kA	DC
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
zaplnit	zaplnit	k5eAaPmF	zaplnit
reklamou	reklama	k1gFnSc7	reklama
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
publikace	publikace	k1gFnPc4	publikace
a	a	k8xC	a
sloupky	sloupek	k1gInPc4	sloupek
s	s	k7c7	s
dopisy	dopis	k1gInPc7	dopis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydavatel	vydavatel	k1gMnSc1	vydavatel
Len	Lena	k1gFnPc2	Lena
Wein	Wein	k1gMnSc1	Wein
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nefér	fér	k6eNd1	fér
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
napsali	napsat	k5eAaPmAgMnP	napsat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgInPc2d1	poslední
čtyř	čtyři	k4xCgInPc2	čtyři
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
použít	použít	k5eAaPmF	použít
tyto	tento	k3xDgFnPc4	tento
stránky	stránka	k1gFnPc4	stránka
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
tak	tak	k9	tak
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
tři	tři	k4xCgNnPc4	tři
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgNnPc4	čtyři
<g/>
,	,	kIx,	,
říkali	říkat	k5eAaImAgMnP	říkat
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaPmIp3nS	vypadat
bez	bez	k7c2	bez
dopisů	dopis	k1gInPc2	dopis
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
míň	málo	k6eAd2	málo
jako	jako	k8xS	jako
komiks	komiks	k1gInSc4	komiks
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsme	být	k5eAaImIp1nP	být
u	u	k7c2	u
toho	ten	k3xDgMnSc4	ten
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Strážci	strážce	k1gMnPc1	strážce
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
"	"	kIx"	"
<g/>
vyprávění	vyprávění	k1gNnSc4	vyprávění
ve	v	k7c6	v
vyprávění	vyprávění	k1gNnSc6	vyprávění
<g/>
"	"	kIx"	"
jménem	jméno	k1gNnSc7	jméno
Příběhy	příběh	k1gInPc1	příběh
Černé	Černé	k2eAgFnSc2d1	Černé
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
komiksovou	komiksový	k2eAgFnSc4d1	komiksová
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
tři	tři	k4xCgInPc4	tři
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
osm	osm	k4xCc4	osm
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
<g/>
,	,	kIx,	,
deset	deset	k4xCc4	deset
a	a	k8xC	a
jedenáct	jedenáct	k4xCc4	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Trosečník	trosečník	k1gMnSc1	trosečník
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
čte	číst	k5eAaImIp3nS	číst
mladý	mladý	k2eAgMnSc1d1	mladý
černoch	černoch	k1gMnSc1	černoch
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
a	a	k8xC	a
Gibbons	Gibbonsa	k1gFnPc2	Gibbonsa
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
pirátský	pirátský	k2eAgInSc4d1	pirátský
komiks	komiks	k1gInSc4	komiks
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Strážcích	strážce	k1gMnPc6	strážce
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
opravdoví	opravdový	k2eAgMnPc1d1	opravdový
superhrdinové	superhrdinový	k2eAgNnSc1d1	superhrdinové
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
lidé	člověk	k1gMnPc1	člověk
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neměli	mít	k5eNaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
superhrdinské	superhrdinský	k2eAgInPc4d1	superhrdinský
komiksy	komiks	k1gInPc4	komiks
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pirátské	pirátský	k2eAgNnSc4d1	pirátské
téma	téma	k1gNnSc4	téma
a	a	k8xC	a
Moore	Moor	k1gInSc5	Moor
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
Bertolda	Bertold	k1gMnSc2	Bertold
Brechta	Brecht	k1gMnSc2	Brecht
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
narážkou	narážka	k1gFnSc7	narážka
na	na	k7c4	na
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Seeräuberjenny	Seeräuberjenn	k1gInPc4	Seeräuberjenn
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Balada	balada	k1gFnSc1	balada
o	o	k7c4	o
pirátské	pirátský	k2eAgFnPc4d1	pirátská
Jenny	Jenna	k1gFnPc4	Jenna
<g/>
)	)	kIx)	)
z	z	k7c2	z
Brechtovy	Brechtův	k2eAgFnSc2d1	Brechtova
Třígrošové	třígrošový	k2eAgFnSc2d1	Třígrošová
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
teoretizoval	teoretizovat	k5eAaImAgMnS	teoretizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nP	kdyby
superhrdinové	superhrdinový	k2eAgNnSc4d1	superhrdinové
opravdu	opravdu	k6eAd1	opravdu
existovali	existovat	k5eAaImAgMnP	existovat
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
předmětem	předmět	k1gInSc7	předmět
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
pohrdání	pohrdání	k1gNnSc6	pohrdání
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nP	by
z	z	k7c2	z
komiksových	komiksový	k2eAgInPc2d1	komiksový
sešitů	sešit	k1gInPc2	sešit
brzy	brzy	k6eAd1	brzy
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
celý	celý	k2eAgInSc1d1	celý
předobraz	předobraz	k1gInSc1	předobraz
pirátství	pirátství	k1gNnSc2	pirátství
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
bohatý	bohatý	k2eAgInSc1d1	bohatý
a	a	k8xC	a
temný	temný	k2eAgInSc1d1	temný
<g/>
,	,	kIx,	,
že	že	k8xS	že
představuje	představovat	k5eAaImIp3nS	představovat
skvělou	skvělý	k2eAgFnSc4d1	skvělá
protiváhu	protiváha	k1gFnSc4	protiváha
světa	svět	k1gInSc2	svět
Strážců	strážce	k1gMnPc2	strážce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
tuto	tento	k3xDgFnSc4	tento
premisu	premisa	k1gFnSc4	premisa
pojal	pojmout	k5eAaPmAgMnS	pojmout
jako	jako	k9	jako
možnost	možnost	k1gFnSc4	možnost
přidat	přidat	k5eAaPmF	přidat
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
podtext	podtext	k1gInSc1	podtext
a	a	k8xC	a
alegorii	alegorie	k1gFnSc3	alegorie
<g/>
.	.	kIx.	.
</s>
<s>
Doplňující	doplňující	k2eAgInSc1d1	doplňující
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
páté	pátá	k1gFnSc2	pátá
části	část	k1gFnSc2	část
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
historii	historie	k1gFnSc4	historie
Příběhů	příběh	k1gInPc2	příběh
černé	černý	k2eAgFnSc2d1	černá
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xS	jako
hlavního	hlavní	k2eAgMnSc4d1	hlavní
tvůrce	tvůrce	k1gMnSc4	tvůrce
série	série	k1gFnSc2	série
skutečného	skutečný	k2eAgMnSc4d1	skutečný
kreslíře	kreslíř	k1gMnSc4	kreslíř
Joea	Joe	k1gInSc2	Joe
Orlanda	Orlanda	k1gFnSc1	Orlanda
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Orlanda	Orlando	k1gNnSc2	Orlando
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
pirátské	pirátský	k2eAgInPc1d1	pirátský
příběhy	příběh	k1gInPc1	příběh
ze	z	k7c2	z
Strážců	strážce	k1gMnPc2	strážce
staly	stát	k5eAaPmAgFnP	stát
populárními	populární	k2eAgFnPc7d1	populární
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
DC	DC	kA	DC
zkusit	zkusit	k5eAaPmF	zkusit
zlákat	zlákat	k5eAaPmF	zlákat
kreslíře	kreslíř	k1gMnPc4	kreslíř
k	k	k7c3	k
nakreslení	nakreslení	k1gNnSc3	nakreslení
takového	takový	k3xDgInSc2	takový
komiksu	komiks	k1gInSc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Orlando	Orlando	k6eAd1	Orlando
přispěl	přispět	k5eAaPmAgInS	přispět
svou	svůj	k3xOyFgFnSc7	svůj
kresbou	kresba	k1gFnSc7	kresba
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
stránky	stránka	k1gFnSc2	stránka
z	z	k7c2	z
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Trosečník	trosečník	k1gMnSc1	trosečník
<g/>
"	"	kIx"	"
popisuje	popisovat	k5eAaImIp3nS	popisovat
cestu	cesta	k1gFnSc4	cesta
mladého	mladý	k2eAgMnSc2d1	mladý
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
varovat	varovat	k5eAaImF	varovat
své	svůj	k3xOyFgNnSc4	svůj
domovské	domovský	k2eAgNnSc4d1	domovské
město	město	k1gNnSc4	město
před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
Černé	Černé	k2eAgFnSc2d1	Černé
lodě	loď	k1gFnSc2	loď
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
přežije	přežít	k5eAaPmIp3nS	přežít
zničení	zničení	k1gNnSc3	zničení
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Reynoldse	Reynolds	k1gMnSc2	Reynolds
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
okolnostmi	okolnost	k1gFnPc7	okolnost
donucen	donutit	k5eAaPmNgInS	donutit
"	"	kIx"	"
<g/>
přestupovat	přestupovat	k5eAaImF	přestupovat
jeden	jeden	k4xCgInSc1	jeden
zákaz	zákaz	k1gInSc1	zákaz
za	za	k7c7	za
druhým	druhý	k4xOgInSc7	druhý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Použije	použít	k5eAaPmIp3nS	použít
těla	tělo	k1gNnPc4	tělo
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
námořníků	námořník	k1gMnPc2	námořník
jako	jako	k8xS	jako
plovák	plovák	k1gInSc1	plovák
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vor	vor	k1gInSc4	vor
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
nevinné	vinný	k2eNgMnPc4d1	nevinný
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dostane	dostat	k5eAaPmIp3nS	dostat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
jíž	jenž	k3xRgFnSc3	jenž
okupováno	okupován	k2eAgNnSc1d1	okupováno
posádkou	posádka	k1gFnSc7	posádka
Černé	Černá	k1gFnSc2	Černá
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomýlen	pomýlen	k2eAgMnSc1d1	pomýlen
napadne	napadnout	k5eAaPmIp3nS	napadnout
v	v	k7c6	v
potemnělém	potemnělý	k2eAgInSc6d1	potemnělý
domě	dům	k1gInSc6	dům
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
k	k	k7c3	k
mořskému	mořský	k2eAgNnSc3d1	mořské
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Černá	černý	k2eAgFnSc1d1	černá
loď	loď	k1gFnSc1	loď
teprve	teprve	k6eAd1	teprve
připlouvá	připlouvat	k5eAaImIp3nS	připlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Nezbývá	zbývat	k5eNaImIp3nS	zbývat
mu	on	k3xPp3gMnSc3	on
než	než	k8xS	než
doplavat	doplavat	k5eAaPmF	doplavat
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
a	a	k8xC	a
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
posádce	posádka	k1gFnSc3	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Reynolds	Reynolds	k6eAd1	Reynolds
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Veidt	Veidt	k1gInSc4	Veidt
si	se	k3xPyFc3	se
námořník	námořník	k1gMnSc1	námořník
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabrání	zabránit	k5eAaPmIp3nS	zabránit
katastrofě	katastrofa	k1gFnSc3	katastrofa
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
použije	použít	k5eAaPmIp3nS	použít
mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
těla	tělo	k1gNnPc4	tělo
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
příběh	příběh	k1gInSc1	příběh
Černé	Černé	k2eAgFnSc2d1	Černé
lodě	loď	k1gFnSc2	loď
končí	končit	k5eAaImIp3nS	končit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
příběh	příběh	k1gInSc1	příběh
Adriana	Adrian	k1gMnSc2	Adrian
Veidta	Veidt	k1gMnSc2	Veidt
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xC	jako
paralela	paralela	k1gFnSc1	paralela
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
částem	část	k1gFnPc3	část
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
třeba	třeba	k6eAd1	třeba
Rorschachovo	Rorschachův	k2eAgNnSc4d1	Rorschachův
zatčení	zatčení	k1gNnSc4	zatčení
nebo	nebo	k8xC	nebo
exil	exil	k1gInSc4	exil
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Manhattana	Manhattan	k1gMnSc2	Manhattan
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgInSc1d3	veliký
vliv	vliv	k1gInSc1	vliv
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
Strážců	strážce	k1gMnPc2	strážce
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Ric	k1gFnSc2	Ric
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jediném	jediný	k2eAgInSc6d1	jediný
komiksovém	komiksový	k2eAgInSc6d1	komiksový
stripu	strip	k1gInSc6	strip
The	The	k1gMnSc1	The
Unspeakable	Unspeakable	k1gMnSc1	Unspeakable
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Hart	Hart	k1gInSc1	Hart
opakuje	opakovat	k5eAaImIp3nS	opakovat
určité	určitý	k2eAgInPc4d1	určitý
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nesou	nést	k5eAaImIp3nP	nést
nějaký	nějaký	k3yIgInSc4	nějaký
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
souvislost	souvislost	k1gFnSc1	souvislost
byla	být	k5eAaImAgFnS	být
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
<g/>
;	;	kIx,	;
Moore	Moor	k1gInSc5	Moor
podotkl	podotknout	k5eAaPmAgMnS	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
některých	některý	k3yIgFnPc2	některý
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
tam	tam	k6eAd1	tam
dal	dát	k5eAaPmAgMnS	dát
Dave	Dav	k1gInSc5	Dav
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
až	až	k9	až
po	po	k7c6	po
šestém	šestý	k4xOgInSc6	šestý
<g/>
,	,	kIx,	,
sedmém	sedmý	k4xOgNnSc6	sedmý
přečtení	přečtení	k1gNnSc6	přečtení
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
něco	něco	k3yInSc1	něco
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
objevilo	objevit	k5eAaPmAgNnS	objevit
jen	jen	k9	jen
náhodou	náhodou	k6eAd1	náhodou
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zakrvácená	zakrvácený	k2eAgFnSc1d1	zakrvácená
smějící	smějící	k2eAgFnSc1d1	smějící
se	se	k3xPyFc4	se
tvář	tvář	k1gFnSc1	tvář
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
v	v	k7c6	v
několika	několik	k4yIc6	několik
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Thierry	Thierra	k1gFnPc1	Thierra
Groensteen	Groenstena	k1gFnPc2	Groenstena
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gMnSc7	The
System	Syst	k1gMnSc7	Syst
of	of	k?	of
Comics	comics	k1gInSc1	comics
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
navracející	navracející	k2eAgInSc4d1	navracející
se	se	k3xPyFc4	se
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
klíčových	klíčový	k2eAgFnPc6d1	klíčová
částech	část	k1gFnPc6	část
Strážců	strážce	k1gMnPc2	strážce
<g/>
,	,	kIx,	,
nejzřejmější	zřejmý	k2eAgMnSc1d3	nejzřejmější
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
první	první	k4xOgInSc4	první
a	a	k8xC	a
poslední	poslední	k2eAgFnSc6d1	poslední
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
smajlíka	smajlíka	k?	smajlíka
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Komediantova	komediantův	k2eAgInSc2d1	komediantův
kostýmu	kostým	k1gInSc2	kostým
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
trochu	trochu	k6eAd1	trochu
"	"	kIx"	"
<g/>
prosvětlil	prosvětlit	k5eAaPmAgMnS	prosvětlit
<g/>
"	"	kIx"	"
celkový	celkový	k2eAgInSc4d1	celkový
vzhled	vzhled	k1gInSc4	vzhled
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
přidal	přidat	k5eAaPmAgMnS	přidat
krvavou	krvavý	k2eAgFnSc4d1	krvavá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
jeho	jeho	k3xOp3gNnSc4	jeho
zavraždění	zavraždění	k1gNnSc4	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakrvácený	zakrvácený	k2eAgInSc1d1	zakrvácený
smajlík	smajlík	k?	smajlík
měli	mít	k5eAaImAgMnP	mít
autoři	autor	k1gMnPc1	autor
za	za	k7c4	za
"	"	kIx"	"
<g/>
symbol	symbol	k1gInSc4	symbol
celé	celý	k2eAgFnSc2d1	celá
série	série	k1gFnSc2	série
<g/>
"	"	kIx"	"
a	a	k8xC	a
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
hodinami	hodina	k1gFnPc7	hodina
odpočítávajícími	odpočítávající	k2eAgFnPc7d1	odpočítávající
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
v	v	k7c6	v
psychologickém	psychologický	k2eAgInSc6d1	psychologický
testu	test	k1gInSc6	test
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
smějící	smějící	k2eAgFnSc6d1	smějící
se	se	k3xPyFc4	se
tváři	tvář	k1gFnSc6	tvář
vidí	vidět	k5eAaImIp3nS	vidět
symbol	symbol	k1gInSc1	symbol
"	"	kIx"	"
<g/>
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
krvavá	krvavý	k2eAgFnSc1d1	krvavá
skvrna	skvrna	k1gFnSc1	skvrna
přes	přes	k7c4	přes
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
význam	význam	k1gInSc4	význam
tváře	tvář	k1gFnSc2	tvář
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
výstižný	výstižný	k2eAgInSc4d1	výstižný
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
obálku	obálka	k1gFnSc4	obálka
prvního	první	k4xOgNnSc2	první
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
symbolů	symbol	k1gInPc2	symbol
začleňovala	začleňovat	k5eAaImAgFnS	začleňovat
úmyslně	úmyslně	k6eAd1	úmyslně
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
náhodou	náhodou	k6eAd1	náhodou
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
konkrétně	konkrétně	k6eAd1	konkrétně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
zásuvky	zásuvka	k1gFnPc4	zásuvka
na	na	k7c6	na
hydrantech	hydrant	k1gInPc6	hydrant
<g/>
:	:	kIx,	:
když	když	k8xS	když
je	on	k3xPp3gInPc4	on
otočíte	otočit	k5eAaPmIp2nP	otočit
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
smějící	smějící	k2eAgFnSc4d1	smějící
se	se	k3xPyFc4	se
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgInPc1d1	další
symboly	symbol	k1gInPc1	symbol
<g/>
,	,	kIx,	,
vyobrazení	vyobrazení	k1gNnPc1	vyobrazení
a	a	k8xC	a
narážky	narážka	k1gFnPc1	narážka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevovaly	objevovat	k5eAaImAgFnP	objevovat
nečekaně	nečekaně	k6eAd1	nečekaně
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
všechno	všechen	k3xTgNnSc1	všechen
kolem	kolem	k7c2	kolem
Strážců	strážce	k1gMnPc2	strážce
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
nějak	nějak	k6eAd1	nějak
zapadá	zapadat	k5eAaPmIp3nS	zapadat
a	a	k8xC	a
odevšad	odevšad	k6eAd1	odevšad
to	ten	k3xDgNnSc1	ten
vyskakuje	vyskakovat	k5eAaImIp3nS	vyskakovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
kráterech	kráter	k1gInPc6	kráter
a	a	k8xC	a
horách	hora	k1gFnPc6	hora
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
Gibbons	Gibbons	k1gInSc1	Gibbons
objevil	objevit	k5eAaPmAgInS	objevit
fotografii	fotografia	k1gFnSc4	fotografia
kráteru	kráter	k1gInSc2	kráter
Galle	Galle	k1gInSc1	Galle
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
smějící	smějící	k2eAgFnSc4d1	smějící
se	se	k3xPyFc4	se
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
zapracoval	zapracovat	k5eAaPmAgMnS	zapracovat
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zjišťovali	zjišťovat	k5eAaImAgMnP	zjišťovat
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyhle	tenhle	k3xDgFnPc1	tenhle
věci	věc	k1gFnPc1	věc
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jako	jako	k8xC	jako
mávnutím	mávnutí	k1gNnSc7	mávnutí
kouzelného	kouzelný	k2eAgInSc2d1	kouzelný
proutku	proutek	k1gInSc2	proutek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
cituje	citovat	k5eAaBmIp3nS	citovat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
zámečnickou	zámečnický	k2eAgFnSc4d1	zámečnická
firmu	firma	k1gFnSc4	firma
"	"	kIx"	"
<g/>
Gordický	gordický	k2eAgInSc1d1	gordický
uzel	uzel	k1gInSc1	uzel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počátečním	počáteční	k2eAgInSc7d1	počáteční
předpokladem	předpoklad	k1gInSc7	předpoklad
bylo	být	k5eAaImAgNnS	být
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
,	,	kIx,	,
jací	jaký	k3yRgMnPc1	jaký
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
superhrdinové	superhrdinový	k2eAgFnPc4d1	superhrdinový
"	"	kIx"	"
<g/>
v	v	k7c6	v
uvěřitelném	uvěřitelný	k2eAgMnSc6d1	uvěřitelný
<g/>
,	,	kIx,	,
skutečném	skutečný	k2eAgInSc6d1	skutečný
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zápletka	zápletka	k1gFnSc1	zápletka
stávala	stávat	k5eAaImAgFnS	stávat
komplexnější	komplexní	k2eAgFnSc1d2	komplexnější
<g/>
,	,	kIx,	,
Moore	Moor	k1gInSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Strážci	strážce	k1gMnPc1	strážce
jsou	být	k5eAaImIp3nP	být
o	o	k7c6	o
"	"	kIx"	"
<g/>
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
ideji	idea	k1gFnSc6	idea
supermanství	supermanství	k1gNnSc2	supermanství
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
série	série	k1gFnSc2	série
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
frázi	fráze	k1gFnSc4	fráze
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
střeží	střežit	k5eAaImIp3nS	střežit
strážce	strážce	k1gMnSc1	strážce
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Moore	Moor	k1gInSc5	Moor
v	v	k7c6	v
interview	interview	k1gNnSc6	interview
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
tato	tento	k3xDgFnSc1	tento
fráze	fráze	k1gFnSc1	fráze
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
sci-fi	scii	k1gNnSc2	sci-fi
Harlan	Harlan	k1gMnSc1	Harlan
Ellison	Ellison	k1gMnSc1	Ellison
tento	tento	k3xDgInSc4	tento
rozhovor	rozhovor	k1gInSc4	rozhovor
přečetl	přečíst	k5eAaPmAgMnS	přečíst
<g/>
,	,	kIx,	,
upozornil	upozornit	k5eAaPmAgMnS	upozornit
Moorea	Moorea	k1gMnSc1	Moorea
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
fráze	fráze	k1gFnSc1	fráze
je	být	k5eAaImIp3nS	být
překladem	překlad	k1gInSc7	překlad
otázky	otázka	k1gFnSc2	otázka
"	"	kIx"	"
<g/>
Quis	Quis	k1gInSc1	Quis
custodiet	custodiet	k1gMnSc1	custodiet
ipsos	ipsos	k1gMnSc1	ipsos
custodes	custodes	k1gMnSc1	custodes
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nastolil	nastolit	k5eAaPmAgMnS	nastolit
římský	římský	k2eAgMnSc1d1	římský
satirik	satirik	k1gMnSc1	satirik
Juvenalis	Juvenalis	k1gFnSc2	Juvenalis
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
Strážců	strážce	k1gMnPc2	strážce
to	ten	k3xDgNnSc1	ten
sedí	sedit	k5eAaImIp3nS	sedit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Střeží	střežit	k5eAaImIp3nS	střežit
nás	my	k3xPp1nPc4	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kdo	kdo	k3yQnSc1	kdo
střeží	střežit	k5eAaImIp3nS	střežit
je	on	k3xPp3gNnPc4	on
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Strážce	strážce	k1gMnSc1	strážce
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
oprostit	oprostit	k5eAaPmF	oprostit
se	se	k3xPyFc4	se
od	od	k7c2	od
superhrdinské	superhrdinský	k2eAgFnSc2d1	superhrdinská
nostalgie	nostalgie	k1gFnSc2	nostalgie
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
našel	najít	k5eAaPmAgMnS	najít
ve	v	k7c6	v
skutečných	skutečný	k2eAgFnPc6d1	skutečná
lidských	lidský	k2eAgFnPc6d1	lidská
bytostech	bytost	k1gFnPc6	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Bradford	Bradford	k1gMnSc1	Bradford
Wright	Wright	k1gMnSc1	Wright
popsal	popsat	k5eAaPmAgMnS	popsat
Strážce	strážce	k1gMnSc1	strážce
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nekrolog	nekrolog	k1gInSc1	nekrolog
hrdinského	hrdinský	k2eAgNnSc2d1	hrdinské
pojetí	pojetí	k1gNnSc2	pojetí
a	a	k8xC	a
superhrdinského	superhrdinský	k2eAgMnSc2d1	superhrdinský
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zasadil	zasadit	k5eAaPmAgMnS	zasadit
Strážce	strážce	k1gMnSc1	strážce
do	do	k7c2	do
soudobého	soudobý	k2eAgInSc2d1	soudobý
společenského	společenský	k2eAgInSc2d1	společenský
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Wrighta	Wright	k1gInSc2	Wright
Moore	Moor	k1gInSc5	Moor
"	"	kIx"	"
<g/>
varoval	varovat	k5eAaImAgMnS	varovat
ty	ten	k3xDgInPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
svěřili	svěřit	k5eAaPmAgMnP	svěřit
svou	svůj	k3xOyFgFnSc4	svůj
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
'	'	kIx"	'
<g/>
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
'	'	kIx"	'
a	a	k8xC	a
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svěřit	svěřit	k5eAaPmF	svěřit
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
takovým	takový	k3xDgInPc3	takový
ikonám	ikona	k1gFnPc3	ikona
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
"	"	kIx"	"
<g/>
všech	všecek	k3xTgMnPc2	všecek
těch	ten	k3xDgMnPc2	ten
Reaganů	Reagan	k1gMnPc2	Reagan
<g/>
,	,	kIx,	,
Thatcherových	Thatcherův	k2eAgMnPc2d1	Thatcherův
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
světových	světový	k2eAgMnPc2d1	světový
'	'	kIx"	'
<g/>
strážců	strážce	k1gMnPc2	strážce
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nás	my	k3xPp1nPc4	my
měli	mít	k5eAaImAgMnP	mít
'	'	kIx"	'
<g/>
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
třeba	třeba	k6eAd1	třeba
zničí	zničit	k5eAaPmIp3nS	zničit
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Moore	Moor	k1gMnSc5	Moor
výslovně	výslovně	k6eAd1	výslovně
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepsal	psát	k5eNaImAgMnS	psát
Strážce	strážce	k1gMnSc1	strážce
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
antiamerikanismus	antiamerikanismus	k1gInSc4	antiamerikanismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
antireaganismus	antireaganismus	k1gInSc1	antireaganismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
Reaganovy	Reaganův	k2eAgFnSc2d1	Reaganova
Ameriky	Amerika	k1gFnSc2	Amerika
ničeho	nic	k3yNnSc2	nic
nebála	bát	k5eNaImAgNnP	bát
<g/>
.	.	kIx.	.
</s>
<s>
Mysleli	myslet	k5eAaImAgMnP	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
nezranitelní	zranitelný	k2eNgMnPc1d1	nezranitelný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
I	i	k9	i
když	když	k8xS	když
chtěl	chtít	k5eAaImAgMnS	chtít
Moore	Moor	k1gInSc5	Moor
psát	psát	k5eAaImF	psát
o	o	k7c6	o
"	"	kIx"	"
<g/>
vysoké	vysoký	k2eAgFnSc6d1	vysoká
politice	politika	k1gFnSc6	politika
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c6	o
obavách	obava	k1gFnPc6	obava
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
děj	děj	k1gInSc1	děj
zasadil	zasadit	k5eAaPmAgInS	zasadit
do	do	k7c2	do
alternativní	alternativní	k2eAgFnSc2d1	alternativní
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
čtenáři	čtenář	k1gMnPc1	čtenář
jednoduše	jednoduše	k6eAd1	jednoduše
"	"	kIx"	"
<g/>
vypnuli	vypnout	k5eAaPmAgMnP	vypnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
obdivovali	obdivovat	k5eAaImAgMnP	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
neustále	neustále	k6eAd1	neustále
snažil	snažit	k5eAaImAgMnS	snažit
psát	psát	k5eAaImF	psát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
lidem	člověk	k1gMnPc3	člověk
nebylo	být	k5eNaImAgNnS	být
příjemné	příjemný	k2eAgNnSc1d1	příjemné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Strážci	strážce	k1gMnPc1	strážce
jsou	být	k5eAaImIp3nP	být
citováni	citován	k2eAgMnPc1d1	citován
jako	jako	k8xS	jako
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
komiks	komiks	k1gInSc1	komiks
"	"	kIx"	"
<g/>
dospěl	dochvít	k5eAaPmAgInS	dochvít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Iain	Iain	k1gMnSc1	Iain
Thomson	Thomson	k1gMnSc1	Thomson
napsal	napsat	k5eAaPmAgMnS	napsat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
eseji	esej	k1gInSc6	esej
"	"	kIx"	"
<g/>
Deconstructing	Deconstructing	k1gInSc1	Deconstructing
the	the	k?	the
Hero	Hero	k1gNnSc1	Hero
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
děj	děj	k1gInSc1	děj
toho	ten	k3xDgNnSc2	ten
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
své	svůj	k3xOyFgMnPc4	svůj
hrdiny	hrdina	k1gMnPc4	hrdina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zbořil	zbořit	k5eAaPmAgInS	zbořit
samé	samý	k3xTgInPc4	samý
základy	základ	k1gInPc4	základ
hrdinství	hrdinství	k1gNnSc2	hrdinství
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
nás	my	k3xPp1nPc4	my
donutil	donutit	k5eAaPmAgMnS	donutit
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
významu	význam	k1gInSc6	význam
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
úhlů	úhel	k1gInPc2	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Thomson	Thomson	k1gMnSc1	Thomson
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
hrdinové	hrdina	k1gMnPc1	hrdina
ze	z	k7c2	z
Strážců	strážce	k1gMnPc2	strážce
sdílejí	sdílet	k5eAaImIp3nP	sdílet
"	"	kIx"	"
<g/>
nihilistický	nihilistický	k2eAgInSc4d1	nihilistický
<g/>
"	"	kIx"	"
náhled	náhled	k1gInSc4	náhled
a	a	k8xC	a
že	že	k8xS	že
Moore	Moor	k1gInSc5	Moor
tento	tento	k3xDgInSc1	tento
vzhled	vzhled	k1gInSc4	vzhled
předkládá	předkládat	k5eAaImIp3nS	předkládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
<g/>
,	,	kIx,	,
nepřikrášlenou	přikrášlený	k2eNgFnSc4d1	nepřikrášlená
pravdu	pravda	k1gFnSc4	pravda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
rozložil	rozložit	k5eAaPmAgInS	rozložit
základní	základní	k2eAgFnSc4d1	základní
motivaci	motivace	k1gFnSc4	motivace
rádobyhrdinů	rádobyhrdin	k1gInPc2	rádobyhrdin
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
snahu	snaha	k1gFnSc4	snaha
přinést	přinést	k5eAaPmF	přinést
světu	svět	k1gInSc3	svět
spásu	spása	k1gFnSc4	spása
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
bytost	bytost	k1gFnSc4	bytost
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
příběh	příběh	k1gInSc1	příběh
"	"	kIx"	"
<g/>
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
hrdiny	hrdina	k1gMnPc4	hrdina
tak	tak	k9	tak
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
ptali	ptat	k5eAaImAgMnP	ptat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
nám	my	k3xPp1nPc3	my
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
nebylo	být	k5eNaImAgNnS	být
lépe	dobře	k6eAd2	dobře
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Thomson	Thomson	k1gMnSc1	Thomson
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
dekonstrukce	dekonstrukce	k1gFnSc2	dekonstrukce
hrdinské	hrdinský	k2eAgFnSc2d1	hrdinská
koncepce	koncepce	k1gFnSc2	koncepce
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
čas	čas	k1gInSc1	čas
pro	pro	k7c4	pro
hrdiny	hrdina	k1gMnPc4	hrdina
již	již	k6eAd1	již
uplynul	uplynout	k5eAaPmAgInS	uplynout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
postmoderní	postmoderní	k2eAgFnSc1d1	postmoderní
práce	práce	k1gFnSc1	práce
<g/>
"	"	kIx"	"
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
dekonstrukce	dekonstrukce	k1gFnSc2	dekonstrukce
hrdinství	hrdinství	k1gNnSc2	hrdinství
během	během	k7c2	během
existenciálního	existenciální	k2eAgNnSc2d1	existenciální
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Reynolds	Reynoldsa	k1gFnPc2	Reynoldsa
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
padouchů	padouch	k1gMnPc2	padouch
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
jsou	být	k5eAaImIp3nP	být
superhrdinové	superhrdinový	k2eAgInPc1d1	superhrdinový
ze	z	k7c2	z
Strážců	strážce	k1gMnPc2	strážce
nuceni	nutit	k5eAaImNgMnP	nutit
ke	k	k7c3	k
konfrontaci	konfrontace	k1gFnSc3	konfrontace
s	s	k7c7	s
"	"	kIx"	"
<g/>
nehmotnými	hmotný	k2eNgFnPc7d1	nehmotná
společenskými	společenský	k2eAgFnPc7d1	společenská
a	a	k8xC	a
morálními	morální	k2eAgNnPc7d1	morální
dilematy	dilema	k1gNnPc7	dilema
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dílo	dílo	k1gNnSc4	dílo
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
žánrové	žánrový	k2eAgFnSc2d1	žánrová
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Reynolds	Reynolds	k6eAd1	Reynolds
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
ironické	ironický	k2eAgNnSc1d1	ironické
sebeuvědomění	sebeuvědomění	k1gNnSc1	sebeuvědomění
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Strážci	strážce	k1gMnPc1	strážce
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
poslední	poslední	k2eAgInSc4d1	poslední
zásadní	zásadní	k2eAgInSc4d1	zásadní
superhrdinský	superhrdinský	k2eAgInSc4d1	superhrdinský
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
první	první	k4xOgMnSc1	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
dospělých	dospělý	k2eAgMnPc2d1	dospělý
zástupců	zástupce	k1gMnPc2	zástupce
žánru	žánr	k1gInSc2	žánr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Geoff	Geoff	k1gMnSc1	Geoff
Klock	Klock	k1gMnSc1	Klock
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
dekonstrukce	dekonstrukce	k1gFnSc1	dekonstrukce
<g/>
"	"	kIx"	"
a	a	k8xC	a
raději	rád	k6eAd2	rád
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
"	"	kIx"	"
<g/>
novém	nový	k2eAgInSc6d1	nový
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
superhrdinský	superhrdinský	k2eAgInSc4d1	superhrdinský
žánr	žánr	k1gInSc4	žánr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS	považovat
Strážce	strážce	k1gMnSc1	strážce
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Návratem	návrat	k1gInSc7	návrat
temného	temný	k2eAgMnSc2d1	temný
rytíře	rytíř	k1gMnSc2	rytíř
od	od	k7c2	od
Franka	Frank	k1gMnSc2	Frank
Millera	Miller	k1gMnSc2	Miller
za	za	k7c7	za
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc4	první
ukázku	ukázka	k1gFnSc4	ukázka
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
komiksu	komiks	k1gInSc2	komiks
<g/>
...	...	k?	...
první	první	k4xOgFnSc3	první
fázi	fáze	k1gFnSc3	fáze
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc1	přechod
superhrdinů	superhrdin	k1gInPc2	superhrdin
z	z	k7c2	z
fantazie	fantazie	k1gFnSc2	fantazie
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
zděšení	zděšení	k1gNnSc4	zděšení
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nelehký	lehký	k2eNgInSc1d1	nelehký
<g/>
,	,	kIx,	,
dekonstruující	dekonstruující	k2eAgInSc1d1	dekonstruující
<g/>
,	,	kIx,	,
postmoderní	postmoderní	k2eAgInSc1d1	postmoderní
komiks	komiks	k1gInSc1	komiks
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
představitelem	představitel	k1gMnSc7	představitel
jsou	být	k5eAaImIp3nP	být
Strážci	strážce	k1gMnPc1	strážce
<g/>
...	...	k?	...
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
žánrem	žánr	k1gInSc7	žánr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Během	během	k7c2	během
15	[number]	k4	15
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Strážců	strážce	k1gMnPc2	strážce
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
spoustu	spousta	k1gFnSc4	spousta
komiksů	komiks	k1gInPc2	komiks
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívaly	využívat	k5eAaPmAgFnP	využívat
ponurost	ponurost	k1gFnSc4	ponurost
<g/>
,	,	kIx,	,
pesimistický	pesimistický	k2eAgInSc4d1	pesimistický
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
ošklivost	ošklivost	k1gFnSc4	ošklivost
nebo	nebo	k8xC	nebo
násilí	násilí	k1gNnSc4	násilí
Strážců	strážce	k1gMnPc2	strážce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ospravedlnily	ospravedlnit	k5eAaPmAgFnP	ospravedlnit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
nechutnými	chutný	k2eNgInPc7d1	nechutný
příběhy	příběh	k1gInPc7	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
nemají	mít	k5eNaImIp3nP	mít
moc	moc	k6eAd1	moc
co	co	k3yRnSc4	co
nabídnout	nabídnout	k5eAaPmF	nabídnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gibbons	Gibbons	k1gInSc1	Gibbons
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
čtenáři	čtenář	k1gMnPc1	čtenář
měli	mít	k5eAaImAgMnP	mít
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
ponurou	ponurý	k2eAgFnSc4d1	ponurá
<g/>
,	,	kIx,	,
těžkou	těžký	k2eAgFnSc4d1	těžká
věc	věc	k1gFnSc4	věc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
na	na	k7c4	na
sérii	série	k1gFnSc4	série
pohlížel	pohlížet	k5eAaImAgMnS	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
"	"	kIx"	"
<g/>
skvělou	skvělý	k2eAgFnSc4d1	skvělá
oslavu	oslava	k1gFnSc4	oslava
superhrdinů	superhrdin	k1gMnPc2	superhrdin
-	-	kIx~	-
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgInSc2d1	jiný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Moore	Moor	k1gInSc5	Moor
s	s	k7c7	s
Gibbonsem	Gibbons	k1gMnSc7	Gibbons
odevzdali	odevzdat	k5eAaPmAgMnP	odevzdat
DC	DC	kA	DC
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
kolegové	kolega	k1gMnPc1	kolega
byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
omráčeni	omráčen	k2eAgMnPc1d1	omráčen
<g/>
.	.	kIx.	.
</s>
<s>
Gibbons	Gibbons	k6eAd1	Gibbons
si	se	k3xPyFc3	se
vybavil	vybavit	k5eAaPmAgMnS	vybavit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Opravdu	opravdu	k6eAd1	opravdu
se	se	k3xPyFc4	se
toho	ten	k3xDgInSc2	ten
chytil	chytit	k5eAaPmAgMnS	chytit
Howard	Howard	k1gMnSc1	Howard
Chaykin	Chaykin	k1gMnSc1	Chaykin
[	[	kIx(	[
<g/>
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechválí	chválit	k5eNaImIp3nS	chválit
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
'	'	kIx"	'
<g/>
Dave	Dav	k1gInSc5	Dav
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jste	být	k5eAaImIp2nP	být
udělali	udělat	k5eAaPmAgMnP	udělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
pecka	pecka	k1gFnSc1	pecka
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
Moore	Moor	k1gInSc5	Moor
slyšet	slyšet	k5eAaImF	slyšet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
DC	DC	kA	DC
nás	my	k3xPp1nPc2	my
podporovalo	podporovat	k5eAaImAgNnS	podporovat
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
...	...	k?	...
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
i	i	k9	i
ty	ten	k3xDgInPc4	ten
největší	veliký	k2eAgInPc4d3	veliký
grafické	grafický	k2eAgInPc4d1	grafický
excesy	exces	k1gInPc4	exces
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k8xC	jako
propagaci	propagace	k1gFnSc6	propagace
DC	DC	kA	DC
vydalo	vydat	k5eAaPmAgNnS	vydat
limitovanou	limitovaný	k2eAgFnSc4d1	limitovaná
sérii	série	k1gFnSc4	série
odznaků	odznak	k1gInPc2	odznak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zobrazovaly	zobrazovat	k5eAaImAgInP	zobrazovat
hrdiny	hrdina	k1gMnPc7	hrdina
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
symboly	symbol	k1gInPc4	symbol
z	z	k7c2	z
komiksu	komiks	k1gInSc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
10	[number]	k4	10
000	[number]	k4	000
sad	sada	k1gFnPc2	sada
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
odznáčcích	odznáček	k1gInPc6	odznáček
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
repliky	replika	k1gFnSc2	replika
Komediantova	komediantův	k2eAgInSc2d1	komediantův
smajlíku	smajlíku	k?	smajlíku
<g/>
.	.	kIx.	.
</s>
<s>
Mayfair	Mayfair	k1gMnSc1	Mayfair
Games	Games	k1gMnSc1	Games
vydalo	vydat	k5eAaPmAgNnS	vydat
modul	modul	k1gInSc1	modul
Strážci	strážce	k1gMnSc3	strážce
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
ještě	ještě	k6eAd1	ještě
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
série	série	k1gFnSc1	série
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Modul	modul	k1gInSc1	modul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Moore	Moor	k1gInSc5	Moor
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
události	událost	k1gFnSc3	událost
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
přidává	přidávat	k5eAaImIp3nS	přidávat
tak	tak	k6eAd1	tak
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
příběhu	příběh	k1gInSc2	příběh
některé	některý	k3yIgFnPc4	některý
podrobnosti	podrobnost	k1gFnPc4	podrobnost
<g/>
.	.	kIx.	.
</s>
<s>
Strážci	strážce	k1gMnPc1	strážce
byli	být	k5eAaImAgMnP	být
publikováni	publikován	k2eAgMnPc1d1	publikován
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
až	až	k9	až
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
série	série	k1gFnSc1	série
měla	mít	k5eAaImAgFnS	mít
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
prodeje	prodej	k1gFnPc1	prodej
dopomohly	dopomoct	k5eAaPmAgFnP	dopomoct
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
krátce	krátce	k6eAd1	krátce
předstihlo	předstihnout	k5eAaPmAgNnS	předstihnout
svého	svůj	k3xOyFgMnSc4	svůj
konkurenta	konkurent	k1gMnSc4	konkurent
Marvel	Marvel	k1gInSc4	Marvel
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Vydávání	vydávání	k1gNnSc1	vydávání
se	se	k3xPyFc4	se
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc1	část
místo	místo	k7c2	místo
šesti	šest	k4xCc2	šest
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
zpoždění	zpoždění	k1gNnSc1	zpoždění
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
dobou	doba	k1gFnSc7	doba
vytváření	vytváření	k1gNnSc2	vytváření
jednoho	jeden	k4xCgNnSc2	jeden
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
série	série	k1gFnSc1	série
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
čísla	číslo	k1gNnPc1	číslo
byla	být	k5eAaImAgNnP	být
spojena	spojen	k2eAgFnSc1d1	spojena
a	a	k8xC	a
prodávána	prodáván	k2eAgFnSc1d1	prodávána
v	v	k7c6	v
paperbackové	paperbackový	k2eAgFnSc6d1	paperbacková
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
publicity	publicita	k1gFnSc2	publicita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
série	série	k1gFnSc1	série
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
získala	získat	k5eAaPmAgFnS	získat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
ucelené	ucelený	k2eAgFnSc6d1	ucelená
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
Graphitti	Graphitti	k1gNnPc2	Graphitti
Design	design	k1gInSc1	design
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
speciální	speciální	k2eAgFnSc4d1	speciální
limitovanou	limitovaný	k2eAgFnSc4d1	limitovaná
edici	edice	k1gFnSc4	edice
v	v	k7c6	v
pevné	pevný	k2eAgFnSc6d1	pevná
vazbě	vazba	k1gFnSc6	vazba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
48	[number]	k4	48
stran	strana	k1gFnPc2	strana
bonusového	bonusový	k2eAgInSc2d1	bonusový
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
původního	původní	k2eAgInSc2d1	původní
návrhu	návrh	k1gInSc2	návrh
a	a	k8xC	a
konceptu	koncept	k1gInSc2	koncept
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
DC	DC	kA	DC
vydali	vydat	k5eAaPmAgMnP	vydat
Absolute	Absolut	k1gInSc5	Absolut
Watchmen	Watchmen	k1gInSc1	Watchmen
<g/>
,	,	kIx,	,
tohle	tenhle	k3xDgNnSc1	tenhle
vydání	vydání	k1gNnSc1	vydání
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bonusové	bonusový	k2eAgInPc4d1	bonusový
materiály	materiál	k1gInPc4	materiál
od	od	k7c2	od
Graphitti	Graphitť	k1gFnSc2	Graphitť
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kreseb	kresba	k1gFnPc2	kresba
vybarvených	vybarvený	k2eAgFnPc2d1	vybarvená
Johnem	John	k1gMnSc7	John
Higginsem	Higgins	k1gMnSc7	Higgins
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Warner	Warnero	k1gNnPc2	Warnero
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
</s>
<s>
Entertainment	Entertainment	k1gMnSc1	Entertainment
vydali	vydat	k5eAaPmAgMnP	vydat
Watchmen	Watchmen	k2eAgInSc4d1	Watchmen
Motion	Motion	k1gInSc4	Motion
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
komiksu	komiks	k1gInSc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kapitola	kapitola	k1gFnSc1	kapitola
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
DC	DC	kA	DC
vydalo	vydat	k5eAaPmAgNnS	vydat
nový	nový	k2eAgInSc1d1	nový
výtisk	výtisk	k1gInSc1	výtisk
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
Strážců	strážce	k1gMnPc2	strážce
za	za	k7c4	za
původní	původní	k2eAgFnSc4d1	původní
cenu	cena	k1gFnSc4	cena
1,5	[number]	k4	1,5
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgMnS	prodávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Strážce	strážce	k1gMnSc4	strážce
chválila	chválit	k5eAaImAgFnS	chválit
kritika	kritika	k1gFnSc1	kritika
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Time	Tim	k1gFnSc2	Tim
ho	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
komiksů	komiks	k1gInPc2	komiks
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
vynikající	vynikající	k2eAgFnSc4d1	vynikající
imaginaci	imaginace	k1gFnSc4	imaginace
kombinující	kombinující	k2eAgFnSc4d1	kombinující
sci-fi	scii	k1gFnSc4	sci-fi
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc4d1	politická
satiru	satira	k1gFnSc4	satira
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
směle	směle	k6eAd1	směle
přepracovává	přepracovávat	k5eAaImIp3nS	přepracovávat
současný	současný	k2eAgInSc4d1	současný
komiksový	komiksový	k2eAgInSc4d1	komiksový
formát	formát	k1gInSc4	formát
v	v	k7c4	v
dysutopickou	dysutopický	k2eAgFnSc4d1	dysutopický
[	[	kIx(	[
<g/>
sic	sic	k8xC	sic
<g/>
]	]	kIx)	]
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
zápletku	zápletka	k1gFnSc4	zápletka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
získali	získat	k5eAaPmAgMnP	získat
Strážci	strážce	k1gMnPc1	strážce
cenu	cena	k1gFnSc4	cena
Hugo	Hugo	k1gMnSc1	Hugo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Other	Other	k1gInSc1	Other
Forms	Forms	k1gInSc1	Forms
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zařadil	zařadit	k5eAaPmAgInS	zařadit
The	The	k1gFnSc2	The
Comics	comics	k1gInSc1	comics
Journal	Journal	k1gMnSc1	Journal
Strážce	strážce	k1gMnSc1	strážce
na	na	k7c4	na
91	[number]	k4	91
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
žebříčku	žebříček	k1gInSc6	žebříček
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
anglicky	anglicky	k6eAd1	anglicky
psaných	psaný	k2eAgInPc2d1	psaný
komiksů	komiks	k1gInPc2	komiks
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Strážci	strážce	k1gMnPc1	strážce
byli	být	k5eAaImAgMnP	být
také	také	k6eAd1	také
jediným	jediný	k2eAgInSc7d1	jediný
komiksem	komiks	k1gInSc7	komiks
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
časopisu	časopis	k1gInSc2	časopis
Time	Tim	k1gFnSc2	Tim
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
All-TIME	All-TIME	k1gFnPc2	All-TIME
100	[number]	k4	100
Greatest	Greatest	k1gFnSc4	Greatest
Novels	Novelsa	k1gFnPc2	Novelsa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
dokonce	dokonce	k9	dokonce
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
série	série	k1gFnSc1	série
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
by	by	kYmCp3nP	by
s	s	k7c7	s
Gibbonsem	Gibbons	k1gMnSc7	Gibbons
vytvořit	vytvořit	k5eAaPmF	vytvořit
dvanáctidílný	dvanáctidílný	k2eAgInSc4d1	dvanáctidílný
prequel	prequel	k1gInSc4	prequel
s	s	k7c7	s
názvem	název	k1gInSc7	název
Minutemen	Minutemen	k1gInSc1	Minutemen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
superhrdinové	superhrdinový	k2eAgMnPc4d1	superhrdinový
ze	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
Mooreovi	Mooreův	k2eAgMnPc1d1	Mooreův
a	a	k8xC	a
Gibbonsovi	Gibbonsův	k2eAgMnPc1d1	Gibbonsův
vydání	vydání	k1gNnPc4	vydání
prequelů	prequel	k1gMnPc2	prequel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Rorschachův	Rorschachův	k2eAgInSc1d1	Rorschachův
deník	deník	k1gInSc1	deník
nebo	nebo	k8xC	nebo
Komediantův	komediantův	k2eAgInSc1d1	komediantův
deník	deník	k1gInSc1	deník
z	z	k7c2	z
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
tento	tento	k3xDgInSc1	tento
svět	svět	k1gInSc1	svět
jiným	jiný	k2eAgMnPc3d1	jiný
autorům	autor	k1gMnPc3	autor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
především	především	k6eAd1	především
Moore	Moor	k1gMnSc5	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Gibbonsovi	Gibbons	k1gMnSc3	Gibbons
se	se	k3xPyFc4	se
nápad	nápad	k1gInSc1	nápad
s	s	k7c7	s
Minutemany	Minuteman	k1gInPc7	Minuteman
líbil	líbit	k5eAaImAgMnS	líbit
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
složil	složit	k5eAaPmAgInS	složit
poctu	pocta	k1gFnSc4	pocta
jednoduchosti	jednoduchost	k1gFnSc2	jednoduchost
a	a	k8xC	a
ryzosti	ryzost	k1gFnSc2	ryzost
komiksům	komiks	k1gInPc3	komiks
ze	z	k7c2	z
zlatých	zlatá	k1gFnPc2	zlatá
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
by	by	kYmCp3nS	by
dramatičnost	dramatičnost	k1gFnSc4	dramatičnost
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rozuzlení	rozuzlení	k1gNnSc4	rozuzlení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
zakončení	zakončení	k1gNnSc3	zakončení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rozpory	rozpora	k1gFnPc1	rozpora
ohledně	ohledně	k7c2	ohledně
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
příběhu	příběh	k1gInSc2	příběh
nakonec	nakonec	k6eAd1	nakonec
Moorea	Mooreum	k1gNnSc2	Mooreum
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
s	s	k7c7	s
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
na	na	k7c6	na
Comic-Conu	Comic-Con	k1gInSc6	Comic-Con
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
Moore	Moor	k1gInSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
tomu	ten	k3xDgNnSc3	ten
dobře	dobře	k6eAd1	dobře
rozumím	rozumět	k5eAaImIp1nS	rozumět
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
DC	DC	kA	DC
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
práva	právo	k1gNnPc4	právo
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydávají	vydávat	k5eAaPmIp3nP	vydávat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
mně	já	k3xPp1nSc6	já
a	a	k8xC	a
Daveovi	Daveův	k2eAgMnPc1d1	Daveův
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Za	za	k7c4	za
Strážce	strážce	k1gMnSc4	strážce
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Gibbons	Gibbons	k1gInSc1	Gibbons
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
8	[number]	k4	8
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
výdělku	výdělek	k1gInSc2	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
nebude	být	k5eNaImBp3nS	být
DC	DC	kA	DC
postavy	postava	k1gFnPc4	postava
využívat	využívat	k5eAaPmF	využívat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
naše	náš	k3xOp1gFnPc4	náš
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Moore	Moor	k1gMnSc5	Moor
i	i	k8xC	i
Gibbons	Gibbonsa	k1gFnPc2	Gibbonsa
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
společnost	společnost	k1gFnSc1	společnost
DC	DC	kA	DC
Comics	comics	k1gInSc4	comics
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
"	"	kIx"	"
<g/>
značnou	značný	k2eAgFnSc4d1	značná
finanční	finanční	k2eAgFnSc4d1	finanční
částku	částka	k1gFnSc4	částka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
práva	práv	k2eAgFnSc1d1	práva
udržela	udržet	k5eAaPmAgFnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
opustil	opustit	k5eAaPmAgMnS	opustit
DC	DC	kA	DC
Comics	comics	k1gInSc4	comics
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
kvůli	kvůli	k7c3	kvůli
smlouvám	smlouvat	k5eAaImIp1nS	smlouvat
týkajících	týkající	k2eAgNnPc2d1	týkající
se	se	k3xPyFc4	se
Strážců	strážce	k1gMnPc2	strážce
a	a	k8xC	a
dalšího	další	k1gNnSc2	další
jeho	on	k3xPp3gInSc2	on
komiksu	komiks	k1gInSc2	komiks
V	V	kA	V
jako	jako	k8xC	jako
Vendeta	vendeta	k1gFnSc1	vendeta
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
návrat	návrat	k1gInSc4	návrat
práva	práv	k2eAgFnSc1d1	práva
autorům	autor	k1gMnPc3	autor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
prakticky	prakticky	k6eAd1	prakticky
bezvýznamná	bezvýznamný	k2eAgNnPc1d1	bezvýznamné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
DC	DC	kA	DC
nikdy	nikdy	k6eAd1	nikdy
nemělo	mít	k5eNaImAgNnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
přestat	přestat	k5eAaPmF	přestat
komiks	komiks	k1gInSc4	komiks
vydávat	vydávat	k5eAaImF	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
The	The	k1gFnSc4	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Řekl	říct	k5eAaPmAgMnS	říct
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
no	no	k9	no
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
mě	já	k3xPp1nSc4	já
podvést	podvést	k5eAaPmF	podvést
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pro	pro	k7c4	pro
vás	vy	k3xPp2nPc4	vy
už	už	k9	už
nebudu	být	k5eNaImBp1nS	být
nikdy	nikdy	k6eAd1	nikdy
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
distancoval	distancovat	k5eAaBmAgInS	distancovat
od	od	k7c2	od
plánů	plán	k1gInPc2	plán
DC	DC	kA	DC
ohledně	ohledně	k7c2	ohledně
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
výročí	výročí	k1gNnSc2	výročí
Strážců	strážce	k1gMnPc2	strážce
a	a	k8xC	a
vydání	vydání	k1gNnSc4	vydání
řady	řada	k1gFnSc2	řada
figurek	figurka	k1gFnPc2	figurka
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
15	[number]	k4	15
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc1	výročí
Strážců	strážce	k1gMnPc2	strážce
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
jenom	jenom	k9	jenom
15	[number]	k4	15
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
DC	DC	kA	DC
vzalo	vzít	k5eAaPmAgNnS	vzít
Strážce	strážce	k1gMnPc4	strážce
mně	já	k3xPp1nSc6	já
a	a	k8xC	a
Daveovi	Daveův	k2eAgMnPc1d1	Daveův
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
DC	DC	kA	DC
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
vydání	vydání	k1gNnSc1	vydání
figurek	figurka	k1gFnPc2	figurka
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
jejich	jejich	k3xOp3gInPc4	jejich
návrhy	návrh	k1gInPc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
vydalo	vydat	k5eAaPmAgNnS	vydat
prequel	prequelit	k5eAaPmRp2nS	prequelit
Before	Befor	k1gInSc5	Befor
Watchmen	Watchmen	k2eAgInSc4d1	Watchmen
o	o	k7c6	o
37	[number]	k4	37
sešitech	sešit	k1gInPc6	sešit
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
ani	ani	k8xC	ani
Gibbons	Gibbons	k1gInSc1	Gibbons
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
nepodíleli	podílet	k5eNaImAgMnP	podílet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Watchmen	Watchmen	k1gInSc1	Watchmen
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
komiksů	komiks	k1gInPc2	komiks
Strážci	strážce	k1gMnPc1	strážce
-	-	kIx~	-
Watchmen	Watchmen	k2eAgInSc4d1	Watchmen
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Watchmen	Watchmen	k1gInSc1	Watchmen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Watchmen	Watchmen	k1gInSc4	Watchmen
Wiki	Wike	k1gFnSc4	Wike
Komentář	komentář	k1gInSc1	komentář
od	od	k7c2	od
Douga	Doug	k1gMnSc2	Doug
Atkinsona	Atkinson	k1gMnSc2	Atkinson
</s>
