<s>
Žralok	žralok	k1gMnSc1
bílý	bílý	k1gMnSc1
(	(	kIx(
<g/>
Carcharodon	Carcharodon	k1gMnSc1
carcharias	carcharias	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
velký	velký	k2eAgMnSc1d1
bílý	bílý	k1gMnSc1
žralok	žralok	k1gMnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
často	často	k6eAd1
nazývaný	nazývaný	k2eAgMnSc1d1
žralok	žralok	k1gMnSc1
lidožravý	lidožravý	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
žralok	žralok	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
lamnovití	lamnovitý	k2eAgMnPc1d1
vyskytující	vyskytující	k2eAgFnSc7d1
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
v	v	k7c6
pobřežních	pobřežní	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
<g/>
.	.	kIx.
</s>