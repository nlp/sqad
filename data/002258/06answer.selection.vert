<s>
Skotské	skotský	k2eAgNnSc1d1	skotské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1707	[number]	k4	1707
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
unii	unie	k1gFnSc4	unie
s	s	k7c7	s
Anglickým	anglický	k2eAgNnSc7d1	anglické
královstvím	království	k1gNnSc7	království
(	(	kIx(	(
<g/>
v	v	k7c6	v
pouze	pouze	k6eAd1	pouze
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Anglickým	anglický	k2eAgNnSc7d1	anglické
královstvím	království	k1gNnSc7	království
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
