<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1537	[number]	k4	1537
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
předzíváno	předzívat	k5eAaImNgNnS	předzívat
"	"	kIx"	"
<g/>
Matka	matka	k1gFnSc1	matka
měst	město	k1gNnPc2	město
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Madre	Madr	k1gMnSc5	Madr
de	de	k?	de
las	laso	k1gNnPc2	laso
ciudades	ciudades	k1gInSc1	ciudades
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
