<p>
<s>
Asunción	Asunción	k1gInSc1	Asunción
(	(	kIx(	(
<g/>
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
guaraní	guaranit	k5eAaPmIp3nS	guaranit
Paraguaý	Paraguaý	k1gFnSc1	Paraguaý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
hranicích	hranice	k1gFnPc6	hranice
žije	žít	k5eAaImIp3nS	žít
542	[number]	k4	542
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
však	však	k9	však
střediskem	středisko	k1gNnSc7	středisko
aglomerace	aglomerace	k1gFnSc2	aglomerace
Gran	Grana	k1gFnPc2	Grana
Asunción	Asunción	k1gInSc1	Asunción
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
přístavem	přístav	k1gInSc7	přístav
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
střediskem	středisko	k1gNnSc7	středisko
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1537	[number]	k4	1537
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
předzíváno	předzívat	k5eAaImNgNnS	předzívat
"	"	kIx"	"
<g/>
Matka	matka	k1gFnSc1	matka
měst	město	k1gNnPc2	město
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Madre	Madr	k1gMnSc5	Madr
de	de	k?	de
las	laso	k1gNnPc2	laso
ciudades	ciudades	k1gInSc1	ciudades
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
"	"	kIx"	"
<g/>
Nanebevzetí	nanebevzetí	k1gNnSc1	nanebevzetí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
La	la	k1gNnPc4	la
Muy	Muy	k1gMnSc2	Muy
Noble	Nobl	k1gMnSc2	Nobl
y	y	k?	y
Leal	Leal	k1gInSc1	Leal
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Nuestra	Nuestr	k1gMnSc2	Nuestr
Señ	Señ	k1gMnSc2	Señ
Santa	Sant	k1gMnSc2	Sant
María	Maríus	k1gMnSc2	Maríus
de	de	k?	de
la	la	k1gNnSc1	la
Asunción	Asunción	k1gInSc1	Asunción
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Asunción	Asunción	k1gInSc4	Asunción
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Asunción	Asunción	k1gInSc1	Asunción
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
