<p>
<s>
Polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
světelné	světelný	k2eAgInPc4d1	světelný
úkazy	úkaz	k1gInPc4	úkaz
nastávající	nastávající	k2eAgInSc4d1	nastávající
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
atmosféře	atmosféra	k1gFnSc6	atmosféra
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
80	[number]	k4	80
do	do	k7c2	do
100	[number]	k4	100
km	km	kA	km
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
100	[number]	k4	100
km	km	kA	km
(	(	kIx(	(
<g/>
v	v	k7c6	v
ionosféře	ionosféra	k1gFnSc6	ionosféra
–	–	k?	–
oblast	oblast	k1gFnSc1	oblast
vysoké	vysoký	k2eAgFnPc1d1	vysoká
koncentrace	koncentrace	k1gFnPc1	koncentrace
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
volných	volný	k2eAgInPc2d1	volný
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zasahující	zasahující	k2eAgFnSc1d1	zasahující
i	i	k9	i
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
záře	záře	k1gFnSc1	záře
–	–	k?	–
aurora	aurora	k1gFnSc1	aurora
australis	australis	k1gFnSc1	australis
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
záře	záře	k1gFnSc1	záře
–	–	k?	–
aurora	aurora	k1gFnSc1	aurora
borealis	borealis	k1gFnSc1	borealis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnPc1d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnPc1d1	severní
záře	zář	k1gFnPc1	zář
jsou	být	k5eAaImIp3nP	být
mírně	mírně	k6eAd1	mírně
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
<g/>
Její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
Aurora	Aurora	k1gFnSc1	Aurora
Borealis	Borealis	k1gFnPc1	Borealis
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
římské	římský	k2eAgFnSc2d1	římská
bohyně	bohyně	k1gFnSc2	bohyně
úsvitu	úsvit	k1gInSc2	úsvit
Aurory	Aurora	k1gFnSc2	Aurora
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
pojmenování	pojmenování	k1gNnSc2	pojmenování
severního	severní	k2eAgInSc2d1	severní
větru	vítr	k1gInSc2	vítr
Boreas	Boreasa	k1gFnPc2	Boreasa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
francouzský	francouzský	k2eAgInSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
Pierre	Pierr	k1gInSc5	Pierr
Gassendi	Gassend	k1gMnPc1	Gassend
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručný	stručný	k2eAgInSc4d1	stručný
průběh	průběh	k1gInSc4	průběh
jedné	jeden	k4xCgFnSc2	jeden
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
vznikají	vznikat	k5eAaImIp3nP	vznikat
vlivem	vlivem	k7c2	vlivem
nerovností	nerovnost	k1gFnPc2	nerovnost
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
sluneční	sluneční	k2eAgFnSc2d1	sluneční
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
skvrn	skvrna	k1gFnPc2	skvrna
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jedna	jeden	k4xCgFnSc1	jeden
masivní	masivní	k2eAgFnSc1d1	masivní
protuberance	protuberance	k1gFnSc1	protuberance
(	(	kIx(	(
<g/>
erupce	erupce	k1gFnPc1	erupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrak	mrak	k1gInSc1	mrak
částic	částice	k1gFnPc2	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
tvořený	tvořený	k2eAgInSc4d1	tvořený
protony	proton	k1gInPc4	proton
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc4	elektron
a	a	k8xC	a
alfa	alfa	k1gFnSc1	alfa
částicemi	částice	k1gFnPc7	částice
(	(	kIx(	(
<g/>
plazmoid	plazmoid	k1gInSc1	plazmoid
<g/>
)	)	kIx)	)
letí	letý	k2eAgMnPc1d1	letý
vesmírem	vesmír	k1gInSc7	vesmír
(	(	kIx(	(
<g/>
rychlostí	rychlost	k1gFnSc7	rychlost
řádově	řádově	k6eAd1	řádově
0,1	[number]	k4	0,1
%	%	kIx~	%
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
ono	onen	k3xDgNnSc1	onen
pole	pole	k1gNnSc2	pole
většinu	většina	k1gFnSc4	většina
odrazí	odrazit	k5eAaPmIp3nS	odrazit
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
ho	on	k3xPp3gMnSc4	on
zachytí	zachytit	k5eAaPmIp3nS	zachytit
a	a	k8xC	a
stáčí	stáčet	k5eAaImIp3nS	stáčet
po	po	k7c6	po
spirálách	spirála	k1gFnPc6	spirála
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
magnetickým	magnetický	k2eAgInPc3d1	magnetický
pólům	pól	k1gInPc3	pól
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
interaguje	interagovat	k5eAaBmIp3nS	interagovat
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc1	vliv
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c6	na
polární	polární	k2eAgFnSc6d1	polární
záři	zář	k1gFnSc6	zář
==	==	k?	==
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
při	při	k7c6	při
slunečních	sluneční	k2eAgFnPc6d1	sluneční
erupcích	erupce	k1gFnPc6	erupce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
způsobovány	způsobovat	k5eAaImNgFnP	způsobovat
nerovnostmi	nerovnost	k1gFnPc7	nerovnost
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
Slunce	slunce	k1gNnSc2	slunce
sledující	sledující	k2eAgInSc4d1	sledující
sluneční	sluneční	k2eAgInSc4d1	sluneční
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
a	a	k8xC	a
magnetické	magnetický	k2eAgFnPc1d1	magnetická
pole	pole	k1gFnPc1	pole
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
faktory	faktor	k1gInPc1	faktor
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
===	===	k?	===
</s>
</p>
<p>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
by	by	k9	by
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
mělo	mít	k5eAaImAgNnS	mít
tvar	tvar	k1gInSc4	tvar
jako	jako	k8xS	jako
pole	pole	k1gNnSc4	pole
tyčového	tyčový	k2eAgInSc2d1	tyčový
magnetu	magnet	k1gInSc2	magnet
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
dobře	dobře	k6eAd1	dobře
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hned	hned	k6eAd1	hned
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
za	za	k7c4	za
prvé	prvý	k4xOgNnSc4	prvý
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
není	být	k5eNaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
těleso	těleso	k1gNnSc1	těleso
(	(	kIx(	(
<g/>
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
<g/>
,	,	kIx,	,
plazma	plazma	k1gNnSc1	plazma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
tvořeno	tvořit	k5eAaImNgNnS	tvořit
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
pevně	pevně	k6eAd1	pevně
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
svou	svůj	k3xOyFgFnSc4	svůj
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgNnSc1d1	výsledné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
proměnné	proměnný	k2eAgInPc4d1	proměnný
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
jeho	jeho	k3xOp3gInSc1	jeho
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
s	s	k7c7	s
jedenáctiletou	jedenáctiletý	k2eAgFnSc7d1	jedenáctiletá
periodou	perioda	k1gFnSc7	perioda
od	od	k7c2	od
klidného	klidný	k2eAgInSc2d1	klidný
až	až	k9	až
po	po	k7c4	po
absolutně	absolutně	k6eAd1	absolutně
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poruchy	poruch	k1gInPc7	poruch
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
===	===	k?	===
</s>
</p>
<p>
<s>
Poruchové	poruchový	k2eAgFnPc1d1	poruchová
indukční	indukční	k2eAgFnPc1d1	indukční
siločáry	siločára	k1gFnPc1	siločára
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
se	se	k3xPyFc4	se
zase	zase	k9	zase
zanořují	zanořovat	k5eAaImIp3nP	zanořovat
do	do	k7c2	do
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
sousedící	sousedící	k2eAgInSc1d1	sousedící
prostor	prostor	k1gInSc1	prostor
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
do	do	k7c2	do
magnetických	magnetický	k2eAgFnPc2d1	magnetická
pastí	past	k1gFnPc2	past
a	a	k8xC	a
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
proudění	proudění	k1gNnSc6	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Dané	daný	k2eAgNnSc1d1	dané
místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
pak	pak	k9	pak
zářením	záření	k1gNnSc7	záření
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
a	a	k8xC	a
oproti	oproti	k7c3	oproti
teplejšímu	teplý	k2eAgNnSc3d2	teplejší
okolí	okolí	k1gNnSc3	okolí
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zdá	zdát	k5eAaPmIp3nS	zdát
tmavé	tmavý	k2eAgNnSc1d1	tmavé
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
pak	pak	k6eAd1	pak
vznikají	vznikat	k5eAaImIp3nP	vznikat
protuberance	protuberance	k1gFnPc1	protuberance
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
odhazovány	odhazován	k2eAgFnPc1d1	odhazována
megatuny	megatuna	k1gFnPc1	megatuna
alfa	alfa	k1gNnSc2	alfa
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
polární	polární	k2eAgFnSc6d1	polární
záři	zář	k1gFnSc6	zář
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
zde	zde	k6eAd1	zde
interakce	interakce	k1gFnSc1	interakce
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
složky	složka	k1gFnPc1	složka
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
(	(	kIx(	(
<g/>
protony	proton	k1gInPc4	proton
a	a	k8xC	a
alfa	alfa	k1gNnSc4	alfa
částice	částice	k1gFnSc2	částice
jsou	být	k5eAaImIp3nP	být
kladné	kladný	k2eAgFnPc1d1	kladná
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc1	elektron
záporné	záporný	k2eAgInPc1d1	záporný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gInSc4	on
tedy	tedy	k9	tedy
odražena	odrazit	k5eAaPmNgFnS	odrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
a	a	k8xC	a
stáčí	stáčet	k5eAaImIp3nS	stáčet
se	se	k3xPyFc4	se
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
po	po	k7c6	po
spirálách	spirála	k1gFnPc6	spirála
až	až	k6eAd1	až
k	k	k7c3	k
atmosféře	atmosféra	k1gFnSc3	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interakce	interakce	k1gFnPc4	interakce
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
==	==	k?	==
</s>
</p>
<p>
<s>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
dostáváme	dostávat	k5eAaImIp1nP	dostávat
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
jevu	jev	k1gInSc3	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
polární	polární	k2eAgNnSc4d1	polární
září	září	k1gNnSc4	září
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
konečně	konečně	k6eAd1	konečně
hrstka	hrstka	k1gFnSc1	hrstka
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
alfa	alfa	k1gNnSc1	alfa
částic	částice	k1gFnPc2	částice
dostane	dostat	k5eAaPmIp3nS	dostat
skrz	skrz	k7c4	skrz
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
až	až	k9	až
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
atmosféře	atmosféra	k1gFnSc3	atmosféra
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
energie	energie	k1gFnSc1	energie
okolních	okolní	k2eAgFnPc2d1	okolní
molekul	molekula	k1gFnPc2	molekula
ze	z	k7c2	z
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
srážet	srážet	k5eAaImF	srážet
s	s	k7c7	s
molekulami	molekula	k1gFnPc7	molekula
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
jiné	jiný	k2eAgNnSc1d1	jiné
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
fyzika	fyzika	k1gFnSc1	fyzika
atomového	atomový	k2eAgInSc2d1	atomový
obalu	obal	k1gInSc2	obal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
ději	děj	k1gInSc6	děj
se	se	k3xPyFc4	se
emituje	emitovat	k5eAaBmIp3nS	emitovat
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kyslík	kyslík	k1gInSc4	kyslík
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
558	[number]	k4	558
nm	nm	k?	nm
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
a	a	k8xC	a
630	[number]	k4	630
nm	nm	k?	nm
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
doutnavý	doutnavý	k2eAgInSc4d1	doutnavý
výboj	výboj	k1gInSc4	výboj
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
proudových	proudový	k2eAgFnPc2d1	proudová
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místa	místo	k1gNnPc4	místo
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Polární	polární	k2eAgFnPc1d1	polární
záře	zář	k1gFnPc1	zář
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reykjavik	Reykjavik	k1gMnSc1	Reykjavik
</s>
</p>
<p>
<s>
Borgarnes	Borgarnes	k1gMnSc1	Borgarnes
</s>
</p>
<p>
<s>
Sandarkrokur	Sandarkrokur	k1gMnSc1	Sandarkrokur
</s>
</p>
<p>
<s>
Akureyri	Akureyri	k6eAd1	Akureyri
</s>
</p>
<p>
<s>
Fairbanks	Fairbanks	k6eAd1	Fairbanks
</s>
</p>
<p>
<s>
Teritorium	teritorium	k1gNnSc1	teritorium
Yukon	Yukona	k1gFnPc2	Yukona
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Skye	Sky	k1gFnSc2	Sky
</s>
</p>
<p>
<s>
Laponsko	Laponsko	k1gNnSc1	Laponsko
</s>
</p>
<p>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
Špicberky	Špicberky	k1gInPc7	Špicberky
</s>
</p>
<p>
<s>
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
-	-	kIx~	-
10	[number]	k4	10
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
nejčastějším	častý	k2eAgInSc7d3	nejčastější
výskytem	výskyt	k1gInSc7	výskyt
polárních	polární	k2eAgFnPc2d1	polární
září	zář	k1gFnPc2	zář
</s>
</p>
