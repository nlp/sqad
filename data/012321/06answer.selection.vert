<s>
Polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
světelné	světelný	k2eAgInPc4d1	světelný
úkazy	úkaz	k1gInPc4	úkaz
nastávající	nastávající	k2eAgInSc4d1	nastávající
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
atmosféře	atmosféra	k1gFnSc6	atmosféra
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
80	[number]	k4	80
do	do	k7c2	do
100	[number]	k4	100
km	km	kA	km
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
100	[number]	k4	100
km	km	kA	km
(	(	kIx(	(
<g/>
v	v	k7c6	v
ionosféře	ionosféra	k1gFnSc6	ionosféra
–	–	k?	–
oblast	oblast	k1gFnSc1	oblast
vysoké	vysoký	k2eAgFnPc1d1	vysoká
koncentrace	koncentrace	k1gFnPc1	koncentrace
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
volných	volný	k2eAgInPc2d1	volný
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zasahující	zasahující	k2eAgFnSc1d1	zasahující
i	i	k9	i
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
