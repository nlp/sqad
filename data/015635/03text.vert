<s>
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
veslonozí	veslonozí	k1gMnPc1
(	(	kIx(
<g/>
Pelecaniformes	Pelecaniformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
volavkovití	volavkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Ardeidae	Ardeidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
BubulcusBonaparte	BubulcusBonapart	k1gMnSc5
<g/>
,	,	kIx,
1855	#num#	k4
Binomické	binomický	k2eAgInPc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Bubulcus	Bubulcus	k1gMnSc1
ibis	ibis	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
výskytu	výskyt	k1gInSc2
volavky	volavka	k1gFnSc2
rusohlavé	rusohlavý	k2eAgFnSc2d1
</s>
<s>
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bubulcus	Bubulcus	k1gMnSc1
ibis	ibis	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velká	velký	k2eAgFnSc1d1
bílá	bílý	k2eAgFnSc1d1
volavka	volavka	k1gFnSc1
s	s	k7c7
černýma	černý	k2eAgFnPc7d1
nohama	noha	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
až	až	k9
56	#num#	k4
cm	cm	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
rozpětí	rozpětí	k1gNnSc1
křídel	křídlo	k1gNnPc2
je	být	k5eAaImIp3nS
až	až	k9
96	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
vážit	vážit	k5eAaImF
od	od	k7c2
200	#num#	k4
do	do	k7c2
500	#num#	k4
g.	g.	k?
U	u	k7c2
této	tento	k3xDgFnSc2
volavky	volavka	k1gFnSc2
neexistuje	existovat	k5eNaImIp3nS
pohlavní	pohlavní	k2eAgFnSc4d1
dvojtvárnost	dvojtvárnost	k1gFnSc4
<g/>
,	,	kIx,
samec	samec	k1gMnSc1
a	a	k8xC
samička	samička	k1gFnSc1
se	se	k3xPyFc4
vzhledově	vzhledově	k6eAd1
nijak	nijak	k6eAd1
výrazně	výrazně	k6eAd1
neliší	lišit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
menší	malý	k2eAgInSc4d2
druh	druh	k1gInSc4
volavky	volavka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
opeření	opeření	k1gNnSc2
je	být	k5eAaImIp3nS
sněhově	sněhově	k6eAd1
bílá	bílý	k2eAgFnSc1d1
s	s	k7c7
nádechem	nádech	k1gInSc7
oranžové	oranžový	k2eAgFnSc2d1
na	na	k7c6
hlavě	hlava	k1gFnSc6
<g/>
,	,	kIx,
hrudi	hruď	k1gFnSc6
a	a	k8xC
zádech	záda	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bývá	bývat	k5eAaImIp3nS
ve	v	k7c6
svatebním	svatební	k2eAgInSc6d1
šatu	šat	k1gInSc6
hlavně	hlavně	k9
u	u	k7c2
samců	samec	k1gMnPc2
pestřejší	pestrý	k2eAgInSc1d2
<g/>
,	,	kIx,
Zobák	zobák	k1gInSc1
je	být	k5eAaImIp3nS
krátký	krátký	k2eAgInSc1d1
<g/>
,	,	kIx,
žlutý	žlutý	k2eAgInSc1d1
až	až	k6eAd1
oranžový	oranžový	k2eAgInSc1d1
<g/>
,	,	kIx,
nohy	noha	k1gFnPc4
jsou	být	k5eAaImIp3nP
žlutavé	žlutavý	k2eAgInPc1d1
někdy	někdy	k6eAd1
až	až	k6eAd1
černé	černý	k2eAgFnPc1d1
či	či	k8xC
šedé	šedý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Obývá	obývat	k5eAaImIp3nS
jižní	jižní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
Asii	Asie	k1gFnSc4
a	a	k8xC
především	především	k9
oblasti	oblast	k1gFnSc2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavlečena	zavlečen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
ale	ale	k9
i	i	k9
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
i	i	k9
do	do	k7c2
Portugalska	Portugalsko	k1gNnSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
byla	být	k5eAaImAgFnS
již	již	k6eAd1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
také	také	k6eAd1
pozorována	pozorovat	k5eAaImNgFnS
a	a	k8xC
nelze	lze	k6eNd1
vyloučit	vyloučit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
divoké	divoký	k2eAgMnPc4d1
ptáky	pták	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volavka	volavka	k1gFnSc1
obývá	obývat	k5eAaImIp3nS
břehy	břeh	k1gInPc4
řek	řeka	k1gFnPc2
a	a	k8xC
močálů	močál	k1gInPc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
ale	ale	k9
rozlehlé	rozlehlý	k2eAgFnPc4d1
travnaté	travnatý	k2eAgFnPc4d1
pláně	pláň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přizpůsobila	přizpůsobit	k5eAaPmAgFnS
se	se	k3xPyFc4
i	i	k9
životu	život	k1gInSc2
poblíž	poblíž	k7c2
lidských	lidský	k2eAgNnPc2d1
obydlí	obydlí	k1gNnPc2
</s>
<s>
Způsob	způsob	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
denního	denní	k2eAgMnSc4d1
ptáka	pták	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
močálech	močál	k1gInPc6
loví	lovit	k5eAaImIp3nP
kořist	kořist	k1gFnSc4
a	a	k8xC
na	na	k7c6
travinách	travina	k1gFnPc6
je	být	k5eAaImIp3nS
často	často	k6eAd1
viděn	vidět	k5eAaImNgInS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
velkých	velký	k2eAgMnPc2d1
savců	savec	k1gMnPc2
(	(	kIx(
<g/>
často	často	k6eAd1
se	se	k3xPyFc4
"	"	kIx"
<g/>
vozí	vozit	k5eAaImIp3nP
<g/>
"	"	kIx"
na	na	k7c6
slonech	slon	k1gMnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
poskytují	poskytovat	k5eAaImIp3nP
bezpečí	bezpečí	k1gNnSc4
a	a	k8xC
plaší	plašit	k5eAaImIp3nP
mraky	mrak	k1gInPc1
hmyzu	hmyz	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
volavky	volavka	k1gFnPc1
živí	živit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
noc	noc	k1gFnSc4
hřaduje	hřadovat	k5eAaImIp3nS
v	v	k7c6
obrovských	obrovský	k2eAgNnPc6d1
hejnech	hejno	k1gNnPc6
na	na	k7c6
stromech	strom	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Volavky	volavka	k1gFnPc1
se	se	k3xPyFc4
dorozumívají	dorozumívat	k5eAaImIp3nP
různými	různý	k2eAgInPc7d1
skřeky	skřek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
v	v	k7c6
případě	případ	k1gInSc6
zastrašování	zastrašování	k1gNnSc2
doplňují	doplňovat	k5eAaImIp3nP
i	i	k9
naježením	naježení	k1gNnSc7
per	pero	k1gNnPc2
na	na	k7c6
zádech	záda	k1gNnPc6
a	a	k8xC
krku	krk	k1gInSc6
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Volavky	volavka	k1gFnPc1
rusohlavé	rusohlavý	k2eAgFnPc1d1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
hlavně	hlavně	k9
hmyzem	hmyz	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
loví	lovit	k5eAaImIp3nS
právě	právě	k9
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
velkých	velký	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
menšími	malý	k2eAgMnPc7d2
savci	savec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepohrdnou	pohrdnout	k5eNaPmIp3nP
ani	ani	k8xC
ptáčaty	ptáče	k1gNnPc7
a	a	k8xC
malými	malý	k2eAgFnPc7d1
rybkami	rybka	k1gFnPc7
a	a	k8xC
obojživelníky	obojživelník	k1gMnPc7
z	z	k7c2
močálů	močál	k1gInPc2
a	a	k8xC
břehů	břeh	k1gInPc2
řek	řeka	k1gFnPc2
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Volavky	volavka	k1gFnPc1
hnízdí	hnízdit	k5eAaImIp3nP
ve	v	k7c6
společnosti	společnost	k1gFnSc6
jiných	jiný	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
v	v	k7c6
obroských	obroský	k2eAgFnPc6d1
koloniích	kolonie	k1gFnPc6
čítajících	čítající	k2eAgInPc2d1
tisíce	tisíc	k4xCgInPc4
jedinců	jedinec	k1gMnPc2
i	i	k8xC
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
monogamní	monogamní	k2eAgMnSc1d1
pták	pták	k1gMnSc1
<g/>
,	,	kIx,
páry	pár	k1gInPc1
se	se	k3xPyFc4
vyhledávají	vyhledávat	k5eAaImIp3nP
na	na	k7c4
sezonu	sezona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgNnSc6d1
hnízdním	hnízdní	k2eAgNnSc6d1
období	období	k1gNnSc6
si	se	k3xPyFc3
samec	samec	k1gMnSc1
může	moct	k5eAaImIp3nS
vybrat	vybrat	k5eAaPmF
jinou	jiný	k2eAgFnSc4d1
samici	samice	k1gFnSc4
i	i	k9
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnízda	hnízdo	k1gNnPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
jak	jak	k6eAd1
původní	původní	k2eAgFnPc1d1
nebo	nebo	k8xC
si	se	k3xPyFc3
staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
nová	nový	k2eAgFnSc1d1
z	z	k7c2
větviček	větvička	k1gFnPc2
a	a	k8xC
různého	různý	k2eAgInSc2d1
podobného	podobný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
si	se	k3xPyFc3
hledají	hledat	k5eAaImIp3nP
pod	pod	k7c7
stromy	strom	k1gInPc7
nebo	nebo	k8xC
ho	on	k3xPp3gMnSc4
kradou	krást	k5eAaImIp3nP
ostatním	ostatní	k2eAgInPc3d1
párům	pár	k1gInPc3
v	v	k7c6
kolonii	kolonie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnízda	hnízdo	k1gNnPc1
si	se	k3xPyFc3
volavky	volavka	k1gFnPc1
mohou	moct	k5eAaImIp3nP
stavět	stavět	k5eAaImF
jak	jak	k6eAd1
na	na	k7c6
zemi	zem	k1gFnSc6
tak	tak	k8xC,k8xS
na	na	k7c6
keřích	keř	k1gInPc6
i	i	k8xC
stromech	strom	k1gInPc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
zde	zde	k6eAd1
najdou	najít	k5eAaPmIp3nP
vhodná	vhodný	k2eAgNnPc1d1
místa	místo	k1gNnPc1
<g/>
,	,	kIx,
Samec	samec	k1gInSc1
se	se	k3xPyFc4
samici	samice	k1gFnSc3
dvoří	dvořit	k5eAaImIp3nS
a	a	k8xC
namlouvací	namlouvací	k2eAgInPc1d1
tance	tanec	k1gInPc1
bývají	bývat	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
a	a	k8xC
složité	složitý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
snáší	snášet	k5eAaImIp3nS
2-5	2-5	k4
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
se	se	k3xPyFc4
střídají	střídat	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
rodiče	rodič	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
líhnou	líhnout	k5eAaImIp3nP
po	po	k7c6
21-26	21-26	k4
dnech	den	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volavky	volavka	k1gFnPc1
jsou	být	k5eAaImIp3nP
schopné	schopný	k2eAgFnPc1d1
přijnout	přijnout	k5eAaPmF,k5eAaImF
za	za	k7c4
vlastní	vlastní	k2eAgNnPc4d1
i	i	k8xC
osiřelá	osiřelý	k2eAgNnPc4d1
mláďata	mládě	k1gNnPc4
mladší	mladý	k2eAgFnSc2d2
14	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
opouštějí	opouštět	k5eAaImIp3nP
hnízdo	hnízdo	k1gNnSc4
velice	velice	k6eAd1
brzy	brzy	k6eAd1
a	a	k8xC
lezou	lézt	k5eAaImIp3nP
po	po	k7c6
okolních	okolní	k2eAgFnPc6d1
větvích	větev	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gMnPc4
rodiče	rodič	k1gMnPc4
stále	stále	k6eAd1
krmí	krmit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
věku	věk	k1gInSc6
45-60	45-60	k4
dní	den	k1gInPc2
opouští	opouštět	k5eAaImIp3nS
definitivně	definitivně	k6eAd1
hnízdo	hnízdo	k1gNnSc1
a	a	k8xC
osamostatňují	osamostatňovat	k5eAaImIp3nP
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohlavně	pohlavně	k6eAd1
dospívají	dospívat	k5eAaImIp3nP
na	na	k7c6
2	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
obrázků	obrázek	k1gInPc2
</s>
<s>
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
v	v	k7c6
Sao	Sao	k1gFnSc6
Paulu	Paula	k1gFnSc4
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
ve	v	k7c6
Francii	Francie	k1gFnSc6
</s>
<s>
Vejce	vejce	k1gNnSc1
volavky	volavka	k1gFnSc2
rusohlavé	rusohlavý	k2eAgFnSc2d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2020.3	2020.3	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Bubulcus	Bubulcus	k1gMnSc1
ibis	ibis	k1gMnSc1
(	(	kIx(
<g/>
cattle	cattle	k1gFnSc1
egret	egret	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-09-27	2008-09-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Volavka	volavka	k1gFnSc1
v	v	k7c6
ZOO	zoo	k1gFnSc6
Liberec	Liberec	k1gInSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
volavka	volavka	k1gFnSc1
rusohlavá	rusohlavý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85021409	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85021409	#num#	k4
</s>
