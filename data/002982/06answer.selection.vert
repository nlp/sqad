<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
loutky	loutka	k1gFnPc1	loutka
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
Závěsné	závěsný	k2eAgFnSc6d1	závěsná
-	-	kIx~	-
marioneta	marioneta	k1gFnSc1	marioneta
-	-	kIx~	-
loutka	loutka	k1gFnSc1	loutka
zavěšená	zavěšený	k2eAgFnSc1d1	zavěšená
na	na	k7c6	na
drátu	drát	k1gInSc6	drát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
niťovým	niťový	k2eAgNnSc7d1	niťový
vedením	vedení	k1gNnSc7	vedení
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
vahadlem	vahadlo	k1gNnSc7	vahadlo
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
loutkoherec	loutkoherec	k1gMnSc1	loutkoherec
mohl	moct	k5eAaImAgMnS	moct
ovládat	ovládat	k5eAaImF	ovládat
jednou	jeden	k4xCgFnSc7	jeden
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
