<s>
Válec	válec	k1gInSc1	válec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prostorové	prostorový	k2eAgFnSc6d1	prostorová
geometrii	geometrie	k1gFnSc6	geometrie
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
vymezené	vymezený	k2eAgNnSc1d1	vymezené
dvěma	dva	k4xCgFnPc7	dva
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1	rovnoběžná
podstavami	podstava	k1gFnPc7	podstava
a	a	k8xC	a
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
rozvinutelná	rozvinutelný	k2eAgFnSc1d1	rozvinutelný
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
povrchové	povrchový	k2eAgFnPc1d1	povrchová
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnPc1d1	tvořící
<g/>
)	)	kIx)	)
přímky	přímka	k1gFnPc1	přímka
pláště	plášť	k1gInPc1	plášť
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
podstavám	podstava	k1gFnPc3	podstava
kolmé	kolmý	k2eAgFnPc1d1	kolmá
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
kolmém	kolmý	k2eAgInSc6d1	kolmý
válci	válec	k1gInSc6	válec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
válec	válec	k1gInSc4	válec
kosý	kosý	k2eAgInSc4d1	kosý
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
podstavami	podstava	k1gFnPc7	podstava
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
výška	výška	k1gFnSc1	výška
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
podstavami	podstava	k1gFnPc7	podstava
podél	podél	k7c2	podél
pláště	plášť	k1gInSc2	plášť
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
podél	podél	k7c2	podél
povrchové	povrchový	k2eAgFnSc2d1	povrchová
přímky	přímka	k1gFnSc2	přímka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
strana	strana	k1gFnSc1	strana
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
podstavou	podstava	k1gFnSc7	podstava
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
válec	válec	k1gInSc4	válec
označíme	označit	k5eAaPmIp1nP	označit
jako	jako	k9	jako
kruhový	kruhový	k2eAgInSc4d1	kruhový
<g/>
.	.	kIx.	.
</s>
<s>
Kolmý	kolmý	k2eAgInSc4d1	kolmý
kruhový	kruhový	k2eAgInSc4d1	kruhový
válec	válec	k1gInSc4	válec
nazýváme	nazývat	k5eAaImIp1nP	nazývat
rotačním	rotační	k2eAgInSc7d1	rotační
válcem	válec	k1gInSc7	válec
<g/>
.	.	kIx.	.
</s>
<s>
Přímku	přímka	k1gFnSc4	přímka
procházející	procházející	k2eAgFnSc2d1	procházející
středy	středa	k1gFnSc2	středa
obou	dva	k4xCgFnPc2	dva
podstav	podstava	k1gFnPc2	podstava
rotačního	rotační	k2eAgInSc2d1	rotační
válce	válec	k1gInSc2	válec
nazýváme	nazývat	k5eAaImIp1nP	nazývat
osou	osa	k1gFnSc7	osa
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
válcem	válec	k1gInSc7	válec
rozumí	rozumět	k5eAaImIp3nS	rozumět
rotační	rotační	k2eAgInSc1d1	rotační
válec	válec	k1gInSc1	válec
<g/>
,	,	kIx,	,
kolmý	kolmý	k2eAgInSc1d1	kolmý
válec	válec	k1gInSc1	válec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
podstavou	podstava	k1gFnSc7	podstava
je	být	k5eAaImIp3nS	být
kruh	kruh	k1gInSc1	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgFnPc2d1	různá
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
objem	objem	k1gInSc4	objem
rotačního	rotační	k2eAgInSc2d1	rotační
válce	válec	k1gInSc2	válec
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
π	π	k?	π
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
⟹	⟹	k?	⟹
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
=	=	kIx~	=
V	V	kA	V
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
(	(	kIx(	(
π	π	k?	π
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
pi	pi	k0	pi
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
implies	implies	k1gInSc1	implies
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
h	h	k?	h
<g/>
=	=	kIx~	=
<g/>
V	V	kA	V
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
podstavy	podstava	k1gFnSc2	podstava
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
výška	výška	k1gFnSc1	výška
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
pláště	plášť	k1gInSc2	plášť
rotačního	rotační	k2eAgInSc2d1	rotační
válce	válec	k1gInSc2	válec
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
=	=	kIx~	=
2	[number]	k4	2
π	π	k?	π
r	r	kA	r
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
rh	rh	k?	rh
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
podstavy	podstava	k1gFnSc2	podstava
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
=	=	kIx~	=
π	π	k?	π
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
obsah	obsah	k1gInSc4	obsah
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
rotačního	rotační	k2eAgInSc2d1	rotační
válce	válec	k1gInSc2	válec
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
2	[number]	k4	2
π	π	k?	π
r	r	kA	r
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
r	r	kA	r
+	+	kIx~	+
h	h	k?	h
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
r	r	kA	r
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
+	+	kIx~	+
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Obecný	obecný	k2eAgInSc1d1	obecný
řez	řez	k1gInSc1	řez
válce	válec	k1gInSc2	válec
rovinou	rovina	k1gFnSc7	rovina
je	být	k5eAaImIp3nS	být
elipsa	elipsa	k1gFnSc1	elipsa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
rovina	rovina	k1gFnSc1	rovina
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kružnice	kružnice	k1gFnSc1	kružnice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obdélník	obdélník	k1gInSc1	obdélník
nebo	nebo	k8xC	nebo
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Označíme	označit	k5eAaPmIp1nP	označit
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
na	na	k7c6	na
podstavě	podstava	k1gFnSc6	podstava
válce	válec	k1gInSc2	válec
libovolný	libovolný	k2eAgInSc4d1	libovolný
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
středu	střed	k1gInSc2	střed
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
valíme	valit	k5eAaImIp1nP	valit
válec	válec	k1gInSc4	válec
po	po	k7c6	po
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
označený	označený	k2eAgInSc1d1	označený
bod	bod	k1gInSc1	bod
opisuje	opisovat	k5eAaImIp3nS	opisovat
cykloidu	cykloida	k1gFnSc4	cykloida
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
představu	představa	k1gFnSc4	představa
rotačního	rotační	k2eAgInSc2d1	rotační
válce	válec	k1gInSc2	válec
lze	lze	k6eAd1	lze
rozšířit	rozšířit	k5eAaPmF	rozšířit
a	a	k8xC	a
zobecnit	zobecnit	k5eAaPmF	zobecnit
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
křivku	křivka	k1gFnSc4	křivka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yIgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
vzájemně	vzájemně	k6eAd1	vzájemně
rovnoběžných	rovnoběžný	k2eAgFnPc6d1	rovnoběžná
přímkách	přímka	k1gFnPc6	přímka
procházejících	procházející	k2eAgInPc2d1	procházející
libovolným	libovolný	k2eAgInSc7d1	libovolný
bodem	bod	k1gInSc7	bod
křivky	křivka	k1gFnSc2	křivka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
válcovou	válcový	k2eAgFnSc4d1	válcová
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
prostoru	prostor	k1gInSc2	prostor
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
válcovou	válcový	k2eAgFnSc7d1	válcová
plochou	plocha	k1gFnSc7	plocha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
válcový	válcový	k2eAgInSc1d1	válcový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Válcová	válcový	k2eAgFnSc1d1	válcová
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
kvadratický	kvadratický	k2eAgInSc1d1	kvadratický
válec	válec	k1gInSc1	válec
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
podle	podle	k7c2	podle
řídící	řídící	k2eAgFnSc2d1	řídící
křivky	křivka	k1gFnSc2	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Eliptický	eliptický	k2eAgInSc1d1	eliptický
kvadratický	kvadratický	k2eAgInSc1d1	kvadratický
válec	válec	k1gInSc1	válec
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
Řídící	řídící	k2eAgFnSc7d1	řídící
křivkou	křivka	k1gFnSc7	křivka
eliptického	eliptický	k2eAgInSc2d1	eliptický
válce	válec	k1gInSc2	válec
je	být	k5eAaImIp3nS	být
elipsa	elipsa	k1gFnSc1	elipsa
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
s	s	k7c7	s
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
a	a	k8xC	a
tvořící	tvořící	k2eAgFnPc1d1	tvořící
přímky	přímka	k1gFnPc1	přímka
válce	válec	k1gInSc2	válec
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgInPc1d1	rovnoběžný
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rotační	rotační	k2eAgInSc4d1	rotační
válec	válec	k1gInSc4	válec
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
rotace	rotace	k1gFnSc1	rotace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Hyperbolický	hyperbolický	k2eAgInSc1d1	hyperbolický
kvadratický	kvadratický	k2eAgInSc1d1	kvadratický
válec	válec	k1gInSc1	válec
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
Řídící	řídící	k2eAgFnSc7d1	řídící
křivkou	křivka	k1gFnSc7	křivka
hyperbolického	hyperbolický	k2eAgInSc2d1	hyperbolický
válce	válec	k1gInSc2	válec
je	být	k5eAaImIp3nS	být
hyperbola	hyperbola	k1gFnSc1	hyperbola
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
s	s	k7c7	s
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
a	a	k8xC	a
tvořící	tvořící	k2eAgFnPc1d1	tvořící
přímky	přímka	k1gFnPc1	přímka
válce	válec	k1gInSc2	válec
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgInPc1d1	rovnoběžný
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Parabolický	parabolický	k2eAgInSc1d1	parabolický
kvadratický	kvadratický	k2eAgInSc1d1	kvadratický
válec	válec	k1gInSc1	válec
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
2	[number]	k4	2
p	p	k?	p
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
px	px	k?	px
<g/>
}	}	kIx)	}
:	:	kIx,	:
Řídící	řídící	k2eAgFnSc7d1	řídící
křivkou	křivka	k1gFnSc7	křivka
parabolického	parabolický	k2eAgInSc2d1	parabolický
válce	válec	k1gInSc2	válec
je	být	k5eAaImIp3nS	být
parabola	parabola	k1gFnSc1	parabola
ležící	ležící	k2eAgFnSc2d1	ležící
<g />
.	.	kIx.	.
</s>
<s hack="1">
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
s	s	k7c7	s
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
2	[number]	k4	2
p	p	k?	p
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
px	px	k?	px
<g/>
}	}	kIx)	}
a	a	k8xC	a
tvořící	tvořící	k2eAgFnPc1d1	tvořící
přímky	přímka	k1gFnPc1	přímka
válce	válec	k1gInSc2	válec
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgInPc1d1	rovnoběžný
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Obecnou	obecný	k2eAgFnSc4d1	obecná
válcovou	válcový	k2eAgFnSc4d1	válcová
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
řídící	řídící	k2eAgFnSc1d1	řídící
křivka	křivka	k1gFnSc1	křivka
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rovnici	rovnice	k1gFnSc4	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
tvořící	tvořící	k2eAgFnPc1d1	tvořící
přímky	přímka	k1gFnPc1	přímka
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
v	v	k7c6	v
rovnici	rovnice	k1gFnSc6	rovnice
plochy	plocha	k1gFnSc2	plocha
chybí	chybět	k5eAaImIp3nS	chybět
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rovnici	rovnice	k1gFnSc4	rovnice
válcové	válcový	k2eAgFnSc2d1	válcová
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
tvořící	tvořící	k2eAgFnPc1d1	tvořící
přímky	přímka	k1gFnPc1	přímka
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
chybějící	chybějící	k2eAgFnPc4d1	chybějící
proměnné	proměnná	k1gFnPc4	proměnná
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
řídící	řídící	k2eAgFnSc1d1	řídící
křivka	křivka	k1gFnSc1	křivka
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
rovnici	rovnice	k1gFnSc4	rovnice
jako	jako	k8xS	jako
daná	daný	k2eAgFnSc1d1	daná
plocha	plocha	k1gFnSc1	plocha
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
kolmé	kolmý	k2eAgFnSc6d1	kolmá
k	k	k7c3	k
tvořícím	tvořící	k2eAgFnPc3d1	tvořící
přímkám	přímka	k1gFnPc3	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tvořící	tvořící	k2eAgFnPc4d1	tvořící
přímky	přímka	k1gFnPc4	přímka
rovnoběžné	rovnoběžný	k2eAgFnPc4d1	rovnoběžná
s	s	k7c7	s
vektorem	vektor	k1gInSc7	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
rovnici	rovnice	k1gFnSc4	rovnice
válcové	válcový	k2eAgFnSc2d1	válcová
plochy	plocha	k1gFnSc2	plocha
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
z	z	k7c2	z
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
x-a_	x_	k?	x-a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
z	z	k0	z
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
y-a_	y_	k?	y-a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Objem	objem	k1gInSc4	objem
válce	válec	k1gInSc2	válec
určíme	určit	k5eAaPmIp1nP	určit
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
S	s	k7c7	s
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
Sh	Sh	k1gFnSc6	Sh
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
podstavy	podstava	k1gFnSc2	podstava
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hloubka	hloubka	k1gFnSc1	hloubka
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
povrchu	povrch	k1gInSc2	povrch
válce	válec	k1gInSc2	válec
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
=	=	kIx~	=
2	[number]	k4	2
S	s	k7c7	s
+	+	kIx~	+
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
+	+	kIx~	+
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
podstavy	podstava	k1gFnSc2	podstava
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
pláště	plášť	k1gInSc2	plášť
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
