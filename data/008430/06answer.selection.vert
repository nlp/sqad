<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
estonské	estonský	k2eAgFnSc2d1	Estonská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Friedrich	Friedrich	k1gMnSc1	Friedrich
Reinhold	Reinhold	k1gMnSc1	Reinhold
Kreutzwald	Kreutzwald	k1gMnSc1	Kreutzwald
<g/>
,	,	kIx,	,
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
estonské	estonský	k2eAgFnSc2d1	Estonská
poezie	poezie	k1gFnSc2	poezie
jsou	být	k5eAaImIp3nP	být
označováni	označován	k2eAgMnPc1d1	označován
Marie	Maria	k1gFnPc4	Maria
Underová	Underová	k1gFnSc1	Underová
a	a	k8xC	a
Kristjan	Kristjan	k1gMnSc1	Kristjan
Jaak	Jaak	k1gMnSc1	Jaak
Peterson	Peterson	k1gMnSc1	Peterson
<g/>
.	.	kIx.	.
</s>
