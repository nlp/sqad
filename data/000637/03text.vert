<s>
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1841	[number]	k4	1841
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1916	[number]	k4	1916
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
knihkupec	knihkupec	k1gMnSc1	knihkupec
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
největší	veliký	k2eAgFnSc2d3	veliký
české	český	k2eAgFnSc2d1	Česká
tištěné	tištěný	k2eAgFnSc2d1	tištěná
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
Ottova	Ottův	k2eAgInSc2d1	Ottův
slovníku	slovník	k1gInSc2	slovník
naučného	naučný	k2eAgInSc2d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
42	[number]	k4	42
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
jako	jako	k9	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
vojenského	vojenský	k2eAgMnSc2d1	vojenský
ranhojiče	ranhojič	k1gMnSc2	ranhojič
.	.	kIx.	.
</s>
<s>
Janovi	Janův	k2eAgMnPc1d1	Janův
rodiče	rodič	k1gMnPc1	rodič
nemohli	moct	k5eNaImAgMnP	moct
synovi	syn	k1gMnSc3	syn
platit	platit	k5eAaImF	platit
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ukončil	ukončit	k5eAaPmAgInS	ukončit
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
ve	v	k7c6	v
13	[number]	k4	13
letech	let	k1gInPc6	let
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
kupcem	kupec	k1gMnSc7	kupec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozornost	pozornost	k1gFnSc1	pozornost
ovšem	ovšem	k9	ovšem
nejvíce	hodně	k6eAd3	hodně
upoutal	upoutat	k5eAaPmAgInS	upoutat
knihtisk	knihtisk	k1gInSc1	knihtisk
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
21	[number]	k4	21
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgMnS	získat
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
v	v	k7c6	v
tiskárně	tiskárna	k1gFnSc6	tiskárna
bratří	bratřit	k5eAaImIp3nS	bratřit
Grégrů	Grégr	k1gMnPc2	Grégr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
účetní	účetní	k1gMnSc1	účetní
a	a	k8xC	a
disponent	disponent	k1gMnSc1	disponent
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
pílí	píle	k1gFnSc7	píle
<g/>
,	,	kIx,	,
pracovitostí	pracovitost	k1gFnSc7	pracovitost
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1869	[number]	k4	1869
také	také	k9	také
výhodným	výhodný	k2eAgInSc7d1	výhodný
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Miladou	Milada	k1gFnSc7	Milada
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
zámožného	zámožný	k2eAgMnSc2d1	zámožný
pražského	pražský	k2eAgMnSc2d1	pražský
tiskaře	tiskař	k1gMnSc2	tiskař
<g/>
,	,	kIx,	,
nakladatele	nakladatel	k1gMnSc2	nakladatel
a	a	k8xC	a
knihkupce	knihkupec	k1gMnSc2	knihkupec
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
převzal	převzít	k5eAaPmAgInS	převzít
Grégrův	Grégrův	k2eAgInSc1d1	Grégrův
podnik	podnik	k1gInSc1	podnik
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úspěšně	úspěšně	k6eAd1	úspěšně
provozoval	provozovat	k5eAaImAgMnS	provozovat
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
a	a	k8xC	a
knihkupectví	knihkupectví	k1gNnSc4	knihkupectví
<g/>
.	.	kIx.	.
</s>
<s>
Vydával	vydávat	k5eAaImAgMnS	vydávat
knihy	kniha	k1gFnPc4	kniha
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
oborů	obor	k1gInPc2	obor
-	-	kIx~	-
od	od	k7c2	od
nenáročné	náročný	k2eNgFnSc2d1	nenáročná
lidové	lidový	k2eAgFnSc2d1	lidová
četby	četba	k1gFnSc2	četba
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Matice	matice	k1gFnSc2	matice
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
Laciná	laciný	k2eAgFnSc1d1	laciná
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
cestopisy	cestopis	k1gInPc4	cestopis
a	a	k8xC	a
odborné	odborný	k2eAgFnPc4d1	odborná
publikace	publikace	k1gFnPc4	publikace
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
,	,	kIx,	,
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
jazykověda	jazykověda	k1gFnSc1	jazykověda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovníky	slovník	k1gInPc1	slovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
časopisy	časopis	k1gInPc1	časopis
(	(	kIx(	(
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gInSc1	Lumír
<g/>
,	,	kIx,	,
Ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
<g/>
,	,	kIx,	,
humoristický	humoristický	k2eAgInSc1d1	humoristický
Paleček	paleček	k1gInSc1	paleček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známá	známý	k2eAgNnPc4d1	známé
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
Brehmův	Brehmův	k2eAgInSc1d1	Brehmův
Život	život	k1gInSc1	život
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
ilustrované	ilustrovaný	k2eAgFnPc1d1	ilustrovaná
Povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
arabesky	arabeska	k1gFnPc1	arabeska
a	a	k8xC	a
humoresky	humoreska	k1gFnPc1	humoreska
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
a	a	k8xC	a
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
cyklus	cyklus	k1gInSc4	cyklus
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1884	[number]	k4	1884
provozoval	provozovat	k5eAaImAgMnS	provozovat
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
i	i	k8xC	i
sortimentní	sortimentní	k2eAgNnSc1d1	sortimentní
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
nejprve	nejprve	k6eAd1	nejprve
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
na	na	k7c4	na
Jungmannovu	Jungmannův	k2eAgFnSc4d1	Jungmannova
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
získal	získat	k5eAaPmAgInS	získat
vlastní	vlastní	k2eAgInSc1d1	vlastní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
534	[number]	k4	534
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
U	u	k7c2	u
Žateckých	žatecký	k2eAgMnPc2d1	žatecký
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnSc4d1	západní
křídlo	křídlo	k1gNnSc4	křídlo
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
34	[number]	k4	34
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
sahalo	sahat	k5eAaImAgNnS	sahat
až	až	k9	až
do	do	k7c2	do
Malé	Malá	k1gFnSc2	Malá
Štěpánské	štěpánský	k2eAgFnSc2d1	Štěpánská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
tiskárna	tiskárna	k1gFnSc1	tiskárna
se	se	k3xPyFc4	se
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Vilímka	Vilímek	k1gMnSc2	Vilímek
a	a	k8xC	a
Topiče	topič	k1gMnSc2	topič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
Otto	Otto	k1gMnSc1	Otto
prodal	prodat	k5eAaPmAgMnS	prodat
svou	svůj	k3xOyFgFnSc4	svůj
tiskárnu	tiskárna	k1gFnSc4	tiskárna
České	český	k2eAgFnSc2d1	Česká
grafické	grafický	k2eAgFnSc2d1	grafická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
nakladatelské	nakladatelský	k2eAgFnSc6d1	nakladatelská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
otevřel	otevřít	k5eAaPmAgMnS	otevřít
filiální	filiální	k2eAgFnSc4d1	filiální
prodejnu	prodejna	k1gFnSc4	prodejna
svých	svůj	k3xOyFgFnPc2	svůj
knih	kniha	k1gFnPc2	kniha
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Gluckgasse	Gluckgasse	k1gFnSc2	Gluckgasse
č.	č.	k?	č.
3	[number]	k4	3
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Ottovým	Ottův	k2eAgNnSc7d1	Ottovo
největším	veliký	k2eAgNnSc7d3	veliký
<g/>
,	,	kIx,	,
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
nepřekonaným	překonaný	k2eNgInSc7d1	nepřekonaný
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
27	[number]	k4	27
dílech	díl	k1gInPc6	díl
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
dodatky	dodatek	k1gInPc1	dodatek
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
několika	několik	k4yIc2	několik
reedicí	reedice	k1gFnPc2	reedice
a	a	k8xC	a
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
na	na	k7c6	na
moderním	moderní	k2eAgNnSc6d1	moderní
médiu	médium	k1gNnSc6	médium
CD-ROM	CD-ROM	k1gFnSc2	CD-ROM
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc2d1	velká
<g/>
"	"	kIx"	"
verze	verze	k1gFnPc1	verze
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
i	i	k8xC	i
Malý	Malý	k1gMnSc1	Malý
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ottův	Ottův	k2eAgInSc4d1	Ottův
kapesní	kapesní	k2eAgInSc4d1	kapesní
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
plodné	plodný	k2eAgFnSc2d1	plodná
vydavatelské	vydavatelský	k2eAgFnSc2d1	vydavatelská
práce	práce	k1gFnSc2	práce
byl	být	k5eAaImAgInS	být
aktivně	aktivně	k6eAd1	aktivně
činný	činný	k2eAgMnSc1d1	činný
také	také	k9	také
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
životě	život	k1gInSc6	život
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
,	,	kIx,	,
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
besedy	beseda	k1gFnSc2	beseda
<g/>
,	,	kIx,	,
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc2	první
občanské	občanský	k2eAgFnSc2d1	občanská
záložny	záložna	k1gFnSc2	záložna
<g/>
,	,	kIx,	,
Průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc2	vedení
Pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
Živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
banky	banka	k1gFnSc2	banka
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
též	též	k9	též
ve	v	k7c6	v
výkonném	výkonný	k2eAgInSc6d1	výkonný
výboru	výbor	k1gInSc6	výbor
Zemské	zemský	k2eAgFnSc2d1	zemská
jubilejní	jubilejní	k2eAgFnSc2d1	jubilejní
výstavy	výstava	k1gFnSc2	výstava
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
a	a	k8xC	a
výkonném	výkonný	k2eAgInSc6d1	výkonný
výboru	výbor	k1gInSc6	výbor
Národopisné	národopisný	k2eAgFnSc2d1	národopisná
výstavy	výstava	k1gFnSc2	výstava
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
členem	člen	k1gInSc7	člen
panské	panský	k2eAgFnPc4d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Udržoval	udržovat	k5eAaImAgMnS	udržovat
čilé	čilý	k2eAgInPc4d1	čilý
styky	styk	k1gInPc4	styk
s	s	k7c7	s
předními	přední	k2eAgFnPc7d1	přední
osobnostmi	osobnost	k1gFnPc7	osobnost
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
)	)	kIx)	)
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgInS	obdržet
řadu	řada	k1gFnSc4	řada
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
-	-	kIx~	-
např.	např.	kA	např.
rytíř	rytíř	k1gMnSc1	rytíř
řádu	řád	k1gInSc2	řád
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
zemské	zemský	k2eAgFnSc6d1	zemská
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
výstavě	výstava	k1gFnSc6	výstava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řád	řád	k1gInSc1	řád
Železné	železný	k2eAgFnSc2d1	železná
koruny	koruna	k1gFnSc2	koruna
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
udělen	udělen	k2eAgInSc1d1	udělen
knížetem	kníže	k1gMnSc7	kníže
Mikulášem	Mikuláš	k1gMnSc7	Mikuláš
Černohorským	Černohorský	k1gMnSc7	Černohorský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
či	či	k8xC	či
titul	titul	k1gInSc4	titul
Knihkupec	knihkupec	k1gMnSc1	knihkupec
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
od	od	k7c2	od
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rodném	rodný	k2eAgInSc6d1	rodný
kraji	kraj	k1gInSc6	kraj
podporoval	podporovat	k5eAaImAgMnS	podporovat
Otto	Otto	k1gMnSc1	Otto
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
a	a	k8xC	a
osvětové	osvětový	k2eAgInPc4d1	osvětový
spolky	spolek	k1gInPc4	spolek
<g/>
,	,	kIx,	,
probouzející	probouzející	k2eAgInPc4d1	probouzející
se	se	k3xPyFc4	se
český	český	k2eAgInSc1d1	český
národohospodářský	národohospodářský	k2eAgInSc1d1	národohospodářský
život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
např.	např.	kA	např.
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Malinským	Malinský	k2eAgMnSc7d1	Malinský
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemalou	malý	k2eNgFnSc7d1	nemalá
měrou	míra	k1gFnSc7wR	míra
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
např.	např.	kA	např.
o	o	k7c4	o
sbírku	sbírka	k1gFnSc4	sbírka
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
realizaci	realizace	k1gFnSc4	realizace
Žižkovy	Žižkův	k2eAgFnSc2d1	Žižkova
mohyly	mohyla	k1gFnSc2	mohyla
nedaleko	nedaleko	k7c2	nedaleko
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc4d1	připomínající
úmrtí	úmrtí	k1gNnSc4	úmrtí
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
(	(	kIx(	(
<g/>
odhalena	odhalen	k2eAgFnSc1d1	odhalena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
popud	popud	k1gInSc4	popud
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
též	též	k9	též
první	první	k4xOgFnSc1	první
pouť	pouť	k1gFnSc1	pouť
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
úmrtí	úmrtí	k1gNnSc2	úmrtí
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
autorství	autorství	k1gNnSc2	autorství
nápadu	nápad	k1gInSc2	nápad
uspořádání	uspořádání	k1gNnSc2	uspořádání
finanční	finanční	k2eAgFnSc2d1	finanční
sbírky	sbírka	k1gFnSc2	sbírka
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
spolku	spolek	k1gInSc2	spolek
obchodníků	obchodník	k1gMnPc2	obchodník
Merkur	Merkur	k1gMnSc1	Merkur
prosadil	prosadit	k5eAaPmAgMnS	prosadit
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
Českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
akademie	akademie	k1gFnSc2	akademie
obchodní	obchodní	k2eAgFnSc2d1	obchodní
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
jako	jako	k8xC	jako
vyučovacím	vyučovací	k2eAgInSc7d1	vyučovací
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
místopředsedou	místopředseda	k1gMnSc7	místopředseda
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
správy	správa	k1gFnSc2	správa
se	se	k3xPyFc4	se
škola	škola	k1gFnSc1	škola
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
vlastní	vlastní	k2eAgFnPc4d1	vlastní
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
penzijní	penzijní	k2eAgInSc1d1	penzijní
fond	fond	k1gInSc1	fond
pro	pro	k7c4	pro
profesorský	profesorský	k2eAgInSc4d1	profesorský
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
založená	založený	k2eAgFnSc1d1	založená
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
obdržela	obdržet	k5eAaPmAgFnS	obdržet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
jméno	jméno	k1gNnSc4	jméno
Ottovo	Ottův	k2eAgNnSc4d1	Ottovo
nadání	nadání	k1gNnSc4	nadání
jubilejní	jubilejní	k2eAgMnSc1d1	jubilejní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
působil	působit	k5eAaImAgMnS	působit
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
starosta	starosta	k1gMnSc1	starosta
grémia	grémium	k1gNnSc2	grémium
knihtiskařů	knihtiskař	k1gMnPc2	knihtiskař
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
Spolku	spolek	k1gInSc2	spolek
českých	český	k2eAgMnPc2d1	český
knihkupců	knihkupec	k1gMnPc2	knihkupec
a	a	k8xC	a
nakladatelů	nakladatel	k1gMnPc2	nakladatel
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
podobných	podobný	k2eAgFnPc6d1	podobná
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
české	český	k2eAgFnSc2d1	Česká
grafické	grafický	k2eAgFnSc2d1	grafická
a	a	k8xC	a
nakladatelské	nakladatelský	k2eAgFnSc2d1	nakladatelská
společnosti	společnost	k1gFnSc2	společnost
Unie	unie	k1gFnSc2	unie
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
uprostřed	uprostřed	k7c2	uprostřed
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
činný	činný	k2eAgMnSc1d1	činný
byl	být	k5eAaImAgInS	být
až	až	k9	až
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
jen	jen	k9	jen
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
potěšení	potěšení	k1gNnSc4	potěšení
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
písemnictví	písemnictví	k1gNnSc3	písemnictví
naše	náš	k3xOp1gNnSc1	náš
dříve	dříve	k6eAd2	dříve
chudé	chudý	k2eAgNnSc1d1	chudé
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
rozmáhati	rozmáhat	k5eAaImF	rozmáhat
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
i	i	k9	i
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
impozantním	impozantní	k2eAgInSc6d1	impozantní
pohřbu	pohřeb	k1gInSc6	pohřeb
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
pochováno	pochovat	k5eAaPmNgNnS	pochovat
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
se	se	k3xPyFc4	se
den	den	k1gInSc1	den
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
úmrtí	úmrtí	k1gNnSc6	úmrtí
sešla	sejít	k5eAaPmAgFnS	sejít
smuteční	smuteční	k2eAgFnSc1d1	smuteční
schůze	schůze	k1gFnSc1	schůze
městského	městský	k2eAgNnSc2d1	Městské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozesláno	rozeslat	k5eAaPmNgNnS	rozeslat
na	na	k7c4	na
200	[number]	k4	200
smutečních	smuteční	k2eAgNnPc2d1	smuteční
oznámení	oznámení	k1gNnPc2	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
Jana	Jan	k1gMnSc2	Jan
Otty	Otta	k1gMnSc2	Otta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
jako	jako	k9	jako
zástupci	zástupce	k1gMnPc1	zástupce
města	město	k1gNnSc2	město
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
starosta	starosta	k1gMnSc1	starosta
Jan	Jan	k1gMnSc1	Jan
Bechyně	Bechyně	k1gMnSc1	Bechyně
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
výročí	výročí	k1gNnSc2	výročí
40	[number]	k4	40
let	léto	k1gNnPc2	léto
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
Ottovy	Ottův	k2eAgFnSc2d1	Ottova
nakladatelské	nakladatelský	k2eAgFnSc2d1	nakladatelská
činnosti	činnost	k1gFnSc2	činnost
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
-	-	kIx~	-
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
vzpomínaly	vzpomínat	k5eAaImAgInP	vzpomínat
a	a	k8xC	a
blahopřejné	blahopřejný	k2eAgInPc1d1	blahopřejný
dopisy	dopis	k1gInPc1	dopis
mu	on	k3xPp3gMnSc3	on
zasílaly	zasílat	k5eAaImAgFnP	zasílat
osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k8xS	jako
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Ignát	Ignát	k1gMnSc1	Ignát
Herrmann	Herrmann	k1gMnSc1	Herrmann
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Šváb-Malostranský	Šváb-Malostranský	k2eAgMnSc1d1	Šváb-Malostranský
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
Otty	Otta	k1gMnSc2	Otta
byl	být	k5eAaImAgMnS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
se	se	k3xPyFc4	se
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vladimír	Vladimír	k1gMnSc1	Vladimír
Otto	Otto	k1gMnSc1	Otto
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nakladatelské	nakladatelský	k2eAgFnSc6d1	nakladatelská
činnosti	činnost	k1gFnSc6	činnost
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
pro	pro	k7c4	pro
rodinné	rodinný	k2eAgInPc4d1	rodinný
dědické	dědický	k2eAgInPc4d1	dědický
spory	spor	k1gInPc4	spor
dlouho	dlouho	k6eAd1	dlouho
nepokračoval	pokračovat	k5eNaImAgInS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
cizím	cizí	k2eAgFnPc3d1	cizí
osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
nemoudré	moudrý	k2eNgNnSc4d1	nemoudré
chápání	chápání	k1gNnSc4	chápání
piety	pieta	k1gFnSc2	pieta
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
téměř	téměř	k6eAd1	téměř
úplnému	úplný	k2eAgInSc3d1	úplný
rozpadu	rozpad	k1gInSc3	rozpad
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k8xS	co
pracné	pracný	k2eAgNnSc1d1	pracné
úsilí	úsilí	k1gNnSc1	úsilí
Jana	Jan	k1gMnSc2	Jan
Otty	Otta	k1gMnSc2	Otta
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Jana	Jan	k1gMnSc2	Jan
Otty	Otta	k1gMnSc2	Otta
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
města	město	k1gNnSc2	město
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Kurfürstova	Kurfürstův	k2eAgInSc2d1	Kurfürstův
domu	dům	k1gInSc2	dům
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1996	[number]	k4	1996
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalit	k5eAaPmNgFnS	odhalit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Jana	Jan	k1gMnSc2	Jan
Otty	Otta	k1gMnSc2	Otta
od	od	k7c2	od
přibyslavského	přibyslavský	k2eAgMnSc2d1	přibyslavský
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
a	a	k8xC	a
starosty	starosta	k1gMnSc2	starosta
Romana	Roman	k1gMnSc2	Roman
Podrázského	Podrázský	k2eAgMnSc2d1	Podrázský
<g/>
.	.	kIx.	.
</s>
<s>
Pražským	pražský	k2eAgNnSc7d1	Pražské
bydlištěm	bydliště	k1gNnSc7	bydliště
rodiny	rodina	k1gFnSc2	rodina
Jana	Jan	k1gMnSc2	Jan
Otty	Otta	k1gMnSc2	Otta
byl	být	k5eAaImAgInS	být
výstavný	výstavný	k2eAgInSc1d1	výstavný
nárožní	nárožní	k2eAgInSc1d1	nárožní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
257	[number]	k4	257
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
14	[number]	k4	14
</s>
