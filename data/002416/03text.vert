<s>
Nuda	nuda	k1gFnSc1	nuda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
"	"	kIx"	"
<g/>
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
přechodný	přechodný	k2eAgInSc1d1	přechodný
duševní	duševní	k2eAgInSc1d1	duševní
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jedinec	jedinec	k1gMnSc1	jedinec
cítí	cítit	k5eAaImIp3nS	cítit
pronikavý	pronikavý	k2eAgInSc4d1	pronikavý
nedostatek	nedostatek	k1gInSc4	nedostatek
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
své	své	k1gNnSc4	své
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
obtížné	obtížný	k2eAgNnSc1d1	obtížné
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
soustředit	soustředit	k5eAaPmF	soustředit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nuda	nuda	k1gFnSc1	nuda
tedy	tedy	k9	tedy
nutně	nutně	k6eAd1	nutně
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
vůbec	vůbec	k9	vůbec
nemá	mít	k5eNaImIp3nS	mít
co	co	k3yInSc1	co
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
pro	pro	k7c4	pro
žádnou	žádný	k3yNgFnSc4	žádný
aktivitu	aktivita	k1gFnSc4	aktivita
nadchnout	nadchnout	k5eAaPmF	nadchnout
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
se	se	k3xPyFc4	se
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
angažovat	angažovat	k5eAaBmF	angažovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nudu	nuda	k1gFnSc4	nuda
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pocity	pocit	k1gInPc1	pocit
omrzelosti	omrzelost	k1gFnSc2	omrzelost
<g/>
,	,	kIx,	,
zbytečnosti	zbytečnost	k1gFnSc2	zbytečnost
<g/>
,	,	kIx,	,
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
<g/>
,	,	kIx,	,
nezajímavosti	nezajímavost	k1gFnSc2	nezajímavost
<g/>
,	,	kIx,	,
snížená	snížený	k2eAgFnSc1d1	snížená
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
únavy	únava	k1gFnSc2	únava
a	a	k8xC	a
depresivní	depresivní	k2eAgFnSc2d1	depresivní
nálady	nálada	k1gFnSc2	nálada
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
člověku	člověk	k1gMnSc6	člověk
zabráněno	zabránit	k5eAaPmNgNnS	zabránit
věnovat	věnovat	k5eAaImF	věnovat
se	se	k3xPyFc4	se
něčemu	něco	k3yInSc3	něco
<g/>
,	,	kIx,	,
čemu	co	k3yInSc3	co
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgInS	chtít
je	on	k3xPp3gFnPc4	on
<g/>
-li	i	k?	-li
nucen	nucen	k2eAgInSc1d1	nucen
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
do	do	k7c2	do
čeho	co	k3yRnSc2	co
nemá	mít	k5eNaImIp3nS	mít
chuť	chuť	k1gFnSc1	chuť
někdy	někdy	k6eAd1	někdy
však	však	k9	však
pocit	pocit	k1gInSc1	pocit
nudy	nuda	k1gFnSc2	nuda
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
bez	bez	k7c2	bez
zjevného	zjevný	k2eAgInSc2d1	zjevný
důvodu	důvod	k1gInSc2	důvod
a	a	k8xC	a
jedinec	jedinec	k1gMnSc1	jedinec
není	být	k5eNaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
schopen	schopen	k2eAgInSc1d1	schopen
se	se	k3xPyFc4	se
angažovat	angažovat	k5eAaBmF	angažovat
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
nudit	nudita	k1gFnPc2	nudita
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
pomocí	pomocí	k7c2	pomocí
dotazníkové	dotazníkový	k2eAgFnSc2d1	dotazníková
škály	škála	k1gFnSc2	škála
Boredom	Boredom	k1gInSc1	Boredom
Proneness	Proneness	k1gInSc1	Proneness
Scale	Scale	k1gFnSc2	Scale
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
pozornosti	pozornost	k1gFnSc2	pozornost
a	a	k8xC	a
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
depresi	deprese	k1gFnSc3	deprese
či	či	k8xC	či
depresivními	depresivní	k2eAgFnPc7d1	depresivní
náladami	nálada	k1gFnPc7	nálada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
korelace	korelace	k1gFnSc1	korelace
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
pozornosti	pozornost	k1gFnSc2	pozornost
je	on	k3xPp3gMnPc4	on
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xC	jako
korelace	korelace	k1gFnSc1	korelace
s	s	k7c7	s
depresí	deprese	k1gFnSc7	deprese
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
samotná	samotný	k2eAgFnSc1d1	samotná
nuda	nuda	k1gFnSc1	nuda
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
nepříliš	příliš	k6eNd1	příliš
závažná	závažný	k2eAgFnSc1d1	závažná
obtíž	obtíž	k1gFnSc1	obtíž
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
nudit	nudita	k1gFnPc2	nudita
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
projevem	projev	k1gInSc7	projev
či	či	k8xC	či
příčinou	příčina	k1gFnSc7	příčina
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
psychologických	psychologický	k2eAgInPc2d1	psychologický
<g/>
,	,	kIx,	,
fyzických	fyzický	k2eAgInPc2d1	fyzický
<g/>
,	,	kIx,	,
pedagogických	pedagogický	k2eAgInPc2d1	pedagogický
a	a	k8xC	a
sociálních	sociální	k2eAgInPc2d1	sociální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
emoce	emoce	k1gFnPc1	emoce
apatie	apatie	k1gFnSc2	apatie
zahálka	zahálka	k1gFnSc1	zahálka
Nuda	nuda	k1gFnSc1	nuda
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nuda	nuda	k1gFnSc1	nuda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Nuda	nuda	k1gFnSc1	nuda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
