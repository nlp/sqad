<s>
Sklon	sklon	k1gInSc1	sklon
nudit	nudita	k1gFnPc2	nudita
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
pomocí	pomocí	k7c2	pomocí
dotazníkové	dotazníkový	k2eAgFnSc2d1	dotazníková
škály	škála	k1gFnSc2	škála
Boredom	Boredom	k1gInSc1	Boredom
Proneness	Proneness	k1gInSc1	Proneness
Scale	Scale	k1gFnSc2	Scale
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
pozornosti	pozornost	k1gFnSc2	pozornost
a	a	k8xC	a
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
depresi	deprese	k1gFnSc3	deprese
či	či	k8xC	či
depresivními	depresivní	k2eAgFnPc7d1	depresivní
náladami	nálada	k1gFnPc7	nálada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
korelace	korelace	k1gFnSc1	korelace
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
pozornosti	pozornost	k1gFnSc2	pozornost
je	on	k3xPp3gMnPc4	on
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
korelace	korelace	k1gFnSc1	korelace
s	s	k7c7	s
depresí	deprese	k1gFnSc7	deprese
<g/>
.	.	kIx.	.
</s>
