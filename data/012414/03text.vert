<p>
<s>
Cootehill	Cootehill	k1gInSc1	Cootehill
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Muinchille	Muinchill	k1gMnSc2	Muinchill
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Cavan	Cavana	k1gFnPc2	Cavana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1892	[number]	k4	1892
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Dromore	Dromor	k1gInSc5	Dromor
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
100	[number]	k4	100
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
přechází	přecházet	k5eAaImIp3nS	přecházet
hranice	hranice	k1gFnSc1	hranice
hrabství	hrabství	k1gNnSc2	hrabství
Monaghan	Monaghany	k1gInPc2	Monaghany
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
R	R	kA	R
<g/>
188	[number]	k4	188
<g/>
,	,	kIx,	,
R189	R189	k1gFnSc1	R189
a	a	k8xC	a
R	R	kA	R
<g/>
190	[number]	k4	190
<g/>
.	.	kIx.	.
</s>
<s>
Cavan	Cavan	k1gInSc1	Cavan
je	být	k5eAaImIp3nS	být
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
23	[number]	k4	23
km	km	kA	km
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
,	,	kIx,	,
Shercock	Shercock	k1gInSc4	Shercock
16	[number]	k4	16
km	km	kA	km
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
,	,	kIx,	,
Bailieborough	Bailieborough	k1gInSc4	Bailieborough
22	[number]	k4	22
km	km	kA	km
na	na	k7c4	na
JJV	JJV	kA	JJV
<g/>
,	,	kIx,	,
Clones	Clones	k1gInSc1	Clones
20	[number]	k4	20
km	km	kA	km
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
,	,	kIx,	,
Monaghan	Monaghan	k1gInSc4	Monaghan
24	[number]	k4	24
km	km	kA	km
na	na	k7c4	na
SSV	SSV	kA	SSV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Cootehill	Cootehill	k1gInSc1	Cootehill
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
jako	jako	k8xC	jako
středisko	středisko	k1gNnSc1	středisko
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
s	s	k7c7	s
tradicí	tradice	k1gFnSc7	tradice
zpracování	zpracování	k1gNnSc2	zpracování
lnu	len	k1gInSc2	len
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc1	město
ekonomicky	ekonomicky	k6eAd1	ekonomicky
rozkvétalo	rozkvétat	k5eAaImAgNnS	rozkvétat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
zde	zde	k6eAd1	zde
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
Ulster	Ulster	k1gInSc4	Ulster
Bank	banka	k1gFnPc2	banka
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgFnPc2	první
poboček	pobočka	k1gFnPc2	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
sídli	sídlet	k5eAaImRp2nS	sídlet
společnost	společnost	k1gFnSc1	společnost
Abbott	Abbott	k1gMnSc1	Abbott
Laboratories	Laboratories	k1gMnSc1	Laboratories
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dětskou	dětský	k2eAgFnSc4d1	dětská
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
5	[number]	k4	5
km	km	kA	km
VJV	VJV	kA	VJV
od	od	k7c2	od
města	město	k1gNnSc2	město
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
megalitické	megalitický	k2eAgNnSc1d1	megalitické
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cootehill	Cootehilla	k1gFnPc2	Cootehilla
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cootehill	Cootehilla	k1gFnPc2	Cootehilla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
