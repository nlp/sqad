<s>
Gocta	Gocta	k1gFnSc1
</s>
<s>
Vodopády	vodopád	k1gInPc1
Gocta	Gocto	k1gNnSc2
</s>
<s>
Gocta	Gocta	k1gFnSc1
je	být	k5eAaImIp3nS
vodopád	vodopád	k1gInSc4
v	v	k7c6
Peru	Peru	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
regionu	region	k1gInSc6
Amazonas	Amazonasa	k1gFnPc2
700	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Limy	Lima	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodopád	vodopád	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
stupňů	stupeň	k1gInPc2
a	a	k8xC
celková	celkový	k2eAgFnSc1d1
výška	výška	k1gFnSc1
činí	činit	k5eAaImIp3nS
771	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodopád	vodopád	k1gInSc1
Gocta	Goct	k1gInSc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
3000	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
množství	množství	k1gNnSc1
vody	voda	k1gFnSc2
značně	značně	k6eAd1
kolísá	kolísat	k5eAaImIp3nS
podle	podle	k7c2
ročního	roční	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
pátý	pátý	k4xOgInSc4
nejvyšší	vysoký	k2eAgInSc4d3
vodopád	vodopád	k1gInSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
záleží	záležet	k5eAaImIp3nS
ovšem	ovšem	k9
na	na	k7c6
zvolených	zvolený	k2eAgNnPc6d1
kritériích	kritérion	k1gNnPc6
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
počítají	počítat	k5eAaImIp3nP
i	i	k9
vodopády	vodopád	k1gInPc1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
protéká	protékat	k5eAaImIp3nS
voda	voda	k1gFnSc1
pouze	pouze	k6eAd1
občas	občas	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
zprávu	zpráva	k1gFnSc4
o	o	k7c6
existenci	existence	k1gFnSc6
vodopádů	vodopád	k1gInPc2
podal	podat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
německý	německý	k2eAgMnSc1d1
cestovatel	cestovatel	k1gMnSc1
Stefan	Stefan	k1gMnSc1
Ziemendorff	Ziemendorff	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://scienceray.com/earth-sciences/geology/the-worlds-most-amazing-waterfalls/	http://scienceray.com/earth-sciences/geology/the-worlds-most-amazing-waterfalls/	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gocta	Goct	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20131029202907/http://www.andina.com.pe/Ingles/Noticia.aspx?id=iErNEUJ+424=	https://web.archive.org/web/20131029202907/http://www.andina.com.pe/Ingles/Noticia.aspx?id=iErNEUJ+424=	k4
</s>
<s>
http://www.worldwaterfalldatabase.com/waterfall/Gocta-Catarata-805/	http://www.worldwaterfalldatabase.com/waterfall/Gocta-Catarata-805/	k4
</s>
