<s>
Kulturistika	kulturistika	k1gFnSc1	kulturistika
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgInSc4d1	individuální
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
vzhledu	vzhled	k1gInSc2	vzhled
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
např.	např.	kA	např.
mohutnost	mohutnost	k1gFnSc1	mohutnost
a	a	k8xC	a
vyrýsovanost	vyrýsovanost	k1gFnSc1	vyrýsovanost
svalstva	svalstvo	k1gNnSc2	svalstvo
-	-	kIx~	-
množství	množství	k1gNnSc1	množství
podkožního	podkožní	k2eAgInSc2d1	podkožní
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
svalové	svalový	k2eAgFnSc2d1	svalová
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc1	tvrdost
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
symetrie	symetrie	k1gFnSc1	symetrie
a	a	k8xC	a
estetičnost	estetičnost	k1gFnSc1	estetičnost
postavy	postava	k1gFnSc2	postava
apod.	apod.	kA	apod.
Zásadní	zásadní	k2eAgFnSc2d1	zásadní
partie	partie	k1gFnSc2	partie
jsou	být	k5eAaImIp3nP	být
široká	široký	k2eAgNnPc4d1	široké
záda	záda	k1gNnPc4	záda
i	i	k8xC	i
ramena	rameno	k1gNnPc4	rameno
<g/>
,	,	kIx,	,
úzký	úzký	k2eAgInSc4d1	úzký
pas	pas	k1gInSc4	pas
a	a	k8xC	a
mohutné	mohutný	k2eAgFnPc4d1	mohutná
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
probíhá	probíhat	k5eAaImIp3nS	probíhat
posilováním	posilování	k1gNnSc7	posilování
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
posilovnách	posilovna	k1gFnPc6	posilovna
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
strava	strava	k1gFnSc1	strava
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
přísun	přísun	k1gInSc4	přísun
potřebných	potřebný	k2eAgFnPc2d1	potřebná
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
minimum	minimum	k1gNnSc4	minimum
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kulturisté	kulturista	k1gMnPc1	kulturista
však	však	k9	však
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
nárůst	nárůst	k1gInSc4	nárůst
svalové	svalový	k2eAgFnSc2d1	svalová
hmoty	hmota	k1gFnSc2	hmota
pomocí	pomocí	k7c2	pomocí
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
doping	doping	k1gInSc1	doping
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
anabolické	anabolický	k2eAgInPc1d1	anabolický
steroidy	steroid	k1gInPc1	steroid
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
faktorem	faktor	k1gInSc7	faktor
důležitým	důležitý	k2eAgInSc7d1	důležitý
pro	pro	k7c4	pro
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
vzhled	vzhled	k1gInSc4	vzhled
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
odpočinku	odpočinek	k1gInSc2	odpočinek
po	po	k7c6	po
namáhavém	namáhavý	k2eAgInSc6d1	namáhavý
tréninku	trénink	k1gInSc6	trénink
<g/>
.	.	kIx.	.
</s>
<s>
Tvarování	tvarování	k1gNnSc1	tvarování
těla	tělo	k1gNnSc2	tělo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zdvihání	zdvihání	k1gNnSc2	zdvihání
závaží	závaží	k1gNnSc2	závaží
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
již	již	k6eAd1	již
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
sport	sport	k1gInSc1	sport
se	se	k3xPyFc4	se
kulturistika	kulturistika	k1gFnSc1	kulturistika
prosadila	prosadit	k5eAaPmAgFnS	prosadit
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
International	International	k1gFnPc2	International
Federation	Federation	k1gInSc4	Federation
of	of	k?	of
Body	bod	k1gInPc4	bod
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
IFBB	IFBB	kA	IFBB
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
bratry	bratr	k1gMnPc7	bratr
Benem	Ben	k1gInSc7	Ben
a	a	k8xC	a
Joem	Joe	k1gNnSc7	Joe
Weiderovými	Weiderův	k2eAgMnPc7d1	Weiderův
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
oficiální	oficiální	k2eAgInSc4d1	oficiální
sport	sport	k1gInSc4	sport
se	se	k3xPyFc4	se
však	však	k9	však
kulturistika	kulturistika	k1gFnSc1	kulturistika
dočkala	dočkat	k5eAaPmAgFnS	dočkat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nese	nést	k5eAaImIp3nS	nést
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
název	název	k1gInSc1	název
International	International	k1gMnSc1	International
Federation	Federation	k1gInSc1	Federation
of	of	k?	of
Bodybuilding	Bodybuilding	k1gInSc1	Bodybuilding
and	and	k?	and
Fitness	Fitness	k1gInSc1	Fitness
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
původní	původní	k2eAgFnSc4d1	původní
zkratku	zkratka	k1gFnSc4	zkratka
IFBB	IFBB	kA	IFBB
používá	používat	k5eAaImIp3nS	používat
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
základy	základ	k1gInPc7	základ
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kulturistiky	kulturistika	k1gFnSc2	kulturistika
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
zkoumáním	zkoumání	k1gNnSc7	zkoumání
silových	silový	k2eAgNnPc2d1	silové
cvičení	cvičení	k1gNnPc2	cvičení
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
systematickým	systematický	k2eAgNnSc7d1	systematické
řízením	řízení	k1gNnSc7	řízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
vznikaly	vznikat	k5eAaImAgInP	vznikat
různé	různý	k2eAgInPc1d1	různý
cvičební	cvičební	k2eAgInPc1d1	cvičební
systémy	systém	k1gInPc1	systém
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
postavou	postava	k1gFnSc7	postava
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
vlastním	vlastní	k2eAgMnSc7d1	vlastní
zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
kulturistiky	kulturistika	k1gFnSc2	kulturistika
je	být	k5eAaImIp3nS	být
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Sandow	Sandow	k1gMnSc1	Sandow
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Müller	Müller	k1gMnSc1	Müller
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Body-Building	Body-Building	k1gInSc1	Body-Building
(	(	kIx(	(
<g/>
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
dnešní	dnešní	k2eAgFnSc2d1	dnešní
kulturistiky	kulturistika	k1gFnSc2	kulturistika
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
dal	dát	k5eAaPmAgInS	dát
jméno	jméno	k1gNnSc4	jméno
celému	celý	k2eAgInSc3d1	celý
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gMnSc4	on
dosud	dosud	k6eAd1	dosud
oficiálně	oficiálně	k6eAd1	oficiálně
používáno	používán	k2eAgNnSc1d1	používáno
<g/>
.	.	kIx.	.
</s>
<s>
Sandowým	Sandowý	k1gMnSc7	Sandowý
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
s	s	k7c7	s
nemenšími	malý	k2eNgFnPc7d2	Nemenší
zásluhami	zásluha	k1gFnPc7	zásluha
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
kulturistiky	kulturistika	k1gFnSc2	kulturistika
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Grimek	Grimek	k1gMnSc1	Grimek
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
slovenských	slovenský	k2eAgMnPc2d1	slovenský
vystěhovalců	vystěhovalec	k1gMnPc2	vystěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Johna	John	k1gMnSc4	John
G.	G.	kA	G.
Grimka	Grimek	k1gMnSc4	Grimek
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
Steve	Steve	k1gMnSc1	Steve
Reeves	Reeves	k1gMnSc1	Reeves
<g/>
.	.	kIx.	.
</s>
<s>
Nástupcem	nástupce	k1gMnSc7	nástupce
Steve	Steve	k1gMnSc1	Steve
Reevese	Reevese	k1gFnSc2	Reevese
byl	být	k5eAaImAgMnS	být
Larry	Larr	k1gInPc4	Larr
Scott	Scotta	k1gFnPc2	Scotta
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
držitel	držitel	k1gMnSc1	držitel
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
zavedeného	zavedený	k2eAgNnSc2d1	zavedené
uznání	uznání	k1gNnSc2	uznání
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
kulturistika	kulturistika	k1gFnSc1	kulturistika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
píše	psát	k5eAaImIp3nS	psát
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
komise	komise	k1gFnSc2	komise
kulturistiky	kulturistika	k1gFnSc2	kulturistika
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vrcholným	vrcholný	k2eAgMnPc3d1	vrcholný
kulturistům	kulturista	k1gMnPc3	kulturista
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
Juraj	Juraj	k1gMnSc1	Juraj
Višný	Višný	k1gMnSc1	Višný
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
Pipasík	Pipasík	k1gMnSc1	Pipasík
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Okša	Okša	k1gMnSc1	Okša
z	z	k7c2	z
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bartoš	Bartoš	k1gMnSc1	Bartoš
z	z	k7c2	z
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Nosek	Nosek	k1gMnSc1	Nosek
z	z	k7c2	z
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
Lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Kulturisté	kulturista	k1gMnPc1	kulturista
používají	používat	k5eAaImIp3nP	používat
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
body	bod	k1gInPc4	bod
pro	pro	k7c4	pro
svalovou	svalový	k2eAgFnSc4d1	svalová
hypertrofii	hypertrofie	k1gFnSc4	hypertrofie
<g/>
:	:	kIx,	:
posilování	posilování	k1gNnSc2	posilování
(	(	kIx(	(
<g/>
zvedání	zvedání	k1gNnSc1	zvedání
činek	činka	k1gFnPc2	činka
v	v	k7c6	v
patřičném	patřičný	k2eAgInSc6d1	patřičný
počtu	počet	k1gInSc6	počet
<g/>
)	)	kIx)	)
strava	strava	k1gFnSc1	strava
(	(	kIx(	(
<g/>
protein	protein	k1gInSc1	protein
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
,	,	kIx,	,
tuky	tuk	k1gInPc1	tuk
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
odpočinek	odpočinek	k1gInSc4	odpočinek
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
a	a	k8xC	a
občas	občas	k6eAd1	občas
podceňovaný	podceňovaný	k2eAgInSc1d1	podceňovaný
Fitness	Fitness	k1gInSc1	Fitness
Vzpírání	vzpírání	k1gNnSc2	vzpírání
Silový	silový	k2eAgInSc1d1	silový
trojboj	trojboj	k1gInSc1	trojboj
</s>
