<s>
Tvarování	tvarování	k1gNnSc1	tvarování
těla	tělo	k1gNnSc2	tělo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zdvihání	zdvihání	k1gNnSc2	zdvihání
závaží	závaží	k1gNnSc2	závaží
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
již	již	k6eAd1	již
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
sport	sport	k1gInSc1	sport
se	se	k3xPyFc4	se
kulturistika	kulturistika	k1gFnSc1	kulturistika
prosadila	prosadit	k5eAaPmAgFnS	prosadit
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
