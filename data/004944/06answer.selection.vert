<s>
Systém	systém	k1gInSc1	systém
japonského	japonský	k2eAgNnSc2d1	Japonské
písma	písmo	k1gNnSc2	písmo
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejsložitějším	složitý	k2eAgNnPc3d3	nejsložitější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
kombinace	kombinace	k1gFnSc1	kombinace
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
fonetické	fonetický	k2eAgFnPc1d1	fonetická
slabičné	slabičný	k2eAgFnPc1d1	slabičná
abecedy	abeceda	k1gFnPc1	abeceda
(	(	kIx(	(
<g/>
hiragana	hiragana	k1gFnSc1	hiragana
a	a	k8xC	a
katakana	katakana	k1gFnSc1	katakana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
latinka	latinka	k1gFnSc1	latinka
(	(	kIx(	(
<g/>
rómadži	rómadzat	k5eAaPmIp1nS	rómadzat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
