<p>
<s>
Velociraptor	Velociraptor	k1gInSc1	Velociraptor
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
srpodrápý	srpodrápý	k2eAgMnSc1d1	srpodrápý
teropodní	teropodní	k2eAgMnSc1d1	teropodní
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
dravé	dravý	k2eAgInPc4d1	dravý
dromeosauridy	dromeosaurid	k1gInPc4	dromeosaurid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Areál	areál	k1gInSc1	areál
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
rozšíření	rozšíření	k1gNnSc2	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
malý	malý	k2eAgInSc1d1	malý
teropod	teropod	k1gInSc1	teropod
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
období	období	k1gNnSc6	období
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
(	(	kIx(	(
<g/>
geol.	geol.	k?	geol.
stupeň	stupeň	k1gInSc1	stupeň
kampán	kampat	k5eAaPmNgInS	kampat
až	až	k9	až
maastricht	maastricht	k1gInSc1	maastricht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
75	[number]	k4	75
až	až	k9	až
71	[number]	k4	71
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
paleontolog	paleontolog	k1gMnSc1	paleontolog
Henry	Henry	k1gMnSc1	Henry
Fairfield	Fairfield	k1gMnSc1	Fairfield
Osborn	Osborn	k1gMnSc1	Osborn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velociraptor	Velociraptor	k1gInSc1	Velociraptor
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
asi	asi	k9	asi
1,5	[number]	k4	1,5
až	až	k9	až
2,1	[number]	k4	2,1
metru	metr	k1gInSc2	metr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
zaživa	zaživa	k6eAd1	zaživa
vážil	vážit	k5eAaImAgInS	vážit
kolem	kolem	k7c2	kolem
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Výškou	výška	k1gFnSc7	výška
hřbetu	hřbet	k1gInSc2	hřbet
nepřesáhl	přesáhnout	k5eNaPmAgMnS	přesáhnout
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
cm	cm	kA	cm
a	a	k8xC	a
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
tedy	tedy	k9	tedy
velikosti	velikost	k1gFnSc6	velikost
statného	statný	k2eAgMnSc4d1	statný
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
každopádně	každopádně	k6eAd1	každopádně
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
zpodobňován	zpodobňovat	k5eAaImNgInS	zpodobňovat
například	například	k6eAd1	například
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
tam	tam	k6eAd1	tam
velikostně	velikostně	k6eAd1	velikostně
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
spíše	spíše	k9	spíše
rodu	rod	k1gInSc2	rod
Deinonychus	Deinonychus	k1gInSc1	Deinonychus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
drobné	drobný	k2eAgInPc4d1	drobný
ostré	ostrý	k2eAgInPc4d1	ostrý
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
"	"	kIx"	"
<g/>
srpovitý	srpovitý	k2eAgInSc4d1	srpovitý
<g/>
"	"	kIx"	"
dráp	dráp	k1gInSc4	dráp
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
kolem	kolem	k7c2	kolem
6,5	[number]	k4	6,5
centimetru	centimetr	k1gInSc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Alespoň	alespoň	k9	alespoň
na	na	k7c6	na
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
byl	být	k5eAaImAgInS	být
nepochybně	pochybně	k6eNd1	pochybně
opeřený	opeřený	k2eAgInSc1d1	opeřený
(	(	kIx(	(
<g/>
paže	paže	k1gFnSc1	paže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velociraptor	Velociraptor	k1gMnSc1	Velociraptor
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
lovil	lovit	k5eAaImAgMnS	lovit
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
smečkách	smečka	k1gFnPc6	smečka
<g/>
,	,	kIx,	,
zabíjel	zabíjet	k5eAaImAgInS	zabíjet
malé	malý	k2eAgFnPc4d1	malá
a	a	k8xC	a
středně	středně	k6eAd1	středně
velké	velký	k2eAgMnPc4d1	velký
obratlovce	obratlovec	k1gMnPc4	obratlovec
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
jiných	jiný	k2eAgMnPc2d1	jiný
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tehdy	tehdy	k6eAd1	tehdy
hojného	hojný	k2eAgMnSc4d1	hojný
rohatého	rohatý	k2eAgMnSc4d1	rohatý
dinosaura	dinosaurus	k1gMnSc4	dinosaurus
druhu	druh	k1gInSc2	druh
Protoceratops	Protoceratops	k1gInSc1	Protoceratops
andrewsi	andrewse	k1gFnSc4	andrewse
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgMnSc7d2	veliký
příbuzným	příbuzný	k1gMnSc7	příbuzný
velociraptora	velociraptor	k1gMnSc2	velociraptor
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
Utahraptor	Utahraptor	k1gInSc1	Utahraptor
objevený	objevený	k2eAgInSc1d1	objevený
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
i	i	k8xC	i
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
běhu	běh	k1gInSc2	běh
velociraptora	velociraptor	k1gMnSc2	velociraptor
(	(	kIx(	(
<g/>
u	u	k7c2	u
jedince	jedinec	k1gMnSc2	jedinec
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
20	[number]	k4	20
kg	kg	kA	kg
<g/>
)	)	kIx)	)
na	na	k7c4	na
10,8	[number]	k4	10,8
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
38,9	[number]	k4	38,9
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc4d1	známý
především	především	k6eAd1	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
záporné	záporný	k2eAgFnSc3d1	záporná
"	"	kIx"	"
<g/>
roli	role	k1gFnSc3	role
<g/>
"	"	kIx"	"
dravého	dravý	k2eAgMnSc2d1	dravý
zabijáka	zabiják	k1gMnSc2	zabiják
ve	v	k7c6	v
velkofilmu	velkofilm	k1gInSc6	velkofilm
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
třech	tři	k4xCgFnPc6	tři
pokračování	pokračování	k1gNnSc3	pokračování
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jurský	jurský	k2eAgInSc1d1	jurský
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
byl	být	k5eAaImAgInS	být
velociraptor	velociraptor	k1gInSc1	velociraptor
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
charakteristikách	charakteristika	k1gFnPc6	charakteristika
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
dosti	dosti	k6eAd1	dosti
přehnaně	přehnaně	k6eAd1	přehnaně
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
inteligentní	inteligentní	k2eAgInPc1d1	inteligentní
<g/>
,	,	kIx,	,
bleskově	bleskově	k6eAd1	bleskově
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
s	s	k7c7	s
nekonečným	konečný	k2eNgInSc7d1	nekonečný
apetitem	apetit	k1gInSc7	apetit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
prostá	prostý	k2eAgFnSc1d1	prostá
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmový	filmový	k2eAgInSc1d1	filmový
velociraptor	velociraptor	k1gInSc1	velociraptor
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
severoamerického	severoamerický	k2eAgMnSc2d1	severoamerický
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
mohutnějšího	mohutný	k2eAgInSc2d2	mohutnější
rodu	rod	k1gInSc2	rod
Deinonychus	Deinonychus	k1gInSc1	Deinonychus
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc6	film
panovala	panovat	k5eAaImAgFnS	panovat
mezi	mezi	k7c7	mezi
některými	některý	k3yIgFnPc7	některý
vědci	vědec	k1gMnSc6	vědec
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
rod	rod	k1gInSc4	rod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
například	například	k6eAd1	například
v	v	k7c6	v
pseudo-dokumentárním	pseudookumentární	k2eAgInSc6d1	pseudo-dokumentární
cyklu	cyklus	k1gInSc6	cyklus
Dinosaur	Dinosaura	k1gFnPc2	Dinosaura
Planet	planeta	k1gFnPc2	planeta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
v	v	k7c6	v
díle	díl	k1gInSc6	díl
seriálu	seriál	k1gInSc2	seriál
Putování	putování	k1gNnPc2	putování
s	s	k7c7	s
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
:	:	kIx,	:
Gigantičtí	gigantický	k2eAgMnPc1d1	gigantický
ještěři	ještěr	k1gMnPc1	ještěr
a	a	k8xC	a
kameo	kamea	k1gFnSc5	kamea
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
v	v	k7c6	v
Putování	putování	k1gNnSc6	putování
s	s	k7c7	s
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
:	:	kIx,	:
Monstra	monstrum	k1gNnSc2	monstrum
pravěkých	pravěký	k2eAgInPc2d1	pravěký
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
Velociraptora	Velociraptor	k1gMnSc2	Velociraptor
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
epizodě	epizoda	k1gFnSc6	epizoda
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
britského	britský	k2eAgNnSc2d1	Britské
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Pravěk	pravěk	k1gInSc1	pravěk
útočí	útočit	k5eAaImIp3nP	útočit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Unikátní	unikátní	k2eAgInSc4d1	unikátní
objev	objev	k1gInSc4	objev
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
objevila	objevit	k5eAaPmAgFnS	objevit
skupina	skupina	k1gFnSc1	skupina
polsko-mongolských	polskoongolský	k2eAgMnPc2d1	polsko-mongolský
paleontologů	paleontolog	k1gMnPc2	paleontolog
dvě	dva	k4xCgFnPc4	dva
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
zaklesnuté	zaklesnutý	k2eAgFnPc1d1	zaklesnutá
kostry	kostra	k1gFnPc1	kostra
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
-	-	kIx~	-
právě	právě	k9	právě
velociraptora	velociraptor	k1gMnSc4	velociraptor
a	a	k8xC	a
býložravého	býložravý	k2eAgMnSc4d1	býložravý
protoceratopse	protoceratops	k1gMnSc4	protoceratops
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
unikátní	unikátní	k2eAgInSc4d1	unikátní
nález	nález	k1gInSc4	nález
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
zachovaných	zachovaný	k2eAgMnPc2d1	zachovaný
a	a	k8xC	a
pohřbených	pohřbený	k2eAgMnPc2d1	pohřbený
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
souboje	souboj	k1gInSc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
doklad	doklad	k1gInSc4	doklad
predátorského	predátorský	k2eAgNnSc2d1	predátorské
chování	chování	k1gNnSc2	chování
velociraptorů	velociraptor	k1gInPc2	velociraptor
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
dromeosauridů	dromeosaurid	k1gInPc2	dromeosaurid
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
nález	nález	k1gInSc1	nález
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
a	a	k8xC	a
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
fragmentárních	fragmentární	k2eAgInPc2d1	fragmentární
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
protoceratopse	protoceratopse	k1gFnSc2	protoceratopse
s	s	k7c7	s
jasně	jasně	k6eAd1	jasně
patrnými	patrný	k2eAgFnPc7d1	patrná
rýhami	rýha	k1gFnPc7	rýha
po	po	k7c6	po
zubech	zub	k1gInPc6	zub
velociraptora	velociraptor	k1gMnSc2	velociraptor
<g/>
.	.	kIx.	.
</s>
<s>
Dravec	dravec	k1gMnSc1	dravec
zde	zde	k6eAd1	zde
zřejmě	zřejmě	k6eAd1	zřejmě
hodoval	hodovat	k5eAaImAgMnS	hodovat
na	na	k7c6	na
mršině	mršin	k2eAgFnSc6d1	mršina
protoceratopse	protoceratopsa	k1gFnSc6	protoceratopsa
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
dva	dva	k4xCgInPc4	dva
své	svůj	k3xOyFgInPc4	svůj
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
jako	jako	k8xS	jako
zkameněliny	zkamenělina	k1gFnSc2	zkamenělina
<g/>
.	.	kIx.	.
<g/>
Tito	tento	k3xDgMnPc1	tento
2	[number]	k4	2
velociraptoři	velociraptor	k1gMnPc1	velociraptor
z	z	k7c2	z
nálezu	nález	k1gInSc2	nález
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
zřejmě	zřejmě	k6eAd1	zřejmě
při	při	k7c6	při
písečné	písečný	k2eAgFnSc6d1	písečná
bouři	bouř	k1gFnSc6	bouř
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gInPc7	jejich
ostatky	ostatek	k1gInPc7	ostatek
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výborném	výborný	k2eAgInSc6d1	výborný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Opeření	opeření	k1gNnSc2	opeření
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
vědecká	vědecký	k2eAgFnSc1d1	vědecká
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
pernatého	pernatý	k2eAgInSc2d1	pernatý
pokryvu	pokryv	k1gInSc2	pokryv
těla	tělo	k1gNnSc2	tělo
u	u	k7c2	u
tohoto	tento	k3xDgMnSc2	tento
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ulnárních	ulnární	k2eAgFnPc2d1	ulnární
papil	papila	k1gFnPc2	papila
na	na	k7c6	na
kostech	kost	k1gFnPc6	kost
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
byli	být	k5eAaImAgMnP	být
alespoň	alespoň	k9	alespoň
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
pernatým	pernaté	k1gNnSc7	pernaté
integumentem	integument	k1gMnSc7	integument
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studie	studie	k1gFnSc2	studie
loketní	loketní	k2eAgFnSc2d1	loketní
kosti	kost	k1gFnSc2	kost
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
14	[number]	k4	14
loketních	loketní	k2eAgFnPc2d1	loketní
letek	letka	k1gFnPc2	letka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potvrzení	potvrzení	k1gNnSc1	potvrzení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
velociraptora	velociraptor	k1gMnSc2	velociraptor
(	(	kIx(	(
<g/>
V.	V.	kA	V.
mongoliensis	mongoliensis	k1gFnSc2	mongoliensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
belgicko-čínskou	belgicko-čínský	k2eAgFnSc7d1	belgicko-čínský
expedicí	expedice	k1gFnSc7	expedice
získáno	získán	k2eAgNnSc1d1	získáno
množství	množství	k1gNnSc1	množství
lebečního	lebeční	k2eAgInSc2d1	lebeční
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
patřícího	patřící	k2eAgMnSc2d1	patřící
zjevně	zjevně	k6eAd1	zjevně
jinému	jiný	k2eAgInSc3d1	jiný
druhu	druh	k1gInSc3	druh
rodu	rod	k1gInSc2	rod
Velociraptor	Velociraptor	k1gInSc4	Velociraptor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
nálezu	nález	k1gInSc2	nález
popsán	popsán	k2eAgInSc1d1	popsán
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
velociraptora	velociraptor	k1gMnSc2	velociraptor
<g/>
,	,	kIx,	,
Velociraptor	Velociraptor	k1gInSc1	Velociraptor
osmolskae	osmolskaat	k5eAaPmIp3nS	osmolskaat
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
(	(	kIx(	(
<g/>
souvrství	souvrství	k1gNnSc1	souvrství
Bayan	Bayana	k1gFnPc2	Bayana
Mandahu	Mandah	k1gInSc2	Mandah
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velociraptor	Velociraptor	k1gInSc1	Velociraptor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Velociraptor	Velociraptor	k1gInSc4	Velociraptor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
Co	co	k9	co
bylo	být	k5eAaImAgNnS	být
špatně	špatně	k6eAd1	špatně
na	na	k7c6	na
velociraptorech	velociraptor	k1gInPc6	velociraptor
z	z	k7c2	z
Jurského	jurský	k2eAgInSc2d1	jurský
parku	park	k1gInSc2	park
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
webu	web	k1gInSc6	web
Natural	Natural	k?	Natural
History	Histor	k1gInPc7	Histor
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
velociraptorovi	velociraptor	k1gMnSc6	velociraptor
na	na	k7c6	na
webu	web	k1gInSc6	web
Dinomuseum	Dinomuseum	k1gInSc1	Dinomuseum
<g/>
.	.	kIx.	.
<g/>
ca	ca	kA	ca
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
