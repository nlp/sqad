<s>
Milan	Milan	k1gMnSc1
Mrukvia	Mrukvium	k1gNnSc2
</s>
<s>
Milan	Milan	k1gMnSc1
MrukviaOsobní	MrukviaOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1946	#num#	k4
(	(	kIx(
<g/>
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Poprad	Poprad	k1gInSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
178	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
93	#num#	k4
kg	kg	kA
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Fofo	Fofo	k1gMnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Číslo	číslo	k1gNnSc1
</s>
<s>
12	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
útočník	útočník	k1gMnSc1
Kluby	klub	k1gInPc1
</s>
<s>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJDDukla	CHZJDDukla	k1gMnSc2
TrenčínLB	TrenčínLB	k1gMnSc2
ZvolenEHC	ZvolenEHC	k1gMnSc1
BaselEHC	BaselEHC	k1gMnSc2
Aarau	Aaraus	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Československá	československý	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
</s>
<s>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
</s>
<s>
Milan	Milan	k1gMnSc1
Mrukvia	Mrukvia	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1946	#num#	k4
Poprad	Poprad	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
slovenský	slovenský	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
<g/>
,	,	kIx,
útočník	útočník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hokejová	hokejový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Slovan	Slovan	k1gInSc4
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
a	a	k8xC
během	během	k7c2
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
za	za	k7c4
Duklu	Dukla	k1gFnSc4
Trenčín	Trenčín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
Slovanem	Slovan	k1gMnSc7
získal	získat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
nastupoval	nastupovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
střední	střední	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
s	s	k7c7
Miroslavem	Miroslav	k1gMnSc7
Miklošovičem	Miklošovič	k1gMnSc7
a	a	k8xC
Dušanem	Dušan	k1gMnSc7
Žiškou	Žiška	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zisku	zisk	k1gInSc6
mistrovského	mistrovský	k2eAgInSc2d1
titulu	titul	k1gInSc2
musel	muset	k5eAaImAgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
LB	LB	kA
Zvolen	Zvolen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
emigraci	emigrace	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
švýcarskou	švýcarský	k2eAgFnSc4d1
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
za	za	k7c4
EHC	EHC	kA
Basel	Basel	k1gInSc4
a	a	k8xC
EHC	EHC	kA
Aarau	Aaraus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
Československa	Československo	k1gNnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1968-1970	1968-1970	k4
v	v	k7c6
7	#num#	k4
utkáních	utkání	k1gNnPc6
a	a	k8xC
dal	dát	k5eAaPmAgMnS
2	#num#	k4
góly	gól	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
<g/>
31691520	#num#	k4
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
<g/>
Dukla	Dukla	k1gFnSc1
Trenčín	Trenčín	k1gInSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
<g/>
2234714	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
<g/>
2052712	#num#	k4
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
<g/>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
CHZJD	CHZJD	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
liga-----	liga-----	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Milan	Milan	k1gMnSc1
Mrukvia	Mrukvium	k1gNnSc2
–	–	k?
statistiky	statistika	k1gFnSc2
na	na	k7c4
Eliteprospects	Eliteprospects	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
světových	světový	k2eAgInPc2d1
hokejových	hokejový	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
a	a	k8xC
zlínského	zlínský	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
</s>
<s>
Milan	Milan	k1gMnSc1
Mrukvia	Mrukvium	k1gNnSc2
<g/>
:	:	kIx,
Mládež	mládež	k1gFnSc1
by	by	kYmCp3nS
nemala	mat	k5eNaBmAgFnS,k5eNaImAgFnS,k5eNaPmAgFnS
vyrastať	vyrastatit	k5eAaImRp2nS,k5eAaPmRp2nS
v	v	k7c6
strese	stres	k1gInSc5
ani	ani	k8xC
podliehať	podliehatit	k5eAaImRp2nS,k5eAaPmRp2nS
rodičovským	rodičovský	k2eAgInPc3d1
snom	snom	k6eAd1
o	o	k7c6
NHL	NHL	kA
</s>
<s>
Mrukvia	Mrukvia	k1gFnSc1
<g/>
:	:	kIx,
Švajčiari	Švajčiare	k1gFnSc4
hrali	hranout	k5eAaPmAgMnP,k5eAaImAgMnP
a	a	k8xC
dýchali	dýchat	k5eAaImAgMnP
ako	ako	k?
jeden	jeden	k4xCgMnSc1
</s>
<s>
Slovan	Slovan	k1gMnSc1
Bratislava	Bratislava	k1gFnSc1
si	se	k3xPyFc3
připomněl	připomnět	k5eAaPmAgMnS
historický	historický	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnPc1d1
mistři	mistr	k1gMnPc1
byli	být	k5eAaImAgMnP
téměř	téměř	k6eAd1
kompletní	kompletní	k2eAgMnPc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
–	–	k?
Slovan	Slovan	k1gMnSc1
CHZJD	CHZJD	kA
Bratislava	Bratislava	k1gFnSc1
Brankáři	brankář	k1gMnSc3
</s>
<s>
Marcel	Marcel	k1gMnSc1
Sakáč	Sakáč	k1gMnSc1
•	•	k?
Pavol	Pavol	k1gInSc1
Norovský	Norovský	k2eAgInSc1d1
Obránci	obránce	k1gMnPc7
</s>
<s>
Jozef	Jozef	k1gMnSc1
Bukovinský	bukovinský	k2eAgMnSc1d1
•	•	k?
Milan	Milan	k1gMnSc1
Kužela	Kužel	k1gMnSc2
•	•	k?
Ivan	Ivan	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Urban	Urban	k1gMnSc1
•	•	k?
Ľubomír	Ľubomír	k1gMnSc1
Roháčik	Roháčik	k1gMnSc1
•	•	k?
Ľubomír	Ľubomír	k1gMnSc1
Ujváry	Ujvár	k1gMnPc7
Útočníci	útočník	k1gMnPc1
</s>
<s>
Marián	Marián	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
Anton	Anton	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
•	•	k?
Marián	Marián	k1gMnSc1
Bezák	Bezák	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Miklošovič	Miklošovič	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Mrukvia	Mrukvium	k1gNnSc2
•	•	k?
Dušan	Dušan	k1gMnSc1
Pašek	Pašek	k1gMnSc1
•	•	k?
Dárius	Dárius	k1gMnSc1
Rusnák	Rusnák	k1gMnSc1
•	•	k?
Ján	Ján	k1gMnSc1
Jaško	Jaška	k1gMnSc5
•	•	k?
František	František	k1gMnSc1
Hejčík	Hejčík	k1gMnSc1
•	•	k?
Eugen	Eugen	k2eAgMnSc1d1
Krajčovič	Krajčovič	k1gMnSc1
•	•	k?
Dušan	Dušan	k1gMnSc1
Žiška	Žiška	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Horský	Horský	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
