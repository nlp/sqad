<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
měsíc	měsíc	k1gInSc1	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
nejvnitřnější	vnitřní	k2eAgMnSc1d3	nejvnitřnější
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
měsíců	měsíc	k1gInPc2	měsíc
objevených	objevený	k2eAgInPc2d1	objevený
Galileem	Galileum	k1gNnSc7	Galileum
<g/>
?	?	kIx.	?
</s>
