<p>
<s>
Io	Io	k?	Io
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
nejvnitřnější	vnitřní	k2eAgMnSc1d3	nejvnitřnější
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
měsíců	měsíc	k1gInPc2	měsíc
objevených	objevený	k2eAgInPc2d1	objevený
Galileem	Galileus	k1gMnSc7	Galileus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
3642,6	[number]	k4	3642,6
km	km	kA	km
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
po	po	k7c6	po
Íó	Íó	k1gFnSc6	Íó
–	–	k?	–
Héřině	Héřin	k2eAgFnSc6d1	Héřina
kněžce	kněžka	k1gFnSc6	kněžka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
milenkou	milenka	k1gFnSc7	milenka
vládce	vládce	k1gMnSc1	vládce
bohů	bůh	k1gMnPc2	bůh
Dia	Dia	k1gFnSc2	Dia
(	(	kIx(	(
<g/>
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc7	jeho
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
Io	Io	k1gMnPc2	Io
činí	činit	k5eAaImIp3nS	činit
geologicky	geologicky	k6eAd1	geologicky
nejaktivnější	aktivní	k2eAgNnSc1d3	nejaktivnější
těleso	těleso	k1gNnSc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Extrémní	extrémní	k2eAgFnSc1d1	extrémní
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
silných	silný	k2eAgInPc2d1	silný
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
způsobených	způsobený	k2eAgFnPc2d1	způsobená
vlivem	vliv	k1gInSc7	vliv
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
měsíc	měsíc	k1gInSc4	měsíc
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
tření	tření	k1gNnSc3	tření
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
plášť	plášť	k1gInSc1	plášť
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnPc1	erupce
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
oblaka	oblaka	k1gNnPc1	oblaka
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc4	výška
až	až	k9	až
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
hor	hora	k1gFnPc2	hora
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
vyzdvižením	vyzdvižení	k1gNnSc7	vyzdvižení
částí	část	k1gFnPc2	část
kůry	kůra	k1gFnSc2	kůra
vlivem	vlivem	k7c2	vlivem
extrémní	extrémní	k2eAgFnSc2d1	extrémní
komprese	komprese	k1gFnSc2	komprese
silikátového	silikátový	k2eAgInSc2d1	silikátový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sahají	sahat	k5eAaImIp3nP	sahat
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
Mount	Mount	k1gMnSc1	Mount
Everest	Everest	k1gInSc1	Everest
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
pozemská	pozemský	k2eAgFnSc1d1	pozemská
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
tlustou	tlustý	k2eAgFnSc4d1	tlustá
vrstvu	vrstva	k1gFnSc4	vrstva
ledu	led	k1gInSc2	led
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Io	Io	k1gFnSc1	Io
skládá	skládat	k5eAaImIp3nS	skládat
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
silikátových	silikátový	k2eAgFnPc2d1	silikátová
hornin	hornina	k1gFnPc2	hornina
obklopujících	obklopující	k2eAgInPc2d1	obklopující
roztavené	roztavený	k2eAgNnSc4d1	roztavené
železné	železný	k2eAgNnSc4d1	železné
či	či	k8xC	či
síroželeznaté	síroželeznatý	k2eAgNnSc4d1	síroželeznatý
planetární	planetární	k2eAgNnSc4d1	planetární
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
pláně	pláň	k1gFnPc4	pláň
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
zbarvení	zbarvení	k1gNnSc4	zbarvení
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
síra	síra	k1gFnSc1	síra
nebo	nebo	k8xC	nebo
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povrchový	povrchový	k2eAgInSc1d1	povrchový
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
je	být	k5eAaImIp3nS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
veliké	veliký	k2eAgNnSc4d1	veliké
množství	množství	k1gNnSc4	množství
unikátních	unikátní	k2eAgInPc2d1	unikátní
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
Io	Io	k1gFnSc6	Io
<g/>
.	.	kIx.	.
</s>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
mračna	mračna	k1gFnSc1	mračna
a	a	k8xC	a
lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
neustále	neustále	k6eAd1	neustále
přetvářejí	přetvářet	k5eAaImIp3nP	přetvářet
povrch	povrch	k6eAd1wR	povrch
měsíce	měsíc	k1gInPc4	měsíc
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gNnPc2	jeho
zbarvení	zbarvení	k1gNnPc2	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
většinou	většinou	k6eAd1	většinou
vlivem	vliv	k1gInSc7	vliv
sloučenin	sloučenina	k1gFnPc2	sloučenina
síry	síra	k1gFnPc1	síra
nabývá	nabývat	k5eAaImIp3nS	nabývat
různých	různý	k2eAgInPc2d1	různý
odstínů	odstín	k1gInPc2	odstín
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
i	i	k8xC	i
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
lávových	lávový	k2eAgInPc2d1	lávový
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
delších	dlouhý	k2eAgNnPc2d2	delší
než	než	k8xS	než
500	[number]	k4	500
km	km	kA	km
<g/>
,	,	kIx,	,
také	také	k9	také
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
rychlým	rychlý	k2eAgFnPc3d1	rychlá
změnám	změna	k1gFnPc3	změna
vzhledu	vzhled	k1gInSc2	vzhled
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
připomíná	připomínat	k5eAaImIp3nS	připomínat
povrch	povrch	k1gInSc4	povrch
pizzy	pizza	k1gFnSc2	pizza
<g/>
.	.	kIx.	.
</s>
<s>
Sopečné	sopečný	k2eAgFnPc1d1	sopečná
erupce	erupce	k1gFnPc1	erupce
stále	stále	k6eAd1	stále
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
materiál	materiál	k1gInSc4	materiál
do	do	k7c2	do
slabé	slabý	k2eAgFnSc2d1	slabá
atmosféry	atmosféra	k1gFnSc2	atmosféra
Io	Io	k1gFnSc2	Io
a	a	k8xC	a
druhotně	druhotně	k6eAd1	druhotně
i	i	k9	i
do	do	k7c2	do
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Io	Io	k1gFnSc2	Io
sehrál	sehrát	k5eAaPmAgInS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
astronomie	astronomie	k1gFnSc2	astronomie
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ho	on	k3xPp3gInSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
velkými	velký	k2eAgMnPc7d1	velký
satelity	satelit	k1gMnPc7	satelit
Jupiteru	Jupiter	k1gInSc2	Jupiter
spatřil	spatřit	k5eAaPmAgMnS	spatřit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc5	Galile
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
podpořil	podpořit	k5eAaPmAgInS	podpořit
obecné	obecný	k2eAgNnSc4d1	obecné
přijetí	přijetí	k1gNnSc4	přijetí
Koperníkova	Koperníkův	k2eAgInSc2d1	Koperníkův
heliocentrického	heliocentrický	k2eAgInSc2d1	heliocentrický
modelu	model	k1gInSc2	model
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
pohybových	pohybový	k2eAgInPc2d1	pohybový
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
první	první	k4xOgNnSc1	první
měření	měření	k1gNnSc1	měření
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
Io	Io	k1gMnSc1	Io
pouhým	pouhý	k2eAgInSc7d1	pouhý
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zlepšení	zlepšení	k1gNnSc2	zlepšení
astronomických	astronomický	k2eAgInPc2d1	astronomický
dalekohledů	dalekohled	k1gInPc2	dalekohled
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
tmavě	tmavě	k6eAd1	tmavě
červené	červený	k2eAgFnPc4d1	červená
polární	polární	k2eAgFnPc4d1	polární
a	a	k8xC	a
světlé	světlý	k2eAgFnPc4d1	světlá
rovníkové	rovníkový	k2eAgFnPc4d1	Rovníková
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prolétly	prolétnout	k5eAaPmAgFnP	prolétnout
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
dvě	dva	k4xCgFnPc4	dva
kosmické	kosmický	k2eAgFnPc4d1	kosmická
sondy	sonda	k1gFnPc4	sonda
Voyager	Voyager	k1gMnSc1	Voyager
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přinesly	přinést	k5eAaPmAgFnP	přinést
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
geologické	geologický	k2eAgFnSc6d1	geologická
aktivitě	aktivita	k1gFnSc6	aktivita
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
velkých	velký	k2eAgFnPc2d1	velká
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
mladého	mladý	k2eAgInSc2d1	mladý
povrchu	povrch	k1gInSc2	povrch
bez	bez	k7c2	bez
zjevného	zjevný	k2eAgNnSc2d1	zjevné
pokrytí	pokrytí	k1gNnSc2	pokrytí
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
několikrát	několikrát	k6eAd1	několikrát
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
kosmická	kosmický	k2eAgFnSc1d1	kosmická
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
Io	Io	k1gFnSc2	Io
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pomocí	pomocí	k7c2	pomocí
přeletu	přelet	k1gInSc2	přelet
sondy	sonda	k1gFnSc2	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Io	Io	k1gFnSc2	Io
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
poloměrem	poloměr	k1gInSc7	poloměr
1821,3	[number]	k4	1821,3
km	km	kA	km
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
náš	náš	k3xOp1gInSc1	náš
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
5	[number]	k4	5
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
8,931	[number]	k4	8,931
<g/>
9	[number]	k4	9
<g/>
×	×	k?	×
<g/>
1022	[number]	k4	1022
kg	kg	kA	kg
(	(	kIx(	(
<g/>
o	o	k7c4	o
21	[number]	k4	21
procent	procento	k1gNnPc2	procento
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Io	Io	k?	Io
má	mít	k5eAaImIp3nS	mít
lehce	lehko	k6eAd1	lehko
elipsovitý	elipsovitý	k2eAgInSc4d1	elipsovitý
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
osou	osa	k1gFnSc7	osa
směřující	směřující	k2eAgFnSc7d1	směřující
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Galileovými	Galileův	k2eAgInPc7d1	Galileův
měsíci	měsíc	k1gInPc7	měsíc
se	se	k3xPyFc4	se
v	v	k7c6	v
uvedených	uvedený	k2eAgInPc6d1	uvedený
parametrech	parametr	k1gInPc6	parametr
řadí	řadit	k5eAaImIp3nS	řadit
před	před	k7c4	před
Europa	Europ	k1gMnSc4	Europ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
Callisto	Callista	k1gMnSc5	Callista
a	a	k8xC	a
Ganymeda	Ganymed	k1gMnSc4	Ganymed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Složením	složení	k1gNnSc7	složení
primárně	primárně	k6eAd1	primárně
ze	z	k7c2	z
silikátů	silikát	k1gInPc2	silikát
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
se	se	k3xPyFc4	se
Io	Io	k1gFnSc1	Io
více	hodně	k6eAd2	hodně
podobá	podobat	k5eAaImIp3nS	podobat
terestrickým	terestrický	k2eAgFnPc3d1	terestrická
planetám	planeta	k1gFnPc3	planeta
než	než	k8xS	než
ostatním	ostatní	k2eAgInPc3d1	ostatní
měsícům	měsíc	k1gInPc3	měsíc
vnější	vnější	k2eAgFnSc2d1	vnější
oblasti	oblast	k1gFnSc2	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
většinou	většinou	k6eAd1	většinou
tvoří	tvořit	k5eAaImIp3nP	tvořit
směsice	směsice	k1gFnPc1	směsice
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
a	a	k8xC	a
silikátů	silikát	k1gInPc2	silikát
<g/>
.	.	kIx.	.
</s>
<s>
Io	Io	k?	Io
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
3,527	[number]	k4	3,527
<g/>
5	[number]	k4	5
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hustota	hustota	k1gFnSc1	hustota
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
pozemského	pozemský	k2eAgInSc2d1	pozemský
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
značně	značně	k6eAd1	značně
vyšší	vysoký	k2eAgInPc4d2	vyšší
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
hmotnosti	hmotnost	k1gFnSc2	hmotnost
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
kvadrupolových	kvadrupolový	k2eAgInPc2d1	kvadrupolový
gravitačních	gravitační	k2eAgInPc2d1	gravitační
koeficientů	koeficient	k1gInPc2	koeficient
(	(	kIx(	(
<g/>
číselné	číselný	k2eAgFnSc2d1	číselná
hodnoty	hodnota	k1gFnSc2	hodnota
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
rozložena	rozložen	k2eAgFnSc1d1	rozložena
okolo	okolo	k7c2	okolo
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
měření	měření	k1gNnSc6	měření
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
a	a	k8xC	a
Galileo	Galilea	k1gFnSc5	Galilea
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
silikátovou	silikátový	k2eAgFnSc4d1	silikátová
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
železné	železný	k2eAgNnSc1d1	železné
či	či	k8xC	či
síroželezité	síroželezitý	k2eAgNnSc1d1	síroželezitý
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1	kovové
jádro	jádro	k1gNnSc1	jádro
měsíce	měsíc	k1gInSc2	měsíc
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
obsahu	obsah	k1gInSc6	obsah
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
čistě	čistě	k6eAd1	čistě
železné	železný	k2eAgNnSc1d1	železné
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
velké	velká	k1gFnSc3	velká
350	[number]	k4	350
až	až	k9	až
650	[number]	k4	650
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
významnější	významný	k2eAgInSc4d2	významnější
podíl	podíl	k1gInSc4	podíl
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
mezi	mezi	k7c7	mezi
550	[number]	k4	550
až	až	k9	až
900	[number]	k4	900
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Sondě	sonda	k1gFnSc3	sonda
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
magnetometrem	magnetometr	k1gInSc7	magnetometr
nepovedlo	povést	k5eNaPmAgNnS	povést
detekovat	detekovat	k5eAaImF	detekovat
žádné	žádný	k3yNgNnSc4	žádný
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jádro	jádro	k1gNnSc1	jádro
patrně	patrně	k6eAd1	patrně
není	být	k5eNaImIp3nS	být
tekuté	tekutý	k2eAgNnSc1d1	tekuté
<g/>
.	.	kIx.	.
<g/>
Modelace	modelace	k1gFnSc1	modelace
pláště	plášť	k1gInSc2	plášť
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
minimálně	minimálně	k6eAd1	minimálně
ze	z	k7c2	z
75	[number]	k4	75
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
fosteritu	fosterit	k1gInSc2	fosterit
<g/>
,	,	kIx,	,
minerálu	minerál	k1gInSc2	minerál
bohatého	bohatý	k2eAgInSc2d1	bohatý
na	na	k7c4	na
hořčík	hořčík	k1gInSc4	hořčík
<g/>
,	,	kIx,	,
s	s	k7c7	s
minoritním	minoritní	k2eAgNnSc7d1	minoritní
zastoupením	zastoupení	k1gNnSc7	zastoupení
podobným	podobný	k2eAgNnSc7d1	podobné
L	L	kA	L
chondritům	chondrit	k1gInPc3	chondrit
a	a	k8xC	a
LL	LL	kA	LL
chondritům	chondrit	k1gInPc3	chondrit
(	(	kIx(	(
<g/>
meteoritům	meteorit	k1gInPc3	meteorit
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
pláště	plášť	k1gInSc2	plášť
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
roztaveno	roztaven	k2eAgNnSc1d1	roztaveno
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
vysokoteplotním	vysokoteplotní	k2eAgInSc7d1	vysokoteplotní
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Litosféra	litosféra	k1gFnSc1	litosféra
Io	Io	k1gFnPc2	Io
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bazaltů	bazalt	k1gInPc2	bazalt
a	a	k8xC	a
sírových	sírový	k2eAgFnPc2d1	sírová
uloženin	uloženina	k1gFnPc2	uloženina
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
silnou	silný	k2eAgFnSc7d1	silná
a	a	k8xC	a
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
je	být	k5eAaImIp3nS	být
litosféra	litosféra	k1gFnSc1	litosféra
až	až	k9	až
12	[number]	k4	12
km	km	kA	km
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
sahat	sahat	k5eAaImF	sahat
až	až	k9	až
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
40	[number]	k4	40
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slapové	slapový	k2eAgInPc4d1	slapový
jevy	jev	k1gInPc4	jev
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
získává	získávat	k5eAaImIp3nS	získávat
Io	Io	k1gFnSc1	Io
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
tepla	tepnout	k5eAaPmAgFnS	tepnout
spíše	spíše	k9	spíše
působením	působení	k1gNnSc7	působení
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vnitřek	vnitřek	k1gInSc4	vnitřek
nežli	nežli	k8xS	nežli
z	z	k7c2	z
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgInPc1d1	slapový
jevy	jev	k1gInPc1	jev
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
rezonancí	rezonance	k1gFnPc2	rezonance
s	s	k7c7	s
měsíci	měsíc	k1gInPc7	měsíc
Europou	Europa	k1gFnSc7	Europa
a	a	k8xC	a
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
takto	takto	k6eAd1	takto
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
tepla	teplo	k1gNnSc2	teplo
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
na	na	k7c6	na
rotační	rotační	k2eAgFnSc6d1	rotační
ose	osa	k1gFnSc6	osa
i	i	k8xC	i
na	na	k7c6	na
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
složení	složení	k1gNnSc6	složení
a	a	k8xC	a
stavu	stav	k1gInSc6	stav
materiálu	materiál	k1gInSc2	materiál
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
laplaceovská	laplaceovský	k2eAgFnSc1d1	laplaceovský
rezonance	rezonance	k1gFnSc1	rezonance
s	s	k7c7	s
Europou	Europa	k1gFnSc7	Europa
a	a	k8xC	a
Ganymedem	Ganymed	k1gMnSc7	Ganymed
udržuje	udržovat	k5eAaImIp3nS	udržovat
excentricitu	excentricita	k1gFnSc4	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Io	Io	k1gFnSc2	Io
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
slapovému	slapový	k2eAgNnSc3d1	slapové
tření	tření	k1gNnSc3	tření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gFnSc4	její
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
<g/>
.	.	kIx.	.
</s>
<s>
Rezonantní	rezonantní	k2eAgFnSc1d1	rezonantní
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
také	také	k9	také
udržuje	udržovat	k5eAaImIp3nS	udržovat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Io	Io	k1gFnSc2	Io
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
;	;	kIx,	;
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
slapy	slap	k1gInPc1	slap
generované	generovaný	k2eAgInPc1d1	generovaný
Jupiterem	Jupiter	k1gInSc7	Jupiter
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc1	měsíc
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
mateřské	mateřský	k2eAgFnSc2d1	mateřská
planety	planeta	k1gFnSc2	planeta
pomalu	pomalu	k6eAd1	pomalu
vzdaloval	vzdalovat	k5eAaImAgMnS	vzdalovat
<g/>
.	.	kIx.	.
</s>
<s>
Vertikální	vertikální	k2eAgFnSc1d1	vertikální
změna	změna	k1gFnSc1	změna
slapové	slapový	k2eAgFnSc2d1	slapová
výdutě	výduť	k1gFnSc2	výduť
Io	Io	k1gFnSc2	Io
mezi	mezi	k7c4	mezi
okamžiky	okamžik	k1gInPc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
měsíc	měsíc	k1gInSc1	měsíc
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
apocentru	apocentrum	k1gNnSc6	apocentrum
a	a	k8xC	a
pericentru	pericentrum	k1gNnSc6	pericentrum
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgNnSc1d1	slapové
tření	tření	k1gNnSc1	tření
vznikající	vznikající	k2eAgNnSc1d1	vznikající
uvnitř	uvnitř	k7c2	uvnitř
Io	Io	k1gFnSc2	Io
díky	díky	k7c3	díky
proměnné	proměnný	k2eAgFnSc3d1	proměnná
slapové	slapový	k2eAgFnSc3d1	slapová
síle	síla	k1gFnSc3	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
významné	významný	k2eAgNnSc4d1	významné
slapové	slapový	k2eAgNnSc4d1	slapové
zahřívání	zahřívání	k1gNnSc4	zahřívání
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tavení	tavení	k1gNnSc3	tavení
významné	významný	k2eAgFnSc2d1	významná
části	část	k1gFnSc2	část
měsíčního	měsíční	k2eAgInSc2d1	měsíční
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
takto	takto	k6eAd1	takto
produkované	produkovaný	k2eAgNnSc1d1	produkované
je	být	k5eAaImIp3nS	být
až	až	k9	až
200	[number]	k4	200
<g/>
krát	krát	k6eAd1	krát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
energie	energie	k1gFnSc1	energie
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
uvolňována	uvolňovat	k5eAaImNgFnS	uvolňovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
pozorovanému	pozorovaný	k2eAgInSc3d1	pozorovaný
vysokému	vysoký	k2eAgInSc3d1	vysoký
tepelnému	tepelný	k2eAgInSc3d1	tepelný
toku	tok	k1gInSc3	tok
(	(	kIx(	(
<g/>
globální	globální	k2eAgInSc1d1	globální
úhrn	úhrn	k1gInSc1	úhrn
<g/>
:	:	kIx,	:
0,6	[number]	k4	0,6
až	až	k9	až
1,6	[number]	k4	1,6
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objem	objem	k1gInSc1	objem
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
nitra	nitro	k1gNnSc2	nitro
Io	Io	k1gFnSc1	Io
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
časem	čas	k1gInSc7	čas
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
není	být	k5eNaImIp3nS	být
reprezentativním	reprezentativní	k2eAgInSc7d1	reprezentativní
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
průměrem	průměr	k1gInSc7	průměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalostí	znalost	k1gFnPc2	znalost
povrchu	povrch	k1gInSc2	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Merkuru	Merkur	k1gInSc2	Merkur
vědci	vědec	k1gMnPc1	vědec
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
povrch	povrch	k1gInSc1	povrch
Io	Io	k1gMnSc2	Io
bude	být	k5eAaImBp3nS	být
silně	silně	k6eAd1	silně
rozrušen	rozrušit	k5eAaPmNgInS	rozrušit
množstvím	množství	k1gNnSc7	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budou	být	k5eAaImBp3nP	být
na	na	k7c6	na
prvních	první	k4xOgInPc6	první
snímcích	snímek	k1gInPc6	snímek
povrchu	povrch	k1gInSc2	povrch
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
jasně	jasně	k6eAd1	jasně
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
kráterů	kráter	k1gInPc2	kráter
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
stáří	stáří	k1gNnSc2	stáří
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vědce	vědec	k1gMnPc4	vědec
překvapil	překvapit	k5eAaPmAgInS	překvapit
povrch	povrch	k1gInSc1	povrch
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
hladkými	hladký	k2eAgFnPc7d1	hladká
planinami	planina	k1gFnPc7	planina
narušovanými	narušovaný	k2eAgFnPc7d1	narušovaná
jak	jak	k8xS	jak
horami	hora	k1gFnPc7	hora
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
lávovými	lávový	k2eAgInPc7d1	lávový
výlevy	výlev	k1gInPc7	výlev
a	a	k8xC	a
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
různě	různě	k6eAd1	různě
barevný	barevný	k2eAgInSc1d1	barevný
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
materiálu	materiál	k1gInSc6	materiál
obsahujícím	obsahující	k2eAgInSc6d1	obsahující
síru	síra	k1gFnSc4	síra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
které	který	k3yRgFnSc6	který
oblasti	oblast	k1gFnSc6	oblast
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
přirovnávání	přirovnávání	k1gNnSc3	přirovnávání
měsíce	měsíc	k1gInSc2	měsíc
k	k	k7c3	k
pomeranči	pomeranč	k1gInSc3	pomeranč
nebo	nebo	k8xC	nebo
pizze	pizza	k1gFnSc3	pizza
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
větších	veliký	k2eAgInPc2d2	veliký
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
velice	velice	k6eAd1	velice
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
neustálým	neustálý	k2eAgNnSc7d1	neustálé
ukládáním	ukládání	k1gNnSc7	ukládání
sopečného	sopečný	k2eAgInSc2d1	sopečný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
pohřbíváním	pohřbívání	k1gNnSc7	pohřbívání
starších	starý	k2eAgInPc2d2	starší
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
závěry	závěr	k1gInPc1	závěr
následně	následně	k6eAd1	následně
podpořily	podpořit	k5eAaPmAgInP	podpořit
snímky	snímek	k1gInPc4	snímek
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
ukazující	ukazující	k2eAgFnSc4d1	ukazující
devět	devět	k4xCc4	devět
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
<g/>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
vlivem	vlivem	k7c2	vlivem
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
mají	mít	k5eAaImIp3nP	mít
teplotu	teplota	k1gFnSc4	teplota
okolo	okolo	k7c2	okolo
17	[number]	k4	17
°	°	k?	°
<g/>
C.	C.	kA	C.
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
lávovými	lávový	k2eAgNnPc7d1	lávové
jezery	jezero	k1gNnPc7	jezero
<g/>
,	,	kIx,	,
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
částečně	částečně	k6eAd1	částečně
ztuhlými	ztuhlý	k2eAgFnPc7d1	ztuhlá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Složení	složení	k1gNnSc1	složení
povrchu	povrch	k1gInSc2	povrch
====	====	k?	====
</s>
</p>
<p>
<s>
Odlišně	odlišně	k6eAd1	odlišně
barevné	barevný	k2eAgFnPc4d1	barevná
oblasti	oblast	k1gFnPc4	oblast
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Io	Io	k1gFnSc2	Io
nejspíše	nejspíše	k9	nejspíše
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
různému	různý	k2eAgInSc3d1	různý
materiálu	materiál	k1gInSc3	materiál
vyvrhovanému	vyvrhovaný	k2eAgInSc3d1	vyvrhovaný
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
při	při	k7c6	při
sopečné	sopečný	k2eAgFnSc6d1	sopečná
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
silikáty	silikát	k1gInPc4	silikát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ortopyroxen	ortopyroxen	k1gInSc1	ortopyroxen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
až	až	k8xS	až
šedivými	šedivý	k2eAgInPc7d1	šedivý
povlaky	povlak	k1gInPc7	povlak
<g/>
.	.	kIx.	.
</s>
<s>
Depozity	depozit	k1gInPc1	depozit
síry	síra	k1gFnSc2	síra
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
jeví	jevit	k5eAaImIp3nP	jevit
žluté	žlutý	k2eAgFnPc1d1	žlutá
až	až	k6eAd1	až
žluto-zelené	žlutoelený	k2eAgFnPc1d1	žluto-zelená
<g/>
.	.	kIx.	.
</s>
<s>
Uloženiny	uloženina	k1gFnPc1	uloženina
síry	síra	k1gFnSc2	síra
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
šířkách	šířka	k1gFnPc6	šířka
a	a	k8xC	a
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
radiaci	radiace	k1gFnSc3	radiace
působící	působící	k2eAgMnSc1d1	působící
na	na	k7c4	na
stabilitu	stabilita	k1gFnSc4	stabilita
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
změnou	změna	k1gFnSc7	změna
počtu	počet	k1gInSc2	počet
vazeb	vazba	k1gFnPc2	vazba
na	na	k7c4	na
8	[number]	k4	8
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
červeno-hnědou	červenonědý	k2eAgFnSc4d1	červeno-hnědá
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
<g/>
Explozivní	explozivní	k2eAgFnSc1d1	explozivní
erupce	erupce	k1gFnSc1	erupce
sopek	sopka	k1gFnPc2	sopka
často	často	k6eAd1	často
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mračna	mračno	k1gNnPc4	mračno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
deštníku	deštník	k1gInSc2	deštník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
dopadá	dopadat	k5eAaImIp3nS	dopadat
materiál	materiál	k1gInSc1	materiál
tvořený	tvořený	k2eAgInSc1d1	tvořený
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
silikáty	silikát	k1gInPc7	silikát
<g/>
;	;	kIx,	;
vlivem	vliv	k1gInSc7	vliv
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
mračnu	mračno	k1gNnSc6	mračno
často	často	k6eAd1	často
načervenalý	načervenalý	k2eAgMnSc1d1	načervenalý
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
mračna	mračno	k1gNnPc1	mračno
vznikají	vznikat	k5eAaImIp3nP	vznikat
nad	nad	k7c7	nad
trhlinami	trhlina	k1gFnPc7	trhlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vylévá	vylévat	k5eAaImIp3nS	vylévat
láva	láva	k1gFnSc1	láva
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
zastoupením	zastoupení	k1gNnSc7	zastoupení
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
právě	právě	k9	právě
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
červených	červený	k2eAgFnPc2d1	červená
usazenin	usazenina	k1gFnPc2	usazenina
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
okolí	okolí	k1gNnSc4	okolí
sopky	sopka	k1gFnSc2	sopka
Pele	pel	k1gInSc5	pel
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgFnPc2d1	tvořená
nejspíše	nejspíše	k9	nejspíše
sírou	síra	k1gFnSc7	síra
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
molekulách	molekula	k1gFnPc6	molekula
o	o	k7c4	o
3	[number]	k4	3
nebo	nebo	k8xC	nebo
4	[number]	k4	4
řetězcích	řetězec	k1gInPc6	řetězec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
žhavá	žhavý	k2eAgFnSc1d1	žhavá
láva	láva	k1gFnSc1	láva
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
existují	existovat	k5eAaImIp3nP	existovat
ložiska	ložisko	k1gNnPc1	ložisko
uložené	uložený	k2eAgFnSc2d1	uložená
síry	síra	k1gFnSc2	síra
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
interakci	interakce	k1gFnSc3	interakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
vznikem	vznik	k1gInSc7	vznik
bílých	bílý	k2eAgNnPc2d1	bílé
či	či	k8xC	či
šedých	šedý	k2eAgNnPc2d1	šedé
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mapování	mapování	k1gNnSc1	mapování
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
měsíce	měsíc	k1gInSc2	měsíc
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
v	v	k7c6	v
minimálním	minimální	k2eAgNnSc6d1	minimální
množství	množství	k1gNnSc6	množství
nebo	nebo	k8xC	nebo
tam	tam	k6eAd1	tam
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
nebo	nebo	k8xC	nebo
hydratovaných	hydratovaný	k2eAgInPc2d1	hydratovaný
minerálů	minerál	k1gInPc2	minerál
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severozápadních	severozápadní	k2eAgInPc2d1	severozápadní
svahů	svah	k1gInPc2	svah
Gish	Gish	k1gInSc4	Gish
Bar	bar	k1gInSc1	bar
Mons	Mons	k1gInSc1	Mons
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobena	způsoben	k2eAgFnSc1d1	způsobena
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
panovaly	panovat	k5eAaImAgFnP	panovat
v	v	k7c6	v
Jupiterově	Jupiterův	k2eAgFnSc6d1	Jupiterova
soustavě	soustava	k1gFnSc6	soustava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
:	:	kIx,	:
tehdy	tehdy	k6eAd1	tehdy
zpočátku	zpočátku	k6eAd1	zpočátku
měl	mít	k5eAaImAgMnS	mít
Jupiter	Jupiter	k1gMnSc1	Jupiter
teplotu	teplota	k1gFnSc4	teplota
nejspíše	nejspíše	k9	nejspíše
dostatečně	dostatečně	k6eAd1	dostatečně
vysokou	vysoká	k1gFnSc7	vysoká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
těkavé	těkavý	k2eAgFnPc1d1	těkavá
látky	látka	k1gFnPc1	látka
vypařily	vypařit	k5eAaPmAgFnP	vypařit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
horký	horký	k2eAgInSc1d1	horký
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zapůsobil	zapůsobit	k5eAaPmAgMnS	zapůsobit
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
ledové	ledový	k2eAgInPc4d1	ledový
měsíce	měsíc	k1gInPc4	měsíc
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vulkanismus	vulkanismus	k1gInSc1	vulkanismus
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
průletů	průlet	k1gInPc2	průlet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
pozorován	pozorován	k2eAgInSc4d1	pozorován
aktivní	aktivní	k2eAgInSc4d1	aktivní
vulkanismus	vulkanismus	k1gInSc4	vulkanismus
jako	jako	k8xS	jako
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
tělese	těleso	k1gNnSc6	těleso
mimo	mimo	k7c4	mimo
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
postupnému	postupný	k2eAgNnSc3d1	postupné
průletu	průlet	k1gInSc2	průlet
obou	dva	k4xCgFnPc2	dva
sond	sonda	k1gFnPc2	sonda
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
pozorován	pozorován	k2eAgMnSc1d1	pozorován
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
odhalit	odhalit	k5eAaPmF	odhalit
devět	devět	k4xCc1	devět
sopek	sopka	k1gFnPc2	sopka
během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
a	a	k8xC	a
srovnáním	srovnání	k1gNnSc7	srovnání
snímků	snímek	k1gInPc2	snímek
povrchu	povrch	k1gInSc2	povrch
pak	pak	k6eAd1	pak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
objevit	objevit	k5eAaPmF	objevit
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc4d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teplo	teplo	k6eAd1	teplo
generované	generovaný	k2eAgInPc4d1	generovaný
slapy	slap	k1gInPc4	slap
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Io	Io	k1gFnPc7	Io
vulkanicky	vulkanicky	k6eAd1	vulkanicky
nejaktivnějším	aktivní	k2eAgNnSc7d3	nejaktivnější
tělesem	těleso	k1gNnSc7	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
sopečných	sopečný	k2eAgFnPc2d1	sopečná
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
lávovými	lávový	k2eAgInPc7d1	lávový
výlevy	výlev	k1gInPc7	výlev
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
silných	silný	k2eAgFnPc2d1	silná
erupcí	erupce	k1gFnPc2	erupce
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
proudy	proud	k1gInPc1	proud
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
tvořené	tvořený	k2eAgNnSc1d1	tvořené
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
bazaltických	bazaltický	k2eAgFnPc2d1	bazaltický
láv	láva	k1gFnPc2	láva
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
hořčík	hořčík	k1gInSc4	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
erupcí	erupce	k1gFnPc2	erupce
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
vyvrhováno	vyvrhován	k2eAgNnSc1d1	vyvrhováno
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
výšky	výška	k1gFnSc2	výška
až	až	k9	až
500	[number]	k4	500
km	km	kA	km
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
pozvolna	pozvolna	k6eAd1	pozvolna
přitahován	přitahován	k2eAgInSc1d1	přitahován
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
či	či	k8xC	či
může	moct	k5eAaImIp3nS	moct
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
volného	volný	k2eAgInSc2d1	volný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
úlomky	úlomek	k1gInPc1	úlomek
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
rychlosti	rychlost	k1gFnPc1	rychlost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
<g/>
Povrch	povrch	k7c2wR	povrch
Io	Io	k1gFnSc2	Io
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
deprese	deprese	k1gFnPc1	deprese
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
paterae	paterae	k6eAd1	paterae
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
paterae	patera	k1gFnPc1	patera
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
rovným	rovný	k2eAgInSc7d1	rovný
povrchem	povrch	k1gInSc7	povrch
ohraničeným	ohraničený	k2eAgInSc7d1	ohraničený
strmými	strmý	k2eAgInPc7d1	strmý
svahy	svah	k1gInPc7	svah
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
pozemské	pozemský	k2eAgFnSc3d1	pozemská
analogii	analogie	k1gFnSc3	analogie
kalderám	kaldera	k1gFnPc3	kaldera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
vyprázdněním	vyprázdnění	k1gNnSc7	vyprázdnění
magmatického	magmatický	k2eAgInSc2d1	magmatický
krbu	krb	k1gInSc2	krb
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
zřícení	zřícení	k1gNnSc3	zřícení
stropu	strop	k1gInSc2	strop
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
kalderám	kaldera	k1gFnPc3	kaldera
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
či	či	k8xC	či
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
Io	Io	k1gFnSc6	Io
tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
situovány	situován	k2eAgInPc1d1	situován
na	na	k7c4	na
vrcholek	vrcholek	k1gInSc4	vrcholek
štítových	štítový	k2eAgFnPc2d1	štítová
sopek	sopka	k1gFnPc2	sopka
a	a	k8xC	a
oproti	oproti	k7c3	oproti
těmto	tento	k3xDgInPc3	tento
příkladům	příklad	k1gInPc3	příklad
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
průměrem	průměr	k1gInSc7	průměr
okolo	okolo	k7c2	okolo
41	[number]	k4	41
km	km	kA	km
(	(	kIx(	(
<g/>
největší	veliký	k2eAgMnSc1d3	veliký
známý	známý	k1gMnSc1	známý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Loki	Lok	k1gFnSc2	Lok
Patera	Patera	k1gMnSc1	Patera
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
202	[number]	k4	202
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
morfologie	morfologie	k1gFnSc2	morfologie
a	a	k8xC	a
rozmístění	rozmístění	k1gNnSc2	rozmístění
napovídají	napovídat	k5eAaBmIp3nP	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
útvarů	útvar	k1gInPc2	útvar
má	mít	k5eAaImIp3nS	mít
spojitost	spojitost	k1gFnSc1	spojitost
se	s	k7c7	s
strukturními	strukturní	k2eAgFnPc7d1	strukturní
deformacemi	deformace	k1gFnPc7	deformace
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
obklopena	obklopen	k2eAgFnSc1d1	obklopena
zlomy	zlom	k1gInPc7	zlom
či	či	k8xC	či
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zhusta	zhusta	k6eAd1	zhusta
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
sopečných	sopečný	k2eAgFnPc2d1	sopečná
erupcí	erupce	k1gFnPc2	erupce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
se	se	k3xPyFc4	se
často	často	k6eAd1	často
rozlijí	rozlít	k5eAaPmIp3nP	rozlít
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
rovném	rovný	k2eAgInSc6d1	rovný
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
roku	rok	k1gInSc3	rok
2001	[number]	k4	2001
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Gish	Gisha	k1gFnPc2	Gisha
Bar	bar	k1gInSc1	bar
Patera	Patera	k1gMnSc1	Patera
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc7	jejich
vyplněním	vyplnění	k1gNnSc7	vyplnění
lávou	láva	k1gFnSc7	láva
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
lávového	lávový	k2eAgNnSc2d1	lávové
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
<g/>
Lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
představují	představovat	k5eAaImIp3nP	představovat
další	další	k2eAgInSc4d1	další
výrazný	výrazný	k2eAgInSc4d1	výrazný
morfologický	morfologický	k2eAgInSc4d1	morfologický
činitel	činitel	k1gInSc4	činitel
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
skrze	skrze	k?	skrze
praskliny	prasklina	k1gFnSc2	prasklina
či	či	k8xC	či
zlomy	zlom	k1gInPc4	zlom
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
rozlévat	rozlévat	k5eAaImF	rozlévat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pozemské	pozemský	k2eAgFnSc2d1	pozemská
sopky	sopka	k1gFnSc2	sopka
Kilauea	Kilaue	k1gInSc2	Kilaue
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgInPc2d1	hlavní
lávových	lávový	k2eAgInPc2d1	lávový
výlevů	výlev	k1gInPc2	výlev
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sopky	sopka	k1gFnSc2	sopka
Prometheus	Prometheus	k1gMnSc1	Prometheus
a	a	k8xC	a
Amirani	Amiran	k1gMnPc1	Amiran
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
překrýváním	překrývání	k1gNnSc7	překrývání
starších	starý	k2eAgInPc2d2	starší
větších	veliký	k2eAgInPc2d2	veliký
lávových	lávový	k2eAgInPc2d1	lávový
výlevů	výlev	k1gInPc2	výlev
menšími	malý	k2eAgInPc7d2	menší
a	a	k8xC	a
mladšími	mladý	k2eAgInPc7d2	mladší
výlevy	výlev	k1gInPc7	výlev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
průlety	průlet	k1gInPc4	průlet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
a	a	k8xC	a
Galileo	Galilea	k1gFnSc5	Galilea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
nedaleko	nedaleko	k7c2	nedaleko
sopky	sopka	k1gFnSc2	sopka
Prometheus	Prometheus	k1gMnSc1	Prometheus
pozorován	pozorovat	k5eAaImNgInS	pozorovat
lávový	lávový	k2eAgInSc1d1	lávový
výlev	výlev	k1gInSc1	výlev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
75	[number]	k4	75
km	km	kA	km
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
na	na	k7c4	na
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
erupce	erupce	k1gFnSc1	erupce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
lávou	láva	k1gFnSc7	láva
přes	přes	k7c4	přes
3500	[number]	k4	3500
km	km	kA	km
<g/>
2	[number]	k4	2
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Pillan	Pillan	k1gMnSc1	Pillan
Patera	Patera	k1gMnSc1	Patera
<g/>
.	.	kIx.	.
<g/>
Analýza	analýza	k1gFnSc1	analýza
snímků	snímek	k1gInPc2	snímek
ze	z	k7c2	z
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
přivedla	přivést	k5eAaPmAgFnS	přivést
vědce	vědec	k1gMnSc4	vědec
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
výlevy	výlev	k1gInPc1	výlev
tvoří	tvořit	k5eAaImIp3nP	tvořit
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
roztavené	roztavený	k2eAgFnSc2d1	roztavená
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pozdější	pozdní	k2eAgNnSc1d2	pozdější
pozemní	pozemní	k2eAgNnSc1d1	pozemní
pozorování	pozorování	k1gNnSc1	pozorování
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
infra-záření	infraáření	k1gNnSc2	infra-záření
a	a	k8xC	a
měření	měření	k1gNnSc2	měření
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
naznačily	naznačit	k5eAaPmAgInP	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
lávové	lávový	k2eAgInPc4d1	lávový
výlevy	výlev	k1gInPc4	výlev
z	z	k7c2	z
bazaltů	bazalt	k1gInPc2	bazalt
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
zastoupením	zastoupení	k1gNnSc7	zastoupení
mafických	mafický	k2eAgFnPc2d1	mafický
a	a	k8xC	a
ultramafických	ultramafický	k2eAgFnPc2d1	ultramafický
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
teploty	teplota	k1gFnSc2	teplota
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
horkých	horký	k2eAgFnPc2d1	horká
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
"	"	kIx"	"
či	či	k8xC	či
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
emisí	emise	k1gFnSc7	emise
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c4	mezi
1200	[number]	k4	1200
K	k	k7c3	k
a	a	k8xC	a
1600	[number]	k4	1600
K.	K.	kA	K.
První	první	k4xOgInPc1	první
výsledky	výsledek	k1gInPc1	výsledek
modelace	modelace	k1gFnSc2	modelace
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
teploty	teplota	k1gFnPc1	teplota
až	až	k6eAd1	až
okolo	okolo	k7c2	okolo
2000	[number]	k4	2000
K	K	kA	K
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
lepšího	dobrý	k2eAgInSc2d2	lepší
termálního	termální	k2eAgInSc2d1	termální
modelu	model	k1gInSc2	model
pro	pro	k7c4	pro
teplotu	teplota	k1gFnSc4	teplota
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
upraveny	upravit	k5eAaPmNgFnP	upravit
<g/>
.	.	kIx.	.
<g/>
Objevení	objevení	k1gNnSc1	objevení
mračen	mračno	k1gNnPc2	mračno
nad	nad	k7c7	nad
sopkami	sopka	k1gFnPc7	sopka
Pele	pel	k1gInSc5	pel
a	a	k8xC	a
Loki	Lok	k1gInSc3	Lok
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgInSc7	první
důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Mračna	mračna	k1gFnSc1	mračna
vyvrhovaného	vyvrhovaný	k2eAgInSc2d1	vyvrhovaný
materiálu	materiál	k1gInSc2	materiál
můžou	můžou	k?	můžou
obsahovat	obsahovat	k5eAaImF	obsahovat
sodík	sodík	k1gInSc1	sodík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
a	a	k8xC	a
chlor	chlor	k1gInSc1	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Mračna	mračno	k1gNnPc1	mračno
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
typech	typ	k1gInPc6	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
tvoří	tvořit	k5eAaImIp3nS	tvořit
plynná	plynný	k2eAgFnSc1d1	plynná
forma	forma	k1gFnSc1	forma
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uniká	unikat	k5eAaImIp3nS	unikat
z	z	k7c2	z
vyvrhovaného	vyvrhovaný	k2eAgNnSc2d1	vyvrhované
magmatu	magma	k1gNnSc2	magma
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
odplynění	odplynění	k1gNnSc1	odplynění
magmatu	magma	k1gNnSc2	magma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mračna	mračno	k1gNnSc2	mračno
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
usazeniny	usazenina	k1gFnSc2	usazenina
červené	červená	k1gFnSc2	červená
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgFnSc4d1	obsahující
síru	síra	k1gFnSc4	síra
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
černé	černý	k2eAgInPc1d1	černý
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInPc1d1	obsahující
silikáty	silikát	k1gInPc1	silikát
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
1000	[number]	k4	1000
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sopky	sopka	k1gFnSc2	sopka
Pele	pel	k1gInSc5	pel
<g/>
,	,	kIx,	,
Tvashtar	Tvashtar	k1gMnSc1	Tvashtar
nebo	nebo	k8xC	nebo
Dazhbog	Dazhbog	k1gMnSc1	Dazhbog
Patera	Patera	k1gMnSc1	Patera
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
mračen	mračno	k1gNnPc2	mračno
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
lávy	láva	k1gFnSc2	láva
se	s	k7c7	s
zmrzlým	zmrzlý	k2eAgInSc7d1	zmrzlý
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zahřátí	zahřátí	k1gNnSc3	zahřátí
a	a	k8xC	a
vypařování	vypařování	k1gNnSc3	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
světlá	světlý	k2eAgNnPc4d1	světlé
mračna	mračno	k1gNnPc4	mračno
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnSc2	výška
maximálně	maximálně	k6eAd1	maximálně
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sopek	sopka	k1gFnPc2	sopka
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
,	,	kIx,	,
Amirani	Amiran	k1gMnPc1	Amiran
a	a	k8xC	a
Masubi	Masub	k1gMnPc1	Masub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
pohoří	pohoří	k1gNnSc2	pohoří
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Io	Io	k1gFnSc6	Io
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
100	[number]	k4	100
až	až	k9	až
150	[number]	k4	150
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
6	[number]	k4	6
km	km	kA	km
vysoké	vysoký	k2eAgFnSc2d1	vysoká
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
oblasti	oblast	k1gFnSc6	oblast
Boösaule	Boösaule	k1gFnSc2	Boösaule
Montes	Montes	k1gInSc1	Montes
17,5	[number]	k4	17,5
±	±	k?	±
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnPc1	hora
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jako	jako	k9	jako
obrovská	obrovský	k2eAgNnPc4d1	obrovské
izolovaná	izolovaný	k2eAgNnPc4d1	izolované
tělesa	těleso	k1gNnPc4	těleso
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jsou	být	k5eAaImIp3nP	být
157	[number]	k4	157
km	km	kA	km
široké	široký	k2eAgFnSc2d1	široká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nespojuje	spojovat	k5eNaImIp3nS	spojovat
očividná	očividný	k2eAgFnSc1d1	očividná
globální	globální	k2eAgFnSc1d1	globální
tektonická	tektonický	k2eAgFnSc1d1	tektonická
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gInSc2	jejich
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
složení	složení	k1gNnSc1	složení
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vyšší	vysoký	k2eAgInSc4d2	vyšší
obsah	obsah	k1gInSc4	obsah
silikátových	silikátový	k2eAgFnPc2d1	silikátová
hornin	hornina	k1gFnPc2	hornina
než	než	k8xS	než
sloučenin	sloučenina	k1gFnPc2	sloučenina
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
<g/>
Přes	přes	k7c4	přes
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
projevy	projev	k1gInPc4	projev
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měsíci	měsíc	k1gInSc6	měsíc
Io	Io	k1gMnPc1	Io
dodávají	dodávat	k5eAaImIp3nP	dodávat
jeho	jeho	k3xOp3gInSc4	jeho
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
hory	hora	k1gFnPc1	hora
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
původu	původ	k1gInSc2	původ
tektonického	tektonický	k2eAgInSc2d1	tektonický
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
sopečného	sopečný	k2eAgInSc2d1	sopečný
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
hor	hora	k1gFnPc2	hora
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stlačování	stlačování	k1gNnSc2	stlačování
podloží	podložit	k5eAaPmIp3nP	podložit
litosféry	litosféra	k1gFnPc1	litosféra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
často	často	k6eAd1	často
vyústí	vyústit	k5eAaPmIp3nP	vyústit
ve	v	k7c4	v
zdvih	zdvih	k1gInSc4	zdvih
<g/>
,	,	kIx,	,
náklon	náklon	k1gInSc4	náklon
části	část	k1gFnSc2	část
kůry	kůra	k1gFnSc2	kůra
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
střih	střih	k1gInSc4	střih
<g/>
.	.	kIx.	.
<g/>
Časem	časem	k6eAd1	časem
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
subsidenci	subsidence	k1gFnSc3	subsidence
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
neustále	neustále	k6eAd1	neustále
pohřbívá	pohřbívat	k5eAaImIp3nS	pohřbívat
další	další	k2eAgInSc4d1	další
sopečný	sopečný	k2eAgInSc4d1	sopečný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgNnSc1d1	globální
rozložení	rozložení	k1gNnSc1	rozložení
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
opozici	opozice	k1gFnSc6	opozice
k	k	k7c3	k
sopečným	sopečný	k2eAgNnPc3d1	sopečné
tělesům	těleso	k1gNnPc3	těleso
<g/>
;	;	kIx,	;
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
sopek	sopka	k1gFnPc2	sopka
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgFnPc1d1	velká
oblasti	oblast	k1gFnPc1	oblast
litosféry	litosféra	k1gFnSc2	litosféra
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
komprese	komprese	k1gFnSc2	komprese
(	(	kIx(	(
<g/>
hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
extenze	extenze	k1gFnSc2	extenze
(	(	kIx(	(
<g/>
sopky	sopka	k1gFnSc2	sopka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
obě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
části	část	k1gFnPc1	část
vzájemně	vzájemně	k6eAd1	vzájemně
překryté	překrytý	k2eAgFnPc1d1	překrytá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
magma	magma	k1gNnSc1	magma
často	často	k6eAd1	často
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
putuje	putovat	k5eAaImIp3nS	putovat
i	i	k9	i
různými	různý	k2eAgInPc7d1	různý
zlomy	zlom	k1gInPc7	zlom
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
<g/>
Hory	hora	k1gFnSc2	hora
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Io	Io	k1gFnSc2	Io
mají	mít	k5eAaImIp3nP	mít
rozmanitý	rozmanitý	k2eAgInSc4d1	rozmanitý
morfologický	morfologický	k2eAgInSc4d1	morfologický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgInSc4d3	nejčastější
vrcholek	vrcholek	k1gInSc4	vrcholek
tvoří	tvořit	k5eAaImIp3nS	tvořit
plató	plató	k1gNnSc1	plató
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
tělesa	těleso	k1gNnPc4	těleso
s	s	k7c7	s
plošinou	plošina	k1gFnSc7	plošina
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mesy	mesa	k1gFnPc4	mesa
s	s	k7c7	s
drsným	drsný	k2eAgInSc7d1	drsný
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
hory	hora	k1gFnPc1	hora
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
ukloněné	ukloněný	k2eAgInPc4d1	ukloněný
bloky	blok	k1gInPc4	blok
kůry	kůra	k1gFnSc2	kůra
s	s	k7c7	s
málo	málo	k6eAd1	málo
příkrými	příkrý	k2eAgInPc7d1	příkrý
svahy	svah	k1gInPc7	svah
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
vytlačením	vytlačení	k1gNnPc3	vytlačení
z	z	k7c2	z
původně	původně	k6eAd1	původně
rovného	rovný	k2eAgInSc2d1	rovný
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
stran	strana	k1gFnPc2	strana
ostré	ostrý	k2eAgInPc1d1	ostrý
svahy	svah	k1gInPc1	svah
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
o	o	k7c4	o
nízké	nízký	k2eAgFnPc4d1	nízká
štítové	štítový	k2eAgFnPc4d1	štítová
sopky	sopka	k1gFnPc4	sopka
se	s	k7c7	s
sklony	sklon	k1gInPc7	sklon
mezi	mezi	k7c4	mezi
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
<g/>
°	°	k?	°
a	a	k8xC	a
malou	malý	k2eAgFnSc7d1	malá
depresí	deprese	k1gFnSc7	deprese
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
centrální	centrální	k2eAgFnSc7d1	centrální
kalderou	kaldera	k1gFnSc7	kaldera
<g/>
.	.	kIx.	.
</s>
<s>
Vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
hory	hora	k1gFnPc1	hora
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnSc2	výška
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
km	km	kA	km
a	a	k8xC	a
šířky	šířka	k1gFnSc2	šířka
od	od	k7c2	od
40	[number]	k4	40
do	do	k7c2	do
60	[number]	k4	60
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
štítové	štítový	k2eAgFnPc1d1	štítová
sopky	sopka	k1gFnPc1	sopka
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
mírnějšími	mírný	k2eAgInPc7d2	mírnější
svahy	svah	k1gInPc7	svah
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ra	ra	k0	ra
Patera	Patera	k1gMnSc1	Patera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
změření	změření	k1gNnSc1	změření
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
<g/>
Všechny	všechen	k3xTgFnPc4	všechen
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
hory	hora	k1gFnPc4	hora
zřejmě	zřejmě	k6eAd1	zřejmě
jeví	jevit	k5eAaImIp3nS	jevit
známky	známka	k1gFnPc4	známka
silné	silný	k2eAgFnSc2d1	silná
eroze	eroze	k1gFnSc2	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
úbočích	úbočí	k1gNnPc6	úbočí
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
sesuvy	sesuv	k1gInPc4	sesuv
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hory	hora	k1gFnSc2	hora
hromadí	hromadit	k5eAaImIp3nP	hromadit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
působí	působit	k5eAaImIp3nS	působit
dominantní	dominantní	k2eAgMnSc1d1	dominantní
erozivní	erozivní	k2eAgMnSc1d1	erozivní
činitel	činitel	k1gMnSc1	činitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
===	===	k?	===
</s>
</p>
<p>
<s>
Io	Io	k?	Io
má	mít	k5eAaImIp3nS	mít
extrémně	extrémně	k6eAd1	extrémně
řídkou	řídký	k2eAgFnSc4d1	řídká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
s	s	k7c7	s
atmosférickým	atmosférický	k2eAgInSc7d1	atmosférický
tlakem	tlak	k1gInSc7	tlak
okolo	okolo	k7c2	okolo
jedné	jeden	k4xCgFnSc2	jeden
miliardtiny	miliardtina	k1gFnSc2	miliardtina
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vyslání	vyslání	k1gNnSc2	vyslání
sondy	sonda	k1gFnSc2	sonda
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pro	pro	k7c4	pro
brzdění	brzdění	k1gNnSc4	brzdění
sondy	sonda	k1gFnSc2	sonda
využít	využít	k5eAaPmF	využít
padák	padák	k1gInSc1	padák
a	a	k8xC	a
sonda	sonda	k1gFnSc1	sonda
nebude	být	k5eNaImBp3nS	být
potřebovat	potřebovat	k5eAaImF	potřebovat
ani	ani	k8xC	ani
tepelný	tepelný	k2eAgInSc4d1	tepelný
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
bude	být	k5eAaImBp3nS	být
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
raketové	raketový	k2eAgInPc4d1	raketový
trysky	trysk	k1gInPc4	trysk
pro	pro	k7c4	pro
měkké	měkký	k2eAgNnSc4d1	měkké
přistání	přistání	k1gNnSc4	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
takto	takto	k6eAd1	takto
slabá	slabý	k2eAgFnSc1d1	slabá
atmosféra	atmosféra	k1gFnSc1	atmosféra
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zabraňovat	zabraňovat	k5eAaImF	zabraňovat
pronikání	pronikání	k1gNnSc4	pronikání
radiačního	radiační	k2eAgNnSc2d1	radiační
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
silně	silně	k6eAd1	silně
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radiace	radiace	k1gFnSc1	radiace
částice	částice	k1gFnSc2	částice
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
neustále	neustále	k6eAd1	neustále
ničí	ničit	k5eAaImIp3nS	ničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
soustavně	soustavně	k6eAd1	soustavně
doplňovány	doplňován	k2eAgFnPc1d1	doplňována
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
nových	nový	k2eAgFnPc2d1	nová
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
oxid	oxid	k1gInSc4	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
taktéž	taktéž	k?	taktéž
sublimací	sublimace	k1gFnSc7	sublimace
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nejhustší	hustý	k2eAgFnSc1d3	nejhustší
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejtepleji	teple	k6eAd3	teple
a	a	k8xC	a
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
největší	veliký	k2eAgFnSc3d3	veliký
sublimaci	sublimace	k1gFnSc3	sublimace
a	a	k8xC	a
úniku	únik	k1gInSc2	únik
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
nehomogenní	homogenní	k2eNgMnSc1d1	nehomogenní
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
blízkosti	blízkost	k1gFnSc6	blízkost
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
na	na	k7c4	na
oslunění	oslunění	k1gNnSc4	oslunění
(	(	kIx(	(
<g/>
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
ledovým	ledový	k2eAgInSc7d1	ledový
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
než	než	k8xS	než
strana	strana	k1gFnSc1	strana
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Snímky	snímek	k1gInPc1	snímek
Io	Io	k1gFnSc2	Io
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
získané	získaný	k2eAgFnSc2d1	získaná
při	při	k7c6	při
zatmění	zatmění	k1gNnSc6	zatmění
měsíce	měsíc	k1gInSc2	měsíc
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
zářící	zářící	k2eAgFnSc4d1	zářící
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
ji	on	k3xPp3gFnSc4	on
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
interakce	interakce	k1gFnPc4	interakce
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Záře	záře	k1gFnSc1	záře
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nad	nad	k7c7	nad
magnetickými	magnetický	k2eAgInPc7d1	magnetický
póly	pól	k1gInPc7	pól
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Io	Io	k1gFnSc2	Io
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Io	Io	k1gFnSc1	Io
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc1d1	vlastní
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
a	a	k8xC	a
elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
střetnou	střetnout	k5eAaPmIp3nP	střetnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
elektronů	elektron	k1gInPc2	elektron
koliduje	kolidovat	k5eAaImIp3nS	kolidovat
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
jasnější	jasný	k2eAgFnSc2d2	jasnější
záře	zář	k1gFnSc2	zář
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
siločáry	siločára	k1gFnPc1	siločára
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
satelitu	satelit	k1gInSc3	satelit
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
poblíž	poblíž	k6eAd1	poblíž
rovníku	rovník	k1gInSc2	rovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
sloupec	sloupec	k1gInSc1	sloupec
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Záře	záře	k1gFnSc1	záře
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
body	bod	k1gInPc7	bod
na	na	k7c6	na
tangentách	tangenta	k1gFnPc6	tangenta
na	na	k7c6	na
Io	Io	k1gFnPc6	Io
jsou	být	k5eAaImIp3nP	být
zaznamenávány	zaznamenávat	k5eAaImNgInP	zaznamenávat
s	s	k7c7	s
měnící	měnící	k2eAgFnSc7d1	měnící
se	se	k3xPyFc4	se
orientací	orientace	k1gFnSc7	orientace
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
nakloněného	nakloněný	k2eAgInSc2d1	nakloněný
magnetického	magnetický	k2eAgInSc2d1	magnetický
dipólu	dipól	k1gInSc2	dipól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
magnetosférou	magnetosféra	k1gFnSc7	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
==	==	k?	==
</s>
</p>
<p>
<s>
Io	Io	k?	Io
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
tvarování	tvarování	k1gNnSc4	tvarování
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
odvádí	odvádět	k5eAaImIp3nS	odvádět
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
prach	prach	k1gInSc4	prach
z	z	k7c2	z
tenké	tenký	k2eAgFnSc2d1	tenká
atmosféry	atmosféra	k1gFnSc2	atmosféra
Io	Io	k1gFnSc7	Io
rychlostí	rychlost	k1gFnSc7	rychlost
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
tuny	tuna	k1gFnPc4	tuna
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
ionizované	ionizovaný	k2eAgFnSc2d1	ionizovaná
a	a	k8xC	a
atomární	atomární	k2eAgFnSc2d1	atomární
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
atomárního	atomární	k2eAgInSc2d1	atomární
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
molekul	molekula	k1gFnPc2	molekula
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
draselnosodíkové	draselnosodíkový	k2eAgFnSc2d1	draselnosodíkový
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
má	mít	k5eAaImIp3nS	mít
přímý	přímý	k2eAgInSc4d1	přímý
původ	původ	k1gInSc4	původ
v	v	k7c6	v
sopečné	sopečný	k2eAgFnSc6d1	sopečná
činnosti	činnost	k1gFnSc6	činnost
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jupiter	Jupiter	k1gMnSc1	Jupiter
ho	on	k3xPp3gNnSc4	on
nezískává	získávat	k5eNaImIp3nS	získávat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
vyvržených	vyvržený	k2eAgFnPc2d1	vyvržená
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Získaný	získaný	k2eAgInSc1d1	získaný
materiál	materiál	k1gInSc1	materiál
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
náboji	náboj	k1gInSc6	náboj
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
prachových	prachový	k2eAgNnPc6d1	prachové
mračnech	mračno	k1gNnPc6	mračno
a	a	k8xC	a
radiačních	radiační	k2eAgInPc6d1	radiační
pásech	pás	k1gInPc6	pás
jupiterovské	jupiterovský	k2eAgFnSc2d1	jupiterovský
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
opustit	opustit	k5eAaPmF	opustit
gravitační	gravitační	k2eAgFnSc4d1	gravitační
oblast	oblast	k1gFnSc4	oblast
ovládanou	ovládaný	k2eAgFnSc4d1	ovládaná
Jupiterem	Jupiter	k1gInSc7	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
šesti	šest	k4xCc2	šest
průměru	průměr	k1gInSc6	průměr
Io	Io	k1gFnPc4	Io
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
nachází	nacházet	k5eAaImIp3nS	nacházet
oblak	oblak	k1gInSc1	oblak
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
vrchní	vrchní	k2eAgFnSc2d1	vrchní
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
excitovány	excitován	k2eAgFnPc1d1	excitován
srážkami	srážka	k1gFnPc7	srážka
s	s	k7c7	s
ionty	ion	k1gInPc7	ion
v	v	k7c6	v
plazmovém	plazmový	k2eAgInSc6d1	plazmový
torusu	torus	k1gInSc6	torus
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
procesy	proces	k1gInPc7	proces
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
Hillovu	Hillův	k2eAgFnSc4d1	Hillova
sféru	sféra	k1gFnSc4	sféra
Ia	ia	k0	ia
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
měsíční	měsíční	k2eAgFnSc1d1	měsíční
gravitace	gravitace	k1gFnSc1	gravitace
dominantní	dominantní	k2eAgFnSc1d1	dominantní
nad	nad	k7c7	nad
Jupiterovou	Jupiterův	k2eAgFnSc7d1	Jupiterova
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
nakonec	nakonec	k6eAd1	nakonec
uniknou	uniknout	k5eAaPmIp3nP	uniknout
z	z	k7c2	z
gravitační	gravitační	k2eAgFnSc2d1	gravitační
studny	studna	k1gFnSc2	studna
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
staženy	stáhnout	k5eAaPmNgInP	stáhnout
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvaceti	dvacet	k4xCc2	dvacet
hodin	hodina	k1gFnPc2	hodina
tyto	tento	k3xDgFnPc1	tento
uniklé	uniklý	k2eAgFnPc1d1	uniklá
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
rozloží	rozložit	k5eAaPmIp3nP	rozložit
mimo	mimo	k7c4	mimo
Io	Io	k1gFnSc4	Io
do	do	k7c2	do
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
mračna	mračno	k1gNnSc2	mračno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
banánu	banán	k1gInSc2	banán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
táhnout	táhnout	k5eAaImF	táhnout
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
šesti	šest	k4xCc2	šest
průměrů	průměr	k1gInPc2	průměr
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
excitují	excitovat	k5eAaBmIp3nP	excitovat
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
také	také	k9	také
občas	občas	k6eAd1	občas
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
sodíkovým	sodíkový	k2eAgInPc3d1	sodíkový
iontům	ion	k1gInPc3	ion
v	v	k7c6	v
plazmovém	plazmový	k2eAgInSc6d1	plazmový
torusu	torus	k1gInSc6	torus
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tyto	tento	k3xDgFnPc1	tento
nové	nový	k2eAgFnPc1d1	nová
"	"	kIx"	"
<g/>
rychlé	rychlý	k2eAgFnPc1d1	rychlá
<g/>
"	"	kIx"	"
neutrální	neutrální	k2eAgFnPc1d1	neutrální
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
rychle	rychle	k6eAd1	rychle
odstraněny	odstranit	k5eAaPmNgInP	odstranit
z	z	k7c2	z
torusu	torus	k1gInSc2	torus
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
udržují	udržovat	k5eAaImIp3nP	udržovat
svou	svůj	k3xOyFgFnSc4	svůj
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
70	[number]	k4	70
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	se	k3xPyFc4	se
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
rychlostí	rychlost	k1gFnSc7	rychlost
u	u	k7c2	u
Io	Io	k1gFnSc2	Io
17	[number]	k4	17
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
tyto	tento	k3xDgInPc4	tento
částice	částice	k1gFnSc1	částice
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
ve	v	k7c6	v
výtryscích	výtrysk	k1gInPc6	výtrysk
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
<g/>
Io	Io	k1gFnSc1	Io
obíhá	obíhat	k5eAaImIp3nS	obíhat
uvnitř	uvnitř	k7c2	uvnitř
pásu	pás	k1gInSc2	pás
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
záření	záření	k1gNnSc2	záření
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
chycených	chycený	k2eAgInPc2d1	chycený
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
Jupitera	Jupiter	k1gMnSc2	Jupiter
známého	známý	k1gMnSc2	známý
jako	jako	k8xC	jako
plazmový	plazmový	k2eAgInSc4d1	plazmový
torus	torus	k1gInSc4	torus
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prstenci	prstenec	k1gInSc6	prstenec
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pneumatiky	pneumatika	k1gFnSc2	pneumatika
obsahující	obsahující	k2eAgFnSc4d1	obsahující
síru	síra	k1gFnSc4	síra
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
a	a	k8xC	a
chlor	chlor	k1gInSc1	chlor
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
ionizaci	ionizace	k1gFnSc6	ionizace
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
oblaku	oblak	k1gInSc6	oblak
obklopujícím	obklopující	k2eAgMnSc7d1	obklopující
Io	Io	k1gMnSc7	Io
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odnášena	odnášet	k5eAaImNgFnS	odnášet
Jupiterovou	Jupiterův	k2eAgFnSc7d1	Jupiterova
magnetosférou	magnetosféra	k1gFnSc7	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
oblaku	oblak	k1gInSc2	oblak
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
rotují	rotovat	k5eAaImIp3nP	rotovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jupiterovou	Jupiterův	k2eAgFnSc7d1	Jupiterova
magnetosférou	magnetosféra	k1gFnSc7	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
obíhají	obíhat	k5eAaImIp3nP	obíhat
rychlostí	rychlost	k1gFnSc7	rychlost
74	[number]	k4	74
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
zbytek	zbytek	k1gInSc4	zbytek
Jupiterova	Jupiterův	k2eAgNnSc2d1	Jupiterovo
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
plazmový	plazmový	k2eAgInSc1d1	plazmový
torus	torus	k1gInSc1	torus
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
oběžné	oběžný	k2eAgFnSc2d1	oběžná
roviny	rovina	k1gFnSc2	rovina
Io	Io	k1gMnSc1	Io
skloněný	skloněný	k2eAgMnSc1d1	skloněný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Io	Io	k1gFnSc1	Io
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
pod	pod	k7c7	pod
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
nad	nad	k7c7	nad
jádrem	jádro	k1gNnSc7	jádro
plazmového	plazmový	k2eAgInSc2d1	plazmový
toru	torus	k1gInSc2	torus
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnPc1d1	vysoká
rychlosti	rychlost	k1gFnPc1	rychlost
a	a	k8xC	a
energie	energie	k1gFnPc1	energie
iontů	ion	k1gInPc2	ion
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
části	část	k1gFnSc2	část
zodpovědné	zodpovědný	k2eAgFnSc2d1	zodpovědná
za	za	k7c4	za
odnášení	odnášení	k1gNnSc4	odnášení
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
a	a	k8xC	a
rozsáhlejšího	rozsáhlý	k2eAgInSc2d2	rozsáhlejší
neutrálního	neutrální	k2eAgInSc2d1	neutrální
oblaku	oblak	k1gInSc2	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Torus	torus	k1gInSc1	torus
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
vnějšího	vnější	k2eAgNnSc2d1	vnější
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
teplého	teplý	k2eAgInSc2d1	teplý
toru	torus	k1gInSc2	torus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
vně	vně	k6eAd1	vně
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
Io	Io	k1gFnSc2	Io
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
svisle	svisle	k6eAd1	svisle
protažená	protažený	k2eAgFnSc1d1	protažená
oblast	oblast	k1gFnSc1	oblast
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ribbon	ribbon	k1gNnSc1	ribbon
<g/>
"	"	kIx"	"
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
neutrální	neutrální	k2eAgFnSc2d1	neutrální
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
chladnějšího	chladný	k2eAgNnSc2d2	chladnější
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Jupitera	Jupiter	k1gMnSc2	Jupiter
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
velikosti	velikost	k1gFnSc2	velikost
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
"	"	kIx"	"
<g/>
chladný	chladný	k2eAgInSc1d1	chladný
torus	torus	k1gInSc1	torus
<g/>
"	"	kIx"	"
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
ve	v	k7c6	v
spirálách	spirála	k1gFnPc6	spirála
pomalu	pomalu	k6eAd1	pomalu
padají	padat	k5eAaImIp3nP	padat
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
asi	asi	k9	asi
40	[number]	k4	40
dnech	den	k1gInPc6	den
strávených	strávený	k2eAgNnPc6d1	strávené
v	v	k7c6	v
toru	torus	k1gInSc6	torus
částice	částice	k1gFnSc2	částice
z	z	k7c2	z
teplého	teplý	k2eAgInSc2d1	teplý
toru	torus	k1gInSc2	torus
unikají	unikat	k5eAaImIp3nP	unikat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
netypicky	typicky	k6eNd1	typicky
velkou	velký	k2eAgFnSc4d1	velká
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
z	z	k7c2	z
Io	Io	k1gFnSc2	Io
detekované	detekovaný	k2eAgFnSc2d1	detekovaná
jako	jako	k8xS	jako
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
magnetosférickém	magnetosférický	k2eAgNnSc6d1	magnetosférický
plazmatu	plazma	k1gNnSc6	plazma
daleko	daleko	k6eAd1	daleko
v	v	k7c6	v
magnetochvostě	magnetochvosta	k1gFnSc6	magnetochvosta
byly	být	k5eAaImAgInP	být
zachyceny	zachytit	k5eAaPmNgInP	zachytit
sondou	sonda	k1gFnSc7	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
spojitost	spojitost	k1gFnSc1	spojitost
s	s	k7c7	s
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
aktivitou	aktivita	k1gFnSc7	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
považují	považovat	k5eAaImIp3nP	považovat
neutrální	neutrální	k2eAgNnPc4d1	neutrální
sodíková	sodíkový	k2eAgNnPc4d1	sodíkové
mračna	mračno	k1gNnPc4	mračno
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
přiblížení	přiblížení	k1gNnSc2	přiblížení
sondy	sonda	k1gFnSc2	sonda
Ulysses	Ulyssesa	k1gFnPc2	Ulyssesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
sonda	sonda	k1gFnSc1	sonda
proudy	proud	k1gInPc1	proud
částic	částice	k1gFnPc2	částice
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
prachu	prach	k1gInSc2	prach
vyvrhovaného	vyvrhovaný	k2eAgInSc2d1	vyvrhovaný
mimo	mimo	k7c4	mimo
soustavu	soustava	k1gFnSc4	soustava
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
proudech	proud	k1gInPc6	proud
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
velikost	velikost	k1gFnSc4	velikost
zrna	zrno	k1gNnSc2	zrno
10	[number]	k4	10
μ	μ	k?	μ
a	a	k8xC	a
blyl	blynout	k5eAaPmAgInS	blynout
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
chloridem	chlorid	k1gInSc7	chlorid
sodným	sodný	k2eAgInSc7d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
měření	měření	k1gNnSc1	měření
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
prachové	prachový	k2eAgInPc1d1	prachový
proudy	proud	k1gInPc1	proud
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
stále	stále	k6eAd1	stále
znám	znám	k2eAgInSc1d1	znám
mechanismus	mechanismus	k1gInSc1	mechanismus
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
<g/>
Pohyb	pohyb	k1gInSc1	pohyb
Io	Io	k1gFnSc2	Io
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
princip	princip	k1gInSc1	princip
generátoru	generátor	k1gInSc2	generátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
napětí	napětí	k1gNnSc4	napětí
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
400	[number]	k4	400
000	[number]	k4	000
V	V	kA	V
napříč	napříč	k7c7	napříč
svým	svůj	k3xOyFgInSc7	svůj
průměrem	průměr	k1gInSc7	průměr
a	a	k8xC	a
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
3	[number]	k4	3
miliónů	milión	k4xCgInPc2	milión
ampér	ampér	k1gInSc4	ampér
proudící	proudící	k2eAgInPc4d1	proudící
do	do	k7c2	do
ionosféry	ionosféra	k1gFnSc2	ionosféra
planety	planeta	k1gFnSc2	planeta
podél	podél	k7c2	podél
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Io	Io	k?	Io
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
421	[number]	k4	421
700	[number]	k4	700
km	km	kA	km
od	od	k7c2	od
středu	střed	k1gInSc2	střed
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
350	[number]	k4	350
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
horních	horní	k2eAgFnPc2d1	horní
vrstev	vrstva	k1gFnPc2	vrstva
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvnitřnější	vnitřní	k2eAgFnPc4d3	nejvnitřnější
z	z	k7c2	z
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
drahami	draha	k1gFnPc7	draha
měsíce	měsíc	k1gInPc4	měsíc
Thebe	Theb	k1gMnSc5	Theb
a	a	k8xC	a
Europe	Europ	k1gMnSc5	Europ
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
malých	malý	k2eAgInPc2d1	malý
(	(	kIx(	(
<g/>
známých	známý	k2eAgInPc2d1	známý
<g/>
)	)	kIx)	)
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
Io	Io	k1gFnSc2	Io
pátým	pátý	k4xOgInSc7	pátý
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rotace	rotace	k1gFnSc1	rotace
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oběžné	oběžný	k2eAgFnSc6d1	oběžná
rezonanci	rezonance	k1gFnSc6	rezonance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
Europou	Europa	k1gFnSc7	Europa
a	a	k8xC	a
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stihne	stihnout	k5eAaPmIp3nS	stihnout
vykonat	vykonat	k5eAaPmF	vykonat
dva	dva	k4xCgInPc4	dva
oběhy	oběh	k1gInPc4	oběh
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
než	než	k8xS	než
Europa	Europa	k1gFnSc1	Europa
jednou	jednou	k6eAd1	jednou
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
oběh	oběh	k1gInSc4	oběh
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
rezonance	rezonance	k1gFnSc1	rezonance
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržet	udržet	k5eAaPmF	udržet
sklon	sklon	k1gInSc4	sklon
oběžné	oběžný	k2eAgFnSc2d1	oběžná
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
0,0041	[number]	k4	0,0041
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
generovat	generovat	k5eAaImF	generovat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
teplo	teplo	k1gNnSc4	teplo
potřebné	potřebný	k2eAgNnSc4d1	potřebné
pro	pro	k7c4	pro
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
činnost	činnost	k1gFnSc4	činnost
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
interakce	interakce	k1gFnSc2	interakce
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
méně	málo	k6eAd2	málo
aktivní	aktivní	k2eAgInSc4d1	aktivní
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
oběhu	oběh	k1gInSc2	oběh
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
přitahován	přitahován	k2eAgMnSc1d1	přitahován
k	k	k7c3	k
Europě	Europa	k1gFnSc3	Europa
a	a	k8xC	a
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vychyluje	vychylovat	k5eAaImIp3nS	vychylovat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
vzdálí	vzdálit	k5eAaPmIp3nS	vzdálit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
přitažen	přitažen	k2eAgInSc1d1	přitažen
Jupiterem	Jupiter	k1gInSc7	Jupiter
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
mimo	mimo	k7c4	mimo
dráhu	dráha	k1gFnSc4	dráha
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
tření	tření	k1gNnSc1	tření
a	a	k8xC	a
vydouvání	vydouvání	k1gNnSc1	vydouvání
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
sta	sto	k4xCgNnPc1	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
synchronicita	synchronicita	k1gFnSc1	synchronicita
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
taktéž	taktéž	k?	taktéž
definici	definice	k1gFnSc3	definice
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
na	na	k7c6	na
Io	Io	k1gFnSc6	Io
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
poledník	poledník	k1gInSc1	poledník
protíná	protínat	k5eAaImIp3nS	protínat
rovník	rovník	k1gInSc4	rovník
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rotace	rotace	k1gFnSc2	rotace
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
oběh	oběh	k1gInSc1	oběh
trvá	trvat	k5eAaImIp3nS	trvat
42,5	[number]	k4	42,5
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
rychlost	rychlost	k1gFnSc1	rychlost
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
jeho	on	k3xPp3gInSc2	on
pohybu	pohyb	k1gInSc2	pohyb
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
či	či	k8xC	či
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
obíhá	obíhat	k5eAaImIp3nS	obíhat
i	i	k9	i
Io	Io	k1gFnSc1	Io
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
tedy	tedy	k9	tedy
Io	Io	k1gFnSc1	Io
má	mít	k5eAaImIp3nS	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
pozičních	poziční	k2eAgFnPc2d1	poziční
koordinát	koordináta	k1gFnPc2	koordináta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
objevu	objev	k1gInSc2	objev
a	a	k8xC	a
pozorování	pozorování	k1gNnPc2	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1610	[number]	k4	1610
Galileem	Galileus	k1gMnSc7	Galileus
během	během	k7c2	během
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
5.0	[number]	k4	5.0
magnitudy	magnituda	k1gFnSc2	magnituda
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
třemi	tři	k4xCgInPc7	tři
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
začaly	začít	k5eAaPmAgFnP	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
původně	původně	k6eAd1	původně
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1610	[number]	k4	1610
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Sidereus	Sidereus	k1gMnSc1	Sidereus
Nuncius	nuncius	k1gMnSc1	nuncius
<g/>
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
publikoval	publikovat	k5eAaBmAgMnS	publikovat
obdobné	obdobný	k2eAgInPc4d1	obdobný
výsledky	výsledek	k1gInPc4	výsledek
svého	svůj	k3xOyFgNnSc2	svůj
pozorování	pozorování	k1gNnSc2	pozorování
roku	rok	k1gInSc2	rok
1614	[number]	k4	1614
v	v	k7c4	v
Mundus	Mundus	k1gInSc4	Mundus
Jovialis	Jovialis	k1gFnSc2	Jovialis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
měsíc	měsíc	k1gInSc1	měsíc
jeden	jeden	k4xCgInSc1	jeden
týden	týden	k1gInSc1	týden
před	před	k7c7	před
Galileem	Galileus	k1gMnSc7	Galileus
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Mauriově	Mauriově	k1gFnSc6	Mauriově
objevu	objev	k1gInSc2	objev
Galileo	Galilea	k1gFnSc5	Galilea
pochyboval	pochybovat	k5eAaImAgMnS	pochybovat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
plagiátorství	plagiátorství	k1gNnSc4	plagiátorství
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dílo	dílo	k1gNnSc1	dílo
Galilea	Galilea	k1gFnSc1	Galilea
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
objevení	objevení	k1gNnSc1	objevení
připisováno	připisovat	k5eAaImNgNnS	připisovat
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
následující	následující	k2eAgFnSc6d1	následující
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
zůstával	zůstávat	k5eAaImAgInS	zůstávat
Io	Io	k1gMnPc4	Io
pro	pro	k7c4	pro
astronomy	astronom	k1gMnPc4	astronom
jen	jen	k9	jen
bodem	bod	k1gInSc7	bod
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
5	[number]	k4	5
<g/>
.	.	kIx.	.
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
námořníci	námořník	k1gMnPc1	námořník
používali	používat	k5eAaImAgMnP	používat
měsíce	měsíc	k1gInPc4	měsíc
objevené	objevený	k2eAgInPc4d1	objevený
Galileem	Galileum	k1gNnSc7	Galileum
pro	pro	k7c4	pro
určování	určování	k1gNnSc4	určování
přesné	přesný	k2eAgFnSc2d1	přesná
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
objev	objev	k1gInSc1	objev
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
podpořil	podpořit	k5eAaPmAgInS	podpořit
obecné	obecný	k2eAgNnSc4d1	obecné
přijetí	přijetí	k1gNnSc4	přijetí
Koperníkova	Koperníkův	k2eAgInSc2d1	Koperníkův
heliocentrického	heliocentrický	k2eAgInSc2d1	heliocentrický
modelu	model	k1gInSc2	model
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vývoji	vývoj	k1gInSc6	vývoj
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
pohybových	pohybový	k2eAgInPc2d1	pohybový
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
první	první	k4xOgNnSc4	první
měření	měření	k1gNnSc4	měření
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
spočíst	spočíst	k5eAaPmF	spočíst
dobu	doba	k1gFnSc4	doba
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
světlu	světlo	k1gNnSc3	světlo
k	k	k7c3	k
uražení	uražení	k1gNnSc3	uražení
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
mezi	mezi	k7c7	mezi
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
rezonančních	rezonanční	k2eAgFnPc2d1	rezonanční
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Ganymedu	Ganymed	k1gMnSc3	Ganymed
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Pierre-Simon	Pierre-Simon	k1gInSc4	Pierre-Simon
Laplace	Laplace	k1gFnSc2	Laplace
matematickou	matematický	k2eAgFnSc4d1	matematická
teorii	teorie	k1gFnSc4	teorie
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
údajích	údaj	k1gInPc6	údaj
Cassiniho	Cassini	k1gMnSc2	Cassini
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
rezonance	rezonance	k1gFnSc1	rezonance
má	mít	k5eAaImIp3nS	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
geologii	geologie	k1gFnSc4	geologie
zmíněných	zmíněný	k2eAgNnPc2d1	zmíněné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
optické	optický	k2eAgFnSc2d1	optická
astronomie	astronomie	k1gFnSc2	astronomie
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
umožnil	umožnit	k5eAaPmAgMnS	umožnit
astronomům	astronom	k1gMnPc3	astronom
zlepšit	zlepšit	k5eAaPmF	zlepšit
pozorování	pozorování	k1gNnPc4	pozorování
a	a	k8xC	a
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
celopovrchové	celopovrchový	k2eAgInPc4d1	celopovrchový
útvary	útvar	k1gInPc4	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Edward	Edward	k1gMnSc1	Edward
E.	E.	kA	E.
Barnard	Barnard	k1gMnSc1	Barnard
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
rozdíly	rozdíl	k1gInPc7	rozdíl
v	v	k7c6	v
jasu	jas	k1gInSc6	jas
polárních	polární	k2eAgFnPc2d1	polární
a	a	k8xC	a
rovníkových	rovníkový	k2eAgFnPc2d1	Rovníková
oblastí	oblast	k1gFnPc2	oblast
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
správně	správně	k6eAd1	správně
určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
různým	různý	k2eAgInSc7d1	různý
albedem	albed	k1gInSc7	albed
a	a	k8xC	a
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
barvami	barva	k1gFnPc7	barva
obou	dva	k4xCgInPc2	dva
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Současník	současník	k1gMnSc1	současník
William	William	k1gInSc4	William
Henry	henry	k1gInSc2	henry
Pickering	Pickering	k1gInSc1	Pickering
navrhoval	navrhovat	k5eAaImAgInS	navrhovat
jiné	jiný	k2eAgNnSc4d1	jiné
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Io	Io	k1gFnSc1	Io
má	mít	k5eAaImIp3nS	mít
vajíčkový	vajíčkový	k2eAgInSc4d1	vajíčkový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
či	či	k8xC	či
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnPc4d2	pozdější
pozorování	pozorování	k1gNnPc4	pozorování
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
polární	polární	k2eAgFnPc1d1	polární
oblasti	oblast	k1gFnPc1	oblast
mají	mít	k5eAaImIp3nP	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
a	a	k8xC	a
rovníkové	rovníkový	k2eAgInPc1d1	rovníkový
žluto-bílou	žlutoílat	k5eAaPmIp3nP	žluto-bílat
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
poznatky	poznatek	k1gInPc1	poznatek
ukazující	ukazující	k2eAgInPc1d1	ukazující
na	na	k7c4	na
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
charakter	charakter	k1gInSc4	charakter
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Spektroskopické	spektroskopický	k2eAgNnSc1d1	spektroskopické
pozorování	pozorování	k1gNnSc1	pozorování
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
Io	Io	k1gFnSc2	Io
postrádá	postrádat	k5eAaImIp3nS	postrádat
přítomnost	přítomnost	k1gFnSc1	přítomnost
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
u	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
podobných	podobný	k2eAgNnPc2d1	podobné
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
byl	být	k5eAaImAgInS	být
pozorován	pozorován	k2eAgInSc1d1	pozorován
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc1d1	stejné
pozorování	pozorování	k1gNnSc1	pozorování
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
solemi	sůl	k1gFnPc7	sůl
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Radioteleskopická	Radioteleskopický	k2eAgNnPc1d1	Radioteleskopický
pozorování	pozorování	k1gNnPc1	pozorování
Io	Io	k1gMnSc2	Io
odhalily	odhalit	k5eAaPmAgInP	odhalit
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
emisemi	emise	k1gFnPc7	emise
decimetrových	decimetrový	k2eAgFnPc2d1	decimetrový
vln	vlna	k1gFnPc2	vlna
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
oběžné	oběžný	k2eAgFnSc3d1	oběžná
době	doba	k1gFnSc3	doba
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sondy	sonda	k1gFnSc2	sonda
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
a	a	k8xC	a
11	[number]	k4	11
====	====	k?	====
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
sondy	sonda	k1gFnPc4	sonda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
proletěly	proletět	k5eAaPmAgFnP	proletět
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
byla	být	k5eAaImAgFnS	být
dvojice	dvojice	k1gFnSc1	dvojice
sond	sonda	k1gFnPc2	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Radiové	radiový	k2eAgNnSc1d1	radiové
pozorování	pozorování	k1gNnSc1	pozorování
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
určit	určit	k5eAaPmF	určit
hmotnost	hmotnost	k1gFnSc4	hmotnost
Io	Io	k1gFnPc2	Io
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
s	s	k7c7	s
porovnáním	porovnání	k1gNnSc7	porovnání
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
měsíce	měsíc	k1gInSc2	měsíc
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
poznatku	poznatek	k1gInSc3	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
Io	Io	k1gFnSc1	Io
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
hustotu	hustota	k1gFnSc4	hustota
ze	z	k7c2	z
4	[number]	k4	4
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
bude	být	k5eAaImBp3nS	být
nejspíše	nejspíše	k9	nejspíše
ze	z	k7c2	z
silikátových	silikátový	k2eAgFnPc2d1	silikátová
hornin	hornina	k1gFnPc2	hornina
namísto	namísto	k7c2	namísto
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Sondy	sonda	k1gFnPc1	sonda
Pioneer	Pioneer	kA	Pioneer
taktéž	taktéž	k?	taktéž
odhalily	odhalit	k5eAaPmAgInP	odhalit
přítomnost	přítomnost	k1gFnSc4	přítomnost
tenké	tenký	k2eAgFnSc2d1	tenká
atmosféry	atmosféra	k1gFnSc2	atmosféra
okolo	okolo	k7c2	okolo
Io	Io	k1gFnSc2	Io
a	a	k8xC	a
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
radiační	radiační	k2eAgInPc4d1	radiační
pásy	pás	k1gInPc4	pás
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kamera	kamera	k1gFnSc1	kamera
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sondy	sonda	k1gFnSc2	sonda
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
pořídila	pořídit	k5eAaPmAgFnS	pořídit
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
odvysílány	odvysílán	k2eAgFnPc1d1	odvysílána
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
fotografie	fotografia	k1gFnPc1	fotografia
pouze	pouze	k6eAd1	pouze
severní	severní	k2eAgFnSc2d1	severní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Detailnější	detailní	k2eAgInPc1d2	detailnější
snímky	snímek	k1gInPc1	snímek
byly	být	k5eAaImAgInP	být
plánovány	plánován	k2eAgInPc1d1	plánován
pořídit	pořídit	k5eAaPmF	pořídit
sondou	sonda	k1gFnSc7	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snímky	snímka	k1gFnPc1	snímka
byly	být	k5eAaImAgFnP	být
ztraceny	ztratit	k5eAaPmNgFnP	ztratit
vlivem	vlivem	k7c2	vlivem
vysoké	vysoký	k2eAgFnSc2d1	vysoká
radiace	radiace	k1gFnSc2	radiace
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
====	====	k?	====
</s>
</p>
<p>
<s>
Když	když	k8xS	když
dvojice	dvojice	k1gFnSc1	dvojice
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
proletěla	proletět	k5eAaPmAgFnS	proletět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
kolem	kolem	k7c2	kolem
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vyspělejší	vyspělý	k2eAgFnSc1d2	vyspělejší
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
technika	technika	k1gFnSc1	technika
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sond	sonda	k1gFnPc2	sonda
pořídit	pořídit	k5eAaPmF	pořídit
mnohem	mnohem	k6eAd1	mnohem
podrobnější	podrobný	k2eAgInSc1d2	podrobnější
obraz	obraz	k1gInSc1	obraz
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
přeletěl	přeletět	k5eAaPmAgInS	přeletět
okolo	okolo	k7c2	okolo
Io	Io	k1gFnSc2	Io
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1979	[number]	k4	1979
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
20	[number]	k4	20
600	[number]	k4	600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
zaslal	zaslat	k5eAaPmAgMnS	zaslat
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ukázaly	ukázat	k5eAaPmAgInP	ukázat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
krajinu	krajina	k1gFnSc4	krajina
mnoha	mnoho	k4c2	mnoho
barev	barva	k1gFnPc2	barva
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
větších	veliký	k2eAgInPc2d2	veliký
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
svědčící	svědčící	k2eAgInSc1d1	svědčící
o	o	k7c6	o
relativně	relativně	k6eAd1	relativně
mladém	mladý	k2eAgInSc6d1	mladý
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgInPc1d1	detailní
snímky	snímek	k1gInPc1	snímek
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
mladé	mladý	k2eAgInPc4d1	mladý
útvary	útvar	k1gInPc4	útvar
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgFnPc4d1	hluboká
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc4	hora
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
Mount	Mount	k1gMnSc1	Mount
Everest	Everest	k1gInSc1	Everest
a	a	k8xC	a
tělesa	těleso	k1gNnPc1	těleso
připomínající	připomínající	k2eAgNnSc1d1	připomínající
lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
známé	známý	k2eAgInPc1d1	známý
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
průletu	průlet	k1gInSc6	průlet
si	se	k3xPyFc3	se
navigační	navigační	k2eAgFnSc1d1	navigační
inženýrka	inženýrka	k1gFnSc1	inženýrka
Linda	Linda	k1gFnSc1	Linda
A.	A.	kA	A.
Morabito	Morabita	k1gMnSc5	Morabita
všimla	všimnout	k5eAaPmAgFnS	všimnout
tmavého	tmavý	k2eAgInSc2d1	tmavý
oblaku	oblak	k1gInSc2	oblak
vycházejícího	vycházející	k2eAgInSc2d1	vycházející
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
dalších	další	k2eAgInPc2d1	další
snímků	snímek	k1gInPc2	snímek
ukázala	ukázat	k5eAaPmAgFnS	ukázat
devět	devět	k4xCc4	devět
takových	takový	k3xDgNnPc2	takový
mračen	mračno	k1gNnPc2	mračno
rozptýlených	rozptýlený	k2eAgNnPc2d1	rozptýlené
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
prokázaly	prokázat	k5eAaPmAgInP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Io	Io	k1gFnSc1	Io
je	být	k5eAaImIp3nS	být
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivním	aktivní	k2eAgNnSc7d1	aktivní
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
dřívější	dřívější	k2eAgFnSc4d1	dřívější
studii	studie	k1gFnSc4	studie
napsanou	napsaný	k2eAgFnSc4d1	napsaná
Stanem	stan	k1gInSc7	stan
J.	J.	kA	J.
Pealem	Pealo	k1gNnSc7	Pealo
<g/>
,	,	kIx,	,
Patrickem	Patrick	k1gInSc7	Patrick
Cassenem	Cassen	k1gInSc7	Cassen
a	a	k8xC	a
R.	R.	kA	R.
T.	T.	kA	T.
Reynoldsem	Reynolds	k1gInSc7	Reynolds
a	a	k8xC	a
publikovanou	publikovaný	k2eAgFnSc7d1	publikovaná
před	před	k7c7	před
cestou	cesta	k1gFnSc7	cesta
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
studie	studie	k1gFnSc2	studie
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
a	a	k8xC	a
spočetli	spočíst	k5eAaPmAgMnP	spočíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
Io	Io	k1gFnSc2	Io
musí	muset	k5eAaImIp3nS	muset
získávat	získávat	k5eAaImF	získávat
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
vlivem	vlivem	k7c2	vlivem
slapového	slapový	k2eAgNnSc2d1	slapové
působení	působení	k1gNnSc2	působení
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
oběžné	oběžný	k2eAgFnSc2d1	oběžná
rezonance	rezonance	k1gFnSc2	rezonance
s	s	k7c7	s
Europou	Europa	k1gFnSc7	Europa
a	a	k8xC	a
Ganymedem	Ganymed	k1gMnSc7	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
z	z	k7c2	z
průletu	průlet	k1gInSc2	průlet
taktéž	taktéž	k?	taktéž
pomohly	pomoct	k5eAaPmAgFnP	pomoct
určit	určit	k5eAaPmF	určit
dominantní	dominantní	k2eAgNnSc4d1	dominantní
zastoupení	zastoupení	k1gNnSc4	zastoupení
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
podobné	podobný	k2eAgNnSc1d1	podobné
složení	složení	k1gNnSc1	složení
tenké	tenký	k2eAgFnSc2d1	tenká
atmosféry	atmosféra	k1gFnSc2	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
měsícem	měsíc	k1gInSc7	měsíc
byl	být	k5eAaImAgInS	být
současně	současně	k6eAd1	současně
pozorován	pozorovat	k5eAaImNgInS	pozorovat
i	i	k9	i
torus	torus	k1gInSc1	torus
<g/>
.	.	kIx.	.
<g/>
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
proletěl	proletět	k5eAaPmAgInS	proletět
okolo	okolo	k7c2	okolo
Io	Io	k1gFnSc2	Io
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
130	[number]	k4	130
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tak	tak	k6eAd1	tak
těsný	těsný	k2eAgInSc4d1	těsný
průlet	průlet	k1gInSc4	průlet
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sesterské	sesterský	k2eAgFnSc2d1	sesterská
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
analýza	analýza	k1gFnSc1	analýza
pořízených	pořízený	k2eAgInPc2d1	pořízený
snímků	snímek	k1gInPc2	snímek
ukázala	ukázat	k5eAaPmAgFnS	ukázat
několik	několik	k4yIc4	několik
změn	změna	k1gFnPc2	změna
na	na	k7c6	na
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pouhých	pouhý	k2eAgInPc2d1	pouhý
pěti	pět	k4xCc2	pět
měsíců	měsíc	k1gInPc2	měsíc
mezi	mezi	k7c7	mezi
průletem	průlet	k1gInSc7	průlet
obou	dva	k4xCgFnPc2	dva
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
průlet	průlet	k1gInSc1	průlet
taktéž	taktéž	k?	taktéž
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
osm	osm	k4xCc1	osm
z	z	k7c2	z
devíti	devět	k4xCc2	devět
pozorovaných	pozorovaný	k2eAgNnPc2d1	pozorované
sopečných	sopečný	k2eAgNnPc2d1	sopečné
mračen	mračno	k1gNnPc2	mračno
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
sopky	sopka	k1gFnPc1	sopka
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgFnPc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
sopka	sopka	k1gFnSc1	sopka
Pele	pel	k1gInSc5	pel
ustala	ustat	k5eAaPmAgFnS	ustat
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
činnost	činnost	k1gFnSc4	činnost
mezi	mezi	k7c7	mezi
průletem	průlet	k1gInSc7	průlet
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
====	====	k?	====
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
doletěla	doletět	k5eAaPmAgFnS	doletět
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
po	po	k7c6	po
šestileté	šestiletý	k2eAgFnSc6d1	šestiletá
cestě	cesta	k1gFnSc6	cesta
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
jupiterské	jupiterský	k2eAgFnSc2d1	Jupiterská
soustavy	soustava	k1gFnSc2	soustava
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončily	skončit	k5eAaPmAgFnP	skončit
sondy	sonda	k1gFnPc1	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc1	měsíc
Io	Io	k1gFnSc2	Io
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgInPc2d3	nejsilnější
radiačních	radiační	k2eAgInPc2d1	radiační
pásů	pás	k1gInPc2	pás
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
sonda	sonda	k1gFnSc1	sonda
provádět	provádět	k5eAaImF	provádět
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
nízké	nízký	k2eAgInPc4d1	nízký
přelety	přelet	k1gInPc4	přelet
nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
měsícem	měsíc	k1gInSc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
jeden	jeden	k4xCgMnSc1	jeden
těsný	těsný	k2eAgInSc1d1	těsný
průlet	průlet	k1gInSc1	průlet
před	před	k7c7	před
navedením	navedení	k1gNnSc7	navedení
na	na	k7c4	na
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
primární	primární	k2eAgFnSc4d1	primární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
během	během	k7c2	během
přeletu	přelet	k1gInSc2	přelet
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
nebyly	být	k5eNaImAgFnP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
žádné	žádný	k3yNgInPc1	žádný
snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
i	i	k8xC	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
potvrdit	potvrdit	k5eAaPmF	potvrdit
existenci	existence	k1gFnSc4	existence
velkého	velký	k2eAgNnSc2d1	velké
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
měsíce	měsíc	k1gInSc2	měsíc
podobného	podobný	k2eAgInSc2d1	podobný
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
mají	mít	k5eAaImIp3nP	mít
terestrické	terestrický	k2eAgFnPc1d1	terestrická
planety	planeta	k1gFnPc1	planeta
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
když	když	k8xS	když
sonda	sonda	k1gFnSc1	sonda
neprováděla	provádět	k5eNaImAgFnS	provádět
přímé	přímý	k2eAgInPc4d1	přímý
přelety	přelet	k1gInPc4	přelet
nad	nad	k7c7	nad
měsícem	měsíc	k1gInSc7	měsíc
a	a	k8xC	a
přes	přes	k7c4	přes
technické	technický	k2eAgFnPc4d1	technická
potíže	potíž	k1gFnPc4	potíž
neumožňující	umožňující	k2eNgNnSc1d1	neumožňující
odeslat	odeslat	k5eAaPmF	odeslat
všechny	všechen	k3xTgInPc4	všechen
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
množství	množství	k1gNnSc4	množství
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c4	o
Io	Io	k1gFnPc4	Io
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgFnP	umožnit
učinit	učinit	k5eAaPmF	učinit
významné	významný	k2eAgInPc4d1	významný
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
erupce	erupce	k1gFnSc1	erupce
sopky	sopka	k1gFnSc2	sopka
Pillan	Pillan	k1gMnSc1	Pillan
Patera	Patera	k1gMnSc1	Patera
a	a	k8xC	a
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyvrhovaný	vyvrhovaný	k2eAgInSc1d1	vyvrhovaný
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
křemičitého	křemičitý	k2eAgNnSc2d1	křemičité
magmatu	magma	k1gNnSc2	magma
mafického	mafický	k2eAgNnSc2d1	mafický
a	a	k8xC	a
ultramafického	ultramafický	k2eAgNnSc2d1	ultramafický
složení	složení	k1gNnSc2	složení
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
hrajícího	hrající	k2eAgInSc2d1	hrající
stejnou	stejný	k2eAgFnSc4d1	stejná
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
snímkování	snímkování	k1gNnSc1	snímkování
Io	Io	k1gFnSc2	Io
téměř	téměř	k6eAd1	téměř
během	během	k7c2	během
každého	každý	k3xTgInSc2	každý
přeletu	přelet	k1gInSc2	přelet
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
hor	hora	k1gFnPc2	hora
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
morfologií	morfologie	k1gFnSc7	morfologie
a	a	k8xC	a
několik	několik	k4yIc4	několik
významných	významný	k2eAgFnPc2d1	významná
změn	změna	k1gFnPc2	změna
oblastí	oblast	k1gFnPc2	oblast
mezi	mezi	k7c4	mezi
přelety	přelet	k1gInPc4	přelet
sond	sonda	k1gFnPc2	sonda
řady	řada	k1gFnSc2	řada
Voyager	Voyager	k1gInSc1	Voyager
či	či	k8xC	či
i	i	k9	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
přelety	přelet	k1gInPc7	přelet
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
<g/>
Mise	mise	k1gFnSc1	mise
sondy	sonda	k1gFnSc2	sonda
byla	být	k5eAaImAgFnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgNnPc2	tento
prodloužení	prodloužení	k1gNnPc2	prodloužení
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
sonda	sonda	k1gFnSc1	sonda
okolo	okolo	k7c2	okolo
Io	Io	k1gFnSc2	Io
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
1999	[number]	k4	1999
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
třikrát	třikrát	k6eAd1	třikrát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Průlety	průlet	k1gInPc1	průlet
pomohly	pomoct	k5eAaPmAgInP	pomoct
zmapovat	zmapovat	k5eAaPmF	zmapovat
rozsah	rozsah	k1gInSc4	rozsah
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
vyloučit	vyloučit	k5eAaPmF	vyloučit
existenci	existence	k1gFnSc4	existence
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
zmapovat	zmapovat	k5eAaPmF	zmapovat
sopky	sopka	k1gFnSc2	sopka
a	a	k8xC	a
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2000	[number]	k4	2000
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
sonda	sonda	k1gFnSc1	sonda
Cassini-Huygens	Cassini-Huygensa	k1gFnPc2	Cassini-Huygensa
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
a	a	k8xC	a
vykonala	vykonat	k5eAaPmAgNnP	vykonat
společná	společný	k2eAgNnPc1d1	společné
pozorování	pozorování	k1gNnPc1	pozorování
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odhalila	odhalit	k5eAaPmAgFnS	odhalit
nový	nový	k2eAgInSc4d1	nový
chochol	chochol	k1gInSc4	chochol
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tvashtar	Tvashtara	k1gFnPc2	Tvashtara
Paterae	Patera	k1gFnSc2	Patera
a	a	k8xC	a
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
okolo	okolo	k7c2	okolo
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Následné	následný	k2eAgNnSc1d1	následné
pozorování	pozorování	k1gNnSc1	pozorování
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
plánovaném	plánovaný	k2eAgNnSc6d1	plánované
navedení	navedení	k1gNnSc6	navedení
a	a	k8xC	a
zániku	zánik	k1gInSc6	zánik
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
2003	[number]	k4	2003
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
další	další	k2eAgNnSc1d1	další
pozorování	pozorování	k1gNnSc1	pozorování
měsíce	měsíc	k1gInSc2	měsíc
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
výkonných	výkonný	k2eAgInPc2d1	výkonný
pozemských	pozemský	k2eAgInPc2d1	pozemský
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
optiky	optika	k1gFnSc2	optika
z	z	k7c2	z
teleskopu	teleskop	k1gInSc2	teleskop
Keck	Kecka	k1gFnPc2	Kecka
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
a	a	k8xC	a
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
teleskopu	teleskop	k1gInSc2	teleskop
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
sledovat	sledovat	k5eAaImF	sledovat
aktivní	aktivní	k2eAgInPc4d1	aktivní
projevy	projev	k1gInPc4	projev
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
nová	nový	k2eAgNnPc1d1	nové
pozorování	pozorování	k1gNnPc1	pozorování
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vědcům	vědec	k1gMnPc3	vědec
sledovat	sledovat	k5eAaImF	sledovat
sopky	sopka	k1gFnPc4	sopka
na	na	k7c6	na
Io	Io	k1gFnSc6	Io
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
vyslat	vyslat	k5eAaPmF	vyslat
do	do	k7c2	do
systému	systém	k1gInSc2	systém
další	další	k2eAgFnSc4d1	další
sondu	sonda	k1gFnSc4	sonda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
New	New	k1gFnPc2	New
Horizons	Horizons	k1gInSc1	Horizons
====	====	k?	====
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
systémem	systém	k1gInSc7	systém
Jupiteru	Jupiter	k1gInSc2	Jupiter
sonda	sonda	k1gFnSc1	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
a	a	k8xC	a
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
množství	množství	k1gNnSc1	množství
pozorování	pozorování	k1gNnSc2	pozorování
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
velká	velký	k2eAgNnPc4d1	velké
sopečná	sopečný	k2eAgNnPc4d1	sopečné
mračna	mračno	k1gNnPc4	mračno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tvashtar	Tvashtar	k1gInSc4	Tvashtar
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
detailní	detailní	k2eAgNnSc1d1	detailní
pozorování	pozorování	k1gNnSc1	pozorování
obrovských	obrovský	k2eAgFnPc2d1	obrovská
erupcí	erupce	k1gFnPc2	erupce
srovnatelných	srovnatelný	k2eAgFnPc2d1	srovnatelná
s	s	k7c7	s
mračny	mračno	k1gNnPc7	mračno
okolo	okolo	k7c2	okolo
Pele	pel	k1gInSc5	pel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
současně	současně	k6eAd1	současně
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
mračna	mračna	k1gFnSc1	mračna
okolo	okolo	k7c2	okolo
Girru	Girr	k1gInSc2	Girr
Patera	Patera	k1gMnSc1	Patera
v	v	k7c6	v
počátečním	počáteční	k2eAgNnSc6d1	počáteční
stádiu	stádium	k1gNnSc6	stádium
erupcí	erupce	k1gFnPc2	erupce
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mise	mise	k1gFnSc2	mise
sondy	sonda	k1gFnSc2	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Juno	Juno	k1gFnSc2	Juno
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
omezené	omezený	k2eAgInPc4d1	omezený
vizuální	vizuální	k2eAgInPc4d1	vizuální
snímače	snímač	k1gInPc4	snímač
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
monitorování	monitorování	k1gNnSc4	monitorování
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
na	na	k7c6	na
Io	Io	k1gFnSc6	Io
využívat	využívat	k5eAaImF	využívat
infračervený	infračervený	k2eAgInSc4d1	infračervený
spektrometr	spektrometr	k1gInSc4	spektrometr
JIRAM	JIRAM	kA	JIRAM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Budoucí	budoucí	k2eAgFnSc2d1	budoucí
mise	mise	k1gFnSc2	mise
===	===	k?	===
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
se	se	k3xPyFc4	se
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
projektu	projekt	k1gInSc2	projekt
s	s	k7c7	s
NASA	NASA	kA	NASA
s	s	k7c7	s
názvem	název	k1gInSc7	název
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gMnSc7	Syst
Mission	Mission	k1gInSc4	Mission
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
odstartovat	odstartovat	k5eAaPmF	odstartovat
dvojice	dvojice	k1gFnSc2	dvojice
sond	sonda	k1gFnPc2	sonda
<g/>
:	:	kIx,	:
americká	americký	k2eAgFnSc1d1	americká
Jupiter	Jupiter	k1gMnSc1	Jupiter
Europa	Europ	k1gMnSc2	Europ
Orbiter	Orbiter	k1gMnSc1	Orbiter
a	a	k8xC	a
evropská	evropský	k2eAgFnSc1d1	Evropská
Jupiter	Jupiter	k1gMnSc1	Jupiter
Ganymede	Ganymed	k1gMnSc5	Ganymed
Orbiter	Orbiter	k1gInSc1	Orbiter
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
sond	sonda	k1gFnPc2	sonda
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
jako	jako	k9	jako
primární	primární	k2eAgInSc4d1	primární
cíl	cíl	k1gInSc4	cíl
výzkum	výzkum	k1gInSc4	výzkum
Io	Io	k1gFnPc2	Io
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
ho	on	k3xPp3gMnSc4	on
budou	být	k5eAaImBp3nP	být
sledovat	sledovat	k5eAaImF	sledovat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Jupiter	Jupiter	k1gInSc1	Jupiter
Europa	Europa	k1gFnSc1	Europa
Orbiter	Orbitrum	k1gNnPc2	Orbitrum
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ale	ale	k9	ale
měla	mít	k5eAaImAgFnS	mít
přiblížit	přiblížit	k5eAaPmF	přiblížit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2025	[number]	k4	2025
a	a	k8xC	a
2026	[number]	k4	2026
k	k	k7c3	k
měsíci	měsíc	k1gInSc3	měsíc
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
těchto	tento	k3xDgFnPc2	tento
misí	mise	k1gFnPc2	mise
NASA	NASA	kA	NASA
již	již	k6eAd1	již
schválila	schválit	k5eAaPmAgFnS	schválit
taktéž	taktéž	k?	taktéž
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
misi	mise	k1gFnSc4	mise
k	k	k7c3	k
Io	Io	k1gFnPc3	Io
tzv.	tzv.	kA	tzv.
Io	Io	k1gFnSc1	Io
Volcano	Volcana	k1gFnSc5	Volcana
Observer	Observra	k1gFnPc2	Observra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
odstartovat	odstartovat	k5eAaPmF	odstartovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
dle	dle	k7c2	dle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
po	po	k7c6	po
kněžce	kněžka	k1gFnSc6	kněžka
Íó	Íó	k1gMnSc2	Íó
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
mnohých	mnohý	k2eAgFnPc2d1	mnohá
lásek	láska	k1gFnPc2	láska
boha	bůh	k1gMnSc4	bůh
Dia	Dia	k1gMnSc4	Dia
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
jméno	jméno	k1gNnSc4	jméno
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nebylo	být	k5eNaImAgNnS	být
Simonovi	Simon	k1gMnSc3	Simon
Mariusovi	Marius	k1gMnSc3	Marius
přiznáno	přiznán	k2eAgNnSc1d1	přiznáno
objevení	objevení	k1gNnSc1	objevení
měsíce	měsíc	k1gInSc2	měsíc
před	před	k7c7	před
Galileem	Galileum	k1gNnSc7	Galileum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
názvy	název	k1gInPc1	název
pro	pro	k7c4	pro
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
vžily	vžít	k5eAaPmAgFnP	vžít
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
publikaci	publikace	k1gFnSc6	publikace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1614	[number]	k4	1614
Mundus	Mundus	k1gInSc1	Mundus
Jovialis	Jovialis	k1gFnSc2	Jovialis
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
měsíci	měsíc	k1gInSc6	měsíc
jako	jako	k8xC	jako
o	o	k7c4	o
Io	Io	k1gMnSc4	Io
<g/>
,	,	kIx,	,
milence	milenec	k1gMnSc4	milenec
Dia	Dia	k1gMnSc4	Dia
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
názvy	název	k1gInPc1	název
zapadly	zapadnout	k5eAaPmAgInP	zapadnout
ale	ale	k9	ale
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
opět	opět	k6eAd1	opět
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
staršího	starý	k2eAgNnSc2d2	starší
data	datum	k1gNnSc2	datum
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
měsíc	měsíc	k1gInSc4	měsíc
Io	Io	k1gMnPc3	Io
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
názvem	název	k1gInSc7	název
Jupiter	Jupiter	k1gMnSc1	Jupiter
I	i	k8xC	i
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
===	===	k?	===
</s>
</p>
<p>
<s>
Útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInPc4	měsíc
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
osobách	osoba	k1gFnPc6	osoba
a	a	k8xC	a
místech	místo	k1gNnPc6	místo
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
mytologií	mytologie	k1gFnSc7	mytologie
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
po	po	k7c6	po
bozích	bůh	k1gMnPc6	bůh
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
sopek	sopka	k1gFnPc2	sopka
či	či	k8xC	či
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
mytických	mytický	k2eAgInPc6d1	mytický
objektech	objekt	k1gInPc6	objekt
jako	jako	k8xC	jako
např.	např.	kA	např.
Inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
názvu	název	k1gInSc2	název
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
Danteho	Dante	k1gMnSc2	Dante
knihy	kniha	k1gFnSc2	kniha
Božská	božská	k1gFnSc1	božská
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
detailnějšího	detailní	k2eAgInSc2d2	detailnější
průzkumu	průzkum	k1gInSc2	průzkum
měsíce	měsíc	k1gInSc2	měsíc
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
1	[number]	k4	1
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
přibližně	přibližně	k6eAd1	přibližně
225	[number]	k4	225
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
rovin	rovina	k1gFnPc2	rovina
a	a	k8xC	a
výrazných	výrazný	k2eAgInPc2d1	výrazný
albedových	albedův	k2eAgInPc2d1	albedův
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc4	označení
např.	např.	kA	např.
patera	pater	k1gMnSc2	pater
<g/>
,	,	kIx,	,
mons	mons	k1gInSc1	mons
<g/>
,	,	kIx,	,
mensa	mensa	k1gFnSc1	mensa
<g/>
,	,	kIx,	,
planum	planum	k1gInSc1	planum
<g/>
,	,	kIx,	,
fluctus	fluctus	k1gInSc1	fluctus
(	(	kIx(	(
<g/>
lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
<g/>
)	)	kIx)	)
či	či	k8xC	či
tholus	tholus	k1gInSc1	tholus
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
sklonu	sklon	k1gInSc6	sklon
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc6	velikost
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Io	Io	k1gMnPc4	Io
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Io	Io	k1gFnSc6	Io
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
knižní	knižní	k2eAgFnSc2d1	knižní
série	série	k1gFnSc2	série
a	a	k8xC	a
seriálu	seriál	k1gInSc2	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Io	Io	k1gFnSc6	Io
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
děj	děj	k1gInSc1	děj
sci-fi	scii	k1gNnSc2	sci-fi
filmu	film	k1gInSc2	film
Outland	Outlanda	k1gFnPc2	Outlanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Io	Io	k1gFnSc2	Io
(	(	kIx(	(
<g/>
moon	moon	k1gMnSc1	moon
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČEMAN	ČEMAN	kA	ČEMAN
<g/>
,	,	kIx,	,
Róbert	Róbert	k1gInSc1	Róbert
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc4	vesmír
1	[number]	k4	1
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Mapa	mapa	k1gFnSc1	mapa
Slovakia	Slovakia	k1gFnSc1	Slovakia
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
8067	[number]	k4	8067
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
72	[number]	k4	72
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
s.	s.	k?	s.
242	[number]	k4	242
až	až	k9	až
245	[number]	k4	245
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GREGERSEN	GREGERSEN	kA	GREGERSEN
<g/>
,	,	kIx,	,
Erik	Erik	k1gMnSc1	Erik
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Outer	Outer	k1gMnSc1	Outer
Solar	Solar	k1gMnSc1	Solar
System	Syst	k1gMnSc7	Syst
<g/>
:	:	kIx,	:
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
Uranus	Uranus	k1gMnSc1	Uranus
<g/>
,	,	kIx,	,
Neptune	Neptun	k1gMnSc5	Neptun
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Dwarf	Dwarf	k1gInSc1	Dwarf
Planets	Planets	k1gInSc1	Planets
<g/>
.	.	kIx.	.
</s>
<s>
Britannica	Britannica	k1gMnSc1	Britannica
Educational	Educational	k1gMnSc1	Educational
Pub	Pub	k1gMnSc1	Pub
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
61530	[number]	k4	61530
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
110	[number]	k4	110
až	až	k9	až
111	[number]	k4	111
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Io	Io	k1gFnSc2	Io
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Io	Io	k1gFnSc2	Io
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Popis	popis	k1gInSc1	popis
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Solarviews	Solarviews	k1gInSc1	Solarviews
–	–	k?	–
Io	Io	k1gFnSc2	Io
</s>
</p>
