<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvykle	obvykle	k6eAd1	obvykle
délky	délka	k1gFnSc2	délka
okolo	okolo	k7c2	okolo
1	[number]	k4	1
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
kusy	kus	k1gInPc1	kus
měřily	měřit	k5eAaImAgInP	měřit
1,9	[number]	k4	1,9
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
