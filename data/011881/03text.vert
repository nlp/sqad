<p>
<s>
Kachnička	kachnička	k1gFnSc1	kachnička
mandarínská	mandarínský	k2eAgFnSc1d1	mandarínská
(	(	kIx(	(
<g/>
Aix	Aix	k1gMnPc3	Aix
galericulata	galericule	k1gNnPc4	galericule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
vrubozobý	vrubozobý	k2eAgMnSc1d1	vrubozobý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kachnovití	kachnovití	k1gMnPc1	kachnovití
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
obývá	obývat	k5eAaImIp3nS	obývat
Amur	Amur	k1gInSc4	Amur
<g/>
,	,	kIx,	,
Sachalin	Sachalin	k1gInSc4	Sachalin
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
<g/>
,	,	kIx,	,
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
byl	být	k5eAaImAgInS	být
dovezen	dovézt	k5eAaPmNgInS	dovézt
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
ozdobný	ozdobný	k2eAgMnSc1d1	ozdobný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
volně	volně	k6eAd1	volně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
mimo	mimo	k6eAd1	mimo
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
850	[number]	k4	850
<g/>
–	–	k?	–
<g/>
3000	[number]	k4	3000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
7000	[number]	k4	7000
ptáků	pták	k1gMnPc2	pták
žije	žít	k5eAaImIp3nS	žít
volně	volně	k6eAd1	volně
na	na	k7c6	na
území	území	k1gNnSc6	území
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
začaly	začít	k5eAaPmAgFnP	začít
pravidelně	pravidelně	k6eAd1	pravidelně
hnízdit	hnízdit	k5eAaImF	hnízdit
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Svratce	Svratka	k1gFnSc6	Svratka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nyní	nyní	k6eAd1	nyní
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
20	[number]	k4	20
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Samec	samec	k1gMnSc1	samec
kachničky	kachnička	k1gFnSc2	kachnička
mandarínské	mandarínský	k2eAgFnSc2d1	mandarínská
ve	v	k7c6	v
svatebním	svatební	k2eAgInSc6d1	svatební
šatu	šat	k1gInSc6	šat
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejzdobnější	zdobný	k2eAgMnPc4d3	zdobný
a	a	k8xC	a
nejkrásnější	krásný	k2eAgMnPc4d3	nejkrásnější
ptáky	pták	k1gMnPc4	pták
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
chocholku	chocholka	k1gFnSc4	chocholka
<g/>
,	,	kIx,	,
péra	péro	k1gNnPc4	péro
zlaté	zlatý	k2eAgFnSc2d1	zlatá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
pár	pár	k4xCyI	pár
jasně	jasně	k6eAd1	jasně
žlutých	žlutý	k2eAgFnPc2d1	žlutá
čepelí	čepel	k1gFnPc2	čepel
tvořených	tvořený	k2eAgFnPc2d1	tvořená
širokými	široký	k2eAgInPc7d1	široký
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
prapory	prapor	k1gInPc7	prapor
křídelních	křídelní	k2eAgFnPc2d1	křídelní
krovek	krovka	k1gFnPc2	krovka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
čepele	čepel	k1gInPc1	čepel
mají	mít	k5eAaImIp3nP	mít
čistě	čistě	k6eAd1	čistě
ornamentální	ornamentální	k2eAgFnSc4d1	ornamentální
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
samec	samec	k1gMnSc1	samec
je	on	k3xPp3gNnPc4	on
může	moct	k5eAaImIp3nS	moct
vztyčit	vztyčit	k5eAaPmF	vztyčit
nad	nad	k7c7	nad
boky	bok	k1gInPc7	bok
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
a	a	k8xC	a
samci	samec	k1gMnPc1	samec
v	v	k7c6	v
prostém	prostý	k2eAgInSc6d1	prostý
šatu	šat	k1gInSc6	šat
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
olivově	olivově	k6eAd1	olivově
hnědí	hnědit	k5eAaImIp3nS	hnědit
<g/>
.	.	kIx.	.
</s>
<s>
Nenápadně	nápadně	k6eNd1	nápadně
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
samici	samice	k1gFnSc4	samice
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
poznáme	poznat	k5eAaPmIp1nP	poznat
dle	dle	k7c2	dle
úzkého	úzký	k2eAgInSc2d1	úzký
bílého	bílý	k2eAgInSc2d1	bílý
proužku	proužek	k1gInSc2	proužek
za	za	k7c7	za
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
kachny	kachna	k1gFnPc1	kachna
sedající	sedající	k2eAgFnPc1d1	sedající
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
má	mít	k5eAaImIp3nS	mít
ostré	ostrý	k2eAgInPc4d1	ostrý
drápy	dráp	k1gInPc4	dráp
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jí	on	k3xPp3gFnSc7	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
pevném	pevný	k2eAgNnSc6d1	pevné
uchopení	uchopení	k1gNnSc6	uchopení
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
široký	široký	k2eAgInSc1d1	široký
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
působící	působící	k2eAgInSc1d1	působící
jako	jako	k8xS	jako
brzda	brzda	k1gFnSc1	brzda
ke	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
když	když	k8xS	když
kachnička	kachnička	k1gFnSc1	kachnička
dosedá	dosedat	k5eAaImIp3nS	dosedat
na	na	k7c4	na
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
lesnatých	lesnatý	k2eAgFnPc6d1	lesnatá
oblastech	oblast	k1gFnPc6	oblast
kolem	kolem	k7c2	kolem
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
i	i	k9	i
v	v	k7c6	v
zatopených	zatopený	k2eAgNnPc6d1	zatopené
rýžových	rýžový	k2eAgNnPc6d1	rýžové
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
mokřinách	mokřina	k1gFnPc6	mokřina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
aktivitu	aktivita	k1gFnSc4	aktivita
projevují	projevovat	k5eAaImIp3nP	projevovat
kachničky	kachnička	k1gFnPc1	kachnička
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
začínají	začínat	k5eAaImIp3nP	začínat
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
rostoucích	rostoucí	k2eAgInPc6d1	rostoucí
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
trvalá	trvalý	k2eAgNnPc1d1	trvalé
manželství	manželství	k1gNnPc1	manželství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
si	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
nového	nový	k2eAgMnSc4d1	nový
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
kachničky	kachnička	k1gFnPc4	kachnička
mandarínské	mandarínský	k2eAgFnPc4d1	mandarínská
převážně	převážně	k6eAd1	převážně
vegetariáni	vegetarián	k1gMnPc1	vegetarián
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
jídelníčku	jídelníček	k1gInSc6	jídelníček
i	i	k9	i
drobné	drobný	k2eAgMnPc4d1	drobný
živočichy	živočich	k1gMnPc4	živočich
–	–	k?	–
např.	např.	kA	např.
měkkýše	měkkýš	k1gMnPc4	měkkýš
či	či	k8xC	či
malé	malý	k2eAgFnPc4d1	malá
rybky	rybka	k1gFnPc4	rybka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
rýžových	rýžový	k2eAgFnPc6d1	rýžová
a	a	k8xC	a
obilných	obilný	k2eAgFnPc6d1	obilná
polích	pole	k1gFnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
si	se	k3xPyFc3	se
zpestřují	zpestřovat	k5eAaImIp3nP	zpestřovat
jídelníček	jídelníček	k1gInSc4	jídelníček
také	také	k9	také
bukvicemi	bukvice	k1gFnPc7	bukvice
<g/>
,	,	kIx,	,
žaludy	žalud	k1gInPc7	žalud
a	a	k8xC	a
kaštany	kaštan	k1gInPc7	kaštan
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
sbírá	sbírat	k5eAaImIp3nS	sbírat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
–	–	k?	–
čvachtáním	čvachtání	k1gNnSc7	čvachtání
<g/>
,	,	kIx,	,
panáčkováním	panáčkování	k1gNnSc7	panáčkování
a	a	k8xC	a
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nS	potápět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Námluvy	námluva	k1gFnSc2	námluva
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
složitých	složitý	k2eAgInPc6d1	složitý
zásnubních	zásnubní	k2eAgInPc6d1	zásnubní
tancích	tanec	k1gInPc6	tanec
vybírá	vybírat	k5eAaImIp3nS	vybírat
samice	samice	k1gFnSc1	samice
místo	místo	k6eAd1	místo
pro	pro	k7c4	pro
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
dutina	dutina	k1gFnSc1	dutina
ve	v	k7c6	v
stromě	strom	k1gInSc6	strom
nedaleko	nedaleko	k7c2	nedaleko
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sezení	sezení	k1gNnSc1	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
pak	pak	k6eAd1	pak
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
musí	muset	k5eAaImIp3nS	muset
mláďata	mládě	k1gNnPc4	mládě
kachničky	kachnička	k1gFnSc2	kachnička
vyskákat	vyskákat	k5eAaPmF	vyskákat
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
pak	pak	k6eAd1	pak
svoje	svůj	k3xOyFgNnSc4	svůj
potomstvo	potomstvo	k1gNnSc4	potomstvo
odvede	odvést	k5eAaPmIp3nS	odvést
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
si	se	k3xPyFc3	se
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
mladé	mladý	k2eAgFnPc1d1	mladá
kachničky	kachnička	k1gFnPc1	kachnička
stávají	stávat	k5eAaImIp3nP	stávat
zcela	zcela	k6eAd1	zcela
soběstačnými	soběstačný	k2eAgInPc7d1	soběstačný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
lov	lov	k1gInSc1	lov
kachničky	kachnička	k1gFnSc2	kachnička
mandarínské	mandarínský	k2eAgInPc4d1	mandarínský
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
původní	původní	k2eAgFnSc6d1	původní
vlasti	vlast	k1gFnSc6	vlast
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velké	velký	k2eAgNnSc1d1	velké
snižování	snižování	k1gNnSc1	snižování
počtů	počet	k1gInPc2	počet
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
světová	světový	k2eAgFnSc1d1	světová
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
7500	[number]	k4	7500
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
s	s	k7c7	s
ochranou	ochrana	k1gFnSc7	ochrana
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
stavy	stav	k1gInPc7	stav
opět	opět	k6eAd1	opět
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
<g/>
,	,	kIx,	,
trvale	trvale	k6eAd1	trvale
působícím	působící	k2eAgInSc7d1	působící
negativním	negativní	k2eAgInSc7d1	negativní
faktorem	faktor	k1gInSc7	faktor
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
změny	změna	k1gFnPc4	změna
vodních	vodní	k2eAgNnPc2d1	vodní
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
byla	být	k5eAaImAgFnS	být
vysazena	vysadit	k5eAaPmNgFnS	vysadit
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
až	až	k9	až
7000	[number]	k4	7000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
dokonce	dokonce	k9	dokonce
zahnízdil	zahnízdit	k5eAaPmAgInS	zahnízdit
pár	pár	k1gInSc1	pár
u	u	k7c2	u
Lednice	Lednice	k1gFnSc2	Lednice
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ptáky	pták	k1gMnPc4	pták
uniklé	uniklý	k2eAgMnPc4d1	uniklý
z	z	k7c2	z
chovů	chov	k1gInPc2	chov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
dříve	dříve	k6eAd2	dříve
darovávali	darovávat	k5eAaImAgMnP	darovávat
párek	párek	k1gInSc4	párek
kachniček	kachnička	k1gFnPc2	kachnička
mandarínských	mandarínský	k2eAgFnPc2d1	mandarínská
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
manželské	manželský	k2eAgFnSc2d1	manželská
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
plnit	plnit	k5eAaImF	plnit
kachničky	kachnička	k1gFnPc4	kachnička
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
dostávají	dostávat	k5eAaImIp3nP	dostávat
nevěsta	nevěsta	k1gFnSc1	nevěsta
a	a	k8xC	a
ženich	ženich	k1gMnSc1	ženich
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
jako	jako	k8xC	jako
dar	dar	k1gInSc4	dar
dvě	dva	k4xCgFnPc4	dva
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
kachničky	kachnička	k1gFnPc4	kachnička
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
přání	přání	k1gNnSc2	přání
šťastného	šťastný	k2eAgNnSc2d1	šťastné
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
kachničky	kachnička	k1gFnSc2	kachnička
mandarínské	mandarínský	k2eAgFnSc2d1	mandarínská
mají	mít	k5eAaImIp3nP	mít
plně	plně	k6eAd1	plně
funkčně	funkčně	k6eAd1	funkčně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
vaječník	vaječník	k1gMnSc1	vaječník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
u	u	k7c2	u
velmi	velmi	k6eAd1	velmi
starých	starý	k2eAgFnPc2d1	stará
samic	samice	k1gFnPc2	samice
zakrní	zakrnět	k5eAaPmIp3nS	zakrnět
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nedokonale	dokonale	k6eNd1	dokonale
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
vaječník	vaječník	k1gInSc4	vaječník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
na	na	k7c4	na
samčí	samčí	k2eAgFnSc4d1	samčí
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
žlázu	žláza	k1gFnSc4	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
neplodného	plodný	k2eNgMnSc4d1	neplodný
samce	samec	k1gMnSc4	samec
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
samčí	samčí	k2eAgNnSc4d1	samčí
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kachnička	kachnička	k1gFnSc1	kachnička
mandarínská	mandarínský	k2eAgFnSc1d1	mandarínská
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
druhem	druh	k1gMnSc7	druh
kachny	kachna	k1gFnSc2	kachna
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
křížit	křížit	k5eAaImF	křížit
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jejích	její	k3xOp3gInPc2	její
chromozomů	chromozom	k1gInPc2	chromozom
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
chromozomů	chromozom	k1gInPc2	chromozom
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
neobvyklé	obvyklý	k2eNgFnSc2d1	neobvyklá
krásy	krása	k1gFnSc2	krása
samců	samec	k1gMnPc2	samec
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
velmi	velmi	k6eAd1	velmi
hojně	hojně	k6eAd1	hojně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gMnSc4	on
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kachnička	kachnička	k1gFnSc1	kachnička
mandarinská	mandarinský	k2eAgFnSc1d1	mandarinská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kachnička	kachnička	k1gFnSc1	kachnička
mandarinská	mandarinský	k2eAgFnSc1d1	mandarinská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
