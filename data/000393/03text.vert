<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
je	on	k3xPp3gNnSc4	on
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
žánru	žánr	k1gInSc2	žánr
RPG	RPG	kA	RPG
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
firmou	firma	k1gFnSc7	firma
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
sklidila	sklidit	k5eAaPmAgFnS	sklidit
veliký	veliký	k2eAgInSc4d1	veliký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
datadisk	datadisk	k1gInSc1	datadisk
vydaný	vydaný	k2eAgInSc1d1	vydaný
společností	společnost	k1gFnSc7	společnost
Sierra	Sierra	k1gFnSc1	Sierra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Hellfire	Hellfir	k1gMnSc5	Hellfir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
pokračování	pokračování	k1gNnSc1	pokračování
Diablo	Diablo	k1gFnSc2	Diablo
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
pokračování	pokračování	k1gNnSc1	pokračování
Diablo	Diablo	k1gFnSc2	Diablo
III	III	kA	III
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
v	v	k7c6	v
2D	[number]	k4	2D
v	v	k7c6	v
izometrickém	izometrický	k2eAgInSc6d1	izometrický
náhledu	náhled	k1gInSc6	náhled
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
hráč	hráč	k1gMnSc1	hráč
pomocí	pomocí	k7c2	pomocí
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vesničce	vesnička	k1gFnSc6	vesnička
Tristram	Tristram	k1gInSc1	Tristram
v	v	k7c6	v
království	království	k1gNnSc6	království
Khandaras	Khandarasa	k1gFnPc2	Khandarasa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Leorica	Leoricus	k1gMnSc2	Leoricus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
sehrál	sehrát	k5eAaPmAgInS	sehrát
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
sám	sám	k3xTgMnSc1	sám
Diablo	Diablo	k1gMnSc1	Diablo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
království	království	k1gNnSc1	království
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Vesnička	vesnička	k1gFnSc1	vesnička
Tristram	Tristram	k1gInSc1	Tristram
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
Leoric	Leoric	k1gMnSc1	Leoric
sídlil	sídlit	k5eAaImAgMnS	sídlit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odříznuta	odříznut	k2eAgFnSc1d1	odříznuta
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
deset	deset	k4xCc4	deset
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
labyrintu	labyrint	k1gInSc6	labyrint
pod	pod	k7c7	pod
místní	místní	k2eAgFnSc7d1	místní
katedrálou	katedrála	k1gFnSc7	katedrála
sídlí	sídlet	k5eAaImIp3nS	sídlet
neznámé	známý	k2eNgNnSc4d1	neznámé
zlo	zlo	k1gNnSc4	zlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
začíná	začínat	k5eAaImIp3nS	začínat
hráč	hráč	k1gMnSc1	hráč
hru	hra	k1gFnSc4	hra
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
probít	probít	k5eAaPmF	probít
se	se	k3xPyFc4	se
labyrintem	labyrint	k1gInSc7	labyrint
až	až	k6eAd1	až
do	do	k7c2	do
nejspodnějšího	spodní	k2eAgNnSc2d3	nejspodnější
patra	patro	k1gNnSc2	patro
a	a	k8xC	a
neznámé	známý	k2eNgNnSc4d1	neznámé
zlo	zlo	k1gNnSc4	zlo
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
disponuje	disponovat	k5eAaBmIp3nS	disponovat
krásnou	krásný	k2eAgFnSc7d1	krásná
grafikou	grafika	k1gFnSc7	grafika
<g/>
,	,	kIx,	,
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
doby	doba	k1gFnSc2	doba
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozlišení	rozlišení	k1gNnSc6	rozlišení
640	[number]	k4	640
<g/>
×	×	k?	×
<g/>
480	[number]	k4	480
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
povedená	povedený	k2eAgFnSc1d1	povedená
je	být	k5eAaImIp3nS	být
i	i	k9	i
zvuková	zvukový	k2eAgFnSc1d1	zvuková
stránka	stránka	k1gFnSc1	stránka
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zvuky	zvuk	k1gInPc1	zvuk
a	a	k8xC	a
namluvení	namluvení	k1gNnSc1	namluvení
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
hráč	hráč	k1gMnSc1	hráč
zabíjením	zabíjení	k1gNnPc3	zabíjení
monster	monstrum	k1gNnPc2	monstrum
získává	získávat	k5eAaImIp3nS	získávat
zkušenostní	zkušenostní	k2eAgInPc4d1	zkušenostní
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
určitého	určitý	k2eAgNnSc2d1	určité
množství	množství	k1gNnSc2	množství
těchto	tento	k3xDgInPc2	tento
bodů	bod	k1gInPc2	bod
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
postoupí	postoupit	k5eAaPmIp3nS	postoupit
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
úroveň	úroveň	k1gFnSc4	úroveň
-	-	kIx~	-
level	level	k1gInSc1	level
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
odolnější	odolný	k2eAgInSc1d2	odolnější
a	a	k8xC	a
mocnější	mocný	k2eAgInSc1d2	mocnější
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tímto	tento	k3xDgInSc7	tento
získá	získat	k5eAaPmIp3nS	získat
pět	pět	k4xCc1	pět
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
rozdělit	rozdělit	k5eAaPmF	rozdělit
mezi	mezi	k7c4	mezi
čtyři	čtyři	k4xCgFnPc4	čtyři
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
,	,	kIx,	,
magii	magie	k1gFnSc4	magie
a	a	k8xC	a
vitalitu	vitalita	k1gFnSc4	vitalita
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hodnoty	hodnota	k1gFnPc1	hodnota
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgFnPc1d1	omezená
a	a	k8xC	a
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
postav	postava	k1gFnPc2	postava
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Hratelné	hratelný	k2eAgInPc1d1	hratelný
jsou	být	k5eAaImIp3nP	být
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc1	tři
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Válečník	válečník	k1gMnSc1	válečník
představuje	představovat	k5eAaImIp3nS	představovat
postavu	postava	k1gFnSc4	postava
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
boj	boj	k1gInSc4	boj
zblízka	zblízka	k6eAd1	zblízka
pomocí	pomocí	k7c2	pomocí
ručních	ruční	k2eAgFnPc2d1	ruční
sečných	sečný	k2eAgFnPc2d1	sečná
a	a	k8xC	a
tupých	tupý	k2eAgFnPc2d1	tupá
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
sekery	sekera	k1gFnPc1	sekera
<g/>
,	,	kIx,	,
palcáty	palcát	k1gInPc1	palcát
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
vitalitu	vitalita	k1gFnSc4	vitalita
a	a	k8xC	a
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontaktním	kontaktní	k2eAgNnSc6d1	kontaktní
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
slabá	slabý	k2eAgFnSc1d1	slabá
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
magii	magie	k1gFnSc6	magie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
kvůli	kvůli	k7c3	kvůli
slabé	slabý	k2eAgFnSc3d1	slabá
vlastnosti	vlastnost	k1gFnSc3	vlastnost
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
tak	tak	k9	tak
kvůli	kvůli	k7c3	kvůli
malému	malý	k2eAgNnSc3d1	malé
množství	množství	k1gNnSc3	množství
many	mana	k1gFnSc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Zlodějka	zlodějka	k1gFnSc1	zlodějka
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc1d1	univerzální
postava	postava	k1gFnSc1	postava
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
boj	boj	k1gInSc4	boj
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
pomocí	pomocí	k7c2	pomocí
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
univerzálnosti	univerzálnost	k1gFnSc3	univerzálnost
může	moct	k5eAaImIp3nS	moct
bojovat	bojovat	k5eAaImF	bojovat
i	i	k9	i
kontaktním	kontaktní	k2eAgInSc7d1	kontaktní
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
válečník	válečník	k1gMnSc1	válečník
nebo	nebo	k8xC	nebo
kouzlením	kouzlení	k1gNnSc7	kouzlení
jako	jako	k9	jako
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
těchto	tento	k3xDgFnPc6	tento
způsobech	způsob	k1gInPc6	způsob
slabší	slabý	k2eAgMnSc1d2	slabší
<g/>
,	,	kIx,	,
než	než	k8xS	než
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
postava	postava	k1gFnSc1	postava
válečníka	válečník	k1gMnSc2	válečník
nebo	nebo	k8xC	nebo
kouzelníka	kouzelník	k1gMnSc2	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
dovedností	dovednost	k1gFnSc7	dovednost
této	tento	k3xDgFnSc2	tento
postavy	postava	k1gFnSc2	postava
je	být	k5eAaImIp3nS	být
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
.	.	kIx.	.
</s>
<s>
Kouzelník	kouzelník	k1gMnSc1	kouzelník
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
boj	boj	k1gInSc4	boj
zdálky	zdálky	k6eAd1	zdálky
pomocí	pomocí	k7c2	pomocí
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
způsoby	způsob	k1gInPc1	způsob
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
kontaktními	kontaktní	k2eAgFnPc7d1	kontaktní
zbraněmi	zbraň	k1gFnPc7	zbraň
nebo	nebo	k8xC	nebo
lukem	luk	k1gInSc7	luk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
takřka	takřka	k6eAd1	takřka
nepoužitelné	použitelný	k2eNgInPc1d1	nepoužitelný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
pokročilý	pokročilý	k2eAgMnSc1d1	pokročilý
kouzelník	kouzelník	k1gMnSc1	kouzelník
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
silným	silný	k2eAgNnPc3d1	silné
kouzlům	kouzlo	k1gNnPc3	kouzlo
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
silnější	silný	k2eAgFnSc4d2	silnější
postavu	postava	k1gFnSc4	postava
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
válečník	válečník	k1gMnSc1	válečník
nebo	nebo	k8xC	nebo
zlodějka	zlodějka	k1gFnSc1	zlodějka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
dovedností	dovednost	k1gFnSc7	dovednost
této	tento	k3xDgFnSc2	tento
postavy	postava	k1gFnSc2	postava
je	být	k5eAaImIp3nS	být
magie	magie	k1gFnSc1	magie
<g/>
.	.	kIx.	.
</s>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
svým	svůj	k3xOyFgNnSc7	svůj
náhodným	náhodný	k2eAgNnSc7d1	náhodné
generováním	generování	k1gNnSc7	generování
herního	herní	k2eAgInSc2d1	herní
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
náhodné	náhodný	k2eAgNnSc1d1	náhodné
generování	generování	k1gNnSc1	generování
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
silnou	silný	k2eAgFnSc7d1	silná
stránkou	stránka	k1gFnSc7	stránka
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
výdrž	výdrž	k1gFnSc4	výdrž
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Náhodně	náhodně	k6eAd1	náhodně
generovány	generován	k2eAgInPc1d1	generován
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
levely	level	k1gInPc1	level
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
samotné	samotný	k2eAgFnSc2d1	samotná
vesničky	vesnička	k1gFnSc2	vesnička
Tristram	Tristram	k1gInSc1	Tristram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
náhodného	náhodný	k2eAgNnSc2d1	náhodné
generování	generování	k1gNnSc2	generování
levelů	level	k1gInPc2	level
je	být	k5eAaImIp3nS	být
i	i	k9	i
náhodně	náhodně	k6eAd1	náhodně
generováno	generován	k2eAgNnSc1d1	generováno
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
druhy	druh	k1gInPc1	druh
nepřátel	nepřítel	k1gMnPc2	nepřítel
se	se	k3xPyFc4	se
v	v	k7c6	v
levelu	level	k1gInSc6	level
objeví	objevit	k5eAaPmIp3nS	objevit
a	a	k8xC	a
protože	protože	k8xS	protože
samotné	samotný	k2eAgNnSc1d1	samotné
rozložení	rozložení	k1gNnSc1	rozložení
levelu	level	k1gInSc2	level
je	být	k5eAaImIp3nS	být
náhodné	náhodný	k2eAgNnSc1d1	náhodné
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
rozmístění	rozmístění	k1gNnSc1	rozmístění
nepřátel	nepřítel	k1gMnPc2	nepřítel
je	být	k5eAaImIp3nS	být
náhodně	náhodně	k6eAd1	náhodně
generováno	generován	k2eAgNnSc1d1	generováno
<g/>
.	.	kIx.	.
</s>
<s>
Náhodně	náhodně	k6eAd1	náhodně
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
generovány	generován	k2eAgInPc4d1	generován
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
brnění	brnění	k1gNnPc4	brnění
a	a	k8xC	a
šperky	šperk	k1gInPc4	šperk
<g/>
)	)	kIx)	)
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
existují	existovat	k5eAaImIp3nP	existovat
tisíce	tisíc	k4xCgInPc1	tisíc
různých	různý	k2eAgMnPc2d1	různý
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Předměty	předmět	k1gInPc1	předmět
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Diablu	Diabl	k1gInSc6	Diabl
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
náhodně	náhodně	k6eAd1	náhodně
generovány	generován	k2eAgFnPc1d1	generována
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
trojího	trojí	k4xRgMnSc2	trojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
obyčejné	obyčejný	k2eAgFnPc1d1	obyčejná
<g/>
,	,	kIx,	,
magické	magický	k2eAgFnPc1d1	magická
nebo	nebo	k8xC	nebo
unikátní	unikátní	k2eAgFnPc1d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejné	obyčejný	k2eAgFnPc1d1	obyčejná
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgFnPc4	žádný
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Magické	magický	k2eAgFnPc1d1	magická
věci	věc	k1gFnPc1	věc
mají	mít	k5eAaImIp3nP	mít
buď	buď	k8xC	buď
magickou	magický	k2eAgFnSc4d1	magická
předponu	předpona	k1gFnSc4	předpona
<g/>
,	,	kIx,	,
magickou	magický	k2eAgFnSc4d1	magická
příponu	přípona	k1gFnSc4	přípona
nebo	nebo	k8xC	nebo
obojí	obojí	k4xRgInPc1	obojí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
sword	sword	k1gInSc1	sword
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
předponu	předpona	k1gFnSc4	předpona
Massive	Massiev	k1gFnSc2	Massiev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
jím	on	k3xPp3gInSc7	on
způsobené	způsobený	k2eAgFnPc4d1	způsobená
zranění	zranění	k1gNnPc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
příponu	přípona	k1gFnSc4	přípona
of	of	k?	of
Haste	hasit	k5eAaImRp2nP	hasit
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
rychlost	rychlost	k1gFnSc4	rychlost
sekání	sekání	k1gNnSc2	sekání
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
obojí	oboj	k1gFnSc7	oboj
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Massive	Massiev	k1gFnSc2	Massiev
sword	sword	k6eAd1	sword
of	of	k?	of
Haste	hasit	k5eAaImRp2nP	hasit
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zlepšenou	zlepšený	k2eAgFnSc4d1	zlepšená
rychlost	rychlost	k1gFnSc4	rychlost
sekání	sekání	k1gNnSc2	sekání
i	i	k8xC	i
velikost	velikost	k1gFnSc4	velikost
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Magických	magický	k2eAgFnPc2d1	magická
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
jsou	být	k5eAaImIp3nP	být
řádově	řádově	k6eAd1	řádově
stovky	stovka	k1gFnPc1	stovka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
podle	podle	k7c2	podle
významu	význam	k1gInSc2	význam
od	od	k7c2	od
nejhorší	zlý	k2eAgFnSc2d3	nejhorší
po	po	k7c4	po
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
předpon	předpona	k1gFnPc2	předpona
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
zranění	zranění	k1gNnSc4	zranění
způsobené	způsobený	k2eAgNnSc4d1	způsobené
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
předpona	předpona	k1gFnSc1	předpona
Massive	Massiev	k1gFnSc2	Massiev
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
určitých	určitý	k2eAgFnPc6d1	určitá
zbraních	zbraň	k1gFnPc6	zbraň
(	(	kIx(	(
<g/>
sečných	sečný	k2eAgFnPc6d1	sečná
zbraních	zbraň	k1gFnPc6	zbraň
<g/>
,	,	kIx,	,
lucích	luk	k1gInPc6	luk
<g/>
,	,	kIx,	,
brnění	brnění	k1gNnSc6	brnění
<g/>
,	,	kIx,	,
špercích	šperk	k1gInPc6	šperk
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
druh	druh	k1gInSc1	druh
nepřátel	nepřítel	k1gMnPc2	nepřítel
může	moct	k5eAaImIp3nS	moct
danou	daný	k2eAgFnSc4d1	daná
věc	věc	k1gFnSc4	věc
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
upustit	upustit	k5eAaPmF	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
silnější	silný	k2eAgMnSc1d2	silnější
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lepší	dobrý	k2eAgFnSc1d2	lepší
magické	magický	k2eAgFnPc4d1	magická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgFnSc1d1	unikátní
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgFnPc1d1	stejná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
šest	šest	k4xCc4	šest
magických	magický	k2eAgFnPc2d1	magická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
brnění	brnění	k1gNnSc3	brnění
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zbraně	zbraň	k1gFnPc1	zbraň
jsou	být	k5eAaImIp3nP	být
buďto	buďto	k8xC	buďto
kontaktní	kontaktní	k2eAgNnSc1d1	kontaktní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktními	kontaktní	k2eAgFnPc7d1	kontaktní
zbraněmi	zbraň	k1gFnPc7	zbraň
jsou	být	k5eAaImIp3nP	být
meče	meč	k1gInPc4	meč
<g/>
,	,	kIx,	,
sekery	sekera	k1gFnPc4	sekera
<g/>
,	,	kIx,	,
palcáty	palcát	k1gInPc4	palcát
a	a	k8xC	a
kouzelnické	kouzelnický	k2eAgFnPc4d1	kouzelnická
hole	hole	k1gFnPc4	hole
<g/>
.	.	kIx.	.
</s>
<s>
Zbraněmi	zbraň	k1gFnPc7	zbraň
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
jsou	být	k5eAaImIp3nP	být
luky	luk	k1gInPc4	luk
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
zbraně	zbraň	k1gFnPc1	zbraň
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používat	k5eAaImNgInP	používat
všemi	všecek	k3xTgFnPc7	všecek
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obrázků	obrázek	k1gInPc2	obrázek
z	z	k7c2	z
pracovních	pracovní	k2eAgFnPc2d1	pracovní
verzí	verze	k1gFnPc2	verze
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
jako	jako	k8xS	jako
zbraně	zbraň	k1gFnPc1	zbraň
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
i	i	k8xC	i
kosy	kosa	k1gFnPc1	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
brnění	brnění	k1gNnSc2	brnění
spadá	spadat	k5eAaImIp3nS	spadat
klasické	klasický	k2eAgNnSc4d1	klasické
brnění	brnění	k1gNnSc4	brnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kryje	krýt	k5eAaImIp3nS	krýt
trup	trup	k1gInSc1	trup
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
látkové	látkový	k2eAgInPc1d1	látkový
(	(	kIx(	(
<g/>
kožené	kožený	k2eAgFnPc1d1	kožená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kroužkové	kroužkový	k2eAgFnPc1d1	kroužková
a	a	k8xC	a
plátové	plátový	k2eAgFnPc1d1	plátová
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
spadají	spadat	k5eAaPmIp3nP	spadat
helmy	helma	k1gFnPc4	helma
a	a	k8xC	a
štíty	štít	k1gInPc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
postava	postava	k1gFnSc1	postava
nese	nést	k5eAaImIp3nS	nést
dvouruční	dvouruční	k2eAgFnSc4d1	dvouruční
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
dvouruční	dvouruční	k2eAgInPc1d1	dvouruční
meče	meč	k1gInPc1	meč
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
sekery	sekera	k1gFnPc1	sekera
<g/>
,	,	kIx,	,
luky	luk	k1gInPc1	luk
a	a	k8xC	a
kouzelnické	kouzelnický	k2eAgFnPc1d1	kouzelnická
hole	hole	k1gFnPc1	hole
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nemůže	moct	k5eNaImIp3nS	moct
nést	nést	k5eAaImF	nést
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kategorií	kategorie	k1gFnSc7	kategorie
jsou	být	k5eAaImIp3nP	být
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
Diablu	Diabl	k1gInSc6	Diabl
znamená	znamenat	k5eAaImIp3nS	znamenat
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
amulety	amulet	k1gInPc4	amulet
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
může	moct	k5eAaImIp3nS	moct
nést	nést	k5eAaImF	nést
dva	dva	k4xCgInPc4	dva
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
amulet	amulet	k1gInSc4	amulet
<g/>
.	.	kIx.	.
</s>
<s>
Samy	Samos	k1gInPc1	Samos
o	o	k7c4	o
sobě	se	k3xPyFc3	se
nemají	mít	k5eNaImIp3nP	mít
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
zásadně	zásadně	k6eAd1	zásadně
magické	magický	k2eAgInPc1d1	magický
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nosiči	nosič	k1gInSc6	nosič
<g/>
"	"	kIx"	"
magických	magický	k2eAgFnPc2d1	magická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
v	v	k7c6	v
Diablu	Diablo	k1gNnSc6	Diablo
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
čeleď	čeleď	k1gFnSc1	čeleď
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
čeleď	čeleď	k1gFnSc1	čeleď
Zombie	Zombie	k1gFnSc2	Zombie
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
Zombie	Zombie	k1gFnPc4	Zombie
<g/>
,	,	kIx,	,
Ghoul	Ghoul	k1gInSc4	Ghoul
<g/>
,	,	kIx,	,
Rotting	Rotting	k1gInSc4	Rotting
Carcass	Carcassa	k1gFnPc2	Carcassa
a	a	k8xC	a
Black	Blacka	k1gFnPc2	Blacka
Death	Deatha	k1gFnPc2	Deatha
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čeledě	čeleď	k1gFnSc2	čeleď
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
nejen	nejen	k6eAd1	nejen
graficky	graficky	k6eAd1	graficky
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgNnSc4d1	jiné
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinou	jiný	k2eAgFnSc7d1	jiná
odolností	odolnost	k1gFnSc7	odolnost
a	a	k8xC	a
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc1	každý
druh	druh	k1gInSc1	druh
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
unikátního	unikátní	k2eAgMnSc4d1	unikátní
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
unikátní	unikátní	k2eAgNnSc4d1	unikátní
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obklopen	obklopit	k5eAaPmNgMnS	obklopit
skupinou	skupina	k1gFnSc7	skupina
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
třemi	tři	k4xCgMnPc7	tři
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nespadají	spadat	k5eNaImIp3nP	spadat
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
čeledi	čeleď	k1gFnSc2	čeleď
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nS	tvořit
čeleď	čeleď	k1gFnSc1	čeleď
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
druhu	druh	k1gInSc6	druh
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Butcher	Butchra	k1gFnPc2	Butchra
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
Leoric	Leoric	k1gMnSc1	Leoric
a	a	k8xC	a
Diablo	Diablo	k1gMnSc1	Diablo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
NPC	NPC	kA	NPC
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Ogden	Ogden	k1gInSc1	Ogden
-	-	kIx~	-
majitel	majitel	k1gMnSc1	majitel
krčmy	krčma	k1gFnSc2	krčma
U	u	k7c2	u
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
NPC	NPC	kA	NPC
pouze	pouze	k6eAd1	pouze
zadá	zadat	k5eAaPmIp3nS	zadat
quest	quest	k5eAaPmF	quest
<g/>
.	.	kIx.	.
</s>
<s>
Griswold	Griswold	k1gInSc1	Griswold
-	-	kIx~	-
místní	místní	k2eAgMnSc1d1	místní
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prodává	prodávat	k5eAaImIp3nS	prodávat
a	a	k8xC	a
kupuje	kupovat	k5eAaImIp3nS	kupovat
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
brnění	brnění	k1gNnSc4	brnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
také	také	k9	také
opravuje	opravovat	k5eAaImIp3nS	opravovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zadá	zadat	k5eAaPmIp3nS	zadat
quest	quest	k5eAaPmF	quest
<g/>
.	.	kIx.	.
</s>
<s>
Pepin	Pepina	k1gFnPc2	Pepina
-	-	kIx~	-
místní	místní	k2eAgMnSc1d1	místní
ranhojič	ranhojič	k1gMnSc1	ranhojič
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
léčivé	léčivý	k2eAgInPc4d1	léčivý
lektvary	lektvar	k1gInPc4	lektvar
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
zdarma	zdarma	k6eAd1	zdarma
uzdravení	uzdravení	k1gNnSc1	uzdravení
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
zadá	zadat	k5eAaPmIp3nS	zadat
hráči	hráč	k1gMnSc3	hráč
quest	quest	k5eAaPmF	quest
<g/>
.	.	kIx.	.
</s>
<s>
Adria	Adria	k1gFnSc1	Adria
-	-	kIx~	-
místní	místní	k2eAgFnSc1d1	místní
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
a	a	k8xC	a
kupuje	kupovat	k5eAaImIp3nS	kupovat
kouzelné	kouzelný	k2eAgFnPc4d1	kouzelná
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
lahvičky	lahvička	k1gFnPc4	lahvička
s	s	k7c7	s
manou	mana	k1gFnSc7	mana
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
magické	magický	k2eAgFnSc2d1	magická
propriety	proprieta	k1gFnSc2	proprieta
<g/>
.	.	kIx.	.
</s>
<s>
Wirt	Wirt	k1gMnSc1	Wirt
-	-	kIx~	-
hoch	hoch	k1gMnSc1	hoch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
vždy	vždy	k6eAd1	vždy
jednu	jeden	k4xCgFnSc4	jeden
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
věc	věc	k1gFnSc4	věc
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mu	on	k3xPp3gMnSc3	on
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vůbec	vůbec	k9	vůbec
ukázal	ukázat	k5eAaPmAgMnS	ukázat
co	co	k3yInSc4	co
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
magické	magický	k2eAgFnPc4d1	magická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
výhradně	výhradně	k6eAd1	výhradně
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Cain	Cain	k1gInSc1	Cain
-	-	kIx~	-
Celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Deckard	Deckard	k1gMnSc1	Deckard
Cain	Cain	k1gMnSc1	Cain
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
identifikování	identifikování	k1gNnSc1	identifikování
nalezených	nalezený	k2eAgInPc2d1	nalezený
magických	magický	k2eAgInPc2d1	magický
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
identifikace	identifikace	k1gFnSc2	identifikace
nejsou	být	k5eNaImIp3nP	být
magické	magický	k2eAgFnPc1d1	magická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
aktivní	aktivní	k2eAgFnPc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Gillian	Gillian	k1gInSc1	Gillian
-	-	kIx~	-
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Ogdenově	Ogdenův	k2eAgFnSc6d1	Ogdenův
krčmě	krčma	k1gFnSc6	krčma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
není	být	k5eNaImIp3nS	být
ničím	ničí	k3xOyNgNnSc7	ničí
podstatná	podstatný	k2eAgFnSc1d1	podstatná
<g/>
.	.	kIx.	.
</s>
<s>
Farnham	Farnham	k1gInSc1	Farnham
-	-	kIx~	-
místní	místní	k2eAgMnSc1d1	místní
opilec	opilec	k1gMnSc1	opilec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
podstatný	podstatný	k2eAgInSc1d1	podstatný
<g/>
.	.	kIx.	.
</s>
<s>
Umírající	umírající	k1gMnSc1	umírající
vesničan	vesničan	k1gMnSc1	vesničan
-	-	kIx~	-
Vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
multiplayeru	multiplayer	k1gInSc6	multiplayer
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
singleplayeru	singleplayer	k1gInSc6	singleplayer
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
umírající	umírající	k1gMnSc1	umírající
vesničan	vesničan	k1gMnSc1	vesničan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zadá	zadat	k5eAaPmIp3nS	zadat
hráči	hráč	k1gMnSc3	hráč
quest	quest	k5eAaPmF	quest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zadání	zadání	k1gNnSc6	zadání
questu	quest	k1gInSc2	quest
umře	umřít	k5eAaPmIp3nS	umřít
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dokončení	dokončení	k1gNnSc6	dokončení
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
Ogdenova	Ogdenův	k2eAgFnSc1d1	Ogdenův
žena	žena	k1gFnSc1	žena
Garda	garda	k1gFnSc1	garda
a	a	k8xC	a
Gillianina	Gillianin	k2eAgFnSc1d1	Gillianin
babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
postavy	postava	k1gFnPc1	postava
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
dozvíte	dozvědět	k5eAaPmIp2nP	dozvědět
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Posledními	poslední	k2eAgFnPc7d1	poslední
postavičkami	postavička	k1gFnPc7	postavička
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc1	tři
krávy	kráva	k1gFnPc1	kráva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
naprosto	naprosto	k6eAd1	naprosto
bezvýznamné	bezvýznamný	k2eAgFnPc1d1	bezvýznamná
<g/>
,	,	kIx,	,
daly	dát	k5eAaPmAgFnP	dát
však	však	k9	však
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
pověsti	pověst	k1gFnSc2	pověst
o	o	k7c6	o
tajném	tajný	k2eAgInSc6d1	tajný
kravím	kraví	k2eAgInSc6d1	kraví
levelu	level	k1gInSc6	level
v	v	k7c6	v
Diablu	Diabl	k1gInSc6	Diabl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pověst	pověst	k1gFnSc1	pověst
není	být	k5eNaImIp3nS	být
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
Diabla	Diabla	k1gFnSc2	Diabla
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
do	do	k7c2	do
vesničky	vesnička	k1gFnSc2	vesnička
Tristram	Tristram	k1gInSc1	Tristram
a	a	k8xC	a
do	do	k7c2	do
labyrintu	labyrint	k1gInSc2	labyrint
pod	pod	k7c7	pod
místní	místní	k2eAgFnSc7d1	místní
katedrálou	katedrála	k1gFnSc7	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Tristram	Tristram	k1gInSc1	Tristram
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nehrozí	hrozit	k5eNaImIp3nS	hrozit
žádná	žádný	k3yNgFnSc1	žádný
újma	újma	k1gFnSc1	újma
a	a	k8xC	a
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
NPC	NPC	kA	NPC
<g/>
.	.	kIx.	.
</s>
<s>
Level	level	k1gInSc4	level
vesničky	vesnička	k1gFnSc2	vesnička
Tristram	Tristram	k1gInSc1	Tristram
je	být	k5eAaImIp3nS	být
neměnný	neměnný	k2eAgInSc1d1	neměnný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
náhodný	náhodný	k2eAgInSc1d1	náhodný
systém	systém	k1gInSc1	systém
generování	generování	k1gNnSc2	generování
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
netýká	týkat	k5eNaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
labyrint	labyrint	k1gInSc1	labyrint
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
16	[number]	k4	16
úrovněmi	úroveň	k1gFnPc7	úroveň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gInPc7	on
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
katakomby	katakomby	k1gFnPc4	katakomby
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnPc4	jeskyně
a	a	k8xC	a
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
levelech	level	k1gInPc6	level
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
levely	level	k1gInPc1	level
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
schodišti	schodiště	k1gNnSc3	schodiště
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
levelu	level	k1gInSc6	level
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
schodiště	schodiště	k1gNnPc1	schodiště
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
do	do	k7c2	do
vyššího	vysoký	k2eAgInSc2d2	vyšší
levelu	level	k1gInSc2	level
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
do	do	k7c2	do
nižšího	nízký	k2eAgInSc2d2	nižší
levelu	level	k1gInSc2	level
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgInPc4	první
levely	level	k1gInPc4	level
katakomb	katakomby	k1gFnPc2	katakomby
<g/>
,	,	kIx,	,
jeskyň	jeskyně	k1gFnPc2	jeskyně
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
třetí	třetí	k4xOgNnSc4	třetí
schodiště	schodiště	k1gNnSc4	schodiště
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Tristramu	Tristram	k1gInSc2	Tristram
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
levely	level	k1gInPc7	level
není	být	k5eNaImIp3nS	být
plynulý	plynulý	k2eAgInSc1d1	plynulý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
nahráváním	nahrávání	k1gNnSc7	nahrávání
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
mezi	mezi	k7c7	mezi
levely	level	k1gInPc7	level
přecházet	přecházet	k5eAaImF	přecházet
nemohou	moct	k5eNaImIp3nP	moct
<g/>
.	.	kIx.	.
</s>
<s>
Grafika	grafika	k1gFnSc1	grafika
každého	každý	k3xTgInSc2	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
typů	typ	k1gInPc2	typ
je	být	k5eAaImIp3nS	být
zásadně	zásadně	k6eAd1	zásadně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
a	a	k8xC	a
odlišné	odlišný	k2eAgNnSc1d1	odlišné
je	být	k5eAaImIp3nS	být
i	i	k9	i
rozložení	rozložení	k1gNnSc1	rozložení
levelu	level	k1gInSc2	level
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
typů	typ	k1gInPc2	typ
levelů	level	k1gInPc2	level
si	se	k3xPyFc3	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
náhodného	náhodný	k2eAgNnSc2d1	náhodné
generování	generování	k1gNnSc2	generování
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
katakomby	katakomby	k1gFnPc1	katakomby
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgInPc1d1	tvořený
úzkými	úzký	k2eAgFnPc7d1	úzká
chodbami	chodba	k1gFnPc7	chodba
a	a	k8xC	a
malými	malý	k2eAgFnPc7d1	malá
místnostmi	místnost	k1gFnPc7	místnost
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc2	jeskyně
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgInPc1d1	tvořený
velkými	velký	k2eAgInPc7d1	velký
otevřenými	otevřený	k2eAgInPc7d1	otevřený
prostory	prostor	k1gInPc7	prostor
s	s	k7c7	s
lávovými	lávový	k2eAgNnPc7d1	lávové
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
Diablo	Diablo	k1gFnSc4	Diablo
složil	složit	k5eAaPmAgInS	složit
Matt	Matt	k2eAgInSc1d1	Matt
Uelmen	Uelmen	k1gInSc1	Uelmen
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
svojí	svůj	k3xOyFgFnSc7	svůj
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kvalitou	kvalita	k1gFnSc7	kvalita
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
atmosféře	atmosféra	k1gFnSc3	atmosféra
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
pět	pět	k4xCc4	pět
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
pro	pro	k7c4	pro
Tristram	Tristram	k1gInSc4	Tristram
a	a	k8xC	a
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
typů	typ	k1gInPc2	typ
levelů	level	k1gInPc2	level
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Diablo	Diablo	k1gFnSc4	Diablo
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jeden	jeden	k4xCgInSc1	jeden
datadisk	datadisk	k1gInSc1	datadisk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Hellfire	Hellfir	k1gInSc5	Hellfir
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
ho	on	k3xPp3gInSc4	on
společnost	společnost	k1gFnSc1	společnost
Sierra	Sierra	k1gFnSc1	Sierra
Entertainment	Entertainment	k1gInSc4	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Diablo	Diabnout	k5eAaPmAgNnS	Diabnout
má	mít	k5eAaImIp3nS	mít
jak	jak	k8xS	jak
singleplayer	singleplayer	k1gInSc1	singleplayer
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
multiplayer	multiplayer	k1gInSc1	multiplayer
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
Diabla	Diabla	k1gFnSc2	Diabla
byla	být	k5eAaImAgFnS	být
společností	společnost	k1gFnSc7	společnost
Blizzard	Blizzarda	k1gFnPc2	Blizzarda
představena	představit	k5eAaPmNgFnS	představit
herní	herní	k2eAgFnSc1d1	herní
služba	služba	k1gFnSc1	služba
Battle	Battle	k1gFnSc2	Battle
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
setkání	setkání	k1gNnSc3	setkání
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c4	na
chatu	chata	k1gFnSc4	chata
a	a	k8xC	a
zakládání	zakládání	k1gNnSc4	zakládání
připojování	připojování	k1gNnSc2	připojování
se	se	k3xPyFc4	se
do	do	k7c2	do
internetových	internetový	k2eAgFnPc2d1	internetová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
hraní	hraní	k1gNnSc2	hraní
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
Diablo	Diablo	k1gFnSc1	Diablo
také	také	k9	také
hru	hra	k1gFnSc4	hra
po	po	k7c6	po
modemu	modem	k1gInSc6	modem
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
LAN	lano	k1gNnPc2	lano
(	(	kIx(	(
<g/>
IPX	IPX	kA	IPX
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
sériový	sériový	k2eAgInSc4d1	sériový
port	port	k1gInSc4	port
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
multiplayerové	multiplayerový	k2eAgFnSc6d1	multiplayerová
hře	hra	k1gFnSc6	hra
můžou	můžou	k?	můžou
hrát	hrát	k5eAaImF	hrát
až	až	k9	až
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
hrána	hrát	k5eAaImNgFnS	hrát
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgMnSc7	jeden
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřením	zaměření	k1gNnSc7	zaměření
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
čistý	čistý	k2eAgInSc4d1	čistý
kooperační	kooperační	k2eAgInSc4d1	kooperační
mod	mod	k?	mod
<g/>
,	,	kIx,	,
multiplayer	multiplayer	k1gInSc1	multiplayer
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skupina	skupina	k1gFnSc1	skupina
postav	postava	k1gFnPc2	postava
vzájemně	vzájemně	k6eAd1	vzájemně
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
oproti	oproti	k7c3	oproti
singleplayeru	singleplayero	k1gNnSc3	singleplayero
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
pokročilosti	pokročilost	k1gFnSc6	pokročilost
postav	postava	k1gFnPc2	postava
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
libovolné	libovolný	k2eAgFnSc2d1	libovolná
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
katakomby	katakomby	k1gFnPc1	katakomby
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
peklo	peklo	k1gNnSc1	peklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
singleplayeru	singleplayer	k1gInSc6	singleplayer
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
postupovat	postupovat	k5eAaImF	postupovat
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Singleplayerová	Singleplayerový	k2eAgFnSc1d1	Singleplayerová
hra	hra	k1gFnSc1	hra
má	mít	k5eAaImIp3nS	mít
ukládání	ukládání	k1gNnSc4	ukládání
a	a	k8xC	a
porážkou	porážka	k1gFnSc7	porážka
Diabla	Diabla	k1gFnSc2	Diabla
"	"	kIx"	"
<g/>
končí	končit	k5eAaImIp3nS	končit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
postavou	postava	k1gFnSc7	postava
začít	začít	k5eAaPmF	začít
novou	nový	k2eAgFnSc4d1	nová
singleplayerovou	singleplayerový	k2eAgFnSc4d1	singleplayerová
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
začíná	začínat	k5eAaImIp3nS	začínat
opět	opět	k6eAd1	opět
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Multiplayerová	Multiplayerový	k2eAgFnSc1d1	Multiplayerová
hra	hra	k1gFnSc1	hra
nemá	mít	k5eNaImIp3nS	mít
ukládání	ukládání	k1gNnSc1	ukládání
a	a	k8xC	a
"	"	kIx"	"
<g/>
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
"	"	kIx"	"
vždy	vždy	k6eAd1	vždy
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
v	v	k7c6	v
multiplayeru	multiplayero	k1gNnSc6	multiplayero
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgFnPc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
multiplayeru	multiplayer	k1gInSc6	multiplayer
takřka	takřka	k6eAd1	takřka
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
questy	quest	k1gInPc4	quest
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
předměty	předmět	k1gInPc1	předmět
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
singleplayeru	singleplayer	k1gInSc6	singleplayer
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
multiplayerové	multiplayerový	k2eAgFnSc6d1	multiplayerová
hře	hra	k1gFnSc6	hra
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
libovolných	libovolný	k2eAgFnPc2d1	libovolná
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
čtyři	čtyři	k4xCgMnPc1	čtyři
válečníci	válečník	k1gMnPc1	válečník
atd.	atd.	kA	atd.
Všechny	všechen	k3xTgFnPc1	všechen
kombinace	kombinace	k1gFnPc1	kombinace
jsou	být	k5eAaImIp3nP	být
povolené	povolený	k2eAgFnPc1d1	povolená
<g/>
.	.	kIx.	.
</s>
<s>
Multiplayer	Multiplayer	k1gInSc1	Multiplayer
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
úspěchů	úspěch	k1gInPc2	úspěch
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
hrála	hrát	k5eAaImAgFnS	hrát
hlavně	hlavně	k9	hlavně
multiplayer	multiplayer	k1gInSc4	multiplayer
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
sólo	sólo	k1gNnSc1	sólo
(	(	kIx(	(
<g/>
multiplayer	multiplayer	k1gInSc1	multiplayer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
většiny	většina	k1gFnSc2	většina
hráčů	hráč	k1gMnPc2	hráč
k	k	k7c3	k
novějším	nový	k2eAgFnPc3d2	novější
hrám	hra	k1gFnPc3	hra
jsou	být	k5eAaImIp3nP	být
channely	channela	k1gFnPc1	channela
battle	battle	k1gFnPc1	battle
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
většinou	většina	k1gFnSc7	většina
prázdné	prázdná	k1gFnSc2	prázdná
na	na	k7c6	na
všech	všecek	k3xTgFnPc2	všecek
gateway	gatewaa	k1gFnSc2	gatewaa
<g/>
.	.	kIx.	.
</s>
<s>
Zbyla	zbýt	k5eAaPmAgFnS	zbýt
ovšem	ovšem	k9	ovšem
menší	malý	k2eAgFnSc1d2	menší
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
vztahy	vztah	k1gInPc4	vztah
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgFnPc7	všecek
gatewayemi	gatewaye	k1gFnPc7	gatewaye
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
E.U.	E.U.	k1gFnSc6	E.U.
Gateway	Gatewaa	k1gFnSc2	Gatewaa
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
channelu	channel	k1gInSc6	channel
"	"	kIx"	"
<g/>
diablo	diabnout	k5eAaPmAgNnS	diabnout
deu-	deu-	k?	deu-
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Komunita	komunita	k1gFnSc1	komunita
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
aktivní	aktivní	k2eAgInPc4d1	aktivní
weby	web	k1gInPc4	web
<g/>
:	:	kIx,	:
EU	EU	kA	EU
<g/>
:	:	kIx,	:
http://tristr.am/	[url]	k?	http://tristr.am/
US	US	kA	US
<g/>
:	:	kIx,	:
http://diablo-stronghold.webs.com/	[url]	k?	http://diablo-stronghold.webs.com/
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
pokračování	pokračování	k1gNnSc1	pokračování
Diabla	Diabla	k1gFnSc2	Diabla
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Diablo	Diablo	k1gMnPc1	Diablo
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
vydán	vydán	k2eAgInSc1d1	vydán
datadisk	datadisk	k1gInSc1	datadisk
Diablo	Diablo	k1gMnSc2	Diablo
II	II	kA	II
<g/>
:	:	kIx,	:
Lord	lord	k1gMnSc1	lord
of	of	k?	of
destruction	destruction	k1gInSc1	destruction
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Diablo	Diablo	k1gFnSc3	Diablo
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
