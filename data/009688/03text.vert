<p>
<s>
Romové	Rom	k1gMnPc1	Rom
(	(	kIx(	(
<g/>
exoetnonymum	exoetnonymum	k1gInSc4	exoetnonymum
Cikáni	cikán	k1gMnPc1	cikán
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
etnikum	etnikum	k1gNnSc4	etnikum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
středověké	středověký	k2eAgFnSc2d1	středověká
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Romské	romský	k2eAgNnSc1d1	romské
etnikum	etnikum	k1gNnSc1	etnikum
je	být	k5eAaImIp3nS	být
nejpočetnější	početní	k2eAgNnSc1d3	nejpočetnější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zejména	zejména	k9	zejména
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
Romů	Rom	k1gMnPc2	Rom
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
romistika	romistika	k1gFnSc1	romistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
Romů	Rom	k1gMnPc2	Rom
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
4	[number]	k4	4
až	až	k9	až
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
romské	romský	k2eAgFnPc1d1	romská
organizace	organizace	k1gFnPc1	organizace
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
až	až	k9	až
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
<g/>
Romové	Rom	k1gMnPc1	Rom
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
se	se	k3xPyFc4	se
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
etnika	etnikum	k1gNnSc2	etnikum
Domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
alespoň	alespoň	k9	alespoň
sdílejí	sdílet	k5eAaImIp3nP	sdílet
společnou	společný	k2eAgFnSc4d1	společná
historii	historie	k1gFnSc4	historie
<g/>
;	;	kIx,	;
předkové	předek	k1gMnPc1	předek
Romů	Rom	k1gMnPc2	Rom
i	i	k9	i
Domů	domů	k6eAd1	domů
odešli	odejít	k5eAaPmAgMnP	odejít
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
.	.	kIx.	.
<g/>
Romský	romský	k2eAgInSc4d1	romský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c6	na
několik	několik	k4yIc4	několik
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
podle	podle	k7c2	podle
publikace	publikace	k1gFnSc2	publikace
Ethnologue	Ethnologu	k1gFnSc2	Ethnologu
kolem	kolem	k7c2	kolem
3	[number]	k4	3
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Romů	Rom	k1gMnPc2	Rom
však	však	k9	však
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
považuje	považovat	k5eAaImIp3nS	považovat
jazyk	jazyk	k1gInSc1	jazyk
země	zem	k1gFnSc2	zem
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mluví	mluvit	k5eAaImIp3nS	mluvit
smíšeným	smíšený	k2eAgInSc7d1	smíšený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
romštiny	romština	k1gFnSc2	romština
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
těmto	tento	k3xDgFnPc3	tento
varietám	varieta	k1gFnPc3	varieta
se	se	k3xPyFc4	se
také	také	k6eAd1	také
říká	říkat	k5eAaImIp3nS	říkat
pararomština	pararomština	k1gFnSc1	pararomština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
bezbřehý	bezbřehý	k2eAgInSc1d1	bezbřehý
a	a	k8xC	a
nepřehledný	přehledný	k2eNgInSc1d1	nepřehledný
počet	počet	k1gInSc1	počet
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
lze	lze	k6eAd1	lze
Romy	Rom	k1gMnPc4	Rom
označovat	označovat	k5eAaImF	označovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poplatný	poplatný	k2eAgInSc1d1	poplatný
systému	systém	k1gInSc3	systém
kast	kasta	k1gFnPc2	kasta
a	a	k8xC	a
podkast	podkast	k1gFnSc4	podkast
<g/>
,	,	kIx,	,
zděděnému	zděděný	k2eAgInSc3d1	zděděný
z	z	k7c2	z
indické	indický	k2eAgFnSc2d1	indická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
a	a	k8xC	a
územnímu	územní	k2eAgInSc3d1	územní
pohybu	pohyb	k1gInSc3	pohyb
Romů	Rom	k1gMnPc2	Rom
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
názvů	název	k1gInPc2	název
jsou	být	k5eAaImIp3nP	být
endonyma	endonyma	k1gNnSc4	endonyma
a	a	k8xC	a
exonyma	exonyma	k1gNnSc4	exonyma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Endonyma	Endonymum	k1gNnSc2	Endonymum
===	===	k?	===
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
a	a	k8xC	a
zastřešující	zastřešující	k2eAgNnSc1d1	zastřešující
endonymum	endonymum	k1gNnSc1	endonymum
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Rom	Rom	k1gMnSc1	Rom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
evropské	evropský	k2eAgInPc1d1	evropský
dialekty	dialekt	k1gInPc1	dialekt
Romštiny	romština	k1gFnSc2	romština
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
rom	rom	k?	rom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pl.	pl.	k?	pl.
"	"	kIx"	"
<g/>
roma	roma	k1gMnSc1	roma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
adj.	adj.	k?	adj.
"	"	kIx"	"
<g/>
romani	roman	k1gMnPc1	roman
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgMnSc1d1	znamenající
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
manžel	manžel	k1gMnSc1	manžel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
romni	romn	k1gMnPc1	romn
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
žena	žena	k1gFnSc1	žena
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
manželka	manželka	k1gFnSc1	manželka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
endonymum	endonymum	k1gNnSc1	endonymum
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
modifikací	modifikace	k1gFnSc7	modifikace
indického	indický	k2eAgNnSc2d1	indické
kastovního	kastovní	k2eAgNnSc2d1	kastovní
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
dom	dom	k?	dom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc4	první
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
romský	romský	k2eAgInSc4d1	romský
sjezd	sjezd	k1gInSc4	sjezd
(	(	kIx(	(
<g/>
World	World	k1gInSc4	World
Romani	Romaň	k1gFnSc3	Romaň
Congress	Congress	k1gInSc1	Congress
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Romové	Rom	k1gMnPc1	Rom
ze	z	k7c2	z
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
používání	používání	k1gNnSc4	používání
slova	slovo	k1gNnSc2	slovo
Romové	Rom	k1gMnPc1	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
romský	romský	k2eAgInSc1d1	romský
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
majoritou	majorita	k1gFnSc7	majorita
respektován	respektován	k2eAgInSc4d1	respektován
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
skupiny	skupina	k1gFnPc1	skupina
Romů	Rom	k1gMnPc2	Rom
označují	označovat	k5eAaImIp3nP	označovat
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
"	"	kIx"	"
<g/>
Rom	Rom	k1gMnSc1	Rom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
společnému	společný	k2eAgInSc3d1	společný
původu	původ	k1gInSc3	původ
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
dichotomii	dichotomie	k1gFnSc4	dichotomie
Rom-gádžo	Romádžo	k1gNnSc1	Rom-gádžo
(	(	kIx(	(
<g/>
nerom	nerom	k1gInSc1	nerom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
skupinám	skupina	k1gFnPc3	skupina
používající	používající	k2eAgFnSc1d1	používající
jiná	jiný	k2eAgFnSc1d1	jiná
endonyma	endonyma	k1gFnSc1	endonyma
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sintové	Sint	k1gMnPc1	Sint
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sintové	Sint	k1gMnPc1	Sint
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nemluví	mluvit	k5eNaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
Romech	Rom	k1gMnPc6	Rom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
označují	označovat	k5eAaImIp3nP	označovat
"	"	kIx"	"
<g/>
romanes	romanes	k1gInSc1	romanes
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mluvit	mluvit	k5eAaImF	mluvit
romsky	romsky	k6eAd1	romsky
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manušové	Manuš	k1gMnPc1	Manuš
<g/>
,	,	kIx,	,
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Manušové	Manuš	k1gMnPc1	Manuš
jsou	být	k5eAaImIp3nP	být
podskupinou	podskupina	k1gFnSc7	podskupina
Sintů	Sint	k1gInPc2	Sint
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
manuš	manuš	k1gInSc1	manuš
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
<g/>
,	,	kIx,	,
novoindických	novoindický	k2eAgInPc6d1	novoindický
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
romštině	romština	k1gFnSc6	romština
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
manuša	manuša	k1gFnSc1	manuša
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Calé	Calé	k1gNnSc1	Calé
(	(	kIx(	(
<g/>
či	či	k8xC	či
Kale	kale	k6eAd1	kale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
Kale	kale	k6eAd1	kale
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
romštině	romština	k1gFnSc6	romština
"	"	kIx"	"
<g/>
černý	černý	k2eAgMnSc1d1	černý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
používají	používat	k5eAaImIp3nP	používat
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
kale	kale	k6eAd1	kale
<g/>
"	"	kIx"	"
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pararomštinu	pararomština	k1gFnSc4	pararomština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kalé	kalý	k2eAgFnPc1d1	kalá
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
;	;	kIx,	;
stejné	stejný	k2eAgNnSc4d1	stejné
endonymum	endonymum	k1gNnSc4	endonymum
se	s	k7c7	s
španělskými	španělský	k2eAgNnPc7d1	španělské
Calé	Calé	k1gNnPc7	Calé
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
náhodně	náhodně	k6eAd1	náhodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kalé	kalý	k2eAgFnPc1d1	kalá
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
</s>
</p>
<p>
<s>
Romanichalové	Romanichalová	k1gFnPc1	Romanichalová
(	(	kIx(	(
<g/>
čteno	číst	k5eAaImNgNnS	číst
"	"	kIx"	"
<g/>
Romaničelové	Romaničelový	k2eAgFnPc4d1	Romaničelový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
<g/>
Výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
menších	malý	k2eAgFnPc2d2	menší
podskupin	podskupina	k1gFnPc2	podskupina
a	a	k8xC	a
rodů	rod	k1gInPc2	rod
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
Olašští	olašský	k2eAgMnPc1d1	olašský
Romové	Rom	k1gMnPc1	Rom
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
romské	romský	k2eAgFnSc2d1	romská
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
Lováry	Lovár	k1gInPc4	Lovár
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
konští	konský	k2eAgMnPc1d1	konský
handlíři	handlíř	k1gMnPc1	handlíř
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kalderaše	Kalderaše	k1gFnSc1	Kalderaše
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
výrobci	výrobce	k1gMnPc1	výrobce
kotlů	kotel	k1gInPc2	kotel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Všechny	všechen	k3xTgFnPc4	všechen
uvedené	uvedený	k2eAgFnPc4d1	uvedená
skupiny	skupina	k1gFnPc4	skupina
si	se	k3xPyFc3	se
ale	ale	k9	ale
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
základní	základní	k2eAgFnSc2d1	základní
dichotomie	dichotomie	k1gFnSc2	dichotomie
vůči	vůči	k7c3	vůči
neromům	nerom	k1gMnPc3	nerom
(	(	kIx(	(
<g/>
gádžům	gádž	k1gInPc3	gádž
<g/>
)	)	kIx)	)
říkají	říkat	k5eAaImIp3nP	říkat
Romové	Rom	k1gMnPc1	Rom
<g/>
.	.	kIx.	.
<g/>
V	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
žije	žít	k5eAaImIp3nS	žít
též	též	k6eAd1	též
národní	národní	k2eAgFnSc1d1	národní
skupina	skupina	k1gFnSc1	skupina
Aškalijů	Aškalij	k1gMnPc2	Aškalij
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
někdy	někdy	k6eAd1	někdy
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
Albáncům	Albánec	k1gMnPc3	Albánec
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
k	k	k7c3	k
Romům	Rom	k1gMnPc3	Rom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
národnostní	národnostní	k2eAgFnSc4d1	národnostní
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
skupina	skupina	k1gFnSc1	skupina
Jeniše	Jeniše	k1gFnSc2	Jeniše
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
zařazovaná	zařazovaný	k2eAgFnSc1d1	zařazovaná
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
cikánským	cikánský	k2eAgNnSc7d1	cikánské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
mnoho	mnoho	k6eAd1	mnoho
společných	společný	k2eAgFnPc2d1	společná
historických	historický	k2eAgFnPc2d1	historická
a	a	k8xC	a
sociologických	sociologický	k2eAgFnPc2d1	sociologická
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
národní	národní	k2eAgFnSc4d1	národní
skupinu	skupina	k1gFnSc4	skupina
-	-	kIx~	-
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
úředně	úředně	k6eAd1	úředně
uznána	uznán	k2eAgFnSc1d1	uznána
jen	jen	k9	jen
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Rom	Rom	k1gMnSc1	Rom
<g/>
"	"	kIx"	"
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Svazu	svaz	k1gInSc2	svaz
Cikánů-Romů	Cikánů-Rom	k1gInPc2	Cikánů-Rom
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
existoval	existovat	k5eAaImAgMnS	existovat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
díky	díky	k7c3	díky
společenskému	společenský	k2eAgNnSc3d1	společenské
uvolnění	uvolnění	k1gNnSc3	uvolnění
během	během	k7c2	během
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
;	;	kIx,	;
během	během	k7c2	během
normalizace	normalizace	k1gFnSc2	normalizace
byl	být	k5eAaImAgInS	být
svaz	svaz	k1gInSc1	svaz
opět	opět	k6eAd1	opět
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
endonymum	endonymum	k1gInSc1	endonymum
Rom	Rom	k1gMnSc1	Rom
vystřídáno	vystřídat	k5eAaPmNgNnS	vystřídat
"	"	kIx"	"
<g/>
občanem	občan	k1gMnSc7	občan
cikánského	cikánský	k2eAgInSc2d1	cikánský
původu	původ	k1gInSc2	původ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Exonyma	Exonymum	k1gNnSc2	Exonymum
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
běžné	běžný	k2eAgNnSc1d1	běžné
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Cikán	cikán	k2eAgMnSc1d1	cikán
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
athinganoi	athinganoi	k6eAd1	athinganoi
<g/>
"	"	kIx"	"
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
označovalo	označovat	k5eAaImAgNnS	označovat
buď	buď	k8xC	buď
separátní	separátní	k2eAgFnSc4d1	separátní
heretickou	heretický	k2eAgFnSc4d1	heretická
sektu	sekta	k1gFnSc4	sekta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
samotné	samotný	k2eAgMnPc4d1	samotný
Romy	Rom	k1gMnPc4	Rom
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
Cikáni	cikán	k1gMnPc1	cikán
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
Romové	Rom	k1gMnPc1	Rom
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
neztotožnili	ztotožnit	k5eNaPmAgMnP	ztotožnit
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
televizi	televize	k1gFnSc6	televize
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
Romské	romský	k2eAgFnSc2d1	romská
občanské	občanský	k2eAgFnSc2d1	občanská
iniciativy	iniciativa	k1gFnSc2	iniciativa
(	(	kIx(	(
<g/>
ROI	ROI	kA	ROI
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Emil	Emil	k1gMnSc1	Emil
Ščuka	Ščuka	k1gMnSc1	Ščuka
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cikáni	cikán	k1gMnPc1	cikán
je	on	k3xPp3gNnSc4	on
označení	označení	k1gNnSc4	označení
deklasované	deklasovaný	k2eAgFnSc2d1	deklasovaná
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Romové	Rom	k1gMnPc1	Rom
je	on	k3xPp3gNnSc4	on
označení	označení	k1gNnSc4	označení
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc4	původ
a	a	k8xC	a
počátek	počátek	k1gInSc4	počátek
kočování	kočování	k1gNnSc2	kočování
===	===	k?	===
</s>
</p>
<p>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
romština	romština	k1gFnSc1	romština
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ze	z	k7c2	z
sanskrtu	sanskrt	k1gInSc2	sanskrt
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
indické	indický	k2eAgFnSc3d1	indická
skupině	skupina	k1gFnSc3	skupina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
lingvistiky	lingvistika	k1gFnSc2	lingvistika
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
však	však	k9	však
dospělo	dochvít	k5eAaPmAgNnS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
Romů	Rom	k1gMnPc2	Rom
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
dialektu	dialekt	k1gInSc2	dialekt
staré	starý	k2eAgFnSc2d1	stará
hindštiny	hindština	k1gFnSc2	hindština
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vlast	vlast	k1gFnSc4	vlast
Romů	Rom	k1gMnPc2	Rom
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
považuje	považovat	k5eAaImIp3nS	považovat
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
jejich	jejich	k3xOp3gInSc2	jejich
odchodu	odchod	k1gInSc2	odchod
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
nejistý	jistý	k2eNgInSc1d1	nejistý
(	(	kIx(	(
<g/>
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
hladomor	hladomor	k1gInSc4	hladomor
<g/>
,	,	kIx,	,
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
invaze	invaze	k1gFnPc1	invaze
<g/>
,	,	kIx,	,
pronikající	pronikající	k2eAgInSc1d1	pronikající
islám	islám	k1gInSc1	islám
nebo	nebo	k8xC	nebo
nízký	nízký	k2eAgInSc1d1	nízký
sociální	sociální	k2eAgInSc1d1	sociální
status	status	k1gInSc1	status
tzv.	tzv.	kA	tzv.
nedotknutelných	nedotknutelných	k?	nedotknutelných
<g/>
,	,	kIx,	,
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
kastovní	kastovní	k2eAgNnSc4d1	kastovní
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zvané	zvaný	k2eAgNnSc1d1	zvané
varnášrama	varnášrama	k1gNnSc1	varnášrama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
se	se	k3xPyFc4	se
jisté	jistý	k2eAgFnSc3d1	jistá
skupině	skupina	k1gFnSc3	skupina
Romů	Rom	k1gMnPc2	Rom
dostalo	dostat	k5eAaPmAgNnS	dostat
pozvání	pozvání	k1gNnSc4	pozvání
od	od	k7c2	od
perského	perský	k2eAgMnSc2d1	perský
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
hudebníky	hudebník	k1gMnPc4	hudebník
k	k	k7c3	k
obveselení	obveselení	k1gNnSc3	obveselení
svých	svůj	k3xOyFgMnPc2	svůj
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
nejdřív	dříve	k6eAd3	dříve
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získali	získat	k5eAaPmAgMnP	získat
označení	označený	k2eAgMnPc1d1	označený
Cikáni	cikán	k1gMnPc1	cikán
podle	podle	k7c2	podle
řeckého	řecký	k2eAgNnSc2d1	řecké
Athiganoi	Athiganoi	k1gNnSc2	Athiganoi
(	(	kIx(	(
<g/>
maloasijská	maloasijský	k2eAgFnSc1d1	maloasijská
sekta	sekta	k1gFnSc1	sekta
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
věštěním	věštění	k1gNnSc7	věštění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kočující	kočující	k2eAgMnPc1d1	kočující
Romové	Rom	k1gMnPc1	Rom
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Romům	Rom	k1gMnPc3	Rom
<g/>
,	,	kIx,	,
Sintiům	Sintius	k1gMnPc3	Sintius
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
všem	všecek	k3xTgFnPc3	všecek
osobám	osoba	k1gFnPc3	osoba
kočujícím	kočující	k2eAgFnPc3d1	kočující
po	po	k7c6	po
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
náleželo	náležet	k5eAaImAgNnS	náležet
jedno	jeden	k4xCgNnSc1	jeden
společné	společný	k2eAgNnSc1d1	společné
označení	označení	k1gNnSc1	označení
–	–	k?	–
cikáni	cikán	k2eAgMnPc1d1	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Kočovníkům	kočovník	k1gMnPc3	kočovník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
Romové	Rom	k1gMnPc1	Rom
a	a	k8xC	a
Sintiové	Sintius	k1gMnPc1	Sintius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říkalo	říkat	k5eAaImAgNnS	říkat
také	také	k9	také
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
,	,	kIx,	,
pohani	pohan	k1gMnPc1	pohan
nebo	nebo	k8xC	nebo
Egypťani	Egypťan	k1gMnPc1	Egypťan
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
výraz	výraz	k1gInSc1	výraz
gypsies	gypsiesa	k1gFnPc2	gypsiesa
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
Malého	Malého	k2eAgInSc2d1	Malého
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
Byzanci	Byzanc	k1gFnSc6	Byzanc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
st.	st.	kA	st.
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
1425	[number]	k4	1425
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
jsou	být	k5eAaImIp3nP	být
Cikáni	cikán	k1gMnPc1	cikán
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
přijímáni	přijímat	k5eAaImNgMnP	přijímat
a	a	k8xC	a
často	často	k6eAd1	často
získávají	získávat	k5eAaImIp3nP	získávat
od	od	k7c2	od
panovníků	panovník	k1gMnPc2	panovník
ochranné	ochranný	k2eAgFnSc2d1	ochranná
listy	lista	k1gFnSc2	lista
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
poutníky	poutník	k1gMnPc4	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
územích	území	k1gNnPc6	území
tak	tak	k6eAd1	tak
dostávají	dostávat	k5eAaImIp3nP	dostávat
průvodní	průvodní	k2eAgInPc4d1	průvodní
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgInPc4	který
odvádějí	odvádět	k5eAaImIp3nP	odvádět
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
však	však	k9	však
natolik	natolik	k6eAd1	natolik
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
života	život	k1gInSc2	život
usedlého	usedlý	k2eAgNnSc2d1	usedlé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
že	že	k8xS	že
záhy	záhy	k6eAd1	záhy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
kočovníky	kočovník	k1gMnPc7	kočovník
a	a	k8xC	a
starousedlíky	starousedlík	k1gMnPc7	starousedlík
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
lichotivých	lichotivý	k2eAgInPc2d1	lichotivý
líčení	líčení	k1gNnSc4	líčení
Cikánů	cikán	k1gMnPc2	cikán
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
pochází	pocházet	k5eAaImIp3nS	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1417	[number]	k4	1417
od	od	k7c2	od
dominikánského	dominikánský	k2eAgMnSc2d1	dominikánský
mnicha	mnich	k1gMnSc2	mnich
Hermanna	Hermann	k1gMnSc2	Hermann
Kornera	Korner	k1gMnSc2	Korner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Chronicon	Chronicon	k1gMnSc1	Chronicon
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
ošklivého	ošklivý	k2eAgInSc2d1	ošklivý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
černí	černý	k1gMnPc1	černý
jako	jako	k8xC	jako
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Putovali	putovat	k5eAaImAgMnP	putovat
v	v	k7c6	v
kolonách	kolona	k1gFnPc6	kolona
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
nocovali	nocovat	k5eAaImAgMnP	nocovat
za	za	k7c7	za
městem	město	k1gNnSc7	město
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
oddávali	oddávat	k5eAaImAgMnP	oddávat
zlodějství	zlodějství	k1gNnSc4	zlodějství
a	a	k8xC	a
báli	bát	k5eAaImAgMnP	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
napadli	napadnout	k5eAaPmAgMnP	napadnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stejně	stejně	k6eAd1	stejně
nelichotivě	lichotivě	k6eNd1	lichotivě
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
i	i	k9	i
Etienne	Etienn	k1gInSc5	Etienn
Pasquier	Pasquier	k1gMnSc1	Pasquier
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podobně	podobně	k6eAd1	podobně
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
příchod	příchod	k1gInSc4	příchod
Cikánů	cikán	k1gMnPc2	cikán
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1427	[number]	k4	1427
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
ustálil	ustálit	k5eAaPmAgInS	ustálit
v	v	k7c6	v
písemných	písemný	k2eAgInPc6d1	písemný
záznamech	záznam	k1gInPc6	záznam
neměnný	měnný	k2eNgInSc1d1	neměnný
topos	topos	k1gInSc1	topos
kočovných	kočovný	k2eAgMnPc2d1	kočovný
Cikánů	cikán	k1gMnPc2	cikán
<g/>
:	:	kIx,	:
Cikáni	cikán	k1gMnPc1	cikán
jsou	být	k5eAaImIp3nP	být
oškliví	ošklivý	k2eAgMnPc1d1	ošklivý
<g/>
,	,	kIx,	,
oddávají	oddávat	k5eAaImIp3nP	oddávat
se	se	k3xPyFc4	se
nicnedělání	nicnedělání	k1gNnPc1	nicnedělání
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
z	z	k7c2	z
krádeží	krádež	k1gFnPc2	krádež
<g/>
,	,	kIx,	,
táboří	tábořit	k5eAaImIp3nS	tábořit
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
vyzvědači	vyzvědač	k1gMnPc1	vyzvědač
Turků	Turek	k1gMnPc2	Turek
a	a	k8xC	a
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
,	,	kIx,	,
žebrají	žebrat	k5eAaImIp3nP	žebrat
<g/>
,	,	kIx,	,
předpovídají	předpovídat	k5eAaImIp3nP	předpovídat
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
ovládají	ovládat	k5eAaImIp3nP	ovládat
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgNnSc4	žádný
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Cikáni	cikán	k1gMnPc1	cikán
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vnímáni	vnímán	k2eAgMnPc1d1	vnímán
jako	jako	k8xC	jako
cizí	cizí	k2eAgMnPc1d1	cizí
<g/>
,	,	kIx,	,
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
a	a	k8xC	a
nepočestní	počestný	k2eNgMnPc1d1	počestný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
distancovali	distancovat	k5eAaBmAgMnP	distancovat
od	od	k7c2	od
usedlých	usedlý	k2eAgMnPc2d1	usedlý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zemích	zem	k1gFnPc6	zem
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
vydávána	vydáván	k2eAgNnPc4d1	vydáváno
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Cikáni	cikán	k1gMnPc1	cikán
byli	být	k5eAaImAgMnP	být
vykázáni	vykázán	k2eAgMnPc1d1	vykázán
roku	rok	k1gInSc2	rok
1493	[number]	k4	1493
z	z	k7c2	z
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
z	z	k7c2	z
Lindau	Lindaus	k1gInSc2	Lindaus
a	a	k8xC	a
Freiburgu	Freiburg	k1gInSc2	Freiburg
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1497	[number]	k4	1497
a	a	k8xC	a
1498	[number]	k4	1498
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
roku	rok	k1gInSc2	rok
1499	[number]	k4	1499
kočovníky	kočovník	k1gMnPc7	kočovník
vyzvalo	vyzvat	k5eAaPmAgNnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
usadili	usadit	k5eAaPmAgMnP	usadit
nebo	nebo	k8xC	nebo
do	do	k7c2	do
60	[number]	k4	60
dnů	den	k1gInPc2	den
zemi	zem	k1gFnSc6	zem
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
je	on	k3xPp3gNnSc4	on
vykázala	vykázat	k5eAaPmAgFnS	vykázat
roku	rok	k1gInSc2	rok
1510	[number]	k4	1510
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
1530	[number]	k4	1530
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
1536	[number]	k4	1536
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
povolil	povolit	k5eAaPmAgInS	povolit
pobyt	pobyt	k1gInSc1	pobyt
jen	jen	k9	jen
Cikánům	cikán	k1gMnPc3	cikán
počestným	počestný	k2eAgMnPc3d1	počestný
a	a	k8xC	a
netrestaným	trestaný	k2eNgMnSc7d1	netrestaný
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
Cikánů	cikán	k1gMnPc2	cikán
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
pronásledování	pronásledování	k1gNnSc4	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Vyvraždění	vyvraždění	k1gNnSc1	vyvraždění
údajných	údajný	k2eAgFnPc2d1	údajná
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
mělo	mít	k5eAaImAgNnS	mít
zamezit	zamezit	k5eAaPmF	zamezit
používání	používání	k1gNnSc2	používání
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vyhubení	vyhubení	k1gNnSc1	vyhubení
vlků	vlk	k1gMnPc2	vlk
a	a	k8xC	a
Cikánů	cikán	k1gMnPc2	cikán
bylo	být	k5eAaImAgNnS	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k9	jako
krocení	krocení	k1gNnSc1	krocení
a	a	k8xC	a
přemáhání	přemáhání	k1gNnSc1	přemáhání
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
Cikáni	cikán	k1gMnPc1	cikán
byli	být	k5eAaImAgMnP	být
navíc	navíc	k6eAd1	navíc
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
hospodářské	hospodářský	k2eAgMnPc4d1	hospodářský
škůdce	škůdce	k1gMnPc4	škůdce
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	s	k7c7	s
drastickou	drastický	k2eAgFnSc7d1	drastická
perzekucí	perzekuce	k1gFnSc7	perzekuce
Cikánů	cikán	k1gMnPc2	cikán
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
zejména	zejména	k9	zejména
vlády	vláda	k1gFnPc1	vláda
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kočující	kočující	k2eAgMnPc1d1	kočující
Romové	Rom	k1gMnPc1	Rom
v	v	k7c6	v
novověké	novověký	k2eAgFnSc6d1	novověká
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
byli	být	k5eAaImAgMnP	být
kočující	kočující	k2eAgMnPc1d1	kočující
Cikáni	cikán	k1gMnPc1	cikán
většinovou	většinový	k2eAgFnSc4d1	většinová
společností	společnost	k1gFnPc2	společnost
vnímáni	vnímán	k2eAgMnPc1d1	vnímán
jako	jako	k8xC	jako
protipól	protipól	k1gInSc4	protipól
pevného	pevný	k2eAgInSc2d1	pevný
pořádku	pořádek	k1gInSc2	pořádek
a	a	k8xC	a
nařízení	nařízení	k1gNnSc2	nařízení
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zostřovala	zostřovat	k5eAaImAgFnS	zostřovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
se	se	k3xPyFc4	se
např.	např.	kA	např.
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nařizovalo	nařizovat	k5eAaImAgNnS	nařizovat
každého	každý	k3xTgMnSc4	každý
Cikána	cikán	k1gMnSc4	cikán
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
popravit	popravit	k5eAaPmF	popravit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
mu	on	k3xPp3gNnSc3	on
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
nějaký	nějaký	k3yIgInSc1	nějaký
trestný	trestný	k2eAgInSc1d1	trestný
čin	čin	k1gInSc1	čin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
také	také	k9	také
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
násilnému	násilný	k2eAgNnSc3d1	násilné
odebírání	odebírání	k1gNnSc3	odebírání
cikánských	cikánský	k2eAgFnPc2d1	cikánská
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc3	jejich
pokřtění	pokřtění	k1gNnSc3	pokřtění
a	a	k8xC	a
převýchově	převýchova	k1gFnSc3	převýchova
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledováno	pronásledován	k2eAgNnSc1d1	pronásledováno
a	a	k8xC	a
pokutováno	pokutován	k2eAgNnSc1d1	pokutováno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
usedlé	usedlý	k2eAgNnSc1d1	usedlé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
občas	občas	k6eAd1	občas
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
kočovníkům	kočovník	k1gMnPc3	kočovník
přístřeší	přístřeší	k1gNnSc2	přístřeší
a	a	k8xC	a
nocleh	nocleh	k1gInSc1	nocleh
<g/>
.	.	kIx.	.
</s>
<s>
Cikáni	cikán	k1gMnPc1	cikán
byli	být	k5eAaImAgMnP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zaháněni	zahánět	k5eAaImNgMnP	zahánět
do	do	k7c2	do
ilegality	ilegalita	k1gFnSc2	ilegalita
a	a	k8xC	a
upadali	upadat	k5eAaPmAgMnP	upadat
nadále	nadále	k6eAd1	nadále
do	do	k7c2	do
bludného	bludný	k2eAgInSc2d1	bludný
kruhu	kruh	k1gInSc2	kruh
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
kriminalizace	kriminalizace	k1gFnSc2	kriminalizace
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
se	se	k3xPyFc4	se
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
promyšleně	promyšleně	k6eAd1	promyšleně
pohybovat	pohybovat	k5eAaImF	pohybovat
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
teritoriálních	teritoriální	k2eAgNnPc2d1	teritoriální
území	území	k1gNnPc2	území
a	a	k8xC	a
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
státě	stát	k1gInSc6	stát
prchat	prchat	k5eAaImF	prchat
rychle	rychle	k6eAd1	rychle
na	na	k7c6	na
území	území	k1gNnSc6	území
jiného	jiný	k2eAgMnSc2d1	jiný
monarchy	monarcha	k1gMnSc2	monarcha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
zase	zase	k9	zase
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byli	být	k5eAaImAgMnP	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
<g/>
,	,	kIx,	,
hrozila	hrozit	k5eAaImAgFnS	hrozit
jim	on	k3xPp3gMnPc3	on
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
poprava	poprava	k1gFnSc1	poprava
nebo	nebo	k8xC	nebo
káznice	káznice	k1gFnSc1	káznice
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
odsuzováni	odsuzovat	k5eAaImNgMnP	odsuzovat
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
razii	razie	k1gFnSc6	razie
zadrženo	zadržet	k5eAaPmNgNnS	zadržet
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
000	[number]	k4	000
až	až	k9	až
12	[number]	k4	12
000	[number]	k4	000
Cikánů	cikán	k1gMnPc2	cikán
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
námořním	námořní	k2eAgInSc6d1	námořní
arzenálu	arzenál	k1gInSc6	arzenál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
také	také	k9	také
činěny	činěn	k2eAgInPc1d1	činěn
většinou	většinou	k6eAd1	většinou
neúspěšné	úspěšný	k2eNgInPc1d1	neúspěšný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
integraci	integrace	k1gFnSc6	integrace
kočujícího	kočující	k2eAgNnSc2d1	kočující
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
usazení	usazení	k1gNnSc4	usazení
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Wittgensteinský	Wittgensteinský	k2eAgMnSc1d1	Wittgensteinský
hrabě	hrabě	k1gMnSc1	hrabě
takovou	takový	k3xDgFnSc4	takový
nabídku	nabídka	k1gFnSc4	nabídka
učinil	učinit	k5eAaImAgMnS	učinit
Cikánům	cikán	k1gMnPc3	cikán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
<g/>
,	,	kIx,	,
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
přijmou	přijmout	k5eAaPmIp3nP	přijmout
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
důstojnost	důstojnost	k1gFnSc4	důstojnost
a	a	k8xC	a
občanské	občanský	k2eAgNnSc4d1	občanské
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
odloží	odložit	k5eAaPmIp3nS	odložit
cikánské	cikánský	k2eAgInPc4d1	cikánský
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
docela	docela	k6eAd1	docela
se	se	k3xPyFc4	se
zřeknou	zřeknout	k5eAaPmIp3nP	zřeknout
veškerého	veškerý	k3xTgNnSc2	veškerý
muzicírování	muzicírování	k?	muzicírování
<g/>
,	,	kIx,	,
potulky	potulka	k1gFnSc2	potulka
<g/>
,	,	kIx,	,
žebroty	žebrota	k1gFnSc2	žebrota
a	a	k8xC	a
krádeží	krádež	k1gFnPc2	krádež
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
všeho	všecek	k3xTgNnSc2	všecek
obcování	obcování	k1gNnSc2	obcování
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
Cikány	cikán	k1gMnPc7	cikán
a	a	k8xC	a
podobnou	podobný	k2eAgFnSc7d1	podobná
verbeží	verbež	k1gFnSc7	verbež
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dobré	dobrý	k2eAgFnPc4d1	dobrá
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
Cikáni	cikán	k1gMnPc1	cikán
nalezli	nalézt	k5eAaBmAgMnP	nalézt
také	také	k9	také
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
hesenských	hesenský	k2eAgNnPc6d1	hesenské
hrabstvích	hrabství	k1gNnPc6	hrabství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
trpěni	trpěn	k2eAgMnPc1d1	trpěn
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
mohli	moct	k5eAaImAgMnP	moct
usazovat	usazovat	k5eAaImF	usazovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
příznivá	příznivý	k2eAgFnSc1d1	příznivá
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
např.	např.	kA	např.
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
Šlesvicku-Holštýnsku	Šlesvicku-Holštýnsko	k1gNnSc6	Šlesvicku-Holštýnsko
a	a	k8xC	a
u	u	k7c2	u
Lübecku	Lübeck	k1gInSc2	Lübeck
<g/>
.	.	kIx.	.
</s>
<s>
Kočovníci	kočovník	k1gMnPc1	kočovník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
integrovali	integrovat	k5eAaBmAgMnP	integrovat
a	a	k8xC	a
usadili	usadit	k5eAaPmAgMnP	usadit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
živili	živit	k5eAaImAgMnP	živit
podomním	podomní	k2eAgInSc7d1	podomní
obchodem	obchod	k1gInSc7	obchod
<g/>
,	,	kIx,	,
obchodováním	obchodování	k1gNnSc7	obchodování
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
<g/>
,	,	kIx,	,
košíkářstvím	košíkářství	k1gNnSc7	košíkářství
<g/>
,	,	kIx,	,
kovářstvím	kovářství	k1gNnSc7	kovářství
nebo	nebo	k8xC	nebo
provozováním	provozování	k1gNnSc7	provozování
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	s	k7c7	s
snahami	snaha	k1gFnPc7	snaha
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
asimilaci	asimilace	k1gFnSc6	asimilace
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
vlády	vláda	k1gFnPc1	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
===	===	k?	===
Romantismus	romantismus	k1gInSc4	romantismus
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Cikány	cikán	k1gMnPc4	cikán
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
nespoutaná	spoutaný	k2eNgFnSc1d1	nespoutaná
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
romantickými	romantický	k2eAgFnPc7d1	romantická
básníky	básník	k1gMnPc4	básník
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
opak	opak	k1gInSc4	opak
stísněných	stísněný	k2eAgInPc2d1	stísněný
poměrů	poměr	k1gInPc2	poměr
panujících	panující	k2eAgInPc2d1	panující
v	v	k7c6	v
usedlé	usedlý	k2eAgFnSc6d1	usedlá
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Romantici	romantik	k1gMnPc1	romantik
si	se	k3xPyFc3	se
však	však	k9	však
těžký	těžký	k2eAgInSc4d1	těžký
a	a	k8xC	a
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
život	život	k1gInSc4	život
kočujících	kočující	k2eAgMnPc2d1	kočující
Cikánů	cikán	k1gMnPc2	cikán
značně	značně	k6eAd1	značně
idealizovali	idealizovat	k5eAaBmAgMnP	idealizovat
<g/>
.	.	kIx.	.
</s>
<s>
Cikáni	cikán	k1gMnPc1	cikán
se	se	k3xPyFc4	se
v	v	k7c4	v
pojetí	pojetí	k1gNnSc4	pojetí
umělců	umělec	k1gMnPc2	umělec
stávají	stávat	k5eAaImIp3nP	stávat
symbolem	symbol	k1gInSc7	symbol
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
líčeni	líčit	k5eAaImNgMnP	líčit
jako	jako	k8xC	jako
krásní	krásný	k2eAgMnPc1d1	krásný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
kočujících	kočující	k2eAgMnPc2d1	kočující
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
působil	působit	k5eAaImAgMnS	působit
tajemně	tajemně	k6eAd1	tajemně
a	a	k8xC	a
nebezpečně	bezpečně	k6eNd1	bezpečně
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
okouzluje	okouzlovat	k5eAaImIp3nS	okouzlovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podvědomí	podvědomí	k1gNnSc6	podvědomí
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
ale	ale	k8xC	ale
nadále	nadále	k6eAd1	nadále
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
strach	strach	k1gInSc1	strach
z	z	k7c2	z
Cikánů	cikán	k1gMnPc2	cikán
<g/>
,	,	kIx,	,
tradovaný	tradovaný	k2eAgInSc1d1	tradovaný
v	v	k7c6	v
pověstech	pověst	k1gFnPc6	pověst
a	a	k8xC	a
brakové	brakový	k2eAgFnSc3d1	braková
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
Cikánů	cikán	k1gMnPc2	cikán
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
osoby	osoba	k1gFnPc4	osoba
žijící	žijící	k2eAgFnPc4d1	žijící
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Oplocování	oplocování	k1gNnSc1	oplocování
a	a	k8xC	a
zcelování	zcelování	k1gNnSc1	zcelování
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
je	on	k3xPp3gMnPc4	on
připravilo	připravit	k5eAaPmAgNnS	připravit
o	o	k7c4	o
tradiční	tradiční	k2eAgNnSc4d1	tradiční
tábořiště	tábořiště	k1gNnSc4	tábořiště
a	a	k8xC	a
nemožnost	nemožnost	k1gFnSc4	nemožnost
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
pracovního	pracovní	k2eAgInSc2d1	pracovní
procesu	proces	k1gInSc2	proces
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
jejich	jejich	k3xOp3gInSc4	jejich
sociální	sociální	k2eAgInSc4d1	sociální
úpadek	úpadek	k1gInSc4	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
násilného	násilný	k2eAgMnSc2d1	násilný
"	"	kIx"	"
<g/>
civilizování	civilizování	k1gNnSc2	civilizování
<g/>
"	"	kIx"	"
Cikánů	cikán	k1gMnPc2	cikán
probíhal	probíhat	k5eAaImAgMnS	probíhat
ještě	ještě	k6eAd1	ještě
celé	celý	k2eAgInPc4d1	celý
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
a	a	k8xC	a
znamenal	znamenat	k5eAaImAgInS	znamenat
postupnou	postupný	k2eAgFnSc4d1	postupná
likvidaci	likvidace	k1gFnSc4	likvidace
jejich	jejich	k3xOp3gInSc2	jejich
tradičního	tradiční	k2eAgInSc2d1	tradiční
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dějiny	dějiny	k1gFnPc1	dějiny
Romů	Rom	k1gMnPc2	Rom
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rasistických	rasistický	k2eAgInPc2d1	rasistický
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
cikánské	cikánský	k2eAgFnPc4d1	cikánská
legitimace	legitimace	k1gFnPc4	legitimace
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
holokaust	holokaust	k1gInSc1	holokaust
(	(	kIx(	(
<g/>
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
českých	český	k2eAgMnPc2d1	český
Romů	Rom	k1gMnPc2	Rom
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komunistických	komunistický	k2eAgFnPc2d1	komunistická
snah	snaha	k1gFnPc2	snaha
o	o	k7c6	o
asimilaci	asimilace	k1gFnSc6	asimilace
(	(	kIx(	(
<g/>
zákaz	zákaz	k1gInSc1	zákaz
kočování	kočování	k1gNnSc2	kočování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
rovnoměrný	rovnoměrný	k2eAgInSc4d1	rovnoměrný
rozptyl	rozptyl	k1gInSc4	rozptyl
po	po	k7c6	po
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sterilizace	sterilizace	k1gFnSc1	sterilizace
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gNnPc2	jejich
vědomí	vědomí	k1gNnPc2	vědomí
<g/>
,	,	kIx,	,
potlačování	potlačování	k1gNnSc1	potlačování
romského	romský	k2eAgInSc2d1	romský
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
tresty	trest	k1gInPc1	trest
za	za	k7c4	za
používání	používání	k1gNnSc4	používání
romštiny	romština	k1gFnSc2	romština
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
romských	romský	k2eAgFnPc2d1	romská
dětí	dítě	k1gFnPc2	dítě
byla	být	k5eAaImAgFnS	být
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
do	do	k7c2	do
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
sílí	sílet	k5eAaImIp3nS	sílet
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
společenskou	společenský	k2eAgFnSc4d1	společenská
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
emancipaci	emancipace	k1gFnSc4	emancipace
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
romské	romský	k2eAgInPc4d1	romský
kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
spolky	spolek	k1gInPc4	spolek
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
organizují	organizovat	k5eAaBmIp3nP	organizovat
se	se	k3xPyFc4	se
celosvětové	celosvětový	k2eAgInPc1d1	celosvětový
sjezdy	sjezd	k1gInPc1	sjezd
jako	jako	k8xC	jako
např.	např.	kA	např.
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
1	[number]	k4	1
<g/>
.	.	kIx.	.
světový	světový	k2eAgInSc1d1	světový
romský	romský	k2eAgInSc1d1	romský
sjezd	sjezd	k1gInSc1	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
emigraci	emigrace	k1gFnSc3	emigrace
Romů	Rom	k1gMnPc2	Rom
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
zejména	zejména	k9	zejména
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
proto	proto	k8xC	proto
znovu	znovu	k6eAd1	znovu
zavedla	zavést	k5eAaPmAgFnS	zavést
víza	vízo	k1gNnPc4	vízo
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Romská	romský	k2eAgFnSc1d1	romská
komunita	komunita	k1gFnSc1	komunita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Romové	Rom	k1gMnPc1	Rom
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
etnickou	etnický	k2eAgFnSc7d1	etnická
minoritou	minorita	k1gFnSc7	minorita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
romské	romský	k2eAgFnSc3d1	romská
národnostní	národnostní	k2eAgFnSc3d1	národnostní
menšině	menšina	k1gFnSc3	menšina
se	se	k3xPyFc4	se
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
2001	[number]	k4	2001
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
celkem	celek	k1gInSc7	celek
11	[number]	k4	11
746	[number]	k4	746
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
příslušníku	příslušník	k1gMnSc3	příslušník
tohoto	tento	k3xDgNnSc2	tento
etnika	etnikum	k1gNnSc2	etnikum
je	být	k5eAaImIp3nS	být
však	však	k9	však
odhadován	odhadovat	k5eAaImNgInS	odhadovat
až	až	k9	až
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
Liégois	Liégois	k1gInSc1	Liégois
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Romové	Rom	k1gMnPc1	Rom
přicházeli	přicházet	k5eAaImAgMnP	přicházet
asi	asi	k9	asi
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc4	několik
větví	větev	k1gFnPc2	větev
romského	romský	k2eAgInSc2d1	romský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nevýznamnější	významný	k2eNgFnSc7d2	nevýznamnější
a	a	k8xC	a
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Slovenští	slovenský	k2eAgMnPc1d1	slovenský
Romové	Rom	k1gMnPc1	Rom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zvaní	zvaný	k2eAgMnPc1d1	zvaný
Rumungři	Rumungr	k1gMnPc1	Rumungr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
přišli	přijít	k5eAaPmAgMnP	přijít
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
původních	původní	k2eAgMnPc2d1	původní
Českých	český	k2eAgMnPc2d1	český
Romů	Rom	k1gMnPc2	Rom
vyhlazena	vyhlazen	k2eAgFnSc1d1	vyhlazena
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
větví	větev	k1gFnSc7	větev
Romského	romský	k2eAgInSc2d1	romský
národa	národ	k1gInSc2	národ
jsou	být	k5eAaImIp3nP	být
olašští	olašský	k2eAgMnPc1d1	olašský
Romové	Rom	k1gMnPc1	Rom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Romové	Rom	k1gMnPc1	Rom
v	v	k7c6	v
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
skupiny	skupina	k1gFnPc4	skupina
zranitelné	zranitelný	k2eAgFnSc2d1	zranitelná
sociálním	sociální	k2eAgNnSc7d1	sociální
vyloučením	vyloučení	k1gNnSc7	vyloučení
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
negativními	negativní	k2eAgInPc7d1	negativní
důsledky	důsledek	k1gInPc7	důsledek
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgMnPc7	jaký
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nízká	nízký	k2eAgFnSc1d1	nízká
úroveň	úroveň	k1gFnSc1	úroveň
dosaženého	dosažený	k2eAgNnSc2d1	dosažené
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
romských	romský	k2eAgFnPc2d1	romská
dětí	dítě	k1gFnPc2	dítě
končí	končit	k5eAaImIp3nS	končit
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
aktivistů	aktivista	k1gMnPc2	aktivista
v	v	k7c6	v
praktických	praktický	k2eAgFnPc6d1	praktická
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
následná	následný	k2eAgFnSc1d1	následná
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
až	až	k9	až
57	[number]	k4	57
%	%	kIx~	%
Romů	Rom	k1gMnPc2	Rom
v	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kriminalita	kriminalita	k1gFnSc1	kriminalita
a	a	k8xC	a
ničení	ničení	k1gNnSc1	ničení
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
získávalo	získávat	k5eAaImAgNnS	získávat
prostředky	prostředek	k1gInPc7	prostředek
k	k	k7c3	k
obživě	obživa	k1gFnSc3	obživa
nelegálně	legálně	k6eNd1	legálně
20-30	[number]	k4	20-30
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivní	aktivní	k2eAgFnSc2d1	aktivní
romské	romský	k2eAgFnSc2d1	romská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<g/>
Souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
postoje	postoj	k1gInPc1	postoj
majoritní	majoritní	k2eAgFnSc2d1	majoritní
části	část	k1gFnSc2	část
občanů	občan	k1gMnPc2	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
protiromské	protiromský	k2eAgFnPc1d1	protiromská
nálady	nálada	k1gFnPc1	nálada
ústící	ústící	k2eAgFnPc1d1	ústící
také	také	k9	také
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
protiromské	protiromský	k2eAgInPc4d1	protiromský
pochody	pochod	k1gInPc4	pochod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
bývají	bývat	k5eAaImIp3nP	bývat
Romové	Rom	k1gMnPc1	Rom
zahrnováni	zahrnován	k2eAgMnPc1d1	zahrnován
mezi	mezi	k7c4	mezi
nepřizpůsobivé	přizpůsobivý	k2eNgMnPc4d1	nepřizpůsobivý
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Protiromské	Protiromský	k2eAgFnPc4d1	Protiromská
nálady	nálada	k1gFnPc4	nálada
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
analytického	analytický	k2eAgNnSc2d1	analytické
střediska	středisko	k1gNnSc2	středisko
Pew	Pew	k1gMnSc1	Pew
Research	Research	k1gMnSc1	Research
Center	centrum	k1gNnPc2	centrum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
vůči	vůči	k7c3	vůči
Romům	Rom	k1gMnPc3	Rom
negativní	negativní	k2eAgInSc1d1	negativní
postoj	postoj	k1gInSc1	postoj
82	[number]	k4	82
<g/>
%	%	kIx~	%
Italů	Ital	k1gMnPc2	Ital
<g/>
,	,	kIx,	,
67	[number]	k4	67
<g/>
%	%	kIx~	%
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
64	[number]	k4	64
<g/>
%	%	kIx~	%
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
61	[number]	k4	61
<g/>
%	%	kIx~	%
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
49	[number]	k4	49
<g/>
%	%	kIx~	%
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
,	,	kIx,	,
47	[number]	k4	47
<g/>
%	%	kIx~	%
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
%	%	kIx~	%
Britů	Brit	k1gMnPc2	Brit
<g/>
,	,	kIx,	,
42	[number]	k4	42
<g/>
%	%	kIx~	%
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
%	%	kIx~	%
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
a	a	k8xC	a
37	[number]	k4	37
<g/>
%	%	kIx~	%
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
spáchano	spáchana	k1gFnSc5	spáchana
krajně	krajně	k6eAd1	krajně
pravicovými	pravicový	k2eAgFnPc7d1	pravicová
extremisty	extremista	k1gMnPc4	extremista
a	a	k8xC	a
nacionalisty	nacionalista	k1gMnPc4	nacionalista
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
útoků	útok	k1gInPc2	útok
proti	proti	k7c3	proti
romské	romský	k2eAgFnSc3d1	romská
menšině	menšina	k1gFnSc3	menšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
rómské	rómský	k2eAgFnSc2d1	rómská
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panuje	panovat	k5eAaImIp3nS	panovat
velká	velký	k2eAgFnSc1d1	velká
soudržnost	soudržnost	k1gFnSc1	soudržnost
<g/>
.	.	kIx.	.
</s>
<s>
Rodinou	rodina	k1gFnSc7	rodina
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Romy	Rom	k1gMnPc4	Rom
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
představitel	představitel	k1gMnSc1	představitel
určitého	určitý	k2eAgNnSc2d1	určité
seskupení	seskupení	k1gNnSc2	seskupení
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
požívá	požívat	k5eAaImIp3nS	požívat
velkou	velký	k2eAgFnSc4d1	velká
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
vajda	vajda	k1gMnSc1	vajda
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
"	"	kIx"	"
<g/>
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kníže	kníže	k1gMnSc1	kníže
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
maďarština	maďarština	k1gFnSc1	maďarština
slovo	slovo	k1gNnSc1	slovo
zase	zase	k9	zase
přejala	přejmout	k5eAaPmAgFnS	přejmout
ze	z	k7c2	z
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgNnPc4d1	tradiční
romská	romský	k2eAgNnPc4d1	romské
povolání	povolání	k1gNnPc4	povolání
patřily	patřit	k5eAaImAgFnP	patřit
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
kovářství	kovářství	k1gNnSc1	kovářství
<g/>
,	,	kIx,	,
košíkářství	košíkářství	k1gNnSc1	košíkářství
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc1	čištění
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
broušení	broušení	k1gNnSc1	broušení
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
věštectví	věštectví	k1gNnPc2	věštectví
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Romština	romština	k1gFnSc1	romština
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
indické	indický	k2eAgFnSc2d1	indická
skupiny	skupina	k1gFnSc2	skupina
indoárijských	indoárijský	k2eAgInPc2d1	indoárijský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
hlavní	hlavní	k2eAgFnSc2d1	hlavní
skupiny	skupina	k1gFnSc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
evropská	evropský	k2eAgFnSc1d1	Evropská
romština	romština	k1gFnSc1	romština
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
=	=	kIx~	=
rom	rom	k?	rom
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
domština	domština	k1gFnSc1	domština
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
=	=	kIx~	=
dom	dom	k?	dom
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Íránu	Írán	k1gInSc2	Írán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lomaverština	lomaverština	k1gFnSc1	lomaverština
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
=	=	kIx~	=
lom	lom	k1gInSc1	lom
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
)	)	kIx)	)
<g/>
Má	mít	k5eAaImIp3nS	mít
nesčetné	sčetný	k2eNgInPc4d1	nesčetný
dialekty	dialekt	k1gInPc4	dialekt
(	(	kIx(	(
<g/>
jen	jen	k9	jen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
kolem	kolem	k6eAd1	kolem
šedesáti	šedesát	k4xCc2	šedesát
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
nesrozumitelné	srozumitelný	k2eNgNnSc4d1	nesrozumitelné
–	–	k?	–
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
procentu	procent	k1gInSc6	procent
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgInPc4d1	národní
symboly	symbol	k1gInPc4	symbol
===	===	k?	===
</s>
</p>
<p>
<s>
Romská	romský	k2eAgFnSc1d1	romská
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
pruhu	pruh	k1gInSc2	pruh
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
<g/>
,	,	kIx,	,
zeleného	zelený	k2eAgInSc2d1	zelený
pruhu	pruh	k1gInSc2	pruh
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dolní	dolní	k2eAgFnSc6d1	dolní
a	a	k8xC	a
z	z	k7c2	z
červené	červený	k2eAgFnSc2d1	červená
čakry	čakra	k1gFnSc2	čakra
s	s	k7c7	s
16	[number]	k4	16
paprsky	paprsek	k1gInPc1	paprsek
umístěné	umístěný	k2eAgInPc1d1	umístěný
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Čakra	čakra	k1gFnSc1	čakra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
indický	indický	k2eAgInSc4d1	indický
původ	původ	k1gInSc4	původ
romského	romský	k2eAgInSc2d1	romský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
život	život	k1gInSc4	život
věčných	věčný	k2eAgMnPc2d1	věčný
poutníků	poutník	k1gMnPc2	poutník
po	po	k7c6	po
zelené	zelený	k2eAgFnSc6d1	zelená
zemi	zem	k1gFnSc6	zem
pod	pod	k7c7	pod
blankytnou	blankytný	k2eAgFnSc7d1	blankytná
oblohou	obloha	k1gFnSc7	obloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
sjezdu	sjezd	k1gInSc2	sjezd
IRU	Ir	k1gMnSc6	Ir
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
za	za	k7c4	za
romskou	romský	k2eAgFnSc4d1	romská
hymnu	hymna	k1gFnSc4	hymna
považuje	považovat	k5eAaImIp3nS	považovat
romská	romský	k2eAgFnSc1d1	romská
píseň	píseň	k1gFnSc1	píseň
Gejľem	Gejľem	k1gInSc1	Gejľem
<g/>
,	,	kIx,	,
gejľem	gejľem	k6eAd1	gejľem
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Geľem	Geľem	k1gInSc4	Geľem
<g/>
,	,	kIx,	,
Geľem	Geľem	k1gInSc4	Geľem
<g/>
;	;	kIx,	;
Djelem	Djel	k1gMnSc7	Djel
<g/>
,	,	kIx,	,
Djelem	Djel	k1gMnSc7	Djel
<g/>
;	;	kIx,	;
Opre	Opr	k1gMnSc2	Opr
Roma	Rom	k1gMnSc2	Rom
<g/>
;	;	kIx,	;
Romale	Romal	k1gMnSc5	Romal
<g/>
,	,	kIx,	,
čhavale	čhavale	k6eAd1	čhavale
atd.	atd.	kA	atd.
Za	za	k7c4	za
romskou	romský	k2eAgFnSc4d1	romská
hymnu	hymna	k1gFnSc4	hymna
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
sjezdu	sjezd	k1gInSc2	sjezd
IRU	Ir	k1gMnSc3	Ir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tradiční	tradiční	k2eAgNnSc4d1	tradiční
příjmení	příjmení	k1gNnSc4	příjmení
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
typická	typický	k2eAgNnPc4d1	typické
příjmení	příjmení	k1gNnSc4	příjmení
českých	český	k2eAgMnPc2d1	český
olašských	olašský	k2eAgMnPc2d1	olašský
Romů	Rom	k1gMnPc2	Rom
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Horváth	Horváth	k1gMnSc1	Horváth
<g/>
,	,	kIx,	,
Oláh	Oláh	k1gMnSc1	Oláh
<g/>
,	,	kIx,	,
Balog	Balog	k1gMnSc1	Balog
a	a	k8xC	a
Lakatoš	Lakatoš	k1gMnSc1	Lakatoš
<g/>
,	,	kIx,	,
Romové	Rom	k1gMnPc1	Rom
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
mají	mít	k5eAaImIp3nP	mít
zase	zase	k9	zase
často	často	k6eAd1	často
příjmení	příjmení	k1gNnSc4	příjmení
Demeter	Demeter	k1gFnSc2	Demeter
<g/>
,	,	kIx,	,
Sivák	Sivák	k1gMnSc1	Sivák
atd.	atd.	kA	atd.
Častá	častý	k2eAgNnPc1d1	časté
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
příjmení	příjmení	k1gNnSc4	příjmení
Žiga	Žigum	k1gNnSc2	Žigum
a	a	k8xC	a
Németh	Németha	k1gFnPc2	Németha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typická	typický	k2eAgNnPc4d1	typické
příjmení	příjmení	k1gNnPc4	příjmení
českých	český	k2eAgMnPc2d1	český
Romů	Rom	k1gMnPc2	Rom
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
převážně	převážně	k6eAd1	převážně
kočovní	kočovní	k2eAgMnPc1d1	kočovní
čeští	český	k2eAgMnPc1d1	český
Romové	Rom	k1gMnPc1	Rom
–	–	k?	–
příjmení	příjmení	k1gNnSc2	příjmení
Růžička	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Charvát	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petržilka	petržilka	k1gFnSc1	petržilka
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
převážně	převážně	k6eAd1	převážně
usedlí	usedlý	k2eAgMnPc1d1	usedlý
moravští	moravský	k2eAgMnPc1d1	moravský
Romové	Rom	k1gMnPc1	Rom
–	–	k?	–
příjmení	příjmení	k1gNnSc2	příjmení
Daniel	Daniela	k1gFnPc2	Daniela
<g/>
,	,	kIx,	,
Ištván	Ištván	k2eAgMnSc1d1	Ištván
<g/>
,	,	kIx,	,
Herák	Herák	k1gMnSc1	Herák
<g/>
,	,	kIx,	,
Holomek	holomek	k1gMnSc1	holomek
<g/>
,	,	kIx,	,
Malík	Malík	k1gMnSc1	Malík
<g/>
,	,	kIx,	,
Kýr	kýr	k1gInSc1	kýr
<g/>
,	,	kIx,	,
Murka	Murka	k1gMnSc1	Murka
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
německy	německy	k6eAd1	německy
hovořící	hovořící	k2eAgMnPc1d1	hovořící
Romové	Rom	k1gMnPc1	Rom
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
–	–	k?	–
příjmení	příjmení	k1gNnSc1	příjmení
Bamberger	Bamberger	k1gMnSc1	Bamberger
<g/>
,	,	kIx,	,
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Klimt	Klimt	k1gMnSc1	Klimt
<g/>
,	,	kIx,	,
Lagryn	Lagryn	k1gMnSc1	Lagryn
atd.	atd.	kA	atd.
<g/>
Rovněž	rovněž	k9	rovněž
některá	některý	k3yIgNnPc1	některý
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
romské	romský	k2eAgFnSc6d1	romská
populaci	populace	k1gFnSc6	populace
častější	častý	k2eAgInSc1d2	častější
než	než	k8xS	než
u	u	k7c2	u
etnických	etnický	k2eAgMnPc2d1	etnický
Čechů	Čech	k1gMnPc2	Čech
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Erika	Erika	k1gFnSc1	Erika
<g/>
,	,	kIx,	,
Koloman	Koloman	k1gMnSc1	Koloman
<g/>
,	,	kIx,	,
Etela	Etela	k1gMnSc1	Etela
<g/>
,	,	kIx,	,
Gizela	Gizela	k1gFnSc1	Gizela
<g/>
,	,	kIx,	,
Eržika	Eržika	k1gFnSc1	Eržika
<g/>
,	,	kIx,	,
Sandra	Sandra	k1gFnSc1	Sandra
<g/>
,	,	kIx,	,
Gejza	Gejza	k1gFnSc1	Gejza
<g/>
,	,	kIx,	,
Imrich	Imrich	k1gMnSc1	Imrich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
výrazný	výrazný	k2eAgInSc1d1	výrazný
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
Romů	Rom	k1gMnPc2	Rom
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
obdobná	obdobný	k2eAgNnPc4d1	obdobné
jména	jméno	k1gNnPc4	jméno
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgNnSc4d1	ostatní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
českých	český	k2eAgMnPc2d1	český
Romů	Rom	k1gMnPc2	Rom
pochází	pocházet	k5eAaImIp3nS	pocházet
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
častěji	často	k6eAd2	často
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
celkem	celkem	k6eAd1	celkem
běžná	běžný	k2eAgFnSc1d1	běžná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zoltán	Zoltán	k1gMnSc1	Zoltán
<g/>
,	,	kIx,	,
Tibor	Tibor	k1gMnSc1	Tibor
<g/>
,	,	kIx,	,
Margita	Margita	k1gMnSc1	Margita
<g/>
,	,	kIx,	,
Elemír	Elemír	k1gMnSc1	Elemír
<g/>
,	,	kIx,	,
Dezider	Dezider	k1gMnSc1	Dezider
<g/>
,	,	kIx,	,
Marika	Marika	k1gFnSc1	Marika
<g/>
,	,	kIx,	,
Attila	Attila	k1gMnSc1	Attila
nebo	nebo	k8xC	nebo
Aladár	Aladár	k1gMnSc1	Aladár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Romové	Rom	k1gMnPc1	Rom
se	se	k3xPyFc4	se
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
náboženství	náboženství	k1gNnSc3	náboženství
místního	místní	k2eAgNnSc2d1	místní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Romů	Rom	k1gMnPc2	Rom
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
přijali	přijmout	k5eAaPmAgMnP	přijmout
Romové	Rom	k1gMnPc1	Rom
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
formální	formální	k2eAgMnPc1d1	formální
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
patronkou	patronka	k1gFnSc7	patronka
je	být	k5eAaImIp3nS	být
Černá	černý	k2eAgFnSc1d1	černá
Sára	Sára	k1gFnSc1	Sára
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
přibývá	přibývat	k5eAaImIp3nS	přibývat
romských	romský	k2eAgInPc2d1	romský
členů	člen	k1gInPc2	člen
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
představy	představa	k1gFnPc1	představa
Romů	Rom	k1gMnPc2	Rom
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
rituály	rituál	k1gInPc1	rituál
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojené	spojený	k2eAgInPc1d1	spojený
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
podle	podle	k7c2	podle
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rodinách	rodina	k1gFnPc6	rodina
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
romské	romský	k2eAgFnSc2d1	romská
tradice	tradice	k1gFnSc2	tradice
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
tradice	tradice	k1gFnPc1	tradice
spojené	spojený	k2eAgFnPc1d1	spojená
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
narozením	narození	k1gNnSc7	narození
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
slovenští	slovenský	k2eAgMnPc1d1	slovenský
Romové	Rom	k1gMnPc1	Rom
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
respekt	respekt	k1gInSc4	respekt
z	z	k7c2	z
duchů	duch	k1gMnPc2	duch
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
,	,	kIx,	,
mulů	mul	k1gMnPc2	mul
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
alespoň	alespoň	k9	alespoň
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
životě	život	k1gInSc6	život
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
setkání	setkání	k1gNnPc2	setkání
mívají	mívat	k5eAaImIp3nP	mívat
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Mulové	mul	k1gMnPc1	mul
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
mohou	moct	k5eAaImIp3nP	moct
zjevovat	zjevovat	k5eAaImF	zjevovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
varovat	varovat	k5eAaImF	varovat
před	před	k7c7	před
hrozícím	hrozící	k2eAgNnSc7d1	hrozící
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
slovenských	slovenský	k2eAgMnPc2d1	slovenský
Romů	Rom	k1gMnPc2	Rom
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
guny	guna	k1gFnSc2	guna
daj	daj	k?	daj
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
obdoby	obdoba	k1gFnSc2	obdoba
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
polednice	polednice	k1gFnSc2	polednice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
uhranout	uhranout	k5eAaPmF	uhranout
jejich	jejich	k3xOp3gNnSc4	jejich
nově	nově	k6eAd1	nově
narozené	narozený	k2eAgNnSc4d1	narozené
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gMnSc4	on
nechají	nechat	k5eAaPmIp3nP	nechat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
pokřtěno	pokřtěn	k2eAgNnSc1d1	pokřtěno
<g/>
.	.	kIx.	.
</s>
<s>
Guny	Guna	k1gFnPc1	Guna
daj	daj	k?	daj
lze	lze	k6eAd1	lze
zahnat	zahnat	k5eAaPmF	zahnat
magickými	magický	k2eAgFnPc7d1	magická
praktikami	praktika	k1gFnPc7	praktika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
věří	věřit	k5eAaImIp3nP	věřit
i	i	k9	i
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
(	(	kIx(	(
<g/>
strig	striga	k1gFnPc2	striga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
strigách	striga	k1gFnPc6	striga
a	a	k8xC	a
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
tabu	tabu	k1gNnSc1	tabu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
===	===	k?	===
</s>
</p>
<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Lacková	Lacková	k1gFnSc1	Lacková
<g/>
:	:	kIx,	:
Narodila	narodit	k5eAaPmAgFnS	narodit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
šťastnou	šťastný	k2eAgFnSc7d1	šťastná
hvězdou	hvězda	k1gFnSc7	hvězda
–	–	k?	–
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
autorky	autorka	k1gFnSc2	autorka
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc2d1	pocházející
ze	z	k7c2	z
Šariše	Šariš	k1gInSc2	Šariš
u	u	k7c2	u
Prešova	Prešov	k1gInSc2	Prešov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
romská	romský	k2eAgFnSc1d1	romská
studentka	studentka	k1gFnSc1	studentka
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tera	Ter	k2eAgFnSc1d1	Tera
Fabiánová	Fabiánová	k1gFnSc1	Fabiánová
<g/>
:	:	kIx,	:
Tulák	tulák	k1gMnSc1	tulák
–	–	k?	–
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
autorčina	autorčin	k2eAgNnSc2d1	autorčino
dětství	dětství	k1gNnSc2	dětství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Margita	Margit	k2eAgFnSc1d1	Margita
Reiznerová	Reiznerová	k1gFnSc1	Reiznerová
–	–	k?	–
Suno	suna	k1gFnSc5	suna
–	–	k?	–
Sny	sen	k1gInPc4	sen
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
prodchnuty	prodchnout	k5eAaPmNgFnP	prodchnout
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
těžkým	těžký	k2eAgInSc7d1	těžký
životním	životní	k2eAgInSc7d1	životní
osudem	osud	k1gInSc7	osud
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlado	Vlado	k1gNnSc1	Vlado
Oláh	Oláha	k1gFnPc2	Oláha
<g/>
:	:	kIx,	:
Khamuto	Khamut	k2eAgNnSc1d1	Khamut
kamiben	kamiben	k2eAgInSc4d1	kamiben
–	–	k?	–
Žár	žár	k1gInSc4	žár
lásky	láska	k1gFnSc2	láska
–	–	k?	–
básně	báseň	k1gFnPc1	báseň
</s>
</p>
<p>
<s>
Gejza	Gejza	k1gFnSc1	Gejza
Horváth	Horváth	k1gInSc1	Horváth
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Trispras	Trisprasa	k1gFnPc2	Trisprasa
</s>
</p>
<p>
<s>
===	===	k?	===
Divadlo	divadlo	k1gNnSc1	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Líšeň	Líšeň	k1gFnSc1	Líšeň
</s>
</p>
<p>
<s>
Pralipe	Pralipat	k5eAaPmIp3nS	Pralipat
(	(	kIx(	(
<g/>
bratrství	bratrství	k1gNnSc1	bratrství
<g/>
,	,	kIx,	,
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
ve	v	k7c6	v
Skopje	Skopje	k1gFnSc1	Skopje
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romathan	Romathan	k1gInSc1	Romathan
(	(	kIx(	(
<g/>
Místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
Romy	Rom	k1gMnPc4	Rom
<g/>
,	,	kIx,	,
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
</s>
</p>
<p>
<s>
Romen	Romen	k1gInSc1	Romen
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Janos	Janos	k1gMnSc1	Janos
Bihári	Bihár	k1gFnSc2	Bihár
–	–	k?	–
oblíbenec	oblíbenec	k1gMnSc1	oblíbenec
Liszta	Liszta	k1gMnSc1	Liszta
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
romského	romský	k2eAgInSc2d1	romský
repertoáru	repertoár	k1gInSc2	repertoár
pochází	pocházet	k5eAaImIp3nS	pocházet
tance	tanec	k1gInPc4	tanec
verbunkos	verbunkosa	k1gFnPc2	verbunkosa
a	a	k8xC	a
flamenco	flamenco	k1gNnSc1	flamenco
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
označovali	označovat	k5eAaImAgMnP	označovat
Romové	Rom	k1gMnPc1	Rom
v	v	k7c6	v
Andalusii	Andalusie	k1gFnSc6	Andalusie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panna	Panna	k1gFnSc1	Panna
Cinková	Cinková	k1gFnSc1	Cinková
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
romská	romský	k2eAgFnSc1d1	romská
Sapfó	Sapfó	k1gFnSc1	Sapfó
<g/>
,	,	kIx,	,
primáška	primáška	k1gFnSc1	primáška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papusza	Papusza	k1gFnSc1	Papusza
(	(	kIx(	(
<g/>
=	=	kIx~	=
Panenka	panenka	k1gFnSc1	panenka
<g/>
)	)	kIx)	)
–	–	k?	–
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
jménem	jméno	k1gNnSc7	jméno
Bronislawa	Bronislaw	k1gInSc2	Bronislaw
Wajs	Wajsa	k1gFnPc2	Wajsa
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
balad	balada	k1gFnPc2	balada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jožka	Jožka	k1gMnSc1	Jožka
Kubík	Kubík	k1gMnSc1	Kubík
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
–	–	k?	–
Horňácko	Horňácko	k1gNnSc1	Horňácko
<g/>
,	,	kIx,	,
primáš	primáš	k1gMnSc1	primáš
<g/>
.	.	kIx.	.
</s>
<s>
CD	CD	kA	CD
Dalekonosné	dalekonosný	k2eAgFnPc4d1	dalekonosná
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
Majstr	Majstr	k1gInSc1	Majstr
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dezider	Dezider	k1gMnSc1	Dezider
Fertö	Fertö	k1gMnSc1	Fertö
–	–	k?	–
maďarský	maďarský	k2eAgMnSc1d1	maďarský
Rom	Rom	k1gMnSc1	Rom
usazený	usazený	k2eAgMnSc1d1	usazený
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Gondolán	Gondolán	k2eAgMnSc1d1	Gondolán
–	–	k?	–
nabídka	nabídka	k1gFnSc1	nabídka
spolupráce	spolupráce	k1gFnSc1	spolupráce
od	od	k7c2	od
Franka	Frank	k1gMnSc2	Frank
Sinatry	Sinatr	k1gInPc4	Sinatr
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gottem	Gott	k1gMnSc7	Gott
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ida	Ida	k1gFnSc1	Ida
Kelarová	Kelarový	k2eAgFnSc1d1	Kelarová
–	–	k?	–
dcera	dcera	k1gFnSc1	dcera
Kolomana	Koloman	k1gMnSc4	Koloman
Bitta	Bitt	k1gMnSc4	Bitt
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Ivy	Iva	k1gFnSc2	Iva
Bittové	Bittová	k1gFnSc2	Bittová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Bílá	bílý	k2eAgFnSc1d1	bílá
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
Kale	kale	k6eAd1	kale
(	(	kIx(	(
<g/>
=	=	kIx~	=
Černí	černit	k5eAaImIp3nS	černit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Laco	Laco	k1gMnSc1	Laco
Deczi	Dech	k1gMnPc1	Dech
–	–	k?	–
vynikající	vynikající	k2eAgMnSc1d1	vynikající
jazzový	jazzový	k2eAgMnSc1d1	jazzový
trumpetista	trumpetista	k1gMnSc1	trumpetista
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
skupinou	skupina	k1gFnSc7	skupina
Celula	celula	k1gFnSc1	celula
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
1938	[number]	k4	1938
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Velebným	velebný	k2eAgMnSc7d1	velebný
v	v	k7c6	v
SHQ	SHQ	kA	SHQ
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
sólový	sólový	k2eAgMnSc1d1	sólový
trumpetista	trumpetista	k1gMnSc1	trumpetista
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
na	na	k7c6	na
slavné	slavný	k2eAgFnSc6d1	slavná
nahrávce	nahrávka	k1gFnSc6	nahrávka
Jazz	jazz	k1gInSc1	jazz
Goes	Goesa	k1gFnPc2	Goesa
To	to	k9	to
Beat	beat	k1gInSc1	beat
Václava	Václav	k1gMnSc2	Václav
Zahradníka	Zahradník	k1gMnSc2	Zahradník
<g/>
,	,	kIx,	,
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Iveta	Iveta	k1gFnSc1	Iveta
Kováčová	Kováčová	k1gFnSc1	Kováčová
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc1	Kadaň
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
Triny	Trina	k1gFnSc2	Trina
</s>
</p>
<p>
<s>
Mário	Mário	k1gMnSc1	Mário
Bihári	Bihár	k1gFnSc2	Bihár
–	–	k?	–
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
akordeonista	akordeonista	k1gMnSc1	akordeonista
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Koa	Koa	k1gFnSc2	Koa
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
společně	společně	k6eAd1	společně
se	s	k7c7	s
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Navarovou	Navarův	k2eAgFnSc7d1	Navarova
</s>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
Romy	Rom	k1gMnPc4	Rom
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
regionálních	regionální	k2eAgMnPc2d1	regionální
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
bez	bez	k7c2	bez
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Tibor	Tibor	k1gMnSc1	Tibor
Červeňák	Červeňák	k1gMnSc1	Červeňák
(	(	kIx(	(
<g/>
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Hranicích	Hranice	k1gFnPc6	Hranice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aladár	Aladár	k1gInSc1	Aladár
Kurej	Kurej	k1gInSc1	Kurej
(	(	kIx(	(
<g/>
Humenné	Humenné	k1gNnSc1	Humenné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bratři	bratřit	k5eAaImRp2nS	bratřit
Tibor	Tibor	k1gMnSc1	Tibor
(	(	kIx(	(
<g/>
Rožňava	Rožňava	k1gFnSc1	Rožňava
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dušan	Dušan	k1gMnSc1	Dušan
(	(	kIx(	(
<g/>
Detva	Detva	k1gFnSc1	Detva
<g/>
)	)	kIx)	)
Oláhovi	Oláhův	k2eAgMnPc1d1	Oláhův
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dzurko	Dzurko	k1gNnSc1	Dzurko
(	(	kIx(	(
<g/>
Praha-Smíchov	Praha-Smíchov	k1gInSc1	Praha-Smíchov
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Edward	Edward	k1gMnSc1	Edward
Majewski	Majewsk	k1gFnSc2	Majewsk
(	(	kIx(	(
<g/>
polský	polský	k2eAgMnSc1d1	polský
Opatow	Opatow	k1gMnSc1	Opatow
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Muzeum	muzeum	k1gNnSc1	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
romské	romský	k2eAgFnSc2d1	romská
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
ulice	ulice	k1gFnSc1	ulice
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnPc1	fotografia
Romů	Rom	k1gMnPc2	Rom
==	==	k?	==
</s>
</p>
<p>
<s>
Romové	Rom	k1gMnPc1	Rom
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
častých	častý	k2eAgInPc2d1	častý
námětů	námět	k1gInPc2	námět
dokumentární	dokumentární	k2eAgFnSc2d1	dokumentární
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
věnovali	věnovat	k5eAaImAgMnP	věnovat
či	či	k8xC	či
věnují	věnovat	k5eAaImIp3nP	věnovat
především	především	k9	především
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Davidová	Davidová	k1gFnSc1	Davidová
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Šebková	Šebková	k1gFnSc1	Šebková
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Cudlín	Cudlín	k1gInSc1	Cudlín
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Kracíková	Kracíková	k1gFnSc1	Kracíková
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
Šimánek	Šimánek	k1gMnSc1	Šimánek
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
o	o	k7c6	o
fotografiích	fotografia	k1gFnPc6	fotografia
Romů	Rom	k1gMnPc2	Rom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Ryčl	Ryčl	k1gMnSc1	Ryčl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Tůma	Tůma	k1gMnSc1	Tůma
či	či	k8xC	či
Libuše	Libuše	k1gFnSc1	Libuše
Rudinská	Rudinský	k2eAgFnSc1d1	Rudinská
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
Tibor	Tibor	k1gMnSc1	Tibor
Huszár	Huszár	k1gMnSc1	Huszár
<g/>
,	,	kIx,	,
Mišo	Mišo	k1gMnSc1	Mišo
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Bán	bán	k1gMnSc1	bán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Cikáni	cikán	k1gMnPc1	cikán
</s>
</p>
<p>
<s>
Romština	romština	k1gFnSc1	romština
</s>
</p>
<p>
<s>
Dekáda	dekáda	k1gFnSc1	dekáda
rómské	rómský	k2eAgFnSc2d1	rómská
integrace	integrace	k1gFnSc2	integrace
</s>
</p>
<p>
<s>
Romská	romský	k2eAgFnSc1d1	romská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Porajmos	Porajmos	k1gMnSc1	Porajmos
</s>
</p>
<p>
<s>
Protiromské	Protiromský	k2eAgInPc1d1	Protiromský
pochody	pochod	k1gInPc1	pochod
</s>
</p>
<p>
<s>
Gadžo	gadža	k1gMnSc5	gadža
</s>
</p>
<p>
<s>
Anticikanismus	Anticikanismus	k1gInSc1	Anticikanismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Romové	Rom	k1gMnPc1	Rom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Romové	Rom	k1gMnPc1	Rom
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Romové	Rom	k1gMnPc1	Rom
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
původ	původ	k1gInSc1	původ
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
romove	romov	k1gInSc5	romov
<g/>
.	.	kIx.	.
<g/>
radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
1,2	[number]	k4	1,2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
romove	romov	k1gInSc5	romov
<g/>
.	.	kIx.	.
<g/>
radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Ctibor	Ctibor	k1gMnSc1	Ctibor
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Mgr.	Mgr.	kA	Mgr.
Marta	Marta	k1gFnSc1	Marta
Miklušáková	Miklušáková	k1gFnSc1	Miklušáková
<g/>
:	:	kIx,	:
<g/>
Historie	historie	k1gFnSc1	historie
Romů	Rom	k1gMnPc2	Rom
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
romove	romov	k1gInSc5	romov
<g/>
.	.	kIx.	.
<g/>
radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
12-02-2002	[number]	k4	12-02-2002
</s>
</p>
<p>
<s>
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Červenka	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Sociolingvistický	sociolingvistický	k2eAgInSc1d1	sociolingvistický
výzkum	výzkum	k1gInSc1	výzkum
situace	situace	k1gFnSc2	situace
romštiny	romština	k1gFnSc2	romština
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
centrální	centrální	k2eAgFnSc2d1	centrální
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Seminář	seminář	k1gInSc1	seminář
romistiky	romistika	k1gFnSc2	romistika
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Homoláč	Homoláč	k1gMnSc1	Homoláč
<g/>
,	,	kIx,	,
Kamila	Kamila	k1gFnSc1	Kamila
Karhanová	Karhanová	k1gFnSc1	Karhanová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Nekvapil	Nekvapil	k1gMnSc1	Nekvapil
<g/>
:	:	kIx,	:
<g/>
Obraz	obraz	k1gInSc1	obraz
Romů	Rom	k1gMnPc2	Rom
v	v	k7c6	v
středoevropských	středoevropský	k2eAgNnPc6d1	středoevropské
masmédiích	masmédium	k1gNnPc6	masmédium
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
</s>
</p>
<p>
<s>
Romea	Romeo	k1gMnSc4	Romeo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Romský	romský	k2eAgInSc4d1	romský
informační	informační	k2eAgInSc4d1	informační
portál	portál	k1gInSc4	portál
www.romea.cz	www.romea.cza	k1gFnPc2	www.romea.cza
</s>
</p>
<p>
<s>
ROMEA	Romeo	k1gMnSc2	Romeo
TV	TV	kA	TV
–	–	k?	–
Romská	romský	k2eAgFnSc1d1	romská
internetová	internetový	k2eAgFnSc1d1	internetová
televize	televize	k1gFnSc1	televize
www.romea.tv/	www.romea.tv/	k?	www.romea.tv/
</s>
</p>
<p>
<s>
Romské	romský	k2eAgNnSc1d1	romské
internetové	internetový	k2eAgNnSc1d1	internetové
rádio	rádio	k1gNnSc1	rádio
Rota	rota	k1gFnSc1	rota
radiorota	radiorota	k1gFnSc1	radiorota
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Archiv	archiv	k1gInSc1	archiv
novin	novina	k1gFnPc2	novina
Romano	Romano	k1gMnSc1	Romano
Hangos	Hangos	k1gMnSc1	Hangos
Společenstvím	společenství	k1gNnSc7	společenství
Romů	Rom	k1gMnPc2	Rom
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
srnm	srnm	k6eAd1	srnm
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
sociálně	sociálně	k6eAd1	sociálně
vyloučených	vyloučený	k2eAgFnPc2d1	vyloučená
nebo	nebo	k8xC	nebo
sociálním	sociální	k2eAgNnSc7d1	sociální
vyloučením	vyloučení	k1gNnSc7	vyloučení
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
romských	romský	k2eAgFnPc2d1	romská
lokalit	lokalita	k1gFnPc2	lokalita
v	v	k7c6	v
ČR	ČR	kA	ČR
realizováno	realizovat	k5eAaBmNgNnS	realizovat
GAC	GAC	kA	GAC
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
pro	pro	k7c4	pro
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
</s>
</p>
<p>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
rozhovor	rozhovor	k1gInSc1	rozhovor
se	s	k7c7	s
sociologem	sociolog	k1gMnSc7	sociolog
Ivanem	Ivan	k1gMnSc7	Ivan
Gabalem	Gabal	k1gMnSc7	Gabal
o	o	k7c6	o
řešení	řešení	k1gNnSc6	řešení
romské	romský	k2eAgFnSc2d1	romská
problematiky	problematika	k1gFnSc2	problematika
</s>
</p>
<p>
<s>
Diskriminace	diskriminace	k1gFnSc1	diskriminace
Romů	Rom	k1gMnPc2	Rom
stojí	stát	k5eAaImIp3nS	stát
Česko	Česko	k1gNnSc1	Česko
ročně	ročně	k6eAd1	ročně
miliardy	miliarda	k4xCgFnPc4	miliarda
korun	koruna	k1gFnPc2	koruna
článek	článek	k1gInSc4	článek
reagující	reagující	k2eAgFnSc2d1	reagující
na	na	k7c4	na
zprávu	zpráva	k1gFnSc4	zpráva
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
o	o	k7c6	o
ztrátách	ztráta	k1gFnPc6	ztráta
způsobených	způsobený	k2eAgFnPc2d1	způsobená
nerovnými	rovný	k2eNgFnPc7d1	nerovná
šancemi	šance	k1gFnPc7	šance
na	na	k7c4	na
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Romská	romský	k2eAgFnSc1d1	romská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
</s>
</p>
