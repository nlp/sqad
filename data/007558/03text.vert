<s>
Klub	klub	k1gInSc1	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
GÓLU	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
klub	klub	k1gInSc1	klub
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
československých	československý	k2eAgMnPc2d1	československý
elitních	elitní	k2eAgMnPc2d1	elitní
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vstřelili	vstřelit	k5eAaPmAgMnP	vstřelit
alespoň	alespoň	k9	alespoň
100	[number]	k4	100
prvoligových	prvoligový	k2eAgInPc2d1	prvoligový
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
týdeníkem	týdeník	k1gInSc7	týdeník
Gól	gól	k1gInSc1	gól
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
klub	klub	k1gInSc1	klub
vnikl	vniknout	k5eAaPmAgInS	vniknout
na	na	k7c4	na
popud	popud	k1gInSc4	popud
fotbalového	fotbalový	k2eAgMnSc2d1	fotbalový
statistika	statistik	k1gMnSc2	statistik
Luboše	Luboš	k1gMnSc2	Luboš
Jeřábka	Jeřábek	k1gMnSc2	Jeřábek
podle	podle	k7c2	podle
podobného	podobný	k2eAgInSc2d1	podobný
Klubu	klub	k1gInSc2	klub
střelců	střelec	k1gMnPc2	střelec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
založil	založit	k5eAaPmAgInS	založit
Grigorij	Grigorij	k1gFnSc3	Grigorij
Fedotov	Fedotov	k1gInSc4	Fedotov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
anketu	anketa	k1gFnSc4	anketa
vede	vést	k5eAaImIp3nS	vést
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
asociace	asociace	k1gFnSc1	asociace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Obdobnou	obdobný	k2eAgFnSc7d1	obdobná
anketou	anketa	k1gFnSc7	anketa
je	být	k5eAaImIp3nS	být
Klub	klub	k1gInSc1	klub
ligových	ligový	k2eAgMnPc2d1	ligový
brankářů	brankář	k1gMnPc2	brankář
GÓLU	gól	k1gInSc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
týdeníku	týdeník	k1gInSc2	týdeník
Gól	gól	k1gInSc1	gól
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
ustavit	ustavit	k5eAaPmF	ustavit
sovětskému	sovětský	k2eAgInSc3d1	sovětský
vzoru	vzor	k1gInSc3	vzor
podobný	podobný	k2eAgInSc1d1	podobný
klub	klub	k1gInSc1	klub
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
podpořila	podpořit	k5eAaPmAgFnS	podpořit
a	a	k8xC	a
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nikde	nikde	k6eAd1	nikde
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
statistiky	statistika	k1gFnSc2	statistika
neevidovaly	evidovat	k5eNaImAgInP	evidovat
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
Jeřábkovi	Jeřábek	k1gMnSc6	Jeřábek
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
projít	projít	k5eAaPmF	projít
tiskoviny	tiskovina	k1gFnPc4	tiskovina
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
všechny	všechen	k3xTgMnPc4	všechen
střelce	střelec	k1gMnPc4	střelec
100	[number]	k4	100
a	a	k8xC	a
více	hodně	k6eAd2	hodně
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
československých	československý	k2eAgInPc6d1	československý
prvoligových	prvoligový	k2eAgInPc6d1	prvoligový
zápasech	zápas	k1gInPc6	zápas
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
vyjma	vyjma	k7c2	vyjma
protektorátních	protektorátní	k2eAgInPc2d1	protektorátní
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
gólů	gól	k1gInPc2	gól
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
prvních	první	k4xOgNnPc6	první
22	[number]	k4	22
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
nárok	nárok	k1gInSc4	nárok
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
jimi	on	k3xPp3gMnPc7	on
Josef	Josef	k1gMnSc1	Josef
Silný	silný	k2eAgMnSc1d1	silný
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Wiecek	Wiecka	k1gFnPc2	Wiecka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kloz	Kloz	k1gMnSc1	Kloz
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Pešek	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Pažický	Pažický	k2eAgMnSc1d1	Pažický
<g/>
,	,	kIx,	,
Raymond	Raymond	k1gMnSc1	Raymond
Braine	Brain	k1gInSc5	Brain
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Puč	puč	k1gInSc1	puč
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Mašek	Mašek	k1gMnSc1	Mašek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Křižák	křižák	k1gMnSc1	křižák
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Moravčík	Moravčík	k1gMnSc1	Moravčík
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Bradáč	Bradáč	k1gMnSc1	Bradáč
<g/>
,	,	kIx,	,
Ota	Ota	k1gMnSc1	Ota
Hemele	Hemel	k1gInSc2	Hemel
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Cejp	Cejp	k1gMnSc1	Cejp
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kadraba	Kadraba	k1gMnSc1	Kadraba
a	a	k8xC	a
Adolf	Adolf	k1gMnSc1	Adolf
Scherer	Scherer	k1gMnSc1	Scherer
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Kadraba	Kadraba	k1gMnSc1	Kadraba
a	a	k8xC	a
Scherer	Scherer	k1gMnSc1	Scherer
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
dopsáni	dopsán	k2eAgMnPc1d1	dopsán
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
uvedení	uvedení	k1gNnSc4	uvedení
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
proto	proto	k8xC	proto
cenzurovaly	cenzurovat	k5eAaImAgInP	cenzurovat
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Dvacet	dvacet	k4xCc1	dvacet
hráčů	hráč	k1gMnPc2	hráč
tedy	tedy	k9	tedy
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
pamětní	pamětní	k2eAgInPc4d1	pamětní
odznaky	odznak	k1gInPc4	odznak
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Františka	František	k1gMnSc2	František
Ulče	Ulč	k1gMnSc2	Ulč
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
celkem	celkem	k6eAd1	celkem
sto	sto	k4xCgNnSc1	sto
<g/>
.	.	kIx.	.
</s>
<s>
Odznak	odznak	k1gInSc1	odznak
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
1	[number]	k4	1
získal	získat	k5eAaPmAgInS	získat
zřejmě	zřejmě	k6eAd1	zřejmě
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
střelec	střelec	k1gMnSc1	střelec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
Josef	Josef	k1gMnSc1	Josef
Bican	Bican	k1gMnSc1	Bican
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
několikrát	několikrát	k6eAd1	několikrát
změnila	změnit	k5eAaPmAgNnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
počítaly	počítat	k5eAaImAgInP	počítat
pouze	pouze	k6eAd1	pouze
góly	gól	k1gInPc1	gól
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
politického	politický	k2eAgInSc2d1	politický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
klub	klub	k1gInSc1	klub
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hráči	hráč	k1gMnPc1	hráč
odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
přidáno	přidán	k2eAgNnSc4d1	Přidáno
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
i	i	k9	i
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
ligách	liga	k1gFnPc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
hráčům	hráč	k1gMnPc3	hráč
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
góly	gól	k1gInPc1	gól
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1939-1944	[number]	k4	1939-1944
(	(	kIx(	(
<g/>
protektorátní	protektorátní	k2eAgFnSc2d1	protektorátní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josefu	Josef	k1gMnSc3	Josef
Bicanovi	Bican	k1gMnSc3	Bican
tak	tak	k6eAd1	tak
značně	značně	k6eAd1	značně
narostlo	narůst	k5eAaPmAgNnS	narůst
konto	konto	k1gNnSc4	konto
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
reforma	reforma	k1gFnSc1	reforma
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Hráčům	hráč	k1gMnPc3	hráč
se	se	k3xPyFc4	se
připočetly	připočíst	k5eAaPmAgFnP	připočíst
i	i	k9	i
branky	branka	k1gFnPc1	branka
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
evropských	evropský	k2eAgFnPc2d1	Evropská
lig	liga	k1gFnPc2	liga
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
ligových	ligový	k2eAgFnPc2d1	ligová
soutěží	soutěž	k1gFnPc2	soutěž
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
světadílů	světadíl	k1gInPc2	světadíl
<g/>
)	)	kIx)	)
před	před	k7c7	před
datem	datum	k1gNnSc7	datum
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
statistiku	statistika	k1gFnSc4	statistika
FAČR	FAČR	kA	FAČR
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
AS	as	k9	as
Press	Press	k1gInSc4	Press
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vydavatelem	vydavatel	k1gMnSc7	vydavatel
internetového	internetový	k2eAgInSc2d1	internetový
týdeníku	týdeník	k1gInSc2	týdeník
GÓL	gól	k1gInSc1	gól
(	(	kIx(	(
<g/>
www.gol.cz	www.gol.cz	k1gInSc1	www.gol.cz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnSc4	majitel
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
Klub	klub	k1gInSc4	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
GÓLU	gól	k1gInSc2	gól
<g/>
,	,	kIx,	,
a	a	k8xC	a
majetkově	majetkově	k6eAd1	majetkově
patří	patřit	k5eAaImIp3nS	patřit
fotbalové	fotbalový	k2eAgFnSc3d1	fotbalová
asociaci	asociace	k1gFnSc3	asociace
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
schválena	schválen	k2eAgNnPc1d1	schváleno
nová	nový	k2eAgNnPc1d1	nové
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
zveřejněna	zveřejnit	k5eAaPmNgNnP	zveřejnit
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
(	(	kIx(	(
<g/>
přepočítaným	přepočítaný	k2eAgInSc7d1	přepočítaný
<g/>
)	)	kIx)	)
aktuálním	aktuální	k2eAgNnSc7d1	aktuální
pořadím	pořadí	k1gNnSc7	pořadí
Klubu	klub	k1gInSc2	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
GÓLU	gól	k1gInSc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
KLK	klk	k1gInSc4	klk
Gólu	gól	k1gInSc2	gól
přibylo	přibýt	k5eAaPmAgNnS	přibýt
devět	devět	k4xCc1	devět
nových	nový	k2eAgNnPc2d1	nové
jmen	jméno	k1gNnPc2	jméno
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc1	Pavol
Diňa	Diňa	k1gFnSc1	Diňa
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kubala	Kubala	k1gMnSc1	Kubala
<g/>
,	,	kIx,	,
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Luhový	luhový	k2eAgMnSc1d1	luhový
<g/>
,	,	kIx,	,
Tibor	Tibor	k1gMnSc1	Tibor
Mičinec	Mičinec	k1gMnSc1	Mičinec
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Obert	Obert	k1gInSc1	Obert
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Panenka	panenka	k1gFnSc1	panenka
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jedenácti	jedenáct	k4xCc2	jedenáct
stávajících	stávající	k2eAgMnPc2d1	stávající
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Scherer	Scherer	k1gInSc1	Scherer
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Luhový	luhový	k2eAgMnSc1d1	luhový
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Griga	Griga	k1gFnSc1	Griga
<g/>
,	,	kIx,	,
T.	T.	kA	T.
Skuhravý	Skuhravý	k1gMnSc1	Skuhravý
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Bičovský	Bičovský	k1gMnSc1	Bičovský
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Silný	silný	k2eAgMnSc1d1	silný
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Vízek	Vízek	k1gMnSc1	Vízek
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Herda	Herda	k1gMnSc1	Herda
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Janečka	Janečka	k1gMnSc1	Janečka
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
jejich	jejich	k3xOp3gNnSc2	jejich
konta	konto	k1gNnSc2	konto
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
u	u	k7c2	u
Václava	Václav	k1gMnSc2	Václav
Daňka	Daněk	k1gMnSc2	Daněk
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
přibylo	přibýt	k5eAaPmAgNnS	přibýt
35	[number]	k4	35
branek	branka	k1gFnPc2	branka
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
do	do	k7c2	do
31.12	[number]	k4	31.12
<g/>
.1990	.1990	k4	.1990
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
kanonýr	kanonýr	k1gMnSc1	kanonýr
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
střelce	střelec	k1gMnSc2	střelec
poválečné	poválečný	k2eAgFnSc2d1	poválečná
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
uznání	uznání	k1gNnSc4	uznání
čekal	čekat	k5eAaImAgMnS	čekat
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
Klubu	klub	k1gInSc2	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
GÓLU	gól	k1gInSc2	gól
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Fotbalista	fotbalista	k1gMnSc1	fotbalista
s	s	k7c7	s
českým	český	k2eAgNnSc7d1	české
občanstvím	občanství	k1gNnSc7	občanství
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vstřelí	vstřelit	k5eAaPmIp3nS	vstřelit
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
soutěžích	soutěž	k1gFnPc6	soutěž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
či	či	k8xC	či
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalista	fotbalista	k1gMnSc1	fotbalista
s	s	k7c7	s
československým	československý	k2eAgNnSc7d1	Československé
občanstvím	občanství	k1gNnSc7	občanství
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vstřelí	vstřelit	k5eAaPmIp3nS	vstřelit
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
soutěžích	soutěž	k1gFnPc6	soutěž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
či	či	k8xC	či
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
gólů	gól	k1gInPc2	gól
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
nebo	nebo	k8xC	nebo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Československo	Československo	k1gNnSc4	Československo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
L.	L.	kA	L.
Kubala	Kubala	k1gMnSc1	Kubala
nebo	nebo	k8xC	nebo
Ľ.	Ľ.	k1gMnSc1	Ľ.
Luhový	luhový	k2eAgMnSc1d1	luhový
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
splnili	splnit	k5eAaPmAgMnP	splnit
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Československo	Československo	k1gNnSc4	Československo
<g/>
;	;	kIx,	;
P.	P.	kA	P.
Diňa	Diňa	k1gMnSc1	Diňa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
splnil	splnit	k5eAaPmAgMnS	splnit
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
většinu	většina	k1gFnSc4	většina
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
majitel	majitel	k1gMnSc1	majitel
toto	tento	k3xDgNnSc4	tento
občanství	občanství	k1gNnSc4	občanství
pozbyl	pozbýt	k5eAaPmAgMnS	pozbýt
nebo	nebo	k8xC	nebo
znova	znova	k6eAd1	znova
nabyl	nabýt	k5eAaPmAgInS	nabýt
-	-	kIx~	-
příklad	příklad	k1gInSc1	příklad
A.	A.	kA	A.
Scherer	Scherer	k1gInSc1	Scherer
atd.	atd.	kA	atd.
Góly	gól	k1gInPc1	gól
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
před	před	k7c7	před
nabytím	nabytí	k1gNnSc7	nabytí
občanství	občanství	k1gNnSc2	občanství
se	se	k3xPyFc4	se
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
–	–	k?	–
např.	např.	kA	např.
Kubala	Kubala	k1gMnSc1	Kubala
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
(	(	kIx(	(
<g/>
nositel	nositel	k1gMnSc1	nositel
jiného	jiný	k2eAgNnSc2d1	jiné
občanství	občanství	k1gNnSc2	občanství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
nejméně	málo	k6eAd3	málo
100	[number]	k4	100
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
nebo	nebo	k8xC	nebo
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
např.	např.	kA	např.
R.	R.	kA	R.
Braine	Brain	k1gInSc5	Brain
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
údaje	údaj	k1gInPc1	údaj
platné	platný	k2eAgInPc1d1	platný
po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
nových	nový	k2eAgNnPc2d1	nové
pravidel	pravidlo	k1gNnPc2	pravidlo
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
KLK	klk	k1gInSc1	klk
Gólu	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
platné	platný	k2eAgInPc1d1	platný
k	k	k7c3	k
18.8	[number]	k4	18.8
<g/>
.2017	.2017	k4	.2017
<g/>
)	)	kIx)	)
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
označeni	označen	k2eAgMnPc1d1	označen
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnPc1d1	aktivní
hráči	hráč	k1gMnPc1	hráč
POZNÁMKA	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
U	u	k7c2	u
hráče	hráč	k1gMnSc2	hráč
Jiřího	Jiří	k1gMnSc2	Jiří
Sobotky	Sobotka	k1gMnSc2	Sobotka
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dohledat	dohledat	k5eAaPmF	dohledat
jeho	jeho	k3xOp3gFnPc4	jeho
případné	případný	k2eAgFnPc4d1	případná
prvoligové	prvoligový	k2eAgFnPc4d1	prvoligová
branky	branka	k1gFnPc4	branka
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
klubu	klub	k1gInSc6	klub
La	la	k1gNnSc1	la
Chaux-de-Fonds	Chauxe-Fonds	k1gInSc1	Chaux-de-Fonds
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnPc1d1	aktivní
prvoligoví	prvoligový	k2eAgMnPc1d1	prvoligový
hráči	hráč	k1gMnPc1	hráč
Seznam	seznam	k1gInSc4	seznam
některých	některý	k3yIgMnPc2	některý
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vstřelit	vstřelit	k5eAaPmF	vstřelit
100	[number]	k4	100
ligových	ligový	k2eAgInPc2d1	ligový
gólů	gól	k1gInPc2	gól
</s>
