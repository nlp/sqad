<s>
Klub	klub	k1gInSc1	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
GÓLU	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
klub	klub	k1gInSc1	klub
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
československých	československý	k2eAgMnPc2d1	československý
elitních	elitní	k2eAgMnPc2d1	elitní
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vstřelili	vstřelit	k5eAaPmAgMnP	vstřelit
alespoň	alespoň	k9	alespoň
100	[number]	k4	100
prvoligových	prvoligový	k2eAgInPc2d1	prvoligový
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
týdeníkem	týdeník	k1gInSc7	týdeník
Gól	gól	k1gInSc1	gól
<g/>
.	.	kIx.	.
</s>
