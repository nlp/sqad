<s>
Natalie	Natalie	k1gFnSc1	Natalie
Portmanová	Portmanová	k1gFnSc1	Portmanová
(	(	kIx(	(
<g/>
nepřechýleně	přechýleně	k6eNd1	přechýleně
Natalie	Natalie	k1gFnSc1	Natalie
Portman	Portman	k1gMnSc1	Portman
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
נ	נ	k?	נ
פ	פ	k?	פ
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Natalie	Natalie	k1gFnSc2	Natalie
Heršlag	Heršlaga	k1gFnPc2	Heršlaga
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
נ	נ	k?	נ
ה	ה	k?	ה
<g/>
;	;	kIx,	;
narozená	narozený	k2eAgFnSc1d1	narozená
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1981	[number]	k4	1981
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izraelsko-americká	izraelskomerický	k2eAgFnSc1d1	izraelsko-americký
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
filmový	filmový	k2eAgInSc1d1	filmový
debut	debut	k1gInSc1	debut
zažila	zažít	k5eAaPmAgFnS	zažít
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Léon	Léona	k1gFnPc2	Léona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
až	až	k9	až
především	především	k6eAd1	především
díky	díky	k7c3	díky
roli	role	k1gFnSc3	role
královny	královna	k1gFnSc2	královna
Padmé	Padmý	k2eAgMnPc4d1	Padmý
Amidaly	Amidal	k1gMnPc4	Amidal
v	v	k7c6	v
prequelové	prequelový	k2eAgFnSc6d1	prequelový
trilogii	trilogie	k1gFnSc6	trilogie
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
Natalie	Natalie	k1gFnSc2	Natalie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
"	"	kIx"	"
<g/>
raději	rád	k6eAd2	rád
budu	být	k5eAaImBp1nS	být
chytrá	chytrý	k2eAgFnSc1d1	chytrá
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dokončila	dokončit	k5eAaPmAgFnS	dokončit
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
studium	studium	k1gNnSc4	studium
psychologie	psychologie	k1gFnSc2	psychologie
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
získala	získat	k5eAaPmAgFnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgFnS	být
nominována	nominován	k2eAgFnSc1d1	nominována
i	i	k9	i
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
porotkyní	porotkyně	k1gFnSc7	porotkyně
na	na	k7c4	na
61	[number]	k4	61
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
režijní	režijní	k2eAgInSc1d1	režijní
debut	debut	k1gInSc1	debut
Eve	Eve	k1gFnPc2	Eve
otevřel	otevřít	k5eAaPmAgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
65	[number]	k4	65
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
Benátského	benátský	k2eAgInSc2d1	benátský
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Černá	černý	k2eAgFnSc1d1	černá
labuť	labuť	k1gFnSc1	labuť
a	a	k8xC	a
za	za	k7c4	za
tentýž	týž	k3xTgInSc4	týž
film	film	k1gInSc4	film
získala	získat	k5eAaPmAgFnS	získat
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
i	i	k9	i
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
Oscara	Oscara	k1gFnSc1	Oscara
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Natalie	Natalie	k1gFnSc1	Natalie
Portmanová	Portmanová	k1gFnSc1	Portmanová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Avner	Avner	k1gMnSc1	Avner
Hershlag	Hershlag	k1gMnSc1	Hershlag
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
izraelský	izraelský	k2eAgMnSc1d1	izraelský
lékař	lékař	k1gMnSc1	lékař
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
plodnost	plodnost	k1gFnSc4	plodnost
a	a	k8xC	a
reprodukční	reprodukční	k2eAgNnSc4d1	reprodukční
lékařství	lékařství	k1gNnSc4	lékařství
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c4	na
reprodukční	reprodukční	k2eAgFnSc4d1	reprodukční
endokrinologii	endokrinologie	k1gFnSc4	endokrinologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Shelley	Shelleum	k1gNnPc7	Shelleum
Stevens	Stevens	k1gInSc1	Stevens
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
její	její	k3xOp3gFnSc7	její
agentkou	agentka	k1gFnSc7	agentka
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
předci	předek	k1gMnPc1	předek
jejího	její	k3xOp3gMnSc4	její
otce	otec	k1gMnSc4	otec
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
podnikli	podniknout	k5eAaPmAgMnP	podniknout
aliju	aliju	k5eAaPmIp1nS	aliju
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
prarodiče	prarodič	k1gMnPc1	prarodič
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
ve	v	k7c6	v
vyhlazovacím	vyhlazovací	k2eAgInSc6d1	vyhlazovací
táboře	tábor	k1gInSc6	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
narozená	narozený	k2eAgFnSc1d1	narozená
prababička	prababička	k1gFnSc1	prababička
byla	být	k5eAaImAgFnS	být
britskou	britský	k2eAgFnSc7d1	britská
špionkou	špionka	k1gFnSc7	špionka
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
v	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
studentském	studentský	k2eAgNnSc6d1	studentské
centru	centrum	k1gNnSc6	centrum
na	na	k7c4	na
Ohio	Ohio	k1gNnSc4	Ohio
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
prodávala	prodávat	k5eAaImAgFnS	prodávat
lístky	lístek	k1gInPc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
si	se	k3xPyFc3	se
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
když	když	k8xS	když
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
o	o	k7c6	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
přijela	přijet	k5eAaPmAgFnS	přijet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
Portmanové	Portmanové	k2eAgInPc1d1	Portmanové
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
získal	získat	k5eAaPmAgMnS	získat
lékařské	lékařský	k2eAgNnSc4d1	lékařské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
nejprve	nejprve	k6eAd1	nejprve
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Portmanová	Portmanová	k1gFnSc1	Portmanová
chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
židovské	židovský	k2eAgFnSc2d1	židovská
denní	denní	k2eAgFnSc2d1	denní
školy	škola	k1gFnSc2	škola
Charlese	Charles	k1gMnSc2	Charles
E.	E.	kA	E.
Smitha	Smith	k1gMnSc2	Smith
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Connecticutu	Connecticut	k1gInSc2	Connecticut
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
natrvalo	natrvalo	k6eAd1	natrvalo
usadila	usadit	k5eAaPmAgFnS	usadit
na	na	k7c6	na
newyorském	newyorský	k2eAgInSc6d1	newyorský
Long	Long	k1gInSc1	Long
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Portmanová	Portmanová	k1gFnSc1	Portmanová
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
"	"	kIx"	"
<g/>
Státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
USA	USA	kA	USA
<g/>
)	)	kIx)	)
opravdu	opravdu	k6eAd1	opravdu
miluji	milovat	k5eAaImIp1nS	milovat
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
moje	můj	k3xOp1gNnSc1	můj
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
cítím	cítit	k5eAaImIp1nS	cítit
být	být	k5eAaImF	být
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
jedináček	jedináček	k1gMnSc1	jedináček
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
často	často	k6eAd1	často
bere	brát	k5eAaImIp3nS	brát
na	na	k7c4	na
premiéry	premiéra	k1gFnPc4	premiéra
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
nebyla	být	k5eNaImAgFnS	být
religiózně	religiózně	k6eAd1	religiózně
založená	založený	k2eAgFnSc1d1	založená
<g/>
,	,	kIx,	,
chodila	chodit	k5eAaImAgFnS	chodit
Portmanová	Portmanová	k1gFnSc1	Portmanová
do	do	k7c2	do
židovské	židovský	k2eAgFnSc2d1	židovská
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
Solomon	Solomon	k1gMnSc1	Solomon
Schechter	Schechter	k1gMnSc1	Schechter
Day	Day	k1gMnSc1	Day
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Glen	Glen	k1gNnSc4	Glen
Cove	Cov	k1gFnSc2	Cov
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Syosset	Syosset	k1gInSc1	Syosset
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
<s>
Nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
se	s	k7c7	s
premiéry	premiér	k1gMnPc7	premiér
Star	star	k1gFnSc4	star
Wars	Warsa	k1gFnPc2	Warsa
<g/>
:	:	kIx,	:
Epizoda	epizoda	k1gFnSc1	epizoda
I	I	kA	I
–	–	k?	–
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
učit	učit	k5eAaImF	učit
na	na	k7c4	na
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
promovala	promovat	k5eAaBmAgFnS	promovat
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
psychologické	psychologický	k2eAgFnSc6d1	psychologická
laboratoři	laboratoř	k1gFnSc6	laboratoř
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
asistentkou	asistentka	k1gFnSc7	asistentka
Alana	Alan	k1gMnSc2	Alan
Dershowitze	Dershowitze	k1gFnSc2	Dershowitze
(	(	kIx(	(
<g/>
děkuje	děkovat	k5eAaImIp3nS	děkovat
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnSc3	The
Case	Case	k1gFnSc3	Case
for	forum	k1gNnPc2	forum
Israel	Israel	k1gInSc1	Israel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
žila	žít	k5eAaImAgFnS	žít
v	v	k7c4	v
Lowell	Lowell	k1gInSc4	Lowell
House	house	k1gNnSc1	house
a	a	k8xC	a
napsala	napsat	k5eAaPmAgFnS	napsat
dopis	dopis	k1gInSc4	dopis
do	do	k7c2	do
studentských	studentský	k2eAgFnPc2d1	studentská
novin	novina	k1gFnPc2	novina
The	The	k1gFnSc1	The
Harvard	Harvard	k1gInSc1	Harvard
Crimson	Crimson	k1gInSc1	Crimson
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
protiizraelskou	protiizraelský	k2eAgFnSc4d1	protiizraelská
esej	esej	k1gFnSc4	esej
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
otiskly	otisknout	k5eAaPmAgFnP	otisknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2004	[number]	k4	2004
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
několik	několik	k4yIc4	několik
postgraduálních	postgraduální	k2eAgInPc2d1	postgraduální
kurzů	kurz	k1gInPc2	kurz
na	na	k7c6	na
Hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
hostující	hostující	k2eAgFnSc1d1	hostující
přednášející	přednášející	k1gFnSc1	přednášející
na	na	k7c6	na
Columbijské	Columbijský	k2eAgFnSc6d1	Columbijská
univerzitě	univerzita	k1gFnSc6	univerzita
na	na	k7c6	na
přednášce	přednáška	k1gFnSc6	přednáška
o	o	k7c6	o
terorismu	terorismus	k1gInSc6	terorismus
a	a	k8xC	a
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hovořila	hovořit	k5eAaImAgFnS	hovořit
o	o	k7c6	o
filmu	film	k1gInSc6	film
V	V	kA	V
jako	jako	k8xS	jako
Vendeta	vendeta	k1gFnSc1	vendeta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bilingvní	bilingvní	k2eAgNnSc1d1	bilingvní
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
studuje	studovat	k5eAaImIp3nS	studovat
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
japonštinu	japonština	k1gFnSc4	japonština
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
a	a	k8xC	a
arabštinu	arabština	k1gFnSc4	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
studentka	studentka	k1gFnSc1	studentka
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
výzkumných	výzkumný	k2eAgInPc6d1	výzkumný
pracích	prak	k1gInPc6	prak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
odborných	odborný	k2eAgNnPc6d1	odborné
vědeckých	vědecký	k2eAgNnPc6d1	vědecké
periodikách	periodikum	k1gNnPc6	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
středoškolskou	středoškolský	k2eAgFnSc7d1	středoškolská
prací	práce	k1gFnSc7	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
metoda	metoda	k1gFnSc1	metoda
k	k	k7c3	k
prokázání	prokázání	k1gNnSc3	prokázání
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
výroby	výroba	k1gFnSc2	výroba
vodíku	vodík	k1gInSc2	vodík
z	z	k7c2	z
cukru	cukr	k1gInSc2	cukr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
A	a	k9	a
Simple	Simple	k1gMnPc1	Simple
Method	Methoda	k1gFnPc2	Methoda
To	ten	k3xDgNnSc4	ten
Demonstrate	Demonstrat	k1gInSc5	Demonstrat
the	the	k?	the
Enzymatic	Enzymatice	k1gFnPc2	Enzymatice
Production	Production	k1gInSc4	Production
of	of	k?	of
Hydrogen	Hydrogen	k1gInSc1	Hydrogen
from	from	k1gMnSc1	from
Sugar	Sugar	k1gMnSc1	Sugar
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
soutěže	soutěž	k1gFnPc4	soutěž
Intel	Intel	kA	Intel
Science	Science	k1gFnSc1	Science
Talent	talent	k1gInSc1	talent
Search	Search	k1gInSc1	Search
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
psychologie	psychologie	k1gFnSc2	psychologie
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c4	na
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
paměti	paměť	k1gFnSc6	paměť
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Aktivace	aktivace	k1gFnSc1	aktivace
čelního	čelní	k2eAgInSc2d1	čelní
laloku	lalok	k1gInSc2	lalok
při	při	k7c6	při
stálosti	stálost	k1gFnSc6	stálost
objektu	objekt	k1gInSc2	objekt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Frontal	Frontal	k1gMnSc1	Frontal
Lobe	lob	k1gInSc5	lob
Activation	Activation	k1gInSc4	Activation
During	During	k1gInSc1	During
Object	Object	k1gInSc1	Object
Permanence	permanence	k1gFnSc1	permanence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dítě	dítě	k1gNnSc1	dítě
trávila	trávit	k5eAaImAgFnS	trávit
prázdniny	prázdniny	k1gFnPc4	prázdniny
na	na	k7c6	na
divadelních	divadelní	k2eAgInPc6d1	divadelní
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
muzikálu	muzikál	k1gInSc2	muzikál
uváděného	uváděný	k2eAgInSc2d1	uváděný
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
mimo	mimo	k7c4	mimo
Broadway	Broadwa	k1gMnPc4	Broadwa
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
jedenácti	jedenáct	k4xCc3	jedenáct
a	a	k8xC	a
půl	půl	k1xP	půl
letech	léto	k1gNnPc6	léto
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
2000	[number]	k4	2000
dalších	další	k2eAgFnPc2d1	další
uchazeček	uchazečka	k1gFnPc2	uchazečka
casting	casting	k1gInSc1	casting
k	k	k7c3	k
filmu	film	k1gInSc3	film
Léon	Léona	k1gFnPc2	Léona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Nelítostný	lítostný	k2eNgInSc1d1	nelítostný
souboj	souboj	k1gInSc1	souboj
<g/>
,	,	kIx,	,
Všichni	všechen	k3xTgMnPc1	všechen
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
:	:	kIx,	:
Miluji	milovat	k5eAaImIp1nS	milovat
tě	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Nádherný	nádherný	k2eAgInSc4d1	nádherný
holky	holka	k1gFnPc1	holka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
Padmé	Padmý	k2eAgMnPc4d1	Padmý
Amidaly	Amidal	k1gMnPc4	Amidal
v	v	k7c6	v
prequelové	prequelový	k2eAgFnSc6d1	prequelový
trilogii	trilogie	k1gFnSc6	trilogie
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
hereckou	herecký	k2eAgFnSc7d1	herecká
kariérou	kariéra	k1gFnSc7	kariéra
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc4	přednost
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
studovala	studovat	k5eAaImAgFnS	studovat
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
obor	obor	k1gInSc1	obor
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Procitnutí	procitnutí	k1gNnSc2	procitnutí
v	v	k7c6	v
Garden	Gardno	k1gNnPc2	Gardno
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
Hlavně	hlavně	k9	hlavně
nezávazně	závazně	k6eNd1	závazně
<g/>
,	,	kIx,	,
Na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
a	a	k8xC	a
V	V	kA	V
jako	jako	k8xC	jako
Vendeta	vendeta	k1gFnSc1	vendeta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
získala	získat	k5eAaPmAgFnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Černá	černý	k2eAgFnSc1d1	černá
labuť	labuť	k1gFnSc1	labuť
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
tanečníkem	tanečník	k1gMnSc7	tanečník
a	a	k8xC	a
choreografem	choreograf	k1gMnSc7	choreograf
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Millepiedem	Millepied	k1gMnSc7	Millepied
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
oznámili	oznámit	k5eAaPmAgMnP	oznámit
své	své	k1gNnSc4	své
zásnuby	zásnuba	k1gFnSc2	zásnuba
a	a	k8xC	a
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
prvorozeného	prvorozený	k2eAgMnSc4d1	prvorozený
syna	syn	k1gMnSc4	syn
Alepha	Aleph	k1gMnSc4	Aleph
Portman-Millepieda	Portman-Millepied	k1gMnSc4	Portman-Millepied
porodila	porodit	k5eAaPmAgFnS	porodit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Natalie	Natalie	k1gFnSc1	Natalie
a	a	k8xC	a
Benjamin	Benjamin	k1gMnSc1	Benjamin
vzali	vzít	k5eAaPmAgMnP	vzít
na	na	k7c6	na
soukromém	soukromý	k2eAgInSc6d1	soukromý
židovském	židovský	k2eAgInSc6d1	židovský
obřadu	obřad	k1gInSc6	obřad
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Big	Big	k1gFnSc2	Big
Sur	Sur	k1gFnSc2	Sur
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Amalia	Amalia	k1gFnSc1	Amalia
<g/>
.	.	kIx.	.
</s>
