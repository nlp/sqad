<s>
Apeniny	Apeniny	k1gFnPc1	Apeniny
jsou	být	k5eAaImIp3nP	být
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
procházející	procházející	k2eAgFnSc1d1	procházející
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
severozápad	severozápad	k1gInSc1	severozápad
–	–	k?	–
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvořící	tvořící	k2eAgNnSc1d1	tvořící
tak	tak	k6eAd1	tak
jakousi	jakýsi	k3yIgFnSc4	jakýsi
páteř	páteř	k1gFnSc4	páteř
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
1	[number]	k4	1
200	[number]	k4	200
km	km	kA	km
a	a	k8xC	a
šířku	šířka	k1gFnSc4	šířka
70	[number]	k4	70
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Corno	Corno	k6eAd1	Corno
Grande	grand	k1gMnSc5	grand
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
2	[number]	k4	2
912	[number]	k4	912
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnSc2	hora
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
zelené	zelený	k2eAgInPc1d1	zelený
a	a	k8xC	a
zalesněné	zalesněný	k2eAgInPc1d1	zalesněný
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
vrcholu	vrchol	k1gInSc2	vrchol
Gran	Gran	k1gMnSc1	Gran
Sasso	Sassa	k1gFnSc5	Sassa
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Ghiacciaio	Ghiacciaio	k6eAd1	Ghiacciaio
del	del	k?	del
Calderone	Calderon	k1gInSc5	Calderon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
strana	strana	k1gFnSc1	strana
hor	hora	k1gFnPc2	hora
směřující	směřující	k2eAgFnSc1d1	směřující
k	k	k7c3	k
Jaderskému	jaderský	k2eAgNnSc3d1	Jaderské
moři	moře	k1gNnSc3	moře
je	být	k5eAaImIp3nS	být
strmá	strmý	k2eAgFnSc1d1	strmá
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
západní	západní	k2eAgInPc1d1	západní
svahy	svah	k1gInPc1	svah
tvoří	tvořit	k5eAaImIp3nP	tvořit
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
celý	celý	k2eAgInSc1d1	celý
Apeninský	apeninský	k2eAgInSc1d1	apeninský
poloostrov	poloostrov	k1gInSc1	poloostrov
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Montes	Montes	k1gMnSc1	Montes
Apenninus	Apenninus	k1gMnSc1	Apenninus
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Apeninský	apeninský	k2eAgInSc1d1	apeninský
řetězec	řetězec	k1gInSc1	řetězec
začíná	začínat	k5eAaImIp3nS	začínat
severozápadním	severozápadní	k2eAgNnSc7d1	severozápadní
sedlem	sedlo	k1gNnSc7	sedlo
Colli	Colle	k1gFnSc4	Colle
di	di	k?	di
Cadibona	Cadibona	k1gFnSc1	Cadibona
(	(	kIx(	(
<g/>
435	[number]	k4	435
m	m	kA	m
<g/>
)	)	kIx)	)
nedaleko	nedaleko	k7c2	nedaleko
přímořského	přímořský	k2eAgNnSc2d1	přímořské
města	město	k1gNnSc2	město
Savona	Savona	k1gFnSc1	Savona
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
u	u	k7c2	u
Janovského	Janovského	k2eAgInSc2d1	Janovského
zálivu	záliv	k1gInSc2	záliv
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
poslední	poslední	k2eAgInPc4d1	poslední
západoalpské	západoalpský	k2eAgInPc4d1	západoalpský
výběžky	výběžek	k1gInPc4	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
vzdáleném	vzdálený	k2eAgMnSc6d1	vzdálený
přes	přes	k7c4	přes
1	[number]	k4	1
200	[number]	k4	200
km	km	kA	km
<g/>
,	,	kIx,	,
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
Apeniny	Apeniny	k1gFnPc4	Apeniny
Messinská	messinský	k2eAgFnSc1d1	Messinská
šíje	šíje	k1gFnSc1	šíje
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
Apenin	Apeniny	k1gFnPc2	Apeniny
bývají	bývat	k5eAaImIp3nP	bývat
považována	považován	k2eAgNnPc1d1	považováno
veškerá	veškerý	k3xTgNnPc1	veškerý
sicilská	sicilský	k2eAgNnPc1d1	sicilské
pohoří	pohoří	k1gNnPc1	pohoří
včetně	včetně	k7c2	včetně
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
činné	činný	k2eAgFnSc2d1	činná
evropské	evropský	k2eAgFnSc2d1	Evropská
sopky	sopka	k1gFnSc2	sopka
Etny	Etna	k1gFnSc2	Etna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
apeninské	apeninský	k2eAgInPc1d1	apeninský
výběžky	výběžek	k1gInPc1	výběžek
Ligurského	ligurský	k2eAgNnSc2d1	Ligurské
<g/>
,	,	kIx,	,
jižněji	jižně	k6eAd2	jižně
Tyrhénského	tyrhénský	k2eAgNnSc2d1	Tyrhénské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Jadran	Jadran	k1gInSc1	Jadran
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
Jónské	jónský	k2eAgNnSc1d1	Jónské
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
vlastního	vlastní	k2eAgInSc2d1	vlastní
horského	horský	k2eAgInSc2d1	horský
hřebene	hřeben	k1gInSc2	hřeben
činí	činit	k5eAaImIp3nS	činit
40	[number]	k4	40
–	–	k?	–
60	[number]	k4	60
km	km	kA	km
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Neapole	Neapol	k1gFnSc2	Neapol
dokonce	dokonce	k9	dokonce
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
mívají	mívat	k5eAaImIp3nP	mívat
apeninské	apeninský	k2eAgInPc1d1	apeninský
hřebeny	hřeben	k1gInPc1	hřeben
víceméně	víceméně	k9	víceméně
paralelní	paralelní	k2eAgFnSc3d1	paralelní
orientaci	orientace	k1gFnSc3	orientace
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
rys	rys	k1gInSc4	rys
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Apeniny	Apeniny	k1gFnPc1	Apeniny
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
namířeného	namířený	k2eAgInSc2d1	namířený
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
masiv	masiv	k1gInSc1	masiv
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
sourodý	sourodý	k2eAgMnSc1d1	sourodý
a	a	k8xC	a
souvislý	souvislý	k2eAgMnSc1d1	souvislý
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
samostatných	samostatný	k2eAgFnPc2d1	samostatná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Apeninů	Apenin	k1gInPc2	Apenin
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
stovky	stovka	k1gFnPc1	stovka
horských	horský	k2eAgInPc2d1	horský
hřbetů	hřbet	k1gInPc2	hřbet
a	a	k8xC	a
samostatných	samostatný	k2eAgInPc2d1	samostatný
hor.	hor.	k?	hor.
Od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
:	:	kIx,	:
Severní	severní	k2eAgFnPc1d1	severní
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
settentrionale	settentrionale	k6eAd1	settentrionale
<g/>
)	)	kIx)	)
Ligurské	ligurský	k2eAgFnPc1d1	Ligurská
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
ligure	ligur	k1gMnSc5	ligur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Monte	Mont	k1gInSc5	Mont
Maggiorasca	Maggiorascum	k1gNnPc1	Maggiorascum
(	(	kIx(	(
<g/>
1	[number]	k4	1
804	[number]	k4	804
m	m	kA	m
<g/>
)	)	kIx)	)
Toskánsko-emiliánské	Toskánskomiliánský	k2eAgFnSc2d1	Toskánsko-emiliánský
<g />
.	.	kIx.	.
</s>
<s>
Apeniny	Apeniny	k1gFnPc1	Apeniny
nebo	nebo	k8xC	nebo
Etruské	etruský	k2eAgFnPc1d1	etruská
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
tosco-emiliano	toscomiliana	k1gFnSc5	tosco-emiliana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Cimone	Cimon	k1gMnSc5	Cimon
(	(	kIx(	(
<g/>
2	[number]	k4	2
165	[number]	k4	165
m	m	kA	m
<g/>
)	)	kIx)	)
Střední	střední	k2eAgFnPc1d1	střední
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
centrale	centrale	k6eAd1	centrale
<g/>
)	)	kIx)	)
Umbrické	Umbrický	k2eAgFnPc1d1	Umbrický
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
umbro-marchigiano	umbroarchigiana	k1gFnSc5	umbro-marchigiana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Vettore	Vettor	k1gMnSc5	Vettor
(	(	kIx(	(
<g/>
2	[number]	k4	2
476	[number]	k4	476
m	m	kA	m
<g/>
)	)	kIx)	)
Abruzské	Abruzský	k2eAgFnPc4d1	Abruzský
Apeniny	Apeniny	k1gFnPc4	Apeniny
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Appennino	Appennina	k1gFnSc5	Appennina
abruzzese	abruzzes	k1gInSc6	abruzzes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Corno	Corno	k6eAd1	Corno
Grande	grand	k1gMnSc5	grand
(	(	kIx(	(
<g/>
2	[number]	k4	2
912	[number]	k4	912
m	m	kA	m
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgFnSc1d1	jižní
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
meridionale	meridionale	k6eAd1	meridionale
<g/>
)	)	kIx)	)
Samnitské	Samnitský	k2eAgFnPc1d1	Samnitský
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
sannita	sannita	k1gFnSc1	sannita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Miletto	Miletta	k1gMnSc5	Miletta
(	(	kIx(	(
<g/>
2	[number]	k4	2
050	[number]	k4	050
m	m	kA	m
<g/>
)	)	kIx)	)
Kampánské	Kampánský	k2eAgFnPc1d1	Kampánský
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
campano	campana	k1gFnSc5	campana
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Cervialto	Cervialta	k1gMnSc5	Cervialta
(	(	kIx(	(
<g/>
1	[number]	k4	1
810	[number]	k4	810
m	m	kA	m
<g/>
)	)	kIx)	)
Lukánské	Lukánský	k2eAgFnPc1d1	Lukánský
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
lucano	lucana	k1gFnSc5	lucana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Serra	Serra	k1gMnSc1	Serra
Dolcedorme	Dolcedorm	k1gInSc5	Dolcedorm
(	(	kIx(	(
<g/>
2	[number]	k4	2
267	[number]	k4	267
m	m	kA	m
<g/>
)	)	kIx)	)
Kalabrijské	Kalabrijský	k2eAgFnPc1d1	Kalabrijský
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
calabro	calabro	k1gNnSc1	calabro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Serra	Serra	k1gMnSc1	Serra
Dolcedorme	Dolcedorm	k1gInSc5	Dolcedorm
(	(	kIx(	(
<g/>
2	[number]	k4	2
267	[number]	k4	267
m	m	kA	m
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Sicilské	sicilský	k2eAgFnPc1d1	sicilská
Apeniny	Apeniny	k1gFnPc1	Apeniny
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Appennino	Appennin	k2eAgNnSc1d1	Appennino
Siculo	Sicula	k1gFnSc5	Sicula
<g/>
)	)	kIx)	)
Pohoří	pohořet	k5eAaPmIp3nP	pohořet
Peloritani	Peloritan	k1gMnPc1	Peloritan
<g/>
,	,	kIx,	,
Nebrodi	Nebrod	k1gMnPc1	Nebrod
a	a	k8xC	a
Madonie	Madonie	k1gFnPc1	Madonie
<g/>
,	,	kIx,	,
Pizzo	pizza	k1gFnSc5	pizza
Carbonara	Carbonar	k1gMnSc2	Carbonar
(	(	kIx(	(
<g/>
1	[number]	k4	1
979	[number]	k4	979
m	m	kA	m
<g/>
)	)	kIx)	)
Subapeniny	Subapenin	k2eAgFnSc2d1	Subapenin
a	a	k8xC	a
Antiapeniny	Antiapenin	k2eAgFnSc2d1	Antiapenin
(	(	kIx(	(
<g/>
Subappennino	Subappennin	k2eAgNnSc1d1	Subappennin
e	e	k0	e
Antiappennino	Antiappennina	k1gFnSc5	Antiappennina
<g/>
)	)	kIx)	)
Subapeniny	Subapenina	k1gFnPc1	Subapenina
jsou	být	k5eAaImIp3nP	být
samostatná	samostatný	k2eAgNnPc4d1	samostatné
pohoří	pohoří	k1gNnPc4	pohoří
a	a	k8xC	a
vrchoviny	vrchovina	k1gFnPc4	vrchovina
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
Apeninami	Apeniny	k1gFnPc7	Apeniny
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
středozápadní	středozápadní	k2eAgFnSc6d1	středozápadní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvíce	hodně	k6eAd3	hodně
známému	známý	k2eAgNnSc3d1	známé
pohoří	pohoří	k1gNnSc3	pohoří
Subapenin	Subapenina	k1gFnPc2	Subapenina
náleží	náležet	k5eAaImIp3nP	náležet
Apuánské	Apuánský	k2eAgFnPc1d1	Apuánský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
Antiapeniny	Antiapenina	k1gFnPc1	Antiapenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
řetězci	řetězec	k1gInSc6	řetězec
Apenin	Apeniny	k1gFnPc2	Apeniny
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
například	například	k6eAd1	například
toskánský	toskánský	k2eAgInSc4d1	toskánský
krystalický	krystalický	k2eAgInSc4d1	krystalický
masiv	masiv	k1gInSc4	masiv
Colline	Collin	k1gInSc5	Collin
Metallifere	Metallifer	k1gInSc5	Metallifer
nebo	nebo	k8xC	nebo
vrchovina	vrchovina	k1gFnSc1	vrchovina
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Gargano	Gargana	k1gFnSc5	Gargana
<g/>
.	.	kIx.	.
</s>
<s>
Apeniny	Apeniny	k1gFnPc1	Apeniny
se	se	k3xPyFc4	se
formovaly	formovat	k5eAaImAgFnP	formovat
zejména	zejména	k9	zejména
v	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
bývala	bývat	k5eAaImAgNnP	bývat
kdysi	kdysi	k6eAd1	kdysi
součástí	součást	k1gFnSc7	součást
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Středozemí	středozemí	k1gNnSc2	středozemí
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
tektonické	tektonický	k2eAgFnSc6d1	tektonická
stránce	stránka	k1gFnSc6	stránka
dodnes	dodnes	k6eAd1	dodnes
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
nejvíc	nejvíc	k6eAd1	nejvíc
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
bohatá	bohatý	k2eAgFnSc1d1	bohatá
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
vydatná	vydatný	k2eAgNnPc1d1	vydatné
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gInPc7	její
nejznámějšími	známý	k2eAgInPc7d3	nejznámější
projevy	projev	k1gInPc7	projev
jsou	být	k5eAaImIp3nP	být
Flegrejská	Flegrejský	k2eAgFnSc1d1	Flegrejský
pole	pole	k1gFnSc1	pole
a	a	k8xC	a
Solfatara	Solfatara	k1gFnSc1	Solfatara
u	u	k7c2	u
Neapole	Neapol	k1gFnSc2	Neapol
nebo	nebo	k8xC	nebo
jediný	jediný	k2eAgInSc4d1	jediný
dosud	dosud	k6eAd1	dosud
činný	činný	k2eAgInSc4d1	činný
vulkán	vulkán	k1gInSc4	vulkán
kontinentu	kontinent	k1gInSc2	kontinent
Vesuv	Vesuv	k1gInSc1	Vesuv
(	(	kIx(	(
<g/>
1	[number]	k4	1
281	[number]	k4	281
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Apuánské	Apuánský	k2eAgFnPc1d1	Apuánský
Alpy	Alpy	k1gFnPc1	Alpy
jsou	být	k5eAaImIp3nP	být
vápencové	vápencový	k2eAgInPc1d1	vápencový
a	a	k8xC	a
nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Antro	Antro	k1gNnSc1	Antro
del	del	k?	del
Corchia	Corchius	k1gMnSc2	Corchius
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejhlubších	hluboký	k2eAgFnPc2d3	nejhlubší
propasťovitých	propasťovitý	k2eAgFnPc2d1	propasťovitá
jeskyní	jeskyně	k1gFnPc2	jeskyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
Apenin	Apeniny	k1gFnPc2	Apeniny
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
ho	on	k3xPp3gInSc4	on
poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Středozemí	Středozem	k1gFnPc2	Středozem
a	a	k8xC	a
účinné	účinný	k2eAgNnSc1d1	účinné
odclonění	odclonění	k1gNnSc1	odclonění
od	od	k7c2	od
evropského	evropský	k2eAgInSc2d1	evropský
severu	sever	k1gInSc2	sever
masivem	masiv	k1gInSc7	masiv
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
poledníková	poledníkový	k2eAgFnSc1d1	Poledníková
orientace	orientace	k1gFnSc1	orientace
a	a	k8xC	a
značná	značný	k2eAgFnSc1d1	značná
délka	délka	k1gFnSc1	délka
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
srážek	srážka	k1gFnPc2	srážka
ubývá	ubývat	k5eAaImIp3nS	ubývat
a	a	k8xC	a
teploty	teplota	k1gFnPc1	teplota
vzrůstají	vzrůstat	k5eAaImIp3nP	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
na	na	k7c6	na
západě	západ	k1gInSc6	západ
významně	významně	k6eAd1	významně
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
úhrny	úhrn	k1gInPc4	úhrn
z	z	k7c2	z
jaderské	jaderský	k2eAgFnSc2d1	Jaderská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnPc1d1	letní
noci	noc	k1gFnPc1	noc
bývají	bývat	k5eAaImIp3nP	bývat
u	u	k7c2	u
vrcholů	vrchol	k1gInPc2	vrchol
studené	studený	k2eAgFnPc1d1	studená
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
prudké	prudký	k2eAgFnPc1d1	prudká
a	a	k8xC	a
vydatné	vydatný	k2eAgFnPc1d1	vydatná
odpolední	odpolední	k2eAgFnPc1d1	odpolední
bouřky	bouřka	k1gFnPc1	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návětrných	návětrný	k2eAgInPc6d1	návětrný
svazích	svah	k1gInPc6	svah
středních	střední	k2eAgFnPc2d1	střední
Apenin	Apeniny	k1gFnPc2	Apeniny
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
2	[number]	k4	2
000	[number]	k4	000
mm	mm	kA	mm
<g/>
,	,	kIx,	,
východ	východ	k1gInSc1	východ
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
několikanásobně	několikanásobně	k6eAd1	několikanásobně
menší	malý	k2eAgFnPc4d2	menší
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
hor	hora	k1gFnPc2	hora
padá	padat	k5eAaImIp3nS	padat
nejvích	nejev	k1gFnPc6	nejev
srážek	srážka	k1gFnPc2	srážka
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
prší	pršet	k5eAaImIp3nS	pršet
nebo	nebo	k8xC	nebo
sněží	sněžit	k5eAaImIp3nS	sněžit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholových	vrcholový	k2eAgFnPc6d1	vrcholová
polohách	poloha	k1gFnPc6	poloha
sníh	sníh	k1gInSc4	sníh
leží	ležet	k5eAaImIp3nS	ležet
až	až	k9	až
devět	devět	k4xCc1	devět
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
firnová	firnový	k2eAgNnPc1d1	firnové
pole	pole	k1gNnPc1	pole
a	a	k8xC	a
sněžníky	sněžník	k1gInPc1	sněžník
neroztávají	roztávat	k5eNaImIp3nP	roztávat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sněhové	sněhový	k2eAgFnPc1d1	sněhová
nadílky	nadílka	k1gFnPc1	nadílka
využívají	využívat	k5eAaImIp3nP	využívat
a	a	k8xC	a
provozovazelé	provozovazelý	k2eAgFnPc1d1	provozovazelý
lyžařských	lyžařský	k2eAgNnPc2d1	lyžařské
center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
magistrál	magistrála	k1gFnPc2	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Lyžovat	lyžovat	k5eAaImF	lyžovat
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
i	i	k9	i
pár	pár	k4xCyI	pár
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
V	v	k7c6	v
Apeninách	Apeniny	k1gFnPc6	Apeniny
pramení	pramenit	k5eAaImIp3nS	pramenit
tisíce	tisíc	k4xCgInPc4	tisíc
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
periodický	periodický	k2eAgInSc4d1	periodický
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
vysychají	vysychat	k5eAaImIp3nP	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
trvalých	trvalý	k2eAgFnPc2d1	trvalá
řek	řeka	k1gFnPc2	řeka
stékají	stékat	k5eAaImIp3nP	stékat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
např.	např.	kA	např.
Arno	Arno	k1gNnSc1	Arno
<g/>
,	,	kIx,	,
Ombrone	Ombron	k1gMnSc5	Ombron
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
Tibera	Tibera	k1gFnSc1	Tibera
<g/>
,	,	kIx,	,
Volturno	Volturno	k1gNnSc1	Volturno
nebo	nebo	k8xC	nebo
Savuto	Savut	k2eAgNnSc1d1	Savut
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vod	voda	k1gFnPc2	voda
Jadranu	Jadran	k1gInSc2	Jadran
tečou	téct	k5eAaImIp3nP	téct
Tornaro	Tornara	k1gFnSc5	Tornara
<g/>
,	,	kIx,	,
Panaro	Panara	k1gFnSc5	Panara
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
pak	pak	k6eAd1	pak
Reno	Reno	k6eAd1	Reno
<g/>
,	,	kIx,	,
Metáuro	Metáura	k1gFnSc5	Metáura
<g/>
,	,	kIx,	,
Pescara	Pescar	k1gMnSc4	Pescar
<g/>
,	,	kIx,	,
Sangro	Sangra	k1gFnSc5	Sangra
<g/>
,	,	kIx,	,
Bradano	Bradana	k1gFnSc5	Bradana
či	či	k8xC	či
Neto	Neto	k1gMnSc1	Neto
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc4	jezero
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
alpské	alpský	k2eAgFnSc2d1	alpská
a	a	k8xC	a
podalpské	podalpský	k2eAgFnSc2d1	podalpská
oblasti	oblast	k1gFnSc2	oblast
není	být	k5eNaImIp3nS	být
apeninská	apeninský	k2eAgFnSc1d1	apeninská
Itálie	Itálie	k1gFnSc1	Itálie
příliš	příliš	k6eAd1	příliš
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
přirozená	přirozený	k2eAgNnPc4d1	přirozené
jezera	jezero	k1gNnPc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
Lago	Lago	k6eAd1	Lago
Trasimeno	Trasimen	k2eAgNnSc1d1	Trasimeno
<g/>
,	,	kIx,	,
Lago	Lago	k6eAd1	Lago
di	di	k?	di
Bolsana	Bolsana	k1gFnSc1	Bolsana
a	a	k8xC	a
Lago	Lago	k1gNnSc1	Lago
di	di	k?	di
Bracciano	Bracciana	k1gFnSc5	Bracciana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tektonicky	tektonicky	k6eAd1	tektonicky
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc4	dva
zbývající	zbývající	k2eAgFnPc4d1	zbývající
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc1	původ
sopečný	sopečný	k2eAgInSc1d1	sopečný
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
vzácněji	vzácně	k6eAd2	vzácně
jsou	být	k5eAaImIp3nP	být
zastoupena	zastoupen	k2eAgNnPc4d1	zastoupeno
jezera	jezero	k1gNnPc4	jezero
krasová	krasový	k2eAgNnPc4d1	krasové
nebo	nebo	k8xC	nebo
ledovcová	ledovcový	k2eAgNnPc4d1	ledovcové
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
některé	některý	k3yIgInPc4	některý
kary	kar	k1gInPc4	kar
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
apeninských	apeninský	k2eAgFnPc6d1	apeninská
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
umělých	umělý	k2eAgFnPc2d1	umělá
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgFnSc1d2	bohatší
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc4	sníh
a	a	k8xC	a
Ledovec	ledovec	k1gInSc4	ledovec
Určitou	určitý	k2eAgFnSc4d1	určitá
glaciologickou	glaciologický	k2eAgFnSc4d1	glaciologický
kuriozitu	kuriozita	k1gFnSc4	kuriozita
Gran	Grana	k1gFnPc2	Grana
Sasso	Sassa	k1gFnSc5	Sassa
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Italia	Italia	k1gFnSc1	Italia
představuje	představovat	k5eAaImIp3nS	představovat
menší	malý	k2eAgInSc1d2	menší
firnový	firnový	k2eAgInSc1d1	firnový
ledovec	ledovec	k1gInSc1	ledovec
Ghiaciaio	Ghiaciaio	k1gMnSc1	Ghiaciaio
del	del	k?	del
Calderone	Calderon	k1gMnSc5	Calderon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
nejjižnějším	jižní	k2eAgMnSc7d3	nejjižnější
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vrcholového	vrcholový	k2eAgInSc2d1	vrcholový
kříže	kříž	k1gInSc2	kříž
hory	hora	k1gFnSc2	hora
Corno	Corno	k6eAd1	Corno
Grande	grand	k1gMnSc5	grand
připomíná	připomínat	k5eAaImIp3nS	připomínat
trochu	trochu	k6eAd1	trochu
větší	veliký	k2eAgInSc4d2	veliký
sněžník	sněžník	k1gInSc4	sněžník
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
se	se	k3xPyFc4	se
však	však	k9	však
pochyby	pochyba	k1gFnPc1	pochyba
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Středoapeninská	Středoapeninský	k2eAgFnSc1d1	Středoapeninský
hranice	hranice	k1gFnSc1	hranice
trvalého	trvalý	k2eAgInSc2d1	trvalý
sněhu	sníh	k1gInSc2	sníh
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
2	[number]	k4	2
900	[number]	k4	900
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
proto	proto	k6eAd1	proto
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
překročit	překročit	k5eAaPmF	překročit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Flora	Flora	k6eAd1	Flora
Ze	z	k7c2	z
skvostů	skvost	k1gInPc2	skvost
centrálních	centrální	k2eAgInPc2d1	centrální
velehorských	velehorský	k2eAgInPc2d1	velehorský
vápenců	vápenec	k1gInPc2	vápenec
připomeňme	připomenout	k5eAaPmRp1nP	připomenout
alespoň	alespoň	k9	alespoň
zlatokvětý	zlatokvětý	k2eAgInSc4d1	zlatokvětý
hlaváček	hlaváček	k1gInSc4	hlaváček
<g/>
,	,	kIx,	,
nenápadný	nápadný	k2eNgInSc4d1	nenápadný
penízek	penízek	k1gInSc4	penízek
u	u	k7c2	u
sněhových	sněhový	k2eAgFnPc2d1	sněhová
výležisek	výležiska	k1gFnPc2	výležiska
<g/>
,	,	kIx,	,
polštáře	polštář	k1gInPc4	polštář
vytvářející	vytvářející	k2eAgInSc4d1	vytvářející
plstnatý	plstnatý	k2eAgInSc4d1	plstnatý
rožec	rožec	k1gInSc4	rožec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
typickou	typický	k2eAgFnSc7d1	typická
rostlinou	rostlina	k1gFnSc7	rostlina
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
žlutý	žlutý	k2eAgInSc1d1	žlutý
mák	mák	k1gInSc1	mák
<g/>
.	.	kIx.	.
<g/>
Až	až	k6eAd1	až
pod	pod	k7c7	pod
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
apeninskou	apeninský	k2eAgFnSc7d1	apeninská
horou	hora	k1gFnSc7	hora
rostou	růst	k5eAaImIp3nP	růst
vzácné	vzácný	k2eAgFnPc4d1	vzácná
fialové	fialový	k2eAgFnPc4d1	fialová
a	a	k8xC	a
žluté	žlutý	k2eAgFnPc4d1	žlutá
violky	violka	k1gFnPc4	violka
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
Také	také	k9	také
původní	původní	k2eAgFnSc1d1	původní
apeninská	apeninský	k2eAgFnSc1d1	apeninská
fauna	fauna	k1gFnSc1	fauna
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
pestrá	pestrý	k2eAgFnSc1d1	pestrá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
horských	horský	k2eAgInPc2d1	horský
lesů	les	k1gInPc2	les
dosud	dosud	k6eAd1	dosud
přežívají	přežívat	k5eAaImIp3nP	přežívat
zbytky	zbytek	k1gInPc1	zbytek
vlčích	vlčí	k2eAgFnPc2d1	vlčí
smeček	smečka	k1gFnPc2	smečka
nebo	nebo	k8xC	nebo
samotářští	samotářský	k2eAgMnPc1d1	samotářský
medvědi	medvěd	k1gMnPc1	medvěd
(	(	kIx(	(
<g/>
Abruzzy	Abruzz	k1gInPc1	Abruzz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trochu	trochu	k6eAd1	trochu
hojnější	hojný	k2eAgFnPc1d2	hojnější
jsou	být	k5eAaImIp3nP	být
divoké	divoký	k2eAgFnPc1d1	divoká
kočky	kočka	k1gFnPc1	kočka
(	(	kIx(	(
<g/>
poddruh	poddruh	k1gInSc1	poddruh
Felis	Felis	k1gFnSc2	Felis
silvestris	silvestris	k1gFnSc1	silvestris
molisana	molisana	k1gFnSc1	molisana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divočáci	divočák	k1gMnPc1	divočák
<g/>
,	,	kIx,	,
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
daňkové	daněk	k1gMnPc1	daněk
a	a	k8xC	a
srnci	srnec	k1gMnPc1	srnec
<g/>
.	.	kIx.	.
</s>
<s>
Drobnější	drobný	k2eAgMnPc4d2	drobnější
savce	savec	k1gMnPc4	savec
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
např.	např.	kA	např.
kuna	kuna	k1gFnSc1	kuna
<g/>
,	,	kIx,	,
vydra	vydra	k1gFnSc1	vydra
a	a	k8xC	a
jezevec	jezevec	k1gMnSc1	jezevec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplých	teplý	k2eAgInPc6d1	teplý
porostech	porost	k1gInPc6	porost
nižších	nízký	k2eAgFnPc2d2	nižší
poloh	poloha	k1gFnPc2	poloha
lze	lze	k6eAd1	lze
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
zastihnout	zastihnout	k5eAaPmF	zastihnout
dokonce	dokonce	k9	dokonce
dikobraza	dikobraz	k1gMnSc2	dikobraz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sněhových	sněhový	k2eAgFnPc2d1	sněhová
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
pod	pod	k7c7	pod
horskými	horský	k2eAgInPc7d1	horský
vrcholy	vrchol	k1gInPc7	vrchol
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
malá	malý	k2eAgNnPc4d1	malé
stáda	stádo	k1gNnPc4	stádo
původních	původní	k2eAgMnPc2d1	původní
kamzíků	kamzík	k1gMnPc2	kamzík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dravých	dravý	k2eAgMnPc2d1	dravý
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
supi	sup	k1gMnPc1	sup
<g/>
,	,	kIx,	,
sokoli	sokol	k1gMnPc1	sokol
a	a	k8xC	a
stále	stále	k6eAd1	stále
vzácnější	vzácný	k2eAgMnPc1d2	vzácnější
orli	orel	k1gMnPc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
ptáci	pták	k1gMnPc1	pták
jako	jako	k8xC	jako
kavčata	kavče	k1gNnPc1	kavče
<g/>
,	,	kIx,	,
pěnkaváci	pěnkavák	k1gMnPc1	pěnkavák
a	a	k8xC	a
zedníčkové	zedníček	k1gMnPc1	zedníček
skalní	skalní	k2eAgMnPc1d1	skalní
<g/>
.	.	kIx.	.
</s>
<s>
Floru	Flor	k1gMnSc3	Flor
a	a	k8xC	a
faunu	faun	k1gMnSc3	faun
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
snaží	snažit	k5eAaImIp3nP	snažit
zachovat	zachovat	k5eAaPmF	zachovat
zdárně	zdárně	k6eAd1	zdárně
se	se	k3xPyFc4	se
rozrůstající	rozrůstající	k2eAgFnSc1d1	rozrůstající
síť	síť	k1gFnSc1	síť
apeninských	apeninský	k2eAgNnPc2d1	Apeninské
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
(	(	kIx(	(
<g/>
Abruzzo	Abruzza	k1gFnSc5	Abruzza
<g/>
,	,	kIx,	,
Calabria	Calabrium	k1gNnSc2	Calabrium
<g/>
,	,	kIx,	,
Sibillini	Sibillin	k2eAgMnPc1d1	Sibillin
a	a	k8xC	a
Gran	Gran	k1gMnSc1	Gran
Sasso	Sassa	k1gFnSc5	Sassa
e	e	k0	e
Monti	Mont	k1gMnPc1	Mont
della	della	k1gFnSc1	della
Laga	Laga	k1gFnSc1	Laga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInPc1d1	regionální
přírodní	přírodní	k2eAgInPc1d1	přírodní
parky	park	k1gInPc1	park
(	(	kIx(	(
<g/>
Alpi	Alp	k1gMnSc5	Alp
Apuane	Apuan	k1gMnSc5	Apuan
<g/>
,	,	kIx,	,
Pollino	Pollina	k1gFnSc5	Pollina
<g/>
,	,	kIx,	,
Montii	Montie	k1gFnSc3	Montie
Simbruini	Simbruin	k1gMnPc5	Simbruin
<g/>
)	)	kIx)	)
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
maloplošných	maloplošný	k2eAgFnPc2d1	maloplošná
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1	kolonizace
Apenin	Apeniny	k1gFnPc2	Apeniny
nerozlučně	rozlučně	k6eNd1	rozlučně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
osídlováním	osídlování	k1gNnSc7	osídlování
celého	celý	k2eAgInSc2d1	celý
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
historie	historie	k1gFnSc1	historie
sahá	sahat	k5eAaImIp3nS	sahat
daleko	daleko	k6eAd1	daleko
před	před	k7c4	před
začátek	začátek	k1gInSc4	začátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Prolnulo	prolnout	k5eAaPmAgNnS	prolnout
a	a	k8xC	a
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nesčetně	sčetně	k6eNd1	sčetně
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
kuturních	kuturní	k2eAgInPc2d1	kuturní
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Připomeňme	připomenout	k5eAaPmRp1nP	připomenout
alespoň	alespoň	k9	alespoň
Ligury	Ligura	k1gFnPc4	Ligura
<g/>
,	,	kIx,	,
Kelty	Kelt	k1gMnPc4	Kelt
nebo	nebo	k8xC	nebo
Etrusky	Etrusk	k1gMnPc4	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
apeninské	apeninský	k2eAgFnSc2d1	apeninská
oblasti	oblast	k1gFnSc2	oblast
pochopitelně	pochopitelně	k6eAd1	pochopitelně
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
a	a	k8xC	a
raný	raný	k2eAgInSc1d1	raný
i	i	k8xC	i
pozdější	pozdní	k2eAgInSc1d2	pozdější
středověk	středověk	k1gInSc1	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInPc4d1	stavební
styly	styl	k1gInPc4	styl
románský	románský	k2eAgInSc1d1	románský
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgInSc1d1	gotický
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInSc1d1	renesanční
i	i	k8xC	i
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Itálii	Itálie	k1gFnSc3	Itálie
poprvé	poprvé	k6eAd1	poprvé
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
Garibaldi	Garibald	k1gMnPc1	Garibald
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
500	[number]	k4	500
m	m	kA	m
uvádějí	uvádět	k5eAaImIp3nP	uvádět
statistiky	statistika	k1gFnPc1	statistika
ještě	ještě	k6eAd1	ještě
padesát	padesát	k4xCc4	padesát
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
1	[number]	k4	1
000	[number]	k4	000
m	m	kA	m
méně	málo	k6eAd2	málo
než	než	k8xS	než
deset	deset	k4xCc1	deset
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
odjakživa	odjakživa	k6eAd1	odjakživa
pastvinářství	pastvinářství	k1gNnSc4	pastvinářství
<g/>
,	,	kIx,	,
podhorské	podhorský	k2eAgNnSc4d1	podhorské
i	i	k8xC	i
horské	horský	k2eAgNnSc4d1	horské
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
tradiční	tradiční	k2eAgNnPc4d1	tradiční
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Současnost	současnost	k1gFnSc1	současnost
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zřetelněji	zřetelně	k6eAd2	zřetelně
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rekreace	rekreace	k1gFnSc2	rekreace
a	a	k8xC	a
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
i	i	k9	i
zde	zde	k6eAd1	zde
množí	množit	k5eAaImIp3nP	množit
nové	nový	k2eAgInPc4d1	nový
hotely	hotel	k1gInPc4	hotel
<g/>
,	,	kIx,	,
ubytovny	ubytovna	k1gFnPc4	ubytovna
<g/>
,	,	kIx,	,
lanovky	lanovka	k1gFnPc4	lanovka
a	a	k8xC	a
lyžařské	lyžařský	k2eAgInPc4d1	lyžařský
vleky	vlek	k1gInPc4	vlek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
známých	známý	k2eAgFnPc2d1	známá
a	a	k8xC	a
atraktivních	atraktivní	k2eAgFnPc2d1	atraktivní
partií	partie	k1gFnPc2	partie
hor	hora	k1gFnPc2	hora
vede	vést	k5eAaImIp3nS	vést
vcelku	vcelku	k6eAd1	vcelku
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
uspokojivě	uspokojivě	k6eAd1	uspokojivě
značených	značený	k2eAgFnPc2d1	značená
turistických	turistický	k2eAgFnPc2d1	turistická
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
údržbu	údržba	k1gFnSc4	údržba
pečuje	pečovat	k5eAaImIp3nS	pečovat
převážně	převážně	k6eAd1	převážně
Italský	italský	k2eAgInSc1d1	italský
alpinistický	alpinistický	k2eAgInSc1d1	alpinistický
klub	klub	k1gInSc1	klub
CAI	CAI	kA	CAI
<g/>
.	.	kIx.	.
</s>
<s>
Apeniny	Apeniny	k1gFnPc1	Apeniny
jsou	být	k5eAaImIp3nP	být
poněkud	poněkud	k6eAd1	poněkud
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
slavných	slavný	k2eAgFnPc2d1	slavná
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc3	jejich
neopakovatelné	opakovatelný	k2eNgFnSc3d1	neopakovatelná
atmosféře	atmosféra	k1gFnSc3	atmosféra
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
převládají	převládat	k5eAaImIp3nP	převládat
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
i	i	k9	i
v	v	k7c6	v
odlehlejších	odlehlý	k2eAgNnPc6d2	odlehlejší
místech	místo	k1gNnPc6	místo
lze	lze	k6eAd1	lze
potkat	potkat	k5eAaPmF	potkat
také	také	k9	také
Angličany	Angličan	k1gMnPc4	Angličan
<g/>
,	,	kIx,	,
Japonce	Japonec	k1gMnPc4	Japonec
<g/>
,	,	kIx,	,
Němce	Němec	k1gMnPc4	Němec
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Jedni	jeden	k4xCgMnPc1	jeden
ocení	ocenit	k5eAaPmIp3nP	ocenit
možnost	možnost	k1gFnSc4	možnost
hvězdicových	hvězdicový	k2eAgInPc2d1	hvězdicový
výletů	výlet	k1gInPc2	výlet
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
dají	dát	k5eAaPmIp3nP	dát
přednost	přednost	k1gFnSc4	přednost
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
hřebenovým	hřebenový	k2eAgFnPc3d1	hřebenová
túrám	túra	k1gFnPc3	túra
a	a	k8xC	a
přechodům	přechod	k1gInPc3	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
si	se	k3xPyFc3	se
přijdou	přijít	k5eAaPmIp3nP	přijít
i	i	k9	i
horolezci	horolezec	k1gMnPc1	horolezec
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávány	vyhledáván	k2eAgInPc1d1	vyhledáván
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
příkré	příkrý	k2eAgInPc1d1	příkrý
severní	severní	k2eAgInPc1d1	severní
srázy	sráz	k1gInPc1	sráz
granssaského	granssaský	k2eAgInSc2d1	granssaský
masivu	masiv	k1gInSc2	masiv
nebo	nebo	k8xC	nebo
stolová	stolový	k2eAgFnSc1d1	stolová
hora	hora	k1gFnSc1	hora
Pietra	Pietrum	k1gNnSc2	Pietrum
di	di	k?	di
Bismantova	Bismantův	k2eAgNnSc2d1	Bismantův
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Reggio	Reggio	k1gMnSc1	Reggio
nell	nell	k1gMnSc1	nell
<g/>
'	'	kIx"	'
<g/>
Emilia	Emilia	k1gFnSc1	Emilia
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
v	v	k7c6	v
Apeninách	Apeniny	k1gFnPc6	Apeniny
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horská	horský	k2eAgFnSc1d1	horská
a	a	k8xC	a
středozemní	středozemní	k2eAgFnSc1d1	středozemní
krajina	krajina	k1gFnSc1	krajina
a	a	k8xC	a
staletý	staletý	k2eAgInSc1d1	staletý
vliv	vliv	k1gInSc1	vliv
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sem	sem	k6eAd1	sem
láká	lákat	k5eAaImIp3nS	lákat
milovníky	milovník	k1gMnPc4	milovník
památek	památka	k1gFnPc2	památka
i	i	k8xC	i
divoké	divoký	k2eAgFnSc2d1	divoká
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
