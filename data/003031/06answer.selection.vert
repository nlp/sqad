<s>
Už	už	k6eAd1	už
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
květen	květen	k1gInSc1	květen
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
lidovými	lidový	k2eAgFnPc7d1	lidová
oslavami	oslava	k1gFnPc7	oslava
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stavění	stavění	k1gNnSc4	stavění
májek	májek	k1gInSc1	májek
nebo	nebo	k8xC	nebo
pálení	pálení	k1gNnSc1	pálení
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
květnovou	květnový	k2eAgFnSc4d1	květnová
neděli	neděle	k1gFnSc4	neděle
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
matek	matka	k1gFnPc2	matka
<g/>
.	.	kIx.	.
</s>
