<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
24	[number]	k4	24
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
města	město	k1gNnSc2	město
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statusem	status	k1gInSc7	status
(	(	kIx(	(
<g/>
Kyjev	Kyjev	k1gInSc1	Kyjev
a	a	k8xC	a
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
)	)	kIx)	)
a	a	k8xC	a
autonomní	autonomní	k2eAgFnSc4d1	autonomní
republiku	republika	k1gFnSc4	republika
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
