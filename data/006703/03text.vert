<s>
Radiometrie	radiometrie	k1gFnSc1	radiometrie
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
měřením	měření	k1gNnSc7	měření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Radiometrie	radiometrie	k1gFnSc1	radiometrie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
měřením	měření	k1gNnSc7	měření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
tedy	tedy	k9	tedy
absolutní	absolutní	k2eAgFnPc4d1	absolutní
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
fotometrie	fotometrie	k1gFnSc1	fotometrie
studuje	studovat	k5eAaImIp3nS	studovat
obdobné	obdobný	k2eAgFnPc4d1	obdobná
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jejich	jejich	k3xOp3gNnSc2	jejich
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Radiometrie	radiometrie	k1gFnSc1	radiometrie
našla	najít	k5eAaPmAgFnS	najít
důležité	důležitý	k2eAgNnSc4d1	důležité
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikání	Fyzikání	k1gNnSc1	Fyzikání
veličiny	veličina	k1gFnSc2	veličina
měřené	měřený	k2eAgFnSc2d1	měřená
v	v	k7c6	v
radiometrii	radiometrie	k1gFnSc6	radiometrie
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
radiometrické	radiometrický	k2eAgFnPc1d1	radiometrická
veličiny	veličina	k1gFnPc1	veličina
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
energetické	energetický	k2eAgFnPc4d1	energetická
veličiny	veličina	k1gFnPc4	veličina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popisují	popisovat	k5eAaImIp3nP	popisovat
přenos	přenos	k1gInSc4	přenos
energie	energie	k1gFnSc2	energie
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Integrální	integrální	k2eAgFnPc1d1	integrální
veličiny	veličina	k1gFnPc1	veličina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
<g/>
)	)	kIx)	)
popisují	popisovat	k5eAaImIp3nP	popisovat
celkový	celkový	k2eAgInSc4d1	celkový
účinek	účinek	k1gInSc4	účinek
záření	záření	k1gNnSc2	záření
všech	všecek	k3xTgFnPc2	všecek
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
nebo	nebo	k8xC	nebo
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
spektrální	spektrální	k2eAgFnPc1d1	spektrální
veličiny	veličina	k1gFnPc1	veličina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
spektrální	spektrální	k2eAgInSc1d1	spektrální
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
<g/>
)	)	kIx)	)
popisují	popisovat	k5eAaImIp3nP	popisovat
účinek	účinek	k1gInSc4	účinek
záření	záření	k1gNnSc2	záření
jedné	jeden	k4xCgFnSc2	jeden
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
λ	λ	k?	λ
nebo	nebo	k8xC	nebo
frekvence	frekvence	k1gFnSc2	frekvence
ν	ν	k?	ν
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
integrální	integrální	k2eAgFnSc3d1	integrální
veličině	veličina	k1gFnSc3	veličina
existují	existovat	k5eAaImIp3nP	existovat
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
spektrální	spektrální	k2eAgFnPc4d1	spektrální
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zářivému	zářivý	k2eAgInSc3d1	zářivý
toku	tok	k1gInSc2	tok
Φ	Φ	k?	Φ
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
spektrální	spektrální	k2eAgInSc1d1	spektrální
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
Φ	Φ	k?	Φ
a	a	k8xC	a
Φ	Φ	k?	Φ
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
z	z	k7c2	z
integrální	integrální	k2eAgFnSc2d1	integrální
veličiny	veličina	k1gFnSc2	veličina
zjistili	zjistit	k5eAaPmAgMnP	zjistit
její	její	k3xOp3gInSc4	její
spektrální	spektrální	k2eAgInSc4d1	spektrální
protějšek	protějšek	k1gInSc4	protějšek
<g/>
,	,	kIx,	,
využijeme	využít	k5eAaPmIp1nP	využít
limitního	limitní	k2eAgInSc2d1	limitní
přechodu	přechod	k1gInSc2	přechod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
foton	foton	k1gInSc1	foton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
právě	právě	k6eAd1	právě
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Ukažme	ukázat	k5eAaPmRp1nP	ukázat
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
zářivého	zářivý	k2eAgInSc2d1	zářivý
toku	tok	k1gInSc2	tok
<g/>
:	:	kIx,	:
Integrální	integrální	k2eAgFnSc4d1	integrální
veličina	veličina	k1gFnSc1	veličina
–	–	k?	–
zářivý	zářivý	k2eAgInSc4d1	zářivý
tok	tok	k1gInSc4	tok
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
W	W	kA	W
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
:	:	kIx,	:
Spektrální	spektrální	k2eAgInSc1d1	spektrální
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
podle	podle	k7c2	podle
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
λ	λ	k?	λ
,	,	kIx,	,
λ	λ	k?	λ
+	+	kIx~	+
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
λ	λ	k?	λ
<g />
.	.	kIx.	.
</s>
<s hack="1">
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gNnSc1	langle
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc2	lambda
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
}	}	kIx)	}
:	:	kIx,	:
Spektrální	spektrální	k2eAgInSc1d1	spektrální
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
podle	podle	k7c2	podle
frekvence	frekvence	k1gFnSc2	frekvence
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
Hz	Hz	kA	Hz
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
frekvencích	frekvence	k1gFnPc6	frekvence
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
ν	ν	k?	ν
,	,	kIx,	,
ν	ν	k?	ν
+	+	kIx~	+
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ν	ν	k?	ν
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gFnPc4	langle
\	\	kIx~	\
<g/>
nu	nu	k9	nu
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
:	:	kIx,	:
Spektrální	spektrální	k2eAgInSc1d1	spektrální
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
W	W	kA	W
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
stejnou	stejný	k2eAgFnSc4d1	stejná
jako	jako	k8xC	jako
integrální	integrální	k2eAgFnSc1d1	integrální
veličina	veličina	k1gFnSc1	veličina
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
ν	ν	k?	ν
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
e	e	k0	e
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
\	\	kIx~	\
<g/>
Phi	Phi	k1gMnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
:	:	kIx,	:
Spektrální	spektrální	k2eAgFnPc1d1	spektrální
veličiny	veličina	k1gFnPc1	veličina
podle	podle	k7c2	podle
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
λ	λ	k?	λ
a	a	k8xC	a
frekvence	frekvence	k1gFnSc2	frekvence
ν	ν	k?	ν
jsou	být	k5eAaImIp3nP	být
svázané	svázaný	k2eAgInPc4d1	svázaný
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
c	c	k0	c
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
λ	λ	k?	λ
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
\	\	kIx~	\
<g/>
over	overa	k1gFnPc2	overa
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
\	\	kIx~	\
<g/>
over	overa	k1gFnPc2	overa
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
nu	nu	k9	nu
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
lambda	lambda	k1gNnSc1	lambda
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
\	\	kIx~	\
<g/>
over	over	k1gInSc4	over
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
:	:	kIx,	:
Integrální	integrální	k2eAgFnSc4d1	integrální
veličinu	veličina	k1gFnSc4	veličina
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
integrací	integrace	k1gFnSc7	integrace
spektrální	spektrální	k2eAgFnSc2d1	spektrální
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
λ	λ	k?	λ
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgFnPc4d1	uvedená
veličiny	veličina	k1gFnPc4	veličina
platí	platit	k5eAaImIp3nP	platit
analogické	analogický	k2eAgInPc1d1	analogický
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
textu	text	k1gInSc2	text
již	již	k6eAd1	již
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zář	zář	k1gFnSc1	zář
je	být	k5eAaImIp3nS	být
výkon	výkon	k1gInSc4	výkon
na	na	k7c4	na
jednotkovou	jednotkový	k2eAgFnSc4d1	jednotková
plochu	plocha	k1gFnSc4	plocha
kolmou	kolmý	k2eAgFnSc4d1	kolmá
k	k	k7c3	k
paprsku	paprsek	k1gInSc3	paprsek
a	a	k8xC	a
na	na	k7c4	na
jednotkový	jednotkový	k2eAgInSc4d1	jednotkový
prostorový	prostorový	k2eAgInSc4d1	prostorový
úhel	úhel	k1gInSc4	úhel
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
paprsku	paprsek	k1gInSc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Definujme	definovat	k5eAaBmRp1nP	definovat
tedy	tedy	k9	tedy
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
bod	bod	k1gInSc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc6	omega
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc4	směr
paprsku	paprsek	k1gInSc2	paprsek
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
theta	theto	k1gNnSc2	theto
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svírá	svírat	k5eAaImIp3nS	svírat
normálový	normálový	k2eAgInSc1d1	normálový
vektor	vektor	k1gInSc1	vektor
plochy	plocha	k1gFnSc2	plocha
se	s	k7c7	s
směrovým	směrový	k2eAgInSc7d1	směrový
vektorem	vektor	k1gInSc7	vektor
paprsku	paprsek	k1gInSc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Úhel	úhel	k1gInSc1	úhel
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
theta	theto	k1gNnSc2	theto
}	}	kIx)	}
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
zář	zář	k1gFnSc4	zář
odvodíme	odvodit	k5eAaPmIp1nP	odvodit
z	z	k7c2	z
veličiny	veličina	k1gFnSc2	veličina
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
pomocí	pomocí	k7c2	pomocí
limitního	limitní	k2eAgInSc2d1	limitní
přechodu	přechod	k1gInSc2	přechod
pro	pro	k7c4	pro
okolí	okolí	k1gNnSc4	okolí
bodu	bod	k1gInSc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
a	a	k8xC	a
pro	pro	k7c4	pro
prostorový	prostorový	k2eAgInSc4d1	prostorový
úhel	úhel	k1gInSc4	úhel
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
směrového	směrový	k2eAgInSc2d1	směrový
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc4	omega
}	}	kIx)	}
blížících	blížící	k2eAgFnPc2d1	blížící
se	se	k3xPyFc4	se
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úvaha	úvaha	k1gFnSc1	úvaha
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
následující	následující	k2eAgInSc4d1	následující
vztah	vztah	k1gInSc4	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
,	,	kIx,	,
ω	ω	k?	ω
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
theta	theta	k1gMnSc1	theta
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ozáření	ozáření	k1gNnSc1	ozáření
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
provedeme	provést	k5eAaPmIp1nP	provést
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
neformálně	formálně	k6eNd1	formálně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nasčítáme	nasčítat	k5eAaImIp1nP	nasčítat
všechny	všechen	k3xTgFnPc1	všechen
záře	zář	k1gFnPc1	zář
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
směrů	směr	k1gInPc2	směr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
omega	omega	k1gNnSc2	omega
}	}	kIx)	}
pomocí	pomocí	k7c2	pomocí
následujícího	následující	k2eAgInSc2d1	následující
vztahu	vztah	k1gInSc2	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
,	,	kIx,	,
ω	ω	k?	ω
)	)	kIx)	)
cos	cos	kA	cos
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
θ	θ	k?	θ
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
L_	L_	k1gFnSc1	L_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
theta	theta	k1gMnSc1	theta
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
theta	theta	k1gMnSc1	theta
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
natočení	natočení	k1gNnSc4	natočení
plochy	plocha	k1gFnSc2	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
hemisféru	hemisféra	k1gFnSc4	hemisféra
nad	nad	k7c7	nad
bodem	bod	k1gInSc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
z	z	k7c2	z
již	již	k6eAd1	již
známých	známý	k2eAgFnPc2d1	známá
veličin	veličina	k1gFnPc2	veličina
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
veličinu	veličina	k1gFnSc4	veličina
zářivý	zářivý	k2eAgInSc1d1	zářivý
tok	tok	k1gInSc1	tok
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
,	,	kIx,	,
který	který	k3yQgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
plochou	plocha	k1gFnSc7	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
sečteme	sečíst	k5eAaPmIp1nP	sečíst
pomocí	pomocí	k7c2	pomocí
integrálního	integrální	k2eAgInSc2d1	integrální
počtu	počet	k1gInSc2	počet
ozáření	ozáření	k1gNnSc2	ozáření
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
bodech	bod	k1gInPc6	bod
plochy	plocha	k1gFnSc2	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
úvahy	úvaha	k1gFnSc2	úvaha
plyne	plynout	k5eAaImIp3nS	plynout
následující	následující	k2eAgInSc1d1	následující
vztah	vztah	k1gInSc1	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Φ	Φ	k?	Φ
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
d	d	k?	d
x	x	k?	x
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
H	H	kA	H
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
,	,	kIx,	,
ω	ω	k?	ω
)	)	kIx)	)
cos	cos	kA	cos
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
d	d	k?	d
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
dx	dx	k?	dx
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
L_	L_	k1gFnSc1	L_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
theta	theta	k1gMnSc1	theta
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
dx	dx	k?	dx
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Optika	optika	k1gFnSc1	optika
Fotometrie	fotometrie	k1gFnSc2	fotometrie
</s>
