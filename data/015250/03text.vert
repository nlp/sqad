<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
(	(	kIx(
<g/>
Rumburk	Rumburk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vavřincev	Vavřincev	k1gFnSc4
Rumburku	Rumburk	k1gInSc2
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
v	v	k7c6
RumburkuMísto	RumburkuMísta	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Rumburk	Rumburk	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
13,22	13,22	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
16,01	16,01	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
litoměřická	litoměřický	k2eAgFnSc1d1
Vikariát	vikariát	k1gInSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
Farnost	farnost	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
Rumburk	Rumburk	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
filiální	filiální	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Užívání	užívání	k1gNnSc1
</s>
<s>
bližší	blízký	k2eAgFnPc4d2
informace	informace	k1gFnPc4
<g/>
:	:	kIx,
<g/>
o	o	k7c6
bohoslužbácho	bohoslužbácho	k6eAd1
programu	program	k1gInSc2
při	při	k7c6
NOCI	noc	k1gFnSc6
KOSTELŮ	kostel	k1gInPc2
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
baroko	baroko	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1683-1690	1683-1690	k4
Specifikace	specifikace	k1gFnSc1
Stavební	stavební	k2eAgFnSc1d1
materiál	materiál	k1gInSc4
</s>
<s>
zdivo	zdivo	k1gNnSc1
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Třída	třída	k1gFnSc1
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
Rumburk	Rumburk	k1gInSc1
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
19486	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
3864	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
součást	součást	k1gFnSc4
památky	památka	k1gFnSc2
kapucínský	kapucínský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolický	římskokatolický	k2eAgMnSc1d1
filiální	filiální	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
s	s	k7c7
kapucínským	kapucínský	k2eAgInSc7d1
klášterem	klášter	k1gInSc7
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
tvoří	tvořit	k5eAaImIp3nS
barokní	barokní	k2eAgInSc1d1
sakrální	sakrální	k2eAgInSc1d1
areál	areál	k1gInSc1
z	z	k7c2
let	léto	k1gNnPc2
1683	#num#	k4
<g/>
–	–	k?
<g/>
1690	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
je	být	k5eAaImIp3nS
komplex	komplex	k1gInSc4
loretánské	loretánský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
s	s	k7c7
ambity	ambit	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
komplex	komplex	k1gInSc1
kapucínského	kapucínský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
s	s	k7c7
kostelem	kostel	k1gInSc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
lorety	loreta	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
od	od	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
kláštera	klášter	k1gInSc2
</s>
<s>
Ve	v	k7c6
správě	správa	k1gFnSc6
řádu	řád	k1gInSc2
kapucínů	kapucín	k1gMnPc2
byl	být	k5eAaImAgInS
klášter	klášter	k1gInSc1
od	od	k7c2
konce	konec	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
areál	areál	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
oblíbeným	oblíbený	k2eAgNnSc7d1
poutním	poutní	k2eAgNnSc7d1
místem	místo	k1gNnSc7
až	až	k9
do	do	k7c2
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gInSc4
významově	významově	k6eAd1
zastínila	zastínit	k5eAaPmAgFnS
nedaleká	daleký	k2eNgFnSc1d1
bazilika	bazilika	k1gFnSc1
ve	v	k7c6
Filipově	Filipův	k2eAgMnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapucíni	kapucín	k1gMnPc1
působili	působit	k5eAaImAgMnP
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
až	až	k9
do	do	k7c2
násilného	násilný	k2eAgNnSc2d1
rozehnání	rozehnání	k1gNnSc2
mužských	mužský	k2eAgInPc2d1
řádů	řád	k1gInPc2
v	v	k7c6
komunistickém	komunistický	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Areál	areál	k1gInSc1
pak	pak	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
podstatě	podstata	k1gFnSc6
prázdný	prázdný	k2eAgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
minimálním	minimální	k2eAgNnSc7d1
využitím	využití	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
začal	začít	k5eAaPmAgMnS
chátrat	chátrat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
probíhala	probíhat	k5eAaImAgFnS
úprava	úprava	k1gFnSc1
kláštera	klášter	k1gInSc2
na	na	k7c4
knihovnu	knihovna	k1gFnSc4
<g/>
,	,	kIx,
dokončená	dokončený	k2eAgFnSc1d1
až	až	k9
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
lze	lze	k6eAd1
rovněž	rovněž	k9
datovat	datovat	k5eAaImF
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
obnovu	obnova	k1gFnSc4
lorety	loreta	k1gFnSc2
a	a	k8xC
ambitů	ambit	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
vypořádat	vypořádat	k5eAaPmF
se	s	k7c7
statickými	statický	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
vzniklými	vzniklý	k2eAgFnPc7d1
provozem	provoz	k1gInSc7
těžkých	těžký	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
na	na	k7c6
okolních	okolní	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
i	i	k8xC
rozpadem	rozpad	k1gInSc7
původního	původní	k2eAgNnSc2d1
odvodnění	odvodnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opravy	oprava	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
i	i	k9
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
dekádě	dekáda	k1gFnSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
restaurováním	restaurování	k1gNnSc7
fresek	freska	k1gFnPc2
a	a	k8xC
zachovalých	zachovalý	k2eAgFnPc2d1
částí	část	k1gFnPc2
mobiliáře	mobiliář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klášter	klášter	k1gInSc1
je	být	k5eAaImIp3nS
čtyřkřídlá	čtyřkřídlý	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
kolem	kolem	k7c2
pravoúhlého	pravoúhlý	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stavba	stavba	k1gFnSc1
patrová	patrový	k2eAgFnSc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
v	v	k7c6
jednom	jeden	k4xCgInSc6
traktu	trakt	k1gInSc6
dvoupatrová	dvoupatrový	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rumburští	rumburský	k2eAgMnPc1d1
kapucíni	kapucín	k1gMnPc1
</s>
<s>
kolem	kolem	k7c2
roku	rok	k1gInSc2
1897	#num#	k4
P.	P.	kA
Hilarius	Hilarius	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
představený	představený	k1gMnSc1
kapucínského	kapucínský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Architektura	architektura	k1gFnSc1
kostela	kostel	k1gInSc2
</s>
<s>
Kapucínský	kapucínský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
kostelem	kostel	k1gInSc7
</s>
<s>
Detail	detail	k1gInSc1
fresky	freska	k1gFnSc2
na	na	k7c6
průčelí	průčelí	k1gNnSc6
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
obdélnou	obdélný	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
obdélný	obdélný	k2eAgInSc4d1
presbytář	presbytář	k1gInSc4
a	a	k8xC
obdélnou	obdélný	k2eAgFnSc4d1
kapli	kaple	k1gFnSc4
po	po	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Presbytář	presbytář	k1gInSc4
i	i	k8xC
loď	loď	k1gFnSc4
mají	mít	k5eAaImIp3nP
valenou	valený	k2eAgFnSc4d1
klenbu	klenba	k1gFnSc4
s	s	k7c7
lunetami	luneta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boční	boční	k2eAgFnPc4d1
kaple	kaple	k1gFnPc4
jsou	být	k5eAaImIp3nP
sklenuty	sklenout	k5eAaPmNgFnP
křížově	křížově	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zařízení	zařízení	k1gNnSc1
kostela	kostel	k1gInSc2
</s>
<s>
Oltářní	oltářní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
oltáře	oltář	k1gInSc2
je	být	k5eAaImIp3nS
barokní	barokní	k2eAgFnSc1d1
a	a	k8xC
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
obrazy	obraz	k1gInPc1
Umučení	umučení	k1gNnSc1
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
<g/>
,	,	kIx,
Umučení	umučení	k1gNnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
a	a	k8xC
Umučení	umučení	k1gNnSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
boční	boční	k2eAgInPc4d1
oltáře	oltář	k1gInPc4
pocházející	pocházející	k2eAgNnSc1d1
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc3
jsou	být	k5eAaImIp3nP
zasvěceny	zasvětit	k5eAaPmNgInP
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
a	a	k8xC
sv.	sv.	kA
Františkovi	František	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rokoková	rokokový	k2eAgFnSc1d1
kazatelna	kazatelna	k1gFnSc1
je	být	k5eAaImIp3nS
s	s	k7c7
reliéfem	reliéf	k1gInSc7
Rozsévače	rozsévač	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varhany	varhany	k1gFnPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
čtvrtiny	čtvrtina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kapli	kaple	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
oltář	oltář	k1gInSc1
z	z	k7c2
období	období	k1gNnSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
má	mít	k5eAaImIp3nS
rokokové	rokokový	k2eAgInPc4d1
doplňky	doplněk	k1gInPc4
a	a	k8xC
obraz	obraz	k1gInSc4
sv.	sv.	kA
Antonína	Antonín	k1gMnSc2
Paduánského	paduánský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
interiéru	interiér	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
barokní	barokní	k2eAgInSc1d1
obraz	obraz	k1gInSc1
Útěk	útěk	k1gInSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Varhany	varhany	k1gFnPc1
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1
historie	historie	k1gFnSc1
varhan	varhany	k1gFnPc2
je	být	k5eAaImIp3nS
vázána	vázat	k5eAaImNgFnS
na	na	k7c4
kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
v	v	k7c6
Děčíně-Nebočadech	Děčíně-Nebočad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
se	se	k3xPyFc4
tam	tam	k6eAd1
zřítil	zřítit	k5eAaPmAgInS
strop	strop	k1gInSc1
a	a	k8xC
tamní	tamní	k2eAgInPc1d1
varhany	varhany	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
přeneseny	přenesen	k2eAgInPc4d1
varhanářem	varhanář	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
Jónem	Jón	k1gMnSc7
do	do	k7c2
rumburského	rumburský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
téhož	týž	k3xTgMnSc2
světce	světec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nástroj	nástroj	k1gInSc4
postavil	postavit	k5eAaPmAgMnS
Heinrich	Heinrich	k1gMnSc1
Schiffner	Schiffner	k1gMnSc1
ze	z	k7c2
Cvikova	Cvikov	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
jako	jako	k8xS,k8xC
dvoumanuálový	dvoumanuálový	k2eAgInSc4d1
s	s	k7c7
pedálem	pedál	k1gInSc7
<g/>
,	,	kIx,
mk	mk	k?
a	a	k8xC
dispozicí	dispozice	k1gFnPc2
<g/>
:	:	kIx,
I.	I.	kA
Principal	Principal	k1gFnSc1
8	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Gedakt	Gedakt	k1gInSc1
8	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Gamba	gamba	k1gFnSc1
8	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Octav	Octav	k1gInSc1
4	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Mixtur	mixtura	k1gFnPc2
4	#num#	k4
<g/>
fach	fach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aeolina	Aeolina	k1gFnSc1
8	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Lieblich	Lieblich	k1gInSc1
gedakt	gedakt	k1gInSc1
8	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Gemshorn	Gemshorn	k1gInSc1
4	#num#	k4
<g/>
´	´	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
P.	P.	kA
Octavbass	Octavbass	k1gInSc4
8	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
Subbass	Subbass	k1gInSc1
16	#num#	k4
<g/>
´	´	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlapky	šlapka	k1gFnPc1
<g/>
:	:	kIx,
Pedal	Pedal	k1gInSc1
Koppel	Koppela	k1gFnPc2
<g/>
,	,	kIx,
Pleno	pleno	k6eAd1
I	i	k9
<g/>
+	+	kIx~
<g/>
II	II	kA
<g/>
,	,	kIx,
Forte	forte	k1gNnSc2
<g/>
,	,	kIx,
Manual	Manual	k1gMnSc1
Koppel	Koppel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
instalaci	instalace	k1gFnSc6
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
byl	být	k5eAaImAgInS
osazen	osadit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
Flétna	flétna	k1gFnSc1
špičatá	špičatý	k2eAgFnSc1d1
2	#num#	k4
<g/>
´	´	k?
<g/>
,	,	kIx,
vyrobený	vyrobený	k2eAgInSc1d1
Janem	Jan	k1gMnSc7
Kubátem	Kubát	k1gMnSc7
ml.	ml.	kA
z	z	k7c2
Kutné	kutný	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
–	–	k?
Kaňku	kaňka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
AD	ad	k7c4
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litoměřice	Litoměřice	k1gInPc4
<g/>
:	:	kIx,
Biskupství	biskupství	k1gNnSc3
litoměřické	litoměřický	k2eAgInPc1d1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
430	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Přehled	přehled	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
diecéze	diecéze	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
168	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
777	#num#	k4
kostelů	kostel	k1gInPc2
<g/>
,	,	kIx,
klášterů	klášter	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Soukup	Soukup	k1gMnSc1
&	&	k?
David	David	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
308	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7011	#num#	k4
<g/>
-	-	kIx~
<g/>
708	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
239	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
130253	#num#	k4
:	:	kIx,
klášter	klášter	k1gInSc1
kapucínský	kapucínský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
POCHE	POCHE	kA
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Čech	Čechy	k1gFnPc2
P	P	kA
<g/>
/	/	kIx~
<g/>
Š	Š	kA
<g/>
,	,	kIx,
sv.	sv.	kA
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
540	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Rumburk	Rumburk	k1gInSc1
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
266	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JABURKOVÁ	JABURKOVÁ	kA
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
,	,	kIx,
MÁGROVÁ	MÁGROVÁ	kA
<g/>
,	,	kIx,
Klára	Klára	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
Šluknovska	Šluknovsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumburk	Rumburk	k1gInSc1
<g/>
:	:	kIx,
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
–	–	k?
děkanství	děkanství	k1gNnSc1
Rumburk	Rumburk	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Varhany	varhany	k1gFnPc1
a	a	k8xC
varhanáři	varhanář	k1gMnPc1
Děčínska	Děčínsko	k1gNnSc2
a	a	k8xC
Šluknovska	Šluknovsko	k1gNnSc2
1995	#num#	k4
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
MUDr.	MUDr.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Loretánská	loretánský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pořad	pořad	k1gInSc1
bohoslužeb	bohoslužba	k1gFnPc2
v	v	k7c6
klášterním	klášterní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
<g/>
,	,	kIx,
Rumburk	Rumburk	k1gInSc1
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
biskupství	biskupství	k1gNnSc2
litoměřického	litoměřický	k2eAgNnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Program	program	k1gInSc1
NOCI	noc	k1gFnSc2
KOSTELŮ	kostel	k1gInPc2
<g/>
,	,	kIx,
Rumburk	Rumburk	k1gInSc1
<g/>
,	,	kIx,
Loretánská	loretánský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
a	a	k8xC
klášterní	klášterní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
</s>
<s>
Klášter	klášter	k1gInSc1
kapucínů	kapucín	k1gMnPc2
s	s	k7c7
kostelem	kostel	k1gInSc7
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
a	a	k8xC
loretou	loreta	k1gFnSc7
na	na	k7c6
webu	web	k1gInSc6
Hrady	hrad	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
110584	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
144746561	#num#	k4
</s>
