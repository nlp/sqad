<s>
Dieter	Dieter	k1gMnSc1	Dieter
Stein	Stein	k1gMnSc1	Stein
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Ingolstadt	Ingolstadt	k1gInSc1	Ingolstadt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
pravicového	pravicový	k2eAgInSc2d1	pravicový
časopisu	časopis	k1gInSc2	časopis
Junge	Jung	k1gFnSc2	Jung
Freiheit	Freiheita	k1gFnPc2	Freiheita
<g/>
.	.	kIx.	.
</s>
<s>
Stein	Stein	k1gMnSc1	Stein
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
politologii	politologie	k1gFnSc4	politologie
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
na	na	k7c4	na
Albert-Ludwigs-Universität	Albert-Ludwigs-Universität	k2eAgInSc4d1	Albert-Ludwigs-Universität
Freiburg	Freiburg	k1gInSc4	Freiburg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
založil	založit	k5eAaPmAgInS	založit
časopis	časopis	k1gInSc1	časopis
Junge	Junge	k1gNnSc1	Junge
Freiheit	Freiheit	k1gInSc1	Freiheit
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
časopisu	časopis	k1gInSc2	časopis
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Dieter	Dieter	k1gMnSc1	Dieter
Stein	Stein	k1gMnSc1	Stein
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
