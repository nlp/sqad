<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1553	[number]	k4	1553
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Pau	Pau	k1gFnSc2	Pau
<g/>
,	,	kIx,	,
Navarra	Navarra	k1gFnSc1	Navarra
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1610	[number]	k4	1610
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1589	[number]	k4	1589
až	až	k9	až
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Navarrskému	navarrský	k2eAgNnSc3d1	Navarrské
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1589	[number]	k4	1589
vládl	vládnout	k5eAaImAgInS	vládnout
oběma	dva	k4xCgFnPc3	dva
zemím	zem	k1gFnPc3	zem
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
králem	král	k1gMnSc7	král
z	z	k7c2	z
Bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vládla	vládnout	k5eAaImAgFnS	vládnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
skrze	skrze	k?	skrze
poboční	poboční	k2eAgFnSc4d1	poboční
linii	linie	k1gFnSc4	linie
Bourbon-Orléans	Bourbon-Orléansa	k1gFnPc2	Bourbon-Orléansa
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
vévoda	vévoda	k1gMnSc1	vévoda
Antonín	Antonín	k1gMnSc1	Antonín
z	z	k7c2	z
Vendôme	Vendôm	k1gInSc5	Vendôm
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
potomkem	potomek	k1gMnSc7	potomek
francouzského	francouzský	k2eAgInSc2d1	francouzský
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
a	a	k8xC	a
díky	díky	k7c3	díky
výhodnému	výhodný	k2eAgInSc3d1	výhodný
sňatku	sňatek	k1gInSc3	sňatek
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
Johanou	Johana	k1gFnSc7	Johana
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
navarrským	navarrský	k2eAgMnSc7d1	navarrský
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1572	[number]	k4	1572
si	se	k3xPyFc3	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
král	král	k1gMnSc1	král
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Markétu	Markéta	k1gFnSc4	Markéta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
Margot	Margot	k1gInSc4	Margot
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
svatba	svatba	k1gFnSc1	svatba
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
ukončit	ukončit	k5eAaPmF	ukončit
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
náboženské	náboženský	k2eAgInPc4d1	náboženský
rozbroje	rozbroj	k1gInPc4	rozbroj
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
nastolit	nastolit	k5eAaPmF	nastolit
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
vnitřně	vnitřně	k6eAd1	vnitřně
rozpolcena	rozpolcen	k2eAgFnSc1d1	rozpolcena
a	a	k8xC	a
sužována	sužovat	k5eAaImNgFnS	sužovat
neustálými	neustálý	k2eAgInPc7d1	neustálý
spory	spor	k1gInPc7	spor
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
přecházejícími	přecházející	k2eAgInPc7d1	přecházející
až	až	k6eAd1	až
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
velmi	velmi	k6eAd1	velmi
násilné	násilný	k2eAgNnSc1d1	násilné
-	-	kIx~	-
hugenotských	hugenotský	k2eAgFnPc2d1	hugenotská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
zvaných	zvaný	k2eAgFnPc2d1	zvaná
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
hugenoty	hugenot	k1gMnPc7	hugenot
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vzájemně	vzájemně	k6eAd1	vzájemně
soupeřili	soupeřit	k5eAaImAgMnP	soupeřit
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
Valois	Valois	k1gFnPc2	Valois
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
hlásila	hlásit	k5eAaImAgFnS	hlásit
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Bourboni	Bourboň	k1gFnSc3	Bourboň
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patřil	patřit	k5eAaImAgMnS	patřit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
převážně	převážně	k6eAd1	převážně
hugenoty	hugenot	k1gMnPc7	hugenot
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
příbuzní	příbuzný	k1gMnPc1	příbuzný
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Navarrského	navarrský	k2eAgMnSc2d1	navarrský
stáli	stát	k5eAaImAgMnP	stát
paradoxně	paradoxně	k6eAd1	paradoxně
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
obou	dva	k4xCgInPc2	dva
znepřátelených	znepřátelený	k2eAgInPc2d1	znepřátelený
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
protestantů	protestant	k1gMnPc2	protestant
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
Jindřichův	Jindřichův	k2eAgMnSc1d1	Jindřichův
strýc	strýc	k1gMnSc1	strýc
Ludvík	Ludvík	k1gMnSc1	Ludvík
I.	I.	kA	I.
de	de	k?	de
Condé	Condý	k2eAgInPc1d1	Condý
a	a	k8xC	a
zájmy	zájem	k1gInPc1	zájem
katolíků	katolík	k1gMnPc2	katolík
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
de	de	k?	de
Guise	Guis	k1gMnSc2	Guis
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Navarrským	navarrský	k2eAgMnSc7d1	navarrský
zase	zase	k9	zase
stejného	stejný	k2eAgMnSc2d1	stejný
pradědečka	pradědeček	k1gMnSc2	pradědeček
Františka	František	k1gMnSc2	František
de	de	k?	de
Bourbon	bourbon	k1gInSc1	bourbon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
své	svůj	k3xOyFgFnSc2	svůj
svatby	svatba	k1gFnSc2	svatba
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
Navarrský	navarrský	k2eAgInSc1d1	navarrský
zdál	zdát	k5eAaImAgInS	zdát
tím	ten	k3xDgInSc7	ten
nejméně	málo	k6eAd3	málo
možným	možný	k2eAgInSc7d1	možný
a	a	k8xC	a
vhodným	vhodný	k2eAgMnSc7d1	vhodný
budoucím	budoucí	k2eAgMnSc7d1	budoucí
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
přímým	přímý	k2eAgMnSc7d1	přímý
mužským	mužský	k2eAgMnSc7d1	mužský
potomkem	potomek	k1gMnSc7	potomek
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dělila	dělit	k5eAaImAgFnS	dělit
je	on	k3xPp3gNnSc4	on
propast	propast	k1gFnSc1	propast
času	čas	k1gInSc2	čas
a	a	k8xC	a
devět	devět	k4xCc4	devět
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Šance	šance	k1gFnSc1	šance
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
francouzského	francouzský	k2eAgInSc2d1	francouzský
trůnu	trůn	k1gInSc2	trůn
se	se	k3xPyFc4	se
nezvýšila	zvýšit	k5eNaPmAgFnS	zvýšit
ani	ani	k8xC	ani
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svatby	svatba	k1gFnSc2	svatba
ještě	ještě	k9	ještě
tři	tři	k4xCgFnPc4	tři
žijící	žijící	k2eAgFnPc4d1	žijící
bratry	bratr	k1gMnPc7	bratr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
byl	být	k5eAaImAgInS	být
králem	král	k1gMnSc7	král
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
dva	dva	k4xCgMnPc4	dva
následníky	následník	k1gMnPc4	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
rodu	rod	k1gInSc2	rod
Valois	Valois	k1gFnPc2	Valois
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgNnP	zdát
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
nadějná	nadějný	k2eAgFnSc1d1	nadějná
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
příbuzní	příbuzný	k1gMnPc1	příbuzný
neměli	mít	k5eNaImAgMnP	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
důvod	důvod	k1gInSc4	důvod
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
panovnické	panovnický	k2eAgNnSc4d1	panovnické
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
opravdu	opravdu	k6eAd1	opravdu
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
náboženské	náboženský	k2eAgFnPc4d1	náboženská
spory	spora	k1gFnPc4	spora
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
ukončit	ukončit	k5eAaPmF	ukončit
právě	právě	k9	právě
v	v	k7c4	v
den	den	k1gInSc4	den
svatby	svatba	k1gFnSc2	svatba
Jindřicha	Jindřich	k1gMnSc2	Jindřich
a	a	k8xC	a
Markéty	Markéta	k1gFnSc2	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
a	a	k8xC	a
následné	následný	k2eAgFnPc1d1	následná
události	událost	k1gFnPc1	událost
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Bartolomějská	bartolomějský	k2eAgFnSc1d1	Bartolomějská
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
jen	jen	k9	jen
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
na	na	k7c4	na
3000	[number]	k4	3000
obětí	oběť	k1gFnPc2	oběť
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
hugenotů	hugenot	k1gMnPc2	hugenot
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
20	[number]	k4	20
000	[number]	k4	000
náboženských	náboženský	k2eAgMnPc2d1	náboženský
odpůrců	odpůrce	k1gMnPc2	odpůrce
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
byl	být	k5eAaImAgMnS	být
ušetřen	ušetřen	k2eAgMnSc1d1	ušetřen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
členů	člen	k1gMnPc2	člen
družiny	družin	k2eAgInPc1d1	družin
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1574	[number]	k4	1574
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bratr	bratr	k1gMnSc1	bratr
Markéty	Markéta	k1gFnSc2	Markéta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
tak	tak	k6eAd1	tak
usedl	usednout	k5eAaPmAgMnS	usednout
další	další	k2eAgMnSc1d1	další
Markétin	Markétin	k2eAgMnSc1d1	Markétin
bratr	bratr	k1gMnSc1	bratr
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
hugenoti	hugenot	k1gMnPc1	hugenot
přišli	přijít	k5eAaPmAgMnP	přijít
při	při	k7c6	při
Bartolomějské	bartolomějský	k2eAgFnSc6d1	Bartolomějská
noci	noc	k1gFnSc6	noc
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
další	další	k2eAgInSc4d1	další
boj	boj	k1gInSc4	boj
s	s	k7c7	s
katolíky	katolík	k1gMnPc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
poslední	poslední	k2eAgMnSc1d1	poslední
žijící	žijící	k2eAgMnSc1d1	žijící
a	a	k8xC	a
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
člen	člen	k1gMnSc1	člen
rodu	rod	k1gInSc2	rod
Valois	Valois	k1gInSc1	Valois
Namurský	Namurský	k2eAgInSc1d1	Namurský
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
z	z	k7c2	z
následnictví	následnictví	k1gNnSc2	následnictví
francouzského	francouzský	k2eAgInSc2d1	francouzský
trůnu	trůn	k1gInSc2	trůn
jeho	jeho	k3xOp3gMnSc2	jeho
právoplatného	právoplatný	k2eAgMnSc2d1	právoplatný
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnPc4	vůdce
hugenotů	hugenot	k1gMnPc2	hugenot
a	a	k8xC	a
manžela	manžel	k1gMnSc2	manžel
své	svůj	k3xOyFgFnPc4	svůj
sestry	sestra	k1gFnPc4	sestra
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Navarrského	navarrský	k2eAgMnSc2d1	navarrský
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
okamžikem	okamžik	k1gInSc7	okamžik
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
Osmá	osmý	k4xOgFnSc1	osmý
hugenotská	hugenotský	k2eAgFnSc1d1	hugenotská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
válka	válka	k1gFnSc1	válka
tří	tři	k4xCgMnPc2	tři
Jindřichů	Jindřich	k1gMnPc2	Jindřich
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
získala	získat	k5eAaPmAgFnS	získat
převahu	převaha	k1gFnSc4	převaha
katolická	katolický	k2eAgFnSc1d1	katolická
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
vůdce	vůdce	k1gMnSc1	vůdce
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Guise	Guise	k1gFnSc1	Guise
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgMnS	začít
klást	klást	k5eAaImF	klást
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
konkurence	konkurence	k1gFnSc2	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vévody	vévoda	k1gMnSc2	vévoda
de	de	k?	de
Guise	Guis	k1gMnSc2	Guis
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
hugenoty	hugenot	k1gMnPc7	hugenot
dohodu	dohoda	k1gFnSc4	dohoda
proti	proti	k7c3	proti
Katolické	katolický	k2eAgFnSc3d1	katolická
lize	liga	k1gFnSc3	liga
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1588	[number]	k4	1588
nechal	nechat	k5eAaPmAgMnS	nechat
vévodu	vévoda	k1gMnSc4	vévoda
de	de	k?	de
Guise	Guis	k1gMnSc4	Guis
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
zahynul	zahynout	k5eAaPmAgMnS	zahynout
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
rukou	ruka	k1gFnSc7	ruka
atentátníka	atentátník	k1gMnSc2	atentátník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
mnicha	mnich	k1gMnSc2	mnich
Jacquese	Jacques	k1gMnSc2	Jacques
Clémenta	Clément	k1gMnSc2	Clément
<g/>
,	,	kIx,	,
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jindřichem	Jindřich	k1gMnSc7	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
vymřela	vymřít	k5eAaPmAgFnS	vymřít
dynastie	dynastie	k1gFnSc1	dynastie
Valois	Valois	k1gFnSc2	Valois
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
<s>
Následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
by	by	kYmCp3nS	by
raději	rád	k6eAd2	rád
viděla	vidět	k5eAaImAgFnS	vidět
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
katolíka	katolík	k1gMnSc4	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
nejprve	nejprve	k6eAd1	nejprve
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
zlomil	zlomit	k5eAaPmAgInS	zlomit
odpor	odpor	k1gInSc1	odpor
Katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
podřídil	podřídit	k5eAaPmAgMnS	podřídit
vůli	vůle	k1gFnSc4	vůle
většiny	většina	k1gFnSc2	většina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1593	[number]	k4	1593
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
konečně	konečně	k6eAd1	konečně
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
a	a	k8xC	a
vévoda	vévoda	k1gMnSc1	vévoda
Bourbonský	bourbonský	k2eAgMnSc1d1	bourbonský
<g/>
,	,	kIx,	,
korunován	korunován	k2eAgMnSc1d1	korunován
jako	jako	k9	jako
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
králem	král	k1gMnSc7	král
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
vydal	vydat	k5eAaPmAgMnS	vydat
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Nantský	nantský	k2eAgInSc1d1	nantský
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hugenotům	hugenot	k1gMnPc3	hugenot
zaručoval	zaručovat	k5eAaImAgMnS	zaručovat
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgInS	provést
také	také	k9	také
finanční	finanční	k2eAgInSc1d1	finanční
<g/>
,	,	kIx,	,
správní	správní	k2eAgFnSc2d1	správní
a	a	k8xC	a
soudní	soudní	k2eAgFnSc2d1	soudní
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgMnS	podporovat
stabilizaci	stabilizace	k1gFnSc4	stabilizace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
koloniální	koloniální	k2eAgFnSc3d1	koloniální
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1606	[number]	k4	1606
založil	založit	k5eAaPmAgInS	založit
Řád	řád	k1gInSc1	řád
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Karmelské	Karmelský	k2eAgFnSc2d1	Karmelská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
se	se	k3xPyFc4	se
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
domluvě	domluva	k1gFnSc6	domluva
a	a	k8xC	a
shodě	shoda	k1gFnSc6	shoda
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgFnPc4	žádný
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Medicejskou	Medicejský	k2eAgFnSc7d1	Medicejská
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
katolickým	katolický	k2eAgMnSc7d1	katolický
fanatikem	fanatik	k1gMnSc7	fanatik
Françoisem	François	k1gInSc7	François
Ravaillacem	Ravaillace	k1gMnSc7	Ravaillace
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
dále	daleko	k6eAd2	daleko
upevňoval	upevňovat	k5eAaImAgInS	upevňovat
absolutistickou	absolutistický	k2eAgFnSc4d1	absolutistická
královskou	královský	k2eAgFnSc4d1	královská
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
–	–	k?	–
především	především	k6eAd1	především
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
území	území	k1gNnSc2	území
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
omezení	omezení	k1gNnSc2	omezení
moci	moc	k1gFnSc2	moc
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
čtrnácti	čtrnáct	k4xCc2	čtrnáct
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
Marií	Maria	k1gFnSc7	Maria
Medicejskou	Medicejský	k2eAgFnSc7d1	Medicejská
měl	mít	k5eAaImAgMnS	mít
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
;	;	kIx,	;
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
∞	∞	k?	∞
Anna	Anna	k1gFnSc1	Anna
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
<g/>
,	,	kIx,	,
španělská	španělský	k2eAgFnSc1d1	španělská
infantka	infantka	k1gFnSc1	infantka
Izabela	Izabela	k1gFnSc1	Izabela
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1615	[number]	k4	1615
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Kristina	Kristin	k2eAgFnSc1d1	Kristina
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
–	–	k?	–
<g/>
1663	[number]	k4	1663
<g/>
)	)	kIx)	)
<g/>
∞	∞	k?	∞
1619	[number]	k4	1619
savojský	savojský	k2eAgMnSc1d1	savojský
vévoda	vévoda	k1gMnSc1	vévoda
Viktor	Viktor	k1gMnSc1	Viktor
Amadeus	Amadeus	k1gMnSc1	Amadeus
I.	I.	kA	I.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
1607	[number]	k4	1607
<g/>
–	–	k?	–
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
Gaston	Gaston	k1gInSc1	Gaston
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
–	–	k?	–
<g/>
1660	[number]	k4	1660
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orléanský	orléanský	k2eAgMnSc1d1	orléanský
vévoda	vévoda	k1gMnSc1	vévoda
∞	∞	k?	∞
1626	[number]	k4	1626
Marie	Marie	k1gFnSc1	Marie
z	z	k7c2	z
Montpensier	Montpensira	k1gFnPc2	Montpensira
∞	∞	k?	∞
1632	[number]	k4	1632
Markéta	Markéta	k1gFnSc1	Markéta
Lotrinská	lotrinský	k2eAgFnSc1d1	lotrinská
Henrietta	Henrietta	k1gFnSc1	Henrietta
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1669	[number]	k4	1669
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1625	[number]	k4	1625
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
S	s	k7c7	s
milenkou	milenka	k1gFnSc7	milenka
Gabrielou	Gabriela	k1gFnSc7	Gabriela
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Estrée	Estrée	k1gInSc1	Estrée
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nositeli	nositel	k1gMnPc7	nositel
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
Vendôme	Vendôm	k1gMnSc5	Vendôm
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
milenkami	milenka	k1gFnPc7	milenka
Henriettou	Henrietta	k1gFnSc7	Henrietta
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Entragues	Entragues	k1gInSc1	Entragues
<g/>
,	,	kIx,	,
Jacquelin	Jacquelin	k2eAgInSc1d1	Jacquelin
de	de	k?	de
Bueil	Bueil	k1gInSc1	Bueil
a	a	k8xC	a
Charlotte	Charlott	k1gInSc5	Charlott
des	des	k1gNnPc2	des
Essorts	Essorts	k1gInSc1	Essorts
měl	mít	k5eAaImAgInS	mít
dohromady	dohromady	k6eAd1	dohromady
dalších	další	k2eAgFnPc2d1	další
pět	pět	k4xCc1	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
