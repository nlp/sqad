<p>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
nebo	nebo	k8xC	nebo
též	též	k9	též
Mauretánie	Mauretánie	k1gFnSc1	Mauretánie
je	být	k5eAaImIp3nS	být
islámská	islámský	k2eAgFnSc1d1	islámská
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
žijí	žít	k5eAaImIp3nP	žít
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Maavíja	Maavíja	k1gMnSc1	Maavíja
uld	uld	k?	uld
Sídí	Sídí	k1gMnSc1	Sídí
Ahmed	Ahmed	k1gMnSc1	Ahmed
Tajá	Tajá	k1gFnSc1	Tajá
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
sesazen	sesazen	k2eAgInSc1d1	sesazen
vojenským	vojenský	k2eAgInSc7d1	vojenský
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
plukovníka	plukovník	k1gMnSc2	plukovník
Alího	Alí	k1gMnSc2	Alí
Uld	Uld	k1gMnSc2	Uld
Mohammeda	Mohammed	k1gMnSc2	Mohammed
Fála	Fála	k1gMnSc1	Fála
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
demokraticky	demokraticky	k6eAd1	demokraticky
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
Sídí	Sídí	k1gNnSc2	Sídí
Uld	Uld	k1gMnSc1	Uld
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
při	při	k7c6	při
vojenském	vojenský	k2eAgInSc6d1	vojenský
převratu	převrat	k1gInSc6	převrat
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Ghanské	ghanský	k2eAgFnSc2d1	ghanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1076	[number]	k4	1076
dobyli	dobýt	k5eAaPmAgMnP	dobýt
berberští	berberský	k2eAgMnPc1d1	berberský
Almorávidé	Almorávidý	k2eAgNnSc4d1	Almorávidé
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
zde	zde	k6eAd1	zde
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgFnSc4d1	zasahující
až	až	k9	až
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Almoravidovská	Almoravidovský	k2eAgFnSc1d1	Almoravidovský
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1147	[number]	k4	1147
a	a	k8xC	a
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
říše	říš	k1gFnSc2	říš
Mali	Mali	k1gNnSc2	Mali
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
arabského	arabský	k2eAgNnSc2d1	arabské
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
Šar	Šar	k1gMnSc2	Šar
Bubu	bubu	k1gNnSc2	bubu
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
až	až	k9	až
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
arabští	arabský	k2eAgMnPc1d1	arabský
beduíni	beduín	k1gMnPc1	beduín
kmene	kmen	k1gInSc2	kmen
Banu	Ban	k2eAgFnSc4d1	Bana
Maqtil	Maqtil	k1gFnSc4	Maqtil
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Jemenu	Jemen	k1gInSc2	Jemen
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Berberů	Berber	k1gMnPc2	Berber
zbytek	zbytek	k1gInSc1	zbytek
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kastovní	kastovní	k2eAgInSc1d1	kastovní
systém	systém	k1gInSc1	systém
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vrcholu	vrchol	k1gInSc6	vrchol
stály	stát	k5eAaImAgInP	stát
arabské	arabský	k2eAgInPc1d1	arabský
válečnické	válečnický	k2eAgInPc1d1	válečnický
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
arabizaci	arabizace	k1gFnSc3	arabizace
Berberů	Berber	k1gMnPc2	Berber
a	a	k8xC	a
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
oázách	oáza	k1gFnPc6	oáza
tzv.	tzv.	kA	tzv.
Haratinů	Haratin	k1gInPc2	Haratin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
evropských	evropský	k2eAgMnPc2d1	evropský
obchodníků	obchodník	k1gMnPc2	obchodník
(	(	kIx(	(
<g/>
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
ustavily	ustavit	k5eAaPmAgInP	ustavit
emiráty	emirát	k1gInPc1	emirát
Trarza	Trarz	k1gMnSc2	Trarz
<g/>
,	,	kIx,	,
Brakna	Brakn	k1gMnSc2	Brakn
a	a	k8xC	a
Tagant	Tagant	k1gInSc4	Tagant
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
severu	sever	k1gInSc6	sever
emirát	emirát	k1gInSc1	emirát
Adrar	Adrar	k1gInSc1	Adrar
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
někdejších	někdejší	k2eAgInPc2d1	někdejší
emirátů	emirát	k1gInPc2	emirát
nesou	nést	k5eAaImIp3nP	nést
4	[number]	k4	4
ze	z	k7c2	z
13	[number]	k4	13
moderních	moderní	k2eAgInPc2d1	moderní
mauritánských	mauritánský	k2eAgInPc2d1	mauritánský
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
probíhala	probíhat	k5eAaImAgFnS	probíhat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
kolonizace	kolonizace	k1gFnSc1	kolonizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1902-1909	[number]	k4	1902-1909
byly	být	k5eAaImAgInP	být
vyhlášeny	vyhlášen	k2eAgInPc1d1	vyhlášen
protektoráty	protektorát	k1gInPc1	protektorát
nad	nad	k7c7	nad
všemi	všecek	k3xTgInPc7	všecek
čtyřmi	čtyři	k4xCgInPc7	čtyři
emiráty	emirát	k1gInPc7	emirát
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
však	však	k9	však
Francie	Francie	k1gFnSc1	Francie
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
správu	správa	k1gFnSc4	správa
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
emírů	emír	k1gMnPc2	emír
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
stala	stát	k5eAaPmAgFnS	stát
autonomní	autonomní	k2eAgFnSc7d1	autonomní
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
dnešní	dnešní	k2eAgNnSc1d1	dnešní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Nuakšott	Nuakšotta	k1gFnPc2	Nuakšotta
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vesnice	vesnice	k1gFnSc2	vesnice
Ksar	Ksara	k1gFnPc2	Ksara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Maavíja	Maavíja	k1gMnSc1	Maavíja
uld	uld	k?	uld
Sídí	Sídí	k1gMnSc1	Sídí
Ahmed	Ahmed	k1gMnSc1	Ahmed
Tajá	Tajá	k1gFnSc1	Tajá
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
sesazen	sesazen	k2eAgInSc1d1	sesazen
vojenským	vojenský	k2eAgInSc7d1	vojenský
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
plukovníka	plukovník	k1gMnSc2	plukovník
Alího	Alí	k1gMnSc2	Alí
Uld	Uld	k1gMnSc2	Uld
Mohammeda	Mohammed	k1gMnSc2	Mohammed
Fála	Fála	k1gMnSc1	Fála
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
demokraticky	demokraticky	k6eAd1	demokraticky
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
Sídí	Sídí	k1gNnSc2	Sídí
Uld	Uld	k1gMnSc1	Uld
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Mauritánská	Mauritánský	k2eAgFnSc1d1	Mauritánská
republika	republika	k1gFnSc1	republika
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Afriky	Afrika	k1gFnSc2	Afrika
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
561	[number]	k4	561
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Marokem	Maroko	k1gNnSc7	Maroko
okupovanou	okupovaný	k2eAgFnSc7d1	okupovaná
Západní	západní	k2eAgFnSc7d1	západní
Saharou	Sahara	k1gFnSc7	Sahara
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
463	[number]	k4	463
km	km	kA	km
s	s	k7c7	s
Alžírskem	Alžírsko	k1gNnSc7	Alžírsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
2	[number]	k4	2
237	[number]	k4	237
km	km	kA	km
s	s	k7c7	s
Mali	Mali	k1gNnSc7	Mali
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
813	[number]	k4	813
km	km	kA	km
se	s	k7c7	s
Senegalem	Senegal	k1gInSc7	Senegal
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
754	[number]	k4	754
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
030	[number]	k4	030
700	[number]	k4	700
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
13,4	[number]	k4	13,4
<g/>
×	×	k?	×
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
zabírá	zabírat	k5eAaImIp3nS	zabírat
asi	asi	k9	asi
3,4	[number]	k4	3,4
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
území	území	k1gNnSc2	území
tvoří	tvořit	k5eAaImIp3nP	tvořit
poušť	poušť	k1gFnSc4	poušť
a	a	k8xC	a
polopoušť	polopoušť	k1gFnSc4	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Kediet	Kediet	k1gMnSc1	Kediet
Ijill	Ijill	k1gMnSc1	Ijill
poblíž	poblíž	k7c2	poblíž
západosaharské	západosaharský	k2eAgFnSc2d1	západosaharský
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
915	[number]	k4	915
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
poušť	poušť	k1gFnSc1	poušť
Sahara	Sahara	k1gFnSc1	Sahara
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
málo	málo	k6eAd1	málo
úrodná	úrodný	k2eAgFnSc1d1	úrodná
oblast	oblast	k1gFnSc1	oblast
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
částí	část	k1gFnSc7	část
mauritánského	mauritánský	k2eAgNnSc2d1	mauritánský
atlantského	atlantský	k2eAgNnSc2d1	Atlantské
pobřeží	pobřeží	k1gNnSc2	pobřeží
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Nationalpark	Nationalpark	k1gInSc1	Nationalpark
Banc	Banc	k1gInSc1	Banc
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arguin	Arguin	k1gMnSc1	Arguin
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Parc	Parc	k1gInSc1	Parc
national	nationat	k5eAaPmAgMnS	nationat
du	du	k?	du
Diawling	Diawling	k1gInSc4	Diawling
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
řeky	řeka	k1gFnSc2	řeka
Senegal	Senegal	k1gInSc1	Senegal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Nuakšott	Nuakšott	k1gInSc1	Nuakšott
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
480	[number]	k4	480
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nuadhibú	Nuadhibú	k?	Nuadhibú
(	(	kIx(	(
<g/>
59	[number]	k4	59
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kaédi	Kaéd	k1gMnPc1	Kaéd
(	(	kIx(	(
<g/>
31	[number]	k4	31
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
uznávajících	uznávající	k2eAgFnPc2d1	uznávající
Izrael	Izrael	k1gInSc4	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
dědictvím	dědictví	k1gNnSc7	dědictví
režimu	režim	k1gInSc2	režim
Maaúja	Maaújus	k1gMnSc2	Maaújus
Uld	Uld	k1gMnSc2	Uld
Sídí	Sídí	k2eAgFnSc1d1	Sídí
Ahmada	Ahmada	k1gFnSc1	Ahmada
Táji	Táj	k1gFnSc2	Táj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nejprve	nejprve	k6eAd1	nejprve
podporoval	podporovat	k5eAaImAgInS	podporovat
Irák	Irák	k1gInSc1	Irák
při	při	k7c6	při
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
v	v	k7c6	v
obavách	obava	k1gFnPc6	obava
z	z	k7c2	z
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
izolace	izolace	k1gFnSc2	izolace
i	i	k8xC	i
domácího	domácí	k2eAgInSc2d1	domácí
odporu	odpor	k1gInSc2	odpor
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
začal	začít	k5eAaPmAgInS	začít
sbližovat	sbližovat	k5eAaImF	sbližovat
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
pouštní	pouštní	k2eAgFnSc2d1	pouštní
oblasti	oblast	k1gFnSc2	oblast
sahelských	sahelský	k2eAgInPc2d1	sahelský
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
malé	malý	k2eAgFnSc3d1	malá
hustotě	hustota	k1gFnSc3	hustota
osídlení	osídlení	k1gNnSc2	osídlení
a	a	k8xC	a
absolutní	absolutní	k2eAgFnSc2d1	absolutní
propustnosti	propustnost	k1gFnSc2	propustnost
hranic	hranice	k1gFnPc2	hranice
staly	stát	k5eAaPmAgFnP	stát
vítaným	vítaný	k2eAgNnSc7d1	vítané
útočištěm	útočiště	k1gNnSc7	útočiště
pro	pro	k7c4	pro
teroristické	teroristický	k2eAgFnPc4d1	teroristická
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
a	a	k8xC	a
sousedního	sousední	k2eAgNnSc2d1	sousední
Mali	Mali	k1gNnSc2	Mali
tak	tak	k9	tak
operuje	operovat	k5eAaImIp3nS	operovat
Saláfistická	Saláfistický	k2eAgFnSc1d1	Saláfistická
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
kázání	kázání	k1gNnSc4	kázání
a	a	k8xC	a
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
údajně	údajně	k6eAd1	údajně
disponuje	disponovat	k5eAaBmIp3nS	disponovat
stovkami	stovka	k1gFnPc7	stovka
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
hrozby	hrozba	k1gFnPc1	hrozba
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
regionem	region	k1gInSc7	region
nezůstaly	zůstat	k5eNaPmAgFnP	zůstat
nepovšimnuty	povšimnut	k2eNgFnPc1d1	nepovšimnuta
ani	ani	k8xC	ani
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
světovému	světový	k2eAgInSc3d1	světový
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Pansahelské	Pansahelský	k2eAgFnSc2d1	Pansahelský
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yIgNnSc1	který
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
pomáhaly	pomáhat	k5eAaImAgInP	pomáhat
budovat	budovat	k5eAaImF	budovat
místní	místní	k2eAgFnPc4d1	místní
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
boji	boj	k1gInSc3	boj
s	s	k7c7	s
teroristickými	teroristický	k2eAgFnPc7d1	teroristická
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Transsaharskou	Transsaharský	k2eAgFnSc7d1	Transsaharský
protiteroristickou	protiteroristický	k2eAgFnSc7d1	protiteroristická
iniciativou	iniciativa	k1gFnSc7	iniciativa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kromě	kromě	k7c2	kromě
citelného	citelný	k2eAgNnSc2d1	citelné
navýšení	navýšení	k1gNnSc2	navýšení
dostupných	dostupný	k2eAgInPc2d1	dostupný
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
roční	roční	k2eAgInSc1d1	roční
rozpočet	rozpočet	k1gInSc1	rozpočet
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
)	)	kIx)	)
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
dimenze	dimenze	k1gFnPc4	dimenze
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
podezřelých	podezřelý	k2eAgInPc2d1	podezřelý
finančních	finanční	k2eAgInPc2d1	finanční
toků	tok	k1gInPc2	tok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
severnějších	severní	k2eAgMnPc2d2	severnější
sousedů	soused	k1gMnPc2	soused
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
ušetřena	ušetřen	k2eAgFnSc1d1	ušetřena
významnějších	významný	k2eAgInPc2d2	významnější
teroristických	teroristický	k2eAgInPc2d1	teroristický
úderů	úder	k1gInPc2	úder
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
zatím	zatím	k6eAd1	zatím
nejvíce	nejvíce	k6eAd1	nejvíce
rozvášnil	rozvášnit	k5eAaPmAgInS	rozvášnit
útok	útok	k1gInSc1	útok
islamistů	islamista	k1gMnPc2	islamista
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
posádku	posádka	k1gFnSc4	posádka
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
15	[number]	k4	15
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Nuakšott	Nuakšott	k1gInSc1	Nuakšott
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
několika	několik	k4yIc6	několik
připravovaných	připravovaný	k2eAgInPc6d1	připravovaný
atentátech	atentát	k1gInPc6	atentát
a	a	k8xC	a
únosech	únos	k1gInPc6	únos
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
místním	místní	k2eAgFnPc3d1	místní
bezpečnostním	bezpečnostní	k2eAgFnPc3d1	bezpečnostní
složkám	složka	k1gFnPc3	složka
podařilo	podařit	k5eAaPmAgNnS	podařit
zhatit	zhatit	k5eAaPmF	zhatit
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
pomoc	pomoc	k1gFnSc1	pomoc
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Washingtonu	Washington	k1gInSc2	Washington
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
místní	místní	k2eAgFnSc7d1	místní
vládou	vláda	k1gFnSc7	vláda
více	hodně	k6eAd2	hodně
než	než	k8xS	než
vítána	vítat	k5eAaImNgFnS	vítat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
její	její	k3xOp3gFnSc6	její
existenci	existence	k1gFnSc6	existence
svědčí	svědčit	k5eAaImIp3nS	svědčit
překvapivě	překvapivě	k6eAd1	překvapivě
dobrá	dobrý	k2eAgFnSc1d1	dobrá
výzbroj	výzbroj	k1gFnSc1	výzbroj
mauritánské	mauritánský	k2eAgFnSc2d1	mauritánská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Senegalem	Senegal	k1gInSc7	Senegal
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
poškozeny	poškozen	k2eAgInPc4d1	poškozen
hraničními	hraniční	k2eAgInPc7d1	hraniční
spory	spor	k1gInPc7	spor
i	i	k8xC	i
pogromy	pogrom	k1gInPc7	pogrom
na	na	k7c4	na
černošské	černošský	k2eAgMnPc4d1	černošský
obyvatele	obyvatel	k1gMnPc4	obyvatel
jižní	jižní	k2eAgFnSc2d1	jižní
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
<g/>
.	.	kIx.	.
</s>
<s>
Nuakšott	Nuakšott	k1gMnSc1	Nuakšott
navíc	navíc	k6eAd1	navíc
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
Hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
západoafrických	západoafrický	k2eAgInPc2d1	západoafrický
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
ECOWAS	ECOWAS	kA	ECOWAS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
Západní	západní	k2eAgFnSc7d1	západní
Saharou	Sahara	k1gFnSc7	Sahara
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
lemuje	lemovat	k5eAaImIp3nS	lemovat
minové	minový	k2eAgNnSc4d1	minové
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
porušuje	porušovat	k5eAaImIp3nS	porušovat
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
ateismus	ateismus	k1gInSc4	ateismus
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
islámu	islám	k1gInSc2	islám
uzákoněn	uzákoněn	k2eAgInSc1d1	uzákoněn
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kritiku	kritika	k1gFnSc4	kritika
islámu	islám	k1gInSc2	islám
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
tamní	tamní	k2eAgMnSc1d1	tamní
novinář	novinář	k1gMnSc1	novinář
Mohamed	Mohamed	k1gMnSc1	Mohamed
Cheikh	Cheikh	k1gMnSc1	Cheikh
Ould	Ould	k1gMnSc1	Ould
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
800	[number]	k4	800
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
otroctví	otroctví	k1gNnSc6	otroctví
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
hlav	hlava	k1gFnPc2	hlava
státu	stát	k1gInSc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
–	–	k?	–
Moktar	Moktar	k1gMnSc1	Moktar
Ould	Ould	k1gMnSc1	Ould
Daddah	Daddah	k1gMnSc1	Daddah
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PRM	PRM	kA	PRM
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
–	–	k?	–
Moktar	Moktar	k1gMnSc1	Moktar
Ould	Ould	k1gMnSc1	Ould
Daddah	Daddah	k1gMnSc1	Daddah
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PPM	PPM	kA	PPM
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
Mustafa	Mustaf	k1gMnSc4	Mustaf
Ould	Ould	k1gMnSc1	Ould
Salek	Salek	k1gMnSc1	Salek
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
obnovy	obnova	k1gFnSc2	obnova
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
Mustafa	Mustaf	k1gMnSc4	Mustaf
Ould	Ould	k1gMnSc1	Ould
Salek	Salek	k1gMnSc1	Salek
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
obnovy	obnova	k1gFnSc2	obnova
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
Mustafa	Mustaf	k1gMnSc4	Mustaf
Ould	Ould	k1gMnSc1	Ould
Salek	Salek	k1gMnSc1	Salek
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
spásy	spása	k1gFnSc2	spása
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
Mohamed	Mohamed	k1gMnSc1	Mohamed
Mahmoud	Mahmoud	k1gMnSc1	Mahmoud
Ould	Ould	k1gMnSc1	Ould
Louly	Loula	k1gFnSc2	Loula
<g/>
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
spásy	spása	k1gFnSc2	spása
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
–	–	k?	–
Mohamed	Mohamed	k1gMnSc1	Mohamed
Khouna	Khouna	k1gFnSc1	Khouna
Ould	Ould	k1gMnSc1	Ould
Haidalla	Haidalla	k1gMnSc1	Haidalla
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
spásy	spása	k1gFnSc2	spása
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
–	–	k?	–
Maaouya	Maaouya	k1gMnSc1	Maaouya
Ould	Ould	k1gMnSc1	Ould
Sid	Sid	k1gMnSc1	Sid
<g/>
'	'	kIx"	'
<g/>
Ahmed	Ahmed	k1gMnSc1	Ahmed
Taya	Taya	k1gMnSc1	Taya
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
spásy	spása	k1gFnSc2	spása
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
–	–	k?	–
Maaouya	Maaouya	k1gMnSc1	Maaouya
Ould	Ould	k1gMnSc1	Ould
Sid	Sid	k1gMnSc1	Sid
<g/>
'	'	kIx"	'
<g/>
Ahmed	Ahmed	k1gMnSc1	Ahmed
Taya	Taya	k1gMnSc1	Taya
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PRDS	PRDS	kA	PRDS
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
–	–	k?	–
Ely	Ela	k1gFnSc2	Ela
Ould	Ould	k1gMnSc1	Ould
Mohamed	Mohamed	k1gMnSc1	Mohamed
Vall	Vall	k1gMnSc1	Vall
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
Sidi	Sid	k1gFnSc2	Sid
Mohamed	Mohamed	k1gMnSc1	Mohamed
Ould	Ould	k1gMnSc1	Ould
Cheikh	Cheikh	k1gMnSc1	Cheikh
Abdellahi	Abdellah	k1gMnSc3	Abdellah
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
–	–	k?	–
Mohammed	Mohammed	k1gMnSc1	Mohammed
Ould	Ould	k1gMnSc1	Ould
Abdel	Abdel	k1gMnSc1	Abdel
Aziz	Aziz	k1gMnSc1	Aziz
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
–	–	k?	–
Ba	ba	k9	ba
Mamadou	Mamada	k1gFnSc7	Mamada
dit	dit	k?	dit
M	M	kA	M
<g/>
'	'	kIx"	'
<g/>
Baré	Barý	k2eAgNnSc1d1	Barý
–	–	k?	–
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
–	–	k?	–
Mohamed	Mohamed	k1gMnSc1	Mohamed
Ould	Ould	k1gMnSc1	Ould
Abdel	Abdel	k1gMnSc1	Abdel
Aziz	Aziz	k1gMnSc1	Aziz
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UPR	UPR	kA	UPR
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
70	[number]	k4	70
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
jsou	být	k5eAaImIp3nP	být
Maurové	Maurové	k?	Maurové
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Maury	Maur	k1gMnPc7	Maur
byli	být	k5eAaImAgMnP	být
označováni	označovat	k5eAaImNgMnP	označovat
také	také	k9	také
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
okupovali	okupovat	k5eAaBmAgMnP	okupovat
jižní	jižní	k2eAgNnSc4d1	jižní
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Mauři	Maur	k1gMnPc1	Maur
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
jsou	být	k5eAaImIp3nP	být
smíšeného	smíšený	k2eAgInSc2d1	smíšený
arabsko-berberského	arabskoerberský	k2eAgInSc2d1	arabsko-berberský
a	a	k8xC	a
černošského	černošský	k2eAgInSc2d1	černošský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Mauři	Maur	k1gMnPc1	Maur
jsou	být	k5eAaImIp3nP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
etno-lingvistických	etnoingvistický	k2eAgFnPc2d1	etno-lingvistický
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozlišováni	rozlišován	k2eAgMnPc1d1	rozlišován
rasově	rasově	k6eAd1	rasově
na	na	k7c4	na
Bílé	bílý	k2eAgMnPc4d1	bílý
Maury	Maur	k1gMnPc4	Maur
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
-	-	kIx~	-
asi	asi	k9	asi
30	[number]	k4	30
<g/>
%	%	kIx~	%
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
Černé	Černé	k2eAgMnPc4d1	Černé
Maury	Maur	k1gMnPc4	Maur
(	(	kIx(	(
<g/>
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
obtížné	obtížný	k2eAgNnSc1d1	obtížné
rozlišit	rozlišit	k5eAaPmF	rozlišit
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
Mauři	Maur	k1gMnPc1	Maur
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Černí	černý	k2eAgMnPc1d1	černý
Mauři	Maur	k1gMnPc1	Maur
(	(	kIx(	(
<g/>
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
také	také	k6eAd1	také
jako	jako	k9	jako
Haratinové	Haratin	k1gMnPc1	Haratin
nebo	nebo	k8xC	nebo
osvobození	osvobozený	k2eAgMnPc1d1	osvobozený
otroci	otrok	k1gMnPc1	otrok
<g/>
)	)	kIx)	)
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
slabší	slabý	k2eAgMnPc1d2	slabší
než	než	k8xS	než
Bílí	bílý	k2eAgMnPc1d1	bílý
Mauři	Maur	k1gMnPc1	Maur
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
černí	černý	k2eAgMnPc1d1	černý
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Afro-Mauritánci	Afro-Mauritánek	k1gMnPc1	Afro-Mauritánek
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Afro-mauritánské	Afroauritánský	k2eAgInPc4d1	Afro-mauritánský
kmeny	kmen	k1gInPc4	kmen
jako	jako	k8xS	jako
např.	např.	kA	např.
Halpulaar	Halpulaar	k1gInSc1	Halpulaar
<g/>
,	,	kIx,	,
Wolof	Wolof	k1gInSc1	Wolof
a	a	k8xC	a
Soninke	Soninke	k1gInSc1	Soninke
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Černí	černý	k2eAgMnPc1d1	černý
Mauři	Maur	k1gMnPc1	Maur
a	a	k8xC	a
Afro-Mauritánci	Afro-Mauritánek	k1gMnPc1	Afro-Mauritánek
jsou	být	k5eAaImIp3nP	být
početně	početně	k6eAd1	početně
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
a	a	k8xC	a
soukromém	soukromý	k2eAgInSc6d1	soukromý
sektoru	sektor	k1gInSc6	sektor
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
podřazené	podřazený	k2eAgNnSc4d1	podřazené
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
mauritánského	mauritánský	k2eAgInSc2d1	mauritánský
parlamentu	parlament	k1gInSc2	parlament
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
Černý	černý	k2eAgMnSc1d1	černý
Maur	Maur	k1gMnSc1	Maur
<g/>
.	.	kIx.	.
<g/>
Francouzi	Francouz	k1gMnPc1	Francouz
tvoří	tvořit	k5eAaImIp3nP	tvořit
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
99	[number]	k4	99
%	%	kIx~	%
Mauritánců	Mauritánec	k1gMnPc2	Mauritánec
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
a	a	k8xC	a
arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
i	i	k9	i
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
mizivá	mizivý	k2eAgFnSc1d1	mizivá
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
menšina	menšina	k1gFnSc1	menšina
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
34	[number]	k4	34
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Problémem	problém	k1gInSc7	problém
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
jsou	být	k5eAaImIp3nP	být
rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
slumy	slum	k1gInPc1	slum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pastevci	pastevec	k1gMnPc1	pastevec
jsou	být	k5eAaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
pouští	poušť	k1gFnSc7	poušť
vyháněni	vyhánět	k5eAaImNgMnP	vyhánět
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
pastev	pastva	k1gFnPc2	pastva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
jde	jít	k5eAaImIp3nS	jít
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
veškerého	veškerý	k3xTgInSc2	veškerý
vývozu	vývoz	k1gInSc2	vývoz
<g/>
)	)	kIx)	)
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
odběrateli	odběratel	k1gMnSc3	odběratel
jsou	být	k5eAaImIp3nP	být
EU	EU	kA	EU
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Veškerou	veškerý	k3xTgFnSc4	veškerý
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
přepravují	přepravovat	k5eAaImIp3nP	přepravovat
vlaky	vlak	k1gInPc1	vlak
po	po	k7c6	po
Mauritánské	Mauritánský	k2eAgFnSc6d1	Mauritánská
železnici	železnice	k1gFnSc6	železnice
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Nuadhibú	Nuadhibú	k1gFnSc2	Nuadhibú
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
má	můj	k3xOp1gFnSc1	můj
země	zem	k1gFnPc1	zem
nadměrně	nadměrně	k6eAd1	nadměrně
bohaté	bohatý	k2eAgFnPc1d1	bohatá
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
lov	lov	k1gInSc1	lov
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
tento	tento	k3xDgInSc1	tento
významný	významný	k2eAgInSc1d1	významný
generátor	generátor	k1gInSc1	generátor
vývozních	vývozní	k2eAgInPc2d1	vývozní
příjmů	příjem	k1gInPc2	příjem
i	i	k8xC	i
zdroj	zdroj	k1gInSc1	zdroj
obživy	obživa	k1gFnSc2	obživa
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
existence	existence	k1gFnSc1	existence
ložisek	ložisko	k1gNnPc2	ložisko
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
měly	mít	k5eAaImAgInP	mít
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
těžby	těžba	k1gFnSc2	těžba
ropy	ropa	k1gFnSc2	ropa
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
47	[number]	k4	47
miliard	miliarda	k4xCgFnPc2	miliarda
ukíjá	ukíjé	k1gNnPc4	ukíjé
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
představovalo	představovat	k5eAaImAgNnS	představovat
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
úplatkářským	úplatkářský	k2eAgInSc7d1	úplatkářský
skandálem	skandál	k1gInSc7	skandál
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
zapojena	zapojen	k2eAgFnSc1d1	zapojena
australská	australský	k2eAgFnSc1d1	australská
firma	firma	k1gFnSc1	firma
Woodside	Woodsid	k1gMnSc5	Woodsid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dovážejí	dovážet	k5eAaImIp3nP	dovážet
se	se	k3xPyFc4	se
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
spotřební	spotřební	k2eAgNnSc4d1	spotřební
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
500	[number]	k4	500
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
mauritánská	mauritánský	k2eAgFnSc1d1	mauritánská
ukíjá	ukíjá	k1gFnSc1	ukíjá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
KOMERS	komers	k1gInSc1	komers
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
–	–	k?	–
země	zem	k1gFnSc2	zem
v	v	k7c6	v
dunách	duna	k1gFnPc6	duna
<g/>
.	.	kIx.	.
</s>
<s>
Koktejl	koktejl	k1gInSc1	koktejl
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2005-08-31	[number]	k4	2005-08-31
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mauritania	Mauritanium	k1gNnPc1	Mauritanium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mauritania	Mauritanium	k1gNnPc1	Mauritanium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Mauritania	Mauritanium	k1gNnSc2	Mauritanium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Mauritania	Mauritanium	k1gNnSc2	Mauritanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Mauritania	Mauritanium	k1gNnSc2	Mauritanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-08	[number]	k4	2011-07-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Rabatu	rabat	k1gInSc6	rabat
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-10-24	[number]	k4	2010-10-24
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DESCHAMPS	DESCHAMPS	kA	DESCHAMPS
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
Jules	Jules	k1gMnSc1	Jules
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Mauritania	Mauritanium	k1gNnPc1	Mauritanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
