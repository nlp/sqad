<s>
Poloměr	poloměr	k1gInSc1	poloměr
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
6,5	[number]	k4	6,5
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
plyne	plynout	k5eAaImIp3nS	plynout
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
křivost	křivost	k1gFnSc1	křivost
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
