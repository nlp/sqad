<p>
<s>
Bohumín	Bohumín	k1gInSc1	Bohumín
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Bogumin	Bogumin	k2eAgInSc1d1	Bogumin
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Oderberg	Oderberg	k1gInSc1	Oderberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Karviná	Karviná	k1gFnSc1	Karviná
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
9	[number]	k4	9
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
části	část	k1gFnSc6	část
historického	historický	k2eAgNnSc2d1	historické
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
také	také	k9	také
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Odry	Odra	k1gFnSc2	Odra
a	a	k8xC	a
Olše	olše	k1gFnSc2	olše
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
31	[number]	k4	31
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
měl	mít	k5eAaImAgInS	mít
Bohumín	Bohumín	k1gInSc1	Bohumín
významnou	významný	k2eAgFnSc4d1	významná
strategickou	strategický	k2eAgFnSc4d1	strategická
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
význam	význam	k1gInSc4	význam
si	se	k3xPyFc3	se
město	město	k1gNnSc1	město
udrželo	udržet	k5eAaPmAgNnS	udržet
do	do	k7c2	do
dnešních	dnešní	k2eAgMnPc2d1	dnešní
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ležel	ležet	k5eAaImAgMnS	ležet
Bohumín	Bohumín	k1gInSc4	Bohumín
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
solná	solný	k2eAgFnSc1d1	solná
<g/>
)	)	kIx)	)
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
jantarová	jantarový	k2eAgFnSc1d1	jantarová
<g/>
)	)	kIx)	)
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
přes	přes	k7c4	přes
Těšín	Těšín	k1gInSc4	Těšín
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
Baltu	Balt	k1gInSc2	Balt
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
tudy	tudy	k6eAd1	tudy
procházela	procházet	k5eAaImAgFnS	procházet
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
tedy	tedy	k9	tedy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
frekventovaném	frekventovaný	k2eAgNnSc6d1	frekventované
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
stavba	stavba	k1gFnSc1	stavba
mostu	most	k1gInSc2	most
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
ten	ten	k3xDgInSc4	ten
byl	být	k5eAaImAgInS	být
však	však	k9	však
nespočetněkrát	nespočetněkrát	k6eAd1	nespočetněkrát
strhnut	strhnout	k5eAaPmNgInS	strhnout
při	při	k7c6	při
rozvodnění	rozvodnění	k1gNnSc6	rozvodnění
Odry	Odra	k1gFnSc2	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Stoupajícím	stoupající	k2eAgInSc7d1	stoupající
cestovním	cestovní	k2eAgInSc7d1	cestovní
ruchem	ruch	k1gInSc7	ruch
přibývalo	přibývat	k5eAaImAgNnS	přibývat
osadníků	osadník	k1gMnPc2	osadník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
osada	osada	k1gFnSc1	osada
nesoucí	nesoucí	k2eAgNnSc4d1	nesoucí
jméno	jméno	k1gNnSc4	jméno
Bogun	Boguna	k1gFnPc2	Boguna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
již	již	k6eAd1	již
velkou	velký	k2eAgFnSc7d1	velká
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Držení	držení	k1gNnSc1	držení
vesnice	vesnice	k1gFnSc2	vesnice
bylo	být	k5eAaImAgNnS	být
finančně	finančně	k6eAd1	finančně
(	(	kIx(	(
<g/>
vybírání	vybírání	k1gNnSc4	vybírání
mýtného	mýtný	k1gMnSc2	mýtný
od	od	k7c2	od
pocestných	pocestná	k1gFnPc2	pocestná
<g/>
)	)	kIx)	)
a	a	k8xC	a
strategicky	strategicky	k6eAd1	strategicky
(	(	kIx(	(
<g/>
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
<g/>
)	)	kIx)	)
výhodné	výhodný	k2eAgFnPc4d1	výhodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
formulářové	formulářový	k2eAgFnSc2d1	formulářová
sbírky	sbírka	k1gFnSc2	sbírka
královny	královna	k1gFnSc2	královna
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gMnSc1	originál
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nejspíš	nejspíš	k9	nejspíš
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
stylistické	stylistický	k2eAgNnSc4d1	stylistické
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1	zeměpisný
poměry	poměr	k1gInPc1	poměr
v	v	k7c6	v
textu	text	k1gInSc6	text
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
velmi	velmi	k6eAd1	velmi
reálně	reálně	k6eAd1	reálně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
Bohumín	Bohumín	k1gInSc4	Bohumín
s	s	k7c7	s
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
Bogunem	Bogun	k1gInSc7	Bogun
<g/>
.	.	kIx.	.
</s>
<s>
Datace	datace	k1gFnSc1	datace
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
rok	rok	k1gInSc4	rok
1256	[number]	k4	1256
a	a	k8xC	a
1262	[number]	k4	1262
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
trvale	trvale	k6eAd1	trvale
osídlená	osídlený	k2eAgFnSc1d1	osídlená
osada	osada	k1gFnSc1	osada
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jména	jméno	k1gNnSc2	jméno
můžeme	moct	k5eAaImIp1nP	moct
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
slovanské	slovanský	k2eAgNnSc4d1	slovanské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
kláštera	klášter	k1gInSc2	klášter
orlovského	orlovský	k2eAgInSc2d1	orlovský
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Bohumín	Bohumín	k1gInSc1	Bohumín
náležel	náležet	k5eAaImAgInS	náležet
k	k	k7c3	k
Ratibořskému	ratibořský	k2eAgNnSc3d1	Ratibořské
knížectví	knížectví	k1gNnSc3	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
listina	listina	k1gFnSc1	listina
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejstarším	starý	k2eAgInSc7d3	nejstarší
dokladem	doklad	k1gInSc7	doklad
německého	německý	k2eAgInSc2d1	německý
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
Bohumín	Bohumín	k1gInSc4	Bohumín
-	-	kIx~	-
Oderberg	Oderberg	k1gInSc1	Oderberg
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bohumín	Bohumín	k1gInSc1	Bohumín
byl	být	k5eAaImAgInS	být
obydlován	obydlovat	k5eAaImNgInS	obydlovat
německými	německý	k2eAgMnPc7d1	německý
osadníky	osadník	k1gMnPc7	osadník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
kostela	kostel	k1gInSc2	kostel
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
zděný	zděný	k2eAgInSc1d1	zděný
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
známý	známý	k2eAgInSc1d1	známý
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
Bohumínsko	Bohumínsko	k1gNnSc4	Bohumínsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
prastarý	prastarý	k2eAgInSc1d1	prastarý
rod	rod	k1gInSc1	rod
Baruthů	Baruth	k1gMnPc2	Baruth
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nepříliš	příliš	k6eNd1	příliš
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přechází	přecházet	k5eAaImIp3nS	přecházet
Bohumínsko	Bohumínsko	k1gNnSc1	Bohumínsko
do	do	k7c2	do
držby	držba	k1gFnSc2	držba
rodu	rod	k1gInSc2	rod
Rašiců	Rašiec	k1gInPc2	Rašiec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
listiny	listina	k1gFnSc2	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1373	[number]	k4	1373
byl	být	k5eAaImAgInS	být
Bohumín	Bohumín	k1gInSc1	Bohumín
již	již	k6eAd1	již
poddanským	poddanský	k2eAgNnSc7d1	poddanské
městečkem	městečko	k1gNnSc7	městečko
pod	pod	k7c7	pod
pravomocí	pravomoc	k1gFnSc7	pravomoc
panské	panský	k2eAgFnSc2d1	Panská
vrchnosti	vrchnost	k1gFnSc2	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Zmínka	zmínka	k1gFnSc1	zmínka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
o	o	k7c6	o
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Bohumínské	bohumínský	k2eAgNnSc1d1	Bohumínské
panství	panství	k1gNnSc1	panství
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
roku	rok	k1gInSc2	rok
1407	[number]	k4	1407
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
ratibořským	ratibořský	k2eAgMnSc7d1	ratibořský
knížetem	kníže	k1gMnSc7	kníže
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
těšínskými	těšínský	k2eAgMnPc7d1	těšínský
knížaty	kníže	k1gMnPc7wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
si	se	k3xPyFc3	se
nárokovaly	nárokovat	k5eAaImAgFnP	nárokovat
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
Bohumínska	Bohumínsko	k1gNnSc2	Bohumínsko
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1407	[number]	k4	1407
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Jana	Jan	k1gMnSc2	Jan
Ratibořského	ratibořský	k2eAgMnSc2d1	ratibořský
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přešlo	přejít	k5eAaPmAgNnS	přejít
panství	panství	k1gNnPc4	panství
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
zemských	zemský	k2eAgMnPc2d1	zemský
vévodů	vévoda	k1gMnPc2	vévoda
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
různých	různý	k2eAgInPc2d1	různý
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1409	[number]	k4	1409
až	až	k9	až
1423	[number]	k4	1423
drželi	držet	k5eAaImAgMnP	držet
panství	panství	k1gNnSc4	panství
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Tvorkova	tvorkův	k2eAgNnSc2d1	tvorkův
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
majetek	majetek	k1gInSc1	majetek
prodán	prodat	k5eAaPmNgInS	prodat
Janu	Jan	k1gMnSc3	Jan
Bělíkovi	Bělík	k1gMnSc3	Bělík
z	z	k7c2	z
Kornic	Kornice	k1gFnPc2	Kornice
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
potomkům	potomek	k1gMnPc3	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Kornicové	Kornicový	k2eAgNnSc1d1	Kornicový
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
panství	panství	k1gNnSc1	panství
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
majetek	majetek	k1gInSc4	majetek
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
i	i	k9	i
ve	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
opolském	opolský	k2eAgNnSc6d1	Opolské
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
majetek	majetek	k1gInSc1	majetek
rodu	rod	k1gInSc2	rod
Korniců	Kornice	k1gMnPc2	Kornice
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
Zabelkow	Zabelkow	k1gFnSc4	Zabelkow
<g/>
,	,	kIx,	,
Odru	Odra	k1gFnSc4	Odra
<g/>
,	,	kIx,	,
Lhotu	Lhota	k1gFnSc4	Lhota
a	a	k8xC	a
Pudlov	Pudlov	k1gInSc4	Pudlov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1451	[number]	k4	1451
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
majitelem	majitel	k1gMnSc7	majitel
Bohumínska	Bohumínsko	k1gNnSc2	Bohumínsko
(	(	kIx(	(
<g/>
Bohunyna	Bohunyna	k1gFnSc1	Bohunyna
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Tovačovský	Tovačovský	k1gMnSc1	Tovačovský
z	z	k7c2	z
Cimburka	Cimburek	k1gMnSc2	Cimburek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	on	k3xPp3gNnSc4	on
prodává	prodávat	k5eAaImIp3nS	prodávat
roku	rok	k1gInSc2	rok
1473	[number]	k4	1473
Janovi	Jan	k1gMnSc6	Jan
z	z	k7c2	z
Vrbna	Vrbno	k1gNnSc2	Vrbno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1482	[number]	k4	1482
přechází	přecházet	k5eAaImIp3nS	přecházet
panství	panství	k1gNnSc4	panství
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Jana	Jan	k1gMnSc2	Jan
Buřeje	Buřej	k1gMnSc2	Buřej
z	z	k7c2	z
Klvova	Klvov	k1gInSc2	Klvov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
záhy	záhy	k6eAd1	záhy
umírá	umírat	k5eAaImIp3nS	umírat
roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
spory	spor	k1gInPc1	spor
o	o	k7c6	o
panství	panství	k1gNnSc6	panství
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
řešily	řešit	k5eAaImAgFnP	řešit
opět	opět	k6eAd1	opět
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ratibořských	ratibořský	k2eAgMnPc2d1	ratibořský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
Valentin	Valentin	k1gMnSc1	Valentin
Hrbatý	hrbatý	k2eAgMnSc1d1	hrbatý
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
Bohumínsko	Bohumínsko	k1gNnSc1	Bohumínsko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Janu	Jan	k1gMnSc3	Jan
Opolskému	opolský	k2eAgMnSc3d1	opolský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
prodal	prodat	k5eAaPmAgMnS	prodat
Jiřímu	Jiří	k1gMnSc3	Jiří
Braniborsko-Ansbašskému	Braniborsko-Ansbašský	k2eAgMnSc3d1	Braniborsko-Ansbašský
<g/>
,	,	kIx,	,
braniborskému	braniborský	k2eAgMnSc3d1	braniborský
markraběti	markrabě	k1gMnSc3	markrabě
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Hohenzollernů	Hohenzollern	k1gInPc2	Hohenzollern
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1528	[number]	k4	1528
přijal	přijmout	k5eAaPmAgMnS	přijmout
markrabí	markrabí	k1gMnSc1	markrabí
lutherskou	lutherský	k2eAgFnSc4d1	lutherská
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nadšencem	nadšenec	k1gMnSc7	nadšenec
pro	pro	k7c4	pro
reformační	reformační	k2eAgNnSc4d1	reformační
hnutí	hnutí	k1gNnSc4	hnutí
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
dostal	dostat	k5eAaPmAgInS	dostat
přídomek	přídomek	k1gInSc1	přídomek
Pobožný	pobožný	k2eAgInSc1d1	pobožný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
nesmlouvavou	smlouvavý	k2eNgFnSc7d1	nesmlouvavá
politikou	politika	k1gFnSc7	politika
vůči	vůči	k7c3	vůči
starousedlé	starousedlý	k2eAgFnSc3d1	starousedlá
šlechtě	šlechta	k1gFnSc3	šlechta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
neoblíbených	oblíbený	k2eNgFnPc2d1	neoblíbená
postav	postava	k1gFnPc2	postava
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Bohumín	Bohumín	k1gInSc1	Bohumín
se	se	k3xPyFc4	se
de	de	k?	de
facto	facto	k1gNnSc4	facto
stal	stát	k5eAaPmAgInS	stát
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
braniborské	braniborský	k2eAgFnSc2d1	Braniborská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
zrušili	zrušit	k5eAaPmAgMnP	zrušit
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
Bohumínsko	Bohumínsko	k1gNnSc4	Bohumínsko
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
pouze	pouze	k6eAd1	pouze
zástavním	zástavní	k2eAgMnSc7d1	zástavní
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
nad	nad	k7c7	nad
državami	država	k1gFnPc7	država
Hohenzollernů	Hohenzollern	k1gMnPc2	Hohenzollern
ujal	ujmout	k5eAaPmAgMnS	ujmout
poručník	poručník	k1gMnSc1	poručník
jeho	jeho	k3xOp3gMnSc1	jeho
nezletilého	zletilý	k2eNgMnSc4d1	nezletilý
syna	syn	k1gMnSc4	syn
Jiřího	Jiří	k1gMnSc4	Jiří
Fridricha	Fridrich	k1gMnSc4	Fridrich
Albrecht	Albrecht	k1gMnSc1	Albrecht
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnPc2	jeho
regentství	regentství	k1gNnPc2	regentství
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
majetek	majetek	k1gInSc4	majetek
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
silně	silně	k6eAd1	silně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
germanizace	germanizace	k1gFnSc1	germanizace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
svých	svůj	k3xOyFgInPc2	svůj
statků	statek	k1gInPc2	statek
Jiří	Jiří	k1gMnSc1	Jiří
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
především	především	k6eAd1	především
jako	jako	k8xC	jako
mecenáš	mecenáš	k1gMnSc1	mecenáš
renesančního	renesanční	k2eAgNnSc2d1	renesanční
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
horlivý	horlivý	k2eAgMnSc1d1	horlivý
protestant	protestant	k1gMnSc1	protestant
a	a	k8xC	a
stoupenec	stoupenec	k1gMnSc1	stoupenec
germanizačního	germanizační	k2eAgInSc2d1	germanizační
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
Jiří	Jiří	k1gMnSc1	Jiří
Fridrich	Fridrich	k1gMnSc1	Fridrich
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Jáchyma	Jáchym	k1gMnSc2	Jáchym
Fridricha	Fridrich	k1gMnSc2	Fridrich
<g/>
,	,	kIx,	,
braniborského	braniborský	k2eAgMnSc2d1	braniborský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1606	[number]	k4	1606
předal	předat	k5eAaPmAgMnS	předat
majetek	majetek	k1gInSc4	majetek
svému	svůj	k3xOyFgMnSc3	svůj
druhorozenému	druhorozený	k2eAgMnSc3d1	druhorozený
synovi	syn	k1gMnSc3	syn
Janu	Jan	k1gMnSc3	Jan
Jiřímu	Jiří	k1gMnSc3	Jiří
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
štrasburským	štrasburský	k2eAgInSc7d1	štrasburský
biskupem	biskup	k1gInSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Krnovska	Krnovsko	k1gNnSc2	Krnovsko
<g/>
,	,	kIx,	,
Bytomska	Bytomsko	k1gNnSc2	Bytomsko
a	a	k8xC	a
Bohumínska	Bohumínsko	k1gNnSc2	Bohumínsko
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
však	však	k9	však
císař	císař	k1gMnSc1	císař
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Majetko-právní	Majetkorávní	k2eAgInSc1d1	Majetko-právní
spor	spor	k1gInSc1	spor
trval	trvat	k5eAaImAgInS	trvat
až	až	k9	až
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
vůdce	vůdce	k1gMnSc1	vůdce
povstalců	povstalec	k1gMnPc2	povstalec
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
zbaven	zbavit	k5eAaPmNgMnS	zbavit
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
Bohumínsko	Bohumínsko	k1gNnSc1	Bohumínsko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
bankéři	bankéř	k1gMnSc3	bankéř
a	a	k8xC	a
důlnímu	důlní	k2eAgMnSc3d1	důlní
podnikateli	podnikatel	k1gMnSc3	podnikatel
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
Lazarovi	Lazar	k1gMnSc3	Lazar
Henckelovi	Henckel	k1gMnSc3	Henckel
von	von	k1gInSc4	von
Donnersmarck	Donnersmarcka	k1gFnPc2	Donnersmarcka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
majetek	majetek	k1gInSc1	majetek
přebral	přebrat	k5eAaPmAgInS	přebrat
již	již	k6eAd1	již
dědičně	dědičně	k6eAd1	dědičně
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
stal	stát	k5eAaPmAgInS	stát
svobodným	svobodný	k2eAgMnSc7d1	svobodný
říšským	říšský	k2eAgMnSc7d1	říšský
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
mezi	mezi	k7c4	mezi
český	český	k2eAgInSc4d1	český
hraběcí	hraběcí	k2eAgInSc4d1	hraběcí
stav	stav	k1gInSc4	stav
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Bohumínsko	Bohumínsko	k1gNnSc1	Bohumínsko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Eliášovi	Eliáš	k1gMnSc3	Eliáš
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Eliáš	Eliáš	k1gMnSc1	Eliáš
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1691	[number]	k4	1691
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Oderbergu	Oderberg	k1gInSc2	Oderberg
<g/>
.	.	kIx.	.
</s>
<s>
Umírá	umírat	k5eAaImIp3nS	umírat
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Arnošt	Arnošt	k1gMnSc1	Arnošt
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
a	a	k8xC	a
dědictví	dědictví	k1gNnSc2	dědictví
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Erdman	Erdman	k1gMnSc1	Erdman
Jindřich	Jindřich	k1gMnSc1	Jindřich
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
syn	syn	k1gMnSc1	syn
prodal	prodat	k5eAaPmAgMnS	prodat
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
část	část	k1gFnSc4	část
bohumínského	bohumínský	k2eAgNnSc2d1	Bohumínské
panství	panství	k1gNnSc2	panství
Karlu	Karel	k1gMnSc3	Karel
knížeti	kníže	k1gMnSc3	kníže
Lichnovskému	Lichnovský	k2eAgMnSc3d1	Lichnovský
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
umřel	umřít	k5eAaPmAgInS	umřít
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
i	i	k9	i
oderberská	oderberský	k2eAgFnSc1d1	oderberský
mužská	mužský	k2eAgFnSc1d1	mužská
linie	linie	k1gFnSc1	linie
rodu	rod	k1gInSc2	rod
Henckel-Donnersmarck	Henckel-Donnersmarcka	k1gFnPc2	Henckel-Donnersmarcka
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
držitelství	držitelství	k1gNnSc1	držitelství
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
Bohumínské	bohumínský	k2eAgNnSc1d1	Bohumínské
panství	panství	k1gNnSc1	panství
povýšeno	povýšen	k2eAgNnSc1d1	povýšeno
císařem	císař	k1gMnSc7	císař
Leopoldem	Leopold	k1gMnSc7	Leopold
na	na	k7c4	na
status	status	k1gInSc4	status
minor	minor	k2eAgInSc4d1	minor
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
nižší	nízký	k2eAgNnSc4d2	nižší
stavovské	stavovský	k2eAgNnSc4d1	Stavovské
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
bylo	být	k5eAaImAgNnS	být
Bohumínsko	Bohumínsko	k1gNnSc1	Bohumínsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
berlínského	berlínský	k2eAgInSc2d1	berlínský
míru	mír	k1gInSc2	mír
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c6	na
rakouské	rakouský	k2eAgFnSc6d1	rakouská
a	a	k8xC	a
pruské	pruský	k2eAgFnSc6d1	pruská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
rodu	rod	k1gInSc2	rod
Gusnarů	Gusnar	k1gMnPc2	Gusnar
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
panství	panství	k1gNnSc4	panství
posléze	posléze	k6eAd1	posléze
prodali	prodat	k5eAaPmAgMnP	prodat
hraběti	hrabě	k1gMnSc3	hrabě
Rudnickému	rudnický	k2eAgMnSc3d1	rudnický
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
manželka	manželka	k1gFnSc1	manželka
dále	daleko	k6eAd2	daleko
odprodala	odprodat	k5eAaPmAgFnS	odprodat
část	část	k1gFnSc4	část
panství	panství	k1gNnSc3	panství
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
koupil	koupit	k5eAaPmAgMnS	koupit
panství	panství	k1gNnSc4	panství
hrabě	hrabě	k1gMnSc1	hrabě
Larisch-Mönnich	Larisch-Mönnich	k1gMnSc1	Larisch-Mönnich
a	a	k8xC	a
tento	tento	k3xDgMnSc1	tento
rod	rod	k1gInSc4	rod
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jeho	jeho	k3xOp3gMnSc7	jeho
držitelem	držitel	k1gMnSc7	držitel
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnPc1d1	okolní
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Šunychl	Šunychl	k1gFnSc1	Šunychl
a	a	k8xC	a
Pudlov	Pudlov	k1gInSc1	Pudlov
<g/>
,	,	kIx,	,
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
díky	díky	k7c3	díky
stavbě	stavba	k1gFnSc3	stavba
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
železáren	železárna	k1gFnPc2	železárna
obrovský	obrovský	k2eAgInSc4d1	obrovský
boom	boom	k1gInSc4	boom
a	a	k8xC	a
význam	význam	k1gInSc4	význam
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
Starého	Starého	k2eAgInSc2d1	Starého
Bohumína	Bohumín	k1gInSc2	Bohumín
začal	začít	k5eAaPmAgInS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pudlov	Pudlov	k1gInSc1	Pudlov
a	a	k8xC	a
Šunychl	Šunychl	k1gFnSc1	Šunychl
byly	být	k5eAaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
jako	jako	k8xS	jako
poddanské	poddanský	k2eAgFnSc2d1	poddanská
vsi	ves	k1gFnSc2	ves
při	při	k7c6	při
Bohumínském	bohumínský	k2eAgNnSc6d1	Bohumínské
panství	panství	k1gNnSc6	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
Pudlov	Pudlov	k1gInSc1	Pudlov
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Vrbicí	vrbice	k1gFnSc7	vrbice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
postavení	postavení	k1gNnSc4	postavení
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Vrbice	vrbice	k1gFnSc1	vrbice
se	se	k3xPyFc4	se
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
stala	stát	k5eAaPmAgFnS	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
Pudlova	pudlův	k2eAgInSc2d1	pudlův
s	s	k7c7	s
Bohumínem	Bohumín	k1gInSc7	Bohumín
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
Šunychl	Šunychl	k1gFnSc2	Šunychl
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
názvem	název	k1gInSc7	název
Nový	nový	k2eAgInSc1d1	nový
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
obec	obec	k1gFnSc1	obec
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bohumín	Bohumín	k1gInSc1	Bohumín
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
obce	obec	k1gFnPc4	obec
<g/>
:	:	kIx,	:
Bohumín	Bohumín	k1gInSc1	Bohumín
a	a	k8xC	a
Pudlov	Pudlov	k1gInSc1	Pudlov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
Bohumín	Bohumín	k1gInSc1	Bohumín
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
Bohumínem	Bohumín	k1gInSc7	Bohumín
<g/>
,	,	kIx,	,
Pudlovem	Pudlov	k1gInSc7	Pudlov
<g/>
,	,	kIx,	,
Skřečoní	Skřečoň	k1gFnSc7	Skřečoň
<g/>
,	,	kIx,	,
Vrbicí	vrbice	k1gFnSc7	vrbice
a	a	k8xC	a
Záblatím	Záblatí	k1gNnSc7	Záblatí
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
obec	obec	k1gFnSc4	obec
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
Vrbice	vrbice	k1gFnSc1	vrbice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
Bohumín	Bohumín	k1gInSc1	Bohumín
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Bohumín	Bohumín	k1gInSc1	Bohumín
I	i	k8xC	i
–	–	k?	–
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Starý	starý	k2eAgInSc1d1	starý
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohumín	Bohumín	k1gInSc1	Bohumín
II	II	kA	II
–	–	k?	–
Nový	nový	k2eAgInSc4d1	nový
Bohumín	Bohumín	k1gInSc4	Bohumín
<g/>
,	,	kIx,	,
Bohumín	Bohumín	k1gInSc4	Bohumín
III	III	kA	III
–	–	k?	–
Pudlov	Pudlov	k1gInSc1	Pudlov
<g/>
,	,	kIx,	,
Bohumín	Bohumín	k1gInSc1	Bohumín
IV	IV	kA	IV
–	–	k?	–
Skřečoň	Skřečoň	k1gFnSc1	Skřečoň
a	a	k8xC	a
Bohumín	Bohumín	k1gInSc1	Bohumín
V	v	k7c6	v
–	–	k?	–
Záblatí	Záblatí	k1gNnSc6	Záblatí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
přijalo	přijmout	k5eAaPmAgNnS	přijmout
město	město	k1gNnSc1	město
Bohumín	Bohumín	k1gInSc1	Bohumín
název	název	k1gInSc1	název
Starý	starý	k2eAgInSc1d1	starý
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
opět	opět	k6eAd1	opět
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
jako	jako	k9	jako
správní	správní	k2eAgFnPc1d1	správní
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
Bohumínem	Bohumín	k1gInSc7	Bohumín
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
znovu	znovu	k6eAd1	znovu
název	název	k1gInSc1	název
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
až	až	k9	až
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
Bohumín	Bohumín	k1gInSc4	Bohumín
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1855	[number]	k4	1855
až	až	k9	až
1868	[number]	k4	1868
i	i	k9	i
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
přináležel	přináležet	k5eAaImAgMnS	přináležet
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
okresu	okres	k1gInSc2	okres
Fryštát	Fryštát	k1gInSc1	Fryštát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
do	do	k7c2	do
správního	správní	k2eAgInSc2d1	správní
okresu	okres	k1gInSc2	okres
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Ostrava-venkov	Ostravaenkov	k1gInSc1	Ostrava-venkov
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
je	být	k5eAaImIp3nS	být
Bohumín	Bohumín	k1gInSc1	Bohumín
součástí	součást	k1gFnPc2	součást
okresu	okres	k1gInSc2	okres
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Teschen	Teschen	k1gInSc1	Teschen
(	(	kIx(	(
<g/>
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1941	[number]	k4	1939-1941
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Řísské	Řísský	k2eAgFnSc2d1	Řísský
provincie	provincie	k1gFnSc2	provincie
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1941-1945	[number]	k4	1941-1945
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Říšské	říšský	k2eAgFnSc2d1	říšská
župy	župa	k1gFnSc2	župa
Horní	horní	k2eAgNnSc4d1	horní
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Bohumín	Bohumín	k1gInSc1	Bohumín
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
časech	čas	k1gInPc6	čas
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přímou	přímý	k2eAgFnSc7d1	přímá
součástí	součást	k1gFnSc7	součást
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Architektonický	architektonický	k2eAgInSc1d1	architektonický
komplex	komplex	k1gInSc1	komplex
současné	současný	k2eAgFnSc2d1	současná
budovy	budova	k1gFnSc2	budova
radnice	radnice	k1gFnSc2	radnice
</s>
</p>
<p>
<s>
Katolický	katolický	k2eAgInSc1d1	katolický
kostel	kostel	k1gInSc1	kostel
Božského	božský	k2eAgNnSc2d1	božské
srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgNnSc2d1	páně
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
fary	fara	k1gFnSc2	fara
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sedmibolestné	sedmibolestný	k2eAgFnSc2d1	Sedmibolestná
ve	v	k7c6	v
Skřečoni	Skřečoň	k1gFnSc6	Skřečoň
</s>
</p>
<p>
<s>
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
filiální	filiální	k2eAgFnSc1d1	filiální
kaple	kaple	k1gFnSc1	kaple
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
</s>
</p>
<p>
<s>
Evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
Slezské	slezský	k2eAgFnSc2d1	Slezská
evangelické	evangelický	k2eAgFnSc2d1	evangelická
církve	církev	k1gFnSc2	církev
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
vyznání	vyznání	k1gNnSc2	vyznání
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Jména	jméno	k1gNnSc2	jméno
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Šunychlu	Šunychl	k1gInSc6	Šunychl
</s>
</p>
<p>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
č.	č.	k?	č.
p.	p.	k?	p.
37	[number]	k4	37
</s>
</p>
<p>
<s>
Výpravní	výpravní	k2eAgFnSc1d1	výpravní
budova	budova	k1gFnSc1	budova
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Hraniční	hraniční	k2eAgInPc1d1	hraniční
meandry	meandr	k1gInPc1	meandr
Odry	Odra	k1gFnSc2	Odra
</s>
</p>
<p>
<s>
Pěchotní	pěchotní	k2eAgInSc1d1	pěchotní
srub	srub	k1gInSc1	srub
MO-S5	MO-S5	k1gFnSc2	MO-S5
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Trati	trať	k1gFnSc6	trať
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Turistické	turistický	k2eAgFnSc2d1	turistická
zajímavosti	zajímavost	k1gFnSc2	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Aquacentrum	Aquacentrum	k1gNnSc1	Aquacentrum
</s>
</p>
<p>
<s>
Park	park	k1gInSc1	park
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
</s>
</p>
<p>
<s>
Mauglího	Mauglí	k2eAgInSc2d1	Mauglí
stezka	stezka	k1gFnSc1	stezka
v	v	k7c6	v
parku	park	k1gInSc6	park
P.	P.	kA	P.
Bezruče	Bezruč	k1gInPc4	Bezruč
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
jako	jako	k9	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Slezsku	Slezsko	k1gNnSc6	Slezsko
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
složité	složitý	k2eAgFnSc3d1	složitá
historii	historie	k1gFnSc3	historie
etnicky	etnicky	k6eAd1	etnicky
velmi	velmi	k6eAd1	velmi
promíchané	promíchaný	k2eAgNnSc1d1	promíchané
<g/>
.	.	kIx.	.
</s>
<s>
Bohumín	Bohumín	k1gInSc1	Bohumín
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
znám	znám	k2eAgInSc1d1	znám
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
řeckou	řecký	k2eAgFnSc4d1	řecká
menšinu	menšina	k1gFnSc4	menšina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
přišla	přijít	k5eAaPmAgFnS	přijít
během	během	k7c2	během
řecké	řecký	k2eAgFnSc2d1	řecká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
početní	početní	k2eAgMnPc1d1	početní
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
národnostně	národnostně	k6eAd1	národnostně
nevyhraněné	vyhraněný	k2eNgFnPc1d1	nevyhraněná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
začali	začít	k5eAaPmAgMnP	začít
hlásit	hlásit	k5eAaImF	hlásit
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
<g/>
,	,	kIx,	,
polské	polský	k2eAgFnSc3d1	polská
či	či	k8xC	či
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
zde	zde	k6eAd1	zde
získali	získat	k5eAaPmAgMnP	získat
početní	početní	k2eAgFnSc4d1	početní
převahu	převaha	k1gFnSc4	převaha
až	až	k9	až
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
u	u	k7c2	u
nemalé	malý	k2eNgFnSc2d1	nemalá
části	část	k1gFnSc2	část
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
fenomén	fenomén	k1gMnSc1	fenomén
tzv.	tzv.	kA	tzv.
šlonzáctví	šlonzáctví	k1gNnSc1	šlonzáctví
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
Šlonzáky	šlonzák	k1gInPc1	šlonzák
<g/>
,	,	kIx,	,
nehlásili	hlásit	k5eNaImAgMnP	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
většinové	většinový	k2eAgFnSc3d1	většinová
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
cítili	cítit	k5eAaImAgMnP	cítit
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
jako	jako	k9	jako
Slezané	Slezan	k1gMnPc1	Slezan
<g/>
,	,	kIx,	,
nepoužívali	používat	k5eNaImAgMnP	používat
žádnou	žádný	k3yNgFnSc4	žádný
formu	forma	k1gFnSc4	forma
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
mluvili	mluvit	k5eAaImAgMnP	mluvit
česko-polsko-německým	českoolskoěmecký	k2eAgNnSc7d1	česko-polsko-německé
nářečím	nářečí	k1gNnSc7	nářečí
"	"	kIx"	"
<g/>
po	po	k7c6	po
naszymu	naszym	k1gInSc6	naszym
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Šlonzáctví	Šlonzáctví	k1gNnSc1	Šlonzáctví
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
i	i	k9	i
toto	tento	k3xDgNnSc4	tento
specifické	specifický	k2eAgNnSc4d1	specifické
nářečí	nářečí	k1gNnSc4	nářečí
<g/>
,	,	kIx,	,
však	však	k9	však
na	na	k7c6	na
území	území	k1gNnSc6	území
Bohumínska	Bohumínsko	k1gNnSc2	Bohumínsko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
mluví	mluvit	k5eAaImIp3nS	mluvit
ostravským	ostravský	k2eAgInSc7d1	ostravský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
holocaustu	holocaust	k1gInSc2	holocaust
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
také	také	k9	také
asi	asi	k9	asi
šest	šest	k4xCc1	šest
set	sto	k4xCgNnPc2	sto
osob	osoba	k1gFnPc2	osoba
židovského	židovský	k2eAgNnSc2d1	Židovské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
zde	zde	k6eAd1	zde
i	i	k9	i
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodáci	rodák	k1gMnPc5	rodák
===	===	k?	===
</s>
</p>
<p>
<s>
Dawid	Dawid	k1gInSc1	Dawid
Emannuel	Emannuel	k1gInSc1	Emannuel
Bachrach	Bachrach	k1gInSc1	Bachrach
-	-	kIx~	-
Bareé	Bareé	k1gNnSc1	Bareé
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
P.	P.	kA	P.
PhDr.	PhDr.	kA	PhDr.
ThDr.	ThDr.	k1gMnSc1	ThDr.
Joseph	Joseph	k1gMnSc1	Joseph
Blokscha	Blokscha	k1gMnSc1	Blokscha
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Dörner	Dörner	k1gMnSc1	Dörner
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
farnosti	farnost	k1gFnSc2	farnost
na	na	k7c6	na
území	území	k1gNnSc6	území
augsburské	augsburský	k2eAgFnSc2d1	augsburská
diecéze	diecéze	k1gFnSc2	diecéze
</s>
</p>
<p>
<s>
Otokar	Otokar	k1gMnSc1	Otokar
Weisl	Weisl	k1gInSc1	Weisl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Hochfelder	Hochfelder	k1gMnSc1	Hochfelder
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Čečetka	čečetka	k1gFnSc1	čečetka
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Švajgr	Švajgr	k1gMnSc1	Švajgr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžec	běžec	k1gMnSc1	běžec
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
tratě	trata	k1gFnSc6	trata
</s>
</p>
<p>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Havelka	Havelka	k1gMnSc1	Havelka
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
ThDr.	ThDr.	k?	ThDr.
Milan	Milan	k1gMnSc1	Milan
Salajka	salajka	k1gFnSc1	salajka
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Husitské	husitský	k2eAgFnSc2d1	husitská
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Bednarčík	Bednarčík	k1gMnSc1	Bednarčík
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Šimečka	Šimeček	k1gMnSc2	Šimeček
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
kolegia	kolegium	k1gNnSc2	kolegium
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pospíchal	Pospíchal	k1gMnSc1	Pospíchal
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
medaile	medaile	k1gFnSc1	medaile
z	z	k7c2	z
MS	MS	kA	MS
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
(	(	kIx(	(
<g/>
Baník	baník	k1gMnSc1	baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Bohemians	Bohemians	k1gInSc1	Bohemians
–	–	k?	–
titul	titul	k1gInSc1	titul
Mistr	mistr	k1gMnSc1	mistr
ČSSR	ČSSR	kA	ČSSR
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MUDr.	MUDr.	kA	MUDr.
Siegfried	Siegfried	k1gMnSc1	Siegfried
Trotnow	Trotnow	k1gMnSc1	Trotnow
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gynekolog	gynekolog	k1gMnSc1	gynekolog
(	(	kIx(	(
<g/>
specializace	specializace	k1gFnSc1	specializace
na	na	k7c4	na
reprodukční	reprodukční	k2eAgFnSc4d1	reprodukční
medicínu	medicína	k1gFnSc4	medicína
<g/>
)	)	kIx)	)
a	a	k8xC	a
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
metody	metoda	k1gFnSc2	metoda
oplodnění	oplodnění	k1gNnSc2	oplodnění
in	in	k?	in
vitro	vitro	k1gNnSc1	vitro
(	(	kIx(	(
<g/>
IVF	IVF	kA	IVF
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Konkolski	Konkolsk	k1gFnSc2	Konkolsk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
osobnost	osobnost	k1gFnSc1	osobnost
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
českého	český	k2eAgInSc2d1	český
námořního	námořní	k2eAgInSc2d1	námořní
jachtingu	jachting	k1gInSc2	jachting
<g/>
,	,	kIx,	,
závodník	závodník	k1gMnSc1	závodník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
světovou	světový	k2eAgFnSc4d1	světová
pověst	pověst	k1gFnSc4	pověst
především	především	k9	především
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
osamělých	osamělý	k2eAgMnPc2d1	osamělý
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
</s>
</p>
<p>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Milkov	Milkov	k1gInSc1	Milkov
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
designér	designér	k1gMnSc1	designér
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
</s>
</p>
<p>
<s>
Dušan	Dušan	k1gMnSc1	Dušan
Sittek	Sittek	k1gMnSc1	Sittek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Alena	Alena	k1gFnSc1	Alena
Mrázová	Mrázová	k1gFnSc1	Mrázová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
a	a	k8xC	a
vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Srniček	srnička	k1gFnPc2	srnička
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
(	(	kIx(	(
<g/>
Baník	baník	k1gMnSc1	baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Newcastle	Newcastle	k1gFnSc1	Newcastle
United	United	k1gInSc1	United
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
FC	FC	kA	FC
<g/>
,	,	kIx,	,
West	West	k2eAgMnSc1d1	West
Ham	ham	k0	ham
United	United	k1gInSc4	United
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Newcastle	Newcastle	k1gFnSc1	Newcastle
United	United	k1gMnSc1	United
FC	FC	kA	FC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Bebarová	Bebarová	k1gFnSc1	Bebarová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc1d1	divadelní
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Kněžíková	Kněžíková	k1gFnSc1	Kněžíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sopranistka	sopranistka	k1gFnSc1	sopranistka
</s>
</p>
<p>
<s>
Patrik	Patrik	k1gMnSc1	Patrik
Děrgel	Děrgel	k1gMnSc1	Děrgel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Železnice	železnice	k1gFnSc1	železnice
===	===	k?	===
</s>
</p>
<p>
<s>
Bohumín	Bohumín	k1gInSc1	Bohumín
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
historicky	historicky	k6eAd1	historicky
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
nádraží	nádraží	k1gNnPc2	nádraží
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
železničních	železniční	k2eAgInPc2d1	železniční
uzlů	uzel	k1gInPc2	uzel
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
odbočkou	odbočka	k1gFnSc7	odbočka
do	do	k7c2	do
Pruska	Prusko	k1gNnSc2	Prusko
z	z	k7c2	z
rakouské	rakouský	k2eAgFnSc2d1	rakouská
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
Krakov	Krakov	k1gInSc1	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1872	[number]	k4	1872
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
Košicko-bohumínská	košickoohumínský	k2eAgFnSc1d1	košicko-bohumínská
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Bohumín	Bohumín	k1gInSc1	Bohumín
–	–	k?	–
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
<g/>
)	)	kIx)	)
Těšín	Těšín	k1gInSc1	Těšín
–	–	k?	–
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
Poprad	Poprad	k1gInSc1	Poprad
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
a	a	k8xC	a
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
hlavní	hlavní	k2eAgFnSc4d1	hlavní
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
provozní	provozní	k2eAgFnSc1d1	provozní
jednotka	jednotka	k1gFnSc1	jednotka
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
depa	depo	k1gNnSc2	depo
a	a	k8xC	a
např.	např.	kA	např.
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
přepřah	přepřah	k1gInSc1	přepřah
přímých	přímý	k2eAgMnPc2d1	přímý
vozů	vůz	k1gInPc2	vůz
na	na	k7c6	na
dálkových	dálkový	k2eAgFnPc6d1	dálková
tratích	trať	k1gFnPc6	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
Břeclav	Břeclav	k1gFnSc1	Břeclav
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Provozně	provozně	k6eAd1	provozně
zde	zde	k6eAd1	zde
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navazují	navazovat	k5eAaImIp3nP	navazovat
trať	trať	k1gFnSc1	trať
SŽDC	SŽDC	kA	SŽDC
č.	č.	k?	č.
270	[number]	k4	270
a	a	k8xC	a
č.	č.	k?	č.
320	[number]	k4	320
<g/>
,	,	kIx,	,
rozvětvuje	rozvětvovat	k5eAaImIp3nS	rozvětvovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc1	třetí
železniční	železniční	k2eAgInSc1d1	železniční
koridor	koridor	k1gInSc1	koridor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silnice	silnice	k1gFnPc1	silnice
===	===	k?	===
</s>
</p>
<p>
<s>
Územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
prochází	procházet	k5eAaImIp3nS	procházet
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
exity	exit	k1gInPc4	exit
<g/>
:	:	kIx,	:
365	[number]	k4	365
–	–	k?	–
Vrbice	vrbice	k1gFnSc2	vrbice
a	a	k8xC	a
370	[number]	k4	370
–	–	k?	–
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
z	z	k7c2	z
Karviné	Karviná	k1gFnSc2	Karviná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přes	přes	k7c4	přes
Skřečoň	Skřečoň	k1gFnSc4	Skřečoň
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc4d1	nový
Bohumín	Bohumín	k1gInSc4	Bohumín
<g/>
,	,	kIx,	,
exit	exit	k1gInSc4	exit
370	[number]	k4	370
a	a	k8xC	a
Starý	starý	k2eAgInSc1d1	starý
Bohumín	Bohumín	k1gInSc1	Bohumín
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
bývalé	bývalý	k2eAgInPc1d1	bývalý
úseky	úsek	k1gInPc1	úsek
této	tento	k3xDgFnSc2	tento
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
dočasně	dočasně	k6eAd1	dočasně
označené	označený	k2eAgNnSc1d1	označené
jako	jako	k8xS	jako
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
H	H	kA	H
a	a	k8xC	a
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
I.	I.	kA	I.
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
647	[number]	k4	647
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Hrušov	Hrušov	k1gInSc1	Hrušov
–	–	k?	–
Vrbice	vrbice	k1gFnSc2	vrbice
–	–	k?	–
Pudlov	Pudlov	k1gInSc1	Pudlov
–	–	k?	–
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
471	[number]	k4	471
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Rychvald	Rychvalda	k1gFnPc2	Rychvalda
–	–	k?	–
Záblatí	Záblatí	k1gNnSc6	Záblatí
–	–	k?	–
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
úsek	úsek	k1gInSc4	úsek
této	tento	k3xDgFnSc2	tento
silnice	silnice	k1gFnSc2	silnice
prochází	procházet	k5eAaImIp3nS	procházet
Starým	starý	k2eAgInSc7d1	starý
Bohumínem	Bohumín	k1gInSc7	Bohumín
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46812	[number]	k4	46812
Skřečoň	Skřečoň	k1gFnSc2	Skřečoň
–	–	k?	–
Nerad	nerad	k2eAgMnSc1d1	nerad
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46813	[number]	k4	46813
Skřečoň	Skřečoň	k1gFnSc7	Skřečoň
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46814	[number]	k4	46814
(	(	kIx(	(
<g/>
Šunychelská	Šunychelský	k2eAgFnSc1d1	Šunychelská
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
–	–	k?	–
Šunychl	Šunychl	k1gMnSc1	Šunychl
–	–	k?	–
Kopytov	Kopytov	k1gInSc1	Kopytov
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46815	[number]	k4	46815
Šunychl	Šunychl	k1gFnSc2	Šunychl
–	–	k?	–
Starý	starý	k2eAgInSc1d1	starý
Bohumín	Bohumín	k1gInSc1	Bohumín
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46816	[number]	k4	46816
Pudlov	Pudlov	k1gInSc1	Pudlov
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
Bohumín	Bohumín	k1gInSc1	Bohumín
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46817	[number]	k4	46817
(	(	kIx(	(
<g/>
Lidická	lidický	k2eAgFnSc1d1	Lidická
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
46818	[number]	k4	46818
mezi	mezi	k7c7	mezi
Novým	nový	k2eAgInSc7d1	nový
Bohumínem	Bohumín	k1gInSc7	Bohumín
a	a	k8xC	a
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
471	[number]	k4	471
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4711	[number]	k4	4711
Nový	nový	k2eAgInSc1d1	nový
Bohumín	Bohumín	k1gInSc1	Bohumín
–	–	k?	–
Záblatí	Záblatí	k1gNnSc1	Záblatí
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4712	[number]	k4	4712
Záblatí	Záblatí	k1gNnSc6	Záblatí
–	–	k?	–
Dolní	dolní	k2eAgFnSc1d1	dolní
Lutyně	Lutyně	k1gFnSc1	Lutyně
</s>
</p>
<p>
<s>
===	===	k?	===
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1902	[number]	k4	1902
až	až	k6eAd1	až
1973	[number]	k4	1973
provozována	provozován	k2eAgFnSc1d1	provozována
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byla	být	k5eAaImAgFnS	být
provozována	provozován	k2eAgFnSc1d1	provozována
jako	jako	k8xS	jako
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
byla	být	k5eAaImAgFnS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
na	na	k7c6	na
úzkém	úzký	k2eAgInSc6d1	úzký
rozchodu	rozchod	k1gInSc6	rozchod
760	[number]	k4	760
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
7	[number]	k4	7
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Bohumín	Bohumín	k1gInSc1	Bohumín
(	(	kIx(	(
<g/>
Oderberg-Bahnhof	Oderberg-Bahnhof	k1gInSc1	Oderberg-Bahnhof
<g/>
)	)	kIx)	)
se	s	k7c7	s
14	[number]	k4	14
068	[number]	k4	068
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Starý	starý	k2eAgInSc1d1	starý
Bohumín	Bohumín	k1gInSc1	Bohumín
(	(	kIx(	(
<g/>
Oderberg-Stadt	Oderberg-Stadt	k1gInSc1	Oderberg-Stadt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
také	také	k9	také
"	"	kIx"	"
<g/>
Bogun	Bogun	k1gMnSc1	Bogun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
1	[number]	k4	1
541	[number]	k4	541
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Skřečoň	Skřečonit	k5eAaPmRp2nS	Skřečonit
(	(	kIx(	(
<g/>
Skretschon	Skretschon	k1gInSc1	Skretschon
<g/>
)	)	kIx)	)
s	s	k7c7	s
2	[number]	k4	2
533	[number]	k4	533
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Záblatí	Záblatí	k1gNnSc1	Záblatí
(	(	kIx(	(
<g/>
Sablath	Sablath	k1gInSc1	Sablath
<g/>
)	)	kIx)	)
s	s	k7c7	s
2	[number]	k4	2
198	[number]	k4	198
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pudlov	Pudlov	k1gInSc1	Pudlov
(	(	kIx(	(
<g/>
Pudlau	Pudlaus	k1gInSc2	Pudlaus
<g/>
)	)	kIx)	)
s	s	k7c7	s
1	[number]	k4	1
465	[number]	k4	465
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Šunychl	Šunychnout	k5eAaPmAgInS	Šunychnout
(	(	kIx(	(
<g/>
Schönichel	Schönichel	k1gInSc1	Schönichel
<g/>
)	)	kIx)	)
s	s	k7c7	s
551	[number]	k4	551
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Vrbice	vrbice	k1gFnSc1	vrbice
(	(	kIx(	(
<g/>
Wirbitz	Wirbitz	k1gInSc1	Wirbitz
<g/>
)	)	kIx)	)
se	s	k7c7	s
471	[number]	k4	471
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starostové	Starostové	k2eAgNnSc2d1	Starostové
města	město	k1gNnSc2	město
Bohumína	Bohumín	k1gInSc2	Bohumín
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgInSc2d1	dnešní
St.	st.	kA	st.
Bohumína	Bohumín	k1gInSc2	Bohumín
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
neúplný	úplný	k2eNgInSc1d1	neúplný
seznam	seznam	k1gInSc1	seznam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Kowarz	Kowarz	k1gMnSc1	Kowarz
(	(	kIx(	(
<g/>
1491	[number]	k4	1491
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
Wenzel	Wenzet	k5eAaPmAgMnS	Wenzet
Warzeszka	Warzeszka	k1gFnSc1	Warzeszka
<g/>
;	;	kIx,	;
Adam	Adam	k1gMnSc1	Adam
Hlawiczka	Hlawiczka	k1gFnSc1	Hlawiczka
<g/>
;	;	kIx,	;
Tobias	Tobias	k1gMnSc1	Tobias
Eisener	Eisener	k1gMnSc1	Eisener
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
;	;	kIx,	;
Simon	Simon	k1gMnSc1	Simon
Gromatka	Gromatka	k1gFnSc1	Gromatka
(	(	kIx(	(
<g/>
1689	[number]	k4	1689
<g/>
–	–	k?	–
<g/>
1717	[number]	k4	1717
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Anton	Anton	k1gMnSc1	Anton
Padwowski	Padwowske	k1gFnSc4	Padwowske
(	(	kIx(	(
<g/>
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
1739	[number]	k4	1739
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elias	Elias	k1gMnSc1	Elias
Czimmerer	Czimmerer	k1gMnSc1	Czimmerer
(	(	kIx(	(
<g/>
1739	[number]	k4	1739
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Jacob	Jacoba	k1gFnPc2	Jacoba
Haagen	Haagen	k1gInSc1	Haagen
<g/>
;	;	kIx,	;
Mathias	Mathias	k1gInSc1	Mathias
Ignatz	Ignatza	k1gFnPc2	Ignatza
Piechaczek	Piechaczka	k1gFnPc2	Piechaczka
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1759	[number]	k4	1759
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Franz	Franz	k1gMnSc1	Franz
Czimmerer	Czimmerer	k1gMnSc1	Czimmerer
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Jacob	Jacoba	k1gFnPc2	Jacoba
Haagen	Haagen	k1gInSc1	Haagen
<g/>
;	;	kIx,	;
Georg	Georg	k1gInSc1	Georg
Schopna	schopen	k2eAgFnSc1d1	schopna
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Matz	Matz	k1gMnSc1	Matz
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kašpar	Kašpar	k1gMnSc1	Kašpar
Kremsser	Kremsser	k1gMnSc1	Kremsser
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
–	–	k?	–
<g/>
1779	[number]	k4	1779
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Jakub	Jakub	k1gMnSc1	Jakub
Rokitta	Rokitta	k1gMnSc1	Rokitta
(	(	kIx(	(
<g/>
1779	[number]	k4	1779
<g/>
–	–	k?	–
<g/>
1786	[number]	k4	1786
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Kullig	Kullig	k1gMnSc1	Kullig
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
–	–	k?	–
<g/>
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Oppawsky	Oppawska	k1gFnSc2	Oppawska
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Sedlaczek	Sedlaczka	k1gFnPc2	Sedlaczka
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Oppawsky	Oppawska	k1gFnSc2	Oppawska
<g/>
;	;	kIx,	;
Joseph	Joseph	k1gMnSc1	Joseph
Furch	Furch	k1gMnSc1	Furch
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Stiller	Stiller	k1gMnSc1	Stiller
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Warosch	Warosch	k1gMnSc1	Warosch
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Georg	Georg	k1gInSc1	Georg
Weczerek	Weczerka	k1gFnPc2	Weczerka
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Stiller	Stiller	k1gMnSc1	Stiller
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Przybill	Przybill	k1gMnSc1	Przybill
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Stiller	Stiller	k1gMnSc1	Stiller
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Klimscha	Klimscha	k1gMnSc1	Klimscha
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JUDr.	JUDr.	kA	JUDr.
Johann	Johann	k1gMnSc1	Johann
Santarius	Santarius	k1gMnSc1	Santarius
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Klimscha	Klimscha	k1gMnSc1	Klimscha
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JUDr.	JUDr.	kA	JUDr.
Carl	Carl	k1gMnSc1	Carl
Franz	Franz	k1gMnSc1	Franz
Johann	Johann	k1gMnSc1	Johann
Ott	Ott	k1gMnSc1	Ott
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Náhlovský	Náhlovský	k2eAgMnSc1d1	Náhlovský
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pověřený	pověřený	k2eAgMnSc1d1	pověřený
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hodný	hodný	k2eAgMnSc1d1	hodný
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pověřený	pověřený	k2eAgMnSc1d1	pověřený
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
JUDr.	JUDr.	kA	JUDr.
Carl	Carl	k1gMnSc1	Carl
Franz	Franz	k1gMnSc1	Franz
Johann	Johann	k1gMnSc1	Johann
Ott	Ott	k1gMnSc1	Ott
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Julius	Julius	k1gMnSc1	Julius
Warosch	Warosch	k1gMnSc1	Warosch
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JUDr.	JUDr.	kA	JUDr.
Karel	Karel	k1gMnSc1	Karel
Říha	Říha	k1gMnSc1	Říha
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Starostové	Starostové	k2eAgNnSc2d1	Starostové
města	město	k1gNnSc2	město
Šunychlu	Šunychl	k1gInSc2	Šunychl
-	-	kIx~	-
Bohumína	Bohumín	k1gInSc2	Bohumín
-	-	kIx~	-
nádraží	nádraží	k1gNnSc2	nádraží
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgInSc2d1	dnešní
N.	N.	kA	N.
Bohumína	Bohumín	k1gInSc2	Bohumín
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
neúplný	úplný	k2eNgInSc1d1	neúplný
seznam	seznam	k1gInSc1	seznam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Krakówka	Krakówka	k1gMnSc1	Krakówka
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Solich	Solich	k1gMnSc1	Solich
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Johann	Johann	k1gMnSc1	Johann
Krakówka	Krakówka	k1gMnSc1	Krakówka
ml.	ml.	kA	ml.
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Lukscha	Lukscha	k1gMnSc1	Lukscha
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Matthias	Matthias	k1gInSc1	Matthias
Sud	sud	k1gInSc1	sud
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Lukscha	Lukscha	k1gMnSc1	Lukscha
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
Joseph	Josepha	k1gFnPc2	Josepha
von	von	k1gInSc1	von
Portenschlang-Ledermayer	Portenschlang-Ledermayer	k1gMnSc1	Portenschlang-Ledermayer
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Lustig	Lustig	k1gMnSc1	Lustig
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Dworzak	Dworzak	k1gMnSc1	Dworzak
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Hein	Hein	k1gMnSc1	Hein
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Jedrníček	Jedrníček	k1gMnSc1	Jedrníček
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pověřený	pověřený	k2eAgMnSc1d1	pověřený
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Malcher	Malchra	k1gFnPc2	Malchra
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Józef	Józef	k1gInSc1	Józef
Wilczek	Wilczek	k1gInSc1	Wilczek
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pověřený	pověřený	k2eAgMnSc1d1	pověřený
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Freisler	Freisler	k1gMnSc1	Freisler
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Kolar	Kolar	k1gMnSc1	Kolar
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Metzig	Metzig	k1gMnSc1	Metzig
(	(	kIx(	(
<g/>
Metziy	Metziy	k1gInPc1	Metziy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Starostové	Starostové	k2eAgNnSc2d1	Starostové
města	město	k1gNnSc2	město
Bohumína	Bohumín	k1gInSc2	Bohumín
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
neúplný	úplný	k2eNgInSc1d1	neúplný
seznam	seznam	k1gInSc1	seznam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Ševčík	Ševčík	k1gMnSc1	Ševčík
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
MěNV	MěNV	k1gMnSc1	MěNV
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Petrek	Petrek	k1gMnSc1	Petrek
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
MěNV	MěNV	k1gMnSc1	MěNV
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
Kačmařík	Kačmařík	k1gMnSc1	Kačmařík
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
MěNV	MěNV	k1gMnSc1	MěNV
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Štverka	Štverka	k1gFnSc1	Štverka
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Petr	Petr	k1gMnSc1	Petr
Vícha	Vích	k1gMnSc2	Vích
(	(	kIx(	(
<g/>
od	od	k7c2	od
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Muzea	muzeum	k1gNnSc2	muzeum
==	==	k?	==
</s>
</p>
<p>
<s>
Podnikové	podnikový	k2eAgNnSc1d1	podnikové
muzeum	muzeum	k1gNnSc1	muzeum
ŽDB	ŽDB	kA	ŽDB
a.s.	a.s.	k?	a.s.
–	–	k?	–
Bezručova	Bezručův	k2eAgInSc2d1	Bezručův
578	[number]	k4	578
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
</s>
</p>
<p>
<s>
Úzkorozchodné	úzkorozchodný	k2eAgFnPc1d1	úzkorozchodná
dráhy	dráha	k1gFnPc1	dráha
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bohumín	Bohumín	k1gInSc1	Bohumín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bohumín	Bohumín	k1gInSc1	Bohumín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bohumín	Bohumín	k1gInSc1	Bohumín
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
http://www.mesto-bohumin.cz	[url]	k1gInSc1	http://www.mesto-bohumin.cz
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
http://www.bohumin.cz	[url]	k1gMnSc1	http://www.bohumin.cz
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgNnSc1d1	virtuální
cestování	cestování	k1gNnSc1	cestování
Bohumínem	Bohumín	k1gInSc7	Bohumín
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
Bohumín	Bohumín	k1gInSc1	Bohumín
</s>
</p>
<p>
<s>
www.ian.cz	www.ian.cz	k1gInSc1	www.ian.cz
<g/>
,	,	kIx,	,
O	o	k7c6	o
svícení	svícení	k1gNnSc6	svícení
32	[number]	k4	32
Světelné	světelný	k2eAgInPc1d1	světelný
znečištění	znečištění	k1gNnSc4	znečištění
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
</s>
</p>
<p>
<s>
http://www.bunkr-bohumin.cz/	[url]	k?	http://www.bunkr-bohumin.cz/
</s>
</p>
