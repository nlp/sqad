<s>
Augšpurský	augšpurský	k2eAgInSc1d1	augšpurský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
či	či	k8xC	či
přesněji	přesně	k6eAd2	přesně
Augšpurský	augšpurský	k2eAgInSc4d1	augšpurský
říšský	říšský	k2eAgInSc4d1	říšský
a	a	k8xC	a
konfesní	konfesní	k2eAgInSc4d1	konfesní
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Augsburger	Augsburger	k1gMnSc1	Augsburger
Reichs-	Reichs-	k1gFnSc2	Reichs-
und	und	k?	und
Religionsfrieden	Religionsfrieden	k2eAgInSc1d1	Religionsfrieden
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
přijaté	přijatý	k2eAgFnSc2d1	přijatá
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
V.	V.	kA	V.
a	a	k8xC	a
říšskými	říšský	k2eAgNnPc7d1	říšské
knížaty	kníže	k1gNnPc7	kníže
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
v	v	k7c6	v
Augsburgu	Augsburg	k1gInSc6	Augsburg
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1555	[number]	k4	1555
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
první	první	k4xOgNnSc4	první
období	období	k1gNnPc2	období
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dohodě	dohoda	k1gFnSc6	dohoda
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zásady	zásada	k1gFnSc2	zásada
cuius	cuius	k1gInSc4	cuius
regio	regio	k6eAd1	regio
<g/>
,	,	kIx,	,
eius	eius	k6eAd1	eius
religio	religio	k6eAd1	religio
(	(	kIx(	(
<g/>
čí	čí	k3xOyRgFnSc2	čí
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
toho	ten	k3xDgNnSc2	ten
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poddaní	poddaný	k1gMnPc1	poddaný
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
také	také	k9	také
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
sekularizace	sekularizace	k1gFnSc1	sekularizace
(	(	kIx(	(
<g/>
převedení	převedení	k1gNnSc1	převedení
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
do	do	k7c2	do
světských	světský	k2eAgFnPc2d1	světská
rukou	ruka	k1gFnPc2	ruka
<g/>
)	)	kIx)	)
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
knížaty	kníže	k1gMnPc7wR	kníže
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1552	[number]	k4	1552
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
slaví	slavit	k5eAaImIp3nS	slavit
Augsburský	augsburský	k2eAgInSc4d1	augsburský
svátek	svátek	k1gInSc4	svátek
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
