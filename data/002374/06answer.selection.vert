<s>
Augšpurský	augšpurský	k2eAgInSc1d1	augšpurský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
či	či	k8xC	či
přesněji	přesně	k6eAd2	přesně
Augšpurský	augšpurský	k2eAgInSc4d1	augšpurský
říšský	říšský	k2eAgInSc4d1	říšský
a	a	k8xC	a
konfesní	konfesní	k2eAgInSc4d1	konfesní
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Augsburger	Augsburger	k1gMnSc1	Augsburger
Reichs-	Reichs-	k1gFnSc2	Reichs-
und	und	k?	und
Religionsfrieden	Religionsfrieden	k2eAgInSc1d1	Religionsfrieden
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
přijaté	přijatý	k2eAgFnSc2d1	přijatá
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
V.	V.	kA	V.
a	a	k8xC	a
říšskými	říšský	k2eAgNnPc7d1	říšské
knížaty	kníže	k1gNnPc7	kníže
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
v	v	k7c6	v
Augsburgu	Augsburg	k1gInSc6	Augsburg
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1555	[number]	k4	1555
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
první	první	k4xOgNnSc4	první
období	období	k1gNnPc2	období
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
