<p>
<s>
Malířství	malířství	k1gNnSc1	malířství
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
sochařství	sochařství	k1gNnSc2	sochařství
a	a	k8xC	a
grafiky	grafika	k1gFnSc2	grafika
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
částí	část	k1gFnPc2	část
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
postup	postup	k1gInSc1	postup
aplikování	aplikování	k1gNnSc2	aplikování
barviva	barvivo	k1gNnSc2	barvivo
rozpuštěného	rozpuštěný	k2eAgNnSc2d1	rozpuštěné
v	v	k7c6	v
médiu	médium	k1gNnSc6	médium
a	a	k8xC	a
nanášení	nanášení	k1gNnSc6	nanášení
(	(	kIx(	(
<g/>
lepivého	lepivý	k2eAgInSc2d1	lepivý
<g/>
)	)	kIx)	)
činidla	činidlo	k1gNnPc4	činidlo
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
malířské	malířský	k2eAgNnSc4d1	malířské
plátno	plátno	k1gNnSc4	plátno
nebo	nebo	k8xC	nebo
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Člověku	člověk	k1gMnSc3	člověk
zabývajícímu	zabývající	k2eAgMnSc3d1	zabývající
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
činností	činnost	k1gFnSc7	činnost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
malíř	malíř	k1gMnSc1	malíř
<g/>
;	;	kIx,	;
tohoto	tento	k3xDgInSc2	tento
termínu	termín	k1gInSc2	termín
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
profesi	profes	k1gFnSc6	profes
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
historie	historie	k1gFnSc1	historie
malování	malování	k1gNnSc2	malování
(	(	kIx(	(
<g/>
jeskynní	jeskynní	k2eAgNnSc1d1	jeskynní
malířství	malířství	k1gNnSc1	malířství
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
šestkrát	šestkrát	k6eAd1	šestkrát
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
historie	historie	k1gFnSc1	historie
užívání	užívání	k1gNnSc2	užívání
psaného	psaný	k2eAgInSc2d1	psaný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
malba	malba	k1gFnSc1	malba
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
forem	forma	k1gFnPc2	forma
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patronem	patron	k1gInSc7	patron
malířů	malíř	k1gMnPc2	malíř
je	být	k5eAaImIp3nS	být
sv.	sv.	kA	sv.
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
malířství	malířství	k1gNnSc2	malířství
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
funkcemonumentální	funkcemonumentální	k2eAgFnSc2d1	funkcemonumentální
malířství	malířství	k1gNnSc2	malířství
(	(	kIx(	(
<g/>
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
architekturou	architektura	k1gFnSc7	architektura
–	–	k?	–
malby	malba	k1gFnSc2	malba
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
stropech	strop	k1gInPc6	strop
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
deskové	deskový	k2eAgNnSc1d1	deskové
malířství	malířství	k1gNnSc1	malířství
(	(	kIx(	(
<g/>
na	na	k7c6	na
dřevěné	dřevěný	k2eAgFnSc6d1	dřevěná
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
triptych	triptych	k1gInSc1	triptych
<g/>
,	,	kIx,	,
křídlový	křídlový	k2eAgInSc1d1	křídlový
oltář	oltář	k1gInSc1	oltář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ornamentální	ornamentální	k2eAgNnSc1d1	ornamentální
malířství	malířství	k1gNnSc1	malířství
–	–	k?	–
dekorativníPodle	dekorativníPodle	k6eAd1	dekorativníPodle
technikyolejomalba	technikyolejomalba	k1gFnSc1	technikyolejomalba
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
nebo	nebo	k8xC	nebo
desku	deska	k1gFnSc4	deska
</s>
</p>
<p>
<s>
tempera	tempera	k1gFnSc1	tempera
<g/>
,	,	kIx,	,
enkaustika	enkaustika	k1gFnSc1	enkaustika
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
<g/>
,	,	kIx,	,
kvaš	kvaš	k1gInSc1	kvaš
atd.	atd.	kA	atd.
na	na	k7c4	na
papír	papír	k1gInSc4	papír
nebo	nebo	k8xC	nebo
lepenku	lepenka	k1gFnSc4	lepenka
</s>
</p>
<p>
<s>
freska	freska	k1gFnSc1	freska
<g/>
,	,	kIx,	,
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
(	(	kIx(	(
<g/>
al-fresco	alresco	k6eAd1	al-fresco
=	=	kIx~	=
na	na	k7c4	na
mokro	mokro	k1gNnSc4	mokro
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
al	ala	k1gFnPc2	ala
secco	secco	k6eAd1	secco
<g/>
,	,	kIx,	,
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
(	(	kIx(	(
<g/>
na	na	k7c4	na
sucho	sucho	k1gNnSc4	sucho
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
malba	malba	k1gFnSc1	malba
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
</s>
</p>
<p>
<s>
sgrafito	sgrafito	k1gNnSc1	sgrafito
(	(	kIx(	(
<g/>
vyškrabování	vyškrabování	k1gNnSc1	vyškrabování
do	do	k7c2	do
dvouvrstvé	dvouvrstvý	k2eAgFnSc2d1	dvouvrstvá
omítky	omítka	k1gFnSc2	omítka
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
barevná	barevný	k2eAgFnSc1d1	barevná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
graffiti	graffiti	k1gNnSc1	graffiti
</s>
</p>
<p>
<s>
airbrushPodle	airbrushPodle	k6eAd1	airbrushPodle
námětunáboženské	námětunáboženský	k2eAgInPc1d1	námětunáboženský
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
např.	např.	kA	např.
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
</s>
</p>
<p>
<s>
historické	historický	k2eAgFnPc1d1	historická
</s>
</p>
<p>
<s>
portréty	portrét	k1gInPc1	portrét
<g/>
,	,	kIx,	,
autoportréty	autoportrét	k1gInPc1	autoportrét
</s>
</p>
<p>
<s>
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
zátiší	zátiší	k1gNnSc1	zátiší
(	(	kIx(	(
<g/>
nature	natur	k1gMnSc5	natur
morte	mort	k1gMnSc5	mort
–	–	k?	–
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
příroda	příroda	k1gFnSc1	příroda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
žánrový	žánrový	k2eAgInSc1d1	žánrový
námět	námět	k1gInSc1	námět
–	–	k?	–
sociální	sociální	k2eAgFnSc1d1	sociální
tematika	tematika	k1gFnSc1	tematika
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alegorické	alegorický	k2eAgNnSc1d1	alegorické
</s>
</p>
<p>
<s>
abstraktní	abstraktní	k2eAgMnPc1d1	abstraktní
<g/>
,	,	kIx,	,
nefigurativní	figurativní	k2eNgMnPc1d1	figurativní
malbyVnímáme	malbyVnímat	k5eAaImIp1nP	malbyVnímat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
malíř	malíř	k1gMnSc1	malíř
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
–	–	k?	–
koloristé	kolorista	k1gMnPc1	kolorista
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
převládá	převládat	k5eAaImIp3nS	převládat
linie	linie	k1gFnSc1	linie
<g/>
;	;	kIx,	;
kompozice	kompozice	k1gFnSc1	kompozice
<g/>
;	;	kIx,	;
práce	práce	k1gFnSc1	práce
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
<g/>
;	;	kIx,	;
šerosvit	šerosvit	k1gInSc1	šerosvit
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
renesance	renesance	k1gFnSc2	renesance
je	být	k5eAaImIp3nS	být
malba	malba	k1gFnSc1	malba
a	a	k8xC	a
kresba	kresba	k1gFnSc1	kresba
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
umělecký	umělecký	k2eAgInSc4d1	umělecký
projev	projev	k1gInSc4	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Malba	malba	k1gFnSc1	malba
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
malířů	malíř	k1gMnPc2	malíř
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
malířů	malíř	k1gMnPc2	malíř
</s>
</p>
<p>
<s>
Starověké	starověký	k2eAgNnSc1d1	starověké
římské	římský	k2eAgNnSc1d1	římské
malířství	malířství	k1gNnSc1	malířství
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
malířství	malířství	k1gNnSc2	malířství
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
