<p>
<s>
Rust	Rust	k1gInSc1	Rust
je	být	k5eAaImIp3nS	být
víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
<g/>
,	,	kIx,	,
multiparadigmatický	multiparadigmatický	k2eAgInSc1d1	multiparadigmatický
<g/>
,	,	kIx,	,
kompilovaný	kompilovaný	k2eAgInSc1d1	kompilovaný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
organizací	organizace	k1gFnSc7	organizace
Mozilla	Mozilla	k1gMnSc1	Mozilla
Research	Research	k1gMnSc1	Research
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k9	jako
"	"	kIx"	"
<g/>
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
<g/>
,	,	kIx,	,
paralelní	paralelní	k2eAgInSc4d1	paralelní
<g/>
,	,	kIx,	,
praktický	praktický	k2eAgInSc4d1	praktický
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgInPc1d1	podporující
čistě	čistě	k6eAd1	čistě
funkcionální	funkcionální	k2eAgInPc1d1	funkcionální
<g/>
,	,	kIx,	,
imperativně-procedurální	imperativněrocedurální	k2eAgInPc1d1	imperativně-procedurální
<g/>
,	,	kIx,	,
strukturované	strukturovaný	k2eAgInPc1d1	strukturovaný
a	a	k8xC	a
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgInPc1d1	orientovaný
programovací	programovací	k2eAgInPc1d1	programovací
styly	styl	k1gInPc1	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
osobního	osobní	k2eAgInSc2d1	osobní
projektu	projekt	k1gInSc2	projekt
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
Mozilly	Mozilla	k1gFnSc2	Mozilla
jménem	jméno	k1gNnSc7	jméno
Graydon	Graydona	k1gFnPc2	Graydona
Hoare	Hoar	k1gMnSc5	Hoar
<g/>
.	.	kIx.	.
</s>
<s>
Mozilla	Mozilla	k1gFnSc1	Mozilla
začala	začít	k5eAaPmAgFnS	začít
sponzorovat	sponzorovat	k5eAaImF	sponzorovat
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
a	a	k8xC	a
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
práce	práce	k1gFnPc1	práce
posunuly	posunout	k5eAaPmAgFnP	posunout
z	z	k7c2	z
počátečního	počáteční	k2eAgInSc2d1	počáteční
překladače	překladač	k1gInSc2	překladač
(	(	kIx(	(
<g/>
naprogramovaného	naprogramovaný	k2eAgInSc2d1	naprogramovaný
v	v	k7c4	v
OCaml	OCaml	k1gInSc4	OCaml
<g/>
)	)	kIx)	)
do	do	k7c2	do
sebe-hostujícího	sebeostující	k2eAgInSc2d1	sebe-hostující
překladače	překladač	k1gInSc2	překladač
napsaného	napsaný	k2eAgInSc2d1	napsaný
v	v	k7c6	v
Rustu	Rust	k1gInSc6	Rust
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
rustc	rustc	k1gFnSc1	rustc
<g/>
,	,	kIx,	,
úspěšně	úspěšně	k6eAd1	úspěšně
přeložil	přeložit	k5eAaPmAgMnS	přeložit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
rustc	rustc	k6eAd1	rustc
používá	používat	k5eAaImIp3nS	používat
LLVM	LLVM	kA	LLVM
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
back-end	backnd	k1gInSc4	back-end
<g/>
.	.	kIx.	.
<g/>
Prvně	Prvně	k?	Prvně
číslovaná	číslovaný	k2eAgFnSc1d1	číslovaná
pre-alpha	prelpha	k1gFnSc1	pre-alpha
release	release	k6eAd1	release
překladače	překladač	k1gMnPc4	překladač
Rust	Rust	k1gMnSc1	Rust
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Rust	Rust	k1gInSc1	Rust
1.0	[number]	k4	1.0
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
stabilní	stabilní	k2eAgFnSc1d1	stabilní
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
<g/>
Třebaže	třebaže	k8xS	třebaže
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc1	vývoj
sponzorován	sponzorován	k2eAgInSc1d1	sponzorován
Mozillou	Mozilla	k1gFnSc7	Mozilla
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
OpenSource	OpenSourka	k1gFnSc6	OpenSourka
projekt	projekt	k1gInSc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Design	design	k1gInSc1	design
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
byl	být	k5eAaImAgInS	být
vyladěn	vyladěn	k2eAgInSc1d1	vyladěn
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
programování	programování	k1gNnSc2	programování
jádra	jádro	k1gNnSc2	jádro
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
Servo	Servo	k1gNnSc1	Servo
a	a	k8xC	a
kompilátoru	kompilátor	k1gInSc2	kompilátor
jazyka	jazyk	k1gInSc2	jazyk
Rust	Rusta	k1gFnPc2	Rusta
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
současných	současný	k2eAgInPc2d1	současný
příspěvků	příspěvek	k1gInPc2	příspěvek
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
členů	člen	k1gMnPc2	člen
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Design	design	k1gInSc1	design
==	==	k?	==
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
Rustu	Rust	k1gInSc2	Rust
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
dobrým	dobrý	k2eAgInSc7d1	dobrý
jazykem	jazyk	k1gInSc7	jazyk
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
vysoce	vysoce	k6eAd1	vysoce
paralelních	paralelní	k2eAgInPc2d1	paralelní
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
bezpečných	bezpečný	k2eAgInPc2d1	bezpečný
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
byl	být	k5eAaImAgInS	být
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
soubor	soubor	k1gInSc4	soubor
vlastností	vlastnost	k1gFnPc2	vlastnost
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
rozvržení	rozvržení	k1gNnSc2	rozvržení
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
a	a	k8xC	a
paralelizmu	paralelizmus	k1gInSc2	paralelizmus
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
Rustu	Rust	k1gInSc2	Rust
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
<g/>
Syntaxe	syntaxe	k1gFnSc1	syntaxe
Rustu	Rust	k1gInSc2	Rust
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
syntaxi	syntaxe	k1gFnSc4	syntaxe
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
s	s	k7c7	s
bloky	blok	k1gInPc7	blok
kódu	kód	k1gInSc2	kód
ohraničenými	ohraničený	k2eAgFnPc7d1	ohraničená
složenými	složený	k2eAgFnPc7d1	složená
závorkami	závorka	k1gFnPc7	závorka
<g/>
,	,	kIx,	,
a	a	k8xC	a
řídící	řídící	k2eAgNnPc1d1	řídící
klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
programu	program	k1gInSc2	program
jako	jako	k8xS	jako
if	if	k?	if
<g/>
,	,	kIx,	,
else	els	k1gMnSc4	els
<g/>
,	,	kIx,	,
while	whil	k1gMnSc4	whil
<g/>
,	,	kIx,	,
a	a	k8xC	a
for	forum	k1gNnPc2	forum
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechna	všechen	k3xTgNnPc1	všechen
klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
jazyků	jazyk	k1gInPc2	jazyk
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
jsou	být	k5eAaImIp3nP	být
přítomna	přítomen	k2eAgNnPc1d1	přítomno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
některá	některý	k3yIgNnPc4	některý
další	další	k2eAgNnPc4d1	další
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
match	matcha	k1gFnPc2	matcha
pro	pro	k7c4	pro
vícenásobné	vícenásobný	k2eAgInPc4d1	vícenásobný
skoky	skok	k1gInPc4	skok
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc4d1	podobný
switch	switch	k1gInSc4	switch
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zaměněna	zaměnit	k5eAaPmNgFnS	zaměnit
nebo	nebo	k8xC	nebo
přidána	přidat	k5eAaPmNgFnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
syntaktickou	syntaktický	k2eAgFnSc4d1	syntaktická
podobnost	podobnost	k1gFnSc4	podobnost
je	být	k5eAaImIp3nS	být
Rust	Rust	k1gInSc1	Rust
sémanticky	sémanticky	k6eAd1	sémanticky
velmi	velmi	k6eAd1	velmi
odlišný	odlišný	k2eAgMnSc1d1	odlišný
od	od	k7c2	od
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k9	jako
paměťově	paměťově	k6eAd1	paměťově
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
<g/>
,	,	kIx,	,
nepovolující	povolující	k2eNgInPc1d1	povolující
ukazatele	ukazatel	k1gInPc1	ukazatel
NULL	NULL	kA	NULL
a	a	k8xC	a
znemožňující	znemožňující	k2eAgInPc1d1	znemožňující
neplatné	platný	k2eNgInPc1d1	neplatný
ukazatele	ukazatel	k1gInPc1	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Datové	datový	k2eAgFnPc1d1	datová
hodnoty	hodnota	k1gFnPc1	hodnota
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
inicializovány	inicializovat	k5eAaImNgInP	inicializovat
pouze	pouze	k6eAd1	pouze
několika	několik	k4yIc7	několik
pevnými	pevný	k2eAgInPc7d1	pevný
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
způsoby	způsob	k1gInPc1	způsob
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
inicializovaný	inicializovaný	k2eAgInSc4d1	inicializovaný
vstup	vstup	k1gInSc4	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
ukazatelů	ukazatel	k1gMnPc2	ukazatel
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
neměnnost	neměnnost	k1gFnSc1	neměnnost
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kompilátoru	kompilátor	k1gMnSc3	kompilátor
předejít	předejít	k5eAaPmF	předejít
mnohým	mnohý	k2eAgInPc3d1	mnohý
typům	typ	k1gInPc3	typ
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
jeho	on	k3xPp3gMnSc2	on
inteligentního	inteligentní	k2eAgMnSc2d1	inteligentní
ukazatele	ukazatel	k1gMnSc2	ukazatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
typový	typový	k2eAgInSc1d1	typový
systém	systém	k1gInSc1	systém
podporuje	podporovat	k5eAaImIp3nS	podporovat
typ	typ	k1gInSc4	typ
jménem	jméno	k1gNnSc7	jméno
trait	traita	k1gFnPc2	traita
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
typu	typ	k1gInSc2	typ
třída	třída	k1gFnSc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Trait	Trait	k1gInSc1	Trait
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
jazykem	jazyk	k1gInSc7	jazyk
Haskell	Haskellum	k1gNnPc2	Haskellum
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
ad-hoc	adoc	k1gFnSc4	ad-hoc
polymorfismus	polymorfismus	k1gInSc1	polymorfismus
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
zákazem	zákaz	k1gInSc7	zákaz
psaní	psaní	k1gNnSc4	psaní
deklarace	deklarace	k1gFnSc2	deklarace
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Haskell	Haskella	k1gFnPc2	Haskella
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
higher-kinded	higherinded	k1gInSc4	higher-kinded
polymorfizmus	polymorfizmus	k1gInSc1	polymorfizmus
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rust	Rust	k1gInSc1	Rust
nepoužívá	používat	k5eNaImIp3nS	používat
automatickou	automatický	k2eAgFnSc4d1	automatická
správu	správa	k1gFnSc4	správa
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
garbage	garbage	k1gInSc1	garbage
collection	collection	k1gInSc1	collection
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c6	na
platformách	platforma	k1gFnPc6	platforma
Java	Jav	k1gInSc2	Jav
a	a	k8xC	a
.	.	kIx.	.
<g/>
NET	NET	kA	NET
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
jazyky	jazyk	k1gInPc7	jazyk
Java	Javum	k1gNnSc2	Javum
a	a	k8xC	a
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
a	a	k8xC	a
nevyžadující	vyžadující	k2eNgNnPc1d1	nevyžadující
přerušování	přerušování	k1gNnPc1	přerušování
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
a	a	k8xC	a
sledování	sledování	k1gNnSc2	sledování
odkazů	odkaz	k1gInPc2	odkaz
<g/>
.	.	kIx.	.
<g/>
Rust	Rusta	k1gFnPc2	Rusta
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odvození	odvození	k1gNnSc1	odvození
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
type-inference	typenference	k1gFnSc1	type-inference
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
proměnné	proměnná	k1gFnPc4	proměnná
deklarované	deklarovaný	k2eAgFnSc3d1	deklarovaná
použitím	použití	k1gNnSc7	použití
klíčového	klíčový	k2eAgNnSc2d1	klíčové
slova	slovo	k1gNnSc2	slovo
let	let	k1gInSc1	let
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
inicializované	inicializovaný	k2eAgInPc1d1	inicializovaný
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
určit	určit	k5eAaPmF	určit
jejich	jejich	k3xOp3gInSc4	jejich
typ	typ	k1gInSc4	typ
<g/>
,	,	kIx,	,
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
zachovat	zachovat	k5eAaPmF	zachovat
původně	původně	k6eAd1	původně
přidělenu	přidělen	k2eAgFnSc4d1	přidělena
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kompilátor	kompilátor	k1gInSc1	kompilátor
nenalezne	naleznout	k5eNaPmIp3nS	naleznout
na	na	k7c6	na
příslušné	příslušný	k2eAgFnSc6d1	příslušná
větvi	větev	k1gFnSc6	větev
proměnné	proměnná	k1gFnSc2	proměnná
přesnou	přesný	k2eAgFnSc4d1	přesná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
kompilace	kompilace	k1gFnSc1	kompilace
končí	končit	k5eAaImIp3nS	končit
chybou	chyba	k1gFnSc7	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnPc4	funkce
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
generické	generický	k2eAgInPc4d1	generický
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgMnPc4	ten
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vázány	vázat	k5eAaImNgFnP	vázat
traity	trait	k2eAgFnPc1d1	trait
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
obejít	obejít	k5eAaPmF	obejít
signatury	signatura	k1gFnPc4	signatura
typů	typ	k1gInPc2	typ
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
ve	v	k7c6	v
funkcích	funkce	k1gFnPc6	funkce
a	a	k8xC	a
operátorech	operátor	k1gInPc6	operátor
využívat	využívat	k5eAaPmF	využívat
generické	generický	k2eAgFnSc3d1	generická
proměnné	proměnná	k1gFnSc3	proměnná
<g/>
.	.	kIx.	.
<g/>
Objektový	objektový	k2eAgInSc1d1	objektový
systém	systém	k1gInSc1	systém
v	v	k7c6	v
Rustu	Rust	k1gInSc6	Rust
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
okolo	okolo	k7c2	okolo
implementací	implementace	k1gFnPc2	implementace
traitů	traita	k1gMnPc2	traita
a	a	k8xC	a
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
roli	role	k1gFnSc4	role
tříd	třída	k1gFnPc2	třída
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
(	(	kIx(	(
<g/>
objektových	objektový	k2eAgInPc6d1	objektový
<g/>
)	)	kIx)	)
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
klíčovým	klíčový	k2eAgNnSc7d1	klíčové
slovem	slovo	k1gNnSc7	slovo
impl	impla	k1gFnPc2	impla
<g/>
.	.	kIx.	.
</s>
<s>
Dědičnost	dědičnost	k1gFnSc1	dědičnost
a	a	k8xC	a
polymorfizmus	polymorfizmus	k1gInSc1	polymorfizmus
jsou	být	k5eAaImIp3nP	být
poskytovány	poskytovat	k5eAaImNgFnP	poskytovat
traity	trait	k2eAgFnPc1d1	trait
<g/>
;	;	kIx,	;
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
je	on	k3xPp3gFnPc4	on
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
v	v	k7c6	v
mixinech	mixino	k1gNnPc6	mixino
k	k	k7c3	k
implementaci	implementace	k1gFnSc3	implementace
<g/>
.	.	kIx.	.
</s>
<s>
Strukturované	strukturovaný	k2eAgInPc1d1	strukturovaný
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
členských	členský	k2eAgFnPc2d1	členská
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Implementace	implementace	k1gFnPc1	implementace
a	a	k8xC	a
traity	traita	k1gFnPc1	traita
nemohou	moct	k5eNaImIp3nP	moct
definovat	definovat	k5eAaBmF	definovat
členské	členský	k2eAgFnSc2d1	členská
proměnné	proměnná	k1gFnSc2	proměnná
samy	sám	k3xTgFnPc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
traity	traita	k1gFnPc1	traita
mohou	moct	k5eAaImIp3nP	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
dědičnost	dědičnost	k1gFnSc4	dědičnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nenastal	nastat	k5eNaPmAgInS	nastat
diamond	diamond	k1gInSc1	diamond
problem	problo	k1gNnSc7	problo
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Projekty	projekt	k1gInPc1	projekt
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Rust	Rusta	k1gFnPc2	Rusta
==	==	k?	==
</s>
</p>
<p>
<s>
Bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
vykreslovací	vykreslovací	k2eAgNnSc1d1	vykreslovací
jádro	jádro	k1gNnSc1	jádro
pro	pro	k7c4	pro
webové	webový	k2eAgMnPc4d1	webový
prohlížeče	prohlížeč	k1gMnPc4	prohlížeč
Servo	Servo	k1gNnSc4	Servo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
Mozilla	Mozilla	k1gFnSc1	Mozilla
a	a	k8xC	a
Samsung	Samsung	kA	Samsung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
test	test	k1gInSc1	test
Acid	Acid	k1gInSc1	Acid
<g/>
2	[number]	k4	2
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jádro	jádro	k1gNnSc1	jádro
Firefoxu	Firefox	k1gInSc2	Firefox
napsané	napsaný	k2eAgNnSc1d1	napsané
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
(	(	kIx(	(
<g/>
Gecko	Gecko	k1gNnSc1	Gecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Parser	Parser	k1gInSc4	Parser
multimédií	multimédium	k1gNnPc2	multimédium
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
Firefox	Firefox	k1gInSc1	Firefox
je	být	k5eAaImIp3nS	být
také	také	k9	také
napsán	napsat	k5eAaBmNgInS	napsat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Rust	Rusta	k1gFnPc2	Rusta
<g/>
.	.	kIx.	.
<g/>
Emulátor	emulátor	k1gInSc1	emulátor
PlayStation	PlayStation	k1gInSc1	PlayStation
napsaný	napsaný	k2eAgInSc1d1	napsaný
v	v	k7c6	v
Rustu	Rust	k1gInSc6	Rust
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
OpenGL	OpenGL	k1gFnSc2	OpenGL
vykreslování	vykreslování	k1gNnSc1	vykreslování
pomocí	pomoc	k1gFnPc2	pomoc
Glium	Glium	k1gNnSc1	Glium
API	API	kA	API
<g/>
.	.	kIx.	.
-	-	kIx~	-
RustationEmulátor	RustationEmulátor	k1gInSc1	RustationEmulátor
N	N	kA	N
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c6	v
Rustu	Rusto	k1gNnSc6	Rusto
-	-	kIx~	-
Rustendo	Rustendo	k6eAd1	Rustendo
<g/>
64	[number]	k4	64
<g/>
Bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
OpenGL	OpenGL	k1gFnSc7	OpenGL
wrapper	wrappra	k1gFnPc2	wrappra
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
Rust	Rust	k2eAgInSc4d1	Rust
-	-	kIx~	-
GliumUnixový	GliumUnixový	k2eAgInSc4d1	GliumUnixový
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
-	-	kIx~	-
Redox	redox	k2eAgInSc4d1	redox
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
přepisování	přepisování	k1gNnSc1	přepisování
parseru	parser	k1gInSc2	parser
multimédií	multimédium	k1gNnPc2	multimédium
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
Mozilla	Mozillo	k1gNnSc2	Mozillo
Firefox	Firefox	k1gInSc1	Firefox
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
Rust	Rusta	k1gFnPc2	Rusta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
parser	parser	k1gInSc1	parser
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
Firefoxu	Firefox	k1gInSc2	Firefox
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
projekty	projekt	k1gInPc1	projekt
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
překladač	překladač	k1gInSc1	překladač
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
Mozillou	Mozilla	k1gFnSc7	Mozilla
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
překládá	překládat	k5eAaImIp3nS	překládat
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
Rust	Rusta	k1gFnPc2	Rusta
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nP	řešit
mj.	mj.	kA	mj.
překlad	překlad	k1gInSc4	překlad
konstrukcí	konstrukce	k1gFnPc2	konstrukce
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
s	s	k7c7	s
příkazem	příkaz	k1gInSc7	příkaz
goto	goto	k1gNnSc1	goto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaPmNgInS	napsat
v	v	k7c6	v
Haskellu	Haskell	k1gInSc6	Haskell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
kódu	kód	k1gInSc2	kód
==	==	k?	==
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
Hello	Hello	k1gNnSc1	Hello
world	world	k1gInSc4	world
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
traitů	traita	k1gMnPc2	traita
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rust	Rust	k1gInSc1	Rust
(	(	kIx(	(
<g/>
programming	programming	k1gInSc1	programming
language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Eclipse	Eclipse	k1gFnSc1	Eclipse
Corrosion	Corrosion	k1gInSc1	Corrosion
<g/>
:	:	kIx,	:
the	the	k?	the
Eclipse	Eclipse	k1gFnSc2	Eclipse
IDE	IDE	kA	IDE
for	forum	k1gNnPc2	forum
Rust	Rust	k1gInSc1	Rust
-	-	kIx~	-
Vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
IDE	IDE	kA	IDE
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Rust	Rust	k1gInSc4	Rust
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Užitečné	užitečný	k2eAgInPc4d1	užitečný
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
Rust	Rust	k2eAgInSc4d1	Rust
<g/>
:	:	kIx,	:
rustup	rustup	k1gInSc4	rustup
a	a	k8xC	a
rust-clippy	rustlipp	k1gInPc4	rust-clipp
-	-	kIx~	-
na	na	k7c4	na
Root	Root	k1gInSc4	Root
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Seriál	seriál	k1gInSc1	seriál
Programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
Rust	Rust	k2eAgInSc4d1	Rust
-	-	kIx~	-
seriál	seriál	k1gInSc4	seriál
na	na	k7c4	na
Root	Root	k1gInSc4	Root
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rust	Rust	k1gInSc4	Rust
by	by	kYmCp3nS	by
Example	Example	k1gMnSc1	Example
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Redox	redox	k2eAgNnSc1d1	redox
<g/>
:	:	kIx,	:
A	a	k9	a
Rust-Written	Rust-Written	k2eAgMnSc1d1	Rust-Written
<g/>
,	,	kIx,	,
Microkernel	Microkernel	k1gMnSc1	Microkernel
Open-Source	Open-Sourka	k1gFnSc3	Open-Sourka
OS	OS	kA	OS
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Rust	Rust	k1gInSc1	Rust
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
2018	[number]	k4	2018
roadmap	roadmap	k1gInSc1	roadmap
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
roadmapa	roadmapa	k1gFnSc1	roadmapa
Rustu	Rust	k1gInSc2	Rust
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Rust	Rust	k1gInSc1	Rust
1.18	[number]	k4	1.18
má	mít	k5eAaImIp3nS	mít
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
kompilátor	kompilátor	k1gInSc4	kompilátor
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
edicí	edice	k1gFnSc7	edice
oficiální	oficiální	k2eAgFnSc2d1	oficiální
knihy	kniha	k1gFnSc2	kniha
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Corrode	Corrod	k1gInSc5	Corrod
automaticky	automaticky	k6eAd1	automaticky
"	"	kIx"	"
<g/>
zrezaví	zrezavit	k5eAaPmIp3nS	zrezavit
<g/>
"	"	kIx"	"
kód	kód	k1gInSc4	kód
z	z	k7c2	z
C	C	kA	C
do	do	k7c2	do
Rust	Rusta	k1gFnPc2	Rusta
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Rust	Rust	k2eAgMnSc1d1	Rust
oficiálně	oficiálně	k6eAd1	oficiálně
ve	v	k7c6	v
Fedoře	Fedora	k1gFnSc6	Fedora
</s>
</p>
