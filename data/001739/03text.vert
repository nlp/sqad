<s>
Violoncello	violoncello	k1gNnSc1	violoncello
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
cello	cello	k1gNnSc4	cello
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc1d1	strunný
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
nástroj	nástroj	k1gInSc1	nástroj
se	s	k7c7	s
strunami	struna	k1gFnPc7	struna
laděnými	laděný	k2eAgFnPc7d1	laděná
v	v	k7c6	v
čistých	čistý	k2eAgFnPc6d1	čistá
kvintách	kvinta	k1gFnPc6	kvinta
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
viola	viola	k1gFnSc1	viola
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
nástroje	nástroj	k1gInSc2	nástroj
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
italského	italský	k2eAgNnSc2d1	italské
slova	slovo	k1gNnSc2	slovo
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
maličký	maličký	k2eAgInSc1d1	maličký
violon	violon	k1gInSc1	violon
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
komorních	komorní	k2eAgInPc2d1	komorní
<g/>
,	,	kIx,	,
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
i	i	k8xC	i
symfonických	symfonický	k2eAgInPc2d1	symfonický
orchestrů	orchestr	k1gInPc2	orchestr
a	a	k8xC	a
také	také	k9	také
mnoha	mnoho	k4c2	mnoho
komorních	komorní	k2eAgInPc2d1	komorní
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
standardním	standardní	k2eAgInSc7d1	standardní
sólovým	sólový	k2eAgInSc7d1	sólový
nástrojem	nástroj	k1gInSc7	nástroj
koncertních	koncertní	k2eAgFnPc2d1	koncertní
skladeb	skladba	k1gFnPc2	skladba
mnoha	mnoho	k4c2	mnoho
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
žánrech	žánr	k1gInPc6	žánr
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
je	být	k5eAaImIp3nS	být
notováno	notován	k2eAgNnSc1d1	notováno
v	v	k7c6	v
basovém	basový	k2eAgInSc6d1	basový
klíči	klíč	k1gInSc6	klíč
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
pasážích	pasáž	k1gFnPc6	pasáž
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
tenorový	tenorový	k2eAgInSc4d1	tenorový
a	a	k8xC	a
houslový	houslový	k2eAgInSc4d1	houslový
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
violoncellista	violoncellista	k1gMnSc1	violoncellista
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
zkráceně	zkráceně	k6eAd1	zkráceně
cellista	cellista	k1gMnSc1	cellista
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
trupu	trup	k1gInSc2	trup
<g/>
:	:	kIx,	:
750	[number]	k4	750
<g/>
-	-	kIx~	-
<g/>
760	[number]	k4	760
mm	mm	kA	mm
Výška	výška	k1gFnSc1	výška
lubů	lub	k1gInPc2	lub
<g/>
:	:	kIx,	:
111	[number]	k4	111
mm	mm	kA	mm
Délka	délka	k1gFnSc1	délka
chvějících	chvějící	k2eAgFnPc2d1	chvějící
se	se	k3xPyFc4	se
strun	struna	k1gFnPc2	struna
<g/>
:	:	kIx,	:
690	[number]	k4	690
mm	mm	kA	mm
Průměr	průměr	k1gInSc4	průměr
strun	struna	k1gFnPc2	struna
<g/>
:	:	kIx,	:
0,8	[number]	k4	0,8
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
mm	mm	kA	mm
Délka	délka	k1gFnSc1	délka
smyčce	smyčec	k1gInSc2	smyčec
<g/>
:	:	kIx,	:
710	[number]	k4	710
<g/>
-	-	kIx~	-
<g/>
730	[number]	k4	730
mm	mm	kA	mm
Cello	cello	k1gNnSc1	cello
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
viola	viola	k1gFnSc1	viola
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
mezi	mezi	k7c7	mezi
koleny	koleno	k1gNnPc7	koleno
a	a	k8xC	a
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
vysouvatelným	vysouvatelný	k2eAgInSc7d1	vysouvatelný
bodcem	bodec	k1gInSc7	bodec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zakončený	zakončený	k2eAgInSc1d1	zakončený
gumovou	gumový	k2eAgFnSc7d1	gumová
špičkou	špička	k1gFnSc7	špička
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
sklouznutí	sklouznutí	k1gNnSc4	sklouznutí
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
viola	viola	k1gFnSc1	viola
je	být	k5eAaImIp3nS	být
i	i	k9	i
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
luby	lub	k1gInPc1	lub
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
deska	deska	k1gFnSc1	deska
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
javorového	javorový	k2eAgNnSc2d1	javorové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
javoru	javor	k1gInSc2	javor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
i	i	k9	i
třešeň	třešeň	k1gFnSc1	třešeň
<g/>
,	,	kIx,	,
hrušeň	hrušeň	k1gFnSc1	hrušeň
<g/>
,	,	kIx,	,
ořešák	ořešák	k1gInSc1	ořešák
nebo	nebo	k8xC	nebo
i	i	k9	i
topol	topol	k1gInSc1	topol
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
rezonančního	rezonanční	k2eAgNnSc2d1	rezonanční
smrkového	smrkový	k2eAgNnSc2d1	smrkové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Hmatník	hmatník	k1gInSc1	hmatník
<g/>
,	,	kIx,	,
struník	struník	k1gInSc1	struník
a	a	k8xC	a
kolíčky	kolíček	k1gInPc1	kolíček
bývají	bývat	k5eAaImIp3nP	bývat
z	z	k7c2	z
ebenového	ebenový	k2eAgNnSc2d1	ebenové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
palisandru	palisandr	k1gInSc2	palisandr
<g/>
,	,	kIx,	,
zimostrázu	zimostráz	k1gInSc2	zimostráz
nebo	nebo	k8xC	nebo
levnějšího	levný	k2eAgNnSc2d2	levnější
začerněného	začerněný	k2eAgNnSc2d1	začerněné
javorového	javorový	k2eAgNnSc2d1	javorové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlevnější	levný	k2eAgInPc4d3	nejlevnější
nástroje	nástroj	k1gInPc4	nástroj
mívají	mívat	k5eAaImIp3nP	mívat
obě	dva	k4xCgFnPc1	dva
desky	deska	k1gFnPc1	deska
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
z	z	k7c2	z
laminátu	laminát	k1gInSc2	laminát
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
některá	některý	k3yIgNnPc4	některý
moderní	moderní	k2eAgNnPc4d1	moderní
cella	cello	k1gNnPc4	cello
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
vyztuženého	vyztužený	k2eAgInSc2d1	vyztužený
uhlíkovým	uhlíkový	k2eAgNnSc7d1	uhlíkové
vláknem	vlákno	k1gNnSc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
má	mít	k5eAaImIp3nS	mít
prakticky	prakticky	k6eAd1	prakticky
stejnou	stejný	k2eAgFnSc4d1	stejná
stavbu	stavba	k1gFnSc4	stavba
jako	jako	k8xS	jako
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
houslemi	housle	k1gFnPc7	housle
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
trupu	trup	k1gInSc2	trup
violoncella	violoncello	k1gNnSc2	violoncello
přibližně	přibližně	k6eAd1	přibližně
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
lubů	lub	k1gInPc2	lub
asi	asi	k9	asi
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
relativně	relativně	k6eAd1	relativně
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
první	první	k4xOgInSc1	první
alikvótní	alikvótní	k2eAgInSc1d1	alikvótní
tón	tón	k1gInSc1	tón
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
pro	pro	k7c4	pro
cello	cello	k1gNnSc4	cello
typický	typický	k2eAgInSc1d1	typický
vřelý	vřelý	k2eAgInSc1d1	vřelý
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
houslí	housle	k1gFnPc2	housle
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
menší	malý	k2eAgInPc1d2	menší
formáty	formát	k1gInPc1	formát
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníci	hudebník	k1gMnPc1	hudebník
menší	malý	k2eAgFnSc2d2	menší
postavy	postava	k1gFnSc2	postava
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
sedmiosminové	sedmiosminový	k2eAgNnSc4d1	sedmiosminový
cello	cello	k1gNnSc4	cello
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jim	on	k3xPp3gFnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohodlnější	pohodlný	k2eAgFnSc4d2	pohodlnější
hru	hra	k1gFnSc4	hra
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
nástroje	nástroj	k1gInPc1	nástroj
ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Ladění	ladění	k1gNnSc1	ladění
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
je	být	k5eAaImIp3nS	být
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
houslových	houslový	k2eAgInPc2d1	houslový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
violovými	violový	k2eAgInPc7d1	violový
nástroji	nástroj	k1gInPc7	nástroj
(	(	kIx(	(
<g/>
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
violy	viola	k1gFnSc2	viola
da	da	k?	da
gamba	gamba	k1gFnSc1	gamba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
moderní	moderní	k2eAgNnSc1d1	moderní
violoncello	violoncello	k1gNnSc1	violoncello
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
v	v	k7c6	v
Cremoně	Cremon	k1gInSc6	Cremon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1572	[number]	k4	1572
Andrea	Andrea	k1gFnSc1	Andrea
Amati	Amať	k1gFnSc2	Amať
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
výrobci	výrobce	k1gMnPc1	výrobce
violoncell	violoncello	k1gNnPc2	violoncello
jsou	být	k5eAaImIp3nP	být
Gasparo	Gaspara	k1gFnSc5	Gaspara
da	da	k?	da
Salo	Salo	k1gNnSc4	Salo
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
-	-	kIx~	-
<g/>
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Giovanni	Giovanň	k1gMnSc5	Giovanň
Paolo	Paola	k1gMnSc5	Paola
Maggini	Maggin	k1gMnPc1	Maggin
(	(	kIx(	(
<g/>
1581	[number]	k4	1581
<g/>
-	-	kIx~	-
<g/>
1632	[number]	k4	1632
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
<g/>
-	-	kIx~	-
<g/>
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dnešních	dnešní	k2eAgInPc2d1	dnešní
nástrojů	nástroj	k1gInPc2	nástroj
neměla	mít	k5eNaImAgFnS	mít
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
violoncella	violoncello	k1gNnSc2	violoncello
bodec	bodec	k1gInSc4	bodec
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
držet	držet	k5eAaImF	držet
pevně	pevně	k6eAd1	pevně
mezi	mezi	k7c7	mezi
koleny	koleno	k1gNnPc7	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dolaďovače	dolaďovač	k1gInPc1	dolaďovač
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
upevnění	upevnění	k1gNnSc2	upevnění
strun	struna	k1gFnPc2	struna
ke	k	k7c3	k
struníku	struník	k1gInSc3	struník
vřazeny	vřazen	k2eAgMnPc4d1	vřazen
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
část	část	k1gFnSc1	část
barokních	barokní	k2eAgInPc2d1	barokní
smyčců	smyčec	k1gInPc2	smyčec
byla	být	k5eAaImAgFnS	být
prohnutá	prohnutý	k2eAgFnSc1d1	prohnutá
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
nástrojů	nástroj	k1gInPc2	nástroj
prohnutá	prohnutý	k2eAgFnSc1d1	prohnutá
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
větší	veliký	k2eAgNnSc4d2	veliký
napětí	napětí	k1gNnSc4	napětí
žíní	žíně	k1gFnPc2	žíně
a	a	k8xC	a
silnější	silný	k2eAgInSc4d2	silnější
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
vydává	vydávat	k5eAaImIp3nS	vydávat
bohatý	bohatý	k2eAgInSc4d1	bohatý
a	a	k8xC	a
vřelý	vřelý	k2eAgInSc4d1	vřelý
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
polohách	poloha	k1gFnPc6	poloha
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
lidskému	lidský	k2eAgInSc3d1	lidský
hlasu	hlas	k1gInSc3	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
vysokých	vysoký	k2eAgInPc2d1	vysoký
tónů	tón	k1gInPc2	tón
barva	barva	k1gFnSc1	barva
tónu	tón	k1gInSc2	tón
připomíná	připomínat	k5eAaImIp3nS	připomínat
violu	viola	k1gFnSc4	viola
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
podladit	podladit	k5eAaImF	podladit
a	a	k8xC	a
pozměnit	pozměnit	k5eAaPmF	pozměnit
tak	tak	k9	tak
rozsah	rozsah	k1gInSc4	rozsah
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
v	v	k7c6	v
Sonátě	sonáta	k1gFnSc6	sonáta
pro	pro	k7c4	pro
cello	cello	k1gNnSc4	cello
sólo	sólo	k2eAgMnSc2d1	sólo
Zoltána	Zoltán	k1gMnSc2	Zoltán
Kodálye	Kodály	k1gMnSc2	Kodály
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
struna	struna	k1gFnSc1	struna
C	C	kA	C
podladí	podladit	k5eAaPmIp3nS	podladit
o	o	k7c4	o
půltón	půltón	k1gInSc4	půltón
níž	nízce	k6eAd2	nízce
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
na	na	k7c6	na
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Violoncello	violoncello	k1gNnSc1	violoncello
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
mezi	mezi	k7c7	mezi
koleny	koleno	k1gNnPc7	koleno
<g/>
,	,	kIx,	,
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
bodcem	bodec	k1gInSc7	bodec
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
vedle	vedle	k7c2	vedle
levého	levý	k2eAgNnSc2d1	levé
ucha	ucho	k1gNnSc2	ucho
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
struna	struna	k1gFnSc1	struna
C	C	kA	C
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
neslušné	slušný	k2eNgNnSc4d1	neslušné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hrály	hrát	k5eAaImAgFnP	hrát
ženy	žena	k1gFnPc1	žena
na	na	k7c4	na
cello	cello	k1gNnSc4	cello
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
měly	mít	k5eAaImAgFnP	mít
nástroj	nástroj	k1gInSc4	nástroj
opřený	opřený	k2eAgInSc4d1	opřený
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
vedle	vedle	k7c2	vedle
obou	dva	k4xCgFnPc2	dva
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Herní	herní	k2eAgFnPc1d1	herní
techniky	technika	k1gFnPc1	technika
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejné	stejný	k2eAgFnPc4d1	stejná
jako	jako	k9	jako
u	u	k7c2	u
houslí	housle	k1gFnPc2	housle
nebo	nebo	k8xC	nebo
u	u	k7c2	u
violy	viola	k1gFnSc2	viola
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
první	první	k4xOgFnSc1	první
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prsty	prst	k1gInPc1	prst
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
na	na	k7c4	na
půlton	půlton	k1gInSc4	půlton
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
půltá	půltý	k2eAgFnSc1d1	půltá
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
poloha	poloha	k1gFnSc1	poloha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
violoncella	violoncello	k1gNnSc2	violoncello
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
používají	používat	k5eAaImIp3nP	používat
tzv.	tzv.	kA	tzv.
palcové	palcový	k2eAgInPc1d1	palcový
polohy	poloh	k1gInPc1	poloh
(	(	kIx(	(
<g/>
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
polohy	poloha	k1gFnSc2	poloha
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
palec	palec	k1gInSc1	palec
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
tiskne	tisknout	k5eAaImIp3nS	tisknout
dvě	dva	k4xCgFnPc4	dva
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
horní	horní	k2eAgFnSc2d1	horní
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
při	při	k7c6	při
barré	barrý	k2eAgFnSc6d1	barrý
hmatech	hmat	k1gInPc6	hmat
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
prsty	prst	k1gInPc7	prst
hráč	hráč	k1gMnSc1	hráč
tiskne	tisknout	k5eAaImIp3nS	tisknout
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
struny	struna	k1gFnPc4	struna
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Popper	Popper	k1gMnSc1	Popper
-	-	kIx~	-
Hochschüle	Hochschüle	k1gFnSc1	Hochschüle
<g/>
,	,	kIx,	,
Alfredo	Alfredo	k1gNnSc1	Alfredo
Piatti	Piatť	k1gFnSc2	Piatť
-	-	kIx~	-
Capriccia	capriccio	k1gNnSc2	capriccio
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
-	-	kIx~	-
Sólové	sólový	k2eAgFnSc2d1	sólová
suity	suita	k1gFnSc2	suita
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
-	-	kIx~	-
Koncert	koncert	k1gInSc1	koncert
C	C	kA	C
dur	dur	k1gNnSc2	dur
a	a	k8xC	a
D	D	kA	D
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
-	-	kIx~	-
Koncert	koncert	k1gInSc1	koncert
h	h	k?	h
moll	moll	k1gNnSc2	moll
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
titulů	titul	k1gInPc2	titul
viz	vidět	k5eAaImRp2nS	vidět
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Steven	Steven	k2eAgInSc4d1	Steven
Sharp	sharp	k1gInSc4	sharp
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
,	,	kIx,	,
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
du	du	k?	du
Pré	pré	k1gNnPc1	pré
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
·	·	k?	·
Mstislav	Mstislava	k1gFnPc2	Mstislava
Leopoldovič	Leopoldovič	k1gMnSc1	Leopoldovič
Rostropovič	Rostropovič	k1gMnSc1	Rostropovič
·	·	k?	·
Paul	Paul	k1gMnSc1	Paul
Tortelier	Tortelier	k1gMnSc1	Tortelier
·	·	k?	·
Boris	Boris	k1gMnSc1	Boris
Pergamenščikov	Pergamenščikov	k1gInSc1	Pergamenščikov
·	·	k?	·
Pablo	Pablo	k1gNnSc1	Pablo
Casals	Casalsa	k1gFnPc2	Casalsa
·	·	k?	·
Domenico	Domenico	k1gMnSc1	Domenico
Gabrielli	Gabriell	k1gMnPc1	Gabriell
·	·	k?	·
Luigi	Luigi	k1gNnSc2	Luigi
Boccherini	Boccherin	k2eAgMnPc1d1	Boccherin
·	·	k?	·
Pierre	Pierr	k1gInSc5	Pierr
Fournier	Fournier	k1gMnSc1	Fournier
·	·	k?	·
Misha	Misha	k1gMnSc1	Misha
Maisky	Maiska	k1gFnSc2	Maiska
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
·	·	k?	·
Yo-Yo	Yo-Yo	k1gMnSc1	Yo-Yo
Ma	Ma	k1gMnSc1	Ma
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
]	]	kIx)	]
·	·	k?	·
Janos	Janos	k1gMnSc1	Janos
Starker	Starker	k1gMnSc1	Starker
·	·	k?	·
Truls	Truls	k1gInSc1	Truls
Mø	Mø	k1gMnSc1	Mø
·	·	k?	·
Apocalyptica	Apocalyptica	k1gMnSc1	Apocalyptica
·	·	k?	·
Zoë	Zoë	k1gFnSc1	Zoë
Keating	Keating	k1gInSc1	Keating
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
David	David	k1gMnSc1	David
Popper	Popper	k1gMnSc1	Popper
·	·	k?	·
Jan	Jan	k1gMnSc1	Jan
Škrdlík	škrdlík	k1gMnSc1	škrdlík
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
·	·	k?	·
Miloš	Miloš	k1gMnSc1	Miloš
Sádlo	sádlo	k1gNnSc1	sádlo
·	·	k?	·
Michaela	Michaela	k1gFnSc1	Michaela
Fukačová	Fukačová	k1gFnSc1	Fukačová
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
·	·	k?	·
Josef	Josef	k1gMnSc1	Josef
Chuchro	Chuchra	k1gFnSc5	Chuchra
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
·	·	k?	·
Daniel	Daniel	k1gMnSc1	Daniel
Veis	Veisa	k1gFnPc2	Veisa
<g />
.	.	kIx.	.
</s>
<s>
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
·	·	k?	·
Jiří	Jiří	k1gMnSc1	Jiří
Hošek	Hošek	k1gMnSc1	Hošek
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
·	·	k?	·
Miroslav	Miroslav	k1gMnSc1	Miroslav
Petráš	Petráš	k1gMnSc1	Petráš
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
·	·	k?	·
Vladan	Vladana	k1gFnPc2	Vladana
Kočí	Kočí	k2eAgFnSc1d1	Kočí
[	[	kIx(	[
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
·	·	k?	·
Jiří	Jiří	k1gMnSc1	Jiří
Bárta	Bárta	k1gMnSc1	Bárta
[	[	kIx(	[
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
<g/>
·	·	k?	·
František	František	k1gMnSc1	František
Brikcius	Brikcius	k1gMnSc1	Brikcius
[	[	kIx(	[
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
·	·	k?	·
Josef	Josef	k1gMnSc1	Josef
Krečmer	Krečmer	k1gMnSc1	Krečmer
·	·	k?	·
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jamník	jamník	k1gMnSc1	jamník
·	·	k?	·
Marek	Marek	k1gMnSc1	Marek
Štryncl	Štryncl	k1gMnSc1	Štryncl
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Klíč	klíč	k1gInSc1	klíč
[	[	kIx(	[
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
·	·	k?	·
Bedřich	Bedřich	k1gMnSc1	Bedřich
Havlík	Havlík	k1gMnSc1	Havlík
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Nouzovský	Nouzovský	k1gMnSc1	Nouzovský
[	[	kIx(	[
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
Petr	Petr	k1gMnSc1	Petr
Hejný	Hejný	k1gMnSc1	Hejný
[	[	kIx(	[
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Cello	cello	k1gNnSc1	cello
na	na	k7c4	na
anglické	anglický	k2eAgFnPc4d1	anglická
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
a	a	k8xC	a
Violoncello	violoncello	k1gNnSc4	violoncello
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
violoncello	violoncello	k1gNnSc4	violoncello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Violoncello	violoncello	k1gNnSc1	violoncello
-	-	kIx~	-
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
o	o	k7c6	o
violoncelle	violoncella	k1gFnSc6	violoncella
</s>
