<s>
Ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
prohloubení	prohloubení	k1gNnSc2	prohloubení
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
