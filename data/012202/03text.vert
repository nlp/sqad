<p>
<s>
Ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
prohloubení	prohloubení	k1gNnSc2	prohloubení
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
jezerech	jezero	k1gNnPc6	jezero
není	být	k5eNaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
zbyla	zbýt	k5eAaPmAgFnS	zbýt
po	po	k7c6	po
roztání	roztání	k1gNnSc6	roztání
ledovců	ledovec	k1gInPc2	ledovec
samotných	samotný	k2eAgInPc2d1	samotný
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
ta	ten	k3xDgFnSc1	ten
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
odtekla	odtéct	k5eAaPmAgFnS	odtéct
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dokonce	dokonce	k9	dokonce
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
ledovců	ledovec	k1gInPc2	ledovec
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
jezerech	jezero	k1gNnPc6	jezero
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horská	horský	k2eAgNnPc1d1	horské
ledovcová	ledovcový	k2eAgNnPc1d1	ledovcové
jezera	jezero	k1gNnPc1	jezero
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jsou	být	k5eAaImIp3nP	být
nazývaná	nazývaný	k2eAgNnPc1d1	nazývané
plesa	pleso	k1gNnPc1	pleso
–	–	k?	–
např.	např.	kA	např.
Štrbské	štrbský	k2eAgNnSc1d1	Štrbské
pleso	pleso	k1gNnSc1	pleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
staw	staw	k?	staw
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Ledovcová	ledovcový	k2eAgNnPc1d1	ledovcové
jezera	jezero	k1gNnPc1	jezero
můžeme	moct	k5eAaImIp1nP	moct
dále	daleko	k6eAd2	daleko
členit	členit	k5eAaImF	členit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
proglaciální	proglaciální	k2eAgFnSc1d1	proglaciální
tj.	tj.	kA	tj.
hrazená	hrazený	k2eAgFnSc1d1	hrazená
ledovcem	ledovec	k1gInSc7	ledovec
</s>
</p>
<p>
<s>
evorzní	evorznit	k5eAaPmIp3nP	evorznit
např.	např.	kA	např.
pod	pod	k7c4	pod
vodopády	vodopád	k1gInPc4	vodopád
(	(	kIx(	(
<g/>
Čertova	čertův	k2eAgFnSc1d1	Čertova
oka	oka	k1gFnSc1	oka
pod	pod	k7c7	pod
Mumlavským	Mumlavský	k2eAgInSc7d1	Mumlavský
vodopádem	vodopád	k1gInSc7	vodopád
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
karová	karový	k2eAgFnSc1d1	Karová
(	(	kIx(	(
<g/>
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Plešné	Plešný	k2eAgNnSc1d1	Plešné
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
morénová	morénový	k2eAgFnSc1d1	morénová
(	(	kIx(	(
<g/>
Śniardwy	Śniardwy	k1gInPc1	Śniardwy
<g/>
,	,	kIx,	,
Mamry	Mamr	k1gInPc1	Mamr
<g/>
,	,	kIx,	,
Roś	Roś	k1gFnPc1	Roś
<g/>
,	,	kIx,	,
Niegocin	Niegocin	k1gInSc1	Niegocin
<g/>
,	,	kIx,	,
Štrbské	štrbský	k2eAgNnSc1d1	Štrbské
pleso	pleso	k1gNnSc1	pleso
<g/>
,	,	kIx,	,
Smreczyński	Smreczyński	k1gNnSc1	Smreczyński
Staw	Staw	k1gFnPc2	Staw
<g/>
,	,	kIx,	,
Mechové	mechový	k2eAgNnSc4d1	mechové
jezírko	jezírko	k1gNnSc4	jezírko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fjordová	fjordový	k2eAgFnSc1d1	fjordový
(	(	kIx(	(
<g/>
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
očka	očko	k1gNnPc1	očko
</s>
</p>
<p>
<s>
rinová	rinová	k1gFnSc1	rinová
(	(	kIx(	(
<g/>
Jeziorak	Jeziorak	k1gInSc1	Jeziorak
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ledovcových	ledovcový	k2eAgNnPc2d1	ledovcové
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jezioro	Jeziora	k1gFnSc5	Jeziora
polodowcowe	polodowcowe	k1gNnPc2	polodowcowe
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
