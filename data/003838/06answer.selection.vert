<s>
Prvoci	prvok	k1gMnPc1	prvok
(	(	kIx(	(
<g/>
Protozoa	Protozoa	k1gMnSc1	Protozoa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
jednobuněčné	jednobuněčný	k2eAgFnPc4d1	jednobuněčná
eukaryotní	eukaryotní	k2eAgFnPc4d1	eukaryotní
heterotrofní	heterotrofní	k2eAgFnPc4d1	heterotrofní
(	(	kIx(	(
<g/>
či	či	k8xC	či
mixotrofní	mixotrofní	k2eAgInPc4d1	mixotrofní
<g/>
)	)	kIx)	)
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
Animalia	Animalia	k1gFnSc1	Animalia
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc4	neschopnost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
