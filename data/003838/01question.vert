<s>
Jaké	jaký	k3yQgNnSc1	jaký
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
jednobuněčné	jednobuněčný	k2eAgInPc4d1	jednobuněčný
eukaryotní	eukaryotní	k2eAgInPc4d1	eukaryotní
heterotrofní	heterotrofní	k2eAgInPc4d1	heterotrofní
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
<g/>
?	?	kIx.	?
</s>
