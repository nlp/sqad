<s>
Ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
rychlosti	rychlost	k1gFnPc1	rychlost
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
km	km	kA	km
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vhodné	vhodný	k2eAgFnPc1d1	vhodná
podmínky	podmínka	k1gFnPc1	podmínka
jako	jako	k8xS	jako
příkrý	příkrý	k2eAgInSc1d1	příkrý
svah	svah	k1gInSc1	svah
či	či	k8xC	či
vysoká	vysoký	k2eAgFnSc1d1	vysoká
rychlost	rychlost	k1gFnSc1	rychlost
tvoření	tvoření	k1gNnSc2	tvoření
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
