<s>
Litovel	Litovel	k1gFnSc1	Litovel
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Littau	Littaus	k1gInSc3	Littaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Olomouce	Olomouc	k1gFnSc2	Olomouc
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
jejíchž	jejíž	k3xOyRp3gMnPc2	jejíž
šest	šest	k4xCc1	šest
ramen	rameno	k1gNnPc2	rameno
dodává	dodávat	k5eAaImIp3nS	dodávat
Litovli	Litovel	k1gFnSc3	Litovel
specifický	specifický	k2eAgInSc4d1	specifický
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
hanácké	hanácký	k2eAgFnPc4d1	Hanácká
Benátky	Benátky	k1gFnPc4	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
Nečíz	Nečíza	k1gFnPc2	Nečíza
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
radniční	radniční	k2eAgFnSc7d1	radniční
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
celým	celý	k2eAgNnSc7d1	celé
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgFnSc1d1	katastrální
výměra	výměra	k1gFnSc1	výměra
činí	činit	k5eAaImIp3nS	činit
46,39	[number]	k4	46,39
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
233	[number]	k4	233
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Drahanské	Drahanský	k2eAgFnSc2d1	Drahanská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
1252	[number]	k4	1252
<g/>
-	-	kIx~	-
<g/>
1256	[number]	k4	1256
-	-	kIx~	-
doba	doba	k1gFnSc1	doba
vzniku	vznik	k1gInSc2	vznik
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
dendrochronologických	dendrochronologický	k2eAgInPc2d1	dendrochronologický
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
město	město	k1gNnSc4	město
založil	založit	k5eAaPmAgMnS	založit
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
1270	[number]	k4	1270
<g/>
-	-	kIx~	-
<g/>
1272	[number]	k4	1272
-	-	kIx~	-
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Litovli	Litovel	k1gFnSc6	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listinách	listina	k1gFnPc6	listina
kláštera	klášter	k1gInSc2	klášter
Hradisko	hradisko	k1gNnSc1	hradisko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jméno	jméno	k1gNnSc4	jméno
Luthowl	Luthowl	k1gInSc1	Luthowl
1287	[number]	k4	1287
-	-	kIx~	-
první	první	k4xOgFnSc1	první
autentická	autentický	k2eAgFnSc1d1	autentická
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
listina	listina	k1gFnSc1	listina
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
města	město	k1gNnSc2	město
1291	[number]	k4	1291
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
udělil	udělit	k5eAaPmAgMnS	udělit
Litovli	Litovel	k1gFnSc6	Litovel
právo	právo	k1gNnSc4	právo
várečné	várečný	k2eAgNnSc4d1	várečné
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
mílové	mílový	k2eAgFnSc2d1	Mílová
<g/>
,	,	kIx,	,
a	a	k8xC	a
Litovel	Litovel	k1gFnSc1	Litovel
byla	být	k5eAaImAgFnS	být
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
trhovou	trhový	k2eAgFnSc4d1	trhová
ves	ves	k1gFnSc4	ves
1327	[number]	k4	1327
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
povolil	povolit	k5eAaPmAgInS	povolit
městu	město	k1gNnSc3	město
stavbu	stavba	k1gFnSc4	stavba
hradeb	hradba	k1gFnPc2	hradba
1899	[number]	k4	1899
-	-	kIx~	-
Čechové	Čech	k1gMnPc1	Čech
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
obecní	obecní	k2eAgFnPc4d1	obecní
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
Litovel	Litovel	k1gFnSc1	Litovel
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
českou	český	k2eAgFnSc4d1	Česká
správu	správa	k1gFnSc4	správa
1893	[number]	k4	1893
-	-	kIx~	-
založení	založení	k1gNnSc6	založení
Rolnického	rolnický	k2eAgInSc2d1	rolnický
Pivovaru	pivovar	k1gInSc2	pivovar
v	v	k7c6	v
Litovli	Litovel	k1gFnSc6	Litovel
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
-	-	kIx~	-
tisíciletá	tisíciletý	k2eAgFnSc1d1	tisíciletá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
katastrofální	katastrofální	k2eAgFnSc7d1	katastrofální
povodní	povodeň	k1gFnSc7	povodeň
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgFnSc2d1	celá
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
domů	dům	k1gInPc2	dům
bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
nebo	nebo	k8xC	nebo
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
porušené	porušený	k2eAgFnSc2d1	porušená
statiky	statika	k1gFnSc2	statika
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
poničeno	poničit	k5eAaPmNgNnS	poničit
tornádem	tornádo	k1gNnSc7	tornádo
(	(	kIx(	(
<g/>
stupeň	stupeň	k1gInSc1	stupeň
F	F	kA	F
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavebních	stavební	k2eAgFnPc6d1	stavební
pracích	práce	k1gFnPc6	práce
odkryli	odkrýt	k5eAaPmAgMnP	odkrýt
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
archeologové	archeolog	k1gMnPc1	archeolog
na	na	k7c6	na
náměsti	náměst	k1gFnSc6	náměst
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
funkční	funkční	k2eAgInSc1d1	funkční
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
Nečízem	Nečíz	k1gInSc7	Nečíz
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
ramen	rameno	k1gNnPc2	rameno
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
teče	téct	k5eAaImIp3nS	téct
pod	pod	k7c7	pod
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Dochovala	dochovat	k5eAaPmAgFnS	dochovat
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
původní	původní	k2eAgFnSc1d1	původní
dlažba	dlažba	k1gFnSc1	dlažba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
8,5	[number]	k4	8,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
datují	datovat	k5eAaImIp3nP	datovat
vznik	vznik	k1gInSc4	vznik
mostu	most	k1gInSc2	most
do	do	k7c2	do
přelomu	přelom	k1gInSc2	přelom
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
a	a	k8xC	a
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
za	za	k7c4	za
Litovel	Litovel	k1gFnSc4	Litovel
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgInP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Litovli	Litovel	k1gFnSc6	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
stavba	stavba	k1gFnSc1	stavba
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc1	sídlo
rychtáře	rychtář	k1gMnSc2	rychtář
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
panské	panský	k2eAgNnSc4d1	panské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1557	[number]	k4	1557
ji	on	k3xPp3gFnSc4	on
odkoupilo	odkoupit	k5eAaPmAgNnS	odkoupit
město	město	k1gNnSc1	město
a	a	k8xC	a
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
zde	zde	k6eAd1	zde
radnici	radnice	k1gFnSc3	radnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1572	[number]	k4	1572
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
72	[number]	k4	72
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc1d1	stojící
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
ramenem	rameno	k1gNnSc7	rameno
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
mostem	most	k1gInSc7	most
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jej	on	k3xPp3gMnSc4	on
Václav	Václav	k1gMnSc1	Václav
Render	Render	k1gMnSc1	Render
roku	rok	k1gInSc2	rok
1724	[number]	k4	1724
<g/>
.	.	kIx.	.
</s>
<s>
Morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
zdobí	zdobit	k5eAaImIp3nS	zdobit
plastiky	plastika	k1gFnPc4	plastika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Pavlíny	Pavlína	k1gFnPc1	Pavlína
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rozálie	Rozálie	k1gFnSc2	Rozálie
a	a	k8xC	a
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc4	Šebestián
a	a	k8xC	a
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
opraveny	opraven	k2eAgFnPc1d1	opravena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
kámen	kámen	k1gInSc1	kámen
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1691	[number]	k4	1691
<g/>
,	,	kIx,	,
zasazený	zasazený	k2eAgInSc1d1	zasazený
do	do	k7c2	do
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Rameno	rameno	k1gNnSc1	rameno
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
protékající	protékající	k2eAgFnSc4d1	protékající
jádrem	jádro	k1gNnSc7	jádro
města	město	k1gNnSc2	město
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
s	s	k7c7	s
vícekrát	vícekrát	k6eAd1	vícekrát
upravovaným	upravovaný	k2eAgNnSc7d1	upravované
korytem	koryto	k1gNnSc7	koryto
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zčásti	zčásti	k6eAd1	zčásti
zatrubněné	zatrubněný	k2eAgNnSc1d1	zatrubněný
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
atraktivit	atraktivita	k1gFnPc2	atraktivita
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
vodní	vodní	k2eAgFnSc1d1	vodní
hladina	hladina	k1gFnSc1	hladina
Nečízu	Nečíz	k1gInSc2	Nečíz
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
směřují	směřovat	k5eAaImIp3nP	směřovat
schody	schod	k1gInPc4	schod
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
plochy	plocha	k1gFnSc2	plocha
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
gymnázium	gymnázium	k1gNnSc1	gymnázium
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
novorenesančního	novorenesanční	k2eAgInSc2d1	novorenesanční
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
profesorem	profesor	k1gMnSc7	profesor
brněnské	brněnský	k2eAgFnSc2d1	brněnská
techniky	technika	k1gFnSc2	technika
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Josefem	Josef	k1gMnSc7	Josef
Bertlem	Bertl	k1gMnSc7	Bertl
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
ve	v	k7c6	v
dnech	den	k1gInPc6	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
František	františek	k1gInSc1	františek
Nerada	nerad	k2eAgFnSc1d1	nerada
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
článku	článek	k1gInSc6	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
"	"	kIx"	"
<g/>
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
výzdoba	výzdoba	k1gFnSc1	výzdoba
budovy	budova	k1gFnSc2	budova
školy	škola	k1gFnSc2	škola
reálné	reálný	k2eAgNnSc1d1	reálné
<g/>
"	"	kIx"	"
budovu	budova	k1gFnSc4	budova
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nad	nad	k7c7	nad
hlavním	hlavní	k2eAgInSc7d1	hlavní
vchodem	vchod	k1gInSc7	vchod
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
prvním	první	k4xOgNnSc7	první
poschodím	poschodí	k1gNnSc7	poschodí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
poprsí	poprsí	k1gNnSc4	poprsí
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
zemí	zem	k1gFnPc2	zem
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
nad	nad	k7c7	nad
druhým	druhý	k4xOgNnSc7	druhý
poschodím	poschodí	k1gNnSc7	poschodí
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
plastických	plastický	k2eAgFnPc2d1	plastická
ozdob	ozdoba	k1gFnPc2	ozdoba
znázorňujících	znázorňující	k2eAgInPc2d1	znázorňující
střídavě	střídavě	k6eAd1	střídavě
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
haluze	haluz	k1gFnPc1	haluz
lípové	lípový	k2eAgFnPc1d1	Lípová
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgNnPc6d1	zvláštní
polích	pole	k1gNnPc6	pole
obrazy	obraz	k1gInPc1	obraz
květin	květina	k1gFnPc2	květina
význačných	význačný	k2eAgFnPc2d1	význačná
pro	pro	k7c4	pro
Litovelsko	Litovelsko	k1gNnSc4	Litovelsko
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
hlavní	hlavní	k2eAgFnSc1d1	hlavní
výzdoba	výzdoba	k1gFnSc1	výzdoba
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
velkých	velký	k2eAgInPc6d1	velký
obrazech	obraz	k1gInPc6	obraz
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
průčelí	průčelí	k1gNnSc6	průčelí
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
nápisy	nápis	k1gInPc1	nápis
vystihujícími	vystihující	k2eAgFnPc7d1	vystihující
jejich	jejich	k3xOp3gInSc4	jejich
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
po	po	k7c6	po
poprsí	poprsí	k1gNnSc6	poprsí
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
4	[number]	k4	4
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
metr	metr	k1gInSc1	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Provedeny	provést	k5eAaPmNgFnP	provést
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
všecky	všecek	k3xTgFnPc4	všecek
obrazové	obrazový	k2eAgFnPc4d1	obrazová
ozdoby	ozdoba	k1gFnPc4	ozdoba
venku	venku	k6eAd1	venku
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
barevné	barevný	k2eAgFnSc6d1	barevná
pálené	pálený	k2eAgFnSc6d1	pálená
hlíně	hlína	k1gFnSc6	hlína
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
sklonovinou	sklonovina	k1gFnSc7	sklonovina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
vzdorovaly	vzdorovat	k5eAaImAgFnP	vzdorovat
počasí	počasí	k1gNnSc4	počasí
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
nezničitelnými	zničitelný	k2eNgMnPc7d1	nezničitelný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mozaiky	mozaika	k1gFnPc1	mozaika
na	na	k7c4	na
průčelí	průčelí	k1gNnSc4	průčelí
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
povyšuje	povyšovat	k5eAaImIp3nS	povyšovat
Litovel	Litovel	k1gFnSc1	Litovel
na	na	k7c4	na
trhovou	trhový	k2eAgFnSc4d1	trhová
<g />
.	.	kIx.	.
</s>
<s>
ves	ves	k1gFnSc1	ves
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1291	[number]	k4	1291
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
udílí	udílet	k5eAaImIp3nS	udílet
Litovli	Litovel	k1gFnSc3	Litovel
městská	městský	k2eAgFnSc1d1	městská
práva	práv	k2eAgFnSc1d1	práva
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1327	[number]	k4	1327
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
Litovle	Litovel	k1gFnSc2	Litovel
Génius	génius	k1gMnSc1	génius
Slovanstva	Slovanstvo	k1gNnSc2	Slovanstvo
žehná	žehnat	k5eAaImIp3nS	žehnat
obrozené	obrozený	k2eAgFnSc3d1	obrozená
Litovli	Litovel	k1gFnSc3	Litovel
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
Památník	památník	k1gInSc1	památník
dobytí	dobytí	k1gNnSc2	dobytí
Litovle	Litovel	k1gFnSc2	Litovel
Švédy	švéda	k1gFnSc2	švéda
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
roku	rok	k1gInSc2	rok
1652	[number]	k4	1652
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zazděny	zazděn	k2eAgFnPc1d1	zazděna
kamenné	kamenný	k2eAgFnPc4d1	kamenná
dělové	dělový	k2eAgFnPc4d1	dělová
koule	koule	k1gFnPc4	koule
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
německého	německý	k2eAgInSc2d1	německý
nápisu	nápis	k1gInSc2	nápis
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Léta	léto	k1gNnPc1	léto
páně	páně	k2eAgInSc2d1	páně
1643	[number]	k4	1643
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
hlavní	hlavní	k2eAgFnSc1d1	hlavní
švédská	švédský	k2eAgFnSc1d1	švédská
armáda	armáda	k1gFnSc1	armáda
město	město	k1gNnSc1	město
Litovel	Litovel	k1gFnSc1	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
prudce	prudko	k6eAd1	prudko
ostřelovala	ostřelovat	k5eAaImAgFnS	ostřelovat
město	město	k1gNnSc4	město
a	a	k8xC	a
pak	pak	k6eAd1	pak
ho	on	k3xPp3gNnSc4	on
dobyla	dobýt	k5eAaPmAgFnS	dobýt
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vyhodila	vyhodit	k5eAaPmAgFnS	vyhodit
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
tři	tři	k4xCgFnPc4	tři
vysoké	vysoký	k2eAgFnPc4d1	vysoká
a	a	k8xC	a
pevné	pevný	k2eAgFnPc4d1	pevná
věže	věž	k1gFnPc4	věž
i	i	k9	i
s	s	k7c7	s
oběma	dva	k4xCgFnPc7	dva
branami	brána	k1gFnPc7	brána
<g/>
,	,	kIx,	,
rozmetala	rozmetat	k5eAaPmAgFnS	rozmetat
hradební	hradební	k2eAgFnSc1d1	hradební
předprseň	předprseň	k1gFnSc1	předprseň
<g/>
,	,	kIx,	,
zbořila	zbořit	k5eAaPmAgFnS	zbořit
příkop	příkop	k1gInSc4	příkop
i	i	k8xC	i
val	val	k1gInSc4	val
a	a	k8xC	a
v	v	k7c6	v
nepřátelské	přátelský	k2eNgFnSc6d1	nepřátelská
zvůli	zvůle	k1gFnSc6	zvůle
žalostně	žalostně	k6eAd1	žalostně
zničila	zničit	k5eAaPmAgFnS	zničit
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svatojánský	svatojánský	k2eAgInSc4d1	svatojánský
most	most	k1gInSc4	most
v	v	k7c6	v
Litovli	Litovel	k1gFnSc6	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
byl	být	k5eAaImAgInS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
mostů	most	k1gInPc2	most
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
nejstarším	starý	k2eAgNnSc6d3	nejstarší
na	na	k7c6	na
území	území	k1gNnSc6	území
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
mostu	most	k1gInSc2	most
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
odkryt	odkryt	k2eAgInSc1d1	odkryt
ještě	ještě	k6eAd1	ještě
starší	starý	k2eAgInSc1d2	starší
most	most	k1gInSc1	most
přes	přes	k7c4	přes
náhon	náhon	k1gInSc4	náhon
Nečíz	Nečíza	k1gFnPc2	Nečíza
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nové	Nové	k2eAgInPc1d1	Nové
Zámky	zámek	k1gInPc1	zámek
u	u	k7c2	u
Litovle	Litovel	k1gFnSc2	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
Zámky	zámek	k1gInPc1	zámek
jsou	být	k5eAaImIp3nP	být
barokní	barokní	k2eAgInSc4d1	barokní
zámek	zámek	k1gInSc4	zámek
postavený	postavený	k2eAgInSc4d1	postavený
Lichtenštejny	Lichtenštejn	k1gInPc4	Lichtenštejn
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Litovle	Litovel	k1gFnSc2	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
ústav	ústav	k1gInSc1	ústav
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
zámku	zámek	k1gInSc2	zámek
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
vnější	vnější	k2eAgFnPc1d1	vnější
neporušené	porušený	k2eNgFnPc1d1	neporušená
empírové	empírový	k2eAgFnPc1d1	empírová
fasády	fasáda	k1gFnPc1	fasáda
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Filipa	Filip	k1gMnSc2	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Marka	Marek	k1gMnSc2	Marek
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc4d1	svatý
Jiří	Jiří	k1gMnSc4	Jiří
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
:	:	kIx,	:
Litovel	Litovel	k1gFnSc1	Litovel
předměstí	předměstí	k1gNnSc2	předměstí
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
273	[number]	k4	273
<g/>
,	,	kIx,	,
274	[number]	k4	274
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litovel	Litovel	k1gFnSc4	Litovel
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
273	[number]	k4	273
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litovel	Litovel	k1gFnSc1	Litovel
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
<g/>
:	:	kIx,	:
273	[number]	k4	273
<g/>
)	)	kIx)	)
autobusové	autobusový	k2eAgNnSc1d1	autobusové
stanoviště	stanoviště	k1gNnSc1	stanoviště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
železniční	železniční	k2eAgFnSc2d1	železniční
zastávky	zastávka	k1gFnSc2	zastávka
Litovel	Litovel	k1gFnSc1	Litovel
město	město	k1gNnSc1	město
silnice	silnice	k1gFnSc2	silnice
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
Litovli	Litovel	k1gFnSc6	Litovel
<g/>
:	:	kIx,	:
449,447,635	[number]	k4	449,447,635
D35	D35	k1gFnPc2	D35
směr	směr	k1gInSc4	směr
Olomouc	Olomouc	k1gFnSc4	Olomouc
a	a	k8xC	a
Mohlenice	Mohlenice	k1gFnPc4	Mohlenice
V	v	k7c6	v
Litovli	Litovel	k1gFnSc6	Litovel
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nS	vařit
stejnojmenné	stejnojmenný	k2eAgNnSc1d1	stejnojmenné
pivo	pivo	k1gNnSc1	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Papcel	Papcet	k5eAaPmAgMnS	Papcet
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Litovel	Litovel	k1gFnSc1	Litovel
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
technologická	technologický	k2eAgNnPc4d1	Technologické
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
papírenský	papírenský	k2eAgInSc4d1	papírenský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Tři	tři	k4xCgInPc4	tři
Dvory	Dvůr	k1gInPc4	Dvůr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
výrobna	výrobna	k1gFnSc1	výrobna
těstovin	těstovina	k1gFnPc2	těstovina
značky	značka	k1gFnSc2	značka
Adriana	Adriana	k1gFnSc1	Adriana
a	a	k8xC	a
výrobna	výrobna	k1gFnSc1	výrobna
sýru	sýr	k1gInSc2	sýr
značky	značka	k1gFnSc2	značka
Grand	grand	k1gMnSc1	grand
Moravia	Moravia	k1gFnSc1	Moravia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Nasobůrky	Nasobůrka	k1gFnSc2	Nasobůrka
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
minerální	minerální	k2eAgFnPc1d1	minerální
vody	voda	k1gFnPc1	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Saguaro	Saguara	k1gFnSc5	Saguara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SEV	SEV	kA	SEV
Litovel	Litovel	k1gFnSc1	Litovel
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
následník	následník	k1gMnSc1	následník
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Tesly	Tesla	k1gFnSc2	Tesla
Litovel	Litovel	k1gFnSc1	Litovel
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
světový	světový	k2eAgMnSc1d1	světový
výrobce	výrobce	k1gMnSc1	výrobce
gramofonů	gramofon	k1gInPc2	gramofon
cukrovar	cukrovar	k1gInSc1	cukrovar
Litovel	Litovel	k1gFnSc1	Litovel
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
11	[number]	k4	11
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Březové	březový	k2eAgFnSc3d1	Březová
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
60	[number]	k4	60
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
186	[number]	k4	186
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Chudobín	Chudobín	k1gInSc1	Chudobín
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
80	[number]	k4	80
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
227	[number]	k4	227
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Litovel	Litovel	k1gFnSc1	Litovel
(	(	kIx(	(
<g/>
1	[number]	k4	1
120	[number]	k4	120
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
6	[number]	k4	6
915	[number]	k4	915
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
osada	osada	k1gFnSc1	osada
Chořelice	Chořelice	k1gFnSc1	Chořelice
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1975	[number]	k4	1975
<g/>
;	;	kIx,	;
78	[number]	k4	78
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
307	[number]	k4	307
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Myslechovice	Myslechovice	k1gFnSc1	Myslechovice
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
115	[number]	k4	115
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
318	[number]	k4	318
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Nasobůrky	Nasobůrka	k1gFnPc1	Nasobůrka
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
152	[number]	k4	152
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
465	[number]	k4	465
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
82	[number]	k4	82
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
216	[number]	k4	216
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Rozvadovice	Rozvadovice	k1gFnSc1	Rozvadovice
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
64	[number]	k4	64
domů	dům	k1gInPc2	dům
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
215	[number]	k4	215
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Savín	Savín	k1gMnSc1	Savín
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
2001	[number]	k4	2001
<g/>
;	;	kIx,	;
63	[number]	k4	63
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
145	[number]	k4	145
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Tři	tři	k4xCgInPc4	tři
Dvory	Dvůr	k1gInPc4	Dvůr
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1976	[number]	k4	1976
<g/>
;	;	kIx,	;
92	[number]	k4	92
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
219	[number]	k4	219
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Unčovice	Unčovice	k1gFnSc1	Unčovice
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
124	[number]	k4	124
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
407	[number]	k4	407
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Víska	víska	k1gFnSc1	víska
(	(	kIx(	(
<g/>
připojena	připojen	k2eAgFnSc1d1	připojena
1980	[number]	k4	1980
<g/>
;	;	kIx,	;
35	[number]	k4	35
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
99	[number]	k4	99
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
také	také	k9	také
obec	obec	k1gFnSc1	obec
Červenka	červenka	k1gFnSc1	červenka
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
také	také	k9	také
obce	obec	k1gFnPc1	obec
Haňovice	Haňovice	k1gFnSc2	Haňovice
a	a	k8xC	a
Pňovice	Pňovice	k1gFnSc2	Pňovice
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
tady	tady	k6eAd1	tady
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
zápasník	zápasník	k1gMnSc1	zápasník
Gustav	Gustav	k1gMnSc1	Gustav
Frištenský	Frištenský	k2eAgMnSc1d1	Frištenský
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kulka	kulka	k1gFnSc1	kulka
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
Rudolf	Rudolf	k1gMnSc1	Rudolf
Wedra	Wedra	k1gMnSc1	Wedra
-	-	kIx~	-
rakouský	rakouský	k2eAgMnSc1d1	rakouský
politik	politik	k1gMnSc1	politik
MUDr.	MUDr.	kA	MUDr.
Alena	Alena	k1gFnSc1	Alena
Šromová	Šromová	k1gFnSc1	Šromová
-	-	kIx~	-
lékařka	lékařka	k1gFnSc1	lékařka
a	a	k8xC	a
senátorka	senátorka	k1gFnSc1	senátorka
Samospráva	samospráva	k1gFnSc1	samospráva
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pravidelně	pravidelně	k6eAd1	pravidelně
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
zlato-červenou	zlato-červený	k2eAgFnSc4d1	zlato-červená
moravskou	moravský	k2eAgFnSc4d1	Moravská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
-	-	kIx~	-
MVDr.	MVDr.	kA	MVDr.
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Grézl	Grézl	k1gMnSc1	Grézl
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
-	-	kIx~	-
dosud	dosud	k6eAd1	dosud
Ing.	ing.	kA	ing.
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Potužák	Potužák	k1gMnSc1	Potužák
Revúca	Revúca	k1gMnSc1	Revúca
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Littau	Littaus	k1gInSc2	Littaus
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Wieliczka	Wieliczka	k1gFnSc1	Wieliczka
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
