<s>
Litovel	Litovel	k1gFnSc1	Litovel
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Littau	Littaus	k1gInSc3	Littaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Olomouce	Olomouc	k1gFnSc2	Olomouc
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
jejíchž	jejíž	k3xOyRp3gMnPc2	jejíž
šest	šest	k4xCc1	šest
ramen	rameno	k1gNnPc2	rameno
dodává	dodávat	k5eAaImIp3nS	dodávat
Litovli	Litovel	k1gFnSc3	Litovel
specifický	specifický	k2eAgInSc4d1	specifický
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
