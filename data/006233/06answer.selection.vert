<s>
Standardní	standardní	k2eAgInSc1d1
makrobiotický	makrobiotický	k2eAgInSc1d1
talíř	talíř	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
z	z	k7c2
50	[number]	k4
%	%	kIx~
celozrnné	celozrnný	k2eAgFnSc2d1
obilniny	obilnina	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
30	[number]	k4
%	%	kIx~
zelenina	zelenina	k1gFnSc1
(	(	kIx(
<g/>
hlavně	hlavně	k9
tepelně	tepelně	k6eAd1
upravená	upravený	k2eAgFnSc1d1
a	a	k8xC
kvašená	kvašený	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
15	[number]	k4
%	%	kIx~
luštěniny	luštěnina	k1gFnPc1
a	a	k8xC
mořské	mořský	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
5	[number]	k4
%	%	kIx~
polévky	polévka	k1gFnPc4
<g/>
.	.	kIx.
</s>