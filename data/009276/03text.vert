<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Keni	Keňa	k1gFnSc2	Keňa
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijata	přijat	k2eAgFnSc1d1	přijata
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
barevných	barevný	k2eAgInPc2d1	barevný
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
černého	černé	k1gNnSc2	černé
<g/>
,	,	kIx,	,
červeného	červené	k1gNnSc2	červené
a	a	k8xC	a
zeleného	zelené	k1gNnSc2	zelené
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
bílé	bílý	k2eAgFnPc1d1	bílá
proužky	proužka	k1gFnPc1	proužka
(	(	kIx(	(
<g/>
lemy	lem	k1gInPc1	lem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
znamená	znamenat	k5eAaImIp3nS	znamenat
svobodu	svoboda	k1gFnSc4	svoboda
milující	milující	k2eAgFnSc4d1	milující
africké	africký	k2eAgNnSc1d1	africké
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
jeho	jeho	k3xOp3gFnSc4	jeho
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
boj	boj	k1gInSc4	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
hustou	hustý	k2eAgFnSc4d1	hustá
vegetaci	vegetace	k1gFnSc4	vegetace
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
proužky	proužka	k1gFnPc1	proužka
jsou	být	k5eAaImIp3nP	být
symbolem	symbol	k1gInSc7	symbol
míru	mír	k1gInSc2	mír
a	a	k8xC	a
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
červeno-bílo-černý	červenoílo-černý	k2eAgInSc1d1	červeno-bílo-černý
štít	štít	k1gInSc1	štít
masajských	masajský	k2eAgMnPc2d1	masajský
válečníků	válečník	k1gMnPc2	válečník
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
dva	dva	k4xCgInPc1	dva
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
zkřížené	zkřížený	k2eAgInPc1d1	zkřížený
oštěpy	oštěp	k1gInPc1	oštěp
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
obranu	obrana	k1gFnSc4	obrana
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnSc2	vlajka
keňských	keňský	k2eAgMnPc2d1	keňský
prezidentů	prezident	k1gMnPc2	prezident
==	==	k?	==
</s>
</p>
<p>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
vlajka	vlajka	k1gFnSc1	vlajka
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
není	být	k5eNaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
dosavadních	dosavadní	k2eAgMnPc2d1	dosavadní
prezidentů	prezident	k1gMnPc2	prezident
měl	mít	k5eAaImAgInS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Společným	společný	k2eAgInSc7d1	společný
prvkem	prvek	k1gInSc7	prvek
všech	všecek	k3xTgFnPc2	všecek
vlajek	vlajka	k1gFnPc2	vlajka
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
masajský	masajský	k2eAgInSc4d1	masajský
(	(	kIx(	(
<g/>
červeno-bílo-černý	červenoílo-černý	k2eAgInSc4d1	červeno-bílo-černý
<g/>
)	)	kIx)	)
štít	štít	k1gInSc4	štít
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
žluté	žlutý	k2eAgInPc4d1	žlutý
<g/>
)	)	kIx)	)
oštěpy	oštěp	k1gInPc4	oštěp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Keňský	keňský	k2eAgInSc1d1	keňský
znak	znak	k1gInSc1	znak
</s>
</p>
<p>
<s>
Keňská	keňský	k2eAgFnSc1d1	keňská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Keňská	keňský	k2eAgFnSc1d1	keňská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
