<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Keni	Keňa	k1gFnSc2	Keňa
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijata	přijat	k2eAgFnSc1d1	přijata
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
barevných	barevný	k2eAgInPc2d1	barevný
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
černého	černé	k1gNnSc2	černé
<g/>
,	,	kIx,	,
červeného	červené	k1gNnSc2	červené
a	a	k8xC	a
zeleného	zelené	k1gNnSc2	zelené
<g/>
.	.	kIx.	.
</s>
