<s>
Caracas	Caracas	k1gMnSc1
</s>
<s>
Santiago	Santiago	k1gNnSc1
de	de	k?
León	León	k1gMnSc1
de	de	k?
Caracas	Caracas	k1gMnSc1
Santiago	Santiago	k1gNnSc1
de	de	k?
León	León	k1gMnSc1
de	de	k?
Caracas	Caracas	k1gInSc1
Pohled	pohled	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
66	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
900	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Venezuela	Venezuela	k1gFnSc1
Venezuela	Venezuela	k1gFnSc1
Administrativní	administrativní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc4
</s>
<s>
5	#num#	k4
obcí	obec	k1gFnPc2
</s>
<s>
Caracas	Caracas	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
2	#num#	k4
050	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
5	#num#	k4
174	#num#	k4
034	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1496	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Helen	Helena	k1gFnPc2
Fernández	Fernándeza	k1gFnPc2
Vznik	vznik	k1gInSc1
</s>
<s>
1567	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.alcaldiametropolitana.gob.ve/portal/	www.alcaldiametropolitana.gob.ve/portal/	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
0212	#num#	k4
PSČ	PSČ	kA
</s>
<s>
1010-A	1010-A	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Caracas	Caracas	k1gInSc1
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Santiago	Santiago	k1gNnSc1
de	de	k?
Léon	Léon	k1gMnSc1
de	de	k?
Caracas	Caracas	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
jihoamerického	jihoamerický	k2eAgInSc2d1
státu	stát	k1gInSc2
Venezuela	Venezuela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
situováno	situován	k2eAgNnSc1d1
mezi	mezi	k7c7
několika	několik	k4yIc7
údolími	údolí	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
ve	v	k7c6
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
900	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nepřehlédnutelné	přehlédnutelný	k2eNgFnPc4d1
dominanty	dominanta	k1gFnPc4
města	město	k1gNnSc2
patří	patřit	k5eAaImIp3nP
futuristické	futuristický	k2eAgInPc1d1
mrakodrapy	mrakodrap	k1gInPc1
společně	společně	k6eAd1
s	s	k7c7
mnohaproudými	mnohaproudý	k2eAgFnPc7d1
dálnicemi	dálnice	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
stojí	stát	k5eAaImIp3nP
v	v	k7c6
přímém	přímý	k2eAgInSc6d1
kontrastu	kontrast	k1gInSc6
k	k	k7c3
množství	množství	k1gNnSc3
chudinských	chudinský	k2eAgInPc2d1
slumů	slum	k1gInPc2
(	(	kIx(
<g/>
ve	v	k7c6
Venezuele	Venezuela	k1gFnSc6
zvaných	zvaný	k2eAgFnPc2d1
barrios	barriosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
lemují	lemovat	k5eAaImIp3nP
okolí	okolí	k1gNnSc4
města	město	k1gNnSc2
mj.	mj.	kA
podél	podél	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
dálnic	dálnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
aglomerace	aglomerace	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
Distrito	Distrita	k1gFnSc5
capital	capitat	k5eAaImAgInS,k5eAaPmAgInS
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
spolkového	spolkový	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgFnPc2d1
čtyř	čtyři	k4xCgFnPc2
obcí	obec	k1gFnPc2
v	v	k7c6
sousedním	sousední	k2eAgInSc6d1
spolkovém	spolkový	k2eAgInSc6d1
státě	stát	k1gInSc6
Miranda	Mirando	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Venezuely	Venezuela	k1gFnSc2
je	být	k5eAaImIp3nS
typické	typický	k2eAgNnSc1d1
jihoamerické	jihoamerický	k2eAgNnSc1d1
velkoměsto	velkoměsto	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
zajímavé	zajímavý	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
depresivní	depresivní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caracas	Caracas	k1gMnSc1
má	mít	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
vymoženosti	vymoženost	k1gFnPc4
velkoměsta	velkoměsto	k1gNnSc2
–	–	k?
výškové	výškový	k2eAgInPc4d1
obytné	obytný	k2eAgInPc4d1
domy	dům	k1gInPc4
pro	pro	k7c4
střední	střední	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
<g/>
,	,	kIx,
luxusní	luxusní	k2eAgNnSc4d1
vilové	vilový	k2eAgFnPc4d1
čtvrti	čtvrt	k1gFnPc4
<g/>
,	,	kIx,
restaurace	restaurace	k1gFnPc4
a	a	k8xC
hotely	hotel	k1gInPc4
<g/>
,	,	kIx,
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
noční	noční	k2eAgInPc1d1
bary	bar	k1gInPc1
a	a	k8xC
nákupní	nákupní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
se	se	k3xPyFc4
ale	ale	k9
již	již	k6eAd1
dlouho	dlouho	k6eAd1
potýká	potýkat	k5eAaImIp3nS
s	s	k7c7
mnoha	mnoho	k4c7
problémy	problém	k1gInPc7
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
chudoba	chudoba	k1gFnSc1
<g/>
,	,	kIx,
kriminalita	kriminalita	k1gFnSc1
a	a	k8xC
znečištění	znečištění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
velké	velká	k1gFnSc3
kriminalitě	kriminalita	k1gFnSc3
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
obytných	obytný	k2eAgInPc2d1
komplexů	komplex	k1gInPc2
pro	pro	k7c4
zámožnější	zámožný	k2eAgFnPc4d2
vrstvy	vrstva	k1gFnPc4
obehnáno	obehnat	k5eAaPmNgNnS
vysokými	vysoký	k2eAgInPc7d1
ploty	plot	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Caracas	Caracas	k1gMnSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
s	s	k7c7
dopravními	dopravní	k2eAgFnPc7d1
zácpami	zácpa	k1gFnPc7
a	a	k8xC
progresivní	progresivní	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
centrem	centr	k1gInSc7
politického	politický	k2eAgInSc2d1
<g/>
,	,	kIx,
vědeckého	vědecký	k2eAgInSc2d1
<g/>
,	,	kIx,
kulturního	kulturní	k2eAgInSc2d1
<g/>
,	,	kIx,
intelektuálního	intelektuální	k2eAgInSc2d1
a	a	k8xC
vzdělávacího	vzdělávací	k2eAgInSc2d1
života	život	k1gInSc2
ve	v	k7c6
Venezuele	Venezuela	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
ukázkovým	ukázkový	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
moderní	moderní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
doplněna	doplněn	k2eAgFnSc1d1
sochami	socha	k1gFnPc7
<g/>
,	,	kIx,
mozaikami	mozaika	k1gFnPc7
a	a	k8xC
freskami	freska	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
výhodné	výhodný	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
na	na	k7c6
karibském	karibský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
byl	být	k5eAaImAgMnS
a	a	k8xC
je	být	k5eAaImIp3nS
Caracas	Caracas	k1gInSc1
-	-	kIx~
prostřednictvím	prostřednictvím	k7c2
mezinárodního	mezinárodní	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
a	a	k8xC
přístavu	přístav	k1gInSc2
u	u	k7c2
města	město	k1gNnSc2
La	la	k1gNnSc2
Guaira	Guairo	k1gNnSc2
-	-	kIx~
vstupní	vstupní	k2eAgFnSc7d1
branou	brána	k1gFnSc7
do	do	k7c2
And	Anda	k1gFnPc2
a	a	k8xC
Amazonie	Amazonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
města	město	k1gNnSc2
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
z	z	k7c2
El	Ela	k1gFnPc2
Calvario	Calvario	k6eAd1
</s>
<s>
Caracas	Caracas	k1gInSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
20	#num#	k4
km	km	kA
dlouhým	dlouhý	k2eAgMnSc7d1
a	a	k8xC
poměrně	poměrně	k6eAd1
úzkým	úzký	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
na	na	k7c6
náhorní	náhorní	k2eAgFnSc6d1
planině	planina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
údolí	údolí	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
lemováno	lemován	k2eAgNnSc1d1
zeleným	zelený	k2eAgInSc7d1
a	a	k8xC
neobydleným	obydlený	k2eNgInSc7d1
národním	národní	k2eAgInSc7d1
parkem	park	k1gInSc7
El	Ela	k1gFnPc2
Ávila	Ávilo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jih	jih	k1gInSc1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
zastavěn	zastavět	k5eAaPmNgInS
moderními	moderní	k2eAgNnPc7d1
předměstími	předměstí	k1gNnPc7
a	a	k8xC
také	také	k9
pochybnými	pochybný	k2eAgNnPc7d1
městečky	městečko	k1gNnPc7
z	z	k7c2
chatrčí	chatrč	k1gFnPc2
ležícími	ležící	k2eAgFnPc7d1
na	na	k7c4
úpatí	úpatí	k1gNnSc4
kopců	kopec	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
barrios	barrios	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
Caracasu	Caracas	k1gInSc2
je	být	k5eAaImIp3nS
dlouhé	dlouhý	k2eAgNnSc4d1
8	#num#	k4
km	km	kA
<g/>
,	,	kIx,
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
El	Ela	k1gFnPc2
Silenciu	Silencium	k1gNnSc6
a	a	k8xC
končí	končit	k5eAaImIp3nS
v	v	k7c6
Chacau	Chacaus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnPc1d1
historické	historický	k2eAgFnPc1d1
čtvrti	čtvrt	k1gFnPc1
(	(	kIx(
<g/>
popř.	popř.	kA
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
zbylo	zbýt	k5eAaPmAgNnS
<g/>
)	)	kIx)
zabírají	zabírat	k5eAaImIp3nP
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
centrálního	centrální	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Caracas	Caracas	k1gMnSc1
lze	lze	k6eAd1
navštívit	navštívit	k5eAaPmF
v	v	k7c6
kterékoliv	kterýkoliv	k3yIgFnSc6
roční	roční	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnebí	podnebí	k1gNnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
suché	suchý	k2eAgNnSc1d1
<g/>
,	,	kIx,
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
od	od	k7c2
června	červen	k1gInSc2
do	do	k7c2
října	říjen	k1gInSc2
panuje	panovat	k5eAaImIp3nS
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
částech	část	k1gFnPc6
Venezuely	Venezuela	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
města	město	k1gNnSc2
příliš	příliš	k6eAd1
nedotýká	dotýkat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teploty	teplota	k1gFnPc1
jsou	být	k5eAaImIp3nP
příjemné	příjemný	k2eAgFnPc1d1
a	a	k8xC
stálé	stálý	k2eAgFnPc1d1
<g/>
,	,	kIx,
celoroční	celoroční	k2eAgInSc1d1
průměr	průměr	k1gInSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
25	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Srážky	srážka	k1gFnSc2
ani	ani	k8xC
v	v	k7c6
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
zpravidla	zpravidla	k6eAd1
nepřesáhnou	přesáhnout	k5eNaPmIp3nP
100	#num#	k4
mm	mm	kA
za	za	k7c4
jeden	jeden	k4xCgInSc4
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Založení	založení	k1gNnSc1
města	město	k1gNnSc2
</s>
<s>
Počátky	počátek	k1gInPc1
města	město	k1gNnSc2
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1560	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
španělský	španělský	k2eAgMnSc1d1
cestovatel	cestovatel	k1gMnSc1
Francisco	Francisco	k1gMnSc1
Fajardo	Fajardo	k1gNnSc4
zamířil	zamířit	k5eAaPmAgInS
směrem	směr	k1gInSc7
na	na	k7c4
jih	jih	k1gInSc4
ze	z	k7c2
španělské	španělský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Margarita	Margarita	k1gFnSc1
40	#num#	k4
km	km	kA
od	od	k7c2
pevniny	pevnina	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
objevil	objevit	k5eAaPmAgMnS
zelené	zelená	k1gFnPc4
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
zastavěno	zastavět	k5eAaPmNgNnS
budovami	budova	k1gFnPc7
a	a	k8xC
silnicemi	silnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fajardo	Fajardo	k1gNnSc1
zde	zde	k6eAd1
založil	založit	k5eAaPmAgMnS
osadu	osada	k1gFnSc4
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
záhy	záhy	k6eAd1
zničena	zničit	k5eAaPmNgFnS
Indiány	Indián	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indiáni	Indián	k1gMnPc1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
útocích	útok	k1gInPc6
pokračovali	pokračovat	k5eAaImAgMnP
šest	šest	k4xCc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
španělský	španělský	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
nedal	dát	k5eNaPmAgMnS
pokyn	pokyn	k1gInSc4
k	k	k7c3
dobytí	dobytí	k1gNnSc3
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1567	#num#	k4
guvernérovo	guvernérův	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vyhnalo	vyhnat	k5eAaPmAgNnS
poslední	poslední	k2eAgInSc4d1
Indiány	Indián	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gFnPc2
Francisco	Francisco	k6eAd1
bylo	být	k5eAaImAgNnS
obnoveno	obnovit	k5eAaPmNgNnS
a	a	k8xC
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
„	„	k?
<g/>
Santiago	Santiago	k1gNnSc4
de	de	k?
León	León	k1gMnSc1
de	de	k?
Caracas	Caracas	k1gMnSc1
<g/>
“	“	k?
<g/>
:	:	kIx,
„	„	k?
<g/>
Santiago	Santiago	k1gNnSc4
<g/>
“	“	k?
po	po	k7c6
patronovi	patronův	k2eAgMnPc1d1
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
León	León	k1gInSc1
<g/>
“	“	k?
po	po	k7c6
guvernérovi	guvernér	k1gMnSc6
provincie	provincie	k1gFnSc2
a	a	k8xC
„	„	k?
<g/>
Caracas	Caracas	k1gInSc1
<g/>
“	“	k?
za	za	k7c4
odměnu	odměna	k1gFnSc4
nejméně	málo	k6eAd3
problémovým	problémový	k2eAgMnPc3d1
Indiánům	Indián	k1gMnPc3
v	v	k7c6
této	tento	k3xDgFnSc6
pobřežní	pobřežní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Sídlem	sídlo	k1gNnSc7
administrativy	administrativa	k1gFnSc2
se	se	k3xPyFc4
Caracas	Caracas	k1gMnSc1
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1577	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
stal	stát	k5eAaPmAgMnS
třetím	třetí	k4xOgMnSc7
a	a	k8xC
posledním	poslední	k2eAgNnSc7d1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Venezuely	Venezuela	k1gFnSc2
(	(	kIx(
<g/>
první	první	k4xOgMnSc1
bylo	být	k5eAaImAgNnS
Coro	Coro	k1gNnSc1
v	v	k7c6
letech	let	k1gInPc6
1527	#num#	k4
<g/>
–	–	k?
<g/>
46	#num#	k4
a	a	k8xC
pak	pak	k6eAd1
El	Ela	k1gFnPc2
Tocuyo	Tocuyo	k6eAd1
1547	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
založení	založení	k1gNnSc6
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
Caracas	Caracas	k1gInSc4
domovem	domov	k1gInSc7
šedesáti	šedesát	k4xCc2
španělských	španělský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
a	a	k8xC
zaujímalo	zaujímat	k5eAaImAgNnS
25	#num#	k4
bloků	blok	k1gInPc2
kolem	kolem	k7c2
Plaza	plaz	k1gMnSc2
Mayor	Mayor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
a	a	k8xC
vyhlášení	vyhlášení	k1gNnSc1
nezávislosti	nezávislost	k1gFnSc2
</s>
<s>
Přes	přes	k7c4
další	další	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
ze	z	k7c2
strany	strana	k1gFnSc2
pirátů	pirát	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
město	město	k1gNnSc4
vyhladili	vyhladit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
přestože	přestože	k8xS
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
zničeno	zničen	k2eAgNnSc1d1
zemětřesením	zemětřesení	k1gNnSc7
<g/>
,	,	kIx,
Caracas	Caracas	k1gInSc1
odolal	odolat	k5eAaPmAgInS
a	a	k8xC
další	další	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
staletí	staletí	k1gNnPc4
byla	být	k5eAaImAgFnS
relativně	relativně	k6eAd1
klidná	klidný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
ve	v	k7c6
městě	město	k1gNnSc6
narodili	narodit	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
nejslavnější	slavný	k2eAgMnPc1d3
synové	syn	k1gMnPc1
Venezuely	Venezuela	k1gFnSc2
<g/>
:	:	kIx,
Francisco	Francisco	k6eAd1
Miranda	Miranda	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
bojoval	bojovat	k5eAaImAgInS
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
Venezuely	Venezuela	k1gFnSc2
na	na	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1750	#num#	k4
<g/>
;	;	kIx,
Simón	Simón	k1gInSc1
Bolívar	Bolívar	k1gInSc1
(	(	kIx(
<g/>
Osvoboditel	osvoboditel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
nakonec	nakonec	k6eAd1
nezávislost	nezávislost	k1gFnSc4
vybojoval	vybojovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1783	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miranda	Miranda	k1gFnSc1
zřídil	zřídit	k5eAaPmAgInS
v	v	k7c6
Caracasu	Caracas	k1gInSc6
nezávislou	závislý	k2eNgFnSc4d1
administrativu	administrativa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
fungovala	fungovat	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1810	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
a	a	k8xC
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
Mirandu	Mirand	k1gInSc2
zajala	zajmout	k5eAaPmAgNnP
španělská	španělský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
obsadila	obsadit	k5eAaPmAgFnS
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
spolubojovníci	spolubojovník	k1gMnPc1
včetně	včetně	k7c2
Bolívara	Bolívar	k1gMnSc2
byli	být	k5eAaImAgMnP
tak	tak	k9
rozzlobení	rozzlobený	k2eAgMnPc1d1
touto	tento	k3xDgFnSc7
kapitulací	kapitulace	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
Mirandu	Miranda	k1gFnSc4
předali	předat	k5eAaPmAgMnP
Španělům	Španěl	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
převezli	převézt	k5eAaPmAgMnP
Mirandu	Miranda	k1gFnSc4
do	do	k7c2
Cádizu	Cádiz	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
ve	v	k7c6
vězení	vězení	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Mirandově	Mirandův	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
revoluce	revoluce	k1gFnSc2
postavil	postavit	k5eAaPmAgInS
Bolívar	Bolívar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
porazit	porazit	k5eAaPmF
Španěly	Španěly	k1gInPc4
ve	v	k7c6
Venezuele	Venezuela	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
donucen	donucen	k2eAgMnSc1d1
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
do	do	k7c2
Kolumbie	Kolumbie	k1gFnSc2
a	a	k8xC
pak	pak	k6eAd1
na	na	k7c4
Jamajku	Jamajka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
přišla	přijít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnPc4
chvíle	chvíle	k1gFnPc4
k	k	k7c3
návratu	návrat	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
skončily	skončit	k5eAaPmAgFnP
napoleonské	napoleonský	k2eAgFnPc1d1
války	válka	k1gFnPc1
a	a	k8xC
Bolívarův	Bolívarův	k2eAgMnSc1d1
zmocněnec	zmocněnec	k1gMnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
mohl	moct	k5eAaImAgInS
vyzvednout	vyzvednout	k5eAaPmF
peníze	peníz	k1gInPc4
a	a	k8xC
zbraně	zbraň	k1gFnPc4
a	a	k8xC
neverbovat	verbovat	k5eNaImF
5	#num#	k4
000	#num#	k4
britských	britský	k2eAgMnPc2d1
veteránů	veterán	k1gMnPc2
Poloostrovní	poloostrovní	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
vojenskou	vojenský	k2eAgFnSc7d1
sílou	síla	k1gFnSc7
a	a	k8xC
s	s	k7c7
armádou	armáda	k1gFnSc7
jezdců	jezdec	k1gMnPc2
z	z	k7c2
Los	los	k1gInSc1
Llanos	Llanos	k1gInPc2
přešel	přejít	k5eAaPmAgInS
Bolívar	Bolívar	k1gInSc1
Andy	Anda	k1gFnSc2
<g/>
,	,	kIx,
porazil	porazit	k5eAaPmAgInS
španělskou	španělský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
přinesl	přinést	k5eAaPmAgInS
nezávislost	nezávislost	k1gFnSc4
do	do	k7c2
Kolumbie	Kolumbie	k1gFnSc2
a	a	k8xC
Venezuely	Venezuela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1824	#num#	k4
osvobodil	osvobodit	k5eAaPmAgInS
Bolívar	Bolívar	k1gInSc1
Ekvádor	Ekvádor	k1gInSc4
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc4
a	a	k8xC
Bolívii	Bolívie	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
dobyl	dobýt	k5eAaPmAgInS
více	hodně	k6eAd2
než	než	k8xS
pět	pět	k4xCc4
milionů	milion	k4xCgInPc2
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
území	území	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
byl	být	k5eAaImAgInS
ohromující	ohromující	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
počin	počin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
Bolívarův	Bolívarův	k2eAgInSc1d1
sen	sen	k1gInSc1
o	o	k7c6
spojené	spojený	k2eAgFnSc6d1
Velké	velký	k2eAgFnSc6d1
Kolumbii	Kolumbie	k1gFnSc6
(	(	kIx(
<g/>
Kolumbie	Kolumbie	k1gFnSc1
<g/>
,	,	kIx,
Venezuela	Venezuela	k1gFnSc1
a	a	k8xC
Ekvádor	Ekvádor	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
příliš	příliš	k6eAd1
ambiciózní	ambiciózní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
stát	stát	k1gInSc1
existoval	existovat	k5eAaImAgInS
pouze	pouze	k6eAd1
deset	deset	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
rozdělil	rozdělit	k5eAaPmAgInS
na	na	k7c4
tři	tři	k4xCgFnPc4
samostatné	samostatný	k2eAgFnPc4d1
země	zem	k1gFnPc4
a	a	k8xC
Osvoboditel	osvoboditel	k1gMnSc1
(	(	kIx(
<g/>
El	Ela	k1gFnPc2
Libertador	Libertadora	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
chudobě	chudoba	k1gFnSc6
roku	rok	k1gInSc2
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dvanáct	dvanáct	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
jeho	jeho	k3xOp3gMnPc1
krajané	krajan	k1gMnPc1
připustili	připustit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
mu	on	k3xPp3gMnSc3
dlužníky	dlužník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
byly	být	k5eAaImAgInP
převezeny	převézt	k5eAaPmNgInP
zpět	zpět	k6eAd1
ze	z	k7c2
Santa	Santo	k1gNnSc2
Marty	Marta	k1gFnSc2
v	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
a	a	k8xC
nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
uloženy	uložit	k5eAaPmNgFnP
v	v	k7c6
caracaském	caracaský	k2eAgInSc6d1
Národním	národní	k2eAgInSc6d1
Pantheonu	Pantheon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
boku	bok	k1gInSc6
je	být	k5eAaImIp3nS
prázdný	prázdný	k2eAgInSc1d1
hrob	hrob	k1gInSc1
čekající	čekající	k2eAgInSc1d1
na	na	k7c4
ostatky	ostatek	k1gInPc4
Francisca	Franciscum	k1gNnSc2
Mirandy	Miranda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
pohřben	pohřbít	k5eAaPmNgMnS
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
do	do	k7c2
masového	masový	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
</s>
<s>
Období	období	k1gNnSc1
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
nezávislosti	nezávislost	k1gFnSc2
se	se	k3xPyFc4
vyznačovalo	vyznačovat	k5eAaImAgNnS
vážnými	vážný	k2eAgInPc7d1
vládními	vládní	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
trvaly	trvat	k5eAaImAgInP
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
do	do	k7c2
doba	doba	k1gFnSc1
krutovlád	krutovláda	k1gFnPc2
a	a	k8xC
anarchie	anarchie	k1gFnSc2
<g/>
,	,	kIx,
zemi	zem	k1gFnSc6
vládlo	vládnout	k5eAaImAgNnS
několik	několik	k4yIc1
vojenských	vojenský	k2eAgMnPc2d1
diktátorů	diktátor	k1gMnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
gaudillos	gaudillos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
svoji	svůj	k3xOyFgFnSc4
významnou	významný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
se	se	k3xPyFc4
Caracas	Caracas	k1gInSc1
rozvíjel	rozvíjet	k5eAaImAgInS
jen	jen	k9
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
až	až	k6eAd1
do	do	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
generál	generál	k1gMnSc1
Guzmán	Guzmán	k2eAgMnSc1d1
'	'	kIx"
<g/>
Modernizátor	modernizátor	k1gMnSc1
<g/>
'	'	kIx"
Blanco	blanco	k6eAd1
schválil	schválit	k5eAaPmAgInS
rozsáhlý	rozsáhlý	k2eAgInSc4d1
program	program	k1gInSc4
modernizace	modernizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
budování	budování	k1gNnSc4
mohutných	mohutný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
zničeny	zničen	k2eAgFnPc1d1
zemětřesením	zemětřesení	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Objev	objev	k1gInSc1
ropy	ropa	k1gFnSc2
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc4
</s>
<s>
Antonio	Antonio	k1gMnSc1
Ledezma	Ledezmum	k1gNnSc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Caracasu	Caracas	k1gInSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
rozmachu	rozmach	k1gInSc2
těžby	těžba	k1gFnSc2
ropy	ropa	k1gFnSc2
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
se	se	k3xPyFc4
z	z	k7c2
koloniálního	koloniální	k2eAgNnSc2d1
městečka	městečko	k1gNnSc2
stalo	stát	k5eAaPmAgNnS
moderní	moderní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgFnPc3,k3yRgFnPc3,k3yQgFnPc3
je	být	k5eAaImIp3nS
Caracas	Caracas	k1gInSc1
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
stavět	stavět	k5eAaImF
dálnice	dálnice	k1gFnPc4
podle	podle	k7c2
amerického	americký	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městský	městský	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
do	do	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
provoz	provoz	k1gInSc1
metra	metro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
populace	populace	k1gFnSc2
Caracasu	Caracas	k1gInSc2
prudce	prudko	k6eAd1
stoupla	stoupnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
také	také	k9
imigrací	imigrace	k1gFnPc2
mj.	mj.	kA
z	z	k7c2
Kolumbie	Kolumbie	k1gFnSc2
a	a	k8xC
Haiti	Haiti	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
a	a	k8xC
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
zažilo	zažít	k5eAaPmAgNnS
město	město	k1gNnSc1
ekonomický	ekonomický	k2eAgInSc1d1
úpadek	úpadek	k1gInSc4
a	a	k8xC
tisíce	tisíc	k4xCgInPc1
venkovanů	venkovan	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
dříve	dříve	k6eAd2
proudily	proudit	k5eAaImAgInP,k5eAaPmAgInP
do	do	k7c2
metropole	metropol	k1gFnSc2
<g/>
,	,	kIx,
najednou	najednou	k6eAd1
vedly	vést	k5eAaImAgFnP
nejistou	jistý	k2eNgFnSc4d1
existenci	existence	k1gFnSc4
ve	v	k7c6
zchátralých	zchátralý	k2eAgInPc6d1
domech	dům	k1gInPc6
na	na	k7c6
kopcích	kopec	k1gInPc6
obklopujících	obklopující	k2eAgInPc2d1
centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1999	#num#	k4
velké	velký	k2eAgInPc1d1
sesuvy	sesuv	k1gInPc1
bahna	bahno	k1gNnSc2
zdevastovaly	zdevastovat	k5eAaPmAgInP
100	#num#	k4
km	km	kA
pobřeží	pobřeží	k1gNnSc2
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
Caracasu	Caracas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neobvykle	obvykle	k6eNd1
vydatné	vydatný	k2eAgFnPc4d1
srážky	srážka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
katastrofu	katastrofa	k1gFnSc4
způsobily	způsobit	k5eAaPmAgFnP
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
vyskytují	vyskytovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
tisíc	tisíc	k4xCgInSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sesuvy	sesuv	k1gInPc4
bahna	bahno	k1gNnSc2
považované	považovaný	k2eAgFnSc2d1
za	za	k7c4
nejhorší	zlý	k2eAgFnSc4d3
katastrofu	katastrofa	k1gFnSc4
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
si	se	k3xPyFc3
vyžádaly	vyžádat	k5eAaPmAgInP
kolem	kolem	k7c2
50	#num#	k4
000	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
domova	domov	k1gInSc2
zůstalo	zůstat	k5eAaPmAgNnS
150	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celá	k1gFnSc1
městečka	městečko	k1gNnSc2
z	z	k7c2
chatrčí	chatrč	k1gFnPc2
zmizela	zmizet	k5eAaPmAgFnS
<g/>
,	,	kIx,
koloniální	koloniální	k2eAgNnSc4d1
město	město	k1gNnSc4
La	la	k1gNnSc2
Guaira	Guair	k1gMnSc2
–	–	k?
kdysi	kdysi	k6eAd1
nejvýznamnější	významný	k2eAgFnSc1d3
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
v	v	k7c6
regionu	region	k1gInSc6
–	–	k?
bylo	být	k5eAaImAgNnS
téměř	téměř	k6eAd1
úplně	úplně	k6eAd1
zničeno	zničen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2002	#num#	k4
se	se	k3xPyFc4
ulice	ulice	k1gFnSc1
centrálního	centrální	k2eAgInSc2d1
Caracasu	Caracas	k1gInSc2
staly	stát	k5eAaPmAgFnP
bitevním	bitevní	k2eAgNnSc7d1
polem	pole	k1gNnSc7
a	a	k8xC
divadelní	divadelní	k2eAgFnSc7d1
scénou	scéna	k1gFnSc7
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
několik	několik	k4yIc4
dní	den	k1gInPc2
paralyzováno	paralyzován	k2eAgNnSc4d1
demonstracemi	demonstrace	k1gFnPc7
proti	proti	k7c3
vládě	vláda	k1gFnSc3
prezidenta	prezident	k1gMnSc2
Hugo	Hugo	k1gMnSc1
Cháveze	Cháveze	k1gFnPc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
shromáždila	shromáždit	k5eAaPmAgFnS
své	svůj	k3xOyFgMnPc4
příznivce	příznivec	k1gMnPc4
a	a	k8xC
uspořádala	uspořádat	k5eAaPmAgFnS
protidemonstrace	protidemonstrace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konfrontace	konfrontace	k1gFnSc1
se	se	k3xPyFc4
zvrhly	zvrhnout	k5eAaPmAgInP
v	v	k7c4
pouliční	pouliční	k2eAgFnPc4d1
bitky	bitka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chávez	Chávez	k1gMnSc1
sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgMnS
krátce	krátce	k6eAd1
zajat	zajmout	k5eAaPmNgMnS
a	a	k8xC
držen	držet	k5eAaImNgMnS
ve	v	k7c6
vazbě	vazba	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
se	s	k7c7
slávou	sláva	k1gFnSc7
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Caracaská	Caracaský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
</s>
<s>
Pomník	pomník	k1gInSc1
Osvoboditele	osvoboditel	k1gMnSc2
Simóna	Simón	k1gMnSc2
Bolívara	Bolívar	k1gMnSc2
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
množství	množství	k1gNnSc2
muzeí	muzeum	k1gNnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yIgFnPc7,k3yRgFnPc7,k3yQgFnPc7
vyniká	vynikat	k5eAaImIp3nS
například	například	k6eAd1
muzeum	muzeum	k1gNnSc1
Casa	Cas	k1gInSc2
Natal	Natal	k1gInSc1
del	del	k?
Libertador	Libertador	k1gInSc1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
rekonstruovaný	rekonstruovaný	k2eAgInSc4d1
rodný	rodný	k2eAgInSc4d1
dům	dům	k1gInSc4
právě	právě	k9
Simóna	Simóna	k1gFnSc1
Bolívara	Bolívara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
zajímavosti	zajímavost	k1gFnPc4
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
památník	památník	k1gInSc1
Panteón	Panteón	k1gMnSc1
Nacional	Nacional	k1gMnSc1
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
změnu	změna	k1gFnSc4
revolucionář	revolucionář	k1gMnSc1
pohřben	pohřbít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
městské	městský	k2eAgFnSc2d1
katedrály	katedrála	k1gFnSc2
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
Španěly	Španěl	k1gMnPc7
roku	rok	k1gInSc2
1614	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
postihlo	postihnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1812	#num#	k4
ničivé	ničivý	k2eAgNnSc1d1
zemětřesení	zemětřesení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
zničilo	zničit	k5eAaPmAgNnS
většinu	většina	k1gFnSc4
historických	historický	k2eAgFnPc2d1
koloniálních	koloniální	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
zapsáno	zapsat	k5eAaPmNgNnS
univerzitní	univerzitní	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
Caracasu	Caracas	k1gInSc6
jako	jako	k9
výjimečný	výjimečný	k2eAgInSc4d1
příklad	příklad	k1gInSc4
moderní	moderní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
a	a	k8xC
urbanismu	urbanismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Průmysl	průmysl	k1gInSc1
</s>
<s>
Místní	místní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
na	na	k7c4
výrobu	výroba	k1gFnSc4
automobilů	automobil	k1gInPc2
<g/>
,	,	kIx,
cementu	cement	k1gInSc2
<g/>
,	,	kIx,
umělých	umělý	k2eAgFnPc2d1
hmot	hmota	k1gFnPc2
<g/>
,	,	kIx,
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
papíru	papír	k1gInSc2
<g/>
,	,	kIx,
chemického	chemický	k2eAgNnSc2d1
a	a	k8xC
textilního	textilní	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Bogotá	Bogotat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
Cluj-Napoca	Cluj-Napoca	k1gFnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Damašek	Damašek	k1gInSc1
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
Havana	Havana	k1gFnSc1
<g/>
,	,	kIx,
Kuba	Kuba	k1gFnSc1
</s>
<s>
Istanbul	Istanbul	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Teherán	Teherán	k1gInSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Managua	Managua	k1gFnSc1
<g/>
,	,	kIx,
Nikaragua	Nikaragua	k1gFnSc1
</s>
<s>
Minsk	Minsk	k1gInSc1
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
New	New	k?
Orleans	Orleans	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Bombaj	Bombaj	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
</s>
<s>
Quezon	Quezon	k1gInSc1
City	City	k1gFnSc2
<g/>
,	,	kIx,
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Santo	Sant	k2eAgNnSc1d1
Domingo	Domingo	k1gNnSc1
<g/>
,	,	kIx,
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
San	San	k?
Francisco	Francisco	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Sã	Sã	k1gFnSc5
Paulo	Paula	k1gFnSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc5
</s>
<s>
Soul	Soul	k1gInSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Caracas	Caracas	k1gInSc4
od	od	k7c2
Valle	Valle	k1gFnSc2
Ariba	Arib	k1gMnSc2
</s>
<s>
Sabana	Sabana	k1gFnSc1
Grande	grand	k1gMnSc5
-	-	kIx~
Torre	torr	k1gInSc5
Buenaventura	Buenaventura	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
chudinských	chudinský	k2eAgFnPc2d1
čtvrtí	čtvrt	k1gFnPc2
zvaných	zvaný	k2eAgFnPc2d1
barrios	barriosa	k1gFnPc2
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
(	(	kIx(
<g/>
barrio	barrio	k1gNnSc1
<g/>
)	)	kIx)
Bolívar	Bolívar	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Instituto	Institut	k2eAgNnSc1d1
Nacional	Nacional	k1gMnSc3
de	de	k?
Estadísticas	Estadísticas	k1gMnSc1
(	(	kIx(
<g/>
Municipio	Municipio	k1gMnSc1
Libertador	Libertador	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Instituto	Institut	k2eAgNnSc1d1
Nacional	Nacional	k1gMnSc3
de	de	k?
Estadísticas	Estadísticas	k1gMnSc1
(	(	kIx(
<g/>
Municipios	Municipios	k1gMnSc1
Baruta	Barut	k1gMnSc2
<g/>
,	,	kIx,
Chacao	Chacao	k6eAd1
<g/>
,	,	kIx,
El	Ela	k1gFnPc2
Hatillo	Hatillo	k1gNnSc1
y	y	k?
Sucre	Sucr	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Caracas	Caracasa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Caracas	Caracasa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Caracas	Caracasa	k1gFnPc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Asunción	Asunción	k1gInSc1
(	(	kIx(
<g/>
Paraguay	Paraguay	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bogotá	Bogotá	k1gFnSc1
(	(	kIx(
<g/>
Kolumbie	Kolumbie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Brasília	Brasília	k1gFnSc1
(	(	kIx(
<g/>
Brazílie	Brazílie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
(	(	kIx(
<g/>
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Caracas	Caracas	k1gInSc1
(	(	kIx(
<g/>
Venezuela	Venezuela	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Georgetown	Georgetown	k1gInSc1
(	(	kIx(
<g/>
Guyana	Guyana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Lima	Lima	k1gFnSc1
(	(	kIx(
<g/>
Peru	Peru	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Montevideo	Montevideo	k1gNnSc1
(	(	kIx(
<g/>
Uruguay	Uruguay	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Paramaribo	Paramariba	k1gFnSc5
(	(	kIx(
<g/>
Surinam	Surinam	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Quito	Quito	k1gNnSc1
(	(	kIx(
<g/>
Ekvádor	Ekvádor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Santiago	Santiago	k1gNnSc4
de	de	k?
Chile	Chile	k1gNnSc2
(	(	kIx(
<g/>
Chile	Chile	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Sucre	Sucr	k1gInSc5
(	(	kIx(
<g/>
Bolívie	Bolívie	k1gFnSc2
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azámořská	azámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Cayenne	Cayennout	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
Francouzská	francouzský	k2eAgFnSc1d1
Guyana	Guyana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Port	port	k1gInSc1
Stanley	Stanlea	k1gFnSc2
(	(	kIx(
<g/>
Falklandy	Falklanda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128904	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4009457-1	4009457-1	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2110	#num#	k4
6689	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79097478	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
139170218	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79097478	#num#	k4
</s>
