<s>
Velká	velký	k2eAgFnSc1d1	velká
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
dějin	dějiny	k1gFnPc2	dějiny
Francie	Francie	k1gFnSc2	Francie
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1789	[number]	k4	1789
a	a	k8xC	a
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
svolání	svolání	k1gNnSc2	svolání
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
do	do	k7c2	do
uchopení	uchopení	k1gNnSc2	uchopení
moci	moc	k1gFnSc2	moc
Napoleonem	Napoleon	k1gMnSc7	Napoleon
Bonapartem	bonapart	k1gInSc7	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
absolutní	absolutní	k2eAgFnSc1d1	absolutní
monarchie	monarchie	k1gFnSc1	monarchie
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
vládou	vláda	k1gFnSc7	vláda
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
republikánstvím	republikánství	k1gNnSc7	republikánství
<g/>
;	;	kIx,	;
francouzská	francouzský	k2eAgFnSc1d1	francouzská
větev	větev	k1gFnSc1	větev
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
byla	být	k5eAaImAgFnS	být
donucena	donutit	k5eAaPmNgFnS	donutit
k	k	k7c3	k
radikální	radikální	k2eAgFnSc3d1	radikální
restrukturalizaci	restrukturalizace	k1gFnSc3	restrukturalizace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
Napoleonovým	Napoleonův	k2eAgInSc7d1	Napoleonův
státním	státní	k2eAgInSc7d1	státní
převratem	převrat	k1gInSc7	převrat
zmítala	zmítat	k5eAaImAgFnS	zmítat
75	[number]	k4	75
let	léto	k1gNnPc2	léto
mezi	mezi	k7c7	mezi
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
císařstvím	císařství	k1gNnSc7	císařství
a	a	k8xC	a
restaurací	restaurace	k1gFnSc7	restaurace
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
revoluce	revoluce	k1gFnSc2	revoluce
přesto	přesto	k8xC	přesto
znamenala	znamenat	k5eAaImAgFnS	znamenat
definitivní	definitivní	k2eAgInSc4d1	definitivní
konec	konec	k1gInSc4	konec
pro	pro	k7c4	pro
starý	starý	k2eAgInSc4d1	starý
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidových	lidový	k2eAgFnPc6d1	lidová
představách	představa	k1gFnPc6	představa
zastiňuje	zastiňovat	k5eAaImIp3nS	zastiňovat
i	i	k9	i
následující	následující	k2eAgFnSc1d1	následující
revoluce	revoluce	k1gFnSc1	revoluce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1830	[number]	k4	1830
a	a	k8xC	a
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
přelomový	přelomový	k2eAgInSc4d1	přelomový
bod	bod	k1gInSc4	bod
evropských	evropský	k2eAgFnPc2d1	Evropská
kontinentálních	kontinentální	k2eAgFnPc2d1	kontinentální
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
absolutismu	absolutismus	k1gInSc2	absolutismus
k	k	k7c3	k
občanství	občanství	k1gNnSc3	občanství
a	a	k8xC	a
ustavení	ustavení	k1gNnSc3	ustavení
lidu	lid	k1gInSc2	lid
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc2d1	hlavní
politické	politický	k2eAgFnSc2d1	politická
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
také	také	k9	také
zrodila	zrodit	k5eAaPmAgFnS	zrodit
moderní	moderní	k2eAgInSc4d1	moderní
politický	politický	k2eAgInSc4d1	politický
teror	teror	k1gInSc4	teror
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
potácela	potácet	k5eAaImAgFnS	potácet
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
státního	státní	k2eAgInSc2d1	státní
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
uskutečňovány	uskutečňován	k2eAgFnPc1d1	uskutečňována
reformy	reforma	k1gFnPc1	reforma
pro	pro	k7c4	pro
povzbuzení	povzbuzení	k1gNnSc4	povzbuzení
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc1	zrušení
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
cla	clo	k1gNnSc2	clo
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
poplatků	poplatek	k1gInPc2	poplatek
zatěžujících	zatěžující	k2eAgInPc2d1	zatěžující
výrobu	výroba	k1gFnSc4	výroba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hospodářským	hospodářský	k2eAgInPc3d1	hospodářský
problémům	problém	k1gInPc3	problém
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
i	i	k9	i
nepřízeň	nepřízeň	k1gFnSc1	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
erupcí	erupce	k1gFnSc7	erupce
islandské	islandský	k2eAgFnSc2d1	islandská
sopky	sopka	k1gFnSc2	sopka
Laki	Lak	k1gFnSc2	Lak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sopečné	sopečný	k2eAgFnSc2d1	sopečná
erupce	erupce	k1gFnSc2	erupce
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
sopečného	sopečný	k2eAgInSc2d1	sopečný
popela	popel	k1gInSc2	popel
a	a	k8xC	a
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
po	po	k7c4	po
několik	několik	k4yIc4	několik
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
neúrodě	neúroda	k1gFnSc6	neúroda
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
strádání	strádání	k1gNnSc6	strádání
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
aristokracii	aristokracie	k1gFnSc3	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
proti	proti	k7c3	proti
reformám	reforma	k1gFnPc3	reforma
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
soudní	soudní	k2eAgFnSc1d1	soudní
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
reformě	reforma	k1gFnSc3	reforma
roku	rok	k1gInSc2	rok
1788	[number]	k4	1788
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Grenoblu	Grenoble	k1gInSc6	Grenoble
v	v	k7c6	v
Dauphiné	Dauphiná	k1gFnSc6	Dauphiná
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1789	[number]	k4	1789
svolal	svolat	k5eAaPmAgMnS	svolat
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
generální	generální	k2eAgInPc1d1	generální
stavy	stav	k1gInPc1	stav
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
po	po	k7c6	po
175	[number]	k4	175
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
generální	generální	k2eAgInPc1d1	generální
stavy	stav	k1gInPc1	stav
sešly	sejít	k5eAaPmAgInP	sejít
roku	rok	k1gInSc2	rok
1614	[number]	k4	1614
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
třetího	třetí	k4xOgInSc2	třetí
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
bylo	být	k5eAaImAgNnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zástupců	zástupce	k1gMnPc2	zástupce
prvního	první	k4xOgNnSc2	první
i	i	k8xC	i
druhého	druhý	k4xOgInSc2	druhý
stavu	stav	k1gInSc2	stav
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
kromě	kromě	k7c2	kromě
placení	placení	k1gNnPc2	placení
daní	daň	k1gFnPc2	daň
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
privilegovanými	privilegovaný	k2eAgInPc7d1	privilegovaný
stavy	stav	k1gInPc7	stav
projednávat	projednávat	k5eAaImF	projednávat
i	i	k8xC	i
reformy	reforma	k1gFnSc2	reforma
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
stavy	stav	k1gInPc4	stav
(	(	kIx(	(
<g/>
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
nesouhlasily	souhlasit	k5eNaImAgInP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
se	s	k7c7	s
šlechtou	šlechta	k1gFnSc7	šlechta
snažili	snažit	k5eAaImAgMnP	snažit
omezit	omezit	k5eAaPmF	omezit
působnost	působnost	k1gFnSc4	působnost
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
na	na	k7c4	na
pouhé	pouhý	k2eAgNnSc4d1	pouhé
poradní	poradní	k2eAgNnSc4d1	poradní
těleso	těleso	k1gNnSc4	těleso
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
třetího	třetí	k4xOgInSc2	třetí
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
rozšíření	rozšíření	k1gNnSc3	rozšíření
jeho	jeho	k3xOp3gNnPc2	jeho
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
zákonodárný	zákonodárný	k2eAgInSc4d1	zákonodárný
orgán	orgán	k1gInSc4	orgán
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
dohadech	dohad	k1gInPc6	dohad
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
třetí	třetí	k4xOgInSc1	třetí
stav	stav	k1gInSc1	stav
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
část	část	k1gFnSc1	část
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
donuceni	donutit	k5eAaPmNgMnP	donutit
přísahat	přísahat	k5eAaImF	přísahat
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k1gMnPc1	duchovní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
nesměli	smět	k5eNaImAgMnP	smět
nadále	nadále	k6eAd1	nadále
vykonávat	vykonávat	k5eAaImF	vykonávat
své	svůj	k3xOyFgFnPc4	svůj
kněžské	kněžský	k2eAgFnPc4d1	kněžská
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Sedmdesátník	sedmdesátník	k1gMnSc1	sedmdesátník
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
papež	papež	k1gMnSc1	papež
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
)	)	kIx)	)
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
toleranci	tolerance	k1gFnSc4	tolerance
přísahy	přísaha	k1gFnSc2	přísaha
(	(	kIx(	(
<g/>
La	la	k1gNnSc2	la
Déclaration	Déclaration	k1gInSc4	Déclaration
des	des	k1gNnSc2	des
Droits	Droits	k1gInSc1	Droits
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gMnSc5	Homm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
prosil	prosit	k5eAaImAgMnS	prosit
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
dekret	dekret	k1gInSc4	dekret
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1790	[number]	k4	1790
sám	sám	k3xTgInSc4	sám
podepsal	podepsat	k5eAaPmAgInS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
zúčastnění	zúčastněný	k2eAgMnPc1d1	zúčastněný
přísahali	přísahat	k5eAaImAgMnP	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nerozejdou	rozejít	k5eNaPmIp3nP	rozejít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
vypracována	vypracován	k2eAgFnSc1d1	vypracována
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
shromáždění	shromáždění	k1gNnSc1	shromáždění
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
Ústavodárným	ústavodárný	k2eAgNnSc7d1	Ústavodárné
národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
sice	sice	k8xC	sice
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
Versailles	Versailles	k1gFnSc3	Versailles
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sněm	sněm	k1gInSc1	sněm
konal	konat	k5eAaImAgInS	konat
<g/>
,	,	kIx,	,
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
Pařížané	Pařížan	k1gMnPc1	Pařížan
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
do	do	k7c2	do
královské	královský	k2eAgFnSc2d1	královská
zbrojnice	zbrojnice	k1gFnSc2	zbrojnice
<g/>
,	,	kIx,	,
zabavili	zabavit	k5eAaPmAgMnP	zabavit
na	na	k7c4	na
32	[number]	k4	32
tisíc	tisíc	k4xCgInPc2	tisíc
pušek	puška	k1gFnPc2	puška
<g/>
,	,	kIx,	,
ukořistili	ukořistit	k5eAaPmAgMnP	ukořistit
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
pík	píka	k1gFnPc2	píka
<g/>
,	,	kIx,	,
sestavili	sestavit	k5eAaPmAgMnP	sestavit
milice	milice	k1gFnPc4	milice
a	a	k8xC	a
časně	časně	k6eAd1	časně
ráno	ráno	k6eAd1	ráno
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1789	[number]	k4	1789
napadli	napadnout	k5eAaPmAgMnP	napadnout
vězení	vězení	k1gNnSc4	vězení
Bastillu	Bastill	k1gInSc2	Bastill
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
královského	královský	k2eAgInSc2d1	královský
útlaku	útlak	k1gInSc2	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
hodinách	hodina	k1gFnPc6	hodina
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
vězení	vězení	k1gNnSc1	vězení
dobyto	dobyt	k2eAgNnSc1d1	dobyto
<g/>
.	.	kIx.	.
</s>
<s>
Správce	správce	k1gMnSc1	správce
Bastilly	Bastilla	k1gFnSc2	Bastilla
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
odevzdat	odevzdat	k5eAaPmF	odevzdat
klíče	klíč	k1gInPc4	klíč
vzbouřencům	vzbouřenec	k1gMnPc3	vzbouřenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
jim	on	k3xPp3gInPc3	on
klíč	klíč	k1gInSc1	klíč
předával	předávat	k5eAaImAgInS	předávat
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
stráží	stráž	k1gFnPc2	stráž
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
do	do	k7c2	do
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dav	dav	k1gInSc4	dav
rozzuřilo	rozzuřit	k5eAaPmAgNnS	rozzuřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zabili	zabít	k5eAaPmAgMnP	zabít
správce	správce	k1gMnSc4	správce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Z	z	k7c2	z
vojenského	vojenský	k2eAgNnSc2d1	vojenské
hlediska	hledisko	k1gNnSc2	hledisko
mělo	mít	k5eAaImAgNnS	mít
vítězství	vítězství	k1gNnSc4	vítězství
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
význam	význam	k1gInSc1	význam
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pevností	pevnost	k1gFnSc7	pevnost
se	se	k3xPyFc4	se
lid	lid	k1gInSc1	lid
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
i	i	k9	i
značných	značný	k2eAgFnPc2d1	značná
zásob	zásoba	k1gFnPc2	zásoba
munice	munice	k1gFnSc2	munice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byly	být	k5eAaImAgFnP	být
uloženy	uložen	k2eAgFnPc1d1	uložena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
psychiku	psychika	k1gFnSc4	psychika
lidí	člověk	k1gMnPc2	člověk
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
význam	význam	k1gInSc4	význam
obrovský	obrovský	k2eAgInSc4d1	obrovský
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Půjde	jít	k5eAaImIp3nS	jít
to	ten	k3xDgNnSc1	ten
<g/>
!!!	!!!	k?	!!!
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
heslem	heslo	k1gNnSc7	heslo
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
Pařížanů	Pařížan	k1gMnPc2	Pařížan
je	být	k5eAaImIp3nS	být
datován	datován	k2eAgInSc4d1	datován
počátek	počátek	k1gInSc4	počátek
revoluce	revoluce	k1gFnSc2	revoluce
i	i	k8xC	i
jejího	její	k3xOp3gNnSc2	její
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
i	i	k9	i
za	za	k7c7	za
jejími	její	k3xOp3gFnPc7	její
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Mocenské	mocenský	k2eAgInPc1d1	mocenský
orgány	orgán	k1gInPc1	orgán
byly	být	k5eAaImAgInP	být
rozpouštěny	rozpouštět	k5eAaImNgInP	rozpouštět
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
ustanovovány	ustanovovat	k5eAaImNgFnP	ustanovovat
nové	nový	k2eAgFnPc1d1	nová
městské	městský	k2eAgFnPc1d1	městská
rady	rada	k1gFnPc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ústavodárné	ústavodárný	k2eAgNnSc1d1	Ústavodárné
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
začalo	začít	k5eAaPmAgNnS	začít
plnit	plnit	k5eAaImF	plnit
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Zrušilo	zrušit	k5eAaPmAgNnS	zrušit
daňová	daňový	k2eAgNnPc1d1	daňové
privilegia	privilegium	k1gNnPc1	privilegium
<g/>
,	,	kIx,	,
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
rovnost	rovnost	k1gFnSc4	rovnost
občanů	občan	k1gMnPc2	občan
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
desátky	desátka	k1gFnSc2	desátka
<g/>
,	,	kIx,	,
cechy	cech	k1gInPc4	cech
i	i	k8xC	i
vrchnostenská	vrchnostenský	k2eAgNnPc4d1	vrchnostenské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Platby	platba	k1gFnPc1	platba
šlechtě	šlechta	k1gFnSc3	šlechta
za	za	k7c4	za
užívání	užívání	k1gNnSc4	užívání
půdy	půda	k1gFnSc2	půda
však	však	k9	však
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Markýz	markýz	k1gMnSc1	markýz
de	de	k?	de
La	la	k1gNnPc2	la
Fayette	Fayett	k1gInSc5	Fayett
vedl	vést	k5eAaImAgMnS	vést
revoluční	revoluční	k2eAgNnSc1d1	revoluční
vojsko	vojsko	k1gNnSc1	vojsko
zvané	zvaný	k2eAgNnSc1d1	zvané
Národní	národní	k2eAgFnSc4d1	národní
garda	garda	k1gFnSc1	garda
<g/>
.	.	kIx.	.
</s>
<s>
Kontrarevoluční	kontrarevoluční	k2eAgFnSc1d1	kontrarevoluční
šlechta	šlechta	k1gFnSc1	šlechta
utekla	utéct	k5eAaPmAgFnS	utéct
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
rolníci	rolník	k1gMnPc1	rolník
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
feudály	feudál	k1gMnPc4	feudál
<g/>
.	.	kIx.	.
</s>
<s>
Plenili	plenit	k5eAaImAgMnP	plenit
zámky	zámek	k1gInPc4	zámek
a	a	k8xC	a
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
platit	platit	k5eAaImF	platit
poddanské	poddanský	k2eAgFnPc4d1	poddanská
dávky	dávka	k1gFnPc4	dávka
<g/>
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1789	[number]	k4	1789
začal	začít	k5eAaPmAgInS	začít
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Ženský	ženský	k2eAgInSc1d1	ženský
pochod	pochod	k1gInSc1	pochod
na	na	k7c6	na
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
dne	den	k1gInSc2	den
propukaly	propukat	k5eAaImAgInP	propukat
nepokoje	nepokoj	k1gInPc1	nepokoj
mezi	mezi	k7c7	mezi
trhovkyněmi	trhovkyně	k1gFnPc7	trhovkyně
na	na	k7c6	na
tržištích	tržiště	k1gNnPc6	tržiště
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc3	nedostatek
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
náladě	nálada	k1gFnSc6	nálada
jedna	jeden	k4xCgFnSc1	jeden
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
tržišti	tržiště	k1gNnSc6	tržiště
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Paříži	Paříž	k1gFnSc6	Paříž
začala	začít	k5eAaPmAgFnS	začít
bubnovat	bubnovat	k5eAaImF	bubnovat
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
dav	dav	k1gInSc1	dav
žen	žena	k1gFnPc2	žena
vynutil	vynutit	k5eAaPmAgInS	vynutit
rozeznění	rozeznění	k1gNnSc4	rozeznění
zvonů	zvon	k1gInPc2	zvon
v	v	k7c6	v
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
dav	dav	k1gInSc1	dav
na	na	k7c4	na
pochod	pochod	k1gInSc4	pochod
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
rozrůstal	rozrůstat	k5eAaImAgInS	rozrůstat
<g/>
,	,	kIx,	,
až	až	k9	až
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
pařížské	pařížský	k2eAgFnSc3d1	Pařížská
radnici	radnice	k1gFnSc3	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požadoval	požadovat	k5eAaImAgMnS	požadovat
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
momentě	moment	k1gInSc6	moment
dav	dav	k1gInSc1	dav
čítal	čítat	k5eAaImAgInS	čítat
asi	asi	k9	asi
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
tisíc	tisíc	k4xCgInSc4	tisíc
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
žen	žena	k1gFnPc2	žena
i	i	k8xC	i
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
radnicí	radnice	k1gFnSc7	radnice
muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Stanislaus	Stanislaus	k1gMnSc1	Stanislaus
Maillard	Maillard	k1gMnSc1	Maillard
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
účastnil	účastnit	k5eAaImAgMnS	účastnit
již	již	k6eAd1	již
útoku	útok	k1gInSc2	útok
na	na	k7c6	na
Bastillu	Bastill	k1gInSc6	Bastill
a	a	k8xC	a
mezi	mezi	k7c7	mezi
trhovkyněmi	trhovkyně	k1gFnPc7	trhovkyně
byl	být	k5eAaImAgInS	být
populární	populární	k2eAgInSc1d1	populární
<g/>
,	,	kIx,	,
vykřikl	vykřiknout	k5eAaPmAgMnS	vykřiknout
"	"	kIx"	"
<g/>
à	à	k?	à
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c6	na
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
neoficiálním	neoficiální	k2eAgMnSc7d1	neoficiální
vůdcem	vůdce	k1gMnSc7	vůdce
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgInS	vynutit
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
zbrojnice	zbrojnice	k1gFnSc2	zbrojnice
v	v	k7c6	v
radnici	radnice	k1gFnSc6	radnice
a	a	k8xC	a
ozbrojil	ozbrojit	k5eAaPmAgMnS	ozbrojit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
pochod	pochod	k1gInSc4	pochod
k	k	k7c3	k
zámku	zámek	k1gInSc3	zámek
Versailles	Versailles	k1gFnSc2	Versailles
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
mu	on	k3xPp3gMnSc3	on
zabral	zabrat	k5eAaPmAgInS	zabrat
asi	asi	k9	asi
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
palácem	palác	k1gInSc7	palác
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
davem	dav	k1gInSc7	dav
nespokojenců	nespokojenec	k1gMnPc2	nespokojenec
z	z	k7c2	z
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
společně	společně	k6eAd1	společně
si	se	k3xPyFc3	se
vynutili	vynutit	k5eAaPmAgMnP	vynutit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
je	být	k5eAaImIp3nS	být
Ústavodárné	ústavodárný	k2eAgNnSc1d1	Ústavodárné
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
pustilo	pustit	k5eAaPmAgNnS	pustit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
zasedacího	zasedací	k2eAgInSc2d1	zasedací
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
členové	člen	k1gMnPc1	člen
shromáždění	shromáždění	k1gNnSc2	shromáždění
učinit	učinit	k5eAaImF	učinit
museli	muset	k5eAaImAgMnP	muset
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
konfrontaci	konfrontace	k1gFnSc4	konfrontace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
nevýhodnou	výhodný	k2eNgFnSc4d1	nevýhodná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
aktuálně	aktuálně	k6eAd1	aktuálně
sami	sám	k3xTgMnPc1	sám
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgMnPc2	jakýkoliv
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
shromážděním	shromáždění	k1gNnSc7	shromáždění
dav	dav	k1gInSc1	dav
přednesl	přednést	k5eAaPmAgMnS	přednést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc7	jejich
jediným	jediný	k2eAgInSc7d1	jediný
důvodem	důvod	k1gInSc7	důvod
příchodu	příchod	k1gInSc2	příchod
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
řešení	řešení	k1gNnSc4	řešení
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
si	se	k3xPyFc3	se
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jako	jako	k8xS	jako
zástupce	zástupce	k1gMnPc4	zástupce
šest	šest	k4xCc4	šest
trhovkyň	trhovkyně	k1gFnPc2	trhovkyně
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
přivedeny	přivést	k5eAaPmNgFnP	přivést
před	před	k7c4	před
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ženy	žena	k1gFnPc1	žena
byly	být	k5eAaImAgFnP	být
okouzleny	okouzlen	k2eAgMnPc4d1	okouzlen
královým	králův	k2eAgInSc7d1	králův
šarmem	šarm	k1gInSc7	šarm
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
částí	část	k1gFnSc7	část
davu	dav	k1gInSc2	dav
uspokojeny	uspokojen	k2eAgFnPc1d1	uspokojena
příslibem	příslib	k1gInSc7	příslib
distribuování	distribuování	k1gNnSc4	distribuování
potravin	potravina	k1gFnPc2	potravina
z	z	k7c2	z
královských	královský	k2eAgFnPc2d1	královská
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
tímto	tento	k3xDgNnSc7	tento
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
Stanislaus	Stanislaus	k1gInSc4	Stanislaus
Maillard	Maillarda	k1gFnPc2	Maillarda
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
davu	dav	k1gInSc2	dav
setrvala	setrvat	k5eAaPmAgFnS	setrvat
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
slib	slib	k1gInSc4	slib
nedodrží	dodržet	k5eNaPmIp3nS	dodržet
<g/>
,	,	kIx,	,
snad	snad	k9	snad
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Antoinety	Antoinet	k1gInPc1	Antoinet
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
večerní	večerní	k2eAgMnSc1d1	večerní
král	král	k1gMnSc1	král
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
další	další	k2eAgInPc4d1	další
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
ústupky	ústupek	k1gInPc4	ústupek
a	a	k8xC	a
přísliby	příslib	k1gInPc4	příslib
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
on	on	k3xPp3gMnSc1	on
i	i	k9	i
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
ráno	ráno	k6eAd1	ráno
rozejde	rozejít	k5eAaPmIp3nS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
kolem	kolem	k7c2	kolem
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
ráno	ráno	k6eAd1	ráno
malá	malý	k2eAgFnSc1d1	malá
skupinka	skupinka	k1gFnSc1	skupinka
z	z	k7c2	z
davu	dav	k1gInSc2	dav
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
bran	brána	k1gFnPc2	brána
paláce	palác	k1gInSc2	palác
není	být	k5eNaImIp3nS	být
hlídána	hlídán	k2eAgFnSc1d1	hlídána
a	a	k8xC	a
vnikla	vniknout	k5eAaPmAgFnS	vniknout
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
hledajíc	hledat	k5eAaImSgFnS	hledat
královninu	královnin	k2eAgFnSc4d1	Královnina
ložnici	ložnice	k1gFnSc4	ložnice
<g/>
,	,	kIx,	,
po	po	k7c6	po
paláci	palác	k1gInSc6	palác
je	být	k5eAaImIp3nS	být
pronásledovaly	pronásledovat	k5eAaImAgFnP	pronásledovat
královské	královský	k2eAgFnPc1d1	královská
stráže	stráž	k1gFnPc1	stráž
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
jeden	jeden	k4xCgMnSc1	jeden
mladý	mladý	k2eAgMnSc1d1	mladý
člen	člen	k1gMnSc1	člen
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
veliký	veliký	k2eAgInSc1d1	veliký
chaos	chaos	k1gInSc1	chaos
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dav	dav	k1gInSc1	dav
začal	začít	k5eAaPmAgInS	začít
zabíjet	zabíjet	k5eAaImF	zabíjet
královy	králův	k2eAgMnPc4d1	králův
strážce	strážce	k1gMnPc4	strážce
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
markýzovi	markýz	k1gMnSc3	markýz
Lafayettovi	Lafayett	k1gMnSc3	Lafayett
podařilo	podařit	k5eAaPmAgNnS	podařit
vyjednáváním	vyjednávání	k1gNnSc7	vyjednávání
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
uklidnit	uklidnit	k5eAaPmF	uklidnit
a	a	k8xC	a
násilí	násilí	k1gNnSc4	násilí
tím	ten	k3xDgNnSc7	ten
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
opustil	opustit	k5eAaPmAgInS	opustit
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
byl	být	k5eAaImAgInS	být
všude	všude	k6eAd1	všude
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc1	okolí
a	a	k8xC	a
královi	králův	k2eAgMnPc1d1	králův
vojáci	voják	k1gMnPc1	voják
odmítali	odmítat	k5eAaImAgMnP	odmítat
na	na	k7c4	na
dav	dav	k1gInSc4	dav
zaútočit	zaútočit	k5eAaPmF	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Lafayette	Lafayette	k5eAaPmIp2nP	Lafayette
krále	král	k1gMnSc4	král
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
společně	společně	k6eAd1	společně
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
před	před	k7c4	před
dav	dav	k1gInSc4	dav
na	na	k7c6	na
balkoně	balkon	k1gInSc6	balkon
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
král	král	k1gMnSc1	král
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
reakcí	reakce	k1gFnSc7	reakce
doprovázenou	doprovázený	k2eAgFnSc7d1	doprovázená
výkřiky	výkřik	k1gInPc7	výkřik
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
král	král	k1gMnSc1	král
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vive	Vive	k1gInSc1	Vive
le	le	k?	le
Roi	Roi	k1gFnSc2	Roi
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
dav	dav	k1gInSc1	dav
žádal	žádat	k5eAaImAgInS	žádat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
na	na	k7c6	na
balkóně	balkón	k1gInSc6	balkón
s	s	k7c7	s
Lafayettem	Lafayett	k1gInSc7	Lafayett
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
i	i	k9	i
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tak	tak	k6eAd1	tak
učinila	učinit	k5eAaImAgFnS	učinit
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
řada	řada	k1gFnSc1	řada
členů	člen	k1gInPc2	člen
davu	dav	k1gInSc2	dav
mířila	mířit	k5eAaImAgFnS	mířit
mušketami	mušketa	k1gFnPc7	mušketa
<g/>
,	,	kIx,	,
Lafayette	Lafayett	k1gInSc5	Lafayett
demonstrativně	demonstrativně	k6eAd1	demonstrativně
políbil	políbit	k5eAaPmAgMnS	políbit
královně	královna	k1gFnSc3	královna
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
dav	dav	k1gInSc4	dav
přijal	přijmout	k5eAaPmAgMnS	přijmout
celé	celý	k2eAgNnSc4d1	celé
vystoupení	vystoupení	k1gNnSc4	vystoupení
královny	královna	k1gFnSc2	královna
s	s	k7c7	s
respektem	respekt	k1gInSc7	respekt
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
ozvaly	ozvat	k5eAaPmAgInP	ozvat
i	i	k9	i
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
výkřiky	výkřik	k1gInPc1	výkřik
Ať	ať	k8xS	ať
žije	žít	k5eAaImIp3nS	žít
královna	královna	k1gFnSc1	královna
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vive	Vive	k1gInSc1	Vive
la	la	k1gNnSc2	la
Reine	Rein	k1gInSc5	Rein
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
odpoledne	odpoledne	k1gNnSc2	odpoledne
se	se	k3xPyFc4	se
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
i	i	k9	i
s	s	k7c7	s
davem	dav	k1gInSc7	dav
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
rozešel	rozejít	k5eAaPmAgInS	rozejít
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
usídlila	usídlit	k5eAaPmAgFnS	usídlit
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Tuileries	Tuileries	k1gInSc1	Tuileries
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
vycházející	vycházející	k2eAgNnSc4d1	vycházející
z	z	k7c2	z
Deklarace	deklarace	k1gFnSc2	deklarace
práv	právo	k1gNnPc2	právo
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
občana	občan	k1gMnSc4	občan
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
rovnost	rovnost	k1gFnSc1	rovnost
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
v	v	k7c6	v
září	září	k1gNnSc6	září
1791	[number]	k4	1791
předložilo	předložit	k5eAaPmAgNnS	předložit
tuto	tento	k3xDgFnSc4	tento
ústavu	ústava	k1gFnSc4	ústava
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
opakovaném	opakovaný	k2eAgInSc6d1	opakovaný
neúspěchu	neúspěch	k1gInSc6	neúspěch
svého	svůj	k3xOyFgNnSc2	svůj
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
díky	díky	k7c3	díky
vojenskému	vojenský	k2eAgInSc3d1	vojenský
tlaku	tlak	k1gInSc3	tlak
přísahou	přísaha	k1gFnSc7	přísaha
stvrdil	stvrdit	k5eAaPmAgMnS	stvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
formálně	formálně	k6eAd1	formálně
stala	stát	k5eAaPmAgFnS	stát
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1791	[number]	k4	1791
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
zástupci	zástupce	k1gMnPc1	zástupce
do	do	k7c2	do
Zákonodárného	zákonodárný	k2eAgNnSc2d1	zákonodárné
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
tím	ten	k3xDgNnSc7	ten
Ústavodárné	ústavodárný	k2eAgNnSc1d1	Ústavodárné
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozešlo	rozejít	k5eAaPmAgNnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
byli	být	k5eAaImAgMnP	být
zcela	zcela	k6eAd1	zcela
noví	nový	k2eAgMnPc1d1	nový
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
Robespierrova	Robespierrův	k2eAgInSc2d1	Robespierrův
návrhu	návrh	k1gInSc2	návrh
nesměli	smět	k5eNaImAgMnP	smět
tzv.	tzv.	kA	tzv.
konstituanti	konstituant	k1gMnPc1	konstituant
na	na	k7c4	na
poslance	poslanec	k1gMnPc4	poslanec
kandidovat	kandidovat	k5eAaImF	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslanci	poslanec	k1gMnPc7	poslanec
převažovali	převažovat	k5eAaImAgMnP	převažovat
odpůrci	odpůrce	k1gMnPc1	odpůrce
dalšího	další	k2eAgNnSc2d1	další
pokračování	pokračování	k1gNnSc2	pokračování
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivu	vliv	k1gInSc2	vliv
začaly	začít	k5eAaPmAgInP	začít
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
nabírat	nabírat	k5eAaImF	nabírat
spíše	spíše	k9	spíše
radikálnější	radikální	k2eAgInPc4d2	radikálnější
politické	politický	k2eAgInPc4d1	politický
kluby	klub	k1gInPc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Jakobíni	jakobín	k1gMnPc1	jakobín
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
bývalého	bývalý	k2eAgInSc2d1	bývalý
svatojakubského	svatojakubský	k2eAgInSc2d1	svatojakubský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
jejich	jejich	k3xOp3gNnPc4	jejich
označení	označení	k1gNnPc4	označení
<g/>
)	)	kIx)	)
nedůvěřovali	důvěřovat	k5eNaImAgMnP	důvěřovat
králi	král	k1gMnSc3	král
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
omezit	omezit	k5eAaPmF	omezit
jeho	jeho	k3xOp3gFnSc4	jeho
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárné	zákonodárný	k2eAgNnSc1d1	zákonodárné
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1792	[number]	k4	1792
válku	válka	k1gFnSc4	válka
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vojensky	vojensky	k6eAd1	vojensky
podpořeno	podpořit	k5eAaPmNgNnS	podpořit
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
samotné	samotný	k2eAgNnSc1d1	samotné
francouzské	francouzský	k2eAgNnSc1d1	francouzské
vojsko	vojsko	k1gNnSc1	vojsko
mělo	mít	k5eAaImAgNnS	mít
nedostatek	nedostatek	k1gInSc4	nedostatek
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc7d1	nízká
morálkou	morálka	k1gFnSc7	morálka
<g/>
,	,	kIx,	,
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
přišly	přijít	k5eAaPmAgInP	přijít
dobrovolnické	dobrovolnický	k2eAgInPc1d1	dobrovolnický
oddíly	oddíl	k1gInPc1	oddíl
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
z	z	k7c2	z
Marseille	Marseille	k1gFnSc2	Marseille
zpívaly	zpívat	k5eAaImAgFnP	zpívat
novou	nový	k2eAgFnSc4d1	nová
revoluční	revoluční	k2eAgFnSc4d1	revoluční
píseň	píseň	k1gFnSc4	píseň
Marseillaisu	Marseillaisa	k1gFnSc4	Marseillaisa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
hymnou	hymna	k1gFnSc7	hymna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1792	[number]	k4	1792
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
rozezvučely	rozezvučet	k5eAaPmAgInP	rozezvučet
zvony	zvon	k1gInPc1	zvon
na	na	k7c4	na
poplach	poplach	k1gInSc4	poplach
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
Georges	Georges	k1gMnSc1	Georges
Danton	Danton	k1gInSc4	Danton
<g/>
.	.	kIx.	.
</s>
<s>
Lid	lid	k1gInSc1	lid
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
na	na	k7c4	na
královské	královský	k2eAgNnSc4d1	královské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vypsány	vypsán	k2eAgFnPc1d1	vypsána
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národního	národní	k2eAgInSc2d1	národní
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
již	již	k6eAd1	již
mohl	moct	k5eAaImAgInS	moct
kandidovat	kandidovat	k5eAaImF	kandidovat
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
získali	získat	k5eAaPmAgMnP	získat
girondisté	girondista	k1gMnPc1	girondista
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
departementu	departement	k1gInSc2	departement
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1792	[number]	k4	1792
Konvent	konvent	k1gInSc1	konvent
sesadil	sesadit	k5eAaPmAgInS	sesadit
krále	král	k1gMnSc4	král
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
den	den	k1gInSc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
revoluční	revoluční	k2eAgNnSc1d1	revoluční
vojsko	vojsko	k1gNnSc1	vojsko
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
pruskou	pruský	k2eAgFnSc4d1	pruská
armádu	armáda	k1gFnSc4	armáda
u	u	k7c2	u
Valmy	Valma	k1gFnSc2	Valma
a	a	k8xC	a
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
ústupu	ústup	k1gInSc2	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Samo	sám	k3xTgNnSc1	sám
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Savojsko	Savojsko	k1gNnSc4	Savojsko
a	a	k8xC	a
Nice	Nice	k1gFnSc4	Nice
<g/>
.	.	kIx.	.
</s>
<s>
Girondistům	girondista	k1gMnPc3	girondista
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
nevedlo	vést	k5eNaImAgNnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
hladomor	hladomor	k1gInSc1	hladomor
a	a	k8xC	a
ceny	cena	k1gFnPc1	cena
potravin	potravina	k1gFnPc2	potravina
neustále	neustále	k6eAd1	neustále
rostly	růst	k5eAaImAgFnP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
také	také	k9	také
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
ostatní	ostatní	k2eAgInPc4d1	ostatní
státy	stát	k1gInPc4	stát
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
proto	proto	k8xC	proto
Konvent	konvent	k1gInSc4	konvent
nejtěsnější	těsný	k2eAgInSc4d3	nejtěsnější
většinou	většina	k1gFnSc7	většina
jednoho	jeden	k4xCgInSc2	jeden
hlasu	hlas	k1gInSc2	hlas
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
popravě	poprava	k1gFnSc6	poprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
zbytek	zbytek	k1gInSc1	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
šokovalo	šokovat	k5eAaBmAgNnS	šokovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Prusku	Prusko	k1gNnSc3	Prusko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
Španělsko	Španělsko	k1gNnSc1	Španělsko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
německými	německý	k2eAgInPc7d1	německý
a	a	k8xC	a
italskými	italský	k2eAgInPc7d1	italský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcem	vůdce	k1gMnSc7	vůdce
této	tento	k3xDgFnSc2	tento
protifrancouzské	protifrancouzský	k2eAgFnSc2d1	protifrancouzská
koalice	koalice	k1gFnSc2	koalice
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
i	i	k9	i
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
bránit	bránit	k5eAaImF	bránit
Francii	Francie	k1gFnSc4	Francie
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vendée	Vendé	k1gFnSc2	Vendé
propuklo	propuknout	k5eAaPmAgNnS	propuknout
kontrarevoluční	kontrarevoluční	k2eAgNnSc1d1	kontrarevoluční
povstání	povstání	k1gNnSc1	povstání
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
verbováním	verbování	k1gNnSc7	verbování
do	do	k7c2	do
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnSc1d2	novější
interpretace	interpretace	k1gFnSc1	interpretace
hovoří	hovořit	k5eAaImIp3nS	hovořit
také	také	k9	také
o	o	k7c6	o
nesouhlasu	nesouhlas	k1gInSc6	nesouhlas
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
postojem	postoj	k1gInSc7	postoj
revolučních	revoluční	k2eAgFnPc2d1	revoluční
sil	síla	k1gFnPc2	síla
vůči	vůči	k7c3	vůči
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
podporováno	podporovat	k5eAaImNgNnS	podporovat
duchovními	duchovní	k1gMnPc7	duchovní
a	a	k8xC	a
šlechtici	šlechtic	k1gMnPc7	šlechtic
<g/>
,	,	kIx,	,
nespokojenými	spokojený	k2eNgMnPc7d1	nespokojený
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
také	také	k9	také
podporu	podpora	k1gFnSc4	podpora
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
krvavě	krvavě	k6eAd1	krvavě
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
.	.	kIx.	.
</s>
<s>
Girondisté	girondista	k1gMnPc1	girondista
nebyli	být	k5eNaImAgMnP	být
populární	populární	k2eAgMnPc1d1	populární
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
využili	využít	k5eAaPmAgMnP	využít
jejich	jejich	k3xOp3gMnPc1	jejich
protivníci	protivník	k1gMnPc1	protivník
v	v	k7c6	v
Konventu	konvent	k1gInSc6	konvent
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
levicoví	levicový	k2eAgMnPc1d1	levicový
poslanci	poslanec	k1gMnPc1	poslanec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Robespierrem	Robespierro	k1gNnSc7	Robespierro
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1793	[number]	k4	1793
zněly	znět	k5eAaImAgInP	znět
zvony	zvon	k1gInPc1	zvon
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
Konvent	konvent	k1gInSc4	konvent
a	a	k8xC	a
předáci	předák	k1gMnPc1	předák
girondistů	girondista	k1gMnPc2	girondista
byli	být	k5eAaImAgMnP	být
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
jakobíni	jakobín	k1gMnPc1	jakobín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konventu	konvent	k1gInSc2	konvent
zřídili	zřídit	k5eAaPmAgMnP	zřídit
výbory	výbor	k1gInPc4	výbor
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
získaly	získat	k5eAaPmAgFnP	získat
i	i	k9	i
moc	moc	k6eAd1	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
řídil	řídit	k5eAaImAgInS	řídit
především	především	k9	především
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
blaho	blaho	k1gNnSc4	blaho
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
organizoval	organizovat	k5eAaBmAgInS	organizovat
zásobování	zásobování	k1gNnSc4	zásobování
<g/>
,	,	kIx,	,
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
válečnou	válečný	k2eAgFnSc4d1	válečná
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Výboru	výbor	k1gInSc6	výbor
dominovala	dominovat	k5eAaImAgFnS	dominovat
osobnost	osobnost	k1gFnSc1	osobnost
Maximiliena	Maximilien	k1gMnSc2	Maximilien
Robespierra	Robespierr	k1gMnSc2	Robespierr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Saint-Justem	Saint-Just	k1gInSc7	Saint-Just
a	a	k8xC	a
Couthonem	Couthon	k1gInSc7	Couthon
tvořili	tvořit	k5eAaImAgMnP	tvořit
neformální	formální	k2eNgInSc4d1	neformální
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
revolučním	revoluční	k2eAgInSc7d1	revoluční
orgánem	orgán	k1gInSc7	orgán
byl	být	k5eAaImAgInS	být
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
bojující	bojující	k2eAgMnSc1d1	bojující
s	s	k7c7	s
vnitřním	vnitřní	k2eAgMnSc7d1	vnitřní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
a	a	k8xC	a
dohlížející	dohlížející	k2eAgFnSc1d1	dohlížející
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Jakobíni	jakobín	k1gMnPc1	jakobín
zrušili	zrušit	k5eAaPmAgMnP	zrušit
veškeré	veškerý	k3xTgFnPc4	veškerý
feudální	feudální	k2eAgFnPc4d1	feudální
dávky	dávka	k1gFnPc4	dávka
<g/>
,	,	kIx,	,
stanovili	stanovit	k5eAaPmAgMnP	stanovit
minimální	minimální	k2eAgFnSc4d1	minimální
mzdu	mzda	k1gFnSc4	mzda
a	a	k8xC	a
maximální	maximální	k2eAgFnSc4d1	maximální
cenu	cena	k1gFnSc4	cena
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Zabavovali	zabavovat	k5eAaImAgMnP	zabavovat
přebytky	přebytek	k1gInPc4	přebytek
a	a	k8xC	a
přerozdělovali	přerozdělovat	k5eAaImAgMnP	přerozdělovat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
mezi	mezi	k7c4	mezi
16	[number]	k4	16
a	a	k8xC	a
25	[number]	k4	25
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
zreformována	zreformovat	k5eAaPmNgFnS	zreformovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
vpád	vpád	k1gInSc4	vpád
nepřátel	nepřítel	k1gMnPc2	nepřítel
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
jakobínů	jakobín	k1gMnPc2	jakobín
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
od	od	k7c2	od
září	září	k1gNnSc2	září
1793	[number]	k4	1793
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1794	[number]	k4	1794
<g/>
,	,	kIx,	,
vešlo	vejít	k5eAaPmAgNnS	vejít
pak	pak	k6eAd1	pak
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xC	jako
Teror	teror	k1gInSc4	teror
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
výsledkem	výsledek	k1gInSc7	výsledek
boje	boj	k1gInSc2	boj
dvou	dva	k4xCgFnPc2	dva
mocichtivých	mocichtivý	k2eAgFnPc2d1	mocichtivá
politických	politický	k2eAgFnPc2d1	politická
frakcí	frakce	k1gFnPc2	frakce
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
girondistů	girondista	k1gMnPc2	girondista
a	a	k8xC	a
jakobínů	jakobín	k1gMnPc2	jakobín
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
nepřátele	nepřítel	k1gMnPc4	nepřítel
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Girondisté	girondista	k1gMnPc1	girondista
byli	být	k5eAaImAgMnP	být
následně	následně	k6eAd1	následně
souzeni	soudit	k5eAaImNgMnP	soudit
Revolučním	revoluční	k2eAgInSc7d1	revoluční
tribunálem	tribunál	k1gInSc7	tribunál
a	a	k8xC	a
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
na	na	k7c6	na
gilotině	gilotina	k1gFnSc6	gilotina
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
16	[number]	k4	16
tisíc	tisíc	k4xCgInPc2	tisíc
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
až	až	k9	až
ke	k	k7c3	k
40	[number]	k4	40
tisícům	tisíc	k4xCgInPc3	tisíc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
procesech	proces	k1gInPc6	proces
s	s	k7c7	s
nepřáteli	nepřítel	k1gMnPc7	nepřítel
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1793	[number]	k4	1793
schválil	schválit	k5eAaPmAgInS	schválit
Konvent	konvent	k1gInSc1	konvent
zavedení	zavedení	k1gNnSc2	zavedení
nového	nový	k2eAgInSc2d1	nový
<g/>
,	,	kIx,	,
revolučního	revoluční	k2eAgInSc2d1	revoluční
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
začátek	začátek	k1gInSc4	začátek
letopočtu	letopočet	k1gInSc2	letopočet
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolen	k2eAgNnSc1d1	zvoleno
datum	datum	k1gNnSc1	datum
22	[number]	k4	22
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1792	[number]	k4	1792
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
kalendáře	kalendář	k1gInSc2	kalendář
se	se	k3xPyFc4	se
rok	rok	k1gInSc1	rok
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
jména	jméno	k1gNnSc2	jméno
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Fabre	Fabr	k1gInSc5	Fabr
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Églantine	Églantin	k1gMnSc5	Églantin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
měsíc	měsíc	k1gInSc4	měsíc
měl	mít	k5eAaImAgMnS	mít
30	[number]	k4	30
dní	den	k1gInPc2	den
a	a	k8xC	a
dělil	dělit	k5eAaImAgMnS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
10	[number]	k4	10
<g/>
denní	denní	k2eAgInPc4d1	denní
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
konečně	konečně	k6eAd1	konečně
zrušeno	zrušen	k2eAgNnSc4d1	zrušeno
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
dělili	dělit	k5eAaImAgMnP	dělit
na	na	k7c4	na
radikály	radikál	k1gMnPc4	radikál
(	(	kIx(	(
<g/>
hébertisty	hébertista	k1gMnPc4	hébertista
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
"	"	kIx"	"
<g/>
zběsilé	zběsilý	k2eAgNnSc4d1	zběsilé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
umírněné	umírněný	k2eAgMnPc4d1	umírněný
(	(	kIx(	(
<g/>
dantonisty	dantonista	k1gMnPc4	dantonista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umírnění	umírnění	k1gNnSc1	umírnění
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
Dantonem	Danton	k1gInSc7	Danton
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
smír	smír	k1gInSc4	smír
s	s	k7c7	s
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
a	a	k8xC	a
zmírnění	zmírnění	k1gNnSc4	zmírnění
revolučních	revoluční	k2eAgNnPc2d1	revoluční
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Radikálové	radikál	k1gMnPc1	radikál
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
revoluce	revoluce	k1gFnSc2	revoluce
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
světové	světový	k2eAgFnSc2d1	světová
revoluční	revoluční	k2eAgFnSc2d1	revoluční
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
také	také	k9	také
zestátnění	zestátnění	k1gNnSc4	zestátnění
veškeré	veškerý	k3xTgFnSc2	veškerý
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
lidový	lidový	k2eAgInSc1d1	lidový
režim	režim	k1gInSc1	režim
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
tvrdě	tvrdě	k6eAd1	tvrdě
stíhal	stíhat	k5eAaImAgMnS	stíhat
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
skončily	skončit	k5eAaPmAgFnP	skončit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
pod	pod	k7c7	pod
gilotinou	gilotina	k1gFnSc7	gilotina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1794	[number]	k4	1794
byli	být	k5eAaImAgMnP	být
popraveni	popraven	k2eAgMnPc1d1	popraven
jak	jak	k8xC	jak
vůdci	vůdce	k1gMnPc1	vůdce
hébertistů	hébertista	k1gMnPc2	hébertista
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
dantonistů	dantonista	k1gMnPc2	dantonista
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
blaho	blaho	k1gNnSc4	blaho
již	již	k6eAd1	již
nikdo	nikdo	k3yNnSc1	nikdo
nestojí	stát	k5eNaImIp3nS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Konventu	konvent	k1gInSc6	konvent
i	i	k8xC	i
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Výboru	výbor	k1gInSc6	výbor
však	však	k9	však
existovaly	existovat	k5eAaImAgFnP	existovat
významné	významný	k2eAgFnPc1d1	významná
rozpory	rozpora	k1gFnPc1	rozpora
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
jako	jako	k9	jako
náboženská	náboženský	k2eAgFnSc1d1	náboženská
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
teror	teror	k1gInSc1	teror
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
otázky	otázka	k1gFnPc4	otázka
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
fungování	fungování	k1gNnSc2	fungování
ekonomiky	ekonomika	k1gFnSc2	ekonomika
v	v	k7c6	v
krizové	krizový	k2eAgFnSc6d1	krizová
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odpůrci	odpůrce	k1gMnPc1	odpůrce
Robespierra	Robespierr	k1gInSc2	Robespierr
viděli	vidět	k5eAaImAgMnP	vidět
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
Kult	kult	k1gInSc4	kult
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Robespierre	Robespierr	k1gInSc5	Robespierr
zavedl	zavést	k5eAaPmAgInS	zavést
<g/>
,	,	kIx,	,
a	a	k8xC	a
obvinili	obvinit	k5eAaPmAgMnP	obvinit
jeho	jeho	k3xOp3gMnPc4	jeho
tvůrce	tvůrce	k1gMnPc4	tvůrce
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
stát	stát	k5eAaImF	stát
nejen	nejen	k6eAd1	nejen
diktátorem	diktátor	k1gMnSc7	diktátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
dosud	dosud	k6eAd1	dosud
nejkrvavější	krvavý	k2eAgFnSc4d3	nejkrvavější
vlnu	vlna	k1gFnSc4	vlna
teroru	teror	k1gInSc2	teror
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
neustále	neustále	k6eAd1	neustále
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
popravami	poprava	k1gFnPc7	poprava
stojí	stát	k5eAaImIp3nS	stát
"	"	kIx"	"
<g/>
krvežíznivý	krvežíznivý	k2eAgMnSc1d1	krvežíznivý
<g/>
"	"	kIx"	"
Robespierre	Robespierr	k1gMnSc5	Robespierr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
zdiskreditovali	zdiskreditovat	k5eAaPmAgMnP	zdiskreditovat
jeho	jeho	k3xOp3gFnSc4	jeho
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Thermidoru	thermidor	k1gInSc2	thermidor
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
"	"	kIx"	"
<g/>
neúplatný	úplatný	k2eNgMnSc1d1	neúplatný
<g/>
"	"	kIx"	"
Robespierre	Robespierr	k1gInSc5	Robespierr
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
přední	přední	k2eAgMnPc1d1	přední
představitelé	představitel	k1gMnPc1	představitel
režimu	režim	k1gInSc2	režim
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
neuspělo	uspět	k5eNaPmAgNnS	uspět
povstání	povstání	k1gNnSc1	povstání
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Komuny	komuna	k1gFnSc2	komuna
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
druhého	druhý	k4xOgMnSc4	druhý
dne	den	k1gInSc2	den
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Thermidoriáni	Thermidorián	k1gMnPc1	Thermidorián
popravili	popravit	k5eAaPmAgMnP	popravit
desítky	desítka	k1gFnPc4	desítka
Robespierrových	Robespierrův	k2eAgMnPc2d1	Robespierrův
příznivců	příznivec	k1gMnPc2	příznivec
a	a	k8xC	a
amnestovali	amnestovat	k5eAaBmAgMnP	amnestovat
část	část	k1gFnSc4	část
proskribovaných	proskribovaný	k2eAgMnPc2d1	proskribovaný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
postavilo	postavit	k5eAaPmAgNnS	postavit
pětičlenné	pětičlenný	k2eAgNnSc1d1	pětičlenné
Direktorium	direktorium	k1gNnSc1	direktorium
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
nastal	nastat	k5eAaPmAgInS	nastat
"	"	kIx"	"
<g/>
odliv	odliv	k1gInSc1	odliv
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
"	"	kIx"	"
a	a	k8xC	a
probíhal	probíhat	k5eAaImAgInS	probíhat
také	také	k9	také
tzv.	tzv.	kA	tzv.
bílý	bílý	k2eAgInSc1d1	bílý
teror	teror	k1gInSc1	teror
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
obětí	oběť	k1gFnSc7	oběť
byli	být	k5eAaImAgMnP	být
především	především	k9	především
jakobínští	jakobínský	k2eAgMnPc1d1	jakobínský
aktivisté	aktivista	k1gMnPc1	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
vlády	vláda	k1gFnSc2	vláda
Direktoria	direktorium	k1gNnSc2	direktorium
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
konec	konec	k1gInSc1	konec
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
některých	některý	k3yIgInPc2	některý
revolučních	revoluční	k2eAgInPc2d1	revoluční
výdobytků	výdobytek	k1gInPc2	výdobytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
situaci	situace	k1gFnSc6	situace
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
nestability	nestabilita	k1gFnSc2	nestabilita
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
při	při	k7c6	při
brumairovém	brumairový	k2eAgInSc6d1	brumairový
převratu	převrat	k1gInSc6	převrat
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
bývalý	bývalý	k2eAgMnSc1d1	bývalý
dělostřelec	dělostřelec	k1gMnSc1	dělostřelec
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
