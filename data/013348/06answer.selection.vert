<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gFnSc1	International
Organization	Organization	k1gInSc1	Organization
for	forum	k1gNnPc2	forum
Standardization	Standardization	k1gInSc1	Standardization
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
světovou	světový	k2eAgFnSc7d1	světová
federací	federace	k1gFnSc7	federace
národních	národní	k2eAgFnPc2d1	národní
normalizačních	normalizační	k2eAgFnPc2d1	normalizační
organizací	organizace	k1gFnPc2	organizace
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
