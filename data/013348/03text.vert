<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gFnSc1	International
Organization	Organization	k1gInSc1	Organization
for	forum	k1gNnPc2	forum
Standardization	Standardization	k1gInSc1	Standardization
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
světovou	světový	k2eAgFnSc7d1	světová
federací	federace	k1gFnSc7	federace
národních	národní	k2eAgFnPc2d1	národní
normalizačních	normalizační	k2eAgFnPc2d1	normalizační
organizací	organizace	k1gFnPc2	organizace
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
omylem	omyl	k1gInSc7	omyl
považovat	považovat	k5eAaImF	považovat
ISO	ISO	kA	ISO
za	za	k7c4	za
zkratku	zkratka	k1gFnSc4	zkratka
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
ἴ	ἴ	k?	ἴ
(	(	kIx(	(
<g/>
isos	isos	k1gInSc1	isos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc4d1	znamenající
stejný	stejný	k2eAgInSc4d1	stejný
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
shodného	shodný	k2eAgNnSc2d1	shodné
pojmenování	pojmenování	k1gNnSc2	pojmenování
téhož	týž	k3xTgNnSc2	týž
bez	bez	k7c2	bez
jazykových	jazykový	k2eAgInPc2d1	jazykový
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
organizace	organizace	k1gFnSc2	organizace
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dal	dát	k5eAaPmAgMnS	dát
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
backronym	backronym	k1gInSc4	backronym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
tvorbou	tvorba	k1gFnSc7	tvorba
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
norem	norma	k1gFnPc2	norma
ISO	ISO	kA	ISO
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
dokumentů	dokument	k1gInPc2	dokument
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
normalizace	normalizace	k1gFnSc2	normalizace
kromě	kromě	k7c2	kromě
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
TS	ts	k0	ts
–	–	k?	–
technické	technický	k2eAgFnSc2d1	technická
specifikace	specifikace	k1gFnSc2	specifikace
</s>
</p>
<p>
<s>
TR	TR	kA	TR
–	–	k?	–
technické	technický	k2eAgFnSc2d1	technická
zprávy	zpráva	k1gFnSc2	zpráva
</s>
</p>
<p>
<s>
PAS	pas	k1gInSc1	pas
–	–	k?	–
veřejně	veřejně	k6eAd1	veřejně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
specifikace	specifikace	k1gFnPc1	specifikace
</s>
</p>
<p>
<s>
TTA	TTA	kA	TTA
–	–	k?	–
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
technických	technický	k2eAgInPc6d1	technický
trendech	trend	k1gInPc6	trend
</s>
</p>
<p>
<s>
IWA	IWA	kA	IWA
–	–	k?	–
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
pracovní	pracovní	k2eAgFnSc2d1	pracovní
konference	konference	k1gFnSc2	konference
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
Pokyny	pokyn	k1gInPc1	pokyn
ISOV	ISOV	kA	ISOV
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
existovalo	existovat	k5eAaImAgNnS	existovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18	[number]	k4	18
000	[number]	k4	000
norem	norma	k1gFnPc2	norma
ISO	ISO	kA	ISO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
13	[number]	k4	13
362	[number]	k4	362
norem	norma	k1gFnPc2	norma
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
494	[number]	k4	494
technických	technický	k2eAgFnPc2d1	technická
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
TR	TR	kA	TR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
dohody	dohoda	k1gFnSc2	dohoda
IWA	IWA	kA	IWA
<g/>
,	,	kIx,	,
9	[number]	k4	9
PAS	pas	k1gNnSc2	pas
<g/>
,	,	kIx,	,
118	[number]	k4	118
TS	ts	k0	ts
<g/>
,	,	kIx,	,
4	[number]	k4	4
dohody	dohoda	k1gFnSc2	dohoda
TTA	TTA	kA	TTA
a	a	k8xC	a
39	[number]	k4	39
pokynů	pokyn	k1gInPc2	pokyn
ISO	ISO	kA	ISO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Technické	technický	k2eAgFnSc2d1	technická
práce	práce	k1gFnSc2	práce
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
210	[number]	k4	210
technických	technický	k2eAgFnPc2d1	technická
komisí	komise	k1gFnPc2	komise
(	(	kIx(	(
<g/>
TC	tc	k0	tc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
519	[number]	k4	519
subkomisí	subkomise	k1gFnPc2	subkomise
(	(	kIx(	(
<g/>
SC	SC	kA	SC
<g/>
)	)	kIx)	)
a	a	k8xC	a
</s>
</p>
<p>
<s>
2	[number]	k4	2
443	[number]	k4	443
pracovních	pracovní	k2eAgFnPc2d1	pracovní
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
WG	WG	kA	WG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pracích	práce	k1gFnPc6	práce
technických	technický	k2eAgFnPc2d1	technická
komisí	komise	k1gFnPc2	komise
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
podílet	podílet	k5eAaImF	podílet
jako	jako	k9	jako
aktivní	aktivní	k2eAgMnPc1d1	aktivní
členové	člen	k1gMnPc1	člen
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
–	–	k?	–
<g/>
členové	člen	k1gMnPc1	člen
–	–	k?	–
participating	participating	k1gInSc1	participating
members	members	k1gInSc1	members
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
povinnost	povinnost	k1gFnSc4	povinnost
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
zasedání	zasedání	k1gNnSc1	zasedání
a	a	k8xC	a
hlasovat	hlasovat	k5eAaImF	hlasovat
k	k	k7c3	k
dokumentům	dokument	k1gInPc3	dokument
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
(	(	kIx(	(
<g/>
O	o	k7c6	o
<g/>
–	–	k?	–
<g/>
členové	člen	k1gMnPc1	člen
–	–	k?	–
observer	observero	k1gNnPc2	observero
members	members	k1gInSc1	members
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dostávají	dostávat	k5eAaImIp3nP	dostávat
pracovní	pracovní	k2eAgInPc4d1	pracovní
dokumenty	dokument	k1gInPc4	dokument
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
povinnost	povinnost	k1gFnSc4	povinnost
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
zasedání	zasedání	k1gNnSc1	zasedání
a	a	k8xC	a
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členství	členství	k1gNnSc1	členství
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
celkem	celkem	k6eAd1	celkem
163	[number]	k4	163
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
110	[number]	k4	110
řádných	řádný	k2eAgInPc2d1	řádný
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
43	[number]	k4	43
korespondenčních	korespondenční	k2eAgMnPc2d1	korespondenční
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
10	[number]	k4	10
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
členství	členství	k1gNnSc4	členství
<g/>
.	.	kIx.	.
<g/>
Členy	člen	k1gInPc7	člen
ISO	ISO	kA	ISO
jsou	být	k5eAaImIp3nP	být
národní	národní	k2eAgFnPc1d1	národní
normalizační	normalizační	k2eAgFnPc1d1	normalizační
organizace	organizace	k1gFnPc1	organizace
zastupující	zastupující	k2eAgFnSc4d1	zastupující
normalizaci	normalizace	k1gFnSc4	normalizace
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
povinnosti	povinnost	k1gFnPc4	povinnost
členů	člen	k1gMnPc2	člen
patří	patřit	k5eAaImIp3nS	patřit
informovat	informovat	k5eAaBmF	informovat
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
organizace	organizace	k1gFnPc4	organizace
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
o	o	k7c6	o
nových	nový	k2eAgFnPc6d1	nová
normalizačních	normalizační	k2eAgFnPc6d1	normalizační
aktivitách	aktivita	k1gFnPc6	aktivita
<g/>
,	,	kIx,	,
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
za	za	k7c4	za
danou	daný	k2eAgFnSc4d1	daná
zemi	zem	k1gFnSc4	zem
jednotné	jednotný	k2eAgNnSc1d1	jednotné
stanovisko	stanovisko	k1gNnSc1	stanovisko
k	k	k7c3	k
předkládaným	předkládaný	k2eAgInPc3d1	předkládaný
dokumentům	dokument	k1gInPc3	dokument
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
činnost	činnost	k1gFnSc4	činnost
ISO	ISO	kA	ISO
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
ISO	ISO	kA	ISO
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
účastnit	účastnit	k5eAaImF	účastnit
se	s	k7c7	s
prací	práce	k1gFnSc7	práce
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
technické	technický	k2eAgFnSc6d1	technická
komisi	komise	k1gFnSc6	komise
a	a	k8xC	a
vykonávat	vykonávat	k5eAaImF	vykonávat
veškerá	veškerý	k3xTgNnPc4	veškerý
hlasovací	hlasovací	k2eAgNnPc4d1	hlasovací
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
ISO	ISO	kA	ISO
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupit	k5eAaPmNgMnP	zastoupit
na	na	k7c6	na
Generálním	generální	k2eAgNnSc6d1	generální
zasedání	zasedání	k1gNnSc6	zasedání
ISO	ISO	kA	ISO
<g/>
.	.	kIx.	.
</s>
<s>
Korespondenční	korespondenční	k2eAgMnSc1d1	korespondenční
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
organizace	organizace	k1gFnSc1	organizace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
plně	plně	k6eAd1	plně
nerozvinula	rozvinout	k5eNaPmAgFnS	rozvinout
národní	národní	k2eAgFnSc1d1	národní
normalizační	normalizační	k2eAgFnSc1d1	normalizační
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Korespondenční	korespondenční	k2eAgMnSc1d1	korespondenční
člen	člen	k1gMnSc1	člen
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
nepodílí	podílet	k5eNaImIp3nP	podílet
na	na	k7c6	na
technických	technický	k2eAgFnPc6d1	technická
a	a	k8xC	a
strategických	strategický	k2eAgFnPc6d1	strategická
pracích	práce	k1gFnPc6	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
informován	informovat	k5eAaBmNgMnS	informovat
o	o	k7c6	o
pracích	prak	k1gInPc6	prak
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
kategorie	kategorie	k1gFnSc1	kategorie
–	–	k?	–
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
členství	členství	k1gNnSc4	členství
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc7d1	malá
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c4	na
členství	členství	k1gNnSc4	členství
platí	platit	k5eAaImIp3nP	platit
snížené	snížený	k2eAgInPc1d1	snížený
poplatky	poplatek	k1gInPc1	poplatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zastupující	zastupující	k2eAgFnSc7d1	zastupující
institucí	instituce	k1gFnSc7	instituce
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
-	-	kIx~	-
jakožto	jakožto	k8xS	jakožto
řádného	řádný	k2eAgMnSc4d1	řádný
člena	člen	k1gMnSc4	člen
ISO	ISO	kA	ISO
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
(	(	kIx(	(
<g/>
ÚNMZ	ÚNMZ	kA	ÚNMZ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GS1	GS1	k4	GS1
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ISO	ISO	kA	ISO
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgInS	Internationat
Organization	Organization	k1gInSc1	Organization
for	forum	k1gNnPc2	forum
Standardization	Standardization	k1gInSc1	Standardization
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
)	)	kIx)	)
</s>
</p>
