<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
slunce	slunce	k1gNnSc2	slunce
ale	ale	k8xC	ale
též	též	k9	též
pšenici	pšenice	k1gFnSc4	pšenice
<g/>
,	,	kIx,	,
litevské	litevský	k2eAgNnSc4d1	litevské
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
získanou	získaný	k2eAgFnSc4d1	získaná
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
rodnou	rodný	k2eAgFnSc4d1	rodná
zem	zem	k1gFnSc4	zem
ale	ale	k8xC	ale
i	i	k9	i
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
pak	pak	k6eAd1	pak
představuje	představovat	k5eAaImIp3nS	představovat
krev	krev	k1gFnSc4	krev
Litevců	Litevec	k1gMnPc2	Litevec
prolitou	prolitý	k2eAgFnSc4d1	prolitá
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
odvahu	odvaha	k1gFnSc4	odvaha
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
nebo	nebo	k8xC	nebo
také	také	k9	také
suverénní	suverénní	k2eAgFnSc1d1	suverénní
moc	moc	k1gFnSc1	moc
<g />
.	.	kIx.	.
</s>
