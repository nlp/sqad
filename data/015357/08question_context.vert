<s>
Vlajka	vlajka	k1gFnSc1
Litvy	Litva	k1gFnSc2
je	být	k5eAaImIp3nS
sestavena	sestavit	k5eAaPmNgFnS
ze	z	k7c2
tří	tři	k4xCgInPc2
horizontálních	horizontální	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
<g/>
:	:	kIx,
žlutého	žlutý	k2eAgMnSc2d1
<g/>
,	,	kIx,
zeleného	zelený	k2eAgInSc2d1
a	a	k8xC
červeného	červený	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vlajka	vlajka	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
změna	změna	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
však	však	k9
změněn	změnit	k5eAaPmNgInS
pouze	pouze	k6eAd1
poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>