Žlutá	žlutý	k2eAgFnSc1d1	žlutá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
slunce	slunce	k1gNnSc2	slunce
ale	ale	k8xC	ale
též	též	k9	též
pšenici	pšenice	k1gFnSc4	pšenice
<g/>
,	,	kIx,	,
litevské	litevský	k2eAgNnSc4d1	litevské
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
získanou	získaný	k2eAgFnSc4d1	získaná
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
rodnou	rodný	k2eAgFnSc4d1	rodná
zem	zem	k1gFnSc4	zem
ale	ale	k8xC	ale
i	i	k9	i
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
