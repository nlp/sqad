<p>
<s>
Sklípkan	sklípkan	k1gMnSc1	sklípkan
největší	veliký	k2eAgMnSc1d3	veliký
(	(	kIx(	(
<g/>
Theraphosa	Theraphosa	k1gFnSc1	Theraphosa
blondi	blondit	k5eAaPmRp2nS	blondit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
pozemního	pozemní	k2eAgMnSc2d1	pozemní
sklípkana	sklípkan	k1gMnSc2	sklípkan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
<g/>
,	,	kIx,	,
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
zástupců	zástupce	k1gMnPc2	zástupce
rodu	rod	k1gInSc2	rod
Theraphosa	Theraphosa	k1gFnSc1	Theraphosa
společně	společně	k6eAd1	společně
s	s	k7c7	s
T.	T.	kA	T.
stirmi	stir	k1gFnPc7	stir
a	a	k8xC	a
T.	T.	kA	T.
apophysis	apophysis	k1gInSc1	apophysis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
nohou	noha	k1gFnPc2	noha
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
28	[number]	k4	28
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
může	moct	k5eAaImIp3nS	moct
měřit	měřit	k5eAaImF	měřit
až	až	k9	až
11,9	[number]	k4	11,9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
pavouků	pavouk	k1gMnPc2	pavouk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
podle	podle	k7c2	podle
rozpětí	rozpětí	k1gNnSc2	rozpětí
nohou	noha	k1gFnSc7	noha
(	(	kIx(	(
<g/>
podobné	podobný	k2eAgFnPc4d1	podobná
velikosti	velikost	k1gFnPc4	velikost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
i	i	k9	i
maloočka	maloočka	k1gFnSc1	maloočka
Heteropoda	Heteropoda	k1gFnSc1	Heteropoda
maxima	maxima	k1gFnSc1	maxima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Pavouk	pavouk	k1gMnSc1	pavouk
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
těla	tělo	k1gNnSc2	tělo
až	až	k9	až
11,9	[number]	k4	11,9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
bývají	bývat	k5eAaImIp3nP	bývat
rezavohnědí	rezavohnědý	k2eAgMnPc1d1	rezavohnědý
až	až	k9	až
černí	černý	k1gMnPc1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
kloubech	kloub	k1gInPc6	kloub
bývají	bývat	k5eAaImIp3nP	bývat
ošoupaní	ošoupaný	k2eAgMnPc1d1	ošoupaný
až	až	k8xS	až
lesklí	lesklý	k2eAgMnPc1d1	lesklý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svlečení	svlečení	k1gNnSc6	svlečení
bývají	bývat	k5eAaImIp3nP	bývat
černohnědí	černohnědý	k2eAgMnPc1d1	černohnědý
s	s	k7c7	s
podélnými	podélný	k2eAgInPc7d1	podélný
načervenalými	načervenalý	k2eAgInPc7d1	načervenalý
pásky	pásek	k1gInPc7	pásek
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Dospívají	dospívat	k5eAaImIp3nP	dospívat
za	za	k7c4	za
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
roste	růst	k5eAaImIp3nS	růst
neobvykle	obvykle	k6eNd1	obvykle
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
během	během	k7c2	během
2	[number]	k4	2
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přerůst	přerůst	k5eAaPmF	přerůst
většinu	většina	k1gFnSc4	většina
druhů	druh	k1gInPc2	druh
sklípkanů	sklípkan	k1gMnPc2	sklípkan
<g/>
.	.	kIx.	.
</s>
<s>
Brání	bránit	k5eAaImIp3nS	bránit
se	se	k3xPyFc4	se
vyčesáváním	vyčesávání	k1gNnSc7	vyčesávání
žahavých	žahavý	k2eAgInPc2d1	žahavý
chloupků	chloupek	k1gInPc2	chloupek
ze	z	k7c2	z
zadečku	zadeček	k1gInSc2	zadeček
(	(	kIx(	(
<g/>
abdomen	abdomen	k1gInSc1	abdomen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
silné	silný	k2eAgNnSc4d1	silné
svědění	svědění	k1gNnSc4	svědění
pokožky	pokožka	k1gFnSc2	pokožka
nebo	nebo	k8xC	nebo
i	i	k9	i
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
kašlání	kašlání	k1gNnSc1	kašlání
<g/>
.	.	kIx.	.
</s>
<s>
T.	T.	kA	T.
blondi	blond	k1gMnPc1	blond
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
chelicerami	chelicera	k1gFnPc7	chelicera
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
kousnutí	kousnutí	k1gNnSc3	kousnutí
je	být	k5eAaImIp3nS	být
mírněji	mírně	k6eAd2	mírně
bolestivější	bolestivý	k2eAgNnSc1d2	bolestivější
než	než	k8xS	než
bodnutí	bodnutí	k1gNnSc1	bodnutí
vosou	vosa	k1gFnSc7	vosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
potravou	potrava	k1gFnSc7	potrava
sklípkana	sklípkan	k1gMnSc2	sklípkan
chovaného	chovaný	k2eAgMnSc2d1	chovaný
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
cvrčci	cvrček	k1gMnPc1	cvrček
<g/>
,	,	kIx,	,
sarančata	saranče	k1gNnPc1	saranče
<g/>
,	,	kIx,	,
červi	červ	k1gMnPc1	červ
a	a	k8xC	a
švábi	šváb	k1gMnPc1	šváb
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
sklípkan	sklípkan	k1gMnSc1	sklípkan
je	být	k5eAaImIp3nS	být
schopen	schopit	k5eAaPmNgMnS	schopit
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
menší	malý	k2eAgMnPc4d2	menší
obratlovce	obratlovec	k1gMnPc4	obratlovec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hlodavce	hlodavec	k1gMnSc4	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
potravu	potrava	k1gFnSc4	potrava
nedoporučují	doporučovat	k5eNaImIp3nP	doporučovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přílišného	přílišný	k2eAgNnSc2d1	přílišné
množství	množství	k1gNnSc2	množství
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
hlodavce	hlodavec	k1gMnSc4	hlodavec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
sklípkana	sklípkan	k1gMnSc2	sklípkan
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
loví	lovit	k5eAaImIp3nP	lovit
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ulovit	ulovit	k5eAaPmF	ulovit
-	-	kIx~	-
žáby	žába	k1gFnPc4	žába
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc4	červ
<g/>
,	,	kIx,	,
cvrčky	cvrček	k1gMnPc4	cvrček
<g/>
,	,	kIx,	,
kudlanky	kudlanka	k1gFnPc4	kudlanka
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc4	myš
<g/>
,	,	kIx,	,
křečky	křeček	k1gMnPc4	křeček
a	a	k8xC	a
podobné	podobný	k2eAgMnPc4d1	podobný
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gInSc1	jeho
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
Goliath	Goliath	k1gMnSc1	Goliath
birdeater	birdeater	k1gMnSc1	birdeater
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
pavouk	pavouk	k1gMnSc1	pavouk
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
sklípkani	sklípkan	k1gMnPc1	sklípkan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kokonu	kokon	k1gInSc6	kokon
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
30	[number]	k4	30
-	-	kIx~	-
80	[number]	k4	80
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
po	po	k7c6	po
páření	páření	k1gNnSc6	páření
snaží	snažit	k5eAaImIp3nS	snažit
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nestal	stát	k5eNaPmAgMnS	stát
kořistí	kořist	k1gFnSc7	kořist
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
anglicke	anglicke	k6eAd1	anglicke
verzi	verze	k1gFnSc4	verze
této	tento	k3xDgFnSc2	tento
wiki	wik	k1gFnSc2	wik
stránky	stránka	k1gFnSc2	stránka
je	být	k5eAaImIp3nS	být
však	však	k9	však
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgMnPc3d1	jiný
druhům	druh	k1gMnPc3	druh
samec	samec	k1gInSc4	samec
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sklípkan	sklípkan	k1gMnSc1	sklípkan
největší	veliký	k2eAgMnSc1d3	veliký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Theraphosa	Theraphosa	k1gFnSc1	Theraphosa
blondi	blondit	k5eAaPmRp2nS	blondit
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
