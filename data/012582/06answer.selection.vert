<s>
Sklípkan	sklípkan	k1gMnSc1	sklípkan
největší	veliký	k2eAgMnSc1d3	veliký
(	(	kIx(	(
<g/>
Theraphosa	Theraphosa	k1gFnSc1	Theraphosa
blondi	blondit	k5eAaPmRp2nS	blondit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
pozemního	pozemní	k2eAgMnSc2d1	pozemní
sklípkana	sklípkan	k1gMnSc2	sklípkan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
<g/>
,	,	kIx,	,
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
.	.	kIx.	.
</s>
