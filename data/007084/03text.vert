<s>
Symbol	symbol	k1gInSc1	symbol
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
též	též	k9	též
značka	značka	k1gFnSc1	značka
prvku	prvek	k1gInSc2	prvek
je	být	k5eAaImIp3nS	být
grafické	grafický	k2eAgNnSc4d1	grafické
znázornění	znázornění	k1gNnSc4	znázornění
prvku	prvek	k1gInSc2	prvek
nebo	nebo	k8xC	nebo
atomu	atom	k1gInSc2	atom
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
v	v	k7c6	v
textu	text	k1gInSc6	text
a	a	k8xC	a
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jedním	jeden	k4xCgInSc7	jeden
až	až	k6eAd1	až
třemi	tři	k4xCgInPc7	tři
znaky	znak	k1gInPc7	znak
latinky	latinka	k1gFnSc2	latinka
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
)	)	kIx)	)
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
případně	případně	k6eAd1	případně
třetí	třetí	k4xOgFnSc7	třetí
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgFnPc1d1	psána
vždy	vždy	k6eAd1	vždy
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
jsou	být	k5eAaImIp3nP	být
odvozeny	odvozen	k2eAgInPc1d1	odvozen
od	od	k7c2	od
latinských	latinský	k2eAgInPc2d1	latinský
názvů	název	k1gInPc2	název
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
kodifikuje	kodifikovat	k5eAaBmIp3nS	kodifikovat
je	on	k3xPp3gFnPc4	on
organizace	organizace	k1gFnPc4	organizace
IUPAC	IUPAC	kA	IUPAC
(	(	kIx(	(
<g/>
International	International	k1gMnSc2	International
Union	union	k1gInSc1	union
for	forum	k1gNnPc2	forum
Pure	Pur	k1gFnSc2	Pur
and	and	k?	and
Applied	Applied	k1gMnSc1	Applied
Chemistry	Chemistr	k1gMnPc4	Chemistr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
transuranů	transuran	k1gInPc2	transuran
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgInP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přidělováno	přidělován	k2eAgNnSc1d1	přidělováno
systematické	systematický	k2eAgNnSc1d1	systematické
provizorní	provizorní	k2eAgNnSc1d1	provizorní
jméno	jméno	k1gNnSc1	jméno
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
latinských	latinský	k2eAgFnPc2d1	Latinská
číslovek	číslovka	k1gFnPc2	číslovka
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
protonovému	protonový	k2eAgNnSc3d1	protonové
číslu	číslo	k1gNnSc3	číslo
příslušného	příslušný	k2eAgInSc2d1	příslušný
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
písmen	písmeno	k1gNnPc2	písmeno
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
prvek	prvek	k1gInSc1	prvek
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
119	[number]	k4	119
má	mít	k5eAaImIp3nS	mít
provizorní	provizorní	k2eAgInSc4d1	provizorní
název	název	k1gInSc4	název
Ununennium	Ununennium	k1gNnSc1	Ununennium
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
Uue	Uue	k1gFnSc1	Uue
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
prvku	prvek	k1gInSc2	prvek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
horními	horní	k2eAgFnPc7d1	horní
a	a	k8xC	a
dolními	dolní	k2eAgInPc7d1	dolní
číselnými	číselný	k2eAgInPc7d1	číselný
indexy	index	k1gInPc7	index
<g/>
,	,	kIx,	,
udávajícími	udávající	k2eAgFnPc7d1	udávající
dodatečné	dodatečný	k2eAgInPc4d1	dodatečný
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
stavu	stav	k1gInSc6	stav
určitého	určitý	k2eAgInSc2d1	určitý
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
atomů	atom	k1gInPc2	atom
<g/>
)	)	kIx)	)
prvku	prvek	k1gInSc2	prvek
podle	podle	k7c2	podle
následujícího	následující	k2eAgNnSc2d1	následující
schematu	schema	k1gNnSc2	schema
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Xx	Xx	k1gFnSc1	Xx
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
Xx	Xx	k1gFnSc1	Xx
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
Xx	Xx	k1gFnSc1	Xx
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgInSc4d1	vlastní
symbol	symbol	k1gInSc4	symbol
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
indexů	index	k1gInPc2	index
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
h	h	k?	h
–	–	k?	–
hmotové	hmotový	k2eAgNnSc4d1	hmotové
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
nukleonové	nukleonový	k2eAgNnSc4d1	nukleonový
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc1	počet
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
tohoto	tento	k3xDgInSc2	tento
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
tedy	tedy	k9	tedy
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
izotopy	izotop	k1gInPc4	izotop
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
<g/>
;	;	kIx,	;
a	a	k8xC	a
–	–	k?	–
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
protonové	protonový	k2eAgNnSc4d1	protonové
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
jádře	jádro	k1gNnSc6	jádro
tohoto	tento	k3xDgInSc2	tento
atomu	atom	k1gInSc2	atom
<g/>
;	;	kIx,	;
n	n	k0	n
–	–	k?	–
náboj	náboj	k1gInSc1	náboj
iontu	ion	k1gInSc2	ion
udává	udávat	k5eAaImIp3nS	udávat
výsledný	výsledný	k2eAgInSc1d1	výsledný
náboj	náboj	k1gInSc1	náboj
iontu	ion	k1gInSc2	ion
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
z	z	k7c2	z
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
atomů	atom	k1gInPc2	atom
odevzdáním	odevzdání	k1gNnSc7	odevzdání
nebo	nebo	k8xC	nebo
přijetím	přijetí	k1gNnSc7	přijetí
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
násobcích	násobek	k1gInPc6	násobek
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
počet	počet	k1gInSc4	počet
atomů	atom	k1gInPc2	atom
různý	různý	k2eAgInSc4d1	různý
od	od	k7c2	od
1	[number]	k4	1
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
náboj	náboj	k1gInSc1	náboj
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
souhrnně	souhrnně	k6eAd1	souhrnně
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
množině	množina	k1gFnSc3	množina
atomů	atom	k1gInPc2	atom
<g/>
;	;	kIx,	;
znaménko	znaménko	k1gNnSc1	znaménko
plus	plus	k1gNnSc1	plus
nebo	nebo	k8xC	nebo
mínus	mínus	k1gInSc1	mínus
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
až	až	k9	až
za	za	k7c4	za
číselný	číselný	k2eAgInSc4d1	číselný
údaj	údaj	k1gInSc4	údaj
<g/>
;	;	kIx,	;
p	p	k?	p
–	–	k?	–
počet	počet	k1gInSc1	počet
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
nebo	nebo	k8xC	nebo
radikálu	radikál	k1gInSc6	radikál
nebo	nebo	k8xC	nebo
víceatomovém	víceatomový	k2eAgInSc6d1	víceatomový
iontu	ion	k1gInSc6	ion
resp.	resp.	kA	resp.
obecně	obecně	k6eAd1	obecně
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
disulfidový	disulfidový	k2eAgInSc4d1	disulfidový
anion	anion	k1gInSc4	anion
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
zápornými	záporný	k2eAgInPc7d1	záporný
náboji	náboj	k1gInPc7	náboj
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
dvěma	dva	k4xCgFnPc7	dva
atomy	atom	k1gInPc1	atom
síry	síra	k1gFnSc2	síra
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
16	[number]	k4	16
a	a	k8xC	a
hmotovým	hmotový	k2eAgNnSc7d1	hmotové
číslem	číslo	k1gNnSc7	číslo
32	[number]	k4	32
se	se	k3xPyFc4	se
zapíše	zapsat	k5eAaPmIp3nS	zapsat
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
16	[number]	k4	16
:	:	kIx,	:
:	:	kIx,	:
32	[number]	k4	32
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
16	[number]	k4	16
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
32	[number]	k4	32
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Indexy	index	k1gInPc4	index
umístěné	umístěný	k2eAgInPc4d1	umístěný
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
symbolu	symbol	k1gInSc2	symbol
prvku	prvek	k1gInSc2	prvek
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
strukturu	struktura	k1gFnSc4	struktura
jádra	jádro	k1gNnSc2	jádro
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
zápis	zápis	k1gInSc4	zápis
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
identifikaci	identifikace	k1gFnSc6	identifikace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
izotopů	izotop	k1gInPc2	izotop
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
izotopy	izotop	k1gInPc4	izotop
jednoho	jeden	k4xCgInSc2	jeden
prvku	prvek	k1gInSc2	prvek
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
stejný	stejný	k2eAgInSc1d1	stejný
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
jen	jen	k9	jen
levý	levý	k2eAgInSc4d1	levý
horní	horní	k2eAgInSc4d1	horní
index	index	k1gInSc4	index
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
tři	tři	k4xCgInPc1	tři
různé	různý	k2eAgInPc1d1	různý
izotopy	izotop	k1gInPc1	izotop
mají	mít	k5eAaImIp3nP	mít
samostatné	samostatný	k2eAgInPc1d1	samostatný
názvy	název	k1gInPc1	název
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
označovány	označován	k2eAgInPc4d1	označován
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
symboly	symbol	k1gInPc7	symbol
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
protium	protium	k1gNnSc1	protium
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
1	[number]	k4	1
<g/>
;	;	kIx,	;
deuterium	deuterium	k1gNnSc1	deuterium
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
2	[number]	k4	2
nebo	nebo	k8xC	nebo
D	D	kA	D
<g/>
;	;	kIx,	;
tritium	tritium	k1gNnSc1	tritium
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
3	[number]	k4	3
nebo	nebo	k8xC	nebo
T.	T.	kA	T.
Přehled	přehled	k1gInSc1	přehled
symbolů	symbol	k1gInPc2	symbol
všech	všecek	k3xTgInPc2	všecek
prvků	prvek	k1gInPc2	prvek
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
