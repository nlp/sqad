<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jedním	jeden	k4xCgInSc7	jeden
až	až	k6eAd1	až
třemi	tři	k4xCgInPc7	tři
znaky	znak	k1gInPc7	znak
latinky	latinka	k1gFnSc2	latinka
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
)	)	kIx)	)
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
případně	případně	k6eAd1	případně
třetí	třetí	k4xOgFnSc7	třetí
jsou	být	k5eAaImIp3nP	být
psány	psán	k2eAgFnPc1d1	psána
vždy	vždy	k6eAd1	vždy
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
