<s>
Cyril	Cyril	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
složitější	složitý	k2eAgNnSc4d2
a	a	k8xC
starší	starý	k2eAgNnSc4d2
písmo	písmo	k1gNnSc4
hlaholici	hlaholice	k1gFnSc4
v	v	k7c6
60	[number]	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
9	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
cyrilice	cyrilice	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
až	až	k6eAd1
koncem	koncem	k7c2
9	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
(	(	kIx(
<g/>
Cyrillus	Cyrillus	k1gMnSc1
et	et	k?
Methudius	Methudius	k1gMnSc1
inventis	inventis	k1gFnSc2
Bulgarorum	Bulgarorum	k1gInSc1
litteris	litteris	k1gFnSc4
<g/>
)	)	kIx)
jako	jako	k8xS
výraz	výraz	k1gInSc1
jasného	jasný	k2eAgInSc2d1
příklonu	příklon	k1gInSc2
k	k	k7c3
byzantské	byzantský	k2eAgFnSc3d1
kulturní	kulturní	k2eAgFnSc3d1
sféře	sféra	k1gFnSc3
<g/>
,	,	kIx,
o	o	k7c4
nějž	jenž	k3xRgMnSc4
usilovali	usilovat	k5eAaImAgMnP
zejména	zejména	k9
řečtí	řecký	k2eAgMnPc1d1
duchovní	duchovní	k1gMnPc1
na	na	k7c6
slovanských	slovanský	k2eAgNnPc6d1
územích	území	k1gNnPc6
<g/>
.	.	kIx.
</s>