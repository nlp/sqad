<s>
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Apple	Apple	kA	Apple
Computer	computer	k1gInSc1	computer
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Cupertino	Cupertin	k2eAgNnSc4d1	Cupertino
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
hardware	hardware	k1gInSc4	hardware
a	a	k8xC	a
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
nejznámějšími	známý	k2eAgInPc7d3	nejznámější
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
řada	řada	k1gFnSc1	řada
počítačů	počítač	k1gMnPc2	počítač
Mac	Mac	kA	Mac
<g/>
,	,	kIx,	,
sada	sada	k1gFnSc1	sada
kapesních	kapesní	k2eAgMnPc2d1	kapesní
počítačů	počítač	k1gMnPc2	počítač
a	a	k8xC	a
MP3	MP3	k1gMnPc2	MP3
přehrávačů	přehrávač	k1gInPc2	přehrávač
iPod	iPoda	k1gFnPc2	iPoda
<g/>
,	,	kIx,	,
chytrý	chytrý	k2eAgInSc4d1	chytrý
telefon	telefon	k1gInSc4	telefon
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
tablet	tablet	k1gInSc4	tablet
iPad	iPad	k6eAd1	iPad
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
hodinky	hodinka	k1gFnPc4	hodinka
Apple	Apple	kA	Apple
Watch	Watch	k1gInSc1	Watch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
Apple	Apple	kA	Apple
je	být	k5eAaImIp3nS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
standardně	standardně	k6eAd1	standardně
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
macOS	macOS	k?	macOS
a	a	k8xC	a
dotyková	dotykový	k2eAgNnPc4d1	dotykové
zařízení	zařízení	k1gNnPc4	zařízení
jsou	být	k5eAaImIp3nP	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
známým	známý	k2eAgInSc7d1	známý
softwarem	software	k1gInSc7	software
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
a	a	k8xC	a
uspořádávání	uspořádávání	k1gNnSc4	uspořádávání
hudební	hudební	k2eAgFnSc2d1	hudební
kolekce	kolekce	k1gFnSc2	kolekce
<g/>
;	;	kIx,	;
iTunes	iTunes	k1gMnSc1	iTunes
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
sada	sada	k1gFnSc1	sada
kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
balíku	balík	k1gInSc2	balík
iWork	iWork	k1gInSc1	iWork
a	a	k8xC	a
sada	sada	k1gFnSc1	sada
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
fotek	fotka	k1gFnPc2	fotka
a	a	k8xC	a
videí	video	k1gNnPc2	video
iLife	iLif	k1gMnSc5	iLif
<g/>
.	.	kIx.	.
</s>
<s>
Apple	Apple	kA	Apple
ale	ale	k8xC	ale
také	také	k9	také
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
zavádí	zavádět	k5eAaImIp3nS	zavádět
produkty	produkt	k1gInPc4	produkt
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
řada	řada	k1gFnSc1	řada
multimediálních	multimediální	k2eAgInPc2d1	multimediální
přehrávačů	přehrávač	k1gInPc2	přehrávač
iPod	iPod	k6eAd1	iPod
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
miliony	milion	k4xCgInPc7	milion
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
nejprodávanějším	prodávaný	k2eAgFnPc3d3	nejprodávanější
MP3	MP3	k1gFnPc4	MP3
přehrávačem	přehrávač	k1gInSc7	přehrávač
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
Apple	Apple	kA	Apple
představil	představit	k5eAaPmAgInS	představit
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
tablet	tableta	k1gFnPc2	tableta
iPad	iPad	k6eAd1	iPad
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Wozniak	Wozniak	k1gMnSc1	Wozniak
a	a	k8xC	a
Ronald	Ronald	k1gMnSc1	Ronald
Wayne	Wayn	k1gInSc5	Wayn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
založili	založit	k5eAaPmAgMnP	založit
Apple	Apple	kA	Apple
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
rodičů	rodič	k1gMnPc2	rodič
Steva	Steve	k1gMnSc2	Steve
Jobse	Jobs	k1gMnSc2	Jobs
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Cupertino	Cupertin	k2eAgNnSc4d1	Cupertino
poblíž	poblíž	k7c2	poblíž
San	San	k1gFnSc2	San
José	Josá	k1gFnSc2	Josá
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
později	pozdě	k6eAd2	pozdě
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc1	Valley
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
počátečního	počáteční	k2eAgInSc2d1	počáteční
kapitálu	kapitál	k1gInSc2	kapitál
Jobs	Jobsa	k1gFnPc2	Jobsa
prodal	prodat	k5eAaPmAgMnS	prodat
svůj	svůj	k3xOyFgInSc4	svůj
mikrobus	mikrobus	k1gInSc4	mikrobus
Volkswagen	volkswagen	k1gInSc1	volkswagen
a	a	k8xC	a
Wozniak	Wozniak	k1gInSc1	Wozniak
svou	svůj	k3xOyFgFnSc4	svůj
programovatelnou	programovatelný	k2eAgFnSc4d1	programovatelná
kalkulačku	kalkulačka	k1gFnSc4	kalkulačka
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
1	[number]	k4	1
350	[number]	k4	350
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgFnPc4	jenž
si	se	k3xPyFc3	se
pořídili	pořídit	k5eAaPmAgMnP	pořídit
čipy	čip	k1gInPc4	čip
typu	typ	k1gInSc2	typ
6502	[number]	k4	6502
firmy	firma	k1gFnSc2	firma
MOS	MOS	kA	MOS
Technology	technolog	k1gMnPc4	technolog
po	po	k7c6	po
20	[number]	k4	20
dolarech	dolar	k1gInPc6	dolar
za	za	k7c4	za
kus	kus	k1gInSc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Modelu	model	k1gInSc3	model
Apple	Apple	kA	Apple
I	i	k8xC	i
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
100	[number]	k4	100
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
Apple	Apple	kA	Apple
II	II	kA	II
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
prototyp	prototyp	k1gInSc1	prototyp
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1977	[number]	k4	1977
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
veletrhu	veletrh	k1gInSc6	veletrh
West	West	k1gInSc1	West
Coast	Coast	k1gInSc1	Coast
Computer	computer	k1gInSc1	computer
Fair	fair	k2eAgInSc1d1	fair
<g/>
,	,	kIx,	,
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
k	k	k7c3	k
osobním	osobní	k2eAgInPc3d1	osobní
počítačům	počítač	k1gInPc3	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
–	–	k?	–
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
prodával	prodávat	k5eAaImAgInS	prodávat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnPc1d2	pozdější
varianty	varianta	k1gFnPc1	varianta
Apple	Apple	kA	Apple
II	II	kA	II
se	se	k3xPyFc4	se
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
používaly	používat	k5eAaImAgFnP	používat
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
model	model	k1gInSc1	model
Apple	Apple	kA	Apple
III	III	kA	III
poškodil	poškodit	k5eAaPmAgInS	poškodit
Jobs	Jobs	k1gInSc1	Jobs
svou	svůj	k3xOyFgFnSc7	svůj
tvrdohlavostí	tvrdohlavost	k1gFnPc2	tvrdohlavost
–	–	k?	–
nechtěl	chtít	k5eNaImAgMnS	chtít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mít	mít	k5eAaImF	mít
ventilátor	ventilátor	k1gInSc4	ventilátor
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgMnS	zakázat
měnit	měnit	k5eAaImF	měnit
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
skříně	skříň	k1gFnSc2	skříň
<g/>
;	;	kIx,	;
počítač	počítač	k1gInSc1	počítač
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
poruchový	poruchový	k2eAgInSc1d1	poruchový
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
týmy	tým	k1gInPc1	tým
ale	ale	k9	ale
pracovaly	pracovat	k5eAaImAgInP	pracovat
na	na	k7c6	na
modelu	model	k1gInSc6	model
Apple	Apple	kA	Apple
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
uvedeném	uvedený	k2eAgMnSc6d1	uvedený
na	na	k7c4	na
trh	trh	k1gInSc1	trh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
o	o	k7c4	o
špičkově	špičkově	k6eAd1	špičkově
vybavený	vybavený	k2eAgInSc4d1	vybavený
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ceně	cena	k1gFnSc3	cena
(	(	kIx(	(
<g/>
9	[number]	k4	9
995	[number]	k4	995
USD	USD	kA	USD
<g/>
)	)	kIx)	)
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Technologický	technologický	k2eAgInSc4d1	technologický
i	i	k8xC	i
komerční	komerční	k2eAgInSc4d1	komerční
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
v	v	k7c4	v
hardware	hardware	k1gInSc4	hardware
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c4	v
software	software	k1gInSc4	software
<g/>
)	)	kIx)	)
představoval	představovat	k5eAaImAgMnS	představovat
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
model	model	k1gInSc1	model
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
Apple	Apple	kA	Apple
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
počítače	počítač	k1gInPc4	počítač
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k1gInSc1	Jobs
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Apple	Apple	kA	Apple
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
firmu	firma	k1gFnSc4	firma
postihl	postihnout	k5eAaPmAgInS	postihnout
úpadek	úpadek	k1gInSc1	úpadek
<g/>
,	,	kIx,	,
dražší	drahý	k2eAgInPc1d2	dražší
počítače	počítač	k1gInPc1	počítač
Apple	Apple	kA	Apple
válcoval	válcovat	k5eAaImAgInS	válcovat
Microsoft	Microsoft	kA	Microsoft
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Windows	Windows	kA	Windows
fungujícím	fungující	k2eAgInSc6d1	fungující
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
Jobs	Jobs	k1gInSc1	Jobs
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
vrátil	vrátit	k5eAaPmAgInS	vrátit
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
ji	on	k3xPp3gFnSc4	on
pomalu	pomalu	k6eAd1	pomalu
vracet	vracet	k5eAaImF	vracet
ke	k	k7c3	k
ziskovosti	ziskovost	k1gFnSc3	ziskovost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
Mac	Mac	kA	Mac
OS	OS	kA	OS
X.	X.	kA	X.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
přešel	přejít	k5eAaPmAgInS	přejít
Apple	Apple	kA	Apple
k	k	k7c3	k
čipům	čip	k1gInPc3	čip
Intel	Intel	kA	Intel
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
počítačů	počítač	k1gInPc2	počítač
řady	řada	k1gFnSc2	řada
Mac	Mac	kA	Mac
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
firma	firma	k1gFnSc1	firma
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
mobilními	mobilní	k2eAgNnPc7d1	mobilní
zařízeními	zařízení	k1gNnPc7	zařízení
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
iPodem	iPod	k1gInSc7	iPod
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
iPhonem	iPhon	k1gInSc7	iPhon
a	a	k8xC	a
iPadem	iPad	k1gInSc7	iPad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
způsobila	způsobit	k5eAaPmAgNnP	způsobit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
segmentech	segment	k1gInPc6	segment
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
MP3	MP3	k1gFnSc2	MP3
přehrávačů	přehrávač	k1gInPc2	přehrávač
<g/>
,	,	kIx,	,
smartphonů	smartphon	k1gInPc2	smartphon
a	a	k8xC	a
tabletů	tablet	k1gInPc2	tablet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
firma	firma	k1gFnSc1	firma
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
z	z	k7c2	z
Apple	Apple	kA	Apple
Computers	Computersa	k1gFnPc2	Computersa
Inc	Inc	k1gFnPc2	Inc
<g/>
.	.	kIx.	.
na	na	k7c6	na
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc6	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
portfolio	portfolio	k1gNnSc4	portfolio
společnosti	společnost	k1gFnSc2	společnost
již	již	k6eAd1	již
netvořily	tvořit	k5eNaImAgInP	tvořit
pouze	pouze	k6eAd1	pouze
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
multimediální	multimediální	k2eAgFnPc4d1	multimediální
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc4	Jobs
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
post	post	k1gInSc4	post
CEO	CEO	kA	CEO
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gMnSc5	Alta
<g/>
)	)	kIx)	)
Na	na	k7c6	na
postu	post	k1gInSc6	post
CEO	CEO	kA	CEO
jej	on	k3xPp3gNnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Tim	Tim	k?	Tim
Cook	Cook	k1gMnSc1	Cook
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Apple	Apple	kA	Apple
stala	stát	k5eAaPmAgFnS	stát
nejhodnotnější	hodnotný	k2eAgFnSc7d3	nejhodnotnější
značkou	značka	k1gFnSc7	značka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
začal	začít	k5eAaPmAgMnS	začít
Apple	Apple	kA	Apple
budovat	budovat	k5eAaImF	budovat
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
Apple	Apple	kA	Apple
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kompletně	kompletně	k6eAd1	kompletně
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
−	−	k?	−
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
zastavit	zastavit	k5eAaPmF	zastavit
prodej	prodej	k1gInSc4	prodej
tabletů	tablet	k1gInPc2	tablet
Galaxy	Galax	k1gInPc1	Galax
Tab	tab	kA	tab
firmy	firma	k1gFnSc2	firma
Samsung	Samsung	kA	Samsung
sérií	série	k1gFnPc2	série
žalob	žaloba	k1gFnPc2	žaloba
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
postupně	postupně	k6eAd1	postupně
odmítány	odmítán	k2eAgFnPc1d1	odmítána
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
úplnou	úplný	k2eAgFnSc4d1	úplná
nebo	nebo	k8xC	nebo
částečnou	částečný	k2eAgFnSc4d1	částečná
neopodstatněnost	neopodstatněnost	k1gFnSc4	neopodstatněnost
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
−	−	k?	−
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
přiznala	přiznat	k5eAaPmAgFnS	přiznat
instalaci	instalace	k1gFnSc4	instalace
sledovacího	sledovací	k2eAgInSc2d1	sledovací
programu	program	k1gInSc2	program
Carrier	Carrira	k1gFnPc2	Carrira
IQ	iq	kA	iq
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
výrobcích	výrobek	k1gInPc6	výrobek
s	s	k7c7	s
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odesílání	odesílání	k1gNnSc1	odesílání
diagnostických	diagnostický	k2eAgNnPc2d1	diagnostické
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
platforem	platforma	k1gFnPc2	platforma
ale	ale	k8xC	ale
program	program	k1gInSc1	program
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
uživatelskému	uživatelský	k2eAgNnSc3d1	Uživatelské
rozhraní	rozhraní	k1gNnSc3	rozhraní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
diagnostickém	diagnostický	k2eAgInSc6d1	diagnostický
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
a	a	k8xC	a
způsobu	způsob	k1gInSc6	způsob
vzniku	vznik	k1gInSc2	vznik
názvu	název	k1gInSc2	název
a	a	k8xC	a
loga	logo	k1gNnSc2	logo
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
loga	logo	k1gNnSc2	logo
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
zakladatelé	zakladatel	k1gMnPc1	zakladatel
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
anekdotou	anekdota	k1gFnSc7	anekdota
o	o	k7c6	o
anglickém	anglický	k2eAgMnSc6d1	anglický
vědci	vědec	k1gMnSc6	vědec
Isaacu	Isaac	k1gMnSc6	Isaac
Newtonovi	Newton	k1gMnSc6	Newton
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
gravitačního	gravitační	k2eAgInSc2d1	gravitační
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
logo	logo	k1gNnSc1	logo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
Jobsovy	Jobsův	k2eAgFnSc2d1	Jobsova
diety	dieta	k1gFnSc2	dieta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
společnosti	společnost	k1gFnSc2	společnost
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
jablečné	jablečný	k2eAgFnSc6d1	jablečná
farmě	farma	k1gFnSc6	farma
a	a	k8xC	a
nekonzumoval	konzumovat	k5eNaBmAgInS	konzumovat
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
Macintosh	Macintosh	kA	Macintosh
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
souvislosti	souvislost	k1gFnSc6	souvislost
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
odrůdě	odrůda	k1gFnSc6	odrůda
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hledalo	hledat	k5eAaImAgNnS	hledat
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
abecedně	abecedně	k6eAd1	abecedně
předcházelo	předcházet	k5eAaImAgNnS	předcházet
Atari	Atari	k1gNnSc1	Atari
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Macintosh	Macintosh	kA	Macintosh
je	být	k5eAaImIp3nS	být
dřívější	dřívější	k2eAgNnSc1d1	dřívější
označení	označení	k1gNnSc1	označení
rodiny	rodina	k1gFnSc2	rodina
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
odrůdy	odrůda	k1gFnSc2	odrůda
jablka	jablko	k1gNnSc2	jablko
Mcintosh	Mcintosha	k1gFnPc2	Mcintosha
<g/>
,	,	kIx,	,
prvnímu	první	k4xOgNnSc3	první
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
ho	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
Jef	Jef	k1gMnSc1	Jef
Raskin	Raskin	k1gMnSc1	Raskin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vedl	vést	k5eAaImAgInS	vést
vývojový	vývojový	k2eAgInSc1d1	vývojový
tým	tým	k1gInSc1	tým
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc4	uvedení
Macintoshe	Macintosh	k1gInSc2	Macintosh
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1984	[number]	k4	1984
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
slavná	slavný	k2eAgFnSc1d1	slavná
televizní	televizní	k2eAgFnSc1d1	televizní
reklama	reklama	k1gFnSc1	reklama
parafrázující	parafrázující	k2eAgFnSc1d1	parafrázující
román	román	k1gInSc4	román
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
figurovala	figurovat	k5eAaImAgNnP	figurovat
jako	jako	k8xC	jako
velký	velký	k2eAgMnSc1d1	velký
bratr	bratr	k1gMnSc1	bratr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
MacBook	MacBook	k1gInSc1	MacBook
<g/>
.	.	kIx.	.
</s>
<s>
MacBook	MacBook	k1gInSc1	MacBook
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
výrobní	výrobní	k2eAgFnSc2d1	výrobní
řady	řada	k1gFnSc2	řada
přenosných	přenosný	k2eAgMnPc2d1	přenosný
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
iBook	iBook	k1gInSc1	iBook
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
první	první	k4xOgFnSc7	první
verzí	verze	k1gFnSc7	verze
MacBooku	MacBook	k1gInSc2	MacBook
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
iBooky	iBook	k1gInPc1	iBook
používaly	používat	k5eAaImAgInP	používat
procesory	procesor	k1gInPc4	procesor
PowerPC	PowerPC	k1gMnPc2	PowerPC
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
MacBook	MacBook	k1gInSc1	MacBook
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
procesor	procesor	k1gInSc1	procesor
značky	značka	k1gFnSc2	značka
Intel	Intel	kA	Intel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
chipset	chipset	k1gInSc4	chipset
Intel	Intel	kA	Intel
945	[number]	k4	945
<g/>
GM	GM	kA	GM
<g/>
,	,	kIx,	,
procesor	procesor	k1gInSc1	procesor
Intel	Intel	kA	Intel
Core	Cor	k1gFnSc2	Cor
Duo	duo	k1gNnSc4	duo
a	a	k8xC	a
integrovanou	integrovaný	k2eAgFnSc4d1	integrovaná
grafickou	grafický	k2eAgFnSc4d1	grafická
kartu	karta	k1gFnSc4	karta
Intel	Intel	kA	Intel
GMA	GMA	kA	GMA
<g/>
950	[number]	k4	950
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
MacBook	MacBook	k1gInSc1	MacBook
dostal	dostat	k5eAaPmAgInS	dostat
novější	nový	k2eAgInPc4d2	novější
procesory	procesor	k1gInPc4	procesor
Intel	Intel	kA	Intel
Core	Core	k1gInSc4	Core
2	[number]	k4	2
Duo	duo	k1gNnSc4	duo
<g/>
,	,	kIx,	,
chipset	chipset	k1gMnSc1	chipset
Intel	Intel	kA	Intel
GM965	GM965	k1gMnSc1	GM965
a	a	k8xC	a
grafickou	grafický	k2eAgFnSc4d1	grafická
kartu	karta	k1gFnSc4	karta
Intel	Intel	kA	Intel
X	X	kA	X
<g/>
3100	[number]	k4	3100
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
postupným	postupný	k2eAgInPc3d1	postupný
upgradům	upgrade	k1gInPc3	upgrade
na	na	k7c4	na
novější	nový	k2eAgFnPc4d2	novější
verze	verze	k1gFnPc4	verze
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
nová	nový	k2eAgFnSc1d1	nová
třída	třída	k1gFnSc1	třída
extra	extra	k2eAgInPc2d1	extra
tenkých	tenký	k2eAgInPc2d1	tenký
a	a	k8xC	a
lehkých	lehký	k2eAgInPc2d1	lehký
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
opět	opět	k6eAd1	opět
jméno	jméno	k1gNnSc1	jméno
MacBook	MacBook	k1gInSc1	MacBook
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc2d1	původní
unibody	uniboda	k1gFnSc2	uniboda
MacBook	MacBook	k1gInSc1	MacBook
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
MacBook	MacBook	k1gInSc1	MacBook
Pro	pro	k7c4	pro
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
MacBook	MacBook	k1gInSc4	MacBook
Pro	pro	k7c4	pro
<g/>
.	.	kIx.	.
</s>
<s>
MacBook	MacBook	k1gInSc1	MacBook
Pro	pro	k7c4	pro
je	být	k5eAaImIp3nS	být
notebook	notebook	k1gInSc1	notebook
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
Intel	Intel	kA	Intel
Core	Cor	k1gInSc2	Cor
i	i	k9	i
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgInS	být
PowerBook	powerbook	k1gInSc4	powerbook
využívající	využívající	k2eAgInSc1d1	využívající
procesor	procesor	k1gInSc1	procesor
IBM	IBM	kA	IBM
PowerPC	PowerPC	k1gFnSc2	PowerPC
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dražší	drahý	k2eAgFnSc4d2	dražší
a	a	k8xC	a
výkonnější	výkonný	k2eAgFnSc4d2	výkonnější
variantu	varianta	k1gFnSc4	varianta
MacBooku	MacBook	k1gInSc2	MacBook
<g/>
,	,	kIx,	,
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
nového	nový	k2eAgInSc2d1	nový
MacBooku	MacBook	k1gInSc2	MacBook
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
typ	typ	k1gInSc4	typ
klasického	klasický	k2eAgInSc2d1	klasický
výkonného	výkonný	k2eAgInSc2d1	výkonný
notebooku	notebook	k1gInSc2	notebook
typicky	typicky	k6eAd1	typicky
s	s	k7c7	s
displejem	displej	k1gInSc7	displej
typu	typ	k1gInSc2	typ
Retina	retina	k1gFnSc1	retina
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnostní	výkonnostní	k2eAgInSc1d1	výkonnostní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
modely	model	k1gInPc1	model
MacBook	MacBook	k1gInSc1	MacBook
a	a	k8xC	a
MacBook	MacBook	k1gInSc1	MacBook
Pro	pro	k7c4	pro
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
především	především	k9	především
v	v	k7c6	v
grafické	grafický	k2eAgFnSc6d1	grafická
kartě	karta	k1gFnSc6	karta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
MacBook	MacBook	k1gInSc4	MacBook
Pro	pro	k7c4	pro
dedikovaná	dedikovaný	k2eAgNnPc4d1	dedikované
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
čipech	čip	k1gInPc6	čip
Nvidia	Nvidium	k1gNnSc2	Nvidium
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
RAM	RAM	kA	RAM
–	–	k?	–
neplatilo	platit	k5eNaImAgNnS	platit
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
13	[number]	k4	13
<g/>
palcové	palcový	k2eAgFnSc2d1	palcová
varianty	varianta	k1gFnSc2	varianta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
používala	používat	k5eAaImAgFnS	používat
integrovanou	integrovaný	k2eAgFnSc4d1	integrovaná
grafickou	grafický	k2eAgFnSc4d1	grafická
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
typu	typ	k1gInSc2	typ
Intel	Intel	kA	Intel
Iris	iris	k1gInSc1	iris
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
především	především	k9	především
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
Pro	pro	k7c4	pro
měla	mít	k5eAaImAgNnP	mít
šasi	šasi	k1gNnPc1	šasi
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
klasický	klasický	k2eAgInSc1d1	klasický
MacBook	MacBook	k1gInSc1	MacBook
měl	mít	k5eAaImAgInS	mít
šasi	šasi	k1gNnSc4	šasi
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
polykarbonátového	polykarbonátový	k2eAgInSc2d1	polykarbonátový
bílého	bílý	k2eAgInSc2d1	bílý
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
MacBook	MacBook	k1gInSc1	MacBook
Pro	pro	k7c4	pro
je	on	k3xPp3gMnPc4	on
vybaven	vybavit	k5eAaPmNgMnS	vybavit
podsvícením	podsvícení	k1gNnSc7	podsvícení
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
automaticky	automaticky	k6eAd1	automaticky
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
intenzity	intenzita	k1gFnSc2	intenzita
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
ve	v	k7c6	v
výbavě	výbava	k1gFnSc6	výbava
a	a	k8xC	a
velikosti	velikost	k1gFnSc3	velikost
displejů	displej	k1gInPc2	displej
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
původní	původní	k2eAgInSc1d1	původní
MacBook	MacBook	k1gInSc1	MacBook
měl	mít	k5eAaImAgInS	mít
velikost	velikost	k1gFnSc4	velikost
displeje	displej	k1gInSc2	displej
pouze	pouze	k6eAd1	pouze
13,3	[number]	k4	13,3
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
MacBook	MacBook	k1gInSc4	MacBook
Pro	pro	k7c4	pro
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
velikostech	velikost	k1gFnPc6	velikost
13	[number]	k4	13
<g/>
"	"	kIx"	"
a	a	k8xC	a
15	[number]	k4	15
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
Intel	Intel	kA	Intel
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
počítače	počítač	k1gInSc2	počítač
taktovány	taktovat	k5eAaImNgInP	taktovat
až	až	k6eAd1	až
do	do	k7c2	do
rychlosti	rychlost	k1gFnSc2	rychlost
2,8	[number]	k4	2,8
GHz	GHz	k1gFnPc2	GHz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
počítačů	počítač	k1gInPc2	počítač
MacBook	MacBook	k1gInSc4	MacBook
Pro	pro	k7c4	pro
je	on	k3xPp3gMnPc4	on
uveden	uveden	k2eAgInSc1d1	uveden
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1	vysokorychlostní
port	port	k1gInSc1	port
Thunderbolt	Thunderbolt	k2eAgInSc1d1	Thunderbolt
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgInPc4d1	další
počítače	počítač	k1gInPc4	počítač
Apple	Apple	kA	Apple
obsahující	obsahující	k2eAgInSc4d1	obsahující
monitor	monitor	k1gInSc4	monitor
má	mít	k5eAaImIp3nS	mít
MacBook	MacBook	k1gInSc1	MacBook
Pro	pro	k7c4	pro
vestavěnou	vestavěný	k2eAgFnSc4d1	vestavěná
kameru	kamera	k1gFnSc4	kamera
FaceTime	FaceTim	k1gInSc5	FaceTim
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
MacBooku	MacBook	k1gInSc2	MacBook
Pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
1,3	[number]	k4	1,3
Mpix	Mpix	k1gInSc4	Mpix
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
slouží	sloužit	k5eAaImIp3nS	sloužit
Wifi	Wifi	k1gNnSc1	Wifi
802.11	[number]	k4	802.11
<g/>
n	n	k0	n
draft	draft	k1gInSc4	draft
<g/>
,	,	kIx,	,
k	k	k7c3	k
bezdrátovému	bezdrátový	k2eAgNnSc3d1	bezdrátové
připojení	připojení	k1gNnSc3	připojení
periferií	periferie	k1gFnPc2	periferie
pak	pak	k6eAd1	pak
Bluetooth	Bluetooth	k1gInSc4	Bluetooth
4.0	[number]	k4	4.0
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
iMac	iMac	k6eAd1	iMac
<g/>
.	.	kIx.	.
iMac	iMac	k6eAd1	iMac
je	být	k5eAaImIp3nS	být
počítač	počítač	k1gInSc1	počítač
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
all	all	k?	all
in	in	k?	in
one	one	k?	one
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
<g/>
"	"	kIx"	"
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
především	především	k9	především
monitor	monitor	k1gInSc4	monitor
a	a	k8xC	a
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
modelem	model	k1gInSc7	model
iMacu	iMacu	k5eAaPmIp1nS	iMacu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Apple	Apple	kA	Apple
návrat	návrat	k1gInSc1	návrat
mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
značky	značka	k1gFnPc4	značka
<g/>
.	.	kIx.	.
iMac	iMac	k6eAd1	iMac
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
produktem	produkt	k1gInSc7	produkt
Applu	Appla	k1gFnSc4	Appla
s	s	k7c7	s
názvem	název	k1gInSc7	název
uvozeným	uvozený	k2eAgNnSc7d1	uvozené
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
iMacu	iMacu	k6eAd1	iMacu
bylo	být	k5eAaImAgNnS	být
zkratkou	zkratka	k1gFnSc7	zkratka
pro	pro	k7c4	pro
Internet	Internet	k1gInSc4	Internet
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
počítač	počítač	k1gInSc1	počítač
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
iPod	iPod	k6eAd1	iPod
<g/>
.	.	kIx.	.
</s>
<s>
iPod	iPod	k6eAd1	iPod
je	být	k5eAaImIp3nS	být
multimediální	multimediální	k2eAgInSc1d1	multimediální
přehrávač	přehrávač	k1gInSc1	přehrávač
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
přenosných	přenosný	k2eAgFnPc2d1	přenosná
MP3	MP3	k1gFnPc2	MP3
přehrávačů	přehrávač	k1gInPc2	přehrávač
od	od	k7c2	od
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
iPod	iPod	k6eAd1	iPod
také	také	k9	také
dlouho	dlouho	k6eAd1	dlouho
používal	používat	k5eAaImAgInS	používat
nejvyspělejší	vyspělý	k2eAgInSc1d3	nejvyspělejší
"	"	kIx"	"
<g/>
klasický	klasický	k2eAgInSc1d1	klasický
<g/>
"	"	kIx"	"
iPod	iPod	k1gInSc1	iPod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dostal	dostat	k5eAaPmAgInS	dostat
později	pozdě	k6eAd2	pozdě
přízvisko	přízvisko	k1gNnSc4	přízvisko
Classic	Classice	k1gFnPc2	Classice
<g/>
.	.	kIx.	.
iPody	iPody	k6eAd1	iPody
mají	mít	k5eAaImIp3nP	mít
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
ovládalo	ovládat	k5eAaImAgNnS	ovládat
pomocí	pomocí	k7c2	pomocí
dotykového	dotykový	k2eAgNnSc2d1	dotykové
kolečka	kolečko	k1gNnSc2	kolečko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
click	click	k1gMnSc1	click
wheel	wheel	k1gMnSc1	wheel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
pouze	pouze	k6eAd1	pouze
model	model	k1gInSc1	model
iPod	iPoda	k1gFnPc2	iPoda
Touch	Touch	k1gInSc1	Touch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ovládal	ovládat	k5eAaImAgInS	ovládat
pomocí	pomocí	k7c2	pomocí
velkého	velký	k2eAgInSc2d1	velký
dotykového	dotykový	k2eAgInSc2d1	dotykový
displeje	displej	k1gInSc2	displej
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
iPhone	iPhon	k1gInSc5	iPhon
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgNnSc1d1	obdobné
ovládání	ovládání	k1gNnSc1	ovládání
dostaly	dostat	k5eAaPmAgFnP	dostat
později	pozdě	k6eAd2	pozdě
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
modely	model	k1gInPc1	model
iPod	iPoda	k1gFnPc2	iPoda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
iPhone	iPhon	k1gInSc5	iPhon
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
je	on	k3xPp3gMnPc4	on
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojuje	spojovat	k5eAaImIp3nS	spojovat
funkce	funkce	k1gFnSc2	funkce
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
s	s	k7c7	s
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
,	,	kIx,	,
multimediálního	multimediální	k2eAgInSc2d1	multimediální
přehrávače	přehrávač	k1gInSc2	přehrávač
(	(	kIx(	(
<g/>
iPod	iPod	k6eAd1	iPod
<g/>
)	)	kIx)	)
a	a	k8xC	a
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgFnSc4d1	mobilní
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
internetem	internet	k1gInSc7	internet
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
velkého	velký	k2eAgInSc2d1	velký
dotykového	dotykový	k2eAgInSc2d1	dotykový
displeje	displej	k1gInSc2	displej
s	s	k7c7	s
virtuální	virtuální	k2eAgFnSc7d1	virtuální
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
chytrém	chytrý	k2eAgInSc6d1	chytrý
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
iPad	iPad	k1gInSc1	iPad
<g/>
.	.	kIx.	.
</s>
<s>
Tablet	tablet	k1gInSc1	tablet
iPad	iPad	k6eAd1	iPad
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc1	produkt
spojující	spojující	k2eAgInSc1d1	spojující
multimediální	multimediální	k2eAgInSc1d1	multimediální
přehrávač	přehrávač	k1gInSc1	přehrávač
iPod	iPoda	k1gFnPc2	iPoda
Touch	Touch	k1gInSc4	Touch
s	s	k7c7	s
plnohodnotným	plnohodnotný	k2eAgInSc7d1	plnohodnotný
osobním	osobní	k2eAgInSc7d1	osobní
počítačem	počítač	k1gInSc7	počítač
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
definoval	definovat	k5eAaBmAgInS	definovat
novou	nový	k2eAgFnSc4d1	nová
kategorii	kategorie	k1gFnSc4	kategorie
technologií	technologie	k1gFnPc2	technologie
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
obrovský	obrovský	k2eAgInSc1d1	obrovský
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
z	z	k7c2	z
trhu	trh	k1gInSc2	trh
netbooky	netbook	k1gInPc1	netbook
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
macOS	macOS	k?	macOS
<g/>
.	.	kIx.	.
macOS	macOS	k?	macOS
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Mac	Mac	kA	Mac
OS	osa	k1gFnPc2	osa
X	X	kA	X
v	v	k7c6	v
<g/>
10.0	[number]	k4	10.0
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jako	jako	k9	jako
kombinace	kombinace	k1gFnSc2	kombinace
několika	několik	k4yIc2	několik
různých	různý	k2eAgFnPc2d1	různá
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
hybridního	hybridní	k2eAgNnSc2d1	hybridní
unixového	unixový	k2eAgNnSc2d1	unixové
jádra	jádro	k1gNnSc2	jádro
XNU	XNU	kA	XNU
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
XNU	XNU	kA	XNU
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Not	nota	k1gFnPc2	nota
Unix	Unix	k1gInSc1	Unix
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
GNU	gnu	k1gNnSc2	gnu
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
open	openo	k1gNnPc2	openo
source	sourec	k1gInSc2	sourec
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
přejaty	přejat	k2eAgFnPc1d1	přejata
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
NeXTSTEPu	NeXTSTEPus	k1gInSc2	NeXTSTEPus
a	a	k8xC	a
předchozího	předchozí	k2eAgInSc2d1	předchozí
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Aqua	Aqu	k1gInSc2	Aqu
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
společností	společnost	k1gFnSc7	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Upravenou	upravený	k2eAgFnSc4d1	upravená
verzi	verze	k1gFnSc4	verze
macOS	macOS	k?	macOS
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
iOS	iOS	k?	iOS
využívá	využívat	k5eAaImIp3nS	využívat
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
Apple	Apple	kA	Apple
iPhone	iPhon	k1gInSc5	iPhon
<g/>
,	,	kIx,	,
multimediální	multimediální	k2eAgInSc1d1	multimediální
kapesní	kapesní	k2eAgInSc1d1	kapesní
přehrávač	přehrávač	k1gInSc1	přehrávač
iPod	iPod	k6eAd1	iPod
Touch	Touch	k1gInSc4	Touch
a	a	k8xC	a
tablet	tablet	k1gInSc4	tablet
iPad	iPada	k1gFnPc2	iPada
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
aktualizovaný	aktualizovaný	k2eAgInSc4d1	aktualizovaný
a	a	k8xC	a
nově	nově	k6eAd1	nově
přináší	přinášet	k5eAaImIp3nS	přinášet
noční	noční	k2eAgInSc4d1	noční
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
mobilní	mobilní	k2eAgInSc4d1	mobilní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
společností	společnost	k1gFnSc7	společnost
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc7	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
mobilních	mobilní	k2eAgNnPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
<g/>
,	,	kIx,	,
iPad	iPad	k1gMnSc1	iPad
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
Apple	Apple	kA	Apple
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
iOS	iOS	k?	iOS
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
až	až	k9	až
od	od	k7c2	od
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
verze	verze	k1gFnSc2	verze
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
nazýván	nazývat	k5eAaImNgInS	nazývat
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
politikou	politika	k1gFnSc7	politika
pojmenovávání	pojmenovávání	k1gNnSc2	pojmenovávání
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
iPod	iPod	k1gMnSc1	iPod
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
iPad	iPad	k6eAd1	iPad
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
nového	nový	k2eAgInSc2d1	nový
názvu	název	k1gInSc2	název
iOS	iOS	k?	iOS
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Apple	Apple	kA	Apple
podána	podán	k2eAgFnSc1d1	podána
žaloba	žaloba	k1gFnSc1	žaloba
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Cisco	Cisco	k6eAd1	Cisco
Systems	Systems	k1gInSc4	Systems
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
název	název	k1gInSc1	název
IOS	IOS	kA	IOS
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
softwaru	software	k1gInSc2	software
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
routerech	router	k1gInPc6	router
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
žalobě	žaloba	k1gFnSc3	žaloba
<g/>
,	,	kIx,	,
licencovala	licencovat	k5eAaBmAgFnS	licencovat
si	se	k3xPyFc3	se
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
použití	použití	k1gNnSc2	použití
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Airport	Airport	k1gInSc1	Airport
Express	express	k1gInSc1	express
Aperture	Apertur	k1gMnSc5	Apertur
Apple	Apple	kA	Apple
I	i	k8xC	i
Apple	Apple	kA	Apple
II	II	kA	II
Apple	Apple	kA	Apple
III	III	kA	III
Apple	Apple	kA	Apple
Lisa	Lisa	k1gFnSc1	Lisa
Apple	Apple	kA	Apple
TV	TV	kA	TV
Cinema	Cinem	k1gMnSc2	Cinem
Display	Displaa	k1gMnSc2	Displaa
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
Studio	studio	k1gNnSc1	studio
iCloud	iCloud	k1gMnSc1	iCloud
iLife	iLif	k1gInSc5	iLif
iWork	iWork	k1gInSc1	iWork
Logic	Logice	k1gFnPc2	Logice
Studio	studio	k1gNnSc1	studio
MacBook	MacBook	k1gInSc4	MacBook
Air	Air	k1gFnSc2	Air
Mac	Mac	kA	Mac
mini	mini	k2eAgFnPc1d1	mini
Mac	Mac	kA	Mac
Pro	pro	k7c4	pro
Magic	Magice	k1gInPc2	Magice
Mouse	Mous	k1gMnSc2	Mous
Mighty	Mighta	k1gMnSc2	Mighta
Mouse	Mous	k1gMnSc2	Mous
Mobile	mobile	k1gNnSc2	mobile
Me	Me	k1gMnSc2	Me
Remote	Remot	k1gInSc5	Remot
Desktop	desktop	k1gInSc1	desktop
Time	Time	k1gNnSc6	Time
Capsule	Capsule	k1gFnSc2	Capsule
Thunderbolt	Thunderbolt	k1gInSc4	Thunderbolt
Display	Displaa	k1gFnSc2	Displaa
Apple	Apple	kA	Apple
Watch	Watch	k1gMnSc1	Watch
Apple	Apple	kA	Apple
Pippin	Pippin	k1gMnSc1	Pippin
</s>
<s>
Apple	Apple	kA	Apple
natočil	natočit	k5eAaBmAgMnS	natočit
své	svůj	k3xOyFgNnSc4	svůj
2	[number]	k4	2
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
reklamní	reklamní	k2eAgFnPc1d1	reklamní
reklamy	reklama	k1gFnPc1	reklama
na	na	k7c6	na
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
spot	spot	k1gInSc1	spot
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Frankie	Frankie	k1gFnSc1	Frankie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Holiday	Holida	k1gMnPc7	Holida
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
Hošťálkově	Hošťálkův	k2eAgNnSc6d1	Hošťálkův
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Žatci	Žatec	k1gInSc6	Žatec
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
spot	spot	k1gInSc1	spot
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Romeo	Romeo	k1gMnSc1	Romeo
&	&	k?	&
Juliet	Juliet	k1gMnSc1	Juliet
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Libochovích	Libochoví	k1gNnPc6	Libochoví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
seriálu	seriál	k1gInSc6	seriál
Simpsonovy	Simpsonův	k2eAgFnSc2d1	Simpsonova
se	se	k3xPyFc4	se
Apple	Apple	kA	Apple
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
Mapple	Mapple	k1gFnSc1	Mapple
a	a	k8xC	a
produkty	produkt	k1gInPc1	produkt
iPhone	iPhon	k1gMnSc5	iPhon
jako	jako	k8xC	jako
MyPhone	MyPhon	k1gMnSc5	MyPhon
<g/>
,	,	kIx,	,
iPod-MyPod	iPod-MyPoda	k1gFnPc2	iPod-MyPoda
<g/>
.	.	kIx.	.
</s>
