<s>
Šu	Šu	k?
(	(	kIx(
<g/>
bůh	bůh	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Šuv	Šuv	k?
hieroglyfickém	hieroglyfický	k2eAgMnSc6d1
zápisu	zápis	k1gInSc3
</s>
<s>
Bůh	bůh	k1gMnSc1
Šu	Šu	k1gMnSc1
</s>
<s>
Šu	Šu	k?
<g/>
,	,	kIx,
čí	čí	k3xOyQgFnSc2,k3xOyRgFnSc2
Šov	Šov	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
staroegyptský	staroegyptský	k2eAgMnSc1d1
bůh	bůh	k1gMnSc1
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
světla	světlo	k1gNnSc2
a	a	k8xC
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Iunu	Iunus	k1gInSc6
byl	být	k5eAaImAgInS
pokládán	pokládat	k5eAaImNgInS
za	za	k7c2
člena	člen	k1gMnSc2
Devatera	devatero	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
sestrou	sestra	k1gFnSc7
a	a	k8xC
manželkou	manželka	k1gFnSc7
Tefnut	Tefnut	k2eAgInSc4d1
tvoří	tvořit	k5eAaImIp3nP
první	první	k4xOgFnSc4
generaci	generace	k1gFnSc4
bohů	bůh	k1gMnPc2
zrozenou	zrozený	k2eAgFnSc4d1
Stvořitelem	Stvořitel	k1gMnSc7
Atumem	Atum	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Zrod	zrod	k1gInSc1
</s>
<s>
Bůh	bůh	k1gMnSc1
Šu	Šu	k1gMnSc1
byl	být	k5eAaImAgMnS
podle	podle	k7c2
egyptských	egyptský	k2eAgInPc2d1
mýtů	mýtus	k1gInPc2
zplozen	zplodit	k5eAaPmNgInS
prvotním	prvotní	k2eAgMnSc7d1
bohem	bůh	k1gMnSc7
Atumem	Atum	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
posvátnou	posvátný	k2eAgFnSc7d1
masturbací	masturbace	k1gFnSc7
Stvořitele	Stvořitel	k1gMnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
sestrou	sestra	k1gFnSc7
Tefnut	Tefnut	k2eAgMnSc1d1
vydal	vydat	k5eAaPmAgMnS
do	do	k7c2
hlubin	hlubina	k1gFnPc2
Nún	Nún	k1gFnSc2
(	(	kIx(
<g/>
vesmíru	vesmír	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ztratili	ztratit	k5eAaPmAgMnP
a	a	k8xC
museli	muset	k5eAaImAgMnP
čekat	čekat	k5eAaImF
na	na	k7c4
záchranu	záchrana	k1gFnSc4
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Atuma	Atum	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
za	za	k7c7
nima	nima	k?
vyslal	vyslat	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
oko	oko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
příběh	příběh	k1gInSc1
byl	být	k5eAaImAgInS
často	často	k6eAd1
zpodobňován	zpodobňovat	k5eAaImNgInS
na	na	k7c6
stěnách	stěna	k1gFnPc6
egyptských	egyptský	k2eAgInPc2d1
chrámů	chrám	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
v	v	k7c6
Hathořině	Hathořin	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
v	v	k7c6
Dendeře	Dendera	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BUDGE	BUDGE	kA
<g/>
,	,	kIx,
E.	E.	kA
A.	A.	kA
Wallis	Wallis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
God	God	k1gFnSc1
of	of	k?
the	the	k?
egyptians	egyptians	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1904	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
308	#num#	k4
<g/>
-	-	kIx~
<g/>
321	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stvoření	stvoření	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
hieratickém	hieratický	k2eAgInSc6d1
zápisu	zápis	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
překlad	překlad	k1gInSc1
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
na	na	k7c6
straně	strana	k1gFnSc6
308	#num#	k4
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
http://www.etana.org/sites/default/files/coretexts/20625.pdf	http://www.etana.org/sites/default/files/coretexts/20625.pdf	k1gInSc1
<g/>
↑	↑	k?
MASSEY	MASSEY	kA
<g/>
,	,	kIx,
Gerald	Gerald	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Natural	Natural	k?
Genesis	Genesis	k1gFnSc1
-	-	kIx~
vol	vol	k6eAd1
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1883	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šu	Šu	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Egyptští	egyptský	k2eAgMnPc1d1
bohové	bůh	k1gMnPc1
Osmero	Osmero	k1gNnSc1
</s>
<s>
Amon	Amon	k1gMnSc1
•	•	k?
Amaunet	Amaunet	k1gMnSc1
•	•	k?
Heh	heh	k0
•	•	k?
Hauhet	Hauhet	k1gInSc1
•	•	k?
Kuk	kuk	k1gInSc1
•	•	k?
Kauket	Kauket	k1gMnSc1
•	•	k?
Nun	Nun	k1gMnSc1
•	•	k?
Naunet	Naunet	k1gMnSc1
Devatero	devatero	k1gNnSc1
</s>
<s>
Atum	Atum	k1gMnSc1
•	•	k?
Šu	Šu	k1gMnSc1
•	•	k?
Tefnut	Tefnut	k1gMnSc1
•	•	k?
Geb	Geb	k1gMnSc1
•	•	k?
Nút	Nút	k1gMnSc1
•	•	k?
Usir	Usir	k1gMnSc1
•	•	k?
Eset	Eset	k1gMnSc1
•	•	k?
Sutech	Sutech	k1gInSc1
•	•	k?
Nebthet	Nebthet	k1gInSc1
</s>
<s>
Aken	Aken	k1gMnSc1
•	•	k?
Aker	Aker	k1gMnSc1
•	•	k?
Achty	acht	k1gInPc1
•	•	k?
Amentet	Amentet	k1gInSc1
•	•	k?
Amenre	Amenr	k1gInSc5
•	•	k?
Amemait	Amemait	k1gMnSc1
•	•	k?
Am-Heh	Am-Heh	k1gMnSc1
•	•	k?
Anat	Anat	k1gMnSc1
•	•	k?
Andžety	Andžeta	k1gFnSc2
•	•	k?
Anput	Anput	k1gMnSc1
•	•	k?
Anti	Ant	k1gFnSc2
•	•	k?
Anuket	Anuket	k1gMnSc1
•	•	k?
Anup	Anup	k1gMnSc1
•	•	k?
Apet	Apet	k1gMnSc1
•	•	k?
Apis	Apis	k1gInSc1
•	•	k?
Apop	Apop	k1gInSc1
•	•	k?
Aš	Aš	k1gInSc1
•	•	k?
Aštoret	Aštoret	k1gInSc1
•	•	k?
Aton	Aton	k1gInSc1
•	•	k?
Ba-pef	Ba-pef	k1gInSc1
•	•	k?
Babi	Babi	k?
•	•	k?
Banebdžedet	Banebdžedet	k1gMnSc1
•	•	k?
Bat	Bat	k1gMnSc1
•	•	k?
Bata	Bata	k1gMnSc1
•	•	k?
Bastet	Bastet	k1gMnSc1
•	•	k?
Behdetej	Behdetej	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Benu	Ben	k1gInSc2
•	•	k?
Bes	Bes	k1gMnSc1
•	•	k?
Cenenet	Cenenet	k1gMnSc1
•	•	k?
Dedun	Dedun	k1gMnSc1
•	•	k?
Dua	duo	k1gNnSc2
•	•	k?
Duamutef	Duamutef	k1gMnSc1
•	•	k?
Dunanuej	Dunanuej	k1gInSc1
•	•	k?
Fetektej	Fetektej	k1gFnSc2
•	•	k?
Gerh	Gerh	k1gInSc1
•	•	k?
Ha	ha	kA
•	•	k?
Hapi	Hap	k1gFnSc2
(	(	kIx(
<g/>
bůh	bůh	k1gMnSc1
záplav	záplava	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Hapi	Hap	k1gMnPc1
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
Hora	Hora	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Harachtej	Harachtej	k1gMnSc1
•	•	k?
Haremachet	Haremachet	k1gMnSc1
•	•	k?
Hathor	Hathor	k1gMnSc1
•	•	k?
Hatmehit	Hatmehit	k1gMnSc1
•	•	k?
Hededet	Hededet	k1gMnSc1
•	•	k?
Hedžhotep	Hedžhotep	k1gMnSc1
•	•	k?
Heka	Heka	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Heket	Heket	k1gMnSc1
•	•	k?
Hemen	Hemen	k1gInSc1
•	•	k?
Hemsut	Hemsut	k1gInSc1
•	•	k?
Hermanubis	Hermanubis	k1gInSc1
•	•	k?
Herišef	Herišef	k1gInSc1
•	•	k?
Hesat	Hesat	k1gInSc1
•	•	k?
Hor	hora	k1gFnPc2
•	•	k?
Huh	huh	k0
•	•	k?
Chentiamenti	Chentiament	k1gMnPc1
•	•	k?
Cheprer	Cheprer	k1gInSc1
•	•	k?
Chnum	Chnum	k1gInSc1
•	•	k?
Chonsu	Chons	k1gInSc2
•	•	k?
Ihi	ihi	k0
•	•	k?
Imhotep	Imhotep	k1gMnSc1
•	•	k?
Imset	Imset	k1gMnSc1
•	•	k?
Inmutef	Inmutef	k1gMnSc1
•	•	k?
Isdes	Isdes	k1gMnSc1
•	•	k?
Jah	Jah	k1gFnSc2
•	•	k?
Jat	jat	k2eAgInSc4d1
•	•	k?
Kadeš	Kadeš	k1gMnSc7
•	•	k?
Kamutef	Kamutef	k1gMnSc1
•	•	k?
Kebehsenuf	Kebehsenuf	k1gMnSc1
•	•	k?
Kebhut	Kebhut	k1gMnSc1
•	•	k?
Kematef	Kematef	k1gMnSc1
•	•	k?
Maat	Maat	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mahes	Mahes	k1gMnSc1
•	•	k?
Mandulis	Mandulis	k1gFnSc2
•	•	k?
Medžed	Medžed	k1gMnSc1
•	•	k?
Mefdet	Mefdet	k1gMnSc1
•	•	k?
Mehen	Mehen	k1gInSc1
•	•	k?
Mehet	Mehet	k1gMnSc1
•	•	k?
Mehetveret	Mehetveret	k1gMnSc1
•	•	k?
Menhit	Menhit	k1gMnSc1
•	•	k?
Menket	Menket	k1gMnSc1
•	•	k?
Merseger	Merseger	k1gMnSc1
•	•	k?
Mert	Mert	k1gMnSc1
•	•	k?
Mešent	Mešent	k1gMnSc1
•	•	k?
Min	min	kA
•	•	k?
Moncu	Moncus	k1gInSc2
•	•	k?
Mut	Mut	k?
•	•	k?
Nechbet	Nechbet	k1gMnSc1
•	•	k?
Neit	Neit	k1gMnSc1
•	•	k?
Neper	prát	k5eNaImRp2nS
•	•	k?
Niau	Nia	k1gMnSc3
•	•	k?
Onhuret	Onhuret	k1gMnSc1
•	•	k?
Osmero	Osmero	k1gNnSc4
•	•	k?
Pachet	Pachet	k1gMnSc1
•	•	k?
Pataikos	Pataikos	k1gMnSc1
•	•	k?
Ptah	Ptah	k1gMnSc1
•	•	k?
Re	re	k9
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Renenutet	Renenutet	k1gMnSc1
•	•	k?
Rešef	Rešef	k1gMnSc1
•	•	k?
Rutej	Rutej	k1gInSc1
•	•	k?
Sah	Sah	k1gFnSc2
•	•	k?
Sachmet	Sachmet	k1gMnSc1
•	•	k?
Satet	Satet	k1gMnSc1
•	•	k?
Selket	Selket	k1gMnSc1
•	•	k?
Sep	Sep	k1gMnSc1
•	•	k?
Serapis	Serapis	k1gFnSc2
•	•	k?
Seret	Seret	k1gMnSc1
•	•	k?
Sešet	Sešet	k1gMnSc1
•	•	k?
Sia	Sia	k1gMnSc1
•	•	k?
Sobek	Sobek	k1gMnSc1
•	•	k?
Sokar	Sokar	k1gMnSc1
•	•	k?
Sopd	Sopd	k1gMnSc1
•	•	k?
Sopdet	Sopdet	k1gInSc1
•	•	k?
Sopdu	Sopd	k1gInSc2
•	•	k?
Šaj	Šaj	k1gMnSc1
•	•	k?
Šed	šedo	k1gNnPc2
•	•	k?
Šesmu	Šesm	k1gInSc6
•	•	k?
Tait	Tait	k1gInSc1
•	•	k?
Tatenen	Tatenen	k1gInSc1
•	•	k?
Tenemu	Tenem	k1gInSc2
•	•	k?
Tenenet	Tenenet	k1gMnSc1
•	•	k?
Thovt	Thovt	k1gMnSc1
•	•	k?
Tveret	Tveret	k1gMnSc1
•	•	k?
Vadžet	Vadžet	k1gMnSc1
•	•	k?
Veneg	Veneg	k1gMnSc1
•	•	k?
Venvet	Venvet	k1gMnSc1
•	•	k?
Vepvovet	Vepvovet	k1gInSc1
•	•	k?
Verethekau	Verethekaus	k1gInSc2
•	•	k?
Vosret	Vosret	k1gInSc1
Seznam	seznam	k1gInSc1
bohů	bůh	k1gMnPc2
•	•	k?
Kategorie	kategorie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2003006484	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
135155767380327761221	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2003006484	#num#	k4
</s>
