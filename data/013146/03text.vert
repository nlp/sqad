<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
had	had	k1gMnSc1	had
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hroznýšovitých	hroznýšovití	k1gMnPc2	hroznýšovití
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
oblastech	oblast	k1gFnPc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
pobytu	pobyt	k1gInSc3	pobyt
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nejčastěji	často	k6eAd3	často
délky	délka	k1gFnPc1	délka
2,5	[number]	k4	2,5
až	až	k9	až
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
velcí	velký	k2eAgMnPc1d1	velký
jedinci	jedinec	k1gMnPc1	jedinec
měří	měřit	k5eAaImIp3nP	měřit
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
a	a	k8xC	a
za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
podmínek	podmínka	k1gFnPc2	podmínka
může	moct	k5eAaImIp3nS	moct
dorůst	dorůst	k5eAaPmF	dorůst
až	až	k9	až
k	k	k7c3	k
7	[number]	k4	7
metrům	metr	k1gInPc3	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
devítimetrových	devítimetrový	k2eAgFnPc6d1	devítimetrová
<g/>
,	,	kIx,	,
desetimetrových	desetimetrový	k2eAgFnPc6d1	desetimetrová
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
delších	dlouhý	k2eAgFnPc2d2	delší
jedincích	jedinec	k1gMnPc6	jedinec
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
věrohodné	věrohodný	k2eAgNnSc4d1	věrohodné
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
robustně	robustně	k6eAd1	robustně
stavěná	stavěný	k2eAgFnSc1d1	stavěná
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejtěžšího	těžký	k2eAgMnSc4d3	nejtěžší
hada	had	k1gMnSc4	had
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
anakonda	anakonda	k1gFnSc1	anakonda
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
jak	jak	k8xS	jak
mezi	mezi	k7c7	mezi
herpetology	herpetolog	k1gMnPc7	herpetolog
tak	tak	k6eAd1	tak
i	i	k9	i
laiky	laik	k1gMnPc7	laik
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
relativně	relativně	k6eAd1	relativně
málo	málo	k4c4	málo
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
rozličnými	rozličný	k2eAgMnPc7d1	rozličný
živočichy	živočich	k1gMnPc7	živočich
až	až	k6eAd1	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
kajmana	kajman	k1gMnSc2	kajman
či	či	k8xC	či
dospělé	dospělý	k2eAgFnSc2d1	dospělá
kapybary	kapybara	k1gFnSc2	kapybara
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
udušením	udušení	k1gNnSc7	udušení
ve	v	k7c6	v
smyčkách	smyčka	k1gFnPc6	smyčka
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
anakondu	anakonda	k1gFnSc4	anakonda
velkou	velká	k1gFnSc4	velká
nevyhodnocuje	vyhodnocovat	k5eNaImIp3nS	vyhodnocovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
anakonda	anakonda	k1gFnSc1	anakonda
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
ε	ε	k?	ε
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
plavec	plavec	k1gMnSc1	plavec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Anakondy	anakonda	k1gFnPc1	anakonda
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejdelší	dlouhý	k2eAgMnPc4d3	nejdelší
hady	had	k1gMnPc4	had
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
okolo	okolo	k7c2	okolo
3-4	[number]	k4	3-4
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
jedinci	jedinec	k1gMnPc1	jedinec
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
dorůst	dorůst	k5eAaPmF	dorůst
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
okolo	okolo	k7c2	okolo
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
krajt	krajta	k1gFnPc2	krajta
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgMnPc1d1	rekordní
jedinci	jedinec	k1gMnPc1	jedinec
měřící	měřící	k2eAgMnPc1d1	měřící
nad	nad	k7c4	nad
8	[number]	k4	8
m	m	kA	m
nejsou	být	k5eNaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
věrohodně	věrohodně	k6eAd1	věrohodně
potvrzené	potvrzený	k2eAgFnSc2d1	potvrzená
<g/>
.	.	kIx.	.
</s>
<s>
Anakondy	anakonda	k1gFnPc1	anakonda
jsou	být	k5eAaImIp3nP	být
nejtěžšími	těžký	k2eAgMnPc7d3	nejtěžší
hady	had	k1gMnPc7	had
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
30	[number]	k4	30
až	až	k9	až
70	[number]	k4	70
kg	kg	kA	kg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hmotnost	hmotnost	k1gFnSc1	hmotnost
velkých	velký	k2eAgMnPc2d1	velký
jedinců	jedinec	k1gMnPc2	jedinec
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
okolo	okolo	k7c2	okolo
100	[number]	k4	100
kg	kg	kA	kg
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
výjimečně	výjimečně	k6eAd1	výjimečně
snad	snad	k9	snad
i	i	k9	i
téměř	téměř	k6eAd1	téměř
200	[number]	k4	200
kg	kg	kA	kg
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
po	po	k7c6	po
odečtení	odečtení	k1gNnSc6	odečtení
váhy	váha	k1gFnSc2	váha
obsahu	obsah	k1gInSc2	obsah
žaludku	žaludek	k1gInSc2	žaludek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc3d2	veliký
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
hmotnostně	hmotnostně	k6eAd1	hmotnostně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
olivově	olivově	k6eAd1	olivově
nebo	nebo	k8xC	nebo
žlutohnědě	žlutohnědě	k6eAd1	žlutohnědě
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
okrouhlými	okrouhlý	k2eAgFnPc7d1	okrouhlá
tmavými	tmavý	k2eAgFnPc7d1	tmavá
až	až	k8xS	až
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
až	až	k9	až
ke	k	k7c3	k
koutkům	koutek	k1gInPc3	koutek
tlamy	tlama	k1gFnSc2	tlama
táhnou	táhnout	k5eAaImIp3nP	táhnout
nažloutlé	nažloutlý	k2eAgInPc1d1	nažloutlý
<g/>
,	,	kIx,	,
bílošedé	bílošedý	k2eAgInPc1d1	bílošedý
nebo	nebo	k8xC	nebo
načervenalé	načervenalý	k2eAgInPc1d1	načervenalý
<g/>
,	,	kIx,	,
černě	černě	k6eAd1	černě
olemované	olemovaný	k2eAgInPc4d1	olemovaný
proužky	proužek	k1gInPc4	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc1	její
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
velice	velice	k6eAd1	velice
pestře	pestro	k6eAd1	pestro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc1	dva
nejznámější	známý	k2eAgInPc1d3	nejznámější
druhy	druh	k1gInPc1	druh
anakond	anakonda	k1gFnPc2	anakonda
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
snad	snad	k9	snad
nejvíce	nejvíce	k6eAd1	nejvíce
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
hroznýšovitých	hroznýšovití	k1gMnPc2	hroznýšovití
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc4	jejich
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
vystouplé	vystouplý	k2eAgFnPc4d1	vystouplá
a	a	k8xC	a
položené	položený	k2eAgFnPc4d1	položená
navrch	navrch	k6eAd1	navrch
hlavy	hlava	k1gFnPc4	hlava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
toho	ten	k3xDgNnSc2	ten
důkazem	důkaz	k1gInSc7	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Anakondy	anakonda	k1gFnPc1	anakonda
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nikdy	nikdy	k6eAd1	nikdy
nevzdalují	vzdalovat	k5eNaImIp3nP	vzdalovat
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
stojaté	stojatý	k2eAgFnPc1d1	stojatá
nebo	nebo	k8xC	nebo
líně	líně	k6eAd1	líně
tekoucí	tekoucí	k2eAgFnSc6d1	tekoucí
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
hojné	hojný	k2eAgFnPc1d1	hojná
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
záplavových	záplavový	k2eAgFnPc6d1	záplavová
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
také	také	k9	také
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
střety	střet	k1gInPc1	střet
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
anakondy	anakonda	k1gFnPc1	anakonda
v	v	k7c6	v
době	doba	k1gFnSc6	doba
povodní	povodeň	k1gFnPc2	povodeň
často	často	k6eAd1	často
dostanou	dostat	k5eAaPmIp3nP	dostat
až	až	k9	až
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bažiny	bažina	k1gFnPc1	bažina
a	a	k8xC	a
řeky	řeka	k1gFnPc1	řeka
vysychají	vysychat	k5eAaImIp3nP	vysychat
<g/>
,	,	kIx,	,
upadá	upadat	k5eAaPmIp3nS	upadat
anakonda	anakonda	k1gFnSc1	anakonda
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
jakési	jakýsi	k3yIgFnSc2	jakýsi
strnulosti	strnulost	k1gFnSc2	strnulost
<g/>
.	.	kIx.	.
</s>
<s>
Výborně	výborně	k6eAd1	výborně
plave	plavat	k5eAaImIp3nS	plavat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
značnou	značný	k2eAgFnSc4d1	značná
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
biotopem	biotop	k1gInSc7	biotop
je	být	k5eAaImIp3nS	být
Amazonský	amazonský	k2eAgInSc1d1	amazonský
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
,	,	kIx,	,
Guyaně	Guyana	k1gFnSc6	Guyana
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
Trinidadu	Trinidad	k1gInSc6	Trinidad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
není	být	k5eNaImIp3nS	být
vybíravá	vybíravý	k2eAgFnSc1d1	vybíravá
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
loví	lovit	k5eAaImIp3nS	lovit
kajmany	kajman	k1gMnPc4	kajman
<g/>
,	,	kIx,	,
želvy	želva	k1gFnPc4	želva
<g/>
,	,	kIx,	,
antilopy	antilopa	k1gFnPc4	antilopa
a	a	k8xC	a
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
kapybary	kapybara	k1gFnPc4	kapybara
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
krmí	krmit	k5eAaImIp3nS	krmit
savci	savec	k1gMnPc7	savec
a	a	k8xC	a
ptáky	pták	k1gMnPc7	pták
přiměřené	přiměřený	k2eAgFnSc2d1	přiměřená
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Anakondy	anakonda	k1gFnPc1	anakonda
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
ulovit	ulovit	k5eAaPmF	ulovit
až	až	k9	až
sedmdesátikilové	sedmdesátikilový	k2eAgNnSc1d1	sedmdesátikilové
prase	prase	k1gNnSc1	prase
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
útočí	útočit	k5eAaImIp3nS	útočit
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
napadla	napadnout	k5eAaPmAgFnS	napadnout
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
tráví	trávit	k5eAaImIp3nS	trávit
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
zalezlá	zalezlý	k2eAgFnSc1d1	zalezlá
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
úkrytech	úkryt	k1gInPc6	úkryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
anakondě	anakonda	k1gFnSc3	anakonda
uznávány	uznáván	k2eAgInPc4d1	uznáván
tři	tři	k4xCgInPc4	tři
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
murinus	murinus	k1gMnSc1	murinus
</s>
</p>
<p>
<s>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
scytale	scytal	k1gMnSc5	scytal
</s>
</p>
<p>
<s>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
gigas	gigas	k1gMnSc1	gigas
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
anakondy	anakonda	k1gFnSc2	anakonda
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
velikosti	velikost	k1gFnSc3	velikost
a	a	k8xC	a
časté	častý	k2eAgFnSc3d1	častá
útočnosti	útočnost	k1gFnSc3	útočnost
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zachovávat	zachovávat	k5eAaImF	zachovávat
potřebná	potřebný	k2eAgNnPc4d1	potřebné
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
kousavé	kousavý	k2eAgMnPc4d1	kousavý
hady	had	k1gMnPc4	had
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
a	a	k8xC	a
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
zuby	zub	k1gInPc7	zub
pro	pro	k7c4	pro
lovení	lovení	k1gNnPc4	lovení
kluzkých	kluzký	k2eAgFnPc2d1	kluzká
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Kousnutí	kousnutí	k1gNnSc1	kousnutí
anakondou	anakonda	k1gFnSc7	anakonda
značně	značně	k6eAd1	značně
krvácí	krvácet	k5eAaImIp3nS	krvácet
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
nepříjemně	příjemně	k6eNd1	příjemně
zanítit	zanítit	k5eAaPmF	zanítit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
terárií	terárium	k1gNnPc2	terárium
jen	jen	k9	jen
nejzkušenějším	zkušený	k2eAgMnPc3d3	nejzkušenější
chovatelům	chovatel	k1gMnPc3	chovatel
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
anakondu	anakonda	k1gFnSc4	anakonda
velkou	velká	k1gFnSc4	velká
zařizujeme	zařizovat	k5eAaImIp1nP	zařizovat
co	co	k9	co
největší	veliký	k2eAgFnSc1d3	veliký
nádrž	nádrž	k1gFnSc1	nádrž
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
větví	větev	k1gFnSc7	větev
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
vyhřívaným	vyhřívaný	k2eAgInSc7d1	vyhřívaný
bazénem	bazén	k1gInSc7	bazén
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
tráví	trávit	k5eAaImIp3nS	trávit
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Teplotu	teplota	k1gFnSc4	teplota
26	[number]	k4	26
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
udržujeme	udržovat	k5eAaImIp1nP	udržovat
po	po	k7c4	po
celých	celý	k2eAgFnPc2d1	celá
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teráriích	terárium	k1gNnPc6	terárium
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nS	pářit
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
bývá	bývat	k5eAaImIp3nS	bývat
gravidní	gravidní	k2eAgFnSc1d1	gravidní
asi	asi	k9	asi
200	[number]	k4	200
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
od	od	k7c2	od
35	[number]	k4	35
do	do	k7c2	do
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
jich	on	k3xPp3gFnPc2	on
kolem	kolem	k6eAd1	kolem
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Anakondy	anakonda	k1gFnPc1	anakonda
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
chována	chovat	k5eAaImNgFnS	chovat
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Anakondu	anakonda	k1gFnSc4	anakonda
chovají	chovat	k5eAaImIp3nP	chovat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
např.	např.	kA	např.
ZOO	zoo	k1gFnSc7	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
či	či	k8xC	či
ZOO	zoo	k1gFnSc1	zoo
Dvorec	dvorec	k1gInSc1	dvorec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
obřích	obří	k2eAgInPc6d1	obří
exemplářích	exemplář	k1gInPc6	exemplář
anakondy	anakonda	k1gFnSc2	anakonda
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc1d3	veliký
délka	délka	k1gFnSc1	délka
anakondy	anakonda	k1gFnSc2	anakonda
845	[number]	k4	845
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
mnohem	mnohem	k6eAd1	mnohem
větších	veliký	k2eAgInPc6d2	veliký
exemplářích	exemplář	k1gInPc6	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
gigantičtí	gigantický	k2eAgMnPc1d1	gigantický
jedinci	jedinec	k1gMnPc1	jedinec
anakondy	anakonda	k1gFnSc2	anakonda
velké	velká	k1gFnSc2	velká
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
legendou	legenda	k1gFnSc7	legenda
amazonských	amazonský	k2eAgMnPc2d1	amazonský
indiánů	indián	k1gMnPc2	indián
o	o	k7c6	o
buiuně	buiuna	k1gFnSc6	buiuna
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc3d1	vodní
nestvůře	nestvůra	k1gFnSc3	nestvůra
se	s	k7c7	s
svítícíma	svítící	k2eAgNnPc7d1	svítící
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInPc4d1	připomínající
obřího	obří	k2eAgMnSc2d1	obří
hada	had	k1gMnSc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
845	[number]	k4	845
centimetrů	centimetr	k1gInPc2	centimetr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
jedinec	jedinec	k1gMnSc1	jedinec
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
samice	samice	k1gFnSc1	samice
vážící	vážící	k2eAgFnSc1d1	vážící
227	[number]	k4	227
kilogramů	kilogram	k1gInPc2	kilogram
a	a	k8xC	a
obvod	obvod	k1gInSc1	obvod
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
byl	být	k5eAaImAgInS	být
111	[number]	k4	111
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
tělo	tělo	k1gNnSc1	tělo
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
měřilo	měřit	k5eAaImAgNnS	měřit
35	[number]	k4	35
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
<g/>
Americký	americký	k2eAgMnSc1d1	americký
cestovatel	cestovatel	k1gMnSc1	cestovatel
Algot	Algot	k1gMnSc1	Algot
Lange	Lange	k1gFnSc4	Lange
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
anakondu	anakonda	k1gFnSc4	anakonda
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
25	[number]	k4	25
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvěrohodnějším	věrohodný	k2eAgNnSc7d3	nejvěrohodnější
tvrzením	tvrzení	k1gNnSc7	tvrzení
o	o	k7c6	o
gigantické	gigantický	k2eAgFnSc6d1	gigantická
anakondě	anakonda	k1gFnSc6	anakonda
je	být	k5eAaImIp3nS	být
zpráva	zpráva	k1gFnSc1	zpráva
anglického	anglický	k2eAgMnSc2d1	anglický
cestovatele	cestovatel	k1gMnSc2	cestovatel
Percyho	Percy	k1gMnSc2	Percy
Fawcetta	Fawcett	k1gMnSc2	Fawcett
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
velice	velice	k6eAd1	velice
barvitě	barvitě	k6eAd1	barvitě
popsal	popsat	k5eAaPmAgMnS	popsat
zastřelení	zastřelení	k1gNnSc4	zastřelení
jedince	jedinec	k1gMnSc2	jedinec
dlouhého	dlouhý	k2eAgMnSc2d1	dlouhý
18,6	[number]	k4	18,6
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
společnost	společnost	k1gFnSc1	společnost
vypsala	vypsat	k5eAaPmAgFnS	vypsat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
exemplář	exemplář	k1gInSc4	exemplář
delší	dlouhý	k2eAgFnSc2d2	delší
9,15	[number]	k4	9,15
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
množství	množství	k1gNnSc4	množství
hlášení	hlášení	k1gNnSc2	hlášení
o	o	k7c6	o
mnohem	mnohem	k6eAd1	mnohem
větších	veliký	k2eAgFnPc6d2	veliký
jedincích	jedinec	k1gMnPc6	jedinec
odměna	odměna	k1gFnSc1	odměna
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
adresáta	adresát	k1gMnSc4	adresát
stále	stále	k6eAd1	stále
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
exemplářů	exemplář	k1gInPc2	exemplář
nebyl	být	k5eNaImAgInS	být
věrohodně	věrohodně	k6eAd1	věrohodně
doložen	doložit	k5eAaPmNgInS	doložit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dobrodružných	dobrodružný	k2eAgInPc6d1	dobrodružný
hororech	horor	k1gInPc6	horor
a	a	k8xC	a
filmech	film	k1gInPc6	film
žánrově	žánrově	k6eAd1	žánrově
podobných	podobný	k2eAgInPc2d1	podobný
Spielbergovým	Spielbergův	k2eAgFnPc3d1	Spielbergova
Čelistem	čelist	k1gFnPc3	čelist
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
variace	variace	k1gFnPc4	variace
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
vraždícího	vraždící	k2eAgMnSc2d1	vraždící
predátora	predátor	k1gMnSc2	predátor
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Ušetřen	ušetřit	k5eAaPmNgInS	ušetřit
nezůstal	zůstat	k5eNaPmAgInS	zůstat
ani	ani	k9	ani
druh	druh	k1gInSc1	druh
anakond	anakonda	k1gFnPc2	anakonda
velkých	velká	k1gFnPc2	velká
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
např.	např.	kA	např.
snímek	snímek	k1gInSc1	snímek
Anakonda	anakonda	k1gFnSc1	anakonda
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
s	s	k7c7	s
Jennifer	Jennifer	k1gMnSc1	Jennifer
Lopez	Lopez	k1gMnSc1	Lopez
a	a	k8xC	a
Jonem	Jonem	k1gInSc1	Jonem
Voightem	Voight	k1gInSc7	Voight
či	či	k8xC	či
kritikou	kritika	k1gFnSc7	kritika
poměrně	poměrně	k6eAd1	poměrně
nepříznivě	příznivě	k6eNd1	příznivě
přijatý	přijatý	k2eAgInSc1d1	přijatý
film	film	k1gInSc1	film
Anakonda	anakonda	k1gFnSc1	anakonda
<g/>
:	:	kIx,	:
Honba	honba	k1gFnSc1	honba
za	za	k7c7	za
krvavou	krvavý	k2eAgFnSc7d1	krvavá
orchidejí	orchidea	k1gFnSc7	orchidea
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RIVAS	RIVAS	kA	RIVAS
<g/>
,	,	kIx,	,
Jesús	Jesús	k1gInSc4	Jesús
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Life	Life	k1gInSc1	Life
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Green	Green	k1gInSc1	Green
Anaconda	Anaconda	k1gFnSc1	Anaconda
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
With	With	k1gMnSc1	With
Emphasis	Emphasis	k1gFnSc2	Emphasis
on	on	k3xPp3gMnSc1	on
its	its	k?	its
Redproductive	Redproductiv	k1gInSc5	Redproductiv
Biology	biolog	k1gMnPc7	biolog
<g/>
.	.	kIx.	.
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
170	[number]	k4	170
s.	s.	k?	s.
Doktorská	doktorský	k2eAgFnSc1d1	doktorská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
University	universita	k1gFnPc4	universita
of	of	k?	of
Tennessee	Tennessee	k1gInSc1	Tennessee
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
anakonda	anakonda	k1gFnSc1	anakonda
velká	velká	k1gFnSc1	velká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
anakonda	anakonda	k1gFnSc1	anakonda
velká	velký	k2eAgFnSc1d1	velká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgMnPc1d3	nejdelší
hadi	had	k1gMnPc1	had
na	na	k7c6	na
Extremescience	Extremescienka	k1gFnSc6	Extremescienka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
