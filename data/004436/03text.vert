<s>
Přerov	Přerov	k1gInSc1	Přerov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Prerau	Preraus	k1gInSc3	Preraus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
21	[number]	k4	21
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Olomouce	Olomouc	k1gFnSc2	Olomouc
v	v	k7c6	v
Hornomoravském	hornomoravský	k2eAgInSc6d1	hornomoravský
úvalu	úval	k1gInSc6	úval
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Bečvě	Bečva	k1gFnSc6	Bečva
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
58,48	[number]	k4	58,48
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
okresu	okres	k1gInSc2	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgInPc2d1	významný
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
(	(	kIx(	(
<g/>
PRECHEZA	PRECHEZA	kA	PRECHEZA
<g/>
,	,	kIx,	,
Přerovské	přerovský	k2eAgFnPc1d1	přerovská
strojírny	strojírna	k1gFnPc1	strojírna
<g/>
,	,	kIx,	,
Meopta	Meopto	k1gNnPc1	Meopto
<g/>
,	,	kIx,	,
Kazeto	kazeta	k1gFnSc5	kazeta
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
městem	město	k1gNnSc7	město
vojenským	vojenský	k2eAgNnSc7d1	vojenské
<g/>
,	,	kIx,	,
sídlila	sídlit	k5eAaImAgFnS	sídlit
zde	zde	k6eAd1	zde
23	[number]	k4	23
<g/>
.	.	kIx.	.
základna	základna	k1gFnSc1	základna
vrtulníkového	vrtulníkový	k2eAgNnSc2d1	vrtulníkové
letectva	letectvo	k1gNnSc2	letectvo
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
Přerov	Přerov	k1gInSc1	Přerov
křižovatkou	křižovatka	k1gFnSc7	křižovatka
Průplavu	průplav	k1gInSc2	průplav
Dunaj-Odra-Labe	Dunaj-Odra-Lab	k1gInSc5	Dunaj-Odra-Lab
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
hlavních	hlavní	k2eAgInPc2d1	hlavní
přístavů	přístav	k1gInPc2	přístav
(	(	kIx(	(
<g/>
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c4	o
studii	studie	k1gFnSc4	studie
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
projektu	projekt	k1gInSc3	projekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Horní	horní	k2eAgNnSc1d1	horní
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgNnSc1d1	někdejší
podhradí	podhradí	k1gNnSc1	podhradí
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Dolní	dolní	k2eAgNnSc1d1	dolní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnPc1	historie
raně	raně	k6eAd1	raně
středověkého	středověký	k2eAgNnSc2d1	středověké
hradiště	hradiště	k1gNnSc2	hradiště
na	na	k7c6	na
území	území	k1gNnSc6	území
Přerova	Přerov	k1gInSc2	Přerov
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1019	[number]	k4	1019
je	být	k5eAaImIp3nS	být
připomínána	připomínán	k2eAgFnSc1d1	připomínána
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
Michala	Michal	k1gMnSc2	Michal
Lutovského	Lutovský	k1gMnSc2	Lutovský
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Přerovu	Přerov	k1gInSc3	Přerov
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
(	(	kIx(	(
<g/>
po	po	k7c6	po
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
1131	[number]	k4	1131
<g/>
)	)	kIx)	)
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
přerovský	přerovský	k2eAgInSc1d1	přerovský
hradní	hradní	k2eAgInSc1d1	hradní
velkofarní	velkofarní	k2eAgInSc1d1	velkofarní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
mezi	mezi	k7c7	mezi
sedmi	sedm	k4xCc2	sedm
nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
(	(	kIx(	(
<g/>
krstnými	krstná	k1gFnPc7	krstná
<g/>
)	)	kIx)	)
kostely	kostel	k1gInPc1	kostel
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1256	[number]	k4	1256
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgInS	povýšit
králem	král	k1gMnSc7	král
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
královské	královský	k2eAgNnSc4d1	královské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Přerova	Přerov	k1gInSc2	Přerov
je	být	k5eAaImIp3nS	být
také	také	k9	také
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
rody	rod	k1gInPc7	rod
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
a	a	k8xC	a
Žerotínů	Žerotín	k1gInPc2	Žerotín
působením	působení	k1gNnSc7	působení
evangelické	evangelický	k2eAgFnSc2d1	evangelická
církve	církev	k1gFnSc2	církev
zvané	zvaný	k2eAgFnSc2d1	zvaná
jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
náleželi	náležet	k5eAaImAgMnP	náležet
významní	významný	k2eAgMnPc1d1	významný
učenci	učenec	k1gMnPc1	učenec
jako	jako	k8xC	jako
přerovský	přerovský	k2eAgMnSc1d1	přerovský
rodák	rodák	k1gMnSc1	rodák
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
a	a	k8xC	a
Učitel	učitel	k1gMnSc1	učitel
národů	národ	k1gInPc2	národ
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
nastal	nastat	k5eAaPmAgInS	nastat
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
hlavní	hlavní	k2eAgFnSc2d1	hlavní
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Bohumína	Bohumín	k1gInSc2	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
Horního	horní	k2eAgNnSc2d1	horní
náměstí	náměstí	k1gNnSc2	náměstí
–	–	k?	–
lze	lze	k6eAd1	lze
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaPmF	nalézt
domy	dům	k1gInPc4	dům
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Přerovský	přerovský	k2eAgInSc1d1	přerovský
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
věž	věž	k1gFnSc1	věž
má	mít	k5eAaImIp3nS	mít
románský	románský	k2eAgInSc4d1	románský
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
i	i	k9	i
zbytky	zbytek	k1gInPc4	zbytek
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
Muzeum	muzeum	k1gNnSc1	muzeum
Komenského	Komenský	k1gMnSc2	Komenský
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
zásluhou	zásluha	k1gFnSc7	zásluha
Františka	František	k1gMnSc2	František
Slaměníka	Slaměník	k1gMnSc2	Slaměník
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
násilně	násilně	k6eAd1	násilně
potlačeno	potlačen	k2eAgNnSc4d1	potlačeno
přerovské	přerovský	k2eAgNnSc4d1	přerovské
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
následoval	následovat	k5eAaImAgInS	následovat
masakr	masakr	k1gInSc4	masakr
na	na	k7c6	na
Švédských	švédský	k2eAgFnPc6d1	švédská
šancích	šance	k1gFnPc6	šance
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
obdobím	období	k1gNnSc7	období
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Přerov	Přerov	k1gInSc4	Přerov
éra	éra	k1gFnSc1	éra
komunismu	komunismus	k1gInSc3	komunismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
významné	významný	k2eAgNnSc1d1	významné
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Přerov	Přerov	k1gInSc1	Přerov
postiženo	postihnout	k5eAaPmNgNnS	postihnout
katastrofální	katastrofální	k2eAgFnSc7d1	katastrofální
povodní	povodeň	k1gFnSc7	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
Přerov	Přerov	k1gInSc1	Přerov
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
primátor	primátor	k1gMnSc1	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
samosprávu	samospráva	k1gFnSc4	samospráva
představuje	představovat	k5eAaImIp3nS	představovat
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
s	s	k7c7	s
35	[number]	k4	35
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
primátorem	primátor	k1gMnSc7	primátor
Vladimír	Vladimír	k1gMnSc1	Vladimír
Puchalský	Puchalský	k2eAgMnSc1d1	Puchalský
z	z	k7c2	z
uskupení	uskupení	k1gNnSc2	uskupení
Společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
Přerov	Přerov	k1gInSc4	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Koalici	koalice	k1gFnSc4	koalice
utvořily	utvořit	k5eAaPmAgInP	utvořit
tyto	tento	k3xDgInPc1	tento
subjekty	subjekt	k1gInPc1	subjekt
<g/>
:	:	kIx,	:
hnutí	hnutí	k1gNnPc1	hnutí
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
Společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
Přerov	Přerov	k1gInSc4	Přerov
<g/>
,	,	kIx,	,
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
a	a	k8xC	a
Za	za	k7c2	za
prosperitu	prosperita	k1gFnSc4	prosperita
Přerova	Přerov	k1gInSc2	Přerov
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
dispnovoala	dispnovoat	k5eAaImAgFnS	dispnovoat
většinou	většinou	k6eAd1	většinou
21	[number]	k4	21
z	z	k7c2	z
35	[number]	k4	35
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
za	za	k7c4	za
Přerov	Přerov	k1gInSc4	Přerov
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgInP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hodnoty	hodnota	k1gFnPc1	hodnota
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
klesající	klesající	k2eAgInSc1d1	klesající
trend	trend	k1gInSc1	trend
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
byla	být	k5eAaImAgNnP	být
objevena	objevit	k5eAaPmNgNnP	objevit
rozsáhlá	rozsáhlý	k2eAgNnPc1d1	rozsáhlé
archeologická	archeologický	k2eAgNnPc1d1	Archeologické
naleziště	naleziště	k1gNnPc1	naleziště
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstarší	starý	k2eAgFnSc1d3	nejstarší
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
cenné	cenný	k2eAgInPc1d1	cenný
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
odborníků	odborník	k1gMnPc2	odborník
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
předmostské	předmostský	k2eAgInPc4d1	předmostský
nálezy	nález	k1gInPc4	nález
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
<g/>
,	,	kIx,	,
když	když	k8xS	když
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
<g/>
,	,	kIx,	,
známého	známý	k2eAgNnSc2d1	známé
již	již	k6eAd1	již
z	z	k7c2	z
výzkumů	výzkum	k1gInPc2	výzkum
Martina	Martin	k1gMnSc2	Martin
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
zůstal	zůstat	k5eAaPmAgInS	zůstat
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
dokladů	doklad	k1gInPc2	doklad
z	z	k7c2	z
období	období	k1gNnSc2	období
slovanského	slovanský	k2eAgNnSc2d1	slovanské
osídlení	osídlení	k1gNnSc2	osídlení
levý	levý	k2eAgInSc4d1	levý
břeh	břeh	k1gInSc4	břeh
Bečvy	Bečva	k1gFnSc2	Bečva
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
několik	několik	k4yIc1	několik
nových	nový	k2eAgInPc2d1	nový
nálezů	nález	k1gInPc2	nález
změnilo	změnit	k5eAaPmAgNnS	změnit
i	i	k9	i
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
více	hodně	k6eAd2	hodně
nálezů	nález	k1gInPc2	nález
starších	starý	k2eAgInPc2d2	starší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
nezveřejněných	zveřejněný	k2eNgInPc2d1	nezveřejněný
i	i	k8xC	i
nálezů	nález	k1gInPc2	nález
nových	nový	k2eAgInPc2d1	nový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
nynějšího	nynější	k2eAgNnSc2d1	nynější
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
okolí	okolí	k1gNnSc6	okolí
probíhal	probíhat	k5eAaImAgInS	probíhat
život	život	k1gInSc4	život
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
údobích	údobí	k1gNnPc6	údobí
pravěku	pravěk	k1gInSc2	pravěk
než	než	k8xS	než
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
naučná	naučný	k2eAgFnSc1d1	naučná
vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
Stezka	stezka	k1gFnSc1	stezka
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
vedoucí	vedoucí	k1gMnPc1	vedoucí
z	z	k7c2	z
Předmostí	předmostí	k1gNnSc2	předmostí
nad	nad	k7c7	nad
́	́	k?	́
<g/>
přerovskou	přerovský	k2eAgFnSc4d1	přerovská
rokli	rokle	k1gFnSc4	rokle
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
přírodního	přírodní	k2eAgInSc2d1	přírodní
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
školní	školní	k2eAgInSc4d1	školní
kopec	kopec	k1gInSc4	kopec
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
Památník	památník	k1gInSc1	památník
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc4d1	krásný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
Přerov	Přerov	k1gInSc4	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
i	i	k9	i
lom	lom	k1gInSc4	lom
Žernava	Žernava	k1gFnSc1	Žernava
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
stezce	stezka	k1gFnSc6	stezka
Přerovská	přerovský	k2eAgFnSc1d1	přerovská
rokle	rokle	k1gFnSc1	rokle
a	a	k8xC	a
socha	socha	k1gFnSc1	socha
mláděte	mládě	k1gNnSc2	mládě
mamuta	mamut	k1gMnSc2	mamut
Toma	Tom	k1gMnSc2	Tom
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
bodem	bod	k1gInSc7	bod
stezky	stezka	k1gFnSc2	stezka
je	být	k5eAaImIp3nS	být
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
na	na	k7c4	na
Přerov	Přerov	k1gInSc4	Přerov
tentokrát	tentokrát	k6eAd1	tentokrát
z	z	k7c2	z
Čekyňského	Čekyňský	k2eAgInSc2d1	Čekyňský
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Žebračka	žebračka	k1gFnSc1	žebračka
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
silnicí	silnice	k1gFnSc7	silnice
Přerov-Prosenice	Přerov-Prosenice	k1gFnSc2	Přerov-Prosenice
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodní	severovýchodní	k2eAgFnSc7d1	severovýchodní
částí	část	k1gFnSc7	část
NPR	NPR	kA	NPR
protéká	protékat	k5eAaImIp3nS	protékat
umělý	umělý	k2eAgInSc4d1	umělý
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
-	-	kIx~	-
mlýnský	mlýnský	k2eAgInSc4d1	mlýnský
a	a	k8xC	a
elektrárenský	elektrárenský	k2eAgInSc4d1	elektrárenský
náhon	náhon	k1gInSc4	náhon
Strhanec	Strhanec	k1gMnSc1	Strhanec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
již	již	k6eAd1	již
získal	získat	k5eAaPmAgMnS	získat
přírodě	příroda	k1gFnSc3	příroda
blízký	blízký	k2eAgInSc1d1	blízký
charakter	charakter	k1gInSc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
NPR	NPR	kA	NPR
Žebračka	žebračka	k1gFnSc1	žebračka
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolní	údolní	k2eAgFnSc6d1	údolní
nivě	niva	k1gFnSc6	niva
řeky	řeka	k1gFnSc2	řeka
Bečvy	Bečva	k1gFnSc2	Bečva
<g/>
,	,	kIx,	,
podloží	podložit	k5eAaPmIp3nP	podložit
je	on	k3xPp3gInPc4	on
tvořené	tvořený	k2eAgInPc4d1	tvořený
štěrkopísky	štěrkopísek	k1gInPc4	štěrkopísek
pleistocenního	pleistocenní	k2eAgNnSc2d1	pleistocenní
až	až	k8xS	až
holocénního	holocénní	k2eAgNnSc2d1	holocénní
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
překryty	překrýt	k5eAaPmNgInP	překrýt
povodňovými	povodňový	k2eAgFnPc7d1	povodňová
hlínami	hlína	k1gFnPc7	hlína
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
je	být	k5eAaImIp3nS	být
rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
se	s	k7c7	s
sníženinami	sníženina	k1gFnPc7	sníženina
celoročně	celoročně	k6eAd1	celoročně
suchých	suchý	k2eAgFnPc2d1	suchá
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
občas	občas	k6eAd1	občas
zvodňovaných	zvodňovaný	k2eAgInPc2d1	zvodňovaný
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
průtočných	průtočný	k2eAgNnPc2d1	průtočné
<g/>
,	,	kIx,	,
říčních	říční	k2eAgNnPc2d1	říční
koryt	koryto	k1gNnPc2	koryto
(	(	kIx(	(
<g/>
nazývaných	nazývaný	k2eAgNnPc2d1	nazývané
"	"	kIx"	"
<g/>
smuhy	smuha	k1gFnPc1	smuha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žebračka	žebračka	k1gFnSc1	žebračka
představuje	představovat	k5eAaImIp3nS	představovat
unikátní	unikátní	k2eAgInSc4d1	unikátní
zbytek	zbytek	k1gInSc4	zbytek
původně	původně	k6eAd1	původně
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
lužních	lužní	k2eAgInPc2d1	lužní
lesů	les	k1gInPc2	les
údolní	údolní	k2eAgFnSc2d1	údolní
nivy	niva	k1gFnSc2	niva
Bečvy	Bečva	k1gFnSc2	Bečva
<g/>
.	.	kIx.	.
</s>
<s>
Dřevinná	dřevinný	k2eAgFnSc1d1	dřevinná
skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
lesního	lesní	k2eAgInSc2d1	lesní
komplexu	komplex	k1gInSc2	komplex
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vcelku	vcelku	k6eAd1	vcelku
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
přirozené	přirozený	k2eAgFnSc3d1	přirozená
skladbě	skladba	k1gFnSc3	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
lužního	lužní	k2eAgInSc2d1	lužní
lesa	les	k1gInSc2	les
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
luhu	luh	k1gInSc2	luh
<g/>
,	,	kIx,	,
význačné	význačný	k2eAgFnPc1d1	význačná
dřeviny	dřevina	k1gFnPc1	dřevina
jsou	být	k5eAaImIp3nP	být
dub	dub	k1gInSc4	dub
letní	letní	k2eAgInSc4d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc4	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jasan	jasan	k1gInSc1	jasan
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
(	(	kIx(	(
<g/>
Fraxinus	Fraxinus	k1gInSc1	Fraxinus
excelsior	excelsior	k1gInSc1	excelsior
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc4	jilm
habrolistý	habrolistý	k2eAgInSc4d1	habrolistý
(	(	kIx(	(
<g/>
Ulmus	Ulmus	k1gInSc4	Ulmus
minor	minor	k2eAgInSc4d1	minor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
babyka	babyka	k1gFnSc1	babyka
(	(	kIx(	(
<g/>
Acer	Acer	k1gInSc1	Acer
campestre	campestr	k1gInSc5	campestr
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc1	jilm
vaz	vaz	k1gInSc1	vaz
(	(	kIx(	(
<g/>
Ulmus	Ulmus	k1gInSc1	Ulmus
laevis	laevis	k1gFnSc2	laevis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střemcha	střemcha	k1gFnSc1	střemcha
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gMnSc1	Prunus
padus	padus	k1gMnSc1	padus
<g/>
)	)	kIx)	)
s	s	k7c7	s
podrostem	podrost	k1gInSc7	podrost
keřů	keř	k1gInPc2	keř
a	a	k8xC	a
především	především	k9	především
bylinného	bylinný	k2eAgNnSc2d1	bylinné
patra	patro	k1gNnSc2	patro
tvořeného	tvořený	k2eAgInSc2d1	tvořený
typickou	typický	k2eAgFnSc7d1	typická
květenou	květena	k1gFnSc7	květena
lužního	lužní	k2eAgInSc2d1	lužní
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
k	k	k7c3	k
nejvýznačnějším	význačný	k2eAgInSc6d3	nejvýznačnější
patří	patřit	k5eAaImIp3nS	patřit
česnek	česnek	k1gInSc1	česnek
medvědí	medvědí	k2eAgInSc1d1	medvědí
(	(	kIx(	(
<g/>
Allium	Allium	k1gNnSc1	Allium
ursinum	ursinum	k1gNnSc1	ursinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sasanka	sasanka	k1gFnSc1	sasanka
hajní	hajní	k2eAgFnSc1d1	hajní
(	(	kIx(	(
<g/>
Anemone	Anemon	k1gInSc5	Anemon
nemorosa	nemorosa	k1gFnSc1	nemorosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
se	se	k3xPyFc4	se
také	také	k9	také
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
světového	světový	k2eAgInSc2d1	světový
hokeje	hokej	k1gInSc2	hokej
<g/>
:	:	kIx,	:
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zlínem	Zlín	k1gInSc7	Zlín
hostil	hostit	k5eAaImAgInS	hostit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
žen	žena	k1gFnPc2	žena
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
překonalo	překonat	k5eAaPmAgNnS	překonat
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
odehrála	odehrát	k5eAaPmAgFnS	odehrát
skupina	skupina	k1gFnSc1	skupina
B.	B.	kA	B.
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgInP	konat
boje	boj	k1gInPc1	boj
o	o	k7c4	o
celkově	celkově	k6eAd1	celkově
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
hokejistky	hokejistka	k1gFnPc1	hokejistka
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgNnSc6d1	celkové
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
finskou	finský	k2eAgFnSc7d1	finská
reprezentací	reprezentace	k1gFnSc7	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
šampionát	šampionát	k1gInSc4	šampionát
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
17	[number]	k4	17
480	[number]	k4	480
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
11	[number]	k4	11
700	[number]	k4	700
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
a	a	k8xC	a
5	[number]	k4	5
780	[number]	k4	780
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
Přerov	Přerov	k1gInSc1	Přerov
žije	žít	k5eAaImIp3nS	žít
hokejem	hokej	k1gInSc7	hokej
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
postup	postup	k1gInSc1	postup
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
národní	národní	k2eAgFnSc2d1	národní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
letošní	letošní	k2eAgFnSc2d1	letošní
sezóny	sezóna	k1gFnSc2	sezóna
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
WSM	WSM	kA	WSM
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
ZUBR	zubr	k1gMnSc1	zubr
Přerov	Přerov	k1gInSc4	Přerov
navrátil	navrátit	k5eAaPmAgMnS	navrátit
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
si	se	k3xPyFc3	se
Zubři	zubr	k1gMnPc1	zubr
udržují	udržovat	k5eAaImIp3nP	udržovat
průměr	průměr	k1gInSc4	průměr
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
2400	[number]	k4	2400
diváků	divák	k1gMnPc2	divák
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Samospráva	samospráva	k1gFnSc1	samospráva
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
moravskou	moravský	k2eAgFnSc4d1	Moravská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
13	[number]	k4	13
číslovaných	číslovaný	k2eAgFnPc2d1	číslovaná
evidenčních	evidenční	k2eAgFnPc2d1	evidenční
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
ani	ani	k8xC	ani
obvody	obvod	k1gInPc4	obvod
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
I-Město	I-Města	k1gMnSc5	I-Města
Přerov	Přerov	k1gInSc1	Přerov
II-Předmostí	II-Předmost	k1gFnPc2	II-Předmost
Přerov	Přerov	k1gInSc4	Přerov
III-Lověšice	III-Lověšice	k1gFnSc2	III-Lověšice
Přerov	Přerov	k1gInSc4	Přerov
IV-Kozlovice	IV-Kozlovice	k1gFnSc2	IV-Kozlovice
Přerov	Přerov	k1gInSc4	Přerov
V-Dluhonice	V-Dluhonice	k1gFnSc2	V-Dluhonice
Přerov	Přerov	k1gInSc1	Přerov
VI-Újezdec	VI-Újezdec	k1gMnSc1	VI-Újezdec
Přerov	Přerov	k1gInSc4	Přerov
VII-Čekyně	VII-Čekyně	k1gFnSc2	VII-Čekyně
Přerov	Přerov	k1gInSc1	Přerov
VIII-Henčlov	VIII-Henčlov	k1gInSc1	VIII-Henčlov
Přerov	Přerov	k1gInSc4	Přerov
IX-Lýsky	IX-Lýska	k1gFnSc2	IX-Lýska
Přerov	Přerov	k1gInSc4	Přerov
X-Popovice	X-Popovice	k1gFnSc2	X-Popovice
Přerov	Přerov	k1gInSc4	Přerov
XI-Vinary	XI-Vinara	k1gFnSc2	XI-Vinara
Přerov	Přerov	k1gInSc4	Přerov
XII-Žeravice	XII-Žeravice	k1gFnSc2	XII-Žeravice
Přerov	Přerov	k1gInSc1	Přerov
XIII-Penčice	XIII-Penčice	k1gFnSc2	XIII-Penčice
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusily	pokusit	k5eAaPmAgFnP	pokusit
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1993	[number]	k4	1993
se	se	k3xPyFc4	se
v	v	k7c6	v
části	část	k1gFnSc6	část
Přerov	Přerov	k1gInSc1	Přerov
V-Dluhonice	V-Dluhonika	k1gFnSc3	V-Dluhonika
konalo	konat	k5eAaImAgNnS	konat
místní	místní	k2eAgNnSc1d1	místní
referendum	referendum	k1gNnSc1	referendum
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
266	[number]	k4	266
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
občanů	občan	k1gMnPc2	občan
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
227	[number]	k4	227
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
152	[number]	k4	152
pro	pro	k7c4	pro
oddělení	oddělení	k1gNnSc4	oddělení
a	a	k8xC	a
71	[number]	k4	71
proti	proti	k7c3	proti
oddělení	oddělení	k1gNnSc3	oddělení
<g/>
,	,	kIx,	,
4	[number]	k4	4
hlasy	hlas	k1gInPc1	hlas
byly	být	k5eAaImAgInP	být
neplatné	platný	k2eNgInPc1d1	neplatný
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
referenda	referendum	k1gNnSc2	referendum
byl	být	k5eAaImAgInS	být
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc1	oddělení
však	však	k9	však
nepovolilo	povolit	k5eNaPmAgNnS	povolit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přerušení	přerušení	k1gNnSc2	přerušení
souvislého	souvislý	k2eAgNnSc2d1	souvislé
území	území	k1gNnSc2	území
obce	obec	k1gFnSc2	obec
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1993	[number]	k4	1993
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
místní	místní	k2eAgNnSc1d1	místní
referendum	referendum	k1gNnSc1	referendum
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
v	v	k7c6	v
části	část	k1gFnSc6	část
Přerov	Přerov	k1gInSc1	Přerov
XI-Vinary	XI-Vinar	k1gInPc1	XI-Vinar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
552	[number]	k4	552
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
občanů	občan	k1gMnPc2	občan
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
373	[number]	k4	373
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
151	[number]	k4	151
pro	pro	k7c4	pro
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
,	,	kIx,	,
193	[number]	k4	193
proti	proti	k7c3	proti
odtržení	odtržení	k1gNnSc3	odtržení
a	a	k8xC	a
7	[number]	k4	7
hlasů	hlas	k1gInPc2	hlas
bylo	být	k5eAaImAgNnS	být
neplatných	platný	k2eNgNnPc2d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
referenda	referendum	k1gNnSc2	referendum
byl	být	k5eAaImAgInS	být
záporný	záporný	k2eAgInSc1d1	záporný
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
místní	místní	k2eAgNnSc1d1	místní
referendum	referendum	k1gNnSc1	referendum
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
v	v	k7c6	v
části	část	k1gFnSc6	část
Přerov	Přerov	k1gInSc4	Přerov
VI-Újezdec	VI-Újezdec	k1gMnSc1	VI-Újezdec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
517	[number]	k4	517
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
občanů	občan	k1gMnPc2	občan
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
351	[number]	k4	351
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
156	[number]	k4	156
pro	pro	k7c4	pro
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
,	,	kIx,	,
191	[number]	k4	191
proti	proti	k7c3	proti
odtržení	odtržení	k1gNnSc3	odtržení
a	a	k8xC	a
26	[number]	k4	26
hlasů	hlas	k1gInPc2	hlas
bylo	být	k5eAaImAgNnS	být
neplatných	platný	k2eNgNnPc2d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
referenda	referendum	k1gNnSc2	referendum
byl	být	k5eAaImAgInS	být
záporný	záporný	k2eAgInSc1d1	záporný
<g/>
.	.	kIx.	.
</s>
<s>
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
(	(	kIx(	(
<g/>
1523	[number]	k4	1523
<g/>
–	–	k?	–
<g/>
1571	[number]	k4	1571
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
humanistický	humanistický	k2eAgMnSc1d1	humanistický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
české	český	k2eAgFnSc2d1	Česká
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
díla	dílo	k1gNnSc2	dílo
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
Jakob	Jakob	k1gMnSc1	Jakob
Gartner	Gartner	k1gMnSc1	Gartner
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
architekt	architekt	k1gMnSc1	architekt
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Josef	Josef	k1gMnSc1	Josef
Syrový	syrový	k2eAgMnSc1d1	syrový
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
Otakar	Otakar	k1gMnSc1	Otakar
Griese	Griese	k1gFnSc1	Griese
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hermetik	hermetik	k1gMnSc1	hermetik
<g/>
,	,	kIx,	,
astrolog	astrolog	k1gMnSc1	astrolog
<g/>
,	,	kIx,	,
martinista	martinista	k1gMnSc1	martinista
<g/>
,	,	kIx,	,
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Rudolf	Rudolf	k1gMnSc1	Rudolf
Weigl	Weigl	k1gMnSc1	Weigl
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
imunolog	imunolog	k1gMnSc1	imunolog
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
očkovací	očkovací	k2eAgFnSc2d1	očkovací
látky	látka	k1gFnSc2	látka
proti	proti	k7c3	proti
skvrnitému	skvrnitý	k2eAgInSc3d1	skvrnitý
tyfu	tyf	k1gInSc3	tyf
Rudolf	Rudolf	k1gMnSc1	Rudolf
Weiser	Weiser	k1gMnSc1	Weiser
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
František	František	k1gMnSc1	František
Rasch	Rasch	k1gMnSc1	Rasch
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
námořník	námořník	k1gMnSc1	námořník
<g/>
,	,	kIx,	,
revolucionář	revolucionář	k1gMnSc1	revolucionář
a	a	k8xC	a
účastník	účastník	k1gMnSc1	účastník
vzpoury	vzpoura	k1gFnSc2	vzpoura
námořníků	námořník	k1gMnPc2	námořník
rakouského	rakouský	k2eAgNnSc2d1	rakouské
válečného	válečný	k2eAgNnSc2d1	válečné
loďstva	loďstvo	k1gNnSc2	loďstvo
v	v	k7c6	v
Boce	boka	k1gFnSc6	boka
Kotorské	kotorský	k2eAgFnSc2d1	Kotorská
Karel	Karel	k1gMnSc1	Karel
Janoušek	Janoušek	k1gMnSc1	Janoušek
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
prvního	první	k4xOgInSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
maršál	maršál	k1gMnSc1	maršál
<g />
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Vignati	Vignati	k1gMnSc1	Vignati
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Eduard	Eduard	k1gMnSc1	Eduard
Světlík	Světlík	k1gMnSc1	Světlík
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
Oldřich	Oldřich	k1gMnSc1	Oldřich
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
básník	básník	k1gMnSc1	básník
Bedřich	Bedřich	k1gMnSc1	Bedřich
Reicin	Reicin	k1gMnSc1	Reicin
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
přednosta	přednosta	k1gMnSc1	přednosta
Obranného	obranný	k2eAgNnSc2d1	obranné
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
protistátního	protistátní	k2eAgNnSc2d1	protistátní
spikleneckého	spiklenecký	k2eAgNnSc2d1	spiklenecké
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgMnSc1d1	popraven
Vilém	Vilém	k1gMnSc1	Vilém
Vignati	Vignati	k1gMnSc1	Vignati
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Josef	Josef	k1gMnSc1	Josef
<g />
.	.	kIx.	.
</s>
<s>
Kainar	Kainar	k1gInSc1	Kainar
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Jiřina	Jiřina	k1gFnSc1	Jiřina
Hauková	Hauková	k1gFnSc1	Hauková
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
básnířka	básnířka	k1gFnSc1	básnířka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
Lubomír	Lubomír	k1gMnSc1	Lubomír
Kostelka	Kostelka	k1gMnSc1	Kostelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
plk.	plk.	kA	plk.
Ing.	ing.	kA	ing.
Břetislav	Břetislav	k1gMnSc1	Břetislav
Cagášek	Cagášek	k1gMnSc1	Cagášek
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Přerova	Přerov	k1gInSc2	Přerov
František	František	k1gMnSc1	František
Venclovský	Venclovský	k2eAgMnSc1d1	Venclovský
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
otužilec	otužilec	k1gMnSc1	otužilec
a	a	k8xC	a
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
přemožitel	přemožitel	k1gMnSc1	přemožitel
kanálu	kanál	k1gInSc2	kanál
La	la	k1gNnSc2	la
Manche	Manche	k1gNnSc2	Manche
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Mazáč	Mazáč	k1gMnSc1	Mazáč
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
Miluše	Miluše	k1gFnSc2	Miluše
Dreiseitlová	Dreiseitlová	k1gFnSc1	Dreiseitlová
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
dabérka	dabérka	k1gFnSc1	dabérka
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Wykrent	Wykrent	k1gMnSc1	Wykrent
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Pavel	Pavel	k1gMnSc1	Pavel
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jiří	Jiří	k1gMnSc1	Jiří
Čapka	Čapka	k1gMnSc1	Čapka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
Milena	Milena	k1gFnSc1	Milena
Vicenová	Vicenový	k2eAgFnSc1d1	Vicenová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
stálá	stálý	k2eAgFnSc1d1	stálá
představitelka	představitelka	k1gFnSc1	představitelka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
při	při	k7c6	při
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Navrátil	Navrátil	k1gMnSc1	Navrátil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
tenisový	tenisový	k2eAgMnSc1d1	tenisový
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
tenista	tenista	k1gMnSc1	tenista
Karel	Karel	k1gMnSc1	Karel
Plíhal	Plíhal	k1gMnSc1	Plíhal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
Miloš	Miloš	k1gMnSc1	Miloš
Říha	Říha	k1gMnSc1	Říha
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
hokejista	hokejista	k1gMnSc1	hokejista
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
český	český	k2eAgMnSc1d1	český
trenér	trenér	k1gMnSc1	trenér
Pavel	Pavel	k1gMnSc1	Pavel
Vrba	Vrba	k1gMnSc1	Vrba
(	(	kIx(	(
<g/>
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
trenér	trenér	k1gMnSc1	trenér
Lubomír	Lubomír	k1gMnSc1	Lubomír
Dostál	Dostál	k1gMnSc1	Dostál
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
výchovy	výchova	k1gFnSc2	výchova
Ivana	Ivana	k1gFnSc1	Ivana
Nováková	Nováková	k1gFnSc1	Nováková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
<g/>
,	,	kIx,	,
účastnice	účastnice	k1gFnSc1	účastnice
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1988	[number]	k4	1988
Milada	Milada	k1gFnSc1	Milada
Spalová	Spalová	k1gFnSc1	Spalová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
volejbalová	volejbalový	k2eAgFnSc1d1	volejbalová
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Zlámal	Zlámal	k1gMnSc1	Zlámal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g />
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
olympionik	olympionik	k1gMnSc1	olympionik
Kateřina	Kateřina	k1gFnSc1	Kateřina
Sokolová	Sokolová	k1gFnSc1	Sokolová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kundrátek	Kundrátek	k1gInSc1	Kundrátek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
KHL	KHL	kA	KHL
za	za	k7c7	za
HC	HC	kA	HC
Slovan	Slovan	k1gInSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g />
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Zaťovič	Zaťovič	k1gMnSc1	Zaťovič
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
v	v	k7c6	v
ELH	ELH	kA	ELH
za	za	k7c2	za
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
Působili	působit	k5eAaImAgMnP	působit
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
Jakub	Jakub	k1gMnSc1	Jakub
Škoda	Škoda	k1gMnSc1	Škoda
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
–	–	k?	–
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
komunální	komunální	k2eAgMnSc1d1	komunální
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Bardejov	Bardejov	k1gInSc1	Bardejov
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Cuijk	Cuijka	k1gFnPc2	Cuijka
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Ivano-Frankivsk	Ivano-Frankivsk	k1gInSc1	Ivano-Frankivsk
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Kędzierzyn-Koźle	Kędzierzyn-Koźle	k1gFnSc1	Kędzierzyn-Koźle
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Kotor	Kotor	k1gInSc1	Kotor
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
Ozimek	ozimek	k1gInSc1	ozimek
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
