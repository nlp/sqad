<p>
<s>
Kredit	kredit	k1gInSc1	kredit
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
kreditní	kreditní	k2eAgInSc4d1	kreditní
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
náročnosti	náročnost	k1gFnSc2	náročnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
a	a	k8xC	a
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
objem	objem	k1gInSc4	objem
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
student	student	k1gMnSc1	student
průměrně	průměrně	k6eAd1	průměrně
vynaložit	vynaložit	k5eAaPmF	vynaložit
na	na	k7c4	na
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
zvládnutí	zvládnutí	k1gNnSc4	zvládnutí
kurzu	kurz	k1gInSc2	kurz
<g/>
,	,	kIx,	,
modulu	modul	k1gInSc2	modul
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
části	část	k1gFnSc2	část
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kreditní	kreditní	k2eAgInSc1d1	kreditní
systém	systém	k1gInSc1	systém
studia	studio	k1gNnSc2	studio
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přesně	přesně	k6eAd1	přesně
předepisovalo	předepisovat	k5eAaImAgNnS	předepisovat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
kurzy	kurz	k1gInPc1	kurz
(	(	kIx(	(
<g/>
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
semináře	seminář	k1gInPc1	seminář
<g/>
,	,	kIx,	,
cvičení	cvičení	k1gNnSc1	cvičení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
student	student	k1gMnSc1	student
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
absolvovat	absolvovat	k5eAaPmF	absolvovat
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
jim	on	k3xPp3gMnPc3	on
kreditový	kreditový	k2eAgInSc1d1	kreditový
systém	systém	k1gInSc1	systém
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgFnSc4d2	veliký
volnost	volnost	k1gFnSc4	volnost
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ale	ale	k9	ale
nespoléhá	spoléhat	k5eNaImIp3nS	spoléhat
úplně	úplně	k6eAd1	úplně
jen	jen	k9	jen
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
píli	píle	k1gFnSc4	píle
studenta	student	k1gMnSc2	student
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
měří	měřit	k5eAaImIp3nS	měřit
objem	objem	k1gInSc1	objem
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
rostoucí	rostoucí	k2eAgFnSc6d1	rostoucí
složitosti	složitost	k1gFnSc6	složitost
a	a	k8xC	a
specializaci	specializace	k1gFnSc6	specializace
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
nedá	dát	k5eNaPmIp3nS	dát
celý	celý	k2eAgInSc4d1	celý
obor	obor	k1gInSc4	obor
rozumně	rozumně	k6eAd1	rozumně
pokrýt	pokrýt	k5eAaPmF	pokrýt
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
hloubce	hloubka	k1gFnSc6	hloubka
a	a	k8xC	a
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
specializovat	specializovat	k5eAaBmF	specializovat
už	už	k6eAd1	už
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
větších	veliký	k2eAgFnPc6d2	veliký
univerzitách	univerzita	k1gFnPc6	univerzita
si	se	k3xPyFc3	se
studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
volit	volit	k5eAaImF	volit
z	z	k7c2	z
několika	několik	k4yIc2	několik
podobných	podobný	k2eAgInPc2d1	podobný
kurzů	kurz	k1gInPc2	kurz
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
podání	podání	k1gNnSc1	podání
a	a	k8xC	a
zaměření	zaměření	k1gNnSc1	zaměření
jim	on	k3xPp3gMnPc3	on
lépe	dobře	k6eAd2	dobře
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
povinných	povinný	k2eAgInPc2d1	povinný
kurzů	kurz	k1gInPc2	kurz
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
tzv.	tzv.	kA	tzv.
Áčkové	Áčkové	k2eAgInPc7d1	Áčkové
předměty	předmět	k1gInPc7	předmět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
povinně	povinně	k6eAd1	povinně
volitelných	volitelný	k2eAgInPc2d1	volitelný
kurzů	kurz	k1gInPc2	kurz
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
tzv.	tzv.	kA	tzv.
Béčkové	béčkový	k2eAgInPc1d1	béčkový
předměty	předmět	k1gInPc1	předmět
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
volitelných	volitelný	k2eAgInPc2d1	volitelný
kurzů	kurz	k1gInPc2	kurz
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
tzv.	tzv.	kA	tzv.
Céčkové	Céčkové	k2eAgInPc7d1	Céčkové
předměty	předmět	k1gInPc7	předmět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
povinný	povinný	k2eAgInSc4d1	povinný
objem	objem	k1gInSc4	objem
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
musí	muset	k5eAaImIp3nS	muset
student	student	k1gMnSc1	student
vynaložit	vynaložit	k5eAaPmF	vynaložit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
právě	právě	k9	právě
kredity	kredit	k1gInPc4	kredit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
ECTS	ECTS	kA	ECTS
==	==	k?	==
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
mobilita	mobilita	k1gFnSc1	mobilita
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
přenášet	přenášet	k5eAaImF	přenášet
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
přístupy	přístup	k1gInPc4	přístup
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kvality	kvalita	k1gFnSc2	kvalita
studia	studio	k1gNnSc2	studio
i	i	k9	i
širšího	široký	k2eAgInSc2d2	širší
rozhledu	rozhled	k1gInSc2	rozhled
studentů	student	k1gMnPc2	student
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
běžná	běžný	k2eAgFnSc1d1	běžná
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
prestižní	prestižní	k2eAgFnPc4d1	prestižní
univerzity	univerzita	k1gFnPc4	univerzita
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
i	i	k9	i
v	v	k7c6	v
současných	současný	k2eAgFnPc6d1	současná
podmínkách	podmínka	k1gFnPc6	podmínka
mohl	moct	k5eAaImAgMnS	moct
student	student	k1gMnSc1	student
část	část	k1gFnSc4	část
studia	studio	k1gNnSc2	studio
absolvovat	absolvovat	k5eAaPmF	absolvovat
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
zavést	zavést	k5eAaPmF	zavést
srovnatelné	srovnatelný	k2eAgNnSc4d1	srovnatelné
měření	měření	k1gNnSc4	měření
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc4	hodnocení
studentova	studentův	k2eAgInSc2d1	studentův
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boloňská	boloňský	k2eAgFnSc1d1	Boloňská
deklarace	deklarace	k1gFnSc1	deklarace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
připojilo	připojit	k5eAaPmAgNnS	připojit
47	[number]	k4	47
evropských	evropský	k2eAgFnPc2d1	Evropská
i	i	k8xC	i
mimoevropských	mimoevropský	k2eAgFnPc2d1	mimoevropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
doporučila	doporučit	k5eAaPmAgFnS	doporučit
-	-	kIx~	-
vedle	vedle	k7c2	vedle
děleného	dělený	k2eAgNnSc2d1	dělené
studia	studio	k1gNnSc2	studio
-	-	kIx~	-
mezinárodně	mezinárodně	k6eAd1	mezinárodně
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
kreditní	kreditní	k2eAgInSc1d1	kreditní
systém	systém	k1gInSc1	systém
ECTS	ECTS	kA	ECTS
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
European	European	k1gInSc4	European
Credit	Credit	k1gFnSc3	Credit
Transfer	transfer	k1gInSc1	transfer
and	and	k?	and
Accumulation	Accumulation	k1gInSc1	Accumulation
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadovaný	požadovaný	k2eAgInSc1d1	požadovaný
objem	objem	k1gInSc1	objem
práce	práce	k1gFnSc2	práce
studenta	student	k1gMnSc2	student
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1500	[number]	k4	1500
až	až	k9	až
1800	[number]	k4	1800
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
kreditním	kreditní	k2eAgInPc3d1	kreditní
bodům	bod	k1gInPc3	bod
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeden	jeden	k4xCgInSc1	jeden
kreditní	kreditní	k2eAgInSc1d1	kreditní
bod	bod	k1gInSc1	bod
představuje	představovat	k5eAaImIp3nS	představovat
studijní	studijní	k2eAgFnSc4d1	studijní
zátěž	zátěž	k1gFnSc4	zátěž
zhruba	zhruba	k6eAd1	zhruba
25-30	[number]	k4	25-30
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Studijní	studijní	k2eAgFnSc1d1	studijní
zátěž	zátěž	k1gFnSc1	zátěž
přitom	přitom	k6eAd1	přitom
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k8xS	jak
hodiny	hodina	k1gFnPc1	hodina
strávené	strávený	k2eAgFnPc1d1	strávená
v	v	k7c6	v
posluchárně	posluchárna	k1gFnSc6	posluchárna
nebo	nebo	k8xC	nebo
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vlastní	vlastní	k2eAgNnSc4d1	vlastní
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
nebo	nebo	k8xC	nebo
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kurz	kurz	k1gInSc1	kurz
úspěšně	úspěšně	k6eAd1	úspěšně
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
příslušný	příslušný	k2eAgInSc4d1	příslušný
počet	počet	k1gInSc4	počet
kreditních	kreditní	k2eAgInPc2d1	kreditní
bodů	bod	k1gInPc2	bod
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
dostal	dostat	k5eAaPmAgMnS	dostat
známku	známka	k1gFnSc4	známka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
přiřazení	přiřazení	k1gNnSc1	přiřazení
kreditů	kredit	k1gInPc2	kredit
kurzům	kurz	k1gInPc3	kurz
<g/>
,	,	kIx,	,
modulům	modul	k1gInPc3	modul
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
částem	část	k1gFnPc3	část
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pravomoci	pravomoc	k1gFnSc6	pravomoc
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
ECTS	ECTS	kA	ECTS
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vůli	vůle	k1gFnSc6	vůle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
signatářských	signatářský	k2eAgFnPc2d1	signatářská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
ani	ani	k8xC	ani
většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
ECTS	ECTS	kA	ECTS
nenařizuje	nařizovat	k5eNaImIp3nS	nařizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobré	dobrý	k2eAgFnPc1d1	dobrá
vysoké	vysoká	k1gFnPc1	vysoká
školy	škola	k1gFnSc2	škola
samy	sám	k3xTgFnPc1	sám
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
zavádět	zavádět	k5eAaImF	zavádět
a	a	k8xC	a
získávat	získávat	k5eAaImF	získávat
tak	tak	k9	tak
osvědčení	osvědčení	k1gNnSc4	osvědčení
"	"	kIx"	"
<g/>
ECTS	ECTS	kA	ECTS
label	label	k1gInSc1	label
<g/>
"	"	kIx"	"
právě	právě	k6eAd1	právě
kvůli	kvůli	k7c3	kvůli
podpoře	podpora	k1gFnSc3	podpora
mobility	mobilita	k1gFnSc2	mobilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
účastnické	účastnický	k2eAgInPc1d1	účastnický
státy	stát	k1gInPc1	stát
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
vytváření	vytváření	k1gNnSc6	vytváření
"	"	kIx"	"
<g/>
Evropského	evropský	k2eAgInSc2d1	evropský
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
prostoru	prostor	k1gInSc2	prostor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Higher	Highra	k1gFnPc2	Highra
Education	Education	k1gInSc1	Education
Area	area	k1gFnSc1	area
<g/>
,	,	kIx,	,
EHEA	EHEA	kA	EHEA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasifikace	klasifikace	k1gFnSc2	klasifikace
===	===	k?	===
</s>
</p>
<p>
<s>
ECTS	ECTS	kA	ECTS
měří	měřit	k5eAaImIp3nS	měřit
pracovní	pracovní	k2eAgFnSc4d1	pracovní
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
,	,	kIx,	,
neklasifikuje	klasifikovat	k5eNaImIp3nS	klasifikovat
ale	ale	k8xC	ale
její	její	k3xOp3gInPc4	její
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
klasifikační	klasifikační	k2eAgInSc1d1	klasifikační
systém	systém	k1gInSc1	systém
ECTS	ECTS	kA	ECTS
doporučoval	doporučovat	k5eAaImAgInS	doporučovat
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
výsledky	výsledek	k1gInPc4	výsledek
studentů	student	k1gMnPc2	student
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
stupních	stupeň	k1gInPc6	stupeň
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
nejběžnějšímu	běžný	k2eAgMnSc3d3	Nejběžnější
známkování	známkování	k1gNnPc2	známkování
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
definují	definovat	k5eAaBmIp3nP	definovat
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
slovně	slovně	k6eAd1	slovně
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc1	kolik
procent	procento	k1gNnPc2	procento
studentů	student	k1gMnPc2	student
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
klasifikaci	klasifikace	k1gFnSc6	klasifikace
v	v	k7c6	v
systému	systém	k1gInSc6	systém
ECTS	ECTS	kA	ECTS
<g/>
.	.	kIx.	.
</s>
<s>
Příručka	příručka	k1gFnSc1	příručka
ECTS	ECTS	kA	ECTS
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zohlednit	zohlednit	k5eAaPmF	zohlednit
velmi	velmi	k6eAd1	velmi
odlišné	odlišný	k2eAgFnPc4d1	odlišná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
i	i	k8xC	i
akademické	akademický	k2eAgFnPc4d1	akademická
tradice	tradice	k1gFnPc4	tradice
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
současně	současně	k6eAd1	současně
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgFnPc4d1	možná
známky	známka	k1gFnPc4	známka
získané	získaný	k2eAgFnSc2d1	získaná
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
akademických	akademický	k2eAgFnPc6d1	akademická
institucích	instituce	k1gFnPc6	instituce
vzájemně	vzájemně	k6eAd1	vzájemně
a	a	k8xC	a
transparentně	transparentně	k6eAd1	transparentně
porovnat	porovnat	k5eAaPmF	porovnat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
každá	každý	k3xTgFnSc1	každý
instituce	instituce	k1gFnSc1	instituce
zapojená	zapojený	k2eAgFnSc1d1	zapojená
do	do	k7c2	do
systému	systém	k1gInSc2	systém
ECTS	ECTS	kA	ECTS
používá	používat	k5eAaImIp3nS	používat
svůj	svůj	k3xOyFgInSc4	svůj
lokální	lokální	k2eAgInSc4d1	lokální
klasifikační	klasifikační	k2eAgInSc4d1	klasifikační
systém	systém	k1gInSc4	systém
a	a	k8xC	a
jako	jako	k9	jako
doplňující	doplňující	k2eAgFnSc4d1	doplňující
informaci	informace	k1gFnSc4	informace
ke	k	k7c3	k
němu	on	k3xPp3gInSc3	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
statistickém	statistický	k2eAgNnSc6d1	statistické
rozdělení	rozdělení	k1gNnSc6	rozdělení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
klasifikačních	klasifikační	k2eAgInPc2d1	klasifikační
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc4	takový
tabulku	tabulka	k1gFnSc4	tabulka
rovněž	rovněž	k9	rovněž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Dodatek	dodatek	k1gInSc1	dodatek
k	k	k7c3	k
diplomu	diplom	k1gInSc3	diplom
(	(	kIx(	(
<g/>
Diploma	Diploma	k1gFnSc1	Diploma
Supplement	Supplement	k1gMnSc1	Supplement
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
zpravidla	zpravidla	k6eAd1	zpravidla
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Zápočty	zápočet	k1gInPc1	zápočet
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
hodnotit	hodnotit	k5eAaImF	hodnotit
jako	jako	k8xS	jako
Splnil	splnit	k5eAaPmAgMnS	splnit
<g/>
,	,	kIx,	,
či	či	k8xC	či
Nesplnil	splnit	k5eNaPmAgMnS	splnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
ECTS	ECTS	kA	ECTS
(	(	kIx(	(
<g/>
Evropského	evropský	k2eAgInSc2d1	evropský
systému	systém	k1gInSc2	systém
přenosu	přenos	k1gInSc2	přenos
a	a	k8xC	a
akumulace	akumulace	k1gFnSc1	akumulace
kreditů	kredit	k1gInPc2	kredit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
DZS	DZS	kA	DZS
MŠMT	MŠMT	kA	MŠMT
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
European	European	k1gInSc1	European
credit	credit	k5eAaImF	credit
transfer	transfer	k1gInSc4	transfer
system	systo	k1gNnSc7	systo
<g/>
:	:	kIx,	:
a	a	k8xC	a
guide	guide	k6eAd1	guide
for	forum	k1gNnPc2	forum
visiting	visiting	k1gInSc4	visiting
students	studentsa	k1gFnPc2	studentsa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Matfyzpres	Matfyzpres	k1gInSc1	Matfyzpres
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
ECTS	ECTS	kA	ECTS
Guide	Guid	k1gInSc5	Guid
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
EU	EU	kA	EU
</s>
</p>
<p>
<s>
ECVET	ECVET	kA	ECVET
Kreditní	kreditní	k2eAgInSc4d1	kreditní
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
profesní	profesní	k2eAgNnSc4d1	profesní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
EHEA	EHEA	kA	EHEA
</s>
</p>
