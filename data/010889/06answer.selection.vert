<s>
Boloňská	boloňský	k2eAgFnSc1d1	Boloňská
deklarace	deklarace	k1gFnSc1	deklarace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
připojilo	připojit	k5eAaPmAgNnS	připojit
47	[number]	k4	47
evropských	evropský	k2eAgFnPc2d1	Evropská
i	i	k8xC	i
mimoevropských	mimoevropský	k2eAgFnPc2d1	mimoevropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
doporučila	doporučit	k5eAaPmAgFnS	doporučit
-	-	kIx~	-
vedle	vedle	k7c2	vedle
děleného	dělený	k2eAgNnSc2d1	dělené
studia	studio	k1gNnSc2	studio
-	-	kIx~	-
mezinárodně	mezinárodně	k6eAd1	mezinárodně
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
kreditní	kreditní	k2eAgInSc1d1	kreditní
systém	systém	k1gInSc1	systém
ECTS	ECTS	kA	ECTS
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
European	European	k1gInSc4	European
Credit	Credit	k1gFnSc3	Credit
Transfer	transfer	k1gInSc1	transfer
and	and	k?	and
Accumulation	Accumulation	k1gInSc1	Accumulation
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadovaný	požadovaný	k2eAgInSc1d1	požadovaný
objem	objem	k1gInSc1	objem
práce	práce	k1gFnSc2	práce
studenta	student	k1gMnSc2	student
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1500	[number]	k4	1500
až	až	k9	až
1800	[number]	k4	1800
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
kreditním	kreditní	k2eAgInPc3d1	kreditní
bodům	bod	k1gInPc3	bod
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeden	jeden	k4xCgInSc1	jeden
kreditní	kreditní	k2eAgInSc1d1	kreditní
bod	bod	k1gInSc1	bod
představuje	představovat	k5eAaImIp3nS	představovat
studijní	studijní	k2eAgFnSc4d1	studijní
zátěž	zátěž	k1gFnSc4	zátěž
zhruba	zhruba	k6eAd1	zhruba
25-30	[number]	k4	25-30
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
