<s>
Pandelela	Pandelet	k5eAaImAgFnS,k5eAaPmAgFnS
Rinongová	Rinongový	k2eAgFnSc1d1
</s>
<s>
Pandelela	Pandelet	k5eAaPmAgFnS,k5eAaImAgFnS
Rinongová	Rinongový	k2eAgFnSc1d1
Osobní	osobní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1993	#num#	k4
(	(	kIx(
<g/>
28	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Bau	Bau	k?
Sportovní	sportovní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Skoky	skok	k1gInPc1
do	do	k7c2
vody	voda	k1gFnSc2
na	na	k7c4
LOH	LOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
Londýn	Londýn	k1gInSc1
2012	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
m	m	kA
věž	věž	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
2016	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
m	m	kA
věž	věž	k1gFnSc1
synchro	synchra	k1gFnSc5
</s>
<s>
Pandelela	Pandelela	k1gFnSc1
Rinong	Rinong	k1gFnSc1
Anak	Anak	k1gFnSc1
Pamg	Pamg	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1993	#num#	k4
Bau	Bau	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malajsijská	malajsijský	k2eAgFnSc1d1
reprezentantka	reprezentantka	k1gFnSc1
ve	v	k7c6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Startovala	startovat	k5eAaBmAgFnS
na	na	k7c6
třech	tři	k4xCgFnPc6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
ve	v	k7c6
skocích	skok	k1gInPc6
z	z	k7c2
věže	věž	k1gFnSc2
27	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
získala	získat	k5eAaPmAgFnS
ve	v	k7c6
skocích	skok	k1gInPc6
z	z	k7c2
věže	věž	k1gFnSc2
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
skončila	skončit	k5eAaPmAgFnS
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
v	v	k7c6
soutěži	soutěž	k1gFnSc6
v	v	k7c6
synchronizovaném	synchronizovaný	k2eAgInSc6d1
skoku	skok	k1gInSc6
spolu	spolu	k6eAd1
s	s	k7c7
Cheong	Cheong	k1gFnSc1
Jun	jun	k1gFnSc1
Hoong	Hoong	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byla	být	k5eAaImAgFnS
vlajkonoškou	vlajkonoška	k1gFnSc7
malajsijské	malajsijský	k2eAgFnSc2d1
olympijské	olympijský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
první	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vybojovala	vybojovat	k5eAaPmAgFnS
pro	pro	k7c4
Malajsii	Malajsie	k1gFnSc4
olympijskou	olympijský	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
také	také	k9
první	první	k4xOgFnSc1
medaile	medaile	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
tato	tento	k3xDgFnSc1
země	země	k1gFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
sportu	sport	k1gInSc6
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
badminton	badminton	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
plavání	plavání	k1gNnSc6
získala	získat	k5eAaPmAgFnS
čtyři	čtyři	k4xCgFnPc4
bronzové	bronzový	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
a	a	k8xC
2017	#num#	k4
v	v	k7c6
synchronizovaných	synchronizovaný	k2eAgInPc6d1
skocích	skok	k1gInPc6
z	z	k7c2
věže	věž	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
v	v	k7c6
individuálních	individuální	k2eAgInPc6d1
skocích	skok	k1gInPc6
z	z	k7c2
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
zlatou	zlatý	k2eAgFnSc7d1
medailistkou	medailistka	k1gFnSc7
z	z	k7c2
Her	hra	k1gFnPc2
Commonwealthu	Commonwealth	k1gInSc2
2010	#num#	k4
a	a	k8xC
osminásobnou	osminásobný	k2eAgFnSc7d1
vítězkou	vítězka	k1gFnSc7
Her	hra	k1gFnPc2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Univerziádě	univerziáda	k1gFnSc6
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
stříbrnou	stříbrný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
v	v	k7c6
individuální	individuální	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
a	a	k8xC
bronzovou	bronzový	k2eAgFnSc4d1
v	v	k7c6
synchronizovaných	synchronizovaný	k2eAgInPc6d1
skocích	skok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
příslušnicí	příslušnice	k1gFnSc7
národa	národ	k1gInSc2
Bidajuhů	Bidajuh	k1gMnPc2
žijícího	žijící	k2eAgInSc2d1
ve	v	k7c6
státě	stát	k1gInSc6
Sarawak	Sarawak	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
studuje	studovat	k5eAaImIp3nS
sportovní	sportovní	k2eAgInSc1d1
management	management	k1gInSc1
na	na	k7c6
Malajské	malajský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c4
Kuala	Kual	k1gMnSc4
Lumpuru	Lumpur	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
držitelkou	držitelka	k1gFnSc7
vyznamenání	vyznamenání	k1gNnSc2
Johan	Johan	k1gMnSc1
Bintang	Bintang	k1gMnSc1
Kenyalang	Kenyalang	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
a	a	k8xC
2015	#num#	k4
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
sportovkyní	sportovkyně	k1gFnPc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.sport.cz/loh/vodni-sporty/clanek/799993-i-treti-zlato-ve-skocich-do-vody-na-oh-patri-cine.html	https://www.sport.cz/loh/vodni-sporty/clanek/799993-i-treti-zlato-ve-skocich-do-vody-na-oh-patri-cine.html	k1gInSc1
<g/>
↑	↑	k?
http://www.theborneopost.com/2012/08/10/blazing-the-trail/	http://www.theborneopost.com/2012/08/10/blazing-the-trail/	k4
<g/>
↑	↑	k?
http://www.freemalaysiatoday.com/category/nation/2016/08/10/pandelela-jun-hoong-victory-brings-cheer-to-bidayuh-folk/	http://www.freemalaysiatoday.com/category/nation/2016/08/10/pandelela-jun-hoong-victory-brings-cheer-to-bidayuh-folk/	k4
<g/>
↑	↑	k?
https://www.thestar.com.my/sport/other-sport/2016/05/25/al-jumeri-pandelela-national-sportsman-sportswoman-awards/	https://www.thestar.com.my/sport/other-sport/2016/05/25/al-jumeri-pandelela-national-sportsman-sportswoman-awards/	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.fina.org/athletes/pandelela-rinong	http://www.fina.org/athletes/pandelela-rinong	k1gMnSc1
</s>
<s>
Pandelela	Pandelet	k5eAaImAgFnS,k5eAaPmAgFnS
Rinongová	Rinongový	k2eAgFnSc1d1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
