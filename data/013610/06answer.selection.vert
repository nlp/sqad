<s desamb="1">
Startovala	startovat	k5eAaBmAgFnS
na	na	k7c6
třech	tři	k4xCgFnPc6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
ve	v	k7c6
skocích	skok	k1gInPc6
z	z	k7c2
věže	věž	k1gFnSc2
27	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
získala	získat	k5eAaPmAgFnS
ve	v	k7c6
skocích	skok	k1gInPc6
z	z	k7c2
věže	věž	k1gFnSc2
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
skončila	skončit	k5eAaPmAgFnS
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
v	v	k7c6
soutěži	soutěž	k1gFnSc6
v	v	k7c6
synchronizovaném	synchronizovaný	k2eAgInSc6d1
skoku	skok	k1gInSc6
spolu	spolu	k6eAd1
s	s	k7c7
Cheong	Cheong	k1gFnSc1
Jun	jun	k1gFnSc1
Hoong	Hoong	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byla	být	k5eAaImAgFnS
vlajkonoškou	vlajkonoška	k1gFnSc7
malajsijské	malajsijský	k2eAgFnSc2d1
olympijské	olympijský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>