<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Brebera	Breber	k1gMnSc2	Breber
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Přelouč	Přelouč	k1gFnSc1	Přelouč
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
chemik	chemik	k1gMnSc1	chemik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
státním	státní	k2eAgInSc6d1	státní
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
při	při	k7c6	při
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Explosii	Explosie	k1gFnSc6	Explosie
Pardubice	Pardubice	k1gInPc4	Pardubice
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Radimem	Radim	k1gMnSc7	Radim
Fukátkem	Fukátek	k1gInSc7	Fukátek
plastickou	plastický	k2eAgFnSc4d1	plastická
trhavinu	trhavina	k1gFnSc4	trhavina
Semtex	semtex	k1gInSc1	semtex
<g/>
.	.	kIx.	.
</s>
<s>
Brebera	Breber	k1gMnSc4	Breber
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
studoval	studovat	k5eAaImAgInS	studovat
chemii	chemie	k1gFnSc4	chemie
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
nadřízení	nadřízený	k1gMnPc1	nadřízený
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
jeho	jeho	k3xOp3gInSc3	jeho
talentu	talent	k1gInSc3	talent
a	a	k8xC	a
poslali	poslat	k5eAaPmAgMnP	poslat
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
vojenského	vojenský	k2eAgInSc2d1	vojenský
technického	technický	k2eAgInSc2d1	technický
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
chemičku	chemička	k1gFnSc4	chemička
Synthesia	Synthesium	k1gNnSc2	Synthesium
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgInSc1d2	pozdější
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
VCHZ	VCHZ	kA	VCHZ
Synthesia	Synthesius	k1gMnSc2	Synthesius
<g/>
.	.	kIx.	.
</s>
