<s>
Jod	jod	k1gInSc1	jod
(	(	kIx(	(
<g/>
též	též	k9	též
jód	jód	k1gInSc1	jód
<g/>
;	;	kIx,	;
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ι	ι	k?	ι
<g/>
,	,	kIx,	,
iódés	iódés	k1gInSc1	iódés
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
fialový	fialový	k2eAgMnSc1d1	fialový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
I	i	k9	i
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Iodum	Iodum	k1gInSc1	Iodum
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
halogenů	halogen	k1gInPc2	halogen
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
tmavě	tmavě	k6eAd1	tmavě
fialové	fialový	k2eAgInPc1d1	fialový
destičkovité	destičkovitý	k2eAgInPc1d1	destičkovitý
krystalky	krystalek	k1gInPc1	krystalek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důležitý	důležitý	k2eAgInSc1d1	důležitý
biogenní	biogenní	k2eAgInSc1d1	biogenní
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jod	jod	k1gInSc1	jod
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
francouzským	francouzský	k2eAgMnSc7d1	francouzský
chemikem	chemik	k1gMnSc7	chemik
Bernardem	Bernard	k1gMnSc7	Bernard
Courtoisem	Courtois	k1gInSc7	Courtois
<g/>
.	.	kIx.	.
</s>
<s>
Pochybný	pochybný	k2eAgMnSc1d1	pochybný
je	být	k5eAaImIp3nS	být
uváděný	uváděný	k2eAgInSc1d1	uváděný
přírodní	přírodní	k2eAgInSc1d1	přírodní
výskyt	výskyt	k1gInSc1	výskyt
elementárního	elementární	k2eAgInSc2d1	elementární
jodu	jod	k1gInSc2	jod
z	z	k7c2	z
italských	italský	k2eAgFnPc2d1	italská
sopek	sopka	k1gFnPc2	sopka
jako	jako	k8xS	jako
minerálu	minerál	k1gInSc2	minerál
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Elementární	elementární	k2eAgInSc1d1	elementární
jod	jod	k1gInSc1	jod
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
fialová	fialový	k2eAgFnSc1d1	fialová
až	až	k8xS	až
černá	černý	k2eAgFnSc1d1	černá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
přechází	přecházet	k5eAaImIp3nS	přecházet
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
plynné	plynný	k2eAgFnSc2d1	plynná
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
páry	pár	k1gInPc1	pár
mají	mít	k5eAaImIp3nP	mít
fialovou	fialový	k2eAgFnSc4d1	fialová
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
dráždivý	dráždivý	k2eAgInSc4d1	dráždivý
zápach	zápach	k1gInSc4	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
nebo	nebo	k8xC	nebo
nepolárních	polární	k2eNgNnPc6d1	nepolární
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
jako	jako	k8xS	jako
sirouhlík	sirouhlík	k1gInSc4	sirouhlík
CS	CS	kA	CS
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
tetrachlormethan	tetrachlormethan	k1gInSc1	tetrachlormethan
CCl	CCl	k1gFnSc1	CCl
<g/>
4	[number]	k4	4
nebo	nebo	k8xC	nebo
benzen	benzen	k1gInSc1	benzen
C6H6	C6H6	k1gFnSc2	C6H6
Je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
jodidu	jodid	k1gInSc2	jodid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yIgInSc7	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
trijodidový	trijodidový	k2eAgInSc1d1	trijodidový
anion	anion	k1gInSc1	anion
<g/>
,	,	kIx,	,
tohoto	tento	k3xDgInSc2	tento
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
jodometrických	jodometrický	k2eAgFnPc6d1	jodometrický
titracích	titrace	k1gFnPc6	titrace
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
jód	jód	k1gInSc1	jód
přítomen	přítomen	k2eAgInSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přítomen	přítomen	k2eAgMnSc1d1	přítomen
nejen	nejen	k6eAd1	nejen
jako	jako	k8xC	jako
jodid	jodid	k1gInSc4	jodid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jodičnanu	jodičnan	k1gInSc2	jodičnan
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jodu	jod	k1gInSc2	jod
analogické	analogický	k2eAgFnPc1d1	analogická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
bromu	brom	k1gInSc2	brom
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgNnSc1d1	relativní
zastoupení	zastoupení	k1gNnSc1	zastoupení
jodu	jod	k1gInSc2	jod
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
jod	jod	k1gInSc1	jod
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,1	[number]	k4	0,1
až	až	k9	až
0,5	[number]	k4	0,5
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
většina	většina	k1gFnSc1	většina
jodu	jod	k1gInSc2	jod
přítomného	přítomný	k2eAgInSc2d1	přítomný
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jeho	jeho	k3xOp3gNnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hodnoty	hodnota	k1gFnSc2	hodnota
0,06	[number]	k4	0,06
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
na	na	k7c4	na
1	[number]	k4	1
atom	atom	k1gInSc4	atom
jódu	jód	k1gInSc2	jód
připadá	připadat	k5eAaPmIp3nS	připadat
70	[number]	k4	70
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
jodu	jod	k1gInSc2	jod
jsou	být	k5eAaImIp3nP	být
mořské	mořský	k2eAgFnPc1d1	mořská
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnPc6	jejichž
pletivech	pletivo	k1gNnPc6	pletivo
se	se	k3xPyFc4	se
jod	jod	k1gInSc1	jod
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
<g/>
.	.	kIx.	.
</s>
<s>
Oxidací	oxidace	k1gFnPc2	oxidace
jodidů	jodid	k1gInPc2	jodid
<g/>
,	,	kIx,	,
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
popelu	popel	k1gInSc6	popel
ze	z	k7c2	z
spálených	spálený	k2eAgFnPc2d1	spálená
řas	řasa	k1gFnPc2	řasa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
elementární	elementární	k2eAgInSc1d1	elementární
jod	jod	k1gInSc1	jod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rafinuje	rafinovat	k5eAaImIp3nS	rafinovat
sublimací	sublimace	k1gFnSc7	sublimace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přeměnou	přeměna	k1gFnSc7	přeměna
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
pevného	pevný	k2eAgMnSc2d1	pevný
do	do	k7c2	do
plynného	plynný	k2eAgNnSc2d1	plynné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
výskyt	výskyt	k1gInSc1	výskyt
jodu	jod	k1gInSc2	jod
jako	jako	k8xC	jako
minerálu	minerál	k1gInSc2	minerál
je	být	k5eAaImIp3nS	být
pochybný	pochybný	k2eAgInSc1d1	pochybný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uváděn	uváděn	k2eAgMnSc1d1	uváděn
bez	bez	k7c2	bez
dalších	další	k2eAgFnPc2d1	další
podrobnějších	podrobný	k2eAgFnPc2d2	podrobnější
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
Vesuvu	Vesuv	k1gInSc2	Vesuv
a	a	k8xC	a
ostrova	ostrov	k1gInSc2	ostrov
Vulcano	Vulcana	k1gFnSc5	Vulcana
(	(	kIx(	(
<g/>
Liparské	Liparský	k2eAgInPc1d1	Liparský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
jodu	jod	k1gInSc2	jod
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
alkohol-voda	alkoholod	k1gMnSc2	alkohol-vod
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
jodová	jodový	k2eAgFnSc1d1	jodová
tinktura	tinktura	k1gFnSc1	tinktura
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k8xC	jako
dezinfekční	dezinfekční	k2eAgNnSc1d1	dezinfekční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jod	jod	k1gInSc1	jod
a	a	k8xC	a
jodid	jodid	k1gInSc1	jodid
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
podobně	podobně	k6eAd1	podobně
účinkujícího	účinkující	k2eAgNnSc2d1	účinkující
Lugolova	Lugolův	k2eAgNnSc2d1	Lugolův
činidla	činidlo	k1gNnSc2	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
jako	jako	k9	jako
antiseptikum	antiseptikum	k1gNnSc1	antiseptikum
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
šetrnější	šetrný	k2eAgMnSc1d2	šetrnější
a	a	k8xC	a
stabilnější	stabilní	k2eAgMnSc1d2	stabilnější
jodovaný	jodovaný	k2eAgMnSc1d1	jodovaný
povidon	povidon	k1gMnSc1	povidon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc4	komplex
jodu	jod	k1gInSc2	jod
a	a	k8xC	a
polyvinylpyrrolidonu	polyvinylpyrrolidon	k1gInSc2	polyvinylpyrrolidon
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
škrobem	škrob	k1gInSc7	škrob
stvoří	stvořit	k5eAaPmIp3nS	stvořit
intenzivně	intenzivně	k6eAd1	intenzivně
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
modrý	modrý	k2eAgInSc1d1	modrý
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
důkaz	důkaz	k1gInSc4	důkaz
jodu	jod	k1gInSc2	jod
nebo	nebo	k8xC	nebo
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
jako	jako	k9	jako
indikace	indikace	k1gFnSc1	indikace
bodu	bod	k1gInSc2	bod
ekvivalence	ekvivalence	k1gFnSc2	ekvivalence
při	při	k7c6	při
jodometrických	jodometrický	k2eAgFnPc6d1	jodometrický
titracích	titrace	k1gFnPc6	titrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgNnSc1d1	základní
reakcí	reakce	k1gFnSc7	reakce
oxidace	oxidace	k1gFnSc2	oxidace
thiosíranu	thiosíran	k1gInSc2	thiosíran
sodného	sodný	k2eAgInSc2d1	sodný
roztokem	roztok	k1gInSc7	roztok
elementárního	elementární	k2eAgInSc2d1	elementární
jódu	jód	k1gInSc2	jód
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
jod	jod	k1gInSc1	jod
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
I	I	kA	I
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
a	a	k8xC	a
I	i	k9	i
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
jodid	jodid	k1gInSc1	jodid
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
<g/>
,	,	kIx,	,
AgI	AgI	k1gFnPc4	AgI
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
využití	využití	k1gNnSc1	využití
ve	v	k7c6	v
fotografickém	fotografický	k2eAgInSc6d1	fotografický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
tvoří	tvořit	k5eAaImIp3nS	tvořit
jod	jod	k1gInSc1	jod
velmi	velmi	k6eAd1	velmi
nestálou	stálý	k2eNgFnSc4d1	nestálá
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
,	,	kIx,	,
jododusík	jododusík	k1gInSc4	jododusík
<g/>
,	,	kIx,	,
NI	on	k3xPp3gFnSc4	on
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
stavu	stav	k1gInSc6	stav
explozivně	explozivně	k6eAd1	explozivně
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
i	i	k9	i
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
slabém	slabý	k2eAgInSc6d1	slabý
podnětu	podnět	k1gInSc6	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
spadá	spadat	k5eAaImIp3nS	spadat
spíše	spíše	k9	spíše
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
chemických	chemický	k2eAgInPc2d1	chemický
"	"	kIx"	"
<g/>
žertů	žert	k1gInPc2	žert
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jod	jod	k1gInSc1	jod
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
prvky	prvek	k1gInPc4	prvek
nezbytné	nezbytný	k2eAgInPc4d1	nezbytný
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
lidského	lidský	k2eAgInSc2d1	lidský
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
hormonů	hormon	k1gInPc2	hormon
vylučovaných	vylučovaný	k2eAgInPc2d1	vylučovaný
štítnou	štítný	k2eAgFnSc7d1	štítná
žlázou	žláza	k1gFnSc7	žláza
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
je	být	k5eAaImIp3nS	být
thyroxin	thyroxin	k1gInSc4	thyroxin
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
strukturní	strukturní	k2eAgInPc4d1	strukturní
vzorec	vzorec	k1gInSc4	vzorec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
hormonů	hormon	k1gInPc2	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
především	především	k9	především
vývoj	vývoj	k1gInSc1	vývoj
pohybové	pohybový	k2eAgFnSc2d1	pohybová
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
mozku	mozek	k1gInSc2	mozek
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
fázích	fáze	k1gFnPc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
jejich	jejich	k3xOp3gInSc4	jejich
nedostatek	nedostatek	k1gInSc4	nedostatek
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
inteligenci	inteligence	k1gFnSc4	inteligence
(	(	kIx(	(
<g/>
kretenismus	kretenismus	k1gInSc4	kretenismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navenek	navenek	k6eAd1	navenek
se	se	k3xPyFc4	se
nedostatek	nedostatek	k1gInSc1	nedostatek
jodu	jod	k1gInSc2	jod
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
vole	vole	k1gNnSc1	vole
neboli	neboli	k8xC	neboli
struma	struma	k1gFnSc1	struma
<g/>
.	.	kIx.	.
</s>
<s>
Jod	jod	k1gInSc1	jod
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
v	v	k7c6	v
krevním	krevní	k2eAgInSc6d1	krevní
oběhu	oběh	k1gInSc6	oběh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
extrémně	extrémně	k6eAd1	extrémně
kolísá	kolísat	k5eAaImIp3nS	kolísat
podle	podle	k7c2	podle
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
metodou	metoda	k1gFnSc7	metoda
saturace	saturace	k1gFnSc2	saturace
jodem	jod	k1gInSc7	jod
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
jodu	jod	k1gInSc2	jod
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
to	ten	k3xDgNnSc1	ten
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
přijatého	přijatý	k2eAgInSc2d1	přijatý
jodu	jod	k1gInSc2	jod
se	se	k3xPyFc4	se
z	z	k7c2	z
organizmu	organizmus	k1gInSc2	organizmus
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
močí	moč	k1gFnSc7	moč
(	(	kIx(	(
<g/>
zbytek	zbytek	k1gInSc1	zbytek
v	v	k7c6	v
potu	pot	k1gInSc6	pot
<g/>
,	,	kIx,	,
dechu	dech	k1gInSc6	dech
<g/>
,	,	kIx,	,
stolici	stolice	k1gFnSc6	stolice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
jodu	jod	k1gInSc2	jod
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
(	(	kIx(	(
<g/>
jodurie	jodurie	k1gFnSc1	jodurie
<g/>
)	)	kIx)	)
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ranním	ranní	k2eAgInSc6d1	ranní
vzorku	vzorek	k1gInSc6	vzorek
moči	moč	k1gFnSc2	moč
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
doporučovaná	doporučovaný	k2eAgFnSc1d1	doporučovaná
pro	pro	k7c4	pro
epidemiologický	epidemiologický	k2eAgInSc4d1	epidemiologický
průzkum	průzkum	k1gInSc4	průzkum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jod	jod	k1gInSc1	jod
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mořské	mořský	k2eAgFnPc1d1	mořská
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
plody	plod	k1gInPc1	plod
moře	moře	k1gNnSc2	moře
jeho	jeho	k3xOp3gInSc7	jeho
dobrým	dobrý	k2eAgInSc7d1	dobrý
zdrojem	zdroj	k1gInSc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vegany	vegan	k1gMnPc4	vegan
jsou	být	k5eAaImIp3nP	být
dobrými	dobrý	k2eAgInPc7d1	dobrý
zdroji	zdroj	k1gInPc7	zdroj
jodizovaná	jodizovaný	k2eAgFnSc1d1	jodizovaná
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
u	u	k7c2	u
té	ten	k3xDgFnSc2	ten
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
zjistit	zjistit	k5eAaPmF	zjistit
na	na	k7c6	na
jaké	jaký	k3yIgFnSc6	jaký
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
jodizovaná	jodizovaný	k2eAgFnSc1d1	jodizovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnPc1d1	mořská
řasy	řasa	k1gFnPc1	řasa
a	a	k8xC	a
minerální	minerální	k2eAgInPc1d1	minerální
doplňky	doplněk	k1gInPc1	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Jod	jod	k1gInSc1	jod
je	být	k5eAaImIp3nS	být
také	také	k9	také
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
koncentraci	koncentrace	k1gFnSc6	koncentrace
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
uměle	uměle	k6eAd1	uměle
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
jogurty	jogurt	k1gInPc4	jogurt
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgInPc4d1	mléčný
nápoje	nápoj	k1gInPc4	nápoj
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgInPc4d1	mléčný
krémy	krém	k1gInPc4	krém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
konzumovány	konzumován	k2eAgFnPc1d1	konzumována
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
miligramová	miligramový	k2eAgNnPc4d1	miligramový
množství	množství	k1gNnSc4	množství
jodistanu	jodistan	k1gInSc2	jodistan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
přísun	přísun	k1gInSc4	přísun
potřebného	potřebný	k2eAgNnSc2d1	potřebné
množství	množství	k1gNnSc2	množství
jodu	jod	k1gInSc2	jod
pro	pro	k7c4	pro
dospívající	dospívající	k2eAgInSc4d1	dospívající
organizmus	organizmus	k1gInSc4	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Jodidové	Jodidový	k2eAgFnPc1d1	Jodidový
tablety	tableta	k1gFnPc1	tableta
dokáží	dokázat	k5eAaPmIp3nP	dokázat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kritického	kritický	k2eAgInSc2d1	kritický
stupně	stupeň	k1gInSc2	stupeň
radiace	radiace	k1gFnSc2	radiace
zabránit	zabránit	k5eAaPmF	zabránit
poškození	poškození	k1gNnSc4	poškození
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
vážné	vážný	k2eAgInPc4d1	vážný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Primárně	primárně	k6eAd1	primárně
se	se	k3xPyFc4	se
jod	jod	k1gInSc1	jod
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
řasách	řasa	k1gFnPc6	řasa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
množství	množství	k1gNnSc1	množství
velice	velice	k6eAd1	velice
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
(	(	kIx(	(
<g/>
Nori	Nore	k1gFnSc4	Nore
řasa	řasa	k1gFnSc1	řasa
16	[number]	k4	16
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
Wakame	Wakam	k1gInSc5	Wakam
42	[number]	k4	42
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
Kombu	Komba	k1gFnSc4	Komba
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
Kelp	Kelp	k1gInSc1	Kelp
2353	[number]	k4	2353
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
jódu	jód	k1gInSc2	jód
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
řasách	řasa	k1gFnPc6	řasa
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rozpustné	rozpustný	k2eAgFnSc6d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dobře	dobře	k6eAd1	dobře
využitelné	využitelný	k2eAgNnSc1d1	využitelné
lidským	lidský	k2eAgInSc7d1	lidský
organismem	organismus	k1gInSc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
dávka	dávka	k1gFnSc1	dávka
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
je	být	k5eAaImIp3nS	být
150	[number]	k4	150
μ	μ	k?	μ
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
těhotné	těhotná	k1gFnPc4	těhotná
ženy	žena	k1gFnSc2	žena
220	[number]	k4	220
μ	μ	k?	μ
a	a	k8xC	a
pro	pro	k7c4	pro
kojící	kojící	k2eAgFnPc4d1	kojící
ženy	žena	k1gFnPc4	žena
290	[number]	k4	290
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
jodu	jod	k1gInSc2	jod
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tmavěoranžová	tmavěoranžový	k2eAgFnSc1d1	tmavěoranžový
jodová	jodový	k2eAgFnSc1d1	jodová
tinktura	tinktura	k1gFnSc1	tinktura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kápnutí	kápnutí	k1gNnSc6	kápnutí
tinktury	tinktura	k1gFnSc2	tinktura
na	na	k7c4	na
škrob	škrob	k1gInSc4	škrob
(	(	kIx(	(
<g/>
stačí	stačit	k5eAaBmIp3nS	stačit
rozemletá	rozemletý	k2eAgFnSc1d1	rozemletá
brambora	brambora	k1gFnSc1	brambora
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
škrob	škrob	k1gInSc1	škrob
obarvit	obarvit	k5eAaPmF	obarvit
do	do	k7c2	do
modro-fialových	modroialův	k2eAgInPc2d1	modro-fialův
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Dnem	den	k1gInSc7	den
jodu	jod	k1gInSc2	jod
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc1	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jod	jod	k1gInSc1	jod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jód	jód	k1gInSc1	jód
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
