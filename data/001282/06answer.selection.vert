<s>
Jod	jod	k1gInSc1	jod
(	(	kIx(	(
<g/>
též	též	k9	též
jód	jód	k1gInSc1	jód
<g/>
;	;	kIx,	;
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ι	ι	k?	ι
<g/>
,	,	kIx,	,
iódés	iódés	k1gInSc1	iódés
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
fialový	fialový	k2eAgMnSc1d1	fialový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
I	i	k9	i
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Iodum	Iodum	k1gInSc1	Iodum
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
halogenů	halogen	k1gInPc2	halogen
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
tmavě	tmavě	k6eAd1	tmavě
fialové	fialový	k2eAgInPc1d1	fialový
destičkovité	destičkovitý	k2eAgInPc1d1	destičkovitý
krystalky	krystalek	k1gInPc1	krystalek
<g/>
.	.	kIx.	.
</s>
