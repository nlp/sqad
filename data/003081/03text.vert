<s>
Limita	limita	k1gFnSc1	limita
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
konstrukce	konstrukce	k1gFnSc1	konstrukce
vyjadřující	vyjadřující	k2eAgFnSc1d1	vyjadřující
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hodnoty	hodnota	k1gFnPc1	hodnota
zadané	zadaný	k2eAgFnSc2d1	zadaná
posloupnosti	posloupnost	k1gFnSc2	posloupnost
nebo	nebo	k8xC	nebo
funkce	funkce	k1gFnSc2	funkce
blíží	blížit	k5eAaImIp3nS	blížit
libovolně	libovolně	k6eAd1	libovolně
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
bodu	bod	k1gInSc3	bod
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
limita	limita	k1gFnSc1	limita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
u	u	k7c2	u
funkcí	funkce	k1gFnPc2	funkce
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
z	z	k7c2	z
→	→	k?	→
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
u	u	k7c2	u
posloupností	posloupnost	k1gFnPc2	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
případně	případně	k6eAd1	případně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
posloupnosti	posloupnost	k1gFnSc6	posloupnost
nebo	nebo	k8xC	nebo
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
limitě	limita	k1gFnSc6	limita
posloupnosti	posloupnost	k1gFnSc6	posloupnost
nebo	nebo	k8xC	nebo
limitě	limita	k1gFnSc6	limita
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
limity	limita	k1gFnSc2	limita
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
na	na	k7c6	na
reálných	reálný	k2eAgInPc6d1	reálný
číslech	číslo	k1gNnPc6	číslo
<g/>
,	,	kIx,	,
obecnější	obecní	k2eAgFnSc1d2	obecní
definice	definice	k1gFnSc1	definice
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
na	na	k7c6	na
libovolném	libovolný	k2eAgInSc6d1	libovolný
metrickém	metrický	k2eAgInSc6d1	metrický
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
obecnější	obecní	k2eAgFnSc1d2	obecní
definice	definice	k1gFnSc1	definice
na	na	k7c6	na
libovolném	libovolný	k2eAgInSc6d1	libovolný
topologickém	topologický	k2eAgInSc6d1	topologický
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
více	hodně	k6eAd2	hodně
definic	definice	k1gFnPc2	definice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
definice	definice	k1gFnPc1	definice
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
metrickým	metrický	k2eAgInSc7d1	metrický
i	i	k8xC	i
topologickým	topologický	k2eAgInSc7d1	topologický
prostorem	prostor	k1gInSc7	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Limita	limita	k1gFnSc1	limita
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}}	}}	k?	}}
má	mít	k5eAaImIp3nS	mít
limitu	limita	k1gFnSc4	limita
A	a	k9	a
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jejími	její	k3xOp3gFnPc7	její
hodnotami	hodnota	k1gFnPc7	hodnota
můžeme	moct	k5eAaImIp1nP	moct
k	k	k7c3	k
A	A	kA	A
libovolně	libovolně	k6eAd1	libovolně
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
kladné	kladný	k2eAgNnSc4d1	kladné
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
nějaký	nějaký	k3yIgInSc1	nějaký
člen	člen	k1gInSc1	člen
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
jsou	být	k5eAaImIp3nP	být
už	už	k9	už
její	její	k3xOp3gFnPc1	její
hodnoty	hodnota	k1gFnPc1	hodnota
od	od	k7c2	od
A	A	kA	A
vzdáleny	vzdálit	k5eAaPmNgFnP	vzdálit
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Zapsáno	zapsán	k2eAgNnSc1d1	zapsáno
symbolicky	symbolicky	k6eAd1	symbolicky
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
∃	∃	k?	∃
n	n	k0	n
∈	∈	k?	∈
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
k	k	k7c3	k
≥	≥	k?	≥
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
A	a	k9	a
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
<g />
.	.	kIx.	.
</s>
<s hack="1">
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
\	\	kIx~	\
<g/>
exists	exists	k6eAd1	exists
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
:	:	kIx,	:
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
n	n	k0	n
<g/>
:	:	kIx,	:
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
-A	-A	k?	-A
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
limitou	limita	k1gFnSc7	limita
posloupnosti	posloupnost	k1gFnSc2	posloupnost
(	(	kIx(	(
<g/>
0,9	[number]	k4	0,9
<g/>
;	;	kIx,	;
0,99	[number]	k4	0,99
<g/>
;	;	kIx,	;
0,999	[number]	k4	0,999
<g/>
;	;	kIx,	;
0,9999	[number]	k4	0,9999
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
formálně	formálně	k6eAd1	formálně
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
j.	j.	k?	j.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
limita	limita	k1gFnSc1	limita
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
a	a	k8xC	a
limitu	limit	k1gInSc2	limit
A	A	kA	A
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
k	k	k7c3	k
libovolnému	libovolný	k2eAgMnSc3d1	libovolný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc6	epsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
existuje	existovat	k5eAaImIp3nS	existovat
takové	takový	k3xDgNnSc4	takový
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
>	>	kIx)	>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
x	x	k?	x
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
}	}	kIx)	}
-okolí	kolí	k1gNnSc1	-okolí
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vyjmeme	vyjmout	k5eAaPmIp1nP	vyjmout
bod	bod	k1gInSc1	bod
a	a	k8xC	a
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
<g />
.	.	kIx.	.
</s>
<s hack="1">
prstencová	prstencový	k2eAgNnPc4d1	prstencové
okolí	okolí	k1gNnSc4	okolí
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
-	-	kIx~	-
A	a	k9	a
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
-A	-A	k?	-A
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnSc1	epsilon
}	}	kIx)	}
.	.	kIx.	.
(	(	kIx(	(
<g/>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
<g/>
:	:	kIx,	:
Pravostranná	pravostranný	k2eAgFnSc1d1	pravostranná
a	a	k8xC	a
levostranná	levostranný	k2eAgFnSc1d1	levostranná
limita	limita	k1gFnSc1	limita
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
(	(	kIx(	(
<g/>
libovolně	libovolně	k6eAd1	libovolně
velké	velký	k2eAgNnSc1d1	velké
<g/>
)	)	kIx)	)
kladné	kladný	k2eAgNnSc1d1	kladné
číslo	číslo	k1gNnSc1	číslo
y	y	k?	y
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
prvek	prvek	k1gInSc4	prvek
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
kterým	který	k3yIgInSc7	který
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
hodnoty	hodnota	k1gFnPc4	hodnota
posloupnosti	posloupnost	k1gFnSc2	posloupnost
větší	veliký	k2eAgFnSc6d2	veliký
než	než	k8xS	než
y	y	k?	y
<g/>
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
posloupnost	posloupnost	k1gFnSc1	posloupnost
roste	růst	k5eAaImIp3nS	růst
nade	nad	k7c4	nad
všechny	všechen	k3xTgFnPc4	všechen
meze	mez	k1gFnPc4	mez
neboli	neboli	k8xC	neboli
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
limitu	limita	k1gFnSc4	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
limita	limita	k1gFnSc1	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
(	(	kIx(	(
<g/>
libovolně	libovolně	k6eAd1	libovolně
velké	velký	k2eAgNnSc1d1	velké
<g/>
)	)	kIx)	)
kladné	kladný	k2eAgNnSc1d1	kladné
číslo	číslo	k1gNnSc1	číslo
y	y	k?	y
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
okolí	okolí	k1gNnSc4	okolí
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
má	mít	k5eAaImIp3nS	mít
funkce	funkce	k1gFnSc1	funkce
hodnotu	hodnota	k1gFnSc4	hodnota
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
y	y	k?	y
<g/>
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
nade	nad	k7c4	nad
všechny	všechen	k3xTgFnPc4	všechen
meze	mez	k1gFnPc4	mez
neboli	neboli	k8xC	neboli
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
limitu	limita	k1gFnSc4	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
limita	limita	k1gFnSc1	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
obdobně	obdobně	k6eAd1	obdobně
<g/>
.	.	kIx.	.
</s>
<s>
Limitou	limita	k1gFnSc7	limita
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nejen	nejen	k6eAd1	nejen
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
posloupností	posloupnost	k1gFnPc2	posloupnost
lze	lze	k6eAd1	lze
zkoumat	zkoumat	k5eAaImF	zkoumat
chování	chování	k1gNnSc4	chování
funkcí	funkce	k1gFnPc2	funkce
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
hodnoty	hodnota	k1gFnPc4	hodnota
argumentu	argument	k1gInSc2	argument
větší	veliký	k2eAgInPc4d2	veliký
než	než	k8xS	než
zadané	zadaný	k2eAgNnSc4d1	zadané
kladné	kladný	k2eAgNnSc4d1	kladné
číslo	číslo	k1gNnSc4	číslo
z.	z.	k?	z.
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hodnoty	hodnota	k1gFnPc1	hodnota
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
určitého	určitý	k2eAgNnSc2d1	určité
čísla	číslo	k1gNnSc2	číslo
A	a	k9	a
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
předem	předem	k6eAd1	předem
zadané	zadaný	k2eAgNnSc4d1	zadané
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc6	epsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
nevlastním	vlastní	k2eNgInSc6d1	nevlastní
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
vlastní	vlastní	k2eAgFnSc4d1	vlastní
limitu	limita	k1gFnSc4	limita
A.	A.	kA	A.
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc4	hodnota
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
libovolné	libovolný	k2eAgFnPc4d1	libovolná
předem	předem	k6eAd1	předem
dané	daný	k2eAgFnPc4d1	daná
y	y	k?	y
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
nevlastním	vlastní	k2eNgInSc6d1	nevlastní
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
limitu	limita	k1gFnSc4	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
limitu	limita	k1gFnSc4	limita
v	v	k7c6	v
nevlastním	vlastní	k2eNgInSc6d1	nevlastní
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
nevlastních	vlastní	k2eNgInPc2d1	nevlastní
bodů	bod	k1gInPc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
funkce	funkce	k1gFnPc4	funkce
vlastní	vlastní	k2eAgFnSc4d1	vlastní
limitu	limita	k1gFnSc4	limita
<g/>
,	,	kIx,	,
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
limitu	limita	k1gFnSc4	limita
nebo	nebo	k8xC	nebo
limita	limita	k1gFnSc1	limita
nemusí	muset	k5eNaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
limitu	limita	k1gFnSc4	limita
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
sinus	sinus	k1gInSc1	sinus
<g/>
.	.	kIx.	.
</s>
<s>
Limita	limita	k1gFnSc1	limita
zobrazení	zobrazení	k1gNnSc2	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
A	a	k9	a
→	→	k?	→
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
B	B	kA	B
<g/>
}	}	kIx)	}
mezi	mezi	k7c7	mezi
topologickými	topologický	k2eAgInPc7d1	topologický
prostory	prostor	k1gInPc7	prostor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
a	a	k8xC	a
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
∈	∈	k?	∈
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
}	}	kIx)	}
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
okolí	okolí	k1gNnSc4	okolí
O	o	k7c4	o
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
bodu	bod	k1gInSc2	bod
b	b	k?	b
existuje	existovat	k5eAaImIp3nS	existovat
okolí	okolí	k1gNnSc4	okolí
O	o	k7c4	o
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
bodu	bůst	k5eAaImIp1nS	bůst
a	a	k8xC	a
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
O	O	kA	O
(	(	kIx(	(
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
O	o	k7c4	o
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
implikuje	implikovat	k5eAaImIp3nS	implikovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
∈	∈	k?	∈
O	o	k7c4	o
(	(	kIx(	(
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
O	o	k7c4	o
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
zobecněním	zobecnění	k1gNnSc7	zobecnění
limity	limita	k1gFnSc2	limita
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc2	funkce
i	i	k8xC	i
zobrazení	zobrazení	k1gNnSc2	zobrazení
jsou	být	k5eAaImIp3nP	být
limity	limit	k1gInPc1	limit
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Limita	limita	k1gFnSc1	limita
zobrazení	zobrazení	k1gNnSc2	zobrazení
nebo	nebo	k8xC	nebo
sítě	síť	k1gFnSc2	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
topologickém	topologický	k2eAgInSc6d1	topologický
prostoru	prostor	k1gInSc6	prostor
víceznačná	víceznačný	k2eAgFnSc1d1	víceznačná
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Hausdorffově	Hausdorffův	k2eAgInSc6d1	Hausdorffův
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
limita	limita	k1gFnSc1	limita
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
každá	každý	k3xTgFnSc1	každý
síť	síť	k1gFnSc4	síť
má	mít	k5eAaImIp3nS	mít
nejvýše	nejvýše	k6eAd1	nejvýše
jednu	jeden	k4xCgFnSc4	jeden
limitu	limita	k1gFnSc4	limita
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
nule	nula	k1gFnSc6	nula
definovaná	definovaný	k2eAgFnSc1d1	definovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
limitu	limita	k1gFnSc4	limita
1	[number]	k4	1
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgFnSc1d1	vlastní
limita	limita	k1gFnSc1	limita
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
bodě	bod	k1gInSc6	bod
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
limitu	limit	k1gInSc2	limit
0	[number]	k4	0
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgFnSc1d1	vlastní
limita	limita	k1gFnSc1	limita
v	v	k7c6	v
nevlastním	vlastní	k2eNgInSc6d1	nevlastní
bodě	bod	k1gInSc6	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nule	nula	k1gFnSc6	nula
spojitá	spojitý	k2eAgFnSc1d1	spojitá
(	(	kIx(	(
<g/>
limita	limita	k1gFnSc1	limita
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
limitu	limit	k1gInSc2	limit
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
tvrzení	tvrzení	k1gNnPc4	tvrzení
platí	platit	k5eAaImIp3nP	platit
i	i	k9	i
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
⋅	⋅	k?	⋅
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
ani	ani	k8xC	ani
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
}}	}}	k?	}}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
v	v	k7c6	v
nule	nula	k1gFnSc6	nula
limitu	limit	k1gInSc2	limit
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
funkcích	funkce	k1gFnPc6	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
či	či	k8xC	či
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
alespoň	alespoň	k9	alespoň
jednostranné	jednostranný	k2eAgInPc1d1	jednostranný
limity	limit	k1gInPc1	limit
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gFnSc1	jejich
pravostranná	pravostranný	k2eAgFnSc1d1	pravostranná
limita	limita	k1gFnSc1	limita
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
a	a	k8xC	a
levostranná	levostranný	k2eAgFnSc1d1	levostranná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
nule	nula	k1gFnSc6	nula
limitu	limit	k1gInSc2	limit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
limita	limita	k1gFnSc1	limita
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
bodě	bod	k1gInSc6	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
limitu	limit	k1gInSc2	limit
0	[number]	k4	0
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgFnSc1d1	vlastní
limita	limita	k1gFnSc1	limita
v	v	k7c6	v
nevlastním	vlastní	k2eNgInSc6d1	nevlastní
bodě	bod	k1gInSc6	bod
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
limitu	limit	k1gInSc2	limit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Hospitalovo	Hospitalův	k2eAgNnSc1d1	Hospitalovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
Vlastní	vlastnit	k5eAaImIp3nS	vlastnit
limita	limita	k1gFnSc1	limita
Nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
limita	limita	k1gFnSc1	limita
Spojitá	spojitý	k2eAgFnSc1d1	spojitá
funkce	funkce	k1gFnSc2	funkce
Konvergence	konvergence	k1gFnSc2	konvergence
Divergence	divergence	k1gFnSc2	divergence
</s>
