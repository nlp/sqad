<s>
Pojem	pojem	k1gInSc1	pojem
limity	limita	k1gFnSc2	limita
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
na	na	k7c6	na
reálných	reálný	k2eAgInPc6d1	reálný
číslech	číslo	k1gNnPc6	číslo
<g/>
,	,	kIx,	,
obecnější	obecní	k2eAgFnSc1d2	obecní
definice	definice	k1gFnSc1	definice
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
na	na	k7c6	na
libovolném	libovolný	k2eAgInSc6d1	libovolný
metrickém	metrický	k2eAgInSc6d1	metrický
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
obecnější	obecní	k2eAgFnSc1d2	obecní
definice	definice	k1gFnSc1	definice
na	na	k7c6	na
libovolném	libovolný	k2eAgInSc6d1	libovolný
topologickém	topologický	k2eAgInSc6d1	topologický
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
