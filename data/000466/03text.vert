<s>
Olympias	Olympias	k1gInSc1	Olympias
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ο	Ο	k?	Ο
<g/>
,	,	kIx,	,
376	[number]	k4	376
<g/>
-	-	kIx~	-
<g/>
316	[number]	k4	316
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
épeirského	épeirský	k2eAgInSc2d1	épeirský
kmene	kmen	k1gInSc2	kmen
Molossů	Moloss	k1gInPc2	Moloss
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
Makedonského	makedonský	k2eAgNnSc2d1	makedonské
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Polyxena	Polyxena	k1gFnSc1	Polyxena
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
dcera	dcera	k1gFnSc1	dcera
épeirského	épeirský	k2eAgMnSc2d1	épeirský
krále	král	k1gMnSc2	král
Neoptolema	Neoptolem	k1gMnSc2	Neoptolem
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Aiakovců	Aiakovec	k1gMnPc2	Aiakovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
pocházel	pocházet	k5eAaImAgMnS	pocházet
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k8xC	i
mytický	mytický	k2eAgMnSc1d1	mytický
hrdina	hrdina	k1gMnSc1	hrdina
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
Achilleus	Achilleus	k1gMnSc1	Achilleus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc3	její
bratr	bratr	k1gMnSc1	bratr
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
Épeirský	Épeirský	k2eAgMnSc1d1	Épeirský
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Arymbás	Arymbása	k1gFnPc2	Arymbása
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
s	s	k7c7	s
makedonským	makedonský	k2eAgMnSc7d1	makedonský
králem	král	k1gMnSc7	král
Filipem	Filip	k1gMnSc7	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c6	na
spojenectví	spojenectví	k1gNnSc6	spojenectví
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
ujednání	ujednání	k1gNnSc4	ujednání
stvrdili	stvrdit	k5eAaPmAgMnP	stvrdit
zaslíbením	zaslíbení	k1gNnSc7	zaslíbení
Olympiady	Olympiada	k1gFnSc2	Olympiada
Filipovi	Filip	k1gMnSc3	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
ji	on	k3xPp3gFnSc4	on
poznal	poznat	k5eAaPmAgMnS	poznat
na	na	k7c6	na
řeckém	řecký	k2eAgInSc6d1	řecký
ostrově	ostrov	k1gInSc6	ostrov
Samothraké	Samothraký	k2eAgFnPc1d1	Samothraký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
společně	společně	k6eAd1	společně
zasvěcováni	zasvěcován	k2eAgMnPc1d1	zasvěcován
do	do	k7c2	do
mystérií	mystérium	k1gNnPc2	mystérium
boha	bůh	k1gMnSc2	bůh
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
359	[number]	k4	359
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ji	on	k3xPp3gFnSc4	on
pojal	pojmout	k5eAaPmAgInS	pojmout
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Olympias	Olympiasa	k1gFnPc2	Olympiasa
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
jeho	jeho	k3xOp3gFnSc7	jeho
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
makedonské	makedonský	k2eAgFnSc2d1	makedonská
šlechty	šlechta	k1gFnSc2	šlechta
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
a	a	k8xC	a
povahu	povaha	k1gFnSc4	povaha
velmi	velmi	k6eAd1	velmi
neoblíbená	oblíbený	k2eNgFnSc1d1	neoblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Épeiru	Épeir	k1gInSc2	Épeir
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
našla	najít	k5eAaPmAgFnS	najít
milence	milenec	k1gMnSc4	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
strýce	strýc	k1gMnSc2	strýc
a	a	k8xC	a
krále	král	k1gMnSc2	král
Épeiru	Épeira	k1gMnSc4	Épeira
Arymba	Arymb	k1gMnSc4	Arymb
se	se	k3xPyFc4	se
ale	ale	k9	ale
opět	opět	k6eAd1	opět
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Makedonie	Makedonie	k1gFnSc2	Makedonie
Pelly	Pella	k1gFnSc2	Pella
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
356	[number]	k4	356
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Alexandra	Alexandr	k1gMnSc4	Alexandr
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
dceru	dcera	k1gFnSc4	dcera
Kleopatru	Kleopatra	k1gFnSc4	Kleopatra
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Filipem	Filip	k1gMnSc7	Filip
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc1d1	šťastné
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stala	stát	k5eAaPmAgFnS	stát
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
řada	řada	k1gFnSc1	řada
roztržek	roztržka	k1gFnPc2	roztržka
a	a	k8xC	a
střetů	střet	k1gInPc2	střet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vývoji	vývoj	k1gInSc3	vývoj
přispěl	přispět	k5eAaPmAgMnS	přispět
i	i	k9	i
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Eurydiké	Eurydiký	k2eAgNnSc1d1	Eurydiký
Kleopatrou	Kleopatra	k1gFnSc7	Kleopatra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nejpřednějších	přední	k2eAgMnPc2d3	nejpřednější
makedonských	makedonský	k2eAgMnPc2d1	makedonský
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
novou	nový	k2eAgFnSc7d1	nová
makedonskou	makedonský	k2eAgFnSc7d1	makedonská
královnou	královna	k1gFnSc7	královna
a	a	k8xC	a
Olympias	Olympias	k1gMnSc1	Olympias
bude	být	k5eAaImBp3nS	být
zapuzena	zapuzen	k2eAgFnSc1d1	zapuzena
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Eurydiké	Eurydiký	k2eAgFnSc3d1	Eurydiký
Kleopatře	Kleopatra	k1gFnSc3	Kleopatra
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
nástupnická	nástupnický	k2eAgFnSc1d1	nástupnická
práva	práv	k2eAgFnSc1d1	práva
Alexandra	Alexandra	k1gFnSc1	Alexandra
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Olympias	Olympias	k1gInSc1	Olympias
se	se	k3xPyFc4	se
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
opět	opět	k6eAd1	opět
uchýlila	uchýlit	k5eAaPmAgNnP	uchýlit
do	do	k7c2	do
Épeiru	Épeir	k1gInSc2	Épeir
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
stal	stát	k5eAaPmAgInS	stát
králem	král	k1gMnSc7	král
její	její	k3xOp3gMnPc4	její
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
se	se	k3xPyFc4	se
Filipovi	Filip	k1gMnSc6	Filip
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
Podněcovala	podněcovat	k5eAaImAgFnS	podněcovat
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
ale	ale	k8xC	ale
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
králi	král	k1gMnSc3	král
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Kleopatru	Kleopatra	k1gFnSc4	Kleopatra
<g/>
.	.	kIx.	.
</s>
<s>
Olympiadin	Olympiadin	k2eAgMnSc1d1	Olympiadin
bratr	bratr	k1gMnSc1	bratr
si	se	k3xPyFc3	se
neteř	neteř	k1gFnSc1	neteř
ochotně	ochotně	k6eAd1	ochotně
vzal	vzít	k5eAaPmAgInS	vzít
a	a	k8xC	a
od	od	k7c2	od
pomsty	pomsta	k1gFnSc2	pomsta
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
zaručovalo	zaručovat	k5eAaImAgNnS	zaručovat
přímé	přímý	k2eAgNnSc1d1	přímé
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
makedonským	makedonský	k2eAgInSc7d1	makedonský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Olympias	Olympias	k1gInSc4	Olympias
byla	být	k5eAaImAgFnS	být
zapletená	zapletený	k2eAgFnSc1d1	zapletená
do	do	k7c2	do
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Filipovy	Filipův	k2eAgFnSc2d1	Filipova
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
totiž	totiž	k9	totiž
vrahovi	vrah	k1gMnSc3	vrah
postavit	postavit	k5eAaPmF	postavit
pomník	pomník	k1gInSc4	pomník
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
žádost	žádost	k1gFnSc4	žádost
byl	být	k5eAaImAgInS	být
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
ubodán	ubodat	k5eAaPmNgMnS	ubodat
<g/>
,	,	kIx,	,
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
v	v	k7c6	v
Apollónově	Apollónův	k2eAgInSc6d1	Apollónův
chrámu	chrám	k1gInSc6	chrám
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zabila	zabít	k5eAaPmAgFnS	zabít
Filipova	Filipův	k2eAgFnSc1d1	Filipova
a	a	k8xC	a
Kleopatřina	Kleopatřin	k2eAgMnSc4d1	Kleopatřin
syna	syn	k1gMnSc4	syn
Karana	Karan	k1gMnSc4	Karan
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Europu	Europ	k1gInSc2	Europ
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
donutila	donutit	k5eAaPmAgFnS	donutit
Eurydiké	Eurydiká	k1gFnSc3	Eurydiká
Kleopatru	Kleopatra	k1gFnSc4	Kleopatra
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Épeiru	Épeir	k1gInSc2	Épeir
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
plná	plný	k2eAgFnSc1d1	plná
nenávisti	nenávist	k1gFnSc3	nenávist
a	a	k8xC	a
mstila	mstít	k5eAaImAgFnS	mstít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
úhlavním	úhlavní	k2eAgMnSc7d1	úhlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Kassandros	Kassandrosa	k1gFnPc2	Kassandrosa
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
makedonského	makedonský	k2eAgMnSc2d1	makedonský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Antipatra	Antipatra	k1gFnSc1	Antipatra
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Alexandra	Alexandr	k1gMnSc4	Alexandr
Velikého	veliký	k2eAgMnSc4d1	veliký
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Peršanům	Peršan	k1gMnPc3	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
Kassandros	Kassandrosa	k1gFnPc2	Kassandrosa
byl	být	k5eAaImAgInS	být
obviňován	obviňován	k2eAgInSc1d1	obviňován
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alexandra	Alexandr	k1gMnSc4	Alexandr
otrávil	otrávit	k5eAaPmAgMnS	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Olympias	Olympias	k1gInSc4	Olympias
postupně	postupně	k6eAd1	postupně
vyvražďovala	vyvražďovat	k5eAaImAgFnS	vyvražďovat
jeho	jeho	k3xOp3gMnSc4	jeho
spojence	spojenec	k1gMnSc4	spojenec
-	-	kIx~	-
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
jí	on	k3xPp3gFnSc7	on
padli	padnout	k5eAaImAgMnP	padnout
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Kassandrův	Kassandrův	k2eAgMnSc1d1	Kassandrův
bratr	bratr	k1gMnSc1	bratr
Níkánor	Níkánor	k1gMnSc1	Níkánor
a	a	k8xC	a
makedonský	makedonský	k2eAgInSc1d1	makedonský
královský	královský	k2eAgInSc1d1	královský
pár	pár	k1gInSc1	pár
Eurydiké	Eurydiká	k1gFnSc2	Eurydiká
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Arrhidaios	Arrhidaios	k1gMnSc1	Arrhidaios
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Olympias	Olympiasa	k1gFnPc2	Olympiasa
vrátila	vrátit	k5eAaPmAgFnS	vrátit
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Épeiru	Épeiro	k1gNnSc6	Épeiro
<g/>
,	,	kIx,	,
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
Pydna	Pydn	k1gInSc2	Pydn
<g/>
.	.	kIx.	.
</s>
<s>
Kassandros	Kassandrosa	k1gFnPc2	Kassandrosa
město	město	k1gNnSc4	město
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
a	a	k8xC	a
Olympias	Olympias	k1gInSc4	Olympias
musela	muset	k5eAaImAgFnS	muset
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Kassandros	Kassandrosa	k1gFnPc2	Kassandrosa
ji	on	k3xPp3gFnSc4	on
původně	původně	k6eAd1	původně
nechtěl	chtít	k5eNaImAgMnS	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
odplula	odplout	k5eAaPmAgFnS	odplout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tudíž	tudíž	k8xC	tudíž
bez	bez	k7c2	bez
slyšení	slyšení	k1gNnSc2	slyšení
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
vojenským	vojenský	k2eAgNnSc7d1	vojenské
shromážděním	shromáždění	k1gNnSc7	shromáždění
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
byla	být	k5eAaImAgFnS	být
vykonána	vykonat	k5eAaPmNgFnS	vykonat
vrženým	vržený	k2eAgInSc7d1	vržený
oštěpem	oštěp	k1gInSc7	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olympias	Olympiasa	k1gFnPc2	Olympiasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Olympias	Olympiasa	k1gFnPc2	Olympiasa
na	na	k7c6	na
serveru	server	k1gInSc6	server
Livius	Livius	k1gInSc1	Livius
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
