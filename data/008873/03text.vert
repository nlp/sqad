<p>
<s>
Růženín	růženín	k1gInSc1	růženín
je	být	k5eAaImIp3nS	být
průsvitná	průsvitný	k2eAgFnSc1d1	průsvitná
až	až	k8xS	až
průhledná	průhledný	k2eAgFnSc1d1	průhledná
odrůda	odrůda	k1gFnSc1	odrůda
křemene	křemen	k1gInSc2	křemen
s	s	k7c7	s
růžovou	růžový	k2eAgFnSc7d1	růžová
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
netvoří	tvořit	k5eNaImIp3nP	tvořit
krystaly	krystal	k1gInPc1	krystal
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
1	[number]	k4	1
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
masivních	masivní	k2eAgInPc2d1	masivní
agregátů	agregát	k1gInPc2	agregát
<g/>
.	.	kIx.	.
</s>
<s>
Růžové	růžový	k2eAgNnSc1d1	růžové
zbarvení	zbarvení	k1gNnSc1	zbarvení
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
stopy	stopa	k1gFnPc4	stopa
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
mléčný	mléčný	k2eAgInSc4d1	mléčný
vzhled	vzhled	k1gInSc4	vzhled
jehličkovité	jehličkovitý	k2eAgFnSc2d1	jehličkovitá
inkluze	inkluze	k1gFnSc2	inkluze
rutilu	rutil	k1gInSc2	rutil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Růžový	růžový	k2eAgMnSc1d1	růžový
nebo	nebo	k8xC	nebo
broskvově	broskvově	k6eAd1	broskvově
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
křemen	křemen	k1gInSc4	křemen
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
růženín	růženín	k1gInSc1	růženín
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
ozdobnou	ozdobný	k2eAgFnSc4d1	ozdobná
řezbu	řezba	k1gFnSc4	řezba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
přítomností	přítomnost	k1gFnSc7	přítomnost
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
na	na	k7c4	na
575	[number]	k4	575
°	°	k?	°
<g/>
C	C	kA	C
zbarvení	zbarvení	k1gNnSc1	zbarvení
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
,	,	kIx,	,
také	také	k9	také
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
zšedne	zšednout	k5eAaPmIp3nS	zšednout
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Krystaly	krystal	k1gInPc1	krystal
růženínu	růženín	k1gInSc2	růženín
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
;	;	kIx,	;
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
celistvých	celistvý	k2eAgInPc2d1	celistvý
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
řezány	řezán	k2eAgInPc4d1	řezán
nebo	nebo	k8xC	nebo
broušeny	broušen	k2eAgInPc4d1	broušen
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čočkovce	čočkovec	k1gInSc2	čočkovec
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
korále	korál	k1gMnPc1	korál
<g/>
.	.	kIx.	.
</s>
<s>
Průhledný	průhledný	k2eAgInSc1d1	průhledný
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
;	;	kIx,	;
růženín	růženín	k1gInSc1	růženín
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zakalený	zakalený	k2eAgMnSc1d1	zakalený
nebo	nebo	k8xC	nebo
popraskaný	popraskaný	k2eAgMnSc1d1	popraskaný
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřeniny	uzavřenina	k1gFnPc1	uzavřenina
rutilu	rutil	k1gInSc2	rutil
v	v	k7c6	v
růženínu	růženín	k1gInSc6	růženín
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
asterický	asterický	k2eAgInSc4d1	asterický
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kámen	kámen	k1gInSc1	kámen
vybroušen	vybroušen	k2eAgMnSc1d1	vybroušen
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čočkovce	čočkovec	k1gInSc2	čočkovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Růženín	růženín	k1gInSc1	růženín
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
pegmatitech	pegmatit	k1gInPc6	pegmatit
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
materiál	materiál	k1gInSc4	materiál
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
však	však	k9	však
produkuje	produkovat	k5eAaImIp3nS	produkovat
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
naleziště	naleziště	k1gNnPc1	naleziště
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Růženín	růženín	k1gInSc1	růženín
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
hypotéz	hypotéza	k1gFnPc2	hypotéza
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
šokovým	šokový	k2eAgInSc7d1	šokový
minerálem	minerál	k1gInSc7	minerál
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
minerálem	minerál	k1gInSc7	minerál
přeměněným	přeměněný	k2eAgInSc7d1	přeměněný
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
meteoritu	meteorit	k1gInSc2	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Růženín	růženín	k1gInSc1	růženín
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
protkán	protkat	k5eAaPmNgInS	protkat
bílými	bílý	k2eAgFnPc7d1	bílá
lamelami	lamela	k1gFnPc7	lamela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
nahloučení	nahloučení	k1gNnSc4	nahloučení
dutin	dutina	k1gFnPc2	dutina
po	po	k7c6	po
ultrazvukové	ultrazvukový	k2eAgFnSc6d1	ultrazvuková
kavitaci	kavitace	k1gFnSc6	kavitace
<g/>
.	.	kIx.	.
</s>
<s>
Dané	daný	k2eAgInPc4d1	daný
růženíny	růženín	k1gInPc4	růženín
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
masivu	masiv	k1gInSc6	masiv
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gInSc3	jehož
vzniku	vznik	k1gInSc3	vznik
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
impaktní	impaktní	k2eAgFnSc1d1	impaktní
hypotéza	hypotéza	k1gFnSc1	hypotéza
Českého	český	k2eAgInSc2d1	český
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
růženínů	růženín	k1gInPc2	růženín
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
obecněji	obecně	k6eAd2	obecně
přisuzována	přisuzován	k2eAgFnSc1d1	přisuzována
stopovému	stopový	k2eAgNnSc3d1	stopové
množství	množství	k1gNnSc3	množství
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
nebo	nebo	k8xC	nebo
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
růženínech	růženín	k1gInPc6	růženín
byla	být	k5eAaImAgNnP	být
objevena	objeven	k2eAgNnPc1d1	objeveno
vlákna	vlákno	k1gNnPc1	vlákno
nerostu	nerost	k1gInSc2	nerost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
dumortieritu	dumortierit	k1gInSc3	dumortierit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Litoterapeutické	Litoterapeutický	k2eAgInPc4d1	Litoterapeutický
a	a	k8xC	a
léčebné	léčebný	k2eAgInPc4d1	léčebný
účinky	účinek	k1gInPc4	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
litoterapeutických	litoterapeutický	k2eAgFnPc2d1	litoterapeutický
příruček	příručka	k1gFnPc2	příručka
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
esoterických	esoterický	k2eAgFnPc2d1	esoterická
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
růženín	růženín	k1gInSc1	růženín
údajně	údajně	k6eAd1	údajně
podporuje	podporovat	k5eAaImIp3nS	podporovat
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
něžnost	něžnost	k1gFnSc4	něžnost
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
nesmělosti	nesmělost	k1gFnSc6	nesmělost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
duševní	duševní	k2eAgFnSc4d1	duševní
tvořivost	tvořivost	k1gFnSc4	tvořivost
a	a	k8xC	a
oživuje	oživovat	k5eAaImIp3nS	oživovat
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
neurózách	neuróza	k1gFnPc6	neuróza
a	a	k8xC	a
stavech	stav	k1gInPc6	stav
úzkosti	úzkost	k1gFnSc2	úzkost
<g/>
.	.	kIx.	.
</s>
<s>
Léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
kamenů	kámen	k1gInPc2	kámen
(	(	kIx(	(
<g/>
krystalů	krystal	k1gInPc2	krystal
<g/>
)	)	kIx)	)
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
komunitou	komunita	k1gFnSc7	komunita
zpochybňovány	zpochybňován	k2eAgFnPc1d1	zpochybňována
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
snad	snad	k9	snad
jen	jen	k9	jen
psychologickými	psychologický	k2eAgInPc7d1	psychologický
efekty	efekt	k1gInPc7	efekt
<g/>
,	,	kIx,	,
placebo	placebo	k1gNnSc4	placebo
efektem	efekt	k1gInSc7	efekt
<g/>
,	,	kIx,	,
autosugescí	autosugesce	k1gFnSc7	autosugesce
a	a	k8xC	a
vyvoláním	vyvolání	k1gNnSc7	vyvolání
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
asociací	asociace	k1gFnPc2	asociace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ďuda	Ďuda	k1gFnSc1	Ďuda
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Rejl	Rejl	k1gInSc1	Rejl
L.	L.	kA	L.
Svět	svět	k1gInSc1	svět
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
třetí	třetí	k4xOgFnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Granit	granit	k1gInSc1	granit
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7296-018-0	[number]	k4	80-7296-018-0
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
růženín	růženín	k1gInSc1	růženín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Růženín	růženín	k1gInSc1	růženín
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
