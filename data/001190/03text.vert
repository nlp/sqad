<s>
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1810	[number]	k4	1810
Praha-Malá	Praha-Malý	k2eAgFnSc1d1	Praha-Malá
Strana	strana	k1gFnSc1	strana
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
Litoměřice	Litoměřice	k1gInPc4	Litoměřice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
českého	český	k2eAgInSc2d1	český
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
dominuje	dominovat	k5eAaImIp3nS	dominovat
lyrickoepická	lyrickoepický	k2eAgFnSc1d1	lyrickoepická
skladba	skladba	k1gFnSc1	skladba
Máj	máj	k1gFnSc1	máj
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvydávanějších	vydávaný	k2eAgFnPc2d3	nejvydávanější
českých	český	k2eAgFnPc2d1	Česká
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Přiložený	přiložený	k2eAgInSc1d1	přiložený
obrázek	obrázek	k1gInSc1	obrázek
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
podobizna	podobizna	k1gFnSc1	podobizna
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k8xS	až
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tuto	tento	k3xDgFnSc4	tento
podobiznou	podobizna	k1gFnSc7	podobizna
ukázali	ukázat	k5eAaPmAgMnP	ukázat
jeho	jeho	k3xOp3gFnSc3	jeho
bývalé	bývalý	k2eAgFnSc3d1	bývalá
partnerce	partnerka	k1gFnSc3	partnerka
Eleonoře	Eleonora	k1gFnSc3	Eleonora
Šomkové	Šomková	k1gFnSc3	Šomková
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takhle	takhle	k6eAd1	takhle
Mácha	Mácha	k1gMnSc1	Mácha
nevypadal	vypadat	k5eNaPmAgMnS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Ignác	Ignác	k1gMnSc1	Ignác
Mácha	Mácha	k1gMnSc1	Mácha
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1810	[number]	k4	1810
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
na	na	k7c6	na
Újezdě	Újezd	k1gInSc6	Újezd
čp.	čp.	k?	čp.
400	[number]	k4	400
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Bílého	bílý	k1gMnSc2	bílý
orla	orel	k1gMnSc2	orel
<g/>
;	;	kIx,	;
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
zbourán	zbourat	k5eAaPmNgInS	zbourat
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
stojí	stát	k5eAaImIp3nS	stát
dům	dům	k1gInSc1	dům
nový	nový	k2eAgInSc1d1	nový
(	(	kIx(	(
<g/>
Újezd	Újezd	k1gInSc1	Újezd
čp.	čp.	k?	čp.
401	[number]	k4	401
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
upozorňující	upozorňující	k2eAgFnSc1d1	upozorňující
na	na	k7c4	na
Máchův	Máchův	k2eAgInSc4d1	Máchův
rodný	rodný	k2eAgInSc4d1	rodný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Ignác	Ignác	k1gMnSc1	Ignác
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc4	jenž
si	se	k3xPyFc3	se
počeštil	počeštit	k5eAaPmAgMnS	počeštit
na	na	k7c4	na
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
kmotrů	kmotr	k1gMnPc2	kmotr
(	(	kIx(	(
<g/>
Ignáci	Ignác	k1gMnSc6	Ignác
Mayerovi	Mayer	k1gMnSc6	Mayer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Máchovým	Máchův	k2eAgMnSc7d1	Máchův
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Mácha	Mácha	k1gMnSc1	Mácha
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
-	-	kIx~	-
<g/>
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mlynářský	mlynářský	k2eAgMnSc1d1	mlynářský
tovaryš	tovaryš	k1gMnSc1	tovaryš
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
majitel	majitel	k1gMnSc1	majitel
krupařského	krupařský	k2eAgInSc2d1	krupařský
krámku	krámek	k1gInSc2	krámek
<g/>
.	.	kIx.	.
</s>
<s>
Máchova	Máchův	k2eAgFnSc1d1	Máchova
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
Anna	Anna	k1gFnSc1	Anna
Kirchnerová	Kirchnerová	k1gFnSc1	Kirchnerová
(	(	kIx(	(
<g/>
1781	[number]	k4	1781
<g/>
-	-	kIx~	-
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
českých	český	k2eAgMnPc2d1	český
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
Hynkovi	Hynek	k1gMnSc6	Hynek
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Máchovým	Máchová	k1gFnPc3	Máchová
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Michal	Michal	k1gMnSc1	Michal
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
finanční	finanční	k2eAgFnSc3d1	finanční
krizi	krize	k1gFnSc3	krize
nežila	žít	k5eNaImAgFnS	žít
rodina	rodina	k1gFnSc1	rodina
na	na	k7c6	na
Újezdě	Újezd	k1gInSc6	Újezd
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Máchovi	Mácha	k1gMnSc3	Mácha
šestnáct	šestnáct	k4xCc4	šestnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
usadila	usadit	k5eAaPmAgFnS	usadit
na	na	k7c6	na
Dobytčím	dobytčí	k2eAgInSc6d1	dobytčí
trhu	trh	k1gInSc6	trh
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Karlovo	Karlův	k2eAgNnSc1d1	Karlovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Hrbků	Hrbek	k1gMnPc2	Hrbek
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc2	Ignác
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Mácha	Mácha	k1gMnSc1	Mácha
bydlel	bydlet	k5eAaImAgMnS	bydlet
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
v	v	k7c6	v
září	září	k1gNnSc6	září
1836	[number]	k4	1836
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gNnSc2	jeho
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Máchovy	Máchův	k2eAgInPc1d1	Máchův
školní	školní	k2eAgInPc1d1	školní
začátky	začátek	k1gInPc1	začátek
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
farní	farní	k2eAgFnSc7d1	farní
školou	škola	k1gFnSc7	škola
při	při	k7c6	při
kostelu	kostel	k1gInSc3	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
na	na	k7c4	na
Poříčí	Poříčí	k1gNnSc4	Poříčí
<g/>
,	,	kIx,	,
obsazovanou	obsazovaný	k2eAgFnSc4d1	obsazovaná
křížovnickým	křížovnický	k2eAgInSc7d1	křížovnický
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
studia	studio	k1gNnSc2	studio
hlavní	hlavní	k2eAgNnSc1d1	hlavní
školy	škola	k1gFnPc4	škola
u	u	k7c2	u
piaristů	piarista	k1gMnPc2	piarista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1824	[number]	k4	1824
až	až	k9	až
1830	[number]	k4	1830
studoval	studovat	k5eAaImAgInS	studovat
piaristické	piaristický	k2eAgNnSc4d1	Piaristické
gymnázium	gymnázium	k1gNnSc4	gymnázium
na	na	k7c6	na
dnešních	dnešní	k2eAgInPc6d1	dnešní
Příkopech	příkop	k1gInPc6	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1830	[number]	k4	1830
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
a	a	k8xC	a
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1832	[number]	k4	1832
a	a	k8xC	a
1836	[number]	k4	1836
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
univerzitě	univerzita	k1gFnSc6	univerzita
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Uměl	umět	k5eAaImAgMnS	umět
německy	německy	k6eAd1	německy
a	a	k8xC	a
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
latinu	latina	k1gFnSc4	latina
<g/>
;	;	kIx,	;
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
polských	polský	k2eAgFnPc2d1	polská
událostí	událost	k1gFnPc2	událost
(	(	kIx(	(
<g/>
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
a	a	k8xC	a
četby	četba	k1gFnSc2	četba
polských	polský	k2eAgMnPc2d1	polský
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Mácha	Mácha	k1gMnSc1	Mácha
začal	začít	k5eAaPmAgMnS	začít
učit	učit	k5eAaImF	učit
polsky	polsky	k6eAd1	polsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1831	[number]	k4	1831
až	až	k9	až
1832	[number]	k4	1832
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
přednášky	přednáška	k1gFnSc2	přednáška
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
povzbuzoval	povzbuzovat	k5eAaImAgMnS	povzbuzovat
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
k	k	k7c3	k
literární	literární	k2eAgFnSc3d1	literární
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
jejich	jejich	k3xOp3gFnSc2	jejich
práce	práce	k1gFnSc2	práce
<g/>
;	;	kIx,	;
Máchovi	Mácha	k1gMnSc3	Mácha
pochválil	pochválit	k5eAaPmAgMnS	pochválit
báseň	báseň	k1gFnSc4	báseň
Svatý	svatý	k2eAgMnSc1d1	svatý
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
básnické	básnický	k2eAgInPc4d1	básnický
pokusy	pokus	k1gInPc4	pokus
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
(	(	kIx(	(
<g/>
Versuche	Versuche	k1gInSc1	Versuche
des	des	k1gNnSc2	des
Ignac	Ignac	k1gFnSc1	Ignac
Macha	macha	k1gFnSc1	macha
<g/>
,	,	kIx,	,
Hoffnung	Hoffnung	k1gInSc1	Hoffnung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
k	k	k7c3	k
češtině	čeština	k1gFnSc3	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1831	[number]	k4	1831
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Večerní	večerní	k2eAgNnSc1d1	večerní
vyražení	vyražení	k1gNnSc1	vyražení
první	první	k4xOgInSc4	první
Máchův	Máchův	k2eAgInSc4d1	Máchův
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc4	báseň
Svatý	svatý	k2eAgMnSc1d1	svatý
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1832	[number]	k4	1832
následuje	následovat	k5eAaImIp3nS	následovat
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
periodiku	periodikum	k1gNnSc6	periodikum
báseň	báseň	k1gFnSc1	báseň
Abaelard	Abaelard	k1gInSc1	Abaelard
Heloíze	Heloíze	k1gFnSc1	Heloíze
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
podepsán	podepsán	k2eAgMnSc1d1	podepsán
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
básnické	básnický	k2eAgFnSc6d1	básnická
tvorbě	tvorba	k1gFnSc6	tvorba
nacházíme	nacházet	k5eAaImIp1nP	nacházet
sonety	sonet	k1gInPc4	sonet
i	i	k8xC	i
lyrickoepické	lyrickoepický	k2eAgFnPc4d1	lyrickoepická
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
prózách	próza	k1gFnPc6	próza
(	(	kIx(	(
<g/>
Marinka	Marinka	k1gFnSc1	Marinka
<g/>
,	,	kIx,	,
Cikáni	cikán	k1gMnPc1	cikán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
próze	próza	k1gFnSc6	próza
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
hlavně	hlavně	k6eAd1	hlavně
historickým	historický	k2eAgNnPc3d1	historické
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
napsat	napsat	k5eAaPmF	napsat
čtyřdílný	čtyřdílný	k2eAgInSc4d1	čtyřdílný
román	román	k1gInSc4	román
Kat	Kata	k1gFnPc2	Kata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc3	jeho
části	část	k1gFnSc3	část
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
,	,	kIx,	,
Valdek	Valdek	k1gInSc4	Valdek
a	a	k8xC	a
Karlův	Karlův	k2eAgInSc4d1	Karlův
tejn	tejn	k1gInSc4	tejn
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
náčrtcích	náčrtek	k1gInPc6	náčrtek
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
dokončená	dokončený	k2eAgFnSc1d1	dokončená
a	a	k8xC	a
časově	časově	k6eAd1	časově
nejpozdější	pozdní	k2eAgFnSc1d3	nejpozdější
část	část	k1gFnSc1	část
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
tetralogie	tetralogie	k1gFnSc2	tetralogie
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
Křivoklad	Křivoklad	k1gInSc1	Křivoklad
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
Obrazy	obraz	k1gInPc1	obraz
ze	z	k7c2	z
života	život	k1gInSc2	život
mého	můj	k1gMnSc2	můj
(	(	kIx(	(
<g/>
Večer	večer	k6eAd1	večer
na	na	k7c6	na
Bezdězu	Bezděz	k1gInSc6	Bezděz
<g/>
,	,	kIx,	,
Marinka	Marinka	k1gFnSc1	Marinka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
současnosti	současnost	k1gFnSc6	současnost
a	a	k8xC	a
kromě	kromě	k7c2	kromě
lyrizujících	lyrizující	k2eAgFnPc2d1	lyrizující
tendencí	tendence	k1gFnPc2	tendence
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nacházíme	nacházet	k5eAaImIp1nP	nacházet
i	i	k9	i
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
ostatně	ostatně	k6eAd1	ostatně
jako	jako	k9	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Máchově	Máchův	k2eAgFnSc6d1	Máchova
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
uveřejněny	uveřejnit	k5eAaPmNgFnP	uveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
v	v	k7c6	v
Květech	květ	k1gInPc6	květ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
prací	práce	k1gFnSc7	práce
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
Cikáni	cikán	k1gMnPc1	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
neprošel	projít	k5eNaPmAgInS	projít
cenzurou	cenzura	k1gFnSc7	cenzura
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
kompletně	kompletně	k6eAd1	kompletně
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dalších	další	k2eAgFnPc2d1	další
próz	próza	k1gFnPc2	próza
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
Pouť	pouť	k1gFnSc4	pouť
krkonošskou	krkonošský	k2eAgFnSc4d1	Krkonošská
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
Klášter	klášter	k1gInSc1	klášter
sázavský	sázavský	k2eAgInSc1d1	sázavský
<g/>
,	,	kIx,	,
Valdice	Valdice	k1gFnSc1	Valdice
<g/>
,	,	kIx,	,
Rozbroj	rozbroj	k1gInSc1	rozbroj
světů	svět	k1gInPc2	svět
či	či	k8xC	či
Sen	sena	k1gFnPc2	sena
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
si	se	k3xPyFc3	se
rovněž	rovněž	k9	rovněž
vedl	vést	k5eAaImAgMnS	vést
literární	literární	k2eAgInSc4d1	literární
zápisník	zápisník	k1gInSc4	zápisník
<g/>
,	,	kIx,	,
deníky	deník	k1gInPc4	deník
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
dopisy	dopis	k1gInPc4	dopis
<g/>
;	;	kIx,	;
právě	právě	k9	právě
tyto	tento	k3xDgInPc1	tento
prameny	pramen	k1gInPc1	pramen
jsou	být	k5eAaImIp3nP	být
důležitými	důležitý	k2eAgInPc7d1	důležitý
zdroji	zdroj	k1gInPc7	zdroj
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzním	kontroverzní	k2eAgMnSc7d1	kontroverzní
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
intimní	intimní	k2eAgInSc4d1	intimní
deník	deník	k1gInSc4	deník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
psaný	psaný	k2eAgInSc1d1	psaný
šifrovaně	šifrovaně	k6eAd1	šifrovaně
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
detaily	detail	k1gInPc4	detail
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
každodennosti	každodennost	k1gFnSc2	každodennost
a	a	k8xC	a
otevřeně	otevřeně	k6eAd1	otevřeně
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Lori	Lori	k1gFnSc7	Lori
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
deník	deník	k1gInSc1	deník
neúplně	úplně	k6eNd1	úplně
rozluštil	rozluštit	k5eAaPmAgInS	rozluštit
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
pak	pak	k6eAd1	pak
Karel	Karel	k1gMnSc1	Karel
Janský	janský	k2eAgMnSc1d1	janský
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgInS	zasazovat
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
nezveřejnění	nezveřejnění	k1gNnSc6	nezveřejnění
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gFnSc1	máj
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
ústřední	ústřední	k2eAgNnSc4d1	ústřední
místo	místo	k1gNnSc4	místo
jak	jak	k8xC	jak
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
skladbě	skladba	k1gFnSc6	skladba
intenzivně	intenzivně	k6eAd1	intenzivně
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1835	[number]	k4	1835
a	a	k8xC	a
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochoval	dochovat	k5eAaPmAgInS	dochovat
se	se	k3xPyFc4	se
náčrt	náčrt	k1gInSc1	náčrt
básně	báseň	k1gFnSc2	báseň
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gInSc1	máj
byl	být	k5eAaImAgInS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
vydat	vydat	k5eAaPmF	vydat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc4	tisk
provedla	provést	k5eAaPmAgFnS	provést
pražská	pražský	k2eAgFnSc1d1	Pražská
tiskárna	tiskárna	k1gFnSc1	tiskárna
Jana	Jan	k1gMnSc2	Jan
Spurného	Spurný	k1gMnSc2	Spurný
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gInSc1	máj
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgInPc2	všecek
600	[number]	k4	600
výtisků	výtisk	k1gInPc2	výtisk
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
rozprodalo	rozprodat	k5eAaPmAgNnS	rozprodat
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gInSc1	máj
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
Hynku	Hynek	k1gMnSc3	Hynek
Kommovi	Komm	k1gMnSc3	Komm
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pražskému	pražský	k2eAgMnSc3d1	pražský
měšťanovi	měšťan	k1gMnSc3	měšťan
<g/>
,	,	kIx,	,
pekařskému	pekařský	k2eAgMnSc3d1	pekařský
mistru	mistr	k1gMnSc3	mistr
<g/>
,	,	kIx,	,
pozdějšímu	pozdní	k2eAgMnSc3d2	pozdější
radnímu	radní	k1gMnSc3	radní
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Máchův	Máchův	k2eAgMnSc1d1	Máchův
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
básni	báseň	k1gFnSc3	báseň
je	být	k5eAaImIp3nS	být
připojen	připojen	k2eAgInSc1d1	připojen
autorův	autorův	k2eAgInSc1d1	autorův
výklad	výklad	k1gInSc1	výklad
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
patrně	patrně	k6eAd1	patrně
pro	pro	k7c4	pro
cenzuru	cenzura	k1gFnSc4	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Mnohovrstevnatá	mnohovrstevnatý	k2eAgFnSc1d1	mnohovrstevnatá
báseň	báseň	k1gFnSc1	báseň
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
zpěvy	zpěv	k1gInPc4	zpěv
a	a	k8xC	a
2	[number]	k4	2
intermezza	intermezzo	k1gNnSc2	intermezzo
<g/>
.	.	kIx.	.
</s>
<s>
Básnický	básnický	k2eAgInSc1d1	básnický
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
metafor	metafora	k1gFnPc2	metafora
<g/>
,	,	kIx,	,
oxymór	oxymóron	k1gNnPc2	oxymóron
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
tragický	tragický	k2eAgInSc4d1	tragický
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
autora	autor	k1gMnSc2	autor
patrně	patrně	k6eAd1	patrně
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
reálná	reálný	k2eAgFnSc1d1	reálná
událost	událost	k1gFnSc1	událost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
Rozprechticích	Rozprechtice	k1gFnPc6	Rozprechtice
u	u	k7c2	u
Dubé	Dubá	k1gFnSc2	Dubá
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otcovraždě	otcovražda	k1gFnSc3	otcovražda
<g/>
.	.	kIx.	.
</s>
<s>
Máchovi	Máchův	k2eAgMnPc1d1	Máchův
událost	událost	k1gFnSc1	událost
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
hostinský	hostinský	k1gMnSc1	hostinský
Kampe	kamp	k1gInSc5	kamp
z	z	k7c2	z
Doks	Doksy	k1gInPc2	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Tematické	tematický	k2eAgNnSc1d1	tematické
"	"	kIx"	"
<g/>
zužování	zužování	k1gNnSc1	zužování
<g/>
"	"	kIx"	"
Máje	máje	k1gFnSc1	máje
na	na	k7c4	na
píseň	píseň	k1gFnSc4	píseň
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
oslavu	oslava	k1gFnSc4	oslava
přírody	příroda	k1gFnSc2	příroda
či	či	k8xC	či
líčení	líčení	k1gNnSc2	líčení
romantického	romantický	k2eAgInSc2d1	romantický
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
zjednodušujících	zjednodušující	k2eAgInPc6d1	zjednodušující
výkladech	výklad	k1gInPc6	výklad
Máje	máj	k1gFnSc2	máj
nikoli	nikoli	k9	nikoli
řídké	řídký	k2eAgInPc1d1	řídký
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gInSc1	máj
je	být	k5eAaImIp3nS	být
inspirativním	inspirativní	k2eAgNnSc7d1	inspirativní
<g/>
,	,	kIx,	,
meditativním	meditativní	k2eAgNnSc7d1	meditativní
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sice	sice	k8xC	sice
těží	těžet	k5eAaImIp3nP	těžet
z	z	k7c2	z
motivů	motiv	k1gInPc2	motiv
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jehož	jehož	k3xOyRp3gNnSc1	jehož
myšlenkové	myšlenkový	k2eAgNnSc1d1	myšlenkové
zázemí	zázemí	k1gNnSc1	zázemí
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
otázek	otázka	k1gFnPc2	otázka
spíše	spíše	k9	spíše
metafyzických	metafyzický	k2eAgNnPc2d1	metafyzické
(	(	kIx(	(
<g/>
především	především	k9	především
druhý	druhý	k4xOgInSc1	druhý
zpěv	zpěv	k1gInSc1	zpěv
skladby	skladba	k1gFnSc2	skladba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
domácí	domácí	k2eAgFnSc1d1	domácí
kritika	kritika	k1gFnSc1	kritika
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Krasoslav	Krasoslav	k1gMnSc1	Krasoslav
Chmelenský	Chmelenský	k2eAgMnSc1d1	Chmelenský
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Máj	máj	k1gFnSc4	máj
nenašla	najít	k5eNaPmAgFnS	najít
pochopení	pochopení	k1gNnPc4	pochopení
(	(	kIx(	(
<g/>
absence	absence	k1gFnSc1	absence
národnostního	národnostní	k2eAgInSc2d1	národnostní
apelu	apel	k1gInSc2	apel
<g/>
,	,	kIx,	,
údajné	údajný	k2eAgNnSc4d1	údajné
kopírování	kopírování	k1gNnSc4	kopírování
Byrona	Byron	k1gMnSc2	Byron
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gFnSc1	máj
se	se	k3xPyFc4	se
však	však	k9	však
šířil	šířit	k5eAaImAgMnS	šířit
v	v	k7c6	v
opisech	opis	k1gInPc6	opis
a	a	k8xC	a
definitivní	definitivní	k2eAgFnSc3d1	definitivní
rehabilitaci	rehabilitace	k1gFnSc3	rehabilitace
mu	on	k3xPp3gNnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
generace	generace	k1gFnSc1	generace
Májovců	májovec	k1gMnPc2	májovec
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Máj	máj	k1gFnSc4	máj
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvydávanějších	vydávaný	k2eAgFnPc2d3	nejvydávanější
českých	český	k2eAgFnPc2d1	Česká
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
vydání	vydání	k1gNnPc1	vydání
ilustrovali	ilustrovat	k5eAaBmAgMnP	ilustrovat
naši	náš	k3xOp1gMnPc1	náš
výtvarníci	výtvarník	k1gMnPc1	výtvarník
<g/>
:	:	kIx,	:
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
<g/>
,	,	kIx,	,
Toyen	Toyen	k2eAgMnSc1d1	Toyen
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Koblasa	Koblasa	k1gFnSc1	Koblasa
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Sukdolák	Sukdolák	k1gMnSc1	Sukdolák
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
česká	český	k2eAgFnSc1d1	Česká
i	i	k8xC	i
německá	německý	k2eAgFnSc1d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ochotnicky	ochotnicky	k6eAd1	ochotnicky
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
a	a	k8xC	a
Kajetánském	kajetánský	k2eAgNnSc6d1	Kajetánské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1834	[number]	k4	1834
až	až	k9	až
1835	[number]	k4	1835
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
v	v	k7c6	v
16	[number]	k4	16
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
Klicperových	Klicperových	k2eAgFnPc1d1	Klicperových
a	a	k8xC	a
Štěpánkových	Štěpánkových	k2eAgFnPc1d1	Štěpánkových
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
student	student	k1gMnSc1	student
univerzity	univerzita	k1gFnSc2	univerzita
musel	muset	k5eAaImAgMnS	muset
vystupovat	vystupovat	k5eAaImF	vystupovat
pod	pod	k7c7	pod
pseudonymy	pseudonym	k1gInPc7	pseudonym
<g/>
:	:	kIx,	:
Milihaj	Milihaj	k1gMnSc1	Milihaj
<g/>
,	,	kIx,	,
Chám	chám	k1gMnSc1	chám
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
včela	včela	k1gFnSc1	včela
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Mácha	Mácha	k1gMnSc1	Mácha
nejvíce	hodně	k6eAd3	hodně
chválen	chválen	k2eAgMnSc1d1	chválen
(	(	kIx(	(
<g/>
Chmelenským	Chmelenský	k2eAgMnSc7d1	Chmelenský
<g/>
)	)	kIx)	)
za	za	k7c4	za
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
roli	role	k1gFnSc6	role
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Prachatického	prachatický	k2eAgMnSc2d1	prachatický
v	v	k7c6	v
Klicperově	Klicperův	k2eAgInSc6d1	Klicperův
Blaníku	Blaník	k1gInSc6	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
také	také	k9	také
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
dochované	dochovaný	k2eAgInPc1d1	dochovaný
zlomky	zlomek	k1gInPc1	zlomek
historických	historický	k2eAgNnPc2d1	historické
dramat	drama	k1gNnPc2	drama
<g/>
,	,	kIx,	,
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1831	[number]	k4	1831
a	a	k8xC	a
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
<s>
Jmenují	jmenovat	k5eAaImIp3nP	jmenovat
se	se	k3xPyFc4	se
Bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Bratrovrah	bratrovrah	k1gMnSc1	bratrovrah
<g/>
.	.	kIx.	.
</s>
<s>
Žádnou	žádný	k3yNgFnSc4	žádný
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
byl	být	k5eAaImAgMnS	být
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
většinou	většinou	k6eAd1	většinou
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
zejména	zejména	k9	zejména
romantická	romantický	k2eAgNnPc4d1	romantické
místa	místo	k1gNnPc4	místo
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
jako	jako	k8xC	jako
osamělý	osamělý	k2eAgMnSc1d1	osamělý
poutník	poutník	k1gMnSc1	poutník
krajinou	krajina	k1gFnSc7	krajina
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
cest	cesta	k1gFnPc2	cesta
nepodnikl	podniknout	k5eNaPmAgMnS	podniknout
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
delší	dlouhý	k2eAgFnSc4d2	delší
cestu	cesta	k1gFnSc4	cesta
vykonal	vykonat	k5eAaPmAgMnS	vykonat
Mácha	Mácha	k1gMnSc1	Mácha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
<g/>
;	;	kIx,	;
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Mělník	Mělník	k1gInSc4	Mělník
<g/>
,	,	kIx,	,
Kokořín	Kokořín	k1gInSc4	Kokořín
<g/>
,	,	kIx,	,
Housku	houska	k1gFnSc4	houska
<g/>
,	,	kIx,	,
Doksy	Doksy	k1gInPc1	Doksy
<g/>
,	,	kIx,	,
na	na	k7c4	na
Bezděz	Bezděz	k1gInSc4	Bezděz
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
jej	on	k3xPp3gMnSc4	on
Eduard	Eduard	k1gMnSc1	Eduard
Hindl	Hindl	k1gMnSc1	Hindl
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolužák	spolužák	k1gMnSc1	spolužák
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Máchova	Máchův	k2eAgFnSc1d1	Máchova
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Mělníka	Mělník	k1gInSc2	Mělník
na	na	k7c4	na
Bezděz	Bezděz	k1gInSc4	Bezděz
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
červeně	červeně	k6eAd1	červeně
značenou	značený	k2eAgFnSc7d1	značená
trasou	trasa	k1gFnSc7	trasa
KČT	KČT	kA	KČT
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	září	k1gNnSc2	září
1833	[number]	k4	1833
podnikl	podniknout	k5eAaPmAgMnS	podniknout
opět	opět	k6eAd1	opět
s	s	k7c7	s
Hindlem	Hindlo	k1gNnSc7	Hindlo
tzv.	tzv.	kA	tzv.
Krkonošskou	krkonošský	k2eAgFnSc4d1	Krkonošská
pouť	pouť	k1gFnSc4	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mj.	mj.	kA	mj.
Valečov	Valečov	k1gInSc1	Valečov
<g/>
,	,	kIx,	,
Kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
Trosky	troska	k1gFnPc1	troska
<g/>
,	,	kIx,	,
Radim	Radim	k1gMnSc1	Radim
u	u	k7c2	u
Jičína	Jičín	k1gInSc2	Jičín
<g/>
,	,	kIx,	,
Třebihošť	Třebihošť	k1gFnSc1	Třebihošť
<g/>
.	.	kIx.	.
</s>
<s>
Máchův	Máchův	k2eAgInSc1d1	Máchův
podpis	podpis	k1gInSc1	podpis
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
pamětní	pamětní	k2eAgFnSc6d1	pamětní
knize	kniha	k1gFnSc6	kniha
Sněžky	Sněžka	k1gFnSc2	Sněžka
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
končin	končina	k1gFnPc2	končina
Mácha	Mácha	k1gMnSc1	Mácha
uskutečňoval	uskutečňovat	k5eAaImAgMnS	uskutečňovat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1834	[number]	k4	1834
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
Mácha	Mácha	k1gMnSc1	Mácha
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
společníkem	společník	k1gMnSc7	společník
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Strobach	Strobach	k1gMnSc1	Strobach
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
pražský	pražský	k2eAgMnSc1d1	pražský
purkmistr	purkmistr	k1gMnSc1	purkmistr
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
trvala	trvat	k5eAaImAgFnS	trvat
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
ušli	ujít	k5eAaPmAgMnP	ujít
denně	denně	k6eAd1	denně
až	až	k9	až
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
,	,	kIx,	,
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Salcburk	Salcburk	k1gInSc1	Salcburk
<g/>
,	,	kIx,	,
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pluli	plout	k5eAaImAgMnP	plout
do	do	k7c2	do
Terstu	Terst	k1gInSc2	Terst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
se	se	k3xPyFc4	se
Mácha	Mácha	k1gMnSc1	Mácha
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
významným	významný	k2eAgMnSc7d1	významný
slovinským	slovinský	k2eAgMnSc7d1	slovinský
básníkem	básník	k1gMnSc7	básník
Francem	Franc	k1gMnSc7	Franc
Prešerenem	Prešeren	k1gMnSc7	Prešeren
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
přes	přes	k7c4	přes
Vídeň	Vídeň	k1gFnSc4	Vídeň
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svatodušních	svatodušní	k2eAgInPc6d1	svatodušní
svátcích	svátek	k1gInPc6	svátek
1835	[number]	k4	1835
šel	jít	k5eAaImAgMnS	jít
Mácha	Mácha	k1gMnSc1	Mácha
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
na	na	k7c4	na
osmidenní	osmidenní	k2eAgInSc4d1	osmidenní
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
boleslavského	boleslavský	k2eAgInSc2d1	boleslavský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
navštívili	navštívit	k5eAaPmAgMnP	navštívit
vlasteneckého	vlastenecký	k2eAgMnSc4d1	vlastenecký
kněze	kněz	k1gMnSc4	kněz
Antonína	Antonín	k1gMnSc4	Antonín
Marka	Marek	k1gMnSc4	Marek
<g/>
,	,	kIx,	,
žijícího	žijící	k2eAgMnSc4d1	žijící
nedaleko	nedaleko	k7c2	nedaleko
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
půvab	půvab	k1gInSc4	půvab
měly	mít	k5eAaImAgFnP	mít
pro	pro	k7c4	pro
Máchu	mách	k1gInSc3	mách
hrady	hrad	k1gInPc4	hrad
a	a	k8xC	a
zříceniny	zřícenina	k1gFnPc4	zřícenina
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
si	se	k3xPyFc3	se
seznam	seznam	k1gInSc4	seznam
"	"	kIx"	"
<g/>
hradů	hrad	k1gInPc2	hrad
spatřených	spatřený	k2eAgInPc2d1	spatřený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c4	na
90	[number]	k4	90
položek	položka	k1gFnPc2	položka
(	(	kIx(	(
<g/>
mnohé	mnohý	k2eAgInPc4d1	mnohý
hrady	hrad	k1gInPc4	hrad
navštívil	navštívit	k5eAaPmAgMnS	navštívit
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
byl	být	k5eAaImAgMnS	být
obdařen	obdařit	k5eAaPmNgMnS	obdařit
i	i	k9	i
výtvarným	výtvarný	k2eAgInSc7d1	výtvarný
talentem	talent	k1gInSc7	talent
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
115	[number]	k4	115
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
akvarelů	akvarel	k1gInPc2	akvarel
"	"	kIx"	"
<g/>
hradů	hrad	k1gInPc2	hrad
spatřených	spatřený	k2eAgInPc2d1	spatřený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
Máchovy	Máchův	k2eAgFnPc1d1	Máchova
práce	práce	k1gFnPc1	práce
vznikaly	vznikat	k5eAaImAgFnP	vznikat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
plenéru	plenér	k1gInSc6	plenér
(	(	kIx(	(
<g/>
doma	doma	k6eAd1	doma
pak	pak	k6eAd1	pak
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
koloroval	kolorovat	k5eAaBmAgInS	kolorovat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
hrady	hrad	k1gInPc4	hrad
kreslil	kreslit	k5eAaImAgInS	kreslit
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
pochází	pocházet	k5eAaImIp3nS	pocházet
Máchova	Máchův	k2eAgFnSc1d1	Máchova
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gInSc7	jeho
autoportrétem	autoportrét	k1gInSc7	autoportrét
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Máchově	Máchův	k2eAgFnSc6d1	Máchova
fascinaci	fascinace	k1gFnSc6	fascinace
výtvarným	výtvarný	k2eAgNnSc7d1	výtvarné
uměním	umění	k1gNnSc7	umění
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
poznámky	poznámka	k1gFnPc4	poznámka
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
obrazárny	obrazárna	k1gFnSc2	obrazárna
(	(	kIx(	(
<g/>
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Máchovou	Máchová	k1gFnSc4	Máchová
první	první	k4xOgFnSc7	první
doloženou	doložený	k2eAgFnSc7d1	doložená
láskou	láska	k1gFnSc7	láska
byla	být	k5eAaImAgFnS	být
Marinka	Marinka	k1gFnSc1	Marinka
Stichová	Stichová	k1gFnSc1	Stichová
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1832	[number]	k4	1832
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Máchovi	Mácha	k1gMnSc3	Mácha
imponoval	imponovat	k5eAaImAgMnS	imponovat
nejen	nejen	k6eAd1	nejen
její	její	k3xOp3gInSc4	její
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
znala	znát	k5eAaImAgFnS	znát
a	a	k8xC	a
četla	číst	k5eAaImAgFnS	číst
české	český	k2eAgFnPc4d1	Česká
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ozvěny	ozvěna	k1gFnPc1	ozvěna
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
Marince	Marinka	k1gFnSc3	Marinka
se	se	k3xPyFc4	se
promítly	promítnout	k5eAaPmAgInP	promítnout
i	i	k9	i
do	do	k7c2	do
Máchovy	Máchův	k2eAgFnSc2d1	Máchova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
vztah	vztah	k1gInSc1	vztah
neměl	mít	k5eNaImAgInS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
se	se	k3xPyFc4	se
v	v	k7c6	v
Pouti	pouť	k1gFnSc6	pouť
krkonošské	krkonošský	k2eAgFnSc3d1	Krkonošská
Mácha	Mácha	k1gMnSc1	Mácha
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
ze	z	k7c2	z
ztracené	ztracený	k2eAgFnSc2d1	ztracená
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
zahořel	zahořet	k5eAaPmAgInS	zahořet
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
mladé	mladý	k2eAgFnSc3d1	mladá
herečce	herečka	k1gFnSc3	herečka
Rošrové	Rošrová	k1gFnSc2	Rošrová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledkem	výsledek	k1gInSc7	výsledek
vzplanutí	vzplanutí	k1gNnSc2	vzplanutí
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
báseň	báseň	k1gFnSc4	báseň
Panně	Panna	k1gFnSc3	Panna
Rošrové	Rošrový	k2eAgFnSc3d1	Rošrový
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc7d1	skutečná
Máchovou	Máchův	k2eAgFnSc7d1	Máchova
partnerkou	partnerka	k1gFnSc7	partnerka
<g/>
,	,	kIx,	,
snoubenkou	snoubenka	k1gFnSc7	snoubenka
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
byla	být	k5eAaImAgFnS	být
Eleonora	Eleonora	k1gFnSc1	Eleonora
Šomková	Šomková	k1gFnSc1	Šomková
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1833	[number]	k4	1833
<g/>
/	/	kIx~	/
<g/>
1834	[number]	k4	1834
po	po	k7c6	po
divadelní	divadelní	k2eAgFnSc6d1	divadelní
zkoušce	zkouška	k1gFnSc6	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Celetné	Celetný	k2eAgFnSc6d1	Celetná
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Červeného	Červený	k1gMnSc2	Červený
orla	orel	k1gMnSc2	orel
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
čp.	čp.	k?	čp.
593	[number]	k4	593
<g/>
/	/	kIx~	/
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
kavárna	kavárna	k1gFnSc1	kavárna
U	u	k7c2	u
Suchých	Suchá	k1gFnPc2	Suchá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
seznámení	seznámení	k1gNnSc3	seznámení
s	s	k7c7	s
Lori	Lori	k1gFnSc7	Lori
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
pozdější	pozdní	k2eAgFnSc2d2	pozdější
ženy	žena	k1gFnSc2	žena
Leni	Leni	k?	Leni
Forchheimové	Forchheimová	k1gFnSc2	Forchheimová
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
ukončila	ukončit	k5eAaPmAgFnS	ukončit
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
třech	tři	k4xCgFnPc6	tři
letech	let	k1gInPc6	let
Máchova	Máchův	k2eAgFnSc1d1	Máchova
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
nastiňuje	nastiňovat	k5eAaImIp3nS	nastiňovat
Máchův	Máchův	k2eAgInSc4d1	Máchův
intimní	intimní	k2eAgInSc4d1	intimní
deník	deník	k1gInSc4	deník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
dopisy	dopis	k1gInPc7	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vztahu	vztah	k1gInSc6	vztah
Máchova	Máchův	k2eAgFnSc1d1	Máchova
povaha	povaha	k1gFnSc1	povaha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
přecitlivělost	přecitlivělost	k1gFnSc4	přecitlivělost
a	a	k8xC	a
žárlivost	žárlivost	k1gFnSc4	žárlivost
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
učinili	učinit	k5eAaPmAgMnP	učinit
i	i	k9	i
rodiče	rodič	k1gMnPc1	rodič
mladých	mladý	k1gMnPc2	mladý
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
Lořina	Lořin	k2eAgFnSc1d1	Lořina
neochota	neochota	k1gFnSc1	neochota
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1836	[number]	k4	1836
porodila	porodit	k5eAaPmAgFnS	porodit
Lori	Lori	k1gFnSc4	Lori
Máchovi	Mácha	k1gMnSc3	Mácha
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
s	s	k7c7	s
Máchou	Mácha	k1gMnSc7	Mácha
viděla	vidět	k5eAaImAgFnS	vidět
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
konat	konat	k5eAaImF	konat
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
zemřel	zemřít	k5eAaPmAgMnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1837	[number]	k4	1837
na	na	k7c4	na
psotník	psotník	k1gInSc4	psotník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
srpna	srpen	k1gInSc2	srpen
1836	[number]	k4	1836
získal	získat	k5eAaPmAgMnS	získat
Mácha	Mácha	k1gMnSc1	Mácha
absolutorium	absolutorium	k1gNnSc4	absolutorium
na	na	k7c6	na
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
však	však	k9	však
odložil	odložit	k5eAaPmAgMnS	odložit
-	-	kIx~	-
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
cestovat	cestovat	k5eAaImF	cestovat
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
koncipient	koncipient	k1gMnSc1	koncipient
u	u	k7c2	u
litoměřického	litoměřický	k2eAgMnSc2d1	litoměřický
justiciára	justiciár	k1gMnSc2	justiciár
Josefa	Josef	k1gMnSc2	Josef
Filipa	Filip	k1gMnSc2	Filip
Durase	Durasa	k1gFnSc6	Durasa
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
-	-	kIx~	-
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
narodil	narodit	k5eAaPmAgMnS	narodit
Lori	lori	k1gMnSc1	lori
syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
jel	jet	k5eAaImAgMnS	jet
Mácha	Mácha	k1gMnSc1	Mácha
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potomka	potomek	k1gMnSc4	potomek
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
doslova	doslova	k6eAd1	doslova
rozběhl	rozběhnout	k5eAaPmAgMnS	rozběhnout
z	z	k7c2	z
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
naposledy	naposledy	k6eAd1	naposledy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Prahu	Praha	k1gFnSc4	Praha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
viděl	vidět	k5eAaImAgMnS	vidět
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
Lori	Lori	k1gFnSc4	Lori
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
si	se	k3xPyFc3	se
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Radobýlu	Radobýl	k1gInSc2	Radobýl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
upravoval	upravovat	k5eAaImAgMnS	upravovat
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
báseň	báseň	k1gFnSc4	báseň
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
všiml	všimnout	k5eAaPmAgMnS	všimnout
požáru	požár	k1gInSc6	požár
dole	dole	k6eAd1	dole
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obětavém	obětavý	k2eAgNnSc6d1	obětavé
hašení	hašení	k1gNnSc6	hašení
se	se	k3xPyFc4	se
nalokal	nalokat	k5eAaPmAgInS	nalokat
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
jeho	on	k3xPp3gNnSc2	on
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
infekčního	infekční	k2eAgNnSc2d1	infekční
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
zdraví	zdraví	k1gNnSc1	zdraví
rychle	rychle	k6eAd1	rychle
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádné	žádný	k3yNgInPc4	žádný
léky	lék	k1gInPc4	lék
neužíval	užívat	k5eNaImAgInS	užívat
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
píše	psát	k5eAaImIp3nS	psát
dva	dva	k4xCgInPc4	dva
poslední	poslední	k2eAgInPc4d1	poslední
dopisy	dopis	k1gInPc4	dopis
-	-	kIx~	-
rodičům	rodič	k1gMnPc3	rodič
a	a	k8xC	a
Lori	Lori	k1gFnPc3	Lori
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
přitížilo	přitížit	k5eAaPmAgNnS	přitížit
<g/>
,	,	kIx,	,
zvracel	zvracet	k5eAaImAgMnS	zvracet
a	a	k8xC	a
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
si	se	k3xPyFc3	se
lékaře	lékař	k1gMnSc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ráno	ráno	k6eAd1	ráno
vstal	vstát	k5eAaPmAgMnS	vstát
a	a	k8xC	a
šel	jít	k5eAaImAgMnS	jít
se	se	k3xPyFc4	se
omluvit	omluvit	k5eAaPmF	omluvit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mu	on	k3xPp3gMnSc3	on
přivolaný	přivolaný	k2eAgMnSc1d1	přivolaný
doktor	doktor	k1gMnSc1	doktor
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgInSc1d1	úřední
záznam	záznam	k1gInSc1	záznam
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
příčinu	příčina	k1gFnSc4	příčina
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
Brechdurchfall	Brechdurchfalla	k1gFnPc2	Brechdurchfalla
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
cholerinu	cholerin	k1gInSc2	cholerin
<g/>
,	,	kIx,	,
mírnější	mírný	k2eAgFnSc4d2	mírnější
formu	forma	k1gFnSc4	forma
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
dávením	dávení	k1gNnSc7	dávení
a	a	k8xC	a
průjmy	průjem	k1gInPc7	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
obava	obava	k1gFnSc1	obava
ze	z	k7c2	z
šíření	šíření	k1gNnSc2	šíření
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
na	na	k7c6	na
úřední	úřední	k2eAgFnSc6d1	úřední
listině	listina	k1gFnSc6	listina
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
posunuto	posunut	k2eAgNnSc1d1	posunuto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pamětní	pamětní	k2eAgFnSc6d1	pamětní
desce	deska	k1gFnSc6	deska
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
i	i	k8xC	i
na	na	k7c6	na
náhrobku	náhrobek	k1gInSc6	náhrobek
uvedeno	uvést	k5eAaPmNgNnS	uvést
datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
již	již	k6eAd1	již
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
litoměřickém	litoměřický	k2eAgInSc6d1	litoměřický
hřbitově	hřbitov	k1gInSc6	hřbitov
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
ve	v	k7c4	v
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Máchova	Máchův	k2eAgMnSc2d1	Máchův
bratra	bratr	k1gMnSc2	bratr
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Máchovy	Máchův	k2eAgInPc1d1	Máchův
ostatky	ostatek	k1gInPc1	ostatek
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
102	[number]	k4	102
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ignáce	Ignác	k1gMnSc2	Ignác
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
slouženo	sloužen	k2eAgNnSc1d1	slouženo
za	za	k7c2	za
Máchu	mách	k1gInSc2	mách
rekviem	rekviem	k1gNnSc1	rekviem
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
plný	plný	k2eAgInSc1d1	plný
<g/>
.	.	kIx.	.
</s>
<s>
Účastni	účasten	k2eAgMnPc1d1	účasten
byli	být	k5eAaImAgMnP	být
Máchovi	Máchův	k2eAgMnPc1d1	Máchův
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
.	.	kIx.	.
</s>
<s>
Máchův	Máchův	k2eAgInSc1d1	Máchův
hrob	hrob	k1gInSc1	hrob
zůstal	zůstat	k5eAaPmAgInS	zůstat
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
zanedbán	zanedbán	k2eAgInSc1d1	zanedbán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1846	[number]	k4	1846
(	(	kIx(	(
<g/>
i	i	k9	i
péčí	péče	k1gFnSc7	péče
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc2	Havlíček
Borovského	Borovský	k1gMnSc2	Borovský
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
náhrobek	náhrobek	k1gInSc1	náhrobek
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Františka	František	k1gMnSc2	František
Linna	Linn	k1gMnSc2	Linn
z	z	k7c2	z
Řetězové	řetězový	k2eAgFnSc2d1	řetězová
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přibyl	přibýt	k5eAaPmAgInS	přibýt
pseudogotický	pseudogotický	k2eAgInSc4d1	pseudogotický
kenotaf	kenotaf	k1gInSc4	kenotaf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
kultovní	kultovní	k2eAgFnSc1d1	kultovní
Máchova	Máchův	k2eAgFnSc1d1	Máchova
socha	socha	k1gFnSc1	socha
Josefa	Josef	k1gMnSc2	Josef
Václava	Václav	k1gMnSc2	Václav
Myslbeka	Myslbeek	k1gMnSc2	Myslbeek
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Petříně	Petřín	k1gInSc6	Petřín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kopie	kopie	k1gFnSc1	kopie
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
také	také	k6eAd1	také
instalována	instalovat	k5eAaBmNgFnS	instalovat
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Mostné	mostný	k1gMnPc4	mostný
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zabrání	zabrání	k1gNnSc1	zabrání
českého	český	k2eAgNnSc2d1	české
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
guvernéra	guvernér	k1gMnSc4	guvernér
Národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
Československé	československý	k2eAgFnSc2d1	Československá
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliše	k1gFnPc1	Engliše
Máchovy	Máchův	k2eAgInPc4d1	Máchův
tělesné	tělesný	k2eAgInPc4d1	tělesný
ostatky	ostatek	k1gInPc4	ostatek
exhumovány	exhumován	k2eAgInPc4d1	exhumován
a	a	k8xC	a
převezeny	převezen	k2eAgInPc4d1	převezen
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
antropologicky	antropologicky	k6eAd1	antropologicky
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
a	a	k8xC	a
pietně	pietně	k6eAd1	pietně
konzervoval	konzervovat	k5eAaBmAgMnS	konzervovat
profesor	profesor	k1gMnSc1	profesor
Jiří	Jiří	k1gMnSc1	Jiří
Malý	Malý	k1gMnSc1	Malý
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Máchův	Máchův	k2eAgInSc1d1	Máchův
pohřeb	pohřeb	k1gInSc1	pohřeb
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
na	na	k7c6	na
vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
národní	národní	k2eAgFnSc7d1	národní
manifestací	manifestace	k1gFnSc7	manifestace
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Máchově	Máchův	k2eAgFnSc6d1	Máchova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
okruh	okruh	k1gInSc1	okruh
jeho	jeho	k3xOp3gMnPc2	jeho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
rozpoznali	rozpoznat	k5eAaPmAgMnP	rozpoznat
jedinečnost	jedinečnost	k1gFnSc4	jedinečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Květech	květ	k1gInPc6	květ
postupně	postupně	k6eAd1	postupně
vyšly	vyjít	k5eAaPmAgFnP	vyjít
básně	báseň	k1gFnPc1	báseň
Karla	Karel	k1gMnSc2	Karel
Sabiny	Sabina	k1gMnSc2	Sabina
Pomněnka	pomněnka	k1gFnSc1	pomněnka
na	na	k7c6	na
hrobě	hrob	k1gInSc6	hrob
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Františka	Františka	k1gFnSc1	Františka
L.	L.	kA	L.
Riegra	Riegro	k1gNnSc2	Riegro
Na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pláč	pláč	k1gInSc1	pláč
nad	nad	k7c7	nad
smrtí	smrt	k1gFnSc7	smrt
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
od	od	k7c2	od
Karola	Karola	k1gFnSc1	Karola
Kuzmányho	Kuzmány	k1gMnSc2	Kuzmány
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Máchu	Mácha	k1gMnSc4	Mácha
nacházely	nacházet	k5eAaImAgFnP	nacházet
větší	veliký	k2eAgNnSc4d2	veliký
pochopení	pochopení	k1gNnSc4	pochopení
oblasti	oblast	k1gFnSc2	oblast
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
německé	německý	k2eAgNnSc4d1	německé
jazykové	jazykový	k2eAgNnSc4d1	jazykové
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
německým	německý	k2eAgInSc7d1	německý
překladem	překlad	k1gInSc7	překlad
Máchovy	Máchův	k2eAgFnSc2d1	Máchova
tvorby	tvorba	k1gFnSc2	tvorba
byla	být	k5eAaImAgFnS	být
Marinka	Marinka	k1gFnSc1	Marinka
<g/>
,	,	kIx,	,
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
časopise	časopis	k1gInSc6	časopis
Adler	Adler	k1gMnSc1	Adler
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
neněmeckým	německý	k2eNgInSc7d1	neněmecký
překladem	překlad	k1gInSc7	překlad
byl	být	k5eAaImAgInS	být
překlad	překlad	k1gInSc4	překlad
Máje	máj	k1gInSc2	máj
do	do	k7c2	do
polštiny	polština	k1gFnSc2	polština
od	od	k7c2	od
Bronislawa	Bronislaw	k1gInSc2	Bronislaw
Maleckého	Malecký	k2eAgInSc2d1	Malecký
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
relativně	relativně	k6eAd1	relativně
úplné	úplný	k2eAgNnSc1d1	úplné
vydání	vydání	k1gNnSc1	vydání
Máchových	Máchových	k2eAgInPc2d1	Máchových
spisů	spis	k1gInPc2	spis
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Ignáce	Ignác	k1gMnSc2	Ignác
Leopolda	Leopold	k1gMnSc2	Leopold
Kobera	Kober	k1gMnSc2	Kober
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
-	-	kIx~	-
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
vyšel	vyjít	k5eAaPmAgInS	vyjít
česky	česky	k6eAd1	česky
Máj	máj	k1gInSc1	máj
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
pochopen	pochopit	k5eAaPmNgMnS	pochopit
a	a	k8xC	a
oceněn	ocenit	k5eAaPmNgMnS	ocenit
byl	být	k5eAaImAgMnS	být
Mácha	Mácha	k1gMnSc1	Mácha
až	až	k9	až
Nerudovou	Nerudová	k1gFnSc4	Nerudová
a	a	k8xC	a
Hálkovou	Hálková	k1gFnSc4	Hálková
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
generací	generace	k1gFnPc2	generace
almanachu	almanach	k1gInSc2	almanach
Máj	máj	k1gInSc4	máj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Májovci	májovec	k1gMnPc1	májovec
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
vlnu	vlna	k1gFnSc4	vlna
máchovského	máchovský	k2eAgInSc2d1	máchovský
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
nakonec	nakonec	k6eAd1	nakonec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
uznání	uznání	k1gNnSc4	uznání
i	i	k8xC	i
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
(	(	kIx(	(
<g/>
Šalda	Šalda	k1gMnSc1	Šalda
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
básníkem	básník	k1gMnSc7	básník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kult	kult	k1gInSc1	kult
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
kultem	kult	k1gInSc7	kult
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Máchovi	Mácha	k1gMnSc6	Mácha
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
mnoho	mnoho	k4c1	mnoho
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc2d1	různá
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
rybník	rybník	k1gInSc1	rybník
dokeský	dokeský	k2eAgInSc1d1	dokeský
(	(	kIx(	(
<g/>
Hirschberský	Hirschberský	k2eAgInSc1d1	Hirschberský
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Máchovo	Máchův	k2eAgNnSc4d1	Máchovo
jezero	jezero	k1gNnSc4	jezero
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Bezdězu	Bezděz	k1gInSc2	Bezděz
je	být	k5eAaImIp3nS	být
zvána	zvát	k5eAaImNgFnS	zvát
Máchovým	Máchův	k2eAgInSc7d1	Máchův
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc1	měsíc
květen	květen	k1gInSc4	květen
Máchovým	Máchův	k2eAgInSc7d1	Máchův
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gInSc1	máj
je	být	k5eAaImIp3nS	být
zhudebňován	zhudebňován	k2eAgInSc1d1	zhudebňován
<g/>
,	,	kIx,	,
recitován	recitován	k2eAgInSc1d1	recitován
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
i	i	k9	i
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
.	.	kIx.	.
</s>
<s>
Osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
synonymem	synonymum	k1gNnSc7	synonymum
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
mladistvého	mladistvý	k2eAgInSc2d1	mladistvý
entuziasmu	entuziasmus	k1gInSc2	entuziasmus
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
navzdory	navzdory	k7c3	navzdory
skutečnému	skutečný	k2eAgInSc3d1	skutečný
Máchovu	Máchův	k2eAgInSc3d1	Máchův
životu	život	k1gInSc3	život
a	a	k8xC	a
metafyzickému	metafyzický	k2eAgInSc3d1	metafyzický
přesahu	přesah	k1gInSc3	přesah
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
