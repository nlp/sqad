<s>
Pretorie	Pretorie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
součástí	součást	k1gFnSc7
aglomerace	aglomerace	k1gFnSc2
Tshwane	Tshwan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	to	k3xDgNnSc1
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
tří	tři	k4xCgNnPc2
hlavních	hlavní	k2eAgNnPc2d1
měst	město	k1gNnPc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
další	další	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
jsou	být	k5eAaImIp3nP
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
-	-	kIx~
sídlo	sídlo	k1gNnSc1
zákonodárné	zákonodárný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
Bloemfontein	Bloemfontein	k1gFnPc1
–	–	k?
sídlo	sídlo	k1gNnSc4
soudní	soudní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>