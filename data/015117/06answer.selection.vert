<s>
Pretorie	Pretorie	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1855	#num#	k4
Martinem	Martin	k1gMnSc7
Pretoriem	Pretorius	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
pojmenoval	pojmenovat	k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
město	město	k1gNnSc4
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
otci	otec	k1gMnSc6
Andriesi	Andries	k1gFnSc3
Pretoriovi	Pretorius	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>