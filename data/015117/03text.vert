<s>
Pretorie	Pretorie	k1gFnSc1
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
25	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
28	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
1	#num#	k4
339	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
+2	+2	k4
Stát	stát	k1gInSc1
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
provincie	provincie	k1gFnSc2
</s>
<s>
Gauteng	Gauteng	k1gMnSc1
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
688	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
741	#num#	k4
651	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
078	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.pretoria.co.za	www.pretoria.co.za	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
součástí	součást	k1gFnSc7
aglomerace	aglomerace	k1gFnSc2
Tshwane	Tshwan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
provincii	provincie	k1gFnSc6
Gauteng	Gautenga	k1gFnPc2
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	to	k3xDgNnSc1
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
tří	tři	k4xCgNnPc2
hlavních	hlavní	k2eAgNnPc2d1
měst	město	k1gNnPc2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
další	další	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
jsou	být	k5eAaImIp3nP
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
-	-	kIx~
sídlo	sídlo	k1gNnSc1
zákonodárné	zákonodárný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
Bloemfontein	Bloemfontein	k1gFnPc1
–	–	k?
sídlo	sídlo	k1gNnSc4
soudní	soudní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
sídlo	sídlo	k1gNnSc4
moci	moc	k1gFnSc2
výkonné	výkonný	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
ale	ale	k9
běžně	běžně	k6eAd1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
město	město	k1gNnSc4
hlavní	hlavní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aglomerace	aglomerace	k1gFnSc1
celého	celý	k2eAgNnSc2d1
města	město	k1gNnSc2
(	(	kIx(
<g/>
Tshwane	Tshwan	k1gMnSc5
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
2	#num#	k4
345	#num#	k4
908	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
přechodné	přechodný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
mezi	mezi	k7c7
Vysokým	vysoký	k2eAgMnSc7d1
Veldem	Veld	k1gMnSc7
a	a	k8xC
Bushveldem	Bushveld	k1gMnSc7
<g/>
,	,	kIx,
asi	asi	k9
50	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Johannesburgu	Johannesburg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
teplém	teplé	k1gNnSc6
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
krytém	krytý	k2eAgNnSc6d1
<g/>
,	,	kIx,
úrodném	úrodný	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
<g/>
,	,	kIx,
obklopena	obklopen	k2eAgFnSc1d1
kopci	kopec	k1gInSc3
z	z	k7c2
Magaliesbergského	Magaliesbergský	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1370	#num#	k4
m.	m.	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Pretorii	Pretorie	k1gFnSc4
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1855	#num#	k4
Martinem	Martin	k1gMnSc7
Pretoriem	Pretorius	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
pojmenoval	pojmenovat	k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
město	město	k1gNnSc4
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
otci	otec	k1gMnSc6
Andriesi	Andries	k1gFnSc3
Pretoriovi	Pretorius	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Obě	dva	k4xCgFnPc1
Búrské	búrský	k2eAgFnPc1d1
války	válka	k1gFnPc1
značně	značně	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
vývoj	vývoj	k1gInSc4
celého	celý	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1931	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Pretorie	Pretorie	k1gFnSc1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
stala	stát	k5eAaPmAgFnS
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
Pretorie	Pretorie	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Pretorie	Pretorie	k1gFnPc4
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
průmyslové	průmyslový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
zpracování	zpracování	k1gNnSc4
železa	železo	k1gNnSc2
a	a	k8xC
ocele	ocel	k1gFnSc2
a	a	k8xC
výroba	výroba	k1gFnSc1
automobilů	automobil	k1gInPc2
<g/>
,	,	kIx,
železnice	železnice	k1gFnSc2
a	a	k8xC
strojního	strojní	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Městské	městský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
čítá	čítat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
jeden	jeden	k4xCgInSc1
milion	milion	k4xCgInSc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
mluví	mluvit	k5eAaImIp3nS
jazykem	jazyk	k1gInSc7
Sepedi	Seped	k1gMnPc1
<g/>
,	,	kIx,
afrikánštinou	afrikánština	k1gFnSc7
a	a	k8xC
angličtinou	angličtina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Školství	školství	k1gNnSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
</s>
<s>
Pretorie	Pretorie	k1gFnSc1
je	být	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
akademických	akademický	k2eAgNnPc2d1
měst	město	k1gNnPc2
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
druhá	druhý	k4xOgFnSc1
největší	veliký	k2eAgFnSc1d3
univerzita	univerzita	k1gFnSc1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
zde	zde	k6eAd1
Pretorijská	pretorijský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
obsahující	obsahující	k2eAgFnSc4d1
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
významnou	významný	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
jihoafrického	jihoafrický	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sporty	sport	k1gInPc1
</s>
<s>
Populárním	populární	k2eAgInSc7d1
sportem	sport	k1gInSc7
je	být	k5eAaImIp3nS
ragby	ragby	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
hostila	hostit	k5eAaImAgFnS
Pretorie	Pretorie	k1gFnSc1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
ragby	ragby	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdejší	zdejší	k2eAgInSc1d1
tým	tým	k1gInSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
stadion	stadion	k1gInSc4
nazývaný	nazývaný	k2eAgMnSc1d1
Loftus	Loftus	k1gMnSc1
Versfeld	Versfeld	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Elon	Elon	k1gMnSc1
Musk	Musk	k1gMnSc1
-	-	kIx~
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
SpaceX	SpaceX	k1gMnSc1
</s>
<s>
Sesterská	sesterský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Ammán	Ammán	k1gInSc1
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Baku	Baku	k1gNnSc1
<g/>
,	,	kIx,
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Bukurešť	Bukurešť	k1gFnSc1
<g/>
,	,	kIx,
Romani	Roman	k1gMnPc1
</s>
<s>
Bulawayo	Bulawayo	k1gNnSc1
<g/>
,	,	kIx,
Zimbabwe	Zimbabwe	k1gNnSc1
</s>
<s>
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Port	port	k1gInSc1
Louis	louis	k1gInSc1
<g/>
,	,	kIx,
Mauritius	Mauritius	k1gInSc1
</s>
<s>
Teherán	Teherán	k1gInSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
</s>
<s>
Tchaj-pej	Tchaj-pat	k5eAaPmRp2nS,k5eAaImRp2nS
<g/>
,	,	kIx,
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BERÁNEK	Beránek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc4
českých	český	k2eAgInPc2d1
exonym	exonym	k1gInSc4
<g/>
:	:	kIx,
standardizované	standardizovaný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
varianty	varianta	k1gFnSc2
=	=	kIx~
List	list	k1gInSc1
of	of	k?
Czech	Czech	k1gInSc1
exonyms	exonyms	k1gInSc1
<g/>
:	:	kIx,
standardized	standardized	k1gInSc1
forms	forms	k1gInSc1
<g/>
,	,	kIx,
variants	variants	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rozš	rozš	k5eAaPmIp2nS
<g/>
.	.	kIx.
a	a	k8xC
aktualiz	aktualiz	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
133	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Geografické	geografický	k2eAgInPc1d1
názvoslovné	názvoslovný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
OSN	OSN	kA
-	-	kIx~
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86918	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
64	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardizované	standardizovaný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
:	:	kIx,
Pretorie	Pretorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pretorie	Pretorie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
Afriky	Afrika	k1gFnSc2
Nezávislé	závislý	k2eNgFnSc2d1
státy	stát	k1gInPc1
</s>
<s>
Abuja	Abuja	k1gFnSc1
(	(	kIx(
<g/>
Nigérie	Nigérie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Akkra	Akkra	k1gFnSc1
(	(	kIx(
<g/>
Ghana	Ghana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Addis	Addis	k1gInSc1
Abeba	Abeba	k1gFnSc1
(	(	kIx(
<g/>
Etiopie	Etiopie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Alžír	Alžír	k1gInSc1
(	(	kIx(
<g/>
Alžírsko	Alžírsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Antananarivo	Antananarivo	k1gNnSc1
(	(	kIx(
<g/>
Madagaskar	Madagaskar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Asmara	Asmara	k1gFnSc1
(	(	kIx(
<g/>
Eritrea	Eritrea	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bamako	Bamako	k1gNnSc1
(	(	kIx(
<g/>
Mali	Mali	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Bangui	Bangu	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Banjul	Banjul	k1gInSc1
(	(	kIx(
<g/>
Gambie	Gambie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bissau	Bissaus	k1gInSc2
(	(	kIx(
<g/>
Guinea-Bissau	Guinea-Bissaus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Brazzaville	Brazzaville	k1gInSc1
(	(	kIx(
<g/>
Konžská	konžský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Gitega	Gitega	k1gFnSc1
(	(	kIx(
<g/>
Burundi	Burundi	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Chartúm	Chartúm	k1gInSc1
(	(	kIx(
<g/>
Súdán	Súdán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Dakar	Dakar	k1gInSc1
(	(	kIx(
<g/>
Senegal	Senegal	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dodoma	Dodoma	k1gNnSc1
(	(	kIx(
<g/>
Tanzanie	Tanzanie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Džibuti	Džibuti	k1gNnSc1
(	(	kIx(
<g/>
Džibutsko	Džibutsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Džuba	Džuba	k1gFnSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Freetown	Freetown	k1gInSc1
(	(	kIx(
<g/>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
<g/>
)	)	kIx)
•	•	k?
Gaborone	Gaboron	k1gInSc5
(	(	kIx(
<g/>
Botswana	Botswana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Harare	Harar	k1gMnSc5
(	(	kIx(
<g/>
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Káhira	Káhira	k1gFnSc1
(	(	kIx(
<g/>
Egypt	Egypt	k1gInSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Kampala	Kampal	k1gMnSc2
(	(	kIx(
<g/>
Uganda	Uganda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kigali	Kigali	k1gFnSc1
(	(	kIx(
<g/>
Rwanda	Rwanda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kinshasa	Kinshasa	k1gFnSc1
(	(	kIx(
<g/>
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Konakry	Konakry	k1gNnSc1
(	(	kIx(
<g/>
Guinea	Guinea	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Libreville	Libreville	k1gInSc1
(	(	kIx(
<g/>
Gabon	Gabon	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Lilongwe	Lilongwe	k1gInSc1
(	(	kIx(
<g/>
Malawi	Malawi	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Lomé	Lomé	k1gNnSc1
(	(	kIx(
<g/>
Togo	Togo	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Luanda	Luanda	k1gFnSc1
(	(	kIx(
<g/>
Angola	Angola	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Lusaka	Lusak	k1gMnSc2
(	(	kIx(
<g/>
Zambie	Zambie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Malabo	Malaba	k1gFnSc5
(	(	kIx(
<g/>
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Maputo	Maput	k2eAgNnSc1d1
(	(	kIx(
<g/>
Mosambik	Mosambik	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Maseru	maser	k1gInSc2
(	(	kIx(
<g/>
Lesotho	Lesot	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Mbabane	Mbaban	k1gMnSc5
(	(	kIx(
<g/>
Svazijsko	Svazijsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Mogadišo	Mogadišo	k1gNnSc1
(	(	kIx(
<g/>
Somálsko	Somálsko	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Monrovia	Monrovia	k1gFnSc1
(	(	kIx(
<g/>
Libérie	Libérie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Moroni	Moroň	k1gFnSc6
(	(	kIx(
<g/>
Komory	komora	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
N	N	kA
<g/>
'	'	kIx"
<g/>
Djamena	Djamen	k2eAgFnSc1d1
(	(	kIx(
<g/>
Čad	Čad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Nairobi	Nairobi	k1gNnSc2
(	(	kIx(
<g/>
Keňa	Keňa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Niamey	Niamea	k1gFnSc2
(	(	kIx(
<g/>
Niger	Niger	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Nuakšott	Nuakšott	k1gInSc1
(	(	kIx(
<g/>
Mauritánie	Mauritánie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ouagadougou	Ouagadouga	k1gFnSc7
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Port	port	k1gInSc1
Louis	Louis	k1gMnSc1
(	(	kIx(
<g/>
Mauricius	Mauricius	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Porto	porto	k1gNnSc1
Novo	nova	k1gFnSc5
(	(	kIx(
<g/>
Benin	Benina	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Praia	Praius	k1gMnSc2
(	(	kIx(
<g/>
Kapverdy	Kapverda	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Pretorie	Pretorie	k1gFnSc1
(	(	kIx(
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rabat	rabat	k1gInSc1
(	(	kIx(
<g/>
Maroko	Maroko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Sã	Sã	k6eAd1
Tomé	Tomé	k1gNnSc1
(	(	kIx(
<g/>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Princův	princův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Tripolis	Tripolis	k1gInSc1
(	(	kIx(
<g/>
Libye	Libye	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tunis	Tunis	k1gInSc1
(	(	kIx(
<g/>
Tunisko	Tunisko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Victoria	Victorium	k1gNnSc2
(	(	kIx(
<g/>
Seychely	Seychely	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Windhoek	Windhoek	k1gInSc1
(	(	kIx(
<g/>
Namibie	Namibie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Yamoussoukro	Yamoussoukro	k1gNnSc1
(	(	kIx(
<g/>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Yaoundé	Yaoundý	k2eAgFnPc4d1
(	(	kIx(
<g/>
Kamerun	Kamerun	k1gInSc4
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azámořská	azámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Al-	Al-	k?
<g/>
´	´	k?
<g/>
Ajún	Ajún	k1gMnSc1
(	(	kIx(
<g/>
Západní	západní	k2eAgFnSc1d1
Sahara	Sahara	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jamestown	Jamestown	k1gInSc1
(	(	kIx(
<g/>
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mamoudzou	Mamoudza	k1gFnSc7
(	(	kIx(
<g/>
Mayotte	Mayott	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Saint-Denis	Saint-Denis	k1gInSc1
(	(	kIx(
<g/>
Réunion	Réunion	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
307213	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4116098-8	4116098-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79023000	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
167343013	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79023000	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
