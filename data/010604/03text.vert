<p>
<s>
Řapík	řapík	k1gInSc1	řapík
(	(	kIx(	(
<g/>
petiolus	petiolus	k1gInSc1	petiolus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stopkovitá	stopkovitý	k2eAgFnSc1d1	stopkovitý
část	část	k1gFnSc1	část
listu	list	k1gInSc2	list
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
čepel	čepel	k1gInSc4	čepel
listu	list	k1gInSc2	list
se	s	k7c7	s
stonkem	stonek	k1gInSc7	stonek
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
s	s	k7c7	s
řapíkem	řapík	k1gInSc7	řapík
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
řapíkatý	řapíkatý	k2eAgInSc1d1	řapíkatý
<g/>
,	,	kIx,	,
list	list	k1gInSc1	list
bez	bez	k7c2	bez
řapíku	řapík	k1gInSc2	řapík
je	být	k5eAaImIp3nS	být
přisedlý	přisedlý	k2eAgInSc1d1	přisedlý
<g/>
.	.	kIx.	.
</s>
<s>
Řapík	řapík	k1gInSc1	řapík
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mechanická	mechanický	k2eAgNnPc4d1	mechanické
pletiva	pletivo	k1gNnPc4	pletivo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
polohy	poloha	k1gFnSc2	poloha
listu	list	k1gInSc2	list
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
odpružení	odpružení	k1gNnSc2	odpružení
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zvýšeného	zvýšený	k2eAgNnSc2d1	zvýšené
mechanického	mechanický	k2eAgNnSc2d1	mechanické
namáhání	namáhání	k1gNnSc2	namáhání
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
nastavení	nastavení	k1gNnSc4	nastavení
do	do	k7c2	do
výhodnější	výhodný	k2eAgFnSc2d2	výhodnější
polohy	poloha	k1gFnSc2	poloha
vůči	vůči	k7c3	vůči
slunečnímu	sluneční	k2eAgInSc3d1	sluneční
svitu	svit	k1gInSc3	svit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
řapíku	řapík	k1gInSc6	řapík
jsou	být	k5eAaImIp3nP	být
vodivá	vodivý	k2eAgNnPc1d1	vodivé
pletiva	pletivo	k1gNnPc1	pletivo
pro	pro	k7c4	pro
transport	transport	k1gInSc4	transport
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
metabolických	metabolický	k2eAgInPc2d1	metabolický
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
spodních	spodní	k2eAgInPc2d1	spodní
listů	list	k1gInPc2	list
jsou	být	k5eAaImIp3nP	být
řapíky	řapík	k1gInPc1	řapík
většinou	většinou	k6eAd1	většinou
delší	dlouhý	k2eAgInPc1d2	delší
než	než	k8xS	než
u	u	k7c2	u
horních	horní	k2eAgInPc2d1	horní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
všechny	všechen	k3xTgInPc1	všechen
listy	list	k1gInPc1	list
mohou	moct	k5eAaImIp3nP	moct
lépe	dobře	k6eAd2	dobře
využívat	využívat	k5eAaPmF	využívat
světelnou	světelný	k2eAgFnSc4d1	světelná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Řapík	řapík	k1gInSc1	řapík
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
listovou	listový	k2eAgFnSc4d1	listová
čepel	čepel	k1gFnSc4	čepel
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
transpirační	transpirační	k2eAgInSc4d1	transpirační
a	a	k8xC	a
asimilující	asimilující	k2eAgInSc4d1	asimilující
listový	listový	k2eAgInSc4d1	listový
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
již	již	k6eAd1	již
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
klíčení	klíčení	k1gNnSc2	klíčení
semenáčků	semenáček	k1gInPc2	semenáček
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
hypokotylu	hypokotyl	k1gInSc2	hypokotyl
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
prodlužováním	prodlužování	k1gNnSc7	prodlužování
listového	listový	k2eAgInSc2d1	listový
meristému	meristém	k1gInSc2	meristém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
akácie	akácie	k1gFnPc1	akácie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řapík	řapík	k1gInSc1	řapík
listovitě	listovitě	k6eAd1	listovitě
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
a	a	k8xC	a
přebírá	přebírat	k5eAaImIp3nS	přebírat
asimilační	asimilační	k2eAgFnPc4d1	asimilační
funkce	funkce	k1gFnPc4	funkce
po	po	k7c6	po
redukovaných	redukovaný	k2eAgFnPc6d1	redukovaná
listových	listový	k2eAgFnPc6d1	listová
čepelích	čepel	k1gFnPc6	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
pěstována	pěstován	k2eAgFnSc1d1	pěstována
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
řapíky	řapík	k1gInPc1	řapík
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
reveň	reveň	k1gFnSc1	reveň
kadeřavá	kadeřavý	k2eAgFnSc1d1	kadeřavá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krytosemenných	krytosemenný	k2eAgFnPc2d1	krytosemenná
rostlin	rostlina	k1gFnPc2	rostlina
někdy	někdy	k6eAd1	někdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
ouškatý	ouškatý	k2eAgInSc1d1	ouškatý
řapík	řapík	k1gInSc1	řapík
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
báze	báze	k1gFnSc1	báze
řapíku	řapík	k1gInSc2	řapík
do	do	k7c2	do
párovitých	párovitý	k2eAgInPc2d1	párovitý
výrůstků	výrůstek	k1gInPc2	výrůstek
nazývaných	nazývaný	k2eAgNnPc2d1	nazývané
ouška	ouško	k1gNnSc2	ouško
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
přisedání	přisedání	k1gNnSc2	přisedání
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
u	u	k7c2	u
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
báze	báze	k1gFnSc2	báze
řapíku	řapík	k1gInSc2	řapík
silně	silně	k6eAd1	silně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
a	a	k8xC	a
objímá	objímat	k5eAaImIp3nS	objímat
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
útvar	útvar	k1gInSc1	útvar
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pochva	pochva	k1gFnSc1	pochva
a	a	k8xC	a
řapík	řapík	k1gInSc1	řapík
je	být	k5eAaImIp3nS	být
pochvatý	pochvatý	k2eAgInSc1d1	pochvatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
základních	základní	k2eAgInPc2d1	základní
tvarů	tvar	k1gInPc2	tvar
řapíku	řapík	k1gInSc2	řapík
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
oblý	oblý	k2eAgMnSc1d1	oblý
<g/>
,	,	kIx,	,
hranatý	hranatý	k2eAgMnSc1d1	hranatý
a	a	k8xC	a
žlábkovaný	žlábkovaný	k2eAgMnSc1d1	žlábkovaný
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
řapíky	řapík	k1gInPc4	řapík
i	i	k9	i
znatelně	znatelně	k6eAd1	znatelně
zploštělé	zploštělý	k2eAgNnSc1d1	zploštělé
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
nebo	nebo	k8xC	nebo
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
křídlaté	křídlatý	k2eAgFnPc1d1	křídlatá
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
podélně	podélně	k6eAd1	podélně
plochá	plochý	k2eAgNnPc1d1	ploché
tenká	tenký	k2eAgNnPc1d1	tenké
křídla	křídlo	k1gNnPc1	křídlo
(	(	kIx(	(
<g/>
lemy	lem	k1gInPc1	lem
<g/>
)	)	kIx)	)
a	a	k8xC	a
řapík	řapík	k1gInSc1	řapík
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
úzce	úzko	k6eAd1	úzko
křídlatý	křídlatý	k2eAgInSc1d1	křídlatý
-	-	kIx~	-
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
užší	úzký	k2eAgInPc1d2	užší
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
řapíku	řapík	k1gInSc2	řapík
</s>
</p>
<p>
<s>
široce	široko	k6eAd1	široko
křídlatý	křídlatý	k2eAgInSc1d1	křídlatý
-	-	kIx~	-
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
širší	široký	k2eAgInSc4d2	širší
než	než	k8xS	než
průměr	průměr	k1gInSc4	průměr
řapíku	řapík	k1gInSc2	řapík
</s>
</p>
<p>
<s>
trojkřídlý	trojkřídlý	k2eAgInSc1d1	trojkřídlý
-	-	kIx~	-
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
i	i	k8xC	i
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
řapíku	řapík	k1gInSc2	řapík
</s>
</p>
<p>
<s>
zvlněné	zvlněný	k2eAgFnSc2d1	zvlněná
křídlatý	křídlatý	k2eAgInSc4d1	křídlatý
-	-	kIx~	-
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
zvlněné	zvlněný	k2eAgFnPc1d1	zvlněná
zprohýbanáNěkteré	zprohýbanáNěkterý	k2eAgFnPc1d1	zprohýbanáNěkterý
vodní	vodní	k2eAgFnPc1d1	vodní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kotvice	kotvice	k1gFnSc1	kotvice
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
řapíky	řapík	k1gInPc1	řapík
nafouklé	nafouklý	k2eAgInPc1d1	nafouklý
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
rostlině	rostlina	k1gFnSc3	rostlina
udržovat	udržovat	k5eAaImF	udržovat
listy	list	k1gInPc4	list
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
popínavých	popínavý	k2eAgFnPc2d1	popínavá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
plaménku	plamének	k1gInSc2	plamének
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
řapíky	řapík	k1gInPc4	řapík
i	i	k9	i
jako	jako	k9	jako
úponky	úponek	k1gInPc1	úponek
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
rostlina	rostlina	k1gFnSc1	rostlina
přichycuje	přichycovat	k5eAaImIp3nS	přichycovat
k	k	k7c3	k
opoře	opora	k1gFnSc3	opora
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
mající	mající	k2eAgInPc4d1	mající
řapíky	řapík	k1gInPc4	řapík
extrémně	extrémně	k6eAd1	extrémně
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
rafie	rafie	k1gFnSc2	rafie
nebo	nebo	k8xC	nebo
banánovník	banánovník	k1gInSc1	banánovník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bývají	bývat	k5eAaImIp3nP	bývat
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
5	[number]	k4	5
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Řapíček	řapíček	k1gInSc1	řapíček
(	(	kIx(	(
<g/>
petiolulus	petiolulus	k1gInSc1	petiolulus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stopkovitá	stopkovitý	k2eAgFnSc1d1	stopkovitý
část	část	k1gFnSc1	část
lístku	lístek	k1gInSc2	lístek
v	v	k7c6	v
dlanitě	dlanitě	k6eAd1	dlanitě
složeném	složený	k2eAgInSc6d1	složený
nebo	nebo	k8xC	nebo
zpeřeném	zpeřený	k2eAgInSc6d1	zpeřený
listu	list	k1gInSc6	list
<g/>
.	.	kIx.	.
</s>
<s>
Řapíček	řapíček	k1gInSc1	řapíček
spojuje	spojovat	k5eAaImIp3nS	spojovat
čepel	čepel	k1gInSc4	čepel
lístku	lístek	k1gInSc2	lístek
s	s	k7c7	s
vřetenem	vřeteno	k1gNnSc7	vřeteno
(	(	kIx(	(
<g/>
osou	osý	k2eAgFnSc4d1	osá
<g/>
)	)	kIx)	)
složeného	složený	k2eAgInSc2d1	složený
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
s	s	k7c7	s
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
řapíkem	řapík	k1gInSc7	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
i	i	k8xC	i
vzhled	vzhled	k1gInSc1	vzhled
řapíčků	řapíček	k1gInPc2	řapíček
jsou	být	k5eAaImIp3nP	být
obdobné	obdobný	k2eAgInPc1d1	obdobný
jako	jako	k8xS	jako
u	u	k7c2	u
řapíků	řapík	k1gInPc2	řapík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Diderot	Diderot	k1gMnSc1	Diderot
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
8	[number]	k4	8
svazků	svazek	k1gInPc2	svazek
(	(	kIx(	(
<g/>
3859	[number]	k4	3859
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86613	[number]	k4	86613
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PECHAROVÁ	PECHAROVÁ	kA	PECHAROVÁ
<g/>
,	,	kIx,	,
Emilie	Emilie	k1gFnSc1	Emilie
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
botanika	botanika	k1gFnSc1	botanika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jihočeska	jihočesko	k1gNnSc2	jihočesko
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FUTÁK	FUTÁK	kA	FUTÁK
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
<g/>
.	.	kIx.	.
</s>
<s>
Flóra	Flóra	k1gFnSc1	Flóra
Slovenska	Slovensko	k1gNnSc2	Slovensko
I	I	kA	I
<g/>
:	:	kIx,	:
Stopka	stopka	k1gFnSc1	stopka
listu	list	k1gInSc2	list
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
VEDA	vést	k5eAaImSgMnS	vést
<g/>
,	,	kIx,	,
Vydavateľstvo	Vydavateľstvo	k1gNnSc1	Vydavateľstvo
Slovenskej	Slovenskej	k?	Slovenskej
akadémie	akadémie	k1gFnSc1	akadémie
vied	vied	k1gInSc1	vied
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
315	[number]	k4	315
<g/>
-	-	kIx~	-
<g/>
316	[number]	k4	316
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KUNA	Kuna	k1gMnSc1	Kuna
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
;	;	kIx,	;
KOŠŤÁL	Košťál	k1gMnSc1	Košťál
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
<g/>
.	.	kIx.	.
</s>
<s>
Terminologický	terminologický	k2eAgInSc1d1	terminologický
slovník	slovník	k1gInSc1	slovník
biológie	biológie	k1gFnSc2	biológie
rastlin	rastlina	k1gFnPc2	rastlina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Konštantína	Konštantín	k1gMnSc2	Konštantín
Filozofa	filozof	k1gMnSc2	filozof
v	v	k7c6	v
Nitre	Nitr	k1gInSc5	Nitr
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
4.11	[number]	k4	4.11
<g/>
.2015	.2015	k4	.2015
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Řapík	řapík	k1gInSc1	řapík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
