<s>
Danny	Danna	k1gFnPc1	Danna
Elfman	Elfman	k1gMnSc1	Elfman
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Daniel	Daniel	k1gMnSc1	Daniel
Robert	Robert	k1gMnSc1	Robert
Elfman	Elfman	k1gMnSc1	Elfman
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Bridget	Bridgeta	k1gFnPc2	Bridgeta
Fondová	fondový	k2eAgFnSc1d1	Fondová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Molly	Molla	k1gFnSc2	Molla
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
mladý	mladý	k1gMnSc1	mladý
projevil	projevit	k5eAaPmAgMnS	projevit
veliké	veliký	k2eAgNnSc4d1	veliké
nadání	nadání	k1gNnSc4	nadání
v	v	k7c6	v
komponování	komponování	k1gNnSc6	komponování
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Richardem	Richard	k1gMnSc7	Richard
se	se	k3xPyFc4	se
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
živili	živit	k5eAaImAgMnP	živit
koncertováním	koncertování	k1gNnSc7	koncertování
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
Oingo	Oingo	k1gMnSc1	Oingo
Boingo	Boingo	k1gMnSc1	Boingo
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
nemá	mít	k5eNaImIp3nS	mít
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
stylově	stylově	k6eAd1	stylově
nejzajímavějším	zajímavý	k2eAgMnPc3d3	nejzajímavější
skladatelům	skladatel	k1gMnPc3	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Timem	Tim	k1gMnSc7	Tim
Burtonem	Burton	k1gInSc7	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tvůrcem	tvůrce	k1gMnSc7	tvůrce
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
úvodní	úvodní	k2eAgFnSc3d1	úvodní
znělce	znělka	k1gFnSc3	znělka
kresleného	kreslený	k2eAgInSc2d1	kreslený
seriálu	seriál	k1gInSc2	seriál
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
a	a	k8xC	a
znělky	znělka	k1gFnPc1	znělka
k	k	k7c3	k
televiznímu	televizní	k2eAgInSc3d1	televizní
seriálu	seriál	k1gInSc3	seriál
Zoufalé	zoufalý	k2eAgFnSc2d1	zoufalá
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
složil	složit	k5eAaPmAgMnS	složit
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
loutkové	loutkový	k2eAgFnSc3d1	loutková
hře	hra	k1gFnSc3	hra
The	The	k1gFnSc2	The
Fortune	Fortun	k1gInSc5	Fortun
Teller	Teller	k1gInSc1	Teller
od	od	k7c2	od
Erika	Erik	k1gMnSc2	Erik
Sanka	Sanek	k1gMnSc2	Sanek
<g/>
.	.	kIx.	.
</s>
<s>
Simpsonovi	Simpson	k1gMnSc3	Simpson
Ukradené	ukradený	k2eAgFnSc2d1	ukradená
Vánoce	Vánoce	k1gFnPc1	Vánoce
Tima	Timus	k1gMnSc4	Timus
Burtona	Burton	k1gMnSc4	Burton
Střihoruký	Střihoruký	k2eAgMnSc1d1	Střihoruký
Edward	Edward	k1gMnSc1	Edward
Krasavec	krasavec	k1gMnSc1	krasavec
Beauty	Beaut	k2eAgFnPc4d1	Beaut
Návrat	návrat	k1gInSc1	návrat
Sommersbyho	Sommersby	k1gMnSc2	Sommersby
Dolores	Dolores	k1gMnSc1	Dolores
Claibornová	Claibornová	k1gFnSc1	Claibornová
Velká	velká	k1gFnSc1	velká
ryba	ryba	k1gFnSc1	ryba
Spy	Spy	k1gFnSc1	Spy
Kids	Kids	k1gInSc4	Kids
Přízraky	přízrak	k1gInPc1	přízrak
Planeta	planeta	k1gFnSc1	planeta
opic	opice	k1gFnPc2	opice
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Pee	Pee	k1gFnSc1	Pee
Weeho	Wee	k1gMnSc2	Wee
velké	velký	k2eAgNnSc4d1	velké
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
Beetlejuice	Beetlejuice	k1gFnSc2	Beetlejuice
Dobrý	dobrý	k2eAgInSc1d1	dobrý
Will	Will	k1gInSc1	Will
Hunting	Hunting	k1gInSc1	Hunting
Milk	Milk	k1gInSc1	Milk
Charlotina	Charlotina	k1gFnSc1	Charlotina
pavučinka	pavučinka	k1gFnSc1	pavučinka
Trilogie	trilogie	k1gFnSc2	trilogie
Spiderman	Spiderman	k1gMnSc1	Spiderman
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
-	-	kIx~	-
téma	téma	k1gNnSc7	téma
Hellboy	Hellboa	k1gFnSc2	Hellboa
2	[number]	k4	2
Terminator	Terminator	k1gInSc1	Terminator
Salvation	Salvation	k1gInSc4	Salvation
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Karlík	Karlík	k1gMnSc1	Karlík
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
Mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
nevěsta	nevěsta	k1gFnSc1	nevěsta
Tima	Timus	k1gMnSc2	Timus
Burtona	Burton	k1gMnSc2	Burton
Zažít	zažít	k5eAaPmF	zažít
Woodstock	Woodstock	k1gInSc4	Woodstock
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
Wanted	Wanted	k1gInSc1	Wanted
</s>
