<s>
Pulitzerova	Pulitzerův	k2eAgFnSc1d1	Pulitzerova
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
udělované	udělovaný	k2eAgNnSc1d1	udělované
prestižní	prestižní	k2eAgNnSc1d1	prestižní
americké	americký	k2eAgNnSc1d1	americké
žurnalistické	žurnalistický	k2eAgNnSc1d1	žurnalistické
a	a	k8xC	a
umělecké	umělecký	k2eAgNnSc1d1	umělecké
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
novinář	novinář	k1gMnSc1	novinář
maďarského	maďarský	k2eAgInSc2d1	maďarský
původu	původ	k1gInSc2	původ
Joseph	Joseph	k1gMnSc1	Joseph
Pulitzer	Pulitzer	k1gMnSc1	Pulitzer
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
udělována	udělovat	k5eAaImNgFnS	udělovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
21	[number]	k4	21
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
kategoriích	kategorie	k1gFnPc6	kategorie
obdrží	obdržet	k5eAaPmIp3nS	obdržet
laureát	laureát	k1gMnSc1	laureát
odměnu	odměna	k1gFnSc4	odměna
10	[number]	k4	10
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
obdrží	obdržet	k5eAaPmIp3nS	obdržet
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
udělována	udělovat	k5eAaImNgFnS	udělovat
za	za	k7c4	za
žurnalistiku	žurnalistika	k1gFnSc4	žurnalistika
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
hudební	hudební	k2eAgFnSc2d1	hudební
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
uděluje	udělovat	k5eAaImIp3nS	udělovat
Kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
beletrii	beletrie	k1gFnSc4	beletrie
je	být	k5eAaImIp3nS	být
udělována	udělovat	k5eAaImNgFnS	udělovat
americkým	americký	k2eAgMnPc3d1	americký
prozaikům	prozaik	k1gMnPc3	prozaik
(	(	kIx(	(
<g/>
dramatu	drama	k1gNnSc2	drama
je	být	k5eAaImIp3nS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
vlastní	vlastní	k2eAgFnSc1d1	vlastní
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
)	)	kIx)	)
a	a	k8xC	a
oceněné	oceněný	k2eAgNnSc1d1	oceněné
dílo	dílo	k1gNnSc1	dílo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
převážně	převážně	k6eAd1	převážně
zabývat	zabývat	k5eAaImF	zabývat
životem	život	k1gInSc7	život
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
1917	[number]	k4	1917
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1918	[number]	k4	1918
-	-	kIx~	-
Ernest	Ernest	k1gMnSc1	Ernest
Poole	Poole	k1gFnSc2	Poole
<g/>
,	,	kIx,	,
His	his	k1gNnSc2	his
Family	Famila	k1gFnSc2	Famila
1919	[number]	k4	1919
-	-	kIx~	-
Booth	Bootha	k1gFnPc2	Bootha
Tarkington	Tarkington	k1gInSc1	Tarkington
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Magnificent	Magnificent	k1gMnSc1	Magnificent
<g />
.	.	kIx.	.
</s>
<s>
Ambersons	Ambersons	k1gInSc1	Ambersons
1920	[number]	k4	1920
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1921	[number]	k4	1921
-	-	kIx~	-
Edith	Editha	k1gFnPc2	Editha
Wharton	Wharton	k1gInSc1	Wharton
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Age	Age	k1gFnSc2	Age
of	of	k?	of
Innocence	Innocence	k1gFnSc2	Innocence
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Věk	věk	k1gInSc1	věk
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
-	-	kIx~	-
Booth	Bootha	k1gFnPc2	Bootha
Tarkington	Tarkington	k1gInSc1	Tarkington
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
Adams	Adams	k1gInSc1	Adams
1923	[number]	k4	1923
-	-	kIx~	-
Willa	Willa	k1gFnSc1	Willa
Cather	Cathra	k1gFnPc2	Cathra
<g/>
,	,	kIx,	,
One	One	k1gFnPc2	One
of	of	k?	of
Ours	Ours	k1gInSc1	Ours
1924	[number]	k4	1924
-	-	kIx~	-
Margaret	Margareta	k1gFnPc2	Margareta
Wilson	Wilson	k1gInSc1	Wilson
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Able	Able	k1gInSc1	Able
McLaughlins	McLaughlins	k1gInSc1	McLaughlins
1925	[number]	k4	1925
-	-	kIx~	-
Edna	Edna	k1gMnSc1	Edna
Ferber	Ferber	k1gMnSc1	Ferber
<g/>
,	,	kIx,	,
So	So	kA	So
Big	Big	k1gFnSc1	Big
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jak	jak	k6eAd1	jak
veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
1926	[number]	k4	1926
-	-	kIx~	-
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
Lewis	Lewis	k1gInSc1	Lewis
<g/>
,	,	kIx,	,
Arrowsmith	Arrowsmith	k1gInSc1	Arrowsmith
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Arrowsmith	Arrowsmith	k1gMnSc1	Arrowsmith
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
1927	[number]	k4	1927
-	-	kIx~	-
Louis	Louis	k1gMnSc1	Louis
Bromfield	Bromfield	k1gMnSc1	Bromfield
<g/>
,	,	kIx,	,
Early	earl	k1gMnPc4	earl
Autumn	Autumna	k1gFnPc2	Autumna
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Časný	časný	k2eAgInSc4d1	časný
podzim	podzim	k1gInSc4	podzim
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
1928	[number]	k4	1928
-	-	kIx~	-
Thornton	Thornton	k1gInSc1	Thornton
Wilder	Wilder	k1gInSc1	Wilder
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Bridge	Bridg	k1gFnSc2	Bridg
of	of	k?	of
San	San	k1gFnSc2	San
Luis	Luisa	k1gFnPc2	Luisa
Rey	Rea	k1gFnSc2	Rea
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Most	most	k1gInSc1	most
Svatého	svatý	k2eAgMnSc2d1	svatý
Ludvíka	Ludvík	k1gMnSc2	Ludvík
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
1929	[number]	k4	1929
-	-	kIx~	-
Julia	Julius	k1gMnSc4	Julius
Peterkin	Peterkin	k2eAgInSc1d1	Peterkin
<g/>
,	,	kIx,	,
Scarlet	Scarlet	k1gInSc1	Scarlet
Sister	Sister	k1gInSc1	Sister
Mary	Mary	k1gFnSc1	Mary
1930	[number]	k4	1930
-	-	kIx~	-
Oliver	Olivra	k1gFnPc2	Olivra
Lafarge	Lafarge	k1gInSc1	Lafarge
<g/>
,	,	kIx,	,
Laughing	Laughing	k1gInSc1	Laughing
Boy	boa	k1gFnSc2	boa
1931	[number]	k4	1931
-	-	kIx~	-
Margaret	Margareta	k1gFnPc2	Margareta
Ayer	Ayer	k1gInSc1	Ayer
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Years	Years	k1gInSc1	Years
of	of	k?	of
Grace	Graec	k1gInSc2	Graec
1932	[number]	k4	1932
-	-	kIx~	-
Pearl	Pearl	k1gMnSc1	Pearl
S.	S.	kA	S.
Buck	Buck	k1gMnSc1	Buck
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Good	Good	k1gMnSc1	Good
Earth	Earth	k1gMnSc1	Earth
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
1933	[number]	k4	1933
-	-	kIx~	-
T.	T.	kA	T.
S.	S.	kA	S.
Stribling	Stribling	k1gInSc4	Stribling
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Store	Stor	k1gMnSc5	Stor
1934	[number]	k4	1934
-	-	kIx~	-
Caroline	Carolin	k1gInSc5	Carolin
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Lamb	Lamb	k1gMnSc1	Lamb
in	in	k?	in
His	his	k1gNnSc4	his
Bosom	Bosom	k1gInSc1	Bosom
1935	[number]	k4	1935
-	-	kIx~	-
Josephine	Josephin	k1gMnSc5	Josephin
Winslow	Winslow	k1gMnSc5	Winslow
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Now	Now	k?	Now
in	in	k?	in
November	November	k1gInSc1	November
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Teď	teď	k6eAd1	teď
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
1936	[number]	k4	1936
-	-	kIx~	-
Harold	Harold	k1gMnSc1	Harold
L.	L.	kA	L.
Davis	Davis	k1gFnPc1	Davis
<g/>
,	,	kIx,	,
Honey	Honea	k1gFnPc1	Honea
in	in	k?	in
the	the	k?	the
Horn	Horn	k1gMnSc1	Horn
1937	[number]	k4	1937
-	-	kIx~	-
Margaret	Margareta	k1gFnPc2	Margareta
Mitchell	Mitchell	k1gInSc1	Mitchell
<g/>
,	,	kIx,	,
Gone	Gone	k1gInSc1	Gone
With	With	k1gInSc1	With
the	the	k?	the
Wind	Wind	k1gInSc1	Wind
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jih	jih	k1gInSc1	jih
proti	proti	k7c3	proti
Severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
-	-	kIx~	-
John	John	k1gMnSc1	John
Phillips	Phillipsa	k1gFnPc2	Phillipsa
<g />
.	.	kIx.	.
</s>
<s>
Marquand	Marquand	k1gInSc1	Marquand
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Late	lat	k1gInSc5	lat
George	Georg	k1gMnSc2	Georg
Apley	Aplea	k1gFnSc2	Aplea
1939	[number]	k4	1939
-	-	kIx~	-
Marjorie	Marjorie	k1gFnSc1	Marjorie
Kinnan	Kinnan	k1gMnSc1	Kinnan
Rawlings	Rawlings	k1gInSc1	Rawlings
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Yearling	Yearling	k1gInSc1	Yearling
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dítě	dítě	k1gNnSc1	dítě
divočiny	divočina	k1gFnSc2	divočina
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
1940	[number]	k4	1940
-	-	kIx~	-
John	John	k1gMnSc1	John
Steinbeck	Steinbeck	k1gMnSc1	Steinbeck
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Grapes	Grapes	k1gMnSc1	Grapes
of	of	k?	of
Wrath	Wrath	k1gMnSc1	Wrath
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hrozny	hrozen	k1gInPc4	hrozen
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
1941	[number]	k4	1941
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
<g />
.	.	kIx.	.
</s>
<s>
udělena	udělen	k2eAgFnSc1d1	udělena
1942	[number]	k4	1942
-	-	kIx~	-
Ellen	Ellen	k2eAgInSc1d1	Ellen
Glasgow	Glasgow	k1gInSc1	Glasgow
<g/>
,	,	kIx,	,
In	In	k1gFnSc1	In
This	Thisa	k1gFnPc2	Thisa
Our	Our	k1gFnSc1	Our
Life	Life	k1gFnSc1	Life
1943	[number]	k4	1943
-	-	kIx~	-
Upton	Upton	k1gMnSc1	Upton
Sinclair	Sinclair	k1gMnSc1	Sinclair
<g/>
,	,	kIx,	,
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Teeth	Teetha	k1gFnPc2	Teetha
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dračí	dračí	k2eAgInPc4d1	dračí
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
1944	[number]	k4	1944
-	-	kIx~	-
Martin	Martin	k1gMnSc1	Martin
Flavin	Flavina	k1gFnPc2	Flavina
<g/>
,	,	kIx,	,
Journey	Journea	k1gFnSc2	Journea
in	in	k?	in
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Cesta	cesta	k1gFnSc1	cesta
tmou	tma	k1gFnSc7	tma
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
1945	[number]	k4	1945
-	-	kIx~	-
John	John	k1gMnSc1	John
Hersey	Hersea	k1gFnSc2	Hersea
<g/>
,	,	kIx,	,
A	a	k8xC	a
Bell	bell	k1gInSc1	bell
for	forum	k1gNnPc2	forum
Adano	Adano	k6eAd1	Adano
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zvon	zvon	k1gInSc1	zvon
pro	pro	k7c4	pro
Adano	Adano	k1gNnSc4	Adano
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1947	[number]	k4	1947
-	-	kIx~	-
Robert	Robert	k1gMnSc1	Robert
Penn	Penn	k1gMnSc1	Penn
Warren	Warrna	k1gFnPc2	Warrna
<g/>
,	,	kIx,	,
All	All	k1gFnPc2	All
the	the	k?	the
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
zbrojnoši	zbrojnoš	k1gMnPc1	zbrojnoš
<g />
.	.	kIx.	.
</s>
<s>
královi	králův	k2eAgMnPc1d1	králův
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
-	-	kIx~	-
James	James	k1gMnSc1	James
A.	A.	kA	A.
Michener	Michener	k1gMnSc1	Michener
<g/>
,	,	kIx,	,
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
the	the	k?	the
South	South	k1gMnSc1	South
Pacific	Pacific	k1gMnSc1	Pacific
1949	[number]	k4	1949
-	-	kIx~	-
James	James	k1gMnSc1	James
Gould	Goulda	k1gFnPc2	Goulda
Cozzens	Cozzens	k1gInSc1	Cozzens
<g/>
,	,	kIx,	,
Guard	Guard	k1gInSc1	Guard
of	of	k?	of
Honor	honor	k1gInSc1	honor
1950	[number]	k4	1950
-	-	kIx~	-
A.	A.	kA	A.
B.	B.	kA	B.
Guthrie	Guthrie	k1gFnSc1	Guthrie
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Way	Way	k1gMnSc1	Way
West	West	k1gMnSc1	West
1951	[number]	k4	1951
-	-	kIx~	-
Conrad	Conrada	k1gFnPc2	Conrada
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Town	Town	k1gInSc1	Town
1952	[number]	k4	1952
-	-	kIx~	-
Herman	Herman	k1gMnSc1	Herman
Wouk	Wouk	k1gMnSc1	Wouk
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Caine	Cain	k1gMnSc5	Cain
Mutiny	Mutin	k2eAgFnPc4d1	Mutin
1953	[number]	k4	1953
-	-	kIx~	-
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Old	Olda	k1gFnPc2	Olda
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Sea	Sea	k1gFnSc2	Sea
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1955	[number]	k4	1955
-	-	kIx~	-
William	William	k1gInSc1	William
Faulkner	Faulknra	k1gFnPc2	Faulknra
<g/>
,	,	kIx,	,
A	a	k8xC	a
Fable	Fable	k1gFnSc1	Fable
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Báj	báj	k1gFnSc1	báj
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
1956	[number]	k4	1956
-	-	kIx~	-
MacKinlay	MacKinlaa	k1gFnSc2	MacKinlaa
Kantor	Kantor	k1gMnSc1	Kantor
<g/>
,	,	kIx,	,
Andersonville	Andersonville	k1gFnSc1	Andersonville
1957	[number]	k4	1957
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1958	[number]	k4	1958
-	-	kIx~	-
James	James	k1gMnSc1	James
Agee	Age	k1gFnSc2	Age
<g/>
,	,	kIx,	,
A	a	k8xC	a
Death	Death	k1gInSc1	Death
In	In	k1gFnSc2	In
The	The	k1gFnSc2	The
Family	Famila	k1gFnSc2	Famila
1959	[number]	k4	1959
-	-	kIx~	-
Robert	Robert	k1gMnSc1	Robert
Lewis	Lewis	k1gFnSc2	Lewis
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Travels	Travels	k1gInSc1	Travels
of	of	k?	of
Jaimie	Jaimie	k1gFnSc1	Jaimie
McPheeters	McPheeters	k1gInSc1	McPheeters
1960	[number]	k4	1960
-	-	kIx~	-
Allen	allen	k1gInSc1	allen
Drury	Drura	k1gFnSc2	Drura
<g/>
,	,	kIx,	,
Advise	Advise	k1gFnSc2	Advise
and	and	k?	and
Consent	Consent	k1gInSc1	Consent
1961	[number]	k4	1961
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Harper	Harper	k1gInSc1	Harper
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
Kill	Kill	k1gMnSc1	Kill
a	a	k8xC	a
Mockingbird	Mockingbird	k1gMnSc1	Mockingbird
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jako	jako	k9	jako
zabít	zabít	k5eAaPmF	zabít
ptáčka	ptáček	k1gMnSc4	ptáček
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
-	-	kIx~	-
Edwin	Edwin	k1gInSc1	Edwin
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Edge	Edg	k1gFnSc2	Edg
of	of	k?	of
Sadness	Sadness	k1gInSc1	Sadness
1963	[number]	k4	1963
-	-	kIx~	-
William	William	k1gInSc1	William
Faulkner	Faulkner	k1gInSc1	Faulkner
,	,	kIx,	,
The	The	k1gFnSc1	The
Reivers	Reivers	k1gInSc1	Reivers
1964	[number]	k4	1964
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1965	[number]	k4	1965
-	-	kIx~	-
Shirley	Shirlea	k1gFnSc2	Shirlea
Ann	Ann	k1gFnSc2	Ann
<g />
.	.	kIx.	.
</s>
<s>
Grau	Grau	k5eAaPmIp1nS	Grau
<g/>
,	,	kIx,	,
The	The	k1gMnPc1	The
Keepers	Keepersa	k1gFnPc2	Keepersa
Of	Of	k1gFnSc2	Of
The	The	k1gFnPc1	The
House	house	k1gNnSc1	house
1966	[number]	k4	1966
-	-	kIx~	-
Katherine	Katherin	k1gInSc5	Katherin
Anne	Anne	k1gFnPc7	Anne
Porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
Collected	Collected	k1gInSc1	Collected
Stories	Stories	k1gInSc1	Stories
1967	[number]	k4	1967
-	-	kIx~	-
Bernard	Bernard	k1gMnSc1	Bernard
Malamud	Malamud	k1gMnSc1	Malamud
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Fixer	Fixer	k1gMnSc1	Fixer
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Správkař	správkař	k1gMnSc1	správkař
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
-	-	kIx~	-
William	William	k1gInSc1	William
Styron	Styron	k1gInSc1	Styron
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Confessions	Confessionsa	k1gFnPc2	Confessionsa
of	of	k?	of
Nat	Nat	k1gMnSc1	Nat
Turner	turner	k1gMnSc1	turner
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Doznání	doznání	k1gNnSc1	doznání
Nata	Nat	k1gInSc2	Nat
<g />
.	.	kIx.	.
</s>
<s>
Turnera	turner	k1gMnSc2	turner
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
-	-	kIx~	-
N.	N.	kA	N.
Scott	Scott	k1gMnSc1	Scott
Momaday	Momadaa	k1gMnSc2	Momadaa
<g/>
,	,	kIx,	,
House	house	k1gNnSc1	house
Made	Mad	k1gInSc2	Mad
of	of	k?	of
Dawn	Dawn	k1gInSc1	Dawn
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dům	dům	k1gInSc1	dům
z	z	k7c2	z
úsvitu	úsvit	k1gInSc2	úsvit
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
386	[number]	k4	386
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
-	-	kIx~	-
Jean	Jean	k1gMnSc1	Jean
Stafford	Stafford	k1gMnSc1	Stafford
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Collected	Collected	k1gInSc1	Collected
Stories	Stories	k1gInSc1	Stories
1971	[number]	k4	1971
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1972	[number]	k4	1972
-	-	kIx~	-
Wallace	Wallace	k1gFnSc1	Wallace
Stegner	Stegner	k1gInSc1	Stegner
<g/>
,	,	kIx,	,
Angle	Angl	k1gMnSc5	Angl
of	of	k?	of
Repose	Reposa	k1gFnSc6	Reposa
1973	[number]	k4	1973
-	-	kIx~	-
Eudora	Eudora	k1gFnSc1	Eudora
Welty	Welta	k1gFnSc2	Welta
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Optimists	Optimistsa	k1gFnPc2	Optimistsa
Daughter	Daughter	k1gMnSc1	Daughter
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Optimistova	optimistův	k2eAgFnSc1d1	optimistův
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1975	[number]	k4	1975
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Shaara	Shaara	k1gFnSc1	Shaara
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Killer	Killer	k1gMnSc1	Killer
Angels	Angels	k1gInSc1	Angels
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
česky	česky	k6eAd1	česky
Andělé	anděl	k1gMnPc1	anděl
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
-	-	kIx~	-
Saul	Saul	k1gMnSc1	Saul
Bellow	Bellow	k1gMnSc1	Bellow
<g/>
,	,	kIx,	,
Humboldt	Humboldt	k1gMnSc1	Humboldt
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gift	Gifta	k1gFnPc2	Gifta
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
1978	[number]	k4	1978
-	-	kIx~	-
James	James	k1gMnSc1	James
Alan	Alan	k1gMnSc1	Alan
McPherson	McPherson	k1gMnSc1	McPherson
<g/>
,	,	kIx,	,
Elbow	Elbow	k1gMnSc1	Elbow
Room	Room	k1gMnSc1	Room
1979	[number]	k4	1979
-	-	kIx~	-
John	John	k1gMnSc1	John
Cheever	Cheever	k1gMnSc1	Cheever
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
The	The	k1gMnSc1	The
Stories	Stories	k1gMnSc1	Stories
of	of	k?	of
John	John	k1gMnSc1	John
Cheever	Cheever	k1gMnSc1	Cheever
1980	[number]	k4	1980
-	-	kIx~	-
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Executioner	Executioner	k1gMnSc1	Executioner
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Song	song	k1gInSc1	song
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Katova	katův	k2eAgFnSc1d1	Katova
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
-	-	kIx~	-
John	John	k1gMnSc1	John
Kennedy	Kenneda	k1gMnSc2	Kenneda
Toole	Toole	k1gNnSc2	Toole
<g/>
,	,	kIx,	,
A	a	k9	a
Confederacy	Confederacy	k1gInPc1	Confederacy
of	of	k?	of
Dunces	Dunces	k1gInSc1	Dunces
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Spolčení	spolčení	k1gNnSc1	spolčení
hlupců	hlupec	k1gMnPc2	hlupec
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85794	[number]	k4	85794
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
-	-	kIx~	-
John	John	k1gMnSc1	John
Updike	Updik	k1gMnSc2	Updik
<g/>
,	,	kIx,	,
Rabbit	Rabbit	k1gMnSc2	Rabbit
Is	Is	k1gMnSc2	Is
Rich	Rich	k1gMnSc1	Rich
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Králík	Králík	k1gMnSc1	Králík
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
-	-	kIx~	-
Alice	Alice	k1gFnSc1	Alice
Walker	Walker	k1gMnSc1	Walker
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Color	Color	k1gMnSc1	Color
Purple	Purple	k1gMnSc1	Purple
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g />
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
nachu	nach	k1gInSc2	nach
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
340	[number]	k4	340
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
-	-	kIx~	-
William	William	k1gInSc1	William
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
Ironweed	Ironweed	k1gInSc1	Ironweed
1985	[number]	k4	1985
-	-	kIx~	-
Alison	Alison	k1gNnSc1	Alison
Lurie	Lurie	k1gFnSc1	Lurie
<g/>
,	,	kIx,	,
Foreign	Foreign	k1gInSc1	Foreign
Affairs	Affairs	k1gInSc1	Affairs
1986	[number]	k4	1986
-	-	kIx~	-
Larry	Larra	k1gFnSc2	Larra
McMurtry	McMurtr	k1gInPc4	McMurtr
<g/>
,	,	kIx,	,
Lonesome	Lonesom	k1gInSc5	Lonesom
Dove	Dove	k1gNnPc6	Dove
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Osamělá	osamělý	k2eAgFnSc1d1	osamělá
<g />
.	.	kIx.	.
</s>
<s>
holubice	holubice	k1gFnSc1	holubice
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85395	[number]	k4	85395
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
A	a	k8xC	a
Summons	Summons	k1gInSc4	Summons
to	ten	k3xDgNnSc1	ten
Memphis	Memphis	k1gFnSc1	Memphis
1988	[number]	k4	1988
-	-	kIx~	-
Toni	Toni	k1gMnSc1	Toni
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
,	,	kIx,	,
Beloved	Beloved	k1gMnSc1	Beloved
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Milovaná	milovaný	k2eAgFnSc1d1	milovaná
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Anne	Annat	k5eAaPmIp3nS	Annat
Tyler	Tyler	k1gInSc1	Tyler
<g/>
,	,	kIx,	,
Breathing	Breathing	k1gInSc1	Breathing
Lessons	Lessons	k1gInSc1	Lessons
1990	[number]	k4	1990
-	-	kIx~	-
Oscar	Oscar	k1gMnSc1	Oscar
Hijuelos	Hijuelos	k1gMnSc1	Hijuelos
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Mambo	Mamba	k1gMnSc5	Mamba
Kings	Kings	k1gInSc1	Kings
Play	play	k0	play
Songs	Songs	k1gInSc1	Songs
of	of	k?	of
Love	lov	k1gInSc5	lov
1991	[number]	k4	1991
-	-	kIx~	-
John	John	k1gMnSc1	John
Updike	Updik	k1gMnSc2	Updik
<g/>
,	,	kIx,	,
Rabbit	Rabbit	k1gMnSc2	Rabbit
At	At	k1gMnSc2	At
Rest	rest	k6eAd1	rest
1992	[number]	k4	1992
-	-	kIx~	-
Art	Art	k1gMnSc1	Art
Spiegelman	Spiegelman	k1gMnSc1	Spiegelman
<g/>
,	,	kIx,	,
Maus	Maus	k1gInSc1	Maus
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Maus	Maus	k1gInSc1	Maus
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
-	-	kIx~	-
Robert	Robert	k1gMnSc1	Robert
Olen	Olen	k1gMnSc1	Olen
Butler	Butler	k1gMnSc1	Butler
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
A	a	k9	a
Good	Good	k1gMnSc1	Good
Scent	Scent	k1gMnSc1	Scent
from	from	k1gMnSc1	from
a	a	k8xC	a
Strange	Strange	k1gFnSc1	Strange
Mountain	Mountain	k1gInSc1	Mountain
1994	[number]	k4	1994
-	-	kIx~	-
E.	E.	kA	E.
Annie	Annie	k1gFnSc1	Annie
Proulx	Proulx	k1gInSc1	Proulx
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Shipping	Shipping	k1gInSc1	Shipping
News	News	k1gInSc1	News
1995	[number]	k4	1995
-	-	kIx~	-
Carol	Carola	k1gFnPc2	Carola
Shields	Shields	k1gInSc1	Shields
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Stone	ston	k1gInSc5	ston
Diaries	Diaries	k1gInSc4	Diaries
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Deníky	deník	k1gInPc1	deník
tesané	tesaný	k2eAgInPc1d1	tesaný
do	do	k7c2	do
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
-	-	kIx~	-
Richard	Richarda	k1gFnPc2	Richarda
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
Independence	Independence	k1gFnSc1	Independence
Day	Day	k1gFnSc1	Day
1997	[number]	k4	1997
-	-	kIx~	-
Steven	Stevna	k1gFnPc2	Stevna
<g />
.	.	kIx.	.
</s>
<s>
Millhauser	Millhauser	k1gMnSc1	Millhauser
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Dressler	Dressler	k1gMnSc1	Dressler
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Tale	Tal	k1gFnSc2	Tal
of	of	k?	of
an	an	k?	an
American	American	k1gInSc1	American
Dreamer	Dreamer	k1gInSc1	Dreamer
1998	[number]	k4	1998
-	-	kIx~	-
Philip	Philip	k1gMnSc1	Philip
Roth	Roth	k1gMnSc1	Roth
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Pastoral	Pastoral	k1gMnSc1	Pastoral
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Americká	americký	k2eAgFnSc1d1	americká
idyla	idyla	k1gFnSc1	idyla
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
-	-	kIx~	-
Michael	Michaela	k1gFnPc2	Michaela
Cunningham	Cunningham	k1gInSc1	Cunningham
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Hours	Hoursa	k1gFnPc2	Hoursa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1112	[number]	k4	1112
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
-	-	kIx~	-
Jhumpa	Jhumpa	k1gFnSc1	Jhumpa
Lahiriová	Lahiriový	k2eAgFnSc1d1	Lahiriová
<g/>
,	,	kIx,	,
Interpreter	interpreter	k1gInSc1	interpreter
of	of	k?	of
Maladies	Maladies	k1gInSc1	Maladies
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Tlumočník	tlumočník	k1gMnSc1	tlumočník
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
-	-	kIx~	-
Michael	Michael	k1gMnSc1	Michael
Chabon	Chabon	k1gMnSc1	Chabon
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Kavalier	Kavalier	k1gMnSc1	Kavalier
&	&	k?	&
Clay	Claa	k1gFnSc2	Claa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Úžasná	úžasný	k2eAgFnSc1d1	úžasná
<g />
.	.	kIx.	.
</s>
<s>
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
Kavaliera	Kavaliero	k1gNnSc2	Kavaliero
a	a	k8xC	a
Claye	Clay	k1gFnSc2	Clay
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1162	[number]	k4	1162
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
-	-	kIx~	-
Richard	Richard	k1gMnSc1	Richard
Russo	Russa	k1gFnSc5	Russa
<g/>
,	,	kIx,	,
Empire	empir	k1gInSc5	empir
Falls	Falls	k1gInSc4	Falls
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zánik	zánik	k1gInSc1	zánik
Empire	empir	k1gInSc5	empir
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
406	[number]	k4	406
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
Eugenides	Eugenides	k1gInSc1	Eugenides
<g/>
,	,	kIx,	,
Middlesex	Middlesex	k1gInSc1	Middlesex
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hermafrodit	hermafrodit	k1gMnSc1	hermafrodit
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7381	[number]	k4	7381
<g/>
-	-	kIx~	-
<g/>
526	[number]	k4	526
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
P.	P.	kA	P.
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Known	Known	k1gMnSc1	Known
World	World	k1gMnSc1	World
2005	[number]	k4	2005
-	-	kIx~	-
Marilynne	Marilynn	k1gInSc5	Marilynn
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Gilead	Gilead	k1gInSc1	Gilead
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Gilead	Gilead	k1gInSc1	Gilead
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7335	[number]	k4	7335
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
-	-	kIx~	-
Geraldine	Geraldin	k1gInSc5	Geraldin
Brooksová	Brooksový	k2eAgFnSc1d1	Brooksová
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
March	March	k1gInSc1	March
2007	[number]	k4	2007
-	-	kIx~	-
Cormac	Cormac	k1gFnSc1	Cormac
McCarthy	McCartha	k1gFnSc2	McCartha
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Road	Road	k1gMnSc1	Road
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
973	[number]	k4	973
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Junot	Junot	k1gMnSc1	Junot
Díaz	Díaz	k1gMnSc1	Díaz
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Brief	Brief	k1gMnSc1	Brief
Wondrous	Wondrous	k1gMnSc1	Wondrous
Life	Lif	k1gFnSc2	Lif
of	of	k?	of
Oscar	Oscar	k1gMnSc1	Oscar
<g />
.	.	kIx.	.
</s>
<s>
Wao	Wao	k?	Wao
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
divuplný	divuplný	k2eAgInSc1d1	divuplný
život	život	k1gInSc1	život
Oskara	Oskar	k1gMnSc2	Oskar
Wajda	Wajd	k1gMnSc2	Wajd
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
155	[number]	k4	155
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Stroutová	Stroutový	k2eAgFnSc1d1	Stroutová
<g/>
,	,	kIx,	,
Olive	Oliev	k1gFnPc1	Oliev
Kitteridge	Kitteridg	k1gInSc2	Kitteridg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g />
.	.	kIx.	.
</s>
<s>
Olive	Olivat	k5eAaPmIp3nS	Olivat
Kitteridgeová	Kitteridgeová	k1gFnSc1	Kitteridgeová
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
754	[number]	k4	754
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Paul	Paula	k1gFnPc2	Paula
Harding	Harding	k1gInSc1	Harding
<g/>
,	,	kIx,	,
Tinkers	Tinkers	k1gInSc1	Tinkers
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Tuláci	tulák	k1gMnPc1	tulák
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
978-80-207-1395-7	[number]	k4	978-80-207-1395-7
2011	[number]	k4	2011
-	-	kIx~	-
Jennifer	Jennifer	k1gInSc1	Jennifer
<g />
.	.	kIx.	.
</s>
<s>
Eganová	Eganová	k1gFnSc1	Eganová
<g/>
,	,	kIx,	,
A	a	k9	a
Visit	visita	k1gFnPc2	visita
from	from	k1gMnSc1	from
the	the	k?	the
Goon	Goon	k1gInSc1	Goon
Squad	Squad	k1gInSc1	Squad
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Návštěva	návštěva	k1gFnSc1	návštěva
bandy	banda	k1gFnSc2	banda
rváčů	rváč	k1gMnPc2	rváč
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1425	[number]	k4	1425
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
2013	[number]	k4	2013
-	-	kIx~	-
Adam	Adam	k1gMnSc1	Adam
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
The	The	k1gMnSc1	The
Orphan	Orphan	k1gMnSc1	Orphan
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Son	son	k1gInSc1	son
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Syn	syn	k1gMnSc1	syn
správce	správce	k1gMnSc2	správce
sirotčince	sirotčinec	k1gInSc2	sirotčinec
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7321	[number]	k4	7321
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
-	-	kIx~	-
Donna	donna	k1gFnSc1	donna
Tartt	Tartt	k1gMnSc1	Tartt
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Goldfinch	Goldfinch	k1gMnSc1	Goldfinch
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
česky	česky	k6eAd1	česky
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g/>
,	,	kIx,	,
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
1603	[number]	k4	1603
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
-	-	kIx~	-
Anthony	Anthona	k1gFnSc2	Anthona
Doerr	Doerr	k1gMnSc1	Doerr
<g/>
,	,	kIx,	,
All	All	k1gMnSc1	All
the	the	k?	the
Light	Light	k1gMnSc1	Light
We	We	k1gMnSc1	We
Cannot	Cannota	k1gFnPc2	Cannota
See	See	k1gMnSc1	See
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jsou	být	k5eAaImIp3nP	být
světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nevidíme	vidět	k5eNaImIp1nP	vidět
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
243	[number]	k4	243
<g/>
-	-	kIx~	-
<g/>
6760	[number]	k4	6760
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
-	-	kIx~	-
Viet	Viet	k2eAgInSc1d1	Viet
Thanh	Thanh	k1gInSc1	Thanh
Nguyen	Nguyna	k1gFnPc2	Nguyna
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Sympathizer	Sympathizra	k1gFnPc2	Sympathizra
Součástí	součást	k1gFnSc7	součást
Pulitzerovy	Pulitzerův	k2eAgFnPc1d1	Pulitzerova
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
fotografická	fotografický	k2eAgNnPc1d1	fotografické
ocenění	ocenění	k1gNnPc1	ocenění
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
udělují	udělovat	k5eAaImIp3nP	udělovat
za	za	k7c4	za
významný	významný	k2eAgInSc4d1	významný
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
černobílé	černobílý	k2eAgFnSc6d1	černobílá
nebo	nebo	k8xC	nebo
barevné	barevný	k2eAgFnSc6d1	barevná
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
kategorie	kategorie	k1gFnSc1	kategorie
-	-	kIx~	-
Pulitzer	Pulitzer	k1gInSc1	Pulitzer
Prize	Prize	k1gFnSc1	Prize
for	forum	k1gNnPc2	forum
Photography	Photographa	k1gFnSc2	Photographa
-	-	kIx~	-
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
spot	spot	k1gInSc4	spot
news	ws	k6eNd1	ws
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
breaking	breaking	k1gInSc1	breaking
news	news	k1gInSc4	news
<g/>
)	)	kIx)	)
a	a	k8xC	a
feature	featur	k1gMnSc5	featur
photography	photograph	k1gInPc1	photograph
<g/>
.	.	kIx.	.
</s>
<s>
Pulitzerova	Pulitzerův	k2eAgFnSc1d1	Pulitzerova
komise	komise	k1gFnSc1	komise
s	s	k7c7	s
oceněním	ocenění	k1gNnSc7	ocenění
vydává	vydávat	k5eAaImIp3nS	vydávat
také	také	k9	také
oficiální	oficiální	k2eAgFnSc4d1	oficiální
zprávu	zpráva	k1gFnSc4	zpráva
vysvětlující	vysvětlující	k2eAgInPc1d1	vysvětlující
důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
udělení	udělení	k1gNnSc4	udělení
<g/>
.	.	kIx.	.
</s>
