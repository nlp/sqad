<p>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
filantrop	filantrop	k1gMnSc1	filantrop
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
jihoafrické	jihoafrický	k2eAgNnSc4d1	jihoafrické
<g/>
,	,	kIx,	,
kanadské	kanadský	k2eAgNnSc4d1	kanadské
a	a	k8xC	a
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Spoluvlastnil	spoluvlastnit	k5eAaImAgInS	spoluvlastnit
internetový	internetový	k2eAgInSc1d1	internetový
platební	platební	k2eAgInSc1d1	platební
systém	systém	k1gInSc1	systém
PayPal	PayPal	k1gInSc1	PayPal
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
společnost	společnost	k1gFnSc4	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
investorem	investor	k1gMnSc7	investor
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
CEO	CEO	kA	CEO
vede	vést	k5eAaImIp3nS	vést
automobilku	automobilka	k1gFnSc4	automobilka
Tesla	Tesla	k1gFnSc1	Tesla
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
společnosti	společnost	k1gFnSc2	společnost
SolarCity	SolarCita	k1gFnSc2	SolarCita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
zabývá	zabývat	k5eAaImIp3nS	zabývat
instalací	instalace	k1gFnSc7	instalace
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
pro	pro	k7c4	pro
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
Musk	Musk	k1gInSc1	Musk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotné	samotný	k2eAgNnSc1d1	samotné
provedení	provedení	k1gNnSc4	provedení
přenechal	přenechat	k5eAaPmAgInS	přenechat
svému	svůj	k3xOyFgMnSc3	svůj
bratranci	bratranec	k1gMnSc3	bratranec
Lyndonu	Lyndon	k1gMnSc3	Lyndon
Riveovi	Riveus	k1gMnSc3	Riveus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
také	také	k9	také
ředitelem	ředitel	k1gMnSc7	ředitel
SolarCity	SolarCita	k1gFnSc2	SolarCita
<g/>
.	.	kIx.	.
</s>
<s>
Musk	Musk	k1gInSc1	Musk
má	mít	k5eAaImIp3nS	mít
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
obchod	obchod	k1gInSc4	obchod
z	z	k7c2	z
The	The	k1gMnPc2	The
Wharton	Wharton	k1gInSc4	Wharton
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
<g/>
,	,	kIx,	,
School	School	k1gInSc1	School
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Sciences	Sciences	k1gInSc1	Sciences
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
,	,	kIx,	,
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
a	a	k8xC	a
zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
i	i	k8xC	i
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
2	[number]	k4	2
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Machete	Mache	k1gNnSc2	Mache
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
<g/>
,	,	kIx,	,
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouříte	kouřit	k5eAaImIp2nP	kouřit
</s>
</p>
<p>
<s>
==	==	k?	==
Názory	názor	k1gInPc1	názor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Musk	Musk	k6eAd1	Musk
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
života	život	k1gInSc2	život
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
planetách	planeta	k1gFnPc6	planeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
"	"	kIx"	"
<g/>
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
existuje	existovat	k5eAaImIp3nS	existovat
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
život	život	k1gInSc1	život
ve	v	k7c6	v
známém	známý	k2eAgInSc6d1	známý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
své	svůj	k3xOyFgFnPc4	svůj
"	"	kIx"	"
<g/>
naděje	naděje	k1gFnPc1	naděje
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
život	život	k1gInSc1	život
ve	v	k7c6	v
známém	známý	k2eAgInSc6d1	známý
vesmíru	vesmír	k1gInSc6	vesmír
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
"	"	kIx"	"
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
pravděpodobnější	pravděpodobný	k2eAgInSc4d2	Pravděpodobnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
odhad	odhad	k1gInSc1	odhad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
===	===	k?	===
</s>
</p>
<p>
<s>
Musk	Musk	k6eAd1	Musk
často	často	k6eAd1	často
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
potenciálním	potenciální	k2eAgNnSc6d1	potenciální
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
(	(	kIx(	(
<g/>
AI	AI	kA	AI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejvážnější	vážní	k2eAgFnSc4d3	nejvážnější
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
lidské	lidský	k2eAgFnSc2d1	lidská
rasy	rasa	k1gFnSc2	rasa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
interview	interview	k1gNnSc2	interview
na	na	k7c6	na
sympoziu	sympozion	k1gNnSc6	sympozion
MIT	MIT	kA	MIT
Musk	Musk	k1gMnSc1	Musk
popsal	popsat	k5eAaPmAgMnS	popsat
AI	AI	kA	AI
jako	jako	k9	jako
"	"	kIx"	"
<g/>
největší	veliký	k2eAgFnSc4d3	veliký
existenční	existenční	k2eAgFnSc4d1	existenční
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
"	"	kIx"	"
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
"	"	kIx"	"
<g/>
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
si	se	k3xPyFc3	se
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
existovat	existovat	k5eAaImF	existovat
nějaká	nějaký	k3yIgFnSc1	nějaký
regulační	regulační	k2eAgFnSc1d1	regulační
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
,	,	kIx,	,
možná	možná	k9	možná
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
neuděláme	udělat	k5eNaPmIp1nP	udělat
nějakou	nějaký	k3yIgFnSc4	nějaký
hloupost	hloupost	k1gFnSc4	hloupost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Musk	Musk	k1gMnSc1	Musk
popsal	popsat	k5eAaPmAgMnS	popsat
vytvoření	vytvoření	k1gNnSc4	vytvoření
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vyvolání	vyvolání	k1gNnSc1	vyvolání
démona	démon	k1gMnSc2	démon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
Musk	Musk	k1gMnSc1	Musk
investoval	investovat	k5eAaBmAgInS	investovat
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
vyvíjející	vyvíjející	k2eAgFnSc2d1	vyvíjející
AI	AI	kA	AI
DeepMind	DeepMind	k1gMnSc1	DeepMind
a	a	k8xC	a
Vicarious	Vicarious	k1gMnSc1	Vicarious
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
společnost	společnost	k1gFnSc1	společnost
pracující	pracující	k2eAgFnSc1d1	pracující
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
inteligence	inteligence	k1gFnSc2	inteligence
strojů	stroj	k1gInPc2	stroj
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
organizaci	organizace	k1gFnSc4	organizace
OpenAI	OpenAI	k1gFnSc2	OpenAI
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Musk	Musk	k1gMnSc1	Musk
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
investice	investice	k1gFnPc1	investice
"	"	kIx"	"
<g/>
nebyly	být	k5eNaImAgFnP	být
provedeny	provést	k5eAaPmNgInP	provést
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jejich	jejich	k3xOp3gFnSc2	jejich
návratnosti	návratnost	k1gFnSc2	návratnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
dohledu	dohled	k1gInSc3	dohled
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Terminátor	terminátor	k1gInSc4	terminátor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
události	událost	k1gFnPc1	událost
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
děsivě	děsivě	k6eAd1	děsivě
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
my	my	k3xPp1nPc1	my
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
postarat	postarat	k5eAaPmF	postarat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
dobré	dobrý	k2eAgFnPc1d1	dobrá
<g/>
,	,	kIx,	,
ne	ne	k9	ne
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
Met	met	k1gInSc1	met
Gala	gala	k2eAgMnSc1d1	gala
po	po	k7c6	po
boku	bok	k1gInSc6	bok
kanadské	kanadský	k2eAgFnSc2d1	kanadská
umělkyně	umělkyně	k1gFnSc2	umělkyně
Grimes	Grimes	k1gMnSc1	Grimes
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spolu	spolu	k6eAd1	spolu
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Grimes	Grimes	k1gInSc1	Grimes
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
na	na	k7c6	na
SpaceX	SpaceX	k1gFnPc6	SpaceX
konferencích	konference	k1gFnPc6	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VANCE	VANCE	kA	VANCE
<g/>
,	,	kIx,	,
Ashlee	Ashlee	k1gNnSc2	Ashlee
<g/>
.	.	kIx.	.
</s>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
,	,	kIx,	,
SpaceX	SpaceX	k1gFnSc1	SpaceX
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
fantastické	fantastický	k2eAgFnSc2d1	fantastická
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Melvil	Melvil	k1gFnSc2	Melvil
publishing	publishing	k1gInSc1	publishing
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
368	[number]	k4	368
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87270	[number]	k4	87270
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Elon	Elona	k1gFnPc2	Elona
Musk	Musko	k1gNnPc2	Musko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Elon	Elono	k1gNnPc2	Elono
Musk	Musko	k1gNnPc2	Musko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
<g/>
:	:	kIx,	:
Myšlenkový	myšlenkový	k2eAgMnSc1d1	myšlenkový
otec	otec	k1gMnSc1	otec
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
,	,	kIx,	,
SpaceX	SpaceX	k1gFnSc2	SpaceX
<g/>
,	,	kIx,	,
SolarCity	SolarCita	k1gFnSc2	SolarCita
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
TED	Ted	k1gMnSc1	Ted
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
</s>
</p>
