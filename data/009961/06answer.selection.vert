<s>
Spoluvlastnil	spoluvlastnit	k5eAaImAgInS	spoluvlastnit
internetový	internetový	k2eAgInSc1d1	internetový
platební	platební	k2eAgInSc1d1	platební
systém	systém	k1gInSc1	systém
PayPal	PayPal	k1gInSc1	PayPal
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
společnost	společnost	k1gFnSc4	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
investorem	investor	k1gMnSc7	investor
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
CEO	CEO	kA	CEO
vede	vést	k5eAaImIp3nS	vést
automobilku	automobilka	k1gFnSc4	automobilka
Tesla	Tesla	k1gFnSc1	Tesla
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
