<s>
Čest	čest	k1gFnSc1	čest
(	(	kIx(	(
<g/>
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
ctíti	ctít	k5eAaImF	ctít
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vlastnost	vlastnost	k1gFnSc1	vlastnost
bytosti	bytost	k1gFnSc2	bytost
(	(	kIx(	(
<g/>
entity	entita	k1gFnSc2	entita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
morální	morální	k2eAgInSc4d1	morální
kredit	kredit	k1gInSc4	kredit
<g/>
,	,	kIx,	,
vážnost	vážnost	k1gFnSc4	vážnost
<g/>
,	,	kIx,	,
hodnověrnost	hodnověrnost	k1gFnSc4	hodnověrnost
nebo	nebo	k8xC	nebo
dobré	dobrý	k2eAgNnSc4d1	dobré
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Čest	čest	k1gFnSc4	čest
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
potenciál	potenciál	k1gInSc4	potenciál
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
důvěry	důvěra	k1gFnSc2	důvěra
nebo	nebo	k8xC	nebo
projevu	projev	k1gInSc2	projev
úcty	úcta	k1gFnSc2	úcta
<g/>
;	;	kIx,	;
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
ctí	ctít	k5eAaImIp3nS	ctít
tato	tento	k3xDgFnSc1	tento
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Čest	čest	k1gFnSc1	čest
je	být	k5eAaImIp3nS	být
budována	budovat	k5eAaImNgFnS	budovat
a	a	k8xC	a
udržována	udržovat	k5eAaImNgFnS	udržovat
příkladným	příkladný	k2eAgNnSc7d1	příkladné
plněním	plnění	k1gNnSc7	plnění
závazků	závazek	k1gInPc2	závazek
<g/>
,	,	kIx,	,
vzdáváním	vzdávání	k1gNnSc7	vzdávání
se	se	k3xPyFc4	se
vlastních	vlastní	k2eAgInPc2d1	vlastní
požitků	požitek	k1gInPc2	požitek
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
vystavení	vystavení	k1gNnSc4	vystavení
riziku	riziko	k1gNnSc3	riziko
v	v	k7c4	v
nějaký	nějaký	k3yIgInSc4	nějaký
prospěch	prospěch	k1gInSc4	prospěch
jiného	jiné	k1gNnSc2	jiné
<g/>
,	,	kIx,	,
pracovním	pracovní	k2eAgNnSc7d1	pracovní
nasazením	nasazení	k1gNnSc7	nasazení
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
takovým	takový	k3xDgNnSc7	takový
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
objektivně	objektivně	k6eAd1	objektivně
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xC	jako
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc4	pokles
nebo	nebo	k8xC	nebo
ztrátu	ztráta	k1gFnSc4	ztráta
cti	čest	k1gFnSc2	čest
lze	lze	k6eAd1	lze
naopak	naopak	k6eAd1	naopak
očekávat	očekávat	k5eAaImF	očekávat
od	od	k7c2	od
podvodu	podvod	k1gInSc2	podvod
<g/>
,	,	kIx,	,
zrady	zrada	k1gFnSc2	zrada
nebo	nebo	k8xC	nebo
zbabělosti	zbabělost	k1gFnSc2	zbabělost
<g/>
.	.	kIx.	.
</s>
<s>
Čest	čest	k1gFnSc1	čest
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
základní	základní	k2eAgInPc4d1	základní
lidské	lidský	k2eAgInPc4d1	lidský
pojmy	pojem	k1gInPc4	pojem
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
definovat	definovat	k5eAaBmF	definovat
<g/>
:	:	kIx,	:
Websterův	Websterův	k2eAgInSc1d1	Websterův
slovník	slovník	k1gInSc1	slovník
uvádí	uvádět	k5eAaImIp3nS	uvádět
osm	osm	k4xCc1	osm
různých	různý	k2eAgInPc2d1	různý
významových	významový	k2eAgInPc2d1	významový
odstínů	odstín	k1gInPc2	odstín
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Čest	čest	k1gFnSc1	čest
je	být	k5eAaImIp3nS	být
–	–	k?	–
čest	čest	k1gFnSc1	čest
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
společnostech	společnost	k1gFnPc6	společnost
převládalo	převládat	k5eAaImAgNnS	převládat
pojetí	pojetí	k1gNnSc1	pojetí
cti	čest	k1gFnSc2	čest
jako	jako	k8xC	jako
ocenění	ocenění	k1gNnSc2	ocenění
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
druhých	druhý	k4xOgFnPc2	druhý
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc1	čest
jako	jako	k8xS	jako
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
hodnost	hodnost	k1gFnSc1	hodnost
nebo	nebo	k8xC	nebo
pocta	pocta	k1gFnSc1	pocta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupnou	postupný	k2eAgFnSc7d1	postupná
individualizací	individualizace	k1gFnSc7	individualizace
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
spíše	spíše	k9	spíše
čest	čest	k1gFnSc4	čest
jako	jako	k8xS	jako
ocenění	ocenění	k1gNnSc4	ocenění
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
rys	rys	k1gInSc1	rys
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
stydět	stydět	k5eAaImF	stydět
sám	sám	k3xTgMnSc1	sám
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dává	dávat	k5eAaImIp3nS	dávat
člověku	člověk	k1gMnSc3	člověk
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
čest	čest	k1gFnSc4	čest
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Machiavelli	Machiavelli	k1gMnSc1	Machiavelli
<g/>
)	)	kIx)	)
V	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
společnostech	společnost	k1gFnPc6	společnost
se	se	k3xPyFc4	se
vysoko	vysoko	k6eAd1	vysoko
cenila	cenit	k5eAaImAgFnS	cenit
statečnost	statečnost	k1gFnSc1	statečnost
a	a	k8xC	a
odvaha	odvaha	k1gFnSc1	odvaha
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
utekl	utéct	k5eAaPmAgMnS	utéct
před	před	k7c7	před
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
I	i	k9	i
když	když	k8xS	když
už	už	k6eAd1	už
mi	já	k3xPp1nSc3	já
zbývá	zbývat	k5eAaImIp3nS	zbývat
jediná	jediný	k2eAgFnSc1d1	jediná
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
ostřelování	ostřelování	k1gNnSc6	ostřelování
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
i	i	k8xC	i
já	já	k3xPp1nSc1	já
dáváme	dávat	k5eAaImIp1nP	dávat
totiž	totiž	k9	totiž
přednost	přednost	k1gFnSc4	přednost
cti	čest	k1gFnSc2	čest
bez	bez	k7c2	bez
lodí	loď	k1gFnPc2	loď
před	před	k7c7	před
loděmi	loď	k1gFnPc7	loď
beze	beze	k7c2	beze
cti	čest	k1gFnSc2	čest
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Naopak	naopak	k6eAd1	naopak
vysoké	vysoký	k2eAgNnSc4d1	vysoké
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
moc	moc	k1gFnSc4	moc
nebo	nebo	k8xC	nebo
štědrost	štědrost	k1gFnSc4	štědrost
přinášely	přinášet	k5eAaImAgInP	přinášet
s	s	k7c7	s
sebou	se	k3xPyFc7	se
úctu	úcta	k1gFnSc4	úcta
druhých	druhý	k4xOgInPc2	druhý
<g/>
,	,	kIx,	,
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
a	a	k8xC	a
pocty	pocta	k1gFnPc4	pocta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
čest	čest	k1gFnSc1	čest
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
"	"	kIx"	"
<g/>
čestné	čestný	k2eAgFnSc3d1	čestná
funkci	funkce	k1gFnSc3	funkce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
čest	čest	k1gFnSc4	čest
svému	svůj	k3xOyFgMnSc3	svůj
nositeli	nositel	k1gMnSc3	nositel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
čestném	čestný	k2eAgNnSc6d1	čestné
předsednictví	předsednictví	k1gNnSc6	předsednictví
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
přináší	přinášet	k5eAaImIp3nS	přinášet
čest	čest	k1gFnSc1	čest
firmě	firma	k1gFnSc3	firma
či	či	k8xC	či
konferenci	konference	k1gFnSc6	konference
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
"	"	kIx"	"
<g/>
vojenské	vojenský	k2eAgFnSc3d1	vojenská
cti	čest	k1gFnSc3	čest
<g/>
"	"	kIx"	"
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
vzdávají	vzdávat	k5eAaImIp3nP	vzdávat
poctu	pocta	k1gFnSc4	pocta
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povoláních	povolání	k1gNnPc6	povolání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
životech	život	k1gInPc6	život
druhých	druhý	k4xOgMnPc2	druhý
jako	jako	k8xS	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
nebo	nebo	k8xC	nebo
kde	kde	k6eAd1	kde
musíme	muset	k5eAaImIp1nP	muset
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
někomu	někdo	k3yInSc3	někdo
svěřit	svěřit	k5eAaPmF	svěřit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kapitánovi	kapitán	k1gMnSc3	kapitán
lodi	loď	k1gFnSc2	loď
nebo	nebo	k8xC	nebo
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
očekáváme	očekávat	k5eAaImIp1nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
řídí	řídit	k5eAaImIp3nP	řídit
"	"	kIx"	"
<g/>
etikou	etika	k1gFnSc7	etika
cti	čest	k1gFnSc2	čest
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
že	že	k8xS	že
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
ručí	ručit	k5eAaImIp3nS	ručit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ctí	čest	k1gFnSc7	čest
i	i	k8xC	i
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
kapitán	kapitán	k1gMnSc1	kapitán
opouští	opouštět	k5eAaImIp3nS	opouštět
tonoucí	tonoucí	k2eAgFnSc4d1	tonoucí
loď	loď	k1gFnSc4	loď
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc4d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
člověka	člověk	k1gMnSc2	člověk
někdo	někdo	k3yInSc1	někdo
dotkl	dotknout	k5eAaPmAgMnS	dotknout
<g/>
,	,	kIx,	,
poškodil	poškodit	k5eAaPmAgInS	poškodit
jeho	jeho	k3xOp3gFnSc4	jeho
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc4	čest
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
hájit	hájit	k5eAaImF	hájit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
protivníka	protivník	k1gMnSc4	protivník
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Smysl	smysl	k1gInSc1	smysl
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
dával	dávat	k5eAaImAgInS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
něčeho	něco	k3yInSc2	něco
váží	vážit	k5eAaImIp3nS	vážit
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
se	s	k7c7	s
"	"	kIx"	"
<g/>
ctí	čest	k1gFnSc7	čest
<g/>
"	"	kIx"	"
často	často	k6eAd1	často
rozumělo	rozumět	k5eAaImAgNnS	rozumět
zachování	zachování	k1gNnSc3	zachování
věrnosti	věrnost	k1gFnSc2	věrnost
nebo	nebo	k8xC	nebo
panenství	panenství	k1gNnSc2	panenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
společnosti	společnost	k1gFnSc6	společnost
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
cti	čest	k1gFnSc6	čest
buďto	buďto	k8xC	buďto
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
někdo	někdo	k3yInSc1	někdo
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
za	za	k7c4	za
něco	něco	k3yInSc4	něco
zaručujeme	zaručovat	k5eAaImIp1nP	zaručovat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
osobou	osoba	k1gFnSc7	osoba
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
ctí	ctít	k5eAaImIp3nS	ctít
–	–	k?	–
například	například	k6eAd1	například
při	při	k7c6	při
přísaze	přísaha	k1gFnSc6	přísaha
a	a	k8xC	a
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
slibu	slib	k1gInSc6	slib
<g/>
.	.	kIx.	.
</s>
<s>
Pojmy	pojem	k1gInPc1	pojem
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
čestný	čestný	k2eAgMnSc1d1	čestný
(	(	kIx(	(
<g/>
nečestný	čestný	k2eNgMnSc1d1	nečestný
<g/>
)	)	kIx)	)
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
čestné	čestný	k2eAgNnSc1d1	čestné
slovo	slovo	k1gNnSc1	slovo
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
užívají	užívat	k5eAaImIp3nP	užívat
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
představou	představa	k1gFnSc7	představa
cti	čest	k1gFnSc2	čest
souvisí	souviset	k5eAaImIp3nS	souviset
také	také	k9	také
pocty	pocta	k1gFnPc4	pocta
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
</s>
