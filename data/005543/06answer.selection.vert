<s>
Čest	čest	k1gFnSc1	čest
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
základní	základní	k2eAgInPc4d1	základní
lidské	lidský	k2eAgInPc4d1	lidský
pojmy	pojem	k1gInPc4	pojem
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
definovat	definovat	k5eAaBmF	definovat
<g/>
:	:	kIx,	:
Websterův	Websterův	k2eAgInSc1d1	Websterův
slovník	slovník	k1gInSc1	slovník
uvádí	uvádět	k5eAaImIp3nS	uvádět
osm	osm	k4xCc1	osm
různých	různý	k2eAgInPc2d1	různý
významových	významový	k2eAgInPc2d1	významový
odstínů	odstín	k1gInPc2	odstín
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
