<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
(	(	kIx(	(
<g/>
Octopoda	Octopoda	k1gMnSc1	Octopoda
Leach	Leach	k1gMnSc1	Leach
<g/>
,	,	kIx,	,
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
řádem	řád	k1gInSc7	řád
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
mnoho	mnoho	k4c4	mnoho
rozličných	rozličný	k2eAgFnPc2d1	rozličná
částí	část	k1gFnPc2	část
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
korálové	korálový	k2eAgInPc4d1	korálový
útesy	útes	k1gInPc4	útes
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
osmi	osm	k4xCc2	osm
rameny	rameno	k1gNnPc7	rameno
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
přísavkami	přísavka	k1gFnPc7	přísavka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
útrobní	útrobní	k2eAgInSc1d1	útrobní
vak	vak	k1gInSc1	vak
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
ploutvovitého	ploutvovitý	k2eAgInSc2d1	ploutvovitý
lemu	lem	k1gInSc2	lem
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
lezením	lezení	k1gNnSc7	lezení
a	a	k8xC	a
plaváním	plavání	k1gNnSc7	plavání
<g/>
.	.	kIx.	.
podřád	podřád	k1gInSc1	podřád
Cirrina	Cirrino	k1gNnSc2	Cirrino
Grimpe	Grimp	k1gInSc5	Grimp
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
čeleď	čeleď	k1gFnSc1	čeleď
Cirroteuthidae	Cirroteuthidae	k1gNnSc2	Cirroteuthidae
Keferstein	Kefersteina	k1gFnPc2	Kefersteina
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
čeleď	čeleď	k1gFnSc1	čeleď
Grimpoteuthididae	Grimpoteuthididae	k1gFnSc1	Grimpoteuthididae
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Shea	Shea	k1gMnSc1	Shea
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
čeleď	čeleď	k1gFnSc1	čeleď
Luteuthididae	Luteuthididae	k1gFnSc1	Luteuthididae
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Shea	Shea	k1gMnSc1	Shea
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
čeleď	čeleď	k1gFnSc1	čeleď
Opisthoteuthidae	Opisthoteuthidae	k1gNnSc2	Opisthoteuthidae
Verrill	Verrilla	k1gFnPc2	Verrilla
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
čeleď	čeleď	k1gFnSc1	čeleď
Stauroteuthidae	Stauroteuthidaus	k1gMnSc5	Stauroteuthidaus
Grimpe	Grimp	k1gMnSc5	Grimp
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1916	[number]	k4	1916
podřád	podřád	k1gInSc1	podřád
Incirrina	Incirrin	k2eAgInSc2d1	Incirrin
Grimpe	Grimp	k1gInSc5	Grimp
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
čeleď	čeleď	k1gFnSc1	čeleď
Alloposidae	Alloposidae	k1gNnSc2	Alloposidae
Verill	Verilla	k1gFnPc2	Verilla
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
čeleď	čeleď	k1gFnSc1	čeleď
Amphitretidae	Amphitretidae	k1gFnSc1	Amphitretidae
Hoyle	Hoyle	k1gFnSc1	Hoyle
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
čeleď	čeleď	k1gFnSc1	čeleď
Argonautidae	Argonautidae	k1gNnSc2	Argonautidae
Tryon	Tryona	k1gFnPc2	Tryona
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
–	–	k?	–
argonautovití	argonautovitý	k2eAgMnPc1d1	argonautovitý
čeleď	čeleď	k1gFnSc4	čeleď
Bolitaenidae	Bolitaenidae	k1gNnSc4	Bolitaenidae
Chun	Chuna	k1gFnPc2	Chuna
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
čeleď	čeleď	k1gFnSc1	čeleď
Idioctopodidae	Idioctopodida	k1gFnSc2	Idioctopodida
Taki	Tak	k1gFnSc2	Tak
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
čeleď	čeleď	k1gFnSc1	čeleď
Octopodidae	Octopodidae	k1gNnSc2	Octopodidae
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Orbigny	Orbigna	k1gMnSc2	Orbigna
<g/>
,	,	kIx,	,
1840	[number]	k4	1840
–	–	k?	–
chobotnicovití	chobotnicovitý	k2eAgMnPc1d1	chobotnicovitý
čeleď	čeleď	k1gFnSc1	čeleď
Ocythoidae	Ocythoida	k1gMnSc2	Ocythoida
Gray	Graa	k1gMnSc2	Graa
<g/>
,	,	kIx,	,
1849	[number]	k4	1849
čeleď	čeleď	k1gFnSc1	čeleď
Palaeoctopodidae	Palaeoctopodida	k1gFnSc2	Palaeoctopodida
Dollo	Dollo	k1gNnSc1	Dollo
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
†	†	k?	†
čeleď	čeleď	k1gFnSc1	čeleď
Tremoctopodidae	Tremoctopodida	k1gFnSc2	Tremoctopodida
Tryon	Tryona	k1gFnPc2	Tryona
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
čeleď	čeleď	k1gFnSc1	čeleď
Vitreledonellidae	Vitreledonellidae	k1gNnSc2	Vitreledonellidae
Robson	Robsona	k1gFnPc2	Robsona
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
Chobotnice	chobotnice	k1gFnSc1	chobotnice
dokáže	dokázat	k5eAaPmIp3nS	dokázat
měnit	měnit	k5eAaImF	měnit
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
strukturu	struktura	k1gFnSc4	struktura
své	svůj	k3xOyFgFnSc2	svůj
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
také	také	k9	také
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
inkoust	inkoust	k1gInSc4	inkoust
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mate	mást	k5eAaImIp3nS	mást
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
testech	test	k1gInPc6	test
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
chobotnice	chobotnice	k1gFnSc1	chobotnice
zbarvit	zbarvit	k5eAaPmF	zbarvit
do	do	k7c2	do
šachovnice	šachovnice	k1gFnSc2	šachovnice
i	i	k8xC	i
puntíků	puntík	k1gInPc2	puntík
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
chránit	chránit	k5eAaImF	chránit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
napodobení	napodobení	k1gNnSc1	napodobení
různých	různý	k2eAgNnPc2d1	různé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Batesovské	Batesovský	k2eAgNnSc4d1	Batesovský
mimikry	mimikry	k1gNnSc4	mimikry
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
dokáže	dokázat	k5eAaPmIp3nS	dokázat
napodobit	napodobit	k5eAaPmF	napodobit
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
platýse	platýs	k1gMnSc5	platýs
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
i	i	k8xC	i
nebezpečné	bezpečný	k2eNgMnPc4d1	nebezpečný
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
přeplavat	přeplavat	k5eAaPmF	přeplavat
přes	přes	k7c4	přes
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chobotnice	chobotnice	k1gFnSc2	chobotnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chobotnice	chobotnice	k1gFnSc2	chobotnice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc4	taxon
Octopoda	Octopod	k1gMnSc4	Octopod
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Octopoda	Octopoda	k1gFnSc1	Octopoda
(	(	kIx(	(
<g/>
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
