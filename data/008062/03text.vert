<s>
Jaro	jaro	k1gNnSc1	jaro
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
vesna	vesna	k1gFnSc1	vesna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
začátkem	začátkem	k7c2	začátkem
vegetativní	vegetativní	k2eAgFnSc2d1	vegetativní
aktivity	aktivita	k1gFnSc2	aktivita
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
aktivity	aktivita	k1gFnSc2	aktivita
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
se	se	k3xPyFc4	se
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologické	meteorologický	k2eAgNnSc1d1	meteorologické
jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jarními	jarní	k2eAgInPc7d1	jarní
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
březen	březen	k1gInSc4	březen
<g/>
,	,	kIx,	,
duben	duben	k1gInSc4	duben
a	a	k8xC	a
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
Astronomické	astronomický	k2eAgNnSc1d1	astronomické
jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
jarní	jarní	k2eAgFnSc7d1	jarní
rovnodenností	rovnodennost	k1gFnSc7	rovnodennost
(	(	kIx(	(
<g/>
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
zpravidla	zpravidla	k6eAd1	zpravidla
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
na	na	k7c4	na
jižní	jižní	k2eAgNnSc4d1	jižní
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
letním	letní	k2eAgInSc7d1	letní
slunovratem	slunovrat	k1gInSc7	slunovrat
(	(	kIx(	(
<g/>
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
zpravidla	zpravidla	k6eAd1	zpravidla
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
na	na	k7c4	na
jižní	jižní	k2eAgNnSc4d1	jižní
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termíny	termín	k1gInPc1	termín
počátku	počátek	k1gInSc2	počátek
a	a	k8xC	a
konce	konec	k1gInSc2	konec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
posunuty	posunut	k2eAgFnPc1d1	posunuta
kvůli	kvůli	k7c3	kvůli
nepravidelnostem	nepravidelnost	k1gFnPc3	nepravidelnost
souvisejícím	související	k2eAgFnPc3d1	související
s	s	k7c7	s
přestupnými	přestupný	k2eAgInPc7d1	přestupný
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
roku	rok	k1gInSc2	rok
na	na	k7c4	na
jaro	jaro	k1gNnSc4	jaro
<g/>
,	,	kIx,	,
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
zimu	zima	k1gFnSc4	zima
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
vidět	vidět	k5eAaImF	vidět
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mírného	mírný	k2eAgInSc2d1	mírný
a	a	k8xC	a
subarktického	subarktický	k2eAgInSc2d1	subarktický
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tropů	trop	k1gInPc2	trop
roční	roční	k2eAgFnSc2d1	roční
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rok	rok	k1gInSc1	rok
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc4	období
–	–	k?	–
období	období	k1gNnSc2	období
dešťů	dešť	k1gInPc2	dešť
a	a	k8xC	a
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
severního	severní	k2eAgInSc2d1	severní
a	a	k8xC	a
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
zimní	zimní	k2eAgNnSc1d1	zimní
počasí	počasí	k1gNnSc1	počasí
(	(	kIx(	(
<g/>
hrají	hrát	k5eAaImIp3nP	hrát
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
