<s>
Kyberpunk	Kyberpunk	k1gInSc1	Kyberpunk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
cyberpunk	cyberpunk	k6eAd1	cyberpunk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podžánr	podžánr	k1gMnSc1	podžánr
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
(	(	kIx(	(
<g/>
vlivný	vlivný	k2eAgMnSc1d1	vlivný
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hrají	hrát	k5eAaImIp3nP	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
