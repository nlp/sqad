<s>
Kyberpunk	Kyberpunk	k1gInSc1	Kyberpunk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
cyberpunk	cyberpunk	k6eAd1	cyberpunk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podžánr	podžánr	k1gMnSc1	podžánr
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
(	(	kIx(	(
<g/>
vlivný	vlivný	k2eAgMnSc1d1	vlivný
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hrají	hrát	k5eAaImIp3nP	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
námětem	námět	k1gInSc7	námět
činnost	činnost	k1gFnSc1	činnost
hackerů	hacker	k1gMnPc2	hacker
pohybujících	pohybující	k2eAgMnPc2d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
undergroundovém	undergroundový	k2eAgNnSc6d1	undergroundové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
dystopického	dystopický	k2eAgInSc2d1	dystopický
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
ovládají	ovládat	k5eAaImIp3nP	ovládat
mamutí	mamutí	k2eAgFnSc1d1	mamutí
nadnárodní	nadnárodní	k2eAgFnSc1d1	nadnárodní
korporace	korporace	k1gFnSc1	korporace
a	a	k8xC	a
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Zápletka	zápletka	k1gFnSc1	zápletka
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
detektivní	detektivní	k2eAgFnSc2d1	detektivní
drsné	drsný	k2eAgFnPc4d1	drsná
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kyberpunk	Kyberpunk	k6eAd1	Kyberpunk
dále	daleko	k6eAd2	daleko
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
nihilistický	nihilistický	k2eAgInSc1d1	nihilistický
nádech	nádech	k1gInSc1	nádech
s	s	k7c7	s
dotekem	dotek	k1gInSc7	dotek
japonské	japonský	k2eAgFnSc2d1	japonská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
všudypřítomné	všudypřítomný	k2eAgFnPc4d1	všudypřítomná
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
s	s	k7c7	s
cynickým	cynický	k2eAgInSc7d1	cynický
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
kyberimplantáty	kyberimplantát	k1gInPc7	kyberimplantát
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
ponořený	ponořený	k2eAgInSc4d1	ponořený
v	v	k7c6	v
kyberprostoru	kyberprostor	k1gInSc6	kyberprostor
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
aspoň	aspoň	k9	aspoň
zčásti	zčásti	k6eAd1	zčásti
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
kyberprostoru	kyberprostor	k1gInSc6	kyberprostor
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
témata	téma	k1gNnPc4	téma
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
současného	současný	k2eAgInSc2d1	současný
života	život	k1gInSc2	život
a	a	k8xC	a
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
přílišné	přílišný	k2eAgFnSc2d1	přílišná
abstrakce	abstrakce	k1gFnSc2	abstrakce
a	a	k8xC	a
představivosti	představivost	k1gFnSc2	představivost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
čtenář	čtenář	k1gMnSc1	čtenář
s	s	k7c7	s
hrdiny	hrdina	k1gMnPc7	hrdina
mohl	moct	k5eAaImAgMnS	moct
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
nadnárodní	nadnárodní	k2eAgFnPc1d1	nadnárodní
korporace	korporace	k1gFnPc1	korporace
známé	známý	k2eAgFnPc1d1	známá
v	v	k7c6	v
čase	čas	k1gInSc6	čas
napsání	napsání	k1gNnSc2	napsání
díla	dílo	k1gNnPc4	dílo
ještě	ještě	k9	ještě
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
se	s	k7c7	s
staršími	starý	k2eAgFnPc7d2	starší
postavami	postava	k1gFnPc7	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
predikcí	predikce	k1gFnPc2	predikce
na	na	k7c6	na
základě	základ	k1gInSc6	základ
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nejsou	být	k5eNaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
děje	děj	k1gInSc2	děj
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
bytosti	bytost	k1gFnSc2	bytost
či	či	k8xC	či
jejich	jejich	k3xOp3gInSc1	jejich
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
obdařenými	obdařený	k2eAgInPc7d1	obdařený
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
roboty	robot	k1gInPc7	robot
<g/>
,	,	kIx,	,
androidy	android	k1gInPc7	android
<g/>
,	,	kIx,	,
kyborgy	kyborg	k1gMnPc7	kyborg
anebo	anebo	k8xC	anebo
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
s	s	k7c7	s
implantáty	implantát	k1gInPc7	implantát
fyzické	fyzický	k2eAgFnSc2d1	fyzická
i	i	k8xC	i
mentální	mentální	k2eAgFnSc2d1	mentální
povahy	povaha	k1gFnSc2	povaha
(	(	kIx(	(
<g/>
rozšířená	rozšířený	k2eAgNnPc4d1	rozšířené
realita	realita	k1gFnSc1	realita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
virtuálními	virtuální	k2eAgFnPc7d1	virtuální
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
avatary	avatar	k1gInPc7	avatar
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Protagonistou	protagonista	k1gMnSc7	protagonista
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
morálně	morálně	k6eAd1	morálně
nejednoznačná	jednoznačný	k2eNgFnSc1d1	nejednoznačná
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
kyberpunk	kyberpunk	k6eAd1	kyberpunk
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
spisovatel	spisovatel	k1gMnSc1	spisovatel
Bruce	Bruce	k1gMnSc1	Bruce
Bethke	Bethke	k1gInSc4	Bethke
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Kyberpunk	Kyberpunk	k1gMnSc1	Kyberpunk
je	být	k5eAaImIp3nS	být
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
slov	slovo	k1gNnPc2	slovo
kybernetika	kybernetik	k1gMnSc2	kybernetik
(	(	kIx(	(
<g/>
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
řízení	řízení	k1gNnSc6	řízení
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
informatikou	informatika	k1gFnSc7	informatika
a	a	k8xC	a
robotikou	robotika	k1gFnSc7	robotika
<g/>
)	)	kIx)	)
a	a	k8xC	a
punk	punk	k1gInSc4	punk
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
však	však	k9	však
už	už	k6eAd1	už
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
pracích	práce	k1gFnPc6	práce
autorů	autor	k1gMnPc2	autor
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
a	a	k8xC	a
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
strukturalizací	strukturalizace	k1gFnSc7	strukturalizace
technologické	technologický	k2eAgFnSc2d1	technologická
fikce	fikce	k1gFnSc2	fikce
a	a	k8xC	a
odcizením	odcizení	k1gNnSc7	odcizení
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
hi-tech	hi	k1gInPc6	hi-t
vykonstruovaných	vykonstruovaný	k2eAgInPc6d1	vykonstruovaný
megalopolích	megalopolí	k1gNnPc6	megalopolí
<g/>
,	,	kIx,	,
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
např.	např.	kA	např.
Samuel	Samuela	k1gFnPc2	Samuela
R.	R.	kA	R.
Delanyho	Delany	k1gMnSc2	Delany
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
předchůdce	předchůdce	k1gMnSc4	předchůdce
kyperpunku	kyperpunk	k1gInSc2	kyperpunk
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
autoři	autor	k1gMnPc1	autor
literárního	literární	k2eAgInSc2d1	literární
undergroundu	underground	k1gInSc2	underground
–	–	k?	–
například	například	k6eAd1	například
William	William	k1gInSc1	William
S.	S.	kA	S.
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
až	až	k9	až
William	William	k1gInSc1	William
Gibson	Gibsona	k1gFnPc2	Gibsona
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Neuromancer	Neuromancer	k1gMnSc1	Neuromancer
specifikoval	specifikovat	k5eAaBmAgMnS	specifikovat
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
"	"	kIx"	"
<g/>
zaobalil	zaobalit	k5eAaPmAgMnS	zaobalit
<g/>
"	"	kIx"	"
rašící	rašící	k2eAgFnSc4d1	rašící
kyberpunkovou	kyberpunkový	k2eAgFnSc4d1	kyberpunková
kulturu	kultura	k1gFnSc4	kultura
z	z	k7c2	z
nahodilého	nahodilý	k2eAgNnSc2d1	nahodilé
povědomí	povědomí	k1gNnSc2	povědomí
v	v	k7c4	v
potuchu	potucha	k1gFnSc4	potucha
a	a	k8xC	a
mínění	mínění	k1gNnSc4	mínění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
význam	význam	k1gInSc1	význam
kyberpunku	kyberpunk	k1gInSc2	kyberpunk
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
dílem	dílo	k1gNnSc7	dílo
kyberpunku	kyberpunk	k1gInSc2	kyberpunk
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
román	román	k1gInSc1	román
Bruce	Bruce	k1gMnSc2	Bruce
Sterlinga	Sterlinga	k1gFnSc1	Sterlinga
Schismatrix	Schismatrix	k1gInSc1	Schismatrix
<g/>
.	.	kIx.	.
</s>
<s>
Kyperpunk	Kyperpunk	k1gInSc1	Kyperpunk
trvale	trvale	k6eAd1	trvale
změnil	změnit	k5eAaPmAgInS	změnit
tvář	tvář	k1gFnSc4	tvář
mainstreamové	mainstreamový	k2eAgFnSc2d1	mainstreamová
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc4	fiction
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
kyberpunkové	kyberpunkový	k2eAgInPc1d1	kyberpunkový
postupy	postup	k1gInPc1	postup
a	a	k8xC	a
související	související	k2eAgNnPc1d1	související
témata	téma	k1gNnPc1	téma
jako	jako	k8xS	jako
nanotechnologie	nanotechnologie	k1gFnPc1	nanotechnologie
a	a	k8xC	a
technologická	technologický	k2eAgFnSc1d1	technologická
singularita	singularita	k1gFnSc1	singularita
staly	stát	k5eAaPmAgFnP	stát
její	její	k3xOp3gFnSc7	její
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
autory	autor	k1gMnPc7	autor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
hranicemi	hranice	k1gFnPc7	hranice
různých	různý	k2eAgMnPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
právě	právě	k6eAd1	právě
kyberpunkem	kyberpunk	k1gInSc7	kyberpunk
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Neal	Neal	k1gMnSc1	Neal
Stephenson	Stephenson	k1gMnSc1	Stephenson
nebo	nebo	k8xC	nebo
Dan	Dan	k1gMnSc1	Dan
Simmons	Simmonsa	k1gFnPc2	Simmonsa
<g/>
.	.	kIx.	.
</s>
<s>
Kyberpunk	Kyberpunk	k6eAd1	Kyberpunk
se	se	k3xPyFc4	se
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zčásti	zčásti	k6eAd1	zčásti
"	"	kIx"	"
<g/>
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
<g/>
"	"	kIx"	"
v	v	k7c6	v
mainstreamu	mainstream	k1gInSc6	mainstream
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
podproudů	podproud	k1gInPc2	podproud
<g/>
,	,	kIx,	,
řízených	řízený	k2eAgInPc2d1	řízený
většinou	většina	k1gFnSc7	většina
tou	ten	k3xDgFnSc7	ten
kterou	který	k3yIgFnSc7	který
vizí	vize	k1gFnSc7	vize
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
některé	některý	k3yIgInPc4	některý
"	"	kIx"	"
<g/>
potomky	potomek	k1gMnPc7	potomek
<g/>
"	"	kIx"	"
kyberpunku	kyberpunka	k1gFnSc4	kyberpunka
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
cypherpunk	cypherpunk	k1gMnSc1	cypherpunk
<g/>
,	,	kIx,	,
biopunk	biopunk	k1gMnSc1	biopunk
<g/>
,	,	kIx,	,
steampunk	steampunk	k1gMnSc1	steampunk
nebo	nebo	k8xC	nebo
postcyberpunk	postcyberpunk	k1gMnSc1	postcyberpunk
<g/>
.	.	kIx.	.
</s>
<s>
Rudy	Rudy	k1gMnSc1	Rudy
Rucker	Rucker	k1gMnSc1	Rucker
Bruce	Bruce	k1gMnSc1	Bruce
Sterling	sterling	k1gInSc1	sterling
–	–	k?	–
Schismatrix	Schismatrix	k1gInSc1	Schismatrix
John	John	k1gMnSc1	John
Shirley	Shirlea	k1gFnSc2	Shirlea
William	William	k1gInSc1	William
Gibson	Gibson	k1gMnSc1	Gibson
–	–	k?	–
Neuromancer	Neuromancer	k1gMnSc1	Neuromancer
Neal	Neal	k1gMnSc1	Neal
Stephenson	Stephenson	k1gMnSc1	Stephenson
Dan	Dan	k1gMnSc1	Dan
Simmons	Simmonsa	k1gFnPc2	Simmonsa
Sergej	Sergej	k1gMnSc1	Sergej
Lukjaněnko	Lukjaněnka	k1gFnSc5	Lukjaněnka
–	–	k?	–
Bludiště	bludiště	k1gNnSc2	bludiště
odrazů	odraz	k1gInPc2	odraz
Masamune	Masamun	k1gInSc5	Masamun
Shirow	Shirow	k1gMnSc1	Shirow
-	-	kIx~	-
Ghost	Ghost	k1gMnSc1	Ghost
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Shell	Shell	k1gMnSc1	Shell
Miroslav	Miroslav	k1gMnSc1	Miroslav
Žamboch	Žamboch	k1gMnSc1	Žamboch
–	–	k?	–
Líheň	líheň	k1gFnSc1	líheň
(	(	kIx(	(
<g/>
1	[number]	k4	1
–	–	k?	–
Smrt	smrt	k1gFnSc1	smrt
zrozená	zrozený	k2eAgFnSc1d1	zrozená
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2	[number]	k4	2
–	–	k?	–
Královna	královna	k1gFnSc1	královna
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Brossmann	Brossmann	k1gMnSc1	Brossmann
–	–	k?	–
Zákony	zákon	k1gInPc1	zákon
silnějšího	silný	k2eAgMnSc2d2	silnější
Jiří	Jiří	k1gMnSc2	Jiří
Walker	Walker	k1gMnSc1	Walker
Procházka	Procházka	k1gMnSc1	Procházka
–	–	k?	–
navazující	navazující	k2eAgFnPc4d1	navazující
povídky	povídka	k1gFnPc4	povídka
Rox	Rox	k1gFnSc2	Rox
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	roll	k1gInSc1	roll
<g/>
,	,	kIx,	,
Pražský	pražský	k2eAgInSc1d1	pražský
průnik	průnik	k1gInSc1	průnik
<g/>
,	,	kIx,	,
Kybernátor	Kybernátor	k1gInSc1	Kybernátor
<g/>
,	,	kIx,	,
Jackův	Jackův	k2eAgInSc1d1	Jackův
konvoj	konvoj	k1gInSc1	konvoj
Jan	Jan	k1gMnSc1	Jan
Janda	Janda	k1gMnSc1	Janda
(	(	kIx(	(
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
)	)	kIx)	)
–	–	k?	–
Hacker	hacker	k1gMnSc1	hacker
–	–	k?	–
Odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
tvář	tvář	k1gFnSc1	tvář
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Zločinci	zločinec	k1gMnSc3	zločinec
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
</s>
