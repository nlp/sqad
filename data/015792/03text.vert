<s>
Medlánecké	Medlánecký	k2eAgInPc4d1
kopce	kopec	k1gInPc4
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaMedlánecké	památkaMedlánecký	k2eAgFnPc4d1
kopceIUCN	kopceIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Medlánecké	Medlánecký	k2eAgInPc4d1
kopce	kopec	k1gInPc4
od	od	k7c2
Letiště	letiště	k1gNnSc2
MedlánkyZákladní	MedlánkyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1988	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
282	#num#	k4
-	-	kIx~
338	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
11,82	11,82	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Brno-město	Brno-město	k6eAd1
Umístění	umístění	k1gNnSc1
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
<g/>
,	,	kIx,
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
3,91	3,91	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
8,05	8,05	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Medlánecké	Medlánecký	k2eAgInPc4d1
kopce	kopec	k1gInPc4
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
1131	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Medlánecké	Medlánecký	k2eAgInPc1d1
kopce	kopec	k1gInPc1
jsou	být	k5eAaImIp3nP
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
,	,	kIx,
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
12,545	12,545	k4
<g/>
6	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
v	v	k7c6
katastrálních	katastrální	k2eAgNnPc6d1
územích	území	k1gNnPc6
Medlánky	Medlánky	k1gFnPc1
a	a	k8xC
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orgánem	orgán	k1gInSc7
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
je	být	k5eAaImIp3nS
krajský	krajský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kus	kus	k1gInSc1
skály	skála	k1gFnSc2
na	na	k7c6
Medláneckém	Medlánecký	k2eAgInSc6d1
kopci	kopec	k1gInSc6
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
dva	dva	k4xCgInPc4
chráněné	chráněný	k2eAgInPc4d1
samostatné	samostatný	k2eAgInPc4d1
kopce	kopec	k1gInPc4
v	v	k7c6
severním	severní	k2eAgNnSc6d1
pokračování	pokračování	k1gNnSc6
Palackého	Palackého	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
-	-	kIx~
severovýchodně	severovýchodně	k6eAd1
položený	položený	k2eAgInSc1d1
menší	malý	k2eAgInSc1d2
Medlánecký	Medlánecký	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
322	#num#	k4
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
západě	západ	k1gInSc6
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Medlánky	Medlánka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jihozápadně	jihozápadně	k6eAd1
položený	položený	k2eAgInSc4d1
větší	veliký	k2eAgInSc4d2
Střelecký	střelecký	k2eAgInSc4d1
kopec	kopec	k1gInSc4
(	(	kIx(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
338	#num#	k4
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
severu	sever	k1gInSc6
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Královo	Králův	k2eAgNnSc4d1
Pole	pole	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
dvě	dva	k4xCgFnPc4
územně	územně	k6eAd1
zcela	zcela	k6eAd1
nesouvisející	související	k2eNgFnSc2d1
části	část	k1gFnSc2
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
rozdělený	rozdělený	k2eAgInSc1d1
i	i	k8xC
areál	areál	k1gInSc1
této	tento	k3xDgFnSc2
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
Medlánecké	Medlánecký	k2eAgInPc4d1
kopce	kopec	k1gInPc4
<g/>
"	"	kIx"
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
až	až	k9
do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
oba	dva	k4xCgMnPc1
náležely	náležet	k5eAaImAgFnP
do	do	k7c2
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Medlánky	Medlánka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
západ	západ	k1gInSc4
od	od	k7c2
památky	památka	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Netopýrky	netopýrek	k1gMnPc4
a	a	k8xC
o	o	k7c4
další	další	k2eAgInSc4d1
kilometr	kilometr	k1gInSc4
větší	veliký	k2eAgFnSc1d2
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Mniší	mniší	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
0,5	0,5	k4
kilometru	kilometr	k1gInSc2
daleko	daleko	k6eAd1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Medlánecká	Medlánecký	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
lokalita	lokalita	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
bohatá	bohatý	k2eAgFnSc1d1
na	na	k7c4
teplomilnou	teplomilný	k2eAgFnSc4d1
květinu	květina	k1gFnSc4
(	(	kIx(
<g/>
koniklec	koniklec	k1gInSc4
velkokvětý	velkokvětý	k2eAgInSc4d1
<g/>
)	)	kIx)
a	a	k8xC
hmyz	hmyz	k1gInSc1
spadající	spadající	k2eAgInSc1d1
do	do	k7c2
biomu	biom	k1gInSc2
stepí	step	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
letišti	letiště	k1gNnSc6
AK	AK	kA
Medlánky	Medlánek	k1gInPc1
poblíž	poblíž	k7c2
rezervace	rezervace	k1gFnSc2
se	se	k3xPyFc4
současně	současně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
kolonie	kolonie	k1gFnSc1
syslů	sysel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
koniklece	koniklec	k1gInSc2
velkokvětého	velkokvětý	k2eAgInSc2d1
je	být	k5eAaImIp3nS
pozoruhodný	pozoruhodný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
ostřice	ostřice	k1gFnSc2
úzkolisté	úzkolistý	k2eAgNnSc1d1
(	(	kIx(
<g/>
nejsevernější	severní	k2eAgFnSc1d3
známá	známý	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
černýše	černýš	k1gInSc2
rolního	rolní	k2eAgInSc2d1
<g/>
,	,	kIx,
hvozdíku	hvozdík	k1gInSc2
Pontederova	Pontederův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Skalní	skalní	k2eAgNnSc1d1
podloží	podloží	k1gNnSc1
<g/>
,	,	kIx,
vystupující	vystupující	k2eAgMnSc1d1
často	často	k6eAd1
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
metabazity	metabazita	k1gFnSc2
brněnského	brněnský	k2eAgInSc2d1
masivu	masiv	k1gInSc2
s	s	k7c7
vložkami	vložka	k1gFnPc7
žulových	žulový	k2eAgMnPc2d1
porfyrů	porfyr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
závětrných	závětrný	k2eAgInPc6d1
východních	východní	k2eAgInPc6d1
svazích	svah	k1gInPc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
spraše	spraš	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
půd	půda	k1gFnPc2
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgInP
kambizem	kambiz	k1gInSc7
typická	typický	k2eAgFnSc1d1
<g/>
,	,	kIx,
kambizem	kambiz	k1gInSc7
eutrofní	eutrofní	k2eAgFnSc2d1
<g/>
,	,	kIx,
rankery	rankera	k1gFnSc2
a	a	k8xC
na	na	k7c6
spraši	spraš	k1gFnSc6
typické	typický	k2eAgFnSc2d1
hnědozemě	hnědozem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Stromové	stromový	k2eAgNnSc1d1
patro	patro	k1gNnSc1
zastupuje	zastupovat	k5eAaImIp3nS
dub	dub	k1gInSc1
zimní	zimní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
petraea	petrae	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
třešeň	třešeň	k1gFnSc1
ptačí	ptačit	k5eAaImIp3nS
(	(	kIx(
<g/>
Cerasus	Cerasus	k1gInSc1
avium	avium	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jižních	jižní	k2eAgInPc6d1
svazích	svah	k1gInPc6
i	i	k8xC
akát	akát	k1gInSc4
(	(	kIx(
<g/>
Robinia	Robinium	k1gNnSc2
pseudacacia	pseudacacium	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
jasan	jasan	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
(	(	kIx(
<g/>
Fraxinus	Fraxinus	k1gInSc1
excelsior	excelsior	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
keřové	keřový	k2eAgNnSc4d1
patro	patro	k1gNnSc4
brslen	brslen	k1gInSc4
bradavičnatý	bradavičnatý	k2eAgInSc4d1
(	(	kIx(
<g/>
Euonymus	Euonymus	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
verrucosus	verrucosus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
b.	b.	k?
evropský	evropský	k2eAgMnSc1d1
(	(	kIx(
<g/>
E.	E.	kA
europaeus	europaeus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krušina	krušina	k1gFnSc1
olšová	olšový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Frangula	Frangula	k1gFnSc1
alnus	alnus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
růže	růže	k1gFnSc1
šípková	šípkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rosa	Rosa	k1gFnSc1
canina	canina	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
r.	r.	kA
vinná	vinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
R.	R.	kA
rubiginosa	rubiginosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řešetlák	řešetlák	k1gInSc1
počistivý	počistivý	k2eAgInSc1d1
(	(	kIx(
<g/>
Rhamnus	Rhamnus	k1gInSc1
cathartica	cathartic	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
slivoň	slivoň	k1gFnSc1
trnitá	trnitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Prunus	Prunus	k1gMnSc1
spinosa	spinosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
teplomilných	teplomilný	k2eAgInPc6d1
trávnících	trávník	k1gInPc6
roste	růst	k5eAaImIp3nS
černýš	černýš	k1gInSc1
rolní	rolní	k2eAgInSc1d1
(	(	kIx(
<g/>
Melampyrum	Melampyrum	k1gInSc1
arvense	arvense	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čilimník	čilimník	k1gInSc4
řezenský	řezenský	k2eAgInSc4d1
(	(	kIx(
<g/>
Chamaecytisus	Chamaecytisus	k1gInSc4
ratisbonensis	ratisbonensis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koniklec	koniklec	k1gInSc4
velkokvětý	velkokvětý	k2eAgInSc4d1
(	(	kIx(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1
grandis	grandis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kozinec	kozinec	k1gInSc1
dánský	dánský	k2eAgInSc1d1
(	(	kIx(
<g/>
Astragalus	Astragalus	k1gInSc1
danicus	danicus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mochna	mochna	k1gFnSc1
písečná	písečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Potentilla	Potentilla	k1gFnSc1
arenaria	arenarium	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
úzkolistá	úzkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
stenophylla	stenophyllo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plamének	plamének	k1gInSc1
přímý	přímý	k2eAgInSc1d1
(	(	kIx(
<g/>
Clamatis	Clamatis	k1gInSc1
recta	rect	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podražec	podražec	k1gInSc4
křovištní	křovištní	k2eAgInSc4d1
(	(	kIx(
<g/>
Aristolochia	Aristolochia	k1gFnSc1
clematitis	clematitis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pochybek	pochybek	k1gInSc4
prodloužený	prodloužený	k2eAgInSc4d1
(	(	kIx(
<g/>
Androsace	Androsace	k1gFnSc1
elongata	elongata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
radyk	radyk	k6eAd1
prutnatý	prutnatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Chondrilla	Chondrilla	k1gMnSc1
juncea	juncea	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozrazil	rozrazil	k1gInSc1
rozprostřený	rozprostřený	k2eAgInSc1d1
(	(	kIx(
<g/>
Veronica	Veronic	k2eAgFnSc1d1
prostrata	prostrata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řebříček	řebříček	k1gInSc1
štětinolistý	štětinolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Achillea	Achilleus	k1gMnSc2
setacea	setaceus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvonek	zvonek	k1gInSc4
klubkatý	klubkatý	k2eAgInSc4d1
(	(	kIx(
<g/>
Campanula	Campanula	k1gFnSc1
glomerata	glomerata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvonek	zvonek	k1gInSc4
moravský	moravský	k2eAgInSc4d1
(	(	kIx(
<g/>
Campanula	Campanula	k1gFnSc4
moravica	moravic	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácnější	vzácný	k2eAgFnPc4d2
houby	houba	k1gFnPc4
zastupuje	zastupovat	k5eAaImIp3nS
helmovka	helmovka	k1gFnSc1
malovaná	malovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Mycena	Mycen	k2eAgFnSc1d1
pseudopicta	pseudopicta	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
kyjovečka	kyjovečka	k1gFnSc1
růžkovitá	růžkovitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Clavulinopsis	Clavulinopsis	k1gFnSc1
corniculata	cornicule	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Kopce	kopec	k1gInPc1
jsou	být	k5eAaImIp3nP
nejsevernějším	severní	k2eAgInSc7d3
výskytem	výskyt	k1gInSc7
petrokřídlece	petrokřídlece	k1gFnPc4
podražcového	podražcový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Zerynthia	Zerynthius	k1gMnSc2
polyxena	polyxen	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
zástupci	zástupce	k1gMnPc7
teplomilného	teplomilný	k2eAgInSc2d1
hmyzu	hmyz	k1gInSc2
jsou	být	k5eAaImIp3nP
otakárek	otakárek	k1gMnSc1
fenyklový	fenyklový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Papilio	Papilio	k1gMnSc1
machaon	machaon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
otakárek	otakárek	k1gMnSc1
ovocný	ovocný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Iphiclides	Iphiclides	k1gMnSc1
podalirius	podalirius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
vakonoš	vakonoš	k1gMnSc1
šedý	šedý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Ptilocephala	Ptilocephal	k1gMnSc4
muscella	muscell	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
kudlanka	kudlanka	k1gFnSc1
nábožná	nábožný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Mantis	mantisa	k1gFnPc2
religiosa	religiosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plazi	plaz	k1gMnPc1
zastupuje	zastupovat	k5eAaImIp3nS
ještěrka	ještěrka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
agilis	agilis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slepýš	slepýš	k1gMnSc1
křehký	křehký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Anguis	Anguis	k1gInSc1
fragilis	fragilis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
užovka	užovka	k1gFnSc1
hladká	hladký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Coronella	Coronella	k1gFnSc1
austriaca	austriaca	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ptáky	pták	k1gMnPc4
krutihlav	krutihlav	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Jynx	Jynx	k1gInSc1
torquilla	torquillo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pěnice	pěnice	k1gFnSc1
vlašská	vlašský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sylvia	Sylvia	k1gFnSc1
nisoria	nisorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ťuhýk	ťuhýk	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Lanius	Lanius	k1gMnSc1
collurio	collurio	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
žluva	žluva	k1gFnSc1
hajní	hajní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Oriolus	Oriolus	k1gMnSc1
oriolus	oriolus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
střelecký	střelecký	k2eAgInSc4d1
kopec	kopec	k1gInSc4
směrem	směr	k1gInSc7
od	od	k7c2
Medláneckého	Medlánecký	k2eAgInSc2d1
kopce	kopec	k1gInSc2
</s>
<s>
Koniklec	koniklec	k1gInSc1
velkokvětý	velkokvětý	k2eAgInSc1d1
-	-	kIx~
Pulsatilla	Pulsatilla	k1gFnSc1
grandis	grandis	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
MACKOVČIN	MACKOVČIN	kA
<g/>
,	,	kIx,
P.	P.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brněnsko	Brněnsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
IX	IX	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
a	a	k8xC
EkoCentrum	EkoCentrum	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
932	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86305	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
251	#num#	k4
<g/>
-	-	kIx~
<g/>
252	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vrchy	vrch	k1gInPc1
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Červený	červený	k2eAgInSc4d1
kopec	kopec	k1gInSc4
•	•	k?
Hády	hádes	k1gInPc4
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
z	z	k7c2
části	část	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Kopeček	kopeček	k1gInSc1
•	•	k?
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Medlánecký	Medlánecký	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Mniší	mniší	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Palackého	Palacký	k1gMnSc2
vrch	vrch	k1gInSc1
•	•	k?
Petrov	Petrov	k1gInSc1
•	•	k?
Stránská	Stránská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Strom	strom	k1gInSc1
•	•	k?
Špilberk	Špilberk	k1gInSc1
•	•	k?
Žlutý	žlutý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
Brně	Brno	k1gNnSc6
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Baba	baba	k1gFnSc1
•	•	k?
Podkomorské	podkomorský	k2eAgInPc4d1
lesy	les	k1gInPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Hádecká	hádecký	k2eAgFnSc1d1
planinka	planinka	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Červený	červený	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Stránská	Stránská	k1gFnSc1
skála	skála	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Babí	babí	k2eAgInPc1d1
doly	dol	k1gInPc1
•	•	k?
Bosonožský	Bosonožský	k2eAgInSc1d1
hájek	hájek	k1gInSc1
•	•	k?
Břenčák	Břenčák	k1gInSc1
•	•	k?
Černovický	Černovický	k2eAgInSc1d1
hájek	hájek	k1gInSc1
•	•	k?
Jelení	jelení	k2eAgInSc1d1
žlíbek	žlíbek	k1gInSc1
•	•	k?
Kamenný	kamenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Krnovec	Krnovec	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Hornek	Hornek	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Augšperský	Augšperský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Holásecká	Holásecká	k1gFnSc1
jezera	jezero	k1gNnSc2
•	•	k?
Junácká	junácký	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Kavky	kavka	k1gFnSc2
•	•	k?
Kůlny	kůlna	k1gFnSc2
•	•	k?
Medlánecká	Medlánecký	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Medlánecké	Medlánecký	k2eAgFnSc6d1
kopce	kopka	k1gFnSc6
•	•	k?
Mniší	mniší	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Na	na	k7c6
Skalách	skála	k1gFnPc6
•	•	k?
Netopýrky	netopýrek	k1gMnPc4
•	•	k?
Obřanská	Obřanský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Pekárna	pekárna	k1gFnSc1
•	•	k?
Rájecká	Rájecký	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Přehrady	přehrada	k1gFnSc2
•	•	k?
Soběšické	Soběšický	k2eAgInPc1d1
rybníčky	rybníček	k1gInPc1
•	•	k?
Údolí	údolí	k1gNnSc6
Kohoutovického	kohoutovický	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
Klajdovka	Klajdovka	k1gFnSc1
•	•	k?
Žebětínský	Žebětínský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
