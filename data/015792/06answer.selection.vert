<s>
Medlánecké	Medlánecký	k2eAgInPc1d1
kopce	kopec	k1gInPc1
jsou	být	k5eAaImIp3nP
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
,	,	kIx,
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
12,545	12,545	k4
<g/>
6	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
v	v	k7c6
katastrálních	katastrální	k2eAgNnPc6d1
územích	území	k1gNnPc6
Medlánky	Medlánky	k1gFnPc1
a	a	k8xC
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
<g/>
.	.	kIx.
</s>