<s>
Arabské	arabský	k2eAgFnPc1d1
číslice	číslice	k1gFnPc4
(	(	kIx(
<g/>
výjimečně	výjimečně	k6eAd1
nazývané	nazývaný	k2eAgFnPc4d1
hindské	hindský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
<g/>
,	,	kIx,
hindsko-arabské	hindsko-arabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
nebo	nebo	k8xC
indo-arabské	indo-arabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
dnes	dnes	k6eAd1
nejrozšířenější	rozšířený	k2eAgInSc1d3
systém	systém	k1gInSc1
symbolického	symbolický	k2eAgInSc2d1
zápisu	zápis	k1gInSc2
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
také	také	k9
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>