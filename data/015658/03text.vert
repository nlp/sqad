<s>
Arabské	arabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
</s>
<s>
Číselný	číselný	k2eAgInSc1d1
zápis	zápis	k1gInSc1
arabských	arabský	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
</s>
<s>
Číselné	číselný	k2eAgFnPc1d1
soustavy	soustava	k1gFnPc1
</s>
<s>
hindsko-arabská	hindsko-arabský	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
západoarabskévýchodoarabskébengálskégurmuskéindickésinhálskétamilskébalijskébarmskédzongkskéjavánskékhmerskélaoskémongolskéthajské	západoarabskévýchodoarabskébengálskégurmuskéindickésinhálskétamilskébalijskébarmskédzongkskéjavánskékhmerskélaoskémongolskéthajský	k2eAgNnSc1d1
</s>
<s>
Východní	východní	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
</s>
<s>
čínskéhokkienskéjaponskékorejskévietnamské	čínskéhokkienskéjaponskékorejskévietnamský	k2eAgNnSc1d1
</s>
<s>
Abecední	abecední	k2eAgInSc1d1
</s>
<s>
abdžadskéarménskéárjabhatacyriliceGe	abdžadskéarménskéárjabhatacyriliceGe	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ez	ez	k?
(	(	kIx(
<g/>
etiopské	etiopský	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
gruzínskéřeckéhebrejskéřímské	gruzínskéřeckéhebrejskéřímský	k2eAgFnSc2d1
</s>
<s>
bývalé	bývalý	k2eAgInPc1d1
</s>
<s>
egejskéattickébabylonskébráhmíegyptskéetruskéinuitskékharóšthímayskémuiskéKipu	egejskéattickébabylonskébráhmíegyptskéetruskéinuitskékharóšthímayskémuiskéKipat	k5eAaPmIp1nS
</s>
<s>
poziční	poziční	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
podle	podle	k7c2
základu	základ	k1gInSc2
</s>
<s>
23456810111216203660	#num#	k4
</s>
<s>
nestandardní	standardní	k2eNgFnPc1d1
poziční	poziční	k2eAgFnPc1d1
číselné	číselný	k2eAgFnPc1d1
soustavy	soustava	k1gFnPc1
</s>
<s>
bijektivní	bijektivní	k2eAgFnSc1d1
numerace	numerace	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
znaménkové	znaménkový	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
(	(	kIx(
<g/>
balancovaná	balancovaný	k2eAgFnSc1d1
trojková	trojkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
faktoriálnízápornékomplexní	faktoriálnízápornékomplexní	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustavanečíselné	soustavanečíselný	k2eAgFnPc4d1
reprezentace	reprezentace	k1gFnPc4
(	(	kIx(
<g/>
φ	φ	k?
<g/>
)	)	kIx)
<g/>
smíšenéasymetrické	smíšenéasymetrický	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
</s>
<s>
Arabský	arabský	k2eAgInSc4d1
a	a	k8xC
evropský	evropský	k2eAgInSc4d1
tvar	tvar	k1gInSc4
číslic	číslice	k1gFnPc2
na	na	k7c6
dopravní	dopravní	k2eAgFnSc6d1
značce	značka	k1gFnSc6
v	v	k7c6
Abú	abú	k1gMnSc2
Zabí	Zabí	k1gFnSc6
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1
číslice	číslice	k1gFnPc4
(	(	kIx(
<g/>
výjimečně	výjimečně	k6eAd1
nazývané	nazývaný	k2eAgFnPc4d1
hindské	hindský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
<g/>
,	,	kIx,
hindsko-arabské	hindsko-arabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
nebo	nebo	k8xC
indo-arabské	indo-arabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
dnes	dnes	k6eAd1
nejrozšířenější	rozšířený	k2eAgInSc1d3
systém	systém	k1gInSc1
symbolického	symbolický	k2eAgInSc2d1
zápisu	zápis	k1gInSc2
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
také	také	k9
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
složky	složka	k1gFnPc4
<g/>
:	:	kIx,
Jednak	jednak	k8xC
samotné	samotný	k2eAgNnSc1d1
používání	používání	k1gNnSc1
desítkové	desítkový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
poziční	poziční	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
o	o	k7c6
základu	základ	k1gInSc6
deset	deset	k4xCc4
<g/>
,	,	kIx,
jednak	jednak	k8xC
konkrétní	konkrétní	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
pro	pro	k7c4
reprezentaci	reprezentace	k1gFnSc4
číslic	číslice	k1gFnPc2
používají	používat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
latince	latinka	k1gFnSc6
<g/>
,	,	kIx,
cyrilici	cyrilice	k1gFnSc6
<g/>
,	,	kIx,
arabském	arabský	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
moderním	moderní	k2eAgNnSc6d1
hebrejském	hebrejský	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgInPc6d1
typech	typ	k1gInPc6
písma	písmo	k1gNnSc2
jsou	být	k5eAaImIp3nP
těmito	tento	k3xDgFnPc7
znaky	znak	k1gInPc1
číslice	číslice	k1gFnSc1
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
Hindsko-arabské	hindsko-arabský	k2eAgFnSc2d1
číselné	číselný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
původ	původ	k1gInSc4
v	v	k7c6
Indii	Indie	k1gFnSc6
v	v	k7c6
období	období	k1gNnSc6
mezi	mezi	k7c7
400	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
400	#num#	k4
n.	n.	k?
l.	l.	k?
Do	do	k7c2
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
díky	díky	k7c3
arabským	arabský	k2eAgMnPc3d1
matematikům	matematik	k1gMnPc3
a	a	k8xC
astronomům	astronom	k1gMnPc3
(	(	kIx(
<g/>
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
díky	díky	k7c3
knize	kniha	k1gFnSc3
od	od	k7c2
Al-Chorezmího	Al-Chorezmí	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
pochází	pocházet	k5eAaImIp3nS
dnešní	dnešní	k2eAgNnSc4d1
označení	označení	k1gNnSc4
<g/>
;	;	kIx,
o	o	k7c6
jejich	jejich	k3xOp3gNnSc6
rozšíření	rozšíření	k1gNnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
zasloužil	zasloužit	k5eAaPmAgMnS
Fibonacci	Fibonacce	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
propagoval	propagovat	k5eAaImAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Liber	libra	k1gFnPc2
abaci	abace	k1gFnSc3
z	z	k7c2
roku	rok	k1gInSc2
1202	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropský	evropský	k2eAgInSc1d1
tvar	tvar	k1gInSc1
číslic	číslice	k1gFnPc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vyvíjel	vyvíjet	k5eAaImAgMnS
až	až	k9
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
arabském	arabský	k2eAgInSc6d1
světě	svět	k1gInSc6
(	(	kIx(
<g/>
kromě	kromě	k7c2
severozápadu	severozápad	k1gInSc2
Maghrebu	Maghreb	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
ale	ale	k9
pro	pro	k7c4
číslice	číslice	k1gFnPc4
používají	používat	k5eAaImIp3nP
odlišné	odlišný	k2eAgInPc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
východoarabské	východoarabský	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
:	:	kIx,
۰	۰	k?
<g/>
۱	۱	k?
<g/>
۲	۲	k?
<g/>
۳	۳	k?
<g/>
۴	۴	k?
<g/>
۵	۵	k?
<g/>
۶	۶	k?
<g/>
۷	۷	k?
<g/>
۸	۸	k?
<g/>
۹	۹	k?
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Číslice	číslice	k1gFnSc1
od	od	k7c2
1	#num#	k4
až	až	k9
do	do	k7c2
9	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
hindsko-arabské	hindsko-arabský	k2eAgFnSc6d1
číselné	číselný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
vyvinuly	vyvinout	k5eAaPmAgFnP
z	z	k7c2
číslic	číslice	k1gFnPc2
jazyka	jazyk	k1gInSc2
bráhmí	bráhmit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buddhistické	buddhistický	k2eAgInPc1d1
nápisy	nápis	k1gInPc1
zhruba	zhruba	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
300	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
používaly	používat	k5eAaImAgFnP
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
pak	pak	k6eAd1
staly	stát	k5eAaPmAgFnP
číslice	číslice	k1gFnPc4
1	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
jedno	jeden	k4xCgNnSc4
století	století	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
používat	používat	k5eAaImF
symboly	symbol	k1gInPc7
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgFnPc2,k3yRgFnPc2,k3yIgFnPc2
se	se	k3xPyFc4
pak	pak	k6eAd1
staly	stát	k5eAaPmAgFnP
číslice	číslice	k1gFnPc4
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnSc1
všeobecně	všeobecně	k6eAd1
přijímaný	přijímaný	k2eAgInSc1d1
nápis	nápis	k1gInSc1
obsahující	obsahující	k2eAgInSc1d1
použití	použití	k1gNnSc4
0	#num#	k4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
870	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
nápisu	nápis	k1gInSc2
u	u	k7c2
Gválijaru	Gválijar	k1gInSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
bylo	být	k5eAaImAgNnS
používání	používání	k1gNnSc1
symbolu	symbol	k1gInSc2
0	#num#	k4
objeveno	objevit	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
Persii	Persie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
zmíněna	zmínit	k5eAaPmNgFnS
v	v	k7c6
Al-Chwárizmího	Al-Chwárizmí	k1gMnSc2
popisu	popis	k1gInSc6
indických	indický	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Početné	početný	k2eAgInPc4d1
indické	indický	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
na	na	k7c6
měděných	měděný	k2eAgFnPc6d1
deskách	deska	k1gFnPc6
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
číslo	číslo	k1gNnSc4
0	#num#	k4
už	už	k9
existovala	existovat	k5eAaImAgFnS
dříve	dříve	k6eAd2
a	a	k8xC
to	ten	k3xDgNnSc4
někdy	někdy	k6eAd1
kolem	kolem	k7c2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Perský	perský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Al-Chorezmí	Al-Chorezmí	k1gNnSc2
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
v	v	k7c6
arabštině	arabština	k1gFnSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
825	#num#	k4
knihu	kniha	k1gFnSc4
„	„	k?
<g/>
Počítání	počítání	k1gNnSc2
s	s	k7c7
indickými	indický	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
<g/>
“	“	k?
a	a	k8xC
arabský	arabský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Alkindus	Alkindus	k1gMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
kolem	kolem	k7c2
roku	rok	k1gInSc2
830	#num#	k4
čtyřdílnou	čtyřdílný	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
O	o	k7c6
používání	používání	k1gNnSc6
indických	indický	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Ketab	Ketab	k1gMnSc1
fi	fi	k0
Isti	Ist	k1gInPc7
<g/>
´	´	k?
<g/>
mal	málit	k5eAaImRp2nS
al-	al-	k?
<g/>
´	´	k?
<g/>
Adad	Adad	k1gInSc4
al-Hindi	al-Hind	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
tato	tento	k3xDgNnPc1
díla	dílo	k1gNnPc1
měla	mít	k5eAaImAgNnP
zásluhu	zásluha	k1gFnSc4
na	na	k7c4
rozšiřování	rozšiřování	k1gNnSc4
indického	indický	k2eAgInSc2d1
systému	systém	k1gInSc2
číslování	číslování	k1gNnSc2
na	na	k7c6
Středním	střední	k2eAgInSc6d1
Východě	východ	k1gInSc6
a	a	k8xC
na	na	k7c6
Západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
středovýchodní	středovýchodní	k2eAgMnPc1d1
matematici	matematik	k1gMnPc1
rozšířili	rozšířit	k5eAaPmAgMnP
desítkovou	desítkový	k2eAgFnSc4d1
číselnou	číselný	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zahrnovala	zahrnovat	k5eAaImAgFnS
zlomky	zlomek	k1gInPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
zaznamenal	zaznamenat	k5eAaPmAgMnS
syrský	syrský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Abu	Abu	k1gMnSc1
<g/>
'	'	kIx"
<g/>
l-Hasan	l-Hasan	k1gMnSc1
al-Uqlidisi	al-Uqlidise	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
952	#num#	k4
<g/>
-	-	kIx~
<g/>
953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desetinná	desetinný	k2eAgFnSc1d1
čárka	čárka	k1gFnSc1
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
Sindem	Sind	k1gInSc7
ibn	ibn	k?
Alim	Alim	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
předtím	předtím	k6eAd1
také	také	k9
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
o	o	k7c6
arabských	arabský	k2eAgFnPc6d1
číslicích	číslice	k1gFnPc6
pojednání	pojednání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Výrazné	výrazný	k2eAgFnPc1d1
západoarabské	západoarabský	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
symbolů	symbol	k1gInPc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
objevovat	objevovat	k5eAaImF
kolem	kolem	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
Maghrebu	Maghreb	k1gInSc6
a	a	k8xC
Al-Andalusu	Al-Andalus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
tam	tam	k6eAd1
objeveny	objeven	k2eAgFnPc1d1
číslice	číslice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
přímými	přímý	k2eAgMnPc7d1
předchůdci	předchůdce	k1gMnPc7
moderních	moderní	k2eAgFnPc2d1
západních	západní	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
používají	používat	k5eAaImIp3nP
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Německý	německý	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
na	na	k7c6
stránce	stránka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
používání	používání	k1gNnSc6
arabských	arabský	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
(	(	kIx(
<g/>
Talhoffer	Talhoffer	k1gMnSc1
Thott	Thott	k1gMnSc1
<g/>
,	,	kIx,
1459	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Řezba	řezba	k1gFnSc1
do	do	k7c2
dřeva	dřevo	k1gNnSc2
zobrazující	zobrazující	k2eAgInSc4d1
orloj	orloj	k1gInSc4
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
katedrále	katedrála	k1gFnSc6
v	v	k7c6
Uppsale	Uppsala	k1gFnSc6
se	s	k7c7
dvěma	dva	k4xCgInPc7
ciferníky	ciferník	k1gInPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeden	jeden	k4xCgMnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
arabské	arabský	k2eAgFnSc2d1
číslice	číslice	k1gFnSc2
a	a	k8xC
druhý	druhý	k4xOgInSc4
římské	římský	k2eAgFnSc2d1
číslice	číslice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Ciferník	ciferník	k1gInSc1
desítkové	desítkový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
v	v	k7c6
období	období	k1gNnSc6
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
ke	k	k7c3
konci	konec	k1gInSc3
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
825	#num#	k4
Al-Chorezmí	Al-Chorezmí	k1gNnPc2
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
pojednání	pojednání	k1gNnSc1
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
O	o	k7c6
počítání	počítání	k1gNnSc6
s	s	k7c7
hindskými	hindský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
přeloženo	přeložit	k5eAaPmNgNnS
do	do	k7c2
latiny	latina	k1gFnSc2
z	z	k7c2
arabštiny	arabština	k1gFnSc2
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Algoritmi	Algorit	k1gFnPc7
de	de	k?
numero	numero	k?
Indorum	Indorum	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnPc1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
arabských	arabský	k2eAgNnPc6d1
číslech	číslo	k1gNnPc6
byly	být	k5eAaImAgFnP
nalezeny	naleznout	k5eAaPmNgFnP,k5eAaBmNgFnP
na	na	k7c6
západě	západ	k1gInSc6
Evropy	Evropa	k1gFnSc2
v	v	k7c6
dokumentech	dokument	k1gInPc6
Codex	Codex	k1gInSc4
Vigilanus	Vigilanus	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
980	#num#	k4
Gerbert	Gerbert	k1gInSc1
z	z	k7c2
Aurillacu	Aurillacus	k1gInSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
papež	papež	k1gMnSc1
Silvestr	Silvestr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
využil	využít	k5eAaPmAgInS
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
a	a	k8xC
šířil	šířit	k5eAaImAgMnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
vědomí	vědomí	k1gNnSc2
o	o	k7c6
arabských	arabský	k2eAgNnPc6d1
číslech	číslo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gerbert	Gerbert	k1gInSc1
z	z	k7c2
Aurillacu	Aurillacus	k1gInSc2
v	v	k7c6
mládí	mládí	k1gNnSc6
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
,	,	kIx,
takže	takže	k8xS
měl	mít	k5eAaImAgMnS
na	na	k7c4
výklad	výklad	k1gInSc4
o	o	k7c6
arabských	arabský	k2eAgNnPc6d1
číslech	číslo	k1gNnPc6
dostatečné	dostatečný	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Fibonacci	Fibonacce	k1gFnSc4
byl	být	k5eAaImAgInS
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Pise	Pisa	k1gFnSc6
a	a	k8xC
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
Béjaï	Béjaï	k1gFnSc6
v	v	k7c6
Alžírsku	Alžírsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporoval	podporovat	k5eAaImAgMnS
šíření	šíření	k1gNnSc4
vědomostí	vědomost	k1gFnPc2
o	o	k7c6
indické	indický	k2eAgFnSc6d1
číselné	číselný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
svou	svůj	k3xOyFgFnSc7
knihou	kniha	k1gFnSc7
Liber	libra	k1gFnPc2
Abaci	Abace	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1202	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
</s>
<s>
Systém	systém	k1gInSc1
číslic	číslice	k1gFnPc2
v	v	k7c6
cyrilici	cyrilice	k1gFnSc6
používali	používat	k5eAaImAgMnP
jižní	jižní	k2eAgMnPc1d1
a	a	k8xC
východní	východní	k2eAgMnPc1d1
Slované	Slovan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
používán	používat	k5eAaImNgInS
v	v	k7c6
Rusku	Rusko	k1gNnSc6
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gNnSc4
Petr	Petr	k1gMnSc1
I.	I.	kA
Veliký	veliký	k2eAgInSc1d1
nahradil	nahradit	k5eAaPmAgInS
arabskými	arabský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
do	do	k7c2
Číny	Čína	k1gFnSc2
</s>
<s>
Během	během	k7c2
vlád	vláda	k1gFnPc2
dynastií	dynastie	k1gFnPc2
Ming	Ming	k1gInSc1
a	a	k8xC
Čching	Čching	k1gInSc1
(	(	kIx(
<g/>
když	když	k8xS
byly	být	k5eAaImAgFnP
arabské	arabský	k2eAgFnPc1d1
číslice	číslice	k1gFnPc1
poprvé	poprvé	k6eAd1
představeny	představit	k5eAaPmNgFnP
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
)	)	kIx)
někteří	některý	k3yIgMnPc1
čínští	čínský	k2eAgMnPc1d1
matematici	matematik	k1gMnPc1
používali	používat	k5eAaImAgMnP
v	v	k7c6
číselné	číselný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
čínské	čínský	k2eAgFnSc2d1
číslice	číslice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dynastii	dynastie	k1gFnSc6
Čching	Čching	k1gInSc1
byly	být	k5eAaImAgInP
jak	jak	k6eAd1
čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
číslice	číslice	k1gFnSc1
ve	v	k7c6
všech	všecek	k3xTgInPc6
matematických	matematický	k2eAgInPc6d1
spisech	spis	k1gInPc6
nahrazeny	nahradit	k5eAaPmNgInP
arabskými	arabský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Arabic	Arabice	k1gInPc2
numerals	numeralsa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Římské	římský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
</s>
<s>
Hindsko-arabská	hindsko-arabský	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Východoarabské	Východoarabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Arabské	arabský	k2eAgFnSc2d1
číslice	číslice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1
číslice	číslice	k1gFnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
Arabské	arabský	k2eAgFnSc2d1
číslice	číslice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1
číslice	číslice	k1gFnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
systému	systém	k1gInSc2
počítání	počítání	k1gNnPc2
a	a	k8xC
čísel	číslo	k1gNnPc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
arabských	arabský	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Arabské	arabský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
kuriosity	kuriosita	k1gFnPc1
číslic	číslice	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
