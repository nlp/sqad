<s>
Zásluha	zásluha	k1gFnSc1	zásluha
za	za	k7c4	za
vytvoření	vytvoření	k1gNnSc4	vytvoření
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gFnPc4	jeho
anglické	anglický	k2eAgFnPc4d1	anglická
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
photography	photographa	k1gFnPc4	photographa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
i	i	k9	i
seznámení	seznámení	k1gNnSc1	seznámení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
slovem	slovo	k1gNnSc7	slovo
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
připisovaná	připisovaný	k2eAgFnSc1d1	připisovaná
Johnu	John	k1gMnSc3	John
Herschelovi	Herschel	k1gMnSc3	Herschel
<g/>
.	.	kIx.	.
</s>
