<s>
Nedaleko	nedaleko	k7c2
Tlos	Tlosa	k1gFnPc2
(	(	kIx(
<g/>
u	u	k7c2
Fethiye	Fethiy	k1gFnSc2
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
přítok	přítok	k1gInSc1
řeky	řeka	k1gFnSc2
Eşen	Eşen	k1gMnSc1
Çayı	Çayı	k1gMnSc1
(	(	kIx(
<g/>
zvané	zvaný	k2eAgNnSc1d1
též	též	k9
Xantos	Xantos	k1gInSc4
nebo	nebo	k8xC
Kocaçay	Kocaçay	k1gInPc4
<g/>
)	)	kIx)
ve	v	k7c6
skalnaté	skalnatý	k2eAgFnSc6d1
náhorní	náhorní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
až	až	k9
300	#num#	k4
m	m	kA
hlubokou	hluboký	k2eAgFnSc4d1
a	a	k8xC
18	#num#	k4
km	km	kA
dlouhou	dlouhý	k2eAgFnSc4d1
rokli	rokle	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejhlubším	hluboký	k2eAgNnPc3d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>