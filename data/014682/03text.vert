<s>
Saklı	Saklı	k1gInSc1
(	(	kIx(
<g/>
kaňon	kaňon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Saklı	Saklı	k1gMnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
</s>
<s>
SvětadílAsiePohoříTaurus	SvětadílAsiePohoříTaurus	k1gMnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
severjih	severjih	k1gMnSc1
</s>
<s>
StátTurecko	StátTurecko	k6eAd1
TureckoPovodíEşen	TureckoPovodíEşen	k2eAgMnSc1d1
Çayı	Çayı	k1gMnSc1
</s>
<s>
Saklı	Saklı	k1gMnSc1
</s>
<s>
Souřadnice	souřadnice	k1gFnSc1
<g/>
36	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
7,25	7,25	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
29	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
19,58	19,58	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Saklı	Saklı	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Saklı	Saklı	k1gMnSc1
Kent	Kent	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
skryté	skrytý	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
výletní	výletní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
nedaleko	nedaleko	k7c2
antického	antický	k2eAgNnSc2d1
města	město	k1gNnSc2
Tlos	Tlosa	k1gFnPc2
v	v	k7c6
turecké	turecký	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Muğ	Muğ	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nedaleko	nedaleko	k7c2
Tlos	Tlosa	k1gFnPc2
(	(	kIx(
<g/>
u	u	k7c2
Fethiye	Fethiy	k1gFnSc2
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
přítok	přítok	k1gInSc1
řeky	řeka	k1gFnSc2
Eşen	Eşen	k1gMnSc1
Çayı	Çayı	k1gMnSc1
(	(	kIx(
<g/>
zvané	zvaný	k2eAgNnSc1d1
též	též	k9
Xantos	Xantos	k1gInSc4
nebo	nebo	k8xC
Kocaçay	Kocaçay	k1gInPc4
<g/>
)	)	kIx)
ve	v	k7c6
skalnaté	skalnatý	k2eAgFnSc6d1
náhorní	náhorní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
až	až	k9
300	#num#	k4
m	m	kA
hlubokou	hluboký	k2eAgFnSc4d1
a	a	k8xC
18	#num#	k4
km	km	kA
dlouhou	dlouhý	k2eAgFnSc4d1
rokli	rokle	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejhlubším	hluboký	k2eAgNnPc3d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
v	v	k7c6
zimě	zima	k1gFnSc6
hladina	hladina	k1gFnSc1
vody	voda	k1gFnSc2
silně	silně	k6eAd1
narůstá	narůstat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
kaňon	kaňon	k1gInSc1
přístupný	přístupný	k2eAgInSc1d1
teprve	teprve	k6eAd1
od	od	k7c2
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
většina	většina	k1gFnSc1
sněhu	sníh	k1gInSc2
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Taurus	Taurus	k1gInSc4
roztála	roztát	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
kaňonu	kaňon	k1gInSc2
dlouhá	dlouhý	k2eAgFnSc1d1
4	#num#	k4
km	km	kA
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
komerčně	komerčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zaplacení	zaplacení	k1gNnSc6
vstupného	vstupné	k1gNnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
projít	projít	k5eAaPmF
prvních	první	k4xOgNnPc6
200	#num#	k4
m	m	kA
na	na	k7c4
ve	v	k7c6
skalách	skála	k1gFnPc6
zapuštěných	zapuštěný	k2eAgFnPc6d1
lávkách	lávka	k1gFnPc6
k	k	k7c3
malé	malý	k2eAgFnSc3d1
restauraci	restaurace	k1gFnSc3
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
dál	daleko	k6eAd2
pokračovat	pokračovat	k5eAaImF
s	s	k7c7
průvodcem	průvodce	k1gMnSc7
nebo	nebo	k8xC
bez	bez	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
restaurace	restaurace	k1gFnSc2
je	být	k5eAaImIp3nS
vícero	vícero	k1gNnSc1
vyvěraček	vyvěračka	k1gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
vyvěrá	vyvěrat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
kaňonu	kaňon	k1gInSc2
byl	být	k5eAaImAgInS
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1996	#num#	k4
vyhlášen	vyhlášen	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
Saklı	Saklı	k1gMnSc1
Milli	Mille	k1gFnSc4
Parkı	Parkı	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Saklı	Saklı	k1gInSc4
National	National	k1gMnPc2
Park	park	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Saklı	Saklı	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
