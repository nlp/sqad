<s>
Existuje	existovat	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
přijatý	přijatý	k2eAgInSc1d1
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Amerika	Amerika	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
italském	italský	k2eAgInSc6d1
cestovateli	cestovatel	k1gMnSc3
Amerigovi	Amerig	k1gMnSc3
Vespuccim	Vespuccim	k1gInSc4
německými	německý	k2eAgMnPc7d1
kartografy	kartograf	k1gMnPc7
Martinem	Martin	k1gMnSc7
Waldseemüllerem	Waldseemüller	k1gMnSc7
a	a	k8xC
Matthiasem	Matthias	k1gMnSc7
Ringmannem	Ringmann	k1gMnSc7
<g/>
.	.	kIx.
</s>