<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
América	América	k1gFnSc1	América
del	del	k?	del
Norte	Nort	k1gInSc5	Nort
nebo	nebo	k8xC	nebo
Norteamérica	Norteaméric	k1gInSc2	Norteaméric
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Amérique	Amérique	k1gInSc1	Amérique
du	du	k?	du
Nord	Nord	k1gInSc1	Nord
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Noord-Amerika	Noord-Amerika	k1gFnSc1	Noord-Amerika
<g/>
,	,	kIx,	,
papiamentsky	papiamentsky	k6eAd1	papiamentsky
Nort	Nort	k1gInSc1	Nort
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
grónsky	grónsky	k6eAd1	grónsky
Amerika	Amerika	k1gFnSc1	Amerika
Avannarleq	Avannarleq	k1gFnSc1	Avannarleq
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světadíl	světadíl	k1gInSc1	světadíl
nacházející	nacházející	k2eAgInSc1d1	nacházející
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
subkontinent	subkontinent	k1gInSc4	subkontinent
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Severním	severní	k2eAgMnSc7d1	severní
ledovým	ledový	k2eAgInSc7d1	ledový
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerikou	Amerika	k1gFnSc7	Amerika
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Tichým	Tichý	k1gMnSc7	Tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
území	území	k1gNnSc4	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
okolo	okolo	k7c2	okolo
24	[number]	k4	24
709	[number]	k4	709
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
po	po	k7c6	po
první	první	k4xOgFnSc6	první
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
Africe	Afrika	k1gFnSc3	Afrika
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
světadílem	světadíl	k1gInSc7	světadíl
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
4,8	[number]	k4	4,8
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
16,5	[number]	k4	16,5
%	%	kIx~	%
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Geograficky	geograficky	k6eAd1	geograficky
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
světa	svět	k1gInSc2	svět
Grónsko	Grónsko	k1gNnSc4	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
světadílu	světadíl	k1gInSc6	světadíl
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
téměř	téměř	k6eAd1	téměř
579	[number]	k4	579
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
světadílem	světadíl	k1gInSc7	světadíl
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
23	[number]	k4	23
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
a	a	k8xC	a
21	[number]	k4	21
závislých	závislý	k2eAgNnPc2d1	závislé
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
nejmenší	malý	k2eAgMnSc1d3	nejmenší
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
Navassa	Navassa	k1gFnSc1	Navassa
<g/>
,	,	kIx,	,
závislé	závislý	k2eAgNnSc1d1	závislé
území	území	k1gNnSc1	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
48	[number]	k4	48
km	km	kA	km
širokou	široký	k2eAgFnSc7d1	široká
Panamskou	panamský	k2eAgFnSc7d1	Panamská
šíjí	šíj	k1gFnSc7	šíj
napojena	napojit	k5eAaPmNgFnS	napojit
na	na	k7c4	na
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
za	za	k7c4	za
rozhraní	rozhraní	k1gNnSc4	rozhraní
obou	dva	k4xCgInPc2	dva
světadílů	světadíl	k1gInPc2	světadíl
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
až	až	k9	až
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Panamou	Panama	k1gFnSc7	Panama
a	a	k8xC	a
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
karibských	karibský	k2eAgInPc2d1	karibský
ostrovů	ostrov	k1gInPc2	ostrov
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
k	k	k7c3	k
Severní	severní	k2eAgFnSc3d1	severní
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc3	souostroví
Trinidad	Trinidad	k1gInSc4	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc4	Tobago
a	a	k8xC	a
ostrovy	ostrov	k1gInPc4	ostrov
spravované	spravovaný	k2eAgInPc4d1	spravovaný
jihoamerickými	jihoamerický	k2eAgInPc7d1	jihoamerický
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
poloostrov	poloostrov	k1gInSc1	poloostrov
Aljaška	Aljaška	k1gFnSc1	Aljaška
oddělen	oddělit	k5eAaPmNgInS	oddělit
88	[number]	k4	88
km	km	kA	km
širokou	široký	k2eAgFnSc7d1	široká
Beringovou	Beringův	k2eAgFnSc7d1	Beringova
úžinou	úžina	k1gFnSc7	úžina
od	od	k7c2	od
asijské	asijský	k2eAgFnSc2d1	asijská
Čukotky	Čukotka	k1gFnSc2	Čukotka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
pevninu	pevnina	k1gFnSc4	pevnina
navazuje	navazovat	k5eAaImIp3nS	navazovat
Kanadské	kanadský	k2eAgNnSc1d1	kanadské
arktické	arktický	k2eAgNnSc1d1	arktické
souostroví	souostroví	k1gNnSc1	souostroví
a	a	k8xC	a
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
280	[number]	k4	280
km	km	kA	km
širokým	široký	k2eAgInSc7d1	široký
Dánským	dánský	k2eAgInSc7d1	dánský
průlivem	průliv	k1gInSc7	průliv
od	od	k7c2	od
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
(	(	kIx(	(
<g/>
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
ovšem	ovšem	k9	ovšem
Island	Island	k1gInSc4	Island
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Středoatlantském	Středoatlantský	k2eAgInSc6d1	Středoatlantský
hřbetu	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
tedy	tedy	k9	tedy
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
obou	dva	k4xCgInPc2	dva
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Cape	capat	k5eAaImIp3nS	capat
Murchison	Murchison	k1gMnSc1	Murchison
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Boothia	Boothium	k1gNnSc2	Boothium
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
71	[number]	k4	71
<g/>
°	°	k?	°
50	[number]	k4	50
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Punta	punto	k1gNnSc2	punto
Mariato	Mariat	k2eAgNnSc1d1	Mariato
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Azuero	Azuero	k1gNnSc4	Azuero
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
°	°	k?	°
12	[number]	k4	12
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Cape	capat	k5eAaImIp3nS	capat
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Seward	Sewarda	k1gFnPc2	Sewarda
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
(	(	kIx(	(
<g/>
168	[number]	k4	168
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
mys	mys	k1gInSc1	mys
Charles	Charles	k1gMnSc1	Charles
na	na	k7c6	na
Labradoru	Labrador	k1gInSc6	Labrador
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
55	[number]	k4	55
<g/>
°	°	k?	°
40	[number]	k4	40
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
<g/>
Nejzazší	zadní	k2eAgInPc4d3	nejzazší
body	bod	k1gInPc4	bod
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
mys	mys	k1gInSc1	mys
Morris	Morris	k1gInSc1	Morris
Jesup	Jesup	k1gInSc1	Jesup
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
(	(	kIx(	(
<g/>
83	[number]	k4	83
<g/>
°	°	k?	°
40	[number]	k4	40
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
výběžkem	výběžek	k1gInSc7	výběžek
souše	souš	k1gFnSc2	souš
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
700	[number]	k4	700
km	km	kA	km
od	od	k7c2	od
Severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Kokosový	kokosový	k2eAgInSc1d1	kokosový
ostrov	ostrov	k1gInSc1	ostrov
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Kostariky	Kostarika	k1gFnSc2	Kostarika
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Wrangelův	Wrangelův	k2eAgInSc1d1	Wrangelův
mys	mys	k1gInSc1	mys
na	na	k7c6	na
aleutském	aleutský	k2eAgInSc6d1	aleutský
ostrově	ostrov	k1gInSc6	ostrov
Attu	Attus	k1gInSc2	Attus
(	(	kIx(	(
<g/>
172	[number]	k4	172
<g/>
°	°	k?	°
27	[number]	k4	27
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Komandorské	Komandorský	k2eAgInPc4d1	Komandorský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
na	na	k7c4	na
Aleutské	aleutský	k2eAgNnSc4d1	aleutský
souostroví	souostroví	k1gNnSc4	souostroví
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
spravované	spravovaný	k2eAgFnPc4d1	spravovaná
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
řadíme	řadit	k5eAaImIp1nP	řadit
je	on	k3xPp3gNnSc4	on
tedy	tedy	k9	tedy
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
mys	mys	k1gInSc1	mys
Nordostrundingen	Nordostrundingen	k1gInSc1	Nordostrundingen
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
°	°	k?	°
39	[number]	k4	39
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
všeobecně	všeobecně	k6eAd1	všeobecně
přijatý	přijatý	k2eAgInSc1d1	přijatý
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Amerika	Amerika	k1gFnSc1	Amerika
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
italském	italský	k2eAgInSc6d1	italský
cestovateli	cestovatel	k1gMnSc3	cestovatel
Amerigovi	Amerig	k1gMnSc3	Amerig
Vespuccim	Vespuccim	k1gInSc4	Vespuccim
německými	německý	k2eAgMnPc7d1	německý
kartografy	kartograf	k1gMnPc7	kartograf
Martinem	Martin	k1gMnSc7	Martin
Waldseemüllerem	Waldseemüller	k1gMnSc7	Waldseemüller
a	a	k8xC	a
Matthiasem	Matthias	k1gMnSc7	Matthias
Ringmannem	Ringmann	k1gMnSc7	Ringmann
<g/>
.	.	kIx.	.
</s>
<s>
Vespuccci	Vespuccce	k1gMnSc3	Vespuccce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1497	[number]	k4	1497
a	a	k8xC	a
1502	[number]	k4	1502
objevil	objevit	k5eAaPmAgInS	objevit
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Amerika	Amerika	k1gFnSc1	Amerika
není	být	k5eNaImIp3nS	být
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odlišný	odlišný	k2eAgInSc1d1	odlišný
kontinent	kontinent	k1gInSc1	kontinent
<g/>
,	,	kIx,	,
Evropanům	Evropan	k1gMnPc3	Evropan
dosud	dosud	k6eAd1	dosud
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1507	[number]	k4	1507
Waldseemüller	Waldseemüller	k1gMnSc1	Waldseemüller
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
mapu	mapa	k1gFnSc4	mapa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
umístil	umístit	k5eAaPmAgMnS	umístit
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
America	Americ	k2eAgFnSc1d1	America
<g/>
"	"	kIx"	"
na	na	k7c4	na
světadíl	světadíl	k1gInSc4	světadíl
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
do	do	k7c2	do
středu	střed	k1gInSc2	střed
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gMnSc1	původ
názvu	název	k1gInSc2	název
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
v	v	k7c6	v
doprovodné	doprovodný	k2eAgFnSc6d1	doprovodná
knize	kniha	k1gFnSc6	kniha
Cosmographiae	Cosmographiae	k1gNnSc7	Cosmographiae
Introductio	Introductio	k1gNnSc1	Introductio
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Waldseemüllera	Waldseemüller	k1gMnSc2	Waldseemüller
by	by	kYmCp3nS	by
nikdo	nikdo	k3yNnSc1	nikdo
neměl	mít	k5eNaImAgMnS	mít
námitky	námitka	k1gFnSc2	námitka
proti	proti	k7c3	proti
pojmenování	pojmenování	k1gNnSc3	pojmenování
země	zem	k1gFnSc2	zem
po	po	k7c6	po
jejím	její	k3xOp3gMnSc6	její
objeviteli	objevitel	k1gMnSc6	objevitel
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgInS	použít
latinizovanou	latinizovaný	k2eAgFnSc4d1	latinizovaná
verzi	verze	k1gFnSc4	verze
Vespucciho	Vespucci	k1gMnSc2	Vespucci
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
Americus	Americus	k1gMnSc1	Americus
Vespucius	Vespucius	k1gMnSc1	Vespucius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
ženském	ženský	k2eAgInSc6d1	ženský
tvaru	tvar	k1gInSc6	tvar
"	"	kIx"	"
<g/>
America	Americum	k1gNnSc2	Americum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
tak	tak	k8xS	tak
vzory	vzor	k1gInPc4	vzor
"	"	kIx"	"
<g/>
Europa	Europa	k1gFnSc1	Europa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Asia	Asia	k1gFnSc1	Asia
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
další	další	k2eAgMnPc1d1	další
kartografové	kartograf	k1gMnPc1	kartograf
přidávali	přidávat	k5eAaImAgMnP	přidávat
do	do	k7c2	do
map	mapa	k1gFnPc2	mapa
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
tak	tak	k6eAd1	tak
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1538	[number]	k4	1538
Gerard	Gerard	k1gMnSc1	Gerard
Mercator	Mercator	k1gMnSc1	Mercator
užil	užít	k5eAaPmAgMnS	užít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
mapě	mapa	k1gFnSc6	mapa
světa	svět	k1gInSc2	svět
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
America	America	k1gFnSc1	America
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
západní	západní	k2eAgFnSc4d1	západní
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
.	.	kIx.	.
<g/>
Odvozenina	odvozenina	k1gFnSc1	odvozenina
z	z	k7c2	z
Vespucciho	Vespucci	k1gMnSc2	Vespucci
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
však	však	k9	však
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
problematická	problematický	k2eAgFnSc1d1	problematická
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevy	objev	k1gInPc4	objev
získávají	získávat	k5eAaImIp3nP	získávat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
příjmení	příjmení	k1gNnSc2	příjmení
svého	svůj	k3xOyFgMnSc2	svůj
objevitele	objevitel	k1gMnSc2	objevitel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Ricardo	Ricardo	k1gNnSc1	Ricardo
Palma	palma	k1gFnSc1	palma
předložil	předložit	k5eAaPmAgMnS	předložit
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Amerických	americký	k2eAgFnPc6d1	americká
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Serranías	Serranías	k1gInSc1	Serranías
Amerrique	Amerriqu	k1gInSc2	Amerriqu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malém	malý	k2eAgNnSc6d1	malé
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vespucci	Vespucce	k1gFnSc4	Vespucce
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
objevil	objevit	k5eAaPmAgInS	objevit
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
také	také	k9	také
Americké	americký	k2eAgFnPc1d1	americká
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
spojuje	spojovat	k5eAaImIp3nS	spojovat
jeho	jeho	k3xOp3gInPc4	jeho
objevy	objev	k1gInPc4	objev
s	s	k7c7	s
těmi	ten	k3xDgFnPc7	ten
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
E.	E.	kA	E.
Hudd	Hudd	k1gMnSc1	Hudd
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
kontinent	kontinent	k1gInSc1	kontinent
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
velšském	velšský	k2eAgMnSc6d1	velšský
obchodníkovi	obchodník	k1gMnSc6	obchodník
Richardu	Richard	k1gMnSc6	Richard
Amerikovi	Amerik	k1gMnSc6	Amerik
z	z	k7c2	z
Bristolu	Bristol	k1gInSc2	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
podle	podle	k7c2	podle
předpokladů	předpoklad	k1gInPc2	předpoklad
<g/>
,	,	kIx,	,
financoval	financovat	k5eAaBmAgInS	financovat
výpravu	výprava	k1gFnSc4	výprava
Johna	John	k1gMnSc2	John
Cabota	Cabot	k1gMnSc2	Cabot
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
do	do	k7c2	do
Newfoundlandu	Newfoundland	k1gInSc2	Newfoundland
<g/>
,	,	kIx,	,
uskutečněnou	uskutečněný	k2eAgFnSc4d1	uskutečněná
roku	rok	k1gInSc2	rok
1497	[number]	k4	1497
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
podrobně	podrobně	k6eAd1	podrobně
probádaná	probádaný	k2eAgFnSc1d1	probádaná
domněnka	domněnka	k1gFnSc1	domněnka
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
podle	podle	k7c2	podle
španělského	španělský	k2eAgMnSc2d1	španělský
námořníka	námořník	k1gMnSc2	námořník
nesoucího	nesoucí	k2eAgMnSc2d1	nesoucí
staré	starý	k2eAgNnSc4d1	staré
vizigótské	vizigótský	k2eAgNnSc4d1	Vizigótské
jméno	jméno	k1gNnSc4	jméno
Amairick	Amairicka	k1gFnPc2	Amairicka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiná	k1gFnSc2	jiná
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
kořeny	kořen	k1gInPc7	kořen
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užívání	užívání	k1gNnSc4	užívání
termínu	termín	k1gInSc2	termín
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
různé	různý	k2eAgFnPc4d1	různá
definice	definice	k1gFnPc4	definice
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
polohou	poloha	k1gFnSc7	poloha
a	a	k8xC	a
kontextem	kontext	k1gInSc7	kontext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pojem	pojem	k1gInSc1	pojem
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
zemím	zem	k1gFnPc3	zem
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
Grónsko	Grónsko	k1gNnSc4	Grónsko
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
North	Northa	k1gFnPc2	Northa
American	American	k1gInSc1	American
Free	Fre	k1gInSc2	Fre
Trade	Trad	k1gInSc5	Trad
Agreement	Agreement	k1gMnSc1	Agreement
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostrovy	ostrov	k1gInPc1	ostrov
nedaleko	nedaleko	k7c2	nedaleko
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Iberoamerice	Iberoamerika	k1gFnSc6	Iberoamerika
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
Evropy	Evropa	k1gFnSc2	Evropa
výraz	výraz	k1gInSc4	výraz
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
označuje	označovat	k5eAaImIp3nS	označovat
subkontinent	subkontinent	k1gInSc1	subkontinent
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
často	často	k6eAd1	často
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
Saint	Saint	k1gInSc1	Saint
Pierre	Pierr	k1gInSc5	Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc4	Miquelon
a	a	k8xC	a
Bermudy	Bermudy	k1gFnPc4	Bermudy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
jinými	jiný	k2eAgInPc7d1	jiný
názvy	název	k1gInPc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
Místokrálovství	Místokrálovství	k1gNnSc1	Místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xS	jako
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
však	však	k9	však
Northern	Northern	k1gMnSc1	Northern
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
oficiální	oficiální	k2eAgNnSc1d1	oficiální
jméno	jméno	k1gNnSc1	jméno
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
celý	celý	k2eAgInSc1d1	celý
americký	americký	k2eAgInSc1d1	americký
kontinent	kontinent	k1gInSc1	kontinent
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Karibik	Karibik	k1gInSc1	Karibik
<g/>
)	)	kIx)	)
označoval	označovat	k5eAaImAgInS	označovat
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k9	jako
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
kontinentů	kontinent	k1gInPc2	kontinent
(	(	kIx(	(
<g/>
dalšími	další	k2eAgMnPc7d1	další
čtyřmi	čtyři	k4xCgNnPc7	čtyři
jsou	být	k5eAaImIp3nP	být
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
lze	lze	k6eAd1	lze
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
severojižní	severojižní	k2eAgFnSc3d1	severojižní
orientaci	orientace	k1gFnSc3	orientace
nalézt	nalézt	k5eAaBmF	nalézt
velmi	velmi	k6eAd1	velmi
různorodé	různorodý	k2eAgInPc4d1	různorodý
typy	typ	k1gInPc4	typ
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
arktických	arktický	k2eAgInPc2d1	arktický
ostrovů	ostrov	k1gInPc2	ostrov
vládne	vládnout	k5eAaImIp3nS	vládnout
polární	polární	k2eAgNnSc4d1	polární
klima	klima	k1gNnSc4	klima
tundry	tundra	k1gFnSc2	tundra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
přechází	přecházet	k5eAaImIp3nS	přecházet
postupně	postupně	k6eAd1	postupně
v	v	k7c4	v
mírný	mírný	k2eAgInSc4d1	mírný
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
zaujímající	zaujímající	k2eAgFnSc4d1	zaujímající
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Kordillery	Kordillery	k1gFnPc4	Kordillery
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
vysokohorské	vysokohorský	k2eAgNnSc1d1	vysokohorské
klima	klima	k1gNnSc1	klima
ovlivňující	ovlivňující	k2eAgNnSc1d1	ovlivňující
i	i	k9	i
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
kraje	kraj	k1gInPc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Pouště	poušť	k1gFnPc1	poušť
a	a	k8xC	a
polopouště	polopoušť	k1gFnPc1	polopoušť
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
jih	jih	k1gInSc4	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
provincie	provincie	k1gFnSc2	provincie
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
pak	pak	k6eAd1	pak
začínají	začínat	k5eAaImIp3nP	začínat
subtropy	subtropy	k1gInPc1	subtropy
a	a	k8xC	a
tropy	trop	k1gInPc1	trop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
ostrovů	ostrov	k1gInPc2	ostrov
mají	mít	k5eAaImIp3nP	mít
kromě	kromě	k7c2	kromě
polohy	poloha	k1gFnSc2	poloha
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
mořské	mořský	k2eAgInPc1d1	mořský
proudy	proud	k1gInPc1	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
Kanady	Kanada	k1gFnSc2	Kanada
===	===	k?	===
</s>
</p>
<p>
<s>
Geologicky	geologicky	k6eAd1	geologicky
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
regionů	region	k1gInPc2	region
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
prekambrických	prekambrický	k2eAgFnPc2d1	prekambrický
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
paleozoika	paleozoikum	k1gNnSc2	paleozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Kanadské	kanadský	k2eAgInPc1d1	kanadský
zdroje	zdroj	k1gInPc1	zdroj
nerostů	nerost	k1gInPc2	nerost
jsou	být	k5eAaImIp3nP	být
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
<g/>
.	.	kIx.	.
</s>
<s>
Napříč	napříč	k7c7	napříč
Kanadským	kanadský	k2eAgInSc7d1	kanadský
štítem	štít	k1gInSc7	štít
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
molybdenu	molybden	k1gInSc2	molybden
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arktické	arktický	k2eAgFnSc6d1	arktická
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
také	také	k9	také
těžba	těžba	k1gFnSc1	těžba
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Kanadu	Kanada	k1gFnSc4	Kanada
činí	činit	k5eAaImIp3nS	činit
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
producentů	producent	k1gMnPc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Kanadském	kanadský	k2eAgInSc6d1	kanadský
štítu	štít	k1gInSc6	štít
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obce	obec	k1gFnSc2	obec
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
těchto	tento	k3xDgInPc2	tento
nerostů	nerost	k1gInPc2	nerost
–	–	k?	–
největší	veliký	k2eAgFnSc7d3	veliký
a	a	k8xC	a
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	on	k3xPp3gInPc4	on
Sudbury	Sudbur	k1gInPc4	Sudbur
v	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Sudbury	Sudbura	k1gFnSc2	Sudbura
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
běžného	běžný	k2eAgInSc2d1	běžný
procesu	proces	k1gInSc2	proces
formování	formování	k1gNnSc2	formování
nerostů	nerost	k1gInPc2	nerost
ve	v	k7c6	v
štítu	štít	k1gInSc6	štít
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
totiž	totiž	k9	totiž
množství	množství	k1gNnSc1	množství
důkazů	důkaz	k1gInPc2	důkaz
potvrzujících	potvrzující	k2eAgInPc2d1	potvrzující
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sudburyjská	Sudburyjský	k2eAgFnSc1d1	Sudburyjský
pánev	pánev	k1gFnSc1	pánev
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc4d1	starý
impaktní	impaktní	k2eAgInSc4d1	impaktní
kráter	kráter	k1gInSc4	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleká	daleký	k2eNgFnSc1d1	nedaleká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
známá	známý	k2eAgFnSc1d1	známá
Temagamijská	Temagamijský	k2eAgFnSc1d1	Temagamijský
magnetická	magnetický	k2eAgFnSc1d1	magnetická
anomálie	anomálie	k1gFnSc1	anomálie
(	(	kIx(	(
<g/>
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
Wanapiteiská	Wanapiteiský	k2eAgFnSc1d1	Wanapiteiský
anomálie	anomálie	k1gFnSc1	anomálie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Sudburyjské	Sudburyjský	k2eAgFnSc3d1	Sudburyjský
pánvi	pánev	k1gFnSc3	pánev
nápadně	nápadně	k6eAd1	nápadně
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
podobnosti	podobnost	k1gFnSc3	podobnost
magnetických	magnetický	k2eAgFnPc2d1	magnetická
anomálií	anomálie	k1gFnPc2	anomálie
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
bohatý	bohatý	k2eAgInSc1d1	bohatý
kráter	kráter	k1gInSc1	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Kanadský	kanadský	k2eAgInSc1d1	kanadský
štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
také	také	k9	také
rozlehlými	rozlehlý	k2eAgInPc7d1	rozlehlý
boreálními	boreální	k2eAgInPc7d1	boreální
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
těžba	těžba	k1gFnSc1	těžba
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
odvětvím	odvětví	k1gNnSc7	odvětví
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologické	geologický	k2eAgFnSc6d1	geologická
oblasti	oblast	k1gFnSc6	oblast
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
Kontinentální	kontinentální	k2eAgInPc1d1	kontinentální
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
fyzickogeografických	fyzickogeografický	k2eAgFnPc2d1	fyzickogeografická
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kordillery	Kordillery	k1gFnPc1	Kordillery
</s>
</p>
<p>
<s>
Kanadský	kanadský	k2eAgInSc1d1	kanadský
štít	štít	k1gInSc1	štít
</s>
</p>
<p>
<s>
stabilní	stabilní	k2eAgFnSc1d1	stabilní
plošina	plošina	k1gFnSc1	plošina
</s>
</p>
<p>
<s>
přímořská	přímořský	k2eAgFnSc1d1	přímořská
rovina	rovina	k1gFnSc1	rovina
</s>
</p>
<p>
<s>
Apalačský	Apalačský	k2eAgInSc1d1	Apalačský
orogenetický	orogenetický	k2eAgInSc1d1	orogenetický
pásGeologie	pásGeologie	k1gFnPc1	pásGeologie
Aljašky	Aljaška	k1gFnSc2	Aljaška
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
té	ten	k3xDgFnSc2	ten
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hlavní	hlavní	k2eAgInPc1d1	hlavní
ostrovy	ostrov	k1gInPc1	ostrov
Havaje	Havaj	k1gFnSc2	Havaj
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
vulkánů	vulkán	k1gInPc2	vulkán
z	z	k7c2	z
období	období	k1gNnSc2	období
neogénu	neogén	k1gInSc2	neogén
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
magma	magma	k1gNnSc1	magma
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
horké	horký	k2eAgFnSc2d1	horká
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnPc4	geologie
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zemětřesením	zemětřesení	k1gNnPc3	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
Guatemalu	Guatemala	k1gFnSc4	Guatemala
velké	velká	k1gFnSc2	velká
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zabilo	zabít	k5eAaPmAgNnS	zabít
asi	asi	k9	asi
23	[number]	k4	23
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Managua	Managua	k1gFnSc1	Managua
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Nikaraguy	Nikaragua	k1gFnSc2	Nikaragua
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
a	a	k8xC	a
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
druhé	druhý	k4xOgMnPc4	druhý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
okolo	okolo	k7c2	okolo
5000	[number]	k4	5000
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Salvador	Salvador	k1gInSc1	Salvador
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
třemi	tři	k4xCgNnPc7	tři
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Kostarice	Kostarika	k1gFnSc6	Kostarika
zabilo	zabít	k5eAaPmAgNnS	zabít
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
nejméně	málo	k6eAd3	málo
34	[number]	k4	34
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
postihlo	postihnout	k5eAaPmAgNnS	postihnout
Honduras	Honduras	k1gInSc4	Honduras
silné	silný	k2eAgNnSc4d1	silné
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
sedm	sedm	k4xCc1	sedm
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sopečné	sopečný	k2eAgFnPc1d1	sopečná
erupce	erupce	k1gFnPc1	erupce
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k9	také
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
erupci	erupce	k1gFnSc3	erupce
sopky	sopka	k1gFnSc2	sopka
Arenal	Arenal	k1gFnSc2	Arenal
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
smrt	smrt	k1gFnSc4	smrt
celkem	celkem	k6eAd1	celkem
87	[number]	k4	87
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Úrodné	úrodný	k2eAgFnPc1d1	úrodná
půdy	půda	k1gFnPc1	půda
ze	z	k7c2	z
zvětralé	zvětralý	k2eAgFnSc2d1	zvětralá
lávy	láva	k1gFnSc2	láva
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
udržovat	udržovat	k5eAaImF	udržovat
husté	hustý	k2eAgNnSc4d1	husté
zalidnění	zalidnění	k1gNnSc4	zalidnění
v	v	k7c6	v
zemědělsky	zemědělsky	k6eAd1	zemědělsky
produktivních	produktivní	k2eAgFnPc6d1	produktivní
vysočinách	vysočina	k1gFnPc6	vysočina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
de	de	k?	de
Chiapas	Chiapasa	k1gFnPc2	Chiapasa
<g/>
,	,	kIx,	,
Cordillera	Cordiller	k1gMnSc2	Cordiller
Isabelia	Isabelius	k1gMnSc2	Isabelius
a	a	k8xC	a
Cordillera	Cordiller	k1gMnSc2	Cordiller
de	de	k?	de
Talamanca	Talamancus	k1gMnSc2	Talamancus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pohořími	pohoří	k1gNnPc7	pohoří
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
úrodná	úrodný	k2eAgNnPc1d1	úrodné
údolí	údolí	k1gNnPc1	údolí
vhodná	vhodný	k2eAgNnPc1d1	vhodné
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
populace	populace	k1gFnSc2	populace
Hondurasu	Honduras	k1gInSc2	Honduras
<g/>
,	,	kIx,	,
Kostariky	Kostarika	k1gFnSc2	Kostarika
a	a	k8xC	a
Guatemaly	Guatemala	k1gFnSc2	Guatemala
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
příznivé	příznivý	k2eAgFnPc1d1	příznivá
také	také	k9	také
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
fazolí	fazole	k1gFnPc2	fazole
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Převažující	převažující	k2eAgInPc1d1	převažující
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Angloamerika	Angloamerika	k1gFnSc1	Angloamerika
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc2d1	mluvící
zemí	zem	k1gFnPc2	zem
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
oficiální	oficiální	k2eAgMnPc4d1	oficiální
zároveň	zároveň	k6eAd1	zároveň
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Belize	Belize	k1gFnSc2	Belize
a	a	k8xC	a
části	část	k1gFnSc2	část
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
dalších	další	k2eAgNnPc2d1	další
území	území	k1gNnPc2	území
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
i	i	k8xC	i
jihu	jih	k1gInSc2	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
portugalština	portugalština	k1gFnSc1	portugalština
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgFnPc1d1	mluvící
země	zem	k1gFnPc1	zem
obvykle	obvykle	k6eAd1	obvykle
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
nejsou	být	k5eNaImIp3nP	být
<g/>
)	)	kIx)	)
převládají	převládat	k5eAaImIp3nP	převládat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
Belize	Belize	k1gFnSc2	Belize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
Karibiku	Karibik	k1gInSc2	Karibik
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
či	či	k8xC	či
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgNnSc4d1	mluvící
území	území	k1gNnSc4	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc4	Mexiko
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
vyjímaje	vyjímaje	k7c4	vyjímaje
Guyanu	Guyana	k1gFnSc4	Guyana
<g/>
,	,	kIx,	,
Surinam	Surinam	k1gInSc4	Surinam
<g/>
,	,	kIx,	,
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Guyanu	Guyana	k1gFnSc4	Guyana
a	a	k8xC	a
Falklandské	Falklandský	k2eAgInPc4d1	Falklandský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
jazyk	jazyk	k1gInSc1	jazyk
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
hlavním	hlavní	k2eAgInSc7d1	hlavní
komunikačním	komunikační	k2eAgInSc7d1	komunikační
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
dvoujazyčná	dvoujazyčný	k2eAgFnSc1d1	dvoujazyčná
–	–	k?	–
francouzština	francouzština	k1gFnSc1	francouzština
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gInSc1	Québec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
mluví	mluvit	k5eAaImIp3nS	mluvit
95	[number]	k4	95
<g/>
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
jako	jako	k8xC	jako
svým	svůj	k3xOyFgMnPc3	svůj
prvním	první	k4xOgInSc7	první
nebo	nebo	k8xC	nebo
druhým	druhý	k4xOgInSc7	druhý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
angličtinou	angličtina	k1gFnSc7	angličtina
oficiální	oficiální	k2eAgFnSc7d1	oficiální
řečí	řeč	k1gFnSc7	řeč
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Nový	nový	k2eAgInSc4d1	nový
Brunšvik	Brunšvik	k1gInSc4	Brunšvik
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
francouzsky	francouzsky	k6eAd1	francouzsky
mluvícími	mluvící	k2eAgFnPc7d1	mluvící
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
provincie	provincie	k1gFnSc1	provincie
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
,	,	kIx,	,
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
Antily	Antily	k1gFnPc1	Antily
a	a	k8xC	a
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc4	Miquelon
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
americká	americký	k2eAgFnSc1d1	americká
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
francouzština	francouzština	k1gFnSc1	francouzština
úředním	úřední	k2eAgMnSc7d1	úřední
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Haiti	Haiti	k1gNnSc1	Haiti
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
zahrnuto	zahrnut	k2eAgNnSc1d1	zahrnuto
kvůli	kvůli	k7c3	kvůli
historickému	historický	k2eAgInSc3d1	historický
svazku	svazek	k1gInSc3	svazek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obyvatelé	obyvatel	k1gMnPc1	obyvatel
hovoří	hovořit	k5eAaImIp3nP	hovořit
jak	jak	k6eAd1	jak
běžnou	běžný	k2eAgFnSc7d1	běžná
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
kreolizovanou	kreolizovaný	k2eAgFnSc7d1	kreolizovaná
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
kreolizované	kreolizovaný	k2eAgInPc1d1	kreolizovaný
jazyky	jazyk	k1gInPc1	jazyk
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Lucii	Lucie	k1gFnSc6	Lucie
a	a	k8xC	a
Dominice	Dominika	k1gFnSc6	Dominika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
jsou	být	k5eAaImIp3nP	být
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nejbohatšími	bohatý	k2eAgFnPc7d3	nejbohatší
a	a	k8xC	a
nejrozvinutějšími	rozvinutý	k2eAgFnPc7d3	nejrozvinutější
zeměmi	zem	k1gFnPc7	zem
na	na	k7c6	na
světadílu	světadíl	k1gInSc6	světadíl
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
industrializovaná	industrializovaný	k2eAgFnSc1d1	industrializovaná
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Karibiku	Karibik	k1gInSc2	Karibik
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
různé	různý	k2eAgFnSc6d1	různá
úrovni	úroveň	k1gFnSc6	úroveň
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
a	a	k8xC	a
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
malé	malý	k2eAgFnPc1d1	malá
karibské	karibský	k2eAgFnPc1d1	karibská
ostrovní	ostrovní	k2eAgFnPc1d1	ostrovní
země	zem	k1gFnPc1	zem
jako	jako	k8xS	jako
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gMnSc1	Tobago
a	a	k8xC	a
Antigua	Antigua	k1gMnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gMnSc1	Barbuda
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgMnPc1d2	vyšší
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
než	než	k8xS	než
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jejich	jejich	k3xOp3gFnSc7	jejich
menší	malý	k2eAgFnSc7d2	menší
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Panama	Panama	k1gFnSc1	Panama
a	a	k8xC	a
Kostarika	Kostarika	k1gFnSc1	Kostarika
mají	mít	k5eAaImIp3nP	mít
významně	významně	k6eAd1	významně
vyšší	vysoký	k2eAgInSc4d2	vyšší
index	index	k1gInSc4	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
HDP	HDP	kA	HDP
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
států	stát	k1gInPc2	stát
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etnicky	etnicky	k6eAd1	etnicky
je	být	k5eAaImIp3nS	být
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
zcela	zcela	k6eAd1	zcela
svébytný	svébytný	k2eAgInSc4d1	svébytný
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Třemi	tři	k4xCgFnPc7	tři
hlavními	hlavní	k2eAgFnPc7d1	hlavní
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
černoši	černoch	k1gMnPc1	černoch
a	a	k8xC	a
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
méně	málo	k6eAd2	málo
početných	početný	k2eAgFnPc2d1	početná
skupin	skupina	k1gFnPc2	skupina
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
indiáni	indián	k1gMnPc1	indián
a	a	k8xC	a
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociálně	sociálně	k6eAd1	sociálně
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
představuje	představovat	k5eAaImIp3nS	představovat
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
dobře	dobře	k6eAd1	dobře
definovaný	definovaný	k2eAgInSc4d1	definovaný
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
sdílejí	sdílet	k5eAaImIp3nP	sdílet
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
toho	ten	k3xDgMnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
jsou	být	k5eAaImIp3nP	být
bývalé	bývalý	k2eAgFnSc2d1	bývalá
britské	britský	k2eAgFnSc2d1	britská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
silným	silný	k2eAgFnPc3d1	silná
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
a	a	k8xC	a
historickým	historický	k2eAgFnPc3d1	historická
vazbám	vazba	k1gFnPc3	vazba
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
národy	národ	k1gInPc7	národ
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
společný	společný	k2eAgInSc1d1	společný
kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
trh	trh	k1gInSc1	trh
<g/>
.	.	kIx.	.
</s>
<s>
Španělsky	španělsky	k6eAd1	španělsky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
část	část	k1gFnSc1	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
sdílí	sdílet	k5eAaImIp3nS	sdílet
společnou	společný	k2eAgFnSc4d1	společná
minulost	minulost	k1gFnSc4	minulost
jako	jako	k8xS	jako
dřívější	dřívější	k2eAgFnSc2d1	dřívější
španělské	španělský	k2eAgFnSc2d1	španělská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
zemích	zem	k1gFnPc6	zem
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
civilizace	civilizace	k1gFnPc1	civilizace
jako	jako	k8xC	jako
Mayové	Mayová	k1gFnPc1	Mayová
<g/>
,	,	kIx,	,
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
tradice	tradice	k1gFnSc2	tradice
i	i	k9	i
navzdory	navzdory	k7c3	navzdory
moderní	moderní	k2eAgFnSc3d1	moderní
době	doba	k1gFnSc3	doba
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
španělsky	španělsky	k6eAd1	španělsky
mluvící	mluvící	k2eAgInPc1d1	mluvící
státy	stát	k1gInPc1	stát
karibských	karibský	k2eAgInPc2d1	karibský
ostrovů	ostrov	k1gInPc2	ostrov
mají	mít	k5eAaImIp3nP	mít
historicky	historicky	k6eAd1	historicky
více	hodně	k6eAd2	hodně
společného	společný	k2eAgInSc2d1	společný
díky	dík	k1gInPc7	dík
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
blízkosti	blízkost	k1gFnSc2	blízkost
a	a	k8xC	a
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
získaly	získat	k5eAaPmAgFnP	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgNnSc4d1	severní
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
městech	město	k1gNnPc6	město
Monterrey	Monterrea	k1gFnSc2	Monterrea
<g/>
,	,	kIx,	,
Tijuana	Tijuana	k1gFnSc1	Tijuana
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
Juárez	Juáreza	k1gFnPc2	Juáreza
a	a	k8xC	a
Mexicali	Mexicali	k1gFnPc2	Mexicali
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
uvedených	uvedený	k2eAgNnPc2d1	uvedené
měst	město	k1gNnPc2	město
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c4	na
Monterrey	Monterrea	k1gFnPc4	Monterrea
pohlíženo	pohlížen	k2eAgNnSc1d1	pohlíženo
jako	jako	k9	jako
na	na	k7c4	na
nejvíce	hodně	k6eAd3	hodně
amerikanizované	amerikanizovaný	k2eAgNnSc4d1	amerikanizované
město	město	k1gNnSc4	město
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalectví	přistěhovalectví	k1gNnSc4	přistěhovalectví
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
podstatným	podstatný	k2eAgInSc7d1	podstatný
rysem	rys	k1gInSc7	rys
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
blízko	blízko	k7c2	blízko
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Anglofonní	anglofonní	k2eAgInPc1d1	anglofonní
karibské	karibský	k2eAgInPc1d1	karibský
státy	stát	k1gInPc1	stát
zažily	zažít	k5eAaPmAgInP	zažít
úpadek	úpadek	k1gInSc4	úpadek
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc2	jeho
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
region	region	k1gInSc4	region
a	a	k8xC	a
nahrazení	nahrazení	k1gNnSc2	nahrazení
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
vlivem	vliv	k1gInSc7	vliv
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
zapříčiněn	zapříčiněn	k2eAgInSc1d1	zapříčiněn
částečně	částečně	k6eAd1	částečně
nízkou	nízký	k2eAgFnSc7d1	nízká
populací	populace	k1gFnSc7	populace
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
anglofonních	anglofonní	k2eAgFnPc2d1	anglofonní
karibských	karibský	k2eAgFnPc2d1	karibská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
také	také	k9	také
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
emigranti	emigrant	k1gMnPc1	emigrant
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
jsou	být	k5eAaImIp3nP	být
početnější	početní	k2eAgFnPc1d2	početnější
než	než	k8xS	než
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
zůstávající	zůstávající	k2eAgFnSc2d1	zůstávající
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Populace	populace	k1gFnSc2	populace
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
zalidněnou	zalidněný	k2eAgFnSc7d1	zalidněná
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
s	s	k7c7	s
303	[number]	k4	303
606	[number]	k4	606
020	[number]	k4	020
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
USA	USA	kA	USA
následuje	následovat	k5eAaImIp3nS	následovat
Mexiko	Mexiko	k1gNnSc1	Mexiko
s	s	k7c7	s
112	[number]	k4	112
322	[number]	k4	322
757	[number]	k4	757
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
udržující	udržující	k2eAgFnPc4d1	udržující
si	se	k3xPyFc3	se
populaci	populace	k1gFnSc4	populace
okolo	okolo	k7c2	okolo
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
populací	populace	k1gFnSc7	populace
32	[number]	k4	32
623	[number]	k4	623
490	[number]	k4	490
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
třetím	třetí	k4xOgNnSc6	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Karibských	karibský	k2eAgInPc2d1	karibský
ostrovů	ostrov	k1gInPc2	ostrov
má	mít	k5eAaImIp3nS	mít
národní	národní	k2eAgFnSc4d1	národní
populaci	populace	k1gFnSc4	populace
pod	pod	k7c4	pod
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
,	,	kIx,	,
Portoriko	Portoriko	k1gNnSc1	Portoriko
-	-	kIx~	-
teritorium	teritorium	k1gNnSc4	teritorium
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
a	a	k8xC	a
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k6eAd1	Tobago
mají	mít	k5eAaImIp3nP	mít
populaci	populace	k1gFnSc4	populace
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
milion	milion	k4xCgInSc4	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osm	osm	k4xCc1	osm
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
deseti	deset	k4xCc2	deset
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
metropolitní	metropolitní	k2eAgFnPc1d1	metropolitní
oblasti	oblast	k1gFnPc1	oblast
mají	mít	k5eAaImIp3nP	mít
okolo	okolo	k7c2	okolo
5,5	[number]	k4	5,5
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
a	a	k8xC	a
Dallas	Dallas	k1gInSc1	Dallas
<g/>
–	–	k?	–
<g/>
Fort	Fort	k?	Fort
Worth	Worth	k1gInSc1	Worth
metroplex	metroplex	k1gInSc1	metroplex
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
největších	veliký	k2eAgFnPc2d3	veliký
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
jsou	být	k5eAaImIp3nP	být
uvnitř	uvnitř	k7c2	uvnitř
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
hostí	hostit	k5eAaImIp3nS	hostit
největší	veliký	k2eAgFnSc4d3	veliký
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
-	-	kIx~	-
Greater	Greater	k1gInSc1	Greater
Mexico	Mexico	k1gNnSc1	Mexico
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
zase	zase	k9	zase
najdeme	najít	k5eAaPmIp1nP	najít
metropolitní	metropolitní	k2eAgNnSc4d1	metropolitní
území	území	k1gNnSc4	území
Velké	velký	k2eAgNnSc1d1	velké
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
5,5	[number]	k4	5,5
milióny	milión	k4xCgInPc7	milión
lidmi	člověk	k1gMnPc7	člověk
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
největších	veliký	k2eAgFnPc2d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Kanady	Kanada	k1gFnSc2	Kanada
-	-	kIx~	-
USA	USA	kA	USA
a	a	k8xC	a
Mexika	Mexiko	k1gNnSc2	Mexiko
-	-	kIx~	-
USA	USA	kA	USA
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Panamericana	Panamericana	k1gFnSc1	Panamericana
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
sítě	síť	k1gFnSc2	síť
cest	cesta	k1gFnPc2	cesta
téměř	téměř	k6eAd1	téměř
48	[number]	k4	48
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
<g/>
,	,	kIx,	,
vedoucích	vedoucí	k2eAgInPc2d1	vedoucí
přes	přes	k7c4	přes
pevninské	pevninský	k2eAgInPc4d1	pevninský
státy	stát	k1gInPc4	stát
celého	celý	k2eAgInSc2d1	celý
Amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
Panamericany	Panamericana	k1gFnSc2	Panamericana
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
daná	daný	k2eAgFnSc1d1	daná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlády	vláda	k1gFnPc1	vláda
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
neoznačily	označit	k5eNaPmAgFnP	označit
specifické	specifický	k2eAgFnPc4d1	specifická
cesty	cesta	k1gFnPc4	cesta
jako	jako	k8xC	jako
části	část	k1gFnPc4	část
Panamericany	Panamericana	k1gFnSc2	Panamericana
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Mexiko	Mexiko	k1gNnSc1	Mexiko
má	mít	k5eAaImIp3nS	mít
oficiálně	oficiálně	k6eAd1	oficiálně
mnoho	mnoho	k4c1	mnoho
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
napojujících	napojující	k2eAgMnPc2d1	napojující
se	se	k3xPyFc4	se
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
části	část	k1gFnSc2	část
Panamericany	Panamericana	k1gFnSc2	Panamericana
je	být	k5eAaImIp3nS	být
však	však	k9	však
zhruba	zhruba	k6eAd1	zhruba
26	[number]	k4	26
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
transkontinentální	transkontinentální	k2eAgFnPc1d1	transkontinentální
železnice	železnice	k1gFnPc1	železnice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
napříč	napříč	k7c7	napříč
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Propojila	propojit	k5eAaPmAgFnS	propojit
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
východu	východ	k1gInSc2	východ
USA	USA	kA	USA
s	s	k7c7	s
Kalifornií	Kalifornie	k1gFnSc7	Kalifornie
na	na	k7c6	na
tichomořském	tichomořský	k2eAgNnSc6d1	Tichomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1869	[number]	k4	1869
na	na	k7c4	na
Promontory	Promontor	k1gMnPc4	Promontor
Summit	summit	k1gInSc1	summit
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
celonárodní	celonárodní	k2eAgFnSc4d1	celonárodní
mechanizovanou	mechanizovaný	k2eAgFnSc4d1	mechanizovaná
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
revolucionovala	revolucionovat	k5eAaBmAgFnS	revolucionovat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
ekonomiku	ekonomika	k1gFnSc4	ekonomika
amerického	americký	k2eAgInSc2d1	americký
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
napomáhajíc	napomáhat	k5eAaImSgNnS	napomáhat
přeměně	přeměna	k1gFnSc6	přeměna
na	na	k7c4	na
moderní	moderní	k2eAgInSc4d1	moderní
dopravní	dopravní	k2eAgInSc4d1	dopravní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
nesčetných	sčetný	k2eNgFnPc2d1	nesčetná
východoamerických	východoamerický	k2eAgFnPc2d1	východoamerický
železnic	železnice	k1gFnPc2	železnice
s	s	k7c7	s
pacifickými	pacifický	k2eAgInPc7d1	pacifický
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
statusu	status	k1gInSc2	status
první	první	k4xOgFnSc2	první
transkontinentální	transkontinentální	k2eAgFnSc2d1	transkontinentální
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nebylo	být	k5eNaImAgNnS	být
většího	veliký	k2eAgInSc2d2	veliký
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
Grand	grand	k1gMnSc1	grand
Trunk	Trunk	k?	Trunk
Railway	Railwaa	k1gFnSc2	Railwaa
(	(	kIx(	(
<g/>
GTR	GTR	kA	GTR
<g/>
)	)	kIx)	)
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
055	[number]	k4	055
kilometrů	kilometr	k1gInPc2	kilometr
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Propojovala	propojovat	k5eAaImAgFnS	propojovat
Portland	Portland	k1gInSc4	Portland
<g/>
,	,	kIx,	,
Maine	Main	k1gInSc5	Main
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
severní	severní	k2eAgInPc1d1	severní
státy	stát	k1gInPc1	stát
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
s	s	k7c7	s
kanadskými	kanadský	k2eAgFnPc7d1	kanadská
atlantickými	atlantický	k2eAgFnPc7d1	Atlantická
provinciemi	provincie	k1gFnPc7	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komunikace	komunikace	k1gFnSc1	komunikace
===	===	k?	===
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
státy	stát	k1gInPc1	stát
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
na	na	k7c4	na
sdílení	sdílení	k1gNnSc4	sdílení
telefonního	telefonní	k2eAgInSc2d1	telefonní
systému	systém	k1gInSc2	systém
známého	známý	k2eAgMnSc2d1	známý
jako	jako	k8xC	jako
North	North	k1gInSc1	North
American	American	k1gInSc1	American
Numbering	Numbering	k1gInSc1	Numbering
Plan	plan	k1gInSc1	plan
(	(	kIx(	(
<g/>
NAPA	napa	k1gFnSc1	napa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
integrovaný	integrovaný	k2eAgInSc4d1	integrovaný
systém	systém	k1gInSc4	systém
telefonních	telefonní	k2eAgNnPc2d1	telefonní
čísel	číslo	k1gNnPc2	číslo
čtyřiadvaceti	čtyřiadvacet	k4xCc2	čtyřiadvacet
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
:	:	kIx,	:
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Bermud	Bermudy	k1gFnPc2	Bermudy
a	a	k8xC	a
šestnácti	šestnáct	k4xCc2	šestnáct
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Regiony	region	k1gInPc4	region
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Státy	stát	k1gInPc1	stát
a	a	k8xC	a
závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
kontinentu	kontinent	k1gInSc6	kontinent
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
velké	velký	k2eAgInPc1d1	velký
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
lidnaté	lidnatý	k2eAgInPc1d1	lidnatý
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
</s>
</p>
<p>
<s>
MexikoKanada	MexikoKanada	k1gFnSc1	MexikoKanada
a	a	k8xC	a
Aljaška	Aljaška	k1gFnSc1	Aljaška
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k9	i
u	u	k7c2	u
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Aljašce	Aljaška	k1gFnSc6	Aljaška
a	a	k8xC	a
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
Kanady	Kanada	k1gFnSc2	Kanada
žijí	žít	k5eAaImIp3nP	žít
Eskymáci	Eskymák	k1gMnPc1	Eskymák
(	(	kIx(	(
<g/>
Inuité	Inuita	k1gMnPc1	Inuita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
menších	malý	k2eAgInPc2d2	menší
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Belize	Belize	k6eAd1	Belize
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
</s>
</p>
<p>
<s>
Honduras	Honduras	k1gInSc1	Honduras
</s>
</p>
<p>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
</s>
</p>
<p>
<s>
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
</s>
</p>
<p>
<s>
Panama	Panama	k1gFnSc1	Panama
</s>
</p>
<p>
<s>
SalvadorV	SalvadorV	k?	SalvadorV
Panamě	Panama	k1gFnSc6	Panama
je	být	k5eAaImIp3nS	být
pevninská	pevninský	k2eAgFnSc1d1	pevninská
šíje	šíje	k1gFnSc1	šíje
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
Panamský	panamský	k2eAgInSc1d1	panamský
průplav	průplav	k1gInSc1	průplav
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
s	s	k7c7	s
Karibským	karibský	k2eAgNnSc7d1	Karibské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Atlantikem	Atlantik	k1gInSc7	Atlantik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
ostrovních	ostrovní	k2eAgInPc2d1	ostrovní
států	stát	k1gInPc2	stát
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
tzv.	tzv.	kA	tzv.
Velkých	velká	k1gFnPc2	velká
a	a	k8xC	a
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
řetězec	řetězec	k1gInSc1	řetězec
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
Floridy	Florida	k1gFnSc2	Florida
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
jihoamerického	jihoamerický	k2eAgNnSc2d1	jihoamerické
venezuelského	venezuelský	k2eAgNnSc2d1	venezuelské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
</s>
</p>
<p>
<s>
Bahamy	Bahamy	k1gFnPc1	Bahamy
</s>
</p>
<p>
<s>
Barbados	Barbados	k1gMnSc1	Barbados
</s>
</p>
<p>
<s>
Dominika	Dominik	k1gMnSc4	Dominik
</s>
</p>
<p>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Grenada	Grenada	k1gFnSc1	Grenada
</s>
</p>
<p>
<s>
Haiti	Haiti	k1gNnSc1	Haiti
</s>
</p>
<p>
<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
</s>
</p>
<p>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
</s>
</p>
<p>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnPc1	grenadina
</s>
</p>
<p>
<s>
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
TobagoKromě	TobagoKromě	k1gFnSc1	TobagoKromě
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
především	především	k6eAd1	především
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
závislých	závislý	k2eAgNnPc2d1	závislé
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
zemí	zem	k1gFnSc7	zem
provázána	provázat	k5eAaPmNgFnS	provázat
různým	různý	k2eAgInSc7d1	různý
stupněm	stupeň	k1gInSc7	stupeň
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgFnSc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Nadřazené	nadřazený	k2eAgInPc4d1	nadřazený
politické	politický	k2eAgInPc4d1	politický
celky	celek	k1gInPc4	celek
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
jsou	být	k5eAaImIp3nP	být
evropské	evropský	k2eAgInPc1d1	evropský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
kolonizovaly	kolonizovat	k5eAaBmAgFnP	kolonizovat
nová	nový	k2eAgNnPc1d1	nové
území	území	k1gNnPc1	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
GrónskoSpojené	grónskospojený	k2eAgNnSc1d1	grónskospojený
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
–	–	k?	–
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
americké	americký	k2eAgFnPc1d1	americká
državy	država	k1gFnPc1	država
mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
Zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anguilla	Anguilla	k6eAd1	Anguilla
</s>
</p>
<p>
<s>
Bermudy	Bermudy	k1gFnPc1	Bermudy
</s>
</p>
<p>
<s>
Britské	britský	k2eAgInPc1d1	britský
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
Kajmanské	Kajmanský	k2eAgInPc1d1	Kajmanský
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
Montserrat	Montserrat	k1gMnSc1	Montserrat
</s>
</p>
<p>
<s>
Turks	Turks	k1gInSc1	Turks
a	a	k8xC	a
CaicosNizozemské	CaicosNizozemský	k2eAgNnSc1d1	CaicosNizozemský
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Aruba	Aruba	k1gFnSc1	Aruba
</s>
</p>
<p>
<s>
Curaçao	curaçao	k1gNnSc1	curaçao
</s>
</p>
<p>
<s>
Bonaire	Bonair	k1gMnSc5	Bonair
</s>
</p>
<p>
<s>
Saba	Saba	k6eAd1	Saba
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
EustachFrancie	EustachFrancie	k1gFnSc2	EustachFrancie
</s>
</p>
<p>
<s>
Guadeloupe	Guadeloupat	k5eAaPmIp3nS	Guadeloupat
–	–	k?	–
zámořský	zámořský	k2eAgInSc4d1	zámořský
departement	departement	k1gInSc4	departement
a	a	k8xC	a
region	region	k1gInSc4	region
DOM-ROM	DOM-ROM	k1gFnSc2	DOM-ROM
</s>
</p>
<p>
<s>
Martinik	Martinik	k1gInSc1	Martinik
–	–	k?	–
zámořský	zámořský	k2eAgInSc1d1	zámořský
departement	departement	k1gInSc1	departement
a	a	k8xC	a
region	region	k1gInSc1	region
DOM-ROM	DOM-ROM	k1gFnSc2	DOM-ROM
</s>
</p>
<p>
<s>
Saint-Pierre	Saint-Pierr	k1gMnSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc1	Miquelon
–	–	k?	–
zámořské	zámořský	k2eAgNnSc4d1	zámořské
společenství	společenství	k1gNnSc4	společenství
COM	COM	kA	COM
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
–	–	k?	–
zámořské	zámořský	k2eAgNnSc1d1	zámořské
společenství	společenství	k1gNnSc1	společenství
COM	COM	kA	COM
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
<g/>
–	–	k?	–
zámořské	zámořský	k2eAgNnSc4d1	zámořské
společenství	společenství	k1gNnSc4	společenství
COMSpojené	COMSpojený	k2eAgInPc4d1	COMSpojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
–	–	k?	–
obě	dva	k4xCgFnPc1	dva
jejich	jejich	k3xOp3gInPc4	jejich
území	území	k1gNnSc4	území
v	v	k7c6	v
karibské	karibský	k2eAgFnSc6d1	karibská
oblasti	oblast	k1gFnSc6	oblast
mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
ostrovní	ostrovní	k2eAgNnSc4d1	ostrovní
území	území	k1gNnSc4	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
území	území	k1gNnPc1	území
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Portoriko	Portoriko	k1gNnSc1	Portoriko
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
přidružený	přidružený	k2eAgInSc1d1	přidružený
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Baar	Baar	k1gMnSc1	Baar
<g/>
:	:	kIx,	:
Anglosaská	anglosaský	k2eAgFnSc1d1	anglosaská
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Ateliér	ateliér	k1gInSc1	ateliér
Milata	Milata	k1gFnSc1	Milata
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Asikinack	Asikinack	k1gMnSc1	Asikinack
<g/>
,	,	kIx,	,
Kate	kat	k1gInSc5	kat
Scarborough	Scarborough	k1gInSc1	Scarborough
<g/>
:	:	kIx,	:
Objevování	objevování	k1gNnSc1	objevování
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
INA	INA	kA	INA
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85680-80-7	[number]	k4	80-85680-80-7
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Reader	Reader	k1gInSc1	Reader
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Digest	Digest	k1gInSc1	Digest
Výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86196-56-9	[number]	k4	80-86196-56-9
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
unie	unie	k1gFnSc1	unie
</s>
</p>
<p>
<s>
Laurentie	Laurentie	k1gFnSc1	Laurentie
</s>
</p>
<p>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
