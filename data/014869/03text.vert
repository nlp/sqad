<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
</s>
<s>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Microsoft	Microsoft	kA
Datum	datum	k1gNnSc1
uvedení	uvedení	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
Typ	typa	k1gFnPc2
</s>
<s>
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
Generace	generace	k1gFnSc2
</s>
<s>
osmá	osmý	k4xOgFnSc1
generace	generace	k1gFnSc1
Prodáno	prodán	k2eAgNnSc1d1
kusů	kus	k1gInPc2
</s>
<s>
40	#num#	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
k	k	k7c3
listopadu	listopad	k1gInSc3
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Procesor	procesor	k1gInSc1
</s>
<s>
APU	APU	kA
AMD	AMD	kA
Jaguar	Jaguar	k1gMnSc1
osmijádrový	osmijádrový	k2eAgMnSc1d1
Paměť	paměť	k1gFnSc4
</s>
<s>
8	#num#	k4
GB	GB	kA
DDR3	DDR3	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
GB	GB	kA
vyhrazeno	vyhrazen	k2eAgNnSc1d1
pro	pro	k7c4
hry	hra	k1gFnPc4
<g/>
)	)	kIx)
Úložiště	úložiště	k1gNnSc1
</s>
<s>
500	#num#	k4
GB	GB	kA
-	-	kIx~
1	#num#	k4
TB	TB	kA
-	-	kIx~
2	#num#	k4
TB	TB	kA
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
Média	médium	k1gNnSc2
</s>
<s>
Blu-ray	Blu-ray	k1gInPc1
<g/>
,	,	kIx,
DVD	DVD	kA
<g/>
,	,	kIx,
CD	CD	kA
Online	Onlin	k1gInSc5
služba	služba	k1gFnSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
Live	Liv	k1gFnSc2
Zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
</s>
<s>
zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
s	s	k7c7
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Xbox	Xbox	k1gInSc1
(	(	kIx(
<g/>
některé	některý	k3yIgFnPc4
hry	hra	k1gFnPc4
<g/>
)	)	kIx)
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Xbox	Xbox	k1gInSc4
360	#num#	k4
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
Series	Series	k1gInSc1
X	X	kA
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
je	být	k5eAaImIp3nS
videoherní	videoherní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
vyvinula	vyvinout	k5eAaPmAgFnS
a	a	k8xC
uvedla	uvést	k5eAaPmAgFnS
na	na	k7c4
trh	trh	k1gInSc4
společnost	společnost	k1gFnSc1
Microsoft	Microsoft	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
<g/>
,	,	kIx,
nástupce	nástupce	k1gMnSc1
Xbox	Xbox	k1gInSc4
360	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
oznámen	oznámit	k5eAaPmNgInS
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
a	a	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c6
třetí	třetí	k4xOgFnSc6
konzoli	konzole	k1gFnSc6
z	z	k7c2
rodiny	rodina	k1gFnSc2
Xbox	Xbox	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
osmé	osmý	k4xOgFnSc2
generace	generace	k1gFnSc2
videoherních	videoherní	k2eAgNnPc2d1
konzolí	konzolí	k1gNnPc2
soutěží	soutěž	k1gFnPc2
na	na	k7c6
trhu	trh	k1gInSc6
s	s	k7c7
Playstation	Playstation	k1gInSc4
4	#num#	k4
od	od	k7c2
Sony	Sony	kA
a	a	k8xC
Wii	Wii	k1gFnSc1
U	u	k7c2
se	se	k3xPyFc4
Switch	Switch	k1gInSc1
od	od	k7c2
Nintenda	Nintend	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
na	na	k7c4
trh	trh	k1gInSc4
20131122	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
několika	několik	k4yIc6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Japonsku	Japonsko	k1gNnSc6
a	a	k8xC
zbývajících	zbývající	k2eAgFnPc6d1
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
září	září	k1gNnSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Microsoftu	Microsoft	k1gInSc2
a	a	k8xC
různých	různý	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
označováno	označován	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
zábavní	zábavní	k2eAgInSc4d1
systém	systém	k1gInSc4
v	v	k7c6
jednom	jeden	k4xCgNnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jej	on	k3xPp3gMnSc4
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
role	role	k1gFnSc2
konkurenta	konkurent	k1gMnSc2
ostatních	ostatní	k2eAgMnPc2d1
domácích	domácí	k1gMnPc2
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
platformy	platforma	k1gFnPc1
Apple	Apple	kA
TV	TV	kA
a	a	k8xC
Google	Google	k1gNnSc7
TV	TV	kA
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
konzole	konzola	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
dříve	dříve	k6eAd2
používané	používaný	k2eAgFnSc3d1
architektuře	architektura	k1gFnSc3
x	x	k?
<g/>
86	#num#	k4
<g/>
,	,	kIx,
použité	použitý	k2eAgInPc1d1
v	v	k7c6
prvním	první	k4xOgInSc6
Xboxu	Xbox	k1gInSc6
<g/>
,	,	kIx,
namísto	namísto	k7c2
architektury	architektura	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c6
PowerPC	PowerPC	k1gFnSc6
z	z	k7c2
Xboxu	Xbox	k1gInSc2
360	#num#	k4
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
procesor	procesor	k1gInSc4
AMD	AMD	kA
postavený	postavený	k2eAgMnSc1d1
okolo	okolo	k7c2
sady	sada	k1gFnSc2
instrukcí	instrukce	k1gFnPc2
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
klade	klást	k5eAaImIp3nS
zvýšený	zvýšený	k2eAgInSc1d1
důraz	důraz	k1gInSc1
na	na	k7c4
integraci	integrace	k1gFnSc4
zábavy	zábava	k1gFnSc2
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
pracovat	pracovat	k5eAaImF
s	s	k7c7
několika	několik	k4yIc7
aplikacemi	aplikace	k1gFnPc7
na	na	k7c6
rozdělené	rozdělený	k2eAgFnSc6d1
obrazovce	obrazovka	k1gFnSc6
a	a	k8xC
zlepšenou	zlepšený	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
druhé	druhý	k4xOgFnSc2
obrazovky	obrazovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc3
zahrnuje	zahrnovat	k5eAaImIp3nS
nově	nově	k6eAd1
vylepšený	vylepšený	k2eAgInSc1d1
pohybový	pohybový	k2eAgInSc1d1
senzor	senzor	k1gInSc1
Kinect	Kinect	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
u	u	k7c2
předchozí	předchozí	k2eAgFnSc2d1
generace	generace	k1gFnSc2
pouze	pouze	k6eAd1
nepovinným	povinný	k2eNgInSc7d1
doplňkem	doplněk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS
integraci	integrace	k1gFnSc4
Kinectu	Kinect	k1gInSc2
do	do	k7c2
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc4
prostřednictvím	prostřednictvím	k7c2
prvků	prvek	k1gInPc2
a	a	k8xC
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
vestavěný	vestavěný	k2eAgMnSc1d1
klient	klient	k1gMnSc1
Skype	Skyp	k1gInSc5
pro	pro	k7c4
videokonference	videokonference	k1gFnPc4
<g/>
,	,	kIx,
rozpoznávání	rozpoznávání	k1gNnSc4
uživatele	uživatel	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
sledování	sledování	k1gNnSc4
a	a	k8xC
možnost	možnost	k1gFnSc4
používat	používat	k5eAaImF
hlasové	hlasový	k2eAgInPc4d1
příkazy	příkaz	k1gInPc4
a	a	k8xC
gesta	gesto	k1gNnPc4
k	k	k7c3
navigaci	navigace	k1gFnSc3
v	v	k7c6
uživatelském	uživatelský	k2eAgInSc6d1
rozhraní	rozhraní	k1gNnSc2
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nové	nový	k2eAgFnPc4d1
herní	herní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
patří	patřit	k5eAaImIp3nS
rozšířená	rozšířený	k2eAgFnSc1d1
služba	služba	k1gFnSc1
Xbox	Xbox	k1gInSc1
Live	Live	k1gFnSc1
<g/>
,	,	kIx,
vylepšená	vylepšený	k2eAgFnSc1d1
funkčnost	funkčnost	k1gFnSc1
Kinectu	Kinect	k1gInSc2
<g/>
,	,	kIx,
využívání	využívání	k1gNnSc4
cloudu	cloud	k1gInSc2
<g/>
,	,	kIx,
schopnost	schopnost	k1gFnSc4
automaticky	automaticky	k6eAd1
nahrávat	nahrávat	k5eAaImF
a	a	k8xC
sdílet	sdílet	k5eAaImF
ukázky	ukázka	k1gFnPc4
z	z	k7c2
her	hra	k1gFnPc2
a	a	k8xC
podpora	podpora	k1gFnSc1
pro	pro	k7c4
online	onlinout	k5eAaPmIp3nS
živé	živý	k2eAgInPc4d1
streamy	stream	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
konzole	konzola	k1gFnSc3
ohledně	ohledně	k6eAd1
požadavků	požadavek	k1gInPc2
na	na	k7c4
online	onlinout	k5eAaPmIp3nS
připojení	připojení	k1gNnSc2
<g/>
,	,	kIx,
povinné	povinný	k2eAgFnSc2d1
integrace	integrace	k1gFnSc2
s	s	k7c7
Kinectem	Kinect	k1gMnSc7
<g/>
,	,	kIx,
nejednoznačná	jednoznačný	k2eNgNnPc1d1
omezení	omezení	k1gNnPc1
týkající	týkající	k2eAgNnPc1d1
se	se	k3xPyFc4
předprodejů	předprodej	k1gInPc2
a	a	k8xC
sdílení	sdílení	k1gNnSc4
použitých	použitý	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
vyšší	vysoký	k2eAgFnSc1d2
cena	cena	k1gFnSc1
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
přímou	přímý	k2eAgFnSc7d1
konkurencí	konkurence	k1gFnSc7
vedly	vést	k5eAaImAgFnP
po	po	k7c4
oznámení	oznámení	k1gNnSc4
nového	nový	k2eAgInSc2d1
Xboxu	Xbox	k1gInSc2
k	k	k7c3
převážně	převážně	k6eAd1
smíšeným	smíšený	k2eAgFnPc3d1
recenzím	recenze	k1gFnPc3
a	a	k8xC
reakcím	reakce	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
kritiky	kritika	k1gFnSc2
Microsoft	Microsoft	kA
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
těchto	tento	k3xDgNnPc2
omezení	omezení	k1gNnPc2
upustí	upustit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
na	na	k7c6
E3	E3	k1gFnSc6
2013	#num#	k4
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
je	být	k5eAaImIp3nS
nástupce	nástupce	k1gMnSc1
Xboxu	Xbox	k1gInSc2
360	#num#	k4
<g/>
,	,	kIx,
předchozí	předchozí	k2eAgFnSc3d1
videoherní	videoherní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
od	od	k7c2
Microsoftu	Microsoft	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
sedmé	sedmý	k4xOgFnSc2
generace	generace	k1gFnSc2
videoherních	videoherní	k2eAgNnPc2d1
konzolí	konzolí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgInSc1d2
typ	typ	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
Microsoft	Microsoft	kA
nadále	nadále	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
<g/>
,	,	kIx,
podstoupil	podstoupit	k5eAaPmAgMnS
několik	několik	k4yIc4
malých	malý	k2eAgFnPc2d1
hardwarových	hardwarový	k2eAgFnPc2d1
revizí	revize	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
cíl	cíl	k1gInSc4
snížit	snížit	k5eAaPmF
jeho	jeho	k3xOp3gFnSc4
velikost	velikost	k1gFnSc4
a	a	k8xC
zlepšit	zlepšit	k5eAaPmF
spolehlivost	spolehlivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
Chris	Chris	k1gFnSc1
Lewis	Lewis	k1gFnSc1
z	z	k7c2
Microsoftu	Microsoft	k1gInSc2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
360	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
půli	půle	k1gFnSc6
cesty	cesta	k1gFnSc2
svého	svůj	k3xOyFgInSc2
životního	životní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
podpořeno	podpořit	k5eAaPmNgNnS
představením	představení	k1gNnSc7
senzoru	senzor	k1gInSc2
Kinect	Kinecta	k1gFnPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
měl	mít	k5eAaImAgMnS
podle	podle	k7c2
Lewise	Lewise	k1gFnSc2
prodloužit	prodloužit	k5eAaPmF
životnost	životnost	k1gFnSc4
konzole	konzola	k1gFnSc3
o	o	k7c4
dalších	další	k2eAgNnPc2d1
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
hardware	hardware	k1gInSc1
nástupce	nástupce	k1gMnSc2
360	#num#	k4
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
označovaný	označovaný	k2eAgInSc1d1
v	v	k7c6
branži	branže	k1gFnSc6
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Xbox	Xbox	k1gInSc1
720	#num#	k4
<g/>
“	“	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
údajně	údajně	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
rukou	ruka	k1gFnPc2
vývojářů	vývojář	k1gMnPc2
již	již	k6eAd1
v	v	k7c6
květnu	květen	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
oficiální	oficiální	k2eAgInSc1d1
vývojářský	vývojářský	k2eAgInSc1d1
kit	kit	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
nesl	nést	k5eAaImAgMnS
kódové	kódový	k2eAgNnSc4d1
označení	označení	k1gNnSc4
Durango	Durango	k6eAd1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
pro	pro	k7c4
vývojáře	vývojář	k1gMnSc4
dostupný	dostupný	k2eAgInSc1d1
v	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uniklé	uniklý	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
nová	nový	k2eAgFnSc1d1
konzole	konzola	k1gFnSc6
obsahovala	obsahovat	k5eAaImAgFnS
vylepšený	vylepšený	k2eAgInSc4d1
Kinect	Kinect	k1gInSc4
<g/>
,	,	kIx,
cloudový	cloudový	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
hrám	hra	k1gFnPc3
a	a	k8xC
médiím	médium	k1gNnPc3
a	a	k8xC
integraci	integrace	k1gFnSc4
s	s	k7c7
telefony	telefon	k1gInPc7
a	a	k8xC
tablety	tablet	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
tyto	tento	k3xDgInPc4
uniklé	uniklý	k2eAgInPc4d1
prvky	prvek	k1gInPc4
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc4
nijak	nijak	k6eAd1
nekomentoval	komentovat	k5eNaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uniklé	uniklý	k2eAgInPc1d1
designové	designový	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
rovněž	rovněž	k9
poukazovaly	poukazovat	k5eAaImAgInP
na	na	k7c4
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Microsoft	Microsoft	kA
snaží	snažit	k5eAaImIp3nP
eliminovat	eliminovat	k5eAaBmF
možnost	možnost	k1gFnSc4
hraní	hraní	k1gNnSc2
použitých	použitý	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
později	pozdě	k6eAd2
vysvětlil	vysvětlit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
stále	stále	k6eAd1
„	„	k?
<g/>
přemýšleli	přemýšlet	k5eAaImAgMnP
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
bude	být	k5eAaImBp3nS
dál	daleko	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
jak	jak	k8xC,k8xS
můžeme	moct	k5eAaImIp1nP
s	s	k7c7
Kinectem	Kinecto	k1gNnSc7
posouvat	posouvat	k5eAaImF
hranice	hranice	k1gFnPc4
technologie	technologie	k1gFnSc2
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
jsme	být	k5eAaImIp1nP
vytvořili	vytvořit	k5eAaPmAgMnP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každopádně	každopádně	k6eAd1
k	k	k7c3
platnosti	platnost	k1gFnSc3
této	tento	k3xDgFnSc2
informace	informace	k1gFnSc2
se	se	k3xPyFc4
opět	opět	k6eAd1
nevyjádřil	vyjádřit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Konzole	konzola	k1gFnSc3
byla	být	k5eAaImAgFnS
veřejně	veřejně	k6eAd1
odhalena	odhalit	k5eAaPmNgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
pokrýt	pokrýt	k5eAaPmF
široké	široký	k2eAgFnPc4d1
multimediální	multimediální	k2eAgFnPc4d1
a	a	k8xC
sociální	sociální	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
nového	nový	k2eAgInSc2d1
Xboxu	Xbox	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
tisková	tiskový	k2eAgFnSc1d1
konference	konference	k1gFnSc1
ohledně	ohledně	k7c2
konzole	konzola	k1gFnSc3
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
během	během	k7c2
E3	E3	k1gFnSc2
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
funkce	funkce	k1gFnPc4
vztahující	vztahující	k2eAgFnPc4d1
se	se	k3xPyFc4
k	k	k7c3
videohrám	videohra	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
Microsoft	Microsoft	kA
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
konzole	konzola	k1gFnSc6
bude	být	k5eAaImBp3nS
uvedena	uvést	k5eAaPmNgFnS
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
21	#num#	k4
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
toto	tento	k3xDgNnSc1
stanovisko	stanovisko	k1gNnSc1
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
změněno	změnit	k5eAaPmNgNnS
na	na	k7c4
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
změna	změna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
znamenala	znamenat	k5eAaImAgFnS
posunutí	posunutí	k1gNnSc4
data	datum	k1gNnSc2
uvedení	uvedení	k1gNnSc2
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
8	#num#	k4
zemích	zem	k1gFnPc6
na	na	k7c4
rok	rok	k1gInSc4
2014	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vysvětlována	vysvětlovat	k5eAaImNgFnS
nepředvídatelnou	předvídatelný	k2eNgFnSc7d1
složitostí	složitost	k1gFnSc7
lokalizace	lokalizace	k1gFnSc2
nového	nový	k2eAgInSc2d1
Kinectu	Kinect	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
generace	generace	k1gFnPc4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc4
S	s	k7c7
2016	#num#	k4
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
X	X	kA
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hardware	hardware	k1gInSc1
</s>
<s>
Vnější	vnější	k2eAgInSc1d1
plášť	plášť	k1gInSc1
Xboxu	Xbox	k1gInSc2
One	One	k1gFnSc2
je	být	k5eAaImIp3nS
opatřen	opatřit	k5eAaPmNgInS
dvoubarevným	dvoubarevný	k2eAgInSc7d1
lakem	lak	k1gInSc7
v	v	k7c6
odstínu	odstín	k1gInSc6
„	„	k?
<g/>
tekuté	tekutý	k2eAgFnSc2d1
černé	černá	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jedna	jeden	k4xCgFnSc1
polovina	polovina	k1gFnSc1
je	být	k5eAaImIp3nS
vyvedena	vyvést	k5eAaPmNgFnS
v	v	k7c6
matně	matně	k6eAd1
šedé	šedý	k2eAgFnSc6d1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
v	v	k7c6
lesklejší	lesklý	k2eAgFnSc6d2
černé	černá	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Design	design	k1gInSc1
má	mít	k5eAaImIp3nS
vyvolávat	vyvolávat	k5eAaImF
dojem	dojem	k1gInSc4
na	na	k7c4
zábavu	zábava	k1gFnSc4
více	hodně	k6eAd2
orientovaného	orientovaný	k2eAgInSc2d1
a	a	k8xC
zjednodušeného	zjednodušený	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
než	než	k8xS
u	u	k7c2
předchozí	předchozí	k2eAgFnSc2d1
generace	generace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
jiných	jiný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
LED	LED	kA
kroužky	kroužek	k1gInPc1
používané	používaný	k2eAgInPc1d1
v	v	k7c6
Xboxu	Xbox	k1gInSc6
360	#num#	k4
za	za	k7c4
zářící	zářící	k2eAgNnSc4d1
bílé	bílý	k2eAgNnSc4d1
logo	logo	k1gNnSc4
Xbox	Xbox	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
uživatel	uživatel	k1gMnSc1
pozná	poznat	k5eAaPmIp3nS
aktuální	aktuální	k2eAgInSc4d1
status	status	k1gInSc4
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
konzoli	konzole	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
APU	APU	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Accelerated	Accelerated	k1gInSc4
Processing	Processing	k1gInSc1
Unit	Unit	k1gInSc1
<g/>
)	)	kIx)
AMD	AMD	kA
„	„	k?
<g/>
Jaguar	Jaguar	k1gInSc1
<g/>
“	“	k?
s	s	k7c7
dvěma	dva	k4xCgInPc7
čtyřjádrovými	čtyřjádrový	k2eAgInPc7d1
moduly	modul	k1gInPc7
s	s	k7c7
celkem	celkem	k6eAd1
osmi	osm	k4xCc7
jádry	jádro	k1gNnPc7
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
taktovanými	taktovaný	k2eAgFnPc7d1
na	na	k7c4
1,75	1,75	k4
GHz	GHz	k1gFnPc2
a	a	k8xC
8	#num#	k4
GB	GB	kA
DDR3	DDR3	k1gFnPc2
RAM	RAM	kA
s	s	k7c7
propustností	propustnost	k1gFnSc7
68,3	68,3	k4
GB	GB	kA
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
Paměťový	paměťový	k2eAgInSc1d1
podsystém	podsystém	k1gInSc1
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
dalších	další	k2eAgInPc2d1
32	#num#	k4
MB	MB	kA
„	„	k?
<g/>
vložené	vložený	k2eAgFnSc2d1
statické	statický	k2eAgFnSc2d1
<g/>
”	”	k?
RAM	RAM	kA
(	(	kIx(
<g/>
neboli	neboli	k8xC
ESRAM	ESRAM	kA
<g/>
)	)	kIx)
s	s	k7c7
propustností	propustnost	k1gFnSc7
109	#num#	k4
GB	GB	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Eurogameru	Eurogamer	k1gInSc2
bylo	být	k5eAaImAgNnS
sděleno	sdělen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
simultánní	simultánní	k2eAgFnPc4d1
operace	operace	k1gFnPc4
čtení	čtení	k1gNnSc2
a	a	k8xC
zápisu	zápis	k1gInSc2
je	být	k5eAaImIp3nS
ESRAM	ESRAM	kA
schopná	schopný	k2eAgFnSc1d1
teoretické	teoretický	k2eAgFnPc4d1
propustnosti	propustnost	k1gFnPc4
192	#num#	k4
GB	GB	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
a	a	k8xC
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
zahrnovaly	zahrnovat	k5eAaImAgFnP
zpracování	zpracování	k1gNnSc4
průhlednosti	průhlednost	k1gFnSc2
textur	textura	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
propustnost	propustnost	k1gFnSc1
133	#num#	k4
GB	GB	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
V	v	k7c6
konzoli	konzole	k1gFnSc6
se	se	k3xPyFc4
dále	daleko	k6eAd2
nachází	nacházet	k5eAaImIp3nS
nevyměnitelný	vyměnitelný	k2eNgInSc4d1
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
o	o	k7c6
kapacitě	kapacita	k1gFnSc6
500	#num#	k4
GB	GB	kA
a	a	k8xC
optická	optický	k2eAgFnSc1d1
Blu-ray	Blu-ra	k2eAgFnPc1d1
mechanika	mechanika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
hry	hra	k1gFnPc4
je	být	k5eAaImIp3nS
na	na	k7c6
pevném	pevný	k2eAgInSc6d1
disku	disk	k1gInSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
okolo	okolo	k7c2
362	#num#	k4
GB	GB	kA
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
pro	pro	k7c4
externí	externí	k2eAgInPc4d1
disky	disk	k1gInPc4
bude	být	k5eAaImBp3nS
přidána	přidán	k2eAgFnSc1d1
v	v	k7c6
některé	některý	k3yIgFnSc6
z	z	k7c2
budoucích	budoucí	k2eAgFnPc2d1
aktualizací	aktualizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
3	#num#	k4
GB	GB	kA
RAM	RAM	kA
jsou	být	k5eAaImIp3nP
vyhrazeny	vyhradit	k5eAaPmNgInP
pro	pro	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
aplikace	aplikace	k1gFnPc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
5	#num#	k4
GB	GB	kA
připadne	připadnout	k5eAaPmIp3nS
na	na	k7c4
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grafický	grafický	k2eAgInSc1d1
procesor	procesor	k1gInSc1
(	(	kIx(
<g/>
GPU	GPU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
architektuře	architektura	k1gFnSc6
AMD	AMD	kA
GCN	GCN	kA
s	s	k7c7
12	#num#	k4
výpočetními	výpočetní	k2eAgInPc7d1
jednotkami	jednotka	k1gFnPc7
s	s	k7c7
celkem	celek	k1gInSc7
768	#num#	k4
jádry	jádro	k1gNnPc7
<g/>
,	,	kIx,
běžících	běžící	k2eAgMnPc2d1
na	na	k7c4
853	#num#	k4
MHz	Mhz	kA
a	a	k8xC
poskytujících	poskytující	k2eAgInPc2d1
odhadovaný	odhadovaný	k2eAgInSc4d1
maximální	maximální	k2eAgInSc4d1
teoretický	teoretický	k2eAgInSc4d1
výkon	výkon	k1gInSc4
1,31	1,31	k4
TFLOPů	TFLOP	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
sítí	sítí	k1gNnSc2
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc4
podporuje	podporovat	k5eAaImIp3nS
gigabitový	gigabitový	k2eAgInSc1d1
ethernet	ethernet	k1gInSc1
<g/>
,	,	kIx,
bezdrátový	bezdrátový	k2eAgInSc1d1
standard	standard	k1gInSc1
802.11	802.11	k4
<g/>
n	n	k0
a	a	k8xC
Wi-Fi	Wi-F	k1gInSc3
Direct	Directa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
podporuje	podporovat	k5eAaImIp3nS
grafický	grafický	k2eAgInSc4d1
výstup	výstup	k1gInSc4
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
4K	4K	k4
(	(	kIx(
<g/>
3840	#num#	k4
<g/>
x	x	k?
<g/>
2160	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2160	#num#	k4
<g/>
p	p	k?
<g/>
)	)	kIx)
a	a	k8xC
prostorový	prostorový	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
7.1	7.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yusuf	Yusuf	k1gInSc1
Mehdi	Mehd	k1gMnPc1
<g/>
,	,	kIx,
viceprezident	viceprezident	k1gMnSc1
marketingu	marketing	k1gInSc2
a	a	k8xC
strategie	strategie	k1gFnSc2
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
neexistuje	existovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc4
hardwarové	hardwarový	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
bránilo	bránit	k5eAaImAgNnS
hrám	hra	k1gFnPc3
ve	v	k7c6
spuštění	spuštění	k1gNnSc6
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
4	#num#	k4
<g/>
K.	K.	kA
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc1
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Xboxu	Xbox	k1gInSc2
360	#num#	k4
–	–	k?
nepodporuje	podporovat	k5eNaImIp3nS
1080	#num#	k4
<g/>
i	i	k8xC
a	a	k8xC
další	další	k2eAgNnSc4d1
prokládané	prokládaný	k2eAgNnSc4d1
rozlišení	rozlišení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
nepodporuje	podporovat	k5eNaImIp3nS
kompozitní	kompozitní	k2eAgNnSc4d1
nebo	nebo	k8xC
komponentní	komponentní	k2eAgNnSc4d1
video	video	k1gNnSc4
a	a	k8xC
naopak	naopak	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
HDMI	HDMI	kA
1.4	1.4	k4
pro	pro	k7c4
vstup	vstup	k1gInSc4
i	i	k8xC
výstup	výstup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Konzole	konzola	k1gFnSc3
dokáže	dokázat	k5eAaPmIp3nS
sledovat	sledovat	k5eAaImF
svou	svůj	k3xOyFgFnSc4
vnitřní	vnitřní	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
a	a	k8xC
patřičně	patřičně	k6eAd1
tak	tak	k6eAd1
zareagovat	zareagovat	k5eAaPmF
v	v	k7c6
případě	případ	k1gInSc6
hrozby	hrozba	k1gFnSc2
přehřátí	přehřátí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
zvýšení	zvýšení	k1gNnSc2
rychlosti	rychlost	k1gFnSc2
větráku	větrák	k1gInSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přijata	přijat	k2eAgNnPc1d1
další	další	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
přinucení	přinucení	k1gNnSc2
hardwaru	hardware	k1gInSc2
ke	k	k7c3
spuštění	spuštění	k1gNnSc3
v	v	k7c6
nižším	nízký	k2eAgInSc6d2
energetickém	energetický	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
vlastnost	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nebyla	být	k5eNaImAgFnS
přítomna	přítomen	k2eAgFnSc1d1
na	na	k7c6
Xboxu	Xbox	k1gInSc6
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezení	omezení	k1gNnSc1
spotřeby	spotřeba	k1gFnSc2
energie	energie	k1gFnSc2
snižuje	snižovat	k5eAaImIp3nS
maximální	maximální	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
tato	tento	k3xDgFnSc1
možnost	možnost	k1gFnSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
jakousi	jakýsi	k3yIgFnSc7
poslední	poslední	k2eAgFnSc7d1
záchrannou	záchranný	k2eAgFnSc7d1
brzdou	brzda	k1gFnSc7
před	před	k7c7
trvalým	trvalý	k2eAgNnSc7d1
poškozením	poškození	k1gNnSc7
přístroje	přístroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ovladač	ovladač	k1gInSc1
</s>
<s>
Ovladač	ovladač	k1gInSc1
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
si	se	k3xPyFc3
zachovává	zachovávat	k5eAaImIp3nS
celkové	celkový	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrový	směrový	k2eAgInSc1d1
ovladač	ovladač	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
čtyřsměrový	čtyřsměrový	k2eAgInSc4d1
<g/>
,	,	kIx,
prostor	prostor	k1gInSc4
pro	pro	k7c4
baterie	baterie	k1gFnPc4
je	být	k5eAaImIp3nS
štíhlejší	štíhlý	k2eAgMnSc1d2
<g/>
,	,	kIx,
tlačítka	tlačítko	k1gNnSc2
Start	start	k1gInSc1
a	a	k8xC
Back	Back	k1gMnSc1
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
tlačítky	tlačítko	k1gNnPc7
Menu	menu	k1gNnSc2
a	a	k8xC
View	View	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
spoušť	spoušť	k1gFnSc4
disponuje	disponovat	k5eAaBmIp3nS
nezávislými	závislý	k2eNgInPc7d1
motorky	motorek	k1gInPc7
(	(	kIx(
<g/>
nazývané	nazývaný	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Impulse	impuls	k1gInSc5
Triggers	Triggers	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
vývojářům	vývojář	k1gMnPc3
naprogramovat	naprogramovat	k5eAaPmF
směrové	směrový	k2eAgFnPc4d1
vibrace	vibrace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
spoušť	spoušť	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
naprogramována	naprogramovat	k5eAaPmNgFnS
k	k	k7c3
vibraci	vibrace	k1gFnSc3
při	při	k7c6
střelbě	střelba	k1gFnSc6
zbraně	zbraň	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
obě	dva	k4xCgNnPc1
mohou	moct	k5eAaImIp3nP
pracovat	pracovat	k5eAaImF
společně	společně	k6eAd1
na	na	k7c6
vytvoření	vytvoření	k1gNnSc6
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
určí	určit	k5eAaPmIp3nS
směr	směr	k1gInSc4
příchozího	příchozí	k1gMnSc2
zásahu	zásah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
investoval	investovat	k5eAaBmAgInS
do	do	k7c2
vylepšení	vylepšení	k1gNnSc2
designu	design	k1gInSc3
ovladače	ovladač	k1gInSc2
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
přes	přes	k7c4
100	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kinect	Kinect	k1gMnSc1
</s>
<s>
K	k	k7c3
Xboxu	Xbox	k1gInSc3
One	One	k1gFnSc2
je	být	k5eAaImIp3nS
přibalen	přibalen	k2eAgInSc1d1
vylepšený	vylepšený	k2eAgInSc1d1
senzor	senzor	k1gInSc1
Kinect	Kinecta	k1gFnPc2
pro	pro	k7c4
sledování	sledování	k1gNnSc4
pohybu	pohyb	k1gInSc2
a	a	k8xC
rozpoznávání	rozpoznávání	k1gNnSc2
hlasu	hlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Kinect	Kinect	k1gInSc1
používá	používat	k5eAaImIp3nS
širokoúhlou	širokoúhlý	k2eAgFnSc4d1
kameru	kamera	k1gFnSc4
s	s	k7c7
rozlišením	rozlišení	k1gNnSc7
1080	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
VGA	VGA	kA
rozlišením	rozlišení	k1gNnSc7
předchozí	předchozí	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
zpracovává	zpracovávat	k5eAaImIp3nS
2	#num#	k4
gigabity	gigabita	k1gFnSc2
dat	datum	k1gNnPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
při	při	k7c6
analyzování	analyzování	k1gNnSc6
svého	svůj	k3xOyFgNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Kinect	Kinect	k1gInSc1
je	být	k5eAaImIp3nS
přesnější	přesný	k2eAgMnSc1d2
než	než	k8xS
jeho	jeho	k3xOp3gMnSc1
předchůdce	předchůdce	k1gMnSc1
<g/>
,	,	kIx,
dokáže	dokázat	k5eAaPmIp3nS
sledovat	sledovat	k5eAaImF
až	až	k9
6	#num#	k4
lidí	člověk	k1gMnPc2
najednou	najednou	k6eAd1
<g/>
,	,	kIx,
sledovat	sledovat	k5eAaImF
tepovou	tepový	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
a	a	k8xC
gesta	gesto	k1gNnPc4
vytvářená	vytvářený	k2eAgNnPc4d1
ovladačem	ovladač	k1gInSc7
Xbox	Xbox	k1gInSc1
One	One	k1gFnPc2
a	a	k8xC
skenovat	skenovat	k5eAaImF
QR	QR	kA
kódy	kód	k1gInPc4
kvůli	kvůli	k7c3
uplatňování	uplatňování	k1gNnSc3
dárkových	dárkový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
Xbox	Xbox	k1gInSc1
Live	Liv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikrofon	mikrofon	k1gInSc1
Kinectu	Kinect	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
ve	v	k7c6
výchozím	výchozí	k2eAgNnSc6d1
nastavení	nastavení	k1gNnSc6
aktivní	aktivní	k2eAgMnSc1d1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
může	moct	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
přijímat	přijímat	k5eAaImF
od	od	k7c2
uživatele	uživatel	k1gMnSc2
hlasové	hlasový	k2eAgInPc1d1
povely	povel	k1gInPc1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
je	být	k5eAaImIp3nS
konzole	konzola	k1gFnSc3
v	v	k7c6
režimu	režim	k1gInSc6
spánku	spánek	k1gInSc2
(	(	kIx(
<g/>
takže	takže	k8xS
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
probuzena	probudit	k5eAaPmNgFnS
pomocí	pomocí	k7c2
povelu	povel	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
na	na	k7c6
Xboxu	Xbox	k1gInSc6
360	#num#	k4
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
na	na	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
Kinectu	Kinect	k1gInSc2
volitelné	volitelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
bude	být	k5eAaImBp3nS
senzor	senzor	k1gInSc1
fungovat	fungovat	k5eAaImF
<g/>
,	,	kIx,
lze	lze	k6eAd1
upravit	upravit	k5eAaPmF
v	v	k7c6
nastavení	nastavení	k1gNnSc6
soukromí	soukromí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
nového	nový	k2eAgInSc2d1
Kinectu	Kinect	k1gInSc2
kompatibilní	kompatibilní	k2eAgMnSc1d1
se	s	k7c7
systémem	systém	k1gInSc7
Windows	Windows	kA
byla	být	k5eAaImAgFnS
k	k	k7c3
dostání	dostání	k1gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
,	,	kIx,
<g/>
ale	ale	k8xC
dnes	dnes	k6eAd1
už	už	k6eAd1
se	se	k3xPyFc4
nevyrábí	vyrábět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Software	software	k1gInSc1
a	a	k8xC
služby	služba	k1gFnPc1
</s>
<s>
Mediální	mediální	k2eAgFnSc1d1
propojitelnost	propojitelnost	k1gFnSc1
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
ve	v	k7c6
Windows	Windows	kA
8	#num#	k4
<g/>
,	,	kIx,
i	i	k8xC
na	na	k7c6
Xboxu	Xbox	k1gInSc6
One	One	k1gFnSc2
lze	lze	k6eAd1
přesouvat	přesouvat	k5eAaImF
aplikace	aplikace	k1gFnPc4
(	(	kIx(
<g/>
jako	jako	k9
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
videa	video	k1gNnSc2
<g/>
,	,	kIx,
Skype	Skyp	k1gInSc5
a	a	k8xC
Internet	Internet	k1gInSc1
Explorer	Explorra	k1gFnPc2
<g/>
)	)	kIx)
ke	k	k7c3
straně	strana	k1gFnSc3
obrazovky	obrazovka	k1gFnSc2
a	a	k8xC
pracovat	pracovat	k5eAaImF
na	na	k7c6
více	hodně	k6eAd2
věcech	věc	k1gFnPc6
naráz	naráz	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Ovládání	ovládání	k1gNnSc1
hlasem	hlas	k1gInSc7
</s>
<s>
Konzole	konzola	k1gFnSc3
nabízí	nabízet	k5eAaImIp3nP
podobnou	podobný	k2eAgFnSc4d1
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
bohatší	bohatý	k2eAgFnSc4d2
sadu	sada	k1gFnSc4
možností	možnost	k1gFnPc2
ovládaných	ovládaný	k2eAgInPc2d1
hlasem	hlasem	k6eAd1
než	než	k8xS
tu	tu	k6eAd1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nabízel	nabízet	k5eAaImAgInS
Kinect	Kinect	k1gInSc1
první	první	k4xOgFnSc2
generace	generace	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
umožňuje	umožňovat	k5eAaImIp3nS
uživateli	uživatel	k1gMnSc3
ovládat	ovládat	k5eAaImF
funkce	funkce	k1gFnPc1
Xboxu	Xbox	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
hlasových	hlasový	k2eAgInPc2d1
povelů	povel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
rovněž	rovněž	k9
iniciovat	iniciovat	k5eAaBmF
konverzaci	konverzace	k1gFnSc4
přes	přes	k7c4
Skype	Skyp	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
V	v	k7c6
zařízení	zařízení	k1gNnSc6
údajně	údajně	k6eAd1
běží	běžet	k5eAaImIp3nP
tři	tři	k4xCgInPc4
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
:	:	kIx,
Xbox	Xbox	k1gInSc4
OS	OS	kA
<g/>
,	,	kIx,
OS	OS	kA
založený	založený	k2eAgInSc1d1
na	na	k7c6
jádře	jádro	k1gNnSc6
systému	systém	k1gInSc2
Windows	Windows	kA
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgInPc1d1
OS	OS	kA
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
umožňuje	umožňovat	k5eAaImIp3nS
dvěma	dva	k4xCgInPc3
operačním	operační	k2eAgInPc3d1
systémům	systém	k1gInPc3
komunikovat	komunikovat	k5eAaImF
virtualizací	virtualizace	k1gFnSc7
(	(	kIx(
<g/>
jako	jako	k9
hypervisor	hypervisor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
integrace	integrace	k1gFnSc1
dovoluje	dovolovat	k5eAaImIp3nS
například	například	k6eAd1
volání	volání	k1gNnSc1
přes	přes	k7c4
Skype	Skyp	k1gMnSc5
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
uživatel	uživatel	k1gMnSc1
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
některé	některý	k3yIgFnSc6
z	z	k7c2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jádro	jádro	k1gNnSc1
Windows	Windows	kA
na	na	k7c6
Xboxu	Xbox	k1gInSc6
není	být	k5eNaImIp3nS
kompatibilní	kompatibilní	k2eAgFnSc1d1
se	s	k7c7
standardními	standardní	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
Windows	Windows	kA
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
vývojáři	vývojář	k1gMnPc1
je	on	k3xPp3gNnSc4
budou	být	k5eAaImBp3nP
schopní	schopný	k2eAgMnPc1d1
portovat	portovat	k5eAaImF
za	za	k7c4
cenu	cena	k1gFnSc4
pouze	pouze	k6eAd1
malého	malý	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
</s>
<s>
Xbox	Xbox	k1gInSc1
Live	Liv	k1gFnSc2
</s>
<s>
Microsoft	Microsoft	kA
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
služba	služba	k1gFnSc1
Xbox	Xbox	k1gInSc4
Live	Liv	k1gFnSc2
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
uživatele	uživatel	k1gMnPc4
Xbox	Xbox	k1gInSc4
One	One	k1gMnPc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c4
300	#num#	k4
000	#num#	k4
serverů	server	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
se	se	k3xPyFc4
však	však	k9
nezmínil	zmínit	k5eNaPmAgMnS
<g/>
,	,	kIx,
kolik	kolik	k9
z	z	k7c2
těchto	tento	k3xDgInPc2
serverů	server	k1gInPc2
je	být	k5eAaImIp3nS
fyzických	fyzický	k2eAgFnPc2d1
a	a	k8xC
kolik	kolik	k4yRc4,k4yIc4,k4yQc4
virtuálních	virtuální	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úložiště	úložiště	k1gNnSc1
cloud	cloud	k6eAd1
je	být	k5eAaImIp3nS
dostupné	dostupný	k2eAgNnSc1d1
k	k	k7c3
ukládání	ukládání	k1gNnSc3
her	hra	k1gFnPc2
a	a	k8xC
dalšího	další	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vývojáři	vývojář	k1gMnPc1
mají	mít	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
využít	využít	k5eAaPmF
servery	server	k1gInPc4
služby	služba	k1gFnSc2
Live	Liv	k1gInSc2
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
cloudovou	cloudový	k2eAgFnSc7d1
výpočetní	výpočetní	k2eAgFnSc7d1
platformou	platforma	k1gFnSc7
Windows	Windows	kA
Azure	azur	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nabídli	nabídnout	k5eAaPmAgMnP
hráčům	hráč	k1gMnPc3
dynamické	dynamický	k2eAgFnPc4d1
změny	změna	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
jejich	jejich	k3xOp3gFnSc2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
si	se	k3xPyFc3
i	i	k9
nadále	nadále	k6eAd1
zachovává	zachovávat	k5eAaImIp3nS
systém	systém	k1gInSc4
předplatného	předplatné	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc4
přátel	přítel	k1gMnPc2
byl	být	k5eAaImAgMnS
rozšířen	rozšířit	k5eAaPmNgMnS
na	na	k7c4
1000	#num#	k4
kontaktů	kontakt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
SmartGlass	SmartGlass	k6eAd1
</s>
<s>
Xbox	Xbox	k1gInSc1
SmartGlass	SmartGlass	k1gInSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
funkčnost	funkčnost	k1gFnSc4
Xbox	Xbox	k1gInSc1
One	One	k1gFnPc1
<g/>
,	,	kIx,
když	když	k8xS
zařízení	zařízení	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
pohánějí	pohánět	k5eAaImIp3nP
systémy	systém	k1gInPc7
Windows	Windows	kA
Phone	Phon	k1gInSc5
<g/>
,	,	kIx,
Windows	Windows	kA
8	#num#	k4
<g/>
,	,	kIx,
iOS	iOS	k?
a	a	k8xC
Android	android	k1gInSc1
<g/>
,	,	kIx,
lze	lze	k6eAd1
použít	použít	k5eAaPmF
jako	jako	k8xS,k8xC
„	„	k?
<g/>
druhou	druhý	k4xOgFnSc4
obrazovku	obrazovka	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázka	ukázka	k1gFnSc1
během	během	k7c2
tiskové	tiskový	k2eAgFnSc2d1
konference	konference	k1gFnSc2
na	na	k7c6
E3	E3	k1gFnSc6
demonstrovala	demonstrovat	k5eAaBmAgFnS
využití	využití	k1gNnSc4
na	na	k7c4
tabletu	tableta	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
na	na	k7c6
pozadí	pozadí	k1gNnSc6
upravit	upravit	k5eAaPmF
nastavení	nastavení	k1gNnSc4
pro	pro	k7c4
multiplayerovou	multiplayerový	k2eAgFnSc4d1
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
televizi	televize	k1gFnSc6
byla	být	k5eAaImAgFnS
hrána	hrát	k5eAaImNgFnS
jiná	jiný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1
a	a	k8xC
streamování	streamování	k1gNnSc1
</s>
<s>
Předplatitelé	předplatitel	k1gMnPc1
Zlatého	zlatý	k2eAgNnSc2d1
členství	členství	k1gNnSc2
Xbox	Xbox	k1gInSc4
Live	Liv	k1gInSc2
mohou	moct	k5eAaImIp3nP
používat	používat	k5eAaImF
aplikaci	aplikace	k1gFnSc4
Upload	Upload	k1gInSc1
Studio	studio	k1gNnSc1
k	k	k7c3
editování	editování	k1gNnSc3
a	a	k8xC
sdílení	sdílení	k1gNnSc3
klipů	klip	k1gInPc2
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
pěti	pět	k4xCc2
minut	minuta	k1gFnPc2
hraní	hraň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
konzole	konzola	k1gFnSc3
automaticky	automaticky	k6eAd1
nahrává	nahrávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrace	integrace	k1gFnSc1
se	s	k7c7
steamovací	steamovací	k2eAgFnSc7d1
platformou	platforma	k1gFnSc7
Twitch	Twitcha	k1gFnPc2
bude	být	k5eAaImBp3nS
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
budou	být	k5eAaImBp3nP
uživatelé	uživatel	k1gMnPc1
schopni	schopen	k2eAgMnPc1d1
pomocí	pomocí	k7c2
hlasových	hlasový	k2eAgInPc2d1
povelů	povel	k1gInPc2
okamžitě	okamžitě	k6eAd1
začít	začít	k5eAaPmF
streamovat	streamovat	k5eAaImF,k5eAaBmF,k5eAaPmF
svou	svůj	k3xOyFgFnSc4
aktuální	aktuální	k2eAgFnSc4d1
hru	hra	k1gFnSc4
a	a	k8xC
použít	použít	k5eAaPmF
mikrofon	mikrofon	k1gInSc4
Kinectu	Kinect	k1gInSc2
ke	k	k7c3
komentování	komentování	k1gNnSc3
svého	svůj	k3xOyFgNnSc2
počínání	počínání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
</s>
<s>
Microsoft	Microsoft	kA
představil	představit	k5eAaPmAgInS
několik	několik	k4yIc4
svých	svůj	k3xOyFgFnPc2
her	hra	k1gFnPc2
i	i	k8xC
her	hra	k1gFnPc2
od	od	k7c2
dalších	další	k2eAgMnPc2d1
vývojářů	vývojář	k1gMnPc2
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
na	na	k7c6
své	svůj	k3xOyFgFnSc6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
na	na	k7c4
E	E	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
některé	některý	k3yIgFnPc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
platformu	platforma	k1gFnSc4
exluzivní	exluzivnět	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
vlastní	vlastní	k2eAgFnPc4d1
odhalené	odhalený	k2eAgFnPc4d1
hry	hra	k1gFnPc4
na	na	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc1
patřily	patřit	k5eAaImAgFnP
Forza	Forz	k1gMnSc4
Motorsport	Motorsport	k1gInSc4
5	#num#	k4
<g/>
,	,	kIx,
Ryse	Rys	k1gMnPc4
<g/>
:	:	kIx,
Son	son	k1gInSc1
of	of	k?
Rome	Rom	k1gMnSc5
<g/>
,	,	kIx,
Quantum	Quantum	k1gNnSc1
Break	break	k1gInSc1
<g/>
,	,	kIx,
vzkříšení	vzkříšení	k1gNnSc1
Killer	Killer	k1gMnSc1
Instinct	Instinct	k1gMnSc1
<g/>
,	,	kIx,
Project	Project	k1gMnSc1
Spark	Spark	k1gInSc1
a	a	k8xC
krátká	krátký	k2eAgFnSc1d1
upoutávka	upoutávka	k1gFnSc1
na	na	k7c4
nadcházející	nadcházející	k2eAgFnSc4d1
hru	hra	k1gFnSc4
z	z	k7c2
univerza	univerzum	k1gNnSc2
Halo	halo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
na	na	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
jsou	být	k5eAaImIp3nP
distribuovány	distribuovat	k5eAaBmNgFnP
na	na	k7c4
Blu-ray	Blu-ra	k2eAgFnPc4d1
discích	disk	k1gInPc6
a	a	k8xC
digitálně	digitálně	k6eAd1
prostřednictvím	prostřednictví	k1gNnSc7
Xbox	Xbox	k1gInSc1
Games	Games	k1gInSc1
Store	Stor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
hry	hra	k1gFnPc1
na	na	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
(	(	kIx(
<g/>
ať	ať	k8xS,k8xC
už	už	k6eAd1
digitálně	digitálně	k6eAd1
nebo	nebo	k8xC
fyzicky	fyzicky	k6eAd1
zakoupené	zakoupený	k2eAgInPc1d1
<g/>
)	)	kIx)
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
uloženy	uložit	k5eAaPmNgInP
na	na	k7c4
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
fyzických	fyzický	k2eAgFnPc2d1
kopií	kopie	k1gFnPc2
je	být	k5eAaImIp3nS
k	k	k7c3
hraní	hraní	k1gNnSc3
zapotřebí	zapotřebí	k6eAd1
disk	disk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
však	však	k9
hra	hra	k1gFnSc1
nainstalována	nainstalován	k2eAgFnSc1d1
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
konzoli	konzole	k1gFnSc6
a	a	k8xC
majitel	majitel	k1gMnSc1
dané	daný	k2eAgFnSc2d1
konzole	konzola	k1gFnSc3
z	z	k7c2
nějakého	nějaký	k3yIgInSc2
důvodu	důvod	k1gInSc2
nemá	mít	k5eNaImIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
disku	disco	k1gNnSc3
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
odemknout	odemknout	k5eAaPmF
si	se	k3xPyFc3
instalaci	instalace	k1gFnSc4
na	na	k7c4
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
po	po	k7c6
zakoupení	zakoupení	k1gNnSc6
hry	hra	k1gFnSc2
na	na	k7c4
Xbox	Xbox	k1gInSc4
Live	Liv	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nainstalovaná	nainstalovaný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
chová	chovat	k5eAaImIp3nS
jako	jako	k9
digitální	digitální	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hry	hra	k1gFnPc4
pro	pro	k7c4
jednoho	jeden	k4xCgMnSc4
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
těží	těžet	k5eAaImIp3nP
z	z	k7c2
výhod	výhoda	k1gFnPc2
cloudu	cloudu	k6eAd1
<g/>
,	,	kIx,
vyžadují	vyžadovat	k5eAaImIp3nP
připojení	připojení	k1gNnSc4
k	k	k7c3
internetu	internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
původně	původně	k6eAd1
nedisponoval	disponovat	k5eNaBmAgMnS
zpětnou	zpětný	k2eAgFnSc7d1
kompatibilitou	kompatibilita	k1gFnSc7
s	s	k7c7
hrami	hra	k1gFnPc7
na	na	k7c4
původní	původní	k2eAgInSc4d1
Xbox	Xbox	k1gInSc4
nebo	nebo	k8xC
Xbox	Xbox	k1gInSc4
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Larry	Larra	k1gFnSc2
„	„	k?
<g/>
Major	major	k1gMnSc1
Nelson	Nelson	k1gMnSc1
<g/>
“	“	k?
Nelson	Nelson	k1gMnSc1
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
programování	programování	k1gNnSc2
Xbox	Xbox	k1gInSc1
Live	Live	k1gNnPc2
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
uživatelé	uživatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
teoreticky	teoreticky	k6eAd1
použít	použít	k5eAaPmF
port	port	k1gInSc4
HDMI-in	HDMI-ina	k1gFnPc2
na	na	k7c4
konzoli	konzole	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
spustili	spustit	k5eAaPmAgMnP
Xbox	Xbox	k1gInSc4
360	#num#	k4
skrze	skrze	k?
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senior	senior	k1gMnSc1
ředitel	ředitel	k1gMnSc1
Albert	Albert	k1gMnSc1
Penello	Penello	k1gNnSc4
odhalil	odhalit	k5eAaPmAgMnS
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
Microsoft	Microsoft	kA
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
nabídnout	nabídnout	k5eAaPmF
zpětnou	zpětný	k2eAgFnSc4d1
kompatibilitu	kompatibilita	k1gFnSc4
se	s	k7c7
staršími	starý	k2eAgFnPc7d2
hrami	hra	k1gFnPc7
prostřednictvím	prostřednictvím	k7c2
cloudového	cloudový	k2eAgInSc2d1
herního	herní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přijetí	přijetí	k1gNnSc1
</s>
<s>
Před	před	k7c7
vydáním	vydání	k1gNnSc7
</s>
<s>
Po	po	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
odhalení	odhalení	k1gNnSc6
v	v	k7c6
květnu	květen	k1gInSc6
2013	#num#	k4
přišla	přijít	k5eAaPmAgFnS
redakce	redakce	k1gFnSc1
Game	game	k1gInSc1
Informeru	Informer	k1gInSc2
jak	jak	k8xS,k8xC
s	s	k7c7
chválou	chvála	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
kritikou	kritika	k1gFnSc7
na	na	k7c4
konzoli	konzole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matt	Matt	k1gMnSc1
Helgeson	Helgeson	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
konzoli	konzole	k1gFnSc4
jako	jako	k8xC,k8xS
záměr	záměr	k1gInSc4
Microsoftu	Microsoft	k1gInSc2
na	na	k7c4
„	„	k?
<g/>
ovládnutí	ovládnutí	k1gNnSc4
obývacího	obývací	k2eAgInSc2d1
pokoje	pokoj	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžité	okamžitý	k2eAgFnPc1d1
přepínací	přepínací	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
Xboxu	Xbox	k1gInSc2
One	One	k1gMnSc2
označil	označit	k5eAaPmAgMnS
za	za	k7c2
„	„	k?
<g/>
působivé	působivý	k2eAgNnSc1d1
<g/>
“	“	k?
a	a	k8xC
samotnou	samotný	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
za	za	k7c4
„	„	k?
<g/>
krok	krok	k1gInSc4
správným	správný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
TV	TV	kA
zábavy	zábava	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
možnost	možnost	k1gFnSc1
vyhnutí	vyhnutí	k1gNnSc2
se	se	k3xPyFc4
používání	používání	k1gNnSc1
neintuitivního	intuitivní	k2eNgNnSc2d1
uživatelského	uživatelský	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
na	na	k7c6
četných	četný	k2eAgInPc6d1
kabelových	kabelový	k2eAgInPc6d1
set-top	set-top	k1gInSc1
boxech	box	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeff	Jeff	k1gMnSc1
Cork	Cork	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Microsoft	Microsoft	kA
má	mít	k5eAaImIp3nS
ohledně	ohledně	k6eAd1
konzole	konzola	k1gFnSc3
„	„	k?
<g/>
několik	několik	k4yIc1
výborných	výborná	k1gFnPc2
nápadů	nápad	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
selhal	selhat	k5eAaPmAgMnS
při	při	k7c6
jejich	jejich	k3xOp3gFnSc6
správné	správný	k2eAgFnSc6d1
interpretaci	interpretace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Microsoftu	Microsoft	k1gInSc2
na	na	k7c6
E3	E3	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
vnímání	vnímání	k1gNnSc2
kritiků	kritik	k1gMnPc2
ohledně	ohledně	k7c2
Xboxu	Xbox	k1gInSc2
One	One	k1gFnSc2
změnilo	změnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
redaktorů	redaktor	k1gMnPc2
serveru	server	k1gInSc2
GameSpot	GameSpota	k1gFnPc2
bylo	být	k5eAaImAgNnS
dosti	dosti	k6eAd1
kritických	kritický	k2eAgNnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mark	Mark	k1gMnSc1
Walton	Walton	k1gInSc4
označil	označit	k5eAaPmAgMnS
startovací	startovací	k2eAgInPc4d1
tituly	titul	k1gInPc4
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
uvedené	uvedený	k2eAgFnSc2d1
na	na	k7c6
konferenci	konference	k1gFnSc6
za	za	k7c4
„	„	k?
<g/>
nemastné	mastný	k2eNgFnPc4d1
<g/>
,	,	kIx,
neslané	slaný	k2eNgFnPc4d1
<g/>
“	“	k?
a	a	k8xC
sužované	sužovaný	k2eAgNnSc4d1
„	„	k?
<g/>
starými	starý	k2eAgMnPc7d1
kravaťáky	kravaťák	k1gMnPc7
a	a	k8xC
proudem	proud	k1gInSc7
populárních	populární	k2eAgFnPc2d1
rádoby	rádoby	k6eAd1
slovíček	slovíčko	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hry	hra	k1gFnPc1
byly	být	k5eAaImAgFnP
prý	prý	k9
„	„	k?
<g/>
povrchní	povrchnět	k5eAaImIp3nS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
oceňují	oceňovat	k5eAaImIp3nP
vizuální	vizuální	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
před	před	k7c7
inovací	inovace	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
přesný	přesný	k2eAgInSc1d1
opak	opak	k1gInSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
slíbil	slíbit	k5eAaPmAgMnS
Microsoft	Microsoft	kA
prezentovat	prezentovat	k5eAaBmF
v	v	k7c6
průběhu	průběh	k1gInSc6
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
„	„	k?
<g/>
novou	nový	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
hraní	hraní	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
nemluvě	nemluva	k1gFnSc3
o	o	k7c6
přísných	přísný	k2eAgFnPc6d1
restrikcích	restrikce	k1gFnPc6
ohledně	ohledně	k7c2
správy	správa	k1gFnSc2
digitálních	digitální	k2eAgNnPc2d1
práv	právo	k1gNnPc2
(	(	kIx(
<g/>
DRM	DRM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redaktor	redaktor	k1gMnSc1
Tom	Tom	k1gMnSc1
McShea	McShea	k1gMnSc1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
přes	přes	k7c4
zvýšený	zvýšený	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
Xboxu	Xbox	k1gInSc2
One	One	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
orientaci	orientace	k1gFnSc4
na	na	k7c6
cloud	cloud	k6eAd1
se	se	k3xPyFc4
prezentace	prezentace	k1gFnSc1
skládala	skládat	k5eAaImAgFnS
pouze	pouze	k6eAd1
z	z	k7c2
„	„	k?
<g/>
krásných	krásný	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nenabízely	nabízet	k5eNaImAgFnP
žádnou	žádný	k3yNgFnSc4
viditelnou	viditelný	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
samotném	samotný	k2eAgInSc6d1
zážitku	zážitek	k1gInSc6
z	z	k7c2
hraní	hraní	k1gNnSc2
oproti	oproti	k7c3
Xboxu	Xbox	k1gInSc3
360	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
stávající	stávající	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
nemají	mít	k5eNaImIp3nP
moc	moc	k6eAd1
důvodu	důvod	k1gInSc2
utratit	utratit	k5eAaPmF
499	#num#	k4
dolarů	dolar	k1gInPc2
za	za	k7c4
novou	nový	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
Novináři	novinář	k1gMnPc1
a	a	k8xC
spotřebitelé	spotřebitel	k1gMnPc1
vtipně	vtipně	k6eAd1
pojmenovali	pojmenovat	k5eAaPmAgMnP
konzoli	konzole	k1gFnSc4
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Xbone	Xbon	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tak	tak	k6eAd1
dali	dát	k5eAaPmAgMnP
Microsoftu	Microsofta	k1gFnSc4
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
mnohá	mnohé	k1gNnPc1
jeho	jeho	k3xOp3gNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
se	se	k3xPyFc4
zakládala	zakládat	k5eAaImAgFnS
na	na	k7c6
špatném	špatný	k2eAgInSc6d1
úsudku	úsudek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Sony	Sony	kA
později	pozdě	k6eAd2
téhož	týž	k3xTgInSc2
večera	večer	k1gInSc2
McShea	McShea	k1gMnSc1
pokračoval	pokračovat	k5eAaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
označil	označit	k5eAaPmAgMnS
Microsoft	Microsoft	kA
za	za	k7c4
protizákaznický	protizákaznický	k2eAgInSc4d1
a	a	k8xC
snažící	snažící	k2eAgInSc4d1
se	s	k7c7
„	„	k?
<g/>
potrestat	potrestat	k5eAaPmF
své	svůj	k3xOyFgMnPc4
věrné	věrný	k2eAgMnPc4d1
zákazníky	zákazník	k1gMnPc4
<g/>
“	“	k?
přísnými	přísný	k2eAgNnPc7d1
omezeními	omezení	k1gNnPc7
a	a	k8xC
že	že	k8xS
kvůli	kvůli	k7c3
„	„	k?
<g/>
trvání	trvání	k1gNnPc1
na	na	k7c6
omezeních	omezení	k1gNnPc6
ohledně	ohledně	k7c2
použitých	použitý	k2eAgFnPc2d1
her	hra	k1gFnPc2
a	a	k8xC
nutnosti	nutnost	k1gFnSc2
být	být	k5eAaImF
neustále	neustále	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
Microsoft	Microsoft	kA
tak	tak	k6eAd1
radostně	radostně	k6eAd1
implementoval	implementovat	k5eAaImAgMnS
do	do	k7c2
Xboxu	Xbox	k1gInSc2
One	One	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Sony	son	k1gInPc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
konzolí	konzolit	k5eAaPmIp3nP,k5eAaImIp3nP
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
pasována	pasovat	k5eAaImNgFnS,k5eAaBmNgFnS
za	za	k7c4
favorita	favorit	k1gMnSc4
letošních	letošní	k2eAgFnPc2d1
Vánoc	Vánoce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Rafi	Rafi	k6eAd1
Mohammed	Mohammed	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
knihy	kniha	k1gFnSc2
„	„	k?
<g/>
The	The	k1gMnSc1
Art	Art	k1gMnSc1
of	of	k?
Pricing	Pricing	k1gInSc1
<g/>
“	“	k?
řekl	říct	k5eAaPmAgInS
televizi	televize	k1gFnSc3
Bloomberg	Bloomberg	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Microsoft	Microsoft	kA
nasadil	nasadit	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
„	„	k?
<g/>
příliš	příliš	k6eAd1
vysoko	vysoko	k6eAd1
<g/>
“	“	k?
a	a	k8xC
že	že	k8xS
těch	ten	k3xDgInPc2
100	#num#	k4
dolarů	dolar	k1gInPc2
navíc	navíc	k6eAd1
oproti	oproti	k7c3
svému	svůj	k3xOyFgMnSc3
konkurentovi	konkurent	k1gMnSc3
by	by	k9
mohlo	moct	k5eAaImAgNnS
konzoli	konzoli	k6eAd1
o	o	k7c6
letošních	letošní	k2eAgFnPc6d1
Vánocích	Vánoce	k1gFnPc6
„	„	k?
<g/>
vykolejit	vykolejit	k5eAaPmF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Prodeje	prodej	k1gInPc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
Microsoft	Microsoft	kA
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
prodal	prodat	k5eAaPmAgMnS
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc4
konzolí	konzole	k1gFnPc2
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc4
během	během	k7c2
prvních	první	k4xOgFnPc2
24	#num#	k4
hodin	hodina	k1gFnPc2
po	po	k7c6
uvedení	uvedení	k1gNnSc6
na	na	k7c4
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
sledování	sledování	k1gNnSc2
zhruba	zhruba	k6eAd1
102	#num#	k4
000	#num#	k4
nákupních	nákupní	k2eAgFnPc2d1
stvrzenek	stvrzenka	k1gFnPc2
společností	společnost	k1gFnPc2
InfoScout	InfoScout	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
v	v	k7c6
1500	#num#	k4
případech	případ	k1gInPc6
objevil	objevit	k5eAaPmAgInS
nákup	nákup	k1gInSc1
buď	buď	k8xC
videohry	videohra	k1gFnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
videoherní	videoherní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
stal	stát	k5eAaPmAgInS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
v	v	k7c6
období	období	k1gNnSc6
slev	sleva	k1gFnPc2
na	na	k7c4
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
nejprodávanější	prodávaný	k2eAgNnSc1d3
konzolí	konzolí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
Microsoft	Microsoft	kA
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
prodal	prodat	k5eAaPmAgMnS
okolo	okolo	k7c2
2	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
za	za	k7c4
prvních	první	k4xOgInPc2
18	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
pak	pak	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
základě	základ	k1gInSc6
údajů	údaj	k1gInPc2
NPD	NPD	kA
Group	Group	k1gInSc1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejrychleji	rychle	k6eAd3
prodávanou	prodávaný	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
zpráva	zpráva	k1gFnSc1
NPD	NPD	kA
objasnila	objasnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
prodeje	prodej	k1gInPc1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
zahrnovaly	zahrnovat	k5eAaImAgFnP
další	další	k2eAgInSc4d1
týden	týden	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
porovnávání	porovnávání	k1gNnSc2
listopadových	listopadový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
oproti	oproti	k7c3
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíváme	podívat	k5eAaPmIp1nP,k5eAaImIp1nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
na	na	k7c4
prodeje	prodej	k1gInPc4
na	na	k7c6
bázi	báze	k1gFnSc6
průměrných	průměrný	k2eAgNnPc2d1
týdenních	týdenní	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
porazil	porazit	k5eAaPmAgInS
PS	PS	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
mít	mít	k5eAaImF
na	na	k7c6
paměti	paměť	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
druhém	druhý	k4xOgInSc6
týdnu	týden	k1gInSc6
po	po	k7c6
uvedení	uvedení	k1gNnSc6
na	na	k7c4
trh	trh	k1gInSc4
bývají	bývat	k5eAaImIp3nP
dodávky	dodávka	k1gFnPc1
zařízení	zařízení	k1gNnPc2
obvykle	obvykle	k6eAd1
značně	značně	k6eAd1
omezeny	omezit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
”	”	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
Microsoft	Microsoft	kA
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
před	před	k7c7
koncem	konec	k1gInSc7
roku	rok	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
celosvětově	celosvětově	k6eAd1
prodalo	prodat	k5eAaPmAgNnS
přes	přes	k7c4
3	#num#	k4
miliony	milion	k4xCgInPc4
konzolí	konzole	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Microsoft	Microsoft	kA
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
finanční	finanční	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
za	za	k7c4
2	#num#	k4
<g/>
.	.	kIx.
kvartál	kvartál	k1gInSc1
fiskálního	fiskální	k2eAgInSc2d1
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
uveřejněna	uveřejnit	k5eAaPmNgFnS
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
celosvětově	celosvětově	k6eAd1
bylo	být	k5eAaImAgNnS
vyexpedováno	vyexpedovat	k5eAaPmNgNnS
3,9	3,9	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
konzole	konzola	k1gFnSc3
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
společnost	společnost	k1gFnSc1
Microsoft	Microsoft	kA
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
dodala	dodat	k5eAaPmAgFnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
téměř	téměř	k6eAd1
10	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
oznámil	oznámit	k5eAaPmAgMnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
nebude	být	k5eNaImBp3nS
zveřejňovat	zveřejňovat	k5eAaImF
údaje	údaj	k1gInPc4
o	o	k7c6
prodejích	prodej	k1gFnPc6
systému	systém	k1gInSc2
<g/>
,	,	kIx,
údaj	údaj	k1gInSc1
z	z	k7c2
listopadu	listopad	k1gInSc2
2014	#num#	k4
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
stále	stále	k6eAd1
nejnovějším	nový	k2eAgNnSc7d3
oficiálním	oficiální	k2eAgNnSc7d1
prodejním	prodejní	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Analytik	analytik	k1gMnSc1
průmyslu	průmysl	k1gInSc2
videoher	videohra	k1gFnPc2
Daniel	Daniel	k1gMnSc1
Ahmad	Ahmad	k1gInSc4
odhadl	odhadnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
lednu	leden	k1gInSc3
2019	#num#	k4
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
přibližně	přibližně	k6eAd1
41	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalifornská	kalifornský	k2eAgFnSc1d1
Turtle	Turtle	k1gFnSc1
Beach	Beach	k1gMnSc1
Corporation	Corporation	k1gInSc1
odhadla	odhadnout	k5eAaPmAgFnS
prodeje	prodej	k1gInSc2
konzole	konzola	k1gFnSc6
ke	k	k7c3
třetímu	třetí	k4xOgNnSc3
čtvrtletí	čtvrtletí	k1gNnSc3
roku	rok	k1gInSc2
2019	#num#	k4
na	na	k7c4
50	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
prodejů	prodej	k1gInPc2
konkurenční	konkurenční	k2eAgFnSc2d1
PS	PS	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Použité	použitý	k2eAgFnPc1d1
hry	hra	k1gFnPc1
a	a	k8xC
politika	politika	k1gFnSc1
ověřování	ověřování	k1gNnSc2
přes	přes	k7c4
internet	internet	k1gInSc4
</s>
<s>
Při	při	k7c6
prvním	první	k4xOgInSc6
odhalení	odhalení	k1gNnSc6
konzole	konzola	k1gFnSc6
Microsoft	Microsoft	kA
představil	představit	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
funkcí	funkce	k1gFnPc2
a	a	k8xC
politik	politika	k1gFnPc2
pro	pro	k7c4
hry	hra	k1gFnPc4
na	na	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
klást	klást	k5eAaImF
důraz	důraz	k1gInSc4
na	na	k7c6
„	„	k?
<g/>
neustále	neustále	k6eAd1
připojenou	připojený	k2eAgFnSc4d1
<g/>
“	“	k?
konzoli	konzole	k1gFnSc4
a	a	k8xC
digitální	digitální	k2eAgInSc4d1
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
přinést	přinést	k5eAaPmF
řadu	řada	k1gFnSc4
výhod	výhoda	k1gFnPc2
jak	jak	k8xS,k8xC
pro	pro	k7c4
vývojáře	vývojář	k1gMnSc4
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
DRM	DRM	kA
by	by	kYmCp3nS
provázal	provázat	k5eAaPmAgMnS
všechny	všechen	k3xTgFnPc4
hry	hra	k1gFnPc4
(	(	kIx(
<g/>
ať	ať	k8xC,k8xS
už	už	k6eAd1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
zakoupeny	zakoupit	k5eAaPmNgFnP
ve	v	k7c4
fyzické	fyzický	k2eAgNnSc4d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
digitální	digitální	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
)	)	kIx)
s	s	k7c7
uživatelovým	uživatelův	k2eAgInSc7d1
účtem	účet	k1gInSc7
Xbox	Xbox	k1gInSc1
Live	Live	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
konzolí	konzolit	k5eAaPmIp3nS,k5eAaImIp3nS
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
by	by	kYmCp3nP
umožnil	umožnit	k5eAaPmAgInS
lokální	lokální	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
uživatelovým	uživatelův	k2eAgFnPc3d1
hrám	hra	k1gFnPc3
prostřednictvím	prostřednictvím	k7c2
herní	herní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c4
cloudu	clouda	k1gFnSc4
na	na	k7c6
kterémkoli	kterýkoli	k3yIgInSc6
dalším	další	k2eAgInSc6d1
Xboxu	Xbox	k1gInSc6
One	One	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herní	herní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
sdílena	sdílet	k5eAaImNgFnS
až	až	k9
s	s	k7c7
deseti	deset	k4xCc7
vybranými	vybraná	k1gFnPc7
„	„	k?
<g/>
rodinnými	rodinný	k2eAgMnPc7d1
<g/>
“	“	k?
členy	člen	k1gMnPc7
(	(	kIx(
<g/>
každou	každý	k3xTgFnSc4
hru	hra	k1gFnSc4
by	by	kYmCp3nP
v	v	k7c4
daný	daný	k2eAgInSc4d1
okamžik	okamžik	k1gInSc4
mohl	moct	k5eAaImAgInS
hrát	hrát	k5eAaImF
pouze	pouze	k6eAd1
jeden	jeden	k4xCgMnSc1
uživatel	uživatel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
by	by	kYmCp3nP
však	však	k9
rovněž	rovněž	k9
vyžadoval	vyžadovat	k5eAaImAgMnS
připojování	připojování	k1gNnSc4
konzole	konzola	k1gFnSc3
k	k	k7c3
internetu	internet	k1gInSc3
v	v	k7c6
pravidelných	pravidelný	k2eAgInPc6d1
intervalech	interval	k1gInPc6
(	(	kIx(
<g/>
nejméně	málo	k6eAd3
jednou	jeden	k4xCgFnSc7
za	za	k7c4
24	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
synchronizaci	synchronizace	k1gFnSc3
knihovny	knihovna	k1gFnSc2
a	a	k8xC
stažení	stažení	k1gNnSc2
aktualizací	aktualizace	k1gFnPc2
do	do	k7c2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nP
k	k	k7c3
tomu	ten	k3xDgNnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
hru	hra	k1gFnSc4
by	by	kYmCp3nS
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
hrát	hrát	k5eAaImF
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
by	by	kYmCp3nS
nebyla	být	k5eNaImAgFnS
konzole	konzola	k1gFnSc3
znovu	znovu	k6eAd1
připojena	připojen	k2eAgFnSc1d1
k	k	k7c3
internetu	internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
by	by	kYmCp3nS
mohli	moct	k5eAaImAgMnP
obchodovat	obchodovat	k5eAaImF
či	či	k8xC
vyměňovat	vyměňovat	k5eAaImF
hry	hra	k1gFnPc4
„	„	k?
<g/>
vybraných	vybraný	k2eAgMnPc2d1
prodejců	prodejce	k1gMnPc2
<g/>
“	“	k?
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgInPc2
dalších	další	k2eAgInPc2d1
poplatků	poplatek	k1gInPc2
a	a	k8xC
také	také	k9
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
půjčovat	půjčovat	k5eAaImF
kterémukoli	kterýkoli	k3yIgMnSc3
svému	svůj	k1gMnSc3
příteli	přítel	k1gMnSc3
ze	z	k7c2
seznamu	seznam	k1gInSc2
na	na	k7c4
Xbox	Xbox	k1gInSc4
Live	Liv	k1gFnSc2
po	po	k7c4
dobu	doba	k1gFnSc4
alespoň	alespoň	k9
30	#num#	k4
dnů	den	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
jedenkrát	jedenkrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
rostoucí	rostoucí	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
Microsoft	Microsoft	kA
vydal	vydat	k5eAaPmAgInS
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
prohlášení	prohlášení	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
nastínil	nastínit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
budou	být	k5eAaImBp3nP
původně	původně	k6eAd1
navržené	navržený	k2eAgFnPc1d1
politiky	politika	k1gFnPc1
zmírněny	zmírnit	k5eAaPmNgFnP
na	na	k7c4
podobnou	podobný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
se	se	k3xPyFc4
už	už	k6eAd1
nepočítalo	počítat	k5eNaImAgNnS
s	s	k7c7
požadavkem	požadavek	k1gInSc7
na	na	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
internetu	internet	k1gInSc3
<g/>
,	,	kIx,
žádným	žádný	k3yNgNnSc7
ověřováním	ověřování	k1gNnSc7
disku	disk	k1gInSc2
a	a	k8xC
regionálními	regionální	k2eAgNnPc7d1
omezeními	omezení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
změn	změna	k1gFnPc2
se	se	k3xPyFc4
upustilo	upustit	k5eAaPmAgNnS
i	i	k9
od	od	k7c2
rodinného	rodinný	k2eAgNnSc2d1
sdílení	sdílení	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
od	od	k7c2
sdílené	sdílený	k2eAgFnSc2d1
digitálních	digitální	k2eAgMnPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marc	Marc	k1gInSc1
Whitten	Whitten	k2eAgInSc1d1
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgMnSc1d1
produktový	produktový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
odstranění	odstranění	k1gNnSc3
možnosti	možnost	k1gFnSc2
rodinného	rodinný	k2eAgNnSc2d1
sdílení	sdílení	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
z	z	k7c2
důvodu	důvod	k1gInSc2
revize	revize	k1gFnSc1
politik	politik	k1gMnSc1
Xbox	Xbox	k1gInSc1
One	One	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
dodat	dodat	k5eAaPmF
konzole	konzola	k1gFnSc3
včas	včas	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
tento	tento	k3xDgInSc1
prvek	prvek	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
zakomponován	zakomponovat	k5eAaPmNgInS
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
prvním	první	k4xOgInSc6
připojení	připojení	k1gNnSc6
Xboxu	Xbox	k1gInSc2
One	One	k1gFnSc2
k	k	k7c3
internetu	internet	k1gInSc3
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
stáhnout	stáhnout	k5eAaPmF
aktualizace	aktualizace	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
zpřístupní	zpřístupnit	k5eAaPmIp3nS
offline	offlin	k1gInSc5
režim	režim	k1gInSc4
a	a	k8xC
aktualizuje	aktualizovat	k5eAaBmIp3nS
systémový	systémový	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
umožní	umožnit	k5eAaPmIp3nS
další	další	k2eAgFnPc4d1
změny	změna	k1gFnPc4
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
prohlášení	prohlášení	k1gNnSc6
týkající	týkající	k2eAgFnSc1d1
se	se	k3xPyFc4
nových	nový	k2eAgFnPc2d1
politik	politika	k1gFnPc2
Mattrick	Mattrick	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
si	se	k3xPyFc3
vzala	vzít	k5eAaPmAgFnS
k	k	k7c3
srdci	srdce	k1gNnSc3
negativní	negativní	k2eAgInSc4d1
názor	názor	k1gInSc4
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
poděkoval	poděkovat	k5eAaPmAgMnS
za	za	k7c4
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
„	„	k?
<g/>
restrukturalizovala	restrukturalizovat	k5eAaImAgFnS
budoucnost	budoucnost	k1gFnSc4
Xbox	Xbox	k1gInSc1
One	One	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mattrick	Mattrick	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
stál	stát	k5eAaImAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
vývoje	vývoj	k1gInSc2
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
<g/>
,	,	kIx,
ohlásil	ohlásit	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
svůj	svůj	k3xOyFgInSc4
odchod	odchod	k1gInSc4
z	z	k7c2
Microsoftu	Microsoft	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
CEO	CEO	kA
ve	v	k7c6
společnosti	společnost	k1gFnSc6
Zynga	Zyng	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytici	analytik	k1gMnPc1
spekulovali	spekulovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInSc1
odchod	odchod	k1gInSc1
je	být	k5eAaImIp3nS
spjat	spjat	k2eAgInSc1d1
se	s	k7c7
špatnou	špatný	k2eAgFnSc7d1
odezvou	odezva	k1gFnSc7
a	a	k8xC
následným	následný	k2eAgNnSc7d1
zrušením	zrušení	k1gNnSc7
četných	četný	k2eAgInPc2d1
plánů	plán	k1gInPc2
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obavy	obava	k1gFnPc1
z	z	k7c2
ochrany	ochrana	k1gFnSc2
soukromí	soukromí	k1gNnSc2
</s>
<s>
Časté	častý	k2eAgNnSc1d1
používání	používání	k1gNnSc1
Kinectu	Kinect	k1gInSc2
ze	z	k7c2
strany	strana	k1gFnSc2
konzole	konzola	k1gFnSc3
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
předmětem	předmět	k1gInSc7
obav	obava	k1gFnPc2
ohledně	ohledně	k7c2
potenciálního	potenciální	k2eAgNnSc2d1
využití	využití	k1gNnSc2
ke	k	k7c3
sledování	sledování	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vyplývalo	vyplývat	k5eAaImAgNnS
z	z	k7c2
původně	původně	k6eAd1
ohlášeného	ohlášený	k2eAgInSc2d1
požadavku	požadavek	k1gInSc2
na	na	k7c4
neustálé	neustálý	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
periferie	periferie	k1gFnSc2
ke	k	k7c3
konzoli	konzole	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastánci	zastánce	k1gMnPc1
soukromí	soukromý	k2eAgMnPc1d1
tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
zvýšené	zvýšený	k2eAgNnSc4d1
množství	množství	k1gNnSc4
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
novým	nový	k2eAgInSc7d1
Kinectem	Kinect	k1gInSc7
shromažďovány	shromažďován	k2eAgInPc4d1
(	(	kIx(
<g/>
například	například	k6eAd1
pohyby	pohyb	k1gInPc4
očí	oko	k1gNnPc2
<g/>
,	,	kIx,
srdeční	srdeční	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
a	a	k8xC
nálada	nálada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
využity	využít	k5eAaPmNgFnP
k	k	k7c3
cílené	cílený	k2eAgFnSc3d1
reklamě	reklama	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
se	se	k3xPyFc4
vyrojily	vyrojit	k5eAaPmAgFnP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
patentech	patent	k1gInPc6
Microsoftu	Microsoft	k1gInSc2
zahrnující	zahrnující	k2eAgInSc1d1
Kinect	Kinect	k1gInSc1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
třeba	třeba	k6eAd1
DRM	DRM	kA
na	na	k7c6
základě	základ	k1gInSc6
detekování	detekování	k1gNnSc2
počtu	počet	k1gInSc2
diváků	divák	k1gMnPc2
v	v	k7c6
místnosti	místnost	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
sledování	sledování	k1gNnSc4
návyků	návyk	k1gInPc2
uživatelů	uživatel	k1gMnPc2
udělováním	udělování	k1gNnSc7
úspěchů	úspěch	k1gInPc2
(	(	kIx(
<g/>
achievementů	achievement	k1gInPc2
<g/>
)	)	kIx)
za	za	k7c4
sledování	sledování	k1gNnSc4
televizních	televizní	k2eAgInPc2d1
programů	program	k1gInPc2
a	a	k8xC
reklam	reklama	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Microsoft	Microsoft	kA
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnPc1
zásady	zásada	k1gFnPc1
ochrany	ochrana	k1gFnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
„	„	k?
<g/>
zakazují	zakazovat	k5eAaImIp3nP
shromažďování	shromažďování	k1gNnSc1
<g/>
,	,	kIx,
uchovávání	uchovávání	k1gNnSc1
nebo	nebo	k8xC
používání	používání	k1gNnSc1
údajů	údaj	k1gInPc2
z	z	k7c2
Kinectu	Kinect	k1gInSc2
pro	pro	k7c4
reklamní	reklamní	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
kritici	kritik	k1gMnPc1
nevyloučili	vyloučit	k5eNaPmAgMnP
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc1
politiky	politika	k1gFnPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
před	před	k7c7
vydáním	vydání	k1gNnSc7
konzole	konzola	k1gFnSc3
změněny	změněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
byly	být	k5eAaImAgFnP
vzneseny	vznesen	k2eAgFnPc1d1
obavy	obava	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
zařízení	zařízení	k1gNnSc1
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
nahrávat	nahrávat	k5eAaImF
konverzaci	konverzace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
jeho	jeho	k3xOp3gInSc1
mikrofon	mikrofon	k1gInSc1
zůstává	zůstávat	k5eAaImIp3nS
aktivní	aktivní	k2eAgMnSc1d1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
kritiku	kritika	k1gFnSc4
mluvčí	mluvčí	k1gMnSc1
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
obsah	obsah	k1gInSc1
vytvářený	vytvářený	k2eAgInSc1d1
uživateli	uživatel	k1gMnPc7
jako	jako	k8xS,k8xC
fotky	fotka	k1gFnSc2
a	a	k8xC
videa	video	k1gNnSc2
„	„	k?
<g/>
neopustí	opustit	k5eNaPmIp3nS
váš	váš	k3xOp2gInSc1
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
bez	bez	k7c2
explicitního	explicitní	k2eAgInSc2d1
souhlasu	souhlas	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
že	že	k8xS
během	během	k7c2
počátečního	počáteční	k2eAgNnSc2d1
nastavení	nastavení	k1gNnSc2
konzole	konzola	k1gFnSc3
budou	být	k5eAaImBp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
klíčové	klíčový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
ohledně	ohledně	k7c2
soukromí	soukromí	k1gNnSc2
a	a	k8xC
nastavení	nastavení	k1gNnSc2
Kinectu	Kinect	k1gInSc2
(	(	kIx(
<g/>
uživatelé	uživatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
Kinect	Kinect	k1gInSc4
zapnutý	zapnutý	k2eAgInSc4d1
<g/>
,	,	kIx,
vypnutý	vypnutý	k2eAgInSc4d1
nebo	nebo	k8xC
v	v	k7c6
pozastaveném	pozastavený	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
vzít	vzít	k5eAaPmF
zpět	zpět	k6eAd1
své	svůj	k3xOyFgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
o	o	k7c6
nutném	nutný	k2eAgNnSc6d1
používání	používání	k1gNnSc6
Kinectu	Kinect	k1gInSc2
na	na	k7c4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
je	být	k5eAaImIp3nS
periferie	periferie	k1gFnSc1
dodávána	dodávat	k5eAaImNgFnS
spolu	spolu	k6eAd1
s	s	k7c7
konzolí	konzole	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Novější	nový	k2eAgFnSc1d2
verze	verze	k1gFnSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
S	s	k7c7
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
X	X	kA
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
S	s	k7c7
</s>
<s>
Zmenšený	zmenšený	k2eAgInSc1d1
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc1
(	(	kIx(
<g/>
typ	typ	k1gInSc1
1681	#num#	k4
<g/>
)	)	kIx)
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
2016	#num#	k4
jako	jako	k8xS,k8xC
nástupnická	nástupnický	k2eAgFnSc1d1
verze	verze	k1gFnSc1
původní	původní	k2eAgFnSc1d1
konzole	konzola	k1gFnSc3
<g/>
,	,	kIx,
k	k	k7c3
dispozici	dispozice	k1gFnSc3
byl	být	k5eAaImAgInS
s	s	k7c7
500	#num#	k4
GB	GB	kA
<g/>
,	,	kIx,
1	#num#	k4
TB	TB	kA
a	a	k8xC
2TB	2TB	k4
diskové	diskový	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
40	#num#	k4
%	%	kIx~
menší	malý	k2eAgFnSc4d2
verzi	verze	k1gFnSc4
než	než	k8xS
původní	původní	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibyl	Přibyl	k1gMnSc1
infračervený	infračervený	k2eAgInSc4d1
port	port	k1gInSc4
<g/>
,	,	kIx,
bluetooth	bluetooth	k1gInSc4
4.0	4.0	k4
a	a	k8xC
HDMI	HDMI	kA
port	port	k1gInSc1
byl	být	k5eAaImAgInS
aktualizován	aktualizovat	k5eAaBmNgInS
z	z	k7c2
verze	verze	k1gFnSc2
1,4	1,4	k4
na	na	k7c4
2,0	2,0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
byl	být	k5eAaImAgInS
odstraněn	odstranit	k5eAaPmNgInS
port	port	k1gInSc1
na	na	k7c4
Kinect	Kinect	k2eAgInSc4d1
senzor	senzor	k1gInSc4
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
připojit	připojit	k5eAaPmF
jen	jen	k9
přes	přes	k7c4
dokoupený	dokoupený	k2eAgInSc4d1
konektor	konektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
změna	změna	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
grafický	grafický	k2eAgInSc1d1
výkon	výkon	k1gInSc1
narostl	narůst	k5eAaPmAgInS
z	z	k7c2
1.31	1.31	k4
teraFLOPS	teraFLOPS	k?
na	na	k7c4
1.4	1.4	k4
teraFLOPS	teraFLOPS	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
S	s	k7c7
All-Digital	All-Digital	k1gMnSc1
Edition	Edition	k1gInSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
Microsoft	Microsoft	kA
uvedl	uvést	k5eAaPmAgMnS
"	"	kIx"
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
"	"	kIx"
verzi	verze	k1gFnSc4
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
S	s	k7c7
nazvanou	nazvaný	k2eAgFnSc7d1
Xbox	Xbox	k1gInSc1
One	One	k1gFnPc4
S	s	k7c7
All-Digital	All-Digital	k1gMnSc1
Edition	Edition	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
optickou	optický	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
Blu-ray	Blu-raa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
1	#num#	k4
TB	TB	kA
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnSc1d1
prodejní	prodejní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
startuje	startovat	k5eAaBmIp3nS
na	na	k7c4
5999	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
ceně	cena	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
hry	hra	k1gFnPc4
Minecraft	Minecraftum	k1gNnPc2
<g/>
,	,	kIx,
Sea	Sea	k1gFnPc2
of	of	k?
Thieves	Thievesa	k1gFnPc2
a	a	k8xC
Fortnite	Fortnit	k1gInSc5
Battle	Battle	k1gFnPc4
Royal	Royal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
X	X	kA
</s>
<s>
Modernizovaná	modernizovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Xbox	Xbox	k1gInSc1
One	One	k1gMnSc1
X	X	kA
(	(	kIx(
<g/>
typ	typ	k1gInSc1
1787	#num#	k4
<g/>
)	)	kIx)
vyšla	vyjít	k5eAaPmAgFnS
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
verzi	verze	k1gFnSc6
se	s	k7c7
zdokonaleným	zdokonalený	k2eAgInSc7d1
hardwarem	hardware	k1gInSc7
umožňující	umožňující	k2eAgNnSc4d1
rozlišení	rozlišení	k1gNnSc4
4	#num#	k4
<g/>
K	K	kA
<g/>
,	,	kIx,
úpravy	úprava	k1gFnSc2
dostál	dostát	k5eAaPmAgInS
také	také	k9
design	design	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disponuje	disponovat	k5eAaBmIp3nS
výkonnějším	výkonný	k2eAgNnSc7d2
GPU	GPU	kA
zvládající	zvládající	k2eAgMnSc1d1
až	až	k8xS
6	#num#	k4
teraFLOPS	teraFLOPS	k?
<g/>
,	,	kIx,
rychlejším	rychlý	k2eAgNnSc6d2
CPU	CPU	kA
v	v	k7c6
taktu	takt	k1gInSc6
2,3	2,3	k4
GHz	GHz	k1gFnSc2
a	a	k8xC
RAM	RAM	kA
12	#num#	k4
GB	GB	kA
DDR	DDR	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
oproti	oproti	k7c3
původní	původní	k2eAgFnSc3d1
konzoli	konzole	k1gFnSc3
s	s	k7c7
grafikou	grafika	k1gFnSc7
1,31	1,31	k4
teraFLOPS	teraFLOPS	k?
<g/>
,	,	kIx,
CPU	CPU	kA
1,75	1,75	k4
GHz	GHz	k1gFnSc2
a	a	k8xC
RAM	RAM	kA
8	#num#	k4
GB	GB	kA
DDR	DDR	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Xbox	Xbox	k1gInSc1
One	One	k1gFnPc2
Total	totat	k5eAaImAgInS
Lifetime	Lifetim	k1gInSc5
Sales	Sales	k1gMnSc1
Pass	Pass	k1gInSc4
40	#num#	k4
Million	Million	k1gInSc1
Milestone	Mileston	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
ScreenRant	ScreenRant	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KARÁSEK	Karásek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
dostane	dostat	k5eAaPmIp3nS
podporu	podpora	k1gFnSc4
her	hra	k1gFnPc2
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
360	#num#	k4
<g/>
,	,	kIx,
Minecraft	Minecraft	k2eAgMnSc1d1
se	se	k3xPyFc4
díky	díky	k7c3
HoloLens	HoloLens	k1gInSc1
proměňuje	proměňovat	k5eAaImIp3nS
ve	v	k7c4
virtuální	virtuální	k2eAgNnSc4d1
Lego	lego	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Http://smartmania.cz/.	Http://smartmania.cz/.	k1gFnSc1
SmartMania	SmartManium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-06-16	2015-06-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hurá	hurá	k0
<g/>
,	,	kIx,
zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
stále	stále	k6eAd1
existuje	existovat	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
přehraje	přehrát	k5eAaPmIp3nS
hry	hra	k1gFnPc4
z	z	k7c2
Xboxu	Xbox	k1gInSc2
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Xbox	Xbox	k1gInSc1
One	One	k1gFnPc2
prý	prý	k9
prodal	prodat	k5eAaPmAgMnS
polovinu	polovina	k1gFnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k8xS
PS	PS	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrej	hrát	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Xbox	Xbox	k1gInSc1
One	One	k1gMnSc1
Sales	Sales	k1gMnSc1
Reach	Reach	k1gMnSc1
50	#num#	k4
Million	Million	k1gInSc1
In	In	k1gFnSc2
New	New	k1gFnSc2
Turtle	Turtle	k1gFnSc2
Beach	Beach	k1gMnSc1
Earnings	Earningsa	k1gFnPc2
Call	Call	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SegmentNext	SegmentNext	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Xbox	Xbox	k1gInSc1
One	One	k1gMnSc2
S	s	k7c7
Vs	Vs	k1gMnSc7
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
:	:	kIx,
What	What	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
The	The	k1gFnSc7
Difference	Differenec	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes	forbes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Xbox	Xbox	k1gInSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
</s>
<s>
Xbox	Xbox	k1gInSc1
Series	Series	k1gInSc1
X	X	kA
</s>
<s>
Microsoft	Microsoft	kA
</s>
<s>
Herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Xbox	Xbox	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Oficiální	oficiální	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
platformy	platforma	k1gFnSc2
Xbox	Xbox	k1gInSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc2
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
na	na	k7c6
Twitteru	Twitter	k1gInSc6
</s>
<s>
Xboxweb	Xboxwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Fanouškovské	fanouškovský	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
o	o	k7c4
Xbox	Xbox	k1gInSc4
360	#num#	k4
a	a	k8xC
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
</s>
