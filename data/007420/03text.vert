<s>
Judith	Judith	k1gInSc1	Judith
Olivia	Olivium	k1gNnSc2	Olivium
"	"	kIx"	"
<g/>
Judi	Judi	k1gNnSc1	Judi
<g/>
"	"	kIx"	"
Dench	Dench	k1gInSc1	Dench
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Yorkshire	Yorkshir	k1gMnSc5	Yorkshir
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
filmová	filmový	k2eAgFnSc1d1	filmová
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc1d1	divadelní
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
zpočátku	zpočátku	k6eAd1	zpočátku
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
hrála	hrát	k5eAaImAgFnS	hrát
Ofélii	Ofélie	k1gFnSc4	Ofélie
v	v	k7c6	v
Hamletovi	Hamlet	k1gMnSc6	Hamlet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
shakespearovských	shakespearovský	k2eAgFnPc6d1	shakespearovská
rolích	role	k1gFnPc6	role
(	(	kIx(	(
<g/>
Macbeth	Macbetha	k1gFnPc2	Macbetha
-	-	kIx~	-
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Antonius	Antonius	k1gInSc1	Antonius
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
-	-	kIx~	-
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ceněný	ceněný	k2eAgInSc1d1	ceněný
byl	být	k5eAaImAgInS	být
i	i	k9	i
její	její	k3xOp3gInSc1	její
výkon	výkon	k1gInSc1	výkon
ve	v	k7c6	v
Wildeově	Wildeův	k2eAgFnSc6d1	Wildeova
komedii	komedie	k1gFnSc6	komedie
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
mít	mít	k5eAaImF	mít
Filipa	Filip	k1gMnSc4	Filip
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
debutovala	debutovat	k5eAaBmAgFnS	debutovat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
Třetí	třetí	k4xOgInSc4	třetí
tajemství	tajemství	k1gNnPc2	tajemství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
první	první	k4xOgFnSc1	první
role	role	k1gFnSc1	role
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
Laury	Laura	k1gFnSc2	Laura
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
A	a	k9	a
Fine	Fin	k1gMnSc5	Fin
Romance	romance	k1gFnSc1	romance
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
britské	britský	k2eAgFnSc2d1	britská
filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Průlomem	průlom	k1gInSc7	průlom
pro	pro	k7c4	pro
zralou	zralý	k2eAgFnSc4d1	zralá
herečku	herečka	k1gFnSc4	herečka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
historické	historický	k2eAgNnSc1d1	historické
drama	drama	k1gNnSc1	drama
Paní	paní	k1gFnSc1	paní
Brownová	Brownová	k1gFnSc1	Brownová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
postavu	postava	k1gFnSc4	postava
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
které	který	k3yQgInPc4	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
prodala	prodat	k5eAaPmAgFnS	prodat
své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
shakespearovských	shakespearovský	k2eAgFnPc2d1	shakespearovská
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
filmu	film	k1gInSc6	film
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
a	a	k8xC	a
za	za	k7c4	za
roli	role	k1gFnSc4	role
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
Americké	americký	k2eAgFnSc2d1	americká
akademie	akademie	k1gFnSc2	akademie
filmových	filmový	k2eAgNnPc2d1	filmové
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
Poslední	poslední	k2eAgFnSc2d1	poslední
sexbomby	sexbomba	k1gFnSc2	sexbomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
celkem	celkem	k6eAd1	celkem
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
(	(	kIx(	(
<g/>
filmy	film	k1gInPc4	film
Iris	iris	k1gInSc1	iris
/	/	kIx~	/
Příběh	příběh	k1gInSc1	příběh
jedné	jeden	k4xCgFnSc2	jeden
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
Show	show	k1gFnSc1	show
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
Zápisky	zápiska	k1gFnPc1	zápiska
o	o	k7c6	o
skandálu	skandál	k1gInSc6	skandál
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
role	role	k1gFnPc4	role
v	v	k7c6	v
Čokoládě	čokoláda	k1gFnSc6	čokoláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Recenzent	recenzent	k1gMnSc1	recenzent
deníku	deník	k1gInSc2	deník
Právo	práv	k2eAgNnSc1d1	právo
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
výkonu	výkon	k1gInSc6	výkon
v	v	k7c6	v
Zápiscích	zápisek	k1gInPc6	zápisek
o	o	k7c6	o
skandálu	skandál	k1gInSc6	skandál
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Judi	Judi	k1gNnSc1	Judi
Denchová	Denchová	k1gFnSc1	Denchová
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roli	role	k1gFnSc6	role
Barbary	Barbara	k1gFnSc2	Barbara
prostě	prostě	k9	prostě
úžasná	úžasný	k2eAgFnSc1d1	úžasná
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Svým	svůj	k3xOyFgNnSc7	svůj
pojetím	pojetí	k1gNnSc7	pojetí
Barbary	Barbara	k1gFnSc2	Barbara
vždy	vždy	k6eAd1	vždy
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
diváky	divák	k1gMnPc4	divák
alespoň	alespoň	k9	alespoň
ke	k	k7c3	k
kousku	kousek	k1gInSc3	kousek
porozumění	porozumění	k1gNnSc2	porozumění
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
lítosti	lítost	k1gFnPc4	lítost
nad	nad	k7c7	nad
zoufale	zoufale	k6eAd1	zoufale
osamělým	osamělý	k2eAgMnSc7d1	osamělý
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c4	za
herce	herec	k1gMnSc4	herec
Michaela	Michael	k1gMnSc4	Michael
Williamse	Williams	k1gMnSc4	Williams
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
manželství	manželství	k1gNnSc4	manželství
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
Williamsova	Williamsův	k2eAgFnSc1d1	Williamsova
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Finty	finta	k1gFnSc2	finta
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyznamenána	vyznamenat	k5eAaPmNgFnS	vyznamenat
Řádem	řád	k1gInSc7	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
jí	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
Řád	řád	k1gInSc1	řád
společníků	společník	k1gMnPc2	společník
cti	čest	k1gFnSc2	čest
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
známou	známá	k1gFnSc7	známá
také	také	k9	také
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
roli	role	k1gFnSc3	role
M	M	kA	M
ve	v	k7c6	v
filmech	film	k1gInPc6	film
o	o	k7c4	o
Jamesi	Jamese	k1gFnSc4	Jamese
Bondovi	Bonda	k1gMnSc3	Bonda
<g/>
.	.	kIx.	.
</s>
<s>
WINSH-BOLARD	WINSH-BOLARD	k?	WINSH-BOLARD
<g/>
,	,	kIx,	,
Linda	Linda	k1gFnSc1	Linda
<g/>
.	.	kIx.	.
</s>
<s>
Judi	Judi	k6eAd1	Judi
Denchová	Denchový	k2eAgFnSc1d1	Denchová
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Tiscali	Tiscat	k5eAaPmAgMnP	Tiscat
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Judi	Jud	k1gFnSc2	Jud
Denchová	Denchová	k1gFnSc1	Denchová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Judi	Jud	k1gFnSc2	Jud
Denchová	Denchová	k1gFnSc1	Denchová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Judi	Judi	k1gNnSc1	Judi
Dench	Dench	k1gMnSc1	Dench
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Judi	Judi	k1gNnSc2	Judi
Dench	Dencha	k1gFnPc2	Dencha
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Judi	Judi	k1gNnPc2	Judi
Dench	Dench	k1gInSc1	Dench
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
Judi	Judi	k1gNnPc2	Judi
Denchová	Denchová	k1gFnSc1	Denchová
<g/>
:	:	kIx,	:
Řekli	říct	k5eAaPmAgMnP	říct
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
film	film	k1gInSc4	film
nehodím	hodit	k5eNaPmIp1nS	hodit
Jana	Jana	k1gFnSc1	Jana
Benediktová	Benediktový	k2eAgFnSc1d1	Benediktová
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
2.7	[number]	k4	2.7
<g/>
.2011	.2011	k4	.2011
</s>
