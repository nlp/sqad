<s>
Andrés	Andrés	k6eAd1	Andrés
Iniesta	Iniesta	k1gFnSc1	Iniesta
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Fuentealbilla	Fuentealbilla	k1gFnSc1	Fuentealbilla
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
španělský	španělský	k2eAgMnSc1d1	španělský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obléká	oblékat	k5eAaImIp3nS	oblékat
dres	dres	k1gInSc4	dres
týmu	tým	k1gInSc2	tým
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
dal	dát	k5eAaPmAgMnS	dát
Iniesta	Iniesta	k1gMnSc1	Iniesta
jediný	jediný	k2eAgInSc4d1	jediný
gól	gól	k1gInSc4	gól
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
Španělé	Španěl	k1gMnPc1	Španěl
se	se	k3xPyFc4	se
tak	tak	k9	tak
stali	stát	k5eAaPmAgMnP	stát
mistry	mistr	k1gMnPc7	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Iniesta	Iniesta	k1gMnSc1	Iniesta
prošel	projít	k5eAaPmAgMnS	projít
La	la	k1gNnSc1	la
Másiou	Másiý	k2eAgFnSc7d1	Másiý
<g/>
,	,	kIx,	,
barcelonskou	barcelonský	k2eAgFnSc7d1	barcelonská
mládežnickou	mládežnický	k2eAgFnSc7d1	mládežnická
akademií	akademie	k1gFnSc7	akademie
<g/>
,	,	kIx,	,
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
dojem	dojem	k1gInSc4	dojem
už	už	k6eAd1	už
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
odehrál	odehrát	k5eAaPmAgMnS	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
debutový	debutový	k2eAgInSc4d1	debutový
zápas	zápas	k1gInSc4	zápas
za	za	k7c4	za
první	první	k4xOgInSc4	první
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
nastupovat	nastupovat	k5eAaImF	nastupovat
až	až	k9	až
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
mužstva	mužstvo	k1gNnSc2	mužstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všech	všecek	k3xTgFnPc2	všecek
6	[number]	k4	6
trofejí	trofej	k1gFnPc2	trofej
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
Španělsko	Španělsko	k1gNnSc4	Španělsko
U	u	k7c2	u
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
U19	U19	k1gFnSc1	U19
a	a	k8xC	a
U	u	k7c2	u
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
Španělska	Španělsko	k1gNnSc2	Španělsko
přezdívaném	přezdívaný	k2eAgNnSc6d1	přezdívané
La	la	k1gNnSc6	la
Furia	Furius	k1gMnSc2	Furius
Roja	Roj	k1gInSc2	Roj
debutoval	debutovat	k5eAaBmAgInS	debutovat
27	[number]	k4	27
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
v	v	k7c6	v
Albacete	Albace	k1gNnSc2	Albace
v	v	k7c6	v
přátelském	přátelský	k2eAgInSc6d1	přátelský
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
týmu	tým	k1gInSc3	tým
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
na	na	k7c6	na
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehrál	odehrát	k5eAaPmAgMnS	odehrát
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
Saúdské	saúdský	k2eAgFnSc3d1	Saúdská
Arábii	Arábie	k1gFnSc3	Arábie
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomohl	pomoct	k5eAaPmAgMnS	pomoct
Španělsku	Španělsko	k1gNnSc3	Španělsko
při	při	k7c6	při
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
EURO	euro	k1gNnSc4	euro
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
hřiště	hřiště	k1gNnSc2	hřiště
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Španělé	Španěl	k1gMnPc1	Španěl
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Odehrál	odehrát	k5eAaPmAgMnS	odehrát
všechny	všechen	k3xTgInPc4	všechen
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
do	do	k7c2	do
UEFA	UEFA	kA	UEFA
Tým	tým	k1gInSc1	tým
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Iniesta	Iniesta	k1gMnSc1	Iniesta
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
MS	MS	kA	MS
2010	[number]	k4	2010
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
hráčem	hráč	k1gMnSc7	hráč
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
se	se	k3xPyFc4	se
finále	finále	k1gNnSc1	finále
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
,	,	kIx,	,
také	také	k9	také
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
Mužem	muž	k1gMnSc7	muž
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
nominován	nominovat	k5eAaBmNgMnS	nominovat
do	do	k7c2	do
All-Star	All-Stara	k1gFnPc2	All-Stara
Týmu	tým	k1gInSc2	tým
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
Mužem	muž	k1gMnSc7	muž
zápasu	zápas	k1gInSc2	zápas
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
dalších	další	k2eAgInPc6d1	další
zápasech	zápas	k1gInPc6	zápas
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vítězství	vítězství	k1gNnSc3	vítězství
Španělska	Španělsko	k1gNnSc2	Španělsko
na	na	k7c4	na
MS	MS	kA	MS
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
Iniesta	Iniesta	k1gMnSc1	Iniesta
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
"	"	kIx"	"
<g/>
Hráčem	hráč	k1gMnSc7	hráč
turnaje	turnaj	k1gInSc2	turnaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Vicente	Vicent	k1gInSc5	Vicent
del	del	k?	del
Bosque	Bosqu	k1gFnSc2	Bosqu
jej	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Španělé	Španěl	k1gMnPc1	Španěl
jakožto	jakožto	k8xS	jakožto
obhájci	obhájce	k1gMnPc1	obhájce
titulu	titul	k1gInSc2	titul
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
porážkách	porážka	k1gFnPc6	porážka
a	a	k8xC	a
jedné	jeden	k4xCgFnSc3	jeden
výhře	výhra	k1gFnSc3	výhra
již	již	k9	již
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
B.	B.	kA	B.
Vicente	Vicent	k1gInSc5	Vicent
del	del	k?	del
Bosque	Bosqu	k1gFnSc2	Bosqu
jej	on	k3xPp3gMnSc4	on
nominoval	nominovat	k5eAaBmAgMnS	nominovat
i	i	k9	i
na	na	k7c4	na
EURO	euro	k1gNnSc4	euro
2016	[number]	k4	2016
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
Španělé	Španěl	k1gMnPc1	Španěl
vyřazeni	vyřadit	k5eAaPmNgMnP	vyřadit
v	v	k7c4	v
osmifinále	osmifinále	k1gNnSc4	osmifinále
Itálií	Itálie	k1gFnPc2	Itálie
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
z	z	k7c2	z
La	la	k1gNnSc2	la
Masie	Masie	k1gFnSc1	Masie
Francesc	Francesc	k1gFnSc1	Francesc
Fà	Fà	k1gFnSc1	Fà
<g/>
,	,	kIx,	,
začínal	začínat	k5eAaImAgMnS	začínat
původně	původně	k6eAd1	původně
Iniesta	Iniesta	k1gMnSc1	Iniesta
jako	jako	k8xC	jako
defenzivní	defenzivní	k2eAgMnSc1d1	defenzivní
záložník	záložník	k1gMnSc1	záložník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
míče	míč	k1gInSc2	míč
a	a	k8xC	a
hbitost	hbitost	k1gFnSc1	hbitost
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
technikou	technika	k1gFnSc7	technika
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
udělala	udělat	k5eAaPmAgFnS	udělat
záložníka	záložník	k1gMnSc4	záložník
ofenzivního	ofenzivní	k2eAgMnSc4d1	ofenzivní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obrovský	obrovský	k2eAgInSc1d1	obrovský
talent	talent	k1gInSc1	talent
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
v	v	k7c6	v
brzkém	brzký	k2eAgInSc6d1	brzký
věku	věk	k1gInSc6	věk
skauty	skaut	k1gMnPc4	skaut
FC	FC	kA	FC
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
všestrannost	všestrannost	k1gFnSc1	všestrannost
<g/>
,	,	kIx,	,
morálka	morálka	k1gFnSc1	morálka
a	a	k8xC	a
vynalézavost	vynalézavost	k1gFnSc1	vynalézavost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
probojovat	probojovat	k5eAaPmF	probojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
týmu	tým	k1gInSc2	tým
už	už	k6eAd1	už
v	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vicente	Vicent	k1gMnSc5	Vicent
del	del	k?	del
Bosque	Bosqu	k1gFnSc2	Bosqu
ho	on	k3xPp3gMnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kompletního	kompletní	k2eAgMnSc4d1	kompletní
fotbalistu	fotbalista	k1gMnSc4	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
útočit	útočit	k5eAaImF	útočit
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
tvořit	tvořit	k5eAaImF	tvořit
hru	hra	k1gFnSc4	hra
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
skórovat	skórovat	k5eAaBmF	skórovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
Rijkaard	Rijkaard	k1gMnSc1	Rijkaard
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Postavil	postavit	k5eAaPmAgMnS	postavit
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
falešné	falešný	k2eAgNnSc1d1	falešné
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
,	,	kIx,	,
středního	střední	k2eAgMnSc4d1	střední
záložníka	záložník	k1gMnSc4	záložník
<g/>
,	,	kIx,	,
defenzivního	defenzivní	k2eAgMnSc4d1	defenzivní
záložníka	záložník	k1gMnSc4	záložník
či	či	k8xC	či
druhého	druhý	k4xOgMnSc2	druhý
útočníka	útočník	k1gMnSc2	útočník
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
vždycky	vždycky	k6eAd1	vždycky
famózní	famózní	k2eAgMnSc1d1	famózní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
Andrés	Andrés	k1gInSc4	Andrés
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Ortiz	Ortiza	k1gFnPc2	Ortiza
<g/>
,	,	kIx,	,
3.4	[number]	k4	3.4
2011	[number]	k4	2011
se	se	k3xPyFc4	se
jinam	jinam	k6eAd1	jinam
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Valerie	Valerie	k1gFnSc1	Valerie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xS	jako
Cristiano	Cristiana	k1gFnSc5	Cristiana
Ronaldo	Ronaldo	k1gNnSc5	Ronaldo
<g/>
.	.	kIx.	.
</s>
<s>
Aktualizováno	aktualizován	k2eAgNnSc1d1	Aktualizováno
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
</s>
