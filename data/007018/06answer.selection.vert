<s>
Instantní	instantní	k2eAgInSc4d1	instantní
(	(	kIx(	(
<g/>
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
<g/>
)	)	kIx)	)
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
představený	představený	k2eAgInSc4d1	představený
společností	společnost	k1gFnSc7	společnost
Polaroid	polaroid	k1gInSc1	polaroid
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
fotografie	fotografie	k1gFnSc1	fotografie
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
nebo	nebo	k8xC	nebo
minut	minuta	k1gFnPc2	minuta
od	od	k7c2	od
pořízení	pořízení	k1gNnSc2	pořízení
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
fotografického	fotografický	k2eAgInSc2d1	fotografický
přístroje	přístroj	k1gInSc2	přístroj
navrženého	navržený	k2eAgInSc2d1	navržený
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
<g/>
.	.	kIx.	.
</s>
