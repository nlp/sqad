<p>
<s>
Fordham	Fordham	k1gInSc1	Fordham
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Fordhamská	Fordhamský	k2eAgFnSc1d1	Fordhamský
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
soukromá	soukromý	k2eAgFnSc1d1	soukromá
univerzita	univerzita	k1gFnSc1	univerzita
z	z	k7c2	z
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
newyorskou	newyorský	k2eAgFnSc7d1	newyorská
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
diecézí	diecéze	k1gFnSc7	diecéze
jako	jako	k8xC	jako
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
tři	tři	k4xCgInPc4	tři
kampusy	kampus	k1gInPc4	kampus
a	a	k8xC	a
studuje	studovat	k5eAaImIp3nS	studovat
zde	zde	k6eAd1	zde
celkem	celek	k1gInSc7	celek
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnPc1d1	univerzitní
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
kaštanová	kaštanový	k2eAgFnSc1d1	Kaštanová
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
maskotem	maskot	k1gInSc7	maskot
je	být	k5eAaImIp3nS	být
beran	beran	k1gInSc1	beran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
absolventi	absolvent	k1gMnPc1	absolvent
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
některé	některý	k3yIgInPc4	některý
ze	z	k7c2	z
slavných	slavný	k2eAgMnPc2d1	slavný
absolventů	absolvent	k1gMnPc2	absolvent
Fordham	Fordham	k1gInSc1	Fordham
University	universita	k1gFnSc2	universita
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alan	Alan	k1gMnSc1	Alan
Alda	Alda	k1gMnSc1	Alda
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Copperfield	Copperfield	k1gMnSc1	Copperfield
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
DeLillo	DeLillo	k1gNnSc4	DeLillo
</s>
</p>
<p>
<s>
Betty	Betty	k1gFnSc1	Betty
Gilpin	Gilpina	k1gFnPc2	Gilpina
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Sean	Sean	k1gMnSc1	Sean
Leonard	Leonard	k1gMnSc1	Leonard
</s>
</p>
<p>
<s>
Denzel	Denzet	k5eAaImAgInS	Denzet
Washington	Washington	k1gInSc1	Washington
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Fordham	Fordham	k1gInSc1	Fordham
University	universita	k1gFnSc2	universita
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fordham	Fordham	k1gInSc1	Fordham
University	universita	k1gFnSc2	universita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
