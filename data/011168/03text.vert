<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
ا	ا	k?	ا
'	'	kIx"	'
<g/>
Arabī	Arabī	k1gMnSc4	Arabī
as-Su	as-Sa	k1gMnSc4	as-Sa
<g/>
'	'	kIx"	'
<g/>
ū	ū	k?	ū
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Saúdskoarabské	saúdskoarabský	k2eAgNnSc4d1	Saúdskoarabské
království	království	k1gNnSc4	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
2	[number]	k4	2
149	[number]	k4	149
690	[number]	k4	690
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
arabského	arabský	k2eAgInSc2d1	arabský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
severovýchodě	severovýchod	k1gInSc6	severovýchod
sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
a	a	k8xC	a
Irákem	Irák	k1gInSc7	Irák
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
<g/>
,	,	kIx,	,
Katarem	katar	k1gInSc7	katar
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
arabskými	arabský	k2eAgInPc7d1	arabský
emiráty	emirát	k1gInPc7	emirát
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Ománem	Omán	k1gInSc7	Omán
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Perský	perský	k2eAgInSc4d1	perský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
26	[number]	k4	26
680	[number]	k4	680
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
asi	asi	k9	asi
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
jsou	být	k5eAaImIp3nP	být
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
pouštního	pouštní	k2eAgNnSc2d1	pouštní
klimatu	klima	k1gNnSc2	klima
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
97	[number]	k4	97
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
z	z	k7c2	z
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
sunnitský	sunnitský	k2eAgMnSc1d1	sunnitský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
povoleným	povolený	k2eAgNnSc7d1	povolené
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Rijád	Rijáda	k1gFnPc2	Rijáda
a	a	k8xC	a
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
kolonizována	kolonizovat	k5eAaBmNgFnS	kolonizovat
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
saúdského	saúdský	k2eAgInSc2d1	saúdský
státu	stát	k1gInSc2	stát
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
saúdský	saúdský	k2eAgInSc1d1	saúdský
stát	stát	k1gInSc1	stát
měl	mít	k5eAaImAgInS	mít
hranice	hranice	k1gFnPc4	hranice
podobné	podobný	k2eAgFnPc4d1	podobná
současnému	současný	k2eAgInSc3d1	současný
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
saúdský	saúdský	k2eAgInSc1d1	saúdský
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgInSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
království	království	k1gNnSc1	království
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
Abd	Abd	k1gMnSc1	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
ibn	ibn	k?	ibn
Saúd	Saúd	k1gMnSc1	Saúd
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemi	zem	k1gFnSc4	zem
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
Saúdové	Saúdová	k1gFnPc1	Saúdová
mu	on	k3xPp3gNnSc3	on
vládnou	vládnout	k5eAaImIp3nP	vládnout
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
Ibn	Ibn	k1gMnSc1	Ibn
Saúd	Saúd	k1gMnSc1	Saúd
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
a	a	k8xC	a
upevňoval	upevňovat	k5eAaImAgMnS	upevňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
wahhábistického	wahhábistický	k2eAgNnSc2d1	wahhábistický
hnutí	hnutí	k1gNnSc2	hnutí
Ichwán	Ichwána	k1gFnPc2	Ichwána
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
druhé	druhý	k4xOgFnPc4	druhý
největší	veliký	k2eAgFnPc4d3	veliký
zásoby	zásoba	k1gFnPc4	zásoba
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
největším	veliký	k2eAgMnSc7d3	veliký
vývozcem	vývozce	k1gMnSc7	vývozce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tato	tento	k3xDgFnSc1	tento
surovina	surovina	k1gFnSc1	surovina
dominuje	dominovat	k5eAaImIp3nS	dominovat
celé	celý	k2eAgFnSc3d1	celá
ekonomice	ekonomika	k1gFnSc3	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozmachu	rozmach	k1gInSc3	rozmach
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
saúdská	saúdský	k2eAgFnSc1d1	Saúdská
vláda	vláda	k1gFnSc1	vláda
ohromně	ohromně	k6eAd1	ohromně
zbohatla	zbohatnout	k5eAaPmAgFnS	zbohatnout
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
přetvářet	přetvářet	k5eAaImF	přetvářet
zaostalou	zaostalý	k2eAgFnSc4d1	zaostalá
zemi	zem	k1gFnSc4	zem
v	v	k7c4	v
moderní	moderní	k2eAgInSc4d1	moderní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nabytému	nabytý	k2eAgNnSc3d1	nabyté
bohatství	bohatství	k1gNnSc3	bohatství
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
značným	značný	k2eAgMnSc7d1	značný
poskytovatelem	poskytovatel	k1gMnSc7	poskytovatel
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
i	i	k8xC	i
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
je	být	k5eAaImIp3nS	být
islámská	islámský	k2eAgFnSc1d1	islámská
absolutní	absolutní	k2eAgFnSc1d1	absolutní
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
široká	široký	k2eAgFnSc1d1	široká
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejteokratičtějších	teokratický	k2eAgFnPc2d3	teokratický
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
;	;	kIx,	;
ústavu	ústav	k1gInSc2	ústav
představují	představovat	k5eAaImIp3nP	představovat
korán	korán	k1gInSc1	korán
a	a	k8xC	a
sunna	sunna	k1gFnSc1	sunna
a	a	k8xC	a
zdrojem	zdroj	k1gInSc7	zdroj
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
šaría	šarí	k2eAgFnSc1d1	šaría
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
nejsvětější	nejsvětější	k2eAgNnPc1d1	nejsvětější
místa	místo	k1gNnPc1	místo
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
Mekka	Mekka	k1gFnSc1	Mekka
a	a	k8xC	a
Medína	Medína	k1gFnSc1	Medína
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
je	být	k5eAaImIp3nS	být
království	království	k1gNnSc1	království
liberální	liberální	k2eAgNnSc1d1	liberální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
saúdskoarabská	saúdskoarabský	k2eAgFnSc1d1	saúdskoarabská
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přetrvávající	přetrvávající	k2eAgInPc4d1	přetrvávající
problémy	problém	k1gInPc4	problém
patří	patřit	k5eAaImIp3nS	patřit
porušování	porušování	k1gNnSc1	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
diskriminace	diskriminace	k1gFnSc1	diskriminace
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
teroristických	teroristický	k2eAgFnPc2d1	teroristická
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
sama	sám	k3xTgMnSc4	sám
podporuje	podporovat	k5eAaImIp3nS	podporovat
a	a	k8xC	a
destabilizuje	destabilizovat	k5eAaBmIp3nS	destabilizovat
tak	tak	k9	tak
sama	sám	k3xTgFnSc1	sám
Blízký	blízký	k2eAgInSc4d1	blízký
Východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
deníku	deník	k1gInSc2	deník
The	The	k1gMnSc1	The
Independent	independent	k1gMnSc1	independent
jsou	být	k5eAaImIp3nP	být
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
Západ	západ	k1gInSc4	západ
právě	právě	k6eAd1	právě
Saúdové	Saúdové	k2eAgMnSc1d1	Saúdové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
Království	království	k1gNnSc2	království
Hidžázu	Hidžáz	k1gInSc2	Hidžáz
a	a	k8xC	a
Království	království	k1gNnSc4	království
Nadždu	Nadžd	k1gInSc2	Nadžd
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1932	[number]	k4	1932
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
ا	ا	k?	ا
ا	ا	k?	ا
ا	ا	k?	ا
(	(	kIx(	(
<g/>
al-Mamlakah	al-Mamlakah	k1gMnSc1	al-Mamlakah
al-ʻ	al-ʻ	k?	al-ʻ
as-Suʻ	as-Suʻ	k?	as-Suʻ
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Saúdskoarabské	saúdskoarabský	k2eAgNnSc1d1	Saúdskoarabské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Saúdský	saúdský	k2eAgInSc1d1	saúdský
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
rodu	rod	k1gInSc2	rod
Saúdů	Saúd	k1gInPc2	Saúd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
názvu	název	k1gInSc6	název
státu	stát	k1gInSc2	stát
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
zakladatel	zakladatel	k1gMnSc1	zakladatel
Abd	Abd	k1gMnSc1	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
ibn	ibn	k?	ibn
Saúd	Saúd	k1gMnSc1	Saúd
ho	on	k3xPp3gMnSc4	on
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
majetek	majetek	k1gInSc4	majetek
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
"	"	kIx"	"
<g/>
Arbabia	Arbabia	k1gFnSc1	Arbabia
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složenina	složenina	k1gFnSc1	složenina
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Arab	Arab	k1gMnSc1	Arab
<g/>
"	"	kIx"	"
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
přípony	přípona	k1gFnSc2	přípona
"	"	kIx"	"
<g/>
-ia	a	k?	-ia
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Arab	Arab	k1gMnSc1	Arab
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
původně	původně	k6eAd1	původně
odvozené	odvozený	k2eAgInPc1d1	odvozený
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
arabha	arabha	k1gFnSc1	arabha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
poušť	poušť	k1gFnSc1	poušť
<g/>
)	)	kIx)	)
vyskytujícího	vyskytující	k2eAgInSc2d1	vyskytující
se	se	k3xPyFc4	se
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
arab	arab	k1gMnSc1	arab
<g/>
"	"	kIx"	"
označovali	označovat	k5eAaImAgMnP	označovat
sami	sám	k3xTgMnPc1	sám
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
dějiny	dějiny	k1gFnPc1	dějiny
Arábie	Arábie	k1gFnSc1	Arábie
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
islámu	islám	k1gInSc2	islám
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
objevili	objevit	k5eAaPmAgMnP	objevit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
před	před	k7c7	před
100	[number]	k4	100
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
regiony	region	k1gInPc4	region
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názvy	název	k1gInPc1	název
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Nadžd	Nadžda	k1gFnPc2	Nadžda
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
al-Hasa	al-Hasa	k1gFnSc1	al-Hasa
na	na	k7c6	na
východě	východ	k1gInSc6	východ
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Hidžáz	Hidžáz	k1gInSc4	Hidžáz
na	na	k7c6	na
západě	západ	k1gInSc6	západ
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
absenci	absence	k1gFnSc3	absence
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
srážek	srážka	k1gFnPc2	srážka
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
byly	být	k5eAaImAgFnP	být
osidlovány	osidlován	k2eAgFnPc1d1	osidlována
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
podzemními	podzemní	k2eAgInPc7d1	podzemní
zdroji	zdroj	k1gInPc7	zdroj
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oázy	oáza	k1gFnSc2	oáza
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
izolovanost	izolovanost	k1gFnSc1	izolovanost
znemožňovala	znemožňovat	k5eAaImAgFnS	znemožňovat
vytvoření	vytvoření	k1gNnSc3	vytvoření
jednotné	jednotný	k2eAgFnSc2d1	jednotná
politické	politický	k2eAgFnSc2d1	politická
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
vznikaly	vznikat	k5eAaImAgInP	vznikat
spíše	spíše	k9	spíše
kmeny	kmen	k1gInPc1	kmen
spoutané	spoutaný	k2eAgInPc1d1	spoutaný
příbuzenstvím	příbuzenství	k1gNnSc7	příbuzenství
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stáli	stát	k5eAaImAgMnP	stát
šajchové	šajch	k1gMnPc1	šajch
a	a	k8xC	a
emírové	emír	k1gMnPc1	emír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
570	[number]	k4	570
se	se	k3xPyFc4	se
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
narodil	narodit	k5eAaPmAgMnS	narodit
Mohamed	Mohamed	k1gMnSc1	Mohamed
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Kurajšovců	Kurajšovec	k1gMnPc2	Kurajšovec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tuto	tento	k3xDgFnSc4	tento
kupeckou	kupecký	k2eAgFnSc4d1	kupecká
republiku	republika	k1gFnSc4	republika
ovládali	ovládat	k5eAaImAgMnP	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
založil	založit	k5eAaPmAgInS	založit
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
původnímu	původní	k2eAgNnSc3d1	původní
odmítnutí	odmítnutí	k1gNnSc3	odmítnutí
islámu	islám	k1gInSc2	islám
většinou	většina	k1gFnSc7	většina
obyvatel	obyvatel	k1gMnSc1	obyvatel
Mekky	Mekka	k1gFnSc2	Mekka
odešel	odejít	k5eAaPmAgMnS	odejít
Mohamed	Mohamed	k1gMnSc1	Mohamed
roku	rok	k1gInSc2	rok
622	[number]	k4	622
do	do	k7c2	do
Medíny	Medína	k1gFnSc2	Medína
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
muslimského	muslimský	k2eAgInSc2d1	muslimský
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
stoupat	stoupat	k5eAaImF	stoupat
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
víra	víra	k1gFnSc1	víra
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mohamedově	Mohamedův	k2eAgFnSc6d1	Mohamedova
smrti	smrt	k1gFnSc6	smrt
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
chalífát	chalífát	k1gInSc1	chalífát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
prvním	první	k4xOgMnSc7	první
vládcem	vládce	k1gMnSc7	vládce
(	(	kIx(	(
<g/>
chalífou	chalífa	k1gMnSc7	chalífa
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Abú	abú	k1gMnSc1	abú
Bakr	Bakr	k1gMnSc1	Bakr
<g/>
.	.	kIx.	.
</s>
<s>
Medína	Medína	k1gFnSc1	Medína
zůstala	zůstat	k5eAaPmAgFnS	zůstat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
661	[number]	k4	661
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
chalífátu	chalífát	k1gInSc2	chalífát
a	a	k8xC	a
Mekka	Mekka	k1gFnSc1	Mekka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
duchovním	duchovní	k2eAgNnSc7d1	duchovní
centrem	centrum	k1gNnSc7	centrum
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
<g/>
Chalífátu	chalífát	k1gInSc2	chalífát
nejdříve	dříve	k6eAd3	dříve
vládli	vládnout	k5eAaImAgMnP	vládnout
volení	volený	k2eAgMnPc1d1	volený
chalífové	chalífa	k1gMnPc1	chalífa
a	a	k8xC	a
poté	poté	k6eAd1	poté
Umajjovci	Umajjovec	k1gMnPc1	Umajjovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
do	do	k7c2	do
Damašku	Damašek	k1gInSc2	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
750	[number]	k4	750
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
chalífátem	chalífát	k1gInSc7	chalífát
ujali	ujmout	k5eAaPmAgMnP	ujmout
Abbásovci	Abbásovec	k1gMnPc1	Abbásovec
a	a	k8xC	a
přenesli	přenést	k5eAaPmAgMnP	přenést
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
do	do	k7c2	do
Bagdádu	Bagdád	k1gInSc2	Bagdád
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
centrem	centrum	k1gNnSc7	centrum
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mekka	Mekka	k1gFnSc1	Mekka
a	a	k8xC	a
Medína	Medína	k1gFnSc1	Medína
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pouze	pouze	k6eAd1	pouze
duchovními	duchovní	k2eAgNnPc7d1	duchovní
středisky	středisko	k1gNnPc7	středisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
včetně	včetně	k7c2	včetně
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
měst	město	k1gNnPc2	město
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
fátimovského	fátimovský	k2eAgInSc2d1	fátimovský
chalífátu	chalífát	k1gInSc2	chalífát
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Karmaté	Karmat	k1gMnPc1	Karmat
<g/>
,	,	kIx,	,
stoupenci	stoupenec	k1gMnPc1	stoupenec
nábožensko-sociálního	náboženskoociální	k2eAgNnSc2d1	nábožensko-sociální
ismáílitského	ismáílitský	k2eAgNnSc2d1	ismáílitský
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
povstali	povstat	k5eAaPmAgMnP	povstat
proti	proti	k7c3	proti
Abbásovcům	Abbásovec	k1gMnPc3	Abbásovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1171	[number]	k4	1171
Fátimovce	Fátimovec	k1gInSc2	Fátimovec
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
Saladin	Saladin	k2eAgMnSc1d1	Saladin
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Ajjúbovců	Ajjúbovec	k1gMnPc2	Ajjúbovec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
vládce	vládce	k1gMnPc4	vládce
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Mekce	Mekka	k1gFnSc3	Mekka
vládli	vládnout	k5eAaImAgMnP	vládnout
Hášimovci	Hášimovec	k1gMnPc1	Hášimovec
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1269	[number]	k4	1269
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
silný	silný	k2eAgInSc4d1	silný
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
mamlúčtí	mamlúcký	k2eAgMnPc1d1	mamlúcký
sultáni	sultán	k1gMnPc1	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
dobyl	dobýt	k5eAaPmAgInS	dobýt
celý	celý	k2eAgInSc1d1	celý
Hidžáz	Hidžáz	k1gInSc1	Hidžáz
osmanský	osmanský	k2eAgMnSc1d1	osmanský
sultán	sultán	k1gMnSc1	sultán
Selim	Selim	k?	Selim
I.	I.	kA	I.
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
tomuto	tento	k3xDgInSc3	tento
území	území	k1gNnSc6	území
pod	pod	k7c7	pod
osmanským	osmanský	k2eAgInSc7d1	osmanský
dozorem	dozor	k1gInSc7	dozor
panovali	panovat	k5eAaImAgMnP	panovat
Hášimovci	Hášimovec	k1gMnPc1	Hášimovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgMnSc1	první
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
saúdský	saúdský	k2eAgInSc1d1	saúdský
stát	stát	k1gInSc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Nadždu	Nadžd	k1gInSc6	Nadžd
začal	začít	k5eAaPmAgMnS	začít
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
drobnými	drobný	k2eAgMnPc7d1	drobný
rolníky	rolník	k1gMnPc7	rolník
rozmáhat	rozmáhat	k5eAaImF	rozmáhat
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
wahhábismus	wahhábismus	k1gInSc4	wahhábismus
<g/>
,	,	kIx,	,
sunnitská	sunnitský	k2eAgFnSc1d1	sunnitská
fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
forma	forma	k1gFnSc1	forma
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
zakladatel	zakladatel	k1gMnSc1	zakladatel
Muhammad	Muhammad	k1gInSc1	Muhammad
ibn	ibn	k?	ibn
Abd	Abd	k1gMnSc1	Abd
al-Wahháb	al-Wahháb	k1gMnSc1	al-Wahháb
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Mohammadem	Mohammad	k1gInSc7	Mohammad
ibn	ibn	k?	ibn
Saúdem	Saúd	k1gMnSc7	Saúd
v	v	k7c6	v
oáze	oáza	k1gFnSc6	oáza
Daríje	Daríj	k1gFnSc2	Daríj
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
spolu	spolu	k6eAd1	spolu
první	první	k4xOgInSc1	první
saúdský	saúdský	k2eAgInSc1d1	saúdský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
a	a	k8xC	a
přesahoval	přesahovat	k5eAaImAgMnS	přesahovat
i	i	k9	i
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
wahhábisté	wahhábistý	k2eAgFnSc2d1	wahhábistý
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
výbojích	výboj	k1gInPc6	výboj
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
iráckou	irácký	k2eAgFnSc4d1	irácká
Karbalu	Karbal	k1gInSc6	Karbal
<g/>
,	,	kIx,	,
pozabíjeli	pozabíjet	k5eAaPmAgMnP	pozabíjet
šíitské	šíitský	k2eAgNnSc4d1	šíitské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
zničili	zničit	k5eAaPmAgMnP	zničit
hrobku	hrobka	k1gFnSc4	hrobka
šíitského	šíitský	k2eAgMnSc2d1	šíitský
imáma	imám	k1gMnSc2	imám
Husajna	Husajn	k1gMnSc2	Husajn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
wahhábisté	wahhábista	k1gMnPc1	wahhábista
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Mekku	Mekka	k1gFnSc4	Mekka
a	a	k8xC	a
zničili	zničit	k5eAaPmAgMnP	zničit
hroby	hrob	k1gInPc4	hrob
muslimských	muslimský	k2eAgMnPc2d1	muslimský
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
někoho	někdo	k3yInSc4	někdo
či	či	k8xC	či
něčeho	něco	k3yInSc2	něco
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
jediného	jediný	k2eAgMnSc2d1	jediný
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
po	po	k7c6	po
několikaletých	několikaletý	k2eAgFnPc6d1	několikaletá
snahách	snaha	k1gFnPc6	snaha
egyptského	egyptský	k2eAgMnSc2d1	egyptský
paši	paša	k1gMnSc2	paša
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
<g/>
.	.	kIx.	.
<g/>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
státu	stát	k1gInSc2	stát
učinil	učinit	k5eAaImAgInS	učinit
emír	emír	k1gMnSc1	emír
Turní	Turní	k1gMnSc1	Turní
ibn	ibn	k?	ibn
Abdulláh	Abdulláh	k1gMnSc1	Abdulláh
ibn	ibn	k?	ibn
Muhammad	Muhammad	k1gInSc1	Muhammad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
Rijád	Rijáda	k1gFnPc2	Rijáda
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
Egypťany	Egypťan	k1gMnPc7	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
druhý	druhý	k4xOgInSc1	druhý
saúdský	saúdský	k2eAgInSc1d1	saúdský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
prvnímu	první	k4xOgNnSc3	první
menší	malý	k2eAgFnPc4d2	menší
–	–	k?	–
Saúdové	Saúdová	k1gFnPc4	Saúdová
tentokrát	tentokrát	k6eAd1	tentokrát
vládli	vládnout	k5eAaImAgMnP	vládnout
jen	jen	k9	jen
Nadždu	Nadžda	k1gFnSc4	Nadžda
a	a	k8xC	a
al-Hase	al-Hase	k6eAd1	al-Hase
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
Saúdy	Saúd	k1gInPc7	Saúd
a	a	k8xC	a
Rašídy	Rašído	k1gNnPc7	Rašído
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
říše	říše	k1gFnSc1	říše
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
oáze	oáza	k1gFnSc6	oáza
Hájl	Hájla	k1gFnPc2	Hájla
na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
Saúdy	Saúd	k1gInPc4	Saúd
z	z	k7c2	z
Nadždu	Nadžd	k1gInSc2	Nadžd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
druhý	druhý	k4xOgInSc1	druhý
saúdský	saúdský	k2eAgInSc1d1	saúdský
stát	stát	k1gInSc1	stát
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
Saúdové	Saúda	k1gMnPc1	Saúda
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
donuceni	donutit	k5eAaPmNgMnP	donutit
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
první	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
chtělo	chtít	k5eAaImAgNnS	chtít
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
zajistit	zajistit	k5eAaPmF	zajistit
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
podpořili	podpořit	k5eAaPmAgMnP	podpořit
saúdského	saúdský	k2eAgMnSc4d1	saúdský
emíra	emír	k1gMnSc4	emír
Abd	Abd	k1gMnSc4	Abd
al-Azíze	al-Azíze	k6eAd1	al-Azíze
ibn	ibn	k?	ibn
Saúda	Saúda	k1gMnSc1	Saúda
v	v	k7c6	v
převratu	převrat	k1gInSc6	převrat
proti	proti	k7c3	proti
proosmanským	proosmanský	k2eAgInPc3d1	proosmanský
Rašídům	Rašíd	k1gInPc3	Rašíd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1902	[number]	k4	1902
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Rijádu	Rijád	k1gInSc2	Rijád
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
celý	celý	k2eAgInSc4d1	celý
Nadžd	Nadžd	k1gInSc4	Nadžd
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgMnS	získávat
vliv	vliv	k1gInSc4	vliv
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgInPc7d1	hlavní
kmeny	kmen	k1gInPc7	kmen
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
primitivní	primitivní	k2eAgFnSc4d1	primitivní
státní	státní	k2eAgFnSc4d1	státní
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
budovat	budovat	k5eAaImF	budovat
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nábožensko-vojenské	náboženskoojenský	k2eAgNnSc1d1	nábožensko-vojenský
(	(	kIx(	(
<g/>
wahhábistické	wahhábistický	k2eAgNnSc1d1	wahhábistický
<g/>
)	)	kIx)	)
hnutí	hnutí	k1gNnSc1	hnutí
Ichwán	Ichwána	k1gFnPc2	Ichwána
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bratři	bratr	k1gMnPc1	bratr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
zakládali	zakládat	k5eAaImAgMnP	zakládat
osady	osada	k1gFnPc4	osada
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgMnPc2	který
usazovali	usazovat	k5eAaImAgMnP	usazovat
beduíny	beduín	k1gMnPc4	beduín
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Abd	Abd	k1gMnSc3	Abd
al-Azízovi	al-Azíz	k1gMnSc3	al-Azíz
pomocí	pomocí	k7c2	pomocí
Ichwánu	Ichwán	k1gInSc2	Ichwán
podařilo	podařit	k5eAaPmAgNnS	podařit
rozšířit	rozšířit	k5eAaPmF	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Wahhábité	Wahhábitý	k2eAgNnSc4d1	Wahhábitý
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1924	[number]	k4	1924
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
zdevastovali	zdevastovat	k5eAaPmAgMnP	zdevastovat
Táif	Táif	k1gInSc4	Táif
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
obléhání	obléhání	k1gNnSc2	obléhání
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
dobytí	dobytí	k1gNnSc6	dobytí
Medíny	Medína	k1gFnSc2	Medína
se	se	k3xPyFc4	se
Abd	Abd	k1gMnSc1	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
králem	král	k1gMnSc7	král
Hidžázu	Hidžáz	k1gInSc2	Hidžáz
<g/>
,	,	kIx,	,
Nadždu	Nadžd	k1gInSc2	Nadžd
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
Britové	Brit	k1gMnPc1	Brit
uznali	uznat	k5eAaPmAgMnP	uznat
jeho	jeho	k3xOp3gNnSc4	jeho
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1932	[number]	k4	1932
oficiálně	oficiálně	k6eAd1	oficiálně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
s	s	k7c7	s
názvem	název	k1gInSc7	název
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
.	.	kIx.	.
<g/>
Abd	Abd	k1gMnSc1	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
považoval	považovat	k5eAaImAgMnS	považovat
stát	stát	k5eAaImF	stát
za	za	k7c4	za
soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Upevňoval	upevňovat	k5eAaImAgMnS	upevňovat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
sférách	sféra	k1gFnPc6	sféra
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
proměňovala	proměňovat	k5eAaImAgFnS	proměňovat
v	v	k7c6	v
despocii	despocie	k1gFnSc6	despocie
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
moc	moc	k1gFnSc4	moc
mu	on	k3xPp3gMnSc3	on
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
britské	britský	k2eAgInPc4d1	britský
příspěvky	příspěvek	k1gInPc4	příspěvek
<g/>
,	,	kIx,	,
muslimské	muslimský	k2eAgFnPc4d1	muslimská
poutě	pouť	k1gFnPc4	pouť
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
vliv	vliv	k1gInSc1	vliv
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
převýšil	převýšit	k5eAaPmAgInS	převýšit
ten	ten	k3xDgInSc1	ten
britský	britský	k2eAgInSc1d1	britský
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
navázalo	navázat	k5eAaPmAgNnS	navázat
USA	USA	kA	USA
se	s	k7c7	s
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
styky	styk	k1gInPc1	styk
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
do	do	k7c2	do
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
půjčce	půjčka	k1gFnSc6	půjčka
a	a	k8xC	a
pronájmu	pronájem	k1gInSc6	pronájem
a	a	k8xC	a
po	po	k7c6	po
jaltské	jaltský	k2eAgFnSc6d1	Jaltská
konferenci	konference	k1gFnSc6	konference
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
několik	několik	k4yIc1	několik
dohod	dohoda	k1gFnPc2	dohoda
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Američané	Američan	k1gMnPc1	Američan
např.	např.	kA	např.
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stavět	stavět	k5eAaImF	stavět
vojenské	vojenský	k2eAgFnPc4d1	vojenská
základny	základna	k1gFnPc4	základna
a	a	k8xC	a
Saúdové	Saúda	k1gMnPc1	Saúda
získali	získat	k5eAaPmAgMnP	získat
dodávky	dodávka	k1gFnPc4	dodávka
amerických	americký	k2eAgFnPc2d1	americká
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
Abd	Abd	k1gFnSc2	Abd
al-Azízovy	al-Azízův	k2eAgFnSc2d1	al-Azízův
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
první	první	k4xOgMnPc4	první
následníci	následník	k1gMnPc1	následník
===	===	k?	===
</s>
</p>
<p>
<s>
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
rapidním	rapidní	k2eAgInSc7d1	rapidní
rozvojem	rozvoj	k1gInSc7	rozvoj
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
příjmy	příjem	k1gInPc1	příjem
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
navyšovaly	navyšovat	k5eAaImAgFnP	navyšovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
socioekonomická	socioekonomický	k2eAgFnSc1d1	socioekonomická
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nevyvíjela	vyvíjet	k5eNaImAgFnS	vyvíjet
a	a	k8xC	a
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejzaostalejších	zaostalý	k2eAgFnPc2d3	nejzaostalejší
zemí	zem	k1gFnPc2	zem
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
katastrofální	katastrofální	k2eAgFnSc6d1	katastrofální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zaviněno	zaviněn	k2eAgNnSc1d1	zaviněno
nepříznivými	příznivý	k2eNgFnPc7d1	nepříznivá
přírodními	přírodní	k2eAgFnPc7d1	přírodní
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
běžné	běžný	k2eAgNnSc4d1	běžné
otrokářství	otrokářství	k1gNnSc4	otrokářství
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bylo	být	k5eAaImAgNnS	být
otrokářství	otrokářství	k1gNnSc1	otrokářství
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
otrockých	otrocký	k2eAgFnPc6d1	otrocká
podmínkách	podmínka	k1gFnPc6	podmínka
odhadem	odhad	k1gInSc7	odhad
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
podnikání	podnikání	k1gNnSc1	podnikání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
Aramco	Aramco	k1gNnSc1	Aramco
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
vláda	vláda	k1gFnSc1	vláda
dostávala	dostávat	k5eAaImAgFnS	dostávat
zpočátku	zpočátku	k6eAd1	zpočátku
od	od	k7c2	od
amerických	americký	k2eAgFnPc2d1	americká
ropných	ropný	k2eAgFnPc2d1	ropná
společností	společnost	k1gFnPc2	společnost
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgInPc1d1	malý
poplatky	poplatek	k1gInPc1	poplatek
za	za	k7c4	za
vytěžené	vytěžený	k2eAgInPc4d1	vytěžený
barely	barel	k1gInPc4	barel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
zisky	zisk	k1gInPc1	zisk
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
50	[number]	k4	50
na	na	k7c4	na
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abd	Abd	k?	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
34	[number]	k4	34
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
následovníků	následovník	k1gMnPc2	následovník
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
Saúd	Saúd	k1gMnSc1	Saúd
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
mrhání	mrhání	k1gNnSc1	mrhání
státními	státní	k2eAgInPc7d1	státní
penězi	peníze	k1gInPc7	peníze
<g/>
;	;	kIx,	;
až	až	k9	až
polovinu	polovina	k1gFnSc4	polovina
státních	státní	k2eAgInPc2d1	státní
příjmů	příjem	k1gInPc2	příjem
utrácela	utrácet	k5eAaImAgFnS	utrácet
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
islámu	islám	k1gInSc2	islám
se	se	k3xPyFc4	se
za	za	k7c2	za
Saúdovy	Saúdův	k2eAgFnSc2d1	Saúdův
vlády	vláda	k1gFnSc2	vláda
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
teokracie	teokracie	k1gFnSc2	teokracie
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
pramenem	pramen	k1gInSc7	pramen
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
šaría	šaría	k1gFnSc1	šaría
a	a	k8xC	a
islám	islám	k1gInSc1	islám
začal	začít	k5eAaPmAgInS	začít
výrazně	výrazně	k6eAd1	výrazně
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
finanční	finanční	k2eAgFnSc1d1	finanční
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgNnP	být
věrná	věrný	k2eAgFnSc1d1	věrná
Saúdovi	Saúda	k1gMnSc3	Saúda
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
chtěla	chtít	k5eAaImAgFnS	chtít
nastolit	nastolit	k5eAaPmF	nastolit
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
třetí	třetí	k4xOgMnSc1	třetí
stál	stát	k5eAaImAgMnS	stát
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
princ	princ	k1gMnSc1	princ
Fajsal	Fajsal	k1gMnSc1	Fajsal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
svých	svůj	k3xOyFgMnPc2	svůj
bratrů	bratr	k1gMnPc2	bratr
předal	předat	k5eAaPmAgMnS	předat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
král	král	k1gMnSc1	král
Saúd	Saúd	k1gMnSc1	Saúd
Fajsalovi	Fajsal	k1gMnSc3	Fajsal
široké	široký	k2eAgFnSc2d1	široká
pravomoci	pravomoc	k1gFnSc2	pravomoc
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
Fajsal	Fajsal	k1gMnSc1	Fajsal
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgMnS	zavést
několik	několik	k4yIc4	několik
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
předchůdcích	předchůdce	k1gMnPc6	předchůdce
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
absolutistické	absolutistický	k2eAgFnSc6d1	absolutistická
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
útisku	útisk	k1gInSc6	útisk
politických	politický	k2eAgMnPc2d1	politický
soupeřů	soupeř	k1gMnPc2	soupeř
a	a	k8xC	a
vměšování	vměšování	k1gNnSc1	vměšování
se	se	k3xPyFc4	se
do	do	k7c2	do
soukromého	soukromý	k2eAgInSc2d1	soukromý
života	život	k1gInSc2	život
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
začala	začít	k5eAaPmAgFnS	začít
výrazně	výrazně	k6eAd1	výrazně
projevovat	projevovat	k5eAaImF	projevovat
modernizace	modernizace	k1gFnSc1	modernizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
převážně	převážně	k6eAd1	převážně
modernizace	modernizace	k1gFnSc1	modernizace
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
stát	stát	k1gInSc1	stát
investoval	investovat	k5eAaBmAgInS	investovat
velké	velký	k2eAgFnPc4d1	velká
částky	částka	k1gFnPc4	částka
do	do	k7c2	do
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Modernizace	modernizace	k1gFnSc1	modernizace
byla	být	k5eAaImAgFnS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
prudkým	prudký	k2eAgInSc7d1	prudký
růstem	růst	k1gInSc7	růst
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
<g/>
Ropný	ropný	k2eAgInSc4d1	ropný
boom	boom	k1gInSc4	boom
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
zajistil	zajistit	k5eAaPmAgInS	zajistit
zemi	zem	k1gFnSc3	zem
silnou	silný	k2eAgFnSc4d1	silná
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
i	i	k8xC	i
politickou	politický	k2eAgFnSc4d1	politická
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
i	i	k8xC	i
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
znárodněna	znárodněn	k2eAgFnSc1d1	znárodněna
společnost	společnost	k1gFnSc1	společnost
Saudi	Saud	k1gMnPc1	Saud
Aramco	Aramco	k1gMnSc1	Aramco
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
plánuje	plánovat	k5eAaImIp3nS	plánovat
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
pětiletky	pětiletka	k1gFnSc2	pětiletka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
za	za	k7c4	za
vlády	vláda	k1gFnPc4	vláda
krále	král	k1gMnSc2	král
Fajsala	Fajsala	k1gMnSc2	Fajsala
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
pomalý	pomalý	k2eAgInSc4d1	pomalý
přechod	přechod	k1gInSc4	přechod
ke	k	k7c3	k
kapitalismu	kapitalismus	k1gInSc3	kapitalismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
druhá	druhý	k4xOgFnSc1	druhý
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
mnohem	mnohem	k6eAd1	mnohem
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
modernizace	modernizace	k1gFnSc1	modernizace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
výdaje	výdaj	k1gInPc1	výdaj
byly	být	k5eAaImAgInP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
nové	nový	k2eAgFnPc1d1	nová
společnosti	společnost	k1gFnPc1	společnost
převážně	převážně	k6eAd1	převážně
smíšeného	smíšený	k2eAgInSc2d1	smíšený
charakteru	charakter	k1gInSc2	charakter
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
patřila	patřit	k5eAaImAgFnS	patřit
státu	stát	k1gInSc3	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
se	se	k3xPyFc4	se
přesouval	přesouvat	k5eAaImAgInS	přesouvat
do	do	k7c2	do
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
státních	státní	k2eAgInPc2d1	státní
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
ze	z	k7c2	z
73	[number]	k4	73
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
na	na	k7c4	na
48	[number]	k4	48
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
<g/>
Obrovské	obrovský	k2eAgInPc1d1	obrovský
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
financovaly	financovat	k5eAaBmAgInP	financovat
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
mohutné	mohutný	k2eAgNnSc4d1	mohutné
zbrojení	zbrojení	k1gNnSc4	zbrojení
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgNnSc4	který
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
utratila	utratit	k5eAaPmAgFnS	utratit
150	[number]	k4	150
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
%	%	kIx~	%
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
peněz	peníze	k1gInPc2	peníze
šla	jít	k5eAaImAgFnS	jít
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
údržbu	údržba	k1gFnSc4	údržba
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prováděli	provádět	k5eAaImAgMnP	provádět
většinou	většinou	k6eAd1	většinou
Američané	Američan	k1gMnPc1	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
zbrojení	zbrojení	k1gNnSc4	zbrojení
využívala	využívat	k5eAaImAgFnS	využívat
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
nabytý	nabytý	k2eAgInSc4d1	nabytý
majetek	majetek	k1gInSc4	majetek
k	k	k7c3	k
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
politiky	politika	k1gFnSc2	politika
států	stát	k1gInPc2	stát
převážně	převážně	k6eAd1	převážně
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
financovala	financovat	k5eAaBmAgFnS	financovat
zpátečnické	zpátečnický	k2eAgFnPc4d1	zpátečnická
vlády	vláda	k1gFnPc4	vláda
a	a	k8xC	a
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
proti	proti	k7c3	proti
demokratizačním	demokratizační	k2eAgFnPc3d1	demokratizační
tendencím	tendence	k1gFnPc3	tendence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Fajsal	Fajsal	k1gMnSc1	Fajsal
byl	být	k5eAaImAgMnS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1975	[number]	k4	1975
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
svým	svůj	k3xOyFgMnSc7	svůj
synovcem	synovec	k1gMnSc7	synovec
Fajsalem	Fajsal	k1gMnSc7	Fajsal
ibn	ibn	k?	ibn
Musájdem	Musájd	k1gMnSc7	Musájd
a	a	k8xC	a
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
král	král	k1gMnSc1	král
Chálid	Chálida	k1gFnPc2	Chálida
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
určitým	určitý	k2eAgNnSc7d1	určité
uvolněním	uvolnění	k1gNnSc7	uvolnění
napětí	napětí	k1gNnSc2	napětí
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
propustil	propustit	k5eAaPmAgInS	propustit
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
a	a	k8xC	a
přerozdělením	přerozdělení	k1gNnSc7	přerozdělení
části	část	k1gFnSc2	část
státních	státní	k2eAgInPc2d1	státní
peněz	peníze	k1gInPc2	peníze
chudým	chudý	k1gMnSc7	chudý
potlačil	potlačit	k5eAaPmAgMnS	potlačit
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
zdroje	zdroj	k1gInPc4	zdroj
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chálida	Chálida	k1gFnSc1	Chálida
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
nahradil	nahradit	k5eAaPmAgMnS	nahradit
král	král	k1gMnSc1	král
Fahd	Fahd	k1gMnSc1	Fahd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
výrazně	výrazně	k6eAd1	výrazně
projevovat	projevovat	k5eAaImF	projevovat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
praktikami	praktika	k1gFnPc7	praktika
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
vznikem	vznik	k1gInSc7	vznik
radikálních	radikální	k2eAgNnPc2d1	radikální
náboženských	náboženský	k2eAgNnPc2d1	náboženské
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
příčinami	příčina	k1gFnPc7	příčina
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
byly	být	k5eAaImAgFnP	být
např.	např.	kA	např.
diskriminace	diskriminace	k1gFnSc1	diskriminace
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itů	itů	k?	itů
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
provincii	provincie	k1gFnSc6	provincie
<g/>
)	)	kIx)	)
a	a	k8xC	a
odstupování	odstupování	k1gNnSc1	odstupování
od	od	k7c2	od
tradičních	tradiční	k2eAgFnPc2d1	tradiční
islámských	islámský	k2eAgFnPc2d1	islámská
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedly	vést	k5eAaImAgInP	vést
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
proti	proti	k7c3	proti
Iráku	Irák	k1gInSc3	Irák
<g/>
,	,	kIx,	,
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
ve	v	k7c6	v
zvýšení	zvýšení	k1gNnSc6	zvýšení
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
některých	některý	k3yIgNnPc2	některý
islámských	islámský	k2eAgNnPc2d1	islámské
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
spojenectví	spojenectví	k1gNnSc1	spojenectví
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
pronikání	pronikání	k1gNnSc4	pronikání
západních	západní	k2eAgFnPc2d1	západní
myšlenek	myšlenka	k1gFnPc2	myšlenka
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
islámských	islámský	k2eAgNnPc2d1	islámské
hnutí	hnutí	k1gNnSc2	hnutí
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
teroristické	teroristický	k2eAgFnPc4d1	teroristická
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgInSc1d3	nejznámější
teroristickou	teroristický	k2eAgFnSc7d1	teroristická
organizací	organizace	k1gFnSc7	organizace
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
provádět	provádět	k5eAaImF	provádět
sebevražedné	sebevražedný	k2eAgInPc4d1	sebevražedný
atentáty	atentát	k1gInPc4	atentát
a	a	k8xC	a
bombové	bombový	k2eAgInPc4d1	bombový
útoky	útok	k1gInPc4	útok
a	a	k8xC	a
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
svou	svůj	k3xOyFgFnSc4	svůj
působnost	působnost	k1gFnSc1	působnost
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
mimo	mimo	k7c4	mimo
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
.	.	kIx.	.
<g/>
Král	Král	k1gMnSc1	Král
Fahd	Fahd	k1gMnSc1	Fahd
zemřel	zemřít	k5eAaPmAgMnS	zemřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
a	a	k8xC	a
saúdskoarabským	saúdskoarabský	k2eAgMnSc7d1	saúdskoarabský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
stal	stát	k5eAaPmAgMnS	stát
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
ibn	ibn	k?	ibn
Abd	Abd	k1gMnSc1	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnSc4	jeho
snahy	snaha	k1gFnPc1	snaha
patřily	patřit	k5eAaImAgFnP	patřit
např.	např.	kA	např.
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
určitá	určitý	k2eAgFnSc1d1	určitá
demokratizace	demokratizace	k1gFnSc1	demokratizace
politického	politický	k2eAgInSc2d1	politický
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
částečná	částečný	k2eAgFnSc1d1	částečná
emancipace	emancipace	k1gFnSc1	emancipace
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Abdalláha	Abdalláh	k1gMnSc2	Abdalláh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
král	král	k1gMnSc1	král
Salmán	Salmán	k2eAgInSc1d1	Salmán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přetrvávající	přetrvávající	k2eAgFnPc4d1	přetrvávající
potíže	potíž	k1gFnPc4	potíž
v	v	k7c6	v
království	království	k1gNnSc6	království
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
nedodržování	nedodržování	k1gNnSc2	nedodržování
základních	základní	k2eAgNnPc2d1	základní
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
teroristických	teroristický	k2eAgFnPc2d1	teroristická
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
událostmi	událost	k1gFnPc7	událost
Arabského	arabský	k2eAgInSc2d1	arabský
jara	jaro	k1gNnSc2	jaro
odehrály	odehrát	k5eAaPmAgInP	odehrát
místní	místní	k2eAgInPc1d1	místní
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
provinciích	provincie	k1gFnPc6	provincie
přiléhajících	přiléhající	k2eAgFnPc6d1	přiléhající
k	k	k7c3	k
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
soustředěna	soustředěn	k2eAgNnPc4d1	soustředěno
centra	centrum	k1gNnPc4	centrum
těžebního	těžební	k2eAgInSc2d1	těžební
a	a	k8xC	a
zpracovatelského	zpracovatelský	k2eAgInSc2d1	zpracovatelský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
protesty	protest	k1gInPc1	protest
však	však	k9	však
byly	být	k5eAaImAgInP	být
nesmělé	smělý	k2eNgInPc1d1	nesmělý
a	a	k8xC	a
nepočetné	početný	k2eNgInPc1d1	nepočetný
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgFnSc1d1	úřední
moc	moc	k1gFnSc1	moc
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
reagovala	reagovat	k5eAaBmAgFnS	reagovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
monarchistické	monarchistický	k2eAgInPc1d1	monarchistický
režimy	režim	k1gInPc1	režim
v	v	k7c6	v
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
nebo	nebo	k8xC	nebo
Maroku	Maroko	k1gNnSc6	Maroko
–	–	k?	–
oživením	oživení	k1gNnSc7	oživení
řady	řada	k1gFnSc2	řada
sociálních	sociální	k2eAgInPc2d1	sociální
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
vložením	vložení	k1gNnSc7	vložení
dalších	další	k2eAgFnPc2d1	další
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
bytové	bytový	k2eAgFnSc2d1	bytová
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
,	,	kIx,	,
podpory	podpora	k1gFnSc2	podpora
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
rovněž	rovněž	k9	rovněž
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
důchodů	důchod	k1gInPc2	důchod
<g/>
,	,	kIx,	,
platů	plat	k1gInPc2	plat
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
,	,	kIx,	,
policii	policie	k1gFnSc3	policie
a	a	k8xC	a
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
tedy	tedy	k8xC	tedy
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
téměř	téměř	k6eAd1	téměř
nezasáhlo	zasáhnout	k5eNaPmAgNnS	zasáhnout
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
nepokoje	nepokoj	k1gInSc2	nepokoj
potlačila	potlačit	k5eAaPmAgFnS	potlačit
v	v	k7c6	v
zárodku	zárodek	k1gInSc6	zárodek
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
ideologicky	ideologicky	k6eAd1	ideologicky
i	i	k9	i
finančně	finančně	k6eAd1	finančně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
provedly	provést	k5eAaPmAgFnP	provést
saúdskoarabské	saúdskoarabský	k2eAgFnPc1d1	saúdskoarabská
jednotky	jednotka	k1gFnPc1	jednotka
přímou	přímý	k2eAgFnSc4d1	přímá
intervenci	intervence	k1gFnSc4	intervence
do	do	k7c2	do
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
sunnitského	sunnitský	k2eAgInSc2d1	sunnitský
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
rodu	rod	k1gInSc2	rod
Ál	Ál	k1gMnSc1	Ál
Chalífa	chalífa	k1gMnSc1	chalífa
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
projížděly	projíždět	k5eAaImAgInP	projíždět
Bahrajnem	Bahrajn	k1gMnSc7	Bahrajn
saúdskoarabské	saúdskoarabský	k2eAgInPc4d1	saúdskoarabský
obrněné	obrněný	k2eAgInPc4d1	obrněný
tanky	tank	k1gInPc4	tank
pod	pod	k7c7	pod
hesly	heslo	k1gNnPc7	heslo
pomoci	pomoc	k1gFnSc2	pomoc
bratrskému	bratrský	k2eAgInSc3d1	bratrský
státu	stát	k1gInSc3	stát
proti	proti	k7c3	proti
podvratným	podvratný	k2eAgFnPc3d1	podvratná
silám	síla	k1gFnPc3	síla
<g/>
,	,	kIx,	,
inspirovaným	inspirovaný	k2eAgInSc7d1	inspirovaný
Íránem	Írán	k1gInSc7	Írán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tak	tak	k6eAd1	tak
malém	malý	k2eAgInSc6d1	malý
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
přehledném	přehledný	k2eAgInSc6d1	přehledný
státu	stát	k1gInSc6	stát
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Bahrajn	Bahrajn	k1gInSc1	Bahrajn
se	s	k7c7	s
s	s	k7c7	s
"	"	kIx"	"
<g/>
bratrskou	bratrský	k2eAgFnSc7d1	bratrská
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
"	"	kIx"	"
několika	několik	k4yIc2	několik
tankových	tankový	k2eAgMnPc2d1	tankový
a	a	k8xC	a
motostřeleckých	motostřelecký	k2eAgInPc2d1	motostřelecký
praporů	prapor	k1gInPc2	prapor
podařilo	podařit	k5eAaPmAgNnS	podařit
situaci	situace	k1gFnSc4	situace
zvládnout	zvládnout	k5eAaPmF	zvládnout
za	za	k7c4	za
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
a	a	k8xC	a
aktivisty	aktivista	k1gMnPc4	aktivista
šíitské	šíitský	k2eAgFnSc2d1	šíitská
opozice	opozice	k1gFnSc2	opozice
fyzicky	fyzicky	k6eAd1	fyzicky
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
"	"	kIx"	"
<g/>
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
ji	on	k3xPp3gFnSc4	on
sledovali	sledovat	k5eAaImAgMnP	sledovat
i	i	k9	i
západní	západní	k2eAgMnPc1d1	západní
spojenci	spojenec	k1gMnPc1	spojenec
Rijádu	Rijád	k1gInSc2	Rijád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohem	mnohem	k6eAd1	mnohem
masívněji	masívně	k6eAd2	masívně
se	se	k3xPyFc4	se
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
vyhrocení	vyhrocení	k1gNnSc6	vyhrocení
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
nenásilným	násilný	k2eNgInPc3d1	nenásilný
protestům	protest	k1gInPc3	protest
připojily	připojit	k5eAaPmAgFnP	připojit
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
jednotky	jednotka	k1gFnPc4	jednotka
šítského	šítský	k2eAgNnSc2d1	šítský
hnutí	hnutí	k1gNnSc2	hnutí
Al-Húthí	Al-Húthí	k1gFnSc2	Al-Húthí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
bojovníci	bojovník	k1gMnPc1	bojovník
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zajdovských	zajdovský	k2eAgInPc2d1	zajdovský
kmenových	kmenový	k2eAgInPc2d1	kmenový
svazů	svaz	k1gInPc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Jemenem	Jemen	k1gInSc7	Jemen
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
početný	početný	k2eAgInSc1d1	početný
saúdskoarabský	saúdskoarabský	k2eAgInSc1d1	saúdskoarabský
kontingent	kontingent	k1gInSc1	kontingent
a	a	k8xC	a
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
šíitským	šíitský	k2eAgNnSc7d1	šíitské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
byly	být	k5eAaImAgFnP	být
ostřelovány	ostřelován	k2eAgFnPc1d1	ostřelována
z	z	k7c2	z
houfnic	houfnice	k1gFnPc2	houfnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
saúdskoarabské	saúdskoarabský	k2eAgNnSc4d1	Saúdskoarabské
letectvo	letectvo	k1gNnSc4	letectvo
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
bombardovalo	bombardovat	k5eAaImAgNnS	bombardovat
mnohé	mnohé	k1gNnSc1	mnohé
lokality	lokalita	k1gFnSc2	lokalita
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
–	–	k?	–
často	často	k6eAd1	často
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
vojenský	vojenský	k2eAgInSc4d1	vojenský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nejednou	jednou	k6eNd1	jednou
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
uprchlické	uprchlický	k2eAgInPc4d1	uprchlický
tábory	tábor	k1gInPc4	tábor
nebo	nebo	k8xC	nebo
také	také	k9	také
nemocnice	nemocnice	k1gFnSc1	nemocnice
organizace	organizace	k1gFnSc2	organizace
Lékaři	lékař	k1gMnPc1	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
definitivně	definitivně	k6eAd1	definitivně
vymkla	vymknout	k5eAaPmAgFnS	vymknout
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
kontrole	kontrola	k1gFnSc3	kontrola
a	a	k8xC	a
změnila	změnit	k5eAaPmAgFnS	změnit
se	se	k3xPyFc4	se
v	v	k7c4	v
nepřehlednou	přehledný	k2eNgFnSc4d1	nepřehledná
kmenovou	kmenový	k2eAgFnSc4d1	kmenová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c2	za
značného	značný	k2eAgInSc2d1	značný
nezájmu	nezájem	k1gInSc2	nezájem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
s	s	k7c7	s
logistickou	logistický	k2eAgFnSc7d1	logistická
podporou	podpora	k1gFnSc7	podpora
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
přímou	přímý	k2eAgFnSc4d1	přímá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
proti	proti	k7c3	proti
jemenským	jemenský	k2eAgMnPc3d1	jemenský
šíitům	šíita	k1gMnPc3	šíita
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
vyčerpává	vyčerpávat	k5eAaImIp3nS	vyčerpávat
země	země	k1gFnSc1	země
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
bojuje	bojovat	k5eAaImIp3nS	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
oficiální	oficiální	k2eAgFnSc2d1	oficiální
vlády	vláda	k1gFnSc2	vláda
Mansúra	Mansúr	k1gMnSc2	Mansúr
Hádího	Hádí	k1gMnSc2	Hádí
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
údajně	údajně	k6eAd1	údajně
podporuje	podporovat	k5eAaImIp3nS	podporovat
hutíjské	hutíjský	k2eAgMnPc4d1	hutíjský
rebely	rebel	k1gMnPc4	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Saúdové	Saúda	k1gMnPc1	Saúda
zahájili	zahájit	k5eAaPmAgMnP	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2015	[number]	k4	2015
společně	společně	k6eAd1	společně
s	s	k7c7	s
koalicí	koalice	k1gFnSc7	koalice
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
včetně	včetně	k7c2	včetně
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
,	,	kIx,	,
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
,	,	kIx,	,
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
také	také	k9	také
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
pátek	pátek	k1gInSc4	pátek
25.8	[number]	k4	25.8
<g/>
.2017	.2017	k4	.2017
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
náletu	nálet	k1gInSc3	nálet
na	na	k7c4	na
jemenskou	jemenský	k2eAgFnSc4d1	Jemenská
metropoli	metropole	k1gFnSc4	metropole
Saná	Saná	k1gFnSc1	Saná
<g/>
.	.	kIx.	.
</s>
<s>
Nálet	nálet	k1gInSc1	nálet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
předměstí	předměstí	k1gNnSc6	předměstí
Saná	Saná	k1gFnSc1	Saná
<g/>
,	,	kIx,	,
podnikla	podniknout	k5eAaPmAgFnS	podniknout
letadla	letadlo	k1gNnSc2	letadlo
koalice	koalice	k1gFnSc2	koalice
vedené	vedený	k2eAgNnSc1d1	vedené
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jemenské	jemenský	k2eAgFnSc3d1	Jemenská
vládě	vláda	k1gFnSc3	vláda
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
šíitským	šíitský	k2eAgMnPc3d1	šíitský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Arabská	arabský	k2eAgFnSc1d1	arabská
koalice	koalice	k1gFnSc1	koalice
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
omylem	omylem	k6eAd1	omylem
<g/>
"	"	kIx"	"
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
:	:	kIx,	:
agentura	agentura	k1gFnSc1	agentura
Reuters	Reutersa	k1gFnPc2	Reutersa
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
psala	psát	k5eAaImAgFnS	psát
o	o	k7c4	o
nejméně	málo	k6eAd3	málo
12	[number]	k4	12
mrtvých	mrtvý	k1gMnPc2	mrtvý
civilistech	civilista	k1gMnPc6	civilista
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
6	[number]	k4	6
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
agentura	agentura	k1gFnSc1	agentura
AFP	AFP	kA	AFP
o	o	k7c4	o
14	[number]	k4	14
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
sourozenců	sourozenec	k1gMnPc2	sourozenec
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
návštěvu	návštěva	k1gFnSc4	návštěva
si	se	k3xPyFc3	se
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
při	pře	k1gFnSc4	pře
které	který	k3yQgNnSc1	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodnutí	dohodnutí	k1gNnSc3	dohodnutí
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
kontraktu	kontrakt	k1gInSc2	kontrakt
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
110	[number]	k4	110
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
zbrojní	zbrojní	k2eAgFnSc4d1	zbrojní
zakázku	zakázka	k1gFnSc4	zakázka
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
oběma	dva	k4xCgMnPc7	dva
stranami	strana	k1gFnPc7	strana
propagována	propagován	k2eAgFnSc1d1	propagována
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Kritikové	kritik	k1gMnPc1	kritik
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
zaprodáním	zaprodání	k1gNnSc7	zaprodání
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
negativním	negativní	k2eAgInSc7d1	negativní
dopadem	dopad	k1gInSc7	dopad
na	na	k7c6	na
situaci	situace	k1gFnSc6	situace
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
OSN	OSN	kA	OSN
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c7	za
největší	veliký	k2eAgFnSc7d3	veliký
světovou	světový	k2eAgFnSc7d1	světová
humanitární	humanitární	k2eAgFnSc7d1	humanitární
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
se	se	k3xPyFc4	se
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
nepřímo	přímo	k6eNd1	přímo
zapojila	zapojit	k5eAaPmAgFnS	zapojit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporuje	podporovat	k5eAaImIp3nS	podporovat
penězi	peníze	k1gInPc7	peníze
a	a	k8xC	a
zbraněmi	zbraň	k1gFnPc7	zbraň
islamistické	islamistický	k2eAgFnSc2d1	islamistická
povstalce	povstalec	k1gMnSc2	povstalec
z	z	k7c2	z
Fronty	fronta	k1gFnSc2	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
organizaci	organizace	k1gFnSc4	organizace
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
leží	ležet	k5eAaImIp3nS	ležet
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Akabským	Akabský	k2eAgInSc7d1	Akabský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
2640	[number]	k4	2640
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
2	[number]	k4	2
149	[number]	k4	149
690	[number]	k4	690
km2	km2	k4	km2
zabírá	zabírat	k5eAaImIp3nS	zabírat
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
%	%	kIx~	%
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Ománem	Omán	k1gInSc7	Omán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
arabskými	arabský	k2eAgInPc7d1	arabský
emiráty	emirát	k1gInPc7	emirát
(	(	kIx(	(
<g/>
SAE	SAE	kA	SAE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Katarem	katar	k1gMnSc7	katar
a	a	k8xC	a
Bahrajnem	Bahrajn	k1gMnSc7	Bahrajn
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
<g/>
,	,	kIx,	,
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
a	a	k8xC	a
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vede	vést	k5eAaImIp3nS	vést
spory	spor	k1gInPc4	spor
o	o	k7c4	o
hranice	hranice	k1gFnPc4	hranice
se	se	k3xPyFc4	se
SAE	SAE	kA	SAE
kvůli	kvůli	k7c3	kvůli
ložiskům	ložisko	k1gNnPc3	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
táhne	táhnout	k5eAaImIp3nS	táhnout
horský	horský	k2eAgInSc4d1	horský
systém	systém	k1gInSc4	systém
Saravátu	Saravát	k1gInSc2	Saravát
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
pohoří	pohoří	k1gNnSc4	pohoří
Hidžáz	Hidžáz	k1gInSc1	Hidžáz
<g/>
,	,	kIx,	,
Asír	Asír	k1gInSc1	Asír
a	a	k8xC	a
Jemenské	jemenský	k2eAgFnPc1d1	Jemenská
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východu	východ	k1gInSc3	východ
hory	hora	k1gFnSc2	hora
klesají	klesat	k5eAaImIp3nP	klesat
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
v	v	k7c4	v
náhorní	náhorní	k2eAgFnSc4d1	náhorní
plošinu	plošina	k1gFnSc4	plošina
Nadžd	Nadžda	k1gFnPc2	Nadžda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
hora	hora	k1gFnSc1	hora
Džabal	Džabal	k1gInSc1	Džabal
Saudá	Saudý	k2eAgFnSc1d1	Saudý
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
3133	[number]	k4	3133
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Asír	Asíra	k1gFnPc2	Asíra
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
úroveň	úroveň	k1gFnSc4	úroveň
hladiny	hladina	k1gFnSc2	hladina
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
země	zem	k1gFnSc2	zem
představuje	představovat	k5eAaImIp3nS	představovat
velká	velký	k2eAgFnSc1d1	velká
poušť	poušť	k1gFnSc1	poušť
Rub	rub	k1gInSc1	rub
al-Chálí	al-Chálet	k5eAaPmIp3nS	al-Chálet
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Pustá	pustý	k2eAgFnSc1d1	pustá
končina	končina	k1gFnSc1	končina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
přes	přes	k7c4	přes
640	[number]	k4	640
000	[number]	k4	000
km2	km2	k4	km2
největší	veliký	k2eAgFnSc7d3	veliký
souvislou	souvislý	k2eAgFnSc7d1	souvislá
písečnou	písečný	k2eAgFnSc7d1	písečná
pouští	poušť	k1gFnSc7	poušť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
pouště	poušť	k1gFnPc1	poušť
Dahná	Dahná	k1gFnSc1	Dahná
a	a	k8xC	a
Nafúd	Nafúd	k1gMnSc1	Nafúd
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
i	i	k9	i
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgFnPc1	žádný
stálé	stálý	k2eAgFnPc1d1	stálá
vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
pouze	pouze	k6eAd1	pouze
mnoho	mnoho	k4c4	mnoho
vádí	vádí	k1gNnPc2	vádí
(	(	kIx(	(
<g/>
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
vyschlá	vyschlý	k2eAgNnPc1d1	vyschlé
koryta	koryto	k1gNnPc1	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
voda	voda	k1gFnSc1	voda
protéká	protékat	k5eAaImIp3nS	protékat
jen	jen	k9	jen
nárazově	nárazově	k6eAd1	nárazově
po	po	k7c6	po
deštích	dešť	k1gInPc6	dešť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
neúrodná	úrodný	k2eNgFnSc1d1	neúrodná
<g/>
,	,	kIx,	,
obdělává	obdělávat	k5eAaImIp3nS	obdělávat
se	s	k7c7	s
1,67	[number]	k4	1,67
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
aluviální	aluviální	k2eAgFnPc4d1	aluviální
usazeniny	usazenina	k1gFnPc4	usazenina
(	(	kIx(	(
<g/>
usazeniny	usazenina	k1gFnPc4	usazenina
říčního	říční	k2eAgInSc2d1	říční
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vádí	vádí	k1gNnPc6	vádí
<g/>
,	,	kIx,	,
kotlinách	kotlina	k1gFnPc6	kotlina
a	a	k8xC	a
oázách	oáza	k1gFnPc6	oáza
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
trpí	trpět	k5eAaImIp3nS	trpět
desertifikací	desertifikace	k1gFnSc7	desertifikace
<g/>
,	,	kIx,	,
vyčerpáváním	vyčerpávání	k1gNnSc7	vyčerpávání
podzemních	podzemní	k2eAgFnPc2d1	podzemní
zásob	zásoba	k1gFnPc2	zásoba
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kontaminací	kontaminace	k1gFnSc7	kontaminace
půdy	půda	k1gFnSc2	půda
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
úniku	únik	k1gInSc2	únik
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
jevem	jev	k1gInSc7	jev
jsou	být	k5eAaImIp3nP	být
písečné	písečný	k2eAgFnPc4d1	písečná
a	a	k8xC	a
prachové	prachový	k2eAgFnPc4d1	prachová
bouře	bouř	k1gFnPc4	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
téměř	téměř	k6eAd1	téměř
nečinné	činný	k2eNgInPc1d1	nečinný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
začalo	začít	k5eAaPmAgNnS	začít
aktivně	aktivně	k6eAd1	aktivně
zapojovat	zapojovat	k5eAaImF	zapojovat
do	do	k7c2	do
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
15	[number]	k4	15
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
80	[number]	k4	80
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
menší	malý	k2eAgMnPc4d2	menší
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
hyena	hyena	k1gFnSc1	hyena
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
medojed	medojed	k1gMnSc1	medojed
<g/>
,	,	kIx,	,
promyka	promyka	k1gFnSc1	promyka
<g/>
,	,	kIx,	,
dikobraz	dikobraz	k1gMnSc1	dikobraz
<g/>
,	,	kIx,	,
pavián	pavián	k1gMnSc1	pavián
<g/>
,	,	kIx,	,
ježek	ježek	k1gMnSc1	ježek
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
a	a	k8xC	a
pískomil	pískomil	k1gMnSc1	pískomil
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
větší	veliký	k2eAgInPc4d2	veliký
pak	pak	k6eAd1	pak
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
gazel	gazela	k1gFnPc2	gazela
<g/>
,	,	kIx,	,
přímorožec	přímorožec	k1gMnSc1	přímorožec
a	a	k8xC	a
levhart	levhart	k1gMnSc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
asi	asi	k9	asi
180	[number]	k4	180
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
11	[number]	k4	11
je	být	k5eAaImIp3nS	být
endemických	endemický	k2eAgFnPc2d1	endemická
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
neendemické	endemický	k2eNgMnPc4d1	endemický
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
orel	orel	k1gMnSc1	orel
<g/>
,	,	kIx,	,
drop	drop	k1gMnSc1	drop
<g/>
,	,	kIx,	,
orlovec	orlovec	k1gMnSc1	orlovec
<g/>
,	,	kIx,	,
ostříž	ostříž	k1gMnSc1	ostříž
a	a	k8xC	a
plameňák	plameňák	k1gMnSc1	plameňák
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
království	království	k1gNnSc6	království
nachází	nacházet	k5eAaImIp3nS	nacházet
60	[number]	k4	60
druhů	druh	k1gInPc2	druh
ještěrů	ještěr	k1gMnPc2	ještěr
<g/>
,	,	kIx,	,
34	[number]	k4	34
druhů	druh	k1gInPc2	druh
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
želv	želva	k1gFnPc2	želva
a	a	k8xC	a
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Bohatý	bohatý	k2eAgMnSc1d1	bohatý
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgInSc1d1	vodní
život	život	k1gInSc1	život
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
většinou	většinou	k6eAd1	většinou
malé	malý	k2eAgFnPc1d1	malá
byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
málo	málo	k1gNnSc4	málo
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
krmivo	krmivo	k1gNnSc1	krmivo
pro	pro	k7c4	pro
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asíru	Asír	k1gInSc6	Asír
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
oblastech	oblast	k1gFnPc6	oblast
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
rostlinou	rostlina	k1gFnSc7	rostlina
je	být	k5eAaImIp3nS	být
datlovník	datlovník	k1gInSc1	datlovník
pravý	pravý	k2eAgInSc1d1	pravý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
západní	západní	k2eAgFnSc2d1	západní
provincie	provincie	k1gFnSc2	provincie
Asír	Asíra	k1gFnPc2	Asíra
suché	suchý	k2eAgNnSc4d1	suché
pouštní	pouštní	k2eAgNnSc4d1	pouštní
podnebí	podnebí	k1gNnSc4	podnebí
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokými	vysoký	k2eAgFnPc7d1	vysoká
denními	denní	k2eAgFnPc7d1	denní
a	a	k8xC	a
nízkými	nízký	k2eAgFnPc7d1	nízká
nočními	noční	k2eAgFnPc7d1	noční
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
minimem	minimum	k1gNnSc7	minimum
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
rozdíly	rozdíl	k1gInPc1	rozdíl
podnebných	podnebný	k2eAgFnPc2d1	podnebná
podmínek	podmínka	k1gFnPc2	podmínka
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
vnitrozemskými	vnitrozemský	k2eAgFnPc7d1	vnitrozemská
a	a	k8xC	a
přímořskými	přímořský	k2eAgFnPc7d1	přímořská
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
ve	v	k7c6	v
dne	den	k1gInSc2	den
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
45	[number]	k4	45
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
až	až	k9	až
na	na	k7c4	na
54	[number]	k4	54
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
19	[number]	k4	19
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
málokdy	málokdy	k6eAd1	málokdy
klesají	klesat	k5eAaImIp3nP	klesat
pod	pod	k7c7	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Výjimečně	výjimečně	k6eAd1	výjimečně
ale	ale	k8xC	ale
napadá	napadat	k5eAaImIp3nS	napadat
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
výšinách	výšina	k1gFnPc6	výšina
nebo	nebo	k8xC	nebo
i	i	k9	i
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Asíru	Asír	k1gInSc2	Asír
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
monzuny	monzun	k1gInPc4	monzun
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
celoročně	celoročně	k6eAd1	celoročně
zde	zde	k6eAd1	zde
spadne	spadnout	k5eAaPmIp3nS	spadnout
zhruba	zhruba	k6eAd1	zhruba
480	[number]	k4	480
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
velmi	velmi	k6eAd1	velmi
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
spadne	spadnout	k5eAaPmIp3nS	spadnout
např.	např.	kA	např.
65	[number]	k4	65
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nezaprší	zapršet	k5eNaPmIp3nS	zapršet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
je	být	k5eAaImIp3nS	být
islámská	islámský	k2eAgFnSc1d1	islámská
absolutní	absolutní	k2eAgFnSc1d1	absolutní
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
šaría	šarí	k2eAgFnSc1d1	šaría
<g/>
.	.	kIx.	.
</s>
<s>
Islámským	islámský	k2eAgNnSc7d1	islámské
právem	právo	k1gNnSc7	právo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
představují	představovat	k5eAaImIp3nP	představovat
korán	korán	k1gInSc4	korán
a	a	k8xC	a
sunna	sunna	k1gFnSc1	sunna
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
řídit	řídit	k5eAaImF	řídit
i	i	k9	i
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
i	i	k9	i
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
v	v	k7c6	v
ústavních	ústavní	k2eAgInPc6d1	ústavní
sporech	spor	k1gInPc6	spor
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
představuje	představovat	k5eAaImIp3nS	představovat
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
král	král	k1gMnSc1	král
Salmán	Salmán	k2eAgInSc1d1	Salmán
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
několik	několik	k4yIc1	několik
členů	člen	k1gMnPc2	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
následovníci	následovník	k1gMnPc1	následovník
trůnu	trůn	k1gInSc2	trůn
byli	být	k5eAaImAgMnP	být
synové	syn	k1gMnPc1	syn
Abd	Abd	k1gFnSc1	Abd
al-Azíze	al-Azíze	k1gFnSc1	al-Azíze
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
novodobé	novodobý	k2eAgFnSc2d1	novodobá
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zřídil	zřídit	k5eAaPmAgMnS	zřídit
Komisi	komise	k1gFnSc4	komise
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zvolit	zvolit	k5eAaPmF	zvolit
další	další	k2eAgMnPc4d1	další
krále	král	k1gMnPc4	král
a	a	k8xC	a
korunní	korunní	k2eAgMnPc4d1	korunní
prince	princ	k1gMnPc4	princ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předcházelo	předcházet	k5eAaImAgNnS	předcházet
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
Poradní	poradní	k2eAgNnSc1d1	poradní
shromáždění	shromáždění	k1gNnSc1	shromáždění
čítající	čítající	k2eAgFnSc4d1	čítající
150	[number]	k4	150
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
zákony	zákon	k1gInPc4	zákon
ní	on	k3xPp3gFnSc2	on
navržené	navržený	k2eAgFnSc2d1	navržená
ale	ale	k8xC	ale
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
až	až	k9	až
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
první	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
místních	místní	k2eAgInPc2d1	místní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
měli	mít	k5eAaImAgMnP	mít
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
starší	starý	k2eAgMnPc1d2	starší
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
státního	státní	k2eAgInSc2d1	státní
aparátu	aparát	k1gInSc2	aparát
jsou	být	k5eAaImIp3nP	být
ulamové	ulam	k1gMnPc1	ulam
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
školství	školství	k1gNnSc4	školství
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Islámské	islámský	k2eAgInPc1d1	islámský
soudy	soud	k1gInPc1	soud
jsou	být	k5eAaImIp3nP	být
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
částí	část	k1gFnSc7	část
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
řeší	řešit	k5eAaImIp3nS	řešit
nejvíce	nejvíce	k6eAd1	nejvíce
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
ně	on	k3xPp3gInPc4	on
existují	existovat	k5eAaImIp3nP	existovat
menší	malý	k2eAgInPc1d2	menší
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
omezené	omezený	k2eAgNnSc4d1	omezené
pole	pole	k1gNnSc4	pole
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
zvrátit	zvrátit	k5eAaPmF	zvrátit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nižších	nízký	k2eAgInPc2d2	nižší
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
individuálním	individuální	k2eAgInSc6d1	individuální
způsobu	způsob	k1gInSc6	způsob
výkladu	výklad	k1gInSc2	výklad
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
<g/>
Nemuslimským	muslimský	k2eNgMnPc3d1	nemuslimský
obyvatelům	obyvatel	k1gMnPc3	obyvatel
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
odpírána	odpírán	k2eAgFnSc1d1	odpírána
svoboda	svoboda	k1gFnSc1	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Abdalláhova	Abdalláhův	k2eAgFnSc1d1	Abdalláhův
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
zčásti	zčásti	k6eAd1	zčásti
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
západních	západní	k2eAgFnPc2d1	západní
mocností	mocnost	k1gFnPc2	mocnost
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c4	o
větší	veliký	k2eAgFnSc4d2	veliký
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
dodržována	dodržován	k2eAgFnSc1d1	dodržována
svoboda	svoboda	k1gFnSc1	svoboda
shromažďování	shromažďování	k1gNnSc1	shromažďování
ani	ani	k8xC	ani
svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
je	být	k5eAaImIp3nS	být
regulován	regulovat	k5eAaImNgInS	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Human	Human	k1gMnSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gMnSc1	Watch
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
odsouzení	odsouzení	k1gNnSc4	odsouzení
v	v	k7c6	v
nespravedlivých	spravedlivý	k2eNgInPc6d1	nespravedlivý
soudních	soudní	k2eAgInPc6d1	soudní
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
týrání	týrání	k1gNnSc1	týrání
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
diskriminaci	diskriminace	k1gFnSc4	diskriminace
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
nucení	nucení	k1gNnSc4	nucení
k	k	k7c3	k
předčasnému	předčasný	k2eAgNnSc3d1	předčasné
manželství	manželství	k1gNnSc3	manželství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
funguje	fungovat	k5eAaImIp3nS	fungovat
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
ctnosti	ctnost	k1gFnSc2	ctnost
a	a	k8xC	a
bránění	bránění	k1gNnSc2	bránění
neřesti	neřest	k1gFnSc2	neřest
<g/>
,	,	kIx,	,
náboženská	náboženský	k2eAgFnSc1d1	náboženská
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nevybíravými	vybíravý	k2eNgInPc7d1	nevybíravý
a	a	k8xC	a
často	často	k6eAd1	často
brutálními	brutální	k2eAgInPc7d1	brutální
způsoby	způsob	k1gInPc7	způsob
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
náboženských	náboženský	k2eAgInPc2d1	náboženský
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnSc3d1	politická
opozici	opozice	k1gFnSc3	opozice
a	a	k8xC	a
lidem	lid	k1gInSc7	lid
podezřelým	podezřelý	k2eAgInSc7d1	podezřelý
z	z	k7c2	z
terorismu	terorismus	k1gInSc2	terorismus
hrozí	hrozit	k5eAaImIp3nS	hrozit
svévolné	svévolný	k2eAgNnSc4d1	svévolné
zadržení	zadržení	k1gNnSc4	zadržení
a	a	k8xC	a
nespravedlivé	spravedlivý	k2eNgNnSc4d1	nespravedlivé
odsouzení	odsouzení	k1gNnSc4	odsouzení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tresty	trest	k1gInPc7	trest
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
rány	rána	k1gFnPc1	rána
holí	holit	k5eAaImIp3nP	holit
nebo	nebo	k8xC	nebo
useknutí	useknutí	k1gNnSc4	useknutí
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
udělen	udělit	k5eAaPmNgInS	udělit
např.	např.	kA	např.
za	za	k7c4	za
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
<g/>
,	,	kIx,	,
zřeknutí	zřeknutí	k1gNnSc1	zřeknutí
se	se	k3xPyFc4	se
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
cizoložství	cizoložství	k1gNnSc2	cizoložství
<g/>
,	,	kIx,	,
loupež	loupež	k1gFnSc1	loupež
nebo	nebo	k8xC	nebo
pašování	pašování	k1gNnSc1	pašování
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pracovníci	pracovník	k1gMnPc1	pracovník
nemají	mít	k5eNaImIp3nP	mít
sociální	sociální	k2eAgNnPc1d1	sociální
<g/>
,	,	kIx,	,
politická	politický	k2eAgNnPc1d1	politické
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnPc1d1	kulturní
ani	ani	k8xC	ani
ekonomická	ekonomický	k2eAgNnPc1d1	ekonomické
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
činnost	činnost	k1gFnSc1	činnost
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
domácí	domácí	k2eAgMnPc1d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
opoziční	opoziční	k2eAgMnPc1d1	opoziční
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
extrémisté	extrémista	k1gMnPc1	extrémista
<g/>
,	,	kIx,	,
liberalisté	liberalista	k1gMnPc1	liberalista
a	a	k8xC	a
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
ité	ité	k?	ité
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
se	s	k7c7	s
smíšeným	smíšený	k2eAgInSc7d1	smíšený
úspěchem	úspěch	k1gInSc7	úspěch
snaží	snažit	k5eAaImIp3nP	snažit
opozici	opozice	k1gFnSc4	opozice
potlačovat	potlačovat	k5eAaImF	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
důkladně	důkladně	k6eAd1	důkladně
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
všechny	všechen	k3xTgInPc4	všechen
aspekty	aspekt	k1gInPc4	aspekt
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
centralizovanou	centralizovaný	k2eAgFnSc7d1	centralizovaná
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
13	[number]	k4	13
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
emirátů	emirát	k1gInPc2	emirát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
guvernér	guvernér	k1gMnSc1	guvernér
ze	z	k7c2	z
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
rodiny	rodina	k1gFnSc2	rodina
jmenovaný	jmenovaný	k2eAgInSc4d1	jmenovaný
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
guvernoráty	guvernorát	k1gMnPc4	guvernorát
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
saúdskými	saúdský	k2eAgMnPc7d1	saúdský
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Rijád	Rijáda	k1gFnPc2	Rijáda
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
;	;	kIx,	;
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žilo	žít	k5eAaImAgNnS	žít
4	[number]	k4	4
725	[number]	k4	725
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Džidda	Džidda	k1gMnSc1	Džidda
(	(	kIx(	(
<g/>
3	[number]	k4	3
234	[number]	k4	234
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mekka	Mekka	k1gFnSc1	Mekka
(	(	kIx(	(
<g/>
1	[number]	k4	1
484	[number]	k4	484
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Medína	Medína	k1gFnSc1	Medína
(	(	kIx(	(
<g/>
1	[number]	k4	1
104	[number]	k4	104
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dammám	Damma	k1gFnPc3	Damma
(	(	kIx(	(
<g/>
902	[number]	k4	902
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
====	====	k?	====
</s>
</p>
<p>
<s>
Království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ligy	liga	k1gFnSc2	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
LAS	laso	k1gNnPc2	laso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
spoluzaložila	spoluzaložit	k5eAaPmAgFnS	spoluzaložit
Organizaci	organizace	k1gFnSc4	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgMnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(	(
<g/>
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
měnovém	měnový	k2eAgInSc6d1	měnový
fondu	fond	k1gInSc6	fond
a	a	k8xC	a
Světové	světový	k2eAgFnSc3d1	světová
bance	banka	k1gFnSc3	banka
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gInPc7	její
hlavními	hlavní	k2eAgInPc7d1	hlavní
cíli	cíl	k1gInPc7	cíl
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
jsou	být	k5eAaImIp3nP	být
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
bránit	bránit	k5eAaImF	bránit
arabské	arabský	k2eAgInPc4d1	arabský
a	a	k8xC	a
islámské	islámský	k2eAgInPc4d1	islámský
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
podporovat	podporovat	k5eAaImF	podporovat
solidaritu	solidarita	k1gFnSc4	solidarita
mezi	mezi	k7c7	mezi
islámskými	islámský	k2eAgFnPc7d1	islámská
vládami	vláda	k1gFnPc7	vláda
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
ropnými	ropný	k2eAgFnPc7d1	ropná
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
největšími	veliký	k2eAgMnPc7d3	veliký
odběrateli	odběratel	k1gMnPc7	odběratel
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
pomoci	pomoc	k1gFnSc2	pomoc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
několika	několik	k4yIc3	několik
arabským	arabský	k2eAgInPc3d1	arabský
<g/>
,	,	kIx,	,
africkým	africký	k2eAgInPc3d1	africký
a	a	k8xC	a
asijským	asijský	k2eAgInPc3d1	asijský
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
mírové	mírový	k2eAgNnSc4d1	Mírové
vyřešení	vyřešení	k1gNnSc4	vyřešení
arabsko-izraelského	arabskozraelský	k2eAgInSc2d1	arabsko-izraelský
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
stáhnutí	stáhnutí	k1gNnSc1	stáhnutí
Izraele	Izrael	k1gInSc2	Izrael
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
jím	jíst	k5eAaImIp1nS	jíst
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
území	území	k1gNnPc2	území
a	a	k8xC	a
uznání	uznání	k1gNnSc2	uznání
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
jako	jako	k8xS	jako
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
USA	USA	kA	USA
====	====	k?	====
</s>
</p>
<p>
<s>
Diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgFnPc4d1	významná
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
kvůli	kvůli	k7c3	kvůli
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnSc3d1	vojenská
nebo	nebo	k8xC	nebo
politické	politický	k2eAgFnSc3d1	politická
spolupráci	spolupráce	k1gFnSc3	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
království	království	k1gNnPc4	království
s	s	k7c7	s
výcvikem	výcvik	k1gInSc7	výcvik
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
dodávkami	dodávka	k1gFnPc7	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
administrativy	administrativa	k1gFnSc2	administrativa
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
prodaly	prodat	k5eAaPmAgInP	prodat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
Saúdské	saúdský	k2eAgInPc1d1	saúdský
Arábii	Arábie	k1gFnSc4	Arábie
zbraně	zbraň	k1gFnPc1	zbraň
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
110	[number]	k4	110
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
byly	být	k5eAaImAgInP	být
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
zeměmi	zem	k1gFnPc7	zem
oslabeny	oslaben	k2eAgInPc4d1	oslaben
(	(	kIx(	(
<g/>
15	[number]	k4	15
sebevražedných	sebevražedný	k2eAgMnPc2d1	sebevražedný
atentátníků	atentátník	k1gMnPc2	atentátník
bylo	být	k5eAaImAgNnS	být
saúdské	saúdský	k2eAgFnSc3d1	Saúdská
národnosti	národnost	k1gFnSc3	národnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
USA	USA	kA	USA
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
udržovalo	udržovat	k5eAaImAgNnS	udržovat
království	království	k1gNnSc1	království
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
vztahy	vztah	k1gInPc4	vztah
(	(	kIx(	(
<g/>
shodují	shodovat	k5eAaImIp3nP	shodovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
ropných	ropný	k2eAgFnPc6d1	ropná
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
,	,	kIx,	,
obraně	obrana	k1gFnSc3	obrana
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
arabsko-izraelský	arabskozraelský	k2eAgInSc4d1	arabsko-izraelský
konflikt	konflikt	k1gInSc4	konflikt
<g/>
)	)	kIx)	)
i	i	k9	i
přes	přes	k7c4	přes
výrazně	výrazně	k6eAd1	výrazně
odlišný	odlišný	k2eAgInSc4d1	odlišný
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vztahy	vztah	k1gInPc1	vztah
prudce	prudko	k6eAd1	prudko
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
a	a	k8xC	a
král	král	k1gMnSc1	král
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
kvůli	kvůli	k7c3	kvůli
<g />
.	.	kIx.	.
</s>
<s>
íránskému	íránský	k2eAgInSc3d1	íránský
jadernému	jaderný	k2eAgInSc3d1	jaderný
programu	program	k1gInSc3	program
opakovaně	opakovaně	k6eAd1	opakovaně
žádal	žádat	k5eAaImAgMnS	žádat
USA	USA	kA	USA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zbaví	zbavit	k5eAaPmIp3nS	zbavit
amerických	americký	k2eAgNnPc2d1	americké
aktiv	aktivum	k1gNnPc2	aktivum
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
více	hodně	k6eAd2	hodně
než	než	k8xS	než
750	[number]	k4	750
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
americký	americký	k2eAgInSc1d1	americký
Kongres	kongres	k1gInSc1	kongres
označí	označit	k5eAaPmIp3nS	označit
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
jako	jako	k8xC	jako
zodpovědnou	zodpovědný	k2eAgFnSc4d1	zodpovědná
za	za	k7c4	za
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
arabskými	arabský	k2eAgInPc7d1	arabský
státy	stát	k1gInPc7	stát
====	====	k?	====
</s>
</p>
<p>
<s>
Diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
vzrůstajícího	vzrůstající	k2eAgInSc2d1	vzrůstající
vlivu	vliv	k1gInSc2	vliv
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itů	itů	k?	itů
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
si	se	k3xPyFc3	se
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
udržuje	udržovat	k5eAaImIp3nS	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
např.	např.	kA	např.
s	s	k7c7	s
Bahrajnem	Bahrajn	k1gInSc7	Bahrajn
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
arabskými	arabský	k2eAgInPc7d1	arabský
emiráty	emirát	k1gInPc7	emirát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
s	s	k7c7	s
Katarem	katar	k1gInSc7	katar
<g/>
,	,	kIx,	,
Ománem	Omán	k1gInSc7	Omán
a	a	k8xC	a
Jemenem	Jemen	k1gInSc7	Jemen
má	mít	k5eAaImIp3nS	mít
vztahy	vztah	k1gInPc4	vztah
spíše	spíše	k9	spíše
špatné	špatný	k2eAgInPc4d1	špatný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
====	====	k?	====
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
opakovaně	opakovaně	k6eAd1	opakovaně
kritizovalo	kritizovat	k5eAaImAgNnS	kritizovat
saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
proti	proti	k7c3	proti
jemenským	jemenský	k2eAgMnPc3d1	jemenský
šíitům	šíita	k1gMnPc3	šíita
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
podporují	podporovat	k5eAaImIp3nP	podporovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
Rusko	Rusko	k1gNnSc1	Rusko
podporuje	podporovat	k5eAaImIp3nS	podporovat
syrského	syrský	k2eAgMnSc4d1	syrský
prezidenta	prezident	k1gMnSc4	prezident
Asada	Asad	k1gMnSc4	Asad
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
podporuje	podporovat	k5eAaImIp3nS	podporovat
povstalce	povstalec	k1gMnPc4	povstalec
proti	proti	k7c3	proti
syrské	syrský	k2eAgFnSc3d1	Syrská
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
k	k	k7c3	k
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
produkci	produkce	k1gFnSc4	produkce
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
snížila	snížit	k5eAaPmAgFnS	snížit
její	její	k3xOp3gFnSc4	její
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
poškodila	poškodit	k5eAaPmAgFnS	poškodit
tak	tak	k9	tak
ekonomiky	ekonomika	k1gFnPc1	ekonomika
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
příjmech	příjem	k1gInPc6	příjem
z	z	k7c2	z
těžby	těžba	k1gFnSc2	těžba
ropy	ropa	k1gFnSc2	ropa
závislé	závislý	k2eAgNnSc1d1	závislé
<g/>
.	.	kIx.	.
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
nicméně	nicméně	k8xC	nicméně
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
investice	investice	k1gFnSc1	investice
bude	být	k5eAaImBp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
investicí	investice	k1gFnSc7	investice
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc3d1	další
spolupráci	spolupráce	k1gFnSc3	spolupráce
Rusko	Rusko	k1gNnSc1	Rusko
plánuje	plánovat	k5eAaImIp3nS	plánovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dodávek	dodávka	k1gFnPc2	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
pro	pro	k7c4	pro
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2016	[number]	k4	2016
Rusko	Rusko	k1gNnSc1	Rusko
připravilo	připravit	k5eAaPmAgNnS	připravit
návrh	návrh	k1gInSc4	návrh
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
prodeje	prodej	k1gFnSc2	prodej
zbraní	zbraň	k1gFnPc2	zbraň
Saúdské	saúdský	k2eAgFnSc3d1	Saúdská
Arábii	Arábie	k1gFnSc3	Arábie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
dodávky	dodávka	k1gFnPc1	dodávka
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
také	také	k9	také
několika	několik	k4yIc2	několik
typů	typ	k1gInPc2	typ
systémů	systém	k1gInPc2	systém
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
různého	různý	k2eAgInSc2d1	různý
doletu	dolet	k1gInSc2	dolet
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
asi	asi	k9	asi
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
vede	vést	k5eAaImIp3nS	vést
průběžně	průběžně	k6eAd1	průběžně
rozhovory	rozhovor	k1gInPc4	rozhovor
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
OPEC	opéct	k5eAaPmRp2nSwK	opéct
a	a	k8xC	a
také	také	k6eAd1	také
se	s	k7c7	s
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
kvůli	kvůli	k7c3	kvůli
poklesu	pokles	k1gInSc3	pokles
ceny	cena	k1gFnSc2	cena
ropy	ropa	k1gFnSc2	ropa
až	až	k9	až
o	o	k7c4	o
70	[number]	k4	70
%	%	kIx~	%
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
udál	udát	k5eAaPmAgMnS	udát
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Petrohradském	petrohradský	k2eAgNnSc6d1	Petrohradské
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
fóru	fórum	k1gNnSc6	fórum
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
Vladimira	Vladimir	k1gMnSc2	Vladimir
Putina	putin	k2eAgMnSc2d1	putin
se	s	k7c7	s
saúdskoarabským	saúdskoarabský	k2eAgMnSc7d1	saúdskoarabský
princem	princ	k1gMnSc7	princ
Muhammadem	Muhammad	k1gInSc7	Muhammad
bin	bin	k?	bin
Salmánem	Salmán	k1gMnSc7	Salmán
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
krále	král	k1gMnSc2	král
Salmána	Salmán	k1gMnSc2	Salmán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Českem	Česko	k1gNnSc7	Česko
====	====	k?	====
</s>
</p>
<p>
<s>
S	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
navázala	navázat	k5eAaPmAgFnS	navázat
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc7	vztah
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
její	její	k3xOp3gInSc4	její
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
do	do	k7c2	do
království	království	k1gNnSc2	království
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
bižuterii	bižuterie	k1gFnSc4	bižuterie
a	a	k8xC	a
produkty	produkt	k1gInPc4	produkt
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
navštívil	navštívit	k5eAaPmAgMnS	navštívit
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
a	a	k8xC	a
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
s	s	k7c7	s
královstvím	království	k1gNnSc7	království
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
politické	politický	k2eAgFnSc2d1	politická
<g/>
,	,	kIx,	,
školské	školský	k2eAgFnSc2d1	školská
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgFnSc2d1	vědecká
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc2d1	kulturní
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc2d1	obchodní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
výstava	výstava	k1gFnSc1	výstava
s	s	k7c7	s
názvem	název	k1gInSc7	název
Saúdskoarabské	saúdskoarabský	k2eAgFnSc2d1	saúdskoarabská
kulturní	kulturní	k2eAgFnSc2d1	kulturní
dědictví	dědictví	k1gNnSc3	dědictví
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
čestným	čestný	k2eAgMnSc7d1	čestný
hostem	host	k1gMnSc7	host
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
veletrhu	veletrh	k1gInSc2	veletrh
Svět	svět	k1gInSc1	svět
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
ochraně	ochrana	k1gFnSc6	ochrana
a	a	k8xC	a
podpoře	podpora	k1gFnSc6	podpora
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
navštívil	navštívit	k5eAaPmAgMnS	navštívit
s	s	k7c7	s
početnou	početný	k2eAgFnSc7d1	početná
delegací	delegace	k1gFnSc7	delegace
českých	český	k2eAgMnPc2d1	český
podnikatelů	podnikatel	k1gMnPc2	podnikatel
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
zahájil	zahájit	k5eAaPmAgMnS	zahájit
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
Salmánem	Salmán	k1gInSc7	Salmán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
diplomatickému	diplomatický	k2eAgInSc3d1	diplomatický
incidentu	incident	k1gInSc3	incident
s	s	k7c7	s
ČR	ČR	kA	ČR
a	a	k8xC	a
k	k	k7c3	k
předvolání	předvolání	k1gNnSc3	předvolání
českého	český	k2eAgMnSc2d1	český
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paseka	paseka	k1gFnSc1	paseka
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
román	román	k1gInSc4	román
Satanské	satanský	k2eAgInPc4d1	satanský
verše	verš	k1gInPc4	verš
od	od	k7c2	od
spisovatele	spisovatel	k1gMnSc2	spisovatel
Salmana	Salman	k1gMnSc2	Salman
Rushdieho	Rushdie	k1gMnSc2	Rushdie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
způsobil	způsobit	k5eAaPmAgMnS	způsobit
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
incident	incident	k1gInSc4	incident
mezi	mezi	k7c7	mezi
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
izraelského	izraelský	k2eAgInSc2d1	izraelský
dne	den	k1gInSc2	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
násilnými	násilný	k2eAgInPc7d1	násilný
akty	akt	k1gInPc7	akt
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
útok	útok	k1gInSc1	útok
v	v	k7c6	v
bruselském	bruselský	k2eAgNnSc6d1	bruselské
židovském	židovský	k2eAgNnSc6d1	Židovské
muzeu	muzeum	k1gNnSc6	muzeum
stojí	stát	k5eAaImIp3nS	stát
"	"	kIx"	"
<g/>
islámská	islámský	k2eAgFnSc1d1	islámská
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
si	se	k3xPyFc3	se
předvolala	předvolat	k5eAaPmAgFnS	předvolat
českého	český	k2eAgMnSc4d1	český
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
a	a	k8xC	a
předložila	předložit	k5eAaPmAgFnS	předložit
soupis	soupis	k1gInSc4	soupis
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
kdy	kdy	k6eAd1	kdy
Zeman	Zeman	k1gMnSc1	Zeman
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
islám	islám	k1gInSc1	islám
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
<g/>
Obchod	obchod	k1gInSc1	obchod
mezi	mezi	k7c7	mezi
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
17	[number]	k4	17
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2014	[number]	k4	2014
a	a	k8xC	a
2018	[number]	k4	2018
ČR	ČR	kA	ČR
vyvezla	vyvézt	k5eAaPmAgFnS	vyvézt
zbraně	zbraň	k1gFnPc4	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
144	[number]	k4	144
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
ale	ale	k9	ale
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
české	český	k2eAgFnPc1d1	Česká
zbraně	zbraň	k1gFnPc1	zbraň
vyvezené	vyvezený	k2eAgFnPc1d1	vyvezená
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
Libyi	Libye	k1gFnSc3	Libye
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
Nesehnutí	nesehnutí	k1gNnSc2	nesehnutí
"	"	kIx"	"
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
bezpočet	bezpočet	k1gInSc1	bezpočet
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
páchá	páchat	k5eAaImIp3nS	páchat
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
již	již	k6eAd1	již
nesmí	smět	k5eNaImIp3nS	smět
zavírat	zavírat	k5eAaImF	zavírat
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
okamžitě	okamžitě	k6eAd1	okamžitě
zastavit	zastavit	k5eAaPmF	zastavit
vývozy	vývoz	k1gInPc4	vývoz
českých	český	k2eAgFnPc2d1	Česká
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
celosvětově	celosvětově	k6eAd1	celosvětově
nejhorších	zlý	k2eAgInPc2d3	Nejhorší
represivních	represivní	k2eAgInPc2d1	represivní
režimů	režim	k1gInPc2	režim
s	s	k7c7	s
masivním	masivní	k2eAgNnSc7d1	masivní
porušováním	porušování	k1gNnSc7	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
přicházet	přicházet	k5eAaImF	přicházet
jako	jako	k8xS	jako
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otevřeně	otevřeně	k6eAd1	otevřeně
se	se	k3xPyFc4	se
hlásit	hlásit	k5eAaImF	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
je	být	k5eAaImIp3nS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
policií	policie	k1gFnSc7	policie
Mutaween	Mutawena	k1gFnPc2	Mutawena
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
28	[number]	k4	28
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
při	při	k7c6	při
zátahu	zátah	k1gInSc6	zátah
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
modlitebně	modlitebna	k1gFnSc6	modlitebna
v	v	k7c6	v
domě	dům	k1gInSc6	dům
indického	indický	k2eAgMnSc2d1	indický
občana	občan	k1gMnSc2	občan
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Chafdží	Chafdží	k1gNnSc2	Chafdží
<g/>
.	.	kIx.	.
<g/>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
používá	používat	k5eAaImIp3nS	používat
drastické	drastický	k2eAgInPc4d1	drastický
tresty	trest	k1gInPc4	trest
jako	jako	k8xC	jako
bičování	bičování	k1gNnPc4	bičování
za	za	k7c4	za
opilství	opilství	k1gNnSc4	opilství
<g/>
.	.	kIx.	.
</s>
<s>
Znásilněná	znásilněný	k2eAgFnSc1d1	znásilněná
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
k	k	k7c3	k
200	[number]	k4	200
ranám	rána	k1gFnPc3	rána
bičem	bič	k1gInSc7	bič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
sama	sám	k3xTgFnSc1	sám
bez	bez	k7c2	bez
mužského	mužský	k2eAgInSc2d1	mužský
doprovodu	doprovod	k1gInSc2	doprovod
a	a	k8xC	a
podle	podle	k7c2	podle
logiky	logika	k1gFnSc2	logika
saúdského	saúdský	k2eAgInSc2d1	saúdský
soudu	soud	k1gInSc2	soud
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
znásilnění	znásilnění	k1gNnSc4	znásilnění
sama	sám	k3xTgMnSc4	sám
přivodila	přivodit	k5eAaBmAgNnP	přivodit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
možnost	možnost	k1gFnSc1	možnost
"	"	kIx"	"
<g/>
vykoupení	vykoupení	k1gNnSc1	vykoupení
viny	vina	k1gFnSc2	vina
<g/>
"	"	kIx"	"
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
zabití	zabití	k1gNnSc4	zabití
nebo	nebo	k8xC	nebo
tělesné	tělesný	k2eAgNnSc4d1	tělesné
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Pachatel	pachatel	k1gMnSc1	pachatel
se	se	k3xPyFc4	se
vyhne	vyhnout	k5eAaPmIp3nS	vyhnout
trestu	trest	k1gInSc3	trest
pokud	pokud	k8xS	pokud
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
rodině	rodina	k1gFnSc3	rodina
oběti	oběť	k1gFnSc2	oběť
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
rodina	rodina	k1gFnSc1	rodina
sama	sám	k3xTgMnSc4	sám
určí	určit	k5eAaPmIp3nS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
ženy	žena	k1gFnSc2	žena
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
poloviční	poloviční	k2eAgInSc4d1	poloviční
obnos	obnos	k1gInSc4	obnos
oproti	oproti	k7c3	oproti
usmrcení	usmrcení	k1gNnSc3	usmrcení
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
vraždy	vražda	k1gFnSc2	vražda
ze	z	k7c2	z
cti	čest	k1gFnSc2	čest
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
"	"	kIx"	"
<g/>
zahanbily	zahanbit	k5eAaPmAgFnP	zahanbit
<g/>
"	"	kIx"	"
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
<g/>
Saúdskoarabský	saúdskoarabský	k2eAgInSc1d1	saúdskoarabský
blogger	blogger	k1gInSc1	blogger
Raíf	Raíf	k1gInSc1	Raíf
Badáví	Badáev	k1gFnPc2	Badáev
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
za	za	k7c4	za
"	"	kIx"	"
<g/>
urážku	urážka	k1gFnSc4	urážka
islámu	islám	k1gInSc2	islám
<g/>
"	"	kIx"	"
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
10	[number]	k4	10
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
ranám	rána	k1gFnPc3	rána
bičem	bič	k1gInSc7	bič
a	a	k8xC	a
pokutě	pokuta	k1gFnSc6	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
saúdských	saúdský	k2eAgInPc2d1	saúdský
rialů	rial	k1gInPc2	rial
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ali	Ali	k?	Ali
Muhammad	Muhammad	k1gInSc1	Muhammad
al-Nimr	al-Nimr	k1gInSc1	al-Nimr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
nezletilý	zletilý	k2eNgInSc1d1	nezletilý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zadržen	zadržet	k5eAaPmNgMnS	zadržet
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
protivládní	protivládní	k2eAgFnSc6d1	protivládní
demonstraci	demonstrace	k1gFnSc6	demonstrace
během	během	k7c2	během
Arabského	arabský	k2eAgNnSc2d1	arabské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
stětím	stětit	k5eAaPmIp1nS	stětit
a	a	k8xC	a
k	k	k7c3	k
následnému	následný	k2eAgNnSc3d1	následné
ukřižování	ukřižování	k1gNnSc3	ukřižování
<g/>
.	.	kIx.	.
<g/>
Saúdskoarabský	saúdskoarabský	k2eAgMnSc1d1	saúdskoarabský
novinář	novinář	k1gMnSc1	novinář
Džamál	Džamála	k1gFnPc2	Džamála
Chášukdží	Chášukdž	k1gFnPc2	Chášukdž
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
exilu	exil	k1gInSc6	exil
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
naposledy	naposledy	k6eAd1	naposledy
spatřen	spatřit	k5eAaPmNgInS	spatřit
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
když	když	k8xS	když
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
saúdskoarabský	saúdskoarabský	k2eAgInSc4d1	saúdskoarabský
konzulát	konzulát	k1gInSc4	konzulát
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
byl	být	k5eAaImAgInS	být
Chášukdží	Chášukdží	k1gNnPc4	Chášukdží
na	na	k7c6	na
saúdském	saúdský	k2eAgInSc6d1	saúdský
konzulátu	konzulát	k1gInSc6	konzulát
při	při	k7c6	při
"	"	kIx"	"
<g/>
výslechu	výslech	k1gInSc6	výslech
<g/>
"	"	kIx"	"
členy	člen	k1gInPc7	člen
královské	královský	k2eAgFnSc2d1	královská
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
umučen	umučen	k2eAgInSc1d1	umučen
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
rozčtvrceno	rozčtvrtit	k5eAaPmNgNnS	rozčtvrtit
a	a	k8xC	a
rozpuštěno	rozpustit	k5eAaPmNgNnS	rozpustit
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
<g/>
.	.	kIx.	.
<g/>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
principů	princip	k1gInPc2	princip
jako	jako	k8xS	jako
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
také	také	k9	také
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
trestá	trestat	k5eAaImIp3nS	trestat
rouhání	rouhání	k1gNnSc4	rouhání
a	a	k8xC	a
homosexualitu	homosexualita	k1gFnSc4	homosexualita
trestem	trest	k1gInSc7	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
cizoložství	cizoložství	k1gNnSc1	cizoložství
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
trestá	trestat	k5eAaImIp3nS	trestat
ukamenováním	ukamenování	k1gNnSc7	ukamenování
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
cizoložství	cizoložství	k1gNnSc2	cizoložství
mimo	mimo	k7c4	mimo
manželství	manželství	k1gNnSc4	manželství
se	se	k3xPyFc4	se
trestá	trestat	k5eAaImIp3nS	trestat
100	[number]	k4	100
ranami	rána	k1gFnPc7	rána
bičem	bič	k1gInSc7	bič
<g/>
,	,	kIx,	,
a	a	k8xC	a
krádež	krádež	k1gFnSc1	krádež
utětím	utětí	k1gNnSc7	utětí
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
léta	léto	k1gNnSc2	léto
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
předsedá	předsedat	k5eAaImIp3nS	předsedat
Radě	rada	k1gFnSc3	rada
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
otroků	otrok	k1gMnPc2	otrok
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
Otrokářství	otrokářství	k1gNnSc2	otrokářství
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
CIA	CIA	kA	CIA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
26	[number]	k4	26
132	[number]	k4	132
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5	[number]	k4	5
576	[number]	k4	576
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
nemá	mít	k5eNaImIp3nS	mít
saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
státní	státní	k2eAgFnSc4d1	státní
příslušnost	příslušnost	k1gFnSc4	příslušnost
(	(	kIx(	(
<g/>
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgNnSc1d1	malé
procento	procento	k1gNnSc1	procento
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
činí	činit	k5eAaImIp3nS	činit
1,5	[number]	k4	1,5
%	%	kIx~	%
a	a	k8xC	a
úhrnná	úhrnný	k2eAgFnSc1d1	úhrnná
plodnost	plodnost	k1gFnSc1	plodnost
je	být	k5eAaImIp3nS	být
2,31	[number]	k4	2,31
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
74	[number]	k4	74
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
72	[number]	k4	72
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
76	[number]	k4	76
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mediánový	mediánový	k2eAgInSc1d1	mediánový
věk	věk	k1gInSc1	věk
činí	činit	k5eAaImIp3nS	činit
26,4	[number]	k4	26,4
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
29,4	[number]	k4	29,4
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
67,6	[number]	k4	67,6
%	%	kIx~	%
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
starší	starý	k2eAgFnPc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
žila	žít	k5eAaImAgFnS	žít
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnSc1	obyvatel
nomádským	nomádský	k2eAgInSc7d1	nomádský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
žilo	žít	k5eAaImAgNnS	žít
již	již	k6eAd1	již
82	[number]	k4	82
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
růst	růst	k1gInSc1	růst
urbanizace	urbanizace	k1gFnSc2	urbanizace
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
2,2	[number]	k4	2,2
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
z	z	k7c2	z
necelého	celý	k2eNgInSc2d1	necelý
7,5	[number]	k4	7,5
milionu	milion	k4xCgInSc2	milion
pracujících	pracující	k2eAgMnPc2d1	pracující
jsou	být	k5eAaImIp3nP	být
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Etnicky	etnicky	k6eAd1	etnicky
tvoří	tvořit	k5eAaImIp3nS	tvořit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
jiného	jiný	k2eAgInSc2d1	jiný
afroasijského	afroasijský	k2eAgInSc2d1	afroasijský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
skupiny	skupina	k1gFnPc4	skupina
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
patří	patřit	k5eAaImIp3nP	patřit
Indové	Ind	k1gMnPc1	Ind
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
jich	on	k3xPp3gNnPc2	on
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pákistánci	Pákistánec	k1gMnPc1	Pákistánec
(	(	kIx(	(
<g/>
900	[number]	k4	900
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Egypťané	Egypťan	k1gMnPc1	Egypťan
(	(	kIx(	(
<g/>
900	[number]	k4	900
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jemenci	Jemence	k1gFnSc4	Jemence
(	(	kIx(	(
<g/>
800	[number]	k4	800
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Filipíňané	Filipíňan	k1gMnPc1	Filipíňan
(	(	kIx(	(
<g/>
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bangladéšané	Bangladéšaná	k1gFnSc2	Bangladéšaná
(	(	kIx(	(
<g/>
400	[number]	k4	400
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalecké	přistěhovalecký	k2eAgFnPc1d1	přistěhovalecká
menšiny	menšina	k1gFnPc1	menšina
hovoří	hovořit	k5eAaImIp3nP	hovořit
svými	svůj	k3xOyFgMnPc7	svůj
vlastními	vlastní	k2eAgMnPc7d1	vlastní
jazyky	jazyk	k1gMnPc7	jazyk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
jsou	být	k5eAaImIp3nP	být
tagalog	tagalog	k1gMnSc1	tagalog
(	(	kIx(	(
<g/>
700	[number]	k4	700
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
urdština	urdština	k1gFnSc1	urdština
(	(	kIx(	(
<g/>
380	[number]	k4	380
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
žádnou	žádný	k3yNgFnSc4	žádný
humanitární	humanitární	k2eAgFnSc4d1	humanitární
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
největších	veliký	k2eAgMnPc2d3	veliký
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
95	[number]	k4	95
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
lékům	lék	k1gInPc3	lék
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
imunizace	imunizace	k1gFnSc2	imunizace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
také	také	k9	také
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgInPc1d1	sociální
problémy	problém	k1gInPc1	problém
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Ženy	žena	k1gFnPc1	žena
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
království	království	k1gNnSc6	království
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
silná	silný	k2eAgFnSc1d1	silná
diskriminace	diskriminace	k1gFnSc1	diskriminace
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgNnSc4d2	nižší
postavení	postavení	k1gNnSc4	postavení
než	než	k8xS	než
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
rozhodující	rozhodující	k2eAgNnSc4d1	rozhodující
slovo	slovo	k1gNnSc4	slovo
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
a	a	k8xC	a
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
mohou	moct	k5eAaImIp3nP	moct
beztrestně	beztrestně	k6eAd1	beztrestně
páchat	páchat	k5eAaImF	páchat
domácí	domácí	k2eAgNnSc4d1	domácí
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
také	také	k9	také
nemohou	moct	k5eNaImIp3nP	moct
např.	např.	kA	např.
řídit	řídit	k5eAaImF	řídit
auta	auto	k1gNnPc4	auto
nebo	nebo	k8xC	nebo
cestovat	cestovat	k5eAaImF	cestovat
bez	bez	k7c2	bez
mužského	mužský	k2eAgMnSc2d1	mužský
příbuzného	příbuzný	k1gMnSc2	příbuzný
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
policií	policie	k1gFnSc7	policie
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
přikazován	přikazován	k2eAgInSc1d1	přikazován
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
způsob	způsob	k1gInSc1	způsob
oblékání	oblékání	k1gNnSc2	oblékání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
saúdského	saúdský	k2eAgNnSc2d1	Saúdské
práva	právo	k1gNnSc2	právo
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
každá	každý	k3xTgFnSc1	každý
dospělá	dospělý	k2eAgFnSc1d1	dospělá
žena	žena	k1gFnSc1	žena
mužského	mužský	k2eAgMnSc2d1	mužský
příbuzného	příbuzný	k1gMnSc2	příbuzný
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
"	"	kIx"	"
<g/>
strážce	strážce	k1gMnSc2	strážce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ženě	žena	k1gFnSc3	žena
uděluje	udělovat	k5eAaImIp3nS	udělovat
eventuální	eventuální	k2eAgNnSc4d1	eventuální
povolení	povolení	k1gNnSc4	povolení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
cestovat	cestovat	k5eAaImF	cestovat
<g/>
,	,	kIx,	,
studovat	studovat	k5eAaImF	studovat
nebo	nebo	k8xC	nebo
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Diskriminace	diskriminace	k1gFnSc1	diskriminace
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
království	království	k1gNnSc6	království
je	být	k5eAaImIp3nS	být
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgFnPc2d3	nejhorší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
za	za	k7c2	za
Abdalláhovy	Abdalláhův	k2eAgFnSc2d1	Abdalláhův
vlády	vláda	k1gFnSc2	vláda
ale	ale	k8xC	ale
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
určitým	určitý	k2eAgNnPc3d1	určité
zlepšením	zlepšení	k1gNnPc3	zlepšení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
počet	počet	k1gInSc4	počet
ženských	ženský	k2eAgFnPc2d1	ženská
pracovnic	pracovnice	k1gFnPc2	pracovnice
ze	z	k7c2	z
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
a	a	k8xC	a
na	na	k7c4	na
mezery	mezera	k1gFnPc4	mezera
v	v	k7c6	v
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
obětí	oběť	k1gFnPc2	oběť
zneužíváno	zneužívat	k5eAaImNgNnS	zneužívat
a	a	k8xC	a
mučeno	mučen	k2eAgNnSc1d1	mučeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Příbuzenské	příbuzenský	k2eAgInPc4d1	příbuzenský
sňatky	sňatek	k1gInPc4	sňatek
<g/>
,	,	kIx,	,
inbreeding	inbreeding	k1gInSc1	inbreeding
====	====	k?	====
</s>
</p>
<p>
<s>
Míra	Míra	k1gFnSc1	Míra
manželství	manželství	k1gNnSc2	manželství
mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
nebo	nebo	k8xC	nebo
druhými	druhý	k4xOgFnPc7	druhý
bratranci	bratranec	k1gMnPc7	bratranec
<g/>
/	/	kIx~	/
<g/>
sestřenicemi	sestřenice	k1gFnPc7	sestřenice
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
"	"	kIx"	"
<g/>
zajištění	zajištění	k1gNnSc3	zajištění
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c4	mezi
kmeny	kmen	k1gInPc4	kmen
a	a	k8xC	a
zachování	zachování	k1gNnPc4	zachování
rodinného	rodinný	k2eAgNnSc2d1	rodinné
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
citována	citován	k2eAgFnSc1d1	citována
jako	jako	k8xS	jako
faktor	faktor	k1gInSc1	faktor
při	při	k7c6	při
vyšším	vysoký	k2eAgInSc6d2	vyšší
výskytu	výskyt	k1gInSc6	výskyt
závažných	závažný	k2eAgNnPc2d1	závažné
genetických	genetický	k2eAgNnPc2d1	genetické
onemocnění	onemocnění	k1gNnPc2	onemocnění
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
cystická	cystický	k2eAgFnSc1d1	cystická
fibróza	fibróza	k1gFnSc1	fibróza
nebo	nebo	k8xC	nebo
talasémie	talasémie	k1gFnPc1	talasémie
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
diabetes	diabetes	k1gInSc1	diabetes
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
asi	asi	k9	asi
32	[number]	k4	32
<g/>
%	%	kIx~	%
dospělých	dospělý	k2eAgMnPc2d1	dospělý
Saúdů	Saúd	k1gMnPc2	Saúd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hypertenze	hypertenze	k1gFnSc1	hypertenze
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
33	[number]	k4	33
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srpkovitá	srpkovitý	k2eAgFnSc1d1	srpkovitá
anémie	anémie	k1gFnSc1	anémie
<g/>
,	,	kIx,	,
spinální	spinální	k2eAgFnSc1d1	spinální
svalová	svalový	k2eAgFnSc1d1	svalová
atrofie	atrofie	k1gFnSc1	atrofie
<g/>
,	,	kIx,	,
hluchota	hluchota	k1gFnSc1	hluchota
a	a	k8xC	a
němota	němota	k1gFnSc1	němota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zneužívání	zneužívání	k1gNnSc1	zneužívání
dětí	dítě	k1gFnPc2	dítě
====	====	k?	====
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
provedl	provést	k5eAaPmAgMnS	provést
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Nura	Nura	k1gMnSc1	Nura
Al-Suwaiyan	Al-Suwaiyan	k1gMnSc1	Al-Suwaiyan
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
rodinného	rodinný	k2eAgInSc2d1	rodinný
programu	program	k1gInSc2	program
pod	pod	k7c7	pod
institutem	institut	k1gInSc7	institut
NGHA	NGHA	kA	NGHA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
zneužíváno	zneužíván	k2eAgNnSc1d1	zneužíváno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
organizace	organizace	k1gFnSc2	organizace
National	National	k1gFnSc2	National
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
Human	Humana	k1gFnPc2	Humana
Rights	Rightsa	k1gFnPc2	Rightsa
téměř	téměř	k6eAd1	téměř
45	[number]	k4	45
<g/>
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
čelí	čelit	k5eAaImIp3nS	čelit
nějakému	nějaký	k3yIgInSc3	nějaký
druhu	druh	k1gInSc3	druh
zneužívání	zneužívání	k1gNnSc2	zneužívání
a	a	k8xC	a
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vláda	vláda	k1gFnSc1	vláda
schválila	schválit	k5eAaPmAgFnS	schválit
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
domácí	domácí	k2eAgMnSc1d1	domácí
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
dětech	dítě	k1gFnPc6	dítě
trestá	trestat	k5eAaImIp3nS	trestat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Chudoba	Chudoba	k1gMnSc1	Chudoba
====	====	k?	====
</s>
</p>
<p>
<s>
Odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
Saúdů	Saúd	k1gMnPc2	Saúd
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
12,7	[number]	k4	12,7
%	%	kIx~	%
do	do	k7c2	do
25	[number]	k4	25
%	%	kIx~	%
Tiskové	tiskový	k2eAgFnPc1d1	tisková
zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
soukromé	soukromý	k2eAgInPc1d1	soukromý
odhady	odhad	k1gInPc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
rodilých	rodilý	k2eAgMnPc2d1	rodilý
Saúdů	Saúd	k1gMnPc2	Saúd
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
asi	asi	k9	asi
530	[number]	k4	530
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
"	"	kIx"	"
<g/>
-	-	kIx~	-
asi	asi	k9	asi
17	[number]	k4	17
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc4	forbes
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
osobní	osobní	k2eAgNnSc1d1	osobní
jmění	jmění	k1gNnSc1	jmění
zemřelého	zemřelý	k1gMnSc2	zemřelý
krále	král	k1gMnSc2	král
Abdalláha	Abdalláh	k1gMnSc2	Abdalláh
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
18	[number]	k4	18
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
založil	založit	k5eAaPmAgMnS	založit
Mohamed	Mohamed	k1gMnSc1	Mohamed
islám	islám	k1gInSc4	islám
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejteokratičtějších	teokratický	k2eAgFnPc2d3	teokratický
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
nejposvátnější	posvátný	k2eAgNnPc1d3	nejposvátnější
místa	místo	k1gNnPc1	místo
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
Mekka	Mekka	k1gFnSc1	Mekka
a	a	k8xC	a
Medína	Medína	k1gFnSc1	Medína
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
pilířů	pilíř	k1gInPc2	pilíř
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
Hadždž	Hadždž	k1gFnSc1	Hadždž
<g/>
,	,	kIx,	,
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
každému	každý	k3xTgMnSc3	každý
muslimovi	muslim	k1gMnSc3	muslim
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
život	život	k1gInSc4	život
provést	provést	k5eAaPmF	provést
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
97	[number]	k4	97
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
sunnité	sunnita	k1gMnPc1	sunnita
a	a	k8xC	a
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
ité	ité	k?	ité
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Dění	dění	k1gNnSc1	dění
v	v	k7c6	v
království	království	k1gNnSc6	království
oficiálně	oficiálně	k6eAd1	oficiálně
podléhá	podléhat	k5eAaImIp3nS	podléhat
striktní	striktní	k2eAgInSc1d1	striktní
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
wahhábistické	wahhábistický	k2eAgFnSc3d1	wahhábistická
interpretaci	interpretace	k1gFnSc3	interpretace
šaríy	šaría	k1gFnSc2	šaría
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
vláda	vláda	k1gFnSc1	vláda
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
organizace	organizace	k1gFnPc1	organizace
působící	působící	k2eAgFnPc1d1	působící
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vynakládají	vynakládat	k5eAaImIp3nP	vynakládat
mnoho	mnoho	k4c4	mnoho
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
zakládání	zakládání	k1gNnSc2	zakládání
mešit	mešita	k1gFnPc2	mešita
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
škol	škola	k1gFnPc2	škola
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
poté	poté	k6eAd1	poté
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
wahhábismu	wahhábismus	k1gInSc2	wahhábismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
financovala	financovat	k5eAaBmAgFnS	financovat
stavbu	stavba	k1gFnSc4	stavba
1,359	[number]	k4	1,359
mešit	mešita	k1gFnPc2	mešita
<g/>
,	,	kIx,	,
210	[number]	k4	210
Islámských	islámský	k2eAgNnPc2d1	islámské
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
202	[number]	k4	202
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
2,000	[number]	k4	2,000
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
muslimskou	muslimský	k2eAgFnSc4d1	muslimská
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
kvůli	kvůli	k7c3	kvůli
přílivu	příliv	k1gInSc3	příliv
západních	západní	k2eAgFnPc2d1	západní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
modernizaci	modernizace	k1gFnSc4	modernizace
země	zem	k1gFnSc2	zem
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
zkorumpovanosti	zkorumpovanost	k1gFnPc1	zkorumpovanost
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
vytvářet	vytvářet	k5eAaImF	vytvářet
militantní	militantní	k2eAgFnPc4d1	militantní
islámské	islámský	k2eAgFnPc4d1	islámská
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
bojují	bojovat	k5eAaImIp3nP	bojovat
za	za	k7c4	za
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
životu	život	k1gInSc3	život
podle	podle	k7c2	podle
fundamentálního	fundamentální	k2eAgInSc2d1	fundamentální
výkladu	výklad	k1gInSc2	výklad
Koránu	korán	k1gInSc2	korán
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
království	království	k1gNnSc6	království
platí	platit	k5eAaImIp3nS	platit
zákaz	zákaz	k1gInSc1	zákaz
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vyznávání	vyznávání	k1gNnSc2	vyznávání
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
než	než	k8xS	než
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
omezovány	omezován	k2eAgFnPc1d1	omezována
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
než	než	k8xS	než
sunnitské	sunnitský	k2eAgFnPc1d1	sunnitská
formy	forma	k1gFnPc1	forma
jako	jako	k8xS	jako
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itský	itský	k2eAgInSc1d1	itský
islám	islám	k1gInSc1	islám
a	a	k8xC	a
súfismus	súfismus	k1gInSc1	súfismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
vyučování	vyučování	k1gNnSc1	vyučování
nenávistných	návistný	k2eNgInPc2d1	nenávistný
názorů	názor	k1gInPc2	názor
proti	proti	k7c3	proti
neislámským	islámský	k2eNgFnPc3d1	neislámská
vírám	víra	k1gFnPc3	víra
a	a	k8xC	a
muslimským	muslimský	k2eAgFnPc3d1	muslimská
menšinám	menšina	k1gFnPc3	menšina
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
Abdalláhova	Abdalláhova	k1gFnSc1	Abdalláhova
vláda	vláda	k1gFnSc1	vláda
snaží	snažit	k5eAaImIp3nS	snažit
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
nemohou	moct	k5eNaImIp3nP	moct
hlásit	hlásit	k5eAaImF	hlásit
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
nejsou	být	k5eNaImIp3nP	být
při	při	k7c6	při
sčítáních	sčítání	k1gNnPc6	sčítání
lidu	lid	k1gInSc2	lid
započítávána	započítáván	k2eAgNnPc1d1	započítáváno
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
ale	ale	k8xC	ale
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
hinduismus	hinduismus	k1gInSc4	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
nemuslimských	muslimský	k2eNgMnPc2d1	nemuslimský
věřících	věřící	k1gMnPc2	věřící
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
500	[number]	k4	500
000	[number]	k4	000
až	až	k9	až
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činil	činit	k5eAaImAgInS	činit
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
622	[number]	k4	622
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
23	[number]	k4	23
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hodnota	hodnota	k1gFnSc1	hodnota
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
HDP	HDP	kA	HDP
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
4	[number]	k4	4
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
nezaměstnaných	nezaměstnaný	k1gMnPc6	nezaměstnaný
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
vlády	vláda	k1gFnSc2	vláda
<g/>
;	;	kIx,	;
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
míru	mír	k1gInSc2	mír
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
%	%	kIx~	%
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
tvořily	tvořit	k5eAaImAgFnP	tvořit
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ropnému	ropný	k2eAgInSc3d1	ropný
boomu	boom	k1gInSc3	boom
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
ekonomik	ekonomika	k1gFnPc2	ekonomika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
z	z	k7c2	z
vývozu	vývoz	k1gInSc2	vývoz
ropy	ropa	k1gFnSc2	ropa
čisté	čistý	k2eAgInPc4d1	čistý
zisky	zisk	k1gInPc4	zisk
194	[number]	k4	194
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
pětiletek	pětiletka	k1gFnPc2	pětiletka
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c6	na
rozšíření	rozšíření	k1gNnSc6	rozšíření
a	a	k8xC	a
posilování	posilování	k1gNnSc6	posilování
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
kromě	kromě	k7c2	kromě
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnění	vlastnění	k1gNnSc6	vlastnění
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
ovládá	ovládat	k5eAaImIp3nS	ovládat
ho	on	k3xPp3gInSc4	on
státní	státní	k2eAgFnSc1d1	státní
společnost	společnost	k1gFnSc1	společnost
Saudi	Saud	k1gMnPc1	Saud
Aramco	Aramco	k1gNnSc1	Aramco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
obrovských	obrovský	k2eAgFnPc2d1	obrovská
zásob	zásoba	k1gFnPc2	zásoba
ropy	ropa	k1gFnSc2	ropa
má	mít	k5eAaImIp3nS	mít
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
také	také	k9	také
4	[number]	k4	4
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc2d3	veliký
zásoby	zásoba	k1gFnSc2	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
vládní	vládní	k2eAgFnPc4d1	vládní
snahy	snaha	k1gFnPc4	snaha
patří	patřit	k5eAaImIp3nP	patřit
lákání	lákání	k1gNnSc3	lákání
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
diverzifikace	diverzifikace	k1gFnPc1	diverzifikace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
vytváření	vytváření	k1gNnSc2	vytváření
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
liberální	liberální	k2eAgFnSc1d1	liberální
<g/>
,	,	kIx,	,
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
i	i	k8xC	i
firmy	firma	k1gFnPc1	firma
platí	platit	k5eAaImIp3nP	platit
nízké	nízký	k2eAgFnPc1d1	nízká
daně	daň	k1gFnPc1	daň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
království	království	k1gNnPc4	království
členem	člen	k1gInSc7	člen
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
výdělečný	výdělečný	k2eAgInSc1d1	výdělečný
bankovní	bankovní	k2eAgInSc1d1	bankovní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
saúdský	saúdský	k2eAgInSc1d1	saúdský
rial	rial	k1gInSc1	rial
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
kurz	kurz	k1gInSc1	kurz
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
celkem	celkem	k6eAd1	celkem
3,75	[number]	k4	3,75
rialů	rial	k1gInPc2	rial
za	za	k7c4	za
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
<g/>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
cíli	cíl	k1gInPc7	cíl
saúdského	saúdský	k2eAgInSc2d1	saúdský
exportu	export	k1gInSc2	export
(	(	kIx(	(
<g/>
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
ropné	ropný	k2eAgInPc1d1	ropný
produkty	produkt	k1gInPc1	produkt
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
14,3	[number]	k4	14,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
13,1	[number]	k4	13,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
8,8	[number]	k4	8,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
8,3	[number]	k4	8,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Singapur	Singapur	k1gInSc1	Singapur
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
importuje	importovat	k5eAaBmIp3nS	importovat
nejčastěji	často	k6eAd3	často
různé	různý	k2eAgInPc4d1	různý
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
,	,	kIx,	,
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
textilie	textilie	k1gFnPc4	textilie
a	a	k8xC	a
vojenský	vojenský	k2eAgInSc4d1	vojenský
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
12,4	[number]	k4	12,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
11,1	[number]	k4	11,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
7,1	[number]	k4	7,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
(	(	kIx(	(
<g/>
6,9	[number]	k4	6,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
6,1	[number]	k4	6,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
4,7	[number]	k4	4,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
(	(	kIx(	(
<g/>
4,2	[number]	k4	4,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Království	království	k1gNnSc1	království
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
podíl	podíl	k1gInSc1	podíl
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1,7	[number]	k4	1,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
plodiny	plodina	k1gFnPc4	plodina
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
rajče	rajče	k1gNnSc1	rajče
<g/>
,	,	kIx,	,
meloun	meloun	k1gInSc1	meloun
<g/>
,	,	kIx,	,
datle	datle	k1gFnSc1	datle
a	a	k8xC	a
citrus	citrus	k1gInSc1	citrus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živočišných	živočišný	k2eAgInPc2d1	živočišný
produktů	produkt	k1gInPc2	produkt
jde	jít	k5eAaImIp3nS	jít
pak	pak	k6eAd1	pak
o	o	k7c4	o
jehněčí	jehněčí	k2eAgNnSc4d1	jehněčí
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kuřecí	kuřecí	k2eAgNnSc1d1	kuřecí
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
soběstačné	soběstačný	k2eAgNnSc1d1	soběstačné
v	v	k7c4	v
pěstování	pěstování	k1gNnSc4	pěstování
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
produkci	produkce	k1gFnSc4	produkce
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinu	většina	k1gFnSc4	většina
jídla	jídlo	k1gNnSc2	jídlo
dováží	dovázat	k5eAaPmIp3nP	dovázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Saúdské	saúdský	k2eAgFnSc3d1	Saúdská
ekonomice	ekonomika	k1gFnSc3	ekonomika
dominuje	dominovat	k5eAaImIp3nS	dominovat
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
navazující	navazující	k2eAgFnSc1d1	navazující
petrochemická	petrochemický	k2eAgFnSc1d1	petrochemická
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
odvětví	odvětví	k1gNnPc4	odvětví
patří	patřit	k5eAaImIp3nS	patřit
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
zpracovávající	zpracovávající	k2eAgFnSc2d1	zpracovávající
místní	místní	k2eAgFnSc2d1	místní
suroviny	surovina	k1gFnSc2	surovina
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
bauxit	bauxit	k1gInSc4	bauxit
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc4	zinek
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc4	vápenec
a	a	k8xC	a
mramor	mramor	k1gInSc4	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
i	i	k9	i
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevyváží	vyvážet	k5eNaImIp3nS	vyvážet
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
zásoby	zásoba	k1gFnPc1	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
jsou	být	k5eAaImIp3nP	být
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
na	na	k7c4	na
7,8	[number]	k4	7,8
bilionů	bilion	k4xCgInPc2	bilion
m3	m3	k4	m3
a	a	k8xC	a
roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
činí	činit	k5eAaImIp3nS	činit
84	[number]	k4	84
miliard	miliarda	k4xCgFnPc2	miliarda
m3	m3	k4	m3
<g/>
.	.	kIx.	.
<g/>
Království	království	k1gNnSc1	království
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
asi	asi	k9	asi
pětinu	pětina	k1gFnSc4	pětina
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
262,6	[number]	k4	262,6
miliard	miliarda	k4xCgFnPc2	miliarda
barelů	barel	k1gInPc2	barel
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
největším	veliký	k2eAgMnSc7d3	veliký
vývozcem	vývozce	k1gMnSc7	vývozce
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
zastává	zastávat	k5eAaImIp3nS	zastávat
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
roli	role	k1gFnSc4	role
v	v	k7c4	v
Organizaci	organizace	k1gFnSc4	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgMnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(	(
<g/>
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Saudi	Saud	k1gMnPc1	Saud
Aramco	Aramco	k1gMnSc1	Aramco
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
ropná	ropný	k2eAgFnSc1d1	ropná
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
66	[number]	k4	66
tisíc	tisíc	k4xCgInPc2	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Výdělky	výdělek	k1gInPc1	výdělek
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
45	[number]	k4	45
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
90	[number]	k4	90
%	%	kIx~	%
procent	procent	k1gInSc4	procent
vývozu	vývoz	k1gInSc2	vývoz
a	a	k8xC	a
75	[number]	k4	75
%	%	kIx~	%
státních	státní	k2eAgInPc2d1	státní
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Naleziště	naleziště	k1gNnSc1	naleziště
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služby	služba	k1gFnPc1	služba
a	a	k8xC	a
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pracují	pracovat	k5eAaImIp3nP	pracovat
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejsou	být	k5eNaImIp3nP	být
rozvinuty	rozvinout	k5eAaPmNgFnP	rozvinout
všechny	všechen	k3xTgFnPc1	všechen
služby	služba	k1gFnPc1	služba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
nebo	nebo	k8xC	nebo
kulturní	kulturní	k2eAgNnPc1d1	kulturní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
finanční	finanční	k2eAgFnPc1d1	finanční
a	a	k8xC	a
logistické	logistický	k2eAgFnPc1d1	logistická
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
naprosté	naprostý	k2eAgFnSc2d1	naprostá
většiny	většina	k1gFnSc2	většina
tvořen	tvořit	k5eAaImNgMnS	tvořit
návštěvami	návštěva	k1gFnPc7	návštěva
islámských	islámský	k2eAgFnPc2d1	islámská
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
Mekky	Mekka	k1gFnSc2	Mekka
a	a	k8xC	a
Medíny	Medína	k1gFnSc2	Medína
<g/>
.	.	kIx.	.
</s>
<s>
Hadždž	Hadždž	k1gFnSc4	Hadždž
<g/>
,	,	kIx,	,
muslimskou	muslimský	k2eAgFnSc4d1	muslimská
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
,	,	kIx,	,
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c6	o
přilákání	přilákání	k1gNnSc6	přilákání
turistů	turist	k1gMnPc2	turist
např.	např.	kA	např.
výstavbou	výstavba	k1gFnSc7	výstavba
pobřežních	pobřežní	k2eAgNnPc2d1	pobřežní
středisek	středisko	k1gNnPc2	středisko
<g/>
,	,	kIx,	,
turismus	turismus	k1gInSc1	turismus
nenáboženského	náboženský	k2eNgInSc2d1	nenáboženský
významu	význam	k1gInSc2	význam
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
saúdskoarabských	saúdskoarabský	k2eAgFnPc2d1	saúdskoarabská
pětiletek	pětiletka	k1gFnPc2	pětiletka
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
rozvoji	rozvoj	k1gInSc3	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
184	[number]	k4	184
000	[number]	k4	000
km	km	kA	km
zpevněných	zpevněný	k2eAgFnPc2d1	zpevněná
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
53	[number]	k4	53
800	[number]	k4	800
km	km	kA	km
je	být	k5eAaImIp3nS	být
asfaltových	asfaltový	k2eAgFnPc2d1	asfaltová
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
železnic	železnice	k1gFnPc2	železnice
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2	[number]	k4	2
400	[number]	k4	400
km	km	kA	km
a	a	k8xC	a
do	do	k7c2	do
železniční	železniční	k2eAgFnSc2d1	železniční
přepravy	přeprava	k1gFnSc2	přeprava
vláda	vláda	k1gFnSc1	vláda
investuje	investovat	k5eAaBmIp3nS	investovat
velké	velký	k2eAgInPc4d1	velký
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
215	[number]	k4	215
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
<g/>
,	,	kIx,	,
Džiddě	Džidd	k1gInSc6	Džidd
<g/>
,	,	kIx,	,
Dammámu	Dammám	k1gInSc6	Dammám
a	a	k8xC	a
Tabuku	Tabuk	k1gInSc6	Tabuk
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
74	[number]	k4	74
obchodních	obchodní	k2eAgFnPc2d1	obchodní
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
největšími	veliký	k2eAgInPc7d3	veliký
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Džidda	Džidda	k1gMnSc1	Džidda
<g/>
,	,	kIx,	,
Dammám	Damma	k1gFnPc3	Damma
<g/>
,	,	kIx,	,
Jubail	Jubaila	k1gFnPc2	Jubaila
a	a	k8xC	a
Janbu	Janb	k1gInSc2	Janb
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
prochází	procházet	k5eAaImIp3nS	procházet
95	[number]	k4	95
%	%	kIx~	%
námořní	námořní	k2eAgFnSc2d1	námořní
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
ropovodů	ropovod	k1gInPc2	ropovod
a	a	k8xC	a
plynovodů	plynovod	k1gInPc2	plynovod
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnPc4	délka
asi	asi	k9	asi
9	[number]	k4	9
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
<g/>
Počet	počet	k1gInSc1	počet
pevných	pevný	k2eAgFnPc2d1	pevná
linek	linka	k1gFnPc2	linka
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
přesahoval	přesahovat	k5eAaImAgInS	přesahovat
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
a	a	k8xC	a
počet	počet	k1gInSc1	počet
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
51	[number]	k4	51
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
uživatelů	uživatel	k1gMnPc2	uživatel
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
cenzurován	cenzurován	k2eAgInSc1d1	cenzurován
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
blokuje	blokovat	k5eAaImIp3nS	blokovat
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
stránkám	stránka	k1gFnPc3	stránka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Kultura	kultura	k1gFnSc1	kultura
království	království	k1gNnSc2	království
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
a	a	k8xC	a
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
islámem	islám	k1gInSc7	islám
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
wahhábistickým	wahhábistický	k2eAgInSc7d1	wahhábistický
výkladem	výklad	k1gInSc7	výklad
Koránu	korán	k1gInSc2	korán
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
náleží	náležet	k5eAaImIp3nS	náležet
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
poezii	poezie	k1gFnSc3	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgInPc4d3	nejčastější
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
patří	patřit	k5eAaImIp3nP	patřit
rebab	rebab	k1gInSc4	rebab
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnSc1	poezie
svou	svůj	k3xOyFgFnSc7	svůj
tradicí	tradice	k1gFnSc7	tradice
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
předislámské	předislámský	k2eAgFnSc2d1	předislámská
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
často	často	k6eAd1	často
používaná	používaný	k2eAgFnSc1d1	používaná
forma	forma	k1gFnSc1	forma
básní	báseň	k1gFnPc2	báseň
kasída	kasída	k1gFnSc1	kasída
(	(	kIx(	(
<g/>
óda	óda	k1gFnSc1	óda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
hrála	hrát	k5eAaImAgFnS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
kmenovém	kmenový	k2eAgNnSc6d1	kmenové
uspořádání	uspořádání	k1gNnSc6	uspořádání
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
nosí	nosit	k5eAaImIp3nS	nosit
tradiční	tradiční	k2eAgNnSc1d1	tradiční
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
představuje	představovat	k5eAaImIp3nS	představovat
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
oděv	oděv	k1gInSc4	oděv
a	a	k8xC	a
pokrývku	pokrývka	k1gFnSc4	pokrývka
hlavy	hlava	k1gFnSc2	hlava
kefíju	kefíju	k5eAaPmIp1nS	kefíju
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
musí	muset	k5eAaImIp3nS	muset
chodit	chodit	k5eAaImF	chodit
zahalené	zahalený	k2eAgNnSc1d1	zahalené
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
hidžáb	hidžáb	k1gMnSc1	hidžáb
a	a	k8xC	a
niqáb	niqáb	k1gMnSc1	niqáb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
islámský	islámský	k2eAgInSc1d1	islámský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
lunární	lunární	k2eAgInSc1d1	lunární
a	a	k8xC	a
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
dva	dva	k4xCgInPc1	dva
národní	národní	k2eAgInPc1d1	národní
náboženské	náboženský	k2eAgInPc1d1	náboženský
svátky	svátek	k1gInPc1	svátek
<g/>
,	,	kIx,	,
íd	íd	k?	íd
al-fitr	alitr	k1gInSc1	al-fitr
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
Ramadánu	ramadán	k1gInSc2	ramadán
<g/>
)	)	kIx)	)
a	a	k8xC	a
íd	íd	k?	íd
al-adhá	aldhat	k5eAaPmIp3nS	al-adhat
(	(	kIx(	(
<g/>
Svátek	svátek	k1gInSc4	svátek
oběti	oběť	k1gFnSc2	oběť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
podle	podle	k7c2	podle
západního	západní	k2eAgInSc2d1	západní
kalendáře	kalendář	k1gInSc2	kalendář
slaví	slavit	k5eAaImIp3nS	slavit
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
výročí	výročí	k1gNnSc2	výročí
sjednocení	sjednocení	k1gNnSc2	sjednocení
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
svátků	svátek	k1gInPc2	svátek
se	se	k3xPyFc4	se
žádné	žádný	k3yNgNnSc1	žádný
oficiálně	oficiálně	k6eAd1	oficiálně
neslaví	slavit	k5eNaImIp3nS	slavit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
islámských	islámský	k2eAgFnPc2d1	islámská
památek	památka	k1gFnPc2	památka
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgNnPc2	dva
nejsvětějších	nejsvětější	k2eAgNnPc2d1	nejsvětější
míst	místo	k1gNnPc2	místo
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
Mekky	Mekka	k1gFnSc2	Mekka
a	a	k8xC	a
Medíny	Medína	k1gFnSc2	Medína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Velká	velký	k2eAgFnSc1d1	velká
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
Mekka	Mekka	k1gFnSc1	Mekka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Prorokova	prorokův	k2eAgFnSc1d1	Prorokova
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
Medína	Medína	k1gFnSc1	Medína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
Velké	velký	k2eAgFnSc2d1	velká
mešity	mešita	k1gFnSc2	mešita
leží	ležet	k5eAaImIp3nS	ležet
Ka	Ka	k1gFnSc1	Ka
<g/>
'	'	kIx"	'
<g/>
ba	ba	k9	ba
a	a	k8xC	a
v	v	k7c6	v
Prorokově	prorokův	k2eAgFnSc6d1	Prorokova
mešitě	mešita	k1gFnSc6	mešita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hrob	hrob	k1gInSc1	hrob
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
památky	památka	k1gFnPc1	památka
na	na	k7c6	na
území	území	k1gNnSc6	území
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
jsou	být	k5eAaImIp3nP	být
zapsané	zapsaný	k2eAgInPc1d1	zapsaný
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
předislámská	předislámský	k2eAgFnSc1d1	předislámská
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
Al-Hijr	Al-Hijra	k1gFnPc2	Al-Hijra
a	a	k8xC	a
původní	původní	k2eAgNnSc4d1	původní
sídlo	sídlo	k1gNnSc4	sídlo
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
Saúdů	Saúd	k1gMnPc2	Saúd
At-Turaif	At-Turaif	k1gInSc1	At-Turaif
v	v	k7c6	v
Daríje	Daríje	k1gFnSc1	Daríje
<g/>
.	.	kIx.	.
<g/>
Velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
potápění	potápění	k1gNnSc1	potápění
s	s	k7c7	s
přístrojem	přístroj	k1gInSc7	přístroj
<g/>
,	,	kIx,	,
windsurfing	windsurfing	k1gInSc1	windsurfing
a	a	k8xC	a
závody	závod	k1gInPc1	závod
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
praktikováno	praktikován	k2eAgNnSc1d1	praktikováno
sokolnictví	sokolnictví	k1gNnSc1	sokolnictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podléhá	podléhat	k5eAaImIp3nS	podléhat
několika	několik	k4yIc7	několik
omezením	omezení	k1gNnSc7	omezení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
sokolů	sokol	k1gMnPc2	sokol
je	být	k5eAaImIp3nS	být
chráněných	chráněný	k2eAgFnPc2d1	chráněná
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
asijských	asijský	k2eAgFnPc2d1	asijská
her	hra	k1gFnPc2	hra
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
také	také	k6eAd1	také
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
účastnila	účastnit	k5eAaImAgFnS	účastnit
saúdskoarabská	saúdskoarabský	k2eAgFnSc1d1	saúdskoarabská
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Saúdskoarabská	saúdskoarabský	k2eAgFnSc1d1	saúdskoarabská
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
okolním	okolní	k2eAgInPc3d1	okolní
státům	stát	k1gInPc3	stát
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
silně	silně	k6eAd1	silně
ji	on	k3xPp3gFnSc4	on
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
turecká	turecký	k2eAgNnPc1d1	turecké
<g/>
,	,	kIx,	,
perská	perský	k2eAgNnPc1d1	perské
a	a	k8xC	a
africká	africký	k2eAgFnSc1d1	africká
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
konzumace	konzumace	k1gFnSc1	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
vepřového	vepřový	k2eAgNnSc2d1	vepřové
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
jídla	jídlo	k1gNnPc4	jídlo
patří	patřit	k5eAaImIp3nP	patřit
khúzí	khúzit	k5eAaPmIp3nS	khúzit
(	(	kIx(	(
<g/>
vycpané	vycpaný	k2eAgNnSc1d1	vycpané
jehněčí	jehněčí	k2eAgNnSc1d1	jehněčí
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švarma	švarma	k1gFnSc1	švarma
(	(	kIx(	(
<g/>
jídlo	jídlo	k1gNnSc1	jídlo
podobné	podobný	k2eAgNnSc1d1	podobné
kebabu	kebab	k1gInSc6	kebab
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kabsa	kabsa	k1gFnSc1	kabsa
(	(	kIx(	(
<g/>
rýže	rýže	k1gFnSc1	rýže
s	s	k7c7	s
kuřecím	kuřecí	k2eAgNnSc7d1	kuřecí
masem	maso	k1gNnSc7	maso
<g/>
)	)	kIx)	)
a	a	k8xC	a
falafel	falafet	k5eAaBmAgMnS	falafet
(	(	kIx(	(
<g/>
smažené	smažený	k2eAgFnPc4d1	smažená
kuličky	kulička	k1gFnPc4	kulička
z	z	k7c2	z
bobů	bob	k1gInPc2	bob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Saúdskoarabská	saúdskoarabský	k2eAgNnPc1d1	Saúdskoarabské
jídla	jídlo	k1gNnPc1	jídlo
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
silně	silně	k6eAd1	silně
kořeněná	kořeněný	k2eAgNnPc4d1	kořeněné
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
je	být	k5eAaImIp3nS	být
podáván	podáván	k2eAgInSc4d1	podáván
chléb	chléb	k1gInSc4	chléb
a	a	k8xC	a
čerstvé	čerstvý	k2eAgNnSc4d1	čerstvé
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejčastější	častý	k2eAgFnPc1d3	nejčastější
jsou	být	k5eAaImIp3nP	být
datle	datle	k1gFnPc1	datle
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
káva	káva	k1gFnSc1	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
===	===	k?	===
</s>
</p>
<p>
<s>
Saúdský	saúdský	k2eAgInSc1d1	saúdský
školní	školní	k2eAgInSc1d1	školní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
občany	občan	k1gMnPc4	občan
bezplatný	bezplatný	k2eAgMnSc1d1	bezplatný
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
99	[number]	k4	99
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
odpovídající	odpovídající	k2eAgFnSc6d1	odpovídající
věkové	věkový	k2eAgFnSc6d1	věková
kategorii	kategorie	k1gFnSc6	kategorie
studuje	studovat	k5eAaImIp3nS	studovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
88	[number]	k4	88
%	%	kIx~	%
jich	on	k3xPp3gInPc2	on
poté	poté	k6eAd1	poté
začne	začít	k5eAaPmIp3nS	začít
studovat	studovat	k5eAaImF	studovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
celkem	celkem	k6eAd1	celkem
50	[number]	k4	50
%	%	kIx~	%
studentů	student	k1gMnPc2	student
a	a	k8xC	a
60	[number]	k4	60
%	%	kIx~	%
studentek	studentka	k1gFnPc2	studentka
po	po	k7c6	po
vystudování	vystudování	k1gNnSc6	vystudování
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Učební	učební	k2eAgFnPc1d1	učební
osnovy	osnova	k1gFnPc1	osnova
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
ovládány	ovládán	k2eAgInPc1d1	ovládán
islámskými	islámský	k2eAgInPc7d1	islámský
duchovními	duchovní	k2eAgInPc7d1	duchovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Abdalláhova	Abdalláhův	k2eAgFnSc1d1	Abdalláhův
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
tento	tento	k3xDgInSc4	tento
vliv	vliv	k1gInSc4	vliv
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
studované	studovaný	k2eAgInPc4d1	studovaný
obory	obor	k1gInPc4	obor
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
patří	patřit	k5eAaImIp3nS	patřit
technické	technický	k2eAgInPc4d1	technický
obory	obor	k1gInPc4	obor
a	a	k8xC	a
teologie	teologie	k1gFnPc4	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
79	[number]	k4	79
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
gramotných	gramotný	k2eAgMnPc2d1	gramotný
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
gramotných	gramotný	k2eAgNnPc2d1	gramotné
méně	málo	k6eAd2	málo
než	než	k8xS	než
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
71	[number]	k4	71
%	%	kIx~	%
oproti	oproti	k7c3	oproti
85	[number]	k4	85
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
z	z	k7c2	z
98	[number]	k4	98
%	%	kIx~	%
gramotná	gramotný	k2eAgNnPc4d1	gramotné
<g/>
.	.	kIx.	.
</s>
<s>
Pětiletka	pětiletka	k1gFnSc1	pětiletka
na	na	k7c4	na
roky	rok	k1gInPc4	rok
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
výstavba	výstavba	k1gFnSc1	výstavba
25	[number]	k4	25
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
na	na	k7c4	na
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Saúdskoarabská	saúdskoarabský	k2eAgFnSc1d1	saúdskoarabská
armáda	armáda	k1gFnSc1	armáda
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pozemní	pozemní	k2eAgFnSc2d1	pozemní
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
sil	síla	k1gFnPc2	síla
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
království	království	k1gNnPc4	království
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mnoho	mnoho	k4c4	mnoho
paramilitantních	paramilitantní	k2eAgFnPc2d1	paramilitantní
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
několik	několik	k4yIc4	několik
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
složky	složka	k1gFnPc1	složka
fungují	fungovat	k5eAaImIp3nP	fungovat
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
síle	síla	k1gFnSc6	síla
200	[number]	k4	200
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byly	být	k5eAaImAgInP	být
počty	počet	k1gInPc1	počet
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
složkách	složka	k1gFnPc6	složka
následující	následující	k2eAgNnSc1d1	následující
<g/>
:	:	kIx,	:
pozemní	pozemní	k2eAgFnSc1d1	pozemní
armáda	armáda	k1gFnSc1	armáda
75	[number]	k4	75
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
letectvo	letectvo	k1gNnSc1	letectvo
18	[number]	k4	18
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
protiletecká	protiletecký	k2eAgFnSc1d1	protiletecká
obrana	obrana	k1gFnSc1	obrana
16	[number]	k4	16
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
15	[number]	k4	15
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
75	[number]	k4	75
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
25	[number]	k4	25
000	[number]	k4	000
kmenových	kmenový	k2eAgMnPc2d1	kmenový
bojovníků	bojovník	k1gMnPc2	bojovník
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
gardě	garda	k1gFnSc6	garda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
také	také	k9	také
funguje	fungovat	k5eAaImIp3nS	fungovat
náboženská	náboženský	k2eAgFnSc1d1	náboženská
policie	policie	k1gFnSc1	policie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sbor	sbor	k1gInSc1	sbor
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
ctnosti	ctnost	k1gFnSc2	ctnost
a	a	k8xC	a
potírání	potírání	k1gNnSc2	potírání
hříchu	hřích	k1gInSc2	hřích
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
náboženských	náboženský	k2eAgNnPc2d1	náboženské
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vojensky	vojensky	k6eAd1	vojensky
království	království	k1gNnSc4	království
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
;	;	kIx,	;
Američané	Američan	k1gMnPc1	Američan
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
dodávají	dodávat	k5eAaImIp3nP	dodávat
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
výcvik	výcvik	k1gInSc4	výcvik
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
výdaje	výdaj	k1gInPc1	výdaj
království	království	k1gNnSc2	království
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
HDP	HDP	kA	HDP
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
7	[number]	k4	7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
armádní	armádní	k2eAgFnSc2d1	armádní
výbavy	výbava	k1gFnSc2	výbava
země	zem	k1gFnSc2	zem
kupuje	kupovat	k5eAaImIp3nS	kupovat
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
utratila	utratit	k5eAaPmAgFnS	utratit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
85	[number]	k4	85
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
je	být	k5eAaImIp3nS	být
dobrovolný	dobrovolný	k2eAgMnSc1d1	dobrovolný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
povolen	povolit	k5eAaPmNgInS	povolit
pouze	pouze	k6eAd1	pouze
mužům	muž	k1gMnPc3	muž
<g/>
.	.	kIx.	.
<g/>
Saúdskoarabská	saúdskoarabský	k2eAgFnSc1d1	saúdskoarabská
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvybavenějších	vybavený	k2eAgFnPc2d3	nejvybavenější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tanků	tank	k1gInPc2	tank
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
315	[number]	k4	315
kusů	kus	k1gInPc2	kus
M1A2	M1A2	k1gFnSc2	M1A2
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
,	,	kIx,	,
290	[number]	k4	290
kusů	kus	k1gInPc2	kus
AMX-30	AMX-30	k1gFnPc2	AMX-30
a	a	k8xC	a
450	[number]	k4	450
kusů	kus	k1gInPc2	kus
M	M	kA	M
<g/>
60	[number]	k4	60
<g/>
A	a	k9	a
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
např.	např.	kA	např.
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
bojových	bojový	k2eAgNnPc2d1	bojové
vozidel	vozidlo	k1gNnPc2	vozidlo
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
3000	[number]	k4	3000
obrněných	obrněný	k2eAgInPc2d1	obrněný
transportérů	transportér	k1gInPc2	transportér
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
200	[number]	k4	200
kusů	kus	k1gInPc2	kus
mobilního	mobilní	k2eAgNnSc2d1	mobilní
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
,	,	kIx,	,
60	[number]	k4	60
vícehlavňových	vícehlavňový	k2eAgInPc2d1	vícehlavňový
raketometů	raketomet	k1gInPc2	raketomet
<g/>
,	,	kIx,	,
400	[number]	k4	400
minometů	minomet	k1gInPc2	minomet
a	a	k8xC	a
12	[number]	k4	12
útočných	útočný	k2eAgInPc2d1	útočný
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
mělo	mít	k5eAaImAgNnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
11	[number]	k4	11
válečných	válečná	k1gFnPc2	válečná
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
65	[number]	k4	65
bojových	bojový	k2eAgFnPc2d1	bojová
hlídkových	hlídkový	k2eAgFnPc2d1	hlídková
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
také	také	k9	také
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
bojových	bojový	k2eAgNnPc2d1	bojové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
hlavní	hlavní	k2eAgFnSc4d1	hlavní
útočnou	útočný	k2eAgFnSc4d1	útočná
sílu	síla	k1gFnSc4	síla
tvořily	tvořit	k5eAaImAgInP	tvořit
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
F-5	F-5	k1gFnPc2	F-5
a	a	k8xC	a
F-	F-	k1gFnPc2	F-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
domácích	domácí	k2eAgInPc2d1	domácí
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
terorismus	terorismus	k1gInSc1	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Teroristické	teroristický	k2eAgFnPc1d1	teroristická
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
al-Káida	al-Káida	k1gFnSc1	al-Káida
založená	založený	k2eAgFnSc1d1	založená
saúdskoarabským	saúdskoarabský	k2eAgMnSc7d1	saúdskoarabský
multimilionářem	multimilionář	k1gMnSc7	multimilionář
Usámou	Usáma	k1gMnSc7	Usáma
bin	bin	k?	bin
Ládinem	Ládin	k1gMnSc7	Ládin
<g/>
,	,	kIx,	,
útočí	útočit	k5eAaImIp3nS	útočit
např.	např.	kA	např.
na	na	k7c4	na
ropovody	ropovod	k1gInPc4	ropovod
nebo	nebo	k8xC	nebo
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
aktivně	aktivně	k6eAd1	aktivně
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
garda	garda	k1gFnSc1	garda
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
citlivých	citlivý	k2eAgInPc2d1	citlivý
cílů	cíl	k1gInPc2	cíl
(	(	kIx(	(
<g/>
ropná	ropný	k2eAgNnPc1d1	ropné
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kontrole	kontrola	k1gFnSc3	kontrola
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc4	provádění
protiteroristických	protiteroristický	k2eAgFnPc2d1	protiteroristická
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
potlačování	potlačování	k1gNnSc1	potlačování
občanských	občanský	k2eAgInPc2d1	občanský
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
zajišťování	zajišťování	k1gNnSc6	zajišťování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
členů	člen	k1gInPc2	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
složky	složka	k1gFnPc1	složka
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
komunikace	komunikace	k1gFnSc1	komunikace
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
minimum	minimum	k1gNnSc4	minimum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
strany	strana	k1gFnSc2	strana
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BOUČEK	Bouček	k1gMnSc1	Bouček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
132	[number]	k4	132
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
218	[number]	k4	218
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
BERÁNEK	Beránek	k1gMnSc1	Beránek
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
mezi	mezi	k7c7	mezi
tradicemi	tradice	k1gFnPc7	tradice
a	a	k8xC	a
moderností	modernost	k1gFnSc7	modernost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
647	[number]	k4	647
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LACEY	LACEY	kA	LACEY
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Inside	Insid	k1gMnSc5	Insid
the	the	k?	the
Kingdom	Kingdom	k1gInSc1	Kingdom
<g/>
:	:	kIx,	:
kings	kings	k1gInSc1	kings
<g/>
,	,	kIx,	,
clerics	clerics	k1gInSc1	clerics
<g/>
,	,	kIx,	,
modernists	modernists	k1gInSc1	modernists
<g/>
,	,	kIx,	,
terrorists	terrorists	k1gInSc1	terrorists
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
struggle	struggle	k1gNnPc2	struggle
for	forum	k1gNnPc2	forum
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabius	k1gMnSc4	Arabius
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Viking	Viking	k1gMnSc1	Viking
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
404	[number]	k4	404
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
670	[number]	k4	670
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2118	[number]	k4	2118
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FÜRTIG	FÜRTIG	kA	FÜRTIG
<g/>
,	,	kIx,	,
Henner	Henner	k1gInSc1	Henner
<g/>
.	.	kIx.	.
</s>
<s>
Iran	Iran	k1gInSc1	Iran
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
rivalry	rivalr	k1gMnPc7	rivalr
with	witha	k1gFnPc2	witha
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
between	betwena	k1gFnPc2	betwena
the	the	k?	the
Gulf	Gulf	k1gInSc1	Gulf
wars	wars	k1gInSc1	wars
<g/>
.	.	kIx.	.
</s>
<s>
Reading	Reading	k1gInSc1	Reading
<g/>
:	:	kIx,	:
Ithaca	Ithaca	k1gFnSc1	Ithaca
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
288	[number]	k4	288
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
86372	[number]	k4	86372
<g/>
-	-	kIx~	-
<g/>
311	[number]	k4	311
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
AL-RASHEED	AL-RASHEED	k?	AL-RASHEED
<g/>
,	,	kIx,	,
Madawi	Madawe	k1gFnSc4	Madawe
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
history	histor	k1gInPc1	histor
of	of	k?	of
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
255	[number]	k4	255
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
64412	[number]	k4	64412
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LONG	LONG	kA	LONG
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
E.	E.	kA	E.
Culture	Cultur	k1gMnSc5	Cultur
and	and	k?	and
customs	customsa	k1gFnPc2	customsa
of	of	k?	of
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
<g/>
.	.	kIx.	.
</s>
<s>
Westport	Westport	k1gInSc1	Westport
<g/>
:	:	kIx,	:
Greenwood	Greenwood	k1gInSc1	Greenwood
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
124	[number]	k4	124
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
313	[number]	k4	313
<g/>
-	-	kIx~	-
<g/>
32021	[number]	k4	32021
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HĀ	HĀ	k?	HĀ
<g/>
,	,	kIx,	,
Manṣ	Manṣ	k1gMnSc1	Manṣ
Ibrā	Ibrā	k1gMnSc1	Ibrā
<g/>
.	.	kIx.	.
</s>
<s>
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
dunes	dunes	k1gMnSc1	dunes
<g/>
:	:	kIx,	:
an	an	k?	an
anthology	antholog	k1gMnPc4	antholog
of	of	k?	of
modern	modern	k1gMnSc1	modern
Saudi	Saud	k1gMnPc1	Saud
literature	literatur	k1gMnSc5	literatur
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Tauris	Tauris	k1gInSc1	Tauris
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
528	[number]	k4	528
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
85043	[number]	k4	85043
<g/>
-	-	kIx~	-
<g/>
972	[number]	k4	972
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Saudi	Saudit	k5eAaPmRp2nS	Saudit
Arabia	Arabia	k1gFnSc1	Arabia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Foreign	Foreign	k1gMnSc1	Foreign
&	&	k?	&
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
Office	Office	kA	Office
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2011-11-09	[number]	k4	2011-11-09
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Saudi	Saudit	k5eAaPmRp2nS	Saudit
Arabia	Arabia	k1gFnSc1	Arabia
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Saudi	Saudit	k5eAaPmRp2nS	Saudit
Arabia	Arabia	k1gFnSc1	Arabia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Near	Near	k1gInSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-05-06	[number]	k4	2011-05-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-08	[number]	k4	2011-07-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabium	k1gNnSc2	Arabium
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006-09-29	[number]	k4	2006-09-29
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-04-01	[number]	k4	2011-04-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OCHSENWALD	OCHSENWALD	kA	OCHSENWALD
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
L	L	kA	L
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Saudi	Saudit	k5eAaPmRp2nS	Saudit
Arabia	Arabia	k1gFnSc1	Arabia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopæ	Encyclopæ	k?	Encyclopæ
Britannica	Britannica	k1gFnSc1	Britannica
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Audiovizuální	audiovizuální	k2eAgInPc1d1	audiovizuální
dokumenty	dokument	k1gInPc1	dokument
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
King	King	k1gMnSc1	King
Abdullah	Abdullah	k1gMnSc1	Abdullah
University	universita	k1gFnSc2	universita
of	of	k?	of
Science	Science	k1gFnSc1	Science
and	and	k?	and
Technology	technolog	k1gMnPc7	technolog
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
House	house	k1gNnSc1	house
of	of	k?	of
Saud	Sauda	k1gFnPc2	Sauda
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
země	země	k1gFnSc1	země
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
</s>
</p>
