<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
London	London	k1gMnSc1	London
Eye	Eye	k1gFnSc2	Eye
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgNnSc1d1	oficiální
označení	označení	k1gNnSc1	označení
Coca-Cola	cocaola	k1gFnSc1	coca-cola
London	London	k1gMnSc1	London
Eye	Eye	k1gMnSc1	Eye
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
označováno	označován	k2eAgNnSc4d1	označováno
Millennium	millennium	k1gNnSc4	millennium
Wheel	Wheela	k1gFnPc2	Wheela
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Kolo	kolo	k1gNnSc1	kolo
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
největší	veliký	k2eAgFnSc4d3	veliký
vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
i	i	k8xC	i
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
