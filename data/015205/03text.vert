<s>
Dálnice	dálnice	k1gFnSc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
</s>
<s>
Dálnice	dálnice	k1gFnSc1
O-1	O-1	k1gFnSc2
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
</s>
<s>
Dálnice	dálnice	k1gFnSc1
O-3	O-3	k1gFnSc2
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
</s>
<s>
Dálnice	dálnice	k1gFnSc1
O-5	O-5	k1gFnSc2
</s>
<s>
Schéma	schéma	k1gNnSc1
turecké	turecký	k2eAgFnSc2d1
dálniční	dálniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
dálnic	dálnice	k1gFnPc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
2452	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
povolená	povolený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
na	na	k7c6
dálnicích	dálnice	k1gFnPc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
osobní	osobní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
120	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Na	na	k7c6
tureckých	turecký	k2eAgFnPc6d1
dálnicích	dálnice	k1gFnPc6
funguje	fungovat	k5eAaImIp3nS
mýtný	mýtný	k1gMnSc1
systém	systém	k1gInSc1
pro	pro	k7c4
všechna	všechen	k3xTgNnPc4
vozidla	vozidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecko	Turecko	k1gNnSc1
má	mít	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
vysoké	vysoký	k2eAgFnPc4d1
normy	norma	k1gFnPc4
pro	pro	k7c4
klasifikaci	klasifikace	k1gFnSc4
dálnice	dálnice	k1gFnSc2
<g/>
:	:	kIx,
dálnice	dálnice	k1gFnSc1
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
minimálně	minimálně	k6eAd1
3	#num#	k4
pruhy	pruh	k1gInPc4
v	v	k7c6
každém	každý	k3xTgInSc6
směru	směr	k1gInSc6
<g/>
,	,	kIx,
úseky	úsek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
nebezpečné	bezpečný	k2eNgInPc4d1
(	(	kIx(
<g/>
mosty	most	k1gInPc1
<g/>
,	,	kIx,
tunely	tunel	k1gInPc1
<g/>
,	,	kIx,
výjezdy	výjezd	k1gInPc1
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
osvětleny	osvětlit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
odpočívkách	odpočívka	k1gFnPc6
bývá	bývat	k5eAaImIp3nS
mimo	mimo	k7c4
parkoviště	parkoviště	k1gNnSc4
a	a	k8xC
čerpací	čerpací	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
i	i	k8xC
motel	motel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dálnicích	dálnice	k1gFnPc6
by	by	kYmCp3nP
se	se	k3xPyFc4
neměly	mít	k5eNaImAgFnP
vyskytovat	vyskytovat	k5eAaImF
příliš	příliš	k6eAd1
ostré	ostrý	k2eAgFnPc1d1
zatáčky	zatáčka	k1gFnPc1
<g/>
,	,	kIx,
klesání	klesání	k1gNnSc1
či	či	k8xC
stoupání	stoupání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
dálnice	dálnice	k1gFnPc4
nemají	mít	k5eNaImIp3nP
přístup	přístup	k1gInSc1
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
méně	málo	k6eAd2
než	než	k8xS
40	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
zemědělské	zemědělský	k2eAgInPc4d1
stroje	stroj	k1gInPc4
či	či	k8xC
cyklistická	cyklistický	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecko	Turecko	k1gNnSc1
má	mít	k5eAaImIp3nS
mimo	mimo	k6eAd1
dálnic	dálnice	k1gFnPc2
ještě	ještě	k6eAd1
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
síť	síť	k1gFnSc4
dvouproudých	dvouproudý	k2eAgFnPc2d1
silnic	silnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
výstavby	výstavba	k1gFnSc2
dálnic	dálnice	k1gFnPc2
</s>
<s>
Výstavba	výstavba	k1gFnSc1
národního	národní	k2eAgInSc2d1
silničního	silniční	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
byla	být	k5eAaImAgNnP
započata	započnout	k5eAaPmNgNnP
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
prudce	prudko	k6eAd1
narostl	narůst	k5eAaPmAgInS
počet	počet	k1gInSc1
automobilů	automobil	k1gInPc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
problémy	problém	k1gInPc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
mnohým	mnohé	k1gNnSc7
meziměststkých	meziměststký	k2eAgFnPc2d1
silnic	silnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
plány	plán	k1gInPc1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
dálnic	dálnice	k1gFnPc2
byly	být	k5eAaImAgInP
uvedeny	uvést	k5eAaPmNgInP
do	do	k7c2
praxe	praxe	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
stavět	stavět	k5eAaImF
první	první	k4xOgFnSc1
víceproudová	víceproudový	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
začala	začít	k5eAaPmAgFnS
výstavba	výstavba	k1gFnSc1
dalších	další	k2eAgFnPc2d1
dálnic	dálnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
tureckým	turecký	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
Turgut	Turgut	k1gMnSc1
Özal	Özal	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
plán	plán	k1gInSc4
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
nových	nový	k2eAgFnPc2d1
dálnic	dálnice	k1gFnPc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
začal	začít	k5eAaPmAgInS
obří	obří	k2eAgInSc1d1
projekt	projekt	k1gInSc1
mezikontinentální	mezikontinentální	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
o	o	k7c6
délce	délka	k1gFnSc6
772	#num#	k4
km	km	kA
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
spojoval	spojovat	k5eAaImAgMnS
město	město	k1gNnSc4
Edirne	Edirn	k1gInSc5
nedaleko	nedaleko	k7c2
bulharských	bulharský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
,	,	kIx,
Istanbul	Istanbul	k1gInSc4
a	a	k8xC
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Ankaru	Ankara	k1gFnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
i	i	k9
dálniční	dálniční	k2eAgInSc1d1
most	most	k1gInSc1
přes	přes	k7c4
Bosporský	bosporský	k2eAgInSc4d1
průliv	průliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
dalších	další	k2eAgInPc2d1
úseků	úsek	k1gInPc2
dále	daleko	k6eAd2
pokračovala	pokračovat	k5eAaImAgFnS
a	a	k8xC
dnes	dnes	k6eAd1
má	mít	k5eAaImIp3nS
Turecko	Turecko	k1gNnSc4
téměř	téměř	k6eAd1
2500	#num#	k4
km	km	kA
dálnic	dálnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1
dálniční	dálniční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
rozšiřuje	rozšiřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
očekává	očekávat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
roku	rok	k1gInSc2
2023	#num#	k4
bude	být	k5eAaImBp3nS
síť	síť	k1gFnSc4
dosahovat	dosahovat	k5eAaImF
4773	#num#	k4
km	km	kA
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
2035	#num#	k4
9312	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
dálnic	dálnice	k1gFnPc2
</s>
<s>
Dálnice	dálnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Turecku	Turecko	k1gNnSc6
označovány	označovat	k5eAaImNgInP
písmenem	písmeno	k1gNnSc7
O	o	k7c4
(	(	kIx(
<g/>
otoyol	otoyol	k1gInSc4
-	-	kIx~
turecky	turecky	k6eAd1
dálnice	dálnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
</s>
<s>
Trasa	trasa	k1gFnSc1
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
výstavbě	výstavba	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
provozu	provoz	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Istanbul	Istanbul	k1gInSc1
(	(	kIx(
<g/>
Haličský	haličský	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
25	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
25	#num#	k4
</s>
<s>
Istanbul	Istanbul	k1gInSc1
(	(	kIx(
<g/>
Most	most	k1gInSc1
sultána	sultán	k1gMnSc2
Mehmeda	Mehmed	k1gMnSc2
Dobyvatele	dobyvatel	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
37	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
37	#num#	k4
</s>
<s>
Istanbul	Istanbul	k1gInSc1
-	-	kIx~
Edirne	Edirn	k1gInSc5
-	-	kIx~
Bulharsko	Bulharsko	k1gNnSc1
(	(	kIx(
<g/>
Dálnice	dálnice	k1gFnSc1
A	a	k9
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
229	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
229	#num#	k4
</s>
<s>
Ankara	Ankara	k1gFnSc1
-	-	kIx~
Istanbul	Istanbul	k1gInSc1
</s>
<s>
407	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
407	#num#	k4
</s>
<s>
Istanbul	Istanbul	k1gInSc1
-	-	kIx~
Bursa	bursa	k1gFnSc1
-	-	kIx~
Izmir	Izmir	k1gInSc1
</s>
<s>
407	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
407	#num#	k4
</s>
<s>
Istanbul	Istanbul	k1gInSc1
-	-	kIx~
Kocaeli	Kocael	k1gInPc7
-	-	kIx~
Sakarya	Sakarya	k1gFnSc1
</s>
<s>
431	#num#	k4
</s>
<s>
336	#num#	k4
</s>
<s>
95	#num#	k4
</s>
<s>
Ankarský	ankarský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
</s>
<s>
110	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
110	#num#	k4
</s>
<s>
Ankara	Ankara	k1gFnSc1
-	-	kIx~
Niğ	Niğ	k1gMnSc5
</s>
<s>
430	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
162	#num#	k4
</s>
<s>
Bursa	bursa	k1gFnSc1
-	-	kIx~
Eskişehir	Eskişehir	k1gInSc1
</s>
<s>
238	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
Izmirská	Izmirský	k2eAgFnSc1d1
spojka	spojka	k1gFnSc1
</s>
<s>
51	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
51	#num#	k4
</s>
<s>
Izmir	Izmir	k1gMnSc1
-	-	kIx~
Aydı	Aydı	k1gMnSc1
</s>
<s>
370	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
97	#num#	k4
</s>
<s>
Izmir	Izmir	k1gMnSc1
</s>
<s>
79	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
79	#num#	k4
</s>
<s>
Izmir	Izmir	k1gMnSc1
</s>
<s>
76	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
76	#num#	k4
</s>
<s>
Adana	Adana	k6eAd1
</s>
<s>
23	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
23	#num#	k4
</s>
<s>
Adana	Adana	k1gFnSc1
-	-	kIx~
Mersin	Mersin	k1gInSc1
</s>
<s>
88	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
88	#num#	k4
</s>
<s>
Adana	Adana	k1gFnSc1
-	-	kIx~
Osmaniye	Osmaniye	k1gInSc1
-	-	kIx~
Şanlı	Şanlı	k1gMnSc2
</s>
<s>
365	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
365	#num#	k4
</s>
<s>
Dörtyol	Dörtyol	k1gInSc1
(	(	kIx(
<g/>
O-	O-	k1gFnSc1
<g/>
52	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
İ	İ	k1gInSc1
</s>
<s>
150	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
150	#num#	k4
</s>
<s>
obchvat	obchvat	k1gInSc1
města	město	k1gNnSc2
Gaziantep	Gaziantep	k1gInSc1
</s>
<s>
32	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
32	#num#	k4
</s>
<s>
Metrobusy	Metrobus	k1gInPc1
</s>
<s>
Logo	logo	k1gNnSc1
istanbulských	istanbulský	k2eAgInPc2d1
metrobusů	metrobus	k1gInPc2
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
turecké	turecký	k2eAgNnSc1d1
město	město	k1gNnSc1
Istanbul	Istanbul	k1gInSc1
provozuje	provozovat	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
Městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
tzv.	tzv.	kA
Metrobusy	Metrobus	k1gInPc1
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
Metrobüs	Metrobüs	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
projíždějí	projíždět	k5eAaImIp3nP
cestami	cesta	k1gFnPc7
ve	v	k7c6
středových	středový	k2eAgInPc6d1
dělících	dělící	k2eAgInPc6d1
pásech	pás	k1gInPc6
u	u	k7c2
dálnic	dálnice	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zastavují	zastavovat	k5eAaImIp3nP
na	na	k7c4
k	k	k7c3
tomu	ten	k3xDgNnSc3
určených	určený	k2eAgFnPc6d1
zastávkách	zastávka	k1gFnPc6
<g/>
,	,	kIx,
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
45	#num#	k4
a	a	k8xC
cestující	cestující	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gMnPc4
dostanou	dostat	k5eAaPmIp3nP
po	po	k7c6
lávkách	lávka	k1gFnPc6
nad	nad	k7c7
dálnicemi	dálnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
síť	síť	k1gFnSc1
metrobusů	metrobus	k1gMnPc2
měří	měřit	k5eAaImIp3nS
celkem	celkem	k6eAd1
50	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linky	linka	k1gFnPc1
jezdí	jezdit	k5eAaImIp3nP
na	na	k7c6
mnoha	mnoho	k4c6
hlavních	hlavní	k2eAgFnPc6d1
dálnicích	dálnice	k1gFnPc6
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
hlavní	hlavní	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
a	a	k8xC
Bosporského	bosporský	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denně	denně	k6eAd1
metrobusy	metrobus	k1gInPc4
přepraví	přepravit	k5eAaPmIp3nS
800	#num#	k4
000	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
List	list	k1gInSc1
of	of	k?
otoyol	otoyol	k1gInSc1
routes	routes	k1gInSc1
in	in	k?
Turkey	Turkea	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dálnice	dálnice	k1gFnSc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dálnice	dálnice	k1gFnSc1
v	v	k7c6
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
</s>
<s>
Dálnice	dálnice	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Území	území	k1gNnSc4
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
:	:	kIx,
Abcházie	Abcházie	k1gFnSc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Podněstří	Podněstří	k1gMnSc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Doprava	doprava	k1gFnSc1
</s>
