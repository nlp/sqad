<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
povolená	povolený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
na	na	k7c6
dálnicích	dálnice	k1gFnPc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
osobní	osobní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
120	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Na	na	k7c6
tureckých	turecký	k2eAgFnPc6d1
dálnicích	dálnice	k1gFnPc6
funguje	fungovat	k5eAaImIp3nS
mýtný	mýtný	k1gMnSc1
systém	systém	k1gInSc1
pro	pro	k7c4
všechna	všechen	k3xTgNnPc4
vozidla	vozidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>