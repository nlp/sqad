<s>
Makoto	Makota	k1gFnSc5	Makota
Šinkai	Šinka	k1gFnSc5	Šinka
(	(	kIx(	(
<g/>
新	新	k?	新
誠	誠	k?	誠
<g/>
,	,	kIx,	,
Šinkai	Šinka	k1gFnPc1	Šinka
Makoto	Makota	k1gFnSc5	Makota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Makoto	Makota	k1gFnSc5	Makota
Niicu	Niicum	k1gNnSc3	Niicum
(	(	kIx(	(
<g/>
新	新	k?	新
誠	誠	k?	誠
<g/>
,	,	kIx,	,
Niicu	Niic	k2eAgFnSc4d1	Niic
Makoto	Makota	k1gFnSc5	Makota
<g/>
,	,	kIx,	,
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonským	japonský	k2eAgMnSc7d1	japonský
animátorem	animátor	k1gMnSc7	animátor
<g/>
,	,	kIx,	,
režisérem	režisér	k1gMnSc7	režisér
a	a	k8xC	a
předním	přední	k2eAgMnSc7d1	přední
dabérem	dabér	k1gMnSc7	dabér
žánru	žánr	k1gInSc2	žánr
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
také	také	k9	také
kreslí	kreslit	k5eAaImIp3nP	kreslit
mangu	mango	k1gNnSc3	mango
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Nagano	Nagano	k1gNnSc1	Nagano
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
japonskou	japonský	k2eAgFnSc4d1	japonská
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Šinkai	Šinkai	k6eAd1	Šinkai
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
recenzích	recenze	k1gFnPc6	recenze
nazván	nazvat	k5eAaPmNgInS	nazvat
"	"	kIx"	"
<g/>
Novým	nový	k2eAgNnSc7d1	nové
Mijazakim	Mijazaki	k1gNnSc7	Mijazaki
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
srovnáním	srovnání	k1gNnSc7	srovnání
sám	sám	k3xTgMnSc1	sám
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
a	a	k8xC	a
cítí	cítit	k5eAaImIp3nS	cítit
se	se	k3xPyFc4	se
přeceňován	přeceňován	k2eAgInSc1d1	přeceňován
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
světy	svět	k1gInPc1	svět
(	(	kIx(	(
<g/>
遠	遠	k?	遠
<g/>
,	,	kIx,	,
Tói	Tói	k1gMnSc1	Tói
sekai	seka	k1gFnSc2	seka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Ona	onen	k3xDgFnSc1	onen
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
彼	彼	k?	彼
<g/>
,	,	kIx,	,
Kanodžo	Kanodžo	k6eAd1	Kanodžo
to	ten	k3xDgNnSc4	ten
kanodžo	kanodžo	k1gNnSc4	kanodžo
no	no	k9	no
neko	ko	k6eNd1	ko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Hlasy	hlas	k1gInPc7	hlas
ze	z	k7c2	z
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
ほ	ほ	k?	ほ
<g/>
,	,	kIx,	,
Hoši	hoch	k1gMnPc1	hoch
no	no	k9	no
koe	koe	k?	koe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g />
.	.	kIx.	.
</s>
<s>
Egao	Egao	k1gNnSc1	Egao
(	(	kIx(	(
<g/>
み	み	k?	み
<g/>
「	「	k?	「
<g/>
笑	笑	k?	笑
<g/>
」	」	k?	」
<g/>
,	,	kIx,	,
Minna	Minen	k2eAgFnSc1d1	Minna
no	no	k9	no
uta	uta	k?	uta
"	"	kIx"	"
<g/>
Egao	Egao	k1gMnSc1	Egao
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Za	za	k7c4	za
mraky	mrak	k1gInPc4	mrak
země	země	k1gFnSc1	země
zaslíbená	zaslíbená	k1gFnSc1	zaslíbená
(	(	kIx(	(
<g/>
雲	雲	k?	雲
<g/>
、	、	k?	、
<g/>
約	約	k?	約
<g/>
,	,	kIx,	,
Kumo	kuma	k1gFnSc5	kuma
no	no	k9	no
mukó	mukó	k?	mukó
<g/>
,	,	kIx,	,
jakusoku	jakusok	k1gInSc2	jakusok
no	no	k9	no
bašo	baša	k1gMnSc5	baša
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
5	[number]	k4	5
centimetrů	centimetr	k1gInPc2	centimetr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
秒	秒	k?	秒
<g/>
5	[number]	k4	5
<g/>
セ	セ	k?	セ
<g/>
,	,	kIx,	,
Bjósoku	Bjósok	k1gInSc6	Bjósok
go	go	k?	go
senčimétoru	senčimétor	k1gInSc2	senčimétor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Kočičí	kočičí	k2eAgNnSc4d1	kočičí
shromáždění	shromáždění	k1gNnSc4	shromáždění
(	(	kIx(	(
<g/>
猫	猫	k?	猫
<g/>
,	,	kIx,	,
Neko	Neko	k6eAd1	Neko
no	no	k9	no
šúkai	šúkai	k6eAd1	šúkai
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
následují	následovat	k5eAaImIp3nP	následovat
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
星	星	k?	星
<g/>
,	,	kIx,	,
Hoši	hoch	k1gMnPc1	hoch
o	o	k7c4	o
ou	ou	k0	ou
kodomo	kodomo	k6eAd1	kodomo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Zahrada	zahrada	k1gFnSc1	zahrada
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
言	言	k?	言
<g/>
,	,	kIx,	,
Kotonoha	Kotonoha	k1gFnSc1	Kotonoha
no	no	k9	no
niwa	niwa	k6eAd1	niwa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
Tvoje	tvůj	k3xOp2gNnSc1	tvůj
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
君	君	k?	君
<g/>
,	,	kIx,	,
Kimi	Kim	k1gMnSc3	Kim
no	no	k9	no
na	na	k7c4	na
wa	wa	k?	wa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
vydání	vydání	k1gNnSc1	vydání
2016	[number]	k4	2016
</s>
