<s>
Makoto	Makota	k1gFnSc5	Makota
Šinkai	Šinka	k1gFnSc5	Šinka
(	(	kIx(	(
<g/>
新	新	k?	新
誠	誠	k?	誠
<g/>
,	,	kIx,	,
Šinkai	Šinka	k1gFnPc1	Šinka
Makoto	Makota	k1gFnSc5	Makota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Makoto	Makota	k1gFnSc5	Makota
Niicu	Niicum	k1gNnSc3	Niicum
(	(	kIx(	(
<g/>
新	新	k?	新
誠	誠	k?	誠
<g/>
,	,	kIx,	,
Niicu	Niic	k2eAgFnSc4d1	Niic
Makoto	Makota	k1gFnSc5	Makota
<g/>
,	,	kIx,	,
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonským	japonský	k2eAgMnSc7d1	japonský
animátorem	animátor	k1gMnSc7	animátor
<g/>
,	,	kIx,	,
režisérem	režisér	k1gMnSc7	režisér
a	a	k8xC	a
předním	přední	k2eAgMnSc7d1	přední
dabérem	dabér	k1gMnSc7	dabér
žánru	žánr	k1gInSc2	žánr
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
také	také	k9	také
kreslí	kreslit	k5eAaImIp3nP	kreslit
mangu	mango	k1gNnSc3	mango
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Nagano	Nagano	k1gNnSc1	Nagano
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
japonskou	japonský	k2eAgFnSc4d1	japonská
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
