<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Thajska	Thajsko	k1gNnSc2	Thajsko
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
pruhů	pruh	k1gInPc2	pruh
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
:	:	kIx,	:
červeného	červené	k1gNnSc2	červené
<g/>
,	,	kIx,	,
bílého	bílé	k1gNnSc2	bílé
<g/>
,	,	kIx,	,
modrého	modré	k1gNnSc2	modré
<g/>
,	,	kIx,	,
bílého	bílé	k1gNnSc2	bílé
a	a	k8xC	a
červeného	červené	k1gNnSc2	červené
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
šířek	šířka	k1gFnPc2	šířka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
trairanga	trairanga	k1gFnSc1	trairanga
(	(	kIx(	(
<g/>
thajsky	thajsky	k6eAd1	thajsky
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejího	její	k3xOp3gNnSc2	její
zavedení	zavedení	k1gNnSc2	zavedení
připomínat	připomínat	k5eAaImF	připomínat
barvu	barva	k1gFnSc4	barva
spojenců	spojenec	k1gMnPc2	spojenec
Siamu	Siam	k1gInSc2	Siam
v	v	k7c6	v
I.	I.	kA	I.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
(	(	kIx(	(
<g/>
vlajky	vlajka	k1gFnPc4	vlajka
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgMnPc2	všecek
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
červenou	červený	k2eAgFnSc7d1	červená
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc7d1	bílá
a	a	k8xC	a
modrou	modrý	k2eAgFnSc7d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
thajskou	thajský	k2eAgFnSc7d1	thajská
národní	národní	k2eAgFnSc7d1	národní
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
dle	dle	k7c2	dle
novodobého	novodobý	k2eAgInSc2d1	novodobý
výkladu	výklad	k1gInSc2	výklad
symboliky	symbolika	k1gFnSc2	symbolika
barev	barva	k1gFnPc2	barva
představuje	představovat	k5eAaImIp3nS	představovat
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
prolitou	prolitý	k2eAgFnSc4d1	prolitá
krev	krev	k1gFnSc4	krev
za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
získání	získání	k1gNnSc4	získání
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
buddhismus	buddhismus	k1gInSc4	buddhismus
a	a	k8xC	a
čistotu	čistota	k1gFnSc4	čistota
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Thajsko	Thajsko	k1gNnSc1	Thajsko
často	často	k6eAd1	často
nazývalo	nazývat	k5eAaImAgNnS	nazývat
"	"	kIx"	"
<g/>
Zemí	zem	k1gFnSc7	zem
bílého	bílý	k1gMnSc2	bílý
slona	slon	k1gMnSc2	slon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc4	obraz
slona	slon	k1gMnSc4	slon
na	na	k7c6	na
thajské	thajský	k2eAgFnSc6d1	thajská
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
není	být	k5eNaImIp3nS	být
už	už	k6eAd1	už
od	od	k7c2	od
konce	konec	k1gInSc2	konec
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
však	však	k9	však
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
thajské	thajský	k2eAgFnSc6d1	thajská
námořní	námořní	k2eAgFnSc6d1	námořní
válečné	válečný	k2eAgFnSc6d1	válečná
vlajce	vlajka	k1gFnSc6	vlajka
(	(	kIx(	(
<g/>
vlajce	vlajka	k1gFnSc6	vlajka
vojenského	vojenský	k2eAgNnSc2d1	vojenské
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
slonem	slon	k1gMnSc7	slon
se	se	k3xPyFc4	se
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
běžně	běžně	k6eAd1	běžně
prodává	prodávat	k5eAaImIp3nS	prodávat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Thajska	Thajsko	k1gNnSc2	Thajsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
známého	známý	k1gMnSc4	známý
i	i	k8xC	i
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Siam	Siam	k1gInSc1	Siam
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
obýváno	obývat	k5eAaImNgNnS	obývat
khmérskými	khmérský	k2eAgInPc7d1	khmérský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sem	sem	k6eAd1	sem
začali	začít	k5eAaPmAgMnP	začít
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Číny	Čína	k1gFnSc2	Čína
pronikat	pronikat	k5eAaImF	pronikat
Thajové	Thajový	k2eAgNnSc4d1	Thajový
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
zbavili	zbavit	k5eAaPmAgMnP	zbavit
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
Khmérském	Khmérský	k2eAgNnSc6d1	Khmérský
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
Angkor	Angkor	k1gMnSc1	Angkor
<g/>
)	)	kIx)	)
a	a	k8xC	a
Království	království	k1gNnSc1	království
Ajutthaja	Ajutthaj	k1gInSc2	Ajutthaj
(	(	kIx(	(
<g/>
Ayutthaya	Ayutthaya	k1gFnSc1	Ayutthaya
<g/>
)	)	kIx)	)
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
většinu	většina	k1gFnSc4	většina
Malajského	malajský	k2eAgInSc2d1	malajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1656	[number]	k4	1656
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgFnPc1	první
státní	státní	k2eAgFnPc1d1	státní
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
prosté	prostý	k2eAgInPc1d1	prostý
červené	červený	k2eAgInPc1d1	červený
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
tradiční	tradiční	k2eAgFnSc7d1	tradiční
panovnickou	panovnický	k2eAgFnSc7d1	panovnická
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
začali	začít	k5eAaPmAgMnP	začít
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pronikat	pronikat	k5eAaImF	pronikat
Evropané	Evropan	k1gMnPc1	Evropan
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1688	[number]	k4	1688
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
zcela	zcela	k6eAd1	zcela
izolovala	izolovat	k5eAaBmAgFnS	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
bylo	být	k5eAaImAgNnS	být
Království	království	k1gNnSc1	království
Ajutthaja	Ajutthaj	k1gInSc2	Ajutthaj
po	po	k7c6	po
barmské	barmský	k2eAgFnSc6d1	barmská
invazi	invaze	k1gFnSc6	invaze
zcela	zcela	k6eAd1	zcela
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
založil	založit	k5eAaPmAgMnS	založit
Ráma	Ráma	k1gMnSc1	Ráma
I.	I.	kA	I.
novou	nova	k1gFnSc7	nova
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
panující	panující	k2eAgFnSc3d1	panující
dynastii	dynastie	k1gFnSc3	dynastie
Chakri	Chakr	k1gFnSc2	Chakr
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Barmánce	Barmánec	k1gMnPc4	Barmánec
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Krung	Krung	k1gMnSc1	Krung
Thep	Thep	k1gMnSc1	Thep
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
Bangkok	Bangkok	k1gInSc1	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Ráma	Ráma	k1gMnSc1	Ráma
I.	I.	kA	I.
doplnil	doplnit	k5eAaPmAgMnS	doplnit
červený	červený	k2eAgInSc4d1	červený
list	list	k1gInSc4	list
vlajky	vlajka	k1gFnSc2	vlajka
o	o	k7c4	o
bílé	bílý	k2eAgNnSc4d1	bílé
buddhistické	buddhistický	k2eAgNnSc4d1	buddhistické
Kolo	kolo	k1gNnSc4	kolo
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
prstenec	prstenec	k1gInSc4	prstenec
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
loukotí	loukoť	k1gFnPc2	loukoť
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
přidal	přidat	k5eAaPmAgMnS	přidat
Ráma	Ráma	k1gMnSc1	Ráma
II	II	kA	II
<g/>
.	.	kIx.	.
do	do	k7c2	do
středu	střed	k1gInSc2	střed
kola	kolo	k1gNnSc2	kolo
bílého	bílý	k2eAgMnSc4d1	bílý
slona	slon	k1gMnSc4	slon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
velmi	velmi	k6eAd1	velmi
vzácných	vzácný	k2eAgMnPc2d1	vzácný
bílých	bílý	k2eAgMnPc2d1	bílý
slonů	slon	k1gMnPc2	slon
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
posuzovala	posuzovat	k5eAaImAgFnS	posuzovat
důstojnost	důstojnost	k1gFnSc1	důstojnost
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
siamských	siamský	k2eAgMnPc2d1	siamský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buddhistické	buddhistický	k2eAgFnSc6d1	buddhistická
víře	víra	k1gFnSc6	víra
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
Buddha	Buddha	k1gMnSc1	Buddha
do	do	k7c2	do
lůna	lůno	k1gNnSc2	lůno
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bílého	bílý	k2eAgNnSc2d1	bílé
slůněte	slůně	k1gNnSc2	slůně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
odstranil	odstranit	k5eAaPmAgMnS	odstranit
Ráma	Ráma	k1gMnSc1	Ráma
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
Kolo	kolo	k1gNnSc1	kolo
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Grafická	grafický	k2eAgFnSc1d1	grafická
podoba	podoba	k1gFnSc1	podoba
bílého	bílý	k2eAgMnSc2d1	bílý
slona	slon	k1gMnSc2	slon
hledícího	hledící	k2eAgInSc2d1	hledící
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
mírně	mírně	k6eAd1	mírně
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
Království	království	k1gNnSc4	království
Siam	Siam	k1gInSc4	Siam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
změnil	změnit	k5eAaPmAgMnS	změnit
Ráma	Ráma	k1gMnSc1	Ráma
VI	VI	kA	VI
<g/>
.	.	kIx.	.
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
přidáním	přidání	k1gNnSc7	přidání
žlutého	žlutý	k2eAgInSc2d1	žlutý
podstavce	podstavec	k1gInSc2	podstavec
pod	pod	k7c4	pod
vyobrazeného	vyobrazený	k2eAgMnSc4d1	vyobrazený
slona	slon	k1gMnSc4	slon
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
ustrojení	ustrojení	k1gNnSc4	ustrojení
do	do	k7c2	do
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgFnSc2d1	zdobená
výstroje	výstroj	k1gFnSc2	výstroj
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	stran	k7c2	stran
vlajky	vlajka	k1gFnSc2	vlajka
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
nebyl	být	k5eNaImAgInS	být
vůbec	vůbec	k9	vůbec
stanoven	stanovit	k5eAaPmNgInS	stanovit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
stejným	stejný	k2eAgMnSc7d1	stejný
králem	král	k1gMnSc7	král
změněna	změnit	k5eAaPmNgNnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
údajně	údajně	k6eAd1	údajně
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
po	po	k7c4	po
království	království	k1gNnPc4	království
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
obráceně	obráceně	k6eAd1	obráceně
vyvěšenou	vyvěšený	k2eAgFnSc4d1	vyvěšená
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
sloní	sloní	k2eAgFnPc1d1	sloní
nohy	noha	k1gFnPc1	noha
směřovaly	směřovat	k5eAaImAgFnP	směřovat
k	k	k7c3	k
hornímu	horní	k2eAgInSc3d1	horní
okraji	okraj	k1gInSc3	okraj
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pruhy	pruh	k1gInPc7	pruh
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zezhora	zezhora	k1gFnSc1	zezhora
střídavě	střídavě	k6eAd1	střídavě
červenými	červený	k2eAgInPc7d1	červený
a	a	k8xC	a
bílými	bílý	k2eAgInPc7d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Stejný	stejný	k2eAgMnSc1d1	stejný
král	král	k1gMnSc1	král
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
nechal	nechat	k5eAaPmAgMnS	nechat
nařízením	nařízení	k1gNnSc7	nařízení
ze	z	k7c2	z
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
vlajku	vlajka	k1gFnSc4	vlajka
opět	opět	k6eAd1	opět
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
místo	místo	k7c2	místo
středního	střední	k2eAgInSc2d1	střední
<g/>
,	,	kIx,	,
červeného	červený	k2eAgInSc2d1	červený
<g/>
,	,	kIx,	,
širšího	široký	k2eAgInSc2d2	širší
pruhu	pruh	k1gInSc2	pruh
měla	mít	k5eAaImAgFnS	mít
pruh	pruh	k1gInSc4	pruh
modrý	modrý	k2eAgInSc4d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.24	.24	k4	.24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1932	[number]	k4	1932
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
absolutistická	absolutistický	k2eAgFnSc1d1	absolutistická
monarchie	monarchie	k1gFnSc1	monarchie
na	na	k7c4	na
monarchii	monarchie	k1gFnSc4	monarchie
konstituční	konstituční	k2eAgMnSc1d1	konstituční
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
Siam	Siam	k1gInSc1	Siam
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Thajsko	Thajsko	k1gNnSc4	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Thajské	thajský	k2eAgInPc1d1	thajský
Muang	Muang	k1gInSc1	Muang
Thai	Tha	k1gFnSc2	Tha
znamená	znamenat	k5eAaImIp3nS	znamenat
Země	země	k1gFnSc1	země
svobodných	svobodný	k2eAgMnPc2d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Thajsko	Thajsko	k1gNnSc1	Thajsko
spojencem	spojenec	k1gMnSc7	spojenec
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
mu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgNnP	být
navrácena	navrácen	k2eAgNnPc1d1	navráceno
ztracená	ztracený	k2eAgNnPc1d1	ztracené
území	území	k1gNnPc1	území
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc6	Laos
a	a	k8xC	a
Malajsku	Malajsko	k1gNnSc6	Malajsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1941	[number]	k4	1941
Japonsko	Japonsko	k1gNnSc1	Japonsko
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Thajsko	Thajsko	k1gNnSc4	Thajsko
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
loutkový	loutkový	k2eAgInSc1d1	loutkový
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
užívání	užívání	k1gNnSc1	užívání
vlajky	vlajka	k1gFnSc2	vlajka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
užívání	užívání	k1gNnSc1	užívání
předválečné	předválečný	k2eAgFnSc2d1	předválečná
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
opět	opět	k6eAd1	opět
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Siam	Siam	k1gInSc4	Siam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Thajska	Thajsko	k1gNnSc2	Thajsko
</s>
</p>
<p>
<s>
Thajská	thajský	k2eAgFnSc1d1	thajská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Thajská	thajský	k2eAgFnSc1d1	thajská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Museum	museum	k1gNnSc1	museum
thajské	thajský	k2eAgFnSc2d1	thajská
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
thajsky	thajsky	k6eAd1	thajsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Historické	historický	k2eAgFnPc1d1	historická
thajské	thajský	k2eAgFnPc1d1	thajská
vlajky	vlajka	k1gFnPc1	vlajka
</s>
</p>
