<s>
Křenovice	Křenovice	k1gFnSc1	Křenovice
(	(	kIx(	(
Křenovice	Křenovice	k1gFnSc1	Křenovice
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Krenowitz	Krenowitz	k1gInSc1	Krenowitz
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Křenovice	Křenovice	k1gFnSc1	Křenovice
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
,	,	kIx,	,
s	s	k7c7	s
předložkou	předložka	k1gFnSc7	předložka
2	[number]	k4	2
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
do	do	k7c2	do
Křenovic	Křenovice	k1gFnPc2	Křenovice
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
v	v	k7c6	v
Křenovicích	Křenovice	k1gFnPc6	Křenovice
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
okresu	okres	k1gInSc3	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Křenovice	Křenovice	k1gFnSc1	Křenovice
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
potoce	potok	k1gInSc6	potok
Rakovci	Rakovec	k1gInSc6	Rakovec
19	[number]	k4	19
<g/>
,	,	kIx,	,
5	[number]	k4	5
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
bývalého	bývalý	k2eAgNnSc2d1	bývalé
okresního	okresní	k2eAgNnSc2d1	okresní
města	město	k1gNnSc2	město
Vyškova	Vyškov	k1gInSc2	Vyškov
<g/>
,	,	kIx,	,
4	[number]	k4	4
km	km	kA	km
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
města	město	k1gNnSc2	město
Slavkova	Slavkov	k1gInSc2	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
17	[number]	k4	17
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
210	[number]	k4	210
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
potok	potok	k1gInSc1	potok
Rakovec	Rakovec	k1gInSc1	Rakovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
250	[number]	k4	250
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
tratě	trať	k1gFnSc2	trať
Zadní	zadní	k2eAgMnSc1d1	zadní
a	a	k8xC	a
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křenovice	Křenovice	k1gFnPc1	Křenovice
měly	mít	k5eAaImAgFnP	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
1897	[number]	k4	1897
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1305	[number]	k4	1305
<g/>
.	.	kIx.	.
</s>
<s>
Křenovice	Křenovice	k1gFnPc1	Křenovice
dodnes	dodnes	k6eAd1	dodnes
nemají	mít	k5eNaImIp3nP	mít
udělený	udělený	k2eAgInSc4d1	udělený
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
symbol	symbol	k1gInSc4	symbol
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
nejstarší	starý	k2eAgFnSc4d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc4d1	dochovaná
pečeť	pečeť	k1gFnSc4	pečeť
obce	obec	k1gFnSc2	obec
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obcí	obec	k1gFnPc2	obec
musela	muset	k5eAaImAgFnS	muset
pořídit	pořídit	k5eAaPmF	pořídit
pečetidlo	pečetidlo	k1gNnSc4	pečetidlo
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1737	[number]	k4	1737
se	se	k3xPyFc4	se
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Křenovic	Křenovice	k1gFnPc2	Křenovice
nepojí	pojíst	k5eNaPmIp3nS	pojíst
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc4	stáří
pečeti	pečeť	k1gFnSc2	pečeť
tak	tak	k9	tak
vychází	vycházet	k5eAaImIp3nS	vycházet
čistě	čistě	k6eAd1	čistě
z	z	k7c2	z
letopočtu	letopočet	k1gInSc2	letopočet
uvedeného	uvedený	k2eAgInSc2d1	uvedený
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
samé	samý	k3xTgFnSc6	samý
<g/>
.	.	kIx.	.
</s>
<s>
Pečetěno	pečetěn	k2eAgNnSc1d1	pečetěn
bylo	být	k5eAaImAgNnS	být
červeným	červený	k2eAgInSc7d1	červený
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Pečeť	pečeť	k1gFnSc1	pečeť
je	být	k5eAaImIp3nS	být
kruhová	kruhový	k2eAgFnSc1d1	kruhová
s	s	k7c7	s
textem	text	k1gInSc7	text
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
a	a	k8xC	a
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
rostliny	rostlina	k1gFnSc2	rostlina
hroznového	hroznový	k2eAgNnSc2d1	hroznové
vína	víno	k1gNnSc2	víno
s	s	k7c7	s
šesti	šest	k4xCc2	šest
svazky	svazek	k1gInPc7	svazek
hroznů	hrozen	k1gInPc2	hrozen
a	a	k8xC	a
včetně	včetně	k7c2	včetně
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
Pečeť	pečeť	k1gFnSc1	pečeť
obce	obec	k1gFnSc2	obec
Křenovic	Křenovice	k1gFnPc2	Křenovice
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Křenovic	Křenovice	k1gFnPc2	Křenovice
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k9	již
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
4500	[number]	k4	4500
až	až	k9	až
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Důkazem	důkaz	k1gInSc7	důkaz
tohoto	tento	k3xDgNnSc2	tento
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
nález	nález	k1gInSc1	nález
malované	malovaný	k2eAgFnSc2d1	malovaná
keramiky	keramika	k1gFnSc2	keramika
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
Křenovic	Křenovice	k1gFnPc2	Křenovice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Křenovicích	Křenovice	k1gFnPc6	Křenovice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
listiny	listina	k1gFnSc2	listina
vydané	vydaný	k2eAgFnSc2d1	vydaná
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1305	[number]	k4	1305
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
starostou	starosta	k1gMnSc7	starosta
Jaromír	Jaromír	k1gMnSc1	Jaromír
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ustavujícím	ustavující	k2eAgNnSc6d1	ustavující
zasedání	zasedání	k1gNnSc6	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
zvolen	zvolit	k5eAaPmNgMnS	zvolit
opětovně	opětovně	k6eAd1	opětovně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
dokončit	dokončit	k5eAaPmF	dokončit
velké	velký	k2eAgFnPc4d1	velká
investiční	investiční	k2eAgFnPc4d1	investiční
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
revitalizace	revitalizace	k1gFnSc2	revitalizace
mlýnského	mlýnský	k2eAgInSc2d1	mlýnský
náhonu	náhon	k1gInSc2	náhon
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
kapacity	kapacita	k1gFnSc2	kapacita
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
bydlení	bydlení	k1gNnSc2	bydlení
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
Smírčí	smírčí	k2eAgInSc1d1	smírčí
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
Krchůvku	krchůvek	k1gInSc6	krchůvek
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Křenovice	Křenovice	k1gFnSc2	Křenovice
mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
silničních	silniční	k2eAgInPc2d1	silniční
výjezdů	výjezd	k1gInPc2	výjezd
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
silnicemi	silnice	k1gFnPc7	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Křenovic	Křenovice	k1gFnPc2	Křenovice
vede	vést	k5eAaImIp3nS	vést
dálnice	dálnice	k1gFnSc1	dálnice
D1	D1	k1gFnSc1	D1
a	a	k8xC	a
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
jsou	být	k5eAaImIp3nP	být
Křenovice	Křenovice	k1gFnPc1	Křenovice
od	od	k7c2	od
západu	západ	k1gInSc2	západ
spojeny	spojit	k5eAaPmNgInP	spojit
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
417	[number]	k4	417
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
brněnských	brněnský	k2eAgFnPc2d1	brněnská
Tuřan	Tuřana	k1gFnPc2	Tuřana
a	a	k8xC	a
končící	končící	k2eAgFnPc1d1	končící
u	u	k7c2	u
křenovického	křenovický	k2eAgInSc2d1	křenovický
kostela	kostel	k1gInSc2	kostel
křížením	křížení	k1gNnSc7	křížení
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
416	[number]	k4	416
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
tvořící	tvořící	k2eAgFnSc4d1	tvořící
silniční	silniční	k2eAgFnSc4d1	silniční
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
Křenovicemi	Křenovice	k1gFnPc7	Křenovice
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
činí	činit	k5eAaImIp3nS	činit
13,4	[number]	k4	13,4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
obcí	obec	k1gFnSc7	obec
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
trase	trasa	k1gFnSc6	trasa
je	být	k5eAaImIp3nS	být
Prace	Prace	k1gFnSc1	Prace
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
protínající	protínající	k2eAgFnSc2d1	protínající
Křenovice	Křenovice	k1gFnSc2	Křenovice
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
416	[number]	k4	416
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
obec	obec	k1gFnSc1	obec
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Slavkovem	Slavkov	k1gInSc7	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
sídly	sídlo	k1gNnPc7	sídlo
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
kilometry	kilometr	k1gInPc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Vesnicí	vesnice	k1gFnSc7	vesnice
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
západu	západ	k1gInSc3	západ
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
směru	směr	k1gInSc6	směr
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Újezda	Újezdo	k1gNnSc2	Újezdo
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
8,2	[number]	k4	8,2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
416	[number]	k4	416
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
silnice	silnice	k1gFnSc2	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4164	[number]	k4	4164
vedoucí	vedoucí	k1gMnSc1	vedoucí
do	do	k7c2	do
Zbýšova	Zbýšův	k2eAgNnSc2d1	Zbýšův
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
2	[number]	k4	2
kilometry	kilometr	k1gInPc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
416	[number]	k4	416
komunikace	komunikace	k1gFnSc2	komunikace
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4161	[number]	k4	4161
vedoucí	vedoucí	k1gMnSc1	vedoucí
do	do	k7c2	do
Holubic	holubice	k1gFnPc2	holubice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
vzdáleny	vzdálen	k2eAgInPc1d1	vzdálen
4,4	[number]	k4	4,4
km	km	kA	km
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
u	u	k7c2	u
nájezdu	nájezd	k1gInSc2	nájezd
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
cestou	cesta	k1gFnSc7	cesta
vedoucí	vedoucí	k1gFnSc2	vedoucí
z	z	k7c2	z
Křenovic	Křenovice	k1gFnPc2	Křenovice
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnPc4d1	místní
komunikace	komunikace	k1gFnPc4	komunikace
mířící	mířící	k2eAgFnPc4d1	mířící
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrek	směrek	k1gMnSc1	směrek
silnici	silnice	k1gFnSc3	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
Velešovice	Velešovice	k1gFnPc4	Velešovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Křenovicích	Křenovice	k1gFnPc6	Křenovice
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
dvě	dva	k4xCgFnPc1	dva
železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
Brno	Brno	k1gNnSc1	Brno
<g/>
-	-	kIx~	-
<g/>
Přerov	Přerov	k1gInSc1	Přerov
(	(	kIx(	(
<g/>
Křenovice	Křenovice	k1gFnSc1	Křenovice
horní	horní	k2eAgNnSc1d1	horní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vlárská	vlárský	k2eAgFnSc1d1	Vlárská
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
Křenovice	Křenovice	k1gFnSc1	Křenovice
dolní	dolní	k2eAgNnSc1d1	dolní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
Křenovicích	Křenovice	k1gFnPc6	Křenovice
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
spolek	spolek	k1gInSc1	spolek
Otakar	Otakara	k1gFnPc2	Otakara
<g/>
.	.	kIx.	.
</s>
<s>
Zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
v	v	k7c6	v
obecní	obecní	k2eAgFnSc6d1	obecní
kronice	kronika	k1gFnSc6	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
knihovna	knihovna	k1gFnSc1	knihovna
Křenovice	Křenovice	k1gFnSc1	Křenovice
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
je	být	k5eAaImIp3nS	být
zmiňována	zmiňován	k2eAgFnSc1d1	zmiňována
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gFnSc1	její
sbírka	sbírka	k1gFnSc1	sbírka
čítala	čítat	k5eAaImAgFnS	čítat
622	[number]	k4	622
knižních	knižní	k2eAgInPc2d1	knižní
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
občanů	občan	k1gMnPc2	občan
nebyl	být	k5eNaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
nepotřebovala	potřebovat	k5eNaImAgFnS	potřebovat
tedy	tedy	k9	tedy
využívat	využívat	k5eAaPmF	využívat
služeb	služba	k1gFnPc2	služba
veřejné	veřejný	k2eAgFnSc2d1	veřejná
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
staly	stát	k5eAaPmAgFnP	stát
prostory	prostora	k1gFnPc1	prostora
bývalého	bývalý	k2eAgInSc2d1	bývalý
Místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
také	také	k9	také
zavedení	zavedení	k1gNnSc4	zavedení
správy	správa	k1gFnSc2	správa
studenty	student	k1gMnPc7	student
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
knihovna	knihovna	k1gFnSc1	knihovna
přestěhován	přestěhován	k2eAgInSc4d1	přestěhován
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
Na	na	k7c6	na
Liškově	Liškův	k2eAgFnSc6d1	Liškova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
větší	veliký	k2eAgNnSc4d2	veliký
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1987	[number]	k4	1987
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
vedení	vedení	k1gNnSc1	vedení
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stanula	stanout	k5eAaPmAgFnS	stanout
paní	paní	k1gFnSc1	paní
Marušincová	Marušincový	k2eAgFnSc1d1	Marušincová
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
vedla	vést	k5eAaImAgFnS	vést
knihovnu	knihovna	k1gFnSc4	knihovna
paní	paní	k1gFnSc1	paní
Filípková	Filípková	k1gFnSc1	Filípková
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
vedla	vést	k5eAaImAgFnS	vést
knihovnu	knihovna	k1gFnSc4	knihovna
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
paní	paní	k1gFnSc1	paní
Kučerová	Kučerová	k1gFnSc1	Kučerová
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
paní	paní	k1gFnSc1	paní
Marková	Marková	k1gFnSc1	Marková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
paní	paní	k1gFnSc1	paní
Zourková	Zourková	k1gFnSc1	Zourková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
Hana	Hana	k1gFnSc1	Hana
Doležalová	Doležalová	k1gFnSc1	Doležalová
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
nárůst	nárůst	k1gInSc1	nárůst
knižního	knižní	k2eAgInSc2d1	knižní
fondu	fond	k1gInSc2	fond
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
spojením	spojení	k1gNnSc7	spojení
s	s	k7c7	s
knihovnami	knihovna	k1gFnPc7	knihovna
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
Hrušky	hruška	k1gFnSc2	hruška
<g/>
,	,	kIx,	,
Šaratice	šaratice	k1gFnSc2	šaratice
<g/>
,	,	kIx,	,
Hostěrádky	Hostěrádka	k1gFnSc2	Hostěrádka
a	a	k8xC	a
Zbýšov	Zbýšov	k1gInSc1	Zbýšov
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
knihovna	knihovna	k1gFnSc1	knihovna
nabyla	nabýt	k5eAaPmAgFnS	nabýt
střediskové	střediskový	k2eAgFnPc4d1	středisková
působnosti	působnost	k1gFnPc4	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Liškově	Liškův	k2eAgMnSc6d1	Liškův
se	se	k3xPyFc4	se
knihovna	knihovna	k1gFnSc1	knihovna
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
do	do	k7c2	do
Svárovské	Svárovské	k2eAgFnSc2d1	Svárovské
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
stržení	stržení	k1gNnSc1	stržení
původní	původní	k2eAgFnSc2d1	původní
budovy	budova	k1gFnSc2	budova
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
havarijního	havarijní	k2eAgInSc2d1	havarijní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
bylo	být	k5eAaImAgNnS	být
zlepšováno	zlepšován	k2eAgNnSc1d1	zlepšováno
její	její	k3xOp3gNnSc4	její
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
nové	nový	k2eAgInPc4d1	nový
regály	regál	k1gInPc4	regál
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgNnSc4d1	elektrické
vytápění	vytápění	k1gNnSc4	vytápění
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
počítač	počítač	k1gInSc1	počítač
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
Svárovské	Svárovské	k2eAgFnSc6d1	Svárovské
ulici	ulice	k1gFnSc6	ulice
bylo	být	k5eAaImAgNnS	být
uvažováno	uvažován	k2eAgNnSc1d1	uvažováno
jako	jako	k8xS	jako
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
začalo	začít	k5eAaPmAgNnS	začít
jevit	jevit	k5eAaImF	jevit
jako	jako	k9	jako
stálé	stálý	k2eAgMnPc4d1	stálý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
zahájila	zahájit	k5eAaPmAgFnS	zahájit
obnovený	obnovený	k2eAgInSc4d1	obnovený
provoz	provoz	k1gInSc4	provoz
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
přístavba	přístavba	k1gFnSc1	přístavba
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
prostory	prostora	k1gFnPc4	prostora
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
v	v	k7c6	v
předešlém	předešlý	k2eAgNnSc6d1	předešlé
sídle	sídlo	k1gNnSc6	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáři	čtenář	k1gMnPc1	čtenář
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
využít	využít	k5eAaPmF	využít
veřejného	veřejný	k2eAgInSc2d1	veřejný
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
nemohoucím	mohoucí	k2eNgInSc6d1	nemohoucí
je	být	k5eAaImIp3nS	být
zajišťován	zajišťovat	k5eAaImNgInS	zajišťovat
též	též	k9	též
rozvoz	rozvoz	k1gInSc1	rozvoz
knih	kniha	k1gFnPc2	kniha
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
válečný	válečný	k2eAgMnSc1d1	válečný
letec	letec	k1gMnSc1	letec
</s>
