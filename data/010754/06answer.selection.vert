<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jitřenka	jitřenka	k1gFnSc1	jitřenka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
večernice	večernice	k1gFnSc1	večernice
<g/>
"	"	kIx"	"
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zdaleka	zdaleka	k6eAd1	zdaleka
nejsilnější	silný	k2eAgInSc4d3	nejsilnější
bodový	bodový	k2eAgInSc4d1	bodový
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
a	a	k8xC	a
Měsíci	měsíc	k1gInSc6	měsíc
o	o	k7c4	o
magnitudě	magnitudě	k6eAd1	magnitudě
−	−	k?	−
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
