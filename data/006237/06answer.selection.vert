<s>
Kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
komplex	komplex	k1gInSc1	komplex
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brněnská	brněnský	k2eAgNnPc4d1	brněnské
trojčata	trojče	k1gNnPc4	trojče
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
trojici	trojice	k1gFnSc4	trojice
administrativních	administrativní	k2eAgFnPc2d1	administrativní
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Brně-Veveří	Brně-Veveří	k1gNnSc6	Brně-Veveří
v	v	k7c6	v
Šumavské	šumavský	k2eAgFnSc6d1	Šumavská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
