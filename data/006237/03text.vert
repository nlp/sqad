<s>
Kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
komplex	komplex	k1gInSc1	komplex
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brněnská	brněnský	k2eAgNnPc4d1	brněnské
trojčata	trojče	k1gNnPc4	trojče
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
trojici	trojice	k1gFnSc4	trojice
administrativních	administrativní	k2eAgFnPc2d1	administrativní
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Brně-Veveří	Brně-Veveří	k1gNnSc6	Brně-Veveří
v	v	k7c6	v
Šumavské	šumavský	k2eAgFnSc6d1	Šumavská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc4	komplex
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
projektová	projektový	k2eAgFnSc1d1	projektová
kancelář	kancelář	k1gFnSc1	kancelář
Stavoprojekt	Stavoprojekt	k1gInSc4	Stavoprojekt
Brno	Brno	k1gNnSc4	Brno
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Zajíce	Zajíc	k1gMnSc4	Zajíc
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
budovy	budova	k1gFnPc4	budova
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
jsou	být	k5eAaImIp3nP	být
tvaru	tvar	k1gInSc2	tvar
kvádru	kvádr	k1gInSc2	kvádr
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
69	[number]	k4	69
×	×	k?	×
24	[number]	k4	24
×	×	k?	×
22	[number]	k4	22
metru	metr	k1gInSc2	metr
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
19	[number]	k4	19
nadzemních	nadzemní	k2eAgMnPc2d1	nadzemní
a	a	k8xC	a
2	[number]	k4	2
podzemní	podzemní	k2eAgFnPc4d1	podzemní
patra	patro	k1gNnPc4	patro
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
železobetonové	železobetonový	k2eAgFnPc1d1	železobetonová
konstrukce	konstrukce	k1gFnPc1	konstrukce
se	s	k7c7	s
stužujícími	stužující	k2eAgInPc7d1	stužující
prvky	prvek	k1gInPc7	prvek
okolo	okolo	k7c2	okolo
výtahových	výtahový	k2eAgFnPc2d1	výtahová
šachet	šachta	k1gFnPc2	šachta
<g/>
,	,	kIx,	,
opláštění	opláštěný	k2eAgMnPc1d1	opláštěný
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
boletické	boletický	k2eAgInPc1d1	boletický
panely	panel	k1gInPc1	panel
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
te	te	k?	te
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budovách	budova	k1gFnPc6	budova
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
mnoho	mnoho	k4c4	mnoho
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Ředitelství	ředitelství	k1gNnSc1	ředitelství
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Projekční	projekční	k2eAgFnPc1d1	projekční
práce	práce	k1gFnPc1	práce
začaly	začít	k5eAaPmAgFnP	začít
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
budova	budova	k1gFnSc1	budova
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
B	B	kA	B
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
a	a	k8xC	a
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
budova	budova	k1gFnSc1	budova
A	a	k9	a
a	a	k8xC	a
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc1d1	poslední
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
budova	budova	k1gFnSc1	budova
C.	C.	kA	C.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
dvoupodlažní	dvoupodlažní	k2eAgFnSc1d1	dvoupodlažní
spojovací	spojovací	k2eAgFnSc1d1	spojovací
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
všechny	všechen	k3xTgFnPc4	všechen
3	[number]	k4	3
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kancelářský	kancelářský	k2eAgInSc4d1	kancelářský
komplex	komplex	k1gInSc4	komplex
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
komplex	komplex	k1gInSc1	komplex
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
v	v	k7c4	v
Encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
dějin	dějiny	k1gFnPc2	dějiny
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
</s>
