<s>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
(	(	kIx(	(
<g/>
Bitis	Bitis	k1gFnSc1	Bitis
arietans	arietans	k1gInSc1	arietans
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgMnSc1d3	nejrozšířenější
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
had	had	k1gMnSc1	had
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
kromě	kromě	k7c2	kromě
Sahary	Sahara	k1gFnSc2	Sahara
a	a	k8xC	a
tropických	tropický	k2eAgFnPc2d1	tropická
džunglí	džungle	k1gFnPc2	džungle
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
agresivního	agresivní	k2eAgMnSc4d1	agresivní
tvora	tvor	k1gMnSc4	tvor
a	a	k8xC	a
připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nejvíce	nejvíce	k6eAd1	nejvíce
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
afrických	africký	k2eAgMnPc2d1	africký
hadů	had	k1gMnPc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
je	být	k5eAaImIp3nS	být
zavalitý	zavalitý	k2eAgMnSc1d1	zavalitý
had	had	k1gMnSc1	had
s	s	k7c7	s
trojúhelníkovou	trojúhelníkový	k2eAgFnSc7d1	trojúhelníková
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvykle	obvykle	k6eAd1	obvykle
délky	délka	k1gFnSc2	délka
okolo	okolo	k7c2	okolo
1	[number]	k4	1
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
kusy	kus	k1gInPc1	kus
měřily	měřit	k5eAaImAgInP	měřit
1,9	[number]	k4	1,9
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
i	i	k9	i
přes	přes	k7c4	přes
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
jsou	být	k5eAaImIp3nP	být
celkově	celkově	k6eAd1	celkově
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
většinou	většina	k1gFnSc7	většina
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
poněkud	poněkud	k6eAd1	poněkud
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgInSc4d2	delší
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnPc1	zbarvení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
výskytu	výskyt	k1gInSc2	výskyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
černé	černá	k1gFnSc2	černá
po	po	k7c4	po
světle	světle	k6eAd1	světle
béžovou	béžový	k2eAgFnSc4d1	béžová
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
či	či	k8xC	či
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
poměrně	poměrně	k6eAd1	poměrně
výraznou	výrazný	k2eAgFnSc4d1	výrazná
kresbu	kresba	k1gFnSc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
poddruhy	poddruh	k1gInPc1	poddruh
tohoto	tento	k3xDgMnSc2	tento
hada	had	k1gMnSc2	had
<g/>
:	:	kIx,	:
Bitis	Bitis	k1gFnSc1	Bitis
arietans	arietans	k1gInSc1	arietans
arietans	arietans	k1gInSc1	arietans
(	(	kIx(	(
<g/>
zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
kapská	kapský	k2eAgFnSc1d1	kapská
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bitis	Bitis	k1gFnSc1	Bitis
arietans	arietans	k6eAd1	arietans
somalica	somalic	k2eAgFnSc1d1	somalic
(	(	kIx(	(
<g/>
zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
somálská	somálský	k2eAgFnSc1d1	Somálská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nežije	žít	k5eNaImIp3nS	žít
jen	jen	k9	jen
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
(	(	kIx(	(
<g/>
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
malé	malý	k2eAgFnSc6d1	malá
lokalitě	lokalita	k1gFnSc6	lokalita
v	v	k7c6	v
Nigeru	Niger	k1gInSc6	Niger
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgMnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vysvětlen	vysvětlen	k2eAgMnSc1d1	vysvětlen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tropických	tropický	k2eAgInPc2d1	tropický
deštných	deštný	k2eAgInPc2d1	deštný
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
při	při	k7c6	při
středomořském	středomořský	k2eAgNnSc6d1	středomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
v	v	k7c6	v
části	část	k1gFnSc6	část
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
nejhojnější	hojný	k2eAgNnSc1d3	nejhojnější
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejhojnějších	hojný	k2eAgMnPc2d3	nejhojnější
hadů	had	k1gMnPc2	had
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
prostředím	prostředí	k1gNnSc7	prostředí
jsou	být	k5eAaImIp3nP	být
travnaté	travnatý	k2eAgFnSc3d1	travnatá
oblasti	oblast	k1gFnSc3	oblast
sušších	suchý	k2eAgFnPc2d2	sušší
savan	savana	k1gFnPc2	savana
plné	plný	k2eAgNnSc1d1	plné
kamení	kamení	k1gNnSc1	kamení
s	s	k7c7	s
řídkým	řídký	k2eAgInSc7d1	řídký
lesním	lesní	k2eAgInSc7d1	lesní
porostem	porost	k1gInSc7	porost
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
křovin	křovina	k1gFnPc2	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dobře	dobře	k6eAd1	dobře
plavat	plavat	k5eAaImF	plavat
a	a	k8xC	a
umí	umět	k5eAaImIp3nS	umět
i	i	k9	i
šplhat	šplhat	k5eAaImF	šplhat
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
křovinách	křovina	k1gFnPc6	křovina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zdaleka	zdaleka	k6eAd1	zdaleka
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
pevné	pevný	k2eAgFnSc6d1	pevná
suché	suchý	k2eAgFnSc6d1	suchá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
had	had	k1gMnSc1	had
plazit	plazit	k5eAaImF	plazit
překvapivou	překvapivý	k2eAgFnSc7d1	překvapivá
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vyrušena	vyrušen	k2eAgFnSc1d1	vyrušena
a	a	k8xC	a
cítí	cítit	k5eAaImIp3nS	cítit
se	se	k3xPyFc4	se
ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
výrazný	výrazný	k2eAgInSc1d1	výrazný
syčivý	syčivý	k2eAgInSc4d1	syčivý
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
stočí	stočit	k5eAaPmIp3nS	stočit
se	se	k3xPyFc4	se
do	do	k7c2	do
obranného	obranný	k2eAgNnSc2d1	obranné
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
nafukuje	nafukovat	k5eAaImIp3nS	nafukovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
odvozen	odvozen	k2eAgInSc1d1	odvozen
i	i	k8xC	i
její	její	k3xOp3gInSc1	její
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
puff	puff	k1gInSc1	puff
adder	adder	k1gInSc4	adder
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
"	"	kIx"	"
<g/>
nafukovací	nafukovací	k2eAgFnPc4d1	nafukovací
zmije	zmije	k1gFnPc4	zmije
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
noční	noční	k2eAgMnSc1d1	noční
živočich	živočich	k1gMnSc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
v	v	k7c6	v
trávě	tráva	k1gFnSc6	tráva
<g/>
,	,	kIx,	,
křovinách	křovina	k1gFnPc6	křovina
či	či	k8xC	či
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgMnPc1d2	menší
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
převážně	převážně	k6eAd1	převážně
za	za	k7c4	za
zálohy	záloha	k1gFnPc4	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Stočená	stočený	k2eAgFnSc1d1	stočená
do	do	k7c2	do
klubíčka	klubíčko	k1gNnSc2	klubíčko
vyčkává	vyčkávat	k5eAaImIp3nS	vyčkávat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
kořist	kořist	k1gFnSc4	kořist
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
kořist	kořist	k1gFnSc1	kořist
vnímá	vnímat	k5eAaImIp3nS	vnímat
několika	několik	k4yIc7	několik
smysly	smysl	k1gInPc7	smysl
<g/>
:	:	kIx,	:
zrakem	zrak	k1gInSc7	zrak
(	(	kIx(	(
<g/>
pohyb	pohyb	k1gInSc1	pohyb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hmatem	hmat	k1gInSc7	hmat
<g/>
/	/	kIx~	/
<g/>
sluchem	sluch	k1gInSc7	sluch
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
(	(	kIx(	(
<g/>
vibrace	vibrace	k1gFnPc1	vibrace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čichem	čich	k1gInSc7	čich
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
pach	pach	k1gInSc1	pach
<g/>
)	)	kIx)	)
a	a	k8xC	a
infračidlem	infračidlo	k1gNnSc7	infračidlo
(	(	kIx(	(
<g/>
teplo	teplo	k1gNnSc1	teplo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
oběť	oběť	k1gFnSc1	oběť
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zmije	zmije	k1gFnSc1	zmije
bleskurychle	bleskurychle	k6eAd1	bleskurychle
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
<g/>
,	,	kIx,	,
uštkne	uštknout	k5eAaPmIp3nS	uštknout
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
zvíře	zvíře	k1gNnSc1	zvíře
pustí	pustit	k5eAaPmIp3nS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
zasažená	zasažený	k2eAgFnSc1d1	zasažená
jedem	jed	k1gInSc7	jed
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nedostane	dostat	k5eNaPmIp3nS	dostat
dál	daleko	k6eAd2	daleko
než	než	k8xS	než
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Had	had	k1gMnSc1	had
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
stopě	stopa	k1gFnSc6	stopa
a	a	k8xC	a
umírající	umírající	k1gFnSc4	umírající
či	či	k8xC	či
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
oběť	oběť	k1gFnSc4	oběť
sežere	sežrat	k5eAaPmIp3nS	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
produkují	produkovat	k5eAaImIp3nP	produkovat
feromon	feromon	k1gInSc4	feromon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
láká	lákat	k5eAaImIp3nS	lákat
samce	samec	k1gMnSc4	samec
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnPc1	páření
provází	provázet	k5eAaImIp3nP	provázet
jakýsi	jakýsi	k3yIgInSc1	jakýsi
"	"	kIx"	"
<g/>
krční	krční	k2eAgInSc1d1	krční
<g/>
"	"	kIx"	"
zápas	zápas	k1gInSc1	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
je	být	k5eAaImIp3nS	být
vejcoživorodá	vejcoživorodý	k2eAgFnSc1d1	vejcoživorodá
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
asi	asi	k9	asi
o	o	k7c4	o
50	[number]	k4	50
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Rekordním	rekordní	k2eAgInSc7d1	rekordní
vrhem	vrh	k1gInSc7	vrh
bylo	být	k5eAaImAgNnS	být
156	[number]	k4	156
háďat	hádě	k1gNnPc2	hádě
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
čerstvě	čerstvě	k6eAd1	čerstvě
narozených	narozený	k2eAgNnPc2d1	narozené
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
12,5	[number]	k4	12,5
<g/>
-	-	kIx~	-
<g/>
17,5	[number]	k4	17,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hadilov	hadilov	k1gMnSc1	hadilov
písař	písař	k1gMnSc1	písař
<g/>
,	,	kIx,	,
medojed	medojed	k1gMnSc1	medojed
kapský	kapský	k2eAgMnSc1d1	kapský
<g/>
,	,	kIx,	,
mangusty	mangust	k1gInPc4	mangust
<g/>
,	,	kIx,	,
promyky	promyk	k1gInPc4	promyk
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
surikaty	surikata	k1gFnSc2	surikata
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmije	zmije	k1gFnSc1	zmije
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
nejvíce	nejvíce	k6eAd1	nejvíce
uštknutí	uštknutí	k1gNnSc1	uštknutí
lidí	člověk	k1gMnPc2	člověk
mezi	mezi	k7c7	mezi
jedovatými	jedovatý	k2eAgMnPc7d1	jedovatý
hady	had	k1gMnPc7	had
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
hojných	hojný	k2eAgInPc6d1	hojný
počtech	počet	k1gInPc6	počet
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
plachá	plachý	k2eAgFnSc1d1	plachá
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
obvykle	obvykle	k6eAd1	obvykle
neprchá	prchat	k5eNaImIp3nS	prchat
a	a	k8xC	a
přes	přes	k7c4	přes
den	den	k1gInSc4	den
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
často	často	k6eAd1	často
poblíže	poblíže	k7c2	poblíže
vyšlapaných	vyšlapaný	k2eAgFnPc2d1	vyšlapaná
stezek	stezka	k1gFnPc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
jedové	jedový	k2eAgInPc4d1	jedový
zuby	zub	k1gInPc4	zub
(	(	kIx(	(
<g/>
až	až	k9	až
35	[number]	k4	35
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
jedu	jed	k1gInSc2	jed
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
vstřikuje	vstřikovat	k5eAaImIp3nS	vstřikovat
do	do	k7c2	do
oběti	oběť	k1gFnSc2	oběť
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
350	[number]	k4	350
mg	mg	kA	mg
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
však	však	k9	však
až	až	k9	až
750	[number]	k4	750
mg	mg	kA	mg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
zmije	zmije	k1gFnSc2	zmije
útočné	útočný	k2eAgFnSc2d1	útočná
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
30	[number]	k4	30
<g/>
x	x	k?	x
slabší	slabý	k2eAgMnSc1d2	slabší
než	než	k8xS	než
jed	jed	k1gInSc1	jed
nejjedovatějšího	jedovatý	k2eAgMnSc2d3	nejjedovatější
suchozemského	suchozemský	k2eAgMnSc2d1	suchozemský
hada	had	k1gMnSc2	had
taipana	taipan	k1gMnSc2	taipan
či	či	k8xC	či
jed	jed	k1gInSc1	jed
kobry	kobra	k1gFnSc2	kobra
indické	indický	k2eAgFnSc2d1	indická
<g/>
.	.	kIx.	.
</s>
<s>
LD50	LD50	k4	LD50
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,4	[number]	k4	0,4
<g/>
-	-	kIx~	-
<g/>
7,7	[number]	k4	7,7
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
podání-uštknutí	podáníštknutí	k1gNnSc2	podání-uštknutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
však	však	k9	však
vpravuje	vpravovat	k5eAaImIp3nS	vpravovat
více	hodně	k6eAd2	hodně
jedu	jed	k1gInSc2	jed
než	než	k8xS	než
jiní	jiný	k2eAgMnPc1d1	jiný
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
dávka	dávka	k1gFnSc1	dávka
asi	asi	k9	asi
100	[number]	k4	100
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k6eAd1	především
hemoragické	hemoragický	k2eAgInPc4d1	hemoragický
a	a	k8xC	a
destrukční	destrukční	k2eAgInPc4d1	destrukční
cytotoxické	cytotoxický	k2eAgInPc4d1	cytotoxický
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
trombolytické	trombolytický	k2eAgNnSc1d1	trombolytický
a	a	k8xC	a
fibrino	fibrin	k2eAgNnSc1d1	fibrin
<g/>
(	(	kIx(	(
<g/>
geno	geno	k1gNnSc1	geno
<g/>
)	)	kIx)	)
<g/>
lytické	lytický	k2eAgInPc1d1	lytický
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
toxin	toxin	k1gInSc1	toxin
proteinové	proteinový	k2eAgFnSc2d1	proteinová
povahy	povaha	k1gFnSc2	povaha
bitistatin	bitistatina	k1gFnPc2	bitistatina
a	a	k8xC	a
kinin	kinin	k1gInSc1	kinin
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
složky	složka	k1gFnPc1	složka
negativně	negativně	k6eAd1	negativně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
kardiovaskulární	kardiovaskulární	k2eAgInSc4d1	kardiovaskulární
a	a	k8xC	a
hemokoagulační	hemokoagulační	k2eAgInSc4d1	hemokoagulační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
silnému	silný	k2eAgNnSc3d1	silné
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
krvácení	krvácení	k1gNnSc3	krvácení
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
otoky	otok	k1gInPc7	otok
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
rozklad	rozklad	k1gInSc4	rozklad
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
oběhového	oběhový	k2eAgInSc2d1	oběhový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnPc1	postižení
trpí	trpět	k5eAaImIp3nP	trpět
vysokou	vysoký	k2eAgFnSc7d1	vysoká
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
zimnicí	zimnice	k1gFnSc7	zimnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rány	rána	k1gFnSc2	rána
občas	občas	k6eAd1	občas
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
gangréně	gangréna	k1gFnSc3	gangréna
a	a	k8xC	a
končetinu	končetina	k1gFnSc4	končetina
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
amputovat	amputovat	k5eAaBmF	amputovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
antiséra	antiséra	k1gFnSc1	antiséra
nemusí	muset	k5eNaImIp3nS	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
uzdravení	uzdravení	k1gNnSc3	uzdravení
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
antiséra	antiséra	k1gFnSc1	antiséra
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
následující	následující	k2eAgFnPc4d1	následující
látky	látka	k1gFnPc4	látka
<g/>
:	:	kIx,	:
SAIMIR	SAIMIR	kA	SAIMIR
POLYVALENT	POLYVALENT	kA	POLYVALENT
SNAKE	SNAKE	kA	SNAKE
ANTIVENOM	ANTIVENOM	kA	ANTIVENOM
<g/>
,	,	kIx,	,
ANTIREPT	ANTIREPT	kA	ANTIREPT
PASTEUR	PASTEUR	kA	PASTEUR
<g/>
,	,	kIx,	,
FAV-AFRIQUE	FAV-AFRIQUE	k1gFnSc1	FAV-AFRIQUE
<g/>
,	,	kIx,	,
FAVIREPT	FAVIREPT	kA	FAVIREPT
aj.	aj.	kA	aj.
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
na	na	k7c4	na
WildAfrica	WildAfricus	k1gMnSc4	WildAfricus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Uštknutí	uštknutí	k1gNnSc4	uštknutí
zmijí	zmijí	k2eAgFnSc4d1	zmijí
útočnou	útočný	k2eAgFnSc4d1	útočná
Bitis	Bitis	k1gFnSc4	Bitis
arietans	arietansa	k1gFnPc2	arietansa
(	(	kIx(	(
<g/>
Merrem	Merrma	k1gFnPc2	Merrma
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hakl	Hakl	k1gMnSc1	Hakl
TERARKA	TERARKA	kA	TERARKA
<g/>
.	.	kIx.	.
<g/>
NET	NET	kA	NET
"	"	kIx"	"
<g/>
Zmije	zmije	k1gFnSc1	zmije
útočná	útočný	k2eAgFnSc1d1	útočná
Herpetology	herpetolog	k1gMnPc4	herpetolog
of	of	k?	of
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
and	and	k?	and
Eritrea	Eritrea	k1gFnSc1	Eritrea
VALENTA	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
Hadi	had	k1gMnPc1	had
<g/>
:	:	kIx,	:
Intoxikace	intoxikace	k1gFnSc1	intoxikace
<g/>
,	,	kIx,	,
terapie	terapie	k1gFnSc1	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Puff	puff	k1gInSc1	puff
Adder	Adder	k1gMnSc1	Adder
ambush	ambush	k1gMnSc1	ambush
-	-	kIx~	-
Serpent	Serpent	k1gMnSc1	Serpent
-	-	kIx~	-
BBC	BBC	kA	BBC
Animals	Animals	k1gInSc1	Animals
</s>
