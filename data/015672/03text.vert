<s>
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
Hluboké	Hluboká	k1gFnSc2
údolí	údolí	k1gNnSc2
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Lomec	Lomec	k1gMnSc1
a	a	k8xC
TřebonínZákladní	TřebonínZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
12,5	12,5	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
16,3	16,3	k4
km²	km²	k?
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Hydrologické	hydrologický	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc4
</s>
<s>
1-04-01-013	1-04-01-013	k4
Pramen	pramen	k1gInSc1
</s>
<s>
rybník	rybník	k1gInSc4
Pohan	pohana	k1gFnPc2
49	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
15,67	15,67	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
12,58	12,58	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
přibližně	přibližně	k6eAd1
474	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
do	do	k7c2
Klejnárky	Klejnárka	k1gFnSc2
u	u	k7c2
Vodrant	Vodrant	k1gMnSc1
49	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
7,52	7,52	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
19,01	19,01	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
přibližně	přibližně	k6eAd1
260	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Okres	okres	k1gInSc1
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Labe	Labe	k1gNnSc1
<g/>
,	,	kIx,
Klejnárka	Klejnárka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
je	být	k5eAaImIp3nS
levostranný	levostranný	k2eAgInSc1d1
přítok	přítok	k1gInSc1
řeky	řeka	k1gFnSc2
Klejnárky	Klejnárka	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Kutná	kutný	k2eAgFnSc1d1
</s>
<s>
Hora	hora	k1gFnSc1
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
toku	tok	k1gInSc2
činí	činit	k5eAaImIp3nS
12,5	12,5	k4
kilometru	kilometr	k1gInSc2
a	a	k8xC
z	z	k7c2
hlediska	hledisko	k1gNnSc2
významnosti	významnost	k1gFnSc2
</s>
<s>
toku	tok	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
databázi	databáze	k1gFnSc6
Výzkumného	výzkumný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
vodohospodářského	vodohospodářský	k2eAgMnSc2d1
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
řazen	řadit	k5eAaImNgInS
mezi	mezi	k7c4
páteřní	páteřní	k2eAgFnPc4d1
toky	toka	k1gFnPc4
základního	základní	k2eAgNnSc2d1
hydrologického	hydrologický	k2eAgNnSc2d1
povodí	povodí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Plocha	plocha	k1gFnSc1
povodí	povodí	k1gNnSc2
měří	měřit	k5eAaImIp3nS
16,3	16,3	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
vytéká	vytékat	k5eAaImIp3nS
z	z	k7c2
rybníka	rybník	k1gInSc2
Pohan	pohana	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
jen	jen	k9
asi	asi	k9
200	#num#	k4
metrů	metr	k1gInPc2
severně	severně	k6eAd1
od	od	k7c2
mnohem	mnohem	k6eAd1
známějšího	známý	k2eAgInSc2d2
a	a	k8xC
většího	veliký	k2eAgInSc2d2
rybníka	rybník	k1gInSc2
Katlov	Katlov	k1gInSc1
<g/>
,	,	kIx,
asi	asi	k9
1,5	1,5	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
Červených	Červených	k2eAgFnPc2d1
Janovic	Janovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
úzké	úzký	k2eAgNnSc1d1
protáhlé	protáhlý	k2eAgNnSc1d1
povodí	povodí	k1gNnSc1
a	a	k8xC
většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gInPc2
přítoků	přítok	k1gInPc2
jsou	být	k5eAaImIp3nP
jen	jen	k9
krátké	krátká	k1gFnPc1
a	a	k8xC
malé	malý	k2eAgInPc1d1
toky	tok	k1gInPc1
(	(	kIx(
<g/>
často	často	k6eAd1
bezejmenné	bezejmenný	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
většinu	většina	k1gFnSc4
délky	délka	k1gFnSc2
teče	téct	k5eAaImIp3nS
převážně	převážně	k6eAd1
severovýchodním	severovýchodní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
z	z	k7c2
Kutnohorské	kutnohorský	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
do	do	k7c2
Čáslavské	Čáslavské	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
přímo	přímo	k6eAd1
neprotéká	protékat	k5eNaImIp3nS
ani	ani	k8xC
jedinou	jediný	k2eAgFnSc7d1
vesnicí	vesnice	k1gFnSc7
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
na	na	k7c6
okolních	okolní	k2eAgFnPc6d1
rovinách	rovina	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
potoce	potok	k1gInSc6
byly	být	k5eAaImAgInP
mlýny	mlýn	k1gInPc1
a	a	k8xC
větší	veliký	k2eAgInSc1d2
počet	počet	k1gInSc1
rybníků	rybník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
(	(	kIx(
<g/>
od	od	k7c2
rybníka	rybník	k1gInSc2
Pohan	pohana	k1gFnPc2
až	až	k9
po	po	k7c4
rybník	rybník	k1gInSc4
Medenice	medenice	k1gFnSc2
<g/>
)	)	kIx)
teče	téct	k5eAaImIp3nS
rovinatou	rovinatý	k2eAgFnSc7d1
zemědělskou	zemědělský	k2eAgFnSc7d1
krajinou	krajina	k1gFnSc7
Červenojanovicka	Červenojanovicko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
rybníka	rybník	k1gInSc2
Medenice	medenice	k1gFnSc2
se	se	k3xPyFc4
obrací	obracet	k5eAaImIp3nS
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c7
rybníkem	rybník	k1gInSc7
Karásek	Karásek	k1gMnSc1
(	(	kIx(
<g/>
východně	východně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Lomec	Lomec	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
původnímu	původní	k2eAgInSc3d1
severovýchodnímu	severovýchodní	k2eAgInSc3d1
směru	směr	k1gInSc3
toku	tok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
protéká	protékat	k5eAaImIp3nS
hlubokým	hluboký	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
s	s	k7c7
bývalými	bývalý	k2eAgInPc7d1
podzemními	podzemní	k2eAgInPc7d1
lomy	lom	k1gInPc7
na	na	k7c4
vápenec	vápenec	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
tohoto	tento	k3xDgNnSc2
údolí	údolí	k1gNnSc2
vtéká	vtékat	k5eAaImIp3nS
do	do	k7c2
Čáslavské	Čáslavské	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dalších	další	k2eAgInPc6d1
asi	asi	k9
dvou	dva	k4xCgInPc2
kilometrech	kilometr	k1gInPc6
<g/>
,	,	kIx,
u	u	k7c2
obce	obec	k1gFnSc2
Vodranty	Vodrant	k1gMnPc4
ústí	ústit	k5eAaImIp3nS
zleva	zleva	k6eAd1
do	do	k7c2
řeky	řeka	k1gFnSc2
Klejnárky	Klejnárka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
jejím	její	k3xOp3gInSc6
22,3	22,3	k4
říčním	říční	k2eAgInSc6d1
kilometru	kilometr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rybníky	rybník	k1gInPc1
na	na	k7c6
toku	tok	k1gInSc6
</s>
<s>
Částečně	částečně	k6eAd1
vypuštěný	vypuštěný	k2eAgInSc1d1
rybník	rybník	k1gInSc1
Turkovec	Turkovec	k1gInSc1
</s>
<s>
Výlov	výlov	k1gInSc4
rybníka	rybník	k1gInSc2
Karásek	Karásek	k1gMnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
délce	délka	k1gFnSc3
toku	tok	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
Medenickém	Medenický	k2eAgInSc6d1
potoku	potok	k1gInSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velký	velký	k2eAgInSc1d1
počet	počet	k1gInSc1
rybníků	rybník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Relativně	relativně	k6eAd1
malý	malý	k2eAgInSc1d1
rybník	rybník	k1gInSc1
Pohan	pohana	k1gFnPc2
<g/>
:	:	kIx,
zde	zde	k6eAd1
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
začíná	začínat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
tok	tok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc4
rybník	rybník	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
potok	potok	k1gInSc1
protéká	protékat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
další	další	k2eAgInSc4d1
malý	malý	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
,	,	kIx,
Březinský	Březinský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
necelý	celý	k2eNgInSc4d1
kilometr	kilometr	k1gInSc4
od	od	k7c2
malých	malý	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
Chvalov	Chvalov	k1gInSc1
a	a	k8xC
Plhov	Plhov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
má	mít	k5eAaImIp3nS
1,4	1,4	k4
hektaru	hektar	k1gInSc2
<g/>
,	,	kIx,
celkovým	celkový	k2eAgNnSc7d1
objem	objem	k1gInSc4
18	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
,	,	kIx,
retenční	retenční	k2eAgInSc4d1
objem	objem	k1gInSc4
8	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rybník	rybník	k1gInSc1
Zápora	zápora	k1gFnSc1
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
malý	malý	k2eAgInSc1d1
rybník	rybník	k1gInSc1
podobné	podobný	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
asi	asi	k9
o	o	k7c4
700	#num#	k4
metrů	metr	k1gInPc2
po	po	k7c6
proudu	proud	k1gInSc6
(	(	kIx(
<g/>
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
od	od	k7c2
silnice	silnice	k1gFnSc2
Červené	Červená	k1gFnPc1
Janovice	Janovice	k1gFnPc1
–	–	k?
Plhov	Plhov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nedovedlo	dovést	k5eNaPmAgNnS
<g/>
:	:	kIx,
jeho	jeho	k3xOp3gFnSc1
vodní	vodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
jen	jen	k9
asi	asi	k9
150	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
hrází	hráz	k1gFnSc7
rybníka	rybník	k1gInSc2
Zápora	zápora	k1gFnSc1
a	a	k8xC
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
10,3	10,3	k4
hektarů	hektar	k1gInPc2
<g/>
,	,	kIx,
celkovým	celkový	k2eAgInSc7d1
objemem	objem	k1gInSc7
100	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
,	,	kIx,
retenčním	retenční	k2eAgInSc7d1
objemem	objem	k1gInSc7
53	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
je	být	k5eAaImIp3nS
největší	veliký	k2eAgInSc4d3
rybník	rybník	k1gInSc4
na	na	k7c6
celém	celý	k2eAgInSc6d1
toku	tok	k1gInSc6
Medeninského	Medeninský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Nejbližší	blízký	k2eAgFnSc1d3
obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
Zadní	zadní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
ne	ne	k9
1	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
rybníka	rybník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kukle	kukle	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
asi	asi	k9
300	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
hrází	hráz	k1gFnSc7
rybníka	rybník	k1gInSc2
Nedovedlo	dovést	k5eNaPmAgNnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
další	další	k2eAgInSc1d1
malý	malý	k2eAgInSc1d1
rybník	rybník	k1gInSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
má	mít	k5eAaImIp3nS
1,7	1,7	k4
hektaru	hektar	k1gInSc2
<g/>
,	,	kIx,
celkový	celkový	k2eAgInSc4d1
objem	objem	k1gInSc4
18	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
,	,	kIx,
retenční	retenční	k2eAgInSc4d1
objem	objem	k1gInSc4
8	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turkovec	Turkovec	k1gInSc1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
poměrně	poměrně	k6eAd1
velký	velký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
4	#num#	k4
hektary	hektar	k1gInPc4
<g/>
,	,	kIx,
celkový	celkový	k2eAgInSc4d1
objem	objem	k1gInSc4
80	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
,	,	kIx,
retenční	retenční	k2eAgInSc4d1
objem	objem	k1gInSc4
27	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
severozápadně	severozápadně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Lány	lán	k1gInPc1
<g/>
,	,	kIx,
podél	podél	k7c2
silnice	silnice	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
339	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zde	zde	k6eAd1
vede	vést	k5eAaImIp3nS
od	od	k7c2
Červených	Červených	k2eAgFnPc2d1
Janovic	Janovice	k1gFnPc2
přes	přes	k7c4
obec	obec	k1gFnSc4
Lány	lán	k1gInPc1
do	do	k7c2
Čáslavi	Čáslav	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Medenice	medenice	k1gFnSc1
<g/>
:	:	kIx,
začíná	začínat	k5eAaImIp3nS
asi	asi	k9
pouhých	pouhý	k2eAgInPc2d1
300	#num#	k4
metrů	metr	k1gInPc2
za	za	k7c7
rybníkem	rybník	k1gInSc7
Turkovec	Turkovec	k1gMnSc1
<g/>
,	,	kIx,
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
opět	opět	k6eAd1
podél	podél	k7c2
silnice	silnice	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
339	#num#	k4
(	(	kIx(
<g/>
u	u	k7c2
odbočky	odbočka	k1gFnSc2
na	na	k7c4
obce	obec	k1gFnPc4
Lomeček	Lomečka	k1gFnPc2
a	a	k8xC
Lomec	Lomec	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Medenický	Medenický	k2eAgInSc1d1
potok	potok	k1gInSc1
zde	zde	k6eAd1
mění	měnit	k5eAaImIp3nS
směr	směr	k1gInSc1
svého	svůj	k3xOyFgInSc2
toku	tok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
rozlohy	rozloha	k1gFnSc2
(	(	kIx(
<g/>
7,7	7,7	k4
hektaru	hektar	k1gInSc2
<g/>
)	)	kIx)
jde	jít	k5eAaImIp3nS
o	o	k7c4
druhý	druhý	k4xOgInSc4
největší	veliký	k2eAgInSc4d3
rybník	rybník	k1gInSc4
na	na	k7c6
potoce	potok	k1gInSc6
<g/>
,	,	kIx,
objemem	objem	k1gInSc7
překonává	překonávat	k5eAaImIp3nS
dokonce	dokonce	k9
i	i	k8xC
rybník	rybník	k1gInSc4
Nedovedlo	dovést	k5eNaPmAgNnS
(	(	kIx(
<g/>
celkový	celkový	k2eAgInSc4d1
objem	objem	k1gInSc4
169	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
,	,	kIx,
retenční	retenční	k2eAgInSc1d1
objem	objem	k1gInSc1
je	být	k5eAaImIp3nS
47	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
m³	m³	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Menší	malý	k2eAgInSc1d2
rybník	rybník	k1gInSc1
Karásek	Karásek	k1gMnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
úzkém	úzký	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
<g/>
,	,	kIx,
od	od	k7c2
rybníka	rybník	k1gInSc2
podél	podél	k7c2
cesty	cesta	k1gFnSc2
do	do	k7c2
Lomce	Lomec	k1gInSc2
je	být	k5eAaImIp3nS
menší	malý	k2eAgFnSc1d2
chatová	chatový	k2eAgFnSc1d1
osada	osada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
obce	obec	k1gFnSc2
Třebonín	Třebonín	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
lokalitě	lokalita	k1gFnSc6
Vápenka	vápenka	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
konci	konec	k1gInSc6
erozního	erozní	k2eAgInSc2d1
zářezu	zářez	k1gInSc2
potoka	potok	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1
malý	malý	k2eAgInSc1d1
chovný	chovný	k2eAgInSc1d1
rybník	rybník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
Souňovského	Souňovský	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgInSc4d1
rybník	rybník	k1gInSc4
na	na	k7c6
Medenickém	Medenický	k2eAgInSc6d1
potoce	potok	k1gInSc6
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
na	na	k7c6
náhonu	náhon	k1gInSc6
z	z	k7c2
toho	ten	k3xDgInSc2
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
odděluje	oddělovat	k5eAaImIp3nS
těsně	těsně	k6eAd1
před	před	k7c7
rybníkem	rybník	k1gInSc7
a	a	k8xC
do	do	k7c2
potoka	potok	k1gInSc2
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
asi	asi	k9
pouhých	pouhý	k2eAgInPc2d1
100	#num#	k4
metrů	metr	k1gInPc2
před	před	k7c7
ústím	ústí	k1gNnSc7
potoka	potok	k1gInSc2
do	do	k7c2
Klejnárky	Klejnárka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mlýny	mlýn	k1gInPc1
na	na	k7c6
toku	tok	k1gInSc6
</s>
<s>
Mlýn	mlýn	k1gInSc1
TurkovecKaráskův	TurkovecKaráskův	k2eAgInSc4d1
mlýn	mlýn	k1gInSc4
</s>
<s>
Na	na	k7c6
Medenickém	Medenický	k2eAgInSc6d1
potoku	potok	k1gInSc6
fungovalo	fungovat	k5eAaImAgNnS
několik	několik	k4yIc1
mlýnů	mlýn	k1gInPc2
a	a	k8xC
protože	protože	k8xS
potok	potok	k1gInSc1
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
vodný	vodný	k2eAgMnSc1d1
<g/>
,	,	kIx,
sloužily	sloužit	k5eAaImAgFnP
k	k	k7c3
akumulaci	akumulace	k1gFnSc3
vody	voda	k1gFnSc2
četné	četný	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
popsané	popsaný	k2eAgFnSc2d1
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
.	.	kIx.
</s>
<s>
Mlýn	mlýn	k1gInSc1
Turkovec	Turkovec	k1gInSc1
leží	ležet	k5eAaImIp3nS
bezprostředně	bezprostředně	k6eAd1
pod	pod	k7c7
hrází	hráz	k1gFnSc7
stejnojmenného	stejnojmenný	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
nazývaný	nazývaný	k2eAgInSc1d1
též	též	k9
Lánský	lánský	k2eAgInSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
leží	ležet	k5eAaImIp3nS
nedaleko	daleko	k6eNd1
od	od	k7c2
obce	obec	k1gFnSc2
Lány	lán	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
krátkou	krátký	k2eAgFnSc7d1
odbočkou	odbočka	k1gFnSc7
ze	z	k7c2
silnice	silnice	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
339	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
jako	jako	k8xC,k8xS
celoroční	celoroční	k2eAgNnSc1d1
bydlení	bydlení	k1gNnSc1
(	(	kIx(
<g/>
Lány	lán	k1gInPc1
čp.	čp.	k?
28	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Medenický	Medenický	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
pod	pod	k7c7
rybníkem	rybník	k1gInSc7
Medenice	medenice	k1gFnSc2
již	již	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Karáskův	Karáskův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
(	(	kIx(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1
též	též	k9
Pitrův	Pitrův	k2eAgInSc1d1
<g/>
)	)	kIx)
leží	ležet	k5eAaImIp3nS
asi	asi	k9
100	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
hrází	hráz	k1gFnSc7
rybníka	rybník	k1gInSc2
Karásek	Karásek	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zachován	zachovat	k5eAaPmNgInS
v	v	k7c6
poměrně	poměrně	k6eAd1
původním	původní	k2eAgInSc6d1
stavu	stav	k1gInSc6
a	a	k8xC
nyní	nyní	k6eAd1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
rekreačním	rekreační	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Mlýn	mlýn	k1gInSc1
Vápenka	vápenka	k1gFnSc1
u	u	k7c2
Třebonína	Třebonín	k1gInSc2
<g/>
,	,	kIx,
pod	pod	k7c7
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
<g/>
)	)	kIx)
chovným	chovný	k2eAgInSc7d1
rybníčkem	rybníček	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
objekt	objekt	k1gInSc1
prakticky	prakticky	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
stále	stále	k6eAd1
ještě	ještě	k6eAd1
nedokončen	dokončen	k2eNgInSc4d1
přestavbou	přestavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Souňovský	Souňovský	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
Nový	nový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
před	před	k7c7
ústím	ústí	k1gNnSc7
Medenického	Medenický	k2eAgInSc2d1
potoka	potok	k1gInSc2
do	do	k7c2
Klejnárky	Klejnárka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zářez	zářez	k1gInSc1
Medenického	Medenický	k2eAgInSc2d1
potoka	potok	k1gInSc2
a	a	k8xC
podzemní	podzemní	k2eAgInPc4d1
lomy	lom	k1gInPc4
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1
<g/>
/	/	kIx~
<g/>
lom	lom	k1gInSc1
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
s	s	k7c7
podpěrným	podpěrný	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
</s>
<s>
Mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Lomec	Lomec	k1gMnSc1
a	a	k8xC
Třebonín	Třebonín	k1gMnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
Medenický	Medenický	k2eAgInSc4d1
potok	potok	k1gInSc4
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
jinak	jinak	k6eAd1
teče	téct	k5eAaImIp3nS
převážně	převážně	k6eAd1
rovinatou	rovinatý	k2eAgFnSc7d1
krajinou	krajina	k1gFnSc7
<g/>
)	)	kIx)
hluboké	hluboký	k2eAgNnSc1d1
a	a	k8xC
úzké	úzký	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
nejhezčí	hezký	k2eAgFnSc7d3
a	a	k8xC
nejzajímavější	zajímavý	k2eAgFnSc7d3
částí	část	k1gFnSc7
celého	celý	k2eAgInSc2d1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
toku	tok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potok	potok	k1gInSc1
zde	zde	k6eAd1
erozním	erozní	k2eAgInSc7d1
zářezem	zářez	k1gInSc7
proráží	prorážet	k5eAaImIp3nS
okrajem	okraj	k1gInSc7
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
Kutnohorské	kutnohorský	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
do	do	k7c2
Čáslavské	Čáslavské	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celá	k1gFnSc2
údolí	údolí	k1gNnSc2
je	být	k5eAaImIp3nS
zalesněné	zalesněný	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
příkrými	příkrý	k2eAgInPc7d1
svahy	svah	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
i	i	k9
poměrně	poměrně	k6eAd1
vysoké	vysoký	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zářez	zářez	k1gInSc1
končí	končit	k5eAaImIp3nS
u	u	k7c2
chovného	chovný	k2eAgInSc2d1
rybníku	rybník	k1gInSc6
nedaleko	nedaleko	k7c2
Třebonína	Třebonín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
zajímavostí	zajímavost	k1gFnSc7
tohoto	tento	k3xDgNnSc2
údolí	údolí	k1gNnSc2
je	být	k5eAaImIp3nS
výskyt	výskyt	k1gInSc1
krystalického	krystalický	k2eAgInSc2d1
vápence	vápenec	k1gInSc2
(	(	kIx(
<g/>
mramoru	mramor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
kvalitní	kvalitní	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vyplatilo	vyplatit	k5eAaPmAgNnS
dobývat	dobývat	k5eAaImF
ho	on	k3xPp3gInSc4
v	v	k7c6
podzemních	podzemní	k2eAgInPc6d1
lomech	lom	k1gInPc6
důlním	důlní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
dochovaných	dochovaný	k2eAgInPc2d1
lomů	lom	k1gInPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
údolí	údolí	k1gNnSc2
(	(	kIx(
<g/>
blíže	blízce	k6eAd2
k	k	k7c3
Třebonínu	Třebonín	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
20	#num#	k4
až	až	k9
30	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
potokem	potok	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytěžený	vytěžený	k2eAgInSc1d1
prostor	prostor	k1gInSc1
má	mít	k5eAaImIp3nS
celkovou	celkový	k2eAgFnSc4d1
délku	délka	k1gFnSc4
asi	asi	k9
60	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nedošlo	dojít	k5eNaPmAgNnS
ke	k	k7c3
zborcení	zborcený	k2eAgMnPc1d1
jeskyně	jeskyně	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chodba	chodba	k1gFnSc1
podepřena	podepřít	k5eAaPmNgFnS
ponechanými	ponechaný	k2eAgInPc7d1
skalními	skalní	k2eAgInPc7d1
pilíři	pilíř	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
několika	několik	k4yIc2
místech	místo	k1gNnPc6
je	být	k5eAaImIp3nS
patrná	patrný	k2eAgFnSc1d1
též	též	k9
grafitová	grafitový	k2eAgFnSc1d1
žíla	žíla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
podzemní	podzemní	k2eAgInSc4d1
lom	lom	k1gInSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
naopak	naopak	k6eAd1
na	na	k7c6
druhém	druhý	k4xOgInSc6
(	(	kIx(
<g/>
levém	levý	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
již	již	k6eAd1
téměř	téměř	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
menší	malý	k2eAgFnSc6d2
výšce	výška	k1gFnSc6
nad	nad	k7c7
potokem	potok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
lom	lom	k1gInSc4
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc4d1
zatopený	zatopený	k2eAgInSc4d1
<g/>
,	,	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
podzemní	podzemní	k2eAgNnSc1d1
jezírko	jezírko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
potápěči	potápěč	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytěžená	vytěžený	k2eAgFnSc1d1
surovina	surovina	k1gFnSc1
se	se	k3xPyFc4
zpracovávala	zpracovávat	k5eAaImAgFnS
na	na	k7c4
vápno	vápno	k1gNnSc4
v	v	k7c6
nedaleké	daleký	k2eNgFnSc6d1
vápence	vápenka	k1gFnSc6
u	u	k7c2
Třebonína	Třebonín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vápenka	vápenka	k1gFnSc1
již	již	k6eAd1
zcela	zcela	k6eAd1
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
lokalita	lokalita	k1gFnSc1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
na	na	k7c6
mapách	mapa	k1gFnPc6
označena	označen	k2eAgFnSc1d1
"	"	kIx"
<g/>
Vápenka	vápenka	k1gFnSc1
<g/>
"	"	kIx"
a	a	k8xC
odsud	odsud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
i	i	k9
název	název	k1gInSc1
nedalekého	daleký	k2eNgInSc2d1
mlýna	mlýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
<g/>
/	/	kIx~
<g/>
tvrziště	tvrziště	k1gNnSc1
nad	nad	k7c7
údolím	údolí	k1gNnSc7
</s>
<s>
Slovanské	slovanský	k2eAgNnSc1d1
hradiště	hradiště	k1gNnSc1
nad	nad	k7c7
údolím	údolí	k1gNnSc7
</s>
<s>
V	v	k7c6
lese	les	k1gInSc6
nad	nad	k7c7
levým	levý	k2eAgInSc7d1
břehem	břeh	k1gInSc7
údolí	údolí	k1gNnSc2
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Lomec	Lomec	k1gMnSc1
a	a	k8xC
Třebonín	Třebonín	k1gMnSc1
(	(	kIx(
<g/>
až	až	k9
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
metrů	metr	k1gInPc2
nad	nad	k7c7
úrovní	úroveň	k1gFnSc7
potoka	potok	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
celkem	celkem	k6eAd1
tři	tři	k4xCgNnPc4
hradiště	hradiště	k1gNnPc4
či	či	k8xC
tvrziště	tvrziště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
jsou	být	k5eAaImIp3nP
zapsána	zapsat	k5eAaPmNgNnP
do	do	k7c2
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
nemovitých	movitý	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názvy	název	k1gInPc4
pod	pod	k7c7
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
jsou	být	k5eAaImIp3nP
zakresleny	zakreslen	k2eAgFnPc1d1
na	na	k7c6
mapách	mapa	k1gFnPc6
Seznamu	seznam	k1gInSc2
jsou	být	k5eAaImIp3nP
však	však	k9
odlišné	odlišný	k2eAgInPc1d1
od	od	k7c2
názvů	název	k1gInPc2
a	a	k8xC
popisů	popis	k1gInPc2
v	v	k7c6
rejstříku	rejstřík	k1gInSc6
ÚSKP	ÚSKP	kA
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
tak	tak	k9
úplně	úplně	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
číslo	číslo	k1gNnSc1
v	v	k7c6
rejstříku	rejstřík	k1gInSc6
odpovídá	odpovídat	k5eAaImIp3nS
jednotlivým	jednotlivý	k2eAgNnPc3d1
místům	místo	k1gNnPc3
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tvrziště	tvrziště	k1gNnSc1
Lomec	Lomec	k1gInSc1
(	(	kIx(
<g/>
název	název	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
nejblíže	blízce	k6eAd3
obci	obec	k1gFnSc3
Lomec	Lomec	k1gInSc1
(	(	kIx(
<g/>
asi	asi	k9
500	#num#	k4
metrů	metr	k1gInPc2
od	od	k7c2
středu	střed	k1gInSc2
obce	obec	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
prvním	první	k4xOgInSc6
skalním	skalní	k2eAgInSc6d1
ostrohu	ostroh	k1gInSc6
pod	pod	k7c7
Lomcem	Lomec	k1gInSc7
<g/>
,	,	kIx,
víceméně	víceméně	k9
nad	nad	k7c7
ústím	ústí	k1gNnSc7
Lomeckého	Lomecký	k2eAgInSc2d1
potůčku	potůček	k1gInSc2
do	do	k7c2
Medenického	Medenický	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rejstříku	rejstřík	k1gInSc6
ÚSKP	ÚSKP	kA
asi	asi	k9
Tvrz	tvrz	k1gFnSc1
-	-	kIx~
tvrziště	tvrziště	k1gNnSc1
Na	na	k7c4
Zelenici	Zelenice	k1gFnSc4
<g/>
,	,	kIx,
archeologické	archeologický	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
(	(	kIx(
<g/>
Lomec	Lomec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
číslo	číslo	k1gNnSc1
rejstříku	rejstřík	k1gInSc2
ÚSKP	ÚSKP	kA
zřejmě	zřejmě	k6eAd1
28922	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1272	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Terénní	terénní	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
hrádku	hrádek	k1gInSc2
(	(	kIx(
<g/>
název	název	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
dalším	další	k2eAgInSc6d1
ostrohu	ostroh	k1gInSc6
<g/>
,	,	kIx,
asi	asi	k9
700	#num#	k4
metrů	metr	k1gInPc2
východně	východně	k6eAd1
od	od	k7c2
první	první	k4xOgFnSc2
lokality	lokalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
Hrad	hrad	k1gInSc1
-	-	kIx~
Hrádek	hrádek	k1gInSc1
<g/>
,	,	kIx,
archeologické	archeologický	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
(	(	kIx(
<g/>
Lomec	Lomec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
rejstříku	rejstřík	k1gInSc2
ÚSKP	ÚSKP	kA
zřejmě	zřejmě	k6eAd1
38617	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2840	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slovanské	slovanský	k2eAgNnSc1d1
hradiště	hradiště	k1gNnSc1
(	(	kIx(
<g/>
název	název	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
po	po	k7c4
dalších	další	k2eAgNnPc2d1
asi	asi	k9
350	#num#	k4
metrech	metr	k1gInPc6
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc1
lokalita	lokalita	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
nejblíže	blízce	k6eAd3
obci	obec	k1gFnSc3
Třebonín	Třebonína	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
seznamu	seznam	k1gInSc6
památek	památka	k1gFnPc2
je	být	k5eAaImIp3nS
však	však	k9
také	také	k9
evidována	evidován	k2eAgFnSc1d1
pod	pod	k7c7
obcí	obec	k1gFnSc7
Lomec	Lomec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
Výšinné	výšinný	k2eAgNnSc4d1
opevněné	opevněný	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
-	-	kIx~
hradiště	hradiště	k1gNnSc1
Hrádek	hrádek	k1gInSc4
<g/>
,	,	kIx,
archeologické	archeologický	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
<g/>
,	,	kIx,
ostroh	ostroh	k1gInSc4
západně	západně	k6eAd1
od	od	k7c2
Třebonína	Třebonín	k1gInSc2
<g/>
,	,	kIx,
Lomec	Lomec	k1gInSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
rejstříku	rejstřík	k1gInSc2
ÚSKP	ÚSKP	kA
zřejmě	zřejmě	k6eAd1
28809	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1271	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Grafitová	grafitový	k2eAgFnSc1d1
žíla	žíla	k1gFnSc1
v	v	k7c4
jeskyni	jeskyně	k1gFnSc4
<g/>
/	/	kIx~
<g/>
lomu	lom	k1gInSc2
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
</s>
<s>
Zatopená	zatopený	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
<g/>
/	/	kIx~
<g/>
lom	lom	k1gInSc1
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
</s>
<s>
Skály	skála	k1gFnPc1
v	v	k7c6
hlubokém	hluboký	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Lomec	Lomec	k1gMnSc1
a	a	k8xC
Třebonín	Třebonín	k1gMnSc1
</s>
<s>
Obnovený	obnovený	k2eAgInSc1d1
chovný	chovný	k2eAgInSc1d1
rybník	rybník	k1gInSc1
u	u	k7c2
Třebonína	Třebonín	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
</s>
<s>
HEIS	HEIS	kA
VÚV	VÚV	kA
T.	T.	kA
G.	G.	kA
M.	M.	kA
–	–	k?
Vodní	vodní	k2eAgFnSc2d1
toky	toka	k1gFnSc2
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
110	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Hydrologický	hydrologický	k2eAgInSc1d1
seznam	seznam	k1gInSc1
podrobného	podrobný	k2eAgNnSc2d1
členění	členění	k1gNnSc2
povodí	povodí	k1gNnSc2
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
</s>
<s>
Tabulka	tabulka	k1gFnSc1
D	D	kA
<g/>
.8	.8	k4
–	–	k?
Seznam	seznam	k1gInSc1
vodních	vodní	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodit	k5eAaPmIp3nS
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
státní	státní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
140258	#num#	k4
:	:	kIx,
Tvrz	tvrz	k1gFnSc1
-	-	kIx~
tvrziště	tvrziště	k1gNnSc1
Na	na	k7c4
Zelenici	Zelenice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
150610	#num#	k4
:	:	kIx,
Hrad	hrad	k1gInSc1
-	-	kIx~
Hrádek	hrádek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
140141	#num#	k4
:	:	kIx,
Výšinné	výšinný	k2eAgNnSc1d1
opevněné	opevněný	k2eAgNnSc1d1
sídliště	sídliště	k1gNnSc1
-	-	kIx~
hradiště	hradiště	k1gNnSc1
Hrádek	hrádek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Medenický	Medenický	k2eAgInSc4d1
potok	potok	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
