<p>
<s>
Jantar	jantar	k1gInSc1	jantar
je	být	k5eAaImIp3nS	být
lehká	lehký	k2eAgFnSc1d1	lehká
a	a	k8xC	a
křehká	křehký	k2eAgFnSc1d1	křehká
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
forma	forma	k1gFnSc1	forma
uhlíkaté	uhlíkatý	k2eAgFnSc2d1	uhlíkatá
nerostné	nerostný	k2eAgFnSc2d1	nerostná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
řazená	řazený	k2eAgFnSc1d1	řazená
mezi	mezi	k7c4	mezi
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
organogenní	organogenní	k2eAgFnPc4d1	organogenní
sedimentární	sedimentární	k2eAgFnPc4d1	sedimentární
horniny	hornina	k1gFnPc4	hornina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fosilizovanou	fosilizovaný	k2eAgFnSc4d1	fosilizovaná
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
některých	některý	k3yIgInPc2	některý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
mineralizovanou	mineralizovaný	k2eAgFnSc4d1	mineralizovaná
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
třetihorních	třetihorní	k2eAgInPc2d1	třetihorní
jehličnanů	jehličnan	k1gInPc2	jehličnan
starou	starý	k2eAgFnSc4d1	stará
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
však	však	k9	však
známý	známý	k2eAgInSc1d1	známý
jantar	jantar	k1gInSc1	jantar
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
až	až	k9	až
320	[number]	k4	320
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
prvohorního	prvohorní	k2eAgInSc2d1	prvohorní
karbonu	karbon	k1gInSc2	karbon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
jantaru	jantar	k1gInSc2	jantar
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
jako	jako	k9	jako
C	C	kA	C
<g/>
10	[number]	k4	10
<g/>
H	H	kA	H
<g/>
16	[number]	k4	16
<g/>
O.	O.	kA	O.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
barva	barva	k1gFnSc1	barva
jantaru	jantar	k1gInSc2	jantar
je	být	k5eAaImIp3nS	být
zlatavě	zlatavě	k6eAd1	zlatavě
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nalézají	nalézat	k5eAaImIp3nP	nalézat
se	se	k3xPyFc4	se
odrůdy	odrůda	k1gFnPc1	odrůda
zcela	zcela	k6eAd1	zcela
průhledné	průhledný	k2eAgFnPc1d1	průhledná
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
kávové	kávový	k2eAgFnPc1d1	kávová
i	i	k8xC	i
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nepodařilo	podařit	k5eNaPmAgNnS	podařit
jantar	jantar	k1gInSc4	jantar
uměle	uměle	k6eAd1	uměle
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
známo	znám	k2eAgNnSc1d1	známo
jeho	jeho	k3xOp3gNnSc4	jeho
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
i	i	k8xC	i
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
postup	postup	k1gInSc4	postup
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Fosilizovaná	fosilizovaný	k2eAgFnSc1d1	fosilizovaná
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
je	být	k5eAaImIp3nS	být
stará	stará	k1gFnSc1	stará
25	[number]	k4	25
až	až	k9	až
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgFnPc6d1	nepravidelná
vrstvách	vrstva	k1gFnPc6	vrstva
třetihorních	třetihorní	k2eAgInPc2d1	třetihorní
písků	písek	k1gInPc2	písek
a	a	k8xC	a
jílovitých	jílovitý	k2eAgFnPc2d1	jílovitá
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stromy	strom	k1gInPc1	strom
produkují	produkovat	k5eAaImIp3nP	produkovat
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
jako	jako	k8xC	jako
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
a	a	k8xC	a
napadení	napadení	k1gNnPc4	napadení
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
kůru	kůra	k1gFnSc4	kůra
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
napadeny	napaden	k2eAgMnPc4d1	napaden
kůrovci	kůrovec	k1gMnPc7	kůrovec
a	a	k8xC	a
podobným	podobný	k2eAgInSc7d1	podobný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
ztvrdla	ztvrdnout	k5eAaPmAgFnS	ztvrdnout
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
sedimentech	sediment	k1gInPc6	sediment
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
jíl	jíl	k1gInSc4	jíl
a	a	k8xC	a
písek	písek	k1gInSc4	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
lagun	laguna	k1gFnPc2	laguna
nebo	nebo	k8xC	nebo
v	v	k7c6	v
deltách	delta	k1gFnPc6	delta
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
uchovala	uchovat	k5eAaPmAgFnS	uchovat
se	se	k3xPyFc4	se
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
po	po	k7c6	po
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jantar	jantar	k1gInSc1	jantar
je	být	k5eAaImIp3nS	být
organického	organický	k2eAgInSc2d1	organický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
amorfní	amorfní	k2eAgFnSc4d1	amorfní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
stromu	strom	k1gInSc6	strom
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
jantaru	jantar	k1gInSc2	jantar
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
terpeny	terpen	k1gInPc1	terpen
nebo	nebo	k8xC	nebo
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgFnPc1d1	společná
ztvrdlým	ztvrdlý	k2eAgFnPc3d1	ztvrdlá
pryskyřicím	pryskyřice	k1gFnPc3	pryskyřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jantar	jantar	k1gInSc1	jantar
z	z	k7c2	z
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
jehličnanu	jehličnan	k1gInSc2	jehličnan
Pinus	Pinus	k1gInSc1	Pinus
succinifera	succinifera	k1gFnSc1	succinifera
<g/>
,	,	kIx,	,
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
luštěniny	luštěnina	k1gFnSc2	luštěnina
Hymenaea	Hymenaea	k1gMnSc1	Hymenaea
courbaril	courbaril	k1gMnSc1	courbaril
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Třeme	třít	k5eAaImIp1nP	třít
<g/>
-li	i	k?	-li
jantar	jantar	k1gInSc4	jantar
vlněnou	vlněný	k2eAgFnSc7d1	vlněná
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
statické	statický	k2eAgFnSc2d1	statická
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
jantaru	jantar	k1gInSc2	jantar
popsal	popsat	k5eAaPmAgInS	popsat
již	již	k6eAd1	již
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Thales	Thales	k1gMnSc1	Thales
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc4	název
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
pro	pro	k7c4	pro
jantar	jantar	k1gInSc4	jantar
je	být	k5eAaImIp3nS	být
elektron	elektron	k1gInSc1	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jantar	jantar	k1gInSc1	jantar
taje	tát	k5eAaImIp3nS	tát
při	při	k7c6	při
287	[number]	k4	287
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hořlavý	hořlavý	k2eAgMnSc1d1	hořlavý
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
je	být	k5eAaImIp3nS	být
2,9	[number]	k4	2,9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
antice	antika	k1gFnSc6	antika
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
jantar	jantar	k1gInSc1	jantar
mystické	mystický	k2eAgFnSc2d1	mystická
a	a	k8xC	a
magické	magický	k2eAgFnSc2d1	magická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
kulturách	kultura	k1gFnPc6	kultura
byl	být	k5eAaImAgMnS	být
používaný	používaný	k2eAgMnSc1d1	používaný
jako	jako	k8xS	jako
talisman	talisman	k1gInSc1	talisman
nebo	nebo	k8xC	nebo
léčebný	léčebný	k2eAgInSc4d1	léčebný
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
jantaru	jantar	k1gInSc2	jantar
==	==	k?	==
</s>
</p>
<p>
<s>
Jantar	jantar	k1gInSc1	jantar
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
osmi	osm	k4xCc6	osm
barevných	barevný	k2eAgFnPc6d1	barevná
variantách	varianta	k1gFnPc6	varianta
<g/>
:	:	kIx,	:
žlutý	žlutý	k2eAgInSc4d1	žlutý
(	(	kIx(	(
<g/>
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oranžový	oranžový	k2eAgInSc1d1	oranžový
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
,	,	kIx,	,
modrozelený	modrozelený	k2eAgInSc1d1	modrozelený
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
nebo	nebo	k8xC	nebo
mechový	mechový	k2eAgInSc1d1	mechový
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
odstínů	odstín	k1gInPc2	odstín
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
<g/>
,	,	kIx,	,
nejcennější	cenný	k2eAgNnSc1d3	nejcennější
a	a	k8xC	a
nejoceňovanější	oceňovaný	k2eAgMnSc1d3	nejoceňovanější
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
jantar	jantar	k1gInSc1	jantar
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Chiapas	Chiapas	k1gInSc1	Chiapas
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
modrý	modrý	k2eAgInSc1d1	modrý
jantar	jantar	k1gInSc1	jantar
–	–	k?	–
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
a	a	k8xC	a
modravě	modravě	k6eAd1	modravě
fluoreskuje	fluoreskovat	k5eAaImIp3nS	fluoreskovat
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
valchovit	valchovit	k1gInSc1	valchovit
–	–	k?	–
neprůhledná	průhledný	k2eNgFnSc1d1	neprůhledná
odrůda	odrůda	k1gFnSc1	odrůda
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
lokality	lokalita	k1gFnSc2	lokalita
ValchovGagát	ValchovGagát	k1gInSc1	ValchovGagát
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaný	zvaný	k2eAgInSc1d1	zvaný
černý	černý	k2eAgInSc1d1	černý
jantar	jantar	k1gInSc1	jantar
<g/>
,	,	kIx,	,
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jantar	jantar	k1gInSc1	jantar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kvalitě	kvalita	k1gFnSc6	kvalita
byl	být	k5eAaImAgMnS	být
ode	ode	k7c2	ode
dávna	dávno	k1gNnSc2	dávno
komerčně	komerčně	k6eAd1	komerčně
využíván	využívat	k5eAaPmNgInS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
obkladových	obkladový	k2eAgFnPc2d1	obkladová
destiček	destička	k1gFnPc2	destička
<g/>
,	,	kIx,	,
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
dnešní	dnešní	k2eAgNnSc4d1	dnešní
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžba	těžba	k1gFnSc1	těžba
jantaru	jantar	k1gInSc2	jantar
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
v	v	k7c6	v
povrchových	povrchový	k2eAgInPc6d1	povrchový
lomech	lom	k1gInPc6	lom
a	a	k8xC	a
podzemních	podzemní	k2eAgInPc6d1	podzemní
dolech	dol	k1gInPc6	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jantar	jantar	k1gInSc1	jantar
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
médium	médium	k1gNnSc1	médium
<g/>
,	,	kIx,	,
uchovávající	uchovávající	k2eAgInPc1d1	uchovávající
ve	v	k7c6	v
fantastických	fantastický	k2eAgInPc6d1	fantastický
detailech	detail	k1gInPc6	detail
organismy	organismus	k1gInPc7	organismus
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
bezobratlé	bezobratlý	k2eAgMnPc4d1	bezobratlý
živočichy	živočich	k1gMnPc4	živočich
<g/>
)	)	kIx)	)
z	z	k7c2	z
minulých	minulý	k2eAgNnPc2d1	Minulé
geologických	geologický	k2eAgNnPc2d1	geologické
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
jantar	jantar	k1gInSc1	jantar
se	s	k7c7	s
zachovaným	zachovaný	k2eAgInSc7d1	zachovaný
hmyzem	hmyz	k1gInSc7	hmyz
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
druhohorního	druhohorní	k2eAgInSc2d1	druhohorní
triasu	trias	k1gInSc2	trias
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
stáří	stář	k1gFnSc7	stář
asi	asi	k9	asi
230	[number]	k4	230
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
zachování	zachování	k1gNnSc1	zachování
pravěké	pravěký	k2eAgFnSc2d1	pravěká
DNA	DNA	kA	DNA
a	a	k8xC	a
výsledné	výsledný	k2eAgNnSc1d1	výsledné
klonování	klonování	k1gNnSc1	klonování
pravěkých	pravěký	k2eAgInPc2d1	pravěký
organismů	organismus	k1gInPc2	organismus
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
Jurského	jurský	k2eAgInSc2d1	jurský
Parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
čistě	čistě	k6eAd1	čistě
teoretická	teoretický	k2eAgFnSc1d1	teoretická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
sběru	sběr	k1gInSc6	sběr
jantaru	jantar	k1gInSc2	jantar
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
<g/>
,	,	kIx,	,
člověkem	člověk	k1gMnSc7	člověk
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
kus	kus	k1gInSc1	kus
jantaru	jantar	k1gInSc2	jantar
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
30	[number]	k4	30
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
ozdobných	ozdobný	k2eAgInPc2d1	ozdobný
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
jantaru	jantar	k1gInSc2	jantar
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gMnPc4	on
lidé	člověk	k1gMnPc1	člověk
zpracovávali	zpracovávat	k5eAaImAgMnP	zpracovávat
od	od	k7c2	od
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
šperky	šperk	k1gInPc1	šperk
a	a	k8xC	a
amulety	amulet	k1gInPc1	amulet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
byl	být	k5eAaImAgInS	být
jantar	jantar	k1gInSc1	jantar
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
oblasti	oblast	k1gFnSc2	oblast
evropských	evropský	k2eAgNnPc2d1	Evropské
nalezišť	naleziště	k1gNnPc2	naleziště
při	při	k7c6	při
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
distribuován	distribuovat	k5eAaBmNgInS	distribuovat
po	po	k7c6	po
trase	trasa	k1gFnSc6	trasa
Jantarové	jantarový	k2eAgFnSc2d1	jantarová
stezky	stezka	k1gFnSc2	stezka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
procházela	procházet	k5eAaImAgFnS	procházet
Polskem	Polsko	k1gNnSc7	Polsko
přes	přes	k7c4	přes
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
,	,	kIx,	,
větvila	větvit	k5eAaImAgFnS	větvit
se	se	k3xPyFc4	se
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
a	a	k8xC	a
italskou	italský	k2eAgFnSc4d1	italská
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zhotovovaly	zhotovovat	k5eAaImAgInP	zhotovovat
vrtané	vrtaný	k2eAgInPc1d1	vrtaný
korále	korále	k1gInPc1	korále
pro	pro	k7c4	pro
náhrdelníky	náhrdelník	k1gInPc4	náhrdelník
nebo	nebo	k8xC	nebo
růžence	růženec	k1gInPc4	růženec
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc4d1	drobná
devocionálie	devocionálie	k1gFnPc4	devocionálie
jako	jako	k8xS	jako
křížky	křížek	k1gInPc4	křížek
či	či	k8xC	či
přívěsky	přívěska	k1gFnPc4	přívěska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
konjunktura	konjunktura	k1gFnSc1	konjunktura
jantaru	jantar	k1gInSc2	jantar
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
šperkovnic	šperkovnice	k1gFnPc2	šperkovnice
<g/>
,	,	kIx,	,
kazet	kazeta	k1gFnPc2	kazeta
a	a	k8xC	a
kabinetů	kabinet	k1gInPc2	kabinet
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
jádro	jádro	k1gNnSc4	jádro
obložené	obložený	k2eAgInPc1d1	obložený
jantarovými	jantarový	k2eAgFnPc7d1	jantarová
tabulkami	tabulka	k1gFnPc7	tabulka
různých	různý	k2eAgInPc2d1	různý
barevných	barevný	k2eAgInPc2d1	barevný
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
kombimaci	kombimace	k1gFnSc6	kombimace
s	s	k7c7	s
kostí	kost	k1gFnSc7	kost
či	či	k8xC	či
slonovinou	slonovina	k1gFnSc7	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oltářní	oltářní	k2eAgInPc1d1	oltářní
kříže	kříž	k1gInPc1	kříž
a	a	k8xC	a
oltáříky	oltářík	k1gInPc1	oltářík
se	se	k3xPyFc4	se
zhotovovaly	zhotovovat	k5eAaImAgInP	zhotovovat
touto	tento	k3xDgFnSc7	tento
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
baroku	barok	k1gInSc6	barok
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
objem	objem	k1gInSc1	objem
produkce	produkce	k1gFnSc2	produkce
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
jantaru	jantar	k1gInSc2	jantar
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
bylo	být	k5eAaImAgNnS	být
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
střediskem	středisko	k1gNnSc7	středisko
východní	východní	k2eAgMnSc1d1	východní
Prusko	Prusko	k1gNnSc4	Prusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
dvě	dva	k4xCgNnPc1	dva
města	město	k1gNnPc1	město
<g/>
:	:	kIx,	:
Königsberg	Königsberg	k1gInSc1	Königsberg
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
sovětský	sovětský	k2eAgMnSc1d1	sovětský
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
ruský	ruský	k2eAgInSc1d1	ruský
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
<g/>
)	)	kIx)	)
a	a	k8xC	a
Danzig	Danzig	k1gInSc1	Danzig
-	-	kIx~	-
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
dělat	dělat	k5eAaImF	dělat
napodobeniny	napodobenina	k1gFnPc4	napodobenina
a	a	k8xC	a
kopie	kopie	k1gFnPc4	kopie
jantaru	jantar	k1gInSc2	jantar
<g/>
:	:	kIx,	:
ušechtilým	ušechtilý	k2eAgInSc7d1	ušechtilý
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
sklo	sklo	k1gNnSc1	sklo
jantarové	jantarový	k2eAgFnSc2d1	jantarová
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
nekvalitním	kvalitní	k2eNgInSc7d1	nekvalitní
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
ambrosit	ambrosit	k5eAaPmF	ambrosit
<g/>
,	,	kIx,	,
odpad	odpad	k1gInSc4	odpad
z	z	k7c2	z
úlomků	úlomek	k1gInPc2	úlomek
jantaru	jantar	k1gInSc2	jantar
znovu	znovu	k6eAd1	znovu
zahřátý	zahřátý	k2eAgInSc1d1	zahřátý
a	a	k8xC	a
přetavený	přetavený	k2eAgInSc1d1	přetavený
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc1	export
jantarové	jantarový	k2eAgFnSc2d1	jantarová
suroviny	surovina	k1gFnSc2	surovina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
geografického	geografický	k2eAgInSc2d1	geografický
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
jantarové	jantarový	k2eAgFnPc4d1	jantarová
mozaiky	mozaika	k1gFnPc4	mozaika
až	až	k9	až
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
Radžastánu	Radžastán	k1gInSc6	Radžastán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
uměleckým	umělecký	k2eAgInSc7d1	umělecký
dílem	díl	k1gInSc7	díl
byla	být	k5eAaImAgFnS	být
barokní	barokní	k2eAgFnSc1d1	barokní
Jantarová	jantarový	k2eAgFnSc1d1	jantarová
komnata	komnata	k1gFnSc1	komnata
<g/>
,	,	kIx,	,
místnost	místnost	k1gFnSc1	místnost
obložená	obložený	k2eAgFnSc1d1	obložená
broušenými	broušený	k2eAgFnPc7d1	broušená
a	a	k8xC	a
řezanými	řezaný	k2eAgFnPc7d1	řezaná
jantarovými	jantarový	k2eAgFnPc7d1	jantarová
bloky	blok	k1gInPc4	blok
s	s	k7c7	s
jantarovými	jantarový	k2eAgInPc7d1	jantarový
doplňky	doplněk	k1gInPc7	doplněk
interiéru	interiér	k1gInSc2	interiér
<g/>
,	,	kIx,	,
darovaná	darovaný	k2eAgFnSc1d1	darovaná
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
do	do	k7c2	do
Carského	carský	k2eAgNnSc2d1	carské
sela	selo	k1gNnSc2	selo
nedaleko	nedaleko	k7c2	nedaleko
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
německá	německý	k2eAgFnSc1d1	německá
okupační	okupační	k2eAgFnSc1d1	okupační
vojska	vojsko	k1gNnSc2	vojsko
Jantarovou	jantarový	k2eAgFnSc4d1	jantarová
komnatu	komnata	k1gFnSc4	komnata
rozebrala	rozebrat	k5eAaPmAgFnS	rozebrat
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
vlakem	vlak	k1gInSc7	vlak
odvézt	odvézt	k5eAaPmF	odvézt
na	na	k7c4	na
neznámé	známý	k2eNgNnSc4d1	neznámé
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
kulturní	kulturní	k2eAgFnSc6d1	kulturní
památce	památka	k1gFnSc6	památka
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
pátrá	pátrat	k5eAaImIp3nS	pátrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
z	z	k7c2	z
dochovaných	dochovaný	k2eAgMnPc2d1	dochovaný
patří	patřit	k5eAaImIp3nS	patřit
renesanční	renesanční	k2eAgInSc1d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgInPc1d1	barokní
kabinety	kabinet	k1gInPc1	kabinet
<g/>
,	,	kIx,	,
kazety	kazeta	k1gFnPc1	kazeta
na	na	k7c4	na
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
oltáříky	oltářík	k1gInPc4	oltářík
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
Státní	státní	k2eAgFnPc1d1	státní
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
Kunstgewerbemuseum	Kunstgewerbemuseum	k1gInSc1	Kunstgewerbemuseum
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
a	a	k8xC	a
obě	dva	k4xCgNnPc4	dva
muzea	muzeum	k1gNnPc4	muzeum
jantaru	jantar	k1gInSc2	jantar
v	v	k7c6	v
Kaliningradu	Kaliningrad	k1gInSc2	Kaliningrad
a	a	k8xC	a
v	v	k7c6	v
Gdaňsku	Gdaňsk	k1gInSc6	Gdaňsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Jantar	jantar	k1gInSc1	jantar
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
valounků	valounek	k1gInPc2	valounek
<g/>
,	,	kIx,	,
hlíz	hlíza	k1gFnPc2	hlíza
a	a	k8xC	a
zrn	zrno	k1gNnPc2	zrno
v	v	k7c6	v
náplavech	náplav	k1gInPc6	náplav
a	a	k8xC	a
v	v	k7c6	v
usazených	usazený	k2eAgFnPc6d1	usazená
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
nalezené	nalezený	k2eAgInPc1d1	nalezený
exempláře	exemplář	k1gInPc1	exemplář
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
rozměrů	rozměr	k1gInPc2	rozměr
lidské	lidský	k2eAgFnSc2d1	lidská
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
váhy	váha	k1gFnSc2	váha
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
sladkovodních	sladkovodní	k2eAgFnPc6d1	sladkovodní
třetihorních	třetihorní	k2eAgFnPc6d1	třetihorní
usazeninách	usazenina	k1gFnPc6	usazenina
české	český	k2eAgFnSc2d1	Česká
křídy	křída	k1gFnSc2	křída
u	u	k7c2	u
Valchova	Valchův	k2eAgInSc2d1	Valchův
<g/>
,	,	kIx,	,
Boršova	Boršův	k2eAgInSc2d1	Boršův
nebo	nebo	k8xC	nebo
Velkých	velký	k2eAgFnPc2d1	velká
Opatovic	Opatovice	k1gFnPc2	Opatovice
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
naleziště	naleziště	k1gNnSc1	naleziště
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
devadesát	devadesát	k4xCc1	devadesát
procent	procento	k1gNnPc2	procento
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
ruského	ruský	k2eAgInSc2d1	ruský
Kaliningradu	Kaliningrad	k1gInSc2	Kaliningrad
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
naleziště	naleziště	k1gNnPc1	naleziště
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pak	pak	k6eAd1	pak
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
či	či	k8xC	či
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
klovatinu	klovatina	k1gFnSc4	klovatina
tropické	tropický	k2eAgFnSc2d1	tropická
dřeviny	dřevina	k1gFnSc2	dřevina
kopálu	kopál	k1gInSc2	kopál
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
barmský	barmský	k2eAgInSc4d1	barmský
jantar	jantar	k1gInSc4	jantar
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc4d1	starý
zhruba	zhruba	k6eAd1	zhruba
99	[number]	k4	99
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
geologické	geologický	k2eAgNnSc1d1	geologické
období	období	k1gNnSc1	období
cenoman	cenoman	k1gMnSc1	cenoman
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
skvěle	skvěle	k6eAd1	skvěle
zachovalých	zachovalý	k2eAgInPc2d1	zachovalý
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
části	část	k1gFnPc1	část
těl	tělo	k1gNnPc2	tělo
praptáků	prapták	k1gMnPc2	prapták
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
pestře	pestro	k6eAd1	pestro
zbarvených	zbarvený	k2eAgNnPc2d1	zbarvené
per	pero	k1gNnPc2	pero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
žab	žába	k1gFnPc2	žába
<g/>
,	,	kIx,	,
ještěrek	ještěrka	k1gFnPc2	ještěrka
nebo	nebo	k8xC	nebo
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
bezobratlých	bezobratlý	k2eAgInPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Asociace	asociace	k1gFnSc1	asociace
různých	různý	k2eAgInPc2d1	různý
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
také	také	k9	také
ekologické	ekologický	k2eAgFnPc4d1	ekologická
interakce	interakce	k1gFnPc4	interakce
mezi	mezi	k7c7	mezi
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
a	a	k8xC	a
obratlovci	obratlovec	k1gMnPc7	obratlovec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
forezi	foreze	k1gFnSc4	foreze
(	(	kIx(	(
<g/>
přenášení	přenášení	k1gNnSc2	přenášení
jednoho	jeden	k4xCgMnSc2	jeden
druhu	druh	k1gInSc2	druh
jiným	jiný	k2eAgNnPc3d1	jiné
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
štírkem	štírek	k1gMnSc7	štírek
a	a	k8xC	a
praptákem	prapták	k1gMnSc7	prapták
<g/>
.	.	kIx.	.
</s>
<s>
Získávání	získávání	k1gNnSc1	získávání
exemplářů	exemplář	k1gInPc2	exemplář
barmského	barmský	k2eAgInSc2d1	barmský
jantaru	jantar	k1gInSc2	jantar
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vlivem	vlivem	k7c2	vlivem
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
relativně	relativně	k6eAd1	relativně
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příměsi	příměs	k1gFnSc6	příměs
==	==	k?	==
</s>
</p>
<p>
<s>
Stávalo	stávat	k5eAaImAgNnS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
vytékající	vytékající	k2eAgFnSc1d1	vytékající
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
stromu	strom	k1gInSc2	strom
zachytila	zachytit	k5eAaPmAgFnS	zachytit
vzduchové	vzduchový	k2eAgFnPc4d1	vzduchová
bubliny	bublina	k1gFnPc4	bublina
<g/>
,	,	kIx,	,
kapky	kapka	k1gFnPc4	kapka
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
prachové	prachový	k2eAgFnPc4d1	prachová
částečky	částečka	k1gFnPc4	částečka
nebo	nebo	k8xC	nebo
malé	malý	k2eAgFnPc4d1	malá
rostliny	rostlina	k1gFnPc4	rostlina
(	(	kIx(	(
<g/>
orchideje	orchidea	k1gFnPc4	orchidea
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
mechy	mech	k1gInPc4	mech
<g/>
,	,	kIx,	,
lišejníky	lišejník	k1gInPc4	lišejník
<g/>
,	,	kIx,	,
semínka	semínko	k1gNnPc4	semínko
a	a	k8xC	a
miniaturní	miniaturní	k2eAgInPc4d1	miniaturní
květy	květ	k1gInPc4	květ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc4	červ
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
drobné	drobný	k2eAgMnPc4d1	drobný
živočichy	živočich	k1gMnPc4	živočich
jako	jako	k8xS	jako
mravence	mravenec	k1gMnPc4	mravenec
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
,	,	kIx,	,
komáry	komár	k1gMnPc4	komár
<g/>
,	,	kIx,	,
včely	včela	k1gFnPc4	včela
<g/>
,	,	kIx,	,
termity	termit	k1gMnPc4	termit
<g/>
,	,	kIx,	,
motýly	motýl	k1gMnPc4	motýl
a	a	k8xC	a
vážky	vážka	k1gFnPc4	vážka
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
žáby	žába	k1gFnPc4	žába
a	a	k8xC	a
štíry	štír	k1gMnPc4	štír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gFnSc1	jejich
buněčná	buněčný	k2eAgFnSc1d1	buněčná
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
fosilizované	fosilizovaný	k2eAgFnSc2d1	fosilizovaná
cypřiše	cypřiš	k1gFnSc2	cypřiš
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
studována	studovat	k5eAaImNgFnS	studovat
buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
buněčné	buněčný	k2eAgNnSc1d1	buněčné
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
chloroplastů	chloroplast	k1gInPc2	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
ovšem	ovšem	k9	ovšem
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zachována	zachován	k2eAgFnSc1d1	zachována
nezůstává	zůstávat	k5eNaImIp3nS	zůstávat
a	a	k8xC	a
předchozí	předchozí	k2eAgInPc1d1	předchozí
nálezy	nález	k1gInPc1	nález
DNA	DNA	kA	DNA
byly	být	k5eAaImAgInP	být
odmítnuty	odmítnout	k5eAaPmNgInP	odmítnout
jako	jako	k8xS	jako
kontaminace	kontaminace	k1gFnPc1	kontaminace
moderní	moderní	k2eAgFnSc2d1	moderní
DNA	DNA	kA	DNA
<g/>
;	;	kIx,	;
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
neuchovává	uchovávat	k5eNaImIp3nS	uchovávat
ani	ani	k8xC	ani
v	v	k7c6	v
kopálu	kopál	k1gInSc6	kopál
(	(	kIx(	(
<g/>
subfosilní	subfosilní	k2eAgFnSc1d1	subfosilní
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Klonování	klonování	k1gNnSc1	klonování
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
DNA	dno	k1gNnSc2	dno
uchované	uchovaný	k2eAgInPc4d1	uchovaný
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zřejmě	zřejmě	k6eAd1	zřejmě
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
nemožné	možný	k2eNgFnPc1d1	nemožná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
příměsi	příměs	k1gFnPc1	příměs
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
nejen	nejen	k6eAd1	nejen
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
velmi	velmi	k6eAd1	velmi
důležitých	důležitý	k2eAgFnPc2d1	důležitá
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
vzorky	vzorek	k1gInPc1	vzorek
jantaru	jantar	k1gInSc2	jantar
velmi	velmi	k6eAd1	velmi
cenné	cenný	k2eAgInPc4d1	cenný
z	z	k7c2	z
paleoambientálního	paleoambientální	k2eAgNnSc2d1	paleoambientální
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vědcům	vědec	k1gMnPc3	vědec
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
modely	model	k1gInPc4	model
dávno	dávno	k6eAd1	dávno
ztracených	ztracený	k2eAgInPc2d1	ztracený
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
viditelnost	viditelnost	k1gFnSc1	viditelnost
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
její	její	k3xOp3gFnSc1	její
poloha	poloha	k1gFnSc1	poloha
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
hodnocení	hodnocení	k1gNnSc4	hodnocení
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Baltský	baltský	k2eAgInSc1d1	baltský
jantar	jantar	k1gInSc1	jantar
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
polodrahokam	polodrahokam	k1gInSc1	polodrahokam
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
fosilní	fosilní	k2eAgFnSc1d1	fosilní
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgFnSc7d2	starší
než	než	k8xS	než
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
baltický	baltický	k2eAgInSc1d1	baltický
jantar	jantar	k1gInSc1	jantar
od	od	k7c2	od
jantaru	jantar	k1gInSc2	jantar
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
kyseliny	kyselina	k1gFnSc2	kyselina
jantarové	jantarový	k2eAgFnSc2d1	jantarová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baltickém	baltický	k2eAgInSc6d1	baltický
jantaru	jantar	k1gInSc6	jantar
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nachází	nacházet	k5eAaImIp3nS	nacházet
5	[number]	k4	5
až	až	k9	až
8	[number]	k4	8
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
typech	typ	k1gInPc6	typ
jantaru	jantar	k1gInSc2	jantar
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
podíl	podíl	k1gInSc4	podíl
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Jantar	jantar	k1gInSc1	jantar
z	z	k7c2	z
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnSc4d3	nejstarší
a	a	k8xC	a
nejcennější	cenný	k2eAgFnSc4d3	nejcennější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Falešný	falešný	k2eAgInSc1d1	falešný
jantar	jantar	k1gInSc1	jantar
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
falešným	falešný	k2eAgInSc7d1	falešný
jantarem	jantar	k1gInSc7	jantar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vyroben	vyroben	k2eAgInSc1d1	vyroben
z	z	k7c2	z
komerčních	komerční	k2eAgFnPc2d1	komerční
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
a	a	k8xC	a
barviv	barvivo	k1gNnPc2	barvivo
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
optickým	optický	k2eAgInSc7d1	optický
efektem	efekt	k1gInSc7	efekt
<g/>
,	,	kIx,	,
charakteristickým	charakteristický	k2eAgMnSc7d1	charakteristický
pro	pro	k7c4	pro
jantar	jantar	k1gInSc4	jantar
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vyrobit	vyrobit	k5eAaPmF	vyrobit
dokonce	dokonce	k9	dokonce
falešný	falešný	k2eAgInSc4d1	falešný
jantar	jantar	k1gInSc4	jantar
obsahující	obsahující	k2eAgInSc4d1	obsahující
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravost	pravost	k1gFnSc1	pravost
jantaru	jantar	k1gInSc2	jantar
lze	lze	k6eAd1	lze
ověřit	ověřit	k5eAaPmF	ověřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vystaví	vystavit	k5eAaPmIp3nS	vystavit
plameni	plamen	k1gInSc3	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Syntetická	syntetický	k2eAgFnSc1d1	syntetická
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
začne	začít	k5eAaPmIp3nS	začít
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
a	a	k8xC	a
krčit	krčit	k5eAaImF	krčit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ověřit	ověřit	k5eAaPmF	ověřit
pravost	pravost	k1gFnSc4	pravost
světlého	světlý	k2eAgInSc2d1	světlý
jantaru	jantar	k1gInSc2	jantar
(	(	kIx(	(
<g/>
žlutého	žlutý	k2eAgMnSc2d1	žlutý
či	či	k8xC	či
oranžového	oranžový	k2eAgNnSc2d1	oranžové
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
vystaven	vystavit	k5eAaPmNgMnS	vystavit
ničivým	ničivý	k2eAgInPc3d1	ničivý
plamenům	plamen	k1gInPc3	plamen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
jantar	jantar	k1gInSc1	jantar
odráží	odrážet	k5eAaImIp3nS	odrážet
tyrkysově	tyrkysově	k6eAd1	tyrkysově
modrý	modrý	k2eAgInSc1d1	modrý
nebo	nebo	k8xC	nebo
zelený	zelený	k2eAgInSc1d1	zelený
odlesk	odlesk	k1gInSc1	odlesk
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
jantarová	jantarový	k2eAgFnSc1d1	jantarová
aura	aura	k1gFnSc1	aura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Auru	aura	k1gFnSc4	aura
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
dopadajících	dopadající	k2eAgInPc2d1	dopadající
pod	pod	k7c7	pod
určitým	určitý	k2eAgInSc7d1	určitý
úhlem	úhel	k1gInSc7	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
kopál	kopál	k1gInSc1	kopál
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jantar	jantar	k1gInSc1	jantar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jantar	jantar	k1gInSc1	jantar
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Bibliographical	Bibliographicat	k5eAaPmAgMnS	Bibliographicat
database	database	k6eAd1	database
of	of	k?	of
the	the	k?	the
international	internationat	k5eAaImAgMnS	internationat
Literature	Literatur	k1gMnSc5	Literatur
about	about	k2eAgInSc4d1	about
the	the	k?	the
Amber	ambra	k1gFnPc2	ambra
Room	Room	k1gMnSc1	Room
</s>
</p>
