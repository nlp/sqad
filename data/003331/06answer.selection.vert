<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zúročit	zúročit	k5eAaPmF	zúročit
svou	svůj	k3xOyFgFnSc4	svůj
nabytou	nabytý	k2eAgFnSc4d1	nabytá
popularitu	popularita	k1gFnSc4	popularita
vydali	vydat	k5eAaPmAgMnP	vydat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
výběrové	výběrový	k2eAgNnSc1d1	výběrové
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
A	a	k8xC	a
Nice	Nice	k1gFnSc4	Nice
Pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
znovuvydané	znovuvydaný	k2eAgFnPc4d1	znovuvydaný
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
desky	deska	k1gFnPc4	deska
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Piper	Piper	k1gMnSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dawn	Dawn	k1gMnSc1	Dawn
a	a	k8xC	a
A	a	k9	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
.	.	kIx.	.
</s>
