<s>
Cmp	Cmp	k?
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
CMP	CMP	kA
<g/>
.	.	kIx.
</s>
<s>
cmp	cmp	k?
</s>
<s>
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
standardní	standardní	k2eAgFnSc1d1
UNIXová	unixový	k2eAgFnSc1d1
utilita	utilita	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
cmp	cmp	k?
je	být	k5eAaImIp3nS
příkaz	příkaz	k1gInSc1
přítomný	přítomný	k2eAgMnSc1d1
v	v	k7c6
textových	textový	k2eAgNnPc6d1
rozhraních	rozhraní	k1gNnPc6
UN	UN	kA
<g/>
*	*	kIx~
<g/>
Xových	Xových	k2eAgInPc2d1
operačních	operační	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porovnává	porovnávat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
soubory	soubor	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc4
jména	jméno	k1gNnPc4
dostane	dostat	k5eAaPmIp3nS
na	na	k7c6
vstupu	vstup	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
vypíše	vypsat	k5eAaPmIp3nS
výsledek	výsledek	k1gInSc1
porovnání	porovnání	k1gNnSc2
na	na	k7c4
standardní	standardní	k2eAgInSc4d1
výstup	výstup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základním	základní	k2eAgNnSc6d1
nastavení	nastavení	k1gNnSc6
nevypisuje	vypisovat	k5eNaImIp3nS
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
soubory	soubor	k1gInPc4
shodné	shodný	k2eAgInPc4d1
<g/>
;	;	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
<g/>
,	,	kIx,
pak	pak	k6eAd1
vypíše	vypsat	k5eAaPmIp3nS
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
pozici	pozice	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
první	první	k4xOgInSc1
rozdíl	rozdíl	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jednak	jednak	k8xC
v	v	k7c6
počtu	počet	k1gInSc6
bajtů	bajt	k1gInPc2
od	od	k7c2
počátku	počátek	k1gInSc2
souboru	soubor	k1gInSc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
jako	jako	k9
číslo	číslo	k1gNnSc1
řádky	řádka	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
rozdíl	rozdíl	k1gInSc1
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Přepínače	přepínač	k1gInPc1
</s>
<s>
cmp	cmp	k?
rozpoznává	rozpoznávat	k5eAaImIp3nS
následující	následující	k2eAgInPc4d1
přepínače	přepínač	k1gInPc4
(	(	kIx(
<g/>
v	v	k7c6
závorkách	závorka	k1gFnPc6
uvedena	uvést	k5eAaPmNgFnS
dlouhá	dlouhý	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
-b	-b	k?
(	(	kIx(
<g/>
--print-bytes	--print-bytes	k1gMnSc1
<g/>
)	)	kIx)
:	:	kIx,
Vypíše	vypsat	k5eAaPmIp3nS
rozdílné	rozdílný	k2eAgInPc4d1
bajty	bajt	k1gInPc4
</s>
<s>
-i	-i	k?
SKIP	skip	k1gInSc1
(	(	kIx(
<g/>
--ignore-initial	--ignore-initial	k1gInSc1
<g/>
=	=	kIx~
<g/>
SKIP	skip	k1gInSc1
<g/>
)	)	kIx)
:	:	kIx,
Ignoruje	ignorovat	k5eAaImIp3nS
prvních	první	k4xOgNnPc6
SKIP	skip	k1gInSc1
bajtů	bajt	k1gInPc2
vstupu	vstup	k1gInSc2
</s>
<s>
-i	-i	k?
SKIP	skip	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
SKIP	skip	k1gInSc1
<g/>
2	#num#	k4
(	(	kIx(
<g/>
--ignore-initial	--ignore-initial	k1gInSc1
<g/>
=	=	kIx~
<g/>
SKIP	skip	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
SKIP	skip	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
:	:	kIx,
Ignoruje	ignorovat	k5eAaImIp3nS
prvních	první	k4xOgMnPc2
SKIP1	SKIP1	k1gMnPc2
bajtů	bajt	k1gInPc2
prvního	první	k4xOgInSc2
souboru	soubor	k1gInSc2
a	a	k8xC
prvních	první	k4xOgFnPc2
SKIP2	SKIP2	k1gFnPc2
bajtů	bajt	k1gInPc2
druhého	druhý	k4xOgMnSc4
souboru	soubor	k1gInSc2
</s>
<s>
-l	-l	k?
(	(	kIx(
<g/>
--verbose	--verbosa	k1gFnSc6
<g/>
)	)	kIx)
:	:	kIx,
Vypíše	vypsat	k5eAaPmIp3nS
hodnoty	hodnota	k1gFnPc4
rozdílných	rozdílný	k2eAgInPc2d1
bajtů	bajt	k1gInPc2
jak	jak	k8xS,k8xC
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
<g/>
,	,	kIx,
tak	tak	k9
v	v	k7c6
osmičkové	osmičkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
</s>
<s>
-n	-n	k?
LIMIT	limit	k1gInSc1
(	(	kIx(
<g/>
--bytes	--bytes	k1gInSc1
<g/>
=	=	kIx~
<g/>
LIMIT	limit	k1gInSc1
<g/>
)	)	kIx)
:	:	kIx,
Porovná	porovnat	k5eAaPmIp3nS
nejvýše	vysoce	k6eAd3,k6eAd1
LIMIT	limit	k1gInSc1
bajtů	bajt	k1gInPc2
</s>
<s>
-s	-s	k?
(	(	kIx(
<g/>
--quiet	--quiet	k1gMnSc1
--silent	--silent	k1gMnSc1
<g/>
)	)	kIx)
:	:	kIx,
Nic	nic	k3yNnSc1
nevypisuje	vypisovat	k5eNaImIp3nS
<g/>
,	,	kIx,
výsledek	výsledek	k1gInSc1
porovnání	porovnání	k1gNnSc2
lze	lze	k6eAd1
získat	získat	k5eAaPmF
pouze	pouze	k6eAd1
z	z	k7c2
návratové	návratový	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
</s>
<s>
-v	-v	k?
(	(	kIx(
<g/>
--version	--version	k1gInSc1
<g/>
)	)	kIx)
:	:	kIx,
Vypíše	vypsat	k5eAaPmIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
verzi	verze	k1gFnSc6
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
--help	--help	k1gInSc1
:	:	kIx,
Vypíše	vypsat	k5eAaPmIp3nS
nápovědu	nápověda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Návratové	návratový	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
</s>
<s>
0	#num#	k4
:	:	kIx,
soubory	soubor	k1gInPc1
jsou	být	k5eAaImIp3nP
shodné	shodný	k2eAgInPc1d1
</s>
<s>
1	#num#	k4
:	:	kIx,
soubory	soubor	k1gInPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
</s>
<s>
2	#num#	k4
:	:	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
chybě	chyba	k1gFnSc3
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Comparing	Comparing	k1gInSc1
and	and	k?
Merging	Merging	k1gInSc1
Files	Files	k1gInSc1
<g/>
:	:	kIx,
Invoking	Invoking	k1gInSc1
cmp	cmp	k?
–	–	k?
anglický	anglický	k2eAgMnSc1d1
originál	originál	k1gMnSc1
manuálu	manuál	k1gInSc2
GNU	gnu	k1gMnSc1
verze	verze	k1gFnSc2
cmp	cmp	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Příkazy	příkaz	k1gInPc1
a	a	k8xC
programy	program	k1gInPc1
příkazové	příkazový	k2eAgInPc1d1
řádky	řádek	k1gInPc1
Unixu	Unix	k1gInSc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
souborů	soubor	k1gInPc2
</s>
<s>
cat	cat	k?
•	•	k?
chattr	chattr	k1gInSc1
•	•	k?
cd	cd	kA
•	•	k?
chmod	chmod	k1gInSc1
•	•	k?
chown	chown	k1gInSc1
•	•	k?
chgrp	chgrp	k1gInSc1
•	•	k?
cksum	cksum	k1gInSc1
•	•	k?
cmp	cmp	k?
•	•	k?
cp	cp	k?
•	•	k?
du	du	k?
•	•	k?
df	df	k?
•	•	k?
file	fil	k1gFnSc2
•	•	k?
fsck	fsck	k1gMnSc1
•	•	k?
fuser	fuser	k1gMnSc1
•	•	k?
ln	ln	k?
•	•	k?
ls	ls	k?
•	•	k?
lsattr	lsattr	k1gMnSc1
•	•	k?
lsof	lsof	k1gMnSc1
•	•	k?
mkdir	mkdir	k1gMnSc1
•	•	k?
mount	mount	k1gMnSc1
•	•	k?
mv	mv	k?
•	•	k?
pwd	pwd	k?
•	•	k?
rm	rm	k?
•	•	k?
rmdir	rmdir	k1gInSc1
•	•	k?
split	split	k1gInSc1
•	•	k?
touch	toucha	k1gFnPc2
Správa	správa	k1gFnSc1
procesů	proces	k1gInPc2
</s>
<s>
at	at	k?
•	•	k?
chroot	chroot	k1gMnSc1
•	•	k?
crontab	crontab	k1gMnSc1
•	•	k?
exit	exit	k1gInSc1
•	•	k?
kill	kill	k1gInSc1
•	•	k?
killall	killall	k1gInSc1
•	•	k?
nice	nika	k1gFnSc3
•	•	k?
pgrep	pgrep	k1gMnSc1
•	•	k?
pidof	pidof	k1gMnSc1
•	•	k?
pkill	pkill	k1gMnSc1
•	•	k?
ps	ps	k0
•	•	k?
pstree	pstree	k1gNnSc6
•	•	k?
sleep	sleep	k1gInSc1
•	•	k?
time	time	k1gInSc1
•	•	k?
top	topit	k5eAaImRp2nS
•	•	k?
wait	wait	k2eAgInSc1d1
•	•	k?
watch	watch	k1gInSc1
Správa	správa	k1gFnSc1
uživatelů	uživatel	k1gMnPc2
a	a	k8xC
prostředí	prostředí	k1gNnSc2
</s>
<s>
env	env	k?
•	•	k?
finger	finger	k1gInSc1
•	•	k?
id	idy	k1gFnPc2
•	•	k?
logname	lognam	k1gInSc5
•	•	k?
mesg	mesg	k1gMnSc1
•	•	k?
passwd	passwd	k1gMnSc1
•	•	k?
su	su	k?
•	•	k?
sudo	suda	k1gFnSc5
•	•	k?
uname	unamat	k5eAaPmIp3nS
•	•	k?
uptime	uptimat	k5eAaPmIp3nS
•	•	k?
w	w	k?
•	•	k?
wall	wall	k1gInSc1
•	•	k?
who	who	k?
•	•	k?
whoami	whoa	k1gFnPc7
•	•	k?
write	wriit	k5eAaBmRp2nP,k5eAaPmRp2nP,k5eAaImRp2nP
Zpracování	zpracování	k1gNnSc4
textu	text	k1gInSc2
</s>
<s>
awk	awk	k?
•	•	k?
comm	comm	k1gInSc1
•	•	k?
csplit	csplit	k1gInSc1
•	•	k?
cut	cut	k?
•	•	k?
diff	diff	k1gInSc1
•	•	k?
ed	ed	k?
•	•	k?
ex	ex	k6eAd1
•	•	k?
fmt	fmt	k?
•	•	k?
head	head	k1gMnSc1
•	•	k?
iconv	iconv	k1gMnSc1
•	•	k?
join	join	k1gMnSc1
•	•	k?
less	less	k1gInSc1
•	•	k?
more	mor	k1gInSc5
•	•	k?
patch	patch	k1gInSc1
•	•	k?
paste	pást	k5eAaImRp2nP
•	•	k?
sed	sed	k1gInSc1
•	•	k?
sort	sorta	k1gFnPc2
•	•	k?
tac	tac	k?
•	•	k?
tail	tail	k1gInSc1
•	•	k?
tee	tee	k?
•	•	k?
tr	tr	k?
•	•	k?
uniq	uniq	k?
•	•	k?
wc	wc	k?
•	•	k?
xargs	xargs	k6eAd1
Programování	programování	k1gNnSc1
v	v	k7c6
shellu	shell	k1gInSc6
</s>
<s>
alias	alias	k9
•	•	k?
basename	basenam	k1gInSc5
•	•	k?
echo	echo	k1gNnSc4
•	•	k?
expr	expr	k1gInSc1
•	•	k?
false	false	k6eAd1
•	•	k?
printf	printf	k1gInSc1
•	•	k?
test	test	k1gInSc1
•	•	k?
true	true	k1gInSc1
•	•	k?
unset	unseta	k1gFnPc2
Síťová	síťový	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
</s>
<s>
dig	dig	k?
•	•	k?
inetd	inetd	k1gMnSc1
•	•	k?
host	host	k1gMnSc1
•	•	k?
ifconfig	ifconfig	k1gMnSc1
•	•	k?
netstat	netstat	k1gInSc1
•	•	k?
nslookup	nslookup	k1gMnSc1
•	•	k?
ping	pingo	k1gNnPc2
•	•	k?
rlogin	rlogin	k1gMnSc1
•	•	k?
nc	nc	k?
•	•	k?
traceroute	tracerout	k1gMnSc5
Hledání	hledání	k1gNnSc1
</s>
<s>
find	find	k?
•	•	k?
grep	grep	k1gInSc1
•	•	k?
strings	strings	k6eAd1
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
banner	banner	k1gInSc1
•	•	k?
bc	bc	k?
•	•	k?
cal	cal	k?
•	•	k?
dd	dd	k?
•	•	k?
lp	lp	k?
•	•	k?
man	man	k1gMnSc1
•	•	k?
size	size	k1gInSc1
•	•	k?
yes	yes	k?
</s>
