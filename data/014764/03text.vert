<s>
Captain	Captain	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Revenge	Revenge	k1gFnSc7
</s>
<s>
Captain	Captain	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
RevengeInterpretAlestormDruh	RevengeInterpretAlestormDruha	k1gFnPc2
albaStudiové	albaStudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
2008	#num#	k4
<g/>
Nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
<g/>
2007	#num#	k4
<g/>
ŽánryPower	ŽánryPowra	k1gFnPc2
metal	metat	k5eAaImAgMnS
<g/>
,	,	kIx,
Folk	folk	k1gInSc1
metalJazykangličtinaVydavatelstvíNapalm	metalJazykangličtinaVydavatelstvíNapalma	k1gFnPc2
RecordsProducentLasse	RecordsProducentLasse	k1gFnSc2
LammertAlestorm	LammertAlestorm	k1gInSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Captain	Captain	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Revenge	Revenge	k1gFnSc7
<g/>
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Black	Black	k6eAd1
Sails	Sails	k1gInSc1
at	at	k?
Midnight	Midnight	k1gInSc1
<g/>
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Captain	Captain	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Revenge	Revenge	k1gFnSc7
je	být	k5eAaImIp3nS
hudební	hudební	k2eAgNnSc4d1
album	album	k1gNnSc4
skupiny	skupina	k1gFnSc2
Alestorm	Alestorm	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydáno	vydán	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Over	Over	k1gInSc1
the	the	k?
Seas	Seas	k1gInSc1
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
</s>
<s>
Captain	Captain	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Revenge	Revenge	k1gInSc1
–	–	k?
6	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
</s>
<s>
The	The	k?
Huntmaster	Huntmaster	k1gInSc1
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
</s>
<s>
Nancy	Nancy	k1gFnSc1
the	the	k?
Tavern	Tavern	k1gInSc1
Wench	Wench	k1gInSc1
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
</s>
<s>
Death	Death	k1gMnSc1
Before	Befor	k1gInSc5
the	the	k?
Mast	mast	k1gFnSc4
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
</s>
<s>
Terror	Terror	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
High	High	k1gInSc1
Seas	Seas	k1gInSc1
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
</s>
<s>
Set	set	k1gInSc1
Sail	Sail	k1gInSc1
and	and	k?
Conquer	Conquer	k1gInSc1
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
</s>
<s>
Of	Of	k?
Treasure	Treasur	k1gMnSc5
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
Wenches	Wenches	k1gInSc1
and	and	k?
Mead	Mead	k1gInSc1
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
</s>
<s>
Flower	Flower	k1gInSc1
of	of	k?
Scotland	Scotland	k1gInSc1
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Alestorm	Alestorm	k1gInSc1
Christopher	Christophra	k1gFnPc2
Bowes	Bowes	k1gMnSc1
•	•	k?
Gareth	Gareth	k1gMnSc1
Murdock	Murdock	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Alcorn	Alcorn	k1gMnSc1
•	•	k?
Elliot	Elliot	k1gMnSc1
Vernon	Vernon	k1gMnSc1
•	•	k?
Máté	Mátá	k1gFnSc2
Bodor	Bodor	k1gInSc4
Studiová	studiový	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Captain	Captain	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Revenge	Revenge	k1gInSc1
•	•	k?
Black	Black	k1gInSc1
Sails	Sails	k1gInSc1
at	at	k?
Midnight	Midnight	k1gInSc1
•	•	k?
Back	Back	k1gInSc1
Through	Through	k1gMnSc1
Time	Time	k1gFnPc2
•	•	k?
Sunset	Sunset	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Golden	Goldna	k1gFnPc2
Age	Age	k1gFnSc1
•	•	k?
No	no	k9
Grave	grave	k1gNnSc1
but	but	k?
the	the	k?
Sea	Sea	k1gFnSc2
•	•	k?
Curse	Curse	k1gFnSc2
of	of	k?
the	the	k?
Crystal	Crystal	k1gMnSc1
Coconut	Coconut	k1gMnSc1
Koncertní	koncertní	k2eAgNnSc4d1
alba	album	k1gNnSc2
</s>
<s>
Live	Live	k1gFnSc1
at	at	k?
the	the	k?
End	End	k1gFnSc2
of	of	k?
the	the	k?
World	World	k1gInSc1
Vydavatelství	vydavatelství	k1gNnSc1
</s>
<s>
Napalm	napalm	k1gInSc1
Records	Recordsa	k1gFnPc2
</s>
