<s>
Opatský	opatský	k2eAgInSc1d1	opatský
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
černým	černý	k2eAgInSc7d1	černý
širokým	široký	k2eAgInSc7d1	široký
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
,	,	kIx,	,
dvanácti	dvanáct	k4xCc7	dvanáct
černými	černý	k2eAgInPc7d1	černý
uzlíky	uzlík	k1gInPc7	uzlík
či	či	k8xC	či
třapci	třapec	k1gInPc7	třapec
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
svisle	svisle	k6eAd1	svisle
postavenou	postavený	k2eAgFnSc7d1	postavená
berlou	berla	k1gFnSc7	berla
za	za	k7c7	za
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
