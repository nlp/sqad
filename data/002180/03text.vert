<s>
Salem	Salem	k6eAd1	Salem
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Oregon	Oregona	k1gFnPc2	Oregona
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Willamette	Willamett	k1gInSc5	Willamett
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Salemu	Salem	k1gInSc6	Salem
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
více	hodně	k6eAd2	hodně
než	než	k8xS	než
154	[number]	k4	154
637	[number]	k4	637
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
po	po	k7c6	po
Portlandu	Portland	k1gInSc6	Portland
a	a	k8xC	a
Eugene	Eugen	k1gInSc5	Eugen
dělá	dělat	k5eAaImIp3nS	dělat
třetí	třetí	k4xOgNnSc4	třetí
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Salem	Salem	k1gInSc1	Salem
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
a	a	k8xC	a
arabského	arabský	k2eAgNnSc2d1	arabské
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
znamenají	znamenat	k5eAaImIp3nP	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Salem	Salem	k6eAd1	Salem
byl	být	k5eAaImAgMnS	být
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Třešňové	třešňový	k2eAgNnSc4d1	třešňové
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
Cherry	Cherr	k1gInPc1	Cherr
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
průmysl	průmysl	k1gInSc1	průmysl
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
třešní	třešeň	k1gFnPc2	třešeň
hrál	hrát	k5eAaImAgInS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Oregonu	Oregon	k1gInSc2	Oregon
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
údolí	údolí	k1gNnSc2	údolí
Willamette	Willamett	k1gInSc5	Willamett
Valley	Vallea	k1gMnSc2	Vallea
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejúrodnějších	úrodný	k2eAgFnPc2d3	nejúrodnější
oblastí	oblast	k1gFnPc2	oblast
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
154	[number]	k4	154
637	[number]	k4	637
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
79,0	[number]	k4	79,0
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,7	[number]	k4	2,7
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
10,1	[number]	k4	10,1
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
20,3	[number]	k4	20,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Kawagoe	Kawagoe	k1gNnSc1	Kawagoe
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Kimhe	Kimh	k1gInSc2	Kimh
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Salem	Salem	k1gInSc1	Salem
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Salem	Salem	k1gInSc1	Salem
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
