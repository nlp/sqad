<s>
Všeobecný	všeobecný	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
občanský	občanský	k2eAgInSc1d1
pro	pro	k7c4
německé	německý	k2eAgFnPc4d1
dědičné	dědičný	k2eAgFnPc4d1
země	zem	k1gFnPc4
Rakouské	rakouský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Allgemeines	Allgemeines	k2eAgNnPc4d1
bürgerliches	bürgerliches	k2eAgNnPc4d1
Gesetzbuch	Gesetzbuch	k1gNnSc1
für	für	k7c4
die	die	kA
gesammten	gesammten	k2eAgMnPc4d1
Deutschen	Deutschen	k2eAgMnPc4d1
Erbländer	Erbländer	k1gMnPc4
der	drát	kA
Österreichischen	Österreichischen	k2eAgFnSc2d1
Monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
ABGB	ABGB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
české	český	k2eAgFnSc6d1
právní	právní	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
obecný	obecný	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
občanský	občanský	k2eAgInSc1d1
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
o.	o.	kA
z.	z.	kA
o.	o.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
základem	základ	k1gInSc7
občanského	občanský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
neuherské	uherský	k2eNgFnSc2d1
části	část	k1gFnSc2
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
od	od	k7c2
skončení	skončení	k1gNnSc2
josefinismu	josefinismus	k1gInSc2
až	až	k8xS
do	do	k7c2
jejího	její	k3xOp3gInSc2
zániku	zánik	k1gInSc2
<g/>
.	.	kIx.
</s>