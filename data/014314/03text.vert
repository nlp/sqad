<s>
Všeobecný	všeobecný	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
občanský	občanský	k2eAgInSc1d1
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
občanský	občanský	k2eAgInSc1d1
Titulní	titulní	k2eAgInSc1d1
list	list	k1gInSc1
ABGB	ABGB	kA
z	z	k7c2
roku	rok	k1gInSc2
1811	#num#	k4
</s>
<s>
Allgemeines	Allgemeines	k1gMnSc1
bürgerliches	bürgerliches	k1gMnSc1
Gesetzbuch	Gesetzbuch	k1gMnSc1
für	für	k?
die	die	k?
gesammten	gesammten	k2eAgMnSc1d1
Deutschen	Deutschen	k2eAgMnSc1d1
Erbländer	Erbländer	k1gMnSc1
der	drát	k5eAaImRp2nS
Österreichischen	Österreichischna	k1gFnPc2
Monarchie	monarchie	k1gFnSc1
Předpis	předpis	k1gInSc1
státu	stát	k1gInSc2
</s>
<s>
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc2
Druh	druh	k1gInSc4
předpisu	předpis	k1gInSc2
</s>
<s>
císařský	císařský	k2eAgInSc4d1
patent	patent	k1gInSc4
Číslo	číslo	k1gNnSc4
předpisu	předpis	k1gInSc2
</s>
<s>
JGS	JGS	kA
Nr	Nr	k1gFnSc1
<g/>
.	.	kIx.
946	#num#	k4
<g/>
/	/	kIx~
<g/>
1811	#num#	k4
Obvyklá	obvyklý	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
ABGB	ABGB	kA
Údaje	údaj	k1gInPc1
Autor	autor	k1gMnSc1
</s>
<s>
Franz	Franz	k1gInSc1
von	von	k1gInSc1
Zeiller	Zeiller	k1gInSc4
Schváleno	schválen	k2eAgNnSc4d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc4
1811	#num#	k4
Platnost	platnost	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1811	#num#	k4
Účinnost	účinnost	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1812	#num#	k4
Zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
1966	#num#	k4
Oblast	oblast	k1gFnSc1
úpravy	úprava	k1gFnSc2
</s>
<s>
občanské	občanský	k2eAgNnSc1d1
právorodinné	právorodinný	k2eAgNnSc1d1
právo	právo	k1gNnSc1
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
občanský	občanský	k2eAgInSc1d1
pro	pro	k7c4
německé	německý	k2eAgFnPc4d1
dědičné	dědičný	k2eAgFnPc4d1
země	zem	k1gFnPc4
Rakouské	rakouský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Allgemeines	Allgemeines	k2eAgNnPc4d1
bürgerliches	bürgerliches	k2eAgNnPc4d1
Gesetzbuch	Gesetzbuch	k1gNnSc1
für	für	k7c4
die	die	kA
gesammten	gesammten	k2eAgMnPc4d1
Deutschen	Deutschen	k2eAgMnPc4d1
Erbländer	Erbländer	k1gMnPc4
der	drát	kA
Österreichischen	Österreichischen	k2eAgFnSc2d1
Monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
ABGB	ABGB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
české	český	k2eAgFnSc6d1
právní	právní	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
obecný	obecný	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
občanský	občanský	k2eAgInSc1d1
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
o.	o.	kA
z.	z.	kA
o.	o.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
základem	základ	k1gInSc7
občanského	občanský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
neuherské	uherský	k2eNgFnSc2d1
části	část	k1gFnSc2
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
od	od	k7c2
skončení	skončení	k1gNnSc2
josefinismu	josefinismus	k1gInSc2
až	až	k8xS
do	do	k7c2
jejího	její	k3xOp3gInSc2
zániku	zánik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášen	vyhlášen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1811	#num#	k4
patentem	patent	k1gInSc7
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
I.	I.	kA
č.	č.	k?
946	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
s.	s.	k?
s	s	k7c7
platností	platnost	k1gFnSc7
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
země	zem	k1gFnPc4
rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
,	,	kIx,
vyjímaje	vyjímaje	k7c4
země	zem	k1gFnPc4
Koruny	koruna	k1gFnSc2
uherské	uherský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
uherské	uherský	k2eAgFnSc6d1
části	část	k1gFnSc6
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
platil	platit	k5eAaImAgInS
ABGB	ABGB	kA
pouze	pouze	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1852	#num#	k4
-	-	kIx~
1861	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášení	vyhlášení	k1gNnSc1
zákoníku	zákoník	k1gInSc2
předcházelo	předcházet	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1797	#num#	k4
krátké	krátká	k1gFnSc2
období	období	k1gNnSc2
„	„	k?
<g/>
platnosti	platnost	k1gFnSc2
na	na	k7c4
zkoušku	zkouška	k1gFnSc4
<g/>
“	“	k?
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Haliči	Halič	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
platnosti	platnost	k1gFnSc6
až	až	k9
do	do	k7c2
vydání	vydání	k1gNnSc2
středního	střední	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
<g/>
,	,	kIx,
některá	některý	k3yIgNnPc1
ustanovení	ustanovení	k1gNnPc1
(	(	kIx(
<g/>
smlouva	smlouva	k1gFnSc1
služební	služební	k2eAgFnSc1d1
<g/>
)	)	kIx)
však	však	k9
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
platnosti	platnost	k1gFnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1965	#num#	k4
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
zákoník	zákoník	k1gInSc1
práce	práce	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
ABGB	ABGB	kA
silně	silně	k6eAd1
inspiroval	inspirovat	k5eAaBmAgMnS
i	i	k9
tvorbu	tvorba	k1gFnSc4
nového	nový	k2eAgInSc2d1
českého	český	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vycházel	vycházet	k5eAaImAgInS
z	z	k7c2
tradic	tradice	k1gFnPc2
římského	římský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k6eAd1
zohledňoval	zohledňovat	k5eAaImAgMnS
koncepci	koncepce	k1gFnSc4
přirozeného	přirozený	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
zákoníku	zákoník	k1gInSc2
</s>
<s>
I.	I.	kA
Úvod	úvod	k1gInSc1
(	(	kIx(
<g/>
§	§	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
první	první	k4xOgInSc1
<g/>
:	:	kIx,
O	o	k7c6
právu	právo	k1gNnSc6
osobním	osobnit	k5eAaImIp1nS
(	(	kIx(
<g/>
§	§	k?
15	#num#	k4
<g/>
–	–	k?
<g/>
284	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
druhý	druhý	k4xOgInSc1
<g/>
:	:	kIx,
O	o	k7c6
právu	právo	k1gNnSc6
k	k	k7c3
věcem	věc	k1gFnPc3
(	(	kIx(
<g/>
§	§	k?
285	#num#	k4
<g/>
–	–	k?
<g/>
1341	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
třetí	třetí	k4xOgInSc1
<g/>
:	:	kIx,
O	o	k7c6
ustanoveních	ustanovení	k1gNnPc6
společných	společný	k2eAgNnPc6d1
pro	pro	k7c4
práva	právo	k1gNnPc4
osobní	osobní	k2eAgNnPc4d1
a	a	k8xC
práva	právo	k1gNnPc1
věcná	věcný	k2eAgNnPc1d1
(	(	kIx(
<g/>
§	§	k?
1342	#num#	k4
<g/>
–	–	k?
<g/>
1502	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úpravy	úprava	k1gFnPc1
a	a	k8xC
novelizace	novelizace	k1gFnPc1
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
dílčím	dílčí	k2eAgFnPc3d1
novelizacím	novelizace	k1gFnPc3
tohoto	tento	k3xDgInSc2
předpisu	předpis	k1gInSc2
zejména	zejména	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
práva	právo	k1gNnSc2
vlastnického	vlastnický	k2eAgNnSc2d1
<g/>
,	,	kIx,
směnečného	směnečný	k2eAgMnSc2d1
<g/>
,	,	kIx,
obchodního	obchodní	k2eAgNnSc2d1
a	a	k8xC
stavebního	stavební	k2eAgNnSc2d1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
nakonec	nakonec	k6eAd1
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
samostatné	samostatný	k2eAgFnPc1d1
právní	právní	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změnami	změna	k1gFnPc7
ovšem	ovšem	k9
procházelo	procházet	k5eAaImAgNnS
i	i	k9
manželské	manželský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pod	pod	k7c7
vlivem	vliv	k1gInSc7
konkordátu	konkordát	k1gInSc2
uzavřeného	uzavřený	k2eAgInSc2d1
s	s	k7c7
katolickou	katolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
vlivem	vliv	k1gInSc7
nového	nový	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vstoupil	vstoupit	k5eAaPmAgInS
v	v	k7c4
platnost	platnost	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1900	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
také	také	k9
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
roku	rok	k1gInSc2
1904	#num#	k4
jmenována	jmenován	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
nového	nový	k2eAgInSc2d1
občanskoprávního	občanskoprávní	k2eAgInSc2d1
kodexu	kodex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
ovšem	ovšem	k9
nebyla	být	k5eNaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
přípravě	příprava	k1gFnSc3
návrhu	návrh	k1gInSc3
začalo	začít	k5eAaPmAgNnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
osnovu	osnova	k1gFnSc4
zveřejnilo	zveřejnit	k5eAaPmAgNnS
po	po	k7c6
přepracování	přepracování	k1gNnSc6
parlamentním	parlamentní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
v	v	k7c6
červenci	červenec	k1gInSc6
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panská	panský	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
několikrát	několikrát	k6eAd1
upravovaný	upravovaný	k2eAgInSc4d1
návrh	návrh	k1gInSc4
přijala	přijmout	k5eAaPmAgFnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1912	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
ale	ale	k8xC
do	do	k7c2
začátku	začátek	k1gInSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
nebyl	být	k5eNaImAgInS
projednán	projednat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgFnP
tři	tři	k4xCgFnPc1
novelizace	novelizace	k1gFnPc1
uskutečněné	uskutečněný	k2eAgFnPc1d1
prostřednictvím	prostřednictví	k1gNnSc7
císařských	císařský	k2eAgNnPc2d1
nařízení	nařízení	k1gNnPc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
Vliv	vliv	k1gInSc1
německého	německý	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
projevil	projevit	k5eAaPmAgInS
zejména	zejména	k9
u	u	k7c2
novelizace	novelizace	k1gFnSc2
první	první	k4xOgFnSc1
a	a	k8xC
třetí	třetí	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
dílčí	dílčí	k2eAgFnSc1d1
novela	novela	k1gFnSc1
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
císařským	císařský	k2eAgNnSc7d1
nařízením	nařízení	k1gNnSc7
č.	č.	k?
276	#num#	k4
<g/>
/	/	kIx~
<g/>
1914	#num#	k4
ř.	ř.	k?
z.	z.	k?
ze	z	k7c2
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahovala	obsahovat	k5eAaImAgFnS
73	#num#	k4
paragrafů	paragraf	k1gInPc2
a	a	k8xC
upravovala	upravovat	k5eAaImAgFnS
<g/>
:	:	kIx,
</s>
<s>
lhůty	lhůta	k1gFnPc4
k	k	k7c3
prohlášení	prohlášení	k1gNnSc3
za	za	k7c4
mrtvého	mrtvý	k2eAgMnSc4d1
(	(	kIx(
<g/>
§	§	k?
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
podmínky	podmínka	k1gFnPc1
způsobilosti	způsobilost	k1gFnSc2
žen	žena	k1gFnPc2
ke	k	k7c3
svědectví	svědectví	k1gNnSc3
(	(	kIx(
<g/>
§	§	k?
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
péči	péče	k1gFnSc4
o	o	k7c4
nezletilce	nezletilec	k1gMnPc4
v	v	k7c6
otcovské	otcovský	k2eAgFnSc6d1
moci	moc	k1gFnSc6
<g/>
,	,	kIx,
péči	péče	k1gFnSc4
o	o	k7c4
děti	dítě	k1gFnPc4
při	při	k7c6
rozvodu	rozvod	k1gInSc6
nebo	nebo	k8xC
rozloučení	rozloučení	k1gNnSc1
manželství	manželství	k1gNnSc2
a	a	k8xC
postavení	postavení	k1gNnSc2
nemanželských	manželský	k2eNgFnPc2d1
(	(	kIx(
<g/>
§	§	k?
4	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
poručenství	poručenství	k1gNnSc1
(	(	kIx(
<g/>
§	§	k?
21	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
dědické	dědický	k2eAgNnSc1d1
právo	právo	k1gNnSc1
(	(	kIx(
<g/>
§	§	k?
55	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
dílčí	dílčí	k2eAgFnSc1d1
novela	novela	k1gFnSc1
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
císařským	císařský	k2eAgNnSc7d1
nařízením	nařízení	k1gNnSc7
č.	č.	k?
208	#num#	k4
<g/>
/	/	kIx~
<g/>
1915	#num#	k4
ř.	ř.	k?
z.	z.	k?
ze	z	k7c2
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahovala	obsahovat	k5eAaImAgFnS
5	#num#	k4
paragrafů	paragraf	k1gInPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
obnovy	obnova	k1gFnPc4
a	a	k8xC
opravy	oprava	k1gFnPc1
hranic	hranice	k1gFnPc2
pozemků	pozemek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
dílčí	dílčí	k2eAgFnSc1d1
novela	novela	k1gFnSc1
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
císařským	císařský	k2eAgNnSc7d1
nařízením	nařízení	k1gNnSc7
č.	č.	k?
69	#num#	k4
<g/>
/	/	kIx~
<g/>
1916	#num#	k4
ř.	ř.	k?
z.	z.	k?
ze	z	k7c2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládala	skládat	k5eAaImAgFnS
se	se	k3xPyFc4
z	z	k7c2
šesti	šest	k4xCc2
oddílů	oddíl	k1gInPc2
o	o	k7c6
202	#num#	k4
paragrafech	paragraf	k1gInPc6
a	a	k8xC
upravovala	upravovat	k5eAaImAgFnS
<g/>
:	:	kIx,
</s>
<s>
ustanovení	ustanovení	k1gNnSc1
o	o	k7c6
osobních	osobní	k2eAgNnPc6d1
právech	právo	k1gNnPc6
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
I.	I.	kA
<g/>
)	)	kIx)
</s>
<s>
popření	popření	k1gNnSc4
manželského	manželský	k2eAgInSc2d1
původu	původ	k1gInSc2
dítěte	dítě	k1gNnSc2
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
práva	právo	k1gNnPc1
věcná	věcný	k2eAgNnPc1d1
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
právo	právo	k1gNnSc1
dědické	dědický	k2eAgFnPc1d1
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
právo	právo	k1gNnSc1
obligační	obligační	k2eAgFnPc1d1
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
V.	V.	kA
<g/>
)	)	kIx)
</s>
<s>
promlčení	promlčení	k1gNnSc1
zákonné	zákonný	k2eAgFnSc2d1
lhůty	lhůta	k1gFnSc2
(	(	kIx(
<g/>
oddíl	oddíl	k1gInSc1
VI	VI	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
ABGB	ABGB	kA
v	v	k7c6
nástupnických	nástupnický	k2eAgInPc6d1
státech	stát	k1gInPc6
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
ABGB	ABGB	kA
byl	být	k5eAaImAgInS
do	do	k7c2
československého	československý	k2eAgNnSc2d1
práva	právo	k1gNnSc2
přijat	přijmout	k5eAaPmNgMnS
zákonem	zákon	k1gInSc7
č.	č.	k?
11	#num#	k4
<g/>
/	/	kIx~
<g/>
1918	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
známým	známý	k1gMnSc7
jako	jako	k8xC,k8xS
recepční	recepční	k2eAgFnSc1d1
norma	norma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc4
ovšem	ovšem	k9
byl	být	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
recipován	recipovat	k5eAaBmNgInS
pouze	pouze	k6eAd1
pro	pro	k7c4
české	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
(	(	kIx(
<g/>
Čechy	Čechy	k1gFnPc4
<g/>
,	,	kIx,
Moravu	Morava	k1gFnSc4
a	a	k8xC
rakouské	rakouský	k2eAgNnSc4d1
Slezsko	Slezsko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
platilo	platit	k5eAaImAgNnS
zvykové	zvykový	k2eAgNnSc1d1
uherské	uherský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
právním	právní	k2eAgInSc7d1
dualismem	dualismus	k1gInSc7
v	v	k7c6
občanském	občanský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
Československá	československý	k2eAgFnSc1d1
republika	republika	k1gFnSc1
neúspěšně	úspěšně	k6eNd1
bojovala	bojovat	k5eAaImAgFnS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
občanské	občanský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
pro	pro	k7c4
území	území	k1gNnSc4
celého	celý	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
období	období	k1gNnSc6
první	první	k4xOgFnPc1
republiky	republika	k1gFnPc1
sjednotit	sjednotit	k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
sice	sice	k8xC
vypracována	vypracován	k2eAgFnSc1d1
osnova	osnova	k1gFnSc1
československého	československý	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
však	však	k9
vzhledem	vzhledem	k7c3
k	k	k7c3
politické	politický	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
konce	konec	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
již	již	k9
nebyla	být	k5eNaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uspěla	uspět	k5eAaPmAgFnS
až	až	k9
právnická	právnický	k2eAgFnSc1d1
dvouletka	dvouletka	k1gFnSc1
po	po	k7c6
nástupu	nástup	k1gInSc6
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
k	k	k7c3
moci	moc	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
během	během	k7c2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
změnila	změnit	k5eAaPmAgNnP
veškeré	veškerý	k3xTgNnSc4
dosavadní	dosavadní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
a	a	k8xC
ukončila	ukončit	k5eAaPmAgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
právní	právní	k2eAgInSc4d1
dualismus	dualismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
ABGB	ABGB	kA
byl	být	k5eAaImAgMnS
několikrát	několikrát	k6eAd1
novelizován	novelizován	k2eAgMnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
z	z	k7c2
něj	on	k3xPp3gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
vypadla	vypadnout	k5eAaPmAgFnS
materie	materie	k1gFnSc1
upravující	upravující	k2eAgNnSc1d1
osvojení	osvojení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
platil	platit	k5eAaImAgInS
ABGB	ABGB	kA
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1950	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
tzv.	tzv.	kA
středním	střední	k2eAgInSc7d1
občanským	občanský	k2eAgInSc7d1
zákoníkem	zákoník	k1gInSc7
<g/>
;	;	kIx,
po	po	k7c6
tomto	tento	k3xDgInSc6
datu	datum	k1gNnSc6
zůstala	zůstat	k5eAaPmAgFnS
z	z	k7c2
ABGB	ABGB	kA
v	v	k7c6
platnosti	platnost	k1gFnSc6
pouze	pouze	k6eAd1
ustanovení	ustanovení	k1gNnSc4
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
služební	služební	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
(	(	kIx(
<g/>
hlava	hlava	k1gFnSc1
XXVI	XXVI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
§	§	k?
1151	#num#	k4
<g/>
–	–	k?
<g/>
1164	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
až	až	k9
zákoníkem	zákoník	k1gInSc7
práce	práce	k1gFnSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
ABGB	ABGB	kA
byl	být	k5eAaImAgInS
také	také	k9
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
uvozovací	uvozovací	k2eAgInSc4d1
dekret	dekret	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
se	se	k3xPyFc4
zákoník	zákoník	k1gInSc1
zaváděl	zavádět	k5eAaImAgInS
do	do	k7c2
praxe	praxe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
paragrafu	paragraf	k1gInSc6
16	#num#	k4
uvozovacího	uvozovací	k2eAgInSc2d1
dekretu	dekret	k1gInSc2
je	být	k5eAaImIp3nS
stanoveno	stanovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
základním	základní	k2eAgNnSc7d1
zněním	znění	k1gNnSc7
ABGB	ABGB	kA
je	být	k5eAaImIp3nS
znění	znění	k1gNnSc1
německé	německý	k2eAgFnSc2d1
vydané	vydaný	k2eAgFnSc2d1
v	v	k7c6
říšské	říšský	k2eAgFnSc6d1
sbírce	sbírka	k1gFnSc6
zákonů	zákon	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
platnosti	platnost	k1gFnSc2
ABGB	ABGB	kA
v	v	k7c6
českém	český	k2eAgNnSc6d1
znění	znění	k1gNnSc6
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
zákonů	zákon	k1gInPc2
ČSR	ČSR	kA
nikdy	nikdy	k6eAd1
nevyšel	vyjít	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
V	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
platil	platit	k5eAaImAgInS
ABGB	ABGB	kA
do	do	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
;	;	kIx,
následně	následně	k6eAd1
byl	být	k5eAaImAgInS
postupně	postupně	k6eAd1
nahrazován	nahrazovat	k5eAaImNgInS
jinými	jiný	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
polské	polský	k2eAgFnSc2d1
Haliče	Halič	k1gFnSc2
platil	platit	k5eAaImAgInS
ABGB	ABGB	kA
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2
platí	platit	k5eAaImIp3nS
ABGB	ABGB	kA
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ustanovení	ustanovení	k1gNnSc1
upravující	upravující	k2eAgFnSc2d1
věcná	věcný	k2eAgNnPc4d1
práva	právo	k1gNnPc4
(	(	kIx(
<g/>
§	§	k?
285	#num#	k4
-	-	kIx~
§	§	k?
530	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
s	s	k7c7
účinnosti	účinnost	k1gFnSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1923	#num#	k4
vypuštěna	vypustit	k5eAaPmNgFnS
a	a	k8xC
nahrazena	nahradit	k5eAaPmNgFnS
lichtenštejnským	lichtenštejnský	k2eAgInSc7d1
občanským	občanský	k2eAgInSc7d1
zákoníkem	zákoník	k1gInSc7
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
č.	č.	k?
4	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedený	uvedený	k2eAgInSc1d1
lichtenštejnský	lichtenštejnský	k2eAgInSc1d1
občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
je	být	k5eAaImIp3nS
převzatá	převzatý	k2eAgFnSc1d1
švýcarská	švýcarský	k2eAgFnSc1d1
právní	právní	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
věcných	věcný	k2eAgNnPc2d1
práv	právo	k1gNnPc2
ze	z	k7c2
švýcarského	švýcarský	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věcná	věcný	k2eAgFnSc1d1
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6
tak	tak	k9
sledují	sledovat	k5eAaImIp3nP
švýcarskou	švýcarský	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
věcných	věcný	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
rozsáhlejší	rozsáhlý	k2eAgInSc4d2
komentář	komentář	k1gInSc4
k	k	k7c3
textu	text	k1gInSc3
lichtenštejnského	lichtenštejnský	k2eAgInSc2d1
ABGB	ABGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
jsou	být	k5eAaImIp3nP
užívány	užíván	k2eAgFnPc1d1
k	k	k7c3
výkladu	výklad	k1gInSc3
textu	text	k1gInSc2
lichtenštejnského	lichtenštejnský	k2eAgInSc2d1
ABGB	ABGB	kA
rakouské	rakouský	k2eAgInPc1d1
komentáře	komentář	k1gInPc1
k	k	k7c3
textu	text	k1gInSc3
rakouského	rakouský	k2eAgInSc2d1
ABGB	ABGB	kA
<g/>
.	.	kIx.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
V	v	k7c6
Rakousku	Rakousko	k1gNnSc6
platí	platit	k5eAaImIp3nP
ABGB	ABGB	kA
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
polovina	polovina	k1gFnSc1
textu	text	k1gInSc2
nebyla	být	k5eNaImAgFnS
dotčena	dotčen	k2eAgFnSc1d1
žádnou	žádný	k3yNgFnSc7
z	z	k7c2
novel	novela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
ABGB	ABGB	kA
jako	jako	k8xC,k8xS
inspirace	inspirace	k1gFnSc1
legislativních	legislativní	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
</s>
<s>
Srbský	srbský	k2eAgInSc1d1
občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1844	#num#	k4
představoval	představovat	k5eAaImAgInS
zkrácenou	zkrácený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
ABGB	ABGB	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ABGB	ABGB	kA
byl	být	k5eAaImAgInS
inspiračním	inspirační	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
občanskoprávní	občanskoprávní	k2eAgFnSc2d1
kodifikace	kodifikace	k1gFnSc2
v	v	k7c6
Atatürkově	Atatürkův	k2eAgNnSc6d1
Turecku	Turecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nový	nový	k2eAgInSc1d1
občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
-	-	kIx~
1	#num#	k4
<g/>
,	,	kIx,
LEGIS	LEGIS	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
www.legis.cz	www.legis.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Krešić	Krešić	k1gFnSc1
<g/>
,	,	kIx,
Mirela	Mirela	k1gFnSc1
<g/>
:	:	kIx,
Yugoslav	Yugoslav	k1gMnSc1
private	privat	k1gInSc5
law	law	k?
between	between	k1gInSc1
the	the	k?
two	two	k?
World	World	k1gInSc1
Wars	Wars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IN	IN	kA
<g/>
:	:	kIx,
Modernisierung	Modernisierung	k1gInSc1
durch	durch	k6eAd1
Transfer	transfer	k1gInSc1
zwischen	zwischen	k2eAgInSc1d1
den	den	k1gInSc1
Weltkriegen	Weltkriegen	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
155	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SCHELLE	SCHELLE	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
TAUCHEN	TAUCHEN	kA
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občanské	občanský	k2eAgInPc1d1
zákoníky	zákoník	k1gInPc1
<g/>
:	:	kIx,
kompletní	kompletní	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
občanských	občanský	k2eAgInPc2d1
zákoníků	zákoník	k1gInPc2
<g/>
,	,	kIx,
důvodových	důvodový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
a	a	k8xC
dobových	dobový	k2eAgInPc2d1
komentářů	komentář	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Key	Key	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074181467	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Občanský	občanský	k2eAgInSc1d1
zákonník	zákonník	k1gInSc1
obecný	obecný	k2eAgInSc1d1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Dílo	dílo	k1gNnSc1
Obecný	obecný	k2eAgMnSc1d1
zákoník	zákoník	k1gMnSc1
občanský	občanský	k2eAgMnSc1d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Původní	původní	k2eAgNnSc1d1
znění	znění	k1gNnSc1
obecného	obecný	k2eAgInSc2d1
zákonníku	zákonník	k1gInSc2
občanského	občanský	k2eAgInSc2d1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
www.kakanien.info	www.kakanien.info	k1gNnSc1
<g/>
:	:	kIx,
Obecný	obecný	k2eAgInSc1d1
občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
včetně	včetně	k7c2
uvozovacího	uvozovací	k2eAgInSc2d1
patentu	patent	k1gInSc2
ve	v	k7c6
znění	znění	k1gNnSc6
platném	platný	k2eAgNnSc6d1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
1934	#num#	k4
–	–	k?
neplatný	platný	k2eNgInSc1d1
odkaz	odkaz	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4123873-4	4123873-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82130754	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
183074287	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82130754	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Právo	právo	k1gNnSc1
</s>
