<s>
Loutka	loutka	k1gFnSc1	loutka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Figurka	figurka	k1gFnSc1	figurka
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
zvířete	zvíře	k1gNnSc2	zvíře
nebo	nebo	k8xC	nebo
nadpozemské	nadpozemský	k2eAgFnSc2d1	nadpozemská
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
animaci	animace	k1gFnSc3	animace
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
oživení	oživení	k1gNnSc3	oživení
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
woodooo	woodooo	k6eAd1	woodooo
<g/>
.	.	kIx.	.
</s>
<s>
Loutka	loutka	k1gFnSc1	loutka
slouží	sloužit	k5eAaImIp3nS	sloužit
buďto	buďto	k8xC	buďto
jako	jako	k9	jako
hračka	hračka	k1gFnSc1	hračka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
oživuje	oživovat	k5eAaImIp3nS	oživovat
dítě	dítě	k1gNnSc1	dítě
samo	sám	k3xTgNnSc1	sám
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
loutku	loutka	k1gFnSc4	loutka
oživují	oživovat	k5eAaImIp3nP	oživovat
loutkoherci	loutkoherec	k1gMnPc1	loutkoherec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
loutkovém	loutkový	k2eAgNnSc6d1	loutkové
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
virtuální	virtuální	k2eAgFnSc1d1	virtuální
-	-	kIx~	-
zástupná	zástupný	k2eAgFnSc1d1	zástupná
osoba	osoba	k1gFnSc1	osoba
wikipedisty	wikipedista	k1gMnSc2	wikipedista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
více	hodně	k6eAd2	hodně
jmen	jméno	k1gNnPc2	jméno
<g/>
;	;	kIx,	;
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
loutkami	loutka	k1gFnPc7	loutka
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
hlasem	hlasem	k6eAd1	hlasem
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
nebo	nebo	k8xC	nebo
mu	on	k3xPp3gNnSc3	on
odporují	odporovat	k5eAaImIp3nP	odporovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
loutkářství	loutkářství	k1gNnSc4	loutkářství
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
organizací	organizace	k1gFnSc7	organizace
UNESCO	UNESCO	kA	UNESCO
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
Mistrovských	mistrovský	k2eAgNnPc2d1	mistrovské
děl	dělo	k1gNnPc2	dělo
ústního	ústní	k2eAgNnSc2d1	ústní
a	a	k8xC	a
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
dědictví	dědictví	k1gNnSc2	dědictví
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
loutka	loutko	k1gNnSc2	loutko
pochází	pocházet	k5eAaImIp3nS	pocházet
patrně	patrně	k6eAd1	patrně
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
lýko	lýko	k1gNnSc1	lýko
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
loutky	loutka	k1gFnPc1	loutka
kdysi	kdysi	k6eAd1	kdysi
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
oživuje	oživovat	k5eAaImIp3nS	oživovat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
jest	být	k5eAaImIp3nS	být
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
loutka	loutka	k1gFnSc1	loutka
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
panenky	panenka	k1gFnSc2	panenka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
děti	dítě	k1gFnPc1	dítě
chovají	chovat	k5eAaImIp3nP	chovat
<g/>
,	,	kIx,	,
oblékají	oblékat	k5eAaImIp3nP	oblékat
a	a	k8xC	a
vozí	vozit	k5eAaImIp3nP	vozit
v	v	k7c6	v
kočárku	kočárek	k1gInSc6	kočárek
jako	jako	k8xS	jako
nemluvně	nemluvně	k1gNnSc1	nemluvně
<g/>
.	.	kIx.	.
</s>
<s>
Loutky	loutka	k1gFnPc1	loutka
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
masky	maska	k1gFnPc1	maska
užívaly	užívat	k5eAaImAgFnP	užívat
už	už	k9	už
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
náboženských	náboženský	k2eAgInPc2d1	náboženský
rituálů	rituál	k1gInPc2	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
dokladem	doklad	k1gInSc7	doklad
použití	použití	k1gNnSc2	použití
loutky	loutka	k1gFnSc2	loutka
je	být	k5eAaImIp3nS	být
plastika	plastika	k1gFnSc1	plastika
vyřezaná	vyřezaný	k2eAgFnSc1d1	vyřezaná
z	z	k7c2	z
mamutího	mamutí	k2eAgInSc2d1	mamutí
klu	kel	k1gInSc2	kel
<g/>
,	,	kIx,	,
nalezená	nalezený	k2eAgFnSc1d1	nalezená
v	v	k7c6	v
hrobě	hrob	k1gInSc6	hrob
z	z	k7c2	z
období	období	k1gNnSc2	období
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
na	na	k7c6	na
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
charakter	charakter	k1gInSc1	charakter
výbavy	výbava	k1gFnSc2	výbava
zemřelého	zemřelý	k1gMnSc2	zemřelý
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
šaman	šaman	k1gMnSc1	šaman
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
loutka	loutka	k1gFnSc1	loutka
byla	být	k5eAaImAgFnS	být
rituálním	rituální	k2eAgInSc7d1	rituální
předmětem	předmět	k1gInSc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc4	objekt
na	na	k7c6	na
divadelním	divadelní	k2eAgNnSc6d1	divadelní
jevišti	jeviště	k1gNnSc6	jeviště
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
loutku	loutka	k1gFnSc4	loutka
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
nést	nést	k5eAaImF	nést
význam	význam	k1gInSc4	význam
bytosti	bytost	k1gFnSc2	bytost
a	a	k8xC	a
projevovat	projevovat	k5eAaImF	projevovat
živost	živost	k1gFnSc4	živost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
pohybovat	pohybovat	k5eAaImF	pohybovat
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
loutka	loutka	k1gFnSc1	loutka
představuje	představovat	k5eAaImIp3nS	představovat
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
historickou	historický	k2eAgFnSc4d1	historická
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc4d1	literární
nebo	nebo	k8xC	nebo
divadelní	divadelní	k2eAgFnSc4d1	divadelní
postavu	postava	k1gFnSc4	postava
-	-	kIx~	-
Doktor	doktor	k1gMnSc1	doktor
Faust	Faust	k1gMnSc1	Faust
<g/>
,	,	kIx,	,
<g/>
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
nadpozemskou	nadpozemský	k2eAgFnSc4d1	nadpozemská
mytologickou	mytologický	k2eAgFnSc4d1	mytologická
bytost	bytost	k1gFnSc4	bytost
sudičky	sudička	k1gFnSc2	sudička
<g/>
,	,	kIx,	,
ďábel	ďábel	k1gMnSc1	ďábel
či	či	k8xC	či
Kašpárek	Kašpárek	k1gMnSc1	Kašpárek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
pohádkovou	pohádkový	k2eAgFnSc4d1	pohádková
bytost	bytost	k1gFnSc4	bytost
nebo	nebo	k8xC	nebo
nějaké	nějaký	k3yIgNnSc1	nějaký
člověku	člověk	k1gMnSc3	člověk
blízké	blízký	k2eAgNnSc1d1	blízké
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
jí	on	k3xPp3gFnSc3	on
být	být	k5eAaImF	být
i	i	k9	i
personifikovaný	personifikovaný	k2eAgInSc4d1	personifikovaný
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Loutka	loutka	k1gFnSc1	loutka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
prakticky	prakticky	k6eAd1	prakticky
z	z	k7c2	z
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nejčastější	častý	k2eAgFnPc4d3	nejčastější
bývají	bývat	k5eAaImIp3nP	bývat
loutky	loutka	k1gFnPc1	loutka
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
<g/>
,	,	kIx,	,
hadrové	hadrový	k2eAgFnPc1d1	hadrová
<g/>
,	,	kIx,	,
kožešinové	kožešinový	k2eAgFnPc1d1	kožešinová
nebo	nebo	k8xC	nebo
papírové	papírový	k2eAgFnPc1d1	papírová
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
loutek	loutka	k1gFnPc2	loutka
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
posouvají	posouvat	k5eAaImIp3nP	posouvat
po	po	k7c6	po
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
mohou	moct	k5eAaImIp3nP	moct
loutkoherci	loutkoherec	k1gMnPc1	loutkoherec
vytvořit	vytvořit	k5eAaPmF	vytvořit
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
pohybů	pohyb	k1gInPc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
velikost	velikost	k1gFnSc1	velikost
loutek	loutka	k1gFnPc2	loutka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
,	,	kIx,	,
limitována	limitován	k2eAgFnSc1d1	limitována
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
technickými	technický	k2eAgInPc7d1	technický
parametry	parametr	k1gInPc7	parametr
-	-	kIx~	-
loutka	loutka	k1gFnSc1	loutka
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
dostatečně	dostatečně	k6eAd1	dostatečně
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
příliš	příliš	k6eAd1	příliš
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
loutkoherec	loutkoherec	k1gMnSc1	loutkoherec
mohl	moct	k5eAaImAgMnS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
loutky	loutka	k1gFnPc1	loutka
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
loutek	loutka	k1gFnPc2	loutka
představují	představovat	k5eAaImIp3nP	představovat
loutky	loutka	k1gFnPc1	loutka
pro	pro	k7c4	pro
loutkový	loutkový	k2eAgInSc4d1	loutkový
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Loutky	loutka	k1gFnPc1	loutka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
jednak	jednak	k8xC	jednak
podle	podle	k7c2	podle
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
sláma	sláma	k1gFnSc1	sláma
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
plastik	plastik	k1gInSc1	plastik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
techniky	technika	k1gFnSc2	technika
vedení	vedení	k1gNnSc2	vedení
čili	čili	k8xC	čili
oživování	oživování	k1gNnSc2	oživování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
loutky	loutka	k1gFnPc1	loutka
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
Závěsné	závěsný	k2eAgFnSc6d1	závěsná
-	-	kIx~	-
marioneta	marioneta	k1gFnSc1	marioneta
-	-	kIx~	-
loutka	loutka	k1gFnSc1	loutka
zavěšená	zavěšený	k2eAgFnSc1d1	zavěšená
na	na	k7c6	na
drátu	drát	k1gInSc6	drát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
niťovým	niťový	k2eAgNnSc7d1	niťový
vedením	vedení	k1gNnSc7	vedení
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
vahadlem	vahadlo	k1gNnSc7	vahadlo
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
loutkoherec	loutkoherec	k1gMnSc1	loutkoherec
mohl	moct	k5eAaImAgMnS	moct
ovládat	ovládat	k5eAaImF	ovládat
jednou	jeden	k4xCgFnSc7	jeden
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
loutka	loutka	k1gFnSc1	loutka
pro	pro	k7c4	pro
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
jarmareční	jarmareční	k2eAgFnSc1d1	jarmareční
loutka	loutka	k1gFnSc1	loutka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
"	"	kIx"	"
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
"	"	kIx"	"
na	na	k7c4	na
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Spodové	spodový	k2eAgFnPc1d1	spodová
<g/>
:	:	kIx,	:
Maňásek	maňásek	k1gInSc1	maňásek
-	-	kIx~	-
látková	látkový	k2eAgFnSc1d1	látková
loutka	loutka	k1gFnSc1	loutka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
navléká	navlékat	k5eAaImIp3nS	navlékat
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
vodí	vodit	k5eAaImIp3nS	vodit
zespodu	zespodu	k6eAd1	zespodu
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
také	také	k9	také
jako	jako	k8xS	jako
hračka	hračka	k1gFnSc1	hračka
<g/>
,	,	kIx,	,
zvířátko	zvířátko	k1gNnSc1	zvířátko
<g/>
.	.	kIx.	.
</s>
<s>
Javajka	javajka	k1gFnSc1	javajka
nebo	nebo	k8xC	nebo
javánka	javánka	k1gFnSc1	javánka
(	(	kIx(	(
<g/>
wajang	wajang	k1gInSc1	wajang
<g/>
)	)	kIx)	)
-	-	kIx~	-
loutka	loutka	k1gFnSc1	loutka
na	na	k7c6	na
tyčce	tyčka	k1gFnSc6	tyčka
nebo	nebo	k8xC	nebo
drátu	drát	k1gInSc2	drát
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgNnPc4d1	vedené
zespodu	zespodu	k6eAd1	zespodu
<g/>
.	.	kIx.	.
</s>
<s>
Vařečková	vařečkový	k2eAgFnSc1d1	Vařečková
loutka	loutka	k1gFnSc1	loutka
-	-	kIx~	-
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
loutka	loutka	k1gFnSc1	loutka
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgNnPc4d1	vedené
zespodu	zespodu	k6eAd1	zespodu
<g/>
.	.	kIx.	.
</s>
<s>
Vedené	vedený	k2eAgInPc1d1	vedený
-	-	kIx~	-
např.	např.	kA	např.
manekýn	manekýn	k1gMnSc1	manekýn
-	-	kIx~	-
velká	velká	k1gFnSc1	velká
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
látková	látkový	k2eAgFnSc1d1	látková
loutka	loutka	k1gFnSc1	loutka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
loutkoherec	loutkoherec	k1gMnSc1	loutkoherec
drží	držet	k5eAaImIp3nS	držet
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnSc1d1	ostatní
<g/>
:	:	kIx,	:
Stínová	stínový	k2eAgFnSc1d1	stínová
loutka	loutka	k1gFnSc1	loutka
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
loutka	loutka	k1gFnSc1	loutka
<g/>
.	.	kIx.	.
</s>
<s>
Loutka	loutka	k1gFnSc1	loutka
pro	pro	k7c4	pro
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
-	-	kIx~	-
bez	bez	k7c2	bez
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
části	část	k1gFnPc1	část
musí	muset	k5eAaImIp3nP	muset
držet	držet	k5eAaImF	držet
v	v	k7c6	v
nastavené	nastavený	k2eAgFnSc6d1	nastavená
poloze	poloha	k1gFnSc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
při	při	k7c6	při
dětské	dětský	k2eAgFnSc6d1	dětská
hře	hra	k1gFnSc6	hra
může	moct	k5eAaImIp3nS	moct
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
auto	auto	k1gNnSc1	auto
<g/>
"	"	kIx"	"
posloužit	posloužit	k5eAaPmF	posloužit
téměř	téměř	k6eAd1	téměř
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
jako	jako	k9	jako
improvizované	improvizovaný	k2eAgFnPc1d1	improvizovaná
"	"	kIx"	"
<g/>
loutky	loutka	k1gFnPc1	loutka
<g/>
"	"	kIx"	"
vystupovat	vystupovat	k5eAaImF	vystupovat
různé	různý	k2eAgInPc4d1	různý
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
animované	animovaný	k2eAgFnPc4d1	animovaná
jen	jen	k6eAd1	jen
celkovým	celkový	k2eAgInSc7d1	celkový
pohybem	pohyb	k1gInSc7	pohyb
a	a	k8xC	a
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Loutkové	loutkový	k2eAgFnSc3d1	loutková
hře	hra	k1gFnSc3	hra
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
také	také	k9	také
technika	technika	k1gFnSc1	technika
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
s	s	k7c7	s
maskou	maska	k1gFnSc7	maska
<g/>
,	,	kIx,	,
také	také	k9	také
ve	v	k7c6	v
složitých	složitý	k2eAgInPc6d1	složitý
kostýmech	kostým	k1gInPc6	kostým
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
herec	herec	k1gMnSc1	herec
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
podobat	podobat	k5eAaImF	podobat
loutkoherci	loutkoherec	k1gMnSc3	loutkoherec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
tradice	tradice	k1gFnSc1	tradice
loutek	loutka	k1gFnPc2	loutka
i	i	k9	i
loutkářů	loutkář	k1gMnPc2	loutkář
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
představuje	představovat	k5eAaImIp3nS	představovat
zejména	zejména	k9	zejména
Matěj	Matěj	k1gMnSc1	Matěj
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgFnPc4d1	tradiční
postavy	postava	k1gFnPc4	postava
(	(	kIx(	(
<g/>
loutky	loutka	k1gFnPc4	loutka
<g/>
)	)	kIx)	)
patřil	patřit	k5eAaImAgMnS	patřit
Kašpárek	Kašpárek	k1gMnSc1	Kašpárek
a	a	k8xC	a
Kalupinka	Kalupinka	k1gFnSc1	Kalupinka
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
sedlák	sedlák	k1gMnSc1	sedlák
<g/>
,	,	kIx,	,
čert	čert	k1gMnSc1	čert
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
loutkářství	loutkářství	k1gNnSc2	loutkářství
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jindřich	Jindřich	k1gMnSc1	Jindřich
Veselý	Veselý	k1gMnSc1	Veselý
a	a	k8xC	a
proslavil	proslavit	k5eAaPmAgMnS	proslavit
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
postavami	postava	k1gFnPc7	postava
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
<g/>
,	,	kIx,	,
loutkové	loutkový	k2eAgInPc1d1	loutkový
filmy	film	k1gInPc1	film
Jiřího	Jiří	k1gMnSc2	Jiří
Trnky	Trnka	k1gMnSc2	Trnka
<g/>
,	,	kIx,	,
Hermíny	Hermína	k1gFnSc2	Hermína
Týrlové	Týrlová	k1gFnSc2	Týrlová
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
loutek	loutka	k1gFnPc2	loutka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
má	mít	k5eAaImIp3nS	mít
divadelní	divadelní	k2eAgNnSc1d1	divadelní
oddělení	oddělení	k1gNnSc1	oddělení
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
expozici	expozice	k1gFnSc6	expozice
v	v	k7c6	v
Prachaticích	Prachatice	k1gFnPc6	Prachatice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
je	být	k5eAaImIp3nS	být
loutka	loutka	k1gFnSc1	loutka
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poslušně	poslušně	k6eAd1	poslušně
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
něčí	něčí	k3xOyIgFnSc4	něčí
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
"	"	kIx"	"
<g/>
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
"	"	kIx"	"
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
také	také	k9	také
loutková	loutkový	k2eAgFnSc1d1	loutková
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
jen	jen	k9	jen
zdánlivě	zdánlivě	k6eAd1	zdánlivě
a	a	k8xC	a
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
slouží	sloužit	k5eAaImIp3nP	sloužit
někomu	někdo	k3yInSc3	někdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
.	.	kIx.	.
</s>
