<s>
Tennessin	Tennessin	k1gInSc1
(	(	kIx(
<g/>
zatím	zatím	k6eAd1
nekodifikovaný	kodifikovaný	k2eNgInSc1d1
český	český	k2eAgInSc1d1
název	název	k1gInSc1
odvozený	odvozený	k2eAgInSc1d1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
názvu	název	k1gInSc2
tennessine	tennessinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
resp.	resp.	kA
ze	z	k7c2
zprávy	zpráva	k1gFnSc2
ČTK	ČTK	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ts	ts	k0
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc1
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
117	#num#	k4
<g/>
,	,	kIx,
řazený	řazený	k2eAgInSc1d1
(	(	kIx(
<g/>
zatím	zatím	k6eAd1
pouze	pouze	k6eAd1
formálně	formálně	k6eAd1
<g/>
)	)	kIx)
mezi	mezi	k7c7
halogeny	halogen	k1gInPc7
<g/>
.	.	kIx.
</s>