<p>
<s>
Kartézská	kartézský	k2eAgFnSc1d1	kartézská
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
jsou	být	k5eAaImIp3nP	být
souřadné	souřadný	k2eAgFnPc1d1	souřadná
osy	osa	k1gFnPc1	osa
vzájemně	vzájemně	k6eAd1	vzájemně
kolmé	kolmý	k2eAgFnPc1d1	kolmá
přímky	přímka	k1gFnPc1	přímka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
-	-	kIx~	-
počátku	počátek	k1gInSc2	počátek
soustavy	soustava	k1gFnSc2	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
volí	volit	k5eAaImIp3nP	volit
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
osách	osa	k1gFnPc6	osa
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
souřadnice	souřadnice	k1gFnPc1	souřadnice
polohy	poloha	k1gFnSc2	poloha
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dostat	dostat	k5eAaPmF	dostat
jako	jako	k9	jako
kolmé	kolmý	k2eAgInPc1d1	kolmý
průměty	průmět	k1gInPc1	průmět
polohy	poloha	k1gFnSc2	poloha
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
osám	osa	k1gFnPc3	osa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
má	mít	k5eAaImIp3nS	mít
kartézská	kartézský	k2eAgFnSc1d1	kartézská
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
3	[number]	k4	3
vzájemně	vzájemně	k6eAd1	vzájemně
kolmé	kolmý	k2eAgFnSc2d1	kolmá
osy	osa	k1gFnSc2	osa
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
označované	označovaný	k2eAgFnSc2d1	označovaná
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
2	[number]	k4	2
kolmé	kolmý	k2eAgFnPc1d1	kolmá
osy	osa	k1gFnPc1	osa
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgMnSc2d1	francouzský
matematika	matematik	k1gMnSc2	matematik
a	a	k8xC	a
filosofa	filosof	k1gMnSc2	filosof
Descarta	Descart	k1gMnSc2	Descart
(	(	kIx(	(
<g/>
1596	[number]	k4	1596
<g/>
-	-	kIx~	-
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cartesius	Cartesius	k1gInSc1	Cartesius
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
"	"	kIx"	"
<g/>
kartézská	kartézský	k2eAgFnSc1d1	kartézská
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
<g/>
)	)	kIx)	)
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
algebry	algebra	k1gFnSc2	algebra
a	a	k8xC	a
eukleidovské	eukleidovský	k2eAgFnSc2d1	eukleidovská
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byly	být	k5eAaImAgFnP	být
souřadnice	souřadnice	k1gFnPc1	souřadnice
objeveny	objevit	k5eAaPmNgFnP	objevit
i	i	k8xC	i
matematikem	matematik	k1gMnSc7	matematik
Pierrem	Pierr	k1gMnSc7	Pierr
de	de	k?	de
Fermatem	Fermat	k1gInSc7	Fermat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
také	také	k6eAd1	také
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
dimenzích	dimenze	k1gFnPc6	dimenze
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
ale	ale	k8xC	ale
nepublikoval	publikovat	k5eNaBmAgMnS	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Descartem	Descart	k1gMnSc7	Descart
a	a	k8xC	a
Fermatem	Fermat	k1gInSc7	Fermat
používal	používat	k5eAaImAgMnS	používat
konstrukce	konstrukce	k1gFnPc4	konstrukce
podobné	podobný	k2eAgFnPc4d1	podobná
kartézským	kartézský	k2eAgInSc7d1	kartézský
souřadnicím	souřadnice	k1gFnPc3	souřadnice
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Oresme	Oresme	k1gMnSc1	Oresme
(	(	kIx(	(
<g/>
†	†	k?	†
1382	[number]	k4	1382
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
a	a	k8xC	a
levotočivá	levotočivý	k2eAgFnSc1d1	levotočivá
soustava	soustava	k1gFnSc1	soustava
prostorových	prostorový	k2eAgFnPc2d1	prostorová
kartézských	kartézský	k2eAgFnPc2d1	kartézská
souřadnic	souřadnice	k1gFnPc2	souřadnice
==	==	k?	==
</s>
</p>
<p>
<s>
Představte	představit	k5eAaPmRp2nP	představit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojíte	stát	k5eAaImIp2nP	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
počátek	počátek	k1gInSc4	počátek
prostorové	prostorový	k2eAgFnSc2d1	prostorová
kartézské	kartézský	k2eAgFnSc2d1	kartézská
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
x	x	k?	x
nechť	nechť	k9	nechť
směřuje	směřovat	k5eAaImIp3nS	směřovat
přímo	přímo	k6eAd1	přímo
vpřed	vpřed	k6eAd1	vpřed
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
díváte	dívat	k5eAaImIp2nP	dívat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osa	osa	k1gFnSc1	osa
y	y	k?	y
nechť	nechť	k9	nechť
směřuje	směřovat	k5eAaImIp3nS	směřovat
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
osa	osa	k1gFnSc1	osa
z	z	k7c2	z
nechť	nechť	k9	nechť
směřuje	směřovat	k5eAaImIp3nS	směřovat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
soustava	soustava	k1gFnSc1	soustava
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
souřadná	souřadný	k2eAgFnSc1d1	souřadná
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
pravotočivé	pravotočivý	k2eAgFnSc2d1	pravotočivá
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Určení	určení	k1gNnSc1	určení
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
či	či	k8xC	či
levotočivá	levotočivý	k2eAgFnSc1d1	levotočivá
<g/>
,	,	kIx,	,
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
pravidlo	pravidlo	k1gNnSc1	pravidlo
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zaměníme	zaměnit	k5eAaPmIp1nP	zaměnit
<g/>
-li	i	k?	-li
osy	osa	k1gFnSc2	osa
x	x	k?	x
a	a	k8xC	a
y	y	k?	y
<g/>
,	,	kIx,	,
získáme	získat	k5eAaPmIp1nP	získat
souřadnou	souřadný	k2eAgFnSc4d1	souřadná
soustavu	soustava	k1gFnSc4	soustava
levotočivou	levotočivý	k2eAgFnSc7d1	levotočivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
pravotočivou	pravotočivý	k2eAgFnSc7d1	pravotočivá
souřadnou	souřadný	k2eAgFnSc7d1	souřadná
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Číselná	číselný	k2eAgFnSc1d1	číselná
osa	osa	k1gFnSc1	osa
===	===	k?	===
</s>
</p>
<p>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
kartézskou	kartézský	k2eAgFnSc7d1	kartézská
soustavou	soustava	k1gFnSc7	soustava
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
osa	osa	k1gFnSc1	osa
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
souřadnicí	souřadnice	k1gFnSc7	souřadnice
každého	každý	k3xTgInSc2	každý
bodu	bod	k1gInSc2	bod
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
-	-	kIx~	-
například	například	k6eAd1	například
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
0	[number]	k4	0
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
brát	brát	k5eAaImF	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dvě	dva	k4xCgFnPc1	dva
dimenze	dimenze	k1gFnPc1	dimenze
-	-	kIx~	-
rovina	rovina	k1gFnSc1	rovina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
kartézskou	kartézský	k2eAgFnSc4d1	kartézská
soustavu	soustava	k1gFnSc4	soustava
dvě	dva	k4xCgFnPc1	dva
vzájemně	vzájemně	k6eAd1	vzájemně
kolmé	kolmý	k2eAgFnPc1d1	kolmá
osy	osa	k1gFnPc1	osa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Souřadnicemi	souřadnice	k1gFnPc7	souřadnice
bodu	bod	k1gInSc2	bod
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInSc4	jeho
vzdálenosti	vzdálenost	k1gFnPc1	vzdálenost
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
y	y	k?	y
(	(	kIx(	(
<g/>
souřadnice	souřadnice	k1gFnSc1	souřadnice
x	x	k?	x
<g/>
,	,	kIx,	,
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
<g/>
,	,	kIx,	,
abscisa	abscisa	k1gFnSc1	abscisa
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
x	x	k?	x
(	(	kIx(	(
<g/>
souřadnice	souřadnice	k1gFnSc1	souřadnice
y	y	k?	y
<g/>
,	,	kIx,	,
svislá	svislý	k2eAgFnSc1d1	svislá
<g/>
,	,	kIx,	,
ordináta	ordináta	k1gFnSc1	ordináta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obrázku	obrázek	k1gInSc6	obrázek
jsou	být	k5eAaImIp3nP	být
zakresleny	zakreslit	k5eAaPmNgInP	zakreslit
čtyři	čtyři	k4xCgInPc1	čtyři
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
chápat	chápat	k5eAaImF	chápat
také	také	k9	také
jako	jako	k9	jako
vektory	vektor	k1gInPc1	vektor
<g/>
,	,	kIx,	,
orientované	orientovaný	k2eAgFnPc1d1	orientovaná
úsečky	úsečka	k1gFnPc1	úsečka
spojující	spojující	k2eAgFnSc2d1	spojující
počátek	počátek	k1gInSc4	počátek
s	s	k7c7	s
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
vektoru	vektor	k1gInSc2	vektor
čili	čili	k8xC	čili
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
bodu	bod	k1gInSc2	bod
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
vypočte	vypočíst	k5eAaPmIp3nS	vypočíst
pomocí	pomocí	k7c2	pomocí
Pythagorovy	Pythagorův	k2eAgFnSc2d1	Pythagorova
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
fyzice	fyzika	k1gFnSc3	fyzika
a	a	k8xC	a
strojírenství	strojírenství	k1gNnSc3	strojírenství
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
osa	osa	k1gFnSc1	osa
obvykle	obvykle	k6eAd1	obvykle
definována	definovat	k5eAaBmNgFnS	definovat
nebo	nebo	k8xC	nebo
znázorněna	znázorněn	k2eAgFnSc1d1	znázorněna
jako	jako	k8xC	jako
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
a	a	k8xC	a
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
svislá	svislý	k2eAgFnSc1d1	svislá
<g/>
,	,	kIx,	,
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
směrem	směr	k1gInSc7	směr
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k8xC	jako
0	[number]	k4	0
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
souřadnice	souřadnice	k1gFnPc4	souřadnice
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
označeny	označit	k5eAaPmNgInP	označit
písmeny	písmeno	k1gNnPc7	písmeno
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
nebo	nebo	k8xC	nebo
x	x	k?	x
a	a	k8xC	a
y.	y.	k?	y.
Osy	osa	k1gFnPc1	osa
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
osa	osa	k1gFnSc1	osa
X	X	kA	X
a	a	k8xC	a
osa	osa	k1gFnSc1	osa
Y.	Y.	kA	Y.
Výběr	výběr	k1gInSc1	výběr
písmen	písmeno	k1gNnPc2	písmeno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
konvence	konvence	k1gFnSc2	konvence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
použít	použít	k5eAaPmF	použít
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
abecedy	abeceda	k1gFnSc2	abeceda
pro	pro	k7c4	pro
značení	značení	k1gNnSc4	značení
neznámých	známý	k2eNgFnPc2d1	neznámá
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
abecedy	abeceda	k1gFnSc2	abeceda
k	k	k7c3	k
značení	značení	k1gNnSc3	značení
hodnot	hodnota	k1gFnPc2	hodnota
známých	známý	k2eAgFnPc2d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
osám	osa	k1gFnPc3	osa
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
je	být	k5eAaImIp3nS	být
poloha	poloha	k1gFnSc1	poloha
libovolného	libovolný	k2eAgInSc2d1	libovolný
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
dvourozměrném	dvourozměrný	k2eAgInSc6d1	dvourozměrný
prostoru	prostor	k1gInSc6	prostor
dána	dát	k5eAaPmNgFnS	dát
uspořádanou	uspořádaný	k2eAgFnSc7d1	uspořádaná
dvojicí	dvojice	k1gFnSc7	dvojice
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každé	každý	k3xTgNnSc4	každý
číslo	číslo	k1gNnSc4	číslo
udává	udávat	k5eAaImIp3nS	udávat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
tohoto	tento	k3xDgInSc2	tento
bodu	bod	k1gInSc2	bod
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
jednotek	jednotka	k1gFnPc2	jednotka
měřených	měřený	k2eAgFnPc2d1	měřená
podél	podél	k7c2	podél
dané	daný	k2eAgFnSc2d1	daná
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Souřadnice	souřadnice	k1gFnPc1	souřadnice
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
psány	psán	k2eAgFnPc1d1	psána
jako	jako	k8xS	jako
dvě	dva	k4xCgNnPc1	dva
čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
-	-	kIx~	-
bod	bod	k1gInSc1	bod
A	A	kA	A
[	[	kIx(	[
<g/>
souřadnice	souřadnice	k1gFnSc1	souřadnice
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
x	x	k?	x
<g/>
,	,	kIx,	,
souřadnice	souřadnice	k1gFnSc1	souřadnice
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
y	y	k?	y
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgFnPc1	dva
osy	osa	k1gFnPc1	osa
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
rovinu	rovina	k1gFnSc4	rovina
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
pravé	pravý	k2eAgInPc4d1	pravý
úhly	úhel	k1gInPc4	úhel
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnPc1	oblast
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
kvadranty	kvadrant	k1gInPc4	kvadrant
<g/>
.	.	kIx.	.
</s>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
nebo	nebo	k8xC	nebo
číslován	číslovat	k5eAaImNgInS	číslovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvadrant	kvadrant	k1gInSc1	kvadrant
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
souřadnice	souřadnice	k1gFnPc4	souřadnice
kladné	kladný	k2eAgFnPc4d1	kladná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
první	první	k4xOgInSc4	první
kvadrant	kvadrant	k1gInSc4	kvadrant
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
široce	široko	k6eAd1	široko
používaným	používaný	k2eAgInSc7d1	používaný
souřadnicovým	souřadnicový	k2eAgInSc7d1	souřadnicový
systémem	systém	k1gInSc7	systém
je	být	k5eAaImIp3nS	být
polární	polární	k2eAgInSc1d1	polární
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
a	a	k8xC	a
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tři	tři	k4xCgFnPc1	tři
dimenze	dimenze	k1gFnPc1	dimenze
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
(	(	kIx(	(
<g/>
třírozměrném	třírozměrný	k2eAgMnSc6d1	třírozměrný
<g/>
)	)	kIx)	)
eukleidovském	eukleidovský	k2eAgInSc6d1	eukleidovský
prostoru	prostor	k1gInSc6	prostor
tvoří	tvořit	k5eAaImIp3nS	tvořit
kartézskou	kartézský	k2eAgFnSc4d1	kartézská
soustavu	soustava	k1gFnSc4	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
tří	tři	k4xCgInPc2	tři
navzájem	navzájem	k6eAd1	navzájem
kolmé	kolmý	k2eAgFnPc1d1	kolmá
osy	osa	k1gFnPc1	osa
<g/>
,	,	kIx,	,
protínající	protínající	k2eAgInPc1d1	protínající
se	se	k3xPyFc4	se
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
[	[	kIx(	[
<g/>
0,0	[number]	k4	0,0
<g/>
,0	,0	k4	,0
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
dvojrozměrném	dvojrozměrný	k2eAgInSc6d1	dvojrozměrný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
osa	osa	k1gFnSc1	osa
stává	stávat	k5eAaImIp3nS	stávat
číselnou	číselný	k2eAgFnSc7d1	číselná
čárou	čára	k1gFnSc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
bod	bod	k1gInSc4	bod
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rovinu	rovina	k1gFnSc4	rovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
ose	osa	k1gFnSc3	osa
souřadnic	souřadnice	k1gFnPc2	souřadnice
v	v	k7c6	v
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kartézskými	kartézský	k2eAgFnPc7d1	kartézská
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc1	tři
čísla	číslo	k1gNnPc1	číslo
ve	v	k7c6	v
zvoleném	zvolený	k2eAgNnSc6d1	zvolené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
standardní	standardní	k2eAgInPc1d1	standardní
názvy	název	k1gInPc1	název
souřadnic	souřadnice	k1gFnPc2	souřadnice
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
osách	osa	k1gFnPc6	osa
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
výrazy	výraz	k1gInPc1	výraz
abscisa	abscisa	k1gFnSc1	abscisa
<g/>
,	,	kIx,	,
ordináta	ordináta	k1gFnSc1	ordináta
a	a	k8xC	a
aplikáta	aplikáta	k1gFnSc1	aplikáta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souřadnice	souřadnice	k1gFnPc1	souřadnice
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
označeny	označit	k5eAaPmNgInP	označit
písmeny	písmeno	k1gNnPc7	písmeno
X	X	kA	X
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Z	Z	kA	Z
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
označovat	označovat	k5eAaImF	označovat
roviny	rovina	k1gFnPc4	rovina
XY	XY	kA	XY
<g/>
,	,	kIx,	,
YZ	YZ	kA	YZ
a	a	k8xC	a
XZ	XZ	kA	XZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
fyzice	fyzika	k1gFnSc6	fyzika
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
osy	osa	k1gFnPc1	osa
často	často	k6eAd1	často
definovány	definován	k2eAgFnPc1d1	definována
nebo	nebo	k8xC	nebo
zobrazovány	zobrazován	k2eAgFnPc1d1	zobrazována
jako	jako	k8xS	jako
horizontální	horizontální	k2eAgFnPc1d1	horizontální
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
třetí	třetí	k4xOgFnSc1	třetí
osa	osa	k1gFnSc1	osa
směřuje	směřovat	k5eAaImIp3nS	směřovat
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
třetí	třetí	k4xOgFnSc1	třetí
souřadnice	souřadnice	k1gFnSc1	souřadnice
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
výška	výška	k1gFnSc1	výška
<g/>
/	/	kIx~	/
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bod	bod	k1gInSc4	bod
v	v	k7c6	v
třírozměrném	třírozměrný	k2eAgInSc6d1	třírozměrný
prostoru	prostor	k1gInSc6	prostor
značíme	značit	k5eAaImIp1nP	značit
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
<g/>
a	a	k8xC	a
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
písmene	písmeno	k1gNnSc2	písmeno
A	a	k9	a
můžeme	moct	k5eAaImIp1nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
bod	bod	k1gInSc1	bod
K	k	k7c3	k
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Souřadnice	souřadnice	k1gFnPc1	souřadnice
bodu	bod	k1gInSc2	bod
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc4	jeho
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
tří	tři	k4xCgFnPc2	tři
rovin	rovina	k1gFnPc2	rovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
osách	osa	k1gFnPc6	osa
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z.	z.	k?	z.
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgNnSc6	jaký
pořadí	pořadí	k1gNnSc6	pořadí
se	se	k3xPyFc4	se
osy	osa	k1gFnPc1	osa
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
a	a	k8xC	a
levotočivá	levotočivý	k2eAgFnSc1d1	levotočivá
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kvadranty	kvadrant	k1gInPc1	kvadrant
a	a	k8xC	a
oktanty	oktant	k1gInPc1	oktant
==	==	k?	==
</s>
</p>
<p>
<s>
Osy	osa	k1gFnPc1	osa
dvojrozměrného	dvojrozměrný	k2eAgInSc2d1	dvojrozměrný
kartezského	kartezský	k2eAgInSc2d1	kartezský
systému	systém	k1gInSc2	systém
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
rovinu	rovina	k1gFnSc4	rovina
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
nekonečné	konečný	k2eNgFnPc4d1	nekonečná
oblasti	oblast	k1gFnPc4	oblast
tzv.	tzv.	kA	tzv.
kvadranty	kvadrant	k1gInPc1	kvadrant
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ohraničeny	ohraničit	k5eAaPmNgInP	ohraničit
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
poloosami	poloosa	k1gFnPc7	poloosa
<g/>
.	.	kIx.	.
</s>
<s>
Kvadranty	kvadrant	k1gInPc1	kvadrant
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nP	značit
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
a	a	k8xC	a
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
od	od	k7c2	od
pravého	pravý	k2eAgNnSc2d1	pravé
horního	horní	k2eAgNnSc2d1	horní
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
severovýchodního	severovýchodní	k2eAgMnSc2d1	severovýchodní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
kvadrantu	kvadrant	k1gInSc2	kvadrant
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
<g/>
.	.	kIx.	.
obrázek	obrázek	k1gInSc1	obrázek
napravo	napravo	k6eAd1	napravo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
kvadrantů	kvadrant	k1gInPc2	kvadrant
je	být	k5eAaImIp3nS	být
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
kladné	kladný	k2eAgNnSc4d1	kladné
nebo	nebo	k8xC	nebo
záporné	záporný	k2eAgNnSc4d1	záporné
<g/>
:	:	kIx,	:
I	i	k8xC	i
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
II	II	kA	II
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
III	III	kA	III
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
)	)	kIx)	)
a	a	k8xC	a
IV	IV	kA	IV
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
i	i	k9	i
u	u	k7c2	u
trojrozměrného	trojrozměrný	k2eAgInSc2d1	trojrozměrný
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
osmi	osm	k4xCc2	osm
částí	část	k1gFnPc2	část
-	-	kIx~	-
oktantů	oktant	k1gInPc2	oktant
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
určitého	určitý	k2eAgInSc2d1	určitý
oktantu	oktant	k1gInSc2	oktant
tvoří	tvořit	k5eAaImIp3nP	tvořit
znaménka	znaménko	k1gNnPc1	znaménko
tří	tři	k4xCgFnPc2	tři
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
(	(	kIx(	(
<g/>
+	+	kIx~	+
+	+	kIx~	+
+	+	kIx~	+
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
-	-	kIx~	-
+	+	kIx~	+
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zobecnění	zobecnění	k1gNnSc1	zobecnění
kvadrantu	kvadrant	k1gInSc2	kvadrant
a	a	k8xC	a
oktantu	oktant	k1gInSc2	oktant
na	na	k7c4	na
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
rozměrů	rozměr	k1gInPc2	rozměr
je	být	k5eAaImIp3nS	být
anglicky	anglicky	k6eAd1	anglicky
orthant	orthant	k1gInSc4	orthant
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
obdobný	obdobný	k2eAgInSc1d1	obdobný
systém	systém	k1gInSc1	systém
označení	označení	k1gNnSc2	označení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kartézská	kartézský	k2eAgFnSc1d1	kartézská
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kartézská	kartézský	k2eAgFnSc1d1	kartézská
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
na	na	k7c4	na
Mathworld	Mathworld	k1gInSc4	Mathworld
</s>
</p>
<p>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
k	k	k7c3	k
výkladu	výklad	k1gInSc3	výklad
souřadnic	souřadnice	k1gFnPc2	souřadnice
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgInSc1d1	interaktivní
transformátor	transformátor	k1gInSc1	transformátor
souřadnic	souřadnice	k1gFnPc2	souřadnice
</s>
</p>
