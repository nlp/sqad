<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
má	mít	k5eAaImIp3nS	mít
kartézská	kartézský	k2eAgFnSc1d1	kartézská
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
3	[number]	k4	3
vzájemně	vzájemně	k6eAd1	vzájemně
kolmé	kolmý	k2eAgFnSc2d1	kolmá
osy	osa	k1gFnSc2	osa
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
označované	označovaný	k2eAgFnSc2d1	označovaná
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
2	[number]	k4	2
kolmé	kolmý	k2eAgFnPc1d1	kolmá
osy	osa	k1gFnPc1	osa
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
