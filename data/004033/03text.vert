<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Alaska	Alasek	k1gMnSc2	Alasek
[	[	kIx(	[
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Alaska	Alaska	k1gFnSc1	Alaska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pacifických	pacifický	k2eAgInPc2d1	pacifický
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
která	který	k3yRgNnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
exklávu	exkláva	k1gFnSc4	exkláva
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
a	a	k8xC	a
nejzápadnějším	západní	k2eAgInSc7d3	nejzápadnější
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
kanadským	kanadský	k2eAgNnSc7d1	kanadské
teritoriem	teritorium	k1gNnSc7	teritorium
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
kanadskou	kanadský	k2eAgFnSc7d1	kanadská
provincií	provincie	k1gFnSc7	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Aljašky	Aljaška	k1gFnSc2	Aljaška
od	od	k7c2	od
státu	stát	k1gInSc2	stát
Washington	Washington	k1gInSc1	Washington
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
800	[number]	k4	800
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc1d1	severní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
717	[number]	k4	717
856	[number]	k4	856
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Aljaška	Aljaška	k1gFnSc1	Aljaška
největším	veliký	k2eAgInSc6d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
740	[number]	k4	740
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
třetím	třetí	k4xOgMnSc7	třetí
nejméně	málo	k6eAd3	málo
lidnatým	lidnatý	k2eAgInSc7d1	lidnatý
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
0,5	[number]	k4	0,5
obyvatele	obyvatel	k1gMnPc4	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
posledním	poslední	k2eAgMnSc6d1	poslední
50	[number]	k4	50
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Juneau	Juneaa	k1gFnSc4	Juneaa
s	s	k7c7	s
30	[number]	k4	30
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Anchorage	Anchorag	k1gInPc1	Anchorag
s	s	k7c7	s
300	[number]	k4	300
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Fairbanks	Fairbanks	k1gInSc1	Fairbanks
(	(	kIx(	(
<g/>
30	[number]	k4	30
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sitka	Sitka	k1gMnSc1	Sitka
(	(	kIx(	(
<g/>
9	[number]	k4	9
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wasilla	Wasilla	k1gMnSc1	Wasilla
(	(	kIx(	(
<g/>
9	[number]	k4	9
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ketchikan	Ketchikan	k1gMnSc1	Ketchikan
(	(	kIx(	(
<g/>
8	[number]	k4	8
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aljašce	Aljaška	k1gFnSc3	Aljaška
patří	patřit	k5eAaImIp3nS	patřit
8980	[number]	k4	8980
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
1706	[number]	k4	1706
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
celých	celý	k2eAgInPc2d1	celý
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
Denali	Denali	k1gFnSc2	Denali
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
6190	[number]	k4	6190
m	m	kA	m
v	v	k7c6	v
Aljašských	aljašský	k2eAgFnPc6d1	aljašská
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
Kuskokwim	Kuskokwima	k1gFnPc2	Kuskokwima
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
pobřeží	pobřeží	k1gNnSc4	pobřeží
Aljašky	Aljaška	k1gFnSc2	Aljaška
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
Rusy	Rus	k1gMnPc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
zde	zde	k6eAd1	zde
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
první	první	k4xOgFnPc4	první
osady	osada	k1gFnPc4	osada
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
španělských	španělský	k2eAgFnPc2d1	španělská
expedic	expedice	k1gFnPc2	expedice
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
region	region	k1gInSc1	region
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Aljaška	Aljaška	k1gFnSc1	Aljaška
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgInSc1d1	ruský
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
А	А	k?	А
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
poloostrov	poloostrov	k1gInSc4	poloostrov
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
aleutského	aleutský	k2eAgInSc2d1	aleutský
výrazu	výraz	k1gInSc2	výraz
označujícího	označující	k2eAgInSc2d1	označující
oblast	oblast	k1gFnSc4	oblast
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
odkoupily	odkoupit	k5eAaPmAgInP	odkoupit
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zde	zde	k6eAd1	zde
zřídily	zřídit	k5eAaPmAgInP	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
existovalo	existovat	k5eAaImAgNnS	existovat
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
se	s	k7c7	s
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
stala	stát	k5eAaPmAgFnS	stát
49	[number]	k4	49
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
Aljašky	Aljaška	k1gFnPc4	Aljaška
byli	být	k5eAaImAgMnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
přes	přes	k7c4	přes
pevninský	pevninský	k2eAgInSc4d1	pevninský
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
dobách	doba	k1gFnPc6	doba
spojoval	spojovat	k5eAaImAgMnS	spojovat
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
000	[number]	k4	000
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
meziledové	meziledový	k2eAgFnSc2d1	meziledová
zalit	zalít	k5eAaPmNgMnS	zalít
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
kontinenty	kontinent	k1gInPc1	kontinent
byly	být	k5eAaImAgInP	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
dnešním	dnešní	k2eAgMnSc7d1	dnešní
Beringovým	Beringův	k2eAgInSc7d1	Beringův
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Aleutské	aleutský	k2eAgInPc1d1	aleutský
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
domovem	domov	k1gInSc7	domov
Aleutů	Aleut	k1gMnPc2	Aleut
<g/>
,	,	kIx,	,
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
Rusy	Rus	k1gMnPc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Aljaška	Aljaška	k1gFnSc1	Aljaška
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
Pupiky	Pupik	k1gMnPc4	Pupik
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jejich	jejich	k3xOp3gMnPc1	jejich
příbuzní	příbuzný	k1gMnPc1	příbuzný
Alutiiky	Alutiika	k1gFnSc2	Alutiika
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jihocentrální	jihocentrální	k2eAgFnSc6d1	jihocentrální
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
kolonizace	kolonizace	k1gFnSc1	kolonizace
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
novodobých	novodobý	k2eAgFnPc2d1	novodobá
dějin	dějiny	k1gFnPc2	dějiny
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgMnPc7	první
ruskými	ruský	k2eAgMnPc7d1	ruský
osadníky	osadník	k1gMnPc7	osadník
sibiřští	sibiřský	k2eAgMnPc1d1	sibiřský
kozáci	kozák	k1gMnPc1	kozák
<g/>
,	,	kIx,	,
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
právě	právě	k9	právě
přes	přes	k7c4	přes
Beringův	Beringův	k2eAgInSc4d1	Beringův
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
Vituse	Vituse	k1gFnSc2	Vituse
Beringa	Bering	k1gMnSc2	Bering
<g/>
,	,	kIx,	,
dánského	dánský	k2eAgMnSc2d1	dánský
kapitána	kapitán	k1gMnSc2	kapitán
v	v	k7c6	v
ruských	ruský	k2eAgFnPc6d1	ruská
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
průlivem	průliv	k1gInSc7	průliv
proplul	proplout	k5eAaPmAgInS	proplout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
evropskou	evropský	k2eAgFnSc4d1	Evropská
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Aljašky	Aljaška	k1gFnPc4	Aljaška
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považována	považován	k2eAgFnSc1d1	považována
ruská	ruský	k2eAgFnSc1d1	ruská
St.	st.	kA	st.
Gabriel	Gabriel	k1gMnSc1	Gabriel
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
navigátora	navigátor	k1gMnSc2	navigátor
Michaila	Michail	k1gMnSc2	Michail
Gvozdeva	Gvozdev	k1gMnSc2	Gvozdev
a	a	k8xC	a
asistenta	asistent	k1gMnSc2	asistent
navigátora	navigátor	k1gMnSc2	navigátor
Ivana	Ivan	k1gMnSc2	Ivan
Fjodorova	Fjodorův	k2eAgInSc2d1	Fjodorův
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
srpna	srpen	k1gInSc2	srpen
1732	[number]	k4	1732
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnSc1d2	menší
sdružení	sdružení	k1gNnSc1	sdružení
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
začalo	začít	k5eAaPmAgNnS	začít
plout	plout	k5eAaImF	plout
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
Sibiře	Sibiř	k1gFnSc2	Sibiř
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Aleutské	aleutský	k2eAgInPc4d1	aleutský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
první	první	k4xOgNnSc1	první
stálé	stálý	k2eAgNnSc1d1	stálé
evropské	evropský	k2eAgNnSc1d1	Evropské
osídlení	osídlení	k1gNnSc1	osídlení
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Rusko-americká	ruskomerický	k2eAgFnSc1d1	rusko-americká
společnost	společnost	k1gFnSc1	společnost
provedla	provést	k5eAaPmAgFnS	provést
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
kolonizační	kolonizační	k2eAgInSc4d1	kolonizační
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
především	především	k6eAd1	především
kožešinovému	kožešinový	k2eAgInSc3d1	kožešinový
obchodu	obchod	k1gInSc3	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Novo-Archangelsk	Novo-Archangelsk	k1gInSc1	Novo-Archangelsk
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Sitka	Sitek	k1gMnSc2	Sitek
<g/>
)	)	kIx)	)
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Aljašce	Aljaška	k1gFnSc6	Aljaška
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Ruské	ruský	k2eAgFnSc2d1	ruská
Ameriky	Amerika	k1gFnSc2	Amerika
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
až	až	k9	až
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
zde	zde	k6eAd1	zde
také	také	k9	také
působili	působit	k5eAaImAgMnP	působit
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
Američané	Američan	k1gMnPc1	Američan
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1867	[number]	k4	1867
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
koupily	koupit	k5eAaPmAgInP	koupit
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
Aljašku	Aljaška	k1gFnSc4	Aljaška
za	za	k7c4	za
7,2	[number]	k4	7,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
5	[number]	k4	5
centů	cent	k1gInPc2	cent
za	za	k7c4	za
hektar	hektar	k1gInSc4	hektar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
prodeje	prodej	k1gInSc2	prodej
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
od	od	k7c2	od
obtížné	obtížný	k2eAgFnSc2d1	obtížná
ruské	ruský	k2eAgFnSc2d1	ruská
finanční	finanční	k2eAgFnSc2d1	finanční
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
po	po	k7c4	po
skutečnost	skutečnost	k1gFnSc4	skutečnost
že	že	k8xS	že
si	se	k3xPyFc3	se
ruské	ruský	k2eAgNnSc1d1	ruské
vedení	vedení	k1gNnSc1	vedení
uvědomovalo	uvědomovat	k5eAaImAgNnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Aljaška	Aljaška	k1gFnSc1	Aljaška
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neudržitelná	udržitelný	k2eNgFnSc1d1	neudržitelná
kvůli	kvůli	k7c3	kvůli
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
a	a	k8xC	a
blízkosti	blízkost	k1gFnSc3	blízkost
britského	britský	k2eAgNnSc2d1	Britské
dominia	dominion	k1gNnSc2	dominion
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
nemalým	malý	k2eNgInSc7d1	nemalý
důvodem	důvod	k1gInSc7	důvod
krach	krach	k1gInSc1	krach
projektu	projekt	k1gInSc2	projekt
výstavby	výstavba	k1gFnSc2	výstavba
telegrafní	telegrafní	k2eAgFnSc2d1	telegrafní
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
do	do	k7c2	do
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Beringovu	Beringův	k2eAgFnSc4d1	Beringova
úžinu	úžina	k1gFnSc4	úžina
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojovala	napojovat	k5eAaImAgFnS	napojovat
na	na	k7c4	na
celoevropskou	celoevropský	k2eAgFnSc4d1	celoevropská
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
investovaly	investovat	k5eAaBmAgFnP	investovat
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
přes	přes	k7c4	přes
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
strana	strana	k1gFnSc1	strana
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc4d2	menší
částku	částka	k1gFnSc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
pro	pro	k7c4	pro
i	i	k8xC	i
proti	proti	k7c3	proti
se	se	k3xPyFc4	se
nasčítala	nasčítat	k5eAaPmAgNnP	nasčítat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
došlo	dojít	k5eAaPmAgNnS	dojít
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1867	[number]	k4	1867
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Williamem	William	k1gInSc7	William
H.	H.	kA	H.
Sewardem	Seward	k1gInSc7	Seward
k	k	k7c3	k
uspořádání	uspořádání	k1gNnSc3	uspořádání
koupě	koupě	k1gFnSc2	koupě
od	od	k7c2	od
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koupě	koupě	k1gFnSc1	koupě
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázala	ukázat	k5eAaPmAgFnS	ukázat
pro	pro	k7c4	pro
USA	USA	kA	USA
velice	velice	k6eAd1	velice
výhodnou	výhodný	k2eAgFnSc4d1	výhodná
z	z	k7c2	z
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
i	i	k8xC	i
strategického	strategický	k2eAgNnSc2d1	strategické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Otevřel	otevřít	k5eAaPmAgInS	otevřít
se	se	k3xPyFc4	se
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
přírodnímu	přírodní	k2eAgInSc3d1	přírodní
bohatství	bohatství	k1gNnSc2	bohatství
–	–	k?	–
nerostné	nerostný	k2eAgFnSc2d1	nerostná
suroviny	surovina	k1gFnSc2	surovina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnPc1	možnost
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
kožešiny	kožešina	k1gFnPc1	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
byla	být	k5eAaImAgFnS	být
také	také	k9	také
objevena	objevit	k5eAaPmNgFnS	objevit
velká	velká	k1gFnSc1	velká
naleziště	naleziště	k1gNnSc2	naleziště
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
okolo	okolo	k7c2	okolo
řek	řeka	k1gFnPc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
Klondike	Klondike	k1gFnPc2	Klondike
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
začaly	začít	k5eAaPmAgFnP	začít
během	během	k7c2	během
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
přicházet	přicházet	k5eAaImF	přicházet
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
kopáčů	kopáč	k1gInPc2	kopáč
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
bohaté	bohatý	k2eAgNnSc4d1	bohaté
naleziště	naleziště	k1gNnSc4	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
se	se	k3xPyFc4	se
za	za	k7c4	za
10	[number]	k4	10
let	léto	k1gNnPc2	léto
téměř	téměř	k6eAd1	téměř
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
lov	lov	k1gInSc1	lov
lososů	losos	k1gMnPc2	losos
a	a	k8xC	a
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objevy	objev	k1gInPc7	objev
nalezišť	naleziště	k1gNnPc2	naleziště
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
vozové	vozový	k2eAgFnPc1d1	vozová
silnice	silnice	k1gFnPc1	silnice
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
koleje	kolej	k1gFnPc1	kolej
<g/>
,	,	kIx,	,
přístavy	přístav	k1gInPc1	přístav
a	a	k8xC	a
trajekty	trajekt	k1gInPc1	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
letectví	letectví	k1gNnSc6	letectví
vláda	vláda	k1gFnSc1	vláda
podporovala	podporovat	k5eAaImAgFnS	podporovat
také	také	k9	také
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
údržbu	údržba	k1gFnSc4	údržba
letišť	letiště	k1gNnPc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
působení	působení	k1gNnSc4	působení
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Eisenhowera	Eisenhower	k1gMnSc2	Eisenhower
byla	být	k5eAaImAgFnS	být
Aljaška	Aljaška	k1gFnSc1	Aljaška
přijata	přijmout	k5eAaPmNgFnS	přijmout
za	za	k7c4	za
stát	stát	k1gInSc4	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Aljaška	Aljaška	k1gFnSc1	Aljaška
stala	stát	k5eAaPmAgFnS	stát
49	[number]	k4	49
<g/>
.	.	kIx.	.
americkým	americký	k2eAgInSc7d1	americký
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
severoamerického	severoamerický	k2eAgInSc2d1	severoamerický
kontinentu	kontinent	k1gInSc2	kontinent
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
USA	USA	kA	USA
jej	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
jednak	jednak	k8xC	jednak
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
Aljašský	aljašský	k2eAgInSc1d1	aljašský
záliv	záliv	k1gInSc1	záliv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Britskou	britský	k2eAgFnSc7d1	britská
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
a	a	k8xC	a
Yukonským	Yukonský	k2eAgNnSc7d1	Yukonský
teritoriem	teritorium	k1gNnSc7	teritorium
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
ji	on	k3xPp3gFnSc4	on
Beringovo	Beringův	k2eAgNnSc1d1	Beringovo
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Beringův	Beringův	k2eAgInSc4d1	Beringův
průliv	průliv	k1gInSc4	průliv
a	a	k8xC	a
Čukotské	čukotský	k2eAgNnSc4d1	Čukotské
moře	moře	k1gNnSc4	moře
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
od	od	k7c2	od
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
Beaufortovo	Beaufortův	k2eAgNnSc1d1	Beaufortovo
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
717	[number]	k4	717
856	[number]	k4	856
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
14	[number]	k4	14
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
měří	měřit	k5eAaImIp3nS	měřit
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
2	[number]	k4	2
240	[number]	k4	240
km	km	kA	km
a	a	k8xC	a
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
3	[number]	k4	3
550	[number]	k4	550
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
580	[number]	k4	580
m.	m.	k?	m.
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
stát	stát	k1gInSc4	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
součet	součet	k1gInSc1	součet
rozlohy	rozloha	k1gFnSc2	rozloha
dvou	dva	k4xCgInPc2	dva
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
následujících	následující	k2eAgFnPc2d1	následující
(	(	kIx(	(
<g/>
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
též	též	k9	též
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
USA	USA	kA	USA
nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
a	a	k8xC	a
nejčlenitější	členitý	k2eAgNnSc1d3	nejčlenitější
mořské	mořský	k2eAgNnSc1d1	mořské
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zemi	zem	k1gFnSc3	zem
patří	patřit	k5eAaImIp3nP	patřit
četné	četný	k2eAgInPc1d1	četný
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
od	od	k7c2	od
poloostrova	poloostrov	k1gInSc2	poloostrov
Aljaška	Aljaška	k1gFnSc1	Aljaška
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
zatíná	zatínat	k5eAaImIp3nS	zatínat
ostruha	ostruha	k1gFnSc1	ostruha
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
četné	četný	k2eAgInPc1d1	četný
aktivní	aktivní	k2eAgInPc1d1	aktivní
vulkány	vulkán	k1gInPc1	vulkán
<g/>
.	.	kIx.	.
</s>
<s>
Členité	členitý	k2eAgNnSc1d1	členité
je	být	k5eAaImIp3nS	být
i	i	k9	i
aljašské	aljašský	k2eAgNnSc1d1	aljašské
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
pohořími	pohoří	k1gNnPc7	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
země	zem	k1gFnSc2	zem
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
permafrost	permafrost	k1gFnSc1	permafrost
a	a	k8xC	a
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
65	[number]	k4	65
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
vedeno	veden	k2eAgNnSc1d1	vedeno
jako	jako	k8xS	jako
některý	některý	k3yIgInSc1	některý
z	z	k7c2	z
typů	typ	k1gInPc2	typ
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
10	[number]	k4	10
%	%	kIx~	%
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
společenství	společenství	k1gNnPc2	společenství
a	a	k8xC	a
korporací	korporace	k1gFnPc2	korporace
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Alaska	Alask	k1gInSc2	Alask
Native	Natiev	k1gFnSc2	Natiev
Claims	Claimsa	k1gFnPc2	Claimsa
Settlement	settlement	k1gInSc1	settlement
Act	Act	k1gMnSc1	Act
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
právě	právě	k9	právě
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
zde	zde	k6eAd1	zde
západovýchodní	západovýchodní	k2eAgInSc4d1	západovýchodní
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
celek	celek	k1gInSc1	celek
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
v	v	k7c6	v
orografickém	orografický	k2eAgNnSc6d1	orografické
členění	členění	k1gNnSc6	členění
i	i	k8xC	i
část	část	k1gFnSc4	část
kanadského	kanadský	k2eAgNnSc2d1	kanadské
území	území	k1gNnSc2	území
představuje	představovat	k5eAaImIp3nS	představovat
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
poloostrov	poloostrov	k1gInSc4	poloostrov
s	s	k7c7	s
obloukem	oblouk	k1gInSc7	oblouk
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
řetěz	řetěz	k1gInSc1	řetěz
150	[number]	k4	150
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
1	[number]	k4	1
800	[number]	k4	800
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
horským	horský	k2eAgInSc7d1	horský
systémem	systém	k1gInSc7	systém
Mackenziho	Mackenzi	k1gMnSc2	Mackenzi
pohoří	pohořet	k5eAaPmIp3nS	pohořet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
Denali	Denali	k1gFnSc2	Denali
(	(	kIx(	(
<g/>
6	[number]	k4	6
190	[number]	k4	190
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přírodním	přírodní	k2eAgNnSc6d1	přírodní
horském	horský	k2eAgNnSc6d1	horské
pásmu	pásmo	k1gNnSc6	pásmo
leží	ležet	k5eAaImIp3nP	ležet
nejrozsáhlejší	rozsáhlý	k2eAgInPc1d3	nejrozsáhlejší
pevninské	pevninský	k2eAgInPc1d1	pevninský
ledovce	ledovec	k1gInPc1	ledovec
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
splazy	splaz	k1gInPc1	splaz
většinou	většinou	k6eAd1	většinou
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
k	k	k7c3	k
mořské	mořský	k2eAgFnSc3d1	mořská
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
svahy	svah	k1gInPc4	svah
i	i	k8xC	i
roviny	rovina	k1gFnPc4	rovina
arktická	arktický	k2eAgFnSc1d1	arktická
tundra	tundra	k1gFnSc1	tundra
a	a	k8xC	a
asi	asi	k9	asi
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
km2	km2	k4	km2
smrkových	smrkový	k2eAgInPc2d1	smrkový
a	a	k8xC	a
jedlových	jedlový	k2eAgInPc2d1	jedlový
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
rozptýleno	rozptýlit	k5eAaPmNgNnS	rozptýlit
přes	přes	k7c4	přes
3	[number]	k4	3
miliony	milion	k4xCgInPc7	milion
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
nad	nad	k7c4	nad
8	[number]	k4	8
hektarů	hektar	k1gInPc2	hektar
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
glaciálních	glaciální	k2eAgInPc2d1	glaciální
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
Aljašky	Aljaška	k1gFnSc2	Aljaška
se	se	k3xPyFc4	se
svažuje	svažovat	k5eAaImIp3nS	svažovat
od	od	k7c2	od
okrajových	okrajový	k2eAgFnPc2d1	okrajová
hor	hora	k1gFnPc2	hora
do	do	k7c2	do
centrálních	centrální	k2eAgFnPc2d1	centrální
pánví	pánev	k1gFnPc2	pánev
a	a	k8xC	a
plošin	plošina	k1gFnPc2	plošina
<g/>
,	,	kIx,	,
ukloněných	ukloněný	k2eAgFnPc2d1	ukloněná
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
protéká	protékat	k5eAaImIp3nS	protékat
největší	veliký	k2eAgFnSc1d3	veliký
aljašská	aljašský	k2eAgFnSc1d1	aljašská
řeka	řeka	k1gFnSc1	řeka
Yukon	Yukon	k1gInSc1	Yukon
<g/>
,	,	kIx,	,
ústící	ústící	k2eAgInSc1d1	ústící
do	do	k7c2	do
Beringova	Beringův	k2eAgNnSc2d1	Beringovo
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
čára	čára	k1gFnSc1	čára
měří	měřit	k5eAaImIp3nS	měřit
téměř	téměř	k6eAd1	téměř
53	[number]	k4	53
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
území	území	k1gNnSc3	území
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
lemujících	lemující	k2eAgFnPc2d1	lemující
zejména	zejména	k9	zejména
jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Orografické	orografický	k2eAgFnPc1d1	orografická
oblasti	oblast	k1gFnPc1	oblast
Aljašky	Aljaška	k1gFnSc2	Aljaška
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
uplatněním	uplatnění	k1gNnSc7	uplatnění
geologických	geologický	k2eAgInPc2d1	geologický
<g/>
,	,	kIx,	,
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
a	a	k8xC	a
klimatických	klimatický	k2eAgInPc2d1	klimatický
faktorů	faktor	k1gInPc2	faktor
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtyři	čtyři	k4xCgInPc4	čtyři
velké	velký	k2eAgInPc4d1	velký
celky	celek	k1gInPc4	celek
<g/>
:	:	kIx,	:
jižní	jižní	k2eAgFnSc4d1	jižní
horskou	horský	k2eAgFnSc4d1	horská
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
mezihorské	mezihorský	k2eAgFnPc4d1	Mezihorská
plošiny	plošina	k1gFnPc4	plošina
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc4d1	severní
horskou	horský	k2eAgFnSc4d1	horská
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
arktické	arktický	k2eAgFnPc4d1	arktická
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
nížiny	nížina	k1gFnPc4	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1	jižní
horskou	horský	k2eAgFnSc4d1	horská
oblast	oblast	k1gFnSc4	oblast
tvoří	tvořit	k5eAaImIp3nS	tvořit
široký	široký	k2eAgInSc1d1	široký
pás	pás	k1gInSc1	pás
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
paralelní	paralelní	k2eAgInPc1d1	paralelní
s	s	k7c7	s
jižním	jižní	k2eAgNnSc7d1	jižní
pobřežím	pobřeží	k1gNnSc7	pobřeží
a	a	k8xC	a
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
konkávní	konkávní	k2eAgInSc4d1	konkávní
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
horské	horský	k2eAgInPc4d1	horský
hřbety	hřbet	k1gInPc4	hřbet
nejmohutnější	mohutný	k2eAgFnSc1d3	nejmohutnější
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východu	východ	k1gInSc3	východ
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
hory	hora	k1gFnPc4	hora
jsou	být	k5eAaImIp3nP	být
pokračováním	pokračování	k1gNnSc7	pokračování
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
pohoří	pohoří	k1gNnSc2	pohoří
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
mají	mít	k5eAaImIp3nP	mít
shodnou	shodný	k2eAgFnSc4d1	shodná
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
podobný	podobný	k2eAgInSc4d1	podobný
reliéf	reliéf	k1gInSc4	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
1	[number]	k4	1
900	[number]	k4	900
km	km	kA	km
podél	podél	k7c2	podél
Aljašského	aljašský	k2eAgInSc2d1	aljašský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
těchto	tento	k3xDgFnPc2	tento
hor	hora	k1gFnPc2	hora
tvoří	tvořit	k5eAaImIp3nS	tvořit
obrovský	obrovský	k2eAgInSc1d1	obrovský
granitový	granitový	k2eAgInSc1d1	granitový
batolit	batolit	k1gInSc1	batolit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvřel	vyvřít	k5eAaPmAgInS	vyvřít
v	v	k7c6	v
juře	jura	k1gFnSc6	jura
až	až	k8xS	až
křídě	křída	k1gFnSc6	křída
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Pobřežními	pobřežní	k2eAgFnPc7d1	pobřežní
horami	hora	k1gFnPc7	hora
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
oblouk	oblouk	k1gInSc1	oblouk
Alexandrova	Alexandrův	k2eAgNnSc2d1	Alexandrovo
souostroví	souostroví	k1gNnSc2	souostroví
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1	[number]	k4	1
100	[number]	k4	100
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
oddělených	oddělený	k2eAgInPc2d1	oddělený
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
četnými	četný	k2eAgInPc7d1	četný
fjordy	fjord	k1gInPc7	fjord
a	a	k8xC	a
hlubokými	hluboký	k2eAgInPc7d1	hluboký
průlivy	průliv	k1gInPc7	průliv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
zlomové	zlomový	k2eAgFnSc6d1	zlomová
linii	linie	k1gFnSc6	linie
směřující	směřující	k2eAgNnPc1d1	směřující
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
důležité	důležitý	k2eAgFnPc1d1	důležitá
vodní	vodní	k2eAgFnPc1d1	vodní
cesty	cesta	k1gFnPc1	cesta
pro	pro	k7c4	pro
příbřežní	příbřežní	k2eAgFnSc4d1	příbřežní
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
ostrovy	ostrov	k1gInPc1	ostrov
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zaplavením	zaplavení	k1gNnSc7	zaplavení
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
vrcholy	vrchol	k1gInPc1	vrchol
se	se	k3xPyFc4	se
příkře	příkro	k6eAd1	příkro
zvedají	zvedat	k5eAaImIp3nP	zvedat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
900	[number]	k4	900
až	až	k9	až
1	[number]	k4	1
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
Elias	Eliasa	k1gFnPc2	Eliasa
Mountains	Mountainsa	k1gFnPc2	Mountainsa
jsou	být	k5eAaImIp3nP	být
horským	horský	k2eAgInSc7d1	horský
uzlem	uzel	k1gInSc7	uzel
a	a	k8xC	a
křižovatkou	křižovatka	k1gFnSc7	křižovatka
aljašských	aljašský	k2eAgInPc2d1	aljašský
pobřežních	pobřežní	k2eAgInPc2d1	pobřežní
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Táhnou	táhnout	k5eAaImIp3nP	táhnout
se	se	k3xPyFc4	se
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
500	[number]	k4	500
km	km	kA	km
od	od	k7c2	od
Cross	Crossa	k1gFnPc2	Crossa
Sound	Sounda	k1gFnPc2	Sounda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
až	až	k6eAd1	až
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
Copper	Coppra	k1gFnPc2	Coppra
River	Rivero	k1gNnPc2	Rivero
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
šířka	šířka	k1gFnSc1	šířka
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
160	[number]	k4	160
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
štít	štít	k1gInSc1	štít
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Mt	Mt	k1gFnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Logan	Logan	k1gInSc1	Logan
(	(	kIx(	(
<g/>
6	[number]	k4	6
050	[number]	k4	050
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
kontinentu	kontinent	k1gInSc2	kontinent
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
kanadském	kanadský	k2eAgNnSc6d1	kanadské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
málo	málo	k1gNnSc4	málo
nižší	nízký	k2eAgMnSc1d2	nižší
je	být	k5eAaImIp3nS	být
štít	štít	k1gInSc1	štít
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Sant	Sant	k2eAgInSc1d1	Sant
Elias	Elias	k1gInSc1	Elias
(	(	kIx(	(
<g/>
5	[number]	k4	5
488	[number]	k4	488
<g/>
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
z	z	k7c2	z
pobřežního	pobřežní	k2eAgInSc2d1	pobřežní
masívu	masív	k1gInSc2	masív
(	(	kIx(	(
<g/>
náleží	náležet	k5eAaImIp3nP	náležet
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmalebnějších	malebný	k2eAgFnPc2d3	nejmalebnější
hor	hora	k1gFnPc2	hora
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
Elias	Eliasa	k1gFnPc2	Eliasa
Mountains	Mountainsa	k1gFnPc2	Mountainsa
jsou	být	k5eAaImIp3nP	být
nejvíce	nejvíce	k6eAd1	nejvíce
zaledněným	zaledněný	k2eAgNnSc7d1	zaledněné
pohořím	pohoří	k1gNnSc7	pohoří
americké	americký	k2eAgFnSc2d1	americká
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Ledovcový	ledovcový	k2eAgInSc1d1	ledovcový
splaz	splaz	k1gInSc1	splaz
Hubbard	Hubbard	k1gInSc1	Hubbard
Glacier	Glacier	k1gInSc1	Glacier
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
144	[number]	k4	144
km	km	kA	km
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
je	být	k5eAaImIp3nS	být
až	až	k9	až
16	[number]	k4	16
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Plošně	plošně	k6eAd1	plošně
největším	veliký	k2eAgMnSc7d3	veliký
aljašským	aljašský	k2eAgMnSc7d1	aljašský
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
severoamerickým	severoamerický	k2eAgInSc7d1	severoamerický
<g/>
)	)	kIx)	)
ledovcem	ledovec	k1gInSc7	ledovec
je	být	k5eAaImIp3nS	být
Malaspina	Malaspina	k1gFnSc1	Malaspina
Glacier	Glacier	k1gInSc1	Glacier
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
plochu	plocha	k1gFnSc4	plocha
2	[number]	k4	2
200	[number]	k4	200
km2	km2	k4	km2
na	na	k7c6	na
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
plošinách	plošina	k1gFnPc6	plošina
mezi	mezi	k7c7	mezi
zátokami	zátoka	k1gFnPc7	zátoka
Jakutat	Jakutat	k1gMnSc1	Jakutat
Bay	Bay	k1gMnSc1	Bay
a	a	k8xC	a
Ice	Ice	k1gMnSc1	Ice
Bay	Bay	k1gMnSc1	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ledovec	ledovec	k1gInSc1	ledovec
"	"	kIx"	"
<g/>
zamrzl	zamrznout	k5eAaPmAgInS	zamrznout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
úzkým	úzký	k2eAgInSc7d1	úzký
<g/>
,	,	kIx,	,
nezaledněným	zaledněný	k2eNgInSc7d1	zaledněný
pruhem	pruh	k1gInSc7	pruh
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ledovci	ledovec	k1gInSc6	ledovec
brání	bránit	k5eAaImIp3nP	bránit
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
jediným	jediný	k2eAgInSc7d1	jediný
odtokem	odtok	k1gInSc7	odtok
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
pouze	pouze	k6eAd1	pouze
úzký	úzký	k2eAgInSc1d1	úzký
fjord	fjord	k1gInSc1	fjord
v	v	k7c6	v
Ice	Ice	k1gFnSc6	Ice
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc1d1	Dlouhé
mořské	mořský	k2eAgInPc1d1	mořský
zátoky	zátok	k1gInPc1	zátok
pronikají	pronikat	k5eAaImIp3nP	pronikat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
závěrech	závěr	k1gInPc6	závěr
trčí	trčet	k5eAaImIp3nP	trčet
bílé	bílý	k2eAgInPc1d1	bílý
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
co	co	k9	co
chvíli	chvíle	k1gFnSc4	chvíle
odlamují	odlamovat	k5eAaImIp3nP	odlamovat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
kry	kra	k1gFnPc4	kra
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
ze	z	k7c2	z
zdejších	zdejší	k2eAgMnPc2d1	zdejší
ledovcových	ledovcový	k2eAgMnPc2d1	ledovcový
splazů	splaz	k1gInPc2	splaz
je	být	k5eAaImIp3nS	být
Brady	brada	k1gFnPc4	brada
Glacier	Glaciero	k1gNnPc2	Glaciero
<g/>
,	,	kIx,	,
stékající	stékající	k2eAgFnSc1d1	stékající
z	z	k7c2	z
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
do	do	k7c2	do
Taylor	Taylora	k1gFnPc2	Taylora
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Udivuje	udivovat	k5eAaImIp3nS	udivovat
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
hmotou	hmota	k1gFnSc7	hmota
ledovce	ledovec	k1gInSc2	ledovec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
šířce	šířka	k1gFnSc6	šířka
čtyř	čtyři	k4xCgInPc2	čtyři
kilometrů	kilometr	k1gInPc2	kilometr
visí	viset	k5eAaImIp3nS	viset
jako	jako	k9	jako
nehybná	hybný	k2eNgFnSc1d1	nehybná
lavina	lavina	k1gFnSc1	lavina
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaPmIp3nS	vypadat
zlověstně	zlověstně	k6eAd1	zlověstně
a	a	k8xC	a
nebezpečně	bezpečně	k6eNd1	bezpečně
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
otevřenými	otevřený	k2eAgFnPc7d1	otevřená
trhlinami	trhlina	k1gFnPc7	trhlina
a	a	k8xC	a
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
<g/>
,	,	kIx,	,
temnými	temný	k2eAgFnPc7d1	temná
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
v	v	k7c6	v
čelní	čelní	k2eAgFnSc6d1	čelní
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tunely	tunel	k1gInPc1	tunel
protavily	protavit	k5eAaPmAgFnP	protavit
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
propadají	propadat	k5eAaPmIp3nP	propadat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
ledovce	ledovec	k1gInSc2	ledovec
do	do	k7c2	do
trhlin	trhlina	k1gFnPc2	trhlina
a	a	k8xC	a
pak	pak	k6eAd1	pak
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
podpovrchovými	podpovrchový	k2eAgInPc7d1	podpovrchový
tunely	tunel	k1gInPc7	tunel
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
tu	tu	k6eAd1	tu
i	i	k9	i
význačná	význačný	k2eAgFnSc1d1	význačná
chráněná	chráněný	k2eAgFnSc1d1	chráněná
oblast	oblast	k1gFnSc1	oblast
Glacier	Glacier	k1gInSc1	Glacier
Bay	Bay	k1gFnSc1	Bay
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
11	[number]	k4	11
000	[number]	k4	000
km2	km2	k4	km2
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
mořského	mořský	k2eAgNnSc2d1	mořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
fjordů	fjord	k1gInPc2	fjord
a	a	k8xC	a
zálivů	záliv	k1gInPc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
velkých	velký	k2eAgInPc2d1	velký
ledovcových	ledovcový	k2eAgInPc2d1	ledovcový
splazů	splaz	k1gInPc2	splaz
se	se	k3xPyFc4	se
spouští	spouštět	k5eAaImIp3nS	spouštět
z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
hřbetů	hřbet	k1gInPc2	hřbet
do	do	k7c2	do
Ice	Ice	k1gFnSc2	Ice
Bay	Bay	k1gFnSc2	Bay
a	a	k8xC	a
zanášejí	zanášet	k5eAaImIp3nP	zanášet
ji	on	k3xPp3gFnSc4	on
obrovskými	obrovský	k2eAgInPc7d1	obrovský
plovoucími	plovoucí	k2eAgInPc7d1	plovoucí
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Zátoka	zátoka	k1gFnSc1	zátoka
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
100	[number]	k4	100
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
z	z	k7c2	z
letadla	letadlo	k1gNnSc2	letadlo
připomíná	připomínat	k5eAaImIp3nS	připomínat
rozvětvený	rozvětvený	k2eAgInSc1d1	rozvětvený
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Chugach	Chugach	k1gInSc1	Chugach
Mountains	Mountains	k1gInSc1	Mountains
–	–	k?	–
Kenai	Kena	k1gFnSc2	Kena
Mountains	Mountainsa	k1gFnPc2	Mountainsa
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
pobřežního	pobřežní	k2eAgNnSc2d1	pobřežní
pásma	pásmo	k1gNnSc2	pásmo
hor	hora	k1gFnPc2	hora
táhnoucích	táhnoucí	k2eAgFnPc2d1	táhnoucí
se	se	k3xPyFc4	se
až	až	k6eAd1	až
k	k	k7c3	k
poloostrovu	poloostrov	k1gInSc3	poloostrov
Kenai	Kena	k1gFnSc2	Kena
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
výběžkem	výběžek	k1gInSc7	výběžek
pobřežního	pobřežní	k2eAgNnSc2d1	pobřežní
horského	horský	k2eAgNnSc2d1	horské
pásma	pásmo	k1gNnSc2	pásmo
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
Kodiak	Kodiak	k1gInSc1	Kodiak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
dost	dost	k6eAd1	dost
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Jím	on	k3xPp3gNnSc7	on
také	také	k9	také
končí	končit	k5eAaImIp3nS	končit
jednotná	jednotný	k2eAgFnSc1d1	jednotná
geologická	geologický	k2eAgFnSc1d1	geologická
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
Pobřežní	pobřežní	k1gMnPc4	pobřežní
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Wrangell	Wrangell	k1gInSc1	Wrangell
Mountains	Mountains	k1gInSc1	Mountains
vyvřely	vyvřít	k5eAaPmAgInP	vyvřít
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
tektonické	tektonický	k2eAgFnSc6d1	tektonická
poruše	porucha	k1gFnSc6	porucha
<g/>
,	,	kIx,	,
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
Pobřežních	pobřežní	k1gMnPc2	pobřežní
hor.	hor.	k?	hor.
Erupce	erupce	k1gFnSc2	erupce
bazaltových	bazaltový	k2eAgFnPc2d1	bazaltová
a	a	k8xC	a
andezitových	andezitový	k2eAgFnPc2d1	andezitová
láv	láva	k1gFnPc2	láva
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
a	a	k8xC	a
poslední	poslední	k2eAgFnSc6d1	poslední
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sopečný	sopečný	k2eAgInSc1d1	sopečný
kužel	kužel	k1gInSc1	kužel
Mount	Mount	k1gMnSc1	Mount
Blackburn	Blackburn	k1gMnSc1	Blackburn
měří	měřit	k5eAaImIp3nS	měřit
5	[number]	k4	5
036	[number]	k4	036
m.	m.	k?	m.
Alaska	Alaska	k1gFnSc1	Alaska
Range	Rang	k1gFnSc2	Rang
patří	patřit	k5eAaImIp3nS	patřit
geneticky	geneticky	k6eAd1	geneticky
k	k	k7c3	k
nevadské	nevadský	k2eAgFnSc3d1	Nevadská
orogenezi	orogeneze	k1gFnSc3	orogeneze
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
aleutským	aleutský	k2eAgNnSc7d1	aleutský
pásmem	pásmo	k1gNnSc7	pásmo
už	už	k9	už
poněkud	poněkud	k6eAd1	poněkud
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
geologickou	geologický	k2eAgFnSc7d1	geologická
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Četná	četný	k2eAgNnPc1d1	četné
granitová	granitový	k2eAgNnPc1d1	granitové
tělesa	těleso	k1gNnPc1	těleso
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeho	jeho	k3xOp3gInPc4	jeho
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
hřebeny	hřeben	k1gInPc4	hřeben
a	a	k8xC	a
štíty	štít	k1gInPc4	štít
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
horstva	horstvo	k1gNnSc2	horstvo
leží	ležet	k5eAaImIp3nS	ležet
nezvrásněné	zvrásněný	k2eNgInPc4d1	zvrásněný
třetihorní	třetihorní	k2eAgInPc4d1	třetihorní
sedimenty	sediment	k1gInPc4	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Aljašské	aljašský	k2eAgNnSc1d1	aljašské
pohoří	pohoří	k1gNnSc1	pohoří
představuje	představovat	k5eAaImIp3nS	představovat
úzký	úzký	k2eAgInSc4d1	úzký
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
řetěz	řetěz	k1gInSc4	řetěz
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jakoby	jakoby	k8xS	jakoby
vycházely	vycházet	k5eAaImAgFnP	vycházet
ze	z	k7c2	z
sopečného	sopečný	k2eAgNnSc2d1	sopečné
Wrangellova	Wrangellův	k2eAgNnSc2d1	Wrangellův
pohoří	pohoří	k1gNnSc2	pohoří
a	a	k8xC	a
táhnou	táhnout	k5eAaImIp3nP	táhnout
se	se	k3xPyFc4	se
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
až	až	k8xS	až
k	k	k7c3	k
tektonické	tektonický	k2eAgFnSc3d1	tektonická
poruše	porucha	k1gFnSc3	porucha
jezera	jezero	k1gNnSc2	jezero
Iliamna	Iliamn	k1gInSc2	Iliamn
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
víceméně	víceméně	k9	víceméně
paralelních	paralelní	k2eAgInPc2d1	paralelní
hřbetů	hřbet	k1gInPc2	hřbet
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
50	[number]	k4	50
km	km	kA	km
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
2	[number]	k4	2
500	[number]	k4	500
m.	m.	k?	m.
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
vyvřelo	vyvřít	k5eAaPmAgNnS	vyvřít
v	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
pět	pět	k4xCc4	pět
sopečných	sopečný	k2eAgInPc2d1	sopečný
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
Denali	Denali	k1gFnSc1	Denali
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
6	[number]	k4	6
194	[number]	k4	194
m	m	kA	m
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Sopečný	sopečný	k2eAgInSc4d1	sopečný
vrchol	vrchol	k1gInSc4	vrchol
Mount	Mount	k1gMnSc1	Mount
Foraker	Foraker	k1gMnSc1	Foraker
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
5	[number]	k4	5
300	[number]	k4	300
m	m	kA	m
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
měří	měřit	k5eAaImIp3nP	měřit
kolem	kolem	k7c2	kolem
4	[number]	k4	4
000	[number]	k4	000
m.	m.	k?	m.
Granitové	granitový	k2eAgInPc1d1	granitový
štíty	štít	k1gInPc1	štít
Aljašského	aljašský	k2eAgNnSc2d1	aljašské
pohoří	pohoří	k1gNnSc2	pohoří
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
případech	případ	k1gInPc6	případ
3	[number]	k4	3
000	[number]	k4	000
m.	m.	k?	m.
Od	od	k7c2	od
pevninského	pevninský	k2eAgNnSc2d1	pevninské
Aleutského	aleutský	k2eAgNnSc2d1	aleutský
pohoří	pohoří	k1gNnSc2	pohoří
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
oblouk	oblouk	k1gInSc1	oblouk
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
západu	západ	k1gInSc3	západ
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
menší	malý	k2eAgFnSc4d2	menší
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
od	od	k7c2	od
8	[number]	k4	8
do	do	k7c2	do
135	[number]	k4	135
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejzápadněji	západně	k6eAd3	západně
leží	ležet	k5eAaImIp3nS	ležet
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc4d1	velký
ostrov	ostrov	k1gInSc4	ostrov
Attu	Attus	k1gInSc2	Attus
<g/>
.	.	kIx.	.
</s>
<s>
Aleutský	aleutský	k2eAgInSc1d1	aleutský
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
oblouk	oblouk	k1gInSc1	oblouk
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
2	[number]	k4	2
500	[number]	k4	500
km	km	kA	km
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
asi	asi	k9	asi
80	[number]	k4	80
velkých	velký	k2eAgFnPc2d1	velká
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
minimálně	minimálně	k6eAd1	minimálně
36	[number]	k4	36
je	být	k5eAaImIp3nS	být
aktivních	aktivní	k2eAgFnPc2d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Výšky	výška	k1gFnPc1	výška
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vulkánů	vulkán	k1gInPc2	vulkán
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
1	[number]	k4	1
500	[number]	k4	500
do	do	k7c2	do
3	[number]	k4	3
000	[number]	k4	000
m	m	kA	m
a	a	k8xC	a
na	na	k7c6	na
vrcholech	vrchol	k1gInPc6	vrchol
starých	starý	k2eAgInPc2d1	starý
kuželů	kužel	k1gInPc2	kužel
někdy	někdy	k6eAd1	někdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
obrovské	obrovský	k2eAgFnPc1d1	obrovská
kaldery	kaldera	k1gFnPc1	kaldera
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2,5	[number]	k4	2,5
až	až	k9	až
13	[number]	k4	13
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Buldirská	Buldirský	k2eAgFnSc1d1	Buldirský
kaldera	kaldera	k1gFnSc1	kaldera
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozměry	rozměr	k1gInPc7	rozměr
43	[number]	k4	43
x	x	k?	x
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
jižním	jižní	k2eAgInSc7d1	jižní
horským	horský	k2eAgInSc7d1	horský
systémem	systém	k1gInSc7	systém
leží	ležet	k5eAaImIp3nS	ležet
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
tektonická	tektonický	k2eAgFnSc1d1	tektonická
sníženina	sníženina	k1gFnSc1	sníženina
<g/>
,	,	kIx,	,
ukloněná	ukloněný	k2eAgFnSc1d1	ukloněná
k	k	k7c3	k
Beringovu	Beringův	k2eAgNnSc3d1	Beringovo
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
dno	dno	k1gNnSc4	dno
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
ploché	plochý	k2eAgInPc1d1	plochý
horské	horský	k2eAgInPc1d1	horský
hřbety	hřbet	k1gInPc1	hřbet
<g/>
,	,	kIx,	,
náhorní	náhorní	k2eAgFnPc1d1	náhorní
plošiny	plošina	k1gFnPc1	plošina
a	a	k8xC	a
široká	široký	k2eAgNnPc1d1	široké
fluviální	fluviální	k2eAgNnPc1d1	fluviální
údolí	údolí	k1gNnPc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
kordillérského	kordillérský	k2eAgInSc2d1	kordillérský
systému	systém	k1gInSc2	systém
Basin	Basin	k1gMnSc1	Basin
and	and	k?	and
Range	Range	k1gInSc1	Range
<g/>
.	.	kIx.	.
</s>
<s>
Vysočiny	vysočina	k1gFnPc1	vysočina
a	a	k8xC	a
plošiny	plošina	k1gFnPc1	plošina
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
západě	západ	k1gInSc6	západ
výšky	výška	k1gFnSc2	výška
kolem	kolem	k7c2	kolem
600	[number]	k4	600
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
až	až	k9	až
do	do	k7c2	do
1	[number]	k4	1
500	[number]	k4	500
m	m	kA	m
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
splývají	splývat	k5eAaImIp3nP	splývat
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
horskými	horský	k2eAgInPc7d1	horský
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
jsou	být	k5eAaImIp3nP	být
význačné	význačný	k2eAgInPc1d1	význačný
horské	horský	k2eAgInPc1d1	horský
celky	celek	k1gInPc1	celek
–	–	k?	–
poloostrov	poloostrov	k1gInSc4	poloostrov
Seward	Sewarda	k1gFnPc2	Sewarda
a	a	k8xC	a
hory	hora	k1gFnSc2	hora
Kuskokwim	Kuskokwim	k1gMnSc1	Kuskokwim
–	–	k?	–
vyzdvižené	vyzdvižený	k2eAgNnSc1d1	vyzdvižené
podél	podél	k7c2	podél
zlomů	zlom	k1gInPc2	zlom
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
kolem	kolem	k7c2	kolem
1	[number]	k4	1
400	[number]	k4	400
m.	m.	k?	m.
V	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
byly	být	k5eAaImAgInP	být
zaledněny	zaledněn	k2eAgInPc1d1	zaledněn
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
na	na	k7c6	na
nich	on	k3xPp3gNnPc6	on
leží	ležet	k5eAaImIp3nP	ležet
malé	malý	k2eAgInPc1d1	malý
ledovce	ledovec	k1gInPc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
vyzdvižené	vyzdvižený	k2eAgFnSc6d1	vyzdvižená
části	část	k1gFnSc6	část
plošin	plošina	k1gFnPc2	plošina
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
horskými	horský	k2eAgMnPc7d1	horský
hřbety	hřbet	k1gMnPc7	hřbet
úzká	úzký	k2eAgFnSc1d1	úzká
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
neobyčejně	obyčejně	k6eNd1	obyčejně
hluboká	hluboký	k2eAgNnPc1d1	hluboké
údolí	údolí	k1gNnPc1	údolí
vyplněná	vyplněný	k2eAgNnPc1d1	vyplněné
jezery	jezero	k1gNnPc7	jezero
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
dna	dna	k1gFnSc1	dna
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
klesají	klesat	k5eAaImIp3nP	klesat
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
srázná	srázný	k2eAgNnPc1d1	srázné
tektonická	tektonický	k2eAgNnPc1d1	tektonické
údolí	údolí	k1gNnPc1	údolí
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgFnSc1d1	typická
hlavně	hlavně	k6eAd1	hlavně
pro	pro	k7c4	pro
severní	severní	k2eAgInPc4d1	severní
svahy	svah	k1gInPc4	svah
Pobřežních	pobřežní	k1gMnPc2	pobřežní
hor.	hor.	k?	hor.
Yukonské	Yukonský	k2eAgFnPc1d1	Yukonská
nížiny	nížina	k1gFnPc1	nížina
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
širokých	široký	k2eAgNnPc6d1	široké
erozních	erozní	k2eAgNnPc6d1	erozní
údolích	údolí	k1gNnPc6	údolí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
mnohá	mnohý	k2eAgFnSc1d1	mnohá
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
zlomových	zlomový	k2eAgFnPc6d1	zlomová
liniích	linie	k1gFnPc6	linie
systému	systém	k1gInSc2	systém
Denali	Denali	k1gFnSc2	Denali
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
údolí	údolí	k1gNnPc1	údolí
zanesena	zanesen	k2eAgNnPc1d1	zaneseno
vrstvami	vrstva	k1gFnPc7	vrstva
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
nížiny	nížina	k1gFnPc1	nížina
Centrální	centrální	k2eAgFnSc2d1	centrální
Aljašky	Aljaška	k1gFnSc2	Aljaška
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
lemují	lemovat	k5eAaImIp3nP	lemovat
i	i	k9	i
její	její	k3xOp3gInPc4	její
přítoky	přítok	k1gInPc4	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
300	[number]	k4	300
km	km	kA	km
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
50	[number]	k4	50
až	až	k9	až
150	[number]	k4	150
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
plochu	plocha	k1gFnSc4	plocha
přes	přes	k7c4	přes
2	[number]	k4	2
300	[number]	k4	300
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Yukonské	Yukonský	k2eAgFnPc1d1	Yukonská
nížiny	nížina	k1gFnPc1	nížina
tvoří	tvořit	k5eAaImIp3nP	tvořit
mocná	mocný	k2eAgNnPc4d1	mocné
třetihorní	třetihorní	k2eAgNnPc4d1	třetihorní
a	a	k8xC	a
čtvrtohorní	čtvrtohorní	k2eAgNnPc4d1	čtvrtohorní
souvrství	souvrství	k1gNnPc4	souvrství
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
meandrují	meandrovat	k5eAaImIp3nP	meandrovat
řeky	řeka	k1gFnPc1	řeka
svými	svůj	k3xOyFgInPc7	svůj
poklidnými	poklidný	k2eAgInPc7d1	poklidný
toky	tok	k1gInPc7	tok
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
menší	malý	k2eAgInSc1d2	menší
rozměr	rozměr	k1gInSc1	rozměr
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nížiny	nížina	k1gFnPc1	nížina
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Kuskokwim	Kuskokwim	k1gInSc1	Kuskokwim
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zdejších	zdejší	k2eAgInPc2d1	zdejší
toků	tok	k1gInPc2	tok
nevlévá	vlévat	k5eNaImIp3nS	vlévat
do	do	k7c2	do
Yukonu	Yukon	k1gInSc2	Yukon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teče	téct	k5eAaImIp3nS	téct
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
paralelně	paralelně	k6eAd1	paralelně
po	po	k7c6	po
zlomové	zlomový	k2eAgFnSc6d1	zlomová
linii	linie	k1gFnSc6	linie
až	až	k9	až
do	do	k7c2	do
Kuskokwimské	Kuskokwimský	k2eAgFnSc2d1	Kuskokwimský
zátoky	zátoka	k1gFnSc2	zátoka
v	v	k7c6	v
Beringově	Beringův	k2eAgNnSc6d1	Beringovo
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
rozlehlých	rozlehlý	k2eAgNnPc2d1	rozlehlé
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
Brooks	Brooksa	k1gFnPc2	Brooksa
Range	Rang	k1gInSc2	Rang
a	a	k8xC	a
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jsou	být	k5eAaImIp3nP	být
posledními	poslední	k2eAgInPc7d1	poslední
články	článek	k1gInPc7	článek
řetězu	řetěz	k1gInSc2	řetěz
ve	v	k7c6	v
vrásném	vrásný	k2eAgInSc6d1	vrásný
systému	systém	k1gInSc6	systém
Skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
hor.	hor.	k?	hor.
Brooks	Brooksa	k1gFnPc2	Brooksa
Range	Range	k1gFnSc1	Range
je	být	k5eAaImIp3nS	být
souborem	soubor	k1gInSc7	soubor
mnoha	mnoho	k4c2	mnoho
horských	horský	k2eAgInPc2d1	horský
hřbetů	hřbet	k1gInPc2	hřbet
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
místními	místní	k2eAgInPc7d1	místní
názvy	název	k1gInPc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnPc1d1	maximální
výšky	výška	k1gFnPc1	výška
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
2700	[number]	k4	2700
m	m	kA	m
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
aljašské	aljašský	k2eAgInPc4d1	aljašský
poměry	poměr	k1gInPc4	poměr
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hory	hora	k1gFnPc1	hora
dost	dost	k6eAd1	dost
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
mají	mít	k5eAaImIp3nP	mít
primát	primát	k1gInSc4	primát
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
pohoří	pohoří	k1gNnSc2	pohoří
za	za	k7c7	za
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
tvary	tvar	k1gInPc1	tvar
reliéfu	reliéf	k1gInSc2	reliéf
vznikaly	vznikat	k5eAaImAgInP	vznikat
při	při	k7c6	při
pleistocénním	pleistocénní	k2eAgNnSc6d1	pleistocénní
zalednění	zalednění	k1gNnSc6	zalednění
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInPc1d1	severní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
bez	bez	k7c2	bez
vegetace	vegetace	k1gFnSc2	vegetace
nebo	nebo	k8xC	nebo
je	on	k3xPp3gFnPc4	on
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
chudá	chudý	k2eAgFnSc1d1	chudá
tundra	tundra	k1gFnSc1	tundra
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
všude	všude	k6eAd1	všude
probíhá	probíhat	k5eAaImIp3nS	probíhat
intenzívní	intenzívní	k2eAgFnSc1d1	intenzívní
kryoplanace	kryoplanace	k1gFnSc1	kryoplanace
<g/>
.	.	kIx.	.
</s>
<s>
Odkryté	odkrytý	k2eAgInPc1d1	odkrytý
skalní	skalní	k2eAgInPc1d1	skalní
výchozy	výchoz	k1gInPc1	výchoz
podléhají	podléhat	k5eAaImIp3nP	podléhat
mrazovému	mrazový	k2eAgNnSc3d1	mrazové
tříštění	tříštění	k1gNnSc3	tříštění
a	a	k8xC	a
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
se	se	k3xPyFc4	se
do	do	k7c2	do
typických	typický	k2eAgInPc2d1	typický
tvarů	tvar	k1gInPc2	tvar
–	–	k?	–
mrazových	mrazový	k2eAgInPc2d1	mrazový
srubů	srub	k1gInPc2	srub
<g/>
,	,	kIx,	,
plošin	plošina	k1gFnPc2	plošina
a	a	k8xC	a
kamenných	kamenný	k2eAgNnPc2d1	kamenné
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mackenzie	Mackenzie	k1gFnPc1	Mackenzie
Mountains	Mountainsa	k1gFnPc2	Mountainsa
tvoří	tvořit	k5eAaImIp3nP	tvořit
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgFnPc1d1	otevřená
vrásy	vrása	k1gFnPc1	vrása
prvohorních	prvohorní	k2eAgInPc2d1	prvohorní
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
druhohorních	druhohorní	k2eAgInPc2d1	druhohorní
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
protknuté	protknutý	k2eAgInPc1d1	protknutý
četnými	četný	k2eAgInPc7d1	četný
zlomy	zlom	k1gInPc7	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
asymetrický	asymetrický	k2eAgInSc4d1	asymetrický
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
rozvodní	rozvodní	k2eAgInPc1d1	rozvodní
hřbety	hřbet	k1gInPc1	hřbet
–	–	k?	–
The	The	k1gMnSc4	The
Great	Great	k1gInSc1	Great
Divide	Divid	k1gInSc5	Divid
se	se	k3xPyFc4	se
štíty	štít	k1gInPc1	štít
Kelle	Kelle	k1gFnSc1	Kelle
Peak	Peak	k1gInSc1	Peak
(	(	kIx(	(
<g/>
2590	[number]	k4	2590
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dome	dům	k1gInSc5	dům
Peak	Peak	k1gMnSc1	Peak
(	(	kIx(	(
<g/>
2743	[number]	k4	2743
m	m	kA	m
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nápadně	nápadně	k6eAd1	nápadně
posunuty	posunout	k5eAaPmNgFnP	posunout
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInPc1d1	východní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
,	,	kIx,	,
mírné	mírný	k2eAgInPc1d1	mírný
<g/>
,	,	kIx,	,
rozryté	rozrytý	k2eAgInPc1d1	rozrytý
paralelními	paralelní	k2eAgInPc7d1	paralelní
trogovými	trogův	k2eAgInPc7d1	trogův
údolími	údolí	k1gNnPc7	údolí
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
protékají	protékat	k5eAaImIp3nP	protékat
četné	četný	k2eAgInPc4d1	četný
levobřežní	levobřežní	k2eAgInPc4d1	levobřežní
přítoky	přítok	k1gInPc4	přítok
řeky	řeka	k1gFnSc2	řeka
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgInPc1d1	krátký
<g/>
,	,	kIx,	,
příkré	příkrý	k2eAgInPc1d1	příkrý
a	a	k8xC	a
vklesávají	vklesávat	k5eAaImIp3nP	vklesávat
se	se	k3xPyFc4	se
do	do	k7c2	do
spleti	spleť	k1gFnSc2	spleť
hřbetů	hřbet	k1gInPc2	hřbet
a	a	k8xC	a
údolí	údolí	k1gNnSc2	údolí
vyzdvižené	vyzdvižený	k2eAgFnSc2d1	vyzdvižená
Yukonské	Yukonský	k2eAgFnSc2d1	Yukonská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Arctic	Arctice	k1gFnPc2	Arctice
Coastal	Coastal	k1gFnSc2	Coastal
Plains	Plainsa	k1gFnPc2	Plainsa
se	se	k3xPyFc4	se
sklánějí	sklánět	k5eAaImIp3nP	sklánět
od	od	k7c2	od
úpatí	úpatí	k1gNnSc2	úpatí
Brooks	Brooks	k1gInSc4	Brooks
Range	Range	k1gNnSc2	Range
zvolna	zvolna	k6eAd1	zvolna
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
vznikaly	vznikat	k5eAaImAgFnP	vznikat
sedimentací	sedimentace	k1gFnSc7	sedimentace
písků	písek	k1gInPc2	písek
<g/>
,	,	kIx,	,
jílů	jíl	k1gInPc2	jíl
a	a	k8xC	a
vápenců	vápenec	k1gInPc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
zdejší	zdejší	k2eAgFnSc1d1	zdejší
řeka	řeka	k1gFnSc1	řeka
Colville	Colville	k1gFnSc2	Colville
protéká	protékat	k5eAaImIp3nS	protékat
po	po	k7c6	po
tektonické	tektonický	k2eAgFnSc6d1	tektonická
poruše	porucha	k1gFnSc6	porucha
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
východní	východní	k2eAgInPc4d1	východní
třetihorní	třetihorní	k2eAgInPc4d1	třetihorní
sedimenty	sediment	k1gInPc4	sediment
od	od	k7c2	od
mocných	mocný	k2eAgFnPc2d1	mocná
vrstev	vrstva	k1gFnPc2	vrstva
křídových	křídový	k2eAgMnPc2d1	křídový
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
arktickém	arktický	k2eAgNnSc6d1	arktické
pobřeží	pobřeží	k1gNnSc6	pobřeží
je	být	k5eAaImIp3nS	být
souvislý	souvislý	k2eAgInSc1d1	souvislý
permafrost	permafrost	k1gFnSc1	permafrost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
nížinách	nížina	k1gFnPc6	nížina
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přímo	přímo	k6eAd1	přímo
ukázkové	ukázkový	k2eAgInPc4d1	ukázkový
kryogenní	kryogenní	k2eAgInPc4d1	kryogenní
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kamenné	kamenný	k2eAgInPc1d1	kamenný
polygony	polygon	k1gInPc1	polygon
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
90	[number]	k4	90
m	m	kA	m
a	a	k8xC	a
pinga	pingo	k1gNnSc2	pingo
(	(	kIx(	(
<g/>
hydrolakolity	hydrolakolit	k1gInPc7	hydrolakolit
<g/>
)	)	kIx)	)
bývají	bývat	k5eAaImIp3nP	bývat
vysoká	vysoká	k1gFnSc1	vysoká
6	[number]	k4	6
až	až	k9	až
60	[number]	k4	60
m	m	kA	m
největších	veliký	k2eAgFnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Buldirská	Buldirský	k2eAgFnSc1d1	Buldirský
kaldera	kaldera	k1gFnSc1	kaldera
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozměry	rozměr	k1gInPc7	rozměr
43	[number]	k4	43
x	x	k?	x
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
000	[number]	k4	000
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tečou	téct	k5eAaImIp3nP	téct
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
na	na	k7c6	na
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
Beaufortova	Beaufortův	k2eAgNnSc2d1	Beaufortovo
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
teče	téct	k5eAaImIp3nS	téct
po	po	k7c6	po
tektonické	tektonický	k2eAgFnSc6d1	tektonická
poruše	porucha	k1gFnSc6	porucha
řeka	řeka	k1gFnSc1	řeka
Colville	Colville	k1gFnSc1	Colville
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
tok	tok	k1gInSc1	tok
Arktické	arktický	k2eAgFnSc2d1	arktická
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Kotzebue	Kotzebu	k1gInSc2	Kotzebu
Sound	Sounda	k1gFnPc2	Sounda
na	na	k7c6	na
západě	západ	k1gInSc6	západ
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Noatak	Noatak	k1gMnSc1	Noatak
<g/>
,	,	kIx,	,
Kobuk	Kobuk	k1gMnSc1	Kobuk
a	a	k8xC	a
Selawik	Selawik	k1gMnSc1	Selawik
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
Beringova	Beringův	k2eAgNnSc2d1	Beringovo
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
Aljašky	Aljaška	k1gFnSc2	Aljaška
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
Kuskokwim	Kuskokwim	k1gMnSc1	Kuskokwim
(	(	kIx(	(
<g/>
1165	[number]	k4	1165
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k1gNnSc1	ostatní
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc2d2	menší
řeky	řeka	k1gFnSc2	řeka
Sustina	Sustina	k1gFnSc1	Sustina
<g/>
,	,	kIx,	,
Matanuska	Matanuska	k1gFnSc1	Matanuska
<g/>
,	,	kIx,	,
Chitina	Chitina	k1gFnSc1	Chitina
<g/>
,	,	kIx,	,
Cooper	Cooper	k1gInSc1	Cooper
pramenící	pramenící	k2eAgInSc1d1	pramenící
v	v	k7c4	v
Alaska	Alasek	k1gMnSc4	Alasek
Range	Rang	k1gMnSc4	Rang
a	a	k8xC	a
Wrangell	Wrangell	k1gMnSc1	Wrangell
Mts	Mts	k1gMnSc1	Mts
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Aljašského	aljašský	k2eAgInSc2d1	aljašský
zálivu	záliv	k1gInSc2	záliv
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
několik	několik	k4yIc4	několik
řek	řeka	k1gFnPc2	řeka
pramenících	pramenící	k2eAgFnPc2d1	pramenící
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
tekoucích	tekoucí	k2eAgInPc2d1	tekoucí
aljašským	aljašský	k2eAgNnSc7d1	aljašské
územím	území	k1gNnSc7	území
na	na	k7c4	na
jih	jih	k1gInSc4	jih
či	či	k8xC	či
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
řeky	řeka	k1gFnPc1	řeka
Alsek	Alsek	k1gMnSc1	Alsek
<g/>
,	,	kIx,	,
Stikine	Stikin	k1gInSc5	Stikin
a	a	k8xC	a
Taku	Takus	k1gInSc6	Takus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
jezer	jezero	k1gNnPc2	jezero
větších	veliký	k2eAgInPc6d2	veliký
než	než	k8xS	než
8	[number]	k4	8
ha	ha	kA	ha
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozptýlena	rozptýlen	k2eAgNnPc1d1	rozptýleno
v	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
i	i	k9	i
mezi	mezi	k7c7	mezi
lesy	les	k1gInPc7	les
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
glaciálního	glaciální	k2eAgInSc2d1	glaciální
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
nepropustném	propustný	k2eNgInSc6d1	nepropustný
permafrostu	permafrost	k1gInSc6	permafrost
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Aljašky	Aljaška	k1gFnSc2	Aljaška
tvoří	tvořit	k5eAaImIp3nP	tvořit
nesčetná	sčetný	k2eNgNnPc1d1	nesčetné
jezera	jezero	k1gNnPc1	jezero
a	a	k8xC	a
bažiny	bažina	k1gFnPc1	bažina
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgNnPc7	jenž
meandrují	meandrovat	k5eAaImIp3nP	meandrovat
potoky	potok	k1gInPc1	potok
a	a	k8xC	a
říčky	říčka	k1gFnPc1	říčka
<g/>
.	.	kIx.	.
</s>
<s>
Stojaté	stojatý	k2eAgFnPc1d1	stojatá
povrchové	povrchový	k2eAgFnPc1d1	povrchová
vody	voda	k1gFnPc1	voda
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
asi	asi	k9	asi
pětinu	pětina	k1gFnSc4	pětina
arktického	arktický	k2eAgNnSc2d1	arktické
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
6,5	[number]	k4	6,5
tisíce	tisíc	k4xCgInPc4	tisíc
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
<g/>
,	,	kIx,	,
eliptický	eliptický	k2eAgInSc4d1	eliptický
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
užším	úzký	k2eAgInSc7d2	užší
koncem	konec	k1gInSc7	konec
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bývají	bývat	k5eAaImIp3nP	bývat
až	až	k9	až
15	[number]	k4	15
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
4	[number]	k4	4
km	km	kA	km
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Malých	Malých	k2eAgInSc1d1	Malých
je	být	k5eAaImIp3nS	být
bezpočet	bezpočet	k1gInSc1	bezpočet
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
Illiamna	Illiamen	k2eAgFnSc1d1	Illiamen
Lake	Lake	k1gFnSc1	Lake
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Aljašského	aljašský	k2eAgInSc2d1	aljašský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
130	[number]	k4	130
km	km	kA	km
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
32	[number]	k4	32
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
2647	[number]	k4	2647
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
Aleknagik	Aleknagik	k1gInSc4	Aleknagik
<g/>
,	,	kIx,	,
Becharof	Becharof	k1gInSc1	Becharof
<g/>
,	,	kIx,	,
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
Minchumina	Minchumin	k2eAgFnSc1d1	Minchumin
<g/>
,	,	kIx,	,
Naknek	Naknek	k1gMnSc1	Naknek
<g/>
,	,	kIx,	,
Selawik	Selawik	k1gMnSc1	Selawik
<g/>
,	,	kIx,	,
Skilak	Skilak	k1gMnSc1	Skilak
<g/>
,	,	kIx,	,
Teshekpuk	Teshekpuk	k1gMnSc1	Teshekpuk
a	a	k8xC	a
Tustumena	Tustumen	k2eAgFnSc1d1	Tustumen
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
čára	čára	k1gFnSc1	čára
měřená	měřený	k2eAgFnSc1d1	měřená
hrubě	hrubě	k6eAd1	hrubě
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
690	[number]	k4	690
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
započteme	započíst	k5eAaPmIp1nP	započíst
všechny	všechen	k3xTgFnPc4	všechen
zátoky	zátoka	k1gFnPc4	zátoka
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
poloostrovy	poloostrov	k1gInPc4	poloostrov
<g/>
,	,	kIx,	,
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
délka	délka	k1gFnSc1	délka
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
čáry	čára	k1gFnSc2	čára
hodnoty	hodnota	k1gFnSc2	hodnota
54	[number]	k4	54
563	[number]	k4	563
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
členité	členitý	k2eAgNnSc1d1	členité
je	být	k5eAaImIp3nS	být
tichomořské	tichomořský	k2eAgNnSc1d1	Tichomořské
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
příliv	příliv	k1gInSc1	příliv
tu	tu	k6eAd1	tu
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
okolo	okolo	k7c2	okolo
6	[number]	k4	6
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
Cookově	Cookův	k2eAgInSc6d1	Cookův
zálivu	záliv	k1gInSc6	záliv
až	až	k9	až
9	[number]	k4	9
m.	m.	k?	m.
Právě	právě	k9	právě
pacifické	pacifický	k2eAgNnSc4d1	pacifické
pobřeží	pobřeží	k1gNnSc4	pobřeží
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
bičováno	bičovat	k5eAaImNgNnS	bičovat
bouřemi	bouř	k1gFnPc7	bouř
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k8xC	i
vlnami	vlna	k1gFnPc7	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
,	,	kIx,	,
vznikajícími	vznikající	k2eAgMnPc7d1	vznikající
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
podmořské	podmořský	k2eAgFnSc2d1	podmořská
zemětřesné	zemětřesný	k2eAgFnSc2d1	zemětřesná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
mořský	mořský	k2eAgInSc1d1	mořský
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
Aljašském	aljašský	k2eAgInSc6d1	aljašský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nS	přenášet
masy	masa	k1gFnPc4	masa
teplé	teplý	k2eAgFnSc2d1	teplá
vody	voda	k1gFnSc2	voda
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
tzv.	tzv.	kA	tzv.
Aljašský	aljašský	k2eAgInSc1d1	aljašský
proud	proud	k1gInSc1	proud
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
podél	podél	k7c2	podél
Aleutského	aleutský	k2eAgNnSc2d1	aleutský
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
stáčí	stáčet	k5eAaImIp3nS	stáčet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
západnímu	západní	k2eAgNnSc3d1	západní
pobřeží	pobřeží	k1gNnSc3	pobřeží
přes	přes	k7c4	přes
Beringův	Beringův	k2eAgInSc4d1	Beringův
průliv	průliv	k1gInSc4	průliv
až	až	k9	až
k	k	k7c3	k
mysu	mys	k1gInSc3	mys
Barrow	Barrow	k1gFnSc2	Barrow
přináší	přinášet	k5eAaImIp3nS	přinášet
teplou	teplý	k2eAgFnSc4d1	teplá
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Příbřežní	příbřežní	k2eAgFnPc1d1	příbřežní
vody	voda	k1gFnPc1	voda
Beringova	Beringův	k2eAgNnSc2d1	Beringovo
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
poloostrova	poloostrov	k1gInSc2	poloostrov
Seward	Sewarda	k1gFnPc2	Sewarda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
mělké	mělký	k2eAgInPc1d1	mělký
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
písčinami	písčina	k1gFnPc7	písčina
a	a	k8xC	a
lagunami	laguna	k1gFnPc7	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Beringova	Beringův	k2eAgNnSc2d1	Beringovo
a	a	k8xC	a
Čukotského	čukotský	k2eAgNnSc2d1	Čukotské
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Beringovo	Beringův	k2eAgNnSc1d1	Beringovo
moře	moře	k1gNnSc1	moře
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
a	a	k8xC	a
okrajové	okrajový	k2eAgFnSc6d1	okrajová
oblasti	oblast	k1gFnSc6	oblast
ledu	led	k1gInSc2	led
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
světových	světový	k2eAgInPc2d1	světový
rybářských	rybářský	k2eAgInPc2d1	rybářský
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
led	led	k1gInSc4	led
postupně	postupně	k6eAd1	postupně
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
taje	tát	k5eAaImIp3nS	tát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
příbřežní	příbřežní	k2eAgFnSc4d1	příbřežní
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
ledovém	ledový	k2eAgInSc6d1	ledový
oceánu	oceán	k1gInSc6	oceán
během	během	k7c2	během
pozdního	pozdní	k2eAgNnSc2d1	pozdní
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgFnPc2	ten
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
asi	asi	k9	asi
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
Juneau	Juneaus	k1gInSc2	Juneaus
<g/>
.	.	kIx.	.
</s>
<s>
Plošně	plošně	k6eAd1	plošně
největším	veliký	k2eAgInPc3d3	veliký
aljašským	aljašský	k2eAgInPc3d1	aljašský
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
severoamerickým	severoamerický	k2eAgInSc7d1	severoamerický
ledovcem	ledovec	k1gInSc7	ledovec
je	být	k5eAaImIp3nS	být
Malaspina	Malaspina	k1gFnSc1	Malaspina
Glacier	Glacier	k1gInSc1	Glacier
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
plochu	plocha	k1gFnSc4	plocha
2200	[number]	k4	2200
km2	km2	k4	km2
na	na	k7c6	na
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
plošinách	plošina	k1gFnPc6	plošina
mezi	mezi	k7c7	mezi
zátokami	zátoka	k1gFnPc7	zátoka
Jakutat	Jakutat	k1gMnSc1	Jakutat
Bay	Bay	k1gMnSc1	Bay
a	a	k8xC	a
Ice	Ice	k1gMnSc1	Ice
Bay	Bay	k1gMnSc1	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
získaných	získaný	k2eAgFnPc2d1	získaná
seismickými	seismický	k2eAgFnPc7d1	seismická
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
ledovce	ledovec	k1gInSc2	ledovec
asi	asi	k9	asi
600	[number]	k4	600
m	m	kA	m
a	a	k8xC	a
lože	lože	k1gNnSc1	lože
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
spočívá	spočívat	k5eAaImIp3nS	spočívat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dno	dno	k1gNnSc1	dno
240	[number]	k4	240
–	–	k?	–
300	[number]	k4	300
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Ledovcový	ledovcový	k2eAgInSc1d1	ledovcový
splaz	splaz	k1gInSc1	splaz
Hubbard	Hubbard	k1gInSc1	Hubbard
Glacier	Glacier	k1gInSc1	Glacier
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
144	[number]	k4	144
km	km	kA	km
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
je	být	k5eAaImIp3nS	být
až	až	k9	až
16	[number]	k4	16
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
ledovcům	ledovec	k1gInPc3	ledovec
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
ledovce	ledovec	k1gInSc2	ledovec
Bering	Bering	k1gMnSc1	Bering
(	(	kIx(	(
<g/>
plošně	plošně	k6eAd1	plošně
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
ledovcový	ledovcový	k2eAgInSc1d1	ledovcový
systém	systém	k1gInSc1	systém
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
Rapids	Rapids	k1gInSc1	Rapids
<g/>
,	,	kIx,	,
Canwell	Canwell	k1gInSc1	Canwell
<g/>
,	,	kIx,	,
Castner	Castner	k1gInSc1	Castner
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
Gulkana	Gulkana	k1gFnSc1	Gulkana
<g/>
,	,	kIx,	,
Matanuska	Matanuska	k1gFnSc1	Matanuska
<g/>
,	,	kIx,	,
Mendenhall	Mendenhall	k1gInSc1	Mendenhall
<g/>
,	,	kIx,	,
Portage	Portage	k1gInSc1	Portage
či	či	k8xC	či
Worthington	Worthington	k1gInSc1	Worthington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Aljašky	Aljaška	k1gFnSc2	Aljaška
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
polární	polární	k2eAgFnPc4d1	polární
a	a	k8xC	a
subpolární	subpolární	k2eAgFnPc4d1	subpolární
oblasti	oblast	k1gFnPc4	oblast
chladného	chladný	k2eAgNnSc2d1	chladné
podnebného	podnebné	k1gNnSc2	podnebné
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
vlhká	vlhký	k2eAgFnSc1d1	vlhká
přímořská	přímořský	k2eAgFnSc1d1	přímořská
oblast	oblast	k1gFnSc1	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
vysokohorská	vysokohorský	k2eAgFnSc1d1	vysokohorská
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
polární	polární	k2eAgFnPc4d1	polární
pustiny	pustina	k1gFnPc4	pustina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
oblasti	oblast	k1gFnSc6	oblast
Aljašky	Aljaška	k1gFnSc2	Aljaška
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
Brooks	Brooksa	k1gFnPc2	Brooksa
Range	Rang	k1gFnSc2	Rang
za	za	k7c7	za
severním	severní	k2eAgInSc7d1	severní
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
krátká	krátký	k2eAgNnPc1d1	krátké
studená	studený	k2eAgNnPc1d1	studené
léta	léto	k1gNnPc1	léto
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
zde	zde	k6eAd1	zde
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nepřesáhne	přesáhnout	k5eNaPmIp3nS	přesáhnout
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
trvá	trvat	k5eAaImIp3nS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
okolo	okolo	k7c2	okolo
200	[number]	k4	200
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
hojnější	hojný	k2eAgFnPc1d2	hojnější
a	a	k8xC	a
přicházejí	přicházet	k5eAaImIp3nP	přicházet
především	především	k9	především
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Subpolární	subpolární	k2eAgFnSc4d1	subpolární
oblast	oblast	k1gFnSc4	oblast
představují	představovat	k5eAaImIp3nP	představovat
tundry	tundra	k1gFnPc1	tundra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
půda	půda	k1gFnSc1	půda
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
několika	několik	k4yIc2	několik
decimetrů	decimetr	k1gInPc2	decimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
působení	působení	k1gNnSc6	působení
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vnitrozemské	vnitrozemský	k2eAgFnSc3d1	vnitrozemská
oblasti	oblast	k1gFnSc3	oblast
–	–	k?	–
okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
Fairbanks	Fairbanksa	k1gFnPc2	Fairbanksa
<g/>
,	,	kIx,	,
nížiny	nížina	k1gFnPc1	nížina
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
Kuskokwim	Kuskokwima	k1gFnPc2	Kuskokwima
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
střední	střední	k2eAgFnSc1d1	střední
teplota	teplota	k1gFnSc1	teplota
nepřesáhne	přesáhnout	k5eNaPmIp3nS	přesáhnout
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
začátkem	začátkem	k7c2	začátkem
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
nejchladnější	chladný	k2eAgNnSc1d3	nejchladnější
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teploty	teplota	k1gFnPc1	teplota
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
kolem	kolem	k7c2	kolem
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
příznačná	příznačný	k2eAgFnSc1d1	příznačná
delší	dlouhý	k2eAgFnSc1d2	delší
mrazivá	mrazivý	k2eAgFnSc1d1	mrazivá
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
jižnějších	jižní	k2eAgFnPc6d2	jižnější
částech	část	k1gFnPc6	část
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
průměrných	průměrný	k2eAgFnPc2d1	průměrná
hodnot	hodnota	k1gFnPc2	hodnota
až	až	k9	až
kolem	kolem	k7c2	kolem
17	[number]	k4	17
°	°	k?	°
<g/>
C.	C.	kA	C.
Ovšem	ovšem	k9	ovšem
roční	roční	k2eAgFnSc1d1	roční
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pod	pod	k7c7	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vnitrozemské	vnitrozemský	k2eAgFnSc6d1	vnitrozemská
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
srážek	srážka	k1gFnPc2	srážka
o	o	k7c6	o
něco	něco	k6eAd1	něco
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
nejsevernějších	severní	k2eAgFnPc6d3	nejsevernější
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
průměrně	průměrně	k6eAd1	průměrně
300	[number]	k4	300
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
minimálním	minimální	k2eAgInSc6d1	minimální
výparu	výpar	k1gInSc6	výpar
a	a	k8xC	a
nepropustném	propustný	k2eNgNnSc6d1	nepropustné
zmrzlém	zmrzlý	k2eAgNnSc6d1	zmrzlé
podloží	podloží	k1gNnSc6	podloží
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
vláhy	vláha	k1gFnSc2	vláha
dostatek	dostatek	k1gInSc1	dostatek
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východu	východ	k1gInSc3	východ
množství	množství	k1gNnSc2	množství
srážek	srážka	k1gFnPc2	srážka
stoupá	stoupat	k5eAaImIp3nS	stoupat
až	až	k9	až
k	k	k7c3	k
průměru	průměr	k1gInSc3	průměr
500	[number]	k4	500
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přímořské	přímořský	k2eAgFnSc2d1	přímořská
oblasti	oblast	k1gFnSc2	oblast
mírného	mírný	k2eAgInSc2d1	mírný
podnebného	podnebný	k2eAgInSc2d1	podnebný
pásu	pás	k1gInSc2	pás
náleží	náležet	k5eAaImIp3nS	náležet
jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Alexandrovo	Alexandrův	k2eAgNnSc1d1	Alexandrovo
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňován	k2eAgMnPc4d1	ovlivňován
teplým	teplý	k2eAgInSc7d1	teplý
Severopacifickým	Severopacifický	k2eAgInSc7d1	Severopacifický
mořským	mořský	k2eAgInSc7d1	mořský
proudem	proud	k1gInSc7	proud
(	(	kIx(	(
<g/>
Kuro-šio	Kuro-šio	k1gNnSc1	Kuro-šio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
od	od	k7c2	od
Japonska	Japonsko	k1gNnSc2	Japonsko
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
zde	zde	k6eAd1	zde
nezamrzá	zamrzat	k5eNaImIp3nS	zamrzat
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
2-3	[number]	k4	2-3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c6	na
Alexandrových	Alexandrův	k2eAgInPc6d1	Alexandrův
ostrovech	ostrov	k1gInPc6	ostrov
ještě	ještě	k6eAd1	ještě
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
poměrně	poměrně	k6eAd1	poměrně
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
léta	léto	k1gNnPc4	léto
naopak	naopak	k6eAd1	naopak
chladnější	chladný	k2eAgMnSc1d2	chladnější
než	než	k8xS	než
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
teploty	teplota	k1gFnPc1	teplota
nejchladnějších	chladný	k2eAgInPc2d3	nejchladnější
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jen	jen	k9	jen
-10	-10	k4	-10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
nejjižnějších	jižní	k2eAgNnPc6d3	nejjižnější
místech	místo	k1gNnPc6	místo
teploty	teplota	k1gFnSc2	teplota
dokonce	dokonce	k9	dokonce
nikdy	nikdy	k6eAd1	nikdy
neklesnou	klesnout	k5eNaPmIp3nP	klesnout
pod	pod	k7c7	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ostrov	ostrov	k1gInSc1	ostrov
Kodiak	Kodiak	k1gInSc1	Kodiak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
maximální	maximální	k2eAgFnPc1d1	maximální
teploty	teplota	k1gFnPc1	teplota
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
14	[number]	k4	14
°	°	k?	°
<g/>
C.	C.	kA	C.
Přímořská	přímořský	k2eAgFnSc1d1	přímořská
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
územím	území	k1gNnSc7	území
s	s	k7c7	s
největšími	veliký	k2eAgFnPc7d3	veliký
srážkami	srážka	k1gFnPc7	srážka
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Naprší	napršet	k5eAaPmIp3nS	napršet
zde	zde	k6eAd1	zde
ročně	ročně	k6eAd1	ročně
až	až	k9	až
2000	[number]	k4	2000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
na	na	k7c6	na
Alexandrových	Alexandrův	k2eAgInPc6d1	Alexandrův
ostrovech	ostrov	k1gInPc6	ostrov
až	až	k9	až
4000	[number]	k4	4000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nad	nad	k7c4	nad
Aleuty	Aleuty	k1gFnPc4	Aleuty
a	a	k8xC	a
Aljašku	Aljaška	k1gFnSc4	Aljaška
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
Aleutská	aleutský	k2eAgFnSc1d1	Aleutská
tlaková	tlakový	k2eAgFnSc1d1	tlaková
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
proudí	proudit	k5eAaPmIp3nP	proudit
bouřlivé	bouřlivý	k2eAgInPc4d1	bouřlivý
větry	vítr	k1gInPc4	vítr
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
níže	níže	k1gFnSc1	níže
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
západně	západně	k6eAd1	západně
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
asijský	asijský	k2eAgInSc4d1	asijský
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
ve	v	k7c4	v
Snag	Snag	k1gInSc4	Snag
u	u	k7c2	u
Alaska	Alask	k1gInSc2	Alask
Highway	Highwaa	k1gFnSc2	Highwaa
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
Yukonu	Yukon	k1gInSc6	Yukon
oficiálně	oficiálně	k6eAd1	oficiálně
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
teplotní	teplotní	k2eAgInSc1d1	teplotní
rekord	rekord	k1gInSc1	rekord
-63	-63	k4	-63
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
severoamerického	severoamerický	k2eAgInSc2d1	severoamerický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
sopek	sopka	k1gFnPc2	sopka
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Aljašské	aljašský	k2eAgNnSc1d1	aljašské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
zachovávání	zachovávání	k1gNnSc4	zachovávání
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
využívání	využívání	k1gNnSc4	využívání
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
znečištění	znečištění	k1gNnSc2	znečištění
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
zavážky	zavážka	k1gFnPc1	zavážka
<g/>
,	,	kIx,	,
splňující	splňující	k2eAgFnPc1d1	splňující
moderní	moderní	k2eAgFnPc1d1	moderní
ekologické	ekologický	k2eAgFnPc1d1	ekologická
normy	norma	k1gFnPc1	norma
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
odpadů	odpad	k1gInPc2	odpad
likvidováno	likvidován	k2eAgNnSc4d1	likvidováno
ve	v	k7c6	v
starých	starý	k2eAgNnPc6d1	staré
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
skládky	skládka	k1gFnPc1	skládka
jsou	být	k5eAaImIp3nP	být
primárními	primární	k2eAgNnPc7d1	primární
zařízeními	zařízení	k1gNnPc7	zařízení
na	na	k7c6	na
likvidaci	likvidace	k1gFnSc6	likvidace
odpadů	odpad	k1gInPc2	odpad
na	na	k7c6	na
aljašském	aljašský	k2eAgInSc6d1	aljašský
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
měl	mít	k5eAaImAgMnS	mít
stát	stát	k5eAaPmF	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
7	[number]	k4	7
nebezpečných	bezpečný	k2eNgNnPc2d1	nebezpečné
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
likvidaci	likvidace	k1gFnSc4	likvidace
odpadu	odpad	k1gInSc2	odpad
na	na	k7c6	na
federálním	federální	k2eAgInSc6d1	federální
seznamu	seznam	k1gInSc6	seznam
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
s	s	k7c7	s
nutností	nutnost	k1gFnSc7	nutnost
přednostního	přednostní	k2eAgNnSc2d1	přednostní
vyčištění	vyčištění	k1gNnSc2	vyčištění
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
vážný	vážný	k2eAgInSc4d1	vážný
stav	stav	k1gInSc4	stav
nebo	nebo	k8xC	nebo
bezprostřední	bezprostřední	k2eAgFnSc4d1	bezprostřední
blízkost	blízkost	k1gFnSc4	blízkost
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
úsilí	úsilí	k1gNnSc6	úsilí
redukovat	redukovat	k5eAaBmF	redukovat
znečištění	znečištění	k1gNnSc1	znečištění
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
let	léto	k1gNnPc2	léto
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
množství	množství	k1gNnSc1	množství
toxických	toxický	k2eAgFnPc2d1	toxická
látek	látka	k1gFnPc2	látka
vypouštěných	vypouštěný	k2eAgFnPc2d1	vypouštěná
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
sníženo	snížen	k2eAgNnSc1d1	sníženo
o	o	k7c4	o
72	[number]	k4	72
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
kvalita	kvalita	k1gFnSc1	kvalita
vzduchu	vzduch	k1gInSc2	vzduch
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
obecně	obecně	k6eAd1	obecně
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
několika	několik	k4yIc7	několik
problémy	problém	k1gInPc7	problém
znečištění	znečištění	k1gNnSc3	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
v	v	k7c6	v
zastavěných	zastavěný	k2eAgFnPc6d1	zastavěná
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
během	během	k7c2	během
teplotních	teplotní	k2eAgFnPc2d1	teplotní
inverzí	inverze	k1gFnPc2	inverze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
polutanty	polutant	k1gInPc1	polutant
nízko	nízko	k6eAd1	nízko
při	při	k7c6	při
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
překračuje	překračovat	k5eAaImIp3nS	překračovat
občas	občas	k6eAd1	občas
znečištění	znečištění	k1gNnSc1	znečištění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
Anchorage	Anchorage	k1gNnSc6	Anchorage
a	a	k8xC	a
Fairbanks	Fairbanks	k1gInSc1	Fairbanks
státní	státní	k2eAgFnSc2d1	státní
limity	limita	k1gFnSc2	limita
<g/>
.	.	kIx.	.
</s>
<s>
Automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
tepelné	tepelný	k2eAgFnSc2d1	tepelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
spalující	spalující	k2eAgNnSc4d1	spalující
uhlí	uhlí	k1gNnSc4	uhlí
a	a	k8xC	a
domácí	domácí	k2eAgNnSc4d1	domácí
vytápění	vytápění	k1gNnSc4	vytápění
přispívají	přispívat	k5eAaImIp3nP	přispívat
ve	v	k7c4	v
Fairbanks	Fairbanks	k1gInSc4	Fairbanks
ke	k	k7c3	k
znečištění	znečištění	k1gNnSc3	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Toxické	toxický	k2eAgFnPc1d1	toxická
emise	emise	k1gFnPc1	emise
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
amoniak	amoniak	k1gInSc4	amoniak
a	a	k8xC	a
benzen	benzen	k1gInSc4	benzen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
oblasti	oblast	k1gFnPc4	oblast
okolo	okolo	k7c2	okolo
ropných	ropný	k2eAgFnPc2d1	ropná
rafinerii	rafinerie	k1gFnSc3	rafinerie
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc4	žádný
vody	voda	k1gFnPc4	voda
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
nejsou	být	k5eNaImIp3nP	být
znečištěny	znečištěn	k2eAgFnPc1d1	znečištěna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k6eAd1	mnoho
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
porušují	porušovat	k5eAaImIp3nP	porušovat
federální	federální	k2eAgInPc1d1	federální
standardy	standard	k1gInPc1	standard
pro	pro	k7c4	pro
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
vody	voda	k1gFnSc2	voda
trpí	trpět	k5eAaImIp3nS	trpět
také	také	k9	také
ropnými	ropný	k2eAgFnPc7d1	ropná
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
likvidací	likvidace	k1gFnSc7	likvidace
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
supertankeru	supertanker	k1gInSc2	supertanker
Exxon	Exxon	k1gInSc1	Exxon
Valdez	Valdez	k1gInSc1	Valdez
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Prince	princ	k1gMnSc2	princ
William	William	k1gInSc4	William
<g/>
.	.	kIx.	.
</s>
<s>
Vyteklo	vytéct	k5eAaPmAgNnS	vytéct
asi	asi	k9	asi
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
litrů	litr	k1gInPc2	litr
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
ropnou	ropný	k2eAgFnSc7d1	ropná
skvrnou	skvrna	k1gFnSc7	skvrna
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
bylo	být	k5eAaImAgNnS	být
znečištěno	znečistit	k5eAaPmNgNnS	znečistit
přes	přes	k7c4	přes
1100	[number]	k4	1100
km	km	kA	km
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
např.	např.	kA	např.
Kenai	Kenai	k1gNnSc4	Kenai
Fjord	fjord	k1gInSc1	fjord
a	a	k8xC	a
Katmai	Katmai	k1gNnSc1	Katmai
National	National	k1gFnSc2	National
Park	park	k1gInSc1	park
a	a	k8xC	a
Alaska	Alaska	k1gFnSc1	Alaska
Maritime	Maritim	k1gMnSc5	Maritim
National	National	k1gMnSc5	National
Wildlife	Wildlif	k1gMnSc5	Wildlif
Refuge	Refugus	k1gMnSc5	Refugus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
34	[number]	k4	34
434	[number]	k4	434
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
9994	[number]	k4	9994
vyder	vydra	k1gFnPc2	vydra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
710	[number]	k4	710
231	[number]	k4	231
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
koncentrována	koncentrovat	k5eAaBmNgFnS	koncentrovat
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
centrální	centrální	k2eAgFnSc2d1	centrální
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Aljašky	Aljaška	k1gFnSc2	Aljaška
je	být	k5eAaImIp3nS	být
neobydlena	obydlen	k2eNgFnSc1d1	neobydlena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
žilo	žít	k5eAaImAgNnS	žít
68	[number]	k4	68
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
klasifikovaných	klasifikovaný	k2eAgFnPc6d1	klasifikovaná
jako	jako	k8xS	jako
městské	městský	k2eAgMnPc4d1	městský
a	a	k8xC	a
populace	populace	k1gFnSc1	populace
tu	tu	k6eAd1	tu
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgInSc4d2	vyšší
přírůstek	přírůstek	k1gInSc4	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
osidlování	osidlování	k1gNnSc2	osidlování
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
přitahovaly	přitahovat	k5eAaImAgInP	přitahovat
menší	malý	k2eAgInPc1d2	menší
či	či	k8xC	či
větší	veliký	k2eAgNnPc4d2	veliký
města	město	k1gNnPc4	město
více	hodně	k6eAd2	hodně
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
venkov	venkov	k1gInSc1	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Běloši	běloch	k1gMnPc1	běloch
tvoří	tvořit	k5eAaImIp3nP	tvořit
69	[number]	k4	69
procent	procento	k1gNnPc2	procento
celé	celý	k2eAgFnSc2d1	celá
aljašské	aljašský	k2eAgFnSc2d1	aljašská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
-	-	kIx~	-
Eskymáci	Eskymák	k1gMnPc1	Eskymák
<g/>
,	,	kIx,	,
Aleuti	Aleut	k1gMnPc1	Aleut
a	a	k8xC	a
místní	místní	k2eAgFnPc1d1	místní
populace	populace	k1gFnPc1	populace
Indiánů	Indián	k1gMnPc2	Indián
-	-	kIx~	-
kteří	který	k3yIgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
15,6	[number]	k4	15,6
procent	procento	k1gNnPc2	procento
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Eskymáci	Eskymák	k1gMnPc1	Eskymák
obývají	obývat	k5eAaImIp3nP	obývat
zejména	zejména	k9	zejména
severní	severní	k2eAgFnSc4d1	severní
arktickou	arktický	k2eAgFnSc4d1	arktická
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
tichomořští	tichomořský	k2eAgMnPc1d1	tichomořský
Eskymáci	Eskymák	k1gMnPc1	Eskymák
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
je	být	k5eAaImIp3nS	být
Aleutů	Aleut	k1gMnPc2	Aleut
<g/>
,	,	kIx,	,
obyvatel	obyvatel	k1gMnSc1	obyvatel
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Aljašky	Aljaška	k1gFnSc2	Aljaška
-	-	kIx~	-
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Aljašky	Aljaška	k1gFnSc2	Aljaška
žijí	žít	k5eAaImIp3nP	žít
skupiny	skupina	k1gFnPc1	skupina
tzv.	tzv.	kA	tzv.
lesních	lesní	k2eAgMnPc2d1	lesní
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejpočetnější	početní	k2eAgInSc1d3	nejpočetnější
je	být	k5eAaImIp3nS	být
kmen	kmen	k1gInSc1	kmen
Tlingitů	Tlingita	k1gMnPc2	Tlingita
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
žije	žít	k5eAaImIp3nS	žít
další	další	k2eAgInSc1d1	další
silný	silný	k2eAgInSc1d1	silný
indiánský	indiánský	k2eAgInSc1d1	indiánský
kmen	kmen	k1gInSc1	kmen
Athabaskové	Athabaskové	k2eAgInSc1d1	Athabaskové
<g/>
.	.	kIx.	.
</s>
<s>
Početně	početně	k6eAd1	početně
slabší	slabý	k2eAgMnPc1d2	slabší
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
přišly	přijít	k5eAaPmAgFnP	přijít
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
-	-	kIx~	-
Haidové	Haidové	k2eAgInSc4d1	Haidové
a	a	k8xC	a
Tsimshianové	Tsimshianové	k2eAgInSc4d1	Tsimshianové
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgInSc4d1	obývající
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
žijí	žít	k5eAaImIp3nP	žít
Asiaté	Asiat	k1gMnPc1	Asiat
4	[number]	k4	4
%	%	kIx~	%
<g/>
,	,	kIx,	,
černoší	černoší	k2eAgFnSc1d1	černoší
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
domorodí	domorodý	k2eAgMnPc1d1	domorodý
Havajci	Havajec	k1gMnPc1	Havajec
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
tichomořské	tichomořský	k2eAgInPc1d1	tichomořský
národy	národ	k1gInPc1	národ
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
7	[number]	k4	7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
jsou	být	k5eAaImIp3nP	být
míšenci	míšenec	k1gMnPc7	míšenec
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
aljašská	aljašský	k2eAgFnSc1d1	aljašská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
mladší	mladý	k2eAgFnSc1d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
evropskými	evropský	k2eAgMnPc7d1	evropský
osadníky	osadník	k1gMnPc7	osadník
Aljašky	Aljaška	k1gFnSc2	Aljaška
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
ruští	ruský	k2eAgMnPc1d1	ruský
obchodníci	obchodník	k1gMnPc1	obchodník
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kodiak	Kodiak	k1gInSc1	Kodiak
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
i	i	k8xC	i
zde	zde	k6eAd1	zde
příchod	příchod	k1gInSc1	příchod
bělochů	běloch	k1gMnPc2	běloch
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vymírání	vymírání	k1gNnSc3	vymírání
populace	populace	k1gFnSc2	populace
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
následkem	následkem	k7c2	následkem
zavlečených	zavlečený	k2eAgFnPc2d1	zavlečená
infekčních	infekční	k2eAgFnPc2d1	infekční
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
domorodci	domorodec	k1gMnPc7	domorodec
a	a	k8xC	a
přistěhovalci	přistěhovalec	k1gMnPc7	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
ruských	ruský	k2eAgMnPc2d1	ruský
lovců	lovec	k1gMnPc2	lovec
kožešin	kožešina	k1gFnPc2	kožešina
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
domorodých	domorodý	k2eAgMnPc2d1	domorodý
Aleuťanů	Aleuťan	k1gMnPc2	Aleuťan
<g/>
,	,	kIx,	,
obývajících	obývající	k2eAgMnPc2d1	obývající
Aleutské	aleutský	k2eAgNnSc1d1	aleutský
souostroví	souostroví	k1gNnSc3	souostroví
<g/>
,	,	kIx,	,
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
20	[number]	k4	20
000	[number]	k4	000
na	na	k7c4	na
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
populace	populace	k1gFnSc1	populace
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
nebezpečně	bezpečně	k6eNd1	bezpečně
snížila	snížit	k5eAaPmAgFnS	snížit
během	během	k7c2	během
epidemií	epidemie	k1gFnPc2	epidemie
tyfu	tyf	k1gInSc2	tyf
a	a	k8xC	a
spalniček	spalničky	k1gFnPc2	spalničky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
první	první	k4xOgNnSc4	první
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
32	[number]	k4	32
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
430	[number]	k4	430
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
USA	USA	kA	USA
koupily	koupit	k5eAaPmAgInP	koupit
Aljašku	Aljaška	k1gFnSc4	Aljaška
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
počet	počet	k1gInSc1	počet
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
mírně	mírně	k6eAd1	mírně
klesal	klesat	k5eAaImAgInS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objevem	objev	k1gInSc7	objev
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
sem	sem	k6eAd1	sem
přicházely	přicházet	k5eAaImAgFnP	přicházet
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
tisíce	tisíc	k4xCgInPc4	tisíc
lidí	člověk	k1gMnPc2	člověk
za	za	k7c7	za
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tu	tu	k6eAd1	tu
založili	založit	k5eAaPmAgMnP	založit
osady	osada	k1gFnSc2	osada
a	a	k8xC	a
již	již	k6eAd1	již
odtud	odtud	k6eAd1	odtud
neodešli	odejít	k5eNaPmAgMnP	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
prudce	prudko	k6eAd1	prudko
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
stoupal	stoupat	k5eAaImAgInS	stoupat
i	i	k9	i
počet	počet	k1gInSc1	počet
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
již	již	k6eAd1	již
tvořili	tvořit	k5eAaImAgMnP	tvořit
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
63	[number]	k4	63
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
objevena	objeven	k2eAgNnPc1d1	objeveno
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
transaljašského	transaljašský	k2eAgInSc2d1	transaljašský
ropovodu	ropovod	k1gInSc2	ropovod
přibývalo	přibývat	k5eAaImAgNnS	přibývat
i	i	k8xC	i
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
o	o	k7c4	o
555	[number]	k4	555
%	%	kIx~	%
(	(	kIx(	(
<g/>
ze	z	k7c2	z
72	[number]	k4	72
500	[number]	k4	500
na	na	k7c4	na
402	[number]	k4	402
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
66,7	[number]	k4	66,7
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
64,1	[number]	k4	64,1
%	%	kIx~	%
+	+	kIx~	+
běloši	běloch	k1gMnPc1	běloch
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
2,6	[number]	k4	2,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
3,3	[number]	k4	3,3
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
14,8	[number]	k4	14,8
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
a	a	k8xC	a
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
5,4	[number]	k4	5,4
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
1,0	[number]	k4	1,0
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,6	[number]	k4	1,6
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
7,3	[number]	k4	7,3
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
5,5	[number]	k4	5,5
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
83	[number]	k4	83
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
protestanti	protestant	k1gMnPc1	protestant
–	–	k?	–
68	[number]	k4	68
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
–	–	k?	–
11	[number]	k4	11
%	%	kIx~	%
luteráni	luterán	k1gMnPc1	luterán
–	–	k?	–
8	[number]	k4	8
%	%	kIx~	%
metodisté	metodista	k1gMnPc1	metodista
–	–	k?	–
6	[number]	k4	6
%	%	kIx~	%
letniční	letniční	k2eAgFnSc2d1	letniční
církve	církev	k1gFnSc2	církev
–	–	k?	–
2	[number]	k4	2
%	%	kIx~	%
episkopální	episkopální	k2eAgFnSc1d1	episkopální
církev	církev	k1gFnSc1	církev
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
kvakeři	kvaker	k1gMnPc1	kvaker
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
–	–	k?	–
8	[number]	k4	8
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
–	–	k?	–
7	[number]	k4	7
%	%	kIx~	%
mormoni	mormon	k1gMnPc1	mormon
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
–	–	k?	–
16	[number]	k4	16
%	%	kIx~	%
Aljaška	Aljaška	k1gFnSc1	Aljaška
není	být	k5eNaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
counties	counties	k1gInSc1	counties
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
borough	borough	k1gInSc1	borough
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustě	hustě	k6eAd1	hustě
obydlené	obydlený	k2eAgFnPc1d1	obydlená
části	část	k1gFnPc1	část
státu	stát	k1gInSc2	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
tvoří	tvořit	k5eAaImIp3nS	tvořit
devatenáct	devatenáct	k4xCc4	devatenáct
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
okresy	okres	k1gInPc1	okres
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
okresů	okres	k1gInPc2	okres
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
49	[number]	k4	49
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
nepokrývají	pokrývat	k5eNaImIp3nP	pokrývat
celou	celý	k2eAgFnSc4d1	celá
rozlohu	rozloha	k1gFnSc4	rozloha
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neleží	ležet	k5eNaImIp3nP	ležet
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
z	z	k7c2	z
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
velkého	velký	k2eAgInSc2d1	velký
neorganizovaného	organizovaný	k2eNgInSc2d1	neorganizovaný
obvodu	obvod	k1gInSc2	obvod
(	(	kIx(	(
<g/>
Unorganized	Unorganized	k1gMnSc1	Unorganized
Borough	Borough	k1gMnSc1	Borough
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
americký	americký	k2eAgInSc1d1	americký
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
ho	on	k3xPp3gNnSc2	on
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
statistické	statistický	k2eAgFnSc2d1	statistická
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
prezentace	prezentace	k1gFnSc2	prezentace
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
do	do	k7c2	do
11	[number]	k4	11
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
aljašských	aljašský	k2eAgNnPc2d1	aljašské
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
při	při	k7c6	při
březích	břeh	k1gInPc6	břeh
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
zejména	zejména	k9	zejména
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Anchorage	Anchorage	k1gNnSc4	Anchorage
s	s	k7c7	s
278	[number]	k4	278
700	[number]	k4	700
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnSc2	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
Anchorage	Anchorage	k1gFnSc4	Anchorage
Města	město	k1gNnSc2	město
s	s	k7c7	s
10	[number]	k4	10
000-100	[number]	k4	000-100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
College	Colleg	k1gFnSc2	Colleg
Fairbanks	Fairbanks	k1gInSc1	Fairbanks
Juneau	Juneaa	k1gFnSc4	Juneaa
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
Města	město	k1gNnSc2	město
s	s	k7c7	s
1	[number]	k4	1
000-10	[number]	k4	000-10
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
Ketchikan	Ketchikan	k1gMnSc1	Ketchikan
Sitka	Sitka	k1gMnSc1	Sitka
Wasilla	Wasilla	k1gMnSc1	Wasilla
Kenai	Kena	k1gFnSc2	Kena
Kodiak	Kodiak	k1gMnSc1	Kodiak
Palmer	Palmer	k1gMnSc1	Palmer
Bethel	Bethel	k1gMnSc1	Bethel
Barrow	Barrow	k1gMnSc1	Barrow
Unalaska	Unalasek	k1gMnSc2	Unalasek
Valdez	Valdez	k1gMnSc1	Valdez
Soldotna	Soldotn	k1gMnSc2	Soldotn
Homer	Homer	k1gMnSc1	Homer
Nome	Nom	k1gMnSc2	Nom
Petersburg	Petersburg	k1gMnSc1	Petersburg
Wrangell	Wrangell	k1gMnSc1	Wrangell
Kotzebue	Kotzebu	k1gFnSc2	Kotzebu
Seward	Seward	k1gMnSc1	Seward
Dillingham	Dillingham	k1gInSc1	Dillingham
Cordova	Cordův	k2eAgFnSc1d1	Cordova
Haines	Haines	k1gInSc4	Haines
North	North	k1gMnSc1	North
Pole	pole	k1gFnSc2	pole
Hooper	Hooper	k1gMnSc1	Hooper
Bay	Bay	k1gMnSc1	Bay
Craig	Craig	k1gMnSc1	Craig
Houston	Houston	k1gInSc4	Houston
Metlakatla	Metlakatla	k1gMnSc3	Metlakatla
Menší	malý	k2eAgFnSc2d2	menší
vesnice	vesnice	k1gFnSc2	vesnice
Aljaška	Aljaška	k1gFnSc1	Aljaška
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgFnPc2d2	menší
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
aljašské	aljašský	k2eAgFnSc6d1	aljašská
divočině	divočina	k1gFnSc6	divočina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
a	a	k8xC	a
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Aljašky	Aljaška	k1gFnSc2	Aljaška
mezi	mezi	k7c7	mezi
rameny	rameno	k1gNnPc7	rameno
Cookova	Cookův	k2eAgInSc2d1	Cookův
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
nazvanými	nazvaný	k2eAgFnPc7d1	nazvaná
Knik	Kniko	k1gNnPc2	Kniko
a	a	k8xC	a
Turnagain	Turnagaina	k1gFnPc2	Turnagaina
<g/>
,	,	kIx,	,
při	při	k7c6	při
úpatí	úpatí	k1gNnSc6	úpatí
Chugach	Chugacha	k1gFnPc2	Chugacha
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
.	.	kIx.	.
</s>
<s>
Anchorage	Anchorage	k1gFnSc1	Anchorage
je	být	k5eAaImIp3nS	být
suverénně	suverénně	k6eAd1	suverénně
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
-	-	kIx~	-
260	[number]	k4	260
283	[number]	k4	283
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
42	[number]	k4	42
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
dopravní	dopravní	k2eAgInPc4d1	dopravní
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
komunikační	komunikační	k2eAgNnSc4d1	komunikační
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dálnici	dálnice	k1gFnSc3	dálnice
Glenn	Glenna	k1gFnPc2	Glenna
Highway	Highwaa	k1gFnSc2	Highwaa
je	být	k5eAaImIp3nS	být
napojeno	napojen	k2eAgNnSc1d1	napojeno
na	na	k7c4	na
Alaska	Alasek	k1gMnSc4	Alasek
Highway	Highwaa	k1gFnSc2	Highwaa
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc4d1	spojující
Aljašku	Aljaška	k1gFnSc4	Aljaška
s	s	k7c7	s
Britskou	britský	k2eAgFnSc7d1	britská
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anchorage	Anchorage	k1gFnSc6	Anchorage
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
hluboce	hluboko	k6eAd1	hluboko
propojena	propojen	k2eAgFnSc1d1	propojena
s	s	k7c7	s
bohatými	bohatý	k2eAgInPc7d1	bohatý
aljašskými	aljašský	k2eAgInPc7d1	aljašský
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
<g/>
,	,	kIx,	,
zemním	zemní	k2eAgInSc7d1	zemní
plynem	plyn	k1gInSc7	plyn
a	a	k8xC	a
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
blízké	blízký	k2eAgFnPc1d1	blízká
vojenské	vojenský	k2eAgFnPc1d1	vojenská
základny	základna	k1gFnPc1	základna
armády	armáda	k1gFnSc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Fort	Fort	k?	Fort
Richardson	Richardson	k1gInSc1	Richardson
a	a	k8xC	a
Elmendorf	Elmendorf	k1gInSc1	Elmendorf
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
Base	basa	k1gFnSc3	basa
jsou	být	k5eAaImIp3nP	být
významnými	významný	k2eAgMnPc7d1	významný
místními	místní	k2eAgMnPc7d1	místní
zaměstnavateli	zaměstnavatel	k1gMnPc7	zaměstnavatel
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
jako	jako	k8xS	jako
ústředí	ústředí	k1gNnSc1	ústředí
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
řízena	řízen	k2eAgFnSc1d1	řízena
stavba	stavba	k1gFnSc1	stavba
železnice	železnice	k1gFnSc1	železnice
do	do	k7c2	do
Fairbanks	Fairbanksa	k1gFnPc2	Fairbanksa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
silném	silný	k2eAgNnSc6d1	silné
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
9,2	[number]	k4	9,2
stupňů	stupeň	k1gInPc2	stupeň
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
způsobeny	způsoben	k2eAgFnPc4d1	způsobena
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
místní	místní	k2eAgFnSc1d1	místní
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
Earthquake	Earthquake	k1gNnSc6	Earthquake
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Anchorage	Anchorage	k1gNnSc1	Anchorage
zažilo	zažít	k5eAaPmAgNnS	zažít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
velký	velký	k2eAgInSc4d1	velký
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
vzrůst	vzrůst	k1gInSc4	vzrůst
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Prudhoe	Prudhoe	k1gNnSc6	Prudhoe
Bay	Bay	k1gFnSc2	Bay
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
objevena	objevit	k5eAaPmNgFnS	objevit
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Anchorage	Anchorage	k6eAd1	Anchorage
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
4	[number]	k4	4
396	[number]	k4	396
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
tvoří	tvořit	k5eAaImIp3nP	tvořit
běloši	běloch	k1gMnPc1	běloch
72,2	[number]	k4	72,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
7,3	[number]	k4	7,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
černoši	černoch	k1gMnPc1	černoch
5,8	[number]	k4	5,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
Asiaté	Asiat	k1gMnPc1	Asiat
5,5	[number]	k4	5,5
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Anchorage	Anchorag	k1gFnSc2	Anchorag
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
174	[number]	k4	174
431	[number]	k4	431
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
226	[number]	k4	226
338	[number]	k4	338
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
260	[number]	k4	260
283	[number]	k4	283
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Aljašky	Aljaška	k1gFnSc2	Aljaška
(	(	kIx(	(
<g/>
30	[number]	k4	30
224	[number]	k4	224
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
a	a	k8xC	a
správní	správní	k2eAgNnSc4d1	správní
centrum	centrum	k1gNnSc4	centrum
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Chena	Chen	k1gInSc2	Chen
<g/>
,	,	kIx,	,
při	při	k7c6	při
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Tanana	Tanan	k1gMnSc2	Tanan
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
obsluhováno	obsluhovat	k5eAaImNgNnS	obsluhovat
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
letištěm	letiště	k1gNnSc7	letiště
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
konečnou	konečný	k2eAgFnSc7d1	konečná
stanicí	stanice	k1gFnSc7	stanice
Aljašské	aljašský	k2eAgFnSc2d1	aljašská
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
dokončena	dokončen	k2eAgFnSc1d1	dokončena
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	dík	k1gInPc1	dík
Richardson	Richardsona	k1gFnPc2	Richardsona
Highway	Highwaa	k1gFnPc1	Highwaa
je	být	k5eAaImIp3nS	být
napojeno	napojen	k2eAgNnSc1d1	napojeno
na	na	k7c4	na
Alaska	Alasek	k1gMnSc4	Alasek
Highway	Highwaa	k1gMnSc2	Highwaa
(	(	kIx(	(
<g/>
dokončena	dokončen	k2eAgFnSc1d1	dokončena
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fairbanks	Fairbanks	k6eAd1	Fairbanks
je	být	k5eAaImIp3nS	být
provozním	provozní	k2eAgNnSc7d1	provozní
centrem	centrum	k1gNnSc7	centrum
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
výzkumu	výzkum	k1gInSc2	výzkum
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
oblasti	oblast	k1gFnSc6	oblast
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tu	ten	k3xDgFnSc4	ten
i	i	k9	i
stavební	stavební	k2eAgNnSc1d1	stavební
ředitelství	ředitelství	k1gNnSc1	ředitelství
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
aljašského	aljašský	k2eAgInSc2d1	aljašský
ropovodu	ropovod	k1gInSc2	ropovod
(	(	kIx(	(
<g/>
dokončen	dokončen	k2eAgMnSc1d1	dokončen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
z	z	k7c2	z
Prudhoe	Prudho	k1gFnSc2	Prudho
Bay	Bay	k1gFnSc2	Bay
do	do	k7c2	do
Valdezu	Valdez	k1gInSc2	Valdez
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
také	také	k9	také
Aljašská	aljašský	k2eAgFnSc1d1	aljašská
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c6	na
hledání	hledání	k1gNnSc6	hledání
nerostných	nerostný	k2eAgNnPc2d1	nerostné
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
trvale	trvale	k6eAd1	trvale
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
severské	severský	k2eAgFnSc2d1	severská
flory	flora	k1gFnSc2	flora
a	a	k8xC	a
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
leží	ležet	k5eAaImIp3nS	ležet
základna	základna	k1gFnSc1	základna
US	US	kA	US
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
s	s	k7c7	s
objevením	objevení	k1gNnSc7	objevení
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
těžební	těžební	k2eAgInSc1d1	těžební
tábor	tábor	k1gInSc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenováno	pojmenován	k2eAgNnSc4d1	pojmenováno
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
viceprezidentu	viceprezident	k1gMnSc6	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Charlesi	Charles	k1gMnPc1	Charles
W.	W.	kA	W.
Fairbanksovi	Fairbanksův	k2eAgMnPc1d1	Fairbanksův
<g/>
.	.	kIx.	.
</s>
<s>
Fairbanks	Fairbanks	k6eAd1	Fairbanks
je	být	k5eAaImIp3nS	být
letecky	letecky	k6eAd1	letecky
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
5	[number]	k4	5
280	[number]	k4	280
km	km	kA	km
od	od	k7c2	od
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
73	[number]	k4	73
%	%	kIx~	%
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
13	[number]	k4	13
%	%	kIx~	%
černochů	černoch	k1gMnPc2	černoch
<g/>
,	,	kIx,	,
9	[number]	k4	9
%	%	kIx~	%
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
Asiatů	Asiat	k1gMnPc2	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
22	[number]	k4	22
645	[number]	k4	645
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
843	[number]	k4	843
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
224	[number]	k4	224
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hor	hora	k1gFnPc2	hora
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Roberts	Roberts	k1gInSc1	Roberts
a	a	k8xC	a
Mt	Mt	k1gFnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Juneau	Juneau	k6eAd1	Juneau
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
státní	státní	k2eAgFnSc1d1	státní
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
,	,	kIx,	,
rybářství	rybářství	k1gNnSc1	rybářství
a	a	k8xC	a
ryby	ryba	k1gFnPc4	ryba
zpracující	zpracující	k2eAgInSc4d1	zpracující
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
dřevařský	dřevařský	k2eAgInSc4d1	dřevařský
a	a	k8xC	a
těžební	těžební	k2eAgInSc4d1	těžební
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
turismem	turismus	k1gInSc7	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
možná	možná	k9	možná
pouze	pouze	k6eAd1	pouze
lodí	loď	k1gFnSc7	loď
či	či	k8xC	či
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Juneau	Juneau	k6eAd1	Juneau
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Aljašského	aljašský	k2eAgNnSc2d1	aljašské
teritoria	teritorium	k1gNnSc2	teritorium
oficiálně	oficiálně	k6eAd1	oficiálně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
z	z	k7c2	z
bývalého	bývalý	k2eAgNnSc2d1	bývalé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Sitka	Sitek	k1gInSc2	Sitek
až	až	k9	až
o	o	k7c4	o
6	[number]	k4	6
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
indiánů	indián	k1gMnPc2	indián
kmene	kmen	k1gInSc2	kmen
Tlingit	Tlingita	k1gFnPc2	Tlingita
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
objeveno	objeven	k2eAgNnSc1d1	objeveno
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Doly	dol	k1gInPc1	dol
byly	být	k5eAaImAgInP	být
uzavřeny	uzavřít	k5eAaPmNgInP	uzavřít
až	až	k9	až
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
mužské	mužský	k2eAgFnSc2d1	mužská
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
19	[number]	k4	19
528	[number]	k4	528
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
26	[number]	k4	26
751	[number]	k4	751
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
711	[number]	k4	711
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Aljašky	Aljaška	k1gFnSc2	Aljaška
za	za	k7c4	za
panovnictví	panovnictví	k1gNnSc4	panovnictví
Ruska	Rusko	k1gNnSc2	Rusko
založil	založit	k5eAaPmAgMnS	založit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Baranov	Baranov	k1gInSc1	Baranov
jako	jako	k8xS	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
městečko	městečko	k1gNnSc4	městečko
je	být	k5eAaImIp3nS	být
rybolov	rybolov	k1gInSc1	rybolov
a	a	k8xC	a
dřevařství	dřevařství	k1gNnSc1	dřevařství
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
8	[number]	k4	8
835	[number]	k4	835
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
přístavů	přístav	k1gInPc2	přístav
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
výlovu	výlov	k1gInSc2	výlov
lososů	losos	k1gMnPc2	losos
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
9	[number]	k4	9
922	[number]	k4	922
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Rusy	Rus	k1gMnPc4	Rus
jako	jako	k8xC	jako
centrum	centrum	k1gNnSc4	centrum
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
sídel	sídlo	k1gNnPc2	sídlo
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
významný	významný	k2eAgInSc1d1	významný
rybářský	rybářský	k2eAgInSc1d1	rybářský
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
hlavní	hlavní	k2eAgInSc1d1	hlavní
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Beringově	Beringův	k2eAgNnSc6d1	Beringovo
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	ten	k3xDgFnSc4	ten
jižní	jižní	k2eAgInSc1d1	jižní
konec	konec	k1gInSc1	konec
Aljašské	aljašský	k2eAgFnSc2d1	aljašská
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
další	další	k2eAgInSc1d1	další
významný	významný	k2eAgInSc1d1	významný
rybářský	rybářský	k2eAgInSc1d1	rybářský
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
Point	pointa	k1gFnPc2	pointa
Barrow	Barrow	k1gFnSc2	Barrow
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejseverněji	severně	k6eAd3	severně
ležícím	ležící	k2eAgNnSc7d1	ležící
sídlem	sídlo	k1gNnSc7	sídlo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
North	North	k1gMnSc1	North
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Future	Futur	k1gMnSc5	Futur
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
pomněnka	pomněnka	k1gFnSc1	pomněnka
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
smrk	smrk	k1gInSc1	smrk
sitka	sitek	k1gInSc2	sitek
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
bělokur	bělokura	k1gFnPc2	bělokura
rousný	rousný	k2eAgMnSc1d1	rousný
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Alaska	Alasek	k1gMnSc2	Alasek
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flag	flaga	k1gFnPc2	flaga
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
koupí	koupě	k1gFnSc7	koupě
Aljašky	Aljaška	k1gFnSc2	Aljaška
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
data	datum	k1gNnSc2	datum
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
pátku	pátek	k1gInSc6	pátek
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1867	[number]	k4	1867
(	(	kIx(	(
<g/>
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
následoval	následovat	k5eAaImAgInS	následovat
pátek	pátek	k1gInSc4	pátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
