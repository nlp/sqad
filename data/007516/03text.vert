<s>
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc2	Brunensis
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
i	i	k8xC	i
českých	český	k2eAgFnPc2d1	Česká
firem	firma	k1gFnPc2	firma
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
oborem	obor	k1gInSc7	obor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
každoročně	každoročně	k6eAd1	každoročně
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2001	[number]	k4	2001
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
nesoutěžní	soutěžní	k2eNgFnSc1d1	nesoutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
<g/>
)	)	kIx)	)
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
město	město	k1gNnSc4	město
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
celé	celý	k2eAgFnSc2d1	celá
události	událost	k1gFnSc2	událost
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgMnSc1d1	latinský
a	a	k8xC	a
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
Plamen	plamen	k1gInSc1	plamen
Brna	Brno	k1gNnSc2	Brno
nebo	nebo	k8xC	nebo
Brněnský	brněnský	k2eAgInSc1d1	brněnský
plamen	plamen	k1gInSc1	plamen
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
podstatu	podstata	k1gFnSc4	podstata
celé	celý	k2eAgFnSc2d1	celá
slavnosti	slavnost	k1gFnSc2	slavnost
a	a	k8xC	a
také	také	k9	také
její	její	k3xOp3gNnSc4	její
umístění	umístění	k1gNnSc4	umístění
do	do	k7c2	do
moravské	moravský	k2eAgFnSc2d1	Moravská
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Ohňostroje	ohňostroj	k1gInPc1	ohňostroj
bývají	bývat	k5eAaImIp3nP	bývat
sladěny	sladěn	k2eAgInPc1d1	sladěn
s	s	k7c7	s
hudebním	hudební	k2eAgInSc7d1	hudební
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
rádiích	rádio	k1gNnPc6	rádio
i	i	k8xC	i
reprodukován	reprodukovat	k5eAaBmNgMnS	reprodukovat
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgInPc1d1	soutěžní
ohňostroje	ohňostroj	k1gInPc1	ohňostroj
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
jsou	být	k5eAaImIp3nP	být
odpalovány	odpalovat	k5eAaImNgInP	odpalovat
z	z	k7c2	z
hladiny	hladina	k1gFnSc2	hladina
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
nesoutěžní	soutěžní	k2eNgNnSc1d1	nesoutěžní
preludium	preludium	k1gNnSc1	preludium
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
Denisových	Denisový	k2eAgInPc6d1	Denisový
sadech	sad	k1gInPc6	sad
na	na	k7c6	na
Petrově	Petrův	k2eAgFnSc6d1	Petrova
<g/>
,	,	kIx,	,
závěrečné	závěrečný	k2eAgFnSc3d1	závěrečná
grandfinále	grandfinála	k1gFnSc3	grandfinála
rovněž	rovněž	k9	rovněž
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
Špilberku	Špilberk	k1gInSc2	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgInP	být
soutěžní	soutěžní	k2eAgInPc1d1	soutěžní
ohňostroje	ohňostroj	k1gInPc1	ohňostroj
odpalovány	odpalován	k2eAgInPc1d1	odpalován
na	na	k7c6	na
Kraví	kraví	k2eAgFnSc6d1	kraví
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
po	po	k7c6	po
přemístění	přemístění	k1gNnSc6	přemístění
na	na	k7c4	na
Brněnskou	brněnský	k2eAgFnSc4d1	brněnská
přehradu	přehrada	k1gFnSc4	přehrada
může	moct	k5eAaImIp3nS	moct
ohňostroje	ohňostroj	k1gInPc4	ohňostroj
sledovat	sledovat	k5eAaImF	sledovat
více	hodně	k6eAd2	hodně
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ohňostroje	ohňostroj	k1gInPc4	ohňostroj
sleduje	sledovat	k5eAaImIp3nS	sledovat
i	i	k9	i
za	za	k7c2	za
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
rekordní	rekordní	k2eAgFnSc1d1	rekordní
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
byla	být	k5eAaImAgFnS	být
200	[number]	k4	200
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvůrci	tvůrce	k1gMnPc1	tvůrce
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
mohutnější	mohutný	k2eAgInPc4d2	mohutnější
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
při	při	k7c6	při
kompozici	kompozice	k1gFnSc6	kompozice
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
odrazy	odraz	k1gInPc7	odraz
na	na	k7c6	na
vodní	vodní	k2eAgFnSc6d1	vodní
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2002	[number]	k4	2002
odpalovaly	odpalovat	k5eAaImAgInP	odpalovat
ohňostroje	ohňostroj	k1gInPc1	ohňostroj
pouze	pouze	k6eAd1	pouze
domácí	domácí	k2eAgFnSc2d1	domácí
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
15	[number]	k4	15
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
ve	v	k7c6	v
dnech	den	k1gInPc6	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Prelude	Prelude	k6eAd1	Prelude
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c4	na
Kraví	kraví	k2eAgNnSc4d1	kraví
hoře	hoře	k1gNnSc4	hoře
<g/>
,	,	kIx,	,
finále	finále	k1gNnSc4	finále
v	v	k7c6	v
Denisových	Denisový	k2eAgInPc6d1	Denisový
sadech	sad	k1gInPc6	sad
<g/>
,	,	kIx,	,
soutěžní	soutěžní	k2eAgInPc1d1	soutěžní
ohňostroje	ohňostroj	k1gInPc1	ohňostroj
již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
ještě	ještě	k9	ještě
druhé	druhý	k4xOgNnSc4	druhý
preludium	preludium	k1gNnSc4	preludium
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
na	na	k7c6	na
Brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
oslav	oslava	k1gFnPc2	oslava
80	[number]	k4	80
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
otevření	otevření	k1gNnSc2	otevření
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ročník	ročník	k1gInSc1	ročník
přehlídky	přehlídka	k1gFnSc2	přehlídka
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ohňostrojné	ohňostrojný	k2eAgNnSc1d1	ohňostrojné
prelude	prelude	k6eAd1	prelude
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
nákupním	nákupní	k2eAgMnSc6d1	nákupní
a	a	k8xC	a
zábavním	zábavní	k2eAgNnSc6d1	zábavní
centru	centrum	k1gNnSc6	centrum
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ve	v	k7c6	v
dnech	den	k1gInPc6	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
prelude	prelude	k6eAd1	prelude
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Denisových	Denisový	k2eAgInPc6d1	Denisový
sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
</s>
<s>
Přehlídka	přehlídka	k1gFnSc1	přehlídka
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc1	Brunensis
bývá	bývat	k5eAaImIp3nS	bývat
jako	jako	k9	jako
každá	každý	k3xTgFnSc1	každý
akce	akce	k1gFnSc1	akce
většího	veliký	k2eAgInSc2d2	veliký
rozsahu	rozsah	k1gInSc2	rozsah
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
některých	některý	k3yIgMnPc2	některý
obyvatel	obyvatel	k1gMnPc2	obyvatel
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Bystrc	Bystrc	k1gFnSc1	Bystrc
pro	pro	k7c4	pro
údajné	údajný	k2eAgNnSc4d1	údajné
mrhání	mrhání	k1gNnSc4	mrhání
penězi	peníze	k1gInPc7	peníze
<g/>
,	,	kIx,	,
rušení	rušení	k1gNnSc3	rušení
nočního	noční	k2eAgInSc2d1	noční
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
komplikace	komplikace	k1gFnSc2	komplikace
<g/>
,	,	kIx,	,
výtržnosti	výtržnost	k1gFnSc2	výtržnost
a	a	k8xC	a
poškozování	poškozování	k1gNnSc2	poškozování
prostředí	prostředí	k1gNnSc2	prostředí
kolem	kolem	k7c2	kolem
brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
negativní	negativní	k2eAgInPc4d1	negativní
ekologické	ekologický	k2eAgInPc4d1	ekologický
vlivy	vliv	k1gInPc4	vliv
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
celého	celý	k2eAgInSc2d1	celý
festivalu	festival	k1gInSc2	festival
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
město	město	k1gNnSc1	město
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
zpochybňována	zpochybňován	k2eAgNnPc4d1	zpochybňováno
čísla	číslo	k1gNnPc4	číslo
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
dodává	dodávat	k5eAaImIp3nS	dodávat
samotný	samotný	k2eAgInSc4d1	samotný
organizátor	organizátor	k1gInSc4	organizátor
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
nekvalifikovaných	kvalifikovaný	k2eNgInPc6d1	nekvalifikovaný
odhadech	odhad	k1gInPc6	odhad
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
nijak	nijak	k6eAd1	nijak
věrohodně	věrohodně	k6eAd1	věrohodně
nedoložili	doložit	k5eNaPmAgMnP	doložit
svá	svůj	k3xOyFgNnPc4	svůj
tvrzení	tvrzení	k1gNnPc4	tvrzení
o	o	k7c6	o
nárůstu	nárůst	k1gInSc6	nárůst
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ignis	Ignis	k1gFnSc2	Ignis
Brunensis	Brunensis	k1gFnSc2	Brunensis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc1	Brunensis
-	-	kIx~	-
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
a	a	k8xC	a
videi	vide	k1gInPc7	vide
z	z	k7c2	z
Ignis	Ignis	k1gFnSc2	Ignis
Brunensis	Brunensis	k1gFnSc2	Brunensis
</s>
