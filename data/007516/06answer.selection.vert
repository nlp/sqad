<s>
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc2	Brunensis
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
i	i	k8xC	i
českých	český	k2eAgFnPc2d1	Česká
firem	firma	k1gFnPc2	firma
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
oborem	obor	k1gInSc7	obor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
každoročně	každoročně	k6eAd1	každoročně
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2001	[number]	k4	2001
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
nesoutěžní	soutěžní	k2eNgFnSc1d1	nesoutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
<g/>
)	)	kIx)	)
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
město	město	k1gNnSc4	město
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
