<s>
Trója	Trója	k1gFnSc1	Trója
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
Ilion	Ilion	k1gInSc1	Ilion
(	(	kIx(	(
<g/>
u	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
Ilios	Iliosa	k1gFnPc2	Iliosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgNnSc1d1	legendární
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgNnSc4d1	známé
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
Homérově	Homérův	k2eAgFnSc3d1	Homérova
Iliadě	Iliada	k1gFnSc3	Iliada
<g/>
,	,	kIx,	,
hrdinskému	hrdinský	k2eAgInSc3d1	hrdinský
eposu	epos	k1gInSc3	epos
o	o	k7c6	o
téměř	téměř	k6eAd1	téměř
desetileté	desetiletý	k2eAgFnSc6d1	desetiletá
Trójské	trójský	k2eAgFnSc6d1	Trójská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
