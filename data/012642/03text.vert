<p>
<s>
Třebčín	Třebčín	k1gMnSc1	Třebčín
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
obec	obec	k1gFnSc4	obec
Lutín	Lutína	k1gFnPc2	Lutína
<g/>
.	.	kIx.	.
</s>
<s>
Třebčín	Třebčín	k1gInSc1	Třebčín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Hané	Haná	k1gFnSc2	Haná
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Kosíře	kosíř	k1gInSc2	kosíř
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
protéká	protékat	k5eAaImIp3nS	protékat
potok	potok	k1gInSc1	potok
Deštná	deštný	k2eAgFnSc1d1	Deštná
<g/>
,	,	kIx,	,
pramenící	pramenící	k2eAgFnSc1d1	pramenící
na	na	k7c6	na
Kosíři	kosíř	k1gInSc6	kosíř
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
Studený	studený	k2eAgInSc4d1	studený
Kout	kout	k1gInSc4	kout
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c4	mezi
225	[number]	k4	225
–	–	k?	–
240	[number]	k4	240
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
úrodná	úrodný	k2eAgFnSc1d1	úrodná
půda	půda	k1gFnSc1	půda
jí	on	k3xPp3gFnSc3	on
předurčila	předurčit	k5eAaPmAgFnS	předurčit
zemědělský	zemědělský	k2eAgInSc4d1	zemědělský
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Třebčínem	Třebčín	k1gInSc7	Třebčín
prochází	procházet	k5eAaImIp3nS	procházet
železniční	železniční	k2eAgFnSc1d1	železniční
lokální	lokální	k2eAgFnSc1d1	lokální
trať	trať	k1gFnSc1	trať
mezi	mezi	k7c7	mezi
Olomoucí	Olomouc	k1gFnSc7	Olomouc
<g/>
,	,	kIx,	,
Litovlí	Litovel	k1gFnSc7	Litovel
a	a	k8xC	a
Prostějovem	Prostějov	k1gInSc7	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
6,3	[number]	k4	6,3
km	km	kA	km
jižně	jižně	k6eAd1	jižně
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
7,8	[number]	k4	7,8
km	km	kA	km
východně	východně	k6eAd1	východně
statutární	statutární	k2eAgNnSc4d1	statutární
město	město	k1gNnSc4	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
Třebčín	Třebčína	k1gFnPc2	Třebčína
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
jménem	jméno	k1gNnSc7	jméno
Třebsa	Třebsa	k1gFnSc1	Třebsa
nebo	nebo	k8xC	nebo
Třěbesa	Třěbesa	k1gFnSc1	Třěbesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
mapách	mapa	k1gFnPc6	mapa
a	a	k8xC	a
pramenech	pramen	k1gInPc6	pramen
najdeme	najít	k5eAaPmIp1nP	najít
Třebčín	Třebčín	k1gInSc4	Třebčín
pod	pod	k7c7	pod
několika	několik	k4yIc7	několik
názvy	název	k1gInPc7	název
<g/>
:	:	kIx,	:
Trepsin	Trepsina	k1gFnPc2	Trepsina
(	(	kIx(	(
<g/>
1141	[number]	k4	1141
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trzebsin	Trzebsin	k1gInSc4	Trzebsin
(	(	kIx(	(
<g/>
1389	[number]	k4	1389
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trzepssin	Trzepssin	k1gInSc4	Trzepssin
(	(	kIx(	(
<g/>
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trzeczin	Trzeczin	k1gInSc4	Trzeczin
<g/>
,	,	kIx,	,
Třzebczin	Třzebczin	k1gInSc4	Třzebczin
(	(	kIx(	(
<g/>
1692	[number]	k4	1692
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trzebssinio	Trzebssinio	k6eAd1	Trzebssinio
(	(	kIx(	(
<g/>
1694	[number]	k4	1694
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trepschin	Trepschin	k1gInSc4	Trepschin
(	(	kIx(	(
<g/>
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trzeptschein	Trzeptschein	k1gInSc4	Trzeptschein
(	(	kIx(	(
<g/>
1684	[number]	k4	1684
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Třepčein	Třepčein	k1gInSc4	Třepčein
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravěk	pravěk	k1gInSc4	pravěk
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebčíně	Třebčín	k1gInSc6	Třebčín
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
archeologických	archeologický	k2eAgFnPc2d1	archeologická
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lokalita	lokalita	k1gFnSc1	lokalita
Na	na	k7c6	na
Skale	Skala	k1gFnSc6	Skala
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
mamutí	mamutí	k2eAgInPc1d1	mamutí
kly	kel	k1gInPc1	kel
<g/>
,	,	kIx,	,
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
z	z	k7c2	z
období	období	k1gNnSc2	období
drahanského	drahanský	k2eAgNnSc2d1	drahanský
paleolitika	paleolitikum	k1gNnSc2	paleolitikum
</s>
</p>
<p>
<s>
úštěpy	úštěp	k1gInPc1	úštěp
a	a	k8xC	a
sekerka	sekerka	k1gFnSc1	sekerka
z	z	k7c2	z
období	období	k1gNnSc2	období
eneolitu	eneolit	k1gInSc2	eneolit
</s>
</p>
<p>
<s>
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
únětické	únětický	k2eAgFnSc2d1	únětická
kultury	kultura	k1gFnSc2	kultura
</s>
</p>
<p>
<s>
hroby	hrob	k1gInPc4	hrob
kultury	kultura	k1gFnSc2	kultura
lužických	lužický	k2eAgFnPc2d1	Lužická
popelnicových	popelnicový	k2eAgFnPc2d1	popelnicová
polí	pole	k1gFnPc2	pole
</s>
</p>
<p>
<s>
různé	různý	k2eAgInPc1d1	různý
artefakty	artefakt	k1gInPc1	artefakt
a	a	k8xC	a
osobní	osobní	k2eAgInPc1d1	osobní
předměty	předmět	k1gInPc1	předmět
Keltů	Kelt	k1gMnPc2	Kelt
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
hloubení	hloubení	k1gNnSc6	hloubení
kanalizace	kanalizace	k1gFnSc2	kanalizace
Třebčín-Lípy	Třebčín-Lípa	k1gFnSc2	Třebčín-Lípa
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
obilné	obilný	k2eAgFnSc6d1	obilná
zásobnici	zásobnice	k1gFnSc6	zásobnice
kostrový	kostrový	k2eAgInSc1d1	kostrový
hrob	hrob	k1gInSc1	hrob
ženy	žena	k1gFnSc2	žena
s	s	k7c7	s
výraznými	výrazný	k2eAgFnPc7d1	výrazná
anatomickými	anatomický	k2eAgFnPc7d1	anatomická
deformacemi	deformace	k1gFnPc7	deformace
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
</s>
</p>
<p>
<s>
různé	různý	k2eAgInPc4d1	různý
zlomky	zlomek	k1gInPc4	zlomek
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
drobných	drobný	k2eAgInPc2d1	drobný
předmětů	předmět	k1gInPc2	předmět
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
obce	obec	k1gFnSc2	obec
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
olomoucké	olomoucký	k2eAgNnSc4d1	olomoucké
biskupství	biskupství	k1gNnSc4	biskupství
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
jedno	jeden	k4xCgNnSc1	jeden
popluží	popluží	k1gNnSc1	popluží
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
pečeť	pečeť	k1gFnSc1	pečeť
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
velmi	velmi	k6eAd1	velmi
starého	starý	k2eAgInSc2d1	starý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
z	z	k7c2	z
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
ve	v	k7c6	v
štítu	štít	k1gInSc6	štít
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
husa	husa	k1gFnSc1	husa
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
úrodnosti	úrodnost	k1gFnPc1	úrodnost
a	a	k8xC	a
bohatosti	bohatost	k1gFnPc1	bohatost
hanáckých	hanácký	k2eAgFnPc2d1	Hanácká
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ves	ves	k1gFnSc1	ves
patřila	patřit	k5eAaImAgFnS	patřit
pánům	pan	k1gMnPc3	pan
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
hrdelním	hrdelní	k2eAgFnPc3d1	hrdelní
právem	právem	k6eAd1	právem
spadala	spadat	k5eAaPmAgNnP	spadat
pod	pod	k7c4	pod
Šternberk	Šternberk	k1gInSc4	Šternberk
až	až	k9	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
dával	dávat	k5eAaImAgInS	dávat
své	své	k1gNnSc4	své
dcery	dcera	k1gFnSc2	dcera
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Kláry	Klára	k1gFnSc2	Klára
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
snad	snad	k9	snad
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
klariskám	klariska	k1gFnPc3	klariska
Třebčín	Třebčína	k1gFnPc2	Třebčína
darován	darovat	k5eAaPmNgMnS	darovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
klášterního	klášterní	k2eAgInSc2d1	klášterní
majetku	majetek	k1gInSc2	majetek
spadal	spadat	k5eAaPmAgInS	spadat
Třebčín	Třebčín	k1gInSc1	Třebčín
pod	pod	k7c4	pod
statek	statek	k1gInSc4	statek
Čelechovice	Čelechovice	k1gFnSc2	Čelechovice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1540	[number]	k4	1540
odpustila	odpustit	k5eAaPmAgFnS	odpustit
třebčínským	třebčínský	k2eAgFnPc3d1	třebčínský
abatyše	abatyše	k1gFnSc2	abatyše
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Kláry	Klára	k1gFnSc2	Klára
roboty	robota	k1gFnSc2	robota
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
dvorských	dvorský	k2eAgInPc6d1	dvorský
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
panském	panský	k2eAgInSc6d1	panský
dvoře	dvůr	k1gInSc6	dvůr
v	v	k7c6	v
Třebčíně	Třebčína	k1gFnSc6	Třebčína
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
třebčínští	třebčínský	k2eAgMnPc1d1	třebčínský
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
protestantů	protestant	k1gMnPc2	protestant
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Třebčín	Třebčín	k1gInSc4	Třebčín
asi	asi	k9	asi
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
poničen	poničen	k2eAgMnSc1d1	poničen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
zobrazení	zobrazení	k1gNnSc1	zobrazení
Třebčína	Třebčín	k1gInSc2	Třebčín
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
Jiří	Jiří	k1gMnSc1	Jiří
Matyáš	Matyáš	k1gMnSc1	Matyáš
Vischer	Vischra	k1gFnPc2	Vischra
<g/>
.	.	kIx.	.
1766-1769	[number]	k4	1766-1769
spory	spor	k1gInPc4	spor
s	s	k7c7	s
vrchností	vrchnost	k1gFnSc7	vrchnost
o	o	k7c6	o
konání	konání	k1gNnSc6	konání
robot	robot	k1gMnSc1	robot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
řád	řád	k1gInSc1	řád
Klarisek	klariska	k1gFnPc2	klariska
a	a	k8xC	a
Třebčín	Třebčín	k1gInSc1	Třebčín
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
státního	státní	k2eAgInSc2d1	státní
Náboženského	náboženský	k2eAgInSc2d1	náboženský
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Panské	panský	k2eAgInPc1d1	panský
pozemky	pozemek	k1gInPc1	pozemek
byly	být	k5eAaImAgInP	být
rozparcelovány	rozparcelovat	k5eAaPmNgInP	rozparcelovat
a	a	k8xC	a
na	na	k7c6	na
části	část	k1gFnSc6	část
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
nová	nový	k2eAgFnSc1d1	nová
vesnice	vesnice	k1gFnSc1	vesnice
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Henneberk	Henneberk	k1gInSc1	Henneberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
spojila	spojit	k5eAaPmAgFnS	spojit
zástavba	zástavba	k1gFnSc1	zástavba
Třebčín	Třebčín	k1gInSc4	Třebčín
a	a	k8xC	a
Henneberk	Henneberk	k1gInSc4	Henneberk
a	a	k8xC	a
Henneberk	Henneberk	k1gInSc4	Henneberk
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
vlastní	vlastní	k2eAgFnSc2d1	vlastní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
byl	být	k5eAaImAgInS	být
statek	statek	k1gInSc1	statek
Čelechovice	Čelechovice	k1gFnSc2	Čelechovice
s	s	k7c7	s
Třebčínem	Třebčín	k1gInSc7	Třebčín
prodán	prodat	k5eAaPmNgInS	prodat
belgickému	belgický	k2eAgMnSc3d1	belgický
šlechtici	šlechtic	k1gMnSc3	šlechtic
Filipu	Filip	k1gMnSc3	Filip
Ludvíku	Ludvík	k1gMnSc3	Ludvík
ze	z	k7c2	z
Saint-Genois	Saint-Genois	k1gFnSc2	Saint-Genois
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Anneaucourt	Anneaucourt	k1gInSc1	Anneaucourt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
statek	statek	k1gInSc1	statek
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Třebčíně	Třebčín	k1gInSc6	Třebčín
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Epidemické	epidemický	k2eAgFnPc4d1	epidemická
nákazy	nákaza	k1gFnPc4	nákaza
tyfu	tyf	k1gInSc2	tyf
a	a	k8xC	a
cholery	cholera	k1gFnSc2	cholera
jsou	být	k5eAaImIp3nP	být
prokázány	prokázat	k5eAaPmNgFnP	prokázat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
<g/>
-	-	kIx~	-
<g/>
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
1836	[number]	k4	1836
a	a	k8xC	a
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trať	trať	k1gFnSc1	trať
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Čelechovice	Čelechovice	k1gFnSc1	Čelechovice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
,	,	kIx,	,
se	s	k7c7	s
zastávkou	zastávka	k1gFnSc7	zastávka
v	v	k7c6	v
Třebčíně	Třebčína	k1gFnSc6	Třebčína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
obec	obec	k1gFnSc1	obec
poničila	poničit	k5eAaPmAgFnS	poničit
povodeň	povodeň	k1gFnSc4	povodeň
po	po	k7c6	po
vydatném	vydatný	k2eAgInSc6d1	vydatný
dešti	dešť	k1gInSc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
vlaková	vlakový	k2eAgFnSc1d1	vlaková
vlečka	vlečka	k1gFnSc1	vlečka
z	z	k7c2	z
Třebčína	Třebčín	k1gInSc2	Třebčín
do	do	k7c2	do
Sigmy	Sigma	k1gFnSc2	Sigma
v	v	k7c6	v
Lutíně	Lutína	k1gFnSc6	Lutína
<g/>
.	.	kIx.	.
</s>
<s>
Stavěli	stavět	k5eAaImAgMnP	stavět
ji	on	k3xPp3gFnSc4	on
internovaní	internovaný	k2eAgMnPc1d1	internovaný
vězni	vězeň	k1gMnPc1	vězeň
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
Svatobořice	Svatobořice	k1gFnSc2	Svatobořice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
mlékárna	mlékárna	k1gFnSc1	mlékárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
padlo	padnout	k5eAaImAgNnS	padnout
8	[number]	k4	8
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
4	[number]	k4	4
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
a	a	k8xC	a
2	[number]	k4	2
byli	být	k5eAaImAgMnP	být
prohlášeni	prohlášen	k2eAgMnPc1d1	prohlášen
za	za	k7c4	za
nezvěstné	zvěstný	k2eNgNnSc4d1	nezvěstné
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
legionářů	legionář	k1gMnPc2	legionář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
hasičský	hasičský	k2eAgInSc1d1	hasičský
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
následující	následující	k2eAgInPc1d1	následující
místní	místní	k2eAgInPc1d1	místní
odbory	odbor	k1gInPc1	odbor
spolků	spolek	k1gInPc2	spolek
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc2d1	národní
Jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
Matice	matice	k1gFnSc2	matice
Cyrilo-Metodějské	Cyrilo-Metodějský	k2eAgFnSc2d1	Cyrilo-Metodějský
<g/>
,	,	kIx,	,
Družiny	družina	k1gFnPc4	družina
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
válečných	válečný	k2eAgMnPc2d1	válečný
poškozenců	poškozenec	k1gMnPc2	poškozenec
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
matice	matice	k1gFnSc1	matice
školské	školská	k1gFnSc2	školská
<g/>
,	,	kIx,	,
Čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
katolických	katolický	k2eAgMnPc2d1	katolický
zemědělců	zemědělec	k1gMnPc2	zemědělec
<g/>
,	,	kIx,	,
Slovan	Slovan	k1gMnSc1	Slovan
–	–	k?	–
spolek	spolek	k1gInSc1	spolek
katolických	katolický	k2eAgMnPc2d1	katolický
divadelních	divadelní	k2eAgMnPc2d1	divadelní
ochotníků	ochotník	k1gMnPc2	ochotník
a	a	k8xC	a
Jednota	jednota	k1gFnSc1	jednota
českého	český	k2eAgMnSc2d1	český
orla	orel	k1gMnSc2	orel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
tajně	tajně	k6eAd1	tajně
založen	založit	k5eAaPmNgInS	založit
revoluční	revoluční	k2eAgInSc1d1	revoluční
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skladovala	skladovat	k5eAaImAgFnS	skladovat
munice	munice	k1gFnSc1	munice
převážená	převážený	k2eAgFnSc1d1	převážená
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pak	pak	k6eAd1	pak
ruští	ruský	k2eAgMnPc1d1	ruský
pyrotechnici	pyrotechnik	k1gMnPc1	pyrotechnik
odstřelovali	odstřelovat	k5eAaImAgMnP	odstřelovat
na	na	k7c4	na
Vápence	vápenec	k1gInPc4	vápenec
u	u	k7c2	u
Slatinek	Slatinka	k1gFnPc2	Slatinka
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1950	[number]	k4	1950
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
založeno	založit	k5eAaPmNgNnS	založit
JZD	JZD	kA	JZD
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Třebčín	Třebčín	k1gMnSc1	Třebčín
stal	stát	k5eAaPmAgMnS	stát
místní	místní	k2eAgFnSc7d1	místní
částí	část	k1gFnSc7	část
Lutína	Lutín	k1gInSc2	Lutín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Třebčín	Třebčín	k1gInSc1	Třebčín
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
660	[number]	k4	660
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
<g/>
,	,	kIx,	,
plynofikována	plynofikovat	k5eAaImNgFnS	plynofikovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
vodovod	vodovod	k1gInSc4	vodovod
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
kontejnery	kontejner	k1gInPc1	kontejner
na	na	k7c4	na
tříděný	tříděný	k2eAgInSc4d1	tříděný
odpad	odpad	k1gInSc4	odpad
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
probíhá	probíhat	k5eAaImIp3nS	probíhat
sběr	sběr	k1gInSc4	sběr
nebezpečného	bezpečný	k2eNgInSc2d1	nebezpečný
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
zeleného	zelený	k2eAgInSc2d1	zelený
odpadu	odpad	k1gInSc2	odpad
ze	z	k7c2	z
zahrádek	zahrádka	k1gFnPc2	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
pohostinství	pohostinství	k1gNnSc4	pohostinství
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
autodopravce	autodopravce	k1gMnSc1	autodopravce
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
automechanici	automechanik	k1gMnPc1	automechanik
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc1	malířství
a	a	k8xC	a
natěračství	natěračství	k1gNnSc1	natěračství
<g/>
,	,	kIx,	,
fotografka	fotografka	k1gFnSc1	fotografka
<g/>
,	,	kIx,	,
čistírna	čistírna	k1gFnSc1	čistírna
<g/>
,	,	kIx,	,
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Třebčín	Třebčín	k1gInSc1	Třebčín
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Sdružení	sdružení	k1gNnSc2	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
Třebčín	Třebčína	k1gFnPc2	Třebčína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebčíně	Třebčína	k1gFnSc6	Třebčína
působí	působit	k5eAaImIp3nP	působit
následující	následující	k2eAgFnPc1d1	následující
firmy	firma	k1gFnPc1	firma
a	a	k8xC	a
společnosti	společnost	k1gFnPc1	společnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Davjos	Davjos	k1gInSc1	Davjos
-	-	kIx~	-
Výroba	výroba	k1gFnSc1	výroba
atypického	atypický	k2eAgInSc2d1	atypický
nábytku	nábytek	k1gInSc2	nábytek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MB	MB	kA	MB
TOOL	TOOL	kA	TOOL
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
komponenty	komponenta	k1gFnPc1	komponenta
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
provozovna	provozovna	k1gFnSc1	provozovna
I.	I.	kA	I.
nástrojárna	nástrojárna	k1gFnSc1	nástrojárna
a	a	k8xC	a
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
Třebčín	Třebčína	k1gFnPc2	Třebčína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PCH	pch	k0wR	pch
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
projekty	projekt	k1gInPc1	projekt
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
komplexními	komplexní	k2eAgFnPc7d1	komplexní
službami	služba	k1gFnPc7	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
projekce	projekce	k1gFnPc1	projekce
elektroinstalací	elektroinstalace	k1gFnPc2	elektroinstalace
<g/>
,	,	kIx,	,
komponenty	komponent	k1gInPc4	komponent
pro	pro	k7c4	pro
čerpadla	čerpadlo	k1gNnPc4	čerpadlo
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc4	stavebnictví
<g/>
,	,	kIx,	,
padáky	padák	k1gInPc1	padák
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
pece	pec	k1gFnPc1	pec
</s>
</p>
<p>
<s>
Mlčoch	Mlčoch	k1gMnSc1	Mlčoch
Libor	Libor	k1gMnSc1	Libor
–	–	k?	–
tesařství	tesařství	k1gNnPc2	tesařství
<g/>
,	,	kIx,	,
pokrývačství	pokrývačství	k1gNnSc1	pokrývačství
<g/>
,	,	kIx,	,
klempířství	klempířství	k1gNnSc1	klempířství
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Třebčíně	Třebčína	k1gFnSc6	Třebčína
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sportovní	sportovní	k2eAgInSc1d1	sportovní
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
areál	areál	k1gInSc1	areál
Ohrada	ohrada	k1gFnSc1	ohrada
<g/>
,	,	kIx,	,
tenisové	tenisový	k2eAgNnSc1d1	tenisové
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
kluziště	kluziště	k1gNnPc4	kluziště
a	a	k8xC	a
volejbalové	volejbalový	k2eAgNnSc4d1	volejbalové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
tři	tři	k4xCgNnPc4	tři
dětská	dětský	k2eAgNnPc4d1	dětské
hřiště	hřiště	k1gNnPc4	hřiště
</s>
</p>
<p>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
zařízení	zařízení	k1gNnSc1	zařízení
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
sálem	sál	k1gInSc7	sál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgFnPc4d1	pořádána
různé	různý	k2eAgFnPc4d1	různá
společenské	společenský	k2eAgFnPc4d1	společenská
a	a	k8xC	a
soukromé	soukromý	k2eAgFnPc4d1	soukromá
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
stupně	stupeň	k1gInSc2	stupeň
JPO	JPO	kA	JPO
III	III	kA	III
<g/>
,	,	kIx,	,
pořádání	pořádání	k1gNnSc1	pořádání
okrskových	okrskový	k2eAgFnPc2d1	okrsková
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CAS	CAS	kA	CAS
25	[number]	k4	25
RTHP	RTHP	kA	RTHP
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
A	A	kA	A
–	–	k?	–
30	[number]	k4	30
K	k	k7c3	k
</s>
</p>
<p>
<s>
dvě	dva	k4xCgFnPc1	dva
PPS	PPS	kA	PPS
-	-	kIx~	-
12	[number]	k4	12
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
Třebčínský	Třebčínský	k2eAgInSc1d1	Třebčínský
košt	košt	k1gInSc1	košt
<g/>
,	,	kIx,	,
soutěž	soutěž	k1gFnSc1	soutěž
o	o	k7c4	o
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
destilát	destilát	k1gInSc4	destilát
</s>
</p>
<p>
<s>
===	===	k?	===
Tradice	tradice	k1gFnSc1	tradice
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
organizaci	organizace	k1gFnSc6	organizace
akcí	akce	k1gFnPc2	akce
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
místní	místní	k2eAgMnPc1d1	místní
zastupitelé	zastupitel	k1gMnPc1	zastupitel
<g/>
,	,	kIx,	,
SDH	SDH	kA	SDH
a	a	k8xC	a
Svaz	svaz	k1gInSc1	svaz
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
Ostatky	ostatek	k1gInPc4	ostatek
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
slet	slet	k1gInSc1	slet
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
</s>
</p>
<p>
<s>
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
hody	hod	k1gInPc4	hod
na	na	k7c6	na
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
</s>
</p>
<p>
<s>
oslavy	oslava	k1gFnPc4	oslava
konce	konec	k1gInSc2	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
oslavy	oslava	k1gFnSc2	oslava
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc3	vznik
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
rozsvěcení	rozsvěcení	k1gNnSc1	rozsvěcení
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromu	strom	k1gInSc2	strom
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
na	na	k7c6	na
návsi	náves	k1gFnSc6	náves
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
stavitelem	stavitel	k1gMnSc7	stavitel
byl	být	k5eAaImAgMnS	být
zednický	zednický	k2eAgMnSc1d1	zednický
mistr	mistr	k1gMnSc1	mistr
Ignác	Ignác	k1gMnSc1	Ignác
Bláha	Bláha	k1gMnSc1	Bláha
z	z	k7c2	z
Drahanovic	Drahanovice	k1gFnPc2	Drahanovice
<g/>
,	,	kIx,	,
vymaloval	vymalovat	k5eAaPmAgMnS	vymalovat
ji	on	k3xPp3gFnSc4	on
loštický	loštický	k2eAgMnSc1d1	loštický
malíř	malíř	k1gMnSc1	malíř
František	František	k1gMnSc1	František
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
5.10	[number]	k4	5.10
<g/>
.1862	.1862	k4	.1862
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věži	věž	k1gFnSc6	věž
zvony	zvon	k1gInPc1	zvon
sv.	sv.	kA	sv.
Florián	Florián	k1gMnSc1	Florián
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
zvonek	zvonek	k1gInSc4	zvonek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
zvony	zvon	k1gInPc1	zvon
rekvírovány	rekvírován	k2eAgInPc1d1	rekvírován
za	za	k7c4	za
válek	válek	k1gInSc4	válek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaplička	kaplička	k1gFnSc1	kaplička
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Očekávání	očekávání	k1gNnSc6	očekávání
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Třebčína	Třebčín	k1gInSc2	Třebčín
a	a	k8xC	a
bývalého	bývalý	k2eAgInSc2d1	bývalý
Henneberku	Henneberk	k1gInSc2	Henneberk
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nechali	nechat	k5eAaPmAgMnP	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
zhotovit	zhotovit	k5eAaPmF	zhotovit
Ignác	Ignác	k1gMnSc1	Ignác
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Zapletalovi	Zapletalův	k2eAgMnPc1d1	Zapletalův
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Václav	Václav	k1gMnSc1	Václav
Beck	Beck	k1gMnSc1	Beck
z	z	k7c2	z
Prostějova	Prostějov	k1gInSc2	Prostějov
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
návsi	náves	k1gFnSc6	náves
před	před	k7c7	před
kaplí	kaple	k1gFnSc7	kaple
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
zde	zde	k6eAd1	zde
stávaly	stávat	k5eAaImAgInP	stávat
kříže	kříž	k1gInPc1	kříž
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
a	a	k8xC	a
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
zvonička	zvonička	k1gFnSc1	zvonička
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kříž	kříž	k1gInSc1	kříž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
vsi	ves	k1gFnSc2	ves
Na	na	k7c4	na
Kaplické	Kaplický	k2eAgNnSc4d1	Kaplické
<g/>
,	,	kIx,	,
říkávalo	říkávat	k5eAaImAgNnS	říkávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
nátěru	nátěr	k1gInSc2	nátěr
předchozího	předchozí	k2eAgInSc2d1	předchozí
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kříž	kříž	k1gInSc1	kříž
před	před	k7c7	před
č.	č.	k?	č.
121	[number]	k4	121
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
s	s	k7c7	s
pašijovými	pašijový	k2eAgInPc7d1	pašijový
výjevy	výjev	k1gInPc7	výjev
a	a	k8xC	a
biblickým	biblický	k2eAgInSc7d1	biblický
textem	text	k1gInSc7	text
v	v	k7c6	v
hanáčtině	hanáčtina	k1gFnSc6	hanáčtina
(	(	kIx(	(
<g/>
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kříž	kříž	k1gInSc1	kříž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
jižně	jižně	k6eAd1	jižně
ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Na	na	k7c4	na
Olšanské	olšanský	k2eAgFnPc4d1	Olšanská
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
cholery	cholera	k1gFnSc2	cholera
v	v	k7c6	v
Třebčíně	Třebčín	k1gInSc6	Třebčín
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc3	jeho
dřevěnému	dřevěný	k2eAgMnSc3d1	dřevěný
předchůdci	předchůdce	k1gMnSc3	předchůdce
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
červený	červený	k2eAgInSc4d1	červený
kříž	kříž	k1gInSc4	kříž
</s>
</p>
<p>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c4	na
Zákantí	Zákantí	k1gNnSc4	Zákantí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
před	před	k7c7	před
bývalou	bývalý	k2eAgFnSc7d1	bývalá
školkou	školka	k1gFnSc7	školka
P.	P.	kA	P.
Jana	Jan	k1gMnSc2	Jan
Vychodila	vychodit	k5eAaImAgFnS	vychodit
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
původně	původně	k6eAd1	původně
stála	stát	k5eAaImAgFnS	stát
před	před	k7c7	před
starou	starý	k2eAgFnSc7d1	stará
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc4d1	dnešní
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc4d1	společenské
zařízení	zařízení	k1gNnSc4	zařízení
</s>
</p>
<p>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
Pieta	pieta	k1gFnSc1	pieta
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Slatinic	Slatinice	k1gFnPc2	Slatinice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Václav	Václav	k1gMnSc1	Václav
Beck	Beck	k1gMnSc1	Beck
z	z	k7c2	z
Prostějova	Prostějov	k1gInSc2	Prostějov
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
padlých	padlý	k1gMnPc2	padlý
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
před	před	k7c7	před
bývalou	bývalý	k2eAgFnSc7d1	bývalá
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Václava	Václav	k1gMnSc2	Václav
Becka	Becek	k1gMnSc2	Becek
z	z	k7c2	z
Prostějova	Prostějov	k1gInSc2	Prostějov
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalen	odhalit	k5eAaPmNgInS	odhalit
16.7	[number]	k4	16.7
<g/>
.1922	.1922	k4	.1922
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mlčoch	Mlčoch	k1gMnSc1	Mlčoch
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1880	[number]	k4	1880
Henneberk	Henneberk	k1gInSc1	Henneberk
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
–	–	k?	–
V	v	k7c6	v
letech	let	k1gInPc6	let
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
byl	být	k5eAaImAgInS	být
ministrem	ministr	k1gMnSc7	ministr
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
a	a	k8xC	a
1932	[number]	k4	1932
ministrem	ministr	k1gMnSc7	ministr
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937-1938	[number]	k4	1937-1938
ministrem	ministr	k1gMnSc7	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
prezidentem	prezident	k1gMnSc7	prezident
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
komory	komora	k1gFnSc2	komora
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Sigmund	Sigmund	k1gMnSc1	Sigmund
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1836	[number]	k4	1836
Třebčín	Třebčína	k1gFnPc2	Třebčína
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
Lutín	Lutína	k1gFnPc2	Lutína
<g/>
)	)	kIx)	)
–	–	k?	–
Otec	otec	k1gMnSc1	otec
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Františka	František	k1gMnSc2	František
<g/>
,	,	kIx,	,
zakladatelů	zakladatel	k1gMnPc2	zakladatel
firmy	firma	k1gFnSc2	firma
Sigma	sigma	k1gNnSc7	sigma
Lutín	Lutína	k1gFnPc2	Lutína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
pumpy	pumpa	k1gFnSc2	pumpa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vychodil	vychodit	k5eAaPmAgMnS	vychodit
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1848	[number]	k4	1848
Třebčín	Třebčína	k1gFnPc2	Třebčína
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1926	[number]	k4	1926
Třebčín	Třebčína	k1gFnPc2	Třebčína
<g/>
)	)	kIx)	)
–	–	k?	–
Byl	být	k5eAaImAgInS	být
kaplanem	kaplan	k1gMnSc7	kaplan
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
a	a	k8xC	a
zabýval	zabývat	k5eAaImAgInS	zabývat
se	s	k7c7	s
svatými	svatý	k1gMnPc7	svatý
Cyrilem	Cyril	k1gMnSc7	Cyril
a	a	k8xC	a
Metodějem	Metoděj	k1gMnSc7	Metoděj
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
v	v	k7c6	v
Třebčíně	Třebčína	k1gFnSc6	Třebčína
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školku	školka	k1gFnSc4	školka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Papajík	Papajík	k1gMnSc1	Papajík
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
obcí	obec	k1gFnPc2	obec
Lutín	Lutína	k1gFnPc2	Lutína
a	a	k8xC	a
Třebčín	Třebčína	k1gFnPc2	Třebčína
<g/>
,	,	kIx,	,
Obec	obec	k1gFnSc1	obec
Lutín	Lutín	k1gMnSc1	Lutín
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85600-98-6	[number]	k4	80-85600-98-6
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
:	:	kIx,	:
Výročí	výročí	k1gNnSc1	výročí
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Sboru	sbor	k1gInSc2	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
v	v	k7c6	v
Třebčíně	Třebčína	k1gFnSc6	Třebčína
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
hasičských	hasičský	k2eAgInPc2d1	hasičský
sborů	sbor	k1gInPc2	sbor
v	v	k7c6	v
Lutíně	Lutína	k1gFnSc6	Lutína
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85973-39-1	[number]	k4	80-85973-39-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Třebčín	Třebčína	k1gFnPc2	Třebčína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Třebčín	Třebčína	k1gFnPc2	Třebčína
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
http://www.lutin.cz/	[url]	k?	http://www.lutin.cz/
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
obce	obec	k1gFnSc2	obec
Lutín	Lutína	k1gFnPc2	Lutína
</s>
</p>
<p>
<s>
http://www.kosirsko.cz/	[url]	k?	http://www.kosirsko.cz/
Mikroregion	mikroregion	k1gInSc1	mikroregion
Kosířsko	Kosířsko	k1gNnSc1	Kosířsko
</s>
</p>
<p>
<s>
Vlakové	vlakový	k2eAgNnSc1d1	vlakové
spojení	spojení	k1gNnSc1	spojení
a	a	k8xC	a
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
vedoucí	vedoucí	k1gFnSc2	vedoucí
přes	přes	k7c4	přes
Třebčín	Třebčín	k1gInSc4	Třebčín
na	na	k7c6	na
webu	web	k1gInSc6	web
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
890709	[number]	k4	890709
Olomouc-Lutín-Slatinice	Olomouc-Lutín-Slatinice	k1gFnSc1	Olomouc-Lutín-Slatinice
</s>
</p>
<p>
<s>
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
890736	[number]	k4	890736
Lutín-Smržice-Prostějov	Lutín-Smržice-Prostějovo	k1gNnPc2	Lutín-Smržice-Prostějovo
</s>
</p>
<p>
<s>
Třebčín	Třebčín	k1gInSc1	Třebčín
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vojenském	vojenský	k2eAgNnSc6d1	vojenské
mapování	mapování	k1gNnSc6	mapování
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1766-1768	[number]	k4	1766-1768
na	na	k7c6	na
webu	web	k1gInSc6	web
Laboratoře	laboratoř	k1gFnSc2	laboratoř
geoinformatiky	geoinformatika	k1gFnSc2	geoinformatika
Univerzity	univerzita	k1gFnSc2	univerzita
J.E.	J.E.	k1gFnSc2	J.E.
Purkyně	Purkyně	k1gFnSc2	Purkyně
</s>
</p>
<p>
<s>
Třebčín	Třebčín	k1gInSc1	Třebčín
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
na	na	k7c6	na
webu	web	k1gInSc6	web
MZA	MZA	kA	MZA
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Letecké	letecký	k2eAgNnSc1d1	letecké
snímkování	snímkování	k1gNnSc1	snímkování
Třebčína	Třebčín	k1gInSc2	Třebčín
na	na	k7c6	na
webu	web	k1gInSc6	web
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
geologické	geologický	k2eAgNnSc4d1	geologické
a	a	k8xC	a
radonové	radonový	k2eAgNnSc4d1	radonové
měření	měření	k1gNnSc4	měření
pro	pro	k7c4	pro
Třebčín	Třebčín	k1gInSc4	Třebčín
na	na	k7c6	na
webu	web	k1gInSc6	web
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
