<s>
Glam	Glam	k6eAd1	Glam
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
podžánr	podžánr	k1gInSc4	podžánr
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xC	jako
hair	hair	k1gMnSc1	hair
metal	metat	k5eAaImAgMnS	metat
nebo	nebo	k8xC	nebo
pop	pop	k1gMnSc1	pop
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
hedonistickými	hedonistický	k2eAgInPc7d1	hedonistický
texty	text	k1gInPc7	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
jako	jako	k8xS	jako
kytarový	kytarový	k2eAgInSc1d1	kytarový
efekt	efekt	k1gInSc1	efekt
používá	používat	k5eAaImIp3nS	používat
distortion	distortion	k1gInSc4	distortion
<g/>
,	,	kIx,	,
rychlá	rychlý	k2eAgNnPc1d1	rychlé
sóla	sólo	k1gNnPc1	sólo
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
údery	úder	k1gInPc1	úder
bicích	bicí	k2eAgFnPc2d1	bicí
a	a	k8xC	a
jako	jako	k9	jako
doplněk	doplněk	k1gInSc4	doplněk
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvky	prvek	k1gInPc4	prvek
klasického	klasický	k2eAgNnSc2d1	klasické
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zjemněného	zjemněný	k2eAgInSc2d1	zjemněný
<g/>
)	)	kIx)	)
heavy	heav	k1gInPc7	heav
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
popové	popový	k2eAgFnSc2d1	popová
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
oblékání	oblékání	k1gNnSc2	oblékání
má	mít	k5eAaImIp3nS	mít
napodobovat	napodobovat	k5eAaImF	napodobovat
prostitutky	prostitutka	k1gFnPc4	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Roztrhané	roztrhaný	k2eAgFnPc1d1	roztrhaná
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
,	,	kIx,	,
make-up	makep	k1gInSc1	make-up
<g/>
;	;	kIx,	;
i	i	k9	i
muži	muž	k1gMnPc1	muž
si	se	k3xPyFc3	se
dávají	dávat	k5eAaImIp3nP	dávat
rtěnku	rtěnka	k1gFnSc4	rtěnka
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
sprej	sprej	k1gInSc4	sprej
na	na	k7c4	na
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
představitele	představitel	k1gMnPc4	představitel
patří	patřit	k5eAaImIp3nP	patřit
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
Def	Def	k1gMnSc1	Def
Leppard	Leppard	k1gMnSc1	Leppard
<g/>
,	,	kIx,	,
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gFnSc2	A.S.
<g/>
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Poison	Poison	k1gNnSc1	Poison
<g/>
,	,	kIx,	,
Mötley	Mötleum	k1gNnPc7	Mötleum
Crüe	Crüe	k1gInSc1	Crüe
<g/>
,	,	kIx,	,
Twisted	Twisted	k1gMnSc1	Twisted
Sister	Sister	k1gMnSc1	Sister
a	a	k8xC	a
Steel	Steel	k1gMnSc1	Steel
Panther	Panthra	k1gFnPc2	Panthra
V	v	k7c6	v
letech	let	k1gInPc6	let
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
významných	významný	k2eAgFnPc2d1	významná
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stvořila	stvořit	k5eAaPmAgFnS	stvořit
tvar	tvar	k1gInSc4	tvar
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
kurzu	kurz	k1gInSc2	kurz
hudby	hudba	k1gFnSc2	hudba
během	během	k7c2	během
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
tato	tento	k3xDgNnPc1	tento
alba	album	k1gNnPc1	album
<g/>
:	:	kIx,	:
Quiet	Quiet	k1gInSc1	Quiet
Riot	Riot	k2eAgInSc1d1	Riot
-	-	kIx~	-
Metal	metal	k1gInSc1	metal
Health	Health	k1gInSc1	Health
Mötley	Mötlea	k1gMnSc2	Mötlea
Crüe	Crü	k1gMnSc2	Crü
-	-	kIx~	-
Too	Too	k1gMnSc1	Too
Fast	Fast	k1gMnSc1	Fast
for	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gFnPc2	A.S.
<g/>
P.	P.	kA	P.
-	-	kIx~	-
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gMnSc1	A.S.
<g/>
P.	P.	kA	P.
Ratt	Ratt	k1gMnSc1	Ratt
-	-	kIx~	-
Out	Out	k1gMnSc1	Out
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Cellar	Cellar	k1gMnSc1	Cellar
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crüe	k1gInSc1	Crüe
-	-	kIx~	-
Shout	Shout	k1gInSc1	Shout
at	at	k?	at
the	the	k?	the
Devil	Devil	k1gInSc1	Devil
Dokken	Dokken	k1gInSc1	Dokken
-	-	kIx~	-
Tooth	Tooth	k1gMnSc1	Tooth
and	and	k?	and
Nail	Nail	k1gMnSc1	Nail
Twisted	Twisted	k1gMnSc1	Twisted
Sister	Sister	k1gMnSc1	Sister
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Stay	Sta	k2eAgFnPc4d1	Sta
Hungry	Hungra	k1gFnPc4	Hungra
Autograph	Autograph	k1gInSc1	Autograph
-	-	kIx~	-
Sign	signum	k1gNnPc2	signum
In	In	k1gFnPc2	In
Please	Pleas	k1gInSc6	Pleas
Def	Def	k1gFnPc4	Def
Leppard	Lepparda	k1gFnPc2	Lepparda
-	-	kIx~	-
Pyromania	Pyromanium	k1gNnPc1	Pyromanium
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gMnPc1	Jov
-	-	kIx~	-
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
</s>
<s>
Skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
v	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalové	metalový	k2eAgFnPc4d1	metalová
a	a	k8xC	a
hard	hard	k6eAd1	hard
rockové	rockový	k2eAgFnSc6d1	rocková
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
paradoxně	paradoxně	k6eAd1	paradoxně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
glam	glam	k6eAd1	glam
metalovými	metalový	k2eAgFnPc7d1	metalová
kapelami	kapela	k1gFnPc7	kapela
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
se	s	k7c7	s
žánrovou	žánrový	k2eAgFnSc7d1	žánrová
stylovostí	stylovost	k1gFnSc7	stylovost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc1	album
od	od	k7c2	od
Kiss	Kissa	k1gFnPc2	Kissa
Lick	Lick	k1gMnSc1	Lick
It	It	k1gMnSc1	It
Up	Up	k1gMnSc1	Up
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
potom	potom	k6eAd1	potom
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
s	s	k7c7	s
albem	album	k1gNnSc7	album
Trash	Trasha	k1gFnPc2	Trasha
<g/>
,	,	kIx,	,
a	a	k8xC	a
albem	album	k1gNnSc7	album
od	od	k7c2	od
Aerosmith	Aerosmith	k1gInSc4	Aerosmith
Permanent	permanent	k1gInSc1	permanent
Vacation	Vacation	k1gInSc1	Vacation
<g/>
,	,	kIx,	,
či	či	k8xC	či
Whitesnake	Whitesnak	k1gFnPc1	Whitesnak
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
albem	album	k1gNnSc7	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
uvedlo	uvést	k5eAaPmAgNnS	uvést
velice	velice	k6eAd1	velice
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
hit	hit	k1gInSc4	hit
Here	Her	k1gFnSc2	Her
I	i	k9	i
Go	Go	k1gMnSc1	Go
Again	Again	k1gMnSc1	Again
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
glam	glam	k6eAd1	glam
metalové	metalový	k2eAgFnSc2d1	metalová
éry	éra	k1gFnSc2	éra
vydali	vydat	k5eAaPmAgMnP	vydat
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
album	album	k1gNnSc4	album
Turbo	turba	k1gFnSc5	turba
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
udrželi	udržet	k5eAaPmAgMnP	udržet
krok	krok	k1gInSc4	krok
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
akceptovali	akceptovat	k5eAaBmAgMnP	akceptovat
pestřejší	pestrý	k2eAgInSc4d2	pestřejší
a	a	k8xC	a
barevnější	barevný	k2eAgInSc4d2	barevnější
image	image	k1gInSc4	image
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
syntezátorů	syntezátor	k1gInPc2	syntezátor
dali	dát	k5eAaPmAgMnP	dát
své	svůj	k3xOyFgFnSc6	svůj
hudbě	hudba	k1gFnSc6	hudba
větší	veliký	k2eAgInSc4d2	veliký
pocit	pocit	k1gInSc4	pocit
opojení	opojení	k1gNnSc2	opojení
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
dvěma	dva	k4xCgInPc7	dva
oddíly	oddíl	k1gInPc7	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
normální	normální	k2eAgFnSc6d1	normální
straně	strana	k1gFnSc6	strana
byly	být	k5eAaImAgFnP	být
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xC	jako
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc2	Jovi
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
album	album	k1gNnSc1	album
Slippery	Slippera	k1gFnSc2	Slippera
When	Wheno	k1gNnPc2	Wheno
Wet	Wet	k1gMnPc2	Wet
mělo	mít	k5eAaImAgNnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
rádiích	rádius	k1gInPc6	rádius
a	a	k8xC	a
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Europe	Europ	k1gInSc5	Europ
se	s	k7c7	s
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Countdown	Countdown	k1gMnSc1	Countdown
<g/>
"	"	kIx"	"
byli	být	k5eAaImAgMnP	být
hitem	hit	k1gInSc7	hit
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
26	[number]	k4	26
zemích	zem	k1gFnPc6	zem
<g/>
;	;	kIx,	;
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
byly	být	k5eAaImAgFnP	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
jsou	být	k5eAaImIp3nP	být
popisovány	popisován	k2eAgInPc1d1	popisován
jako	jako	k8xC	jako
Pop	pop	k1gInSc1	pop
Metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
kapely	kapela	k1gFnPc1	kapela
včetně	včetně	k7c2	včetně
Firehouse	Firehouse	k1gFnSc2	Firehouse
a	a	k8xC	a
Winger	Winger	k1gInSc4	Winger
byla	být	k5eAaImAgFnS	být
plochou	plocha	k1gFnSc7	plocha
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
tohoto	tento	k3xDgNnSc2	tento
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
podporovalo	podporovat	k5eAaImAgNnS	podporovat
více	hodně	k6eAd2	hodně
ostrovní	ostrovní	k2eAgFnSc4d1	ostrovní
scénu	scéna	k1gFnSc4	scéna
kolem	kolem	k7c2	kolem
Sunset	Sunseta	k1gFnPc2	Sunseta
Strip	Strip	k1gInSc4	Strip
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
plodilo	plodit	k5eAaImAgNnS	plodit
kapely	kapela	k1gFnSc2	kapela
jako	jako	k9	jako
Poison	Poison	k1gMnSc1	Poison
<g/>
,	,	kIx,	,
Faster	Faster	k1gMnSc1	Faster
Pussycat	Pussycat	k1gMnSc1	Pussycat
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
a	a	k8xC	a
L.A.	L.A.	k1gFnSc1	L.A.
Guns	Gunsa	k1gFnPc2	Gunsa
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
přišly	přijít	k5eAaPmAgFnP	přijít
i	i	k9	i
z	z	k7c2	z
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Cinderella	Cinderella	k1gFnSc1	Cinderella
<g/>
,	,	kIx,	,
Britny	Britno	k1gNnPc7	Britno
Fox	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
z	z	k7c2	z
Philadelphie	Philadelphia	k1gFnSc2	Philadelphia
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
příkladem	příklad	k1gInSc7	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Styper	Styper	k1gInSc1	Styper
přinesl	přinést	k5eAaPmAgInS	přinést
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
texty	text	k1gInPc4	text
do	do	k7c2	do
glam	glama	k1gFnPc2	glama
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Christian	Christian	k1gMnSc1	Christian
Metal	metal	k1gInSc4	metal
byl	být	k5eAaImAgMnS	být
také	také	k9	také
populární	populární	k2eAgMnSc1d1	populární
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
glam	glam	k6eAd1	glam
metalu	metal	k1gInSc2	metal
dokázala	dokázat	k5eAaPmAgFnS	dokázat
být	být	k5eAaImF	být
nejvíce	hodně	k6eAd3	hodně
komerčně	komerčně	k6eAd1	komerčně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
pro	pro	k7c4	pro
žánr	žánr	k1gInSc4	žánr
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
a	a	k8xC	a
těšila	těšit	k5eAaImAgFnS	těšit
svůj	svůj	k3xOyFgInSc4	svůj
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
úspěch	úspěch	k1gInSc4	úspěch
během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
ocitnou	ocitnout	k5eAaPmIp3nP	ocitnout
na	na	k7c6	na
špatné	špatný	k2eAgFnSc6d1	špatná
straně	strana	k1gFnSc6	strana
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
alb	alba	k1gFnPc2	alba
od	od	k7c2	od
1985-1991	[number]	k4	1985-1991
byly	být	k5eAaImAgInP	být
<g/>
:	:	kIx,	:
Mötley	Mötle	k2eAgInPc1d1	Mötle
Crüe	Crü	k1gInPc1	Crü
-	-	kIx~	-
Girls	girl	k1gFnPc1	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc1	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc1	girl
Ratt	Ratt	k1gInSc1	Ratt
-	-	kIx~	-
Invasion	Invasion	k1gInSc1	Invasion
of	of	k?	of
Your	Your	k1gInSc1	Your
Privacy	Privaca	k1gMnSc2	Privaca
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gFnSc2	A.S.
<g/>
P.	P.	kA	P.
-	-	kIx~	-
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Command	Commanda	k1gFnPc2	Commanda
Poison	Poison	k1gMnSc1	Poison
-	-	kIx~	-
Look	Look	k1gMnSc1	Look
What	What	k1gMnSc1	What
the	the	k?	the
Cat	Cat	k1gMnSc1	Cat
Dragged	Dragged	k1gMnSc1	Dragged
In	In	k1gMnSc1	In
Cinderella	Cinderella	k1gMnSc1	Cinderella
-	-	kIx~	-
Night	Night	k2eAgInSc1d1	Night
Songs	Songs	k1gInSc1	Songs
Stryper	Strypra	k1gFnPc2	Strypra
-	-	kIx~	-
To	to	k9	to
Hell	Hell	k1gMnSc1	Hell
with	with	k1gMnSc1	with
the	the	k?	the
Devil	Devil	k1gInSc1	Devil
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
-	-	kIx~	-
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gMnSc1	When
Wet	Wet	k1gMnSc1	Wet
Def	Def	k1gMnSc1	Def
Leppard	Leppard	k1gMnSc1	Leppard
-	-	kIx~	-
Hysteria	Hysterium	k1gNnPc1	Hysterium
Europe	Europ	k1gInSc5	Europ
-	-	kIx~	-
The	The	k1gFnPc2	The
Final	Final	k1gInSc1	Final
Countdown	Countdown	k1gMnSc1	Countdown
Whitesnake	Whitesnake	k1gInSc1	Whitesnake
-	-	kIx~	-
Whitesnake	Whitesnake	k1gInSc1	Whitesnake
Mötley	Mötlea	k1gMnSc2	Mötlea
Crüe	Crü	k1gMnSc2	Crü
-	-	kIx~	-
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Feelgood	Feelgood	k1gInSc1	Feelgood
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
-	-	kIx~	-
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
Winger	Winger	k1gMnSc1	Winger
-	-	kIx~	-
Winger	Winger	k1gMnSc1	Winger
Poison	Poison	k1gMnSc1	Poison
-	-	kIx~	-
Open	Open	k1gMnSc1	Open
Up	Up	k1gMnSc1	Up
and	and	k?	and
Say	Say	k1gMnSc1	Say
<g/>
...	...	k?	...
<g/>
Ahh	Ahh	k1gMnSc1	Ahh
<g/>
!	!	kIx.	!
</s>
<s>
Warrant	Warrant	k1gMnSc1	Warrant
-	-	kIx~	-
Cherry	Cherr	k1gMnPc4	Cherr
Pie	Pius	k1gMnPc4	Pius
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crü	k1gInSc2	Crü
-	-	kIx~	-
Theatre	Theatr	k1gMnSc5	Theatr
Of	Of	k1gMnSc5	Of
Pain	Paina	k1gFnPc2	Paina
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gMnSc1	Row
-	-	kIx~	-
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gMnSc1	Row
Pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
příklad	příklad	k1gInSc4	příklad
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crü	k1gFnSc2	Crü
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Soundscan	Soundscana	k1gFnPc2	Soundscana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
alba	alba	k1gFnSc1	alba
grafu	graf	k1gInSc2	graf
Billboardu	billboard	k1gInSc2	billboard
byla	být	k5eAaImAgFnS	být
kombinace	kombinace	k1gFnSc1	kombinace
zpráv	zpráva	k1gFnPc2	zpráva
od	od	k7c2	od
maloobchodníků	maloobchodník	k1gMnPc2	maloobchodník
<g/>
,	,	kIx,	,
velkoobchodníků	velkoobchodník	k1gMnPc2	velkoobchodník
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
filmových	filmový	k2eAgMnPc2d1	filmový
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c4	na
skutečných	skutečný	k2eAgInPc2d1	skutečný
prodejů	prodej	k1gInPc2	prodej
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kapela	kapela	k1gFnSc1	kapela
na	na	k7c4	na
související	související	k2eAgInSc4d1	související
týden	týden	k1gInSc4	týden
MTV	MTV	kA	MTV
v	v	k7c4	v
Rock	rock	k1gInSc4	rock
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
vyšplhalo	vyšplhat	k5eAaPmAgNnS	vyšplhat
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
2	[number]	k4	2
na	na	k7c4	na
grafy	graf	k1gInPc4	graf
vývěšení	vývěšení	k1gNnSc4	vývěšení
tabule	tabule	k1gFnSc2	tabule
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
prodávané	prodávaný	k2eAgNnSc1d1	prodávané
album	album	k1gNnSc1	album
tohoto	tento	k3xDgInSc2	tento
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
profesionálové	profesionál	k1gMnPc1	profesionál
dali	dát	k5eAaPmAgMnP	dát
extra	extra	k2eAgFnSc4d1	extra
váhu	váha	k1gFnSc4	váha
s	s	k7c7	s
Whitney	Whitneum	k1gNnPc7	Whitneum
Houston	Houston	k1gInSc1	Houston
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jí	on	k3xPp3gFnSc7	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
udržet	udržet	k5eAaPmF	udržet
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
průmysl	průmysl	k1gInSc1	průmysl
jednoduše	jednoduše	k6eAd1	jednoduše
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
jejich	jejich	k3xOp3gNnSc4	jejich
album	album	k1gNnSc4	album
držet	držet	k5eAaImF	držet
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crüe	k1gFnSc1	Crüe
nakonec	nakonec	k6eAd1	nakonec
dobyla	dobýt	k5eAaPmAgFnS	dobýt
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
dalším	další	k2eAgNnSc7d1	další
albem	album	k1gNnSc7	album
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Feelgood	Feelgood	k1gInSc1	Feelgood
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
albem	album	k1gNnSc7	album
jejich	jejich	k3xOp3gFnSc2	jejich
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Glam	Glam	k6eAd1	Glam
metal	metal	k1gInSc1	metal
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
svých	svůj	k3xOyFgMnPc2	svůj
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
postupovala	postupovat	k5eAaImAgFnS	postupovat
80	[number]	k4	80
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Def	Def	k?	Def
Leppard	Leppard	k1gMnSc1	Leppard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
s	s	k7c7	s
albem	album	k1gNnSc7	album
Hysteria	Hysterium	k1gNnSc2	Hysterium
plodilo	plodit	k5eAaImAgNnS	plodit
sedm	sedm	k4xCc1	sedm
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
Hairmetalových	Hairmetalův	k2eAgMnPc2d1	Hairmetalův
alb	album	k1gNnPc2	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgNnPc2d3	nejpopulárnější
alb	album	k1gNnPc2	album
v	v	k7c6	v
80	[number]	k4	80
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poison	Poison	k1gInSc1	Poison
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
Open	Open	k1gMnSc1	Open
Up	Up	k1gMnSc1	Up
and	and	k?	and
Say	Say	k1gMnSc1	Say
<g/>
...	...	k?	...
<g/>
Ahh	Ahh	k1gMnSc1	Ahh
<g/>
!	!	kIx.	!
</s>
<s>
plodilo	plodit	k5eAaImAgNnS	plodit
úspěch	úspěch	k1gInSc4	úspěch
např.	např.	kA	např.
jejich	jejich	k3xOp3gInSc4	jejich
Power	Power	k1gInSc4	Power
Balladou	Ballada	k1gFnSc7	Ballada
Every	Evera	k1gMnSc2	Evera
Rose	Ros	k1gMnSc2	Ros
Has	hasit	k5eAaImRp2nS	hasit
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Thorn	Thorn	k1gInSc1	Thorn
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
prodala	prodat	k5eAaPmAgFnS	prodat
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
dalšími	další	k2eAgInPc7d1	další
hity	hit	k1gInPc7	hit
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Skid	Skid	k6eAd1	Skid
Row	Row	k1gMnPc1	Row
vydali	vydat	k5eAaPmAgMnP	vydat
svoje	svůj	k3xOyFgNnSc4	svůj
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
tvrdší	tvrdý	k2eAgInSc4d2	tvrdší
zvuk	zvuk	k1gInSc4	zvuk
než	než	k8xS	než
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
kapel	kapela	k1gFnPc2	kapela
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Rosesa	k1gFnPc2	Rosesa
změnili	změnit	k5eAaPmAgMnP	změnit
směr	směr	k1gInSc4	směr
glam	glama	k1gFnPc2	glama
metalu	metal	k1gInSc2	metal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
hardrocková	hardrockový	k2eAgFnSc1d1	hardrocková
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
blues-rocku	bluesock	k1gInSc2	blues-rock
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
drželi	držet	k5eAaImAgMnP	držet
některé	některý	k3yIgInPc4	některý
znaky	znak	k1gInPc4	znak
z	z	k7c2	z
glam	glama	k1gFnPc2	glama
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
díky	díky	k7c3	díky
skladbám	skladba	k1gFnPc3	skladba
jako	jako	k9	jako
Sweet	Sweet	k1gMnSc1	Sweet
Child	Child	k1gMnSc1	Child
O	O	kA	O
<g/>
'	'	kIx"	'
Mine	minout	k5eAaImIp3nS	minout
<g/>
,	,	kIx,	,
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Jungle	Jungle	k1gNnSc1	Jungle
<g/>
,	,	kIx,	,
Paradise	Paradise	k1gFnSc1	Paradise
City	City	k1gFnSc2	City
a	a	k8xC	a
prodali	prodat	k5eAaPmAgMnP	prodat
miliony	milion	k4xCgInPc7	milion
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgInSc2	tento
času	čas	k1gInSc2	čas
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
hudebními	hudební	k2eAgInPc7d1	hudební
styly	styl	k1gInPc7	styl
jsou	být	k5eAaImIp3nP	být
Faster	Faster	k1gMnSc1	Faster
Pussycat	Pussycat	k1gMnSc1	Pussycat
<g/>
,	,	kIx,	,
L.A.	L.A.	k1gMnSc1	L.A.
Guns	Guns	k1gInSc1	Guns
<g/>
,	,	kIx,	,
Roxx	Roxx	k1gInSc1	Roxx
Gang	gang	k1gInSc1	gang
a	a	k8xC	a
Dangerous	Dangerous	k1gInSc1	Dangerous
Toys	Toysa	k1gFnPc2	Toysa
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
hnutí	hnutí	k1gNnPc1	hnutí
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
silně	silně	k6eAd1	silně
ovlivněny	ovlivněn	k2eAgFnPc1d1	ovlivněna
rock	rock	k1gInSc4	rock
&	&	k?	&
rollem	roll	k1gMnSc7	roll
a	a	k8xC	a
punk	punk	k1gInSc4	punk
rockem	rock	k1gInSc7	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
Hanoi	Hanoi	k1gNnSc1	Hanoi
Rocks	Rocksa	k1gFnPc2	Rocksa
<g/>
,	,	kIx,	,
kapely	kapela	k1gFnPc1	kapela
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
anglického	anglický	k2eAgNnSc2d1	anglické
hnutí	hnutí	k1gNnSc2	hnutí
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Dogs	Dogs	k1gInSc4	Dogs
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Quireboys	Quireboysa	k1gFnPc2	Quireboysa
nepovažovali	považovat	k5eNaImAgMnP	považovat
se	se	k3xPyFc4	se
bohužel	bohužel	k9	bohužel
za	za	k7c4	za
Metal	metal	k1gInSc4	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Rock	rock	k1gInSc1	rock
&	&	k?	&
Roll	Roll	k1gInSc1	Roll
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
přiřazovány	přiřazován	k2eAgFnPc1d1	přiřazován
k	k	k7c3	k
Glam	Glam	k1gFnPc3	Glam
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Sleaze	Sleaha	k1gFnSc3	Sleaha
<g/>
/	/	kIx~	/
<g/>
Glam	Glam	k1gInSc1	Glam
Metal	metal	k1gInSc1	metal
vrátil	vrátit	k5eAaPmAgInS	vrátit
trochu	trochu	k6eAd1	trochu
díky	dík	k1gInPc7	dík
Buckcherry	Buckcherra	k1gFnSc2	Buckcherra
a	a	k8xC	a
Brides	Brides	k1gInSc1	Brides
of	of	k?	of
Destruction	Destruction	k1gInSc1	Destruction
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vrátili	vrátit	k5eAaPmAgMnP	vrátit
rysy	rys	k1gInPc4	rys
hudby	hudba	k1gFnSc2	hudba
členové	člen	k1gMnPc1	člen
některých	některý	k3yIgFnPc2	některý
Glam	Glama	k1gFnPc2	Glama
metalových	metalový	k2eAgFnPc2d1	metalová
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
Nikki	Nikke	k1gFnSc4	Nikke
Sixx	Sixx	k1gInSc4	Sixx
a	a	k8xC	a
Tracii	Tracie	k1gFnSc4	Tracie
Guns	Gunsa	k1gFnPc2	Gunsa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
moderní	moderní	k2eAgFnPc4d1	moderní
glamové	glamový	k2eAgFnPc4d1	glamový
kapely	kapela	k1gFnPc4	kapela
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
napřiklad	napřiklad	k6eAd1	napřiklad
Steel	Steel	k1gInSc4	Steel
Panther	Panthra	k1gFnPc2	Panthra
<g/>
,	,	kIx,	,
či	či	k8xC	či
The	The	k1gMnSc1	The
Poodles	Poodles	k1gMnSc1	Poodles
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
moderní	moderní	k2eAgFnPc4d1	moderní
glamové	glamový	k2eAgFnPc4d1	glamový
kapely	kapela	k1gFnPc4	kapela
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
napřiklad	napřiklad	k6eAd1	napřiklad
Steel	Steel	k1gInSc4	Steel
Panther	Panthra	k1gFnPc2	Panthra
<g/>
,	,	kIx,	,
The	The	k1gFnPc2	The
Poodles	Poodlesa	k1gFnPc2	Poodlesa
<g/>
,	,	kIx,	,
Reckless	Recklessa	k1gFnPc2	Recklessa
love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Crashdï	Crashdï	k1gMnSc1	Crashdï
či	či	k8xC	či
Santa	Santa	k1gMnSc1	Santa
cruz	cruz	k1gMnSc1	cruz
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrovu	poloostrov	k1gInSc3	poloostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
hodně	hodně	k6eAd1	hodně
jemnější	jemný	k2eAgInPc1d2	jemnější
kousky	kousek	k1gInPc1	kousek
ovlivněné	ovlivněný	k2eAgInPc1d1	ovlivněný
rockovou	rockový	k2eAgFnSc7d1	rocková
klasikou	klasika	k1gFnSc7	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgFnPc4	ten
nejznámnější	známný	k2eAgFnPc4d3	nejznámnější
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
kapely	kapela	k1gFnPc4	kapela
současné	současný	k2eAgFnSc2d1	současná
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Crashdï	Crashdï	k?	Crashdï
Reckless	Reckless	k1gInSc1	Reckless
love	lov	k1gInSc5	lov
Santa	Sant	k1gMnSc4	Sant
cruz	cruz	k1gMnSc1	cruz
Crazy	Craza	k1gFnSc2	Craza
lixx	lixx	k1gInSc1	lixx
Hardcore	Hardcor	k1gInSc5	Hardcor
Superstar	superstar	k1gFnPc6	superstar
A	a	k9	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
máme	mít	k5eAaImIp1nP	mít
i	i	k9	i
my	my	k3xPp1nPc1	my
Češi	Čech	k1gMnPc1	Čech
svou	svůj	k3xOyFgFnSc7	svůj
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
první	první	k4xOgMnSc1	první
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
českého	český	k2eAgInSc2d1	český
glam	gla	k1gNnSc7	gla
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Sweet	Sweet	k1gMnSc1	Sweet
leopard	leopard	k1gMnSc1	leopard
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
tu	tu	k6eAd1	tu
sami	sám	k3xTgMnPc1	sám
nezůstali	zůstat	k5eNaPmAgMnP	zůstat
<g/>
,	,	kIx,	,
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
Nasty	Nast	k1gMnPc7	Nast
ratz	ratza	k1gFnPc2	ratza
<g/>
,	,	kIx,	,
Bad	Bad	k1gMnSc1	Bad
jocker	jocker	k1gMnSc1	jocker
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
cream	cream	k1gInSc1	cream
<g/>
,	,	kIx,	,
<g/>
Sleazy	Sleaz	k1gMnPc4	Sleaz
roxxx	roxxx	k1gInSc4	roxxx
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
jediný	jediný	k2eAgInSc1d1	jediný
glam	glam	k6eAd1	glam
metalový	metalový	k2eAgInSc1d1	metalový
festival	festival	k1gInSc1	festival
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pořádají	pořádat	k5eAaImIp3nP	pořádat
jej	on	k3xPp3gNnSc4	on
kluci	kluk	k1gMnPc1	kluk
ze	z	k7c2	z
Sweet	Sweeta	k1gFnPc2	Sweeta
leopard	leopard	k1gMnSc1	leopard
a	a	k8xC	a
na	na	k7c4	na
jarní	jarní	k2eAgInSc4d1	jarní
ročník	ročník	k1gInSc4	ročník
2017	[number]	k4	2017
přijel	přijet	k5eAaPmAgMnS	přijet
Chris	Chris	k1gFnSc4	Chris
Holmes	Holmes	k1gMnSc1	Holmes
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gMnSc1	A.S.
<g/>
P.	P.	kA	P.
<g/>
,	,	kIx,	,
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kapelou	kapela	k1gFnSc7	kapela
Mean	Meana	k1gFnPc2	Meana
man	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
kapely	kapela	k1gFnPc1	kapela
Jettblack	Jettblack	k1gMnSc1	Jettblack
The	The	k1gMnSc1	The
Treatment	Treatment	k1gMnSc1	Treatment
The	The	k1gMnSc1	The
Darkness	Darknessa	k1gFnPc2	Darknessa
Speed	Speed	k1gMnSc1	Speed
stroke	strok	k1gFnSc2	strok
Cream	Cream	k1gInSc1	Cream
pie	pie	k?	pie
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
</s>
