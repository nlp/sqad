<s>
Solipsismus	solipsismus	k1gInSc1	solipsismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
solus	solus	k1gInSc4	solus
ipse	ips	k1gFnPc4	ips
=	=	kIx~	=
jen	jen	k9	jen
já	já	k3xPp1nSc1	já
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
krajní	krajní	k2eAgFnSc1d1	krajní
forma	forma	k1gFnSc1	forma
subjektivismu	subjektivismus	k1gInSc2	subjektivismus
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
moje	můj	k3xOp1gNnSc4	můj
vědomí	vědomí	k1gNnSc4	vědomí
a	a	k8xC	a
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Solipsismus	solipsismus	k1gInSc1	solipsismus
jako	jako	k8xS	jako
pojem	pojem	k1gInSc1	pojem
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
radikální	radikální	k2eAgFnSc1d1	radikální
epistemologická	epistemologický	k2eAgFnSc1d1	epistemologická
skepse	skepse	k1gFnSc1	skepse
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
světa	svět	k1gInSc2	svět
a	a	k8xC	a
myslí	mysl	k1gFnPc2	mysl
druhých	druhý	k4xOgMnPc2	druhý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
metodického	metodický	k2eAgNnSc2d1	metodické
pochybování	pochybování	k1gNnSc2	pochybování
Descartova	Descartův	k2eAgFnSc1d1	Descartova
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poslední	poslední	k2eAgFnSc4d1	poslední
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
jistotu	jistota	k1gFnSc4	jistota
založil	založit	k5eAaPmAgMnS	založit
právě	právě	k9	právě
na	na	k7c4	na
(	(	kIx(	(
<g/>
hypotetické	hypotetický	k2eAgFnPc4d1	hypotetická
<g/>
)	)	kIx)	)
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
smyslového	smyslový	k2eAgNnSc2d1	smyslové
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
jako	jako	k9	jako
nepochybné	pochybný	k2eNgNnSc1d1	nepochybné
ukáže	ukázat	k5eAaPmIp3nS	ukázat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Solipsismus	solipsismus	k1gInSc1	solipsismus
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc4	tento
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
experiment	experiment	k1gInSc4	experiment
bere	brát	k5eAaImIp3nS	brát
doslova	doslova	k6eAd1	doslova
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
idealistický	idealistický	k2eAgMnSc1d1	idealistický
filosof	filosof	k1gMnSc1	filosof
George	Georg	k1gMnSc2	Georg
Berkeley	Berkelea	k1gFnSc2	Berkelea
soudil	soudit	k5eAaImAgMnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věci	věc	k1gFnPc1	věc
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vnímány	vnímat	k5eAaImNgInP	vnímat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vyvodil	vyvodit	k5eAaBmAgMnS	vyvodit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnější	vnější	k2eAgInSc1d1	vnější
svět	svět	k1gInSc1	svět
existuje	existovat	k5eAaImIp3nS	existovat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
vnímání	vnímání	k1gNnSc2	vnímání
Božího	boží	k2eAgNnSc2d1	boží
<g/>
;	;	kIx,	;
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
představou	představa	k1gFnSc7	představa
pracoval	pracovat	k5eAaImAgInS	pracovat
i	i	k9	i
Isaac	Isaac	k1gInSc1	Isaac
Newton	newton	k1gInSc1	newton
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgMnSc4	jenž
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
Boží	boží	k2eAgInSc1d1	boží
"	"	kIx"	"
<g/>
sensorium	sensorium	k1gNnSc1	sensorium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
krok	krok	k1gInSc1	krok
učinil	učinit	k5eAaPmAgInS	učinit
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgMnSc4	jenž
první	první	k4xOgFnSc6	první
větou	věta	k1gFnSc7	věta
jeho	jeho	k3xOp3gFnSc2	jeho
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gFnSc1	můj
představa	představa	k1gFnSc1	představa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgNnSc3	každý
pozorování	pozorování	k1gNnSc3	pozorování
patří	patřit	k5eAaImIp3nP	patřit
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
bez	bez	k7c2	bez
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
ještě	ještě	k9	ještě
solipsismus	solipsismus	k1gInSc1	solipsismus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
důsledného	důsledný	k2eAgMnSc4d1	důsledný
solipsistu	solipsista	k1gMnSc4	solipsista
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
pokládá	pokládat	k5eAaImIp3nS	pokládat
německý	německý	k2eAgMnSc1d1	německý
myslitel	myslitel	k1gMnSc1	myslitel
Max	Max	k1gMnSc1	Max
Stirner	Stirner	k1gMnSc1	Stirner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Der	drát	k5eAaImRp2nS	drát
Einzige	Einzige	k1gInSc1	Einzige
und	und	k?	und
sein	sein	k1gInSc1	sein
Eigentum	Eigentum	k1gNnSc1	Eigentum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jedinec	jedinec	k1gMnSc1	jedinec
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
zastává	zastávat	k5eAaImIp3nS	zastávat
radikální	radikální	k2eAgInSc4d1	radikální
egoismus	egoismus	k1gInSc4	egoismus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nic	nic	k3yNnSc1	nic
mi	já	k3xPp1nSc3	já
nestojí	stát	k5eNaImIp3nS	stát
nade	nad	k7c4	nad
mnou	já	k3xPp1nSc7	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Stirnera	Stirner	k1gMnSc4	Stirner
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
solipsismus	solipsismus	k1gInSc1	solipsismus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
egoismus	egoismus	k1gInSc1	egoismus
každému	každý	k3xTgMnSc3	každý
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
každé	každý	k3xTgFnSc2	každý
vnější	vnější	k2eAgFnSc2d1	vnější
autority	autorita	k1gFnSc2	autorita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zvnitřněného	zvnitřněný	k2eAgInSc2d1	zvnitřněný
"	"	kIx"	"
<g/>
nad-já	nadá	k1gFnSc1	nad-já
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
superego	superego	k6eAd1	superego
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
filozofiích	filozofie	k1gFnPc6	filozofie
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
přítomna	přítomen	k2eAgFnSc1d1	přítomna
taky	taky	k6eAd1	taky
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
myšlenek	myšlenka	k1gFnPc2	myšlenka
není	být	k5eNaImIp3nS	být
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
-	-	kIx~	-
Ramana	Ramana	k1gFnSc1	Ramana
Maharši	Maharše	k1gFnSc4	Maharše
Solipsismus	solipsismus	k1gInSc4	solipsismus
tedy	tedy	k8xC	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
metodologický	metodologický	k2eAgMnSc1d1	metodologický
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
pokus	pokus	k1gInSc1	pokus
nebo	nebo	k8xC	nebo
provokace	provokace	k1gFnSc1	provokace
<g/>
,	,	kIx,	,
epistemologický	epistemologický	k2eAgInSc1d1	epistemologický
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnější	vnější	k2eAgInSc4d1	vnější
svět	svět	k1gInSc4	svět
ani	ani	k8xC	ani
myšlenky	myšlenka	k1gFnPc4	myšlenka
druhých	druhý	k4xOgMnPc2	druhý
nelze	lze	k6eNd1	lze
bezpečně	bezpečně	k6eAd1	bezpečně
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ontologický	ontologický	k2eAgMnSc1d1	ontologický
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
světu	svět	k1gInSc3	svět
a	a	k8xC	a
druhým	druhý	k4xOgNnSc7	druhý
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgMnSc2	ten
upírá	upírat	k5eAaImIp3nS	upírat
existenci	existence	k1gFnSc4	existence
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Námitky	námitka	k1gFnPc1	námitka
proti	proti	k7c3	proti
solipsismu	solipsismus	k1gInSc3	solipsismus
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
filosofie	filosofie	k1gFnSc1	filosofie
jazyka	jazyk	k1gInSc2	jazyk
namítá	namítat	k5eAaImIp3nS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
solipsista	solipsista	k1gMnSc1	solipsista
používá	používat	k5eAaImIp3nS	používat
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
filosofické	filosofický	k2eAgFnSc6d1	filosofická
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
solipsismus	solipsismus	k1gInSc1	solipsismus
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Sartra	Sartra	k1gFnSc1	Sartra
nebo	nebo	k8xC	nebo
u	u	k7c2	u
českého	český	k2eAgMnSc2d1	český
filosofa	filosof	k1gMnSc2	filosof
Ladislava	Ladislav	k1gMnSc2	Ladislav
Klímy	Klíma	k1gMnSc2	Klíma
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
snažil	snažit	k5eAaImAgMnS	snažit
"	"	kIx"	"
<g/>
solipsisticky	solipsisticky	k6eAd1	solipsisticky
<g/>
"	"	kIx"	"
také	také	k9	také
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
