<s>
Solipsismus	solipsismus	k1gInSc1	solipsismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
solus	solus	k1gInSc4	solus
ipse	ips	k1gFnPc4	ips
=	=	kIx~	=
jen	jen	k9	jen
já	já	k3xPp1nSc1	já
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
krajní	krajní	k2eAgFnSc1d1	krajní
forma	forma	k1gFnSc1	forma
subjektivismu	subjektivismus	k1gInSc2	subjektivismus
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
moje	můj	k3xOp1gNnSc4	můj
vědomí	vědomí	k1gNnSc4	vědomí
a	a	k8xC	a
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
