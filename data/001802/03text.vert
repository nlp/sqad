<s>
Psion	Psion	kA	Psion
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
kapesních	kapesní	k2eAgMnPc2d1	kapesní
počítačů	počítač	k1gMnPc2	počítač
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
firmy	firma	k1gFnSc2	firma
velikosti	velikost	k1gFnSc2	velikost
školního	školní	k2eAgInSc2d1	školní
penálu	penál	k1gInSc2	penál
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
řady	řada	k1gFnPc1	řada
Psionů	Psion	k1gInPc2	Psion
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
standardní	standardní	k2eAgFnPc1d1	standardní
řady	řada	k1gFnPc1	řada
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Příslušenstvím	příslušenství	k1gNnSc7	příslušenství
počítačů	počítač	k1gInPc2	počítač
Psion	Psion	kA	Psion
je	být	k5eAaImIp3nS	být
síťový	síťový	k2eAgInSc4d1	síťový
adaptér	adaptér	k1gInSc4	adaptér
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
a	a	k8xC	a
záložní	záložní	k2eAgFnPc4d1	záložní
baterie	baterie	k1gFnPc4	baterie
včetně	včetně	k7c2	včetně
paměťových	paměťový	k2eAgFnPc2d1	paměťová
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
kabelu	kabela	k1gFnSc4	kabela
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Psion	Psion	kA	Psion
má	mít	k5eAaImIp3nS	mít
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
EPOC	EPOC	kA	EPOC
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
EPOC16	EPOC16	k1gFnSc4	EPOC16
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnPc1d2	starší
verze	verze	k1gFnPc1	verze
<g/>
)	)	kIx)	)
a	a	k8xC	a
EPOC32	EPOC32	k1gFnSc1	EPOC32
(	(	kIx(	(
<g/>
novější	nový	k2eAgFnSc1d2	novější
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Psion	Psion	kA	Psion
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
a	a	k8xC	a
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
software	software	k1gInSc4	software
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
ZX81	ZX81	k1gMnPc2	ZX81
a	a	k8xC	a
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
firma	firma	k1gFnSc1	firma
uvádí	uvádět	k5eAaImIp3nS	uvádět
kapesní	kapesní	k2eAgInSc4d1	kapesní
počítač	počítač	k1gInSc4	počítač
Psion	Psion	kA	Psion
Organiser	Organiser	k1gInSc1	Organiser
s	s	k7c7	s
jednořádkovým	jednořádkový	k2eAgInSc7d1	jednořádkový
displejem	displej	k1gInSc7	displej
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
velmi	velmi	k6eAd1	velmi
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
"	"	kIx"	"
<g/>
databázi	databáze	k1gFnSc4	databáze
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
a	a	k8xC	a
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
neobsahoval	obsahovat	k5eNaImAgInS	obsahovat
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
výrazně	výrazně	k6eAd1	výrazně
vylepšený	vylepšený	k2eAgInSc1d1	vylepšený
model	model	k1gInSc1	model
Psion	Psion	kA	Psion
Organiser	Organiser	k1gInSc4	Organiser
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgNnSc4d1	hlavní
vylepšení	vylepšení	k1gNnSc4	vylepšení
patřil	patřit	k5eAaImAgInS	patřit
dvouřádkový	dvouřádkový	k2eAgInSc1d1	dvouřádkový
displej	displej	k1gInSc1	displej
<g/>
,	,	kIx,	,
lepší	lepšit	k5eAaImIp3nS	lepšit
paměťové	paměťový	k2eAgInPc4d1	paměťový
moduly	modul	k1gInPc4	modul
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
diář	diář	k1gInSc1	diář
nebo	nebo	k8xC	nebo
budík	budík	k1gInSc1	budík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
programování	programování	k1gNnSc2	programování
vlastních	vlastní	k2eAgFnPc2d1	vlastní
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
OPL	OPL	kA	OPL
(	(	kIx(	(
<g/>
Organiser	Organiser	k1gInSc1	Organiser
Programming	Programming	k1gInSc1	Programming
Language	language	k1gFnSc1	language
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
BASICu	BASICum	k1gNnSc3	BASICum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
kompilátorem	kompilátor	k1gInSc7	kompilátor
do	do	k7c2	do
mezikódu	mezikód	k1gInSc2	mezikód
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tehdy	tehdy	k6eAd1	tehdy
nebylo	být	k5eNaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
připojit	připojit	k5eAaPmF	připojit
další	další	k2eAgInPc4d1	další
moduly	modul	k1gInPc4	modul
(	(	kIx(	(
<g/>
např.	např.	kA	např.
port	port	k1gInSc4	port
RS	RS	kA	RS
<g/>
232	[number]	k4	232
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
značně	značně	k6eAd1	značně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
možnosti	možnost	k1gFnPc1	možnost
využití	využití	k1gNnSc2	využití
tohoto	tento	k3xDgInSc2	tento
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
tak	tak	k9	tak
"	"	kIx"	"
<g/>
éra	éra	k1gFnSc1	éra
Psionů	Psion	k1gInPc2	Psion
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
vývoj	vývoj	k1gInSc1	vývoj
zařízení	zařízení	k1gNnSc2	zařízení
"	"	kIx"	"
<g/>
SIBO	SIBO	kA	SIBO
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
SIxteen	SIxteen	k2eAgInSc1d1	SIxteen
Bit	bit	k2eAgInSc1d1	bit
Organiser	Organiser	k1gInSc1	Organiser
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
také	také	k9	také
multitaskingového	multitaskingový	k2eAgInSc2d1	multitaskingový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
EPOC	EPOC	kA	EPOC
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
prodávat	prodávat	k5eAaImF	prodávat
počítače	počítač	k1gInPc4	počítač
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
verze	verze	k1gFnPc1	verze
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
3	[number]	k4	3
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
3	[number]	k4	3
<g/>
a	a	k8xC	a
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
3	[number]	k4	3
<g/>
c	c	k0	c
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
3	[number]	k4	3
<g/>
mx	mx	k?	mx
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
ruská	ruský	k2eAgFnSc1d1	ruská
verze	verze	k1gFnSc1	verze
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
3	[number]	k4	3
<g/>
aR	ar	k1gInSc4	ar
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
počítače	počítač	k1gInPc4	počítač
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
možné	možný	k2eAgNnSc1d1	možné
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
PDA	PDA	kA	PDA
(	(	kIx(	(
<g/>
Personal	Personal	k1gMnSc1	Personal
Digital	Digital	kA	Digital
Assistant	Assistant	k1gMnSc1	Assistant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
textový	textový	k2eAgInSc4d1	textový
procesor	procesor	k1gInSc4	procesor
<g/>
,	,	kIx,	,
tabulkový	tabulkový	k2eAgInSc4d1	tabulkový
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
<g/>
,	,	kIx,	,
databáze	databáze	k1gFnPc4	databáze
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
tvořit	tvořit	k5eAaImF	tvořit
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
OPL	OPL	kA	OPL
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
modemem	modem	k1gInSc7	modem
se	se	k3xPyFc4	se
počítač	počítač	k1gInSc1	počítač
dokázal	dokázat	k5eAaPmAgInS	dokázat
připojit	připojit	k5eAaPmF	připojit
do	do	k7c2	do
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
verze	verze	k1gFnPc1	verze
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
více	hodně	k6eAd2	hodně
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
až	až	k9	až
2	[number]	k4	2
MB	MB	kA	MB
RAM	RAM	kA	RAM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgInSc1d2	lepší
procesor	procesor	k1gInSc1	procesor
(	(	kIx(	(
<g/>
až	až	k9	až
27	[number]	k4	27
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc1d2	veliký
displej	displej	k1gInSc1	displej
(	(	kIx(	(
<g/>
480	[number]	k4	480
<g/>
×	×	k?	×
<g/>
160	[number]	k4	160
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
rozhraní	rozhraní	k1gNnSc1	rozhraní
RS232	RS232	k1gFnSc2	RS232
a	a	k8xC	a
různá	různý	k2eAgNnPc4d1	různé
softwarová	softwarový	k2eAgNnPc4d1	softwarové
vylepšení	vylepšení	k1gNnPc4	vylepšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
nový	nový	k2eAgInSc4d1	nový
kompletně	kompletně	k6eAd1	kompletně
32	[number]	k4	32
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
model	model	k1gInSc1	model
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
následují	následovat	k5eAaImIp3nP	následovat
verze	verze	k1gFnPc1	verze
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
5	[number]	k4	5
<g/>
mx	mx	k?	mx
<g/>
,	,	kIx,	,
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
5	[number]	k4	5
<g/>
mx	mx	k?	mx
Pro	pro	k7c4	pro
a	a	k8xC	a
Psion	Psion	kA	Psion
Revo	Revo	k6eAd1	Revo
(	(	kIx(	(
<g/>
odlehčený	odlehčený	k2eAgMnSc1d1	odlehčený
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
5	[number]	k4	5
<g/>
mx	mx	k?	mx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
počítače	počítač	k1gInSc2	počítač
tvořil	tvořit	k5eAaImAgInS	tvořit
RISCový	RISCový	k2eAgInSc1d1	RISCový
procesor	procesor	k1gInSc1	procesor
ARM710T	ARM710T	k1gFnSc2	ARM710T
(	(	kIx(	(
<g/>
až	až	k9	až
36	[number]	k4	36
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
až	až	k9	až
16	[number]	k4	16
MB	MB	kA	MB
RAM	RAM	kA	RAM
<g/>
,	,	kIx,	,
dotykový	dotykový	k2eAgInSc1d1	dotykový
displej	displej	k1gInSc1	displej
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
640	[number]	k4	640
<g/>
×	×	k?	×
<g/>
240	[number]	k4	240
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
šestnácti	šestnáct	k4xCc7	šestnáct
odstíny	odstín	k1gInPc7	odstín
šedé	šedý	k2eAgFnSc2d1	šedá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
byla	být	k5eAaImAgFnS	být
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
rozhraní	rozhraní	k1gNnSc4	rozhraní
byla	být	k5eAaImAgFnS	být
RS	RS	kA	RS
<g/>
232	[number]	k4	232
<g/>
,	,	kIx,	,
IA	ia	k0	ia
<g/>
,	,	kIx,	,
mikrofon	mikrofon	k1gInSc1	mikrofon
a	a	k8xC	a
reproduktor	reproduktor	k1gInSc1	reproduktor
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
ukládala	ukládat	k5eAaImAgNnP	ukládat
na	na	k7c4	na
CompactFlash	CompactFlash	k1gInSc4	CompactFlash
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
firmami	firma	k1gFnPc7	firma
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
Ericsson	Ericsson	kA	Ericsson
a	a	k8xC	a
Motorola	Motorola	kA	Motorola
založena	založen	k2eAgFnSc1d1	založena
společnost	společnost	k1gFnSc1	společnost
Symbian	Symbian	k1gMnSc1	Symbian
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
EPOC	EPOC	kA	EPOC
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Symbian	Symbian	k1gInSc4	Symbian
OS	osa	k1gFnPc2	osa
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
základem	základ	k1gInSc7	základ
nových	nový	k2eAgInPc2d1	nový
chytrých	chytrý	k2eAgInPc2d1	chytrý
telefonů	telefon	k1gInPc2	telefon
(	(	kIx(	(
<g/>
smartphone	smartphon	k1gInSc5	smartphon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
Psion	Psion	kA	Psion
Series	Series	k1gInSc4	Series
7	[number]	k4	7
a	a	k8xC	a
Psion	Psion	kA	Psion
netBook	netBook	k1gInSc1	netBook
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
displejem	displej	k1gInSc7	displej
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
produkty	produkt	k1gInPc1	produkt
Psionu	Psion	k1gInSc2	Psion
již	již	k6eAd1	již
nebyly	být	k5eNaImAgFnP	být
tak	tak	k6eAd1	tak
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
stahovat	stahovat	k5eAaImF	stahovat
z	z	k7c2	z
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
společnost	společnost	k1gFnSc1	společnost
Motorola	Motorola	kA	Motorola
Solutions	Solutions	k1gInSc1	Solutions
koupila	koupit	k5eAaPmAgFnS	koupit
Psion	Psion	kA	Psion
za	za	k7c4	za
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Psion	Psion	kA	Psion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Historie	historie	k1gFnSc1	historie
Psionů	Psion	k1gInPc2	Psion
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
na	na	k7c4	na
idnes	idnes	k1gInSc4	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
http://www.engadget.com/2012/06/15/motorola-buys-psion-for-200-million/	[url]	k4	http://www.engadget.com/2012/06/15/motorola-buys-psion-for-200-million/
</s>
