<s>
Jan	Jan	k1gMnSc1
Kaliba	Kaliba	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Kaliba	Kalib	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1983	#num#	k4
(	(	kIx(
<g/>
37	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
redaktor	redaktor	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Novinářská	novinářský	k2eAgFnSc1d1
křepelka	křepelka	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Kaliba	Kaliba	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Kaliba	Kaliba	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1983	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
novinář	novinář	k1gMnSc1
a	a	k8xC
reportér	reportér	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
zahraniční	zahraniční	k2eAgInSc1d1
zpravodaj	zpravodaj	k1gInSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1999	#num#	k4
až	až	k9
2003	#num#	k4
vystudoval	vystudovat	k5eAaPmAgInS
všeobecné	všeobecný	k2eAgNnSc4d1
Gymnázium	gymnázium	k1gNnSc4
Na	na	k7c6
Vítězné	vítězný	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
následně	následně	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
2003	#num#	k4
až	až	k9
2009	#num#	k4
mediální	mediální	k2eAgInSc4d1
studia	studium	k1gNnSc4
a	a	k8xC
žurnalistiku	žurnalistika	k1gFnSc4
na	na	k7c6
Institutu	institut	k1gInSc6
komunikačních	komunikační	k2eAgFnPc2d1
studií	studie	k1gFnPc2
a	a	k8xC
žurnalistiky	žurnalistika	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc4
Mgr	Mgr	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
až	až	k9
2007	#num#	k4
byl	být	k5eAaImAgInS
redaktorem	redaktor	k1gMnSc7
<g/>
,	,	kIx,
resp.	resp.	kA
externím	externí	k2eAgMnSc7d1
spolupracovníkem	spolupracovník	k1gMnSc7
portálu	portál	k1gInSc2
HR	hr	k2eAgFnSc1d1
Server	server	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
oblast	oblast	k1gFnSc4
řízení	řízení	k1gNnSc2
a	a	k8xC
rozvoje	rozvoj	k1gInSc2
lidských	lidský	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
začal	začít	k5eAaPmAgMnS
působit	působit	k5eAaImF
v	v	k7c6
Českém	český	k2eAgInSc6d1
rozhlase	rozhlas	k1gInSc6
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
jako	jako	k9
sportovní	sportovní	k2eAgMnSc1d1
redaktor	redaktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřoval	zaměřovat	k5eAaImAgMnS
se	se	k3xPyFc4
především	především	k9
na	na	k7c4
fotbal	fotbal	k1gInSc4
a	a	k8xC
alpské	alpský	k2eAgNnSc4d1
a	a	k8xC
klasické	klasický	k2eAgNnSc4d1
lyžování	lyžování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
2017	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
zahraničním	zahraniční	k2eAgInSc7d1
zpravodajem	zpravodaj	k1gInSc7
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
rok	rok	k1gInSc4
2013	#num#	k4
získal	získat	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
Novinářská	novinářský	k2eAgFnSc1d1
křepelka	křepelka	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
uděluje	udělovat	k5eAaImIp3nS
mladým	mladý	k2eAgMnSc7d1
a	a	k8xC
perspektivním	perspektivní	k2eAgMnPc3d1
novinářům	novinář	k1gMnPc3
do	do	k7c2
33	#num#	k4
let	léto	k1gNnPc2
věku	věk	k1gInSc2
za	za	k7c4
jejich	jejich	k3xOp3gInSc4
významný	významný	k2eAgInSc4d1
žurnalistický	žurnalistický	k2eAgInSc4d1
počin	počin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porotu	porota	k1gFnSc4
oslovil	oslovit	k5eAaPmAgInS
mimořádnou	mimořádný	k2eAgFnSc7d1
schopností	schopnost	k1gFnSc7
hledat	hledat	k5eAaImF
v	v	k7c6
životech	život	k1gInPc6
sportovců	sportovec	k1gMnPc2
lidský	lidský	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
spoluautorem	spoluautor	k1gMnSc7
dvou	dva	k4xCgInPc2
CD	CD	kA
„	„	k?
<g/>
Stříbrní	stříbrnit	k5eAaImIp3nP
Chilané	Chilan	k1gMnPc1
aneb	aneb	k?
Legenda	legenda	k1gFnSc1
žije	žít	k5eAaImIp3nS
<g/>
“	“	k?
a	a	k8xC
projektu	projekt	k1gInSc3
„	„	k?
<g/>
Fotbalové	fotbalový	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jan	Jan	k1gMnSc1
Kaliba	Kaliba	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
LinkedIn	LinkedIn	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ceny	cena	k1gFnPc1
od	od	k7c2
Českého	český	k2eAgInSc2d1
literárního	literární	k2eAgInSc2d1
fondu	fond	k1gInSc2
ovládl	ovládnout	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
informace	informace	k1gFnPc1
<g/>
.	.	kIx.
<g/>
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2014-05-22	2014-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pandemii	pandemie	k1gFnSc4
v	v	k7c6
Americe	Amerika	k1gFnSc6
zpolitizovali	zpolitizovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
rozhlasovým	rozhlasový	k2eAgInSc7d1
zpravodajem	zpravodaj	k1gInSc7
v	v	k7c6
USA	USA	kA
Janem	Jan	k1gMnSc7
Kalibou	Kaliba	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-08-31	2020-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novinářské	novinářský	k2eAgFnPc4d1
křepelky	křepelka	k1gFnPc4
mají	mít	k5eAaImIp3nP
Kaliba	Kaliba	k1gMnSc1
a	a	k8xC
Koutník	Koutník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Médiář	Médiář	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-22	2014-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kaliba	Kaliba	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
1983-	1983-	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
170691	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
301309913	#num#	k4
</s>
