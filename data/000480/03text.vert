<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
geosféra	geosféra	k1gFnSc1	geosféra
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
2900	[number]	k4	2900
km	km	kA	km
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zhruba	zhruba	k6eAd1	zhruba
31	[number]	k4	31
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
asi	asi	k9	asi
mají	mít	k5eAaImIp3nP	mít
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
nikl	nikl	k1gInSc4	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc4d2	veliký
měrnou	měrný	k2eAgFnSc4d1	měrná
hmotnost	hmotnost	k1gFnSc4	hmotnost
než	než	k8xS	než
zemský	zemský	k2eAgInSc4d1	zemský
plášť	plášť	k1gInSc4	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
polotekuté	polotekutý	k2eAgNnSc4d1	polotekuté
vnější	vnější	k2eAgNnSc4d1	vnější
jádro	jádro	k1gNnSc4	jádro
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgInSc4d1	vnější
poloměr	poloměr	k1gInSc4	poloměr
3470	[number]	k4	3470
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
pevné	pevný	k2eAgNnSc1d1	pevné
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
poloměr	poloměr	k1gInSc1	poloměr
přibližně	přibližně	k6eAd1	přibližně
1220	[number]	k4	1220
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vnějším	vnější	k2eAgMnSc7d1	vnější
a	a	k8xC	a
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
jádrem	jádro	k1gNnSc7	jádro
se	se	k3xPyFc4	se
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
5150	[number]	k4	5150
km	km	kA	km
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
jakási	jakýsi	k3yIgFnSc1	jakýsi
přechodná	přechodný	k2eAgFnSc1d1	přechodná
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
160	[number]	k4	160
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
km	km	kA	km
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
Lehmanové	Lehmanová	k1gFnSc2	Lehmanová
<g/>
.	.	kIx.	.
</s>
<s>
Hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
pláštěm	plášť	k1gInSc7	plášť
je	být	k5eAaImIp3nS	být
obdobně	obdobně	k6eAd1	obdobně
Gutenbergova	Gutenbergův	k2eAgFnSc1d1	Gutenbergova
diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
složení	složení	k1gNnSc1	složení
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
86,2	[number]	k4	86,2
<g/>
%	%	kIx~	%
železa	železo	k1gNnSc2	železo
7,25	[number]	k4	7,25
<g/>
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
0,40	[number]	k4	0,40
<g/>
%	%	kIx~	%
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
5,96	[number]	k4	5,96
<g/>
%	%	kIx~	%
síry	síra	k1gFnPc1	síra
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
siderofilní	siderofilní	k2eAgInPc1d1	siderofilní
prvky	prvek	k1gInPc1	prvek
mají	mít	k5eAaImIp3nP	mít
0,04	[number]	k4	0,04
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Polotekuté	polotekutý	k2eAgNnSc1d1	polotekuté
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
vyjma	vyjma	k7c2	vyjma
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
tvořeno	tvořit	k5eAaImNgNnS	tvořit
nejspíše	nejspíše	k9	nejspíše
ještě	ještě	k9	ještě
kobaltem	kobalt	k1gInSc7	kobalt
<g/>
,	,	kIx,	,
sírou	síra	k1gFnSc7	síra
<g/>
,	,	kIx,	,
křemíkem	křemík	k1gInSc7	křemík
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
polotekutou	polotekutý	k2eAgFnSc4d1	polotekutá
strukturu	struktura	k1gFnSc4	struktura
(	(	kIx(	(
<g/>
silito-likvidní	silitoikvidní	k2eAgInSc1d1	silito-likvidní
substrát	substrát	k1gInSc1	substrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
obal	obal	k1gInSc1	obal
jádra	jádro	k1gNnSc2	jádro
tekutý	tekutý	k2eAgInSc1d1	tekutý
<g/>
,	,	kIx,	,
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
pronikání	pronikání	k1gNnSc1	pronikání
zemětřesných	zemětřesný	k2eAgFnPc2d1	zemětřesná
s-vln	slna	k1gFnPc2	s-vlna
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgFnPc1d1	sekundární
vlny	vlna	k1gFnPc1	vlna
-	-	kIx~	-
příčné	příčný	k2eAgFnPc1d1	příčná
vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
skrz	skrz	k7c4	skrz
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tyto	tento	k3xDgFnPc1	tento
vlny	vlna	k1gFnPc1	vlna
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
procházet	procházet	k5eAaImF	procházet
skrz	skrz	k6eAd1	skrz
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgNnSc1d1	vnější
jádro	jádro	k1gNnSc1	jádro
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
alespoň	alespoň	k9	alespoň
10	[number]	k4	10
%	%	kIx~	%
lehkých	lehký	k2eAgInPc2d1	lehký
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
z	z	k7c2	z
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Působící	působící	k2eAgInPc1d1	působící
vysoké	vysoký	k2eAgInPc1d1	vysoký
tlaky	tlak	k1gInPc1	tlak
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
plášť-jádro	plášťádro	k6eAd1	plášť-jádro
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
lehkým	lehký	k2eAgInSc7d1	lehký
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
bude	být	k5eAaImBp3nS	být
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
troilitu	troilit	k1gInSc2	troilit
(	(	kIx(	(
<g/>
FeS	fes	k1gNnSc2	fes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1	pevné
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
pevným	pevný	k2eAgNnSc7d1	pevné
skupenstvím	skupenství	k1gNnSc7	skupenství
zmiňovaného	zmiňovaný	k2eAgNnSc2d1	zmiňované
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
(	(	kIx(	(
<g/>
před	před	k7c7	před
1	[number]	k4	1
až	až	k9	až
1,5	[number]	k4	1,5
miliardou	miliarda	k4xCgFnSc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysvětlován	vysvětlovat	k5eAaImNgInS	vysvětlovat
gravitační	gravitační	k2eAgFnSc7d1	gravitační
krystalizací	krystalizace	k1gFnSc7	krystalizace
původní	původní	k2eAgFnSc2d1	původní
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
bylo	být	k5eAaImAgNnS	být
jádro	jádro	k1gNnSc1	jádro
roztaveno	roztavit	k5eAaPmNgNnS	roztavit
zcela	zcela	k6eAd1	zcela
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jenom	jenom	k9	jenom
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
roztaveno	roztavit	k5eAaPmNgNnS	roztavit
v	v	k7c6	v
období	období	k1gNnSc6	období
planetisimál	planetisimála	k1gFnPc2	planetisimála
celé	celá	k1gFnSc2	celá
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tvar	tvar	k1gInSc1	tvar
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
kulovému	kulový	k2eAgInSc3d1	kulový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zploštělé	zploštělý	k2eAgNnSc1d1	zploštělé
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
elipsoidu	elipsoid	k1gInSc2	elipsoid
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
otočí	otočit	k5eAaPmIp3nS	otočit
o	o	k7c4	o
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
stupně	stupeň	k1gInSc2	stupeň
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polotekutý	polotekutý	k2eAgInSc4d1	polotekutý
obal	obal	k1gInSc4	obal
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
vzniká	vznikat	k5eAaImIp3nS	vznikat
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
není	být	k5eNaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
vycentrované	vycentrovaný	k2eAgNnSc1d1	vycentrované
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
rychlost	rychlost	k1gFnSc1	rychlost
své	svůj	k3xOyFgFnSc2	svůj
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rotací	rotace	k1gFnSc7	rotace
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obrovským	obrovský	k2eAgInPc3d1	obrovský
tlakům	tlak	k1gInPc3	tlak
(	(	kIx(	(
<g/>
odhadovány	odhadovat	k5eAaImNgInP	odhadovat
na	na	k7c4	na
1,4	[number]	k4	1,4
miliónu	milión	k4xCgInSc2	milión
atmosfér	atmosféra	k1gFnPc2	atmosféra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
velice	velice	k6eAd1	velice
žhavé	žhavý	k2eAgNnSc1d1	žhavé
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velikou	veliký	k2eAgFnSc4d1	veliká
hustotu	hustota	k1gFnSc4	hustota
(	(	kIx(	(
<g/>
teploty	teplota	k1gFnSc2	teplota
do	do	k7c2	do
cca	cca	kA	cca
5100	[number]	k4	5100
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
11,3	[number]	k4	11,3
<g/>
-	-	kIx~	-
<g/>
17,3	[number]	k4	17,3
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
spočteno	spočíst	k5eAaPmNgNnS	spočíst
pomocí	pomocí	k7c2	pomocí
setrvačného	setrvačný	k2eAgInSc2d1	setrvačný
momentu	moment	k1gInSc2	moment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
současné	současný	k2eAgFnSc2d1	současná
Země	zem	k1gFnSc2	zem
tvoří	tvořit	k5eAaImIp3nP	tvořit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
železo	železo	k1gNnSc4	železo
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
protoplanety	protoplanet	k1gInPc7	protoplanet
Theia	Theium	k1gNnSc2	Theium
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
