<s>
Republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
res	res	k?	res
publica	publicum	k1gNnSc2	publicum
věc	věc	k1gFnSc1	věc
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
prezident	prezident	k1gMnSc1	prezident
<g/>
)	)	kIx)	)
i	i	k9	i
parlament	parlament	k1gInSc1	parlament
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
počet	počet	k1gInSc1	počet
republik	republika	k1gFnPc2	republika
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
počet	počet	k1gInSc1	počet
monarchií	monarchie	k1gFnPc2	monarchie
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
císařství	císařství	k1gNnSc4	císařství
a	a	k8xC	a
království	království	k1gNnSc4	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
:	:	kIx,	:
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
přímo	přímo	k6eAd1	přímo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poloprezidentská	poloprezidentský	k2eAgFnSc1d1	poloprezidentská
republika	republika	k1gFnSc1	republika
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
parlamentu	parlament	k1gInSc3	parlament
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
není	být	k5eNaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
obsáhlejší	obsáhlý	k2eAgFnPc4d2	obsáhlejší
pravomoci	pravomoc	k1gFnPc4	pravomoc
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
parlamentu	parlament	k1gInSc3	parlament
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
přímo	přímo	k6eAd1	přímo
volena	volit	k5eAaImNgFnS	volit
parlamentem	parlament	k1gInSc7	parlament
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
nepřímo	přímo	k6eNd1	přímo
parlamentem	parlament	k1gInSc7	parlament
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutnou	nutný	k2eAgFnSc7d1	nutná
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgFnPc4d1	malá
pravomoci	pravomoc	k1gFnPc4	pravomoc
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
ultraprezidentská	ultraprezidentský	k2eAgFnSc1d1	ultraprezidentský
republika	republika	k1gFnSc1	republika
–	–	k?	–
podobná	podobný	k2eAgFnSc1d1	podobná
prezidentské	prezidentský	k2eAgFnSc3d1	prezidentská
republice	republika	k1gFnSc3	republika
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
není	být	k5eNaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k9	již
žádná	žádný	k3yNgFnSc1	žádný
republika	republika	k1gFnSc1	republika
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
První	první	k4xOgFnSc7	první
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
-	-	kIx~	-
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gNnSc6	jehož
čele	čelo	k1gNnSc6	čelo
obvykle	obvykle	k6eAd1	obvykle
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
vedoucí	vedoucí	k1gMnSc1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Republiky	republika	k1gFnSc2	republika
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
Autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
forma	forma	k1gFnSc1	forma
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
volebním	volební	k2eAgNnSc7d1	volební
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
na	na	k7c4	na
vyšší	vysoký	k2eAgFnPc4d2	vyšší
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
udělováno	udělován	k2eAgNnSc1d1	udělováno
všem	všecek	k3xTgMnPc3	všecek
občanům	občan	k1gMnPc3	občan
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
občany	občan	k1gMnPc4	občan
se	se	k3xPyFc4	se
však	však	k9	však
počítala	počítat	k5eAaImAgFnS	počítat
jen	jen	k9	jen
část	část	k1gFnSc1	část
svobodných	svobodný	k2eAgMnPc2d1	svobodný
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgFnSc2d1	antická
republiky	republika	k1gFnSc2	republika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
formou	forma	k1gFnSc7	forma
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
největší	veliký	k2eAgInSc4d3	veliký
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
nejvýznamnějších	významný	k2eAgInPc6d3	nejvýznamnější
řeckých	řecký	k2eAgInPc6d1	řecký
městských	městský	k2eAgInPc6d1	městský
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
za	za	k7c2	za
Gaia	Gai	k1gInSc2	Gai
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
republiky	republika	k1gFnPc1	republika
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnSc3d1	omezená
míře	míra	k1gFnSc3	míra
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
měly	mít	k5eAaImAgFnP	mít
víceméně	víceméně	k9	víceméně
status	status	k1gInSc4	status
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
republik	republika	k1gFnPc2	republika
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Banánová	banánový	k2eAgFnSc1d1	banánová
republika	republika	k1gFnSc1	republika
Monarchie	monarchie	k1gFnSc1	monarchie
Republikanismus	republikanismus	k1gInSc4	republikanismus
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
republika	republika	k1gFnSc1	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
republika	republika	k1gFnSc1	republika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
