<s>
Visla	Visla	k1gFnSc1	Visla
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Wisła	Wisł	k2eAgFnSc1d1	Wisła
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Weichsel	Weichsel	k1gInSc1	Weichsel
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Vistula	Vistula	k1gFnSc1	Vistula
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
a	a	k8xC	a
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
nejvodnatějším	vodnatý	k2eAgMnPc3d3	vodnatý
(	(	kIx(	(
<g/>
po	po	k7c6	po
Něvě	Něva	k1gFnSc6	Něva
<g/>
)	)	kIx)	)
přítokem	přítok	k1gInSc7	přítok
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
středem	střed	k1gInSc7	střed
země	zem	k1gFnSc2	zem
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
<g/>
)	)	kIx)	)
na	na	k7c6	na
území	území	k1gNnSc6	území
osmi	osm	k4xCc2	osm
vojvodství	vojvodství	k1gNnPc2	vojvodství
(	(	kIx(	(
<g/>
Slezské	Slezská	k1gFnSc2	Slezská
<g/>
,	,	kIx,	,
Malopolské	malopolský	k2eAgFnSc6d1	Malopolská
<g/>
,	,	kIx,	,
Svatokřížské	Svatokřížský	k2eAgFnSc6d1	Svatokřížský
<g/>
,	,	kIx,	,
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
<g/>
,	,	kIx,	,
Lublinské	lublinský	k2eAgFnSc6d1	Lublinská
<g/>
,	,	kIx,	,
Mazovské	Mazovský	k2eAgFnSc6d1	Mazovský
<g/>
,	,	kIx,	,
Kujavsko-pomořského	Kujavskoomořský	k2eAgNnSc2d1	Kujavsko-pomořské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
,	,	kIx,	,
Pomořského	pomořský	k2eAgNnSc2d1	Pomořské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
1047	[number]	k4	1047
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
194	[number]	k4	194
424	[number]	k4	424
km2	km2	k4	km2
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
000	[number]	k4	000
km2	km2	k4	km2
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1106	[number]	k4	1106
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
svahu	svah	k1gInSc6	svah
Beraní	beraní	k2eAgFnSc2d1	beraní
hory	hora	k1gFnSc2	hora
v	v	k7c6	v
těšínských	těšínský	k2eAgInPc6d1	těšínský
Slezských	slezský	k2eAgInPc6d1	slezský
Beskydech	Beskyd	k1gInPc6	Beskyd
jako	jako	k8xC	jako
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Viselka	Viselka	k1gFnSc1	Viselka
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
Visly	Visla	k1gFnSc2	Visla
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
města	město	k1gNnSc2	město
Sandoměř	Sandoměř	k1gFnSc4	Sandoměř
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
60	[number]	k4	60
km	km	kA	km
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc1	charakter
horské	horský	k2eAgFnSc2d1	horská
bystřiny	bystřina	k1gFnSc2	bystřina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Krakovem	Krakov	k1gInSc7	Krakov
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
mnoha	mnoho	k4c3	mnoho
přítokům	přítok	k1gInPc3	přítok
z	z	k7c2	z
Karpat	Karpaty	k1gInPc2	Karpaty
stává	stávat	k5eAaImIp3nS	stávat
vodnatější	vodnatý	k2eAgFnSc1d2	vodnatější
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
koryta	koryto	k1gNnSc2	koryto
pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Dunajce	Dunajec	k1gInSc2	Dunajec
už	už	k6eAd1	už
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Sanu	Sanus	k1gInSc2	Sanus
už	už	k6eAd1	už
600	[number]	k4	600
až	až	k9	až
1000	[number]	k4	1000
m.	m.	k?	m.
V	v	k7c6	v
těch	ten	k3xDgNnPc6	ten
místech	místo	k1gNnPc6	místo
začíná	začínat	k5eAaImIp3nS	začínat
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
Narwí	Narw	k1gFnSc7	Narw
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
pak	pak	k6eAd1	pak
níže	nízce	k6eAd2	nízce
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
Visla	Visla	k1gFnSc1	Visla
typická	typický	k2eAgFnSc1d1	typická
rovinná	rovinný	k2eAgFnSc1d1	rovinná
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
a	a	k8xC	a
místy	místy	k6eAd1	místy
terasovité	terasovitý	k2eAgFnSc6d1	terasovitá
dolině	dolina	k1gFnSc6	dolina
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
členité	členitý	k2eAgNnSc1d1	členité
a	a	k8xC	a
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
průtoky	průtok	k1gInPc4	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
koryta	koryto	k1gNnSc2	koryto
je	být	k5eAaImIp3nS	být
nestálý	stálý	k2eNgMnSc1d1	nestálý
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
množství	množství	k1gNnSc6	množství
mělčin	mělčina	k1gFnPc2	mělčina
a	a	k8xC	a
prahů	práh	k1gInPc2	práh
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Toruní	Toruň	k1gFnSc7	Toruň
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
plně	plně	k6eAd1	plně
regulováno	regulovat	k5eAaImNgNnS	regulovat
a	a	k8xC	a
výše	vysoce	k6eAd2	vysoce
jsou	být	k5eAaImIp3nP	být
aspoň	aspoň	k9	aspoň
zpevněny	zpevněn	k2eAgInPc1d1	zpevněn
břehy	břeh	k1gInPc1	břeh
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejvíce	hodně	k6eAd3	hodně
hrozí	hrozit	k5eAaImIp3nS	hrozit
podemílání	podemílání	k1gNnSc4	podemílání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
50	[number]	k4	50
km	km	kA	km
začíná	začínat	k5eAaImIp3nS	začínat
delta	delta	k1gFnSc1	delta
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
povodí	povodí	k1gNnSc2	povodí
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
2663	[number]	k4	2663
m	m	kA	m
(	(	kIx(	(
<g/>
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
v	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asymetrie	asymetrie	k1gFnSc1	asymetrie
povodí	povodí	k1gNnSc2	povodí
(	(	kIx(	(
<g/>
pravobřežního	pravobřežní	k2eAgNnSc2d1	pravobřežní
ku	k	k7c3	k
levobřežnímu	levobřežní	k2eAgNnSc3d1	levobřežní
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
73	[number]	k4	73
<g/>
:	:	kIx,	:
<g/>
27	[number]	k4	27
%	%	kIx~	%
<g/>
.	.	kIx.	.
zleva	zleva	k6eAd1	zleva
-	-	kIx~	-
Kamienna	Kamienna	k1gFnSc1	Kamienna
<g/>
,	,	kIx,	,
Przemsza	Przemsza	k1gFnSc1	Przemsza
<g/>
,	,	kIx,	,
Nida	Nidum	k1gNnSc2	Nidum
<g/>
,	,	kIx,	,
Pilica	Pilicum	k1gNnSc2	Pilicum
<g/>
,	,	kIx,	,
Nidzica	Nidzicum	k1gNnSc2	Nidzicum
<g/>
,	,	kIx,	,
Bzura	Bzuro	k1gNnSc2	Bzuro
<g/>
,	,	kIx,	,
Brda	brdo	k1gNnSc2	brdo
<g/>
,	,	kIx,	,
Wda	Wda	k1gMnSc1	Wda
<g/>
,	,	kIx,	,
Wierzyca	Wierzyca	k1gMnSc1	Wierzyca
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
-	-	kIx~	-
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
,	,	kIx,	,
Soła	Sołus	k1gMnSc4	Sołus
<g/>
,	,	kIx,	,
Skawa	Skawus	k1gMnSc4	Skawus
<g/>
,	,	kIx,	,
Raba	rab	k1gMnSc4	rab
<g/>
,	,	kIx,	,
Dunajec	Dunajec	k1gInSc1	Dunajec
<g/>
,	,	kIx,	,
Wisłoka	Wisłoka	k1gMnSc1	Wisłoka
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
<g/>
,	,	kIx,	,
Kurówka	Kurówka	k1gMnSc1	Kurówka
<g/>
,	,	kIx,	,
Wieprz	Wieprz	k1gMnSc1	Wieprz
<g/>
,	,	kIx,	,
Narew	Narew	k1gMnSc1	Narew
s	s	k7c7	s
Bugem	Bug	k1gMnSc7	Bug
<g/>
,	,	kIx,	,
Drwęca	Drwęca	k1gFnSc1	Drwęca
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
60	[number]	k4	60
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
tvoří	tvořit	k5eAaImIp3nP	tvořit
širokou	široký	k2eAgFnSc4d1	široká
říční	říční	k2eAgFnSc4d1	říční
deltu	delta	k1gFnSc4	delta
(	(	kIx(	(
<g/>
Žulawy	Žulawy	k1gInPc4	Žulawy
<g/>
)	)	kIx)	)
s	s	k7c7	s
několika	několik	k4yIc7	několik
rameny	rameno	k1gNnPc7	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
jsou	být	k5eAaImIp3nP	být
Leniwka	Leniwko	k1gNnPc4	Leniwko
(	(	kIx(	(
<g/>
levé	levý	k2eAgNnSc4d1	levé
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nogat	Nogat	k1gInSc1	Nogat
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgNnSc1d1	pravé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
do	do	k7c2	do
Gdaňského	gdaňský	k2eAgInSc2d1	gdaňský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Viselského	viselský	k2eAgInSc2d1	viselský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
delty	delta	k1gFnSc2	delta
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
hrázemi	hráz	k1gFnPc7	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ústí	ústí	k1gNnSc1	ústí
Visly	Visla	k1gFnSc2	Visla
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
východní	východní	k2eAgNnSc4d1	východní
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
Vislu	Visla	k1gFnSc4	Visla
Elbląskou	Elbląská	k1gFnSc4	Elbląská
a	a	k8xC	a
menší	malý	k2eAgNnSc4d2	menší
západní	západní	k2eAgNnSc4d1	západní
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
Vislu	Visla	k1gFnSc4	Visla
Gdaňskou	gdaňský	k2eAgFnSc4d1	Gdaňská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1371	[number]	k4	1371
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
ramenem	rameno	k1gNnSc7	rameno
stává	stávat	k5eAaImIp3nS	stávat
Visla	Visla	k1gFnSc1	Visla
Gdaňská	gdaňský	k2eAgFnSc1d1	Gdaňská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povodni	povodeň	k1gFnSc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
další	další	k2eAgNnSc1d1	další
rameno	rameno	k1gNnSc1	rameno
<g/>
,	,	kIx,	,
Visla	Visla	k1gFnSc1	Visla
Smělá	Smělá	k1gFnSc1	Smělá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
průkop	průkop	k1gInSc1	průkop
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Świbno	Świbna	k1gFnSc5	Świbna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
došlo	dojít	k5eAaPmAgNnS	dojít
mnohokrát	mnohokrát	k6eAd1	mnohokrát
ke	k	k7c3	k
katastrofálním	katastrofální	k2eAgFnPc3d1	katastrofální
povodním	povodeň	k1gFnPc3	povodeň
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
:	:	kIx,	:
1813	[number]	k4	1813
<g/>
,	,	kIx,	,
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
ovlivněn	ovlivněn	k2eAgInSc4d1	ovlivněn
přítoky	přítok	k1gInPc4	přítok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stékají	stékat	k5eAaImIp3nP	stékat
z	z	k7c2	z
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
hladiny	hladina	k1gFnSc2	hladina
díky	díky	k7c3	díky
tajícímu	tající	k2eAgInSc3d1	tající
sněhu	sníh	k1gInSc3	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
i	i	k8xC	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
povodním	povodeň	k1gFnPc3	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Rychlé	Rychlé	k2eAgFnPc1d1	Rychlé
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc1d1	vysoká
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
10	[number]	k4	10
m	m	kA	m
<g/>
)	)	kIx)	)
vzestupy	vzestup	k1gInPc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
někdy	někdy	k6eAd1	někdy
až	až	k9	až
katastrofální	katastrofální	k2eAgFnPc1d1	katastrofální
škody	škoda	k1gFnPc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
hladiny	hladina	k1gFnSc2	hladina
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ucpání	ucpání	k1gNnSc2	ucpání
koryta	koryto	k1gNnSc2	koryto
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
povodněmi	povodeň	k1gFnPc7	povodeň
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
mělká	mělký	k2eAgFnSc1d1	mělká
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
průtok	průtok	k1gInSc1	průtok
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
84	[number]	k4	84
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
590	[number]	k4	590
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
u	u	k7c2	u
Tczewa	Tczew	k1gInSc2	Tczew
nedaleko	nedaleko	k7c2	nedaleko
ústí	ústí	k1gNnSc2	ústí
1054	[number]	k4	1054
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Ledová	ledový	k2eAgFnSc1d1	ledová
pokrývka	pokrývka	k1gFnSc1	pokrývka
je	být	k5eAaImIp3nS	být
především	především	k9	především
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
nestálá	stálý	k2eNgFnSc1d1	nestálá
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
splavná	splavný	k2eAgFnSc1d1	splavná
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
940	[number]	k4	940
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
k	k	k7c3	k
městu	město	k1gNnSc3	město
Brzeszcze	Brzeszcze	k1gFnSc2	Brzeszcze
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
umělých	umělý	k2eAgInPc2d1	umělý
kanálů	kanál	k1gInPc2	kanál
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
třemi	tři	k4xCgNnPc7	tři
sousedními	sousední	k2eAgNnPc7d1	sousední
povodími	povodí	k1gNnPc7	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Bydhošťský	Bydhošťský	k2eAgInSc1d1	Bydhošťský
kanál	kanál	k1gInSc1	kanál
a	a	k8xC	a
řeky	řeka	k1gFnPc1	řeka
Noteć	Noteć	k1gFnSc2	Noteć
a	a	k8xC	a
Warta	Wart	k1gInSc2	Wart
s	s	k7c7	s
Odrou	Odra	k1gFnSc7	Odra
<g/>
,	,	kIx,	,
Augustovský	Augustovský	k2eAgInSc1d1	Augustovský
kanál	kanál	k1gInSc1	kanál
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
Czarna	Czarna	k1gFnSc1	Czarna
Hańcza	Hańcza	k1gFnSc1	Hańcza
s	s	k7c7	s
Němenem	Němen	k1gInSc7	Němen
<g/>
,	,	kIx,	,
Dněpersko-bugský	Dněperskougský	k1gMnSc1	Dněpersko-bugský
kanál	kanál	k1gInSc1	kanál
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
Pripjať	Pripjať	k1gFnSc2	Pripjať
s	s	k7c7	s
Dněprem	Dněpr	k1gInSc7	Dněpr
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
města	město	k1gNnPc1	město
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Tarnobrzeg	Tarnobrzeg	k1gInSc1	Tarnobrzeg
<g/>
,	,	kIx,	,
Sandoměř	Sandoměř	k1gInSc1	Sandoměř
<g/>
,	,	kIx,	,
Puławy	Puławy	k1gInPc1	Puławy
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Płock	Płock	k1gInSc1	Płock
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Włocławek	Włocławek	k1gInSc1	Włocławek
<g/>
,	,	kIx,	,
Toruň	Toruň	k1gFnSc1	Toruň
<g/>
,	,	kIx,	,
Bydhošť	Bydhošť	k1gFnSc1	Bydhošť
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Fordon	Fordona	k1gFnPc2	Fordona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Grudziądz	Grudziądz	k1gInSc1	Grudziądz
<g/>
,	,	kIx,	,
Malbork	Malbork	k1gInSc1	Malbork
(	(	kIx(	(
<g/>
na	na	k7c6	na
rameni	rameno	k1gNnSc6	rameno
delty	delta	k1gFnSc2	delta
Nogatu	Nogat	k1gInSc2	Nogat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tczew	Tczew	k1gFnSc1	Tczew
<g/>
,	,	kIx,	,
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
(	(	kIx(	(
<g/>
na	na	k7c6	na
rameni	rameno	k1gNnSc6	rameno
delty	delta	k1gFnSc2	delta
Leniwky	Leniwka	k1gFnSc2	Leniwka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Ihnatowicz	Ihnatowicz	k1gInSc1	Ihnatowicz
St.	st.	kA	st.
<g/>
,	,	kIx,	,
Obuchowski	Obuchowski	k1gNnSc1	Obuchowski
An	An	k1gFnPc2	An
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Turnau	Turna	k2eAgFnSc4d1	Turna
Bir	Bir	k1gFnSc4	Bir
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ogolne	Ogoln	k1gInSc5	Ogoln
koncepcje	koncepcj	k1gInSc2	koncepcj
techniczne	technicznout	k5eAaPmIp3nS	technicznout
zagospodorowania	zagospodorowanium	k1gNnSc2	zagospodorowanium
zasobow	zasobow	k?	zasobow
wodnych	wodnych	k1gMnSc1	wodnych
w	w	k?	w
Polsce	Polska	k1gFnSc3	Polska
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Gospodarka	Gospodarka	k1gFnSc1	Gospodarka
Wodna	Wodna	k1gFnSc1	Wodna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
č.	č.	k?	č.
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Beć	Beć	k?	Beć
St.	st.	kA	st.
<g/>
,	,	kIx,	,
System	Syst	k1gMnSc7	Syst
wodny	wodna	k1gMnSc2	wodna
Wisly	Wisla	k1gMnSc2	Wisla
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Gospodarka	Gospodarka	k1gFnSc1	Gospodarka
Wodna	Wodna	k1gFnSc1	Wodna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
č.	č.	k?	č.
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Lencevič	Lencevič	k1gInSc1	Lencevič
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgInSc1d1	ruský
překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
polštiny	polština	k1gFnSc2	polština
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Л	Л	k?	Л
С	С	k?	С
<g/>
,	,	kIx,	,
Ф	Ф	k?	Ф
г	г	k?	г
П	П	k?	П
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
.	.	kIx.	.
с	с	k?	с
п	п	k?	п
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
В	В	k?	В
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Visla	visnout	k5eAaImAgFnS	visnout
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Visla	Visla	k1gFnSc1	Visla
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Visla	visnout	k5eAaImAgFnS	visnout
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
