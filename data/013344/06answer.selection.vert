<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1830	[number]	k4	1830
zámek	zámek	k1gInSc1	zámek
Schönbrunn	Schönbrunn	k1gInSc4	Schönbrunn
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc4	Vídeň
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
zámek	zámek	k1gInSc1	zámek
Schönbrunn	Schönbrunn	k1gInSc4	Schönbrunn
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsbursko-lotrinského	habsburskootrinský	k2eAgInSc2d1	habsbursko-lotrinský
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
68	[number]	k4	68
let	léto	k1gNnPc2	léto
císař	císař	k1gMnSc1	císař
rakouský	rakouský	k2eAgMnSc1d1	rakouský
(	(	kIx(	(
<g/>
nekorunovaný	korunovaný	k2eNgInSc1d1	nekorunovaný
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
(	(	kIx(	(
<g/>
nekorunovaný	korunovaný	k2eNgMnSc1d1	nekorunovaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
uherský	uherský	k2eAgInSc1d1	uherský
(	(	kIx(	(
<g/>
korunovace	korunovace	k1gFnSc1	korunovace
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
lombardský	lombardský	k2eAgMnSc1d1	lombardský
a	a	k8xC	a
benátský	benátský	k2eAgMnSc1d1	benátský
<g/>
,	,	kIx,	,
dalmátský	dalmátský	k2eAgMnSc1d1	dalmátský
<g/>
,	,	kIx,	,
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
<g/>
,	,	kIx,	,
slavonský	slavonský	k2eAgMnSc1d1	slavonský
atd.	atd.	kA	atd.
</s>
