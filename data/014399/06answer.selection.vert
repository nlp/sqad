<s>
Matricí	matrice	k1gFnSc7
asistovaná	asistovaný	k2eAgFnSc1d1
laserová	laserový	k2eAgFnSc1d1
desorpce	desorpce	k1gFnSc1
<g/>
/	/	kIx~
<g/>
ionizace	ionizace	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
MALDI	MALDI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
ionizace	ionizace	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS
matrice	matrice	k1gFnSc1
absorbující	absorbující	k2eAgFnSc1d1
energii	energie	k1gFnSc4
laseru	laser	k1gInSc2
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
iontů	ion	k1gInPc2
z	z	k7c2
velkých	velký	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
za	za	k7c4
minimální	minimální	k2eAgFnSc2d1
fragmentace	fragmentace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
analýzu	analýza	k1gFnSc4
biomolekul	biomolekula	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
DNA	DNA	kA
<g/>
,	,	kIx,
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
peptidů	peptid	k1gInPc2
a	a	k8xC
sacharidů	sacharid	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
velkých	velký	k2eAgFnPc2d1
organických	organický	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
polymery	polymer	k1gInPc1
a	a	k8xC
dendrimery	dendrimer	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
jsou	být	k5eAaImIp3nP
nestabilní	stabilní	k2eNgInPc1d1
a	a	k8xC
při	při	k7c6
použití	použití	k1gNnSc6
jiných	jiný	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
ionizace	ionizace	k1gFnSc2
u	u	k7c2
nich	on	k3xPp3gFnPc2
dochází	docházet	k5eAaImIp3nP
k	k	k7c3
fragmentaci	fragmentace	k1gFnSc3
<g/>
.	.	kIx.
</s>