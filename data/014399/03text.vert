<s>
MALDI	MALDI	kA
</s>
<s>
MALDI-TOF	MALDI-TOF	k?
hmotnostní	hmotnostní	k2eAgInSc4d1
spektrometr	spektrometr	k1gInSc4
</s>
<s>
Matricí	matrice	k1gFnSc7
asistovaná	asistovaný	k2eAgFnSc1d1
laserová	laserový	k2eAgFnSc1d1
desorpce	desorpce	k1gFnSc1
<g/>
/	/	kIx~
<g/>
ionizace	ionizace	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
MALDI	MALDI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
ionizace	ionizace	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS
matrice	matrice	k1gFnSc1
absorbující	absorbující	k2eAgFnSc1d1
energii	energie	k1gFnSc4
laseru	laser	k1gInSc2
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
iontů	ion	k1gInPc2
z	z	k7c2
velkých	velký	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
za	za	k7c4
minimální	minimální	k2eAgFnSc2d1
fragmentace	fragmentace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
analýzu	analýza	k1gFnSc4
biomolekul	biomolekula	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
DNA	DNA	kA
<g/>
,	,	kIx,
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
peptidů	peptid	k1gInPc2
a	a	k8xC
sacharidů	sacharid	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
velkých	velký	k2eAgFnPc2d1
organických	organický	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
polymery	polymer	k1gInPc1
a	a	k8xC
dendrimery	dendrimer	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
jsou	být	k5eAaImIp3nP
nestabilní	stabilní	k2eNgInPc1d1
a	a	k8xC
při	při	k7c6
použití	použití	k1gNnSc6
jiných	jiný	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
ionizace	ionizace	k1gFnSc2
u	u	k7c2
nich	on	k3xPp3gFnPc2
dochází	docházet	k5eAaImIp3nP
k	k	k7c3
fragmentaci	fragmentace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobá	podobat	k5eAaImIp3nS
se	se	k3xPyFc4
elektrosprejové	elektrosprejový	k2eAgFnSc3d1
ionizaci	ionizace	k1gFnSc3
(	(	kIx(
<g/>
ESI	ESI	kA
<g/>
)	)	kIx)
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
obou	dva	k4xCgFnPc2
metod	metoda	k1gFnPc2
je	být	k5eAaImIp3nS
nízká	nízký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
fragmentace	fragmentace	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
způsobem	způsob	k1gInSc7
tvorby	tvorba	k1gFnSc2
iontů	ion	k1gInPc2
z	z	k7c2
velkých	velký	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
v	v	k7c6
plynné	plynný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
u	u	k7c2
MALDI	MALDI	kA
obvykle	obvykle	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
méně	málo	k6eAd2
iontů	ion	k1gInPc2
s	s	k7c7
vícenásobnými	vícenásobný	k2eAgInPc7d1
náboji	náboj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Provedení	provedení	k1gNnSc1
MALDI	MALDI	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgInPc2
kroků	krok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
se	se	k3xPyFc4
vzorek	vzorek	k1gInSc1
smíchá	smíchat	k5eAaPmIp3nS
s	s	k7c7
vhodnou	vhodný	k2eAgFnSc7d1
látkou	látka	k1gFnSc7
nebo	nebo	k8xC
směsí	směs	k1gFnSc7
tvořící	tvořící	k2eAgFnSc4d1
matrici	matrice	k1gFnSc4
a	a	k8xC
aplikuje	aplikovat	k5eAaBmIp3nS
na	na	k7c4
kovovou	kovový	k2eAgFnSc4d1
destičku	destička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
vzorek	vzorek	k1gInSc1
ozáří	ozářit	k5eAaPmIp3nS
pulzním	pulzní	k2eAgInSc7d1
laserem	laser	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
vyvolá	vyvolat	k5eAaPmIp3nS
ablaci	ablace	k1gFnSc3
a	a	k8xC
desorpci	desorpce	k1gFnSc3
vzorku	vzorek	k1gInSc2
a	a	k8xC
matrice	matrice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
molekuly	molekula	k1gFnPc1
analytu	analyt	k1gInSc2
ionizují	ionizovat	k5eAaBmIp3nP
protonací	protonací	k2eAgMnPc1d1
nebo	nebo	k8xC
deprotonací	deprotonací	k2eAgMnPc1d1
v	v	k7c6
proudu	proud	k1gInSc6
horkých	horký	k2eAgInPc2d1
ablatovaných	ablatovaný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
a	a	k8xC
analyzují	analyzovat	k5eAaImIp3nP
hmotnostním	hmotnostní	k2eAgInSc7d1
spektrometrem	spektrometr	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Označení	označení	k1gNnSc1
matricí	matrice	k1gFnPc2
asistovaná	asistovaný	k2eAgFnSc1d1
laserová	laserový	k2eAgFnSc1d1
desorpce	desorpce	k1gFnSc1
<g/>
/	/	kIx~
<g/>
ionizace	ionizace	k1gFnSc1
(	(	kIx(
<g/>
MALDI	MALDI	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
používat	používat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Franz	Franz	k1gMnSc1
Hillenkamp	Hillenkamp	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Karas	Karas	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
aminokyselina	aminokyselina	k1gFnSc1
alanin	alanina	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
snadněji	snadno	k6eAd2
ionizována	ionizován	k2eAgFnSc1d1
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
smíchána	smíchán	k2eAgFnSc1d1
s	s	k7c7
tryptofanem	tryptofan	k1gMnSc7
a	a	k8xC
ozářena	ozářen	k2eAgFnSc1d1
pulzním	pulzní	k2eAgInSc7d1
266	#num#	k4
<g/>
nm	nm	k?
laserem	laser	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tryptofan	Tryptofan	k1gInSc1
pohlcuje	pohlcovat	k5eAaImIp3nS
energii	energie	k1gFnSc4
laseru	laser	k1gInSc2
a	a	k8xC
usnadňuje	usnadňovat	k5eAaImIp3nS
ionizaci	ionizace	k1gFnSc4
neabsorbujícího	absorbující	k2eNgInSc2d1
alaninu	alanin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
lze	lze	k6eAd1
ionizovat	ionizovat	k5eAaBmF
peptidy	peptid	k1gInPc4
s	s	k7c7
molekulovou	molekulový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
až	až	k9
2843	#num#	k4
Da	Da	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
melitin	melitin	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
Kóiči	Kóič	k1gInSc6
Tanaka	Tanak	k1gMnSc4
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
spolupracovníky	spolupracovník	k1gMnPc7
použil	použít	k5eAaPmAgMnS
metodu	metoda	k1gFnSc4
„	„	k?
<g/>
ultratenkého	ultratenký	k2eAgInSc2d1
kovu	kov	k1gInSc2
a	a	k8xC
kapalné	kapalný	k2eAgFnSc2d1
matrice	matrice	k1gFnSc2
<g/>
“	“	k?
kombinující	kombinující	k2eAgInSc1d1
30	#num#	k4
<g/>
nm	nm	k?
částice	částice	k1gFnSc1
kobaltu	kobalt	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
glycerolu	glycerol	k1gInSc6
s	s	k7c7
337	#num#	k4
<g/>
nm	nm	k?
dusíkovým	dusíkový	k2eAgInSc7d1
laserem	laser	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Touto	tento	k3xDgFnSc7
metodou	metoda	k1gFnSc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ionizovány	ionizován	k2eAgInPc4d1
biomolekuly	biomolekul	k1gInPc4
s	s	k7c7
molekulovými	molekulový	k2eAgFnPc7d1
hmotnostmi	hmotnost	k1gFnPc7
až	až	k9
34	#num#	k4
472	#num#	k4
Da	Da	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
například	například	k6eAd1
karboxypeptidáza	karboxypeptidáza	k1gFnSc1
A.	A.	kA
Tanaka	Tanak	k1gMnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
stal	stát	k5eAaPmAgMnS
spoludržitelem	spoludržitel	k1gMnSc7
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
chemii	chemie	k1gFnSc4
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
že	že	k8xS
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vhodnou	vhodný	k2eAgFnSc7d1
kombinací	kombinace	k1gFnSc7
vlnové	vlnový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
laseru	laser	k1gInSc2
a	a	k8xC
složení	složení	k1gNnSc2
matrice	matrice	k1gFnSc2
lze	lze	k6eAd1
ionizovat	ionizovat	k5eAaBmF
bílkoviny	bílkovina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Karas	Karas	k1gMnSc1
a	a	k8xC
Hillenkamp	Hillenkamp	k1gMnSc1
následně	následně	k6eAd1
dokázali	dokázat	k5eAaPmAgMnP
ionizovat	ionizovat	k5eAaBmF
67	#num#	k4
<g/>
kDa	kDa	k?
bílkovinu	bílkovina	k1gFnSc4
albumin	albumin	k1gInSc1
pomocí	pomocí	k7c2
matrice	matrice	k1gFnSc2
z	z	k7c2
kyseliny	kyselina	k1gFnSc2
nikotinové	nikotinový	k2eAgFnSc2d1
a	a	k8xC
266	#num#	k4
<g/>
nm	nm	k?
laseru	laser	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalších	další	k2eAgInPc2d1
pokroků	pokrok	k1gInPc2
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
pomocí	pomocí	k7c2
355	#num#	k4
<g/>
nm	nm	k?
laseru	laser	k1gInSc2
a	a	k8xC
matric	matrice	k1gFnPc2
obsahujících	obsahující	k2eAgFnPc2d1
deriváty	derivát	k1gInPc7
kyseliny	kyselina	k1gFnSc2
skořicové	skořicová	k1gFnSc2
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
byly	být	k5eAaImAgFnP
kyselina	kyselina	k1gFnSc1
ferulová	ferulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
kyselina	kyselina	k1gFnSc1
kávová	kávový	k2eAgFnSc1d1
a	a	k8xC
kyselina	kyselina	k1gFnSc1
sinapová	sinapová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Dostupnost	dostupnost	k1gFnSc1
malých	malý	k2eAgFnPc2d1
a	a	k8xC
nepříliš	příliš	k6eNd1
nákladných	nákladný	k2eAgInPc2d1
dusíkových	dusíkový	k2eAgInPc2d1
laserů	laser	k1gInPc2
poskytujících	poskytující	k2eAgNnPc2d1
záření	záření	k1gNnPc2
s	s	k7c7
vlnovou	vlnový	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
337	#num#	k4
nm	nm	k?
a	a	k8xC
první	první	k4xOgInPc1
přístroje	přístroj	k1gInPc1
uvedené	uvedený	k2eAgInPc1d1
na	na	k7c4
trh	trh	k1gInSc4
začátkem	začátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
umožnily	umožnit	k5eAaPmAgFnP
větší	veliký	k2eAgNnSc4d2
využití	využití	k1gNnSc4
MALDI	MALDI	kA
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
používají	používat	k5eAaImIp3nP
organické	organický	k2eAgFnPc4d1
matrice	matrice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Matrice	matrice	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
matric	matrice	k1gFnPc2
pro	pro	k7c4
MALDI	MALDI	kA
</s>
<s>
SloučeninaOstatní	SloučeninaOstatní	k2eAgFnSc1d1
názvyRozpouštědloVlnová	názvyRozpouštědloVlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
(	(	kIx(
<g/>
nm	nm	k?
<g/>
)	)	kIx)
<g/>
Použití	použití	k1gNnSc1
</s>
<s>
kyselina	kyselina	k1gFnSc1
2,5	2,5	k4
<g/>
-dihydroxybenzoová	-dihydroxybenzoová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
DHB	DHB	kA
<g/>
,	,	kIx,
kyselina	kyselina	k1gFnSc1
gentisováacetonitril	gentisováacetonitril	k1gInSc1
<g/>
,	,	kIx,
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
methanol	methanol	k1gInSc1
<g/>
,	,	kIx,
aceton	aceton	k1gInSc1
<g/>
,	,	kIx,
chloroform	chloroform	k1gInSc1
<g/>
337	#num#	k4
<g/>
,	,	kIx,
355	#num#	k4
<g/>
,	,	kIx,
266	#num#	k4
<g/>
peptidy	peptid	k1gInPc1
<g/>
,	,	kIx,
nukleotidy	nukleotid	k1gInPc1
<g/>
,	,	kIx,
oligonukleotidy	oligonukleotid	k1gInPc1
<g/>
,	,	kIx,
oligosacharidy	oligosacharid	k1gInPc1
</s>
<s>
kyselina	kyselina	k1gFnSc1
3,5	3,5	k4
<g/>
-dimethoxy-	-dimethoxy-	k?
<g/>
4	#num#	k4
<g/>
-hydroxyskořicová	-hydroxyskořicová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kyselina	kyselina	k1gFnSc1
sinapováacetonitril	sinapováacetonitril	k1gInSc1
<g/>
,	,	kIx,
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
aceton	aceton	k1gInSc1
<g/>
,	,	kIx,
chloroform	chloroform	k1gInSc1
<g/>
337	#num#	k4
<g/>
,	,	kIx,
355	#num#	k4
<g/>
,	,	kIx,
266	#num#	k4
<g/>
peptidy	peptid	k1gInPc1
<g/>
,	,	kIx,
bílkoviny	bílkovina	k1gFnPc1
<g/>
,	,	kIx,
lipidy	lipid	k1gInPc1
</s>
<s>
kyselina	kyselina	k1gFnSc1
4	#num#	k4
<g/>
-hydroxy-	-hydroxy-	k?
<g/>
3	#num#	k4
<g/>
-methoxyskořicová	-methoxyskořicová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kyselina	kyselina	k1gFnSc1
ferulováacetonitril	ferulováacetonitril	k1gInSc1
<g/>
,	,	kIx,
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
propanol	propanol	k1gInSc1
<g/>
337	#num#	k4
<g/>
,	,	kIx,
355	#num#	k4
<g/>
,	,	kIx,
266	#num#	k4
<g/>
bílkoviny	bílkovina	k1gFnPc4
</s>
<s>
kyselina	kyselina	k1gFnSc1
α	α	k?
<g/>
4	#num#	k4
<g/>
-hydroxyskořicová	-hydroxyskořicová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
acetonitril	acetonitril	k1gInSc1
<g/>
,	,	kIx,
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
ethanol	ethanol	k1gInSc1
<g/>
,	,	kIx,
aceton	aceton	k1gInSc1
<g/>
337	#num#	k4
<g/>
,	,	kIx,
355	#num#	k4
<g/>
peptidy	peptid	k1gInPc1
<g/>
,	,	kIx,
lipidy	lipid	k1gInPc1
<g/>
,	,	kIx,
nukleotidy	nukleotid	k1gInPc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
pikolinová	pikolinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ethanol	ethanol	k1gInSc1
<g/>
266	#num#	k4
<g/>
oligonukleotidy	oligonukleotida	k1gFnSc2
</s>
<s>
kyselina	kyselina	k1gFnSc1
3	#num#	k4
<g/>
-hydroxypikolinová	-hydroxypikolinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ethanol	ethanol	k1gInSc1
<g/>
337	#num#	k4
<g/>
,	,	kIx,
355	#num#	k4
<g/>
oligonukleotidy	oligonukleotid	k1gInPc1
</s>
<s>
Matrice	matrice	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
krystalizované	krystalizovaný	k2eAgFnPc4d1
molekuly	molekula	k1gFnPc4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
kyselina	kyselina	k1gFnSc1
sinapová	sinapová	k1gFnSc1
<g/>
,	,	kIx,
alfa-kyano-	alfa-kyano-	k?
<g/>
4	#num#	k4
<g/>
-hydroxyskořicová	-hydroxyskořicový	k2eAgFnSc1d1
a	a	k8xC
2,5	2,5	k4
<g/>
-dihydroxybenzoová	-dihydroxybenzoová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Roztok	roztok	k1gInSc1
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
tří	tři	k4xCgFnPc2
uvedených	uvedený	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
připravuje	připravovat	k5eAaImIp3nS
z	z	k7c2
ultračisté	ultračistý	k2eAgFnSc2d1
vody	voda	k1gFnSc2
a	a	k8xC
organického	organický	k2eAgNnSc2d1
rozpouštědla	rozpouštědlo	k1gNnSc2
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
je	být	k5eAaImIp3nS
například	například	k6eAd1
acetonitril	acetonitril	k1gInSc1
nebo	nebo	k8xC
ethanol	ethanol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
roztoku	roztok	k1gInSc2
přidává	přidávat	k5eAaImIp3nS
zdroj	zdroj	k1gInSc1
[	[	kIx(
<g/>
M	M	kA
<g/>
+	+	kIx~
<g/>
H	H	kA
<g/>
]	]	kIx)
protiiontů	protiiont	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
kyselina	kyselina	k1gFnSc1
trifluoroctová	trifluoroctová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Příkladem	příklad	k1gInSc7
roztoku	roztok	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
20	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
ml	ml	kA
kyseliny	kyselina	k1gFnSc2
sinapové	sinapová	k1gFnSc2
ve	v	k7c6
směsi	směs	k1gFnSc6
acetonitrilu	acetonitril	k1gInSc2
<g/>
,	,	kIx,
vody	voda	k1gFnSc2
a	a	k8xC
kyseliny	kyselina	k1gFnSc2
trifluoroctové	trifluoroctový	k2eAgFnSc2d1
v	v	k7c6
poměru	poměr	k1gInSc6
50	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
:	:	kIx,
<g/>
0,1	0,1	k4
<g/>
.	.	kIx.
</s>
<s>
Znázornění	znázornění	k1gNnSc1
pozic	pozice	k1gFnPc2
substituce	substituce	k1gFnSc2
u	u	k7c2
kyseliny	kyselina	k1gFnSc2
skořicové	skořicový	k2eAgFnSc2d1
</s>
<s>
Hledání	hledání	k1gNnSc1
vhodných	vhodný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
matrice	matrice	k1gFnSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
pokusu	pokus	k1gInSc6
a	a	k8xC
omylu	omyl	k1gInSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
s	s	k7c7
uvážením	uvážení	k1gNnSc7
potřeby	potřeba	k1gFnSc2
určitých	určitý	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
mít	mít	k5eAaImF
nižší	nízký	k2eAgFnSc2d2
molekulové	molekulový	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
(	(	kIx(
<g/>
aby	aby	kYmCp3nP
se	se	k3xPyFc4
snadno	snadno	k6eAd1
odpařovaly	odpařovat	k5eAaImAgInP
<g/>
)	)	kIx)
ale	ale	k8xC
měly	mít	k5eAaImAgFnP
by	by	kYmCp3nP
být	být	k5eAaImF
dostatečně	dostatečně	k6eAd1
velké	velký	k2eAgNnSc1d1
(	(	kIx(
<g/>
a	a	k8xC
tedy	tedy	k9
mít	mít	k5eAaImF
nízký	nízký	k2eAgInSc4d1
tlak	tlak	k1gInSc4
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
nevypařovaly	vypařovat	k5eNaImAgFnP
během	během	k7c2
přípravy	příprava	k1gFnSc2
vzorku	vzorek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
kyseliny	kyselina	k1gFnPc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
deprotonizují	deprotonizovat	k5eAaImIp3nP,k5eAaPmIp3nP,k5eAaBmIp3nP
a	a	k8xC
tím	ten	k3xDgNnSc7
pomáhají	pomáhat	k5eAaImIp3nP
ionizovat	ionizovat	k5eAaBmF
analyt	analyt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
však	však	k9
popsány	popsat	k5eAaPmNgFnP
i	i	k8xC
zásadité	zásaditý	k2eAgFnPc1d1
matrice	matrice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Matrice	matrice	k1gFnPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
vykazovat	vykazovat	k5eAaImF
silnou	silný	k2eAgFnSc4d1
absorbci	absorbce	k1gMnPc7
v	v	k7c6
ultrafialové	ultrafialový	k2eAgFnSc6d1
nebo	nebo	k8xC
infračervené	infračervený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
aby	aby	kYmCp3nS
rychle	rychle	k6eAd1
a	a	k8xC
účinně	účinně	k6eAd1
zachycovaly	zachycovat	k5eAaImAgInP
laserové	laserový	k2eAgInPc1d1
paprsky	paprsek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
absorpce	absorpce	k1gFnPc1
se	se	k3xPyFc4
většinou	většinou	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
u	u	k7c2
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
molekuly	molekula	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
konjugované	konjugovaný	k2eAgInPc1d1
systémy	systém	k1gInPc1
dvojných	dvojný	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
má	mít	k5eAaImIp3nS
například	například	k6eAd1
kyselina	kyselina	k1gFnSc1
skořicová	skořicový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tyto	tento	k3xDgInPc4
systémy	systém	k1gInPc4
bývají	bývat	k5eAaImIp3nP
navázány	navázán	k2eAgFnPc4d1
polární	polární	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yIgFnPc3,k3yRgFnPc3,k3yQgFnPc3
lze	lze	k6eAd1
dané	daný	k2eAgFnPc1d1
látky	látka	k1gFnPc1
použít	použít	k5eAaPmF
ve	v	k7c6
vodných	vodný	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
chromoforové	chromoforový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Roztok	roztok	k1gInSc1
matrice	matrice	k1gFnSc2
se	se	k3xPyFc4
smíchá	smíchat	k5eAaPmIp3nS
s	s	k7c7
analytem	analyt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
s	s	k7c7
organickým	organický	k2eAgNnSc7d1
rozpouštědlem	rozpouštědlo	k1gNnSc7
umožní	umožnit	k5eAaPmIp3nS
rozpuštění	rozpuštění	k1gNnSc4
hydrofobním	hydrofobní	k2eAgNnSc7d1
i	i	k8xC
hydrofilním	hydrofilní	k2eAgNnSc7d1
<g/>
)	)	kIx)
molekulám	molekula	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravený	připravený	k2eAgInSc1d1
roztok	roztok	k1gInSc1
se	se	k3xPyFc4
aplikuje	aplikovat	k5eAaBmIp3nS
na	na	k7c6
MALDI	MALDI	kA
destičku	destička	k1gFnSc4
(	(	kIx(
<g/>
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
kovová	kovový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpouštědlo	rozpouštědlo	k1gNnSc1
se	se	k3xPyFc4
odpaří	odpařit	k5eAaPmIp3nS
a	a	k8xC
zbude	zbýt	k5eAaPmIp3nS
jen	jen	k9
rekrystalizovaná	rekrystalizovaný	k2eAgFnSc1d1
matrice	matrice	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
molekuly	molekula	k1gFnPc1
analytu	analyt	k1gInSc2
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
tedy	tedy	k9
ke	k	k7c3
okrystalizaci	okrystalizace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kokrystalizace	Kokrystalizace	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
důležitých	důležitý	k2eAgNnPc2d1
hledisek	hledisko	k1gNnPc2
při	při	k7c6
výběru	výběr	k1gInSc6
matrice	matrice	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
vhodná	vhodný	k2eAgFnSc1d1
matrice	matrice	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
získat	získat	k5eAaPmF
dobré	dobrý	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
zkoumaného	zkoumaný	k2eAgInSc2d1
analytu	analyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
analýze	analýza	k1gFnSc6
biologických	biologický	k2eAgInPc2d1
systémů	systém	k1gInPc2
anorganické	anorganický	k2eAgFnSc2d1
soli	sůl	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
také	také	k9
bývají	bývat	k5eAaImIp3nP
obsaženy	obsáhnout	k5eAaPmNgInP
v	v	k7c6
bílkovinných	bílkovinný	k2eAgInPc6d1
extraktech	extrakt	k1gInPc6
<g/>
,	,	kIx,
narušují	narušovat	k5eAaImIp3nP
ionizaci	ionizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstraněny	odstraněn	k2eAgFnPc1d1
mohou	moct	k5eAaImIp3nP
extrakcí	extrakce	k1gFnSc7
v	v	k7c6
pevné	pevný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
nebo	nebo	k8xC
vymytím	vymytí	k1gNnSc7
chladnou	chladný	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
tyto	tento	k3xDgInPc4
postupy	postup	k1gInPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
i	i	k9
k	k	k7c3
odstranění	odstranění	k1gNnSc3
ostatních	ostatní	k2eAgFnPc2d1
látek	látka	k1gFnPc2
ze	z	k7c2
vzorku	vzorek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směs	směs	k1gFnSc1
matrice	matrice	k1gFnSc2
a	a	k8xC
proteinu	protein	k1gInSc2
není	být	k5eNaImIp3nS
homogenní	homogenní	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
polaritě	polarita	k1gFnSc6
způsobují	způsobovat	k5eAaImIp3nP
oddělování	oddělování	k1gNnSc4
obou	dva	k4xCgFnPc2
složek	složka	k1gFnPc2
během	během	k7c2
kokrystalizace	kokrystalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Naftalen	naftalen	k1gInSc1
a	a	k8xC
jemu	on	k3xPp3gInSc3
podobné	podobný	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
rovně	roveň	k1gFnPc1
použity	použít	k5eAaPmNgFnP
k	k	k7c3
ionizaci	ionizace	k1gFnSc3
vzorku	vzorek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Matrici	matrice	k1gFnSc4
lze	lze	k6eAd1
použít	použít	k5eAaPmF
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
k	k	k7c3
ionizaci	ionizace	k1gFnSc3
docházelo	docházet	k5eAaImAgNnS
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ionizaci	ionizace	k1gFnSc3
se	se	k3xPyFc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
výše	vysoce	k6eAd2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
používají	používat	k5eAaImIp3nP
acidobazické	acidobazický	k2eAgFnPc1d1
reakce	reakce	k1gFnPc1
<g/>
,	,	kIx,
molekuly	molekula	k1gFnPc1
s	s	k7c7
konjugovanými	konjugovaný	k2eAgNnPc7d1
pí	pí	k1gNnPc7
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
například	například	k6eAd1
naftaleny	naftalen	k1gInPc4
<g/>
,	,	kIx,
také	také	k9
mohou	moct	k5eAaImIp3nP
fungovat	fungovat	k5eAaImF
jako	jako	k8xC,k8xS
akceptory	akceptor	k1gInPc4
elektronů	elektron	k1gInPc2
a	a	k8xC
tak	tak	k6eAd1
být	být	k5eAaImF
použity	použít	k5eAaPmNgFnP
jako	jako	k9
matrice	matrice	k1gFnPc1
pro	pro	k7c4
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
je	být	k5eAaImIp3nS
výhodná	výhodný	k2eAgFnSc1d1
při	při	k7c6
analýze	analýza	k1gFnSc6
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
také	také	k9
obsahují	obsahovat	k5eAaImIp3nP
konjugované	konjugovaný	k2eAgInPc4d1
pí	pí	kA
systémy	systém	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Nejvíce	nejvíce	k6eAd1,k6eAd3
se	se	k3xPyFc4
takovéto	takovýto	k3xDgFnPc1
matrice	matrice	k1gFnPc1
používají	používat	k5eAaImIp3nP
při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
porfyrinových	porfyrinův	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
chlorofyly	chlorofyl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
lepší	dobrý	k2eAgFnPc1d2
ionizační	ionizační	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
a	a	k8xC
neobjevují	objevovat	k5eNaImIp3nP
se	se	k3xPyFc4
u	u	k7c2
nich	on	k3xPp3gFnPc2
nevhodné	vhodný	k2eNgInPc4d1
způsoby	způsob	k1gInPc4
fragmentace	fragmentace	k1gFnSc2
nebo	nebo	k8xC
dokonce	dokonce	k9
ztráty	ztráta	k1gFnPc4
postranních	postranní	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Konjugované	konjugovaný	k2eAgInPc1d1
porfyriny	porfyrin	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
samy	sám	k3xTgInPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
jako	jako	k9
matrice	matrice	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
používat	používat	k5eAaImF
zvláštní	zvláštní	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
na	na	k7c4
přípravu	příprava	k1gFnSc4
matric	matrice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přístroje	přístroj	k1gInPc1
</s>
<s>
Znázornění	znázornění	k1gNnSc1
přístroje	přístroj	k1gInSc2
na	na	k7c6
MALDI-TOF	MALDI-TOF	k1gFnSc6
<g/>
;	;	kIx,
ionizovaná	ionizovaný	k2eAgFnSc1d1
matrice	matrice	k1gFnSc1
vzorku	vzorek	k1gInSc2
se	se	k3xPyFc4
odděluje	oddělovat	k5eAaImIp3nS
od	od	k7c2
povrchu	povrch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzorek	vzorek	k1gInSc1
putuje	putovat	k5eAaImIp3nS
do	do	k7c2
hmotnostního	hmotnostní	k2eAgInSc2d1
analyzátoru	analyzátor	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
analyzován	analyzovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
variant	varianta	k1gFnPc2
MALDI	MALDI	kA
přístrojů	přístroj	k1gInPc2
<g/>
;	;	kIx,
podobná	podobný	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použita	použít	k5eAaPmNgFnS
k	k	k7c3
odlišným	odlišný	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotnostní	hmotnostní	k2eAgFnSc1d1
spektrometrie	spektrometrie	k1gFnSc1
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
do	do	k7c2
podoby	podoba	k1gFnSc2
hmotnostní	hmotnostní	k2eAgFnSc2d1
spektrometrie	spektrometrie	k1gFnSc2
s	s	k7c7
velmi	velmi	k6eAd1
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
FT-ICR	FT-ICR	k1gFnSc4
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k4c1
MALDI-MS	MALDI-MS	k1gFnPc2
lze	lze	k6eAd1
zakoupit	zakoupit	k5eAaPmF
s	s	k7c7
výměnným	výměnný	k2eAgInSc7d1
ionizátorem	ionizátor	k1gInSc7
(	(	kIx(
<g/>
například	například	k6eAd1
elektrosprejovým	elektrosprejový	k2eAgMnPc3d1
<g/>
,	,	kIx,
MALDI	MALDI	kA
nebo	nebo	k8xC
APCI	APCI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
i	i	k9
měkké	měkký	k2eAgFnPc1d1
ionizační	ionizační	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Laser	laser	k1gInSc1
</s>
<s>
MALDI	MALDI	kA
metody	metoda	k1gFnPc1
obvykle	obvykle	k6eAd1
zahrnují	zahrnovat	k5eAaImIp3nP
použití	použití	k1gNnSc4
ultrafialových	ultrafialový	k2eAgInPc2d1
laserů	laser	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
dusíkových	dusíkový	k2eAgInPc2d1
(	(	kIx(
<g/>
337	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
a	a	k8xC
Nd	Nd	k1gMnSc1
<g/>
:	:	kIx,
<g/>
YAG	YAG	kA
laserů	laser	k1gInPc2
(	(	kIx(
<g/>
355	#num#	k4
nebo	nebo	k8xC
266	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Infračervené	infračervený	k2eAgInPc1d1
lasery	laser	k1gInPc1
používané	používaný	k2eAgInPc1d1
s	s	k7c7
MALDI	MALDI	kA
mívají	mívat	k5eAaImIp3nP
vlnové	vlnový	k2eAgFnPc4d1
délky	délka	k1gFnPc4
2,94	2,94	k4
μ	μ	k1gFnPc2
(	(	kIx(
<g/>
Er	Er	k1gMnSc1
<g/>
:	:	kIx,
<g/>
YAG	YAG	kA
laser	laser	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
10,6	10,6	k4
μ	μ	k1gFnPc2
CO2	CO2	k1gFnPc2
laser	laser	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infračervené	infračervený	k2eAgInPc1d1
lasery	laser	k1gInPc1
nejsou	být	k5eNaImIp3nP
běžné	běžný	k2eAgInPc1d1
<g/>
;	;	kIx,
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
díky	díky	k7c3
nižší	nízký	k2eAgFnSc3d2
míře	míra	k1gFnSc3
fragmentace	fragmentace	k1gFnSc2
při	při	k7c6
ionizaci	ionizace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Infračervená	infračervený	k2eAgFnSc1d1
MALDI	MALDI	kA
se	se	k3xPyFc4
rovněž	rovněž	k9
vyznačuje	vyznačovat	k5eAaImIp3nS
lepším	dobrý	k2eAgNnSc7d2
odstraňováním	odstraňování	k1gNnSc7
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
výhodné	výhodný	k2eAgNnSc1d1
u	u	k7c2
biologických	biologický	k2eAgInPc2d1
vzorků	vzorek	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
menším	malý	k2eAgNnSc7d2
nízkohmotnostním	nízkohmotnostní	k2eAgNnSc7d1
rušením	rušení	k1gNnSc7
a	a	k8xC
kompatibilitou	kompatibilita	k1gFnSc7
s	s	k7c7
bezmatricovými	bezmatricová	k1gFnPc7
laserově	laserově	k6eAd1
desorpčními	desorpční	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
hmotnostní	hmotnostní	k2eAgFnSc2d1
spektrometrie	spektrometrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Spektrometr	spektrometr	k1gInSc1
</s>
<s>
Destička	destička	k1gFnSc1
na	na	k7c4
aplikaci	aplikace	k1gFnSc4
vzorku	vzorek	k1gInSc2
u	u	k7c2
MALDI	MALDI	kA
hmotnostní	hmotnostní	k2eAgFnSc2d1
spektrometrie	spektrometrie	k1gFnSc2
</s>
<s>
Na	na	k7c6
MALDI	MALDI	kA
nejčastěji	často	k6eAd3
napojovaným	napojovaný	k2eAgInSc7d1
druhem	druh	k1gInSc7
hmotnostního	hmotnostní	k2eAgInSc2d1
spektrometru	spektrometr	k1gInSc2
je	být	k5eAaImIp3nS
hmotnostní	hmotnostní	k2eAgInSc1d1
spektrometr	spektrometr	k1gInSc1
doby	doba	k1gFnSc2
letu	let	k1gInSc2
(	(	kIx(
<g/>
TOF-MS	TOF-MS	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
použít	použít	k5eAaPmF
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
rozmezí	rozmezí	k1gNnSc6
molekulových	molekulový	k2eAgFnPc2d1
hmotností	hmotnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TOF	TOF	kA
se	se	k3xPyFc4
nejlépe	dobře	k6eAd3
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
MALDI	MALDI	kA
<g/>
,	,	kIx,
protože	protože	k8xS
pulzní	pulzní	k2eAgInSc1d1
laser	laser	k1gInSc1
vypouští	vypouštět	k5eAaImIp3nS
jednotlivé	jednotlivý	k2eAgFnPc4d1
dávky	dávka	k1gFnPc4
záření	záření	k1gNnSc2
namísto	namísto	k7c2
souvislého	souvislý	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI-TOF	MALDI-TOF	k1gFnSc1
přístroje	přístroj	k1gInSc2
často	často	k6eAd1
mají	mít	k5eAaImIp3nP
reflektron	reflektron	k1gInSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
iontové	iontový	k2eAgNnSc1d1
zrcadlo	zrcadlo	k1gNnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
odráží	odrážet	k5eAaImIp3nS
ionty	ion	k1gInPc4
pomocí	pomocí	k7c2
elektrického	elektrický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
prodlužuje	prodlužovat	k5eAaImIp3nS
dráha	dráha	k1gFnSc1
iontů	ion	k1gInPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
rozdíl	rozdíl	k1gInSc1
časů	čas	k1gInPc2
letu	let	k1gInSc2
u	u	k7c2
iontů	ion	k1gInPc2
s	s	k7c7
rozdílnými	rozdílný	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
m	m	kA
<g/>
/	/	kIx~
<g/>
z	z	k7c2
a	a	k8xC
dochází	docházet	k5eAaImIp3nP
i	i	k9
k	k	k7c3
vylepšení	vylepšení	k1gNnSc3
rozlišení	rozlišení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc4d1
reflektronové	reflektronový	k2eAgInPc4d1
TOF	TOF	kA
přístroje	přístroj	k1gInPc4
dosahují	dosahovat	k5eAaImIp3nP
rozlišení	rozlišený	k2eAgMnPc1d1
m	m	kA
<g/>
/	/	kIx~
<g/>
Δ	Δ	k6eAd1
50	#num#	k4
000	#num#	k4
FWHM	FWHM	kA
a	a	k8xC
někdy	někdy	k6eAd1
i	i	k9
vyššího	vysoký	k2eAgMnSc4d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MALDI	MALDI	kA
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
jako	jako	k8xS,k8xC
zdroj	zdroj	k1gInSc1
iontů	ion	k1gInPc2
pro	pro	k7c4
IMS-TOF	IMS-TOF	k1gFnSc4
MS	MS	kA
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
fosforylovaných	fosforylovaný	k2eAgInPc2d1
a	a	k8xC
nefosforylovaných	fosforylovaný	k2eNgInPc2d1
peptidů	peptid	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MALDI-FT-ICR	MALDI-FT-ICR	k?
MS	MS	kA
je	být	k5eAaImIp3nS
také	také	k9
vhodnou	vhodný	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
vysoké	vysoký	k2eAgNnSc1d1
rozlišení	rozlišení	k1gNnSc1
MALDI-MS	MALDI-MS	k1gFnSc2
analýz	analýza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MALDI	MALDI	kA
za	za	k7c2
atmosférického	atmosférický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
</s>
<s>
Matricí	matrice	k1gFnSc7
asistovaná	asistovaný	k2eAgFnSc1d1
laserová	laserový	k2eAgFnSc1d1
desorpce	desorpce	k1gFnSc1
<g/>
/	/	kIx~
<g/>
ionizace	ionizace	k1gFnSc1
za	za	k7c2
atmosférického	atmosférický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
(	(	kIx(
<g/>
AP-MALDI	AP-MALDI	k1gFnSc1
<g/>
)	)	kIx)
ke	k	k7c3
metoda	metoda	k1gFnSc1
ionizace	ionizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
protipólem	protipól	k1gInSc7
vakuové	vakuový	k2eAgFnSc2d1
MALDI	MALDI	kA
fungujícím	fungující	k2eAgMnSc6d1
za	za	k7c2
atmosférického	atmosférický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
mezi	mezi	k7c7
vakuovou	vakuový	k2eAgFnSc7d1
a	a	k8xC
AP-MALDI	AP-MALDI	k1gFnSc7
je	být	k5eAaImIp3nS
tlak	tlak	k1gInSc1
v	v	k7c6
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nS
ionty	ion	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vakuové	vakuový	k2eAgFnSc6d1
MALDI	MALDI	kA
ionty	ion	k1gInPc7
vznikají	vznikat	k5eAaImIp3nP
za	za	k7c2
tlaků	tlak	k1gInPc2
kolem	kolem	k7c2
1	#num#	k4
Pa	Pa	kA
i	i	k9
nižších	nízký	k2eAgMnPc2d2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
AP-MALDI	AP-MALDI	k1gFnSc2
za	za	k7c2
atmosférického	atmosférický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
měl	mít	k5eAaImAgMnS
tento	tento	k3xDgInSc4
postup	postup	k1gInSc4
nevýhodu	nevýhoda	k1gFnSc4
oproti	oproti	k7c3
vakuové	vakuový	k2eAgFnPc4d1
MALDI	MALDI	kA
v	v	k7c6
podobě	podoba	k1gFnSc6
nízké	nízký	k2eAgFnSc3d1
citlivosti	citlivost	k1gFnSc3
<g/>
;	;	kIx,
ionty	ion	k1gInPc4
ovšem	ovšem	k9
lze	lze	k6eAd1
přemístit	přemístit	k5eAaPmF
do	do	k7c2
hmotnostního	hmotnostní	k2eAgInSc2d1
spektrometru	spektrometr	k1gInSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
popsány	popsat	k5eAaPmNgFnP
meze	mez	k1gFnPc1
detekce	detekce	k1gFnSc2
v	v	k7c6
řádu	řád	k1gInSc6
attomolů	attomol	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
AP-MALDI	AP-MALDI	k1gFnSc2
MS	MS	kA
má	mít	k5eAaImIp3nS
využití	využití	k1gNnSc1
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
od	od	k7c2
proteomiky	proteomika	k1gFnSc2
přes	přes	k7c4
analýzu	analýza	k1gFnSc4
DNA	DNA	kA
<g/>
,	,	kIx,
RNA	RNA	kA
<g/>
,	,	kIx,
PNA	pnout	k5eAaImSgMnS
<g/>
,	,	kIx,
lipidů	lipid	k1gInPc2
<g/>
,	,	kIx,
oligosacharidů	oligosacharid	k1gInPc2
<g/>
,	,	kIx,
fosfopeptidů	fosfopeptid	k1gInPc2
<g/>
,	,	kIx,
bakterií	bakterie	k1gFnPc2
<g/>
,	,	kIx,
malých	malý	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
a	a	k8xC
syntetických	syntetický	k2eAgInPc2d1
polymerů	polymer	k1gInPc2
k	k	k7c3
vývoji	vývoj	k1gInSc3
léčiv	léčivo	k1gNnPc2
<g/>
;	;	kIx,
podobné	podobný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
nacházejí	nacházet	k5eAaImIp3nP
také	také	k9
vakuové	vakuový	k2eAgInPc1d1
MALDI	MALDI	kA
přístroje	přístroj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AP-MALDI	AP-MALDI	k1gMnSc1
iontový	iontový	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
lze	lze	k6eAd1
lehce	lehko	k6eAd1
spojit	spojit	k5eAaPmF
s	s	k7c7
hmotnostní	hmotnostní	k2eAgFnSc7d1
spektrometrií	spektrometrie	k1gFnSc7
iontové	iontový	k2eAgFnSc2d1
pasti	past	k1gFnSc2
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
ESI	ESI	kA
<g/>
.	.	kIx.
</s>
<s>
Aerosolová	aerosolový	k2eAgFnSc1d1
MALDI	MALDI	kA
</s>
<s>
Při	při	k7c6
aerosolové	aerosolový	k2eAgFnSc6d1
hmotnostní	hmotnostní	k2eAgFnSc6d1
spektrometrii	spektrometrie	k1gFnSc6
laserové	laserový	k2eAgInPc1d1
paprsky	paprsek	k1gInPc1
ionizují	ionizovat	k5eAaBmIp3nP
jednotlivé	jednotlivý	k2eAgFnPc1d1
kapky	kapka	k1gFnPc1
vzorku	vzorek	k1gInSc2
<g/>
;	;	kIx,
odpovídající	odpovídající	k2eAgInPc1d1
přístroje	přístroj	k1gInPc1
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
jednočásticové	jednočásticový	k2eAgInPc1d1
hmotnostní	hmotnostní	k2eAgInPc1d1
spektrometry	spektrometr	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
vzorku	vzorek	k1gInSc2
lze	lze	k6eAd1
před	před	k7c7
aerosolizací	aerosolizace	k1gFnSc7
přidat	přidat	k5eAaPmF
MALDI	MALDI	kA
matrici	matrice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1
ionizace	ionizace	k1gFnSc2
</s>
<s>
Laserové	laserový	k2eAgInPc1d1
paprsky	paprsek	k1gInPc1
dopadají	dopadat	k5eAaImIp3nP
na	na	k7c4
krystaly	krystal	k1gInPc4
matrice	matrice	k1gFnSc2
v	v	k7c6
kapce	kapka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrice	matrice	k1gFnSc1
pohlcuje	pohlcovat	k5eAaImIp3nS
energii	energie	k1gFnSc4
laseru	laser	k1gInSc2
a	a	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
desorpci	desorpce	k1gFnSc3
a	a	k8xC
ionizaci	ionizace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořená	vytvořený	k2eAgFnSc1d1
mlha	mlha	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
neutrální	neutrální	k2eAgFnSc1d1
<g/>
,	,	kIx,
ionizované	ionizovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
protonované	protonovaný	k2eAgFnPc1d1
i	i	k8xC
deprotonované	deprotonovaný	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
matrice	matrice	k1gFnSc2
a	a	k8xC
nanokapky	nanokapka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
částice	částice	k1gFnPc1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nP
na	na	k7c4
ionizaci	ionizace	k1gFnSc4
analytu	analyt	k1gInSc2
<g/>
,	,	kIx,
přesná	přesný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
mechanismu	mechanismus	k1gInSc2
však	však	k9
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
matrice	matrice	k1gFnSc1
předává	předávat	k5eAaImIp3nS
protony	proton	k1gInPc4
molekulám	molekula	k1gFnPc3
analytu	analyt	k1gInSc3
(	(	kIx(
<g/>
například	například	k6eAd1
bílkoviny	bílkovina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
mu	on	k3xPp3gMnSc3
dodává	dodávat	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc1d1
náboj	náboj	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Ionty	ion	k1gInPc1
pozorované	pozorovaný	k2eAgInPc1d1
po	po	k7c6
tomto	tento	k3xDgInSc6
procesu	proces	k1gInSc6
jsou	být	k5eAaImIp3nP
původní	původní	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
[	[	kIx(
<g/>
M	M	kA
<g/>
]	]	kIx)
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgMnPc2,k3yRgMnPc2,k3yIgMnPc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odštěpení	odštěpení	k1gNnSc3
nebo	nebo	k8xC
zachycení	zachycení	k1gNnSc3
iontu	ion	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
umožňuje	umožňovat	k5eAaImIp3nS
tvorbu	tvorba	k1gFnSc4
iontů	ion	k1gInPc2
s	s	k7c7
jednoduchými	jednoduchý	k2eAgInPc7d1
i	i	k8xC
vícenásobnými	vícenásobný	k2eAgInPc7d1
náboji	náboj	k1gInPc7
(	(	kIx(
<g/>
[	[	kIx(
<g/>
M	M	kA
<g/>
+	+	kIx~
<g/>
nH	nH	k?
<g/>
]	]	kIx)
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
vlastnostech	vlastnost	k1gFnPc6
matrice	matrice	k1gFnSc2
<g/>
,	,	kIx,
vydatnosti	vydatnost	k1gFnSc2
laseru	laser	k1gInSc2
a	a	k8xC
použitém	použitý	k2eAgNnSc6d1
napětí	napětí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
přenosu	přenos	k1gInSc2
protonů	proton	k1gInPc2
v	v	k7c6
plynné	plynný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
zavedený	zavedený	k2eAgInSc4d1
jako	jako	k8xC,k8xS
fyzikálně-chemický	fyzikálně-chemický	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
primární	primární	k2eAgMnSc1d1
i	i	k8xC
sekundární	sekundární	k2eAgMnSc1d1
ionizační	ionizační	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Primární	primární	k2eAgInPc1d1
procesy	proces	k1gInPc1
spočívají	spočívat	k5eAaImIp3nP
v	v	k7c6
oddělení	oddělení	k1gNnSc6
náboje	náboj	k1gInSc2
absorpcí	absorpce	k1gFnPc2
fotonů	foton	k1gInPc2
v	v	k7c6
matrici	matrice	k1gFnSc6
a	a	k8xC
uvolněném	uvolněný	k2eAgInSc6d1
energie	energie	k1gFnSc2
za	za	k7c2
vzniku	vznik	k1gInSc2
matricových	matricový	k2eAgInPc2d1
iontových	iontový	k2eAgInPc2d1
párů	pár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primární	primární	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
iontů	ion	k1gInPc2
probíhá	probíhat	k5eAaImIp3nS
skrz	skrz	k7c4
absorbování	absorbování	k1gNnSc4
ultrafialových	ultrafialový	k2eAgInPc2d1
fotonů	foton	k1gInPc2
a	a	k8xC
vytváření	vytváření	k1gNnSc2
excitovaných	excitovaný	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
podle	podle	k7c2
těchto	tento	k3xDgFnPc2
rovnic	rovnice	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
S0	S0	k4
+	+	kIx~
hν	hν	k?
→	→	k?
S1	S1	k1gFnSc2
</s>
<s>
S1	S1	k4
+	+	kIx~
S1	S1	k1gMnSc1
→	→	k?
S0	S0	k1gMnSc1
+	+	kIx~
Sn	Sn	k1gMnSc1
</s>
<s>
S1	S1	k4
+	+	kIx~
Sn	Sn	k1gFnSc1
→	→	k?
M	M	kA
<g/>
+	+	kIx~
+	+	kIx~
M	M	kA
<g/>
−	−	k?
</s>
<s>
kde	kde	k6eAd1
S0	S0	k1gFnSc1
je	být	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
,	,	kIx,
S1	S1	k1gFnSc4
první	první	k4xOgInSc1
excitovaný	excitovaný	k2eAgInSc1d1
stav	stav	k1gInSc1
a	a	k8xC
Sn	Sn	k1gFnSc2
vyšší	vysoký	k2eAgFnSc2d2
excitovaný	excitovaný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Ionty	ion	k1gInPc1
mohou	moct	k5eAaImIp3nP
vznikat	vznikat	k5eAaImF
přesunem	přesun	k1gInSc7
protonů	proton	k1gInPc2
nebo	nebo	k8xC
elektronů	elektron	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
vyjádřeno	vyjádřit	k5eAaPmNgNnS
symboly	symbol	k1gInPc7
M	M	kA
<g/>
+	+	kIx~
a	a	k8xC
M	M	kA
<g/>
−	−	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
sekundárním	sekundární	k2eAgInPc3d1
dějům	děj	k1gInPc3
patří	patřit	k5eAaImIp3nS
reakce	reakce	k1gFnPc4
iontů	ion	k1gInPc2
a	a	k8xC
molekul	molekula	k1gFnPc2
za	za	k7c2
tvorby	tvorba	k1gFnSc2
iontů	ion	k1gInPc2
analytu	analyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
tepelného	tepelný	k2eAgInSc2d1
modelu	model	k1gInSc2
vysoké	vysoký	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
vyvolávají	vyvolávat	k5eAaImIp3nP
přenos	přenos	k1gInSc4
protonů	proton	k1gInPc2
mezi	mezi	k7c7
matricí	matrice	k1gFnSc7
a	a	k8xC
analytem	analyt	k1gInSc7
v	v	k7c6
kapalné	kapalný	k2eAgFnSc6d1
matrici	matrice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Důležitým	důležitý	k2eAgInSc7d1
parametrem	parametr	k1gInSc7
ověřujícím	ověřující	k2eAgInSc7d1
správnost	správnost	k1gFnSc4
modelu	model	k1gInSc2
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
množství	množství	k1gNnSc2
iontů	ion	k1gInPc2
a	a	k8xC
neutrálních	neutrální	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
chyba	chyba	k1gFnSc1
v	v	k7c4
určení	určení	k1gNnSc4
tohoto	tento	k3xDgInSc2
poměru	poměr	k1gInSc2
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
nesprávnému	správný	k2eNgNnSc3d1
určení	určení	k1gNnSc3
mechanismu	mechanismus	k1gInSc2
ionizace	ionizace	k1gFnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Model	model	k1gInSc1
předpovídá	předpovídat	k5eAaImIp3nS
nárůst	nárůst	k1gInSc4
iontové	iontový	k2eAgFnSc2d1
intenzity	intenzita	k1gFnSc2
s	s	k7c7
koncentrací	koncentrace	k1gFnSc7
<g/>
,	,	kIx,
protonovou	protonový	k2eAgFnSc7d1
afinitou	afinita	k1gFnSc7
analytů	analyt	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
frekvence	frekvence	k1gFnSc1
laserových	laserový	k2eAgInPc2d1
záblesků	záblesk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
tohoto	tento	k3xDgInSc2
modelu	model	k1gInSc2
se	se	k3xPyFc4
adukty	adukt	k1gInPc7
kovových	kovový	k2eAgInPc2d1
iontů	ion	k1gInPc2
(	(	kIx(
<g/>
například	například	k6eAd1
[	[	kIx(
<g/>
M	M	kA
<g/>
+	+	kIx~
<g/>
Na	na	k7c4
<g/>
]	]	kIx)
<g/>
+	+	kIx~
nebo	nebo	k8xC
[	[	kIx(
<g/>
M	M	kA
<g/>
+	+	kIx~
<g/>
K	k	k7c3
<g/>
]	]	kIx)
<g/>
+	+	kIx~
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nP
převážně	převážně	k6eAd1
tepelně	tepelně	k6eAd1
podněcovaným	podněcovaný	k2eAgNnSc7d1
rozpouštěním	rozpouštění	k1gNnSc7
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
matricí	matrice	k1gFnPc2
asistované	asistovaný	k2eAgFnSc3d1
ionizaci	ionizace	k1gFnSc3
(	(	kIx(
<g/>
MAI	MAI	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
matrice	matrice	k1gFnSc2
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
MALDI	MALDI	kA
<g/>
,	,	kIx,
ovšem	ovšem	k9
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
iontů	ion	k1gInPc2
analytu	analyt	k1gInSc2
není	být	k5eNaImIp3nS
potřeba	potřeba	k6eAd1
laserová	laserový	k2eAgFnSc1d1
ablace	ablace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Samotným	samotný	k2eAgNnSc7d1
vystavením	vystavení	k1gNnSc7
matrice	matrice	k1gFnSc2
a	a	k8xC
analytu	analyt	k1gInSc2
vakuovému	vakuový	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
hmotnostního	hmotnostní	k2eAgInSc2d1
spektrometru	spektrometr	k1gInSc2
vznikají	vznikat	k5eAaImIp3nP
ionty	ion	k1gInPc1
téměř	téměř	k6eAd1
stejné	stejný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
při	při	k7c6
elektrosprejové	elektrosprejový	k2eAgFnSc6d1
ionizaci	ionizace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Pravděpodobně	pravděpodobně	k6eAd1
existují	existovat	k5eAaImIp3nP
mechanistické	mechanistický	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
mezi	mezi	k7c7
tímto	tento	k3xDgInSc7
procesem	proces	k1gInSc7
a	a	k8xC
MALDI	MALDI	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výtěžnost	výtěžnost	k1gFnSc1
iontů	ion	k1gInPc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
10	#num#	k4
<g/>
−	−	k?
<g/>
4	#num#	k4
a	a	k8xC
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
při	při	k7c6
některých	některý	k3yIgInPc6
experimentech	experiment	k1gInPc6
byly	být	k5eAaImAgInP
zjištěny	zjistit	k5eAaPmNgInP
i	i	k9
nižší	nízký	k2eAgFnPc1d2
hodnoty	hodnota	k1gFnPc1
kolem	kolem	k7c2
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
již	již	k6eAd1
předtím	předtím	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
byly	být	k5eAaImAgInP
provedeny	proveden	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
o	o	k7c6
provedení	provedení	k1gNnSc6
dodatečné	dodatečný	k2eAgFnSc2d1
ionizace	ionizace	k1gFnSc2
pomocí	pomocí	k7c2
druhého	druhý	k4xOgInSc2
laseru	laser	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
těchto	tento	k3xDgInPc2
pokusů	pokus	k1gInPc2
nebyla	být	k5eNaImAgFnS
příliš	příliš	k6eAd1
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
lze	lze	k6eAd1
vysvětlit	vysvětlit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
byly	být	k5eAaImAgInP
použity	použít	k5eAaPmNgInP
axiální	axiální	k2eAgInPc1d1
TOF	TOF	kA
přístroje	přístroj	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgNnPc6,k3yQgNnPc6,k3yRgNnPc6
se	se	k3xPyFc4
tlak	tlak	k1gInSc1
ve	v	k7c6
zdroji	zdroj	k1gInSc6
iontů	ion	k1gInPc2
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
10	#num#	k4
<g/>
−	−	k?
<g/>
5	#num#	k4
až	až	k9
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
prudké	prudký	k2eAgFnSc3d1
expanzi	expanze	k1gFnSc3
a	a	k8xC
vzestupu	vzestup	k1gInSc3
rychlosti	rychlost	k1gFnSc2
částic	částice	k1gFnPc2
až	až	k9
k	k	k7c3
1000	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchu	úspěch	k1gInSc3
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
za	za	k7c4
použití	použití	k1gNnSc4
pozměněného	pozměněný	k2eAgNnSc2d1
MALDI	MALDI	kA
iontového	iontový	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
s	s	k7c7
využitím	využití	k1gNnSc7
vyššího	vysoký	k2eAgInSc2d2
tlaku	tlak	k1gInSc2
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
300	#num#	k4
Pa	Pa	kA
<g/>
)	)	kIx)
spojeného	spojený	k2eAgNnSc2d1
s	s	k7c7
ortogonálním	ortogonální	k2eAgNnSc7d1
TOF	TOF	kA
hmotnostním	hmotnostní	k2eAgInSc7d1
spektrometrem	spektrometr	k1gInSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
laditelný	laditelný	k2eAgInSc1d1
laser	laser	k1gInSc1
s	s	k7c7
vlnovými	vlnový	k2eAgFnPc7d1
délkami	délka	k1gFnPc7
od	od	k7c2
260	#num#	k4
do	do	k7c2
280	#num#	k4
nm	nm	k?
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
účinnost	účinnost	k1gFnSc1
tvorby	tvorba	k1gFnSc2
iontů	ion	k1gInPc2
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
zvýšila	zvýšit	k5eAaPmAgFnS
o	o	k7c4
tři	tři	k4xCgInPc4
řády	řád	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
použit	použít	k5eAaPmNgInS
i	i	k9
u	u	k7c2
ostatních	ostatní	k2eAgInPc2d1
druhů	druh	k1gInPc2
hmotnostních	hmotnostní	k2eAgInPc2d1
spektrometrů	spektrometr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Biochemie	biochemie	k1gFnSc1
</s>
<s>
V	v	k7c6
proteomice	proteomika	k1gFnSc6
se	se	k3xPyFc4
MALDI	MALDI	kA
používá	používat	k5eAaImIp3nS
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
bílkovin	bílkovina	k1gFnPc2
izolovaných	izolovaný	k2eAgMnPc2d1
pomocí	pomocí	k7c2
gelové	gelový	k2eAgFnSc2d1
elektroforézy	elektroforéza	k1gFnSc2
<g/>
:	:	kIx,
SDS-PAGE	SDS-PAGE	k1gFnSc2
<g/>
,	,	kIx,
afinitní	afinitní	k2eAgFnSc2d1
chromatografie	chromatografie	k1gFnSc2
<g/>
,	,	kIx,
izotopového	izotopový	k2eAgNnSc2d1
značkování	značkování	k1gNnSc2
bílkovin	bílkovina	k1gFnPc2
nebo	nebo	k8xC
dvourozměrné	dvourozměrný	k2eAgFnSc2d1
elektroforézy	elektroforéza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
TOF	TOF	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
k	k	k7c3
určení	určení	k1gNnSc3
pořadí	pořadí	k1gNnSc2
aminokyselin	aminokyselina	k1gFnPc2
v	v	k7c6
peptidech	peptid	k1gInPc6
pomocí	pomocí	k7c2
vysokoenergetických	vysokoenergetický	k2eAgFnPc2d1
disociací	disociace	k1gFnPc2
vyvolávaných	vyvolávaný	k2eAgFnPc2d1
srážkami	srážka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Odštěpení	odštěpení	k1gNnSc1
sialových	sialový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
lze	lze	k6eAd1
detekovat	detekovat	k5eAaImF
při	při	k7c6
použití	použití	k1gNnSc6
kyseliny	kyselina	k1gFnSc2
gentisové	gentisový	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
matrice	matrice	k1gFnSc2
při	při	k7c6
MALDI-MS	MALDI-MS	k1gFnSc6
analýze	analýza	k1gFnSc6
glykosylovaných	glykosylovaný	k2eAgInPc2d1
peptidů	peptid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
matrice	matrice	k1gFnSc2
obsahující	obsahující	k2eAgFnSc4d1
kyselinu	kyselina	k1gFnSc4
sinapovou	sinapová	k1gFnSc4
<g/>
,	,	kIx,
4-HCCA	4-HCCA	k4
a	a	k8xC
kyseliny	kyselina	k1gFnPc1
gentisové	gentisový	k2eAgFnPc1d1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
vyšší	vysoký	k2eAgFnSc2d2
citlivosti	citlivost	k1gFnSc2
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
amidací	amidací	k2eAgFnSc2d1
sialové	sialový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Přidáním	přidání	k1gNnSc7
iontové	iontový	k2eAgFnSc2d1
kapaliny	kapalina	k1gFnSc2
do	do	k7c2
matrice	matrice	k1gFnSc2
se	se	k3xPyFc4
omezí	omezit	k5eAaPmIp3nP
ztráty	ztráta	k1gFnPc1
sialových	sialový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
TOF-MS	TOF-MS	k1gFnSc1
analýzy	analýza	k1gFnSc2
sialylovaných	sialylovaný	k2eAgInPc2d1
oligosacharidů	oligosacharid	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
2,4	2,4	k4
<g/>
,6	,6	k4
<g/>
-trihydroxyacetofenon	-trihydroxyacetofenona	k1gFnPc2
(	(	kIx(
<g/>
THAP	THAP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
dihydroxyacetonfosfát	dihydroxyacetonfosfát	k1gMnSc1
(	(	kIx(
<g/>
DHAP	DHAP	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
směs	směs	k1gFnSc1
2	#num#	k4
<g/>
-aza-	-aza-	k?
<g/>
2	#num#	k4
<g/>
-thiothyminu	-thiothymin	k1gInSc2
s	s	k7c7
fenylhydrazinem	fenylhydrazin	k1gInSc7
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
luze	luza	k1gFnSc6
použít	použít	k5eAaPmF
na	na	k7c4
přípravu	příprava	k1gFnSc4
matricí	matrice	k1gFnPc2
omezujících	omezující	k2eAgFnPc2d1
ztráty	ztráta	k1gFnPc4
sialových	sialový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
během	během	k7c2
MALDI-MS	MALDI-MS	k1gFnSc2
analýzy	analýza	k1gFnSc2
glykosylovaných	glykosylovaný	k2eAgInPc2d1
peptidů	peptid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
omezení	omezení	k1gNnSc1
některých	některý	k3yIgFnPc2
posttranslačních	posttranslační	k2eAgFnPc2d1
modifikací	modifikace	k1gFnPc2
při	při	k7c6
použití	použití	k1gNnSc6
infračervené	infračervený	k2eAgFnSc2d1
MALDI	MALDI	kA
namísto	namísto	k7c2
ultrafialové	ultrafialový	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
molekulární	molekulární	k2eAgFnSc6d1
biologii	biologie	k1gFnSc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
směsi	směs	k1gFnPc4
kyseliny	kyselina	k1gFnSc2
5	#num#	k4
<g/>
-methoxysalicylové	-methoxysalicylový	k2eAgInPc1d1
a	a	k8xC
sperminu	spermin	k1gInSc2
použity	použít	k5eAaPmNgInP
jako	jako	k8xC,k8xS
matrice	matrice	k1gFnPc1
k	k	k7c3
analýze	analýza	k1gFnSc3
oligonukleotidů	oligonukleotid	k1gMnPc2
pomocí	pomocí	k7c2
MALDI	MALDI	kA
hmotnostní	hmotnostní	k2eAgFnSc2d1
spektrometrie	spektrometrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organická	organický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
</s>
<s>
Některé	některý	k3yIgFnPc1
syntetické	syntetický	k2eAgFnPc1d1
makromolekuly	makromolekula	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
katenany	katenana	k1gFnPc1
a	a	k8xC
rotaxany	rotaxana	k1gFnPc1
<g/>
,	,	kIx,
dendrimery	dendrimera	k1gFnPc1
a	a	k8xC
hyperrozvětvené	hyperrozvětvený	k2eAgInPc1d1
polymery	polymer	k1gInPc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
molekulové	molekulový	k2eAgFnPc1d1
hmotnosti	hmotnost	k1gFnPc1
v	v	k7c6
tisících	tisíc	k4xCgInPc6,k4xOgInPc6
až	až	k6eAd1
desetitisících	desetitisíce	k1gInPc6
Da	Da	k1gMnPc2
a	a	k8xC
u	u	k7c2
většiny	většina	k1gFnSc2
ionizačních	ionizační	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
vytvořit	vytvořit	k5eAaPmF
z	z	k7c2
nich	on	k3xPp3gMnPc2
molekulární	molekulární	k2eAgMnPc1d1
ionty	ion	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
využitím	využití	k1gNnSc7
MALDI	MALDI	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
snadno	snadno	k6eAd1
a	a	k8xC
rychle	rychle	k6eAd1
analyzovat	analyzovat	k5eAaImF
tyto	tento	k3xDgFnPc4
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Polymery	polymer	k1gInPc1
</s>
<s>
V	v	k7c6
chemii	chemie	k1gFnSc6
polymerů	polymer	k1gInPc2
lze	lze	k6eAd1
MALDI	MALDI	kA
použít	použít	k5eAaPmF
k	k	k7c3
určení	určení	k1gNnSc3
rozdělení	rozdělení	k1gNnSc2
molekulových	molekulový	k2eAgFnPc2d1
hmotností	hmotnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Polymery	polymer	k1gInPc1
s	s	k7c7
polydisperzitou	polydisperzita	k1gFnSc7
nad	nad	k7c4
1,2	1,2	k4
se	se	k3xPyFc4
pomocí	pomocí	k7c2
MALDI	MALDI	kA
zkoumají	zkoumat	k5eAaImIp3nP
obtížně	obtížně	k6eAd1
kvůli	kvůli	k7c3
nižším	nízký	k2eAgFnPc3d2
intenzitám	intenzita	k1gFnPc3
signálu	signál	k1gInSc2
oproti	oproti	k7c3
oligomerům	oligomer	k1gMnPc3
s	s	k7c7
vyššími	vysoký	k2eAgFnPc7d2
molekulovými	molekulový	k2eAgFnPc7d1
hmotnostmi	hmotnost	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
přípravu	příprava	k1gFnSc4
matric	matrice	k1gFnPc2
pro	pro	k7c4
analýzu	analýza	k1gFnSc4
polymerů	polymer	k1gInPc2
se	se	k3xPyFc4
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
dithranol	dithranol	k1gInSc1
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
trifluoroctan	trifluoroctan	k1gInSc1
stříbrný	stříbrný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
vzorku	vzorek	k1gInSc2
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
přidá	přidat	k5eAaPmIp3nS
dithranol	dithranol	k1gInSc4
a	a	k8xC
poté	poté	k6eAd1
trifluoroctan	trifluoroctan	k1gInSc1
stříbrný	stříbrný	k2eAgInSc1d1
<g/>
,	,	kIx,
při	při	k7c6
opačném	opačný	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
vzorek	vzorek	k1gInSc1
z	z	k7c2
roztoku	roztok	k1gInSc2
vysrážel	vysrážet	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Mikrobiologie	mikrobiologie	k1gFnSc1
</s>
<s>
MALDI-TOF	MALDI-TOF	k?
spektra	spektrum	k1gNnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
použít	použít	k5eAaPmF
k	k	k7c3
určení	určení	k1gNnSc3
mikroorganismů	mikroorganismus	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
bakterie	bakterie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
zkoumané	zkoumaný	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
mikroorganismů	mikroorganismus	k1gInPc2
se	se	k3xPyFc4
umístí	umístit	k5eAaPmIp3nS
na	na	k7c4
destičku	destička	k1gFnSc4
poro	poro	k6eAd1
aplikaci	aplikace	k1gFnSc4
vzorku	vzorek	k1gInSc2
a	a	k8xC
překryje	překrýt	k5eAaPmIp3nS
matricí	matrice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získaná	získaný	k2eAgNnPc4d1
hmotnostní	hmotnostní	k2eAgNnPc4d1
spektra	spektrum	k1gNnPc4
bílkovin	bílkovina	k1gFnPc2
se	se	k3xPyFc4
analyzují	analyzovat	k5eAaImIp3nP
a	a	k8xC
porovnávají	porovnávat	k5eAaImIp3nP
s	s	k7c7
databází	databáze	k1gFnSc7
profilů	profil	k1gInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
přináší	přinášet	k5eAaImIp3nS
výhody	výhoda	k1gFnPc4
pro	pro	k7c4
další	další	k2eAgInPc4d1
imunologické	imunologický	k2eAgInPc4d1
či	či	k8xC
biochemické	biochemický	k2eAgInPc4d1
úkony	úkon	k1gInPc4
a	a	k8xC
běžně	běžně	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
klinických	klinický	k2eAgFnPc6d1
mikrobiologických	mikrobiologický	k2eAgFnPc6d1
laboratořích	laboratoř	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
MALDI-MS	MALDI-MS	k1gFnSc1
s	s	k7c7
iontovou	iontový	k2eAgFnSc7d1
cyklotronovou	cyklotronový	k2eAgFnSc7d1
rezonanční	rezonanční	k2eAgFnSc7d1
hmotnostní	hmotnostní	k2eAgFnSc7d1
spektrometrií	spektrometrie	k1gFnSc7
a	a	k8xC
Fourierovou	Fourierův	k2eAgFnSc7d1
transformací	transformace	k1gFnSc7
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
FT-MS	FT-MS	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
k	k	k7c3
určování	určování	k1gNnSc3
virů	vir	k1gInPc2
detekcí	detekce	k1gFnPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
iontů	ion	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oproti	oproti	k7c3
ostatním	ostatní	k2eAgFnPc3d1
mikrobiologickým	mikrobiologický	k2eAgFnPc3d1
identifikačním	identifikační	k2eAgFnPc3d1
metodám	metoda	k1gFnPc3
má	mít	k5eAaImIp3nS
MALDI-FT-MS	MALDI-FT-MS	k1gMnSc1
výhodu	výhoda	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
rychlé	rychlý	k2eAgFnSc2d1
a	a	k8xC
spolehlivé	spolehlivý	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
nízkých	nízký	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
a	a	k8xC
širokého	široký	k2eAgNnSc2d1
rozmezí	rozmezí	k1gNnSc2
mikroorganismů	mikroorganismus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
lze	lze	k6eAd1
určit	určit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
předpovídat	předpovídat	k5eAaImF
citlivost	citlivost	k1gFnSc4
bakterií	bakterie	k1gFnPc2
na	na	k7c4
antibiotika	antibiotikum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
MALDI	MALDI	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
předvídat	předvídat	k5eAaImF
odolnost	odolnost	k1gFnSc4
bakterie	bakterie	k1gFnSc2
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
vůči	vůči	k7c3
methicilinu	methicilin	k1gInSc3
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
také	také	k9
najít	najít	k5eAaPmF
karbapenemázu	karbapenemáza	k1gFnSc4
u	u	k7c2
enterobakterií	enterobakterie	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Acinetobacter	Acinetobacter	k1gInSc4
baumannii	baumannium	k1gNnPc7
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Klebsiella	Klebsiella	k1gFnSc1
pneumoniae	pneumoniae	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
bílkovin	bílkovina	k1gFnPc2
ovládajících	ovládající	k2eAgFnPc2d1
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
antibiotikům	antibiotikum	k1gNnPc3
má	mít	k5eAaImIp3nS
ovšem	ovšem	k9
molekulovou	molekulový	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
mimo	mimo	k7c4
rozmezí	rozmezí	k1gNnSc4
2	#num#	k4
až	až	k9
20	#num#	k4
kDa	kDa	k?
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
lze	lze	k6eAd1
analyzovat	analyzovat	k5eAaImF
látky	látka	k1gFnPc4
pomocí	pomocí	k7c2
MALDI-TOF	MALDI-TOF	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Parazitologie	parazitologie	k1gFnSc1
</s>
<s>
MALDI-TOF	MALDI-TOF	k?
spektra	spektrum	k1gNnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
k	k	k7c3
detekci	detekce	k1gFnSc3
a	a	k8xC
určení	určení	k1gNnSc4
různých	různý	k2eAgMnPc2d1
parazitů	parazit	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
trypanozomy	trypanozoma	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Leishmania	Leishmanium	k1gNnSc2
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zimničky	zimnička	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
těchto	tento	k3xDgMnPc2
jednobuněčných	jednobuněčný	k2eAgMnPc2d1
parazitů	parazit	k1gMnPc2
se	se	k3xPyFc4
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
také	také	k9
používá	používat	k5eAaImIp3nS
k	k	k7c3
určování	určování	k1gNnSc3
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1
parazitů	parazit	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
vši	veš	k1gFnPc1
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
cerkarie	cerkarie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lékařství	lékařství	k1gNnSc1
</s>
<s>
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
se	se	k3xPyFc4
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
analytickými	analytický	k2eAgFnPc7d1
a	a	k8xC
spektroskopickými	spektroskopický	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
k	k	k7c3
diagnostikování	diagnostikování	k1gNnSc3
nemocí	nemoc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
umožňuje	umožňovat	k5eAaImIp3nS
rychlou	rychlý	k2eAgFnSc4d1
identifikaci	identifikace	k1gFnSc4
bílkovin	bílkovina	k1gFnPc2
a	a	k8xC
s	s	k7c7
nimi	on	k3xPp3gMnPc7
souvisejících	související	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
při	při	k7c6
nižších	nízký	k2eAgInPc6d2
nákladech	náklad	k1gInPc6
než	než	k8xS
u	u	k7c2
sekvenování	sekvenování	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
při	při	k7c6
ní	on	k3xPp3gFnSc6
nejsou	být	k5eNaImIp3nP
tolik	tolik	k6eAd1
potřeba	potřeba	k6eAd1
výpočetní	výpočetní	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
dovednosti	dovednost	k1gFnPc1
nebo	nebo	k8xC
čas	čas	k1gInSc1
jako	jako	k8xS,k8xC
při	při	k7c6
určování	určování	k1gNnSc6
struktury	struktura	k1gFnSc2
pomocí	pomocí	k7c2
rentgenové	rentgenový	k2eAgFnSc2d1
krystalografie	krystalografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
diagnostikování	diagnostikování	k1gNnSc4
nekrotizující	krotizující	k2eNgFnSc2d1
enterokolitidy	enterokolitis	k1gFnSc2
(	(	kIx(
<g/>
NEC	NEC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nemoc	nemoc	k1gFnSc1
postihující	postihující	k2eAgFnSc1d1
střeva	střevo	k1gNnSc2
u	u	k7c2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příznaky	příznak	k1gInPc7
jsou	být	k5eAaImIp3nP
podobné	podobný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
u	u	k7c2
sepse	sepse	k1gFnSc2
a	a	k8xC
mnoho	mnoho	k4c4
dětí	dítě	k1gFnPc2
umírá	umírat	k5eAaImIp3nS
kvůli	kvůli	k7c3
příliš	příliš	k6eAd1
pozdnímu	pozdní	k2eAgNnSc3d1
diagnostikování	diagnostikování	k1gNnSc3
a	a	k8xC
léčení	léčení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
lze	lze	k6eAd1
použít	použít	k5eAaPmF
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
bakterií	bakterie	k1gFnPc2
ve	v	k7c6
výkalech	výkal	k1gInPc6
dětí	dítě	k1gFnPc2
s	s	k7c7
NEC	NEC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
studie	studie	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
characterizaci	characterizace	k1gFnSc6
fekálího	fekálí	k2eAgInSc2d1
mikrobiomu	mikrobiom	k1gInSc2
spojeného	spojený	k2eAgNnSc2d1
s	s	k7c7
NEC	NEC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
se	se	k3xPyFc4
také	také	k9
používá	používat	k5eAaImIp3nS
ke	k	k7c3
zjišťování	zjišťování	k1gNnSc3
odolnosti	odolnost	k1gFnSc2
bakterií	bakterie	k1gFnPc2
vůči	vůči	k7c3
léčivům	léčivo	k1gNnPc3
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
β	β	k2eAgNnPc3d1
antibiotikům	antibiotikum	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
TOF	TOF	kA
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použita	použít	k5eAaPmNgFnS
k	k	k7c3
detekci	detekce	k1gFnSc3
karbapenemáz	karbapenemáza	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
naznačují	naznačovat	k5eAaImIp3nP
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
běžným	běžný	k2eAgNnPc3d1
antibiotikům	antibiotikum	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
prokázat	prokázat	k5eAaPmF
odolnost	odolnost	k1gFnSc4
bakterie	bakterie	k1gFnSc2
proti	proti	k7c3
léčivům	léčivo	k1gNnPc3
do	do	k7c2
tří	tři	k4xCgFnPc2
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
pomáhá	pomáhat	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
předepsána	předepsán	k2eAgNnPc1d1
silnější	silný	k2eAgNnPc1d2
antibiotika	antibiotikum	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1
proteinových	proteinový	k2eAgInPc2d1
komplexů	komplex	k1gInPc2
</s>
<s>
Po	po	k7c6
zjištění	zjištění	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
některé	některý	k3yIgInPc4
komplexy	komplex	k1gInPc4
peptidů	peptid	k1gInPc2
jsou	být	k5eAaImIp3nP
odolné	odolný	k2eAgFnPc1d1
vůči	vůči	k7c3
depozici	depozice	k1gFnSc3
a	a	k8xC
ionizaci	ionizace	k1gFnSc3
při	při	k7c6
MALDI	MALDI	kA
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgFnP
studie	studie	k1gFnPc1
zabývající	zabývající	k2eAgFnPc1d1
se	se	k3xPyFc4
analýzou	analýza	k1gFnSc7
proteinových	proteinový	k2eAgMnPc2d1
komplexů	komplex	k1gInPc2
pomocí	pomocí	k7c2
MALDI-	MALDI-	k1gFnSc2
<g/>
MS.	MS.	k1gFnSc2
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
PEGylace	PEGylace	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Matrix-assisted	Matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Franz	Franz	k1gMnSc1
Hillenkamp	Hillenkamp	k1gMnSc1
<g/>
;	;	kIx,
Michael	Michael	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
Ronald	Ronald	k1gMnSc1
C.	C.	kA
Beavis	Beavis	k1gFnSc1
<g/>
;	;	kIx,
Brian	Brian	k1gMnSc1
T.	T.	kA
Chait	Chait	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix-assisted	Matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
of	of	k?
biopolymers	biopolymers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1193	#num#	k4
<g/>
A	a	k8xC
<g/>
–	–	k?
<g/>
1203	#num#	k4
<g/>
A.	A.	kA
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
1789447	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Michael	Michael	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
Ralf	Ralf	k1gMnSc1
Krüger	Krüger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ion	ion	k1gInSc1
Formation	Formation	k1gInSc4
in	in	k?
MALDI	MALDI	kA
<g/>
:	:	kIx,
The	The	k1gFnSc1
Cluster	cluster	k1gInSc4
Ionization	Ionization	k1gInSc1
Mechanism	Mechanism	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemical	Chemical	k1gFnSc1
Reviews	Reviewsa	k1gFnPc2
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
427	#num#	k4
<g/>
–	–	k?
<g/>
440	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2665	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
cr	cr	k0
<g/>
0	#num#	k4
<g/>
10376	#num#	k4
<g/>
a.	a.	k?
PMID	PMID	kA
12580637	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Michael	Michael	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
D.	D.	kA
Bachmann	Bachmann	k1gMnSc1
<g/>
;	;	kIx,
F.	F.	kA
Hillenkamp	Hillenkamp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Influence	influence	k1gFnSc1
of	of	k?
the	the	k?
Wavelength	Wavelength	k1gInSc1
in	in	k?
High-Irradiance	High-Irradianec	k1gInSc2
Ultraviolet	Ultraviolet	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
of	of	k?
Organic	Organice	k1gFnPc2
Molecules	Molecules	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1985	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2935	#num#	k4
<g/>
–	–	k?
<g/>
2939	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
291	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
42	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Michael	Michael	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
D.	D.	kA
Bachmann	Bachmann	k1gMnSc1
<g/>
;	;	kIx,
U.	U.	kA
Bahr	Bahr	k1gMnSc1
<g/>
;	;	kIx,
F.	F.	kA
Hillenkamp	Hillenkamp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Ultraviolet	Ultraviolet	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
of	of	k?
Non-Volatile	Non-Volatil	k1gMnSc5
Compounds	Compounds	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
and	and	k?
Ion	ion	k1gInSc1
Processes	Processes	k1gInSc1
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
53	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
168	#num#	k4
<g/>
-	-	kIx~
<g/>
1176	#num#	k4
<g/>
(	(	kIx(
<g/>
87	#num#	k4
<g/>
)	)	kIx)
<g/>
87041	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1987	#num#	k4
<g/>
IJMSI	IJMSI	kA
<g/>
.	.	kIx.
<g/>
.78	.78	k4
<g/>
..	..	k?
<g/>
.53	.53	k4
<g/>
K.	K.	kA
↑	↑	k?
K.	K.	kA
Tanaka	Tanak	k1gMnSc2
<g/>
;	;	kIx,
H.	H.	kA
Waki	Wak	k1gFnSc2
<g/>
;	;	kIx,
Y.	Y.	kA
Ido	ido	k1gNnSc4
<g/>
;	;	kIx,
S.	S.	kA
Akita	Akita	k1gMnSc1
<g/>
;	;	kIx,
Y.	Y.	kA
Yoshida	Yoshida	k1gFnSc1
<g/>
;	;	kIx,
T.	T.	kA
Yoshida	Yoshida	k1gFnSc1
<g/>
;	;	kIx,
T.	T.	kA
Matsuo	Matsuo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protein	protein	k1gInSc1
and	and	k?
Polymer	polymer	k1gInSc1
Analyses	Analyses	k1gInSc1
up	up	k?
to	ten	k3xDgNnSc4
m	m	kA
<g/>
/	/	kIx~
<g/>
z	z	k7c2
100	#num#	k4
000	#num#	k4
by	by	k9
Laser	laser	k1gInSc1
Ionization	Ionization	k1gInSc1
Time-of	Time-of	k1gInSc1
flight	flight	k2eAgInSc1d1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1988	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
151	#num#	k4
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.1290020802	.1290020802	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1988	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
...	...	k?
<g/>
.2	.2	k4
<g/>
.	.	kIx.
<g/>
.151	.151	k4
<g/>
T.	T.	kA
↑	↑	k?
K.	K.	kA
Markides	Markides	k1gMnSc1
<g/>
;	;	kIx,
A.	A.	kA
Gräslund	Gräslund	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Advanced	Advanced	k1gInSc1
information	information	k1gInSc4
on	on	k3xPp3gMnSc1
the	the	k?
Nobel	Nobel	k1gMnSc1
Prize	Prize	k1gFnSc2
in	in	k?
Chemistry	Chemistr	k1gMnPc4
2002	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
F.	F.	kA
Hillenkamp	Hillenkamp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laser	laser	k1gInSc1
desorption	desorption	k1gInSc4
ionization	ionization	k1gInSc1
of	of	k?
proteins	proteins	k1gInSc1
with	with	k1gMnSc1
molecular	molecular	k1gMnSc1
masses	masses	k1gMnSc1
exceeding	exceeding	k1gInSc4
10,000	10,000	k4
daltons	daltonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1988	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2299	#num#	k4
<g/>
–	–	k?
<g/>
2301	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
171	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
3239801	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
BEAVIS	BEAVIS	kA
<g/>
,	,	kIx,
R.	R.	kA
C.	C.	kA
<g/>
;	;	kIx,
CHAIT	CHAIT	kA
<g/>
,	,	kIx,
B.	B.	kA
T.	T.	kA
<g/>
;	;	kIx,
STANDING	STANDING	kA
<g/>
,	,	kIx,
K.	K.	kA
G.	G.	kA
Matrix-assisted	Matrix-assisted	k1gInSc1
laser-desorption	laser-desorption	k1gInSc1
mass	mass	k1gInSc1
spectrometry	spectrometr	k1gInPc4
using	using	k1gInSc1
355	#num#	k4
nm	nm	k?
radiation	radiation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
436	#num#	k4
<g/>
–	–	k?
<g/>
439	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.1290031208	.1290031208	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
2520224	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1989	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
...	...	k?
<g/>
.3	.3	k4
<g/>
.	.	kIx.
<g/>
.436	.436	k4
<g/>
B.	B.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
M.	M.	kA
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
U.	U.	kA
Bahr	Bahr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc4
Ionization	Ionization	k1gInSc1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
of	of	k?
Large	Larg	k1gMnSc4
Biomolecules	Biomolecules	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trends	Trends	k1gInSc1
in	in	k?
Analytical	Analytical	k1gFnSc2
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
321	#num#	k4
<g/>
–	–	k?
<g/>
325	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
165	#num#	k4
<g/>
-	-	kIx~
<g/>
9936	#num#	k4
<g/>
(	(	kIx(
<g/>
90	#num#	k4
<g/>
)	)	kIx)
<g/>
85065	#num#	k4
<g/>
-	-	kIx~
<g/>
F.	F.	kA
↑	↑	k?
STRUPAT	STRUPAT	kA
<g/>
,	,	kIx,
K.	K.	kA
<g/>
;	;	kIx,
KARAS	Karas	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
HILLENKAMP	HILLENKAMP	kA
<g/>
,	,	kIx,
F.	F.	kA
2,5	2,5	k4
<g/>
-Dihidroxybenzoic	-Dihidroxybenzoice	k1gInPc2
acid	acida	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
new	new	k?
matrix	matrix	k1gInSc1
for	forum	k1gNnPc2
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
—	—	k?
<g/>
ionization	ionization	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
and	and	k?
Ion	ion	k1gInSc1
Processes	Processes	k1gInSc1
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
89	#num#	k4
<g/>
–	–	k?
<g/>
102	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
168	#num#	k4
<g/>
-	-	kIx~
<g/>
1176	#num#	k4
<g/>
(	(	kIx(
<g/>
91	#num#	k4
<g/>
)	)	kIx)
<g/>
85050	#num#	k4
<g/>
-	-	kIx~
<g/>
V.	V.	kA
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1991	#num#	k4
<g/>
IJMSI	IJMSI	kA
<g/>
.111	.111	k4
<g/>
..	..	k?
<g/>
.89	.89	k4
<g/>
S.	S.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
zde	zde	k6eAd1
použita	použít	k5eAaPmNgFnS
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
BEAVIS	BEAVIS	kA
<g/>
,	,	kIx,
R.	R.	kA
C.	C.	kA
<g/>
;	;	kIx,
CHAIT	CHAIT	kA
<g/>
,	,	kIx,
B.	B.	kA
T.	T.	kA
<g/>
;	;	kIx,
FALES	FALES	kA
<g/>
,	,	kIx,
H.	H.	kA
M.	M.	kA
Cinnamic	Cinnamic	k1gMnSc1
acid	acid	k1gMnSc1
derivatives	derivatives	k1gMnSc1
as	as	k9
matrices	matrices	k1gInSc4
for	forum	k1gNnPc2
ultraviolet	ultraviolet	k5eAaImF,k5eAaPmF
laser	laser	k1gInSc4
desorption	desorption	k1gInSc4
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
of	of	k?
proteins	proteins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
432	#num#	k4
<g/>
–	–	k?
<g/>
435	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.1290031207	.1290031207	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
2520223	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1989	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
...	...	k?
<g/>
.3	.3	k4
<g/>
.	.	kIx.
<g/>
.432	.432	k4
<g/>
B.	B.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BEAVIS	BEAVIS	kA
<g/>
,	,	kIx,
R.	R.	kA
C.	C.	kA
<g/>
;	;	kIx,
CHAUDHARY	CHAUDHARY	kA
<g/>
,	,	kIx,
T.	T.	kA
<g/>
;	;	kIx,
CHAIT	CHAIT	kA
<g/>
,	,	kIx,
B.	B.	kA
T.	T.	kA
α	α	k?
<g/>
4	#num#	k4
<g/>
-hydroxycinnamic	-hydroxycinnamice	k1gFnPc2
acid	acida	k1gFnPc2
as	as	k1gInSc1
a	a	k8xC
matrix	matrix	k1gInSc1
for	forum	k1gNnPc2
matrix-assisted	matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
mass	mass	k1gInSc4
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organic	Organice	k1gFnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
156	#num#	k4
<g/>
–	–	k?
<g/>
158	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
oms	oms	k?
<g/>
.1210270217	.1210270217	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TANG	tango	k1gNnPc2
<g/>
,	,	kIx,
K.	K.	kA
<g/>
;	;	kIx,
TARANENKO	TARANENKO	kA
<g/>
,	,	kIx,
N.	N.	kA
I.	I.	kA
<g/>
;	;	kIx,
ALLMAN	ALLMAN	kA
<g/>
,	,	kIx,
S.	S.	kA
L.	L.	kA
<g/>
;	;	kIx,
CHÁNG	CHÁNG	kA
<g/>
,	,	kIx,
L.	L.	kA
Y.	Y.	kA
<g/>
;	;	kIx,
CHEN	CHEN	kA
<g/>
,	,	kIx,
C.	C.	kA
H.	H.	kA
<g/>
;	;	kIx,
LUBMAN	LUBMAN	kA
<g/>
,	,	kIx,
D.	D.	kA
M.	M.	kA
Detection	Detection	k1gInSc1
of	of	k?
500	#num#	k4
<g/>
-nucleotide	-nucleotid	k1gInSc5
DNA	DNA	kA
by	by	kYmCp3nS
laser	laser	k1gInSc1
desorption	desorption	k1gInSc4
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
727	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.1290080913	.1290080913	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
7949335	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1994	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
...	...	k?
<g/>
.8	.8	k4
<g/>
.	.	kIx.
<g/>
.727	.727	k4
<g/>
T.	T.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WU	WU	kA
<g/>
,	,	kIx,
K.	K.	kA
J.	J.	kA
<g/>
;	;	kIx,
STEDING	STEDING	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
BECKER	BECKER	kA
<g/>
,	,	kIx,
C.	C.	kA
H.	H.	kA
Matrix-assisted	Matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
time-of-flight	time-of-flight	k2eAgInSc1d1
mass	mass	k1gInSc1
spectrometry	spectrometr	k1gInPc1
of	of	k?
oligonucleotides	oligonucleotides	k1gInSc4
using	usinga	k1gFnPc2
3	#num#	k4
<g/>
-hydroxypicolinic	-hydroxypicolinice	k1gFnPc2
acid	acid	k6eAd1
as	as	k9
an	an	k?
ultraviolet-sensitive	ultraviolet-sensitiv	k1gInSc5
matrix	matrix	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.1290070206	.1290070206	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
8457722	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1993	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
...	...	k?
<g/>
.7	.7	k4
<g/>
.	.	kIx.
<g/>
.142	.142	k4
<g/>
W.	W.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Walter	Walter	k1gMnSc1
A.	A.	kA
Korfmacher	Korfmachra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Using	Using	k1gInSc1
Mass	Mass	k1gInSc4
Spectrometry	Spectrometr	k1gInPc1
for	forum	k1gNnPc2
Drug	Drug	k1gMnSc1
Metabolism	Metabolism	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
CRC	CRC	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781420092219	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
342	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
C.	C.	kA
Fitzgerald	Fitzgerald	k1gMnSc1
<g/>
;	;	kIx,
G.	G.	kA
R.	R.	kA
Parr	Parr	k1gMnSc1
<g/>
;	;	kIx,
L.	L.	kA
M.	M.	kA
Smith	Smith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basic	Basic	kA
matrixes	matrixesa	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
matrix-assisted	matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
of	of	k?
proteins	proteins	k1gInSc1
and	and	k?
oligonucleotides	oligonucleotides	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3204	#num#	k4
<g/>
–	–	k?
<g/>
3211	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
70	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
8291672	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
R.	R.	kA
Zenobi	Zenob	k1gFnSc2
<g/>
;	;	kIx,
R.	R.	kA
Knochenmuss	Knochenmuss	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ion	ion	k1gInSc1
formation	formation	k1gInSc4
in	in	k?
MALDI	MALDI	kA
mass	mass	k1gInSc1
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
Reviews	Reviews	k1gInSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
337	#num#	k4
<g/>
–	–	k?
<g/>
366	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
SICI	SICI	kA
<g/>
)	)	kIx)
<g/>
1098	#num#	k4
<g/>
-	-	kIx~
<g/>
2787	#num#	k4
<g/>
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
<	<	kIx(
<g/>
337	#num#	k4
<g/>
::	::	k?
<g/>
AID-MAS	AID-MAS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
>	>	kIx)
<g/>
3.0	3.0	k4
<g/>
.	.	kIx.
<g/>
CO	co	k6eAd1
<g/>
;	;	kIx,
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
S.	S.	kA
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1998	#num#	k4
<g/>
MSRv	MSRva	k1gFnPc2
<g/>
..	..	k?
<g/>
.17	.17	k4
<g/>
.	.	kIx.
<g/>
.337	.337	k4
<g/>
Z.	Z.	kA
↑	↑	k?
Yingda	Yingda	k1gMnSc1
Xu	Xu	k1gMnSc1
<g/>
;	;	kIx,
Merlin	Merlin	k1gInSc1
L.	L.	kA
Bruening	Bruening	k1gInSc1
<g/>
;	;	kIx,
J.	J.	kA
Throck	Throck	k1gMnSc1
Watson	Watson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Non-specific	Non-specifice	k1gInPc2
<g/>
,	,	kIx,
on-probe	on-probat	k5eAaPmIp3nS
cleanup	cleanup	k1gInSc1
methods	methodsa	k1gFnPc2
for	forum	k1gNnPc2
MALDI-MS	MALDI-MS	k1gMnSc1
samples	samples	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
Reviews	Reviews	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
429	#num#	k4
<g/>
–	–	k?
<g/>
440	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
277	#num#	k4
<g/>
-	-	kIx~
<g/>
7037	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
mas	masa	k1gFnPc2
<g/>
.10064	.10064	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
14528495	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2003	#num#	k4
<g/>
MSRv	MSRva	k1gFnPc2
<g/>
..	..	k?
<g/>
.22	.22	k4
<g/>
.	.	kIx.
<g/>
.429	.429	k4
<g/>
X.	X.	kA
↑	↑	k?
M.	M.	kA
Nazim	Nazim	k1gInSc4
Boutaghou	Boutagha	k1gFnSc7
<g/>
;	;	kIx,
R.	R.	kA
B.	B.	kA
Cole	cola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Non-specific	Non-specifice	k1gInPc2
<g/>
,	,	kIx,
on-probe	on-probat	k5eAaPmIp3nS
cleanup	cleanup	k1gInSc1
methods	methodsa	k1gFnPc2
for	forum	k1gNnPc2
MALDI-MS	MALDI-MS	k1gMnSc1
samples	samples	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
995	#num#	k4
<g/>
–	–	k?
<g/>
1003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
277	#num#	k4
<g/>
-	-	kIx~
<g/>
7037	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
jms	jms	k?
<g/>
.3027	.3027	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
22899508	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2012	#num#	k4
<g/>
JMSp	JMSp	k1gInSc1
<g/>
..	..	k?
<g/>
.47	.47	k4
<g/>
.	.	kIx.
<g/>
.995	.995	k4
<g/>
N.	N.	kA
↑	↑	k?
T.	T.	kA
Suzuki	suzuki	k1gNnSc4
<g/>
;	;	kIx,
H.	H.	kA
Midonoya	Midonoya	k1gMnSc1
<g/>
;	;	kIx,
Y.	Y.	kA
Shioi	Shio	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analysis	Analysis	k1gFnSc1
of	of	k?
chlorophylls	chlorophylls	k6eAd1
and	and	k?
their	their	k1gMnSc1
derivatives	derivatives	k1gMnSc1
by	by	k9
matrix-assisted	matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
<g/>
–	–	k?
<g/>
time-of-flight	time-of-flight	k2eAgInSc1d1
mass	mass	k1gInSc1
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Biochemistry	Biochemistr	k1gMnPc7
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
57	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
ab	ab	k?
<g/>
.2009	.2009	k4
<g/>
.04	.04	k4
<g/>
.005	.005	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19364490	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
J.	J.	kA
Wei	Wei	k1gFnSc2
<g/>
;	;	kIx,
H.	H.	kA
Li	li	k9
<g/>
;	;	kIx,
M.	M.	kA
P.	P.	kA
Barrow	Barrow	k1gMnSc1
<g/>
;	;	kIx,
P.	P.	kA
B.	B.	kA
O	O	kA
<g/>
'	'	kIx"
<g/>
Connor	Connor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Structural	Structural	k1gFnPc2
characterization	characterization	k1gInSc4
of	of	k?
chlorophyll-a	chlorophyll-	k1gInSc2
by	by	kYmCp3nS
high	high	k1gInSc4
resolution	resolution	k1gInSc1
tandem	tandem	k1gInSc1
mass	mass	k1gInSc4
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
753	#num#	k4
<g/>
–	–	k?
<g/>
760	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
577	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23504642	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2013	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.24	.24	k4
<g/>
.	.	kIx.
<g/>
.753	.753	k4
<g/>
W.	W.	kA
↑	↑	k?
N.	N.	kA
Srinivasan	Srinivasan	k1gMnSc1
<g/>
;	;	kIx,
C.	C.	kA
A.	A.	kA
Haney	Hanea	k1gMnSc2
<g/>
;	;	kIx,
J.	J.	kA
S.	S.	kA
Lindsey	Lindsea	k1gMnSc2
<g/>
;	;	kIx,
W.	W.	kA
Zhang	Zhang	k1gMnSc1
<g/>
;	;	kIx,
B.	B.	kA
T.	T.	kA
Chait	Chait	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investigation	Investigation	k1gInSc1
of	of	k?
MALDI-TOF	MALDI-TOF	k1gMnSc2
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
of	of	k?
diverse	diverse	k1gFnSc2
synthetic	synthetice	k1gFnPc2
metalloporphyrins	metalloporphyrins	k1gInSc1
<g/>
,	,	kIx,
phthalocyanines	phthalocyanines	k1gInSc1
and	and	k?
multiporphyrin	multiporphyrin	k1gInSc1
arrays	arrays	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Porphyrins	Porphyrins	k1gInSc1
and	and	k?
Phthalocyanines	Phthalocyanines	k1gInSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
283	#num#	k4
<g/>
–	–	k?
<g/>
291	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
SICI	SICI	kA
<g/>
)	)	kIx)
<g/>
1099	#num#	k4
<g/>
-	-	kIx~
<g/>
1409	#num#	k4
<g/>
(	(	kIx(
<g/>
199904	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
<	<	kIx(
<g/>
283	#num#	k4
<g/>
::	::	k?
<g/>
AID-JPP	AID-JPP	k1gFnSc1
<g/>
132	#num#	k4
<g/>
>	>	kIx)
<g/>
3.0	3.0	k4
<g/>
.	.	kIx.
<g/>
CO	co	k6eAd1
<g/>
;	;	kIx,
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
F.	F.	kA
↑	↑	k?
Talking	Talking	k1gInSc1
About	About	k1gInSc1
a	a	k8xC
Revolution	Revolution	k1gInSc1
<g/>
:	:	kIx,
FT-ICR	FT-ICR	k1gMnPc1
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc4
Offers	Offersa	k1gFnPc2
High	Higha	k1gFnPc2
Resolution	Resolution	k1gInSc1
and	and	k?
Mass	Mass	k1gInSc1
Accuracy	Accuraca	k1gMnSc2
for	forum	k1gNnPc2
Pr	pr	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
P.	P.	kA
Schmitt-Kopplin	Schmitt-Kopplin	k1gInSc1
<g/>
;	;	kIx,
N.	N.	kA
Hertkorn	Hertkorn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ultrahigh	Ultrahigha	k1gFnPc2
resolution	resolution	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gMnSc1
and	and	k?
Bioanalytical	Bioanalytical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1309	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
216	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1589	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Jonas	Jonas	k1gMnSc1
Ghyselinck	Ghyselinck	k1gMnSc1
<g/>
;	;	kIx,
Koenraad	Koenraad	k1gInSc1
Van	vana	k1gFnPc2
Hoorde	Hoord	k1gInSc5
<g/>
;	;	kIx,
Bart	Bart	k1gInSc1
Hoste	host	k1gMnSc5
<g/>
;	;	kIx,
Kim	Kim	k1gFnSc7
Heylen	Heylen	k2eAgInSc4d1
<g/>
;	;	kIx,
Pau	Pau	k1gFnSc1
De	De	k?
Vos	vosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evaluation	Evaluation	k1gInSc1
of	of	k?
MALDI-TOF	MALDI-TOF	k1gFnSc2
MS	MS	kA
as	as	k1gInSc4
a	a	k8xC
tool	tool	k1gInSc4
for	forum	k1gNnPc2
high-throughput	high-throughput	k2eAgInSc1d1
dereplication	dereplication	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Microbiological	Microbiological	k1gFnSc2
Methods	Methodsa	k1gFnPc2
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
327	#num#	k4
<g/>
–	–	k?
<g/>
336	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
mimet	mimet	k1gInSc1
<g/>
.2011	.2011	k4
<g/>
.06	.06	k4
<g/>
.004	.004	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
21699925	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Klaus	Klaus	k1gMnSc1
Dreisewerd	Dreisewerd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recent	Recent	k1gInSc1
methodological	methodologicat	k5eAaPmAgInS
advances	advances	k1gInSc4
in	in	k?
MALDI	MALDI	kA
mass	mass	k1gInSc1
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gMnSc1
and	and	k?
Bioanalytical	Bioanalytical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2261	#num#	k4
<g/>
–	–	k?
<g/>
2278	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1618	#num#	k4
<g/>
-	-	kIx~
<g/>
2642	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
216	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
7646	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24652146	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
K.	K.	kA
Murray	Murraa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Michael	Michaela	k1gFnPc2
L.	L.	kA
Gross	Gross	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
M.	M.	kA
Caprioli	Capriole	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Elsevier	Elsevier	k1gInSc1
Science	Science	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
kapitola	kapitola	k1gFnSc1
=	=	kIx~
Chapter	Chapter	k1gMnSc1
9	#num#	k4
Desorption	Desorption	k1gInSc1
by	by	kYmCp3nS
Photons	Photons	k1gInSc1
<g/>
:	:	kIx,
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
and	and	k?
Matridx-Assisted	Matridx-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization	Ionization	k1gInSc1
(	(	kIx(
<g/>
MALDI	MALDI	kA
<g/>
)	)	kIx)
–	–	k?
Infrared	Infrared	k1gInSc1
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization	Ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780080438016	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Feng	Feng	k1gMnSc1
Xian	Xian	k1gMnSc1
<g/>
;	;	kIx,
Christopher	Christophra	k1gFnPc2
L.	L.	kA
Hendrickson	Hendrickson	k1gMnSc1
<g/>
;	;	kIx,
Alan	Alan	k1gMnSc1
G.	G.	kA
Marshall	Marshall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
High	High	k1gInSc1
Resolution	Resolution	k1gInSc4
Mass	Mass	k1gInSc4
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
708	#num#	k4
<g/>
–	–	k?
<g/>
719	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
203191	#num#	k4
<g/>
t.	t.	k?
PMID	PMID	kA
22263633	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
B.	B.	kA
T.	T.	kA
Ruotolo	Ruotola	k1gFnSc5
<g/>
;	;	kIx,
K.	K.	kA
J.	J.	kA
Gillig	Gillig	k1gMnSc1
<g/>
;	;	kIx,
A.	A.	kA
S.	S.	kA
Woods	Woodsa	k1gFnPc2
<g/>
;	;	kIx,
T.	T.	kA
F.	F.	kA
Egan	Egan	k1gMnSc1
<g/>
;	;	kIx,
M.	M.	kA
V.	V.	kA
Ugarov	Ugarov	k1gInSc1
<g/>
;	;	kIx,
J.	J.	kA
A.	A.	kA
Schultz	Schultz	k1gMnSc1
<g/>
;	;	kIx,
D.	D.	kA
H.	H.	kA
Russell	Russell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analysis	Analysis	k1gInSc1
of	of	k?
Phosphorylated	Phosphorylated	k1gInSc1
Peptides	Peptidesa	k1gFnPc2
by	by	kYmCp3nS
Ion	ion	k1gInSc1
Mobility-Mass	Mobility-Mass	k1gInSc4
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6727	#num#	k4
<g/>
–	–	k?
<g/>
6733	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
498009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15538797	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
B.	B.	kA
T.	T.	kA
Ruotolo	Ruotola	k1gFnSc5
<g/>
;	;	kIx,
G.	G.	kA
F.	F.	kA
Verbeck	Verbeck	k1gMnSc1
<g/>
;	;	kIx,
L.	L.	kA
M.	M.	kA
Thomson	Thomson	k1gMnSc1
<g/>
;	;	kIx,
A.	A.	kA
S.	S.	kA
Woods	Woodsa	k1gFnPc2
<g/>
;	;	kIx,
K.	K.	kA
J.	J.	kA
Gillig	Gillig	k1gMnSc1
<g/>
;	;	kIx,
D.	D.	kA
H.	H.	kA
Russell	Russell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distinguishing	Distinguishing	k1gInSc1
between	between	k2eAgInSc4d1
Phosphorylated	Phosphorylated	k1gInSc4
and	and	k?
Nonphosphorylated	Nonphosphorylated	k1gInSc1
Peptides	Peptides	k1gMnSc1
with	with	k1gMnSc1
Ion	ion	k1gInSc4
Mobility	mobilita	k1gFnSc2
<g/>
−	−	k?
<g/>
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Proteome	Proteom	k1gInSc5
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
303	#num#	k4
<g/>
–	–	k?
<g/>
306	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
pr	pr	k0
<g/>
0	#num#	k4
<g/>
25516	#num#	k4
<g/>
r.	r.	kA
PMID	PMID	kA
12645885	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
L.	L.	kA
Pasa-Tolic	Pasa-Tolice	k1gFnPc2
<g/>
;	;	kIx,
Y.	Y.	kA
Huang	Huang	k1gMnSc1
<g/>
;	;	kIx,
S.	S.	kA
Guan	Guan	k1gMnSc1
<g/>
;	;	kIx,
H.	H.	kA
S.	S.	kA
Kim	Kim	k1gMnSc1
<g/>
;	;	kIx,
A.	A.	kA
G.	G.	kA
Marshall	Marshall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ultrahigh-resolution	Ultrahigh-resolution	k1gInSc1
matrix-assisted	matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
Fourier	Fourier	k1gInSc1
transform	transform	k1gInSc1
ion	ion	k1gInSc1
cyclotron	cyclotron	k1gInSc4
resonance	resonance	k1gFnSc2
mass	mass	k6eAd1
spectra	spectr	k1gMnSc2
of	of	k?
peptides	peptides	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
825	#num#	k4
<g/>
–	–	k?
<g/>
833	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
jms	jms	k?
<g/>
.1190300607	.1190300607	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1995	#num#	k4
<g/>
JMSp	JMSp	k1gInSc1
<g/>
..	..	k?
<g/>
.30	.30	k4
<g/>
.	.	kIx.
<g/>
.825	.825	k4
<g/>
P.	P.	kA
↑	↑	k?
V.	V.	kA
V.	V.	kA
Laiko	Laiko	k1gNnSc4
<g/>
;	;	kIx,
M.	M.	kA
A.	A.	kA
Baldwin	Baldwin	k1gMnSc1
<g/>
;	;	kIx,
A.	A.	kA
L.	L.	kA
Burlingame	Burlingam	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atmospheric	Atmospheric	k1gMnSc1
pressure	pressur	k1gMnSc5
matrix-assisted	matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
652	#num#	k4
<g/>
–	–	k?
<g/>
657	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
990998	#num#	k4
<g/>
k.	k.	k?
PMID	PMID	kA
10701247	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
K.	K.	kA
Strupat	Strupat	k1gFnSc2
<g/>
;	;	kIx,
O.	O.	kA
Scheibner	Scheibner	k1gMnSc1
<g/>
;	;	kIx,
T.	T.	kA
Arrey	Arrea	k1gFnSc2
<g/>
;	;	kIx,
M.	M.	kA
Bromirski	Bromirsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biological	Biological	k1gFnPc2
Applications	Applications	k1gInSc4
of	of	k?
AP	ap	kA
MALDI	MALDI	kA
with	with	k1gMnSc1
Thermo	Therma	k1gFnSc5
Scientific	Scientifice	k1gFnPc2
Exactive	Exactiv	k1gInSc5
Orbitrap	Orbitrap	k1gMnSc1
MS	MS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thermo	Therma	k1gFnSc5
Scientific	Scientific	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V.	V.	kA
V.	V.	kA
Laiko	Laiko	k1gNnSc4
<g/>
;	;	kIx,
S.	S.	kA
C.	C.	kA
Moyer	Moyer	k1gMnSc1
<g/>
;	;	kIx,
R.	R.	kA
J.	J.	kA
Cotter	Cotter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atmospheric	Atmospheric	k1gMnSc1
pressure	pressur	k1gMnSc5
MALDI	MALDI	kA
<g/>
/	/	kIx~
<g/>
ion	ion	k1gInSc1
trap	trap	k1gInSc1
mass	mass	k1gInSc4
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5239	#num#	k4
<g/>
–	–	k?
<g/>
5243	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
530	#num#	k4
<g/>
d.	d.	k?
PMID	PMID	kA
11080870	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Yong	Yong	k1gMnSc1
Jie	Jie	k1gMnSc1
Li	li	k8xS
<g/>
;	;	kIx,
Yele	Yele	k1gInSc1
Sun	Sun	kA
<g/>
;	;	kIx,
Qi	Qi	k1gMnSc1
Zhang	Zhang	k1gMnSc1
<g/>
;	;	kIx,
Xue	Xue	k1gMnSc1
Li	li	k8xS
<g/>
;	;	kIx,
Mei	Mei	k1gFnSc1
Li	li	k9
<g/>
;	;	kIx,
Zhen	Zhen	k1gInSc1
Zhou	Zhous	k1gInSc2
<g/>
;	;	kIx,
Chak	Chak	k1gInSc1
K.	K.	kA
Chan	Chan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real-time	Real-tim	k1gInSc5
chemical	chemicat	k5eAaPmAgInS
characterization	characterization	k1gInSc4
of	of	k?
atmospheric	atmospherice	k1gInPc2
particulate	particulat	k1gInSc5
matter	matter	k1gMnSc1
in	in	k?
China	China	k1gFnSc1
<g/>
:	:	kIx,
A	a	k9
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atmospheric	Atmospheric	k1gMnSc1
Environment	Environment	k1gMnSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
270	#num#	k4
<g/>
–	–	k?
<g/>
304	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
atmosenv	atmosenv	k1gInSc1
<g/>
.2017	.2017	k4
<g/>
.02	.02	k4
<g/>
.027	.027	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2017	#num#	k4
<g/>
AtmEn	AtmEn	k1gInSc1
<g/>
.158	.158	k4
<g/>
.	.	kIx.
<g/>
.270	.270	k4
<g/>
L.	L.	kA
1	#num#	k4
2	#num#	k4
R.	R.	kA
Knochenmuss	Knochenmussa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ion	ion	k1gInSc1
formation	formation	k1gInSc4
mechanisms	mechanisms	k1gInSc1
in	in	k?
UV-MALDI	UV-MALDI	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analyst	Analyst	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
966	#num#	k4
<g/>
–	–	k?
<g/>
986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
b	b	k?
<g/>
605646	#num#	k4
<g/>
f.	f.	k?
PMID	PMID	kA
17047796	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2006	#num#	k4
<g/>
Ana	Ana	k1gFnPc2
<g/>
..	..	k?
<g/>
.131	.131	k4
<g/>
.	.	kIx.
<g/>
.966	.966	k4
<g/>
K.	K.	kA
1	#num#	k4
2	#num#	k4
Richard	Richarda	k1gFnPc2
Knochenmuss	Knochenmussa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Coupled	Coupled	k1gMnSc1
Chemical	Chemical	k1gMnSc1
and	and	k?
Physical	Physical	k1gFnSc2
Dynamics	Dynamicsa	k1gFnPc2
Model	model	k1gInSc1
of	of	k?
MALDI	MALDI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annual	Annual	k1gMnSc1
Review	Review	k1gMnSc1
of	of	k?
Analytical	Analytical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
365	#num#	k4
<g/>
–	–	k?
<g/>
385	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
1327	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
annurev-anchem-	annurev-anchem-	k?
<g/>
0	#num#	k4
<g/>
71015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
41750	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
27070182	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2016	#num#	k4
<g/>
ARAC	ARAC	kA
<g/>
...	...	k?
<g/>
.9	.9	k4
<g/>
.	.	kIx.
<g/>
.365	.365	k4
<g/>
K.	K.	kA
↑	↑	k?
I.	I.	kA
<g/>
-Chung	-Chung	k1gMnSc1
Lu	Lu	k1gMnSc1
<g/>
;	;	kIx,
Chuping	Chuping	k1gInSc1
Lee	Lea	k1gFnSc3
<g/>
;	;	kIx,
Yuan-Tseh	Yuan-Tseh	k1gInSc1
Lee	Lea	k1gFnSc3
<g/>
;	;	kIx,
Chi-Kung	Chi-Kung	k1gInSc1
Ni	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ionization	Ionization	k1gInSc1
Mechanism	Mechanism	k1gInSc4
of	of	k?
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annual	Annual	k1gMnSc1
Review	Review	k1gMnSc1
of	of	k?
Analytical	Analytical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
annurev-anchem-	annurev-anchem-	k?
<g/>
0	#num#	k4
<g/>
71114	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
40315	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
26132345	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2015	#num#	k4
<g/>
ARAC	ARAC	kA
<g/>
...	...	k?
<g/>
.8	.8	k4
<g/>
..	..	k?
<g/>
.21	.21	k4
<g/>
L.	L.	kA
↑	↑	k?
Ion-to-neutral	Ion-to-neutral	k1gMnSc1
ratio	ratio	k1gNnSc1
of	of	k?
2,5	2,5	k4
<g/>
-dihydroxybenzoic	-dihydroxybenzoice	k1gInPc2
acid	acid	k1gInSc4
in	in	k?
matrix-assisted	matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
955	#num#	k4
<g/>
–	–	k?
<g/>
963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1097	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
231	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.6534	.6534	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23592197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2013	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
..	..	k?
<g/>
.27	.27	k4
<g/>
.	.	kIx.
<g/>
.955	.955	k4
<g/>
T.	T.	kA
↑	↑	k?
Ion	ion	k1gInSc1
Intensity	intensita	k1gFnSc2
and	and	k?
Thermal	Thermal	k1gInSc1
Proton	proton	k1gInSc1
Transfer	transfer	k1gInSc1
in	in	k?
Ultraviolet	Ultraviolet	k1gInSc1
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Physical	Physical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc4
B.	B.	kA
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4132	#num#	k4
<g/>
–	–	k?
<g/>
4139	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1520	#num#	k4
<g/>
-	-	kIx~
<g/>
6106	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
jp	jp	k?
<g/>
5008076	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24707818	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ion-to-Neutral	Ion-to-Neutral	k1gMnSc1
Ratios	Ratios	k1gMnSc1
and	and	k?
Thermal	Thermal	k1gInSc1
Proton	proton	k1gInSc1
Transfer	transfer	k1gInSc1
in	in	k?
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1242	#num#	k4
<g/>
–	–	k?
<g/>
1251	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
1112	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
25851654	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2015	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.26	.26	k4
<g/>
.1242	.1242	k4
<g/>
L.	L.	kA
</s>
<s>
↑	↑	k?
Chuping	Chuping	k1gInSc1
Lee	Lea	k1gFnSc3
<g/>
;	;	kIx,
I.	I.	kA
<g/>
-Chung	-Chung	k1gMnSc1
Lu	Lu	k1gMnSc1
<g/>
;	;	kIx,
Hsu	Hsu	k1gMnSc1
Chen	Chen	k1gMnSc1
Hsu	Hsu	k1gMnSc1
<g/>
;	;	kIx,
Hou-Yu	Hou-Ya	k1gFnSc4
Lin	Lina	k1gFnPc2
<g/>
;	;	kIx,
Sheng-Ping	Sheng-Ping	k1gInSc1
Liang	Liang	k1gInSc1
<g/>
;	;	kIx,
Yuan-Tseh	Yuan-Tseh	k1gInSc1
Lee	Lea	k1gFnSc3
<g/>
;	;	kIx,
Chi-Kung	Chi-Kung	k1gInSc1
Ni	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formation	Formation	k1gInSc1
of	of	k?
Metal-Related	Metal-Related	k1gInSc1
Ions	Ions	k1gInSc1
in	in	k?
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization	Ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1491	#num#	k4
<g/>
–	–	k?
<g/>
1498	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
1424	#num#	k4
<g/>
-	-	kIx~
<g/>
y.	y.	k?
PMID	PMID	kA
27306427	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2016	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.27	.27	k4
<g/>
.1491	.1491	k4
<g/>
.	.	kIx.
↑	↑	k?
Sarah	Sarah	k1gFnSc1
Trimpin	Trimpin	k1gMnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Magic	Magic	k1gMnSc1
<g/>
"	"	kIx"
Ionization	Ionization	k1gInSc1
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
1424	#num#	k4
<g/>
-	-	kIx~
<g/>
y.	y.	k?
PMID	PMID	kA
26486514	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2016	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.27	.27	k4
<g/>
...	...	k?
<g/>
.4	.4	k4
<g/>
T.	T.	kA
↑	↑	k?
S.	S.	kA
Trimpin	Trimpin	k1gMnSc1
<g/>
;	;	kIx,
E.	E.	kA
D.	D.	kA
Inutan	Inutan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix	Matrix	k1gInSc1
Assisted	Assisted	k1gMnSc1
Ionization	Ionization	k1gInSc1
in	in	k?
Vacuum	Vacuum	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
Sensitive	sensitiv	k1gMnSc5
and	and	k?
Widely	Widel	k1gInPc1
Applicable	Applicable	k1gFnSc1
Ionization	Ionization	k1gInSc1
Method	Method	k1gInSc1
for	forum	k1gNnPc2
Mass	Mass	k1gInSc4
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
722	#num#	k4
<g/>
–	–	k?
<g/>
732	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
571	#num#	k4
<g/>
-	-	kIx~
<g/>
z.	z.	k?
PMID	PMID	kA
23526166	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2013	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.24	.24	k4
<g/>
.	.	kIx.
<g/>
.722	.722	k4
<g/>
T.	T.	kA
↑	↑	k?
MCEWEN	MCEWEN	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
N.	N.	kA
<g/>
;	;	kIx,
LARSEN	larsena	k1gFnPc2
<g/>
,	,	kIx,
Barbara	Barbara	k1gFnSc1
S.	S.	kA
Fifty	Fifta	k1gFnSc2
years	years	k6eAd1
of	of	k?
desorption	desorption	k1gInSc1
ionization	ionization	k1gInSc1
of	of	k?
nonvolatile	nonvolatile	k6eAd1
compounds	compounds	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
515	#num#	k4
<g/>
–	–	k?
<g/>
531	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1387	#num#	k4
<g/>
-	-	kIx~
<g/>
3806	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
ijms	ijms	k1gInSc1
<g/>
.2014	.2014	k4
<g/>
.07	.07	k4
<g/>
.018	.018	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2015	#num#	k4
<g/>
IJMSp	IJMSp	k1gInSc1
<g/>
.377	.377	k4
<g/>
.	.	kIx.
<g/>
.515	.515	k4
<g/>
M.	M.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ion-to-Neutral	Ion-to-Neutral	k1gMnSc1
Ratios	Ratios	k1gMnSc1
and	and	k?
Thermal	Thermal	k1gInSc1
Proton	proton	k1gInSc1
Transfer	transfer	k1gInSc1
in	in	k?
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1242	#num#	k4
<g/>
–	–	k?
<g/>
1251	#num#	k4
url	url	k?
=	=	kIx~
https://pubs.acs.org/doi/10.1021/jasms.8b05059.	https://pubs.acs.org/doi/10.1021/jasms.8b05059.	k4
ISSN	ISSN	kA
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
1112	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
25851654	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2015	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.26	.26	k4
<g/>
.1242	.1242	k4
<g/>
L.	L.	kA
↑	↑	k?
Kenneth	Kenneth	k1gMnSc1
N.	N.	kA
Robinson	Robinson	k1gMnSc1
<g/>
;	;	kIx,
Rory	Rora	k1gMnSc2
T.	T.	kA
Steven	Steven	k2eAgInSc1d1
<g/>
;	;	kIx,
Alan	alan	k1gInSc1
M.	M.	kA
Race	Race	k1gInSc4
<g/>
;	;	kIx,
Josephine	Josephin	k1gMnSc5
Bunch	Bun	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Influence	influence	k1gFnSc2
of	of	k?
MS	MS	kA
Imaging	Imaging	k1gInSc1
Parameters	Parameters	k1gInSc4
on	on	k3xPp3gMnSc1
UV-MALDI	UV-MALDI	k1gMnSc1
Desorption	Desorption	k1gInSc1
and	and	k?
Ion	ion	k1gInSc1
Yield	Yield	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1284	#num#	k4
<g/>
–	–	k?
<g/>
1293	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
13361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2193	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
30949969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2019	#num#	k4
<g/>
JASMS	JASMS	kA
<g/>
.	.	kIx.
<g/>
.30	.30	k4
<g/>
.1284	.1284	k4
<g/>
R.	R.	kA
↑	↑	k?
B.	B.	kA
Spengler	Spengler	k1gMnSc1
<g/>
;	;	kIx,
U.	U.	kA
Bahr	Bahr	k1gMnSc1
<g/>
;	;	kIx,
M.	M.	kA
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
Josephine	Josephin	k1gInSc5
Bunch	Bunch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postionization	Postionization	k1gInSc1
of	of	k?
Laser-Desorbed	Laser-Desorbed	k1gInSc1
Organic	Organice	k1gFnPc2
and	and	k?
Inorganic	Inorganice	k1gFnPc2
Compounds	Compounds	k1gInSc4
in	in	k?
a	a	k8xC
Time	Tim	k1gMnSc2
of	of	k?
Flight	Flight	k2eAgInSc4d1
Mass	Mass	k1gInSc4
Spectrometer	Spectrometra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instrumentation	Instrumentation	k1gInSc1
Science	Science	k1gFnSc2
&	&	k?
Technology	technolog	k1gMnPc7
<g/>
.	.	kIx.
1988	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
173	#num#	k4
<g/>
–	–	k?
<g/>
193	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1073	#num#	k4
<g/>
-	-	kIx~
<g/>
9149	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
10739148808543672	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1988	#num#	k4
<g/>
IS	IS	kA
<g/>
&	&	k?
<g/>
T.	T.	kA
<g/>
.	.	kIx.
<g/>
.17	.17	k4
<g/>
.	.	kIx.
<g/>
.173	.173	k4
<g/>
S.	S.	kA
</s>
<s>
↑	↑	k?
Klaus	Klaus	k1gMnSc1
Dreisewerd	Dreisewerd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Desorption	Desorption	k1gInSc4
Process	Process	k1gInSc1
in	in	k?
MALDI	MALDI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemical	Chemical	k1gFnSc1
Reviews	Reviewsa	k1gFnPc2
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
395	#num#	k4
<g/>
–	–	k?
<g/>
426	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2665	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
cr	cr	k0
<g/>
0	#num#	k4
<g/>
10375	#num#	k4
<g/>
i.	i.	k?
PMID	PMID	kA
12580636	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
J.	J.	kA
Soltwisch	Soltwisch	k1gMnSc1
<g/>
;	;	kIx,
H.	H.	kA
Kettling	Kettling	k1gInSc1
<g/>
;	;	kIx,
S.	S.	kA
Vens-Cappell	Vens-Cappell	k1gMnSc1
<g/>
;	;	kIx,
M.	M.	kA
Wiegelmann	Wiegelmann	k1gMnSc1
<g/>
;	;	kIx,
J.	J.	kA
Muthing	Muthing	k1gInSc1
<g/>
;	;	kIx,
K.	K.	kA
Dreisewerd	Dreisewerd	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Massa	k1gFnPc2
spectrometry	spectrometr	k1gInPc1
imaging	imaging	k1gInSc1
with	with	k1gMnSc1
laser-induced	laser-induced	k1gMnSc1
postionization	postionization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
211	#num#	k4
<g/>
–	–	k?
<g/>
215	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
8075	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.	.	kIx.
<g/>
aaa	aaa	k?
<g/>
1051	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
25745064	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2015	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.348	.348	k4
<g/>
.	.	kIx.
<g/>
.211	.211	k4
<g/>
S.	S.	kA
↑	↑	k?
Jens	Jens	k1gInSc1
Soltwisch	Soltwischa	k1gFnPc2
<g/>
;	;	kIx,
Bram	brama	k1gFnPc2
Heijs	Heijs	k1gInSc1
<g/>
;	;	kIx,
Annika	Annika	k1gFnSc1
Koch	Koch	k1gMnSc1
<g/>
;	;	kIx,
Simeon	Simeon	k1gMnSc1
Vens-Cappell	Vens-Cappell	k1gMnSc1
<g/>
;	;	kIx,
Jens	Jens	k1gInSc1
Höhndorf	Höhndorf	k1gMnSc1
<g/>
;	;	kIx,
Klaus	Klaus	k1gMnSc1
Dreisewerd	Dreisewerd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI-2	MALDI-2	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Trapped	Trapped	k1gInSc4
Ion	ion	k1gInSc1
Mobility	mobilita	k1gFnSc2
Quadrupole	Quadrupole	k1gFnSc2
Time-of-Flight	Time-of-Flight	k2eAgInSc4d1
Instrument	instrument	k1gInSc4
for	forum	k1gNnPc2
Rapid	rapid	k1gInSc1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
Imaging	Imaging	k1gInSc1
and	and	k?
Ion	ion	k1gInSc4
Mobility	mobilita	k1gFnSc2
Separation	Separation	k1gInSc1
of	of	k?
Complex	Complex	k1gInSc1
Lipid	lipid	k1gInSc1
Profiles	Profiles	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8697	#num#	k4
<g/>
–	–	k?
<g/>
8703	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
acs	acs	k?
<g/>
.	.	kIx.
<g/>
analchem	analch	k1gInSc7
<g/>
.0	.0	k4
<g/>
c	c	k0
<g/>
0	#num#	k4
<g/>
1747	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
32449347	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
S.	S.	kA
R.	R.	kA
Ellis	Ellis	k1gFnSc1
<g/>
;	;	kIx,
Jens	Jens	k1gInSc1
Soltwisch	Soltwisch	k1gInSc1
<g/>
;	;	kIx,
M.	M.	kA
R.	R.	kA
L.	L.	kA
Paine	Pain	k1gInSc5
<g/>
;	;	kIx,
K.	K.	kA
Dreisewerd	Dreisewerd	k1gMnSc1
<g/>
;	;	kIx,
R.	R.	kA
M.	M.	kA
A.	A.	kA
Heeren	Heerna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laser	laser	k1gInSc1
post-ionisation	post-ionisation	k1gInSc4
combined	combined	k1gMnSc1
with	with	k1gMnSc1
a	a	k8xC
high	high	k1gInSc1
resolving	resolving	k1gInSc1
power	power	k1gInSc1
orbitrap	orbitrap	k1gInSc1
mass	mass	k1gInSc1
spectrometer	spectrometra	k1gFnPc2
for	forum	k1gNnPc2
enhanced	enhanced	k1gMnSc1
MALDI-MS	MALDI-MS	k1gFnSc2
imaging	imaging	k1gInSc1
of	of	k?
lipids	lipids	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemical	Chemical	k1gFnSc1
Communications	Communicationsa	k1gFnPc2
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7246	#num#	k4
<g/>
–	–	k?
<g/>
7249	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1359	#num#	k4
<g/>
-	-	kIx~
<g/>
7345	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
7	#num#	k4
<g/>
CC	CC	kA
<g/>
0	#num#	k4
<g/>
2325	#num#	k4
<g/>
A.	A.	kA
PMID	PMID	kA
28573274	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
C.	C.	kA
Huberty	Hubert	k1gMnPc7
<g/>
;	;	kIx,
J.	J.	kA
E.	E.	kA
Vath	Vath	k1gMnSc1
<g/>
;	;	kIx,
W.	W.	kA
Yu	Yu	k1gMnSc1
<g/>
;	;	kIx,
S.	S.	kA
A.	A.	kA
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Site-specific	Site-specific	k1gMnSc1
carbohydrate	carbohydrat	k1gInSc5
identification	identification	k1gInSc4
in	in	k?
recombinant	recombinant	k1gInSc1
proteins	proteinsa	k1gFnPc2
using	using	k1gInSc4
MALD-TOF	MALD-TOF	k1gFnSc4
MS.	MS.	k1gMnPc2
Analytical	Analytical	k1gFnSc2
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2791	#num#	k4
<g/>
–	–	k?
<g/>
2800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
68	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
8250262	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
S.	S.	kA
Sekiya	Sekiya	k1gMnSc1
<g/>
;	;	kIx,
Y.	Y.	kA
Wada	Wada	k1gMnSc1
<g/>
;	;	kIx,
K.	K.	kA
Tanaka	Tanak	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Derivatization	Derivatization	k1gInSc1
for	forum	k1gNnPc2
Stabilizing	Stabilizing	k1gInSc1
Sialic	Sialice	k1gFnPc2
Acids	Acidsa	k1gFnPc2
in	in	k?
MALDI-	MALDI-	k1gMnSc1
<g/>
MS.	MS.	k1gMnSc1
Analytical	Analytical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4962	#num#	k4
<g/>
–	–	k?
<g/>
4968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
50287	#num#	k4
<g/>
o.	o.	k?
PMID	PMID	kA
16053310	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Y.	Y.	kA
Fukuyama	Fukuyama	k1gNnSc4
<g/>
;	;	kIx,
S.	S.	kA
Nakaya	Nakaya	k1gMnSc1
<g/>
;	;	kIx,
Y.	Y.	kA
Yamazaki	Yamazak	k1gFnSc2
<g/>
;	;	kIx,
K.	K.	kA
Tanaka	Tanak	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ionic	Ionic	k1gMnSc1
Liquid	Liquida	k1gFnPc2
Matrixes	Matrixes	k1gMnSc1
Optimized	Optimized	k1gMnSc1
for	forum	k1gNnPc2
MALDI-MS	MALDI-MS	k1gMnSc7
of	of	k?
Sulfated	Sulfated	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Sialylated	Sialylated	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Neutral	Neutral	k1gMnSc1
Oligosaccharides	Oligosaccharides	k1gMnSc1
and	and	k?
Glycopeptides	Glycopeptides	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2171	#num#	k4
<g/>
–	–	k?
<g/>
2179	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
7021986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
18275166	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
D.	D.	kA
I.	I.	kA
Papac	Papac	k1gFnSc1
<g/>
;	;	kIx,
A.	A.	kA
Wong	Wong	k1gMnSc1
<g/>
;	;	kIx,
A.	A.	kA
J.	J.	kA
S.	S.	kA
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analysis	Analysis	k1gFnSc1
of	of	k?
Acidic	Acidic	k1gMnSc1
Oligosaccharides	Oligosaccharides	k1gMnSc1
and	and	k?
Glycopeptides	Glycopeptides	k1gInSc1
by	by	kYmCp3nS
Matrix-Assisted	Matrix-Assisted	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
Time-of-Flight	Time-of-Flight	k2eAgInSc1d1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3215	#num#	k4
<g/>
–	–	k?
<g/>
3223	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
960324	#num#	k4
<g/>
z.	z.	k?
PMID	PMID	kA
8797382	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
D.	D.	kA
J.	J.	kA
Harvey	Harvea	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix-assisted	Matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
of	of	k?
carbohydrates	carbohydrates	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
Reviews	Reviews	k1gInSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
349	#num#	k4
<g/>
–	–	k?
<g/>
451	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
110639030	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1999	#num#	k4
<g/>
MSRv	MSRva	k1gFnPc2
<g/>
..	..	k?
<g/>
.18	.18	k4
<g/>
.	.	kIx.
<g/>
.349	.349	k4
<g/>
H.	H.	kA
↑	↑	k?
E.	E.	kA
Lattova	Lattův	k2eAgFnSc1d1
<g/>
;	;	kIx,
V.	V.	kA
C.	C.	kA
Chen	Chen	k1gMnSc1
<g/>
;	;	kIx,
S.	S.	kA
Varma	Varma	k1gFnSc1
<g/>
;	;	kIx,
T.	T.	kA
Bezabeh	Bezabeh	k1gMnSc1
<g/>
;	;	kIx,
H.	H.	kA
Perreault	Perreault	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix-assisted	Matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
on-target	on-target	k1gInSc1
method	methoda	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
investigation	investigation	k1gInSc1
of	of	k?
oligosaccharides	oligosaccharides	k1gInSc1
and	and	k?
glycosylation	glycosylation	k1gInSc1
sites	sites	k1gMnSc1
in	in	k?
glycopeptides	glycopeptides	k1gMnSc1
and	and	k?
glycoproteins	glycoproteins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1644	#num#	k4
<g/>
–	–	k?
<g/>
1650	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
rcm	rcm	k?
<g/>
.3007	.3007	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
17465012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2007	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
..	..	k?
<g/>
.21	.21	k4
<g/>
.1644	.1644	k4
<g/>
L.	L.	kA
↑	↑	k?
M.	M.	kA
Tajiri	Tajir	k1gFnSc2
<g/>
;	;	kIx,
T.	T.	kA
Takeuchi	Takeuch	k1gFnSc2
<g/>
;	;	kIx,
Y.	Y.	kA
Wada	Wada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distinct	Distinct	k1gMnSc1
Features	Features	k1gMnSc1
of	of	k?
Matrix-Assisted	Matrix-Assisted	k1gMnSc1
6	#num#	k4
μ	μ	k1gInSc1
Infrared	Infrared	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
in	in	k?
Biomolecular	Biomolecular	k1gInSc4
Analysis	Analysis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6750	#num#	k4
<g/>
–	–	k?
<g/>
6755	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
900695	#num#	k4
<g/>
q.	q.	k?
PMID	PMID	kA
19627133	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
A.	A.	kA
M.	M.	kA
Distler	Distler	k1gMnSc1
<g/>
;	;	kIx,
J.	J.	kA
Allison	Allison	k1gNnSc4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
-Methoxysalicylic	-Methoxysalicylice	k1gFnPc2
acid	acida	k1gFnPc2
and	and	k?
spermine	spermin	k1gInSc5
<g/>
:	:	kIx,
A	a	k9
new	new	k?
matrix	matrix	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
matrix-assisted	matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
analysis	analysis	k1gFnSc2
of	of	k?
oligonucleotides	oligonucleotides	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
456	#num#	k4
<g/>
–	–	k?
<g/>
462	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
212	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11322192	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
W.	W.	kA
Schrepp	Schrepp	k1gMnSc1
<g/>
;	;	kIx,
H.	H.	kA
Pasch	Pasch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI-TOF	MALDI-TOF	k1gFnSc1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
of	of	k?
Synthetic	Synthetice	k1gFnPc2
Polymers	Polymers	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer-Verlag	Springer-Verlag	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
540	#num#	k4
<g/>
-	-	kIx~
<g/>
44259	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
W.	W.	kA
F.	F.	kA
Nielen	Nielna	k1gFnPc2
<g/>
;	;	kIx,
S.	S.	kA
Malucha	Malucha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Characterization	Characterization	k1gInSc1
of	of	k?
polydisperse	polydisperse	k1gFnSc2
synthetic	synthetice	k1gFnPc2
polymers	polymers	k6eAd1
by	by	kYmCp3nS
size-exclusion	size-exclusion	k1gInSc1
chromatography	chromatographa	k1gFnSc2
<g/>
/	/	kIx~
<g/>
matrix-assisted	matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc4
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc4
time-of-flight	time-of-flight	k5eAaPmF
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1194	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
SICI	SICI	kA
<g/>
)	)	kIx)
<g/>
1097	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
231	#num#	k4
<g/>
(	(	kIx(
<g/>
199707	#num#	k4
<g/>
)	)	kIx)
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
<	<	kIx(
<g/>
1194	#num#	k4
<g/>
::	::	k?
<g/>
AID-RCM	AID-RCM	k1gFnSc1
<g/>
935	#num#	k4
<g/>
>	>	kIx)
<g/>
3.0	3.0	k4
<g/>
.	.	kIx.
<g/>
CO	co	k6eAd1
<g/>
;	;	kIx,
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
L.	L.	kA
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1997	#num#	k4
<g/>
RCMS	RCMS	kA
<g/>
..	..	k?
<g/>
.11	.11	k4
<g/>
.1194	.1194	k4
<g/>
N.	N.	kA
↑	↑	k?
K.	K.	kA
J.	J.	kA
Wu	Wu	k1gMnSc1
<g/>
;	;	kIx,
R.	R.	kA
W.	W.	kA
Odom	Odom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Characterizing	Characterizing	k1gInSc1
synthetic	synthetice	k1gFnPc2
polymers	polymers	k6eAd1
by	by	kYmCp3nS
MALDI	MALDI	kA
MS.	MS.	k1gMnSc1
Analytical	Analytical	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
456	#num#	k4
<g/>
A	a	k8xC
<g/>
–	–	k?
<g/>
461	#num#	k4
<g/>
A.	A.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
981910	#num#	k4
<g/>
q.	q.	k?
PMID	PMID	kA
9666717	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
D.	D.	kA
C.	C.	kA
Schriemer	Schriemer	k1gMnSc1
<g/>
;	;	kIx,
L.	L.	kA
Li	li	k8xS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Mass	k1gInSc1
Discrimination	Discrimination	k1gInSc4
in	in	k?
the	the	k?
Analysis	Analysis	k1gFnSc2
of	of	k?
Polydisperse	Polydisperse	k1gFnSc2
Polymers	Polymersa	k1gFnPc2
by	by	kYmCp3nP
MALDI	MALDI	kA
Time-of-Flight	Time-of-Flight	k2eAgInSc1d1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instrumental	Instrumental	k1gMnSc1
Issues	Issues	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4176	#num#	k4
<g/>
–	–	k?
<g/>
4183	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
9707794	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Audrey	Audrea	k1gFnSc2
M.	M.	kA
<g/>
;	;	kIx,
Jason	Jason	k1gInSc1
A.	A.	kA
Moss	Moss	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Optimized	Optimized	k1gInSc1
sample	sample	k6eAd1
preparation	preparation	k1gInSc1
for	forum	k1gNnPc2
MALDI	MALDI	kA
mass	massa	k1gFnPc2
spectrometry	spectrometr	k1gInPc1
analysis	analysis	k1gFnSc2
of	of	k?
protected	protected	k1gInSc1
synthetic	synthetice	k1gFnPc2
peptides	peptidesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
614	#num#	k4
<g/>
–	–	k?
<g/>
619	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jasms	jasms	k1gInSc1
<g/>
.2008	.2008	k4
<g/>
.01	.01	k4
<g/>
.010	.010	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
18295503	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ute	Ute	k1gMnSc1
Bahr	Bahr	k1gMnSc1
<g/>
;	;	kIx,
Andreas	Andreas	k1gMnSc1
Deppe	Depp	k1gInSc5
<g/>
;	;	kIx,
Michael	Michael	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
;	;	kIx,
Franz	Franz	k1gMnSc1
Hillenkamp	Hillenkamp	k1gMnSc1
<g/>
;	;	kIx,
Ulrich	Ulrich	k1gMnSc1
Giessmann	Giessmann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Mass	k1gInSc1
spectrometry	spectrometr	k1gInPc1
of	of	k?
synthetic	synthetice	k1gFnPc2
polymers	polymers	k6eAd1
by	by	kYmCp3nS
UV-matrix-assisted	UV-matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
ionization	ionization	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2866	#num#	k4
<g/>
–	–	k?
<g/>
2869	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
36	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
P.	P.	kA
Seng	Seng	k1gMnSc1
<g/>
;	;	kIx,
M.	M.	kA
Drancourt	Drancourt	k1gInSc1
<g/>
;	;	kIx,
F.	F.	kA
Gouriet	Gouriet	k1gMnSc1
<g/>
;	;	kIx,
B.	B.	kA
La	la	k1gNnSc1
Scola	Scola	k1gMnSc1
<g/>
;	;	kIx,
P.	P.	kA
E.	E.	kA
Fournier	Fournier	k1gMnSc1
<g/>
;	;	kIx,
J.	J.	kA
M.	M.	kA
Rolain	Rolain	k1gMnSc1
<g/>
;	;	kIx,
D.	D.	kA
Raoult	Raoult	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ongoing	Ongoing	k1gInSc1
revolution	revolution	k1gInSc4
in	in	k?
bacteriology	bacteriolog	k1gMnPc7
<g/>
:	:	kIx,
routine	routin	k1gInSc5
identification	identification	k1gInSc4
of	of	k?
bacteria	bacterium	k1gNnSc2
by	by	k9
matrix-assisted	matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
ionization	ionization	k1gInSc1
time-of-flight	time-of-flight	k2eAgInSc1d1
mass	mass	k1gInSc1
spectrometry	spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clinical	Clinical	k1gMnSc1
Infectious	Infectious	k1gMnSc1
Diseases	Diseases	k1gMnSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
552	#num#	k4
<g/>
–	–	k?
<g/>
553	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
600885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19583519	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Todd	Todd	k1gInSc1
R.	R.	kA
Sandrin	Sandrin	k1gInSc1
<g/>
;	;	kIx,
Jason	Jason	k1gInSc1
E.	E.	kA
Goldstein	Goldstein	k1gInSc1
<g/>
;	;	kIx,
Stephanie	Stephanie	k1gFnSc1
Schumaker	Schumakra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI	MALDI	kA
TOF	TOF	kA
MS	MS	kA
profiling	profiling	k1gInSc1
of	of	k?
bacteria	bacterium	k1gNnSc2
at	at	k?
the	the	k?
strain	strain	k2eAgInSc4d1
level	level	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
Reviews	Reviews	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
188	#num#	k4
<g/>
–	–	k?
<g/>
217	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
277	#num#	k4
<g/>
-	-	kIx~
<g/>
7037	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
mas	masa	k1gFnPc2
<g/>
.21359	.21359	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
22996584	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2013	#num#	k4
<g/>
MSRv	MSRva	k1gFnPc2
<g/>
..	..	k?
<g/>
.32	.32	k4
<g/>
.	.	kIx.
<g/>
.188	.188	k4
<g/>
S.	S.	kA
↑	↑	k?
Kevin	Kevin	k1gMnSc1
M.	M.	kA
Downard	Downard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proteotyping	Proteotyping	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
rapid	rapid	k1gInSc1
identification	identification	k1gInSc1
of	of	k?
influenza	influenza	k1gFnSc1
virus	virus	k1gInSc1
and	and	k?
other	other	k1gInSc1
biopathogens	biopathogens	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemical	Chemical	k1gFnSc1
Society	societa	k1gFnSc2
Reviews	Reviewsa	k1gFnPc2
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8584	#num#	k4
<g/>
–	–	k?
<g/>
8595	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1460	#num#	k4
<g/>
-	-	kIx~
<g/>
4744	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
3	#num#	k4
<g/>
cs	cs	k?
<g/>
60081	#num#	k4
<g/>
e.	e.	k?
PMID	PMID	kA
23632861	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
D.	D.	kA
D.	D.	kA
Rhoads	Rhoadsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
presence	presence	k1gFnSc1
of	of	k?
a	a	k8xC
single	singl	k1gInSc5
MALDI-TOF	MALDI-TOF	k1gMnSc4
mass	mass	k1gInSc1
spectral	spectrat	k5eAaPmAgInS,k5eAaImAgInS
peak	peak	k6eAd1
predicts	predicts	k1gInSc1
methicillin	methicillina	k1gFnPc2
resistance	resistance	k1gFnSc2
in	in	k?
staphylococci	staphylococce	k1gFnSc4
<g/>
..	..	k?
Diagn	Diagn	k1gMnSc1
Microbiol	Microbiola	k1gFnPc2
Infect	Infect	k1gMnSc1
Dis.	Dis.	k1gMnSc1
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
–	–	k?
<g/>
261	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
diagmicrobio	diagmicrobio	k6eAd1
<g/>
.2016	.2016	k4
<g/>
.08	.08	k4
<g/>
.001	.001	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
27568365	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
A	a	k9
simple	simple	k6eAd1
<g/>
,	,	kIx,
robust	robust	k1gInSc1
and	and	k?
rapid	rapid	k1gInSc1
approach	approach	k1gInSc1
to	ten	k3xDgNnSc1
detect	detect	k2eAgMnSc1d1
carbapenemases	carbapenemases	k1gMnSc1
in	in	k?
Gram-negative	Gram-negativ	k1gInSc5
isolates	isolatesa	k1gFnPc2
by	by	kYmCp3nP
MALDI-TOF	MALDI-TOF	k1gMnPc1
mass	massa	k1gFnPc2
spectrometry	spectrometr	k1gInPc4
validation	validation	k1gInSc1
with	with	k1gInSc1
triple	tripl	k1gInSc5
quadripole	quadripola	k1gFnSc3
tandem	tandem	k1gInSc1
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
<g/>
,	,	kIx,
microarray	microarray	k1gInPc4
and	and	k?
PCR	PCR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clinical	Clinical	k1gFnSc1
Microbiology	Microbiolog	k1gMnPc4
and	and	k?
Infection	Infection	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
O	o	k7c4
<g/>
1106	#num#	k4
<g/>
-	-	kIx~
<g/>
1112	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1469	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
691.127	691.127	k4
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24930405	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
N.	N.	kA
Abouseada	Abouseada	k1gFnSc1
<g/>
;	;	kIx,
M.	M.	kA
Raouf	Raouf	k1gMnSc1
<g/>
;	;	kIx,
E.	E.	kA
El-Attar	El-Attar	k1gMnSc1
<g/>
;	;	kIx,
P.	P.	kA
Moez	Moez	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix-assisted	Matrix-assisted	k1gInSc1
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
ionisation	ionisation	k1gInSc1
time-of-flight	time-of-flighta	k1gFnPc2
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc1
rapid	rapid	k1gInSc1
detection	detection	k1gInSc1
of	of	k?
carbapenamase	carbapenamase	k6eAd1
activity	activita	k1gFnSc2
in	in	k?
Acinetobacter	Acinetobacter	k1gMnSc1
baumannii	baumannie	k1gFnSc4
isolates	isolatesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indian	Indiana	k1gFnPc2
Journal	Journal	k1gFnPc2
of	of	k?
Medical	Medical	k1gMnSc1
Microbiology	Microbiolog	k1gMnPc7
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
85	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.410	10.410	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
255	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
857.202	857.202	k4
<g/>
335	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
28303824	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
C.	C.	kA
Sakarikou	Sakarika	k1gFnSc7
<g/>
;	;	kIx,
M.	M.	kA
Ciotti	Ciotť	k1gFnSc2
<g/>
;	;	kIx,
C.	C.	kA
Dolfa	Dolf	k1gMnSc2
<g/>
;	;	kIx,
S.	S.	kA
Angeletti	Angeletť	k1gFnSc2
<g/>
;	;	kIx,
C.	C.	kA
Favalli	Favalle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc1
detection	detection	k1gInSc4
of	of	k?
carbapenemase-producing	carbapenemase-producing	k1gInSc1
Klebsiella	Klebsiell	k1gMnSc2
pneumoniae	pneumonia	k1gMnSc2
strains	strains	k6eAd1
derived	derived	k1gMnSc1
from	from	k1gMnSc1
blood	blooda	k1gFnPc2
cultures	cultures	k1gMnSc1
by	by	k9
Matrix-Assisted	Matrix-Assisted	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization-Time	Ionization-Tim	k1gInSc5
of	of	k?
Flight	Flight	k2eAgInSc1d1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
(	(	kIx(
<g/>
MALDI-TOF	MALDI-TOF	k1gFnSc1
MS	MS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMC	BMC	kA
Microbiol	Microbiola	k1gFnPc2
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.118	10.118	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
12866	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
952	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
28274205	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
A	a	k8xC
Rapid	rapid	k1gInSc1
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization	Ionization	k1gInSc1
<g/>
–	–	k?
<g/>
Time	Time	k1gInSc1
of	of	k?
Flight	Flight	k2eAgInSc1d1
Mass	Mass	k1gInSc1
Spectrometry-Based	Spectrometry-Based	k1gInSc1
Method	Methoda	k1gFnPc2
for	forum	k1gNnPc2
Single-Plasmid	Single-Plasmid	k1gInSc1
Tracking	Tracking	k1gInSc1
in	in	k?
an	an	k?
Outbreak	Outbreak	k1gMnSc1
of	of	k?
Carbapenem-Resistant	Carbapenem-Resistant	k1gMnSc1
Enterobacteriaceae	Enterobacteriacea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gMnSc1
Microbiology	Microbiolog	k1gMnPc7
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2804	#num#	k4
<g/>
-	-	kIx~
<g/>
2812	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
JCM	JCM	kA
<g/>
.00694	.00694	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
4136129	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
C.	C.	kA
C.	C.	kA
Avila	Avila	k1gMnSc1
<g/>
;	;	kIx,
F.	F.	kA
G.	G.	kA
Almeida	Almeida	k1gFnSc1
<g/>
;	;	kIx,
G.	G.	kA
Palmisano	Palmisana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Direct	Direct	k2eAgInSc1d1
identification	identification	k1gInSc1
of	of	k?
trypanosomatids	trypanosomatids	k1gInSc1
by	by	kYmCp3nS
matrix-assisted	matrix-assisted	k1gInSc4
laser	laser	k1gInSc1
desorption	desorption	k1gInSc1
ionization-time	ionization-timat	k5eAaPmIp3nS
of	of	k?
flight	flight	k5eAaPmF
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
(	(	kIx(
<g/>
DIT	Dita	k1gFnPc2
MALDI-TOF	MALDI-TOF	k1gMnPc2
MS	MS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
549	#num#	k4
<g/>
-	-	kIx~
<g/>
557	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1076	#num#	k4
<g/>
-	-	kIx~
<g/>
5174	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
jms	jms	k?
<g/>
.3763	.3763	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
27659938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2016	#num#	k4
<g/>
JMSp	JMSp	k1gInSc1
<g/>
..	..	k?
<g/>
.51	.51	k4
<g/>
.	.	kIx.
<g/>
.549	.549	k4
<g/>
A.	A.	kA
↑	↑	k?
LACHAUD	LACHAUD	kA
<g/>
,	,	kIx,
Laurence	Laurence	k1gFnSc1
<g/>
;	;	kIx,
FERNÁNDEZ-ARÉVALO	FERNÁNDEZ-ARÉVALO	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
;	;	kIx,
NORMAND	NORMAND	k?
<g/>
,	,	kIx,
Anne-Cécile	Anne-Cécila	k1gFnSc6
<g/>
;	;	kIx,
LAMI	LAMI	kA
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
<g/>
;	;	kIx,
NABET	NABET	kA
<g/>
,	,	kIx,
Cécile	Cécila	k1gFnSc6
<g/>
;	;	kIx,
DONNADIEU	DONNADIEU	kA
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
Luc	Luc	k1gMnSc1
<g/>
;	;	kIx,
PIARROUX	PIARROUX	kA
<g/>
,	,	kIx,
Martine	Martin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identification	Identification	k1gInSc1
of	of	k?
Leishmania	Leishmanium	k1gNnSc2
by	by	k9
Matrix-Assisted	Matrix-Assisted	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization	Ionization	k1gInSc1
<g/>
–	–	k?
<g/>
Time	Time	k1gInSc1
of	of	k?
Flight	Flight	k1gInSc1
(	(	kIx(
<g/>
MALDI-TOF	MALDI-TOF	k1gFnSc1
<g/>
)	)	kIx)
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
Using	Using	k1gInSc1
a	a	k8xC
Free	Free	k1gFnSc1
Web-Based	Web-Based	k1gInSc1
Application	Application	k1gInSc1
and	and	k?
a	a	k8xC
Dedicated	Dedicated	k1gInSc1
Mass-Spectral	Mass-Spectral	k1gFnSc2
Library	Librara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gMnSc1
Microbiology	Microbiolog	k1gMnPc7
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2924	#num#	k4
<g/>
–	–	k?
<g/>
2933	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
1137	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
JCM	JCM	kA
<g/>
.00845	.00845	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
28724559	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Maureen	Maureen	k2eAgInSc4d1
Laroche	Laroche	k1gInSc4
<g/>
;	;	kIx,
Lionel	Lionel	k1gMnSc1
Almeras	Almeras	k1gMnSc1
<g/>
;	;	kIx,
Emilie	Emilie	k1gFnSc1
Pecchi	Pecch	k1gFnSc2
<g/>
;	;	kIx,
Yassina	Yassina	k1gFnSc1
Bechah	Bechah	k1gMnSc1
<g/>
;	;	kIx,
Didier	Didier	k1gMnSc1
Raoult	Raoult	k1gMnSc1
<g/>
;	;	kIx,
Angè	Angè	k1gFnSc1
Viola	Viola	k1gFnSc1
<g/>
;	;	kIx,
Philippe	Philipp	k1gInSc5
Parola	parola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI-TOF	MALDI-TOF	k1gFnSc1
MS	MS	kA
as	as	k9
an	an	k?
innovative	innovativ	k1gInSc5
tool	tool	k1gInSc1
for	forum	k1gNnPc2
detection	detection	k1gInSc1
of	of	k?
Plasmodium	plasmodium	k1gNnSc1
parasites	parasites	k1gMnSc1
in	in	k?
Anopheles	Anopheles	k1gMnSc1
mosquitoes	mosquitoes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malaria	Malarium	k1gNnPc1
Journal	Journal	k1gFnPc2
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
549	#num#	k4
<g/>
-	-	kIx~
<g/>
557	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1475	#num#	k4
<g/>
-	-	kIx~
<g/>
2875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.118	10.118	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
12936	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
1657	#num#	k4
<g/>
-	-	kIx~
<g/>
z.	z.	k?
PMID	PMID	kA
28049524	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Basma	Basma	k1gFnSc1
Ouarti	Ouart	k1gMnPc1
<g/>
;	;	kIx,
Maureen	Maureen	k2eAgInSc4d1
Laroche	Laroche	k1gInSc4
<g/>
;	;	kIx,
Souad	Souad	k1gInSc1
Righi	Righ	k1gFnSc2
<g/>
;	;	kIx,
Mohamed	Mohamed	k1gMnSc1
Nadir	nadir	k1gInSc4
Meguini	Meguin	k1gMnPc1
<g/>
;	;	kIx,
Ahmed	Ahmed	k1gMnSc1
Benakhla	Benakhla	k1gMnSc1
<g/>
;	;	kIx,
Didier	Didier	k1gMnSc1
Raoult	Raoult	k1gMnSc1
<g/>
;	;	kIx,
Philippe	Philipp	k1gInSc5
Parola	parola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Development	Development	k1gInSc1
of	of	k?
MALDI-TOF	MALDI-TOF	k1gMnSc2
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc4
for	forum	k1gNnPc2
the	the	k?
identification	identification	k1gInSc1
of	of	k?
lice	licat	k5eAaPmIp3nS
isolated	isolated	k1gInSc4
from	from	k6eAd1
farm	farm	k6eAd1
animals	animals	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parasite	parasit	k1gMnSc5
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1776	#num#	k4
<g/>
-	-	kIx~
<g/>
1042	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
parasite	parasit	k1gMnSc5
<g/>
/	/	kIx~
<g/>
2020026	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
32351208	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Antoine	Antoin	k1gInSc5
Huguenin	Huguenin	k2eAgInSc4d1
<g/>
;	;	kIx,
Isabelle	Isabelle	k1gFnSc1
Villena	Villena	k1gFnSc1
<g/>
;	;	kIx,
Hubert	Hubert	k1gMnSc1
Ferté	Ferta	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MALDI-TOF	MALDI-TOF	k1gFnPc2
mass	mass	k6eAd1
spectrometry	spectrometr	k1gInPc1
<g/>
:	:	kIx,
a	a	k8xC
new	new	k?
tool	toolum	k1gNnPc2
for	forum	k1gNnPc2
rapid	rapid	k1gInSc1
identification	identification	k1gInSc1
of	of	k?
cercariae	cercaria	k1gMnSc2
(	(	kIx(
<g/>
Trematoda	trematod	k1gMnSc2
<g/>
,	,	kIx,
Digenea	Digene	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parasite	parasit	k1gMnSc5
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1776	#num#	k4
<g/>
-	-	kIx~
<g/>
1042	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
parasite	parasit	k1gMnSc5
<g/>
/	/	kIx~
<g/>
2019011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
30838972	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sim	sima	k1gFnPc2
<g/>
,	,	kIx,
K.	K.	kA
<g/>
;	;	kIx,
Shaw	Shaw	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
G.	G.	kA
<g/>
;	;	kIx,
Randell	Randell	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
Cox	Cox	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
J.	J.	kA
<g/>
;	;	kIx,
McClure	McClur	k1gMnSc5
<g/>
,	,	kIx,
Z.	Z.	kA
E.	E.	kA
<g/>
;	;	kIx,
Li	li	k8xS
<g/>
,	,	kIx,
M.	M.	kA
S.	S.	kA
<g/>
;	;	kIx,
Haddad	Haddad	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
Langford	Langford	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
R.	R.	kA
<g/>
;	;	kIx,
Cookson	Cookson	k1gMnSc1
<g/>
,	,	kIx,
W.	W.	kA
O.	O.	kA
<g/>
;	;	kIx,
Moffatt	Moffatt	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
F.	F.	kA
<g/>
;	;	kIx,
Kroll	Kroll	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
S.	S.	kA
Dysbiosis	Dysbiosis	k1gFnSc1
anticipating	anticipating	k1gInSc1
necrotizing	necrotizing	k1gInSc1
enterocolitis	enterocolitis	k1gFnSc1
in	in	k?
very	vera	k1gFnSc2
premature	prematur	k1gMnSc5
infants	infantsit	k5eAaPmRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clin	Clina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infect	Infecta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dis.	Dis.	k1gFnSc1
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrabák	Hrabák	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Detection	Detection	k1gInSc1
of	of	k?
Carbapenemases	Carbapenemases	k1gInSc1
Using	Using	k1gInSc1
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
Time-of-Flight	Time-of-Flight	k2eAgInSc1d1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
(	(	kIx(
<g/>
MALDI-TOF	MALDI-TOF	k1gFnSc1
MS	MS	kA
<g/>
)	)	kIx)
Meropenem	Meropeno	k1gNnSc7
Hydrolysis	Hydrolysis	k1gFnSc2
Assay	Assaa	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Methods	Methods	k1gInSc1
in	in	k?
Molecular	Molecular	k1gInSc1
Biology	biolog	k1gMnPc7
(	(	kIx(
<g/>
1064	#num#	k4
<g/>
-	-	kIx~
<g/>
3745	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1237	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
91	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A.	A.	kA
S.	S.	kA
Woods	Woodsa	k1gFnPc2
<g/>
;	;	kIx,
J.	J.	kA
S.	S.	kA
Buchsbaum	Buchsbaum	k1gNnSc4
<g/>
;	;	kIx,
T.	T.	kA
A.	A.	kA
Worrall	Worrall	k1gMnSc1
<g/>
;	;	kIx,
J.	J.	kA
M.	M.	kA
Berg	Berg	k1gMnSc1
<g/>
;	;	kIx,
R.	R.	kA
J.	J.	kA
Cotter	Cotter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrix-Assisted	Matrix-Assisted	k1gInSc1
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
of	of	k?
Non-covalently	Non-covalently	k1gFnSc2
Bound	Bound	k1gMnSc1
Compounds	Compounds	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analytical	Analytical	k1gFnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4462	#num#	k4
<g/>
–	–	k?
<g/>
4465	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ac	ac	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
120	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
J.	J.	kA
J.	J.	kA
G.	G.	kA
Kisela	Kisela	k1gFnSc1
<g/>
;	;	kIx,
K.	K.	kA
M.	M.	kA
Downard	Downard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preservation	Preservation	k1gInSc1
and	and	k?
Detection	Detection	k1gInSc1
of	of	k?
Specific	Specifice	k1gInPc2
Antibody-Peptide	Antibody-Peptid	k1gInSc5
Complexes	Complexes	k1gInSc1
by	by	kYmCp3nS
Matrix-Assisted	Matrix-Assisted	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ionization	Ionization	k1gInSc1
Mass	Massa	k1gFnPc2
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gMnSc1
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
746	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
305	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
144	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10937798	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
K.	K.	kA
M.	M.	kA
Downard	Downard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Softly	Softly	k1gMnSc1
<g/>
,	,	kIx,
Softly	Softly	k1gMnSc1
<g/>
—	—	k?
<g/>
Detection	Detection	k1gInSc1
of	of	k?
Protein	protein	k1gInSc1
Complexes	Complexes	k1gInSc1
by	by	kYmCp3nS
Matrix‐	Matrix‐	k1gInSc4
Laser	laser	k1gInSc1
Desorption	Desorption	k1gInSc1
Ionization	Ionization	k1gInSc1
Mass	Mass	k1gInSc1
Spectrometry	Spectrometr	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wiley	Wiley	k1gInPc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780470146330	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
9780470146330	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
25	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
</s>
