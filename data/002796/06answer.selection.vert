<s>
Po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
demo	demo	k2eAgFnPc2d1	demo
nahrávek	nahrávka	k1gFnPc2	nahrávka
vydala	vydat	k5eAaPmAgFnS	vydat
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Ritual	Ritual	k1gMnSc1	Ritual
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
další	další	k2eAgInSc4d1	další
s	s	k7c7	s
názvem	název	k1gInSc7	název
Jilemnický	jilemnický	k2eAgMnSc1d1	jilemnický
okultista	okultista	k1gMnSc1	okultista
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
undergroundu	underground	k1gInSc2	underground
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
kultovní	kultovní	k2eAgInSc4d1	kultovní
status	status	k1gInSc4	status
dokonce	dokonce	k9	dokonce
i	i	k9	i
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
