<s>
Klávesa	klávesa	k1gFnSc1
Delete	Dele	k1gNnSc2
</s>
<s>
Klávesa	klávesa	k1gFnSc1
DELETE	DELETE	kA
z	z	k7c2
běžné	běžný	k2eAgFnSc2d1
klávesnice	klávesnice	k1gFnSc2
</s>
<s>
Klávesa	klávesa	k1gFnSc1
Delete	Dele	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
odstranit	odstranit	k5eAaPmF
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
počítačové	počítačový	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
systémových	systémový	k2eAgFnPc2d1
kláves	klávesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značí	značit	k5eAaImIp3nS
se	s	k7c7
zkratkou	zkratka	k1gFnSc7
Del	Del	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
vymazání	vymazání	k1gNnSc3
znaku	znak	k1gInSc2
na	na	k7c4
pozici	pozice	k1gFnSc4
kurzoru	kurzor	k1gInSc2
během	během	k7c2
psaní	psaní	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
klávesy	klávesa	k1gFnSc2
Backspace	Backspace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
maže	mazat	k5eAaImIp3nS
znak	znak	k1gInSc4
na	na	k7c6
pozici	pozice	k1gFnSc6
před	před	k7c7
kurzorem	kurzor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Klávesa	klávesa	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
v	v	k7c6
tzv.	tzv.	kA
trojhmatu	trojhmat	k1gInSc2
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Del	Del	k1gMnSc2
v	v	k7c6
operačních	operační	k2eAgInPc6d1
systémech	systém	k1gInPc6
DOS	DOS	kA
a	a	k8xC
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s>
Klávesa	klávesa	k1gFnSc1
Delete	Dele	k1gNnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
některých	některý	k3yIgInPc6
noteboocích	notebook	k1gInPc6
použita	použit	k2eAgFnSc1d1
ke	k	k7c3
vstupu	vstup	k1gInSc3
do	do	k7c2
BIOSu	BIOSus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
klávesa	klávesa	k1gFnSc1
Delete	Dele	k1gNnSc2
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
i	i	k9
znak	znak	k1gInSc1
.	.	kIx.
na	na	k7c6
numerické	numerický	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Unicode	Unicod	k1gMnSc5
má	mít	k5eAaImIp3nS
pro	pro	k7c4
klávesu	klávesa	k1gFnSc4
Delete	Dele	k1gNnSc2
speciální	speciální	k2eAgInSc4d1
znak	znak	k1gInSc4
⌦	⌦	k?
(	(	kIx(
<g/>
U	u	k7c2
<g/>
+	+	kIx~
<g/>
2326	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Delete	Delet	k1gInSc5
</s>
<s>
Backspace	Backspace	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
klávesnice	klávesnice	k1gFnSc1
QWERTZ	QWERTZ	kA
</s>
<s>
Esc	Esc	k?
</s>
<s>
F1	F1	k4
</s>
<s>
F2	F2	k4
</s>
<s>
F3	F3	k4
</s>
<s>
F4	F4	k4
</s>
<s>
F5	F5	k4
</s>
<s>
F6	F6	k4
</s>
<s>
F7	F7	k4
</s>
<s>
F8	F8	k4
</s>
<s>
F9	F9	k4
</s>
<s>
F10	F10	k4
</s>
<s>
F11	F11	k4
</s>
<s>
F12	F12	k4
</s>
<s>
PrtScSysRq	PrtScSysRq	k?
</s>
<s>
ScrLk	ScrLk	k6eAd1
</s>
<s>
Pause	pausa	k1gFnSc3
</s>
<s>
Ins	Ins	k?
</s>
<s>
Home	Home	k6eAd1
</s>
<s>
PgUp	PgUp	k1gMnSc1
</s>
<s>
NumLk	NumLk	k6eAd1
</s>
<s>
/	/	kIx~
</s>
<s>
*	*	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Del	Del	k?
</s>
<s>
End	End	k?
</s>
<s>
PgDn	PgDn	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
↑	↑	k?
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Ent	Ent	k?
</s>
<s>
←	←	k?
</s>
<s>
↓	↓	k?
</s>
<s>
→	→	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
