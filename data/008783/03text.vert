<p>
<s>
Gejzír	gejzír	k1gInSc1	gejzír
je	být	k5eAaImIp3nS	být
pramen	pramen	k1gInSc4	pramen
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
nepravidelným	pravidelný	k2eNgInSc7d1	nepravidelný
únikem	únik	k1gInSc7	únik
vody	voda	k1gFnSc2	voda
vyvrhované	vyvrhovaný	k2eAgFnPc1d1	vyvrhovaná
turbulentně	turbulentně	k6eAd1	turbulentně
(	(	kIx(	(
<g/>
vířivě	vířivě	k6eAd1	vířivě
<g/>
)	)	kIx)	)
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
geologii	geologie	k1gFnSc6	geologie
dle	dle	k7c2	dle
definice	definice	k1gFnSc2	definice
schválené	schválený	k2eAgFnSc2d1	schválená
USGS	USGS	kA	USGS
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
vyvrhování	vyvrhování	k1gNnSc4	vyvrhování
vařícího	vařící	k2eAgInSc2d1	vařící
proudu	proud	k1gInSc2	proud
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
časově	časově	k6eAd1	časově
omezených	omezený	k2eAgFnPc6d1	omezená
periodách	perioda	k1gFnPc6	perioda
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
i	i	k9	i
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivních	aktivní	k2eAgFnPc6d1	aktivní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c6	na
měsících	měsíc	k1gInPc6	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
na	na	k7c6	na
Saturnově	Saturnův	k2eAgInSc6d1	Saturnův
měsíci	měsíc	k1gInSc6	měsíc
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
definice	definice	k1gFnSc2	definice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
islandského	islandský	k2eAgInSc2d1	islandský
nejznámějšího	známý	k2eAgInSc2d3	nejznámější
gejzíru	gejzír	k1gInSc2	gejzír
Geysir	Geysira	k1gFnPc2	Geysira
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Haukadalur	Haukadalura	k1gFnPc2	Haukadalura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
islandského	islandský	k2eAgNnSc2d1	islandské
slovesa	sloveso	k1gNnSc2	sloveso
geysa	geysa	k1gFnSc1	geysa
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
"	"	kIx"	"
<g/>
proudit	proudit	k5eAaPmF	proudit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
okolo	okolo	k6eAd1	okolo
tisíce	tisíc	k4xCgInSc2	tisíc
gejzírů	gejzír	k1gInPc2	gejzír
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
počet	počet	k1gInSc4	počet
700	[number]	k4	700
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližná	přibližný	k2eAgFnSc1d1	přibližná
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Yellowstonském	Yellowstonský	k2eAgInSc6d1	Yellowstonský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
i	i	k9	i
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámější	známý	k2eAgInSc1d3	nejznámější
gejzír	gejzír	k1gInSc1	gejzír
Old	Olda	k1gFnPc2	Olda
Faithful	Faithfula	k1gFnPc2	Faithfula
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
oblasti	oblast	k1gFnPc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
Údolí	údolí	k1gNnSc4	údolí
gejzírů	gejzír	k1gInPc2	gejzír
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Tatio	Tatio	k6eAd1	Tatio
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
Taupo	Taupa	k1gFnSc5	Taupa
Volcanic	Volcanice	k1gFnPc2	Volcanice
Zone	Zone	k1gFnPc7	Zone
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
gejzírů	gejzír	k1gInPc2	gejzír
dochází	docházet	k5eAaImIp3nP	docházet
jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panují	panovat	k5eAaImIp3nP	panovat
specifické	specifický	k2eAgFnPc4d1	specifická
hydrogeologické	hydrogeologický	k2eAgFnPc4d1	hydrogeologická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
řídký	řídký	k2eAgInSc4d1	řídký
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivními	aktivní	k2eAgFnPc7d1	aktivní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
žhavé	žhavý	k2eAgNnSc1d1	žhavé
magma	magma	k1gNnSc1	magma
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
dodává	dodávat	k5eAaImIp3nS	dodávat
vodě	voda	k1gFnSc3	voda
teplo	teplo	k6eAd1	teplo
potřebné	potřebný	k2eAgFnSc2d1	potřebná
k	k	k7c3	k
přehřátí	přehřátí	k1gNnSc3	přehřátí
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
či	či	k8xC	či
podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
vsakuje	vsakovat	k5eAaImIp3nS	vsakovat
systémem	systém	k1gInSc7	systém
trhlin	trhlina	k1gFnPc2	trhlina
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
okolo	okolo	k7c2	okolo
2000	[number]	k4	2000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
horkými	horký	k2eAgFnPc7d1	horká
horninami	hornina	k1gFnPc7	hornina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zahřátí	zahřátí	k1gNnSc3	zahřátí
<g/>
,	,	kIx,	,
přehřátí	přehřátí	k1gNnSc3	přehřátí
a	a	k8xC	a
explozivnímu	explozivní	k2eAgNnSc3d1	explozivní
vytlačení	vytlačení	k1gNnSc3	vytlačení
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
erupce	erupce	k1gFnPc1	erupce
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
různé	různý	k2eAgFnPc1d1	různá
výšky	výška	k1gFnPc1	výška
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tlaku	tlak	k1gInSc6	tlak
dosaženém	dosažený	k2eAgInSc6d1	dosažený
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
aktivní	aktivní	k2eAgInSc1d1	aktivní
gejzír	gejzír	k1gInSc1	gejzír
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Steamboat	Steamboat	k2eAgMnSc1d1	Steamboat
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
okolo	okolo	k7c2	okolo
90	[number]	k4	90
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
erupce	erupce	k1gFnPc1	erupce
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
a	a	k8xC	a
nastávají	nastávat	k5eAaImIp3nP	nastávat
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
potřebné	potřebný	k2eAgFnSc3d1	potřebná
akumulaci	akumulace	k1gFnSc3	akumulace
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
přívodových	přívodový	k2eAgFnPc6d1	přívodová
cestách	cesta	k1gFnPc6	cesta
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
přehřátí	přehřátí	k1gNnSc3	přehřátí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Voda	voda	k1gFnSc1	voda
vyvržená	vyvržený	k2eAgFnSc1d1	vyvržená
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nasycena	nasycen	k2eAgFnSc1d1	nasycena
minerálními	minerální	k2eAgFnPc7d1	minerální
látkami	látka	k1gFnPc7	látka
uvolňovanými	uvolňovaný	k2eAgFnPc7d1	uvolňovaná
z	z	k7c2	z
magmatu	magma	k1gNnSc2	magma
či	či	k8xC	či
okolních	okolní	k2eAgFnPc2d1	okolní
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
klesá	klesat	k5eAaImIp3nS	klesat
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
z	z	k7c2	z
vodního	vodní	k2eAgNnSc2d1	vodní
prostředí	prostředí	k1gNnSc2	prostředí
srážejí	srážet	k5eAaImIp3nP	srážet
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikají	vznikat	k5eAaImIp3nP	vznikat
různé	různý	k2eAgFnPc4d1	různá
vápnité	vápnitý	k2eAgFnPc4d1	vápnitá
nebo	nebo	k8xC	nebo
křemité	křemitý	k2eAgFnPc4d1	křemitá
sedimentární	sedimentární	k2eAgFnPc4d1	sedimentární
horniny	hornina	k1gFnPc4	hornina
jako	jako	k8xS	jako
například	například	k6eAd1	například
sintry	sintr	k1gInPc1	sintr
(	(	kIx(	(
<g/>
či	či	k8xC	či
gejzírity	gejzírita	k1gFnSc2	gejzírita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srážením	srážení	k1gNnSc7	srážení
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
místa	místo	k1gNnSc2	místo
erupcí	erupce	k1gFnPc2	erupce
nová	nový	k2eAgFnSc1d1	nová
platforma	platforma	k1gFnSc1	platforma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kužel	kužel	k1gInSc1	kužel
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
vystřikují	vystřikovat	k5eAaImIp3nP	vystřikovat
další	další	k2eAgFnSc1d1	další
erupce	erupce	k1gFnSc1	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
čase	čas	k1gInSc6	čas
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
utlumení	utlumení	k1gNnSc3	utlumení
či	či	k8xC	či
zesílení	zesílení	k1gNnSc3	zesílení
<g/>
,	,	kIx,	,
zániku	zánik	k1gInSc3	zánik
i	i	k8xC	i
vzniku	vznik	k1gInSc3	vznik
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
sedimentaci	sedimentace	k1gFnSc4	sedimentace
hornin	hornina	k1gFnPc2	hornina
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
přesunutí	přesunutí	k1gNnSc4	přesunutí
zdroje	zdroj	k1gInSc2	zdroj
magmatu	magma	k1gNnSc2	magma
<g/>
,	,	kIx,	,
uzavření	uzavření	k1gNnSc2	uzavření
či	či	k8xC	či
otevření	otevření	k1gNnSc2	otevření
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
,	,	kIx,	,
zemětřeseních	zemětřesení	k1gNnPc6	zemětřesení
či	či	k8xC	či
zásahem	zásah	k1gInSc7	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
Výtrysky	výtrysk	k1gInPc1	výtrysk
materiálu	materiál	k1gInSc2	materiál
často	často	k6eAd1	často
označované	označovaný	k2eAgInPc4d1	označovaný
za	za	k7c4	za
gejzíry	gejzír	k1gInPc4	gejzír
byly	být	k5eAaImAgFnP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
na	na	k7c6	na
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
ve	v	k7c6	v
vnějších	vnější	k2eAgInPc6d1	vnější
okrajích	okraj	k1gInPc6	okraj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
okolních	okolní	k2eAgInPc2d1	okolní
nízkých	nízký	k2eAgInPc2d1	nízký
tlaků	tlak	k1gInPc2	tlak
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
výtrysky	výtrysk	k1gInPc1	výtrysk
tvořeny	tvořit	k5eAaImNgInP	tvořit
plyny	plyn	k1gInPc7	plyn
bez	bez	k7c2	bez
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
;	;	kIx,	;
unikající	unikající	k2eAgInPc1d1	unikající
plyny	plyn	k1gInPc1	plyn
ovšem	ovšem	k9	ovšem
často	často	k6eAd1	často
vynesou	vynést	k5eAaPmIp3nP	vynést
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
drobné	drobný	k2eAgFnSc2d1	drobná
prachové	prachový	k2eAgFnSc2d1	prachová
částice	částice	k1gFnSc2	částice
či	či	k8xC	či
kousky	kousek	k1gInPc4	kousek
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Výtrysky	výtrysk	k1gInPc1	výtrysk
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
výtrysky	výtrysk	k1gInPc4	výtrysk
dusíku	dusík	k1gInSc2	dusík
u	u	k7c2	u
Tritonu	triton	k1gInSc2	triton
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc4	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
polární	polární	k2eAgFnSc2d1	polární
čepičky	čepička	k1gFnSc2	čepička
Marsu	Mars	k1gInSc2	Mars
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
náznaky	náznak	k1gInPc1	náznak
existence	existence	k1gFnSc2	existence
výtrysků	výtrysk	k1gInPc2	výtrysk
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Tritonu	triton	k1gInSc2	triton
není	být	k5eNaImIp3nS	být
výtrysk	výtrysk	k1gInSc1	výtrysk
způsobován	způsobován	k2eAgInSc1d1	způsobován
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
geotermální	geotermální	k2eAgFnSc7d1	geotermální
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
ohřev	ohřev	k1gInSc4	ohřev
povrchu	povrch	k1gInSc2	povrch
tělesa	těleso	k1gNnSc2	těleso
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
jsou	být	k5eAaImIp3nP	být
dočasné	dočasný	k2eAgInPc1d1	dočasný
geologické	geologický	k2eAgInPc1d1	geologický
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
životnost	životnost	k1gFnSc4	životnost
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
zaniknou	zaniknout	k5eAaPmIp3nP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
gejzírů	gejzír	k1gInPc2	gejzír
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
aktivitou	aktivita	k1gFnSc7	aktivita
anebo	anebo	k8xC	anebo
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
dozníváním	doznívání	k1gNnSc7	doznívání
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
gejzír	gejzír	k1gInSc1	gejzír
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
splněny	splněn	k2eAgInPc4d1	splněn
čtyři	čtyři	k4xCgInPc4	čtyři
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgInPc4d1	jiný
zdroje	zdroj	k1gInPc4	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
roli	role	k1gFnSc4	role
vysrážených	vysrážený	k2eAgFnPc2d1	vysrážená
hornin	hornina	k1gFnPc2	hornina
<g/>
)	)	kIx)	)
specifické	specifický	k2eAgFnPc1d1	specifická
geologické	geologický	k2eAgFnPc1d1	geologická
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
:	:	kIx,	:
přítomnost	přítomnost	k1gFnSc1	přítomnost
zdroje	zdroj	k1gInSc2	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
zdroje	zdroj	k1gInSc2	zdroj
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
utěsnění	utěsnění	k1gNnSc2	utěsnění
trhlin	trhlina	k1gFnPc2	trhlina
gejzíritem	gejzírit	k1gInSc7	gejzírit
<g/>
,	,	kIx,	,
proudění	proudění	k1gNnSc6	proudění
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
akumulace	akumulace	k1gFnSc2	akumulace
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nS	aby
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
gejzír	gejzír	k1gInSc4	gejzír
ve	v	k7c6	v
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgFnSc6d1	aktivní
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
povrchová	povrchový	k2eAgFnSc1d1	povrchová
voda	voda	k1gFnSc1	voda
nashromážděná	nashromážděný	k2eAgFnSc1d1	nashromážděná
ze	z	k7c2	z
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
odtávajícího	odtávající	k2eAgInSc2d1	odtávající
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
řek	řeka	k1gFnPc2	řeka
či	či	k8xC	či
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
magmatem	magma	k1gNnSc7	magma
<g/>
.	.	kIx.	.
</s>
<s>
Popraskaná	popraskaný	k2eAgFnSc1d1	popraskaná
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pronikání	pronikání	k1gNnSc4	pronikání
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
spodních	spodní	k2eAgFnPc2d1	spodní
částí	část	k1gFnPc2	část
či	či	k8xC	či
shromažďování	shromažďování	k1gNnSc2	shromažďování
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
vhodných	vhodný	k2eAgFnPc2d1	vhodná
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chladnější	chladný	k2eAgFnSc1d2	chladnější
voda	voda	k1gFnSc1	voda
naráží	narážet	k5eAaPmIp3nS	narážet
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
žhavého	žhavý	k2eAgNnSc2d1	žhavé
magmatu	magma	k1gNnSc2	magma
vystouplého	vystouplý	k2eAgInSc2d1	vystouplý
blízko	blízko	k7c2	blízko
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pronikání	pronikání	k1gNnSc1	pronikání
vody	voda	k1gFnSc2	voda
horninami	hornina	k1gFnPc7	hornina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
zabrat	zabrat	k5eAaPmF	zabrat
i	i	k9	i
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
vznik	vznik	k1gInSc1	vznik
gejzírů	gejzír	k1gInPc2	gejzír
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
kilometrech	kilometr	k1gInPc6	kilometr
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
gejzírů	gejzír	k1gInPc2	gejzír
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivními	aktivní	k2eAgNnPc7d1	aktivní
místy	místo	k1gNnPc7	místo
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
gejzírů	gejzír	k1gInPc2	gejzír
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
zdroje	zdroj	k1gInPc4	zdroj
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
systému	systém	k1gInSc2	systém
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
povrchové	povrchový	k2eAgFnSc2d1	povrchová
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vařící	vařící	k2eAgFnSc2d1	vařící
vody	voda	k1gFnSc2	voda
ohřáté	ohřátý	k2eAgFnSc6d1	ohřátá
magmatem	magma	k1gNnSc7	magma
<g/>
.	.	kIx.	.
</s>
<s>
Teplá	teplý	k2eAgFnSc1d1	teplá
a	a	k8xC	a
studená	studený	k2eAgFnSc1d1	studená
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
mísí	mísit	k5eAaImIp3nP	mísit
v	v	k7c6	v
podzemním	podzemní	k2eAgInSc6d1	podzemní
rezervoáru	rezervoár	k1gInSc6	rezervoár
<g/>
.	.	kIx.	.
</s>
<s>
Horká	Horká	k1gFnSc1	Horká
méně	málo	k6eAd2	málo
hustá	hustý	k2eAgFnSc1d1	hustá
voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
stoupat	stoupat	k5eAaImF	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
konvekcí	konvekce	k1gFnSc7	konvekce
systémem	systém	k1gInSc7	systém
trhlin	trhlina	k1gFnPc2	trhlina
a	a	k8xC	a
prasklin	prasklina	k1gFnPc2	prasklina
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
gejzírů	gejzír	k1gInPc2	gejzír
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
zlomů	zlom	k1gInPc2	zlom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
studená	studený	k2eAgFnSc1d1	studená
hustší	hustý	k2eAgFnSc1d2	hustší
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
tlačí	tlačit	k5eAaImIp3nS	tlačit
do	do	k7c2	do
spodních	spodní	k2eAgFnPc2d1	spodní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
systém	systém	k1gInSc1	systém
naplní	naplnit	k5eAaPmIp3nS	naplnit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
si	se	k3xPyFc3	se
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
magma	magma	k1gNnSc1	magma
se	se	k3xPyFc4	se
nepatrně	patrně	k6eNd1	patrně
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
ohřívána	ohříván	k2eAgFnSc1d1	ohřívána
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
vystavena	vystavit	k5eAaPmNgFnS	vystavit
okolnímu	okolní	k2eAgInSc3d1	okolní
tlaku	tlak	k1gInSc2	tlak
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
nad	nad	k7c7	nad
místem	místo	k1gNnSc7	místo
ohřevu	ohřev	k1gInSc2	ohřev
<g/>
,	,	kIx,	,
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
začne	začít	k5eAaPmIp3nS	začít
přehřívat	přehřívat	k5eAaImF	přehřívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
gejzíru	gejzír	k1gInSc2	gejzír
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
ochlazována	ochlazován	k2eAgFnSc1d1	ochlazována
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malý	malý	k2eAgInSc1d1	malý
přívodní	přívodní	k2eAgInSc1d1	přívodní
kanál	kanál	k1gInSc1	kanál
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
efektivní	efektivní	k2eAgNnSc4d1	efektivní
chlazení	chlazení	k1gNnSc4	chlazení
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
oblastech	oblast	k1gFnPc6	oblast
kondukcí	kondukce	k1gFnPc2	kondukce
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
vystupující	vystupující	k2eAgFnSc1d1	vystupující
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
prosycena	prosytit	k5eAaPmNgFnS	prosytit
křemičitany	křemičitan	k1gInPc7	křemičitan
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
gejzíritu	gejzírit	k1gInSc2	gejzírit
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
křemičitany	křemičitan	k1gInPc7	křemičitan
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
vysrážejí	vysrážet	k5eAaPmIp3nP	vysrážet
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
izolační	izolační	k2eAgFnSc1d1	izolační
vrstva	vrstva	k1gFnSc1	vrstva
okolo	okolo	k7c2	okolo
vývodní	vývodní	k2eAgFnSc2d1	vývodní
praskliny	prasklina	k1gFnSc2	prasklina
zamezující	zamezující	k2eAgFnSc2d1	zamezující
úniku	únik	k1gInSc2	únik
tlaku	tlak	k1gInSc2	tlak
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
prostupných	prostupný	k2eAgFnPc2d1	prostupná
vrstev	vrstva	k1gFnPc2	vrstva
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
písek	písek	k1gInSc4	písek
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
porézní	porézní	k2eAgFnPc4d1	porézní
horniny	hornina	k1gFnPc4	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
zahřátá	zahřátý	k2eAgFnSc1d1	zahřátá
na	na	k7c4	na
kritickou	kritický	k2eAgFnSc4d1	kritická
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgFnSc1d1	vystupující
masa	masa	k1gFnSc1	masa
vody	voda	k1gFnSc2	voda
má	mít	k5eAaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
energii	energie	k1gFnSc4	energie
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
potřebné	potřebný	k2eAgFnSc2d1	potřebná
teploty	teplota	k1gFnSc2	teplota
pro	pro	k7c4	pro
bod	bod	k1gInSc4	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
voda	voda	k1gFnSc1	voda
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
omezující	omezující	k2eAgInSc1d1	omezující
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
působí	působit	k5eAaImIp3nP	působit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
bodu	bod	k1gInSc2	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
přehřátá	přehřátý	k2eAgFnSc1d1	přehřátá
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
okamžitě	okamžitě	k6eAd1	okamžitě
do	do	k7c2	do
plynného	plynný	k2eAgNnSc2d1	plynné
skupenství	skupenství	k1gNnSc2	skupenství
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
značně	značně	k6eAd1	značně
větší	veliký	k2eAgInSc1d2	veliký
objem	objem	k1gInSc1	objem
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
až	až	k9	až
1600	[number]	k4	1600
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc1d2	veliký
objem	objem	k1gInSc1	objem
než	než	k8xS	než
původní	původní	k2eAgFnSc1d1	původní
vodní	vodní	k2eAgFnSc1d1	vodní
masa	masa	k1gFnSc1	masa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
vytlačit	vytlačit	k5eAaPmF	vytlačit
část	část	k1gFnSc4	část
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
váha	váha	k1gFnSc1	váha
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
udržet	udržet	k5eAaPmF	udržet
přehřátou	přehřátý	k2eAgFnSc4d1	přehřátá
vodu	voda	k1gFnSc4	voda
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
množství	množství	k1gNnSc1	množství
bublin	bublina	k1gFnPc2	bublina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytlačí	vytlačit	k5eAaPmIp3nP	vytlačit
svrchní	svrchní	k2eAgFnSc4d1	svrchní
vodu	voda	k1gFnSc4	voda
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
plyn	plyn	k1gInSc1	plyn
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
z	z	k7c2	z
gejzíru	gejzír	k1gInSc2	gejzír
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
<g/>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
erupce	erupce	k1gFnPc4	erupce
gejzírů	gejzír	k1gInPc2	gejzír
má	mít	k5eAaImIp3nS	mít
přítomnost	přítomnost	k1gFnSc1	přítomnost
plynů	plyn	k1gInPc2	plyn
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nejhojnější	hojný	k2eAgInSc4d3	nejhojnější
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
metan	metan	k1gInSc1	metan
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
či	či	k8xC	či
sirovodík	sirovodík	k1gInSc1	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
sopečné	sopečný	k2eAgInPc4d1	sopečný
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
přítomnost	přítomnost	k1gFnSc4	přítomnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
hydrostatický	hydrostatický	k2eAgInSc4d1	hydrostatický
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
voda	voda	k1gFnSc1	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rozpuštěné	rozpuštěný	k2eAgInPc4d1	rozpuštěný
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vody	voda	k1gFnSc2	voda
prosté	prostý	k2eAgFnSc2d1	prostá
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
plynů	plyn	k1gInPc2	plyn
vysoké	vysoká	k1gFnSc3	vysoká
<g/>
,	,	kIx,	,
gejzíry	gejzír	k1gInPc1	gejzír
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
bodu	bod	k1gInSc2	bod
varu	var	k1gInSc2	var
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podzemní	podzemní	k2eAgInSc1d1	podzemní
rezervoár	rezervoár	k1gInSc1	rezervoár
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
gejzíru	gejzír	k1gInSc2	gejzír
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
nacházel	nacházet	k5eAaImAgMnS	nacházet
vhodný	vhodný	k2eAgInSc4d1	vhodný
rezervoár	rezervoár	k1gInSc4	rezervoár
tvořený	tvořený	k2eAgInSc4d1	tvořený
systémem	systém	k1gInSc7	systém
puklin	puklina	k1gFnPc2	puklina
či	či	k8xC	či
puklinou	puklina	k1gFnSc7	puklina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
akumulaci	akumulace	k1gFnSc4	akumulace
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
každý	každý	k3xTgInSc4	každý
gejzír	gejzír	k1gInSc4	gejzír
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgNnSc1d1	unikátní
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
průzkumu	průzkum	k1gInSc2	průzkum
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
rezervoáry	rezervoár	k1gInPc4	rezervoár
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
šest	šest	k4xCc4	šest
hlavních	hlavní	k2eAgInPc2d1	hlavní
typů	typ	k1gInPc2	typ
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
rezervoáru	rezervoár	k1gInSc2	rezervoár
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
úzkým	úzký	k2eAgInSc7d1	úzký
rovným	rovný	k2eAgInSc7d1	rovný
rezervoárem	rezervoár	k1gInSc7	rezervoár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
typ	typ	k1gInSc1	typ
gejzíru	gejzír	k1gInSc2	gejzír
zpravidla	zpravidla	k6eAd1	zpravidla
tryská	tryskat	k5eAaImIp3nS	tryskat
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
erupce	erupce	k1gFnPc1	erupce
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
gejzír	gejzír	k1gInSc1	gejzír
Old	Olda	k1gFnPc2	Olda
Faithful	Faithful	k1gInSc1	Faithful
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
prozkoumán	prozkoumán	k2eAgInSc4d1	prozkoumán
kamerou	kamera	k1gFnSc7	kamera
spuštěnou	spuštěný	k2eAgFnSc7d1	spuštěná
do	do	k7c2	do
rezervoáru	rezervoár	k1gInSc2	rezervoár
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
dutinu	dutina	k1gFnSc4	dutina
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
varu	var	k1gInSc3	var
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
rovnou	rovný	k2eAgFnSc7d1	rovná
a	a	k8xC	a
širokou	široký	k2eAgFnSc7d1	široká
trhlinou	trhlina	k1gFnSc7	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnSc1	erupce
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
projevem	projev	k1gInSc7	projev
a	a	k8xC	a
krátkým	krátký	k2eAgNnSc7d1	krátké
trváním	trvání	k1gNnSc7	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
Round	round	k1gInSc4	round
Geyser	Geysra	k1gFnPc2	Geysra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
prvnímu	první	k4xOgNnSc3	první
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
ústí	ústí	k1gNnSc2	ústí
bazén	bazén	k1gInSc4	bazén
zaplněný	zaplněný	k2eAgInSc4d1	zaplněný
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Great	Great	k2eAgInSc1d1	Great
Fountain	Fountain	k1gInSc1	Fountain
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc4	pátý
a	a	k8xC	a
šestý	šestý	k4xOgInSc4	šestý
typ	typ	k1gInSc4	typ
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
fontánové	fontán	k1gMnPc1	fontán
erupce	erupce	k1gFnPc1	erupce
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
rozvětvenou	rozvětvený	k2eAgFnSc7d1	rozvětvená
soustavou	soustava	k1gFnSc7	soustava
podzemních	podzemní	k2eAgFnPc2d1	podzemní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc1	pátý
rezervoárem	rezervoár	k1gInSc7	rezervoár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
širší	široký	k2eAgFnSc6d2	širší
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
a	a	k8xC	a
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
zúžení	zúžení	k1gNnSc1	zúžení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
rychlému	rychlý	k2eAgInSc3d1	rychlý
úniku	únik	k1gInSc3	únik
přehřáté	přehřátý	k2eAgFnSc2d1	přehřátá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
dlouhotrvající	dlouhotrvající	k2eAgFnSc6d1	dlouhotrvající
fontánovité	fontánovitý	k2eAgFnSc6d1	fontánovitý
erupci	erupce	k1gFnSc6	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Šestý	šestý	k4xOgInSc1	šestý
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
rovná	rovný	k2eAgFnSc1d1	rovná
prasklina	prasklina	k1gFnSc1	prasklina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
výrazně	výrazně	k6eAd1	výrazně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
do	do	k7c2	do
povrchového	povrchový	k2eAgInSc2d1	povrchový
bazénu	bazén	k1gInSc2	bazén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Erupce	erupce	k1gFnPc1	erupce
===	===	k?	===
</s>
</p>
<p>
<s>
Gejzír	gejzír	k1gInSc1	gejzír
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
horké	horký	k2eAgInPc1d1	horký
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
kontaktem	kontakt	k1gInSc7	kontakt
podzemní	podzemní	k2eAgFnPc1d1	podzemní
vody	voda	k1gFnPc1	voda
se	s	k7c7	s
žhavým	žhavý	k2eAgNnSc7d1	žhavé
magmatem	magma	k1gNnSc7	magma
<g/>
.	.	kIx.	.
</s>
<s>
Geotermálně	Geotermálně	k6eAd1	Geotermálně
zahřívaná	zahřívaný	k2eAgFnSc1d1	zahřívaná
voda	voda	k1gFnSc1	voda
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
řadou	řada	k1gFnSc7	řada
prasklin	prasklina	k1gFnPc2	prasklina
a	a	k8xC	a
puklin	puklina	k1gFnPc2	puklina
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
erupce	erupce	k1gFnSc2	erupce
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
gejzíry	gejzír	k1gInPc4	gejzír
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
islandský	islandský	k2eAgMnSc1d1	islandský
Strokkur	Strokkur	k1gMnSc1	Strokkur
tryská	tryskat	k5eAaImIp3nS	tryskat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
každých	každý	k3xTgFnPc2	každý
pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Grand	grand	k1gMnSc1	grand
Geysir	Geysir	k1gMnSc1	Geysir
v	v	k7c6	v
USA	USA	kA	USA
tryská	tryskat	k5eAaImIp3nS	tryskat
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
každých	každý	k3xTgNnPc2	každý
8	[number]	k4	8
až	až	k8xS	až
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
tryská	tryskat	k5eAaImIp3nS	tryskat
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgInPc6d1	nepravidelný
intervalech	interval	k1gInPc6	interval
a	a	k8xC	a
jen	jen	k9	jen
menšina	menšina	k1gFnSc1	menšina
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k8xC	jak
plyn	plyn	k1gInSc1	plyn
vyvrhne	vyvrhnout	k5eAaPmIp3nS	vyvrhnout
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
část	část	k1gFnSc4	část
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
transportoval	transportovat	k5eAaBmAgInS	transportovat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
tlak	tlak	k1gInSc1	tlak
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
na	na	k7c4	na
rezervoár	rezervoár	k1gInSc4	rezervoár
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
bodu	bod	k1gInSc2	bod
varu	var	k1gInSc2	var
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
sloupce	sloupec	k1gInSc2	sloupec
přehřáté	přehřátý	k2eAgFnSc2d1	přehřátá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
v	v	k7c4	v
páru	pára	k1gFnSc4	pára
<g/>
.	.	kIx.	.
</s>
<s>
Pára	pára	k1gFnSc1	pára
začne	začít	k5eAaPmIp3nS	začít
hlasitě	hlasitě	k6eAd1	hlasitě
unikat	unikat	k5eAaImF	unikat
ze	z	k7c2	z
sloupce	sloupec	k1gInSc2	sloupec
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vytryskne	vytrysknout	k5eAaPmIp3nS	vytrysknout
směs	směs	k1gFnSc1	směs
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
páry	pár	k1gInPc4	pár
unikající	unikající	k2eAgInPc4d1	unikající
z	z	k7c2	z
trhliny	trhlina	k1gFnSc2	trhlina
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnSc1	erupce
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
voda	voda	k1gFnSc1	voda
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
po	po	k7c6	po
hlavní	hlavní	k2eAgFnSc6d1	hlavní
erupci	erupce	k1gFnSc6	erupce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
následuje	následovat	k5eAaImIp3nS	následovat
ještě	ještě	k9	ještě
série	série	k1gFnSc1	série
dalších	další	k2eAgFnPc2d1	další
erupcí	erupce	k1gFnPc2	erupce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
jícnu	jícen	k1gInSc2	jícen
uniká	unikat	k5eAaImIp3nS	unikat
pouze	pouze	k6eAd1	pouze
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
než	než	k8xS	než
opět	opět	k6eAd1	opět
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
soustavy	soustava	k1gFnSc2	soustava
trhlin	trhlina	k1gFnPc2	trhlina
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
<g/>
Pára	pára	k1gFnSc1	pára
může	moct	k5eAaImIp3nS	moct
vypudit	vypudit	k5eAaPmF	vypudit
vodu	voda	k1gFnSc4	voda
vzhůru	vzhůru	k6eAd1	vzhůru
takovou	takový	k3xDgFnSc7	takový
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytryskne	vytrysknout	k5eAaPmIp3nS	vytrysknout
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
páry	pára	k1gFnSc2	pára
není	být	k5eNaImIp3nS	být
dost	dost	k6eAd1	dost
silná	silný	k2eAgFnSc1d1	silná
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
horké	horký	k2eAgInPc1d1	horký
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
soustavně	soustavně	k6eAd1	soustavně
vylévá	vylévat	k5eAaImIp3nS	vylévat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
žhavé	žhavý	k2eAgNnSc1d1	žhavé
magma	magma	k1gNnSc1	magma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
panuje	panovat	k5eAaImIp3nS	panovat
zde	zde	k6eAd1	zde
nedostatek	nedostatek	k1gInSc4	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
fumaroly	fumarola	k1gFnSc2	fumarola
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
trhlinu	trhlina	k1gFnSc4	trhlina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pouze	pouze	k6eAd1	pouze
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
bez	bez	k7c2	bez
doprovodné	doprovodný	k2eAgFnSc2d1	doprovodná
erupce	erupce	k1gFnSc2	erupce
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
fumarola	fumarola	k1gFnSc1	fumarola
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zavodněném	zavodněný	k2eAgNnSc6d1	zavodněné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
bahenní	bahenní	k2eAgFnSc1d1	bahenní
sopka	sopka	k1gFnSc1	sopka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přítomné	přítomný	k2eAgNnSc1d1	přítomné
magma	magma	k1gNnSc1	magma
<g/>
,	,	kIx,	,
dostatek	dostatek	k1gInSc1	dostatek
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
geyseterit	geyseterit	k1gInSc1	geyseterit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
praskliny	prasklina	k1gFnPc1	prasklina
nemají	mít	k5eNaImIp3nP	mít
správný	správný	k2eAgInSc4d1	správný
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
termální	termální	k2eAgNnSc1d1	termální
jezero	jezero	k1gNnSc1	jezero
či	či	k8xC	či
jiný	jiný	k2eAgInSc1d1	jiný
menší	malý	k2eAgInSc1d2	menší
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahromaděná	nahromaděný	k2eAgFnSc1d1	nahromaděná
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
může	moct	k5eAaImIp3nS	moct
volně	volně	k6eAd1	volně
unikat	unikat	k5eAaImF	unikat
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přehřála	přehřát	k5eAaPmAgFnS	přehřát
na	na	k7c4	na
kritickou	kritický	k2eAgFnSc4d1	kritická
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Erupce	erupce	k1gFnSc1	erupce
gejzíru	gejzír	k1gInSc2	gejzír
Strokkur	Strokkura	k1gFnPc2	Strokkura
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ústí	ústí	k1gNnSc1	ústí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
gejzíru	gejzír	k1gInSc2	gejzír
vytvořit	vytvořit	k5eAaPmF	vytvořit
vysrážené	vysrážený	k2eAgInPc4d1	vysrážený
minerály	minerál	k1gInPc4	minerál
zátku	zátka	k1gFnSc4	zátka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zmenší	zmenšit	k5eAaPmIp3nS	zmenšit
otvor	otvor	k1gInSc4	otvor
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
může	moct	k5eAaImIp3nS	moct
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
párou	pára	k1gFnSc7	pára
unikat	unikat	k5eAaImF	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
útvar	útvar	k1gInSc1	útvar
podobný	podobný	k2eAgInSc1d1	podobný
trysce	tryska	k1gFnSc3	tryska
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vyvrhovaný	vyvrhovaný	k2eAgInSc4d1	vyvrhovaný
materiál	materiál	k1gInSc4	materiál
tlačen	tlačen	k2eAgInSc4d1	tlačen
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
ústí	ústit	k5eAaImIp3nS	ústit
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
fontánový	fontánový	k2eAgInSc1d1	fontánový
typ	typ	k1gInSc1	typ
<g/>
"	"	kIx"	"
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
hlasitou	hlasitý	k2eAgFnSc7d1	hlasitá
a	a	k8xC	a
intenzivní	intenzivní	k2eAgFnSc7d1	intenzivní
erupcí	erupce	k1gFnSc7	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Grand	grand	k1gMnSc1	grand
Geysir	Geysir	k1gMnSc1	Geysir
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
trysky	tryska	k1gFnSc2	tryska
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
menší	malý	k2eAgInSc1d2	menší
kráter	kráter	k1gInSc1	kráter
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
či	či	k8xC	či
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
vyplněn	vyplněn	k2eAgInSc4d1	vyplněn
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
kužele	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
erupcím	erupce	k1gFnPc3	erupce
–	–	k?	–
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
sloupcový	sloupcový	k2eAgInSc1d1	sloupcový
typ	typ	k1gInSc1	typ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
gejzír	gejzír	k1gInSc1	gejzír
Old	Olda	k1gFnPc2	Olda
Faithful	Faithfula	k1gFnPc2	Faithfula
<g/>
.	.	kIx.	.
<g/>
Jelikož	jelikož	k8xS	jelikož
voda	voda	k1gFnSc1	voda
často	často	k6eAd1	často
prochází	procházet	k5eAaImIp3nS	procházet
okolní	okolní	k2eAgFnSc7d1	okolní
horninou	hornina	k1gFnSc7	hornina
zpravidla	zpravidla	k6eAd1	zpravidla
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
ryolitem	ryolit	k1gInSc7	ryolit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
obohacena	obohatit	k5eAaPmNgFnS	obohatit
křemičitany	křemičitan	k1gInPc7	křemičitan
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
do	do	k7c2	do
podmínek	podmínka	k1gFnPc2	podmínka
normálního	normální	k2eAgInSc2d1	normální
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
poklesu	pokles	k1gInSc2	pokles
teploty	teplota	k1gFnSc2	teplota
vysrážejí	vysrážet	k5eAaPmIp3nP	vysrážet
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
ryolitů	ryolit	k1gInPc2	ryolit
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
gejzírů	gejzír	k1gInPc2	gejzír
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
více	hodně	k6eAd2	hodně
mafické	mafický	k2eAgFnSc2d1	mafický
horniny	hornina	k1gFnSc2	hornina
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
andezity	andezit	k1gInPc4	andezit
či	či	k8xC	či
bazalty	bazalt	k1gInPc4	bazalt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zánik	zánik	k1gInSc1	zánik
==	==	k?	==
</s>
</p>
<p>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
zanikají	zanikat	k5eAaImIp3nP	zanikat
samovolně	samovolně	k6eAd1	samovolně
či	či	k8xC	či
zásahem	zásah	k1gInSc7	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
zánik	zánik	k1gInSc1	zánik
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
utlumením	utlumení	k1gNnSc7	utlumení
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
či	či	k8xC	či
jejím	její	k3xOp3gNnSc7	její
ukončením	ukončení	k1gNnSc7	ukončení
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
nedostatek	nedostatek	k1gInSc1	nedostatek
vody	voda	k1gFnSc2	voda
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
mohutnosti	mohutnost	k1gFnSc2	mohutnost
výtrysků	výtrysk	k1gInPc2	výtrysk
a	a	k8xC	a
přeměně	přeměna	k1gFnSc3	přeměna
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Specifickým	specifický	k2eAgNnSc7d1	specifické
ukončením	ukončení	k1gNnSc7	ukončení
gejzíru	gejzír	k1gInSc2	gejzír
je	být	k5eAaImIp3nS	být
vlivem	vliv	k1gInSc7	vliv
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
puklinový	puklinový	k2eAgInSc1d1	puklinový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
proudila	proudit	k5eAaPmAgFnS	proudit
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Hebgen	Hebgen	k1gInSc1	Hebgen
Lake	Lake	k1gFnPc2	Lake
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Montana	Montana	k1gFnSc1	Montana
poblíž	poblíž	k7c2	poblíž
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
otřesů	otřes	k1gInPc2	otřes
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
změnila	změnit	k5eAaPmAgFnS	změnit
frekvence	frekvence	k1gFnSc1	frekvence
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
množství	množství	k1gNnSc2	množství
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
<g/>
Člověk	člověk	k1gMnSc1	člověk
taktéž	taktéž	k?	taktéž
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
cíleným	cílený	k2eAgInSc7d1	cílený
vandalským	vandalský	k2eAgInSc7d1	vandalský
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poškodí	poškodit	k5eAaPmIp3nS	poškodit
ústí	ústí	k1gNnSc1	ústí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
například	například	k6eAd1	například
stavbou	stavba	k1gFnSc7	stavba
geotermálních	geotermální	k2eAgFnPc2d1	geotermální
elektráren	elektrárna	k1gFnPc2	elektrárna
odvádějící	odvádějící	k2eAgNnSc1d1	odvádějící
teplo	teplo	k1gNnSc1	teplo
či	či	k8xC	či
narušující	narušující	k2eAgNnSc1d1	narušující
proudění	proudění	k1gNnSc1	proudění
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
její	její	k3xOp3gFnSc2	její
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc1	biologie
gejzírů	gejzír	k1gInPc2	gejzír
==	==	k?	==
</s>
</p>
<p>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
specifickou	specifický	k2eAgFnSc4d1	specifická
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
způsobena	způsobit	k5eAaPmNgFnS	způsobit
organismy	organismus	k1gInPc7	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	on	k3xPp3gMnPc4	on
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Teplé	Teplé	k2eAgNnSc1d1	Teplé
okolí	okolí	k1gNnSc1	okolí
gejzírů	gejzír	k1gInPc2	gejzír
(	(	kIx(	(
<g/>
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
horkých	horký	k2eAgFnPc2d1	horká
oblastí	oblast	k1gFnPc2	oblast
<g/>
)	)	kIx)	)
obývají	obývat	k5eAaImIp3nP	obývat
termofilní	termofilní	k2eAgInPc1d1	termofilní
prokaryotické	prokaryotický	k2eAgInPc1d1	prokaryotický
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
archebakterie	archebakterie	k1gFnSc2	archebakterie
(	(	kIx(	(
<g/>
žádná	žádný	k3yNgFnSc1	žádný
známá	známý	k2eAgFnSc1d1	známá
eukaryota	eukaryota	k1gFnSc1	eukaryota
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
přežít	přežít	k5eAaPmF	přežít
teplotu	teplota	k1gFnSc4	teplota
přes	přes	k7c4	přes
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
zkoumat	zkoumat	k5eAaImF	zkoumat
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
panoval	panovat	k5eAaImAgInS	panovat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
život	život	k1gInSc1	život
nemůže	moct	k5eNaImIp3nS	moct
přežít	přežít	k5eAaPmF	přežít
teplotu	teplota	k1gFnSc4	teplota
dosahující	dosahující	k2eAgFnSc4d1	dosahující
73	[number]	k4	73
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgInSc1d1	horní
limit	limit	k1gInSc1	limit
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
teploty	teplota	k1gFnSc2	teplota
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
deoxyribonukleové	deoxyribonukleový	k2eAgFnSc2d1	deoxyribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
termofilní	termofilní	k2eAgFnPc4d1	termofilní
bakterie	bakterie	k1gFnPc4	bakterie
byla	být	k5eAaImAgNnP	být
tehdy	tehdy	k6eAd1	tehdy
stanovena	stanovit	k5eAaPmNgNnP	stanovit
ještě	ještě	k6eAd1	ještě
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
55	[number]	k4	55
°	°	k?	°
<g/>
C.	C.	kA	C.
Nicméně	nicméně	k8xC	nicméně
pozorování	pozorování	k1gNnSc2	pozorování
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
formy	forma	k1gFnPc1	forma
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
že	že	k8xS	že
některým	některý	k3yIgMnPc3	některý
dokonce	dokonce	k9	dokonce
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
teploty	teplota	k1gFnPc1	teplota
přesahující	přesahující	k2eAgFnSc4d1	přesahující
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hypertermofilové	hypertermofil	k1gMnPc1	hypertermofil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termofilní	termofilní	k2eAgInPc1d1	termofilní
organismy	organismus	k1gInPc1	organismus
preferují	preferovat	k5eAaImIp3nP	preferovat
teploty	teplota	k1gFnPc4	teplota
od	od	k7c2	od
50	[number]	k4	50
do	do	k7c2	do
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
hypertermofilní	hypertermofilní	k2eAgMnPc1d1	hypertermofilní
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
nejlépe	dobře	k6eAd3	dobře
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
teplot	teplota	k1gFnPc2	teplota
80	[number]	k4	80
až	až	k9	až
110	[number]	k4	110
°	°	k?	°
<g/>
C.	C.	kA	C.
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tepelně	tepelně	k6eAd1	tepelně
stabilní	stabilní	k2eAgInPc1d1	stabilní
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
aktivitu	aktivita	k1gFnSc4	aktivita
i	i	k9	i
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
například	například	k6eAd1	například
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
biotechnologiích	biotechnologie	k1gFnPc6	biotechnologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
ujaly	ujmout	k5eAaPmAgFnP	ujmout
například	například	k6eAd1	například
termostabilní	termostabilní	k2eAgFnPc4d1	termostabilní
amylázy	amyláza	k1gFnPc4	amyláza
(	(	kIx(	(
<g/>
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
škrobu	škrob	k1gInSc2	škrob
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
enzymy	enzym	k1gInPc1	enzym
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
či	či	k8xC	či
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
života	život	k1gInSc2	život
v	v	k7c6	v
tak	tak	k6eAd1	tak
nehostinných	hostinný	k2eNgFnPc6d1	nehostinná
podmínkách	podmínka	k1gFnPc6	podmínka
mimoto	mimoto	k6eAd1	mimoto
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
lidské	lidský	k2eAgFnPc4d1	lidská
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
existence	existence	k1gFnSc2	existence
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
života	život	k1gInSc2	život
o	o	k7c4	o
další	další	k1gNnSc4	další
potenciálně	potenciálně	k6eAd1	potenciálně
obyvatelné	obyvatelný	k2eAgFnSc2d1	obyvatelná
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gejzíry	gejzír	k1gInPc7	gejzír
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
aktivní	aktivní	k2eAgInSc1d1	aktivní
gejzír	gejzír	k1gInSc1	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
gejzír	gejzír	k1gInSc1	gejzír
Castle	Castle	k1gFnSc2	Castle
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
gejzíru	gejzír	k1gInSc2	gejzír
Old	Olda	k1gFnPc2	Olda
Faithful	Faithfula	k1gFnPc2	Faithfula
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
jeho	on	k3xPp3gInSc2	on
kužele	kužel	k1gInSc2	kužel
připomínal	připomínat	k5eAaImAgInS	připomínat
lidem	člověk	k1gMnPc3	člověk
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
dalo	dát	k5eAaPmAgNnS	dát
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
castle	castle	k6eAd1	castle
znamená	znamenat	k5eAaImIp3nS	znamenat
anglicky	anglicky	k6eAd1	anglicky
hrad	hrad	k1gInSc4	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
kužele	kužel	k1gInSc2	kužel
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
starý	starý	k1gMnSc1	starý
5000	[number]	k4	5000
až	až	k6eAd1	až
40	[number]	k4	40
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Gejzíry	gejzír	k1gInPc1	gejzír
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
se	se	k3xPyFc4	se
do	do	k7c2	do
pěti	pět	k4xCc2	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Yellowstone	Yellowston	k1gInSc5	Yellowston
v	v	k7c6	v
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
Yellowstone	Yellowston	k1gInSc5	Yellowston
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
lokalita	lokalita	k1gFnSc1	lokalita
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
tisíce	tisíc	k4xCgInPc1	tisíc
horkých	horký	k2eAgMnPc2d1	horký
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
asi	asi	k9	asi
300	[number]	k4	300
až	až	k9	až
500	[number]	k4	500
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
polovině	polovina	k1gFnSc6	polovina
gejzírů	gejzír	k1gInPc2	gejzír
v	v	k7c6	v
pěti	pět	k4xCc6	pět
hlavních	hlavní	k2eAgFnPc6d1	hlavní
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Wyoming	Wyoming	k1gInSc1	Wyoming
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
Idaho	Ida	k1gMnSc4	Ida
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
známý	známý	k2eAgInSc1d1	známý
gejzír	gejzír	k1gInSc1	gejzír
Steamboat	Steamboat	k2eAgInSc4d1	Steamboat
Geyser	Geyser	k1gInSc4	Geyser
v	v	k7c6	v
Norris	Norris	k1gFnSc6	Norris
Geyser	Geyser	k1gMnSc1	Geyser
Basin	Basin	k1gMnSc1	Basin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
vytryskl	vytrysknout	k5eAaPmAgMnS	vytrysknout
přibližně	přibližně	k6eAd1	přibližně
desetkrát	desetkrát	k6eAd1	desetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
gejzíry	gejzír	k1gInPc1	gejzír
Old	Olda	k1gFnPc2	Olda
Faithful	Faithfula	k1gFnPc2	Faithfula
<g/>
,	,	kIx,	,
Beehive	Beehiev	k1gFnPc1	Beehiev
Geyser	Geyser	k1gInSc1	Geyser
<g/>
,	,	kIx,	,
Giantess	Giantess	k1gInSc1	Giantess
Geyser	Geyser	k1gInSc1	Geyser
<g/>
,	,	kIx,	,
Lion	Lion	k1gMnSc1	Lion
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
Plume	Plum	k1gInSc5	Plum
Geyser	Geyser	k1gInSc1	Geyser
<g/>
,	,	kIx,	,
Aurum	Aurum	k1gInSc1	Aurum
Geyser	Geyser	k1gInSc1	Geyser
<g/>
,	,	kIx,	,
Castle	Castle	k1gFnSc1	Castle
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
Sawmill	Sawmill	k1gMnSc1	Sawmill
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
Oblong	Oblong	k1gMnSc1	Oblong
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
Giant	Giant	k1gMnSc1	Giant
Geyser	Geyser	k1gMnSc1	Geyser
<g/>
,	,	kIx,	,
Daisy	Daisa	k1gFnPc1	Daisa
Geyser	Geyser	k1gInSc1	Geyser
<g/>
,	,	kIx,	,
Grotto	Grotto	k1gNnSc1	Grotto
Geyser	Geysra	k1gFnPc2	Geysra
<g/>
,	,	kIx,	,
Fan	Fana	k1gFnPc2	Fana
&	&	k?	&
Mortar	Mortar	k1gInSc1	Mortar
Geysers	Geysersa	k1gFnPc2	Geysersa
nebo	nebo	k8xC	nebo
Riverside	Riversid	k1gInSc5	Riversid
Geyser	Geyser	k1gInSc4	Geyser
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Upper	Upper	k1gMnSc1	Upper
Geyser	Geyser	k1gMnSc1	Geyser
Basin	Basin	k1gMnSc1	Basin
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgMnSc1d1	obsahující
téměř	téměř	k6eAd1	téměř
180	[number]	k4	180
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Údolí	údolí	k1gNnSc3	údolí
gejzírů	gejzír	k1gInPc2	gejzír
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
Údolí	údolí	k1gNnSc1	údolí
gejzírů	gejzír	k1gInPc2	gejzír
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
pole	pole	k1gNnSc4	pole
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c4	na
území	území	k1gNnSc4	území
Eurasie	Eurasie	k1gFnSc2	Eurasie
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
největší	veliký	k2eAgFnSc3d3	veliký
gejzírové	gejzírový	k2eAgFnSc3d1	gejzírová
pole	pola	k1gFnSc3	pola
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
geoložkou	geoložka	k1gFnSc7	geoložka
Taťánou	Taťána	k1gFnSc7	Taťána
Ustinovovou	Ustinovová	k1gFnSc7	Ustinovová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
teplých	teplý	k2eAgInPc2d1	teplý
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
zásobovány	zásobován	k2eAgMnPc4d1	zásobován
aktivním	aktivní	k2eAgInSc7d1	aktivní
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
místních	místní	k2eAgInPc2d1	místní
gejzírů	gejzír	k1gInPc2	gejzír
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
párou	pára	k1gFnSc7	pára
pod	pod	k7c7	pod
ostrým	ostrý	k2eAgInSc7d1	ostrý
úhlem	úhel	k1gInSc7	úhel
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
kolmo	kolmo	k6eAd1	kolmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
gejzírů	gejzír	k1gInPc2	gejzír
má	mít	k5eAaImIp3nS	mít
kolem	kolem	k7c2	kolem
ústí	ústí	k1gNnSc2	ústí
kužel	kužel	k1gInSc1	kužel
z	z	k7c2	z
vysrážených	vysrážený	k2eAgFnPc2d1	vysrážená
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc1d1	běžný
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
lokalit	lokalita	k1gFnPc2	lokalita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
k	k	k7c3	k
masivnímu	masivní	k2eAgInSc3d1	masivní
sesuvu	sesuv	k1gInSc3	sesuv
bahna	bahno	k1gNnSc2	bahno
<g/>
,	,	kIx,	,
ledu	led	k1gInSc2	led
a	a	k8xC	a
kamení	kamení	k1gNnSc2	kamení
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
přehrazení	přehrazení	k1gNnSc2	přehrazení
údolí	údolí	k1gNnPc1	údolí
nánosy	nános	k1gInPc4	nános
zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
termální	termální	k2eAgNnSc1d1	termální
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
dostaly	dostat	k5eAaPmAgInP	dostat
některé	některý	k3yIgInPc1	některý
zaplavené	zaplavený	k2eAgInPc1d1	zaplavený
gejzíry	gejzír	k1gInPc1	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Sesuv	sesuv	k1gInSc1	sesuv
nepohřbil	pohřbít	k5eNaPmAgInS	pohřbít
největší	veliký	k2eAgInSc1d3	veliký
gejzír	gejzír	k1gInSc1	gejzír
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvaný	zvaný	k2eAgInSc1d1	zvaný
Velikan	Velikan	k1gInSc1	Velikan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
pozorování	pozorování	k1gNnPc2	pozorování
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
El	Ela	k1gFnPc2	Ela
Tatio	Tatio	k6eAd1	Tatio
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
===	===	k?	===
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
El	Ela	k1gFnPc2	Ela
Tatio	Tatio	k1gMnSc1	Tatio
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vysokohorských	vysokohorský	k2eAgNnPc6d1	vysokohorské
údolích	údolí	k1gNnPc6	údolí
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
obklopena	obklopen	k2eAgNnPc1d1	obklopeno
řadou	řada	k1gFnSc7	řada
aktivních	aktivní	k2eAgMnPc2d1	aktivní
chilských	chilský	k2eAgMnPc2d1	chilský
vulkánů	vulkán	k1gInPc2	vulkán
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
okolo	okolo	k7c2	okolo
80	[number]	k4	80
aktivních	aktivní	k2eAgInPc2d1	aktivní
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
gejzírové	gejzírový	k2eAgNnSc4d1	gejzírové
pole	pole	k1gNnSc4	pole
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bylo	být	k5eAaImAgNnS	být
množství	množství	k1gNnSc1	množství
gejzírů	gejzír	k1gInPc2	gejzír
zničeno	zničit	k5eAaPmNgNnS	zničit
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
vlivem	vlivem	k7c2	vlivem
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
pole	pole	k1gNnSc4	pole
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
gejzírů	gejzír	k1gInPc2	gejzír
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
výška	výška	k1gFnSc1	výška
erupcí	erupce	k1gFnPc2	erupce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maxima	maximum	k1gNnPc4	maximum
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
útvary	útvar	k1gInPc4	útvar
až	až	k9	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
erupcí	erupce	k1gFnPc2	erupce
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
0,75	[number]	k4	0,75
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Taupo	Taupa	k1gFnSc5	Taupa
Volcanic	Volcanice	k1gFnPc2	Volcanice
Zone	Zone	k1gFnPc7	Zone
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
===	===	k?	===
</s>
</p>
<p>
<s>
Taupo	Taupa	k1gFnSc5	Taupa
Volcanic	Volcanice	k1gFnPc2	Volcanice
Zone	Zone	k1gFnPc7	Zone
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
350	[number]	k4	350
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
50	[number]	k4	50
km	km	kA	km
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
nad	nad	k7c7	nad
subdukční	subdukční	k2eAgFnSc7d1	subdukční
zónou	zóna	k1gFnSc7	zóna
dvou	dva	k4xCgFnPc2	dva
zemských	zemský	k2eAgFnPc2d1	zemská
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
subdukující	subdukující	k2eAgFnSc7d1	subdukující
se	se	k3xPyFc4	se
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
a	a	k8xC	a
Australské	australský	k2eAgFnSc2d1	australská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
ohraničena	ohraničen	k2eAgFnSc1d1	ohraničena
horou	hora	k1gFnSc7	hora
Ruapehu	Ruapeh	k1gInSc2	Ruapeh
a	a	k8xC	a
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
pak	pak	k6eAd1	pak
podvodní	podvodní	k2eAgFnSc7d1	podvodní
sopkou	sopka	k1gFnSc7	sopka
Whakatane	Whakatan	k1gInSc5	Whakatan
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
gejzírů	gejzír	k1gInPc2	gejzír
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zničeno	zničen	k2eAgNnSc1d1	zničeno
využíváním	využívání	k1gNnSc7	využívání
geotermální	geotermální	k2eAgFnSc2d1	geotermální
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
vybudováním	vybudování	k1gNnSc7	vybudování
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ještě	ještě	k6eAd1	ještě
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházel	nacházet	k5eAaImAgInS	nacházet
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc1d1	známý
gejzír	gejzír	k1gInSc1	gejzír
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Waimangu	Waimang	k1gInSc3	Waimang
Geyser	Geyser	k1gInSc1	Geyser
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
tryskat	tryskat	k5eAaImF	tryskat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Fungoval	fungovat	k5eAaImAgMnS	fungovat
po	po	k7c4	po
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sesuv	sesuv	k1gInSc1	sesuv
svahu	svah	k1gInSc2	svah
změnil	změnit	k5eAaPmAgInS	změnit
hladinu	hladina	k1gFnSc4	hladina
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Výtrysky	výtrysk	k1gInPc1	výtrysk
gejzíru	gejzír	k1gInSc2	gejzír
Waimangu	Waimang	k1gInSc2	Waimang
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
obyčejně	obyčejně	k6eAd1	obyčejně
výšky	výška	k1gFnSc2	výška
okolo	okolo	k7c2	okolo
160	[number]	k4	160
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
supervýtrysky	supervýtrysek	k1gInPc1	supervýtrysek
vyvrhly	vyvrhnout	k5eAaPmAgInP	vyvrhnout
vodu	voda	k1gFnSc4	voda
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
460	[number]	k4	460
m.	m.	k?	m.
Moderní	moderní	k2eAgInPc1d1	moderní
výzkumy	výzkum	k1gInPc1	výzkum
oblasti	oblast	k1gFnSc2	oblast
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
km	km	kA	km
mocná	mocný	k2eAgFnSc1d1	mocná
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
nachází	nacházet	k5eAaImIp3nS	nacházet
magmatické	magmatický	k2eAgNnSc4d1	magmatické
těleso	těleso	k1gNnSc4	těleso
50	[number]	k4	50
km	km	kA	km
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
160	[number]	k4	160
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Island	Island	k1gInSc1	Island
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
a	a	k8xC	a
horké	horký	k2eAgInPc1d1	horký
prameny	pramen	k1gInPc1	pramen
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
rozesety	rozeset	k2eAgFnPc1d1	rozeseta
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Haukadalur	Haukadalura	k1gFnPc2	Haukadalura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
Islandu	Island	k1gInSc2	Island
je	být	k5eAaImIp3nS	být
i	i	k9	i
Geysir	Geysir	k1gInSc1	Geysir
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tryská	tryskat	k5eAaImIp3nS	tryskat
již	již	k6eAd1	již
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
který	který	k3yIgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
pojmenování	pojmenování	k1gNnSc4	pojmenování
všem	všecek	k3xTgInPc3	všecek
gejzírům	gejzír	k1gInPc3	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
před	před	k7c7	před
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
gejzír	gejzír	k1gInSc1	gejzír
odmlčel	odmlčet	k5eAaPmAgMnS	odmlčet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
otřesech	otřes	k1gInPc6	otřes
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
vyvrhovat	vyvrhovat	k5eAaImF	vyvrhovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
páru	pára	k1gFnSc4	pára
několikrát	několikrát	k6eAd1	několikrát
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc1	všechen
erupce	erupce	k1gFnPc1	erupce
gejzíru	gejzír	k1gInSc2	gejzír
ustaly	ustat	k5eAaPmAgFnP	ustat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
gejzír	gejzír	k1gInSc1	gejzír
aktivoval	aktivovat	k5eAaBmAgInS	aktivovat
a	a	k8xC	a
deaktivoval	deaktivovat	k5eAaImAgInS	deaktivovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zemětřeseních	zemětřesení	k1gNnPc6	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
opět	opět	k5eAaPmF	opět
Geysir	Geysir	k1gInSc4	Geysir
aktivovalo	aktivovat	k5eAaBmAgNnS	aktivovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
materiál	materiál	k1gInSc4	materiál
nepravidelně	pravidelně	k6eNd1	pravidelně
a	a	k8xC	a
nepředvídatelně	předvídatelně	k6eNd1	předvídatelně
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
gejzír	gejzír	k1gInSc1	gejzír
Strokkur	Strokkura	k1gFnPc2	Strokkura
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tryská	tryskat	k5eAaImIp3nS	tryskat
každých	každý	k3xTgNnPc2	každý
5	[number]	k4	5
až	až	k8xS	až
8	[number]	k4	8
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
30	[number]	k4	30
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Neaktivní	aktivní	k2eNgFnSc6d1	neaktivní
oblasti	oblast	k1gFnSc6	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
dvě	dva	k4xCgNnPc4	dva
další	další	k2eAgNnPc4d1	další
gejzírová	gejzírový	k2eAgNnPc4d1	gejzírové
pole	pole	k1gNnPc4	pole
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Beowawe	Beowawe	k1gNnSc1	Beowawe
a	a	k8xC	a
Steamboat	Steamboat	k2eAgInSc1d1	Steamboat
Springs	Springs	k1gInSc1	Springs
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
výstavbou	výstavba	k1gFnSc7	výstavba
geotermální	geotermální	k2eAgFnSc2d1	geotermální
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
vrtů	vrt	k1gInPc2	vrt
potřebných	potřebný	k2eAgInPc2d1	potřebný
pro	pro	k7c4	pro
chod	chod	k1gInSc4	chod
elektrárny	elektrárna	k1gFnSc2	elektrárna
způsobila	způsobit	k5eAaPmAgFnS	způsobit
snížení	snížení	k1gNnSc4	snížení
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
hladiny	hladina	k1gFnSc2	hladina
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
gejzírů	gejzír	k1gInPc2	gejzír
v	v	k7c6	v
Orakei	Orake	k1gInSc6	Orake
Korako	Korako	k1gNnSc4	Korako
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byly	být	k5eAaImAgInP	být
zaplaveny	zaplavit	k5eAaPmNgInP	zaplavit
během	během	k7c2	během
budování	budování	k1gNnSc2	budování
přehrady	přehrada	k1gFnSc2	přehrada
pro	pro	k7c4	pro
vodní	vodní	k2eAgFnSc4d1	vodní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
Ohakuri	Ohakur	k1gFnSc2	Ohakur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Wairakei	Wairake	k1gFnSc2	Wairake
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
byla	být	k5eAaImAgNnP	být
zničena	zničit	k5eAaPmNgNnP	zničit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
taktéž	taktéž	k?	taktéž
výstavbou	výstavba	k1gFnSc7	výstavba
geotermální	geotermální	k2eAgFnSc2d1	geotermální
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
novozélandské	novozélandský	k2eAgFnPc1d1	novozélandská
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
také	také	k9	také
zničeny	zničit	k5eAaPmNgFnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Taupo	Taupa	k1gFnSc5	Taupa
Spa	Spa	k1gMnSc1	Spa
doplatila	doplatit	k5eAaPmAgFnS	doplatit
na	na	k7c4	na
pokles	pokles	k1gInSc4	pokles
hladiny	hladina	k1gFnSc2	hladina
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Waikato	Waikat	k2eAgNnSc1d1	Waikato
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
Rotomahana	Rotomahana	k1gFnSc1	Rotomahana
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
sopky	sopka	k1gFnSc2	sopka
Mount	Mount	k1gMnSc1	Mount
Tarawera	Tarawera	k1gFnSc1	Tarawera
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepravé	pravý	k2eNgInPc1d1	nepravý
gejzíry	gejzír	k1gInPc1	gejzír
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
množství	množství	k1gNnSc1	množství
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
jinými	jiný	k2eAgInPc7d1	jiný
procesy	proces	k1gInPc7	proces
než	než	k8xS	než
ohříváním	ohřívání	k1gNnSc7	ohřívání
vody	voda	k1gFnSc2	voda
o	o	k7c4	o
magma	magma	k1gNnSc4	magma
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
voda	voda	k1gFnSc1	voda
není	být	k5eNaImIp3nS	být
pak	pak	k6eAd1	pak
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
expanzí	expanze	k1gFnSc7	expanze
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
z	z	k7c2	z
přísně	přísně	k6eAd1	přísně
geologického	geologický	k2eAgInSc2d1	geologický
pohledu	pohled	k1gInSc2	pohled
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
pravé	pravý	k2eAgInPc4d1	pravý
gejzíry	gejzír	k1gInPc4	gejzír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
často	často	k6eAd1	často
pojmenování	pojmenování	k1gNnSc1	pojmenování
gejzír	gejzír	k1gInSc1	gejzír
používáno	používán	k2eAgNnSc4d1	používáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozšířeném	rozšířený	k2eAgNnSc6d1	rozšířené
použití	použití	k1gNnSc6	použití
slovo	slovo	k1gNnSc1	slovo
gejzír	gejzír	k1gInSc4	gejzír
označuje	označovat	k5eAaImIp3nS	označovat
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
voda	voda	k1gFnSc1	voda
tryská	tryskat	k5eAaImIp3nS	tryskat
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
přerušovaně	přerušovaně	k6eAd1	přerušovaně
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Umělé	umělý	k2eAgInPc1d1	umělý
gejzíry	gejzír	k1gInPc1	gejzír
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
geotermální	geotermální	k2eAgInSc1d1	geotermální
gradient	gradient	k1gInSc1	gradient
pro	pro	k7c4	pro
ohřev	ohřev	k1gInSc4	ohřev
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
uměle	uměle	k6eAd1	uměle
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
vrty	vrt	k1gInPc1	vrt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
fungovaly	fungovat	k5eAaImAgInP	fungovat
jako	jako	k9	jako
gejzíry	gejzír	k1gInPc1	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
proudí	proudit	k5eAaPmIp3nS	proudit
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
uměle	uměle	k6eAd1	uměle
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
zásobován	zásobovat	k5eAaImNgInS	zásobovat
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
gejzír	gejzír	k1gInSc1	gejzír
Little	Little	k1gFnSc2	Little
Old	Olda	k1gFnPc2	Olda
Faithful	Faithfula	k1gFnPc2	Faithfula
v	v	k7c6	v
Calistoze	Calistoz	k1gInSc5	Calistoz
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
John	John	k1gMnSc1	John
Rinehart	Rinehart	k1gInSc1	Rinehart
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Geyser	Geyser	k1gMnSc1	Geyser
Gazing	Gazing	k1gInSc1	Gazing
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
49	[number]	k4	49
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
náhodou	náhodou	k6eAd1	náhodou
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
snažili	snažit	k5eAaImAgMnP	snažit
najít	najít	k5eAaPmF	najít
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
vyhloubit	vyhloubit	k5eAaPmF	vyhloubit
studnu	studna	k1gFnSc4	studna
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
navrtali	navrtat	k5eAaPmAgMnP	navrtat
systém	systém	k1gInSc4	systém
starého	starý	k2eAgInSc2d1	starý
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gejzíry	gejzír	k1gInPc1	gejzír
se	s	k7c7	s
studenou	studený	k2eAgFnSc7d1	studená
vodou	voda	k1gFnSc7	voda
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
gejzíry	gejzír	k1gInPc4	gejzír
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nevyvrhují	vyvrhovat	k5eNaImIp3nP	vyvrhovat
vařící	vařící	k2eAgFnSc4d1	vařící
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vodu	voda	k1gFnSc4	voda
studenou	studený	k2eAgFnSc4d1	studená
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
jsou	být	k5eAaImIp3nP	být
takovéto	takovýto	k3xDgInPc4	takovýto
gejzíry	gejzír	k1gInPc4	gejzír
řízeny	řízen	k2eAgInPc4d1	řízen
nashromážděným	nashromážděný	k2eAgInSc7d1	nashromážděný
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
vlivem	vliv	k1gInSc7	vliv
průchodnosti	průchodnost	k1gFnSc2	průchodnost
propustnými	propustný	k2eAgFnPc7d1	propustná
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
pak	pak	k6eAd1	pak
uniká	unikat	k5eAaImIp3nS	unikat
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
jen	jen	k9	jen
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nějak	nějak	k6eAd1	nějak
narušena	narušen	k2eAgFnSc1d1	narušena
kůra	kůra	k1gFnSc1	kůra
(	(	kIx(	(
<g/>
zlomy	zlom	k1gInPc1	zlom
<g/>
,	,	kIx,	,
praskliny	prasklina	k1gFnPc1	prasklina
<g/>
,	,	kIx,	,
vrty	vrt	k1gInPc1	vrt
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
natlakované	natlakovaný	k2eAgNnSc1d1	natlakované
místo	místo	k1gNnSc1	místo
nashromáždění	nashromáždění	k1gNnSc1	nashromáždění
CO2	CO2	k1gFnSc2	CO2
proraženo	prorazit	k5eAaPmNgNnS	prorazit
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
CO2	CO2	k1gFnSc1	CO2
unikat	unikat	k5eAaImF	unikat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
vynášet	vynášet	k5eAaImF	vynášet
okolní	okolní	k2eAgInSc4d1	okolní
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
CO2	CO2	k1gMnPc2	CO2
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
stavu	stav	k1gInSc2	stav
rozpuštěny	rozpuštěn	k2eAgInPc4d1	rozpuštěn
jako	jako	k8xC	jako
malé	malý	k2eAgFnPc4d1	malá
bublinky	bublinka	k1gFnPc4	bublinka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
když	když	k8xS	když
ale	ale	k8xC	ale
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
zvětšovat	zvětšovat	k5eAaImF	zvětšovat
svůj	svůj	k3xOyFgInSc4	svůj
objem	objem	k1gInSc4	objem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vytlačení	vytlačení	k1gNnSc3	vytlačení
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Studený	studený	k2eAgInSc1d1	studený
gejzír	gejzír	k1gInSc1	gejzír
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
horkým	horký	k2eAgInPc3d1	horký
gejzírům	gejzír	k1gInPc3	gejzír
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
jen	jen	k9	jen
více	hodně	k6eAd2	hodně
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
zpěněná	zpěněný	k2eAgFnSc1d1	zpěněná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
studené	studený	k2eAgInPc4d1	studený
gejzíry	gejzír	k1gInPc4	gejzír
patří	patřit	k5eAaImIp3nS	patřit
Crystal	Crystal	k1gMnSc1	Crystal
Geyser	Geyser	k1gMnSc1	Geyser
nedaleko	nedaleko	k7c2	nedaleko
Green	Grena	k1gFnPc2	Grena
River	Rivero	k1gNnPc2	Rivero
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
studené	studený	k2eAgInPc1d1	studený
gejzíry	gejzír	k1gInPc1	gejzír
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Brubbel	Brubbel	k1gMnSc1	Brubbel
a	a	k8xC	a
Andernach	Andernach	k1gMnSc1	Andernach
<g/>
,	,	kIx,	,
v	v	k7c6	v
lázeňském	lázeňský	k2eAgInSc6d1	lázeňský
areálu	areál	k1gInSc6	areál
Herľany-Rankovce	Herľany-Rankovec	k1gInSc2	Herľany-Rankovec
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Herlianský	Herlianský	k2eAgInSc1d1	Herlianský
gejzír	gejzír	k1gInSc1	gejzír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Permanentní	permanentní	k2eAgInPc1d1	permanentní
chrliče	chrlič	k1gInPc1	chrlič
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
chrlí	chrlit	k5eAaImIp3nP	chrlit
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
doba	doba	k1gFnSc1	doba
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
podzemních	podzemní	k2eAgFnPc2d1	podzemní
puklin	puklina	k1gFnPc2	puklina
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
označení	označení	k1gNnSc4	označení
gejzír	gejzír	k1gInSc4	gejzír
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
přínos	přínos	k1gInSc1	přínos
==	==	k?	==
</s>
</p>
<p>
<s>
Gejzíry	gejzír	k1gInPc1	gejzír
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
jako	jako	k9	jako
turistická	turistický	k2eAgFnSc1d1	turistická
atrakce	atrakce	k1gFnSc1	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
islandské	islandský	k2eAgInPc1d1	islandský
gejzíry	gejzír	k1gInPc1	gejzír
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejvíce	nejvíce	k6eAd1	nejvíce
turisticky	turisticky	k6eAd1	turisticky
navštěvované	navštěvovaný	k2eAgFnPc1d1	navštěvovaná
lokality	lokalita	k1gFnPc1	lokalita
s	s	k7c7	s
gejzíry	gejzír	k1gInPc7	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
vystřikující	vystřikující	k2eAgFnSc1d1	vystřikující
z	z	k7c2	z
gejzírů	gejzír	k1gInPc2	gejzír
používána	používán	k2eAgFnSc1d1	používána
pro	pro	k7c4	pro
vytápění	vytápění	k1gNnSc4	vytápění
skleníků	skleník	k1gInPc2	skleník
a	a	k8xC	a
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
nemohly	moct	k5eNaImAgInP	moct
růst	růst	k1gInSc4	růst
kvůli	kvůli	k7c3	kvůli
chladnému	chladný	k2eAgNnSc3d1	chladné
nehostinnému	hostinný	k2eNgNnSc3d1	nehostinné
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
<s>
Pára	pára	k1gFnSc1	pára
a	a	k8xC	a
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
byla	být	k5eAaImAgFnS	být
taktéž	taktéž	k?	taktéž
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
vyhřívání	vyhřívání	k1gNnSc4	vyhřívání
islandských	islandský	k2eAgFnPc2d1	islandská
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
americké	americký	k2eAgFnSc2d1	americká
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
pro	pro	k7c4	pro
energetiku	energetika	k1gFnSc4	energetika
(	(	kIx(	(
<g/>
DOE	DOE	kA	DOE
<g/>
)	)	kIx)	)
podporovalo	podporovat	k5eAaImAgNnS	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
využití	využití	k1gNnSc2	využití
geotermální	geotermální	k2eAgFnSc2d1	geotermální
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Geysers-Calistoga	Geysers-Calistoga	k1gFnSc1	Geysers-Calistoga
Known	Known	k1gInSc4	Known
Geothermal	Geothermal	k1gInSc1	Geothermal
Resource	Resourka	k1gFnSc6	Resourka
Area	Ares	k1gMnSc2	Ares
poblíž	poblíž	k7c2	poblíž
Calistogy	Calistoga	k1gFnSc2	Calistoga
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
pomocí	pomocí	k7c2	pomocí
vědeckých	vědecký	k2eAgInPc2d1	vědecký
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Geothermal	Geothermal	k1gMnSc1	Geothermal
Loan	Loan	k1gMnSc1	Loan
Guarantee	Guarantee	k1gFnPc2	Guarantee
Program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgInSc1d3	veliký
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
přínos	přínos	k1gInSc1	přínos
z	z	k7c2	z
gejzírů	gejzír	k1gInPc2	gejzír
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
turismu	turismus	k1gInSc6	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zavítalo	zavítat	k5eAaPmAgNnS	zavítat
na	na	k7c4	na
Island	Island	k1gInSc4	Island
přibližně	přibližně	k6eAd1	přibližně
550	[number]	k4	550
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
75,4	[number]	k4	75,4
%	%	kIx~	%
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
oblast	oblast	k1gFnSc4	oblast
Haukadalur	Haukadalura	k1gFnPc2	Haukadalura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
gejzíry	gejzír	k1gInPc7	gejzír
Strokkur	Strokkura	k1gFnPc2	Strokkura
a	a	k8xC	a
The	The	k1gFnSc7	The
Great	Great	k2eAgInSc4d1	Great
Geysir	Geysir	k1gInSc4	Geysir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
"	"	kIx"	"
<g/>
Gejzíry	gejzír	k1gInPc1	gejzír
<g/>
"	"	kIx"	"
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
tělesech	těleso	k1gNnPc6	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
(	(	kIx(	(
<g/>
či	či	k8xC	či
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
<g/>
)	)	kIx)	)
výtrysky	výtrysk	k1gInPc1	výtrysk
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc2	označení
gejzíry	gejzír	k1gInPc1	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výtrysky	výtrysk	k1gInPc1	výtrysk
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pozemských	pozemský	k2eAgInPc2d1	pozemský
gejzírů	gejzír	k1gInPc2	gejzír
tvořeny	tvořit	k5eAaImNgInP	tvořit
hlavně	hlavně	k6eAd1	hlavně
plyny	plyn	k1gInPc1	plyn
s	s	k7c7	s
pevnými	pevný	k2eAgFnPc7d1	pevná
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
plyny	plyn	k1gInPc1	plyn
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
výstupu	výstup	k1gInSc2	výstup
vynesly	vynést	k5eAaPmAgInP	vynést
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
mimozemské	mimozemský	k2eAgInPc1d1	mimozemský
gejzíry	gejzír	k1gInPc1	gejzír
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
vodu	voda	k1gFnSc4	voda
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Výtrysky	výtrysk	k1gInPc1	výtrysk
podobné	podobný	k2eAgInPc1d1	podobný
gejzírům	gejzír	k1gInPc3	gejzír
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
společně	společně	k6eAd1	společně
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
ledu	led	k1gInSc2	led
a	a	k8xC	a
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
čpavek	čpavek	k1gInSc1	čpavek
<g/>
,	,	kIx,	,
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
a	a	k8xC	a
křemičitany	křemičitan	k1gInPc1	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
gejzíry	gejzír	k1gInPc1	gejzír
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
Tygřích	tygří	k2eAgInPc2d1	tygří
drápů	dráp	k1gInPc2	dráp
na	na	k7c6	na
Saturnově	Saturnův	k2eAgInSc6d1	Saturnův
měsíci	měsíc	k1gInSc6	měsíc
Enceladu	Encelad	k1gInSc2	Encelad
během	během	k7c2	během
oběhu	oběh	k1gInSc2	oběh
sondy	sonda	k1gFnPc4	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
mechanismus	mechanismus	k1gInSc1	mechanismus
vzniku	vznik	k1gInSc2	vznik
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
plně	plně	k6eAd1	plně
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
slapovými	slapový	k2eAgInPc7d1	slapový
procesy	proces	k1gInPc7	proces
generujícími	generující	k2eAgInPc7d1	generující
teplo	teplo	k6eAd1	teplo
způsobované	způsobovaný	k2eAgInPc4d1	způsobovaný
orbitální	orbitální	k2eAgInPc4d1	orbitální
rezonancí	rezonance	k1gFnSc7	rezonance
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Dione	Dion	k1gInSc5	Dion
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
výtrysky	výtrysk	k1gInPc4	výtrysk
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
Enceladu	Encelad	k1gInSc2	Encelad
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zodpovědné	zodpovědný	k2eAgFnPc1d1	zodpovědná
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
prstence	prstenec	k1gInSc2	prstenec
E.	E.	kA	E.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
překvapením	překvapení	k1gNnSc7	překvapení
průletu	průlet	k1gInSc2	průlet
americké	americký	k2eAgFnSc2d1	americká
planetární	planetární	k2eAgFnSc2d1	planetární
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
okolo	okolo	k7c2	okolo
Neptunu	Neptun	k1gInSc2	Neptun
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
objevení	objevení	k1gNnSc1	objevení
výtrysků	výtrysk	k1gInPc2	výtrysk
podobných	podobný	k2eAgInPc2d1	podobný
gejzírům	gejzír	k1gInPc3	gejzír
na	na	k7c6	na
Neptunově	Neptunův	k2eAgInSc6d1	Neptunův
měsíci	měsíc	k1gInSc6	měsíc
Tritonu	triton	k1gInSc2	triton
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
výtrysk	výtrysk	k1gInSc4	výtrysk
materiálu	materiál	k1gInSc2	materiál
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
km	km	kA	km
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
ukládání	ukládání	k1gNnSc2	ukládání
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
až	až	k9	až
150	[number]	k4	150
km	km	kA	km
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
výtrysku	výtrysk	k1gInSc2	výtrysk
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážně	převážně	k6eAd1	převážně
špatně	špatně	k6eAd1	špatně
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
dusík	dusík	k1gInSc1	dusík
společně	společně	k6eAd1	společně
s	s	k7c7	s
prachovými	prachový	k2eAgFnPc7d1	prachová
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
Tritonovy	tritonův	k2eAgInPc1d1	tritonův
gejzíry	gejzír	k1gInPc1	gejzír
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
subsolárního	subsolární	k2eAgInSc2d1	subsolární
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebné	potřebný	k2eAgNnSc1d1	potřebné
teplo	teplo	k1gNnSc1	teplo
je	být	k5eAaImIp3nS	být
dodávané	dodávaný	k2eAgNnSc1d1	dodávané
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
Tritonu	triton	k1gInSc2	triton
tvoří	tvořit	k5eAaImIp3nS	tvořit
poloprůsvitná	poloprůsvitný	k2eAgFnSc1d1	poloprůsvitná
vrstva	vrstva	k1gFnSc1	vrstva
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
tmavšího	tmavý	k2eAgInSc2d2	tmavší
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
podobný	podobný	k2eAgInSc1d1	podobný
jev	jev	k1gInSc1	jev
jako	jako	k8xS	jako
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tmavá	tmavý	k2eAgFnSc1d1	tmavá
vrstva	vrstva	k1gFnSc1	vrstva
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
účinněji	účinně	k6eAd2	účinně
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
se	se	k3xPyFc4	se
a	a	k8xC	a
akumulované	akumulovaný	k2eAgNnSc1d1	akumulované
teplo	teplo	k1gNnSc1	teplo
pak	pak	k6eAd1	pak
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
zmrzlý	zmrzlý	k2eAgInSc4d1	zmrzlý
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
<g/>
.	.	kIx.	.
</s>
<s>
Vypařováním	vypařování	k1gNnSc7	vypařování
narůstá	narůstat	k5eAaImIp3nS	narůstat
tlak	tlak	k1gInSc1	tlak
pod	pod	k7c7	pod
zmrzlou	zmrzlý	k2eAgFnSc7d1	zmrzlá
vrstvou	vrstva	k1gFnSc7	vrstva
až	až	k9	až
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
prolomení	prolomení	k1gNnSc3	prolomení
a	a	k8xC	a
výtrysku	výtrysk	k1gInSc3	výtrysk
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc4	snímek
sondy	sonda	k1gFnSc2	sonda
Voyger	Voyger	k1gInSc1	Voyger
2	[number]	k4	2
ukázaly	ukázat	k5eAaPmAgInP	ukázat
množství	množství	k1gNnSc4	množství
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
tmavým	tmavý	k2eAgInSc7d1	tmavý
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
<g/>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobné	podobný	k2eAgInPc1d1	podobný
procesy	proces	k1gInPc1	proces
jako	jako	k8xC	jako
u	u	k7c2	u
Tritonu	triton	k1gInSc2	triton
probíhají	probíhat	k5eAaImIp3nP	probíhat
i	i	k9	i
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polární	polární	k2eAgFnSc6d1	polární
čepičce	čepička	k1gFnSc6	čepička
Marsu	Mars	k1gInSc2	Mars
během	během	k7c2	během
každého	každý	k3xTgNnSc2	každý
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
nepovedlo	povést	k5eNaPmAgNnS	povést
tyto	tento	k3xDgFnPc4	tento
erupce	erupce	k1gFnPc4	erupce
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
přímo	přímo	k6eAd1	přímo
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
nepřímých	přímý	k2eNgInPc2d1	nepřímý
důkazů	důkaz	k1gInPc2	důkaz
jejich	jejich	k3xOp3gFnSc2	jejich
existence	existence	k1gFnSc2	existence
jako	jako	k8xC	jako
tmavé	tmavý	k2eAgFnSc2d1	tmavá
skvrny	skvrna	k1gFnSc2	skvrna
a	a	k8xC	a
světlejší	světlý	k2eAgFnSc2d2	světlejší
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Tmavší	tmavý	k2eAgFnPc1d2	tmavší
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
prachem	prach	k1gInSc7	prach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyvržen	vyvrhnout	k5eAaPmNgInS	vyvrhnout
erupcí	erupce	k1gFnSc7	erupce
gejzíru	gejzír	k1gInSc2	gejzír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Geyser	Geysra	k1gFnPc2	Geysra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RINEHART	RINEHART	kA	RINEHART
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Sargent	Sargent	k1gMnSc1	Sargent
<g/>
.	.	kIx.	.
</s>
<s>
Geysers	Geysers	k1gInSc1	Geysers
and	and	k?	and
Geothermal	Geothermal	k1gInSc1	Geothermal
Energy	Energ	k1gInPc1	Energ
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Springer	Springer	k1gMnSc1	Springer
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
-	-	kIx~	-
<g/>
90489	[number]	k4	90489
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SCOTT	SCOTT	kA	SCOTT
<g/>
,	,	kIx,	,
T.	T.	kA	T.
Bryan	Bryan	k1gInSc1	Bryan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Geysers	Geysers	k1gInSc1	Geysers
of	of	k?	of
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
,	,	kIx,	,
Third	Third	k1gInSc1	Third
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
Press	Press	k1gInSc1	Press
of	of	k?	of
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
87081	[number]	k4	87081
<g/>
-	-	kIx~	-
<g/>
365	[number]	k4	365
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SCHREIER	SCHREIER	kA	SCHREIER
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
field	field	k6eAd1	field
guide	guide	k6eAd1	guide
to	ten	k3xDgNnSc1	ten
Yellowstone	Yellowston	k1gInSc5	Yellowston
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
geysers	geysers	k1gInSc1	geysers
<g/>
,	,	kIx,	,
hot	hot	k0	hot
springs	springs	k6eAd1	springs
and	and	k?	and
fumaroles	fumaroles	k1gInSc1	fumaroles
<g/>
.	.	kIx.	.
</s>
<s>
Moose	Moose	k1gFnSc1	Moose
Wyo	Wyo	k1gFnSc1	Wyo
<g/>
:	:	kIx,	:
Homestead	Homestead	k1gInSc1	Homestead
Pub	Pub	k1gFnSc2	Pub
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
943972	[number]	k4	943972
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mofetta	Mofetta	k1gFnSc1	Mofetta
</s>
</p>
<p>
<s>
Termální	termální	k2eAgInSc4d1	termální
pramen	pramen	k1gInSc4	pramen
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gejzír	gejzír	k1gInSc1	gejzír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gejzír	gejzír	k1gInSc1	gejzír
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Gejzíry	gejzír	k1gInPc7	gejzír
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
geology	geolog	k1gMnPc4	geolog
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
věnující	věnující	k2eAgFnPc1d1	věnující
se	se	k3xPyFc4	se
dlouhodobému	dlouhodobý	k2eAgNnSc3d1	dlouhodobé
sledování	sledování	k1gNnSc3	sledování
gejzírů	gejzír	k1gInPc2	gejzír
a	a	k8xC	a
shromažďování	shromažďování	k1gNnSc6	shromažďování
dat	datum	k1gNnPc2	datum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Návod	návod	k1gInSc1	návod
na	na	k7c4	na
experimentální	experimentální	k2eAgFnSc4d1	experimentální
výrobu	výroba	k1gFnSc4	výroba
gejzíru	gejzír	k1gInSc2	gejzír
doma	doma	k6eAd1	doma
</s>
</p>
