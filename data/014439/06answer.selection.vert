<s>
Časové	časový	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
<g/>
,	,	kIx,
časový	časový	k2eAgInSc1d1
multiplex	multiplex	k1gInSc1
nebo	nebo	k8xC
multiplex	multiplex	k1gInSc1
s	s	k7c7
časovým	časový	k2eAgNnSc7d1
dělením	dělení	k1gNnSc7
(	(	kIx(
<g/>
TDM	TDM	kA
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
time	tim	k1gInSc2
division	division	k1gInSc1
multiplex	multiplex	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
princip	princip	k1gInSc1
přenosu	přenos	k1gInSc2
více	hodně	k6eAd2
signálů	signál	k1gInPc2
jedním	jeden	k4xCgInSc7
společným	společný	k2eAgNnSc7d1
přenosovým	přenosový	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc4d1
signály	signál	k1gInPc4
odděleny	oddělen	k2eAgInPc4d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
každý	každý	k3xTgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
vysílán	vysílán	k2eAgMnSc1d1
pouze	pouze	k6eAd1
v	v	k7c6
krátkých	krátký	k2eAgInPc6d1
pevně	pevně	k6eAd1
definovaných	definovaný	k2eAgInPc6d1
časových	časový	k2eAgInPc6d1
intervalech	interval	k1gInPc6
nazývaných	nazývaný	k2eAgInPc2d1
časové	časový	k2eAgInPc4d1
sloty	slot	k1gInPc4
(	(	kIx(
<g/>
timeslot	timeslot	k1gMnSc1
–	–	k?
TS	ts	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>