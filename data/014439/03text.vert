<s>
Časový	časový	k2eAgInSc1d1
multiplex	multiplex	k1gInSc1
</s>
<s>
Časové	časový	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
<g/>
,	,	kIx,
časový	časový	k2eAgInSc1d1
multiplex	multiplex	k1gInSc1
nebo	nebo	k8xC
multiplex	multiplex	k1gInSc1
s	s	k7c7
časovým	časový	k2eAgNnSc7d1
dělením	dělení	k1gNnSc7
(	(	kIx(
<g/>
TDM	TDM	kA
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
time	tim	k1gInSc2
division	division	k1gInSc1
multiplex	multiplex	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
princip	princip	k1gInSc1
přenosu	přenos	k1gInSc2
více	hodně	k6eAd2
signálů	signál	k1gInPc2
jedním	jeden	k4xCgInSc7
společným	společný	k2eAgNnSc7d1
přenosovým	přenosový	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc4d1
signály	signál	k1gInPc4
odděleny	oddělen	k2eAgInPc4d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
každý	každý	k3xTgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
vysílán	vysílán	k2eAgMnSc1d1
pouze	pouze	k6eAd1
v	v	k7c6
krátkých	krátký	k2eAgInPc6d1
pevně	pevně	k6eAd1
definovaných	definovaný	k2eAgInPc6d1
časových	časový	k2eAgInPc6d1
intervalech	interval	k1gInPc6
nazývaných	nazývaný	k2eAgInPc2d1
časové	časový	k2eAgInPc4d1
sloty	slot	k1gInPc4
(	(	kIx(
<g/>
timeslot	timeslot	k1gMnSc1
–	–	k?
TS	ts	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laicky	laicky	k6eAd1
řečeno	říct	k5eAaPmNgNnS
„	„	k?
<g/>
každý	každý	k3xTgInSc1
chvilku	chvilka	k1gFnSc4
tahá	tahat	k5eAaImIp3nS
pilku	pilka	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
rámcové	rámcový	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
časové	časový	k2eAgFnPc1d1
sloty	slota	k1gFnPc1
seskupeny	seskupit	k5eAaPmNgFnP
do	do	k7c2
větší	veliký	k2eAgFnSc2d2
pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
opakující	opakující	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
nazývané	nazývaný	k2eAgFnPc1d1
rámec	rámec	k1gInSc4
(	(	kIx(
<g/>
frame	framat	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
časového	časový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
</s>
<s>
Multiplex	multiplex	k1gInSc1
s	s	k7c7
časovým	časový	k2eAgNnSc7d1
dělením	dělení	k1gNnSc7
(	(	kIx(
<g/>
TDM	TDM	kA
<g/>
)	)	kIx)
</s>
<s>
Pokud	pokud	k8xS
budeme	být	k5eAaImBp1nP
mít	mít	k5eAaImF
čtyři	čtyři	k4xCgInPc4
signály	signál	k1gInPc4
A	A	kA
<g/>
,	,	kIx,
<g/>
B	B	kA
<g/>
,	,	kIx,
<g/>
C	C	kA
a	a	k8xC
D	D	kA
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
vysílání	vysílání	k1gNnSc1
pomocí	pomocí	k7c2
časového	časový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
vypadat	vypadat	k5eAaPmF,k5eAaImF
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
ABCDABCDABCDABCD	ABCDABCDABCDABCD	kA
<g/>
…	…	k?
a	a	k8xC
tak	tak	k9
stále	stále	k6eAd1
dokola	dokola	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
problém	problém	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
rozlišit	rozlišit	k5eAaPmF
<g/>
,	,	kIx,
kde	kde	k6eAd1
timeslot	timeslot	k1gInSc1
pro	pro	k7c4
konkrétní	konkrétní	k2eAgInSc4d1
signál	signál	k1gInSc4
začíná	začínat	k5eAaImIp3nS
a	a	k8xC
kde	kde	k6eAd1
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastějším	častý	k2eAgNnSc7d3
řešením	řešení	k1gNnSc7
je	být	k5eAaImIp3nS
přidání	přidání	k1gNnSc4
speciálního	speciální	k2eAgInSc2d1
timeslotu	timeslot	k1gInSc2
s	s	k7c7
přesně	přesně	k6eAd1
definovaným	definovaný	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
na	na	k7c4
začátek	začátek	k1gInSc4
rámce	rámec	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
označuje	označovat	k5eAaImIp3nS
„	„	k?
<g/>
toto	tento	k3xDgNnSc4
je	být	k5eAaImIp3nS
začátek	začátek	k1gInSc1
rámce	rámec	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
timesloty	timeslota	k1gFnPc1
stejně	stejně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
(	(	kIx(
<g/>
časově	časově	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
na	na	k7c6
straně	strana	k1gFnSc6
přijímače	přijímač	k1gInSc2
najít	najít	k5eAaPmF
tento	tento	k3xDgInSc4
startovní	startovní	k2eAgInSc4d1
bod	bod	k1gInSc4
a	a	k8xC
potom	potom	k6eAd1
odpočítat	odpočítat	k5eAaPmF
potřebnou	potřebný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
do	do	k7c2
začátku	začátek	k1gInSc2
hledaného	hledaný	k2eAgNnSc2d1
TS	ts	k0
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
výše	vysoce	k6eAd2
uvedeném	uvedený	k2eAgInSc6d1
případě	případ	k1gInSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
tedy	tedy	k9
vysílal	vysílat	k5eAaImAgInS
rámec	rámec	k1gInSc1
vypadající	vypadající	k2eAgInSc1d1
takto	takto	k6eAd1
<g/>
:	:	kIx,
SABCDSABCDSABCDSABCD	SABCDSABCDSABCDSABCD	kA
<g/>
…	…	k?
<g/>
kde	kde	k6eAd1
S	s	k7c7
označuje	označovat	k5eAaImIp3nS
„	„	k?
<g/>
startovní	startovní	k2eAgInSc4d1
<g/>
“	“	k?
timeslot	timeslot	k1gInSc4
<g/>
,	,	kIx,
častěji	často	k6eAd2
označovaný	označovaný	k2eAgInSc1d1
spíše	spíše	k9
jako	jako	k8xC,k8xS
synchronizační	synchronizační	k2eAgInSc1d1
timeslot	timeslot	k1gInSc1
<g/>
,	,	kIx,
synchroskupina	synchroskupina	k1gFnSc1
rámce	rámec	k1gInSc2
nebo	nebo	k8xC
také	také	k9
rámcová	rámcový	k2eAgFnSc1d1
synchronizace	synchronizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
přístup	přístup	k1gInSc1
nám	my	k3xPp1nPc3
pomůže	pomoct	k5eAaPmIp3nS
s	s	k7c7
problémem	problém	k1gInSc7
určení	určení	k1gNnSc2
konkrétní	konkrétní	k2eAgFnSc2d1
časové	časový	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
přináší	přinášet	k5eAaImIp3nS
objem	objem	k1gInSc4
dat	datum	k1gNnPc2
navíc	navíc	k6eAd1
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yQgNnPc4,k3yIgNnPc4
nelze	lze	k6eNd1
využít	využít	k5eAaPmF
pro	pro	k7c4
přenášené	přenášený	k2eAgInPc4d1
signály	signál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klesá	klesat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
tedy	tedy	k9
efektivita	efektivita	k1gFnSc1
přenosu	přenos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
našem	náš	k3xOp1gInSc6
případě	případ	k1gInSc6
jsme	být	k5eAaImIp1nP
pro	pro	k7c4
přenos	přenos	k1gInSc4
našich	náš	k3xOp1gInPc2
čtyř	čtyři	k4xCgInPc2
signálů	signál	k1gInPc2
mohli	moct	k5eAaImAgMnP
použít	použít	k5eAaPmF
jen	jen	k9
80	#num#	k4
<g/>
%	%	kIx~
přenášené	přenášený	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
pětinu	pětina	k1gFnSc4
zabrala	zabrat	k5eAaPmAgFnS
synchroskupina	synchroskupina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
snížení	snížení	k1gNnSc1
efektivity	efektivita	k1gFnSc2
přichází	přicházet	k5eAaImIp3nS
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
daném	daný	k2eAgInSc6d1
timeslotu	timeslot	k1gInSc6
nepřenáší	přenášet	k5eNaImIp3nS
žádná	žádný	k3yNgNnPc4
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
přenos	přenos	k1gInSc1
telefonního	telefonní	k2eAgInSc2d1
hovoru	hovor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užitečná	užitečný	k2eAgFnSc1d1
data	datum	k1gNnPc4
se	se	k3xPyFc4
v	v	k7c6
timeslotu	timeslot	k1gInSc6
přenáší	přenášet	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
hovor	hovor	k1gInSc1
spojený	spojený	k2eAgInSc1d1
a	a	k8xC
účastníci	účastník	k1gMnPc1
spolu	spolu	k6eAd1
mluví	mluvit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
si	se	k3xPyFc3
například	například	k6eAd1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
odejde	odejít	k5eAaPmIp3nS
pro	pro	k7c4
tužku	tužka	k1gFnSc4
a	a	k8xC
papír	papír	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
poznamenal	poznamenat	k5eAaPmAgMnS
zjištěnou	zjištěný	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
<g/>
,	,	kIx,
nebudou	být	k5eNaImBp3nP
se	se	k3xPyFc4
v	v	k7c4
timeslotu	timeslota	k1gFnSc4
přenášet	přenášet	k5eAaImF
žádná	žádný	k3yNgNnPc4
data	datum	k1gNnPc4
a	a	k8xC
tato	tento	k3xDgFnSc1
volná	volný	k2eAgFnSc1d1
přenosová	přenosový	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
využít	využít	k5eAaPmF
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
představu	představa	k1gFnSc4
opět	opět	k6eAd1
náš	náš	k3xOp1gInSc1
příklad	příklad	k1gInSc1
<g/>
,	,	kIx,
timesloty	timeslot	k1gInPc1
A	A	kA
<g/>
,	,	kIx,
<g/>
B	B	kA
a	a	k8xC
D	D	kA
budou	být	k5eAaImBp3nP
obsazené	obsazený	k2eAgNnSc1d1
hovorem	hovor	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
účastníci	účastník	k1gMnPc1
hovoru	hovor	k1gInSc2
přenášeném	přenášený	k2eAgInSc6d1
v	v	k7c6
timeslotu	timeslot	k1gInSc6
C	C	kA
budou	být	k5eAaImBp3nP
mlčet	mlčet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílaný	vysílaný	k2eAgInSc1d1
rámec	rámec	k1gInSc1
tedy	tedy	k9
bude	být	k5eAaImBp3nS
SAB_DSAB_DSAB_DSAB_D	SAB_DSAB_DSAB_DSAB_D	k1gMnSc7
<g/>
…	…	k?
kde	kde	k6eAd1
S	s	k7c7
je	on	k3xPp3gNnSc4
opět	opět	k6eAd1
rámcová	rámcový	k2eAgFnSc1d1
synchroskupina	synchroskupin	k2eAgFnSc1d1
a	a	k8xC
_	_	kIx~
označuje	označovat	k5eAaImIp3nS
timeslot	timeslot	k1gInSc4
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c4
tuto	tento	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
nic	nic	k3yNnSc1
nepřenáší	přenášet	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Efektivita	efektivita	k1gFnSc1
přenosu	přenos	k1gInSc2
tedy	tedy	k9
znovu	znovu	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
konkrétním	konkrétní	k2eAgInSc6d1
případě	případ	k1gInSc6
na	na	k7c4
60	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
protože	protože	k8xS
potřebná	potřebný	k2eAgNnPc4d1
data	datum	k1gNnPc4
přenáší	přenášet	k5eAaImIp3nS
už	už	k9
jen	jen	k9
tři	tři	k4xCgInPc4
z	z	k7c2
pěti	pět	k4xCc2
timeslotů	timeslot	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
výhoda	výhoda	k1gFnSc1
používání	používání	k1gNnSc2
časového	časový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
je	být	k5eAaImIp3nS
garantovaná	garantovaný	k2eAgFnSc1d1
přenosová	přenosový	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
pro	pro	k7c4
každý	každý	k3xTgInSc4
z	z	k7c2
přenášených	přenášený	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
velmi	velmi	k6eAd1
často	často	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
telefonii	telefonie	k1gFnSc4
a	a	k8xC
multimediální	multimediální	k2eAgFnPc4d1
relace	relace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Principu	princip	k1gInSc3
časového	časový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
TDM	TDM	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
např.	např.	kA
Optická	optický	k2eAgFnSc1d1
Transportní	transportní	k2eAgFnSc1d1
Hiearchie	Hiearchie	k1gFnSc1
(	(	kIx(
<g/>
OTH	OTH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Plesiochronní	Plesiochronní	k2eAgFnSc1d1
Digitální	digitální	k2eAgFnSc1d1
Hierarchie	hierarchie	k1gFnSc1
(	(	kIx(
<g/>
PDH	PDH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Synchronní	synchronní	k2eAgFnSc1d1
Digitální	digitální	k2eAgFnSc1d1
Hierarchie	hierarchie	k1gFnSc1
(	(	kIx(
<g/>
SDH	SDH	kA
<g/>
/	/	kIx~
<g/>
SONET	sonet	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
ISDN	ISDN	kA
přípojky	přípojka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
TDM	TDM	kA
pro	pro	k7c4
telefonii	telefonie	k1gFnSc4
</s>
<s>
TDM	TDM	kA
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
přenosu	přenos	k1gInSc2
signálů	signál	k1gInPc2
klasické	klasický	k2eAgFnSc2d1
digitální	digitální	k2eAgFnSc2d1
telefonie	telefonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlasový	hlasový	k2eAgInSc1d1
signál	signál	k1gInSc1
se	se	k3xPyFc4
pomocí	pomocí	k7c2
modulace	modulace	k1gFnSc2
typu	typ	k1gInSc2
PCM	PCM	kA
digitalizuje	digitalizovat	k5eAaImIp3nS
a	a	k8xC
několik	několik	k4yIc4
hovorových	hovorový	k2eAgInPc2d1
kanálů	kanál	k1gInPc2
spolu	spolu	k6eAd1
s	s	k7c7
pomocnými	pomocný	k2eAgInPc7d1
a	a	k8xC
řídícími	řídící	k2eAgInPc7d1
kanály	kanál	k1gInPc7
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
do	do	k7c2
časového	časový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
TDM	TDM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
vznikne	vzniknout	k5eAaPmIp3nS
např.	např.	kA
ISDN	ISDN	kA
BRI	BRI	kA
(	(	kIx(
<g/>
2	#num#	k4
hovory	hovor	k1gInPc4
+	+	kIx~
1	#num#	k4
úzký	úzký	k2eAgInSc1d1
signalizační	signalizační	k2eAgInSc1d1
kanál	kanál	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
ISDN	ISDN	kA
PRI	PRI	kA
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
32	#num#	k4
kanálů	kanál	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
30	#num#	k4
hovorových	hovorový	k2eAgFnPc2d1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
signalizační	signalizační	k2eAgInSc1d1
a	a	k8xC
jeden	jeden	k4xCgInSc1
synchronizační	synchronizační	k2eAgInSc1d1
timeslot	timeslot	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časový	časový	k2eAgInSc1d1
multiplex	multiplex	k1gInSc1
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
také	také	k9
uvnitř	uvnitř	k7c2
digitálních	digitální	k2eAgFnPc2d1
ústředen	ústředna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
s	s	k7c7
ním	on	k3xPp3gInSc7
pracuje	pracovat	k5eAaImIp3nS
spojovací	spojovací	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Novější	nový	k2eAgInSc1d2
princip	princip	k1gInSc1
telefonie	telefonie	k1gFnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
princip	princip	k1gInSc1
VoIP	VoIP	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
přenos	přenos	k1gInSc4
hlasu	hlas	k1gInSc2
datovými	datový	k2eAgInPc7d1
pakety	paket	k1gInPc7
přes	přes	k7c4
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
není	být	k5eNaImIp3nS
spojově	spojově	k6eAd1
orientována	orientovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
zde	zde	k6eAd1
vyhrazený	vyhrazený	k2eAgInSc1d1
kanál	kanál	k1gInSc1
pro	pro	k7c4
hlasovou	hlasový	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
a	a	k8xC
sdílení	sdílení	k1gNnSc4
sítě	síť	k1gFnSc2
různými	různý	k2eAgInPc7d1
datovými	datový	k2eAgInPc7d1
toky	tok	k1gInPc7
se	se	k3xPyFc4
neřídí	řídit	k5eNaImIp3nS
zásadami	zásada	k1gFnPc7
synchronního	synchronní	k2eAgInSc2d1
časového	časový	k2eAgInSc2d1
multiplexu	multiplex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgNnSc6
je	být	k5eAaImIp3nS
VoIP	VoIP	k1gMnSc1
zásadně	zásadně	k6eAd1
odlišné	odlišný	k2eAgFnPc1d1
od	od	k7c2
TDM	TDM	kA
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Telefonie	telefonie	k1gFnSc1
</s>
<s>
Multiplexování	Multiplexování	k1gNnSc1
</s>
