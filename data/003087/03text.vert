<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Republika	republika	k1gFnSc1	republika
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
(	(	kIx(	(
<g/>
uzbecky	uzbecky	k6eAd1	uzbecky
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
zbekiston	zbekiston	k1gInSc4	zbekiston
Respublikasi	Respublikas	k1gMnPc1	Respublikas
/	/	kIx~	/
Ў	Ў	k?	Ў
Р	Р	k?	Р
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc4	jeho
sousedy	soused	k1gMnPc4	soused
jsou	být	k5eAaImIp3nP	být
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
státy	stát	k1gInPc4	stát
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
a	a	k8xC	a
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
;	;	kIx,	;
nezávislost	nezávislost	k1gFnSc4	nezávislost
získal	získat	k5eAaPmAgMnS	získat
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
rozpadu	rozpad	k1gInSc6	rozpad
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
uzbecké	uzbecký	k2eAgFnSc2d1	Uzbecká
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgFnPc4d1	spjata
s	s	k7c7	s
Perskou	perský	k2eAgFnSc7d1	perská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
turkinský	turkinský	k2eAgInSc1d1	turkinský
kaganát	kaganát	k1gInSc1	kaganát
a	a	k8xC	a
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sem	sem	k6eAd1	sem
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sem	sem	k6eAd1	sem
přinesli	přinést	k5eAaPmAgMnP	přinést
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
násilně	násilně	k6eAd1	násilně
islamizována	islamizován	k2eAgFnSc1d1	islamizována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1219	[number]	k4	1219
<g/>
-	-	kIx~	-
<g/>
1220	[number]	k4	1220
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
Čingischán	Čingischán	k1gMnSc1	Čingischán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
rabováním	rabování	k1gNnSc7	rabování
a	a	k8xC	a
vražděním	vraždění	k1gNnSc7	vraždění
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
chána	chán	k1gMnSc2	chán
Oz	Oz	k1gMnSc2	Oz
Bega	beg	k1gMnSc2	beg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1313	[number]	k4	1313
<g/>
-	-	kIx~	-
<g/>
1340	[number]	k4	1340
<g/>
.	.	kIx.	.
</s>
<s>
Obrovskou	obrovský	k2eAgFnSc4d1	obrovská
říši	říše	k1gFnSc4	říše
rozkládající	rozkládající	k2eAgFnSc4d1	rozkládající
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
<g/>
,	,	kIx,	,
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Samarkand	Samarkando	k1gNnPc2	Samarkando
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Tamerlán	Tamerlán	k1gInSc4	Tamerlán
(	(	kIx(	(
<g/>
též	též	k9	též
Timur	Timura	k1gFnPc2	Timura
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Tímur	Tímur	k1gMnSc1	Tímur
Lenk	Lenk	k1gMnSc1	Lenk
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1336	[number]	k4	1336
<g/>
-	-	kIx~	-
<g/>
1405	[number]	k4	1405
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
krutý	krutý	k2eAgMnSc1d1	krutý
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
nechal	nechat	k5eAaPmAgMnS	nechat
bohatství	bohatství	k1gNnSc4	bohatství
z	z	k7c2	z
dobytých	dobytý	k2eAgFnPc2d1	dobytá
zemí	zem	k1gFnPc2	zem
vozit	vozit	k5eAaImF	vozit
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
jeho	jeho	k3xOp3gFnSc2	jeho
říše	říš	k1gFnSc2	říš
-	-	kIx~	-
do	do	k7c2	do
Samarkandu	Samarkand	k1gInSc2	Samarkand
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Timúrovců	Timúrovec	k1gMnPc2	Timúrovec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Muhammad	Muhammad	k1gInSc1	Muhammad
Taragaj	Taragaj	k1gMnSc1	Taragaj
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
Ulugbek	Ulugbek	k1gMnSc1	Ulugbek
(	(	kIx(	(
<g/>
1394	[number]	k4	1394
<g/>
-	-	kIx~	-
<g/>
1449	[number]	k4	1449
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
milovníkem	milovník	k1gMnSc7	milovník
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
čtyřicetileté	čtyřicetiletý	k2eAgFnSc2d1	čtyřicetiletá
vlády	vláda	k1gFnSc2	vláda
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
říše	říše	k1gFnSc1	říše
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
Samarkand	Samarkand	k1gInSc1	Samarkand
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
center	centrum	k1gNnPc2	centrum
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přicházet	přicházet	k5eAaImF	přicházet
kočovní	kočovní	k2eAgMnPc1d1	kočovní
Uzbekové	Uzbek	k1gMnPc1	Uzbek
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
několik	několik	k4yIc4	několik
chanátů	chanát	k1gInPc2	chanát
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
válčit	válčit	k5eAaImF	válčit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c4	na
chanáty	chanát	k1gInPc4	chanát
carské	carský	k2eAgNnSc4d1	carské
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
si	se	k3xPyFc3	se
podmanilo	podmanit	k5eAaPmAgNnS	podmanit
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
proti	proti	k7c3	proti
ruské	ruský	k2eAgFnSc3d1	ruská
nadvládě	nadvláda	k1gFnSc3	nadvláda
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
neuhasl	uhasnout	k5eNaPmAgMnS	uhasnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Uzbecká	uzbecký	k2eAgFnSc1d1	Uzbecká
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
období	období	k1gNnSc1	období
sovětské	sovětský	k2eAgFnSc2d1	sovětská
nadvlády	nadvláda	k1gFnSc2	nadvláda
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
etnickými	etnický	k2eAgInPc7d1	etnický
konflikty	konflikt	k1gInPc7	konflikt
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
60	[number]	k4	60
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
využíval	využívat	k5eAaPmAgInS	využívat
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
hlavně	hlavně	k9	hlavně
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
roce	rok	k1gInSc6	rok
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
na	na	k7c4	na
15	[number]	k4	15
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
republiky	republika	k1gFnSc2	republika
stanul	stanout	k5eAaPmAgMnS	stanout
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
místní	místní	k2eAgFnSc2d1	místní
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Islam	Islam	k1gInSc1	Islam
Karimov	Karimov	k1gInSc1	Karimov
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
křesla	křeslo	k1gNnSc2	křeslo
vynesly	vynést	k5eAaPmAgInP	vynést
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
nedemokratické	demokratický	k2eNgInPc4d1	nedemokratický
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
Karimov	Karimov	k1gInSc4	Karimov
podporoval	podporovat	k5eAaImAgInS	podporovat
protiruské	protiruský	k2eAgFnPc4d1	protiruská
nálady	nálada	k1gFnPc4	nálada
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
etnické	etnický	k2eAgInPc1d1	etnický
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
opustilo	opustit	k5eAaPmAgNnS	opustit
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
na	na	k7c4	na
2	[number]	k4	2
milióny	milión	k4xCgInPc1	milión
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
zakázal	zakázat	k5eAaPmAgInS	zakázat
všechny	všechen	k3xTgFnPc4	všechen
opoziční	opoziční	k2eAgFnPc4d1	opoziční
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
jejich	jejich	k3xOp3gMnSc1	jejich
vůdce	vůdce	k1gMnSc1	vůdce
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
za	za	k7c4	za
protistátní	protistátní	k2eAgFnPc4d1	protistátní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
útočištěm	útočiště	k1gNnSc7	útočiště
islámských	islámský	k2eAgMnPc2d1	islámský
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Radikálové	radikál	k1gMnPc1	radikál
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
spáchat	spáchat	k5eAaPmF	spáchat
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Karimova	Karimův	k2eAgMnSc4d1	Karimův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc4	ten
atentát	atentát	k1gInSc4	atentát
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
Karimov	Karimov	k1gInSc1	Karimov
opět	opět	k6eAd1	opět
zvolen	zvolen	k2eAgMnSc1d1	zvolen
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
znovu	znovu	k6eAd1	znovu
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
zfalšované	zfalšovaný	k2eAgInPc4d1	zfalšovaný
a	a	k8xC	a
nedemokratické	demokratický	k2eNgInPc4d1	nedemokratický
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
tehdy	tehdy	k6eAd1	tehdy
získal	získat	k5eAaPmAgInS	získat
téměř	téměř	k6eAd1	téměř
92	[number]	k4	92
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
přidal	přidat	k5eAaPmAgInS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
vojsku	vojsko	k1gNnSc3	vojsko
USA	USA	kA	USA
při	při	k7c6	při
vpádu	vpád	k1gInSc6	vpád
do	do	k7c2	do
Tálibánem	Tálibán	k1gInSc7	Tálibán
ovládaného	ovládaný	k2eAgInSc2d1	ovládaný
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
s	s	k7c7	s
islámskými	islámský	k2eAgInPc7d1	islámský
radikály	radikál	k1gInPc7	radikál
se	se	k3xPyFc4	se
vyostřovaly	vyostřovat	k5eAaImAgFnP	vyostřovat
a	a	k8xC	a
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
mířilo	mířit	k5eAaImAgNnS	mířit
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
11	[number]	k4	11
sebevražedným	sebevražedný	k2eAgInPc3d1	sebevražedný
bombovým	bombový	k2eAgInPc3d1	bombový
atentátům	atentát	k1gInPc3	atentát
islamistů	islamista	k1gMnPc2	islamista
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Taškentu	Taškent	k1gInSc2	Taškent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
vybuchly	vybuchnout	k5eAaPmAgFnP	vybuchnout
bomby	bomba	k1gFnPc4	bomba
na	na	k7c6	na
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c6	na
generální	generální	k2eAgFnSc6d1	generální
prokuratuře	prokuratura	k1gFnSc6	prokuratura
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Taškentu	Taškent	k1gInSc2	Taškent
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útoku	útok	k1gInSc3	útok
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
uzbecká	uzbecký	k2eAgFnSc1d1	Uzbecká
vláda	vláda	k1gFnSc1	vláda
zapojila	zapojit	k5eAaPmAgFnS	zapojit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
USA	USA	kA	USA
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Andižan	Andižany	k1gInPc2	Andižany
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
protivládní	protivládní	k2eAgFnSc2d1	protivládní
akce	akce	k1gFnSc2	akce
-	-	kIx~	-
ozbrojení	ozbrojený	k2eAgMnPc1d1	ozbrojený
povstalci	povstalec	k1gMnPc1	povstalec
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
napadli	napadnout	k5eAaPmAgMnP	napadnout
policejní	policejní	k2eAgFnSc4d1	policejní
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
kasárny	kasárna	k1gNnPc7	kasárna
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
pak	pak	k6eAd1	pak
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
526	[number]	k4	526
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
vypálení	vypálení	k1gNnSc1	vypálení
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
celodenní	celodenní	k2eAgFnSc1d1	celodenní
demonstrace	demonstrace	k1gFnSc1	demonstrace
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Navečer	navečer	k6eAd1	navečer
vojáci	voják	k1gMnPc1	voják
stříleli	střílet	k5eAaImAgMnP	střílet
do	do	k7c2	do
davu	dav	k1gInSc2	dav
a	a	k8xC	a
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
demonstrantů	demonstrant	k1gMnPc2	demonstrant
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
uvalil	uvalit	k5eAaPmAgInS	uvalit
na	na	k7c4	na
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
embargo	embargo	k1gNnSc4	embargo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
nedošlo	dojít	k5eNaPmAgNnS	dojít
v	v	k7c6	v
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
k	k	k7c3	k
žádným	žádný	k3yNgInPc3	žádný
teroristickým	teroristický	k2eAgInPc3d1	teroristický
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
Karimov	Karimov	k1gInSc1	Karimov
zvolen	zvolit	k5eAaPmNgInS	zvolit
opět	opět	k6eAd1	opět
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
a	a	k8xC	a
Karimov	Karimov	k1gInSc4	Karimov
je	být	k5eAaImIp3nS	být
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
s	s	k7c7	s
90,39	[number]	k4	90,39
<g/>
%	%	kIx~	%
procenty	procent	k1gInPc1	procent
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
uzbecký	uzbecký	k2eAgInSc1d1	uzbecký
režim	režim	k1gInSc1	režim
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
demokratizačními	demokratizační	k2eAgInPc7d1	demokratizační
procesy	proces	k1gInPc7	proces
nastartovanými	nastartovaný	k2eAgInPc7d1	nastartovaný
samotným	samotný	k2eAgMnSc7d1	samotný
prezidentem	prezident	k1gMnSc7	prezident
Karimovem	Karimovo	k1gNnSc7	Karimovo
však	však	k8xC	však
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupným	postupný	k2eAgFnPc3d1	postupná
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
zakázal	zakázat	k5eAaPmAgInS	zakázat
práci	práce	k1gFnSc4	práce
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
tradičně	tradičně	k6eAd1	tradičně
každoročně	každoročně	k6eAd1	každoročně
využívány	využívat	k5eAaPmNgInP	využívat
při	při	k7c6	při
podzimním	podzimní	k2eAgInSc6d1	podzimní
sběru	sběr	k1gInSc6	sběr
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
reformy	reforma	k1gFnPc1	reforma
v	v	k7c6	v
justici	justice	k1gFnSc6	justice
a	a	k8xC	a
vězeňství	vězeňství	k1gNnSc6	vězeňství
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
mučení	mučení	k1gNnSc4	mučení
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc4	násilí
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
nelidské	lidský	k2eNgFnPc4d1	nelidská
<g/>
,	,	kIx,	,
kruté	krutý	k2eAgFnPc4d1	krutá
či	či	k8xC	či
ponižující	ponižující	k2eAgNnSc1d1	ponižující
zacházení	zacházení	k1gNnSc1	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Islama	Islam	k1gMnSc2	Islam
Karimova	Karimův	k2eAgMnSc2d1	Karimův
byl	být	k5eAaImAgInS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Šavkat	Šavkat	k1gMnSc1	Šavkat
Mirzijojev	Mirzijojev	k1gMnSc1	Mirzijojev
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
447	[number]	k4	447
400	[number]	k4	400
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
56	[number]	k4	56
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
5,7	[number]	k4	5,7
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Khazret	Khazreta	k1gFnPc2	Khazreta
Sultan	Sultan	k1gInSc4	Sultan
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
4	[number]	k4	4
643	[number]	k4	643
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Tádžikistánem	Tádžikistán	k1gInSc7	Tádžikistán
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
58	[number]	k4	58
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
zabírá	zabírat	k5eAaImIp3nS	zabírat
poušť	poušť	k1gFnSc1	poušť
Kyzylkum	Kyzylkum	k1gInSc1	Kyzylkum
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristikou	charakteristika	k1gFnSc7	charakteristika
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
rozložení	rozložení	k1gNnSc1	rozložení
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
chudá	chudý	k2eAgFnSc1d1	chudá
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
řeky	řeka	k1gFnPc1	řeka
vodu	voda	k1gFnSc4	voda
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
<g/>
,	,	kIx,	,
prosakováním	prosakování	k1gNnSc7	prosakování
a	a	k8xC	a
vypařováním	vypařování	k1gNnSc7	vypařování
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
vysychají	vysychat	k5eAaImIp3nP	vysychat
a	a	k8xC	a
často	často	k6eAd1	často
končí	končit	k5eAaImIp3nS	končit
slepým	slepý	k2eAgNnSc7d1	slepé
ústím	ústí	k1gNnSc7	ústí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
je	být	k5eAaImIp3nS	být
rozvětvená	rozvětvený	k2eAgFnSc1d1	rozvětvená
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
řeky	řeka	k1gFnPc1	řeka
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
povodí	povodí	k1gNnSc3	povodí
Amudarji	Amudarj	k1gInSc3	Amudarj
a	a	k8xC	a
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
sněhovo-ledovcový	sněhovoedovcový	k2eAgInSc4d1	sněhovo-ledovcový
zdroj	zdroj	k1gInSc4	zdroj
s	s	k7c7	s
maximálními	maximální	k2eAgInPc7d1	maximální
stavy	stav	k1gInPc7	stav
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rovin	rovina	k1gFnPc2	rovina
povodí	povodí	k1gNnSc2	povodí
Amudarji	Amudarje	k1gFnSc4	Amudarje
stéká	stékat	k5eAaImIp3nS	stékat
ročně	ročně	k6eAd1	ročně
79	[number]	k4	79
km3	km3	k4	km3
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
8	[number]	k4	8
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c6	na
horské	horský	k2eAgFnSc6d1	horská
oblasti	oblast	k1gFnSc6	oblast
<g/>
;	;	kIx,	;
do	do	k7c2	do
rovin	rovina	k1gFnPc2	rovina
povodí	povodí	k1gNnSc6	povodí
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
stéká	stékat	k5eAaImIp3nS	stékat
ročně	ročně	k6eAd1	ročně
38	[number]	k4	38
km3	km3	k4	km3
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
10	[number]	k4	10
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c6	na
horské	horský	k2eAgFnSc6d1	horská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
přítoků	přítok	k1gInPc2	přítok
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
odvedena	odveden	k2eAgFnSc1d1	odvedena
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
jí	on	k3xPp3gFnSc3	on
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
dolinách	dolina	k1gFnPc6	dolina
a	a	k8xC	a
deltách	delta	k1gFnPc6	delta
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
zavlažovaných	zavlažovaný	k2eAgFnPc2d1	zavlažovaná
oáz	oáza	k1gFnPc2	oáza
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
jezera	jezero	k1gNnSc2	jezero
Aralské	aralský	k2eAgNnSc1d1	Aralské
a	a	k8xC	a
Sarykamyšské	Sarykamyšský	k2eAgNnSc1d1	Sarykamyšský
a	a	k8xC	a
vnitrozemská	vnitrozemský	k2eAgFnSc1d1	vnitrozemská
Ajdarkul	Ajdarkul	k1gInSc1	Ajdarkul
a	a	k8xC	a
Dengizkul	Dengizkul	k1gInSc1	Dengizkul
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
770	[number]	k4	770
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
500	[number]	k4	500
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Amudarji	Amudarj	k1gInSc6	Amudarj
a	a	k8xC	a
270	[number]	k4	270
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
95	[number]	k4	95
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
1	[number]	k4	1
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
umělých	umělý	k2eAgFnPc2d1	umělá
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
:	:	kIx,	:
Kattakurganská	Kattakurganský	k2eAgNnPc1d1	Kattakurganský
<g/>
,	,	kIx,	,
Kajrakkumská	Kajrakkumský	k2eAgNnPc1d1	Kajrakkumský
<g/>
,	,	kIx,	,
Čardarinská	Čardarinský	k2eAgNnPc1d1	Čardarinský
<g/>
,	,	kIx,	,
Kyjumazarská	Kyjumazarský	k2eAgNnPc1d1	Kyjumazarský
<g/>
,	,	kIx,	,
Kasansajská	Kasansajský	k2eAgNnPc1d1	Kasansajský
<g/>
,	,	kIx,	,
Tjujabuguzská	Tjujabuguzský	k2eAgNnPc1d1	Tjujabuguzský
a	a	k8xC	a
Čarvakská	Čarvakský	k2eAgNnPc1d1	Čarvakský
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
proklamativně	proklamativně	k6eAd1	proklamativně
demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
ovládán	ovládat	k5eAaImNgMnS	ovládat
režimem	režim	k1gInSc7	režim
autoritativního	autoritativní	k2eAgMnSc2d1	autoritativní
prezidenta	prezident	k1gMnSc2	prezident
Islama	Islam	k1gMnSc2	Islam
Karimova	Karimův	k2eAgMnSc2d1	Karimův
<g/>
.	.	kIx.	.
</s>
<s>
Islam	Islam	k1gInSc1	Islam
Karimov	Karimov	k1gInSc1	Karimov
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
i	i	k9	i
po	po	k7c4	po
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
v	v	k7c6	v
pochybných	pochybný	k2eAgFnPc6d1	pochybná
volbách	volba	k1gFnPc6	volba
zůstala	zůstat	k5eAaPmAgFnS	zůstat
moc	moc	k6eAd1	moc
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Karimov	Karimov	k1gInSc4	Karimov
uplatňoval	uplatňovat	k5eAaImAgMnS	uplatňovat
vůči	vůči	k7c3	vůči
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
zostřenou	zostřený	k2eAgFnSc4d1	zostřená
represivní	represivní	k2eAgFnSc4d1	represivní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nominálně	nominálně	k6eAd1	nominálně
odůvodněna	odůvodnit	k5eAaPmNgFnS	odůvodnit
nutnou	nutný	k2eAgFnSc7d1	nutná
obranou	obrana	k1gFnSc7	obrana
před	před	k7c7	před
islámským	islámský	k2eAgInSc7d1	islámský
extremismem	extremismus	k1gInSc7	extremismus
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
islamismu	islamismus	k1gInSc2	islamismus
potlačoval	potlačovat	k5eAaImAgMnS	potlačovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Uzbecký	uzbecký	k2eAgMnSc1d1	uzbecký
prezident	prezident	k1gMnSc1	prezident
Islam	Islam	k1gInSc4	Islam
Karimov	Karimov	k1gInSc1	Karimov
zemřel	zemřít	k5eAaPmAgInS	zemřít
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
před	před	k7c7	před
oslavami	oslava	k1gFnPc7	oslava
25	[number]	k4	25
let	léto	k1gNnPc2	léto
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiální	oficiální	k2eAgNnSc1d1	oficiální
datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
je	být	k5eAaImIp3nS	být
posunut	posunout	k5eAaPmNgInS	posunout
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Šavkat	Šavkat	k1gMnSc1	Šavkat
Mirzijojev	Mirzijojev	k1gMnSc1	Mirzijojev
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
největší	veliký	k2eAgMnSc1d3	veliký
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgMnSc1d3	veliký
vývozce	vývozce	k1gMnSc1	vývozce
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
těžební	těžební	k2eAgInSc1d1	těžební
a	a	k8xC	a
zpracovatelský	zpracovatelský	k2eAgInSc1d1	zpracovatelský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
sedmý	sedmý	k4xOgMnSc1	sedmý
největší	veliký	k2eAgMnSc1d3	veliký
producent	producent	k1gMnSc1	producent
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
významný	významný	k2eAgMnSc1d1	významný
producent	producent	k1gMnSc1	producent
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
věnují	věnovat	k5eAaPmIp3nP	věnovat
se	se	k3xPyFc4	se
zemědělství	zemědělství	k1gNnSc4	zemědělství
v	v	k7c6	v
intenzivně	intenzivně	k6eAd1	intenzivně
využívaných	využívaný	k2eAgNnPc6d1	využívané
údolích	údolí	k1gNnPc6	údolí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgInPc2d1	tvořící
11	[number]	k4	11
<g/>
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
vláda	vláda	k1gFnSc1	vláda
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
centrálně	centrálně	k6eAd1	centrálně
řízené	řízený	k2eAgFnSc6d1	řízená
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
výrazně	výrazně	k6eAd1	výrazně
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
krize	krize	k1gFnPc1	krize
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asijských	asijský	k2eAgFnPc2d1	asijská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vláda	vláda	k1gFnSc1	vláda
mírně	mírně	k6eAd1	mírně
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
liberalizovat	liberalizovat	k5eAaImF	liberalizovat
trh	trh	k1gInSc4	trh
a	a	k8xC	a
přilákat	přilákat	k5eAaPmF	přilákat
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
investory	investor	k1gMnPc4	investor
<g/>
.	.	kIx.	.
</s>
<s>
Chudoba	Chudoba	k1gMnSc1	Chudoba
jakož	jakož	k8xC	jakož
i	i	k8xC	i
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
30	[number]	k4	30
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
po	po	k7c6	po
Ruské	ruský	k2eAgFnSc6d1	ruská
SFSR	SFSR	kA	SFSR
a	a	k8xC	a
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
byl	být	k5eAaImAgInS	být
třetí	třetí	k4xOgFnSc7	třetí
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
34,1	[number]	k4	34,1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Uzbeci	Uzbek	k1gMnPc1	Uzbek
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
80	[number]	k4	80
%	%	kIx~	%
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
mírně	mírně	k6eAd1	mírně
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tádžikové	Tádžik	k1gMnPc1	Tádžik
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazaši	Kazach	k1gMnPc1	Kazach
(	(	kIx(	(
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karakalpakové	Karakalpak	k1gMnPc1	Karakalpak
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tataři	Tatar	k1gMnPc1	Tatar
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
tádžické	tádžický	k2eAgFnSc2d1	tádžická
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
oficiální	oficiální	k2eAgInPc1d1	oficiální
údaje	údaj	k1gInPc1	údaj
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
uvádí	uvádět	k5eAaImIp3nP	uvádět
5,0	[number]	k4	5,0
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
údaje	údaj	k1gInPc1	údaj
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
uvádějí	uvádět	k5eAaImIp3nP	uvádět
20-30	[number]	k4	20-30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
hlavně	hlavně	k9	hlavně
islám	islám	k1gInSc4	islám
(	(	kIx(	(
<g/>
88	[number]	k4	88
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
sunnitský	sunnitský	k2eAgInSc1d1	sunnitský
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itský	itský	k2eAgInSc1d1	itský
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
5,0	[number]	k4	5,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
náboženství	náboženství	k1gNnPc1	náboženství
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
pravoslaví	pravoslaví	k1gNnSc4	pravoslaví
(	(	kIx(	(
<g/>
9,0	[number]	k4	9,0
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
náboženství	náboženství	k1gNnPc1	náboženství
(	(	kIx(	(
<g/>
3,0	[number]	k4	3,0
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
používá	používat	k5eAaImIp3nS	používat
uzbečtina	uzbečtina	k1gFnSc1	uzbečtina
(	(	kIx(	(
<g/>
74,3	[number]	k4	74,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ruština	ruština	k1gFnSc1	ruština
(	(	kIx(	(
<g/>
14,2	[number]	k4	14,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tádžičtina	tádžičtina	k1gFnSc1	tádžičtina
(	(	kIx(	(
<g/>
4,4	[number]	k4	4,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
7,1	[number]	k4	7,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ROUX	ROUX	kA	ROUX
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
867	[number]	k4	867
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc1	kategorie
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
uzbecky	uzbecky	k6eAd1	uzbecky
<g/>
)	)	kIx)	)
Facebook	Facebook	k1gInSc1	Facebook
–	–	k?	–
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
Braňte	bránit	k5eAaImRp2nP	bránit
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
ne	ne	k9	ne
diktátory	diktátor	k1gMnPc4	diktátor
<g/>
:	:	kIx,	:
Češi	Čech	k1gMnPc1	Čech
zatkli	zatknout	k5eAaPmAgMnP	zatknout
uzbeckého	uzbecký	k2eAgMnSc2d1	uzbecký
"	"	kIx"	"
<g/>
demokrata	demokrat	k1gMnSc2	demokrat
<g/>
"	"	kIx"	"
lang_en	lang_en	k1gInSc4	lang_en
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
zadržení	zadržení	k1gNnSc6	zadržení
Uzbeckého	uzbecký	k2eAgMnSc2d1	uzbecký
disidenta	disident	k1gMnSc2	disident
českou	český	k2eAgFnSc7d1	Česká
policií	policie	k1gFnSc7	policie
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
tisku	tisk	k1gInSc6	tisk
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Stav	stav	k1gInSc1	stav
jezer	jezero	k1gNnPc2	jezero
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
У	У	k?	У
с	с	k?	с
с	с	k?	с
р	р	k?	р
(	(	kIx(	(
<g/>
В	В	k?	В
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistan	Uzbekistan	k1gInSc1	Uzbekistan
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Uzbekistan	Uzbekistan	k1gInSc1	Uzbekistan
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Uzbekistan	Uzbekistan	k1gInSc1	Uzbekistan
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
South	Southa	k1gFnPc2	Southa
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Uzbekistan	Uzbekistan	k1gInSc1	Uzbekistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Uzbekistan	Uzbekistan	k1gInSc1	Uzbekistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Uzbekistan	Uzbekistan	k1gInSc4	Uzbekistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v.	v.	k?	v.
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
ALLWORTH	ALLWORTH	kA	ALLWORTH
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistan	Uzbekistan	k1gInSc1	Uzbekistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
