<s desamb="1">
Agropoli	Agropoli	k1gNs1c1
je	být	k5eAaImIp3nS
vzdálená	vzdálený	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
100	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Neapole	Neapol	k1gFnSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Kampánie	Kampánie	k1gFnSc2
<g/>
.	.	kIx.
10	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Agropoli	Agropoli	k1gNs1c2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
velmi	velmi	k6eAd1
významná	významný	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
,	,	kIx,
původní	původní	k2eAgFnSc1d1
antické	antický	k2eAgNnSc4d1
město	město	k1gNnSc4
Paestum	Paestum	k1gNnSc1
<g/>
.	.	kIx.
</s>