<s>
Agropoli	Agropoli	k6eAd1
</s>
<s>
Agropoli	Agropoli	k6eAd1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
40	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
30	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Kampánie	Kampánie	k1gFnSc1
provincie	provincie	k1gFnSc2
</s>
<s>
Salerno	Salerna	k1gFnSc5
</s>
<s>
Agropoli	Agropoli	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
39	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
20	#num#	k4
610	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
528,5	528,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.comune.agropoli.sa.it	www.comune.agropoli.sa.it	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Agropoli	Agropoli	k6eAd1
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
italské	italský	k2eAgFnSc6d1
Kampánii	Kampánie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
provincii	provincie	k1gFnSc6
Salerno	Salerna	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
Salernském	Salernský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Tyrhénského	tyrhénský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
počátku	počátek	k1gInSc6
tzv.	tzv.	kA
Cilentského	Cilentský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pobřežní	pobřežní	k2eAgFnSc1d1
hornatá	hornatý	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
mezi	mezi	k7c7
řekami	řeka	k1gFnPc7
Calore	Calor	k1gInSc5
a	a	k8xC
Tanagro	tanagra	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agropoli	Agropoli	k1gNs1c1
je	být	k5eAaImIp3nS
vzdálená	vzdálený	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
100	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Neapole	Neapol	k1gFnSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Kampánie	Kampánie	k1gFnSc2
<g/>
.	.	kIx.
10	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Agropoli	Agropoli	k1gNs1c2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
velmi	velmi	k6eAd1
významná	významný	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
,	,	kIx,
původní	původní	k2eAgFnSc1d1
antické	antický	k2eAgNnSc4d1
město	město	k1gNnSc4
Paestum	Paestum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kampánie	Kampánie	k1gFnSc1
•	•	k?
Obce	obec	k1gFnSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Salerno	Salerna	k1gFnSc5
</s>
<s>
Acerno	Acerno	k1gNnSc1
•	•	k?
Agropoli	Agropole	k1gFnSc6
•	•	k?
Albanella	Albanella	k1gFnSc1
•	•	k?
Alfano	Alfana	k1gFnSc5
•	•	k?
Altavilla	Altavill	k1gMnSc2
Silentina	Silentin	k1gMnSc2
•	•	k?
Amalfi	Amalf	k1gFnSc2
•	•	k?
Angri	Angr	k1gFnSc2
•	•	k?
Aquara	Aquar	k1gMnSc2
•	•	k?
Ascea	Asceus	k1gMnSc2
•	•	k?
Atena	Aten	k1gMnSc2
Lucana	Lucan	k1gMnSc2
•	•	k?
Atrani	Atraň	k1gFnSc3
•	•	k?
Auletta	Auletta	k1gMnSc1
•	•	k?
Baronissi	Baronisse	k1gFnSc4
•	•	k?
Battipaglia	Battipaglia	k1gFnSc1
•	•	k?
Bellizzi	Bellizze	k1gFnSc3
•	•	k?
Bellosguardo	Bellosguardo	k1gNnSc4
•	•	k?
Bracigliano	Bracigliana	k1gFnSc5
•	•	k?
Buccino	Buccin	k2eAgNnSc1d1
•	•	k?
Buonabitacolo	Buonabitacola	k1gFnSc5
•	•	k?
Caggiano	Caggiana	k1gFnSc5
•	•	k?
Calvanico	Calvanico	k1gNnSc4
•	•	k?
Camerota	Camerot	k1gMnSc2
•	•	k?
Campagna	Campagn	k1gMnSc2
•	•	k?
Campora	Campor	k1gMnSc2
•	•	k?
Cannalonga	Cannalong	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Capaccio	Capaccio	k1gMnSc1
•	•	k?
Casal	Casal	k1gMnSc1
Velino	Velino	k1gNnSc1
•	•	k?
Casalbuono	Casalbuona	k1gFnSc5
•	•	k?
Casaletto	Casaletta	k1gFnSc5
Spartano	Spartana	k1gFnSc5
•	•	k?
Caselle	Caselle	k1gNnPc2
in	in	k?
Pittari	Pittar	k1gFnSc2
•	•	k?
Castel	Castel	k1gMnSc1
San	San	k1gMnSc1
Giorgio	Giorgio	k1gMnSc1
•	•	k?
Castel	Castel	k1gMnSc1
San	San	k1gMnSc1
Lorenzo	Lorenza	k1gFnSc5
•	•	k?
Castelcivita	Castelcivita	k1gFnSc1
•	•	k?
Castellabate	Castellabat	k1gInSc5
•	•	k?
Castelnuovo	Castelnuův	k2eAgNnSc1d1
Cilento	Cilento	k1gNnSc1
•	•	k?
Castelnuovo	Castelnuovo	k1gNnSc1
di	di	k?
Conza	Conz	k1gMnSc2
•	•	k?
Castiglione	Castiglion	k1gInSc5
del	del	k?
Genovesi	Genovese	k1gFnSc3
•	•	k?
Cava	Cav	k1gInSc2
de	de	k?
<g/>
'	'	kIx"
Tirreni	Tirren	k2eAgMnPc1d1
•	•	k?
Celle	Celle	k1gFnPc4
di	di	k?
Bulgheria	Bulgherium	k1gNnSc2
•	•	k?
Centola	Centola	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Ceraso	Cerasa	k1gFnSc5
•	•	k?
Cetara	Cetar	k1gMnSc2
•	•	k?
Cicerale	Ciceral	k1gMnSc5
•	•	k?
Colliano	Colliana	k1gFnSc5
•	•	k?
Conca	Conc	k2eAgMnSc4d1
dei	dei	k?
Marini	Marin	k2eAgMnPc1d1
•	•	k?
Controne	Contron	k1gInSc5
•	•	k?
Contursi	Conturse	k1gFnSc3
Terme	term	k1gInSc5
•	•	k?
Corbara	Corbara	k1gFnSc1
•	•	k?
Corleto	Corlet	k2eAgNnSc4d1
Monforte	Monfort	k1gInSc5
•	•	k?
Cuccaro	Cuccara	k1gFnSc5
Vetere	Veter	k1gInSc5
•	•	k?
Eboli	Ebole	k1gFnSc3
•	•	k?
Felitto	Felitto	k1gNnSc4
•	•	k?
Fisciano	Fisciana	k1gFnSc5
•	•	k?
Furore	furor	k1gInSc5
•	•	k?
Futani	Futaň	k1gFnSc3
•	•	k?
Giffoni	Giffoň	k1gFnSc3
Sei	Sei	k1gFnSc2
Casali	Casali	k1gFnSc3
•	•	k?
Giffoni	Giffoň	k1gFnSc3
Valle	Valle	k1gFnSc2
Piana	piano	k1gNnSc2
•	•	k?
Gioi	Gioi	k1gNnSc2
•	•	k?
Giungano	Giungana	k1gFnSc5
•	•	k?
Ispani	Ispaň	k1gFnSc6
•	•	k?
Laureana	Laureana	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Cilento	Cilento	k1gNnSc1
•	•	k?
Laurino	Laurino	k1gNnSc1
•	•	k?
Laurito	Laurita	k1gFnSc5
•	•	k?
Laviano	Laviana	k1gFnSc5
•	•	k?
Lustra	lustrum	k1gNnSc2
•	•	k?
Magliano	Magliana	k1gFnSc5
Vetere	Veter	k1gInSc5
•	•	k?
Maiori	Maior	k1gFnSc2
•	•	k?
Mercato	Mercat	k2eAgNnSc1d1
San	San	k1gMnSc1
Severino	Severin	k2eAgNnSc1d1
•	•	k?
Minori	Minori	k1gNnSc1
•	•	k?
Moio	Moio	k1gMnSc1
della	della	k1gMnSc1
Civitella	Civitella	k1gMnSc1
•	•	k?
Montano	Montana	k1gFnSc5
Antilia	Antilium	k1gNnPc1
•	•	k?
Monte	Mont	k1gMnSc5
San	San	k1gMnSc5
Giacomo	Giacoma	k1gFnSc5
•	•	k?
Montecorice	Montecorika	k1gFnSc6
•	•	k?
Montecorvino	Montecorvina	k1gFnSc5
Pugliano	Pugliana	k1gFnSc5
•	•	k?
Montecorvino	Montecorvina	k1gFnSc5
Rovella	Rovell	k1gMnSc2
•	•	k?
Monteforte	Montefort	k1gInSc5
Cilento	Cilento	k1gNnSc1
•	•	k?
Montesano	Montesana	k1gFnSc5
sulla	sulla	k6eAd1
Marcellana	Marcellan	k1gMnSc2
•	•	k?
Morigerati	Morigerat	k1gMnPc1
•	•	k?
Nocera	Nocero	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Inferiore	Inferior	k1gInSc5
•	•	k?
Nocera	Nocer	k1gMnSc2
Superiore	superior	k1gMnSc5
•	•	k?
Novi	Novi	k1gNnSc2
Velia	Velius	k1gMnSc2
•	•	k?
Ogliastro	Ogliastro	k1gNnSc1
Cilento	Cilento	k1gNnSc1
•	•	k?
Olevano	Olevana	k1gFnSc5
sul	sout	k5eAaImAgInS
Tusciano	Tusciana	k1gFnSc5
•	•	k?
Oliveto	oliveta	k1gFnSc5
Citra	Citr	k1gMnSc2
•	•	k?
Omignano	Omignana	k1gFnSc5
•	•	k?
Orria	Orrius	k1gMnSc2
•	•	k?
Ottati	Ottat	k1gMnPc1
•	•	k?
Padula	Padula	k1gFnSc1
•	•	k?
Pagani	Pagaň	k1gFnSc6
•	•	k?
Palomonte	Palomont	k1gInSc5
•	•	k?
Pellezzano	Pellezzana	k1gFnSc5
•	•	k?
Perdifumo	Perdifuma	k1gFnSc5
•	•	k?
Perito	Perit	k2eAgNnSc1d1
•	•	k?
Pertosa	Pertosa	k1gFnSc1
•	•	k?
Petina	Petina	k1gFnSc1
•	•	k?
Piaggine	Piaggin	k1gInSc5
•	•	k?
Pisciotta	Pisciotta	k1gMnSc1
•	•	k?
Polla	Polla	k1gMnSc1
•	•	k?
Pollica	Pollica	k1gMnSc1
•	•	k?
Pontecagnano	Pontecagnana	k1gFnSc5
Faiano	Faiana	k1gFnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Positano	Positana	k1gFnSc5
•	•	k?
Postiglione	Postiglion	k1gInSc5
•	•	k?
Praiano	Praiana	k1gFnSc5
•	•	k?
Prignano	Prignana	k1gFnSc5
Cilento	Cilento	k1gNnSc1
•	•	k?
Ravello	Ravello	k1gNnSc1
•	•	k?
Ricigliano	Ricigliana	k1gFnSc5
•	•	k?
Roccadaspide	Roccadaspid	k1gInSc5
•	•	k?
Roccagloriosa	Roccagloriosa	k1gFnSc1
•	•	k?
Roccapiemonte	Roccapiemont	k1gInSc5
•	•	k?
Rofrano	Rofrana	k1gFnSc5
•	•	k?
Romagnano	Romagnana	k1gFnSc5
al	ala	k1gFnPc2
Monte	Mont	k1gInSc5
•	•	k?
Roscigno	Roscigen	k2eAgNnSc1d1
•	•	k?
Rutino	rutina	k1gFnSc5
•	•	k?
Sacco	Sacco	k6eAd1
•	•	k?
Sala	Sal	k2eAgFnSc1d1
Consilina	Consilina	k1gFnSc1
•	•	k?
Salento	Salento	k1gNnSc1
•	•	k?
Salerno	Salerna	k1gFnSc5
•	•	k?
Salvitelle	Salvitell	k1gMnSc2
•	•	k?
San	San	k1gMnSc2
Cipriano	Cipriana	k1gFnSc5
Picentino	Picentin	k2eAgNnSc4d1
•	•	k?
San	San	k1gFnSc7
Giovanni	Giovanen	k2eAgMnPc1d1
a	a	k8xC
Piro	Piro	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
San	San	k1gMnSc1
Gregorio	Gregorio	k1gMnSc1
Magno	Magno	k6eAd1
•	•	k?
San	San	k1gMnSc1
Mango	mango	k1gNnSc1
Piemonte	Piemont	k1gInSc5
•	•	k?
San	San	k1gMnSc1
Marzano	Marzana	k1gFnSc5
sul	sout	k5eAaImAgMnS
Sarno	Sarno	k1gNnSc4
•	•	k?
San	San	k1gMnSc1
Mauro	Mauro	k1gNnSc1
Cilento	Cilento	k1gNnSc4
•	•	k?
San	San	k1gFnPc2
Mauro	Mauro	k1gNnSc4
la	la	k1gNnSc2
Bruca	Brucus	k1gMnSc2
•	•	k?
San	San	k1gFnSc4
Pietro	Pietro	k1gNnSc4
al	ala	k1gFnPc2
Tanagro	tanagra	k1gFnSc5
•	•	k?
San	San	k1gMnSc1
Rufo	Rufo	k1gMnSc1
•	•	k?
San	San	k1gMnSc1
Valentino	Valentina	k1gFnSc5
Torio	Torio	k1gMnSc1
•	•	k?
Sant	Sant	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Angelo	Angela	k1gFnSc5
a	a	k8xC
Fasanella	Fasanella	k1gMnSc1
•	•	k?
Sant	Sant	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Arsenio	Arsenio	k1gMnSc1
•	•	k?
Sant	Sant	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
'	'	kIx"
<g/>
Egidio	Egidio	k6eAd1
del	del	k?
Monte	Mont	k1gMnSc5
Albino	Albina	k1gMnSc5
•	•	k?
Santa	Santa	k1gFnSc1
Marina	Marina	k1gFnSc1
•	•	k?
Santomenna	Santomenn	k1gMnSc2
•	•	k?
Sanza	Sanz	k1gMnSc2
•	•	k?
Sapri	Sapr	k1gFnSc2
•	•	k?
Sarno	Sarno	k6eAd1
•	•	k?
Sassano	Sassana	k1gFnSc5
•	•	k?
Scafati	Scafati	k1gFnSc1
•	•	k?
Scala	scát	k5eAaImAgFnS
•	•	k?
Serramezzana	Serramezzana	k1gFnSc1
•	•	k?
Serre	Serr	k1gInSc5
•	•	k?
Sessa	Sessa	k1gFnSc1
Cilento	Cilento	k1gNnSc1
•	•	k?
Siano	Siano	k6eAd1
•	•	k?
Sicignano	Sicignana	k1gFnSc5
degli	degl	k1gMnSc6
Alburni	Alburni	k1gFnSc7
•	•	k?
Stella	Stella	k1gFnSc1
Cilento	Cilento	k1gNnSc1
•	•	k?
Stio	Stio	k1gMnSc1
•	•	k?
Teggiano	Teggiana	k1gFnSc5
•	•	k?
Torchiara	Torchiar	k1gMnSc2
•	•	k?
Torraca	Torracus	k1gMnSc2
•	•	k?
Torre	torr	k1gInSc5
Orsaia	Orsaium	k1gNnPc1
•	•	k?
Tortorella	Tortorell	k1gMnSc2
•	•	k?
Tramonti	Tramonť	k1gFnSc2
•	•	k?
Trentinara	Trentinar	k1gMnSc2
•	•	k?
Valle	Vall	k1gMnSc2
dell	delnout	k5eAaPmAgMnS
<g/>
'	'	kIx"
<g/>
Angelo	Angela	k1gFnSc5
•	•	k?
Vallo	Vallo	k1gNnSc1
della	dell	k1gMnSc2
Lucania	Lucanium	k1gNnSc2
•	•	k?
Valva	Valv	k1gMnSc2
•	•	k?
Vibonati	Vibonati	k1gMnSc2
•	•	k?
Vietri	Vietr	k1gFnSc2
sul	sout	k5eAaImAgInS
Mare	Mare	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
242297198	#num#	k4
</s>
