<s>
Město	město	k1gNnSc1	město
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
(	(	kIx(	(
<g/>
též	též	k9	též
Kuwait	Kuwait	k1gInSc1	Kuwait
City	city	k1gNnSc1	city
nebo	nebo	k8xC	nebo
Al-Kuwait	Al-Kuwait	k1gInSc1	Al-Kuwait
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgInSc2d1	ležící
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
