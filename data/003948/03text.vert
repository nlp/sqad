<s>
Město	město	k1gNnSc1	město
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
(	(	kIx(	(
<g/>
též	též	k9	též
Kuwait	Kuwait	k1gInSc1	Kuwait
City	city	k1gNnSc1	city
nebo	nebo	k8xC	nebo
Al-Kuwait	Al-Kuwait	k1gInSc1	Al-Kuwait
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgInSc2d1	ležící
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
534.964	[number]	k4	534.964
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
uvnitř	uvnitř	k7c2	uvnitř
hranic	hranice	k1gFnPc2	hranice
města	město	k1gNnSc2	město
a	a	k8xC	a
2,38	[number]	k4	2,38
milionu	milion	k4xCgInSc2	milion
v	v	k7c4	v
metropolitní	metropolitní	k2eAgFnPc4d1	metropolitní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kuvajtský	kuvajtský	k2eAgInSc1d1	kuvajtský
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Majlis	Majlis	k1gFnSc1	Majlis
al-Umma	al-Umma	k1gFnSc1	al-Umma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
většiny	většina	k1gFnSc2	většina
kuvajtských	kuvajtský	k2eAgFnPc2d1	kuvajtská
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
politickým	politický	k2eAgNnSc7d1	politické
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgNnSc7d1	kulturní
a	a	k8xC	a
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
centrum	centrum	k1gNnSc1	centrum
emirátu	emirát	k1gInSc2	emirát
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
osídleno	osídlit	k5eAaPmNgNnS	osídlit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
klanem	klan	k1gInSc7	klan
Al-Sabah	Al-Sabaha	k1gFnPc2	Al-Sabaha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vládnoucí	vládnoucí	k2eAgInSc1d1	vládnoucí
rod	rod	k1gInSc1	rod
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
,	,	kIx,	,
větev	větev	k1gFnSc1	větev
kmene	kmen	k1gInSc2	kmen
Al-Utū	Al-Utū	k1gMnSc2	Al-Utū
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
Al-Chálífa	Al-Chálíf	k1gMnSc2	Al-Chálíf
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
Bahrajnu	Bahrajn	k1gInSc6	Bahrajn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
odvozené	odvozený	k2eAgNnSc4d1	odvozené
z	z	k7c2	z
opuštěné	opuštěný	k2eAgFnSc2d1	opuštěná
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
Kut	kut	k2eAgMnSc1d1	kut
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ك	ك	k?	ك
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
arabský	arabský	k2eAgInSc1d1	arabský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
pevnost	pevnost	k1gFnSc4	pevnost
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
první	první	k4xOgInSc1	první
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
osada	osada	k1gFnSc1	osada
rychle	rychle	k6eAd1	rychle
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
a	a	k8xC	a
časem	časem	k6eAd1	časem
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
flotilu	flotila	k1gFnSc4	flotila
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
plachetnic	plachetnice	k1gFnPc2	plachetnice
"	"	kIx"	"
<g/>
Dhow	Dhow	k1gFnSc1	Dhow
<g/>
"	"	kIx"	"
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
mnoho	mnoho	k4c1	mnoho
obchodních	obchodní	k2eAgInPc2d1	obchodní
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Bagdádem	Bagdád	k1gInSc7	Bagdád
a	a	k8xC	a
Damaškem	Damašek	k1gInSc7	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
prosperující	prosperující	k2eAgInSc1d1	prosperující
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Prosperita	prosperita	k1gFnSc1	prosperita
a	a	k8xC	a
konkurenceschopnost	konkurenceschopnost	k1gFnSc1	konkurenceschopnost
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
mnoha	mnoho	k4c3	mnoho
sporům	spor	k1gInPc3	spor
a	a	k8xC	a
válkám	válka	k1gFnPc3	válka
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgMnPc3	který
byl	být	k5eAaImAgInS	být
emirát	emirát	k1gInSc4	emirát
nucen	nucen	k2eAgInSc4d1	nucen
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
britů	brita	k1gMnPc2	brita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
zajistili	zajistit	k5eAaPmAgMnP	zajistit
námořní	námořní	k2eAgFnSc4d1	námořní
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
objevu	objev	k1gInSc3	objev
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
a	a	k8xC	a
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
se	se	k3xPyFc4	se
irácké	irácký	k2eAgFnPc1d1	irácká
síly	síla	k1gFnPc1	síla
zmocnily	zmocnit	k5eAaPmAgFnP	zmocnit
města	město	k1gNnPc1	město
a	a	k8xC	a
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
srpna	srpen	k1gInSc2	srpen
jej	on	k3xPp3gNnSc2	on
připojé	připojé	k1gNnSc2	připojé
emirátu	emirát	k1gInSc2	emirát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
okupace	okupace	k1gFnSc2	okupace
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
značně	značně	k6eAd1	značně
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
budov	budova	k1gFnPc2	budova
bylo	být	k5eAaImAgNnS	být
nenávratně	návratně	k6eNd1	návratně
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
včetně	včetně	k7c2	včetně
kuvajtského	kuvajtský	k2eAgNnSc2d1	Kuvajtské
národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
iráckých	irácký	k2eAgNnPc2d1	irácké
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
investoři	investor	k1gMnPc1	investor
a	a	k8xC	a
kuvajtská	kuvajtský	k2eAgFnSc1d1	kuvajtská
vláda	vláda	k1gFnSc1	vláda
aktivně	aktivně	k6eAd1	aktivně
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
města	město	k1gNnSc2	město
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
konkurenceschopné	konkurenceschopný	k2eAgNnSc1d1	konkurenceschopné
centrum	centrum	k1gNnSc1	centrum
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
začal	začít	k5eAaPmAgInS	začít
emirát	emirát	k1gInSc1	emirát
hospodářsky	hospodářsky	k6eAd1	hospodářsky
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
mnoho	mnoho	k4c1	mnoho
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
nákupních	nákupní	k2eAgNnPc2d1	nákupní
středisek	středisko	k1gNnPc2	středisko
<g/>
,	,	kIx,	,
úřadů	úřad	k1gInPc2	úřad
ap.	ap.	kA	ap.
Kuvajtská	kuvajtský	k2eAgFnSc1d1	kuvajtská
vzkvétající	vzkvétající	k2eAgFnSc1d1	vzkvétající
ekonomika	ekonomika	k1gFnSc1	ekonomika
dovolila	dovolit	k5eAaPmAgFnS	dovolit
mnoha	mnoho	k4c2	mnoho
mezinárodním	mezinárodní	k2eAgMnPc3d1	mezinárodní
hotelovým	hotelový	k2eAgMnPc3d1	hotelový
řetězcům	řetězec	k1gInPc3	řetězec
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
"	"	kIx"	"
<g/>
Kuwait	Kuwait	k1gInSc1	Kuwait
Hotel	hotel	k1gInSc1	hotel
Owners	Owners	k1gInSc1	Owners
Association	Association	k1gInSc1	Association
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
plánováno	plánovat	k5eAaImNgNnS	plánovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
nových	nový	k2eAgInPc2d1	nový
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
následujících	následující	k2eAgFnPc2d1	následující
<g/>
:	:	kIx,	:
Golden	Goldna	k1gFnPc2	Goldna
Tulip	Tulip	k1gInSc1	Tulip
Kuwait	Kuwait	k1gMnSc1	Kuwait
-	-	kIx~	-
otevření	otevření	k1gNnSc1	otevření
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Hilton	Hilton	k1gInSc1	Hilton
Olympia	Olympia	k1gFnSc1	Olympia
Kuwait	Kuwait	k1gMnSc1	Kuwait
-	-	kIx~	-
opening	opening	k1gInSc1	opening
late	lat	k1gInSc5	lat
2009	[number]	k4	2009
Hilton	Hilton	k1gInSc4	Hilton
Olympia	Olympia	k1gFnSc1	Olympia
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
-	-	kIx~	-
otevření	otevření	k1gNnSc1	otevření
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Jumeirah	Jumeirah	k1gMnSc1	Jumeirah
Messilah	Messilah	k1gMnSc1	Messilah
Beach	Beach	k1gMnSc1	Beach
Kuwait	Kuwait	k1gMnSc1	Kuwait
-	-	kIx~	-
otevření	otevření	k1gNnSc1	otevření
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Novotel	Novotela	k1gFnPc2	Novotela
Mina	mina	k1gFnSc1	mina
Abdullah	Abdullah	k1gInSc1	Abdullah
Resort	resort	k1gInSc1	resort
-	-	kIx~	-
otevření	otevření	k1gNnSc1	otevření
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Four	Foura	k1gFnPc2	Foura
Seasons	Seasonsa	k1gFnPc2	Seasonsa
hotel	hotel	k1gInSc1	hotel
Podnebí	podnebí	k1gNnSc2	podnebí
emirátu	emirát	k1gInSc2	emirát
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velmi	velmi	k6eAd1	velmi
horkým	horký	k2eAgNnSc7d1	horké
létem	léto	k1gNnSc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
okolo	okolo	k7c2	okolo
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
občas	občas	k6eAd1	občas
prší	pršet	k5eAaImIp3nS	pršet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
je	být	k5eAaImIp3nS	být
déšť	déšť	k1gInSc1	déšť
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Nejdeštivější	deštivý	k2eAgInSc1d3	nejdeštivější
měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc4	leden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
navíc	navíc	k6eAd1	navíc
dochází	docházet	k5eAaImIp3nS	docházet
díky	díky	k7c3	díky
větru	vítr	k1gInSc3	vítr
k	k	k7c3	k
silným	silný	k2eAgFnPc3d1	silná
prachovým	prachový	k2eAgFnPc3d1	prachová
bouřím	bouř	k1gFnPc3	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
kdykoliv	kdykoliv	k6eAd1	kdykoliv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgMnPc1d3	nejčastější
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Priština	Priština	k1gFnSc1	Priština
<g/>
,	,	kIx,	,
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
-	-	kIx~	-
stát	stát	k5eAaPmF	stát
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
Madinat	Madinat	k1gMnSc1	Madinat
al-Hareer	al-Hareer	k1gMnSc1	al-Hareer
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
