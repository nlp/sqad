<s>
World	Worlda	k1gFnPc2
Geodetic	Geodetice	k1gFnPc2
System	Syst	k1gInSc7
1984	#num#	k4
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
WGS	WGS	kA
<g/>
84	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
Světový	světový	k2eAgInSc1d1
geodetický	geodetický	k2eAgInSc1d1
systém	systém	k1gInSc1
1984	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
světově	světově	k6eAd1
uznávaný	uznávaný	k2eAgInSc1d1
geodetický	geodetický	k2eAgInSc1d1
standard	standard	k1gInSc1
vydaný	vydaný	k2eAgInSc1d1
ministerstvem	ministerstvo	k1gNnSc7
obrany	obrana	k1gFnSc2
USA	USA	kA
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
definuje	definovat	k5eAaBmIp3nS
souřadnicový	souřadnicový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
referenční	referenční	k2eAgInSc1d1
elipsoid	elipsoid	k1gInSc1
pro	pro	k7c4
geodézii	geodézie	k1gFnSc4
a	a	k8xC
navigaci	navigace	k1gFnSc4
<g/>
.	.	kIx.
</s>