<s>
World	Worlda	k1gFnPc2
Geodetic	Geodetice	k1gFnPc2
System	Syst	k1gInSc7
</s>
<s>
Referenční	referenční	k2eAgInSc1d1
rámec	rámec	k1gInSc1
WGS	WGS	kA
84	#num#	k4
</s>
<s>
World	Worlda	k1gFnPc2
Geodetic	Geodetice	k1gFnPc2
System	Syst	k1gInSc7
1984	#num#	k4
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
WGS	WGS	kA
<g/>
84	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
Světový	světový	k2eAgInSc1d1
geodetický	geodetický	k2eAgInSc1d1
systém	systém	k1gInSc1
1984	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
světově	světově	k6eAd1
uznávaný	uznávaný	k2eAgInSc1d1
geodetický	geodetický	k2eAgInSc1d1
standard	standard	k1gInSc1
vydaný	vydaný	k2eAgInSc1d1
ministerstvem	ministerstvo	k1gNnSc7
obrany	obrana	k1gFnSc2
USA	USA	kA
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
definuje	definovat	k5eAaBmIp3nS
souřadnicový	souřadnicový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
referenční	referenční	k2eAgInSc1d1
elipsoid	elipsoid	k1gInSc1
pro	pro	k7c4
geodézii	geodézie	k1gFnSc4
a	a	k8xC
navigaci	navigace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odchylky	odchylka	k1gFnSc2
od	od	k7c2
referenčního	referenční	k2eAgInSc2d1
elipsoidu	elipsoid	k1gInSc2
pak	pak	k6eAd1
popisují	popisovat	k5eAaImIp3nP
geoid	geoid	k1gInSc4
EGM	EGM	kA
<g/>
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byl	být	k5eAaImAgInS
rozšířen	rozšířit	k5eAaPmNgInS
o	o	k7c4
zpřesněnou	zpřesněný	k2eAgFnSc4d1
definici	definice	k1gFnSc4
geoidu	geoid	k1gInSc2
EGM	EGM	kA
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
měření	měření	k1gNnSc2
pozemních	pozemní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
družicového	družicový	k2eAgInSc2d1
polohového	polohový	k2eAgInSc2d1
systému	systém	k1gInSc2
TRANSIT	transit	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nahrazuje	nahrazovat	k5eAaImIp3nS
dřívější	dřívější	k2eAgInPc4d1
systémy	systém	k1gInPc4
WGS	WGS	kA
60	#num#	k4
<g/>
,	,	kIx,
WGS	WGS	kA
66	#num#	k4
a	a	k8xC
WGS	WGS	kA
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Souřadnice	souřadnice	k1gFnPc1
WGS84	WGS84	k1gMnPc2
vycházejí	vycházet	k5eAaImIp3nP
ze	z	k7c2
souřadnic	souřadnice	k1gFnPc2
zeměpisných	zeměpisný	k2eAgFnPc2d1
<g/>
,	,	kIx,
polohu	poloha	k1gFnSc4
tedy	tedy	k8xC
určíme	určit	k5eAaPmIp1nP
pomocí	pomocí	k7c2
zeměpisné	zeměpisný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
,	,	kIx,
šířky	šířka	k1gFnSc2
a	a	k8xC
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šířka	šířka	k1gFnSc1
nabývá	nabývat	k5eAaImIp3nS
0	#num#	k4
<g/>
°	°	k?
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
°	°	k?
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
rovníku	rovník	k1gInSc2
a	a	k8xC
0	#num#	k4
<g/>
°	°	k?
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
°	°	k?
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
rovníku	rovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
pak	pak	k6eAd1
nabývá	nabývat	k5eAaImIp3nS
hodnot	hodnota	k1gFnPc2
0	#num#	k4
<g/>
°	°	k?
<g/>
–	–	k?
<g/>
180	#num#	k4
<g/>
°	°	k?
na	na	k7c4
západ	západ	k1gInSc4
od	od	k7c2
nultého	nultý	k4xOgInSc2
poledníku	poledník	k1gInSc2
a	a	k8xC
0	#num#	k4
<g/>
°	°	k?
<g/>
–	–	k?
<g/>
180	#num#	k4
<g/>
°	°	k?
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
nultého	nultý	k4xOgInSc2
poledníku	poledník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nultým	nultý	k4xOgMnSc7
poledníkem	poledník	k1gMnSc7
ve	v	k7c6
WGS84	WGS84	k1gFnSc6
je	být	k5eAaImIp3nS
„	„	k?
<g/>
IERS	IERS	kA
Reference	reference	k1gFnPc1
Meridian	Meridian	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
5,31	5,31	k4
úhlových	úhlový	k2eAgFnPc2d1
vteřin	vteřina	k1gFnPc2
východně	východně	k6eAd1
od	od	k7c2
„	„	k?
<g/>
Greenwich	Greenwich	k1gInSc1
Prime	prim	k1gInSc5
Meridian	Meridiana	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souřadnicový	souřadnicový	k2eAgInSc1d1
systém	systém	k1gInSc1
WGS84	WGS84	k1gFnSc2
je	být	k5eAaImIp3nS
pravotočivá	pravotočivý	k2eAgFnSc1d1
kartézská	kartézský	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
souřadnic	souřadnice	k1gFnPc2
se	s	k7c7
středem	střed	k1gInSc7
v	v	k7c6
těžišti	těžiště	k1gNnSc6
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
moří	moře	k1gNnPc2
a	a	k8xC
atmosféry	atmosféra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladná	kladný	k2eAgFnSc1d1
osa	osa	k1gFnSc1
x	x	k?
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
průsečíku	průsečík	k1gInSc2
nultého	nultý	k4xOgInSc2
poledníku	poledník	k1gInSc2
a	a	k8xC
rovníku	rovník	k1gInSc2
<g/>
,	,	kIx,
kladná	kladný	k2eAgFnSc1d1
osa	osa	k1gFnSc1
z	z	k7c2
k	k	k7c3
severnímu	severní	k2eAgNnSc3d1
pólu	pólo	k1gNnSc3
a	a	k8xC
kladná	kladný	k2eAgFnSc1d1
osa	osa	k1gFnSc1
y	y	k?
je	být	k5eAaImIp3nS
na	na	k7c4
obě	dva	k4xCgNnPc4
předchozí	předchozí	k2eAgNnPc4d1
kolmá	kolmý	k2eAgNnPc4d1
ve	v	k7c6
směru	směr	k1gInSc6
doleva	doleva	k6eAd1
(	(	kIx(
<g/>
90	#num#	k4
<g/>
°	°	k?
východní	východní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
a	a	k8xC
0	#num#	k4
<g/>
°	°	k?
šířky	šířka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
tak	tak	k6eAd1
pravotočivou	pravotočivý	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
souřadnic	souřadnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Parametry	parametr	k1gInPc1
definující	definující	k2eAgInSc1d1
referenční	referenční	k2eAgInSc4d1
elipsoid	elipsoid	k1gInSc4
WGS84	WGS84	k1gFnSc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
délka	délka	k1gFnSc1
hlavní	hlavní	k2eAgFnSc2d1
poloosy	poloosa	k1gFnSc2
<g/>
:	:	kIx,
a	a	k8xC
=	=	kIx~
6	#num#	k4
378	#num#	k4
137	#num#	k4
m	m	kA
</s>
<s>
převrácená	převrácený	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
zploštění	zploštění	k1gNnSc2
(	(	kIx(
<g/>
f	f	k?
=	=	kIx~
1	#num#	k4
−	−	k?
b	b	k?
<g/>
/	/	kIx~
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1	#num#	k4
<g/>
/	/	kIx~
<g/>
f	f	k?
=	=	kIx~
298,257	298,257	k4
<g/>
223563	#num#	k4
</s>
<s>
úhlová	úhlový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
Země	zem	k1gFnSc2
<g/>
:	:	kIx,
ω	ω	k?
=	=	kIx~
7	#num#	k4
<g/>
,	,	kIx,
292	#num#	k4
115	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
5	#num#	k4
rad	rada	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
gravitační	gravitační	k2eAgInSc1d1
parametr	parametr	k1gInSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
součin	součin	k1gInSc1
hmotnosti	hmotnost	k1gFnSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
atmosféry	atmosféra	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
gravitační	gravitační	k2eAgFnPc1d1
konstanty	konstanta	k1gFnPc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
GM	GM	kA
=	=	kIx~
(	(	kIx(
<g/>
3986004,418	3986004,418	k4
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
0,008	0,008	k4
<g/>
)	)	kIx)
<g/>
×	×	k?
<g/>
108	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
nich	on	k3xPp3gInPc2
lze	lze	k6eAd1
spočítat	spočítat	k5eAaPmF
další	další	k2eAgInPc4d1
odvozené	odvozený	k2eAgInPc4d1
parametry	parametr	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
délka	délka	k1gFnSc1
vedlejší	vedlejší	k2eAgFnSc2d1
poloosy	poloosa	k1gFnSc2
<g/>
:	:	kIx,
b	b	k?
=	=	kIx~
6	#num#	k4
356	#num#	k4
752,314	752,314	k4
<g/>
2	#num#	k4
m	m	kA
</s>
<s>
první	první	k4xOgFnSc1
excentricita	excentricita	k1gFnSc1
<g/>
:	:	kIx,
e	e	k0
=	=	kIx~
8,181	8,181	k4
<g/>
9190842622	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
2	#num#	k4
</s>
<s>
a	a	k8xC
řadu	řada	k1gFnSc4
dalších	další	k2eAgFnPc2d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
parametry	parametr	k1gInPc1
geoidu	geoid	k1gInSc2
EGM	EGM	kA
<g/>
96	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
pravidelným	pravidelný	k2eAgInSc7d1
rastrem	rastr	k1gInSc7
bodů	bod	k1gInPc2
vzdálených	vzdálený	k2eAgInPc2d1
15	#num#	k4
<g/>
'	'	kIx"
(	(	kIx(
<g/>
tj.	tj.	kA
130	#num#	k4
317	#num#	k4
sférických	sférický	k2eAgFnPc2d1
expanzí	expanze	k1gFnPc2
oproti	oproti	k7c3
32	#num#	k4
757	#num#	k4
expanzím	expanze	k1gFnPc3
definovaným	definovaný	k2eAgFnPc3d1
ve	v	k7c6
WGS	WGS	kA
84	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
bývalého	bývalý	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
bylo	být	k5eAaImAgNnS
započato	započnout	k5eAaPmNgNnS
s	s	k7c7
realizací	realizace	k1gFnSc7
WGS-84	WGS-84	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
kampaně	kampaň	k1gFnSc2
VGSN	VGSN	kA
<g/>
'	'	kIx"
<g/>
92	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1998	#num#	k4
je	být	k5eAaImIp3nS
WGS84	WGS84	k1gFnSc4
zaveden	zavést	k5eAaPmNgInS
ve	v	k7c6
vojenském	vojenský	k2eAgNnSc6d1
a	a	k8xC
civilním	civilní	k2eAgNnSc6d1
letectvu	letectvo	k1gNnSc6
a	a	k8xC
v	v	k7c6
AČR	AČR	kA
je	být	k5eAaImIp3nS
běžně	běžně	k6eAd1
používán	používat	k5eAaImNgInS
v	v	k7c6
rámci	rámec	k1gInSc6
kooperace	kooperace	k1gFnSc2
a	a	k8xC
armádami	armáda	k1gFnPc7
NATO	NATO	kA
a	a	k8xC
standardizace	standardizace	k1gFnSc2
v	v	k7c6
geodézii	geodézie	k1gFnSc6
a	a	k8xC
kartografii	kartografie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elipsoid	elipsoid	k1gInSc1
</s>
<s>
Geoid	geoid	k1gInSc1
</s>
<s>
Global	globat	k5eAaImAgInS
Positioning	Positioning	k1gInSc1
System	Systo	k1gNnSc7
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
RAPANT	RAPANT	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
první	první	k4xOgFnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
VŠB	VŠB	kA
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
124	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NIMA	NIMA	k?
Technical	Technical	k1gFnSc1
Report	report	k1gInSc1
TR	TR	kA
<g/>
8350.2	8350.2	k4
Archivováno	archivován	k2eAgNnSc4d1
4	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
Oficiální	oficiální	k2eAgFnPc1d1
definice	definice	k1gFnPc1
WGS	WGS	kA
<g/>
84	#num#	k4
<g/>
,	,	kIx,
třetí	třetí	k4xOgNnSc4
vydání	vydání	k1gNnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Maria	Maria	k1gFnSc1
Ivanovna	Ivanovna	k1gFnSc1
Jurkina	Jurkina	k1gFnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Pick	Picka	k1gFnPc2
<g/>
:	:	kIx,
Numerické	numerický	k2eAgInPc1d1
výpočty	výpočet	k1gInPc1
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
geodetickém	geodetický	k2eAgInSc6d1
referenčním	referenční	k2eAgInSc6d1
systému	systém	k1gInSc6
1984	#num#	k4
(	(	kIx(
<g/>
WGS	WGS	kA
<g/>
84	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
in	in	k?
Vojenský	vojenský	k2eAgInSc1d1
geografický	geografický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
,	,	kIx,
Příloha	příloha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Geofyzikální	geofyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ČAV	ČAV	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
