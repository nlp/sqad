<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví日	souostroví日	k?
Fyzická	fyzický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
souostrovíGeografie	souostrovíGeografie	k1gFnSc1
Poloha	poloha	k1gFnSc1
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
137	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
44	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
~	~	kIx~
<g/>
368	#num#	k4
000	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
ostrovů	ostrov	k1gInPc2
</s>
<s>
~	~	kIx~
<g/>
3	#num#	k4
000	#num#	k4
Časové	časový	k2eAgInPc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
9	#num#	k4
Hlavní	hlavní	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
</s>
<s>
Honšú	Honšú	k?
Země	země	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Jazyk	jazyk	k1gInSc1
</s>
<s>
japonština	japonština	k1gFnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
日	日	k?
[	[	kIx(
<g/>
Nihon	Nihon	k1gMnSc1
Rettó	Rettó	k1gMnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
leží	ležet	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
obloukem	oblouk	k1gInSc7
dlouhým	dlouhý	k2eAgInSc7d1
2	#num#	k4
990	#num#	k4
km	km	kA
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
podél	podél	k7c2
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
asijského	asijský	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odděluje	oddělovat	k5eAaImIp3nS
Japonské	japonský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
od	od	k7c2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
Filipínského	filipínský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
i	i	k9
od	od	k7c2
Ochotského	ochotský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc4d1
část	část	k1gFnSc4
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
Japonsku	Japonsko	k1gNnSc3
náleží	náležet	k5eAaImIp3nS
ještě	ještě	k9
další	další	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
(	(	kIx(
<g/>
Rjúkjú	Rjúkjú	k1gNnSc1
<g/>
,	,	kIx,
Boninské	Boninský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nebývají	bývat	k5eNaImIp3nP
do	do	k7c2
Japonského	japonský	k2eAgNnSc2d1
souostroví	souostroví	k1gNnSc2
počítány	počítán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
definici	definice	k1gFnSc6
souostroví	souostroví	k1gNnSc2
nepanuje	panovat	k5eNaImIp3nS
jednota	jednota	k1gFnSc1
a	a	k8xC
především	především	k6eAd1
japonská	japonský	k2eAgFnSc1d1
geografie	geografie	k1gFnSc1
upřednostňuje	upřednostňovat	k5eAaImIp3nS
dělení	dělení	k1gNnSc4
podle	podle	k7c2
administrativních	administrativní	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
údaje	údaj	k1gInPc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
mohou	moct	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
,	,	kIx,
níže	nízce	k6eAd2
uvedené	uvedený	k2eAgInPc1d1
od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
:	:	kIx,
</s>
<s>
Hokkaidó	Hokkaidó	k?
</s>
<s>
Honšú	Honšú	k?
</s>
<s>
Šikoku	Šikok	k1gInSc3
</s>
<s>
Kjúšú	Kjúšú	k?
</s>
<s>
Termín	termín	k1gInSc1
Hlavní	hlavní	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Home	Home	k1gInSc1
Islands	Islands	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Káhirské	káhirský	k2eAgFnSc6d1
deklaraci	deklarace	k1gFnSc6
k	k	k7c3
určení	určení	k1gNnSc3
území	území	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c4
než	než	k8xS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
po	po	k7c6
válce	válka	k1gFnSc6
omezena	omezen	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
užíván	užíván	k2eAgInSc1d1
k	k	k7c3
rozlišení	rozlišení	k1gNnSc3
souostroví	souostroví	k1gNnSc2
od	od	k7c2
kolonií	kolonie	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
náležely	náležet	k5eAaImAgInP
Japonsku	Japonsko	k1gNnSc3
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Souostroví	souostroví	k1gNnSc1
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
geografie	geografie	k1gFnSc1
k	k	k7c3
japonskému	japonský	k2eAgNnSc3d1
souostroví	souostroví	k1gNnSc3
řadí	řadit	k5eAaImIp3nS
<g/>
,	,	kIx,
kromě	kromě	k7c2
ostrovů	ostrov	k1gInPc2
poblíž	poblíž	k6eAd1
hlavních	hlavní	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
i	i	k8xC
další	další	k2eAgNnPc1d1
souostroví	souostroví	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
Rjúkjú	Rjúkjú	k1gNnSc1
</s>
<s>
Izu	Izu	k?
</s>
<s>
Kurily	Kurily	k1gFnPc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senkaku	Senkak	k1gMnSc3
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Japonska	Japonsko	k1gNnSc2
</s>
<s>
Podle	podle	k7c2
této	tento	k3xDgFnSc2
definice	definice	k1gFnSc2
se	se	k3xPyFc4
souostroví	souostroví	k1gNnSc1
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
6	#num#	k4
852	#num#	k4
ostrovů	ostrov	k1gInPc2
majících	mající	k2eAgInPc2d1
obvod	obvod	k1gInSc4
delší	dlouhý	k2eAgInSc4d2
než	než	k8xS
100	#num#	k4
m	m	kA
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
430	#num#	k4
je	být	k5eAaImIp3nS
obydlených	obydlený	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Microsoft	Microsoft	kA
Encarta	Encarta	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
do	do	k7c2
Japonského	japonský	k2eAgNnSc2d1
souostroví	souostroví	k1gNnSc2
v	v	k7c6
širším	široký	k2eAgInSc6d2
pojmu	pojem	k1gInSc6
i	i	k8xC
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
,	,	kIx,
Sachalin	Sachalin	k1gInSc1
a	a	k8xC
Aleutské	aleutský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Paleogeografie	paleogeografie	k1gFnSc1
</s>
<s>
Geologicky	geologicky	k6eAd1
je	být	k5eAaImIp3nS
souostroví	souostroví	k1gNnSc1
tvořeno	tvořit	k5eAaImNgNnS
převážně	převážně	k6eAd1
podmořskými	podmořský	k2eAgInPc7d1
sedimenty	sediment	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgFnP
zemskou	zemský	k2eAgFnSc7d1
tektonikou	tektonika	k1gFnSc7
vyzvednuty	vyzvednut	k2eAgInPc4d1
nad	nad	k7c4
hladinu	hladina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
Japonského	japonský	k2eAgNnSc2d1
souostroví	souostroví	k1gNnSc2
v	v	k7c6
neogénu	neogén	k1gInSc6
udává	udávat	k5eAaImIp3nS
následující	následující	k2eAgInPc4d1
obrázky	obrázek	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
<g/>
,	,	kIx,
Japonské	japonský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
a	a	k8xC
okolní	okolní	k2eAgFnPc1d1
části	část	k1gFnPc1
asijského	asijský	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
v	v	k7c6
raném	raný	k2eAgInSc6d1
miocénu	miocén	k1gInSc6
(	(	kIx(
<g/>
23	#num#	k4
-	-	kIx~
18	#num#	k4
Ma	Ma	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
<g/>
,	,	kIx,
Japonské	japonský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
a	a	k8xC
okolní	okolní	k2eAgFnPc1d1
části	část	k1gFnPc1
asijského	asijský	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
ve	v	k7c6
středním	střední	k2eAgInSc6d1
a	a	k8xC
pozdním	pozdní	k2eAgInSc6d1
pliocénu	pliocén	k1gInSc6
(	(	kIx(
<g/>
3,5	3,5	k4
-	-	kIx~
2	#num#	k4
Ma	Ma	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
při	při	k7c6
maximálním	maximální	k2eAgNnSc6d1
zalednění	zalednění	k1gNnSc6
v	v	k7c6
ledové	ledový	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
pleistocénu	pleistocén	k1gInSc6
asi	asi	k9
před	před	k7c7
20	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
moře	moře	k1gNnSc2
pevnina	pevnina	k1gFnSc1
pevnina	pevnina	k1gFnSc1
bez	bez	k7c2
vegetace	vegetace	k1gFnSc2
současné	současný	k2eAgFnSc2d1
pobřeží	pobřeží	k1gNnSc3
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Japonsko	Japonsko	k1gNnSc1
nárokuje	nárokovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
nejjižnější	jižní	k2eAgInPc4d3
ostrovy	ostrov	k1gInPc4
Kunaširi	Kunašir	k1gFnSc2
a	a	k8xC
Etorofu	Etorof	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
japonských	japonský	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
nejsou	být	k5eNaImIp3nP
explicitně	explicitně	k6eAd1
uváděny	uvádět	k5eAaImNgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
součást	součást	k1gFnSc4
Rjúkjú	Rjúkjú	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SHAHGEDANOVA	SHAHGEDANOVA	kA
<g/>
,	,	kIx,
Maria	Maria	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Physical	Physical	k1gFnSc2
Geography	Geographa	k1gFnSc2
of	of	k?
Northern	Northern	k1gInSc1
Eurasia	Eurasia	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
571	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
823384	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Wyspy	Wyspa	k1gFnSc2
Japońskie	Japońskie	k1gFnSc2
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
日	日	k?
na	na	k7c6
japonské	japonský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Japanese	Japanese	k1gFnSc1
archipelago	archipelago	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Look	Look	k1gInSc1
Japan	japan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
43	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Look	Look	k1gInSc1
Japan	japan	k1gInSc1
<g/>
,	,	kIx,
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
35	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
ostrovů	ostrov	k1gInPc2
Japonska	Japonsko	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Landforms	Landforms	k1gInSc1
and	and	k?
Geology	geolog	k1gMnPc4
of	of	k?
Japan	japan	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GLGArcs	GLGArcsa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
258174637	#num#	k4
</s>
