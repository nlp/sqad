<p>
<s>
Výreček	výreček	k1gMnSc1	výreček
malý	malý	k2eAgMnSc1d1	malý
(	(	kIx(	(
<g/>
Otus	Otus	k1gInSc1	Otus
scops	scops	k1gInSc1	scops
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
druh	druh	k1gInSc1	druh
sovy	sova	k1gFnSc2	sova
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
puštíkovití	puštíkovitý	k2eAgMnPc1d1	puštíkovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Výreček	výreček	k1gMnSc1	výreček
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc7d1	malá
sovou	sova	k1gFnSc7	sova
s	s	k7c7	s
pernatými	pernatý	k2eAgNnPc7d1	pernaté
oušky	ouško	k1gNnPc7	ouško
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
mezi	mezi	k7c4	mezi
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
63	[number]	k4	63
cm	cm	kA	cm
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
křídlo	křídlo	k1gNnSc1	křídlo
měří	měřit	k5eAaImIp3nS	měřit
obvykle	obvykle	k6eAd1	obvykle
okolo	okolo	k7c2	okolo
16	[number]	k4	16
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
zhruba	zhruba	k6eAd1	zhruba
70-85	[number]	k4	70-85
g	g	kA	g
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
přes	přes	k7c4	přes
100	[number]	k4	100
g.	g.	k?	g.
Typickými	typický	k2eAgFnPc7d1	typická
barvami	barva	k1gFnPc7	barva
peří	peřit	k5eAaImIp3nP	peřit
jsou	být	k5eAaImIp3nP	být
hnědá	hnědý	k2eAgNnPc1d1	hnědé
<g/>
,	,	kIx,	,
šedá	šedý	k2eAgNnPc1d1	šedé
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Výreček	výreček	k1gMnSc1	výreček
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dokonale	dokonale	k6eAd1	dokonale
splynout	splynout	k5eAaPmF	splynout
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
maskování	maskování	k1gNnSc1	maskování
velmi	velmi	k6eAd1	velmi
připomíná	připomínat	k5eAaImIp3nS	připomínat
kůru	kůra	k1gFnSc4	kůra
běžných	běžný	k2eAgInPc2d1	běžný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
biotop	biotop	k1gInSc1	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
Výreček	výreček	k1gMnSc1	výreček
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
především	především	k9	především
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
odlétá	odlétat	k5eAaPmIp3nS	odlétat
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
lesostepích	lesostep	k1gFnPc6	lesostep
<g/>
,	,	kIx,	,
hájích	háj	k1gInPc6	háj
<g/>
,	,	kIx,	,
parcích	park	k1gInPc6	park
a	a	k8xC	a
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
spatřen	spatřit	k5eAaPmNgMnS	spatřit
hnízdit	hnízdit	k5eAaImF	hnízdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
každoročně	každoročně	k6eAd1	každoročně
několik	několik	k4yIc1	několik
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nepatrný	patrný	k2eNgInSc4d1	patrný
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
<g/>
,	,	kIx,	,
škvírách	škvíra	k1gFnPc6	škvíra
a	a	k8xC	a
výklencích	výklenek	k1gInPc6	výklenek
ve	v	k7c6	v
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
opuštěných	opuštěný	k2eAgNnPc6d1	opuštěné
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc4d1	vlastní
hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
nestaví	stavit	k5eNaImIp3nP	stavit
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdiště	hnízdiště	k1gNnSc1	hnízdiště
vybírá	vybírat	k5eAaImIp3nS	vybírat
samec	samec	k1gInSc4	samec
<g/>
,	,	kIx,	,
intenzivně	intenzivně	k6eAd1	intenzivně
ho	on	k3xPp3gMnSc4	on
brání	bránit	k5eAaImIp3nS	bránit
a	a	k8xC	a
láká	lákat	k5eAaImIp3nS	lákat
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
samici	samice	k1gFnSc4	samice
monotónním	monotónní	k2eAgNnSc7d1	monotónní
pískáním	pískání	k1gNnSc7	pískání
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
dá	dát	k5eAaPmIp3nS	dát
najevo	najevo	k6eAd1	najevo
spokojenost	spokojenost	k1gFnSc1	spokojenost
s	s	k7c7	s
hnízdem	hnízdo	k1gNnSc7	hnízdo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
2-8	[number]	k4	2-8
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
sedí	sedit	k5eAaImIp3nS	sedit
sama	sám	k3xTgMnSc4	sám
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
jí	on	k3xPp3gFnSc7	on
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
krmí	krmit	k5eAaImIp3nS	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
24-26	[number]	k4	24-26
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgMnPc4	jenž
pečují	pečovat	k5eAaImIp3nP	pečovat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
dokáží	dokázat	k5eAaPmIp3nP	dokázat
mláďata	mládě	k1gNnPc4	mládě
opustit	opustit	k5eAaPmF	opustit
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gMnPc4	on
krmí	krmit	k5eAaImIp3nP	krmit
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
lovy	lov	k1gInPc7	lov
<g/>
.	.	kIx.	.
</s>
<s>
Výreček	výreček	k1gMnSc1	výreček
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
až	až	k9	až
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Výreček	výreček	k1gMnSc1	výreček
malý	malý	k1gMnSc1	malý
loví	lovit	k5eAaImIp3nS	lovit
v	v	k7c6	v
podvečer	podvečer	k6eAd1	podvečer
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úlovku	úlovek	k1gInSc6	úlovek
pátrá	pátrat	k5eAaImIp3nS	pátrat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
stanoviště	stanoviště	k1gNnSc2	stanoviště
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgFnPc4	všechen
sovy	sova	k1gFnPc4	sova
má	mít	k5eAaImIp3nS	mít
zúžený	zúžený	k2eAgInSc1d1	zúžený
zorný	zorný	k2eAgInSc1d1	zorný
úhel	úhel	k1gInSc1	úhel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
to	ten	k3xDgNnSc1	ten
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
otáčením	otáčení	k1gNnSc7	otáčení
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
kořistí	kořist	k1gFnSc7	kořist
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
sarančata	saranče	k1gNnPc1	saranče
<g/>
,	,	kIx,	,
kobylky	kobylka	k1gFnPc1	kobylka
<g/>
,	,	kIx,	,
vážky	vážka	k1gFnPc1	vážka
<g/>
,	,	kIx,	,
cikády	cikáda	k1gFnPc1	cikáda
<g/>
,	,	kIx,	,
mouchy	moucha	k1gFnPc1	moucha
<g/>
,	,	kIx,	,
různí	různý	k2eAgMnPc1d1	různý
brouci	brouk	k1gMnPc1	brouk
<g/>
,	,	kIx,	,
můry	můra	k1gFnPc1	můra
<g/>
,	,	kIx,	,
škvoři	škvor	k1gMnPc1	škvor
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnPc1	housenka
a	a	k8xC	a
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
jiné	jiný	k2eAgMnPc4d1	jiný
členovce	členovec	k1gMnPc4	členovec
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
stonožky	stonožka	k1gFnPc1	stonožka
<g/>
,	,	kIx,	,
mnohonožky	mnohonožka	k1gFnPc1	mnohonožka
<g/>
,	,	kIx,	,
pavouci	pavouk	k1gMnPc1	pavouk
a	a	k8xC	a
žížaly	žížala	k1gFnPc1	žížala
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
i	i	k8xC	i
různé	různý	k2eAgMnPc4d1	různý
malé	malý	k2eAgMnPc4d1	malý
hlodavce	hlodavec	k1gMnPc4	hlodavec
-	-	kIx~	-
hraboše	hraboš	k1gMnPc4	hraboš
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc4	myš
<g/>
,	,	kIx,	,
myšice	myšice	k1gFnPc4	myšice
a	a	k8xC	a
rejsky	rejsek	k1gInPc4	rejsek
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
zabije	zabít	k5eAaPmIp3nS	zabít
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
žáby	žába	k1gFnPc4	žába
<g/>
,	,	kIx,	,
malé	malý	k2eAgMnPc4d1	malý
hady	had	k1gMnPc4	had
nebo	nebo	k8xC	nebo
drobné	drobný	k2eAgMnPc4d1	drobný
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hýla	hýl	k1gMnSc4	hýl
či	či	k8xC	či
strnada	strnad	k1gMnSc4	strnad
<g/>
.	.	kIx.	.
</s>
<s>
Malou	malý	k2eAgFnSc4d1	malá
kořist	kořist	k1gFnSc4	kořist
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
celou	celý	k2eAgFnSc4d1	celá
(	(	kIx(	(
<g/>
létavému	létavý	k2eAgInSc3d1	létavý
hmyzu	hmyz	k1gInSc3	hmyz
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
trhá	trhat	k5eAaImIp3nS	trhat
křídla	křídlo	k1gNnSc2	křídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc1d2	veliký
úlovek	úlovek	k1gInSc1	úlovek
si	se	k3xPyFc3	se
natrhá	natrhat	k5eAaBmIp3nS	natrhat
na	na	k7c6	na
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Ptáky	pták	k1gMnPc4	pták
před	před	k7c7	před
pozřením	pozření	k1gNnPc3	pozření
oškube	oškubat	k5eAaPmIp3nS	oškubat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
výreček	výreček	k1gMnSc1	výreček
veden	vést	k5eAaImNgMnS	vést
jako	jako	k9	jako
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgMnSc1d1	dotčený
(	(	kIx(	(
<g/>
least	least	k1gMnSc1	least
concern	concern	k1gMnSc1	concern
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
světě	svět	k1gInSc6	svět
žije	žít	k5eAaImIp3nS	žít
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
-	-	kIx~	-
3	[number]	k4	3
000	[number]	k4	000
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgInSc1d1	populační
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
úbytek	úbytek	k1gInSc1	úbytek
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ptáka	pták	k1gMnSc4	pták
nesmírně	smírně	k6eNd1	smírně
vzácného	vzácný	k2eAgNnSc2d1	vzácné
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
chráněného	chráněný	k2eAgNnSc2d1	chráněné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČERVENÝ	Červený	k1gMnSc1	Červený
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
myslivosti	myslivost	k1gFnSc2	myslivost
<g/>
.	.	kIx.	.
</s>
<s>
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
výreček	výreček	k1gMnSc1	výreček
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výreček	výreček	k1gMnSc1	výreček
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Ohrada	ohrada	k1gFnSc1	ohrada
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Výreček	výreček	k1gMnSc1	výreček
malý	malý	k1gMnSc1	malý
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Otus	Otus	k1gInSc1	Otus
scops	scops	k6eAd1	scops
</s>
</p>
<p>
<s>
Výreček	výreček	k1gMnSc1	výreček
malý	malý	k2eAgMnSc1d1	malý
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Ptačí	ptačí	k2eAgInSc1d1	ptačí
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
Výreček	výreček	k1gMnSc1	výreček
malý	malý	k1gMnSc1	malý
</s>
</p>
<p>
<s>
Javier	Javier	k1gMnSc1	Javier
Blasco-Zumeta	Blasco-Zumeto	k1gNnSc2	Blasco-Zumeto
&	&	k?	&
Gerd-Michael	Gerd-Michael	k1gInSc1	Gerd-Michael
Heinze	Heinze	k1gFnSc1	Heinze
<g/>
:	:	kIx,	:
Scops	Scops	k1gInSc1	Scops
Owl	Owl	k1gFnSc2	Owl
</s>
</p>
