<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
Durynské	durynský	k2eAgNnSc1d1	Durynské
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
kostel	kostel	k1gInSc4	kostel
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
plochou	plocha	k1gFnSc7	plocha
1200	[number]	k4	1200
m2	m2	k4	m2
a	a	k8xC	a
kapacitou	kapacita	k1gFnSc7	kapacita
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5000	[number]	k4	5000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvýchodnější	východní	k2eAgFnSc1d3	nejvýchodnější
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
