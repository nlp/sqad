<p>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
Durynské	durynský	k2eAgNnSc1d1	Durynské
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
kostel	kostel	k1gInSc4	kostel
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
plochou	plocha	k1gFnSc7	plocha
1200	[number]	k4	1200
m2	m2	k4	m2
a	a	k8xC	a
kapacitou	kapacita	k1gFnSc7	kapacita
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5000	[number]	k4	5000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvýchodnější	východní	k2eAgFnSc1d3	nejvýchodnější
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ji	on	k3xPp3gFnSc4	on
formuje	formovat	k5eAaImIp3nS	formovat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
chrámem	chrám	k1gInSc7	chrám
Košické	košický	k2eAgFnSc2d1	Košická
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
a	a	k8xC	a
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
farnosti	farnost	k1gFnSc2	farnost
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
délka	délka	k1gFnSc1	délka
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
36	[number]	k4	36
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
severní	severní	k2eAgFnSc2d1	severní
věže	věž	k1gFnSc2	věž
59	[number]	k4	59
m	m	kA	m
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc1d1	hlavní
lodě	loď	k1gFnPc1	loď
24	[number]	k4	24
m	m	kA	m
a	a	k8xC	a
bočných	bočný	k2eAgFnPc2d1	bočná
lodí	loď	k1gFnPc2	loď
12	[number]	k4	12
m.	m.	k?	m.
Stavba	stavba	k1gFnSc1	stavba
měla	mít	k5eAaImAgFnS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
stavitelskou	stavitelský	k2eAgFnSc4d1	stavitelská
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
okolních	okolní	k2eAgNnPc6d1	okolní
městech	město	k1gNnPc6	město
Prešov	Prešov	k1gInSc4	Prešov
<g/>
,	,	kIx,	,
Bardejov	Bardejov	k1gInSc1	Bardejov
<g/>
,	,	kIx,	,
Sabinov	Sabinov	k1gInSc1	Sabinov
<g/>
,	,	kIx,	,
Rožňava	Rožňava	k1gFnSc1	Rožňava
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
výstavbu	výstavba	k1gFnSc4	výstavba
dalších	další	k2eAgInPc2d1	další
chrámů	chrám	k1gInPc2	chrám
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
(	(	kIx(	(
<g/>
Sibiu	Sibium	k1gNnSc6	Sibium
<g/>
,	,	kIx,	,
Brašov	Brašov	k1gInSc4	Brašov
a	a	k8xC	a
Kluž	Kluž	k1gFnSc4	Kluž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Košickou	košický	k2eAgFnSc4d1	Košická
gotickou	gotický	k2eAgFnSc4d1	gotická
katedrálu	katedrála	k1gFnSc4	katedrála
tvoří	tvořit	k5eAaImIp3nS	tvořit
svatyně	svatyně	k1gFnSc1	svatyně
s	s	k7c7	s
pětidílným	pětidílný	k2eAgInSc7d1	pětidílný
uzávěrem	uzávěr	k1gInSc7	uzávěr
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
poschoďová	poschoďový	k2eAgFnSc1d1	poschoďová
sakristie	sakristie	k1gFnSc1	sakristie
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
kaple	kaple	k1gFnPc4	kaple
a	a	k8xC	a
předsíň	předsíň	k1gFnSc4	předsíň
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
disposice	disposice	k1gFnSc1	disposice
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavní	hlavní	k2eAgFnSc4d1	hlavní
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
boční	boční	k2eAgFnPc4d1	boční
lodě	loď	k1gFnPc4	loď
kříži	kříž	k1gInSc6	kříž
v	v	k7c6	v
polovici	polovice	k1gFnSc6	polovice
jejich	jejich	k3xOp3gFnSc2	jejich
délky	délka	k1gFnSc2	délka
jedna	jeden	k4xCgFnSc1	jeden
příčná	příčný	k2eAgFnSc1d1	příčná
loď	loď	k1gFnSc1	loď
stejné	stejný	k2eAgFnSc2d1	stejná
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
šířky	šířka	k1gFnSc2	šířka
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
řecký	řecký	k2eAgInSc1d1	řecký
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ve	v	k7c4	v
středu	středa	k1gFnSc4	středa
katedrály	katedrála	k1gFnSc2	katedrála
vzniká	vznikat	k5eAaImIp3nS	vznikat
objemný	objemný	k2eAgInSc1d1	objemný
centrální	centrální	k2eAgInSc1d1	centrální
prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
v	v	k7c6	v
exteriéru	exteriér	k1gInSc2	exteriér
tři	tři	k4xCgInPc1	tři
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
štíty	štít	k1gInPc1	štít
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
dekorovanými	dekorovaný	k2eAgInPc7d1	dekorovaný
portály	portál	k1gInPc7	portál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
středověkého	středověký	k2eAgNnSc2d1	středověké
kamenického	kamenický	k2eAgNnSc2d1	kamenické
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
staveb	stavba	k1gFnPc2	stavba
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
a	a	k8xC	a
Urbanovy	Urbanův	k2eAgFnSc2d1	Urbanova
věže	věž	k1gFnSc2	věž
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předchůdce	předchůdce	k1gMnSc2	předchůdce
chrámu	chrám	k1gInSc2	chrám
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zasvěcen	zasvětit	k5eAaPmNgMnS	zasvětit
byl	být	k5eAaImAgMnS	být
svatému	svatý	k2eAgMnSc3d1	svatý
Michalovi	Michal	k1gMnSc3	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
románském	románský	k2eAgInSc6d1	románský
slohu	sloh	k1gInSc6	sloh
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
současného	současný	k2eAgInSc2d1	současný
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jako	jako	k8xS	jako
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
písemné	písemný	k2eAgFnSc6d1	písemná
zmínce	zmínka	k1gFnSc6	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
usazení	usazení	k1gNnSc6	usazení
německých	německý	k2eAgMnPc2d1	německý
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
svatá	svatý	k2eAgFnSc1d1	svatá
Alžběta	Alžběta	k1gFnSc1	Alžběta
stala	stát	k5eAaPmAgFnS	stát
patronkou	patronka	k1gFnSc7	patronka
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
původního	původní	k2eAgNnSc2d1	původní
patrocinia	patrocinium	k1gNnSc2	patrocinium
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
právě	právě	k6eAd1	právě
svaté	svatý	k2eAgFnSc3d1	svatá
Alžbětě	Alžběta	k1gFnSc3	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
byl	být	k5eAaImAgInS	být
postel	postel	k1gInSc1	postel
upravován	upravovat	k5eAaImNgInS	upravovat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc4	kostel
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgMnS	zachovat
románskou	románský	k2eAgFnSc4d1	románská
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgMnS	dostat
gotickou	gotický	k2eAgFnSc4d1	gotická
klenbu	klenba	k1gFnSc4	klenba
a	a	k8xC	a
uzávěr	uzávěr	k1gInSc4	uzávěr
svatyně	svatyně	k1gFnSc2	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Listina	listina	k1gFnSc1	listina
papeže	papež	k1gMnSc2	papež
Martina	Martin	k1gMnSc2	Martin
IV	IV	kA	IV
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1283	[number]	k4	1283
už	už	k6eAd1	už
uvádí	uvádět	k5eAaImIp3nS	uvádět
zasvěcení	zasvěcení	k1gNnSc1	zasvěcení
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc3d1	svatá
Alžbětě	Alžběta	k1gFnSc3	Alžběta
<g/>
.	.	kIx.	.
<g/>
Základy	základ	k1gInPc4	základ
tohoto	tento	k3xDgInSc2	tento
románsko-gotického	románskootický	k2eAgInSc2d1	románsko-gotický
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
letech	let	k1gInPc6	let
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Svatyně	svatyně	k1gFnSc1	svatyně
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
na	na	k7c4	na
východ	východ	k1gInSc4	východ
měla	mít	k5eAaImAgFnS	mít
11,5	[number]	k4	11,5
×	×	k?	×
10,25	[number]	k4	10,25
m	m	kA	m
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
27,8	[number]	k4	27,8
×	×	k?	×
14	[number]	k4	14
m.	m.	k?	m.
Celý	celý	k2eAgInSc1d1	celý
prostor	prostor	k1gInSc1	prostor
měl	mít	k5eAaImAgInS	mít
plochu	plocha	k1gFnSc4	plocha
520	[number]	k4	520
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
upraven	upravit	k5eAaPmNgInS	upravit
a	a	k8xC	a
udržován	udržovat	k5eAaImNgInS	udržovat
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
až	až	k9	až
do	do	k7c2	do
výstavby	výstavba	k1gFnSc2	výstavba
současného	současný	k2eAgInSc2d1	současný
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
několik	několik	k4yIc1	několik
románských	románský	k2eAgInPc2d1	románský
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
soška	soška	k1gFnSc1	soška
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
a	a	k8xC	a
několik	několik	k4yIc1	několik
náhrobních	náhrobní	k2eAgInPc2d1	náhrobní
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
výstavby	výstavba	k1gFnSc2	výstavba
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
===	===	k?	===
</s>
</p>
<p>
<s>
Požár	požár	k1gInSc1	požár
starého	starý	k2eAgInSc2d1	starý
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
byl	být	k5eAaImAgInS	být
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
příležitostí	příležitost	k1gFnSc7	příležitost
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
nového	nový	k2eAgMnSc2d1	nový
<g/>
,	,	kIx,	,
okázalého	okázalý	k2eAgInSc2d1	okázalý
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
významu	význam	k1gInSc2	význam
středověkých	středověký	k2eAgMnPc2d1	středověký
Košic	Košice	k1gInPc2	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
a	a	k8xC	a
řemesla	řemeslo	k1gNnSc2	řemeslo
prožívaly	prožívat	k5eAaImAgInP	prožívat
po	po	k7c4	po
udělení	udělení	k1gNnSc4	udělení
početných	početný	k2eAgNnPc2d1	početné
privilegií	privilegium	k1gNnPc2	privilegium
městu	město	k1gNnSc3	město
Anjouovci	Anjouovec	k1gInSc3	Anjouovec
období	období	k1gNnSc1	období
konjunktury	konjunktura	k1gFnSc2	konjunktura
<g/>
.	.	kIx.	.
</s>
<s>
Bohatí	bohatý	k2eAgMnPc1d1	bohatý
měšťané	měšťan	k1gMnPc1	měšťan
financovali	financovat	k5eAaBmAgMnP	financovat
výstavbu	výstavba	k1gFnSc4	výstavba
gotické	gotický	k2eAgFnSc2d1	gotická
katedrály	katedrála	k1gFnSc2	katedrála
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
podporou	podpora	k1gFnSc7	podpora
panovníka	panovník	k1gMnSc2	panovník
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
výrazně	výrazně	k6eAd1	výrazně
podporovala	podporovat	k5eAaImAgFnS	podporovat
i	i	k9	i
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Odpustkovou	odpustkový	k2eAgFnSc4d1	odpustková
bulu	bula	k1gFnSc4	bula
<g/>
.	.	kIx.	.
</s>
<s>
Všem	všecek	k3xTgMnPc3	všecek
poutníkům	poutník	k1gMnPc3	poutník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přispěli	přispět	k5eAaPmAgMnP	přispět
na	na	k7c4	na
košický	košický	k2eAgInSc4d1	košický
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
odpuštěny	odpuštěn	k2eAgInPc4d1	odpuštěn
hříchy	hřích	k1gInPc4	hřích
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
byly	být	k5eAaImAgInP	být
odpuštěny	odpustit	k5eAaPmNgInP	odpustit
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
Chrámu	chrámat	k5eAaImIp1nS	chrámat
svatého	svatý	k2eAgMnSc4d1	svatý
Marka	Marek	k1gMnSc4	Marek
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
nebo	nebo	k8xC	nebo
kaple	kaple	k1gFnSc1	kaple
Porcinkule	porcinkule	k1gFnSc2	porcinkule
v	v	k7c6	v
Assisi	Assise	k1gFnSc6	Assise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
začátku	začátek	k1gInSc2	začátek
výstavby	výstavba	k1gFnSc2	výstavba
nového	nový	k2eAgInSc2d1	nový
chrámu	chrám	k1gInSc2	chrám
není	být	k5eNaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1380	[number]	k4	1380
(	(	kIx(	(
<g/>
požár	požár	k1gInSc1	požár
starého	starý	k2eAgInSc2d1	starý
kostela	kostel	k1gInSc2	kostel
<g/>
)	)	kIx)	)
a	a	k8xC	a
1402	[number]	k4	1402
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
fáze	fáze	k1gFnPc1	fáze
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
etapě	etapa	k1gFnSc6	etapa
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
stavěný	stavěný	k2eAgInSc1d1	stavěný
jako	jako	k8xC	jako
pětilodní	pětilodní	k2eAgFnSc1d1	pětilodní
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
za	za	k7c2	za
současné	současný	k2eAgFnSc2d1	současná
existence	existence	k1gFnSc2	existence
starého	starý	k2eAgInSc2d1	starý
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
systémem	systém	k1gInSc7	systém
obstavování	obstavování	k1gNnSc2	obstavování
nové	nový	k2eAgFnSc2d1	nová
katedrály	katedrála	k1gFnSc2	katedrála
okolo	okolo	k7c2	okolo
původního	původní	k2eAgInSc2d1	původní
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnPc1d1	jižní
polygonální	polygonální	k2eAgFnPc1d1	polygonální
apsidy	apsida	k1gFnPc1	apsida
bočních	boční	k2eAgFnPc2d1	boční
lodí	loď	k1gFnPc2	loď
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stavět	stavět	k5eAaImF	stavět
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
stavba	stavba	k1gFnSc1	stavba
jižní	jižní	k2eAgFnSc2d1	jižní
obvodové	obvodový	k2eAgFnSc2d1	obvodová
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
jižního	jižní	k2eAgInSc2d1	jižní
portálu	portál	k1gInSc2	portál
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozestavěli	rozestavět	k5eAaPmAgMnP	rozestavět
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
poschodí	poschodí	k1gNnSc6	poschodí
obou	dva	k4xCgFnPc2	dva
věží	věž	k1gFnPc2	věž
zakomponovaných	zakomponovaný	k2eAgFnPc2d1	zakomponovaná
do	do	k7c2	do
půdorysu	půdorys	k1gInSc2	půdorys
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
chrámu	chrám	k1gInSc2	chrám
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
stavební	stavební	k2eAgFnSc1d1	stavební
huť	huť	k1gFnSc1	huť
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c4	na
slezskou	slezský	k2eAgFnSc4d1	Slezská
gotiku	gotika	k1gFnSc4	gotika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
na	na	k7c6	na
konci	konec	k1gInSc6	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
františkánského	františkánský	k2eAgInSc2d1	františkánský
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
čtvrti	čtvrt	k1gFnSc6	čtvrt
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1411	[number]	k4	1411
existuje	existovat	k5eAaImIp3nS	existovat
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
mistru	mistr	k1gMnSc6	mistr
Mikulášovi	Mikuláš	k1gMnSc6	Mikuláš
a	a	k8xC	a
o	o	k7c6	o
Zikmundově	Zikmundův	k2eAgMnSc6d1	Zikmundův
dvorním	dvorní	k2eAgMnSc6d1	dvorní
architektu	architekt	k1gMnSc6	architekt
Petrovi	Petr	k1gMnSc6	Petr
z	z	k7c2	z
Budína	Budín	k1gInSc2	Budín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
etapa	etapa	k1gFnSc1	etapa
výstavby	výstavba	k1gFnSc2	výstavba
1420	[number]	k4	1420
<g/>
–	–	k?	–
<g/>
1440	[number]	k4	1440
===	===	k?	===
</s>
</p>
<p>
<s>
Výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
koncepci	koncepce	k1gFnSc6	koncepce
výstavby	výstavba	k1gFnSc2	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
přinesla	přinést	k5eAaPmAgFnS	přinést
nová	nový	k2eAgFnSc1d1	nová
stavební	stavební	k2eAgFnSc1d1	stavební
dílna	dílna	k1gFnSc1	dílna
(	(	kIx(	(
<g/>
huť	huť	k1gFnSc1	huť
<g/>
)	)	kIx)	)
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
architekt	architekt	k1gMnSc1	architekt
huti	huť	k1gFnSc2	huť
anonymní	anonymní	k2eAgMnSc1d1	anonymní
<g/>
.	.	kIx.	.
</s>
<s>
Pětilodní	pětilodní	k2eAgFnSc1d1	pětilodní
disposice	disposice	k1gFnSc1	disposice
bazilikálního	bazilikální	k2eAgInSc2d1	bazilikální
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
jevila	jevit	k5eAaImAgFnS	jevit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
už	už	k6eAd1	už
jako	jako	k9	jako
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vertikálnost	vertikálnost	k1gFnSc4	vertikálnost
<g/>
,	,	kIx,	,
odlehčené	odlehčený	k2eAgFnPc4d1	odlehčená
hmoty	hmota	k1gFnPc4	hmota
a	a	k8xC	a
prostornost	prostornost	k1gFnSc4	prostornost
chrámu	chrám	k1gInSc2	chrám
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
třílodní	třílodní	k2eAgFnSc2d1	třílodní
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zásadní	zásadní	k2eAgFnSc7d1	zásadní
inovativní	inovativní	k2eAgFnSc7d1	inovativní
změnou	změna	k1gFnSc7	změna
plánu	plán	k1gInSc2	plán
bylo	být	k5eAaImAgNnS	být
přidání	přidání	k1gNnSc1	přidání
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
lodi	loď	k1gFnSc3	loď
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
příčné	příčný	k2eAgFnPc1d1	příčná
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
situovaná	situovaný	k2eAgFnSc1d1	situovaná
netradičně	tradičně	k6eNd1	tradičně
do	do	k7c2	do
středu	střed	k1gInSc2	střed
délky	délka	k1gFnSc2	délka
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodě	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
obvyklým	obvyklý	k2eAgNnSc7d1	obvyklé
křížením	křížení	k1gNnSc7	křížení
na	na	k7c6	na
styku	styk	k1gInSc6	styk
se	s	k7c7	s
svatyní	svatyně	k1gFnSc7	svatyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
unikátní	unikátní	k2eAgInSc4d1	unikátní
centrální	centrální	k2eAgInSc4d1	centrální
halový	halový	k2eAgInSc4d1	halový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
stavební	stavební	k2eAgFnSc2d1	stavební
fáze	fáze	k1gFnSc2	fáze
spadá	spadat	k5eAaImIp3nS	spadat
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
portálů	portál	k1gInPc2	portál
hlavní	hlavní	k2eAgFnSc1d1	hlavní
a	a	k8xC	a
příčné	příčný	k2eAgFnPc1d1	příčná
lodě	loď	k1gFnPc1	loď
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
gotickými	gotický	k2eAgFnPc7d1	gotická
stavbami	stavba	k1gFnPc7	stavba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
ukázku	ukázka	k1gFnSc4	ukázka
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
gotického	gotický	k2eAgNnSc2d1	gotické
kamenického	kamenický	k2eAgNnSc2d1	kamenické
umění	umění	k1gNnSc2	umění
evropského	evropský	k2eAgInSc2d1	evropský
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
Parléřovské	Parléřovský	k2eAgFnSc2d1	Parléřovský
huti	huť	k1gFnSc2	huť
působící	působící	k2eAgFnSc2d1	působící
u	u	k7c2	u
pražské	pražský	k2eAgFnSc2d1	Pražská
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
královského	královský	k2eAgNnSc2d1	královské
oratoria	oratorium	k1gNnSc2	oratorium
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vedoucích	vedoucí	k1gMnPc2	vedoucí
sdružených	sdružený	k2eAgInPc2d1	sdružený
točitých	točitý	k2eAgInPc2d1	točitý
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
kruhového	kruhový	k2eAgNnSc2d1	kruhové
zábradlí	zábradlí	k1gNnSc2	zábradlí
oratoria	oratorium	k1gNnSc2	oratorium
a	a	k8xC	a
kamenné	kamenný	k2eAgFnSc2d1	kamenná
pavlače	pavlač	k1gFnSc2	pavlač
nad	nad	k7c7	nad
sakristií	sakristie	k1gFnSc7	sakristie
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
pražské	pražský	k2eAgInPc1d1	pražský
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
severní	severní	k2eAgFnSc2d1	severní
obvodové	obvodový	k2eAgFnSc2d1	obvodová
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
polygonálních	polygonální	k2eAgFnPc2d1	polygonální
apsid	apsida	k1gFnPc2	apsida
severní	severní	k2eAgFnSc2d1	severní
boční	boční	k2eAgFnSc2d1	boční
lodě	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
paralely	paralela	k1gFnPc1	paralela
jižních	jižní	k2eAgFnPc2d1	jižní
apsid	apsida	k1gFnPc2	apsida
<g/>
)	)	kIx)	)
a	a	k8xC	a
osmibokých	osmiboký	k2eAgNnPc2d1	osmiboké
horních	horní	k2eAgNnPc2d1	horní
poschodí	poschodí	k1gNnPc2	poschodí
Zikmundovy	Zikmundův	k2eAgFnSc2d1	Zikmundova
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
stavební	stavební	k2eAgFnSc2d1	stavební
fáze	fáze	k1gFnSc2	fáze
byla	být	k5eAaImAgFnS	být
katedrála	katedrála	k1gFnSc1	katedrála
připravena	připravit	k5eAaPmNgFnS	připravit
na	na	k7c4	na
zaklenutí	zaklenutí	k1gNnSc4	zaklenutí
a	a	k8xC	a
starý	starý	k2eAgInSc4d1	starý
kostel	kostel	k1gInSc4	kostel
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
strhnout	strhnout	k5eAaPmF	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Dočasnou	dočasný	k2eAgFnSc4d1	dočasná
funkci	funkce	k1gFnSc4	funkce
košického	košický	k2eAgInSc2d1	košický
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
převzal	převzít	k5eAaPmAgInS	převzít
paralelně	paralelně	k6eAd1	paralelně
budovaný	budovaný	k2eAgInSc1d1	budovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
dokončený	dokončený	k2eAgInSc1d1	dokončený
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
kaplí	kaple	k1gFnSc7	kaple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgFnSc1	třetí
etapa	etapa	k1gFnSc1	etapa
výstavby	výstavba	k1gFnSc2	výstavba
1440	[number]	k4	1440
<g/>
–	–	k?	–
<g/>
1462	[number]	k4	1462
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zbourání	zbourání	k1gNnSc6	zbourání
starého	starý	k2eAgInSc2d1	starý
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
zaklenutý	zaklenutý	k2eAgInSc1d1	zaklenutý
hvězdicovou	hvězdicový	k2eAgFnSc7d1	hvězdicová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
symetrické	symetrický	k2eAgInPc1d1	symetrický
útvary	útvar	k1gInPc1	útvar
klenbových	klenbový	k2eAgFnPc2d1	klenbová
polí	pole	k1gFnPc2	pole
byly	být	k5eAaImAgFnP	být
navzájem	navzájem	k6eAd1	navzájem
odlišné	odlišný	k2eAgFnPc1d1	odlišná
a	a	k8xC	a
nepřekrývaly	překrývat	k5eNaImAgFnP	překrývat
se	se	k3xPyFc4	se
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
pole	pole	k1gNnSc2	pole
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
stavební	stavební	k2eAgFnSc2d1	stavební
fáze	fáze	k1gFnSc2	fáze
spadá	spadat	k5eAaPmIp3nS	spadat
i	i	k9	i
půdorysně	půdorysně	k6eAd1	půdorysně
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
část	část	k1gFnSc4	část
–	–	k?	–
svatyně	svatyně	k1gFnSc2	svatyně
a	a	k8xC	a
sakristie	sakristie	k1gFnSc2	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Dokončovala	dokončovat	k5eAaImAgFnS	dokončovat
se	se	k3xPyFc4	se
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1453	[number]	k4	1453
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
pátém	pátý	k4xOgNnSc6	pátý
poschodí	poschodí	k1gNnSc6	poschodí
nechali	nechat	k5eAaPmAgMnP	nechat
vytesat	vytesat	k5eAaPmF	vytesat
nově	nově	k6eAd1	nově
udělený	udělený	k2eAgInSc1d1	udělený
a	a	k8xC	a
polepšený	polepšený	k2eAgInSc1d1	polepšený
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
od	od	k7c2	od
krále	král	k1gMnSc4	král
Ladislava	Ladislav	k1gMnSc2	Ladislav
Pohrobka	pohrobek	k1gMnSc2	pohrobek
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1462	[number]	k4	1462
vytesaný	vytesaný	k2eAgInSc4d1	vytesaný
nad	nad	k7c7	nad
vstupním	vstupní	k2eAgInSc7d1	vstupní
portálem	portál	k1gInSc7	portál
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
ukončení	ukončení	k1gNnSc2	ukončení
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
etapa	etapa	k1gFnSc1	etapa
výstavby	výstavba	k1gFnSc2	výstavba
1462	[number]	k4	1462
<g/>
–	–	k?	–
<g/>
1490	[number]	k4	1490
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
Zikmundovy	Zikmundův	k2eAgFnSc2d1	Zikmundova
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
veškerá	veškerý	k3xTgFnSc1	veškerý
pozornost	pozornost	k1gFnSc1	pozornost
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
dostavbu	dostavba	k1gFnSc4	dostavba
jižní	jižní	k2eAgFnSc2d1	jižní
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
pojmenování	pojmenování	k1gNnSc4	pojmenování
po	po	k7c6	po
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
vladaři	vladař	k1gMnSc6	vladař
a	a	k8xC	a
přispěvateli	přispěvatel	k1gMnPc7	přispěvatel
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
věže	věž	k1gFnSc2	věž
Matyáši	Matyáš	k1gMnSc3	Matyáš
Korvínovi	Korvín	k1gMnSc3	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
stavěna	stavit	k5eAaImNgFnS	stavit
zdobněji	zdobně	k6eAd2	zdobně
a	a	k8xC	a
vertikálněji	vertikálně	k6eAd2	vertikálně
než	než	k8xS	než
severní	severní	k2eAgFnSc1d1	severní
věž	věž	k1gFnSc1	věž
chrámu	chrámat	k5eAaImIp1nS	chrámat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
stavební	stavební	k2eAgFnPc4d1	stavební
hutě	huť	k1gFnPc4	huť
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dokončoval	dokončovat	k5eAaImAgInS	dokončovat
jižní	jižní	k2eAgInSc1d1	jižní
štít	štít	k1gInSc1	štít
a	a	k8xC	a
portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
patrno	patrn	k2eAgNnSc1d1	patrno
mnoho	mnoho	k4c4	mnoho
prvků	prvek	k1gInPc2	prvek
připomínajících	připomínající	k2eAgFnPc2d1	připomínající
Matyášovu	Matyášův	k2eAgFnSc4d1	Matyášova
donační	donační	k2eAgFnSc4d1	donační
štědrost	štědrost	k1gFnSc4	štědrost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1464	[number]	k4	1464
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
chrámu	chrámat	k5eAaImIp1nS	chrámat
kameník	kameník	k1gMnSc1	kameník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jako	jako	k9	jako
mistr	mistr	k1gMnSc1	mistr
Stephan	Stephan	k1gMnSc1	Stephan
Lapicidus	Lapicidus	k1gMnSc1	Lapicidus
či	či	k8xC	či
Maister	Maister	k1gMnSc1	Maister
Steffen	Steffna	k1gFnPc2	Steffna
Staimecz	Staimecz	k1gMnSc1	Staimecz
werkmaister	werkmaister	k1gMnSc1	werkmaister
zu	zu	k?	zu
Khassaw	Khassaw	k1gMnSc1	Khassaw
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
chráněncem	chráněnec	k1gMnSc7	chráněnec
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
dílny	dílna	k1gFnPc4	dílna
s	s	k7c7	s
vlastními	vlastní	k2eAgMnPc7d1	vlastní
řemeslníky	řemeslník	k1gMnPc7	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vynikající	vynikající	k2eAgFnSc1d1	vynikající
pověst	pověst	k1gFnSc1	pověst
mu	on	k3xPp3gMnSc3	on
zaručila	zaručit	k5eAaPmAgFnS	zaručit
i	i	k9	i
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
nového	nový	k2eAgInSc2d1	nový
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Bardejově	Bardejov	k1gInSc6	Bardejov
v	v	k7c6	v
letech	let	k1gInPc6	let
1464	[number]	k4	1464
<g/>
–	–	k?	–
<g/>
1465	[number]	k4	1465
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Štěpán	Štěpán	k1gMnSc1	Štěpán
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
v	v	k7c6	v
košickém	košický	k2eAgInSc6d1	košický
chrámu	chrám	k1gInSc6	chrám
boční	boční	k2eAgFnSc2d1	boční
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
původně	původně	k6eAd1	původně
na	na	k7c6	na
plánech	plán	k1gInPc6	plán
nefigurovaly	figurovat	k5eNaImAgFnP	figurovat
<g/>
.	.	kIx.	.
</s>
<s>
Financovaly	financovat	k5eAaBmAgFnP	financovat
je	on	k3xPp3gNnSc4	on
bohaté	bohatý	k2eAgFnPc1d1	bohatá
měšťanské	měšťanský	k2eAgFnPc1d1	měšťanská
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1475	[number]	k4	1475
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
kaple	kaple	k1gFnSc1	kaple
Svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
senátorem	senátor	k1gMnSc7	senátor
Augustem	August	k1gMnSc7	August
Cromerem	Cromer	k1gMnSc7	Cromer
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1477	[number]	k4	1477
kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
rodem	rod	k1gInSc7	rod
Satmáryovců	Satmáryovec	k1gInPc2	Satmáryovec
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
stejného	stejný	k2eAgNnSc2d1	stejné
století	století	k1gNnSc2	století
i	i	k8xC	i
dne	den	k1gInSc2	den
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc1d1	neexistující
kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
zařízení	zařízení	k1gNnSc2	zařízení
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
mistra	mistr	k1gMnSc2	mistr
Štěpána	Štěpán	k1gMnSc2	Štěpán
kamenné	kamenný	k2eAgNnSc4d1	kamenné
pastoforium	pastoforium	k1gNnSc4	pastoforium
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
reliéf	reliéf	k1gInSc1	reliéf
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
sakristie	sakristie	k1gFnSc2	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
etapa	etapa	k1gFnSc1	etapa
výstavby	výstavba	k1gFnSc2	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
vlády	vláda	k1gFnSc2	vláda
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přálo	přát	k5eAaImAgNnS	přát
rozvoji	rozvoj	k1gInSc6	rozvoj
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
zařízen	zařídit	k5eAaPmNgInS	zařídit
bohatým	bohatý	k2eAgInSc7d1	bohatý
gotickým	gotický	k2eAgInSc7d1	gotický
mobiliářem	mobiliář	k1gInSc7	mobiliář
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
mnoho	mnoho	k6eAd1	mnoho
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Zachoval	Zachoval	k1gMnSc1	Zachoval
se	se	k3xPyFc4	se
však	však	k9	však
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1474	[number]	k4	1474
<g/>
–	–	k?	–
<g/>
1477	[number]	k4	1477
od	od	k7c2	od
neznámého	známý	k2eNgMnSc2d1	neznámý
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ukončení	ukončení	k1gNnSc1	ukončení
výstavby	výstavba	k1gFnSc2	výstavba
1491	[number]	k4	1491
<g/>
–	–	k?	–
<g/>
1508	[number]	k4	1508
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
nastaly	nastat	k5eAaPmAgInP	nastat
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
boje	boj	k1gInSc2	boj
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
polsko-litevský	polskoitevský	k2eAgMnSc1d1	polsko-litevský
regent	regent	k1gMnSc1	regent
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
Olbracht	Olbracht	k1gMnSc1	Olbracht
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1491	[number]	k4	1491
obléhal	obléhat	k5eAaImAgMnS	obléhat
Košice	Košice	k1gInPc4	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
dělostřelecky	dělostřelecky	k6eAd1	dělostřelecky
ostřelovány	ostřelován	k2eAgFnPc1d1	ostřelována
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
zůstal	zůstat	k5eAaPmAgInS	zůstat
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
obnovou	obnova	k1gFnSc7	obnova
byl	být	k5eAaImAgInS	být
pověřený	pověřený	k2eAgMnSc1d1	pověřený
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Krumpholz	Krumpholz	k1gMnSc1	Krumpholz
z	z	k7c2	z
Nisy	Nisa	k1gFnSc2	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Asistoval	asistovat	k5eAaImAgMnS	asistovat
mu	on	k3xPp3gMnSc3	on
přizvaný	přizvaný	k2eAgMnSc1d1	přizvaný
stavitel	stavitel	k1gMnSc1	stavitel
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
nápisů	nápis	k1gInPc2	nápis
na	na	k7c6	na
římse	římsa	k1gFnSc6	římsa
západního	západní	k2eAgNnSc2d1	západní
průčelí	průčelí	k1gNnSc2	průčelí
spadají	spadat	k5eAaImIp3nP	spadat
opravné	opravný	k2eAgFnPc1d1	opravná
práce	práce	k1gFnPc1	práce
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1496	[number]	k4	1496
<g/>
–	–	k?	–
<g/>
1498	[number]	k4	1498
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1496	[number]	k4	1496
<g/>
–	–	k?	–
<g/>
1497	[number]	k4	1497
opravili	opravit	k5eAaPmAgMnP	opravit
Zikmundovu	Zikmundův	k2eAgFnSc4d1	Zikmundova
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
krov	krov	k1gInSc4	krov
a	a	k8xC	a
štíty	štít	k1gInPc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
dostala	dostat	k5eAaPmAgFnS	dostat
navíc	navíc	k6eAd1	navíc
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jejich	jejich	k3xOp3gNnSc7	jejich
vedením	vedení	k1gNnSc7	vedení
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
i	i	k9	i
presbyterium	presbyterium	k1gNnSc1	presbyterium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1508	[number]	k4	1508
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
ukončení	ukončení	k1gNnSc4	ukončení
výstavby	výstavba	k1gFnSc2	výstavba
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Dokládá	dokládat	k5eAaImIp3nS	dokládat
to	ten	k3xDgNnSc1	ten
pergamenový	pergamenový	k2eAgInSc1d1	pergamenový
svitek	svitek	k1gInSc1	svitek
s	s	k7c7	s
uvedeným	uvedený	k2eAgInSc7d1	uvedený
rokem	rok	k1gInSc7	rok
a	a	k8xC	a
jménem	jméno	k1gNnSc7	jméno
stavitele	stavitel	k1gMnSc2	stavitel
Krumholze	Krumholze	k1gFnSc2	Krumholze
nalezený	nalezený	k2eAgInSc1d1	nalezený
v	v	k7c6	v
pilíři	pilíř	k1gInSc6	pilíř
presbyteria	presbyterium	k1gNnSc2	presbyterium
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
reformace	reformace	k1gFnSc2	reformace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
zachvátil	zachvátit	k5eAaPmAgMnS	zachvátit
Košice	Košice	k1gInPc1	Košice
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poškodil	poškodit	k5eAaPmAgInS	poškodit
i	i	k9	i
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Shořela	shořet	k5eAaPmAgFnS	shořet
střecha	střecha	k1gFnSc1	střecha
chrámu	chrám	k1gInSc2	chrám
i	i	k9	i
s	s	k7c7	s
krovy	krov	k1gInPc7	krov
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potřebné	potřebný	k2eAgFnPc4d1	potřebná
opravy	oprava	k1gFnPc4	oprava
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
stavitel	stavitel	k1gMnSc1	stavitel
Stanislav	Stanislav	k1gMnSc1	Stanislav
z	z	k7c2	z
Krakova	Krakov	k1gInSc2	Krakov
<g/>
,	,	kIx,	,
tesařští	tesařský	k2eAgMnPc1d1	tesařský
mistři	mistr	k1gMnPc1	mistr
Juraj	Juraj	k1gMnSc1	Juraj
a	a	k8xC	a
Gebriel	Gebriel	k1gMnSc1	Gebriel
a	a	k8xC	a
kameník	kameník	k1gMnSc1	kameník
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
připadl	připadnout	k5eAaPmAgInS	připadnout
chrám	chrám	k1gInSc1	chrám
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jej	on	k3xPp3gNnSc4	on
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
násilně	násilně	k6eAd1	násilně
obsadili	obsadit	k5eAaPmAgMnP	obsadit
katolíci	katolík	k1gMnPc1	katolík
a	a	k8xC	a
egerská	egerský	k2eAgFnSc1d1	egerský
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
našla	najít	k5eAaPmAgFnS	najít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
útočiště	útočiště	k1gNnSc2	útočiště
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
<g/>
.	.	kIx.	.
</s>
<s>
Incident	incident	k1gInSc1	incident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rozbušek	rozbuška	k1gFnPc2	rozbuška
následného	následný	k2eAgNnSc2d1	následné
protihabsburského	protihabsburský	k2eAgNnSc2d1	protihabsburské
povstání	povstání	k1gNnSc2	povstání
Štěpána	Štěpán	k1gMnSc2	Štěpán
Bočkaje	Bočkaje	k1gMnSc2	Bočkaje
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
chrám	chrám	k1gInSc4	chrám
Kalvínům	Kalvín	k1gMnPc3	Kalvín
<g/>
.	.	kIx.	.
</s>
<s>
Egerské	Egerský	k2eAgFnSc3d1	Egerská
kapitule	kapitula	k1gFnSc3	kapitula
se	se	k3xPyFc4	se
chrám	chrám	k1gInSc1	chrám
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1671	[number]	k4	1671
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
Tehdy	tehdy	k6eAd1	tehdy
vykonali	vykonat	k5eAaPmAgMnP	vykonat
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
nevyhnutelné	vyhnutelný	k2eNgFnPc4d1	nevyhnutelná
opravy	oprava	k1gFnPc4	oprava
a	a	k8xC	a
umístili	umístit	k5eAaPmAgMnP	umístit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
kapitulní	kapitulní	k2eAgFnSc2d1	kapitulní
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
povstání	povstání	k1gNnPc2	povstání
Imricha	Imrich	k1gMnSc2	Imrich
Tököliho	Tököli	k1gMnSc2	Tököli
v	v	k7c6	v
letech	let	k1gInPc6	let
1682	[number]	k4	1682
<g/>
–	–	k?	–
<g/>
1685	[number]	k4	1685
přešel	přejít	k5eAaPmAgInS	přejít
chrám	chrám	k1gInSc1	chrám
opět	opět	k6eAd1	opět
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zikmundově	Zikmundův	k2eAgFnSc6d1	Zikmundova
věži	věž	k1gFnSc6	věž
nahradili	nahradit	k5eAaPmAgMnP	nahradit
katolický	katolický	k2eAgInSc4d1	katolický
kříž	kříž	k1gInSc4	kříž
pozlaceným	pozlacený	k2eAgInSc7d1	pozlacený
kohoutem	kohout	k1gInSc7	kohout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1685	[number]	k4	1685
se	se	k3xPyFc4	se
chrám	chrám	k1gInSc1	chrám
dostal	dostat	k5eAaPmAgInS	dostat
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Egerská	Egerský	k2eAgFnSc1d1	Egerská
kapitula	kapitula	k1gFnSc1	kapitula
byla	být	k5eAaImAgFnS	být
zmocněna	zmocněn	k2eAgFnSc1d1	zmocněna
potrestat	potrestat	k5eAaPmF	potrestat
ty	ten	k3xDgMnPc4	ten
košické	košický	k2eAgMnPc4d1	košický
měšťany	měšťan	k1gMnPc4	měšťan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
povstání	povstání	k1gNnSc2	povstání
oloupili	oloupit	k5eAaPmAgMnP	oloupit
katedrální	katedrální	k2eAgFnSc4d1	katedrální
pokladnici	pokladnice	k1gFnSc4	pokladnice
o	o	k7c4	o
monstrance	monstrance	k1gFnPc4	monstrance
<g/>
,	,	kIx,	,
kalichy	kalich	k1gInPc4	kalich
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
baroka	baroko	k1gNnSc2	baroko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1706	[number]	k4	1706
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
poškozen	poškodit	k5eAaPmNgInS	poškodit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
obléhání	obléhání	k1gNnSc2	obléhání
Františkem	František	k1gMnSc7	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákóczim	Rákóczim	k6eAd1	Rákóczim
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	hodně	k6eAd3	hodně
poškozena	poškozen	k2eAgFnSc1d1	poškozena
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc1	jeho
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
města	město	k1gNnSc2	město
nechal	nechat	k5eAaPmAgInS	nechat
Rákóczi	Rákócze	k1gFnSc4	Rákócze
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
držbě	držba	k1gFnSc6	držba
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
průběžně	průběžně	k6eAd1	průběžně
opravovaly	opravovat	k5eAaImAgFnP	opravovat
a	a	k8xC	a
zkrášlovaly	zkrášlovat	k5eAaImAgFnP	zkrášlovat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měl	mít	k5eAaImAgInS	mít
kostel	kostel	k1gInSc1	kostel
14	[number]	k4	14
oltářů	oltář	k1gInPc2	oltář
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
současnými	současný	k2eAgInPc7d1	současný
deseti	deset	k4xCc2	deset
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barokně-rokokovou	Barokněokokový	k2eAgFnSc4d1	Barokně-rokokový
helmici	helmice	k1gFnSc4	helmice
s	s	k7c7	s
vyhlídkovým	vyhlídkový	k2eAgInSc7d1	vyhlídkový
ochozem	ochoz	k1gInSc7	ochoz
získala	získat	k5eAaPmAgFnS	získat
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fábryho	Fábry	k1gMnSc4	Fábry
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
století	století	k1gNnSc6	století
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
zanedbané	zanedbaný	k2eAgFnSc2d1	zanedbaná
údržby	údržba	k1gFnSc2	údržba
byla	být	k5eAaImAgFnS	být
obnova	obnova	k1gFnSc1	obnova
chrámu	chrám	k1gInSc2	chrám
potřebná	potřebný	k2eAgFnSc1d1	potřebná
už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
postihlo	postihnout	k5eAaPmAgNnS	postihnout
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
bylo	být	k5eAaImAgNnS	být
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
povodeň	povodeň	k1gFnSc1	povodeň
sahala	sahat	k5eAaImAgFnS	sahat
až	až	k9	až
k	k	k7c3	k
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
dlažba	dlažba	k1gFnSc1	dlažba
se	se	k3xPyFc4	se
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
novogotického	novogotický	k2eAgNnSc2d1	novogotické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
šířícího	šířící	k2eAgMnSc2d1	šířící
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dostavbou	dostavba	k1gFnSc7	dostavba
katedrály	katedrála	k1gFnSc2	katedrála
německém	německý	k2eAgNnSc6d1	německé
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
převzali	převzít	k5eAaPmAgMnP	převzít
iniciativu	iniciativa	k1gFnSc4	iniciativa
nad	nad	k7c7	nad
celkovou	celkový	k2eAgFnSc7d1	celková
obnovou	obnova	k1gFnSc7	obnova
kostela	kostel	k1gInSc2	kostel
biskup	biskup	k1gMnSc1	biskup
Ignác	Ignác	k1gMnSc1	Ignác
Fábry	Fábra	k1gFnSc2	Fábra
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
uměnovědy	uměnověda	k1gFnSc2	uměnověda
a	a	k8xC	a
památkové	památkový	k2eAgFnSc2d1	památková
obnovy	obnova	k1gFnSc2	obnova
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
Imrich	Imrich	k1gMnSc1	Imrich
Henszlmann	Henszlmann	k1gMnSc1	Henszlmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Spolek	spolek	k1gInSc1	spolek
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
členové	člen	k1gMnPc1	člen
přispívali	přispívat	k5eAaImAgMnP	přispívat
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
letech	let	k1gInPc6	let
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Císařsko-královské	císařskorálovský	k2eAgFnSc2d1	císařsko-královská
centrální	centrální	k2eAgFnSc2d1	centrální
komise	komise	k1gFnSc2	komise
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
obnovu	obnova	k1gFnSc4	obnova
stavebních	stavební	k2eAgFnPc2d1	stavební
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
asistoval	asistovat	k5eAaImAgMnS	asistovat
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
sám	sám	k3xTgMnSc1	sám
Henszlmann	Henszlmann	k1gMnSc1	Henszlmann
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
vedli	vést	k5eAaImAgMnP	vést
stavitelé	stavitel	k1gMnPc1	stavitel
Karol	Karola	k1gFnPc2	Karola
Gerster	Gerstrum	k1gNnPc2	Gerstrum
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Frey	Frea	k1gFnSc2	Frea
<g/>
.	.	kIx.	.
</s>
<s>
Fábryho	Fábryze	k6eAd1	Fábryze
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
se	se	k3xPyFc4	se
dotkla	dotknout	k5eAaPmAgFnS	dotknout
výměny	výměna	k1gFnPc4	výměna
některých	některý	k3yIgFnPc2	některý
portálových	portálový	k2eAgFnPc2d1	portálová
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
výměny	výměna	k1gFnSc2	výměna
šindelů	šindel	k1gInPc2	šindel
za	za	k7c4	za
keramické	keramický	k2eAgFnPc4d1	keramická
tašky	taška	k1gFnPc4	taška
<g/>
,	,	kIx,	,
osazení	osazení	k1gNnSc4	osazení
nových	nový	k2eAgFnPc2d1	nová
vitráží	vitráž	k1gFnPc2	vitráž
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc4	oprava
jižní	jižní	k2eAgFnSc2d1	jižní
předsíně	předsíň	k1gFnSc2	předsíň
a	a	k8xC	a
romantizující	romantizující	k2eAgFnSc2d1	romantizující
výmalby	výmalba	k1gFnSc2	výmalba
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
neodstranila	odstranit	k5eNaPmAgFnS	odstranit
důležité	důležitý	k2eAgNnSc4d1	důležité
statické	statický	k2eAgNnSc4d1	statické
narušení	narušení	k1gNnSc4	narušení
konstrukce	konstrukce	k1gFnSc2	konstrukce
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
čase	čas	k1gInSc6	čas
už	už	k6eAd1	už
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pilíře	pilíř	k1gInPc1	pilíř
byly	být	k5eAaImAgInP	být
vychýlené	vychýlený	k2eAgInPc4d1	vychýlený
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
základy	základ	k1gInPc1	základ
stály	stát	k5eAaImAgInP	stát
na	na	k7c6	na
vrstvě	vrstva	k1gFnSc6	vrstva
štěrku	štěrk	k1gInSc2	štěrk
prosáknutého	prosáknutý	k2eAgInSc2d1	prosáknutý
spodní	spodní	k2eAgNnSc4d1	spodní
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Praskliny	prasklina	k1gFnPc1	prasklina
v	v	k7c6	v
klenbách	klenba	k1gFnPc6	klenba
a	a	k8xC	a
na	na	k7c6	na
žebrech	žebr	k1gInPc6	žebr
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
zakryli	zakrýt	k5eAaPmAgMnP	zakrýt
maltou	malta	k1gFnSc7	malta
nebo	nebo	k8xC	nebo
obložili	obložit	k5eAaPmAgMnP	obložit
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Neodborný	odborný	k2eNgInSc1d1	neodborný
postup	postup	k1gInSc1	postup
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
vichřicích	vichřice	k1gFnPc6	vichřice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
zůstala	zůstat	k5eAaPmAgFnS	zůstat
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Klenbovému	klenbový	k2eAgInSc3d1	klenbový
systému	systém	k1gInSc3	systém
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zřícení	zřícení	k1gNnSc1	zřícení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
neblahý	blahý	k2eNgInSc4d1	neblahý
stav	stav	k1gInSc4	stav
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
upozorňoval	upozorňovat	k5eAaImAgMnS	upozorňovat
uherskou	uherský	k2eAgFnSc4d1	uherská
vládu	vláda	k1gFnSc4	vláda
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
biskup	biskup	k1gMnSc1	biskup
Ján	Ján	k1gMnSc1	Ján
Perger	Perger	k1gMnSc1	Perger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Uherská	uherský	k2eAgFnSc1d1	uherská
dočasná	dočasný	k2eAgFnSc1d1	dočasná
památková	památkový	k2eAgFnSc1d1	památková
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
funkci	funkce	k1gFnSc4	funkce
tajemníka	tajemník	k1gMnSc2	tajemník
zastával	zastávat	k5eAaImAgMnS	zastávat
Košičan	Košičan	k1gMnSc1	Košičan
Imrich	Imrich	k1gMnSc1	Imrich
Henszlmann	Henszlmann	k1gMnSc1	Henszlmann
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
chrámu	chrámat	k5eAaImIp1nS	chrámat
realizovaná	realizovaný	k2eAgFnSc1d1	realizovaná
v	v	k7c6	v
letech	let	k1gInPc6	let
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
památkové	památkový	k2eAgFnSc2d1	památková
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
financovaná	financovaný	k2eAgFnSc1d1	financovaná
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
uherskou	uherský	k2eAgFnSc7d1	uherská
vládou	vláda	k1gFnSc7	vláda
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
projektant	projektant	k1gMnSc1	projektant
prací	práce	k1gFnPc2	práce
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
profesor	profesor	k1gMnSc1	profesor
středověké	středověký	k2eAgFnSc2d1	středověká
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
Budapešťské	budapešťský	k2eAgFnSc6d1	Budapešťská
technické	technický	k2eAgFnSc6d1	technická
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
architekt	architekt	k1gMnSc1	architekt
uherské	uherský	k2eAgFnSc2d1	uherská
neogotiky	neogotika	k1gFnSc2	neogotika
<g/>
,	,	kIx,	,
Imrich	Imrich	k1gMnSc1	Imrich
Steindl	Steindl	k1gMnSc1	Steindl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
prasklin	prasklina	k1gFnPc2	prasklina
v	v	k7c6	v
klenbách	klenba	k1gFnPc6	klenba
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozložení	rozložení	k1gNnSc1	rozložení
pilířů	pilíř	k1gInPc2	pilíř
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
havarijního	havarijní	k2eAgInSc2d1	havarijní
stavu	stav	k1gInSc2	stav
statiky	statika	k1gFnSc2	statika
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
úplně	úplně	k6eAd1	úplně
novou	nový	k2eAgFnSc4d1	nová
puristickou	puristický	k2eAgFnSc4d1	puristická
projekci	projekce	k1gFnSc4	projekce
přestavby	přestavba	k1gFnSc2	přestavba
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přebudování	přebudování	k1gNnSc3	přebudování
třílodní	třílodní	k2eAgFnSc2d1	třílodní
katedrály	katedrála	k1gFnSc2	katedrála
na	na	k7c4	na
katedrálu	katedrála	k1gFnSc4	katedrála
pětilodní	pětilodní	k2eAgNnSc1d1	pětilodní
přidáním	přidání	k1gNnSc7	přidání
další	další	k2eAgFnSc2d1	další
řady	řada	k1gFnSc2	řada
pilířů	pilíř	k1gInPc2	pilíř
a	a	k8xC	a
arkád	arkáda	k1gFnPc2	arkáda
v	v	k7c6	v
bočních	boční	k2eAgFnPc6d1	boční
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
originální	originální	k2eAgFnSc2d1	originální
středověké	středověký	k2eAgFnSc2d1	středověká
hvězdicové	hvězdicový	k2eAgFnSc2d1	hvězdicová
klenby	klenba	k1gFnSc2	klenba
jak	jak	k8xC	jak
hlavních	hlavní	k2eAgInPc2d1	hlavní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
bočních	boční	k2eAgFnPc2d1	boční
lodí	loď	k1gFnPc2	loď
byly	být	k5eAaImAgFnP	být
přebudovány	přebudován	k2eAgFnPc1d1	přebudována
na	na	k7c4	na
síťové	síťový	k2eAgNnSc4d1	síťové
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
chór	chór	k1gInSc1	chór
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
Steindl	Steindl	k1gMnSc1	Steindl
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
jeho	jeho	k3xOp3gFnSc4	jeho
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
repliku	replika	k1gFnSc4	replika
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
pilířů	pilíř	k1gInPc2	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
zásahy	zásah	k1gInPc1	zásah
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
i	i	k9	i
exteriéru	exteriér	k1gInSc2	exteriér
–	–	k?	–
úprava	úprava	k1gFnSc1	úprava
obvodových	obvodový	k2eAgFnPc2d1	obvodová
zdí	zeď	k1gFnPc2	zeď
a	a	k8xC	a
štítů	štít	k1gInPc2	štít
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
opěrných	opěrný	k2eAgInPc2d1	opěrný
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,	,
chrličů	chrlič	k1gInPc2	chrlič
<g/>
,	,	kIx,	,
říms	římsa	k1gFnPc2	římsa
a	a	k8xC	a
okenních	okenní	k2eAgFnPc2d1	okenní
kružeb	kružba	k1gFnPc2	kružba
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
plastiková	plastikový	k2eAgFnSc1d1	plastiková
výzdoba	výzdoba	k1gFnSc1	výzdoba
portálů	portál	k1gInPc2	portál
a	a	k8xC	a
vyměněna	vyměněn	k2eAgFnSc1d1	vyměněna
stříška	stříška	k1gFnSc1	stříška
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
jako	jako	k8xS	jako
slohově	slohově	k6eAd1	slohově
nečistá	čistý	k2eNgFnSc1d1	nečistá
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
byla	být	k5eAaImAgFnS	být
novogotická	novogotický	k2eAgFnSc1d1	novogotická
věžička	věžička	k1gFnSc1	věžička
na	na	k7c6	na
křížení	křížení	k1gNnSc6	křížení
hlavní	hlavní	k2eAgFnPc1d1	hlavní
a	a	k8xC	a
příčné	příčný	k2eAgFnPc1d1	příčná
lodě	loď	k1gFnPc1	loď
(	(	kIx(	(
<g/>
sanktusník	sanktusník	k1gInSc1	sanktusník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původním	původní	k2eAgInSc7d1	původní
Steindlovým	Steindlový	k2eAgInSc7d1	Steindlový
plánem	plán	k1gInSc7	plán
bylo	být	k5eAaImAgNnS	být
přestavět	přestavět	k5eAaPmF	přestavět
všechny	všechen	k3xTgFnPc4	všechen
negotické	gotický	k2eNgFnPc4d1	gotický
přístavby	přístavba	k1gFnPc4	přístavba
a	a	k8xC	a
prvky	prvek	k1gInPc4	prvek
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
nahradit	nahradit	k5eAaPmF	nahradit
rokokovou	rokokový	k2eAgFnSc4d1	rokoková
helmici	helmice	k1gFnSc4	helmice
Zikmundovy	Zikmundův	k2eAgFnSc2d1	Zikmundova
věže	věž	k1gFnSc2	věž
osmihranným	osmihranný	k2eAgInSc7d1	osmihranný
jehlanem	jehlan	k1gInSc7	jehlan
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
kamenickou	kamenický	k2eAgFnSc7d1	kamenická
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Počítal	počítat	k5eAaImAgMnS	počítat
i	i	k9	i
s	s	k7c7	s
dostavbou	dostavba	k1gFnSc7	dostavba
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
objemných	objemný	k2eAgFnPc2d1	objemná
katedrálních	katedrální	k2eAgFnPc2d1	katedrální
věží	věž	k1gFnPc2	věž
německé	německý	k2eAgFnSc2d1	německá
neogotiky	neogotika	k1gFnSc2	neogotika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
záměry	záměr	k1gInPc1	záměr
však	však	k8xC	však
památková	památkový	k2eAgFnSc1d1	památková
komise	komise	k1gFnSc1	komise
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
a	a	k8xC	a
požadovala	požadovat	k5eAaImAgFnS	požadovat
co	co	k9	co
nejúspornější	úsporný	k2eAgFnSc4d3	nejúspornější
výměnu	výměna	k1gFnSc4	výměna
starých	starý	k2eAgFnPc2d1	stará
kamenných	kamenný	k2eAgFnPc2d1	kamenná
částí	část	k1gFnPc2	část
za	za	k7c4	za
nové	nový	k2eAgFnPc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Regotizace	regotizace	k1gFnSc1	regotizace
věží	věž	k1gFnPc2	věž
se	se	k3xPyFc4	se
nerealizovala	realizovat	k5eNaBmAgFnS	realizovat
i	i	k9	i
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
co	co	k9	co
nejlacinějšího	laciný	k2eAgInSc2d3	nejlacinější
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výměně	výměna	k1gFnSc6	výměna
opěrného	opěrný	k2eAgInSc2d1	opěrný
systému	systém	k1gInSc2	systém
svatyně	svatyně	k1gFnSc2	svatyně
v	v	k7c6	v
letech	let	k1gInPc6	let
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc1d1	použit
laciný	laciný	k2eAgInSc1d1	laciný
ale	ale	k8xC	ale
nekvalitní	kvalitní	k2eNgInSc1d1	nekvalitní
pískovec	pískovec	k1gInSc1	pískovec
z	z	k7c2	z
kamenolomu	kamenolom	k1gInSc2	kamenolom
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Spišské	spišský	k2eAgInPc4d1	spišský
Vlachy	Vlachy	k1gInPc4	Vlachy
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rychle	rychle	k6eAd1	rychle
zvětrávající	zvětrávající	k2eAgInSc1d1	zvětrávající
povrch	povrch	k1gInSc1	povrch
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
fiály	fiála	k1gFnPc1	fiála
a	a	k8xC	a
chrliče	chrlič	k1gInPc1	chrlič
ze	z	k7c2	z
svatyně	svatyně	k1gFnSc2	svatyně
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
chodce	chodec	k1gMnPc4	chodec
na	na	k7c4	na
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
dalších	další	k2eAgFnPc6d1	další
přestavbách	přestavba	k1gFnPc6	přestavba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
už	už	k6eAd1	už
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
pískovec	pískovec	k1gInSc1	pískovec
z	z	k7c2	z
Králiků	Králik	k1gMnPc2	Králik
u	u	k7c2	u
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
.	.	kIx.	.
<g/>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
stavbyvedoucím	stavbyvedoucí	k1gMnSc7	stavbyvedoucí
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
Jozef	Jozef	k1gMnSc1	Jozef
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
i	i	k9	i
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
regotizaci	regotizace	k1gFnSc4	regotizace
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
v	v	k7c6	v
Bardejově	Bardejov	k1gInSc6	Bardejov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
stavbyvedoucím	stavbyvedoucí	k1gMnSc7	stavbyvedoucí
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
stavitel	stavitel	k1gMnSc1	stavitel
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Fröde	Fröd	k1gMnSc5	Fröd
<g/>
.	.	kIx.	.
</s>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
stavbou	stavba	k1gFnSc7	stavba
měl	mít	k5eAaImAgInS	mít
rakouský	rakouský	k2eAgMnSc1d1	rakouský
architekt	architekt	k1gMnSc1	architekt
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
novogoticky	novogoticky	k6eAd1	novogoticky
upravoval	upravovat	k5eAaImAgInS	upravovat
chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
hrad	hrad	k1gInSc1	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
co	co	k3yRnSc4	co
slavný	slavný	k2eAgMnSc1d1	slavný
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
Schmidtův	Schmidtův	k2eAgMnSc1d1	Schmidtův
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
Imrich	Imrich	k1gMnSc1	Imrich
Steindl	Steindl	k1gMnSc1	Steindl
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
budapešťského	budapešťský	k2eAgInSc2d1	budapešťský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
Otto	Otto	k1gMnSc1	Otto
Sztehló	Sztehló	k1gMnSc1	Sztehló
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
architekt	architekt	k1gMnSc1	architekt
už	už	k6eAd1	už
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
konzervační	konzervační	k2eAgFnSc4d1	konzervační
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
puristické	puristický	k2eAgFnSc2d1	puristická
metody	metoda	k1gFnSc2	metoda
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
novogotických	novogotický	k2eAgFnPc2d1	novogotická
přestaveb	přestavba	k1gFnPc2	přestavba
byla	být	k5eAaImAgFnS	být
uchráněna	uchráněn	k2eAgFnSc1d1	uchráněna
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
Matyášově	Matyášův	k2eAgFnSc6d1	Matyášova
věž	věž	k1gFnSc4	věž
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
stříšky	stříška	k1gFnSc2	stříška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
strany	strana	k1gFnPc1	strana
obvodových	obvodový	k2eAgFnPc2d1	obvodová
zdí	zeď	k1gFnPc2	zeď
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgInPc4d1	středověký
portály	portál	k1gInPc4	portál
s	s	k7c7	s
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
,	,	kIx,	,
kamenný	kamenný	k2eAgInSc1d1	kamenný
inventář	inventář	k1gInSc1	inventář
interiéru	interiér	k1gInSc2	interiér
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
svatyně	svatyně	k1gFnSc1	svatyně
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
interiérová	interiérový	k2eAgFnSc1d1	interiérová
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
posvěcení	posvěcení	k1gNnSc1	posvěcení
(	(	kIx(	(
<g/>
konsekrace	konsekrace	k1gFnSc1	konsekrace
<g/>
)	)	kIx)	)
nově	nově	k6eAd1	nově
zrekonstruovaného	zrekonstruovaný	k2eAgInSc2d1	zrekonstruovaný
dómu	dóm	k1gInSc2	dóm
egerským	egerský	k2eAgMnSc7d1	egerský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Josefem	Josef	k1gMnSc7	Josef
Samassem	Samass	k1gMnSc7	Samass
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
oslav	oslava	k1gFnPc2	oslava
milénia	milénium	k1gNnSc2	milénium
od	od	k7c2	od
příchodu	příchod	k1gInSc2	příchod
Maďarů	Maďar	k1gMnPc2	Maďar
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
i	i	k9	i
početné	početný	k2eAgNnSc1d1	početné
nově	nově	k6eAd1	nově
dodané	dodaný	k2eAgNnSc1d1	dodané
neogotické	ogotický	k2eNgNnSc1d1	neogotické
zařízení	zařízení	k1gNnSc1	zařízení
chrámu	chrám	k1gInSc2	chrám
(	(	kIx(	(
<g/>
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
pro	pro	k7c4	pro
košickou	košický	k2eAgFnSc4d1	Košická
katedrálu	katedrála	k1gFnSc4	katedrála
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
darem	dar	k1gInSc7	dar
uherský	uherský	k2eAgInSc4d1	uherský
klérus	klérus	k1gInSc4	klérus
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
košický	košický	k2eAgMnSc1d1	košický
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
mecenáš	mecenáš	k1gMnSc1	mecenáš
Zikmund	Zikmund	k1gMnSc1	Zikmund
Bubics	Bubics	k1gInSc4	Bubics
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
postavili	postavit	k5eAaPmAgMnP	postavit
pod	pod	k7c7	pod
severní	severní	k2eAgFnSc7d1	severní
boční	boční	k2eAgFnSc7d1	boční
lodí	loď	k1gFnSc7	loď
novou	nový	k2eAgFnSc4d1	nová
katedrální	katedrální	k2eAgFnSc4d1	katedrální
kryptu	krypta	k1gFnSc4	krypta
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Frigyesa	Frigyesa	k1gFnSc1	Frigyesa
Schuleka	Schuleka	k1gFnSc1	Schuleka
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
přenesených	přenesený	k2eAgInPc2d1	přenesený
ostatků	ostatek	k1gInPc2	ostatek
knížete	kníže	k1gMnSc2	kníže
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
společníků	společník	k1gMnPc2	společník
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
1978	[number]	k4	1978
–	–	k?	–
dodnes	dodnes	k6eAd1	dodnes
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
chrámu	chrám	k1gInSc2	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
následnou	následný	k2eAgFnSc4d1	následná
památkovou	památkový	k2eAgFnSc4d1	památková
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
stav	stav	k1gInSc1	stav
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
po	po	k7c6	po
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
,	,	kIx,	,
právě	právě	k9	právě
pro	pro	k7c4	pro
nekvalitní	kvalitní	k2eNgInSc4d1	nekvalitní
pískovec	pískovec	k1gInSc4	pískovec
použitý	použitý	k2eAgInSc4d1	použitý
při	při	k7c6	při
výměně	výměna	k1gFnSc6	výměna
kamenných	kamenný	k2eAgInPc2d1	kamenný
článků	článek	k1gInPc2	článek
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
presbyteria	presbyterium	k1gNnSc2	presbyterium
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
profilované	profilovaný	k2eAgInPc4d1	profilovaný
architektonické	architektonický	k2eAgInPc4d1	architektonický
útvary	útvar	k1gInPc4	útvar
(	(	kIx(	(
<g/>
fiály	fiála	k1gFnPc4	fiála
<g/>
,	,	kIx,	,
chrliče	chrlič	k1gInPc4	chrlič
<g/>
,	,	kIx,	,
římsa	římsa	k1gFnSc1	římsa
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
buď	buď	k8xC	buď
zničené	zničený	k2eAgNnSc1d1	zničené
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
demontované	demontovaný	k2eAgNnSc1d1	demontované
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
značně	značně	k6eAd1	značně
zvětralém	zvětralý	k2eAgInSc6d1	zvětralý
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
kamenická	kamenický	k2eAgFnSc1d1	kamenická
výzdoba	výzdoba	k1gFnSc1	výzdoba
severního	severní	k2eAgInSc2d1	severní
portálu	portál	k1gInSc2	portál
<g/>
.	.	kIx.	.
<g/>
Rekonstrukční	rekonstrukční	k2eAgFnSc2d1	rekonstrukční
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
chrámu	chrám	k1gInSc6	chrám
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
1978	[number]	k4	1978
podrobnou	podrobný	k2eAgFnSc7d1	podrobná
architektonickou	architektonický	k2eAgFnSc7d1	architektonická
dokumentací	dokumentace	k1gFnSc7	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
přestávce	přestávka	k1gFnSc6	přestávka
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozběhly	rozběhnout	k5eAaPmAgInP	rozběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
úřady	úřad	k1gInPc1	úřad
začaly	začít	k5eAaPmAgInP	začít
s	s	k7c7	s
komplexnějším	komplexní	k2eAgNnSc7d2	komplexnější
řešením	řešení	k1gNnSc7	řešení
památkové	památkový	k2eAgFnSc2d1	památková
obnovy	obnova	k1gFnSc2	obnova
centra	centrum	k1gNnSc2	centrum
Košic	Košice	k1gInPc2	Košice
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
největší	veliký	k2eAgFnSc7d3	veliký
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
přispívajícím	přispívající	k2eAgInSc7d1	přispívající
k	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
starostlivosti	starostlivost	k1gFnSc2	starostlivost
i	i	k8xC	i
chrám	chrám	k1gInSc4	chrám
bylo	být	k5eAaImAgNnS	být
vyloučení	vyloučení	k1gNnSc1	vyloučení
automobilové	automobilový	k2eAgFnSc2d1	automobilová
dopravy	doprava	k1gFnSc2	doprava
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
ulice	ulice	k1gFnSc2	ulice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
a	a	k8xC	a
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
opravách	oprava	k1gFnPc6	oprava
chrámu	chrámat	k5eAaImIp1nS	chrámat
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgInSc4d1	zvolen
postup	postup	k1gInSc4	postup
metody	metoda	k1gFnSc2	metoda
zachování	zachování	k1gNnSc4	zachování
stavu	stav	k1gInSc2	stav
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byla	být	k5eAaImAgFnS	být
opravována	opravován	k2eAgFnSc1d1	opravována
střecha	střecha	k1gFnSc1	střecha
hlavní	hlavní	k2eAgFnSc2d1	hlavní
a	a	k8xC	a
příčné	příčný	k2eAgFnSc2d1	příčná
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
vyměněny	vyměněn	k2eAgFnPc1d1	vyměněna
keramické	keramický	k2eAgFnPc1d1	keramická
barevně	barevně	k6eAd1	barevně
glazované	glazovaný	k2eAgFnPc1d1	glazovaná
tašky	taška	k1gFnPc1	taška
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
původního	původní	k2eAgInSc2d1	původní
vzoru	vzor	k1gInSc2	vzor
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruován	rekonstruovat	k5eAaBmNgInS	rekonstruovat
byl	být	k5eAaImAgInS	být
sanktusník	sanktusník	k1gInSc1	sanktusník
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
doplnit	doplnit	k5eAaPmF	doplnit
264	[number]	k4	264
kusů	kus	k1gInPc2	kus
klempířských	klempířský	k2eAgFnPc2d1	klempířská
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rekonstrukční	rekonstrukční	k2eAgFnPc1d1	rekonstrukční
práce	práce	k1gFnPc1	práce
budou	být	k5eAaImBp3nP	být
stát	stát	k5eAaImF	stát
čtvrt	čtvrt	k1gFnSc4	čtvrt
miliardy	miliarda	k4xCgFnPc4	miliarda
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
potrvají	potrvat	k5eAaPmIp3nP	potrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
nejpoškozenější	poškozený	k2eAgFnSc1d3	poškozený
strana	strana	k1gFnSc1	strana
svatyně	svatyně	k1gFnSc2	svatyně
a	a	k8xC	a
sakristie	sakristie	k1gFnSc2	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
polské	polský	k2eAgFnPc1d1	polská
firmě	firma	k1gFnSc3	firma
Pracownie	Pracownie	k1gFnSc2	Pracownie
Konserwacji	Konserwacje	k1gFnSc4	Konserwacje
Zabytkow	Zabytkow	k1gMnSc2	Zabytkow
z	z	k7c2	z
Vratislavi	Vratislav	k1gFnSc2	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
nové	nový	k2eAgInPc4d1	nový
korunní	korunní	k2eAgInPc4d1	korunní
římsy	říms	k1gInPc4	říms
<g/>
,	,	kIx,	,
chrliče	chrlič	k1gInPc4	chrlič
a	a	k8xC	a
fiály	fiála	k1gFnPc4	fiála
–	–	k?	–
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
pramenných	pramenný	k2eAgInPc2d1	pramenný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
projektové	projektový	k2eAgFnSc2d1	projektová
dokumentace	dokumentace	k1gFnSc2	dokumentace
z	z	k7c2	z
Archivu	archiv	k1gInSc2	archiv
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byly	být	k5eAaImAgFnP	být
zhotoveny	zhotoven	k2eAgFnPc1d1	zhotovena
i	i	k8xC	i
schodištní	schodištní	k2eAgFnPc1d1	schodištní
věžičky	věžička	k1gFnPc1	věžička
na	na	k7c6	na
styku	styk	k1gInSc6	styk
lodě	loď	k1gFnSc2	loď
se	se	k3xPyFc4	se
svatyní	svatyně	k1gFnPc2	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obnově	obnova	k1gFnSc6	obnova
chrličů	chrlič	k1gInPc2	chrlič
se	se	k3xPyFc4	se
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
originálních	originální	k2eAgInPc2d1	originální
středověkých	středověký	k2eAgInPc2d1	středověký
vzorů	vzor	k1gInPc2	vzor
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
v	v	k7c6	v
muzeálních	muzeální	k2eAgFnPc6d1	muzeální
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
i	i	k9	i
opravu	oprava	k1gFnSc4	oprava
interiéru	interiér	k1gInSc2	interiér
svatyně	svatyně	k1gFnSc2	svatyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
se	se	k3xPyFc4	se
rekonstruovala	rekonstruovat	k5eAaBmAgFnS	rekonstruovat
jižní	jižní	k2eAgFnSc1d1	jižní
fasáda	fasáda	k1gFnSc1	fasáda
(	(	kIx(	(
<g/>
čištění	čištění	k1gNnSc1	čištění
a	a	k8xC	a
konzervace	konzervace	k1gFnSc1	konzervace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vitráží	vitráž	k1gFnPc2	vitráž
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
fasády	fasáda	k1gFnSc2	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Důslednou	důsledný	k2eAgFnSc7d1	důsledná
obnovou	obnova	k1gFnSc7	obnova
prošla	projít	k5eAaPmAgFnS	projít
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
měděná	měděný	k2eAgFnSc1d1	měděná
rokoková	rokokový	k2eAgFnSc1d1	rokoková
helmice	helmice	k1gFnSc1	helmice
<g/>
.	.	kIx.	.
</s>
<s>
Očištěné	očištěný	k2eAgFnPc1d1	očištěná
pozlacené	pozlacený	k2eAgFnPc1d1	pozlacená
klempířské	klempířský	k2eAgFnPc1d1	klempířská
ozdoby	ozdoba	k1gFnPc1	ozdoba
jsou	být	k5eAaImIp3nP	být
originály	originál	k1gInPc4	originál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
krypty	krypta	k1gFnSc2	krypta
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
nejvzácnějšího	vzácný	k2eAgInSc2d3	nejvzácnější
severního	severní	k2eAgInSc2d1	severní
portálu	portál	k1gInSc2	portál
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
probíhají	probíhat	k5eAaImIp3nP	probíhat
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
exteriéru	exteriér	k1gInSc6	exteriér
severní	severní	k2eAgFnSc2d1	severní
fasády	fasáda	k1gFnSc2	fasáda
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
západního	západní	k2eAgInSc2d1	západní
portálu	portál	k1gInSc2	portál
a	a	k8xC	a
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
křížové	křížový	k2eAgFnSc2d1	křížová
severní	severní	k2eAgFnSc2d1	severní
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
čeká	čekat	k5eAaImIp3nS	čekat
Matyášova	Matyášův	k2eAgFnSc1d1	Matyášova
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interiér	interiér	k1gInSc1	interiér
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Oltáře	Oltář	k1gInPc4	Oltář
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
====	====	k?	====
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
oltář	oltář	k1gInSc1	oltář
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1474	[number]	k4	1474
až	až	k9	až
1477	[number]	k4	1477
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
památky	památka	k1gFnPc4	památka
středověkého	středověký	k2eAgNnSc2d1	středověké
umění	umění	k1gNnSc2	umění
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
centrální	centrální	k2eAgFnSc3d1	centrální
části	část	k1gFnSc3	část
jsou	být	k5eAaImIp3nP	být
připojeny	připojen	k2eAgInPc1d1	připojen
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
oboustranně	oboustranně	k6eAd1	oboustranně
zdobených	zdobený	k2eAgNnPc2d1	zdobené
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc4	šest
gotických	gotický	k2eAgFnPc2d1	gotická
maleb	malba	k1gFnPc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
48	[number]	k4	48
maleb	malba	k1gFnPc2	malba
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
tematických	tematický	k2eAgInPc6d1	tematický
cyklech	cyklus	k1gInPc6	cyklus
–	–	k?	–
alžbětínský	alžbětínský	k2eAgInSc1d1	alžbětínský
<g/>
,	,	kIx,	,
pašijový	pašijový	k2eAgInSc1d1	pašijový
a	a	k8xC	a
adventní	adventní	k2eAgInSc1d1	adventní
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
konstrukce	konstrukce	k1gFnSc1	konstrukce
oltáře	oltář	k1gInSc2	oltář
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
měřítku	měřítko	k1gNnSc6	měřítko
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc4	Oltář
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
====	====	k?	====
</s>
</p>
<p>
<s>
Křídlový	křídlový	k2eAgInSc1d1	křídlový
oltář	oltář	k1gInSc1	oltář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
dal	dát	k5eAaPmAgMnS	dát
vyhotovit	vyhotovit	k5eAaPmF	vyhotovit
bohatý	bohatý	k2eAgMnSc1d1	bohatý
košický	košický	k2eAgMnSc1d1	košický
kupec	kupec	k1gMnSc1	kupec
Michal	Michal	k1gMnSc1	Michal
Günthert	Günthert	k1gMnSc1	Günthert
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
motivem	motiv	k1gInSc7	motiv
oltáře	oltář	k1gInSc2	oltář
je	být	k5eAaImIp3nS	být
sousoší	sousoší	k1gNnSc1	sousoší
setkání	setkání	k1gNnSc2	setkání
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
arše	archa	k1gFnSc6	archa
<g/>
.	.	kIx.	.
</s>
<s>
Oltář	Oltář	k1gInSc1	Oltář
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k4xCyI	pár
pohyblivých	pohyblivý	k2eAgNnPc2d1	pohyblivé
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
scény	scéna	k1gFnPc1	scéna
<g/>
:	:	kIx,	:
Andělské	andělský	k2eAgNnSc1d1	andělské
pozdravení	pozdravení	k1gNnSc1	pozdravení
<g/>
,	,	kIx,	,
Narození	narození	k1gNnSc1	narození
<g/>
,	,	kIx,	,
Klanění	klanění	k1gNnSc1	klanění
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
a	a	k8xC	a
útěk	útěk	k1gInSc4	útěk
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
zavřené	zavřený	k2eAgFnPc1d1	zavřená
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
postavy	postava	k1gFnPc1	postava
svatých	svatá	k1gFnPc2	svatá
<g/>
:	:	kIx,	:
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Alexandrijskou	alexandrijský	k2eAgFnSc4d1	Alexandrijská
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
Barvoru	Barvor	k1gInSc2	Barvor
a	a	k8xC	a
apoštola	apoštol	k1gMnSc2	apoštol
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Predella	predella	k1gFnSc1	predella
je	být	k5eAaImIp3nS	být
vymalována	vymalovat	k5eAaPmNgFnS	vymalovat
motivy	motiv	k1gInPc7	motiv
<g/>
:	:	kIx,	:
Vir	vir	k1gInSc1	vir
dolorum	dolorum	k1gInSc1	dolorum
<g/>
,	,	kIx,	,
Panna	Panna	k1gFnSc1	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgMnSc1d1	svatý
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
Antiochijská	antiochijský	k2eAgFnSc1d1	Antiochijská
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
svatí	svatý	k1gMnPc1	svatý
byli	být	k5eAaImAgMnP	být
zároveň	zároveň	k6eAd1	zároveň
patrony	patrona	k1gFnPc4	patrona
mecenášské	mecenášský	k2eAgFnSc2d1	mecenášská
rodiny	rodina	k1gFnSc2	rodina
Günthertů	Günthert	k1gMnPc2	Günthert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
je	být	k5eAaImIp3nS	být
nástavec	nástavec	k1gInSc1	nástavec
oltáře	oltář	k1gInSc2	oltář
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
původně	původně	k6eAd1	původně
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc1d1	gotický
<g/>
.	.	kIx.	.
</s>
<s>
Zdobí	zdobit	k5eAaImIp3nP	zdobit
jej	on	k3xPp3gInSc4	on
tři	tři	k4xCgFnPc1	tři
sousoší	sousoší	k1gNnSc4	sousoší
–	–	k?	–
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
manželstvích	manželství	k1gNnPc6	manželství
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
plastiky	plastika	k1gFnPc1	plastika
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
plastika	plastika	k1gFnSc1	plastika
Madony	Madona	k1gFnSc2	Madona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc1	Oltář
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
====	====	k?	====
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
tohoto	tento	k3xDgInSc2	tento
oltáře	oltář	k1gInSc2	oltář
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
dvou	dva	k4xCgInPc2	dva
pozdně	pozdně	k6eAd1	pozdně
gotických	gotický	k2eAgInPc2d1	gotický
oltářů	oltář	k1gInPc2	oltář
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zachránily	zachránit	k5eAaPmAgFnP	zachránit
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
<g/>
.	.	kIx.	.
</s>
<s>
Malby	malba	k1gFnPc1	malba
křídel	křídlo	k1gNnPc2	křídlo
jsou	být	k5eAaImIp3nP	být
nejstarší	starý	k2eAgFnPc4d3	nejstarší
datované	datovaný	k2eAgFnPc4d1	datovaná
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
oltářních	oltářní	k2eAgFnPc2d1	oltářní
maleb	malba	k1gFnPc2	malba
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
16	[number]	k4	16
svatých	svatý	k1gMnPc2	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
archy	archa	k1gFnSc2	archa
umístili	umístit	k5eAaPmAgMnP	umístit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
oltářní	oltářní	k2eAgInSc1d1	oltářní
obraz	obraz	k1gInSc1	obraz
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
od	od	k7c2	od
košického	košický	k2eAgMnSc2d1	košický
malíře	malíř	k1gMnSc2	malíř
Františka	František	k1gMnSc2	František
Klimkoviče	Klimkovič	k1gMnSc2	Klimkovič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc1	Oltář
Mettercie	Mettercie	k1gFnSc2	Mettercie
====	====	k?	====
</s>
</p>
<p>
<s>
Neogotický	Neogotický	k2eAgInSc1d1	Neogotický
oltář	oltář	k1gInSc1	oltář
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vyhotoven	vyhotovit	k5eAaPmNgMnS	vyhotovit
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
košického	košický	k2eAgMnSc2d1	košický
biskupa	biskup	k1gMnSc2	biskup
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Bubicse	Bubics	k1gMnSc2	Bubics
pro	pro	k7c4	pro
umístění	umístění	k1gNnSc4	umístění
nově	nově	k6eAd1	nově
nalezeného	nalezený	k2eAgInSc2d1	nalezený
pozdně	pozdně	k6eAd1	pozdně
gotického	gotický	k2eAgInSc2d1	gotický
obrazu	obraz	k1gInSc2	obraz
Mettercie	Mettercie	k1gFnSc2	Mettercie
<g/>
.	.	kIx.	.
</s>
<s>
Vzácná	vzácný	k2eAgFnSc1d1	vzácná
malba	malba	k1gFnSc1	malba
tyrolské	tyrolský	k2eAgFnSc2d1	tyrolská
provenience	provenience	k1gFnSc2	provenience
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
Annu	Anna	k1gFnSc4	Anna
Samotřetí	Samotřetí	k1gNnSc2	Samotřetí
a	a	k8xC	a
rodokmen	rodokmen	k1gInSc1	rodokmen
starozákonního	starozákonní	k2eAgMnSc2d1	starozákonní
Izajáše	Izajáš	k1gMnSc2	Izajáš
<g/>
.	.	kIx.	.
</s>
<s>
Malbu	malba	k1gFnSc4	malba
nechala	nechat	k5eAaPmAgFnS	nechat
vyhotovit	vyhotovit	k5eAaPmF	vyhotovit
rodina	rodina	k1gFnSc1	rodina
lékárníka	lékárník	k1gMnSc2	lékárník
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
Czottmana	Czottman	k1gMnSc2	Czottman
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
lékárník	lékárník	k1gMnSc1	lékárník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
malbě	malba	k1gFnSc6	malba
vyobrazeni	vyobrazit	k5eAaPmNgMnP	vyobrazit
i	i	k8xC	i
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
erby	erb	k1gInPc7	erb
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgInPc7	který
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
znak	znak	k1gInSc1	znak
Košic	Košice	k1gInPc2	Košice
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
poslední	poslední	k2eAgFnSc2d1	poslední
erbovní	erbovní	k2eAgFnSc2d1	erbovní
listiny	listina	k1gFnSc2	listina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městským	městský	k2eAgInSc7d1	městský
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
moždíř	moždíř	k1gInSc1	moždíř
jako	jako	k8xC	jako
znak	znak	k1gInSc1	znak
lékárníků	lékárník	k1gMnPc2	lékárník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc4	Oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
====	====	k?	====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
série	série	k1gFnSc2	série
neogotických	ogotický	k2eNgInPc2d1	neogotický
oltářů	oltář	k1gInPc2	oltář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
zakoupených	zakoupený	k2eAgFnPc2d1	zakoupená
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
oslav	oslava	k1gFnPc2	oslava
milénia	milénium	k1gNnSc2	milénium
od	od	k7c2	od
příchodů	příchod	k1gInPc2	příchod
Maďarů	Maďar	k1gMnPc2	Maďar
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ukončení	ukončení	k1gNnSc1	ukončení
restauračních	restaurační	k2eAgFnPc2d1	restaurační
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Oltář	Oltář	k1gInSc1	Oltář
je	být	k5eAaImIp3nS	být
darem	dar	k1gInSc7	dar
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
mecenáše	mecenáš	k1gMnSc2	mecenáš
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Bubicse	Bubics	k1gMnSc2	Bubics
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc4	Oltář
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
====	====	k?	====
</s>
</p>
<p>
<s>
Dar	dar	k1gInSc1	dar
biskupa	biskup	k1gMnSc2	biskup
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Bubicse	Bubics	k1gMnSc2	Bubics
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Zakoupen	zakoupen	k2eAgMnSc1d1	zakoupen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc1	Oltář
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
====	====	k?	====
</s>
</p>
<p>
<s>
Dar	dar	k1gInSc1	dar
vácovského	vácovský	k2eAgMnSc2d1	vácovský
biskupa	biskup	k1gMnSc2	biskup
Konstantina	Konstantin	k1gMnSc2	Konstantin
Schustera	Schuster	k1gMnSc2	Schuster
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
jej	on	k3xPp3gNnSc4	on
piešťanský	piešťanský	k2eAgMnSc1d1	piešťanský
sochař	sochař	k1gMnSc1	sochař
Ľudovít	Ľudovít	k1gFnSc2	Ľudovít
Lantay	Lantaa	k1gFnSc2	Lantaa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
byly	být	k5eAaImAgFnP	být
zakoupeny	zakoupit	k5eAaPmNgFnP	zakoupit
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc1	Oltář
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
I.	I.	kA	I.
====	====	k?	====
</s>
</p>
<p>
<s>
Dar	dar	k1gInSc1	dar
košického	košický	k2eAgMnSc2d1	košický
kanovníka	kanovník	k1gMnSc2	kanovník
Františka	František	k1gMnSc2	František
Pagáče	Pagáče	k?	Pagáče
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
jej	on	k3xPp3gNnSc4	on
Tyrolský	tyrolský	k2eAgMnSc1d1	tyrolský
řezbář	řezbář	k1gMnSc1	řezbář
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Stufflesser	Stufflesser	k1gMnSc1	Stufflesser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc4	Oltář
Třech	tři	k4xCgMnPc2	tři
košických	košický	k2eAgMnPc2d1	košický
mučedníků	mučedník	k1gMnPc2	mučedník
====	====	k?	====
</s>
</p>
<p>
<s>
Svatí	svatý	k2eAgMnPc1d1	svatý
košičtí	košický	k2eAgMnPc1d1	košický
mučedníci	mučedník	k1gMnPc1	mučedník
byli	být	k5eAaImAgMnP	být
blahoslaveni	blahoslavit	k5eAaImNgMnP	blahoslavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
oltář	oltář	k1gInSc4	oltář
vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
Ľudovít	Ľudovít	k1gFnSc2	Ľudovít
Tihanyi	Tihany	k1gFnSc2	Tihany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
skříni	skříň	k1gFnSc6	skříň
na	na	k7c6	na
predele	predela	k1gFnSc6	predela
oltáře	oltář	k1gInSc2	oltář
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
ostatky	ostatek	k1gInPc1	ostatek
těchto	tento	k3xDgMnPc2	tento
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oltář	Oltář	k1gInSc1	Oltář
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
====	====	k?	====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nejmladším	mladý	k2eAgInSc7d3	nejmladší
oltářem	oltář	k1gInSc7	oltář
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
Buchnerem	Buchner	k1gMnSc7	Buchner
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
obětí	oběť	k1gFnPc2	oběť
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
železné	železný	k2eAgFnPc1d1	železná
tabule	tabule	k1gFnPc1	tabule
u	u	k7c2	u
oltáře	oltář	k1gInSc2	oltář
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc1	jméno
všech	všecek	k3xTgFnPc2	všecek
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přispěly	přispět	k5eAaPmAgFnP	přispět
na	na	k7c4	na
zhotovení	zhotovení	k1gNnSc4	zhotovení
oltáře	oltář	k1gInSc2	oltář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Neinstalované	instalovaný	k2eNgInPc4d1	instalovaný
oltáře	oltář	k1gInPc4	oltář
====	====	k?	====
</s>
</p>
<p>
<s>
Oltář	Oltář	k1gInSc1	Oltář
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
Páně	páně	k2eAgFnSc2d1	páně
–	–	k?	–
triptychový	triptychový	k2eAgInSc4d1	triptychový
oltář	oltář	k1gInSc4	oltář
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
rozkouskován	rozkouskovat	k5eAaPmNgInS	rozkouskovat
v	v	k7c6	v
depozitáři	depozitář	k1gInSc6	depozitář
Východoslovenského	východoslovenský	k2eAgNnSc2d1	Východoslovenské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oltář	Oltář	k1gInSc1	Oltář
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
–	–	k?	–
neogotický	ogotický	k2eNgInSc1d1	neogotický
oltář	oltář	k1gInSc1	oltář
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
pro	pro	k7c4	pro
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
<g/>
,	,	kIx,	,
olejovou	olejový	k2eAgFnSc7d1	olejová
temperou	tempera	k1gFnSc7	tempera
malovanou	malovaný	k2eAgFnSc7d1	malovaná
<g/>
,	,	kIx,	,
tabuli	tabule	k1gFnSc6	tabule
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
letech	let	k1gInPc6	let
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
tabuli	tabule	k1gFnSc6	tabule
získalo	získat	k5eAaPmAgNnS	získat
Východoslovenské	východoslovenský	k2eAgNnSc1d1	Východoslovenské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
tabule	tabule	k1gFnSc1	tabule
je	být	k5eAaImIp3nS	být
výjev	výjev	k1gInSc4	výjev
Kristova	Kristův	k2eAgInSc2d1	Kristův
křtu	křest	k1gInSc2	křest
v	v	k7c6	v
Jordánu	Jordán	k1gInSc6	Jordán
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Mučení	mučení	k1gNnSc4	mučení
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mimořádně	mimořádně	k6eAd1	mimořádně
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
malby	malba	k1gFnPc4	malba
s	s	k7c7	s
citelným	citelný	k2eAgInSc7d1	citelný
vlivem	vliv	k1gInSc7	vliv
nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
renesančního	renesanční	k2eAgInSc2d1	renesanční
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oltář	Oltář	k1gInSc1	Oltář
smrti	smrt	k1gFnSc2	smrt
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
–	–	k?	–
původní	původní	k2eAgInSc1d1	původní
gotický	gotický	k2eAgInSc1d1	gotický
oltář	oltář	k1gInSc1	oltář
nebyl	být	k5eNaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
instalován	instalovat	k5eAaBmNgInS	instalovat
<g/>
,	,	kIx,	,
zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
predella	predella	k1gFnSc1	predella
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
ztracená	ztracený	k2eAgFnSc1d1	ztracená
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
skladu	sklad	k1gInSc2	sklad
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
===	===	k?	===
</s>
</p>
<p>
<s>
Románsko	Románsko	k6eAd1	Románsko
–	–	k?	–
gotická	gotický	k2eAgFnSc1d1	gotická
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
památkou	památka	k1gFnSc7	památka
zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ještě	ještě	k9	ještě
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
dnešního	dnešní	k2eAgInSc2d1	dnešní
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Noha	noha	k1gFnSc1	noha
křtitelnice	křtitelnice	k1gFnSc2	křtitelnice
je	být	k5eAaImIp3nS	být
dekorovaná	dekorovaný	k2eAgFnSc1d1	dekorovaná
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
<g/>
,	,	kIx,	,
kalich	kalich	k1gInSc1	kalich
zdobí	zdobit	k5eAaImIp3nS	zdobit
zoomorfní	zoomorfní	k2eAgInSc1d1	zoomorfní
reliéf	reliéf	k1gInSc1	reliéf
s	s	k7c7	s
lvy	lev	k1gMnPc7	lev
<g/>
,	,	kIx,	,
gryfy	gryf	k1gMnPc7	gryf
a	a	k8xC	a
orly	orel	k1gMnPc7	orel
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInPc4d1	horní
okraje	okraj	k1gInPc4	okraj
je	být	k5eAaImIp3nS	být
lemován	lemovat	k5eAaImNgInS	lemovat
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nečitelným	čitelný	k2eNgInSc7d1	nečitelný
latinským	latinský	k2eAgInSc7d1	latinský
nápisem	nápis	k1gInSc7	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Vrchník	vrchník	k1gMnSc1	vrchník
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nástěnné	nástěnný	k2eAgFnPc4d1	nástěnná
fresky	freska	k1gFnPc4	freska
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalit	k5eAaPmNgNnS	odhalit
několik	několik	k4yIc1	několik
původních	původní	k2eAgFnPc2d1	původní
gotických	gotický	k2eAgFnPc2d1	gotická
fresek	freska	k1gFnPc2	freska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
reformace	reformace	k1gFnSc2	reformace
skryty	skryt	k2eAgFnPc4d1	skryta
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
omítky	omítka	k1gFnSc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
apsidě	apsida	k1gFnSc6	apsida
<g/>
:	:	kIx,	:
Spasitel	spasitel	k1gMnSc1	spasitel
v	v	k7c6	v
mandorle	mandorl	k1gInSc6	mandorl
u	u	k7c2	u
posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
sedící	sedící	k2eAgMnSc1d1	sedící
Kristus	Kristus	k1gMnSc1	Kristus
držící	držící	k2eAgInSc4d1	držící
meč	meč	k1gInSc4	meč
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
Panna	Panna	k1gFnSc1	Panna
Marie	Marie	k1gFnSc1	Marie
a	a	k8xC	a
svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dvanáct	dvanáct	k4xCc1	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Krista	Kristus	k1gMnSc2	Kristus
(	(	kIx(	(
<g/>
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
držící	držící	k2eAgInSc4d1	držící
prapor	prapor	k1gInSc4	prapor
<g/>
,	,	kIx,	,
levou	levá	k1gFnSc4	levá
žehnající	žehnající	k2eAgMnSc1d1	žehnající
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
boční	boční	k2eAgFnSc6d1	boční
apsidě	apsida	k1gFnSc6	apsida
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
sakristie	sakristie	k1gFnSc2	sakristie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgInSc1d1	další
soubor	soubor	k1gInSc1	soubor
původních	původní	k2eAgFnPc2d1	původní
gotických	gotický	k2eAgFnPc2d1	gotická
fresek	freska	k1gFnPc2	freska
<g/>
:	:	kIx,	:
Snímání	snímání	k1gNnSc1	snímání
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xC	jako
nástěnný	nástěnný	k2eAgInSc1d1	nástěnný
křídlový	křídlový	k2eAgInSc1d1	křídlový
oltář	oltář	k1gInSc1	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Vpravo	vpravo	k6eAd1	vpravo
jsou	být	k5eAaImIp3nP	být
malby	malba	k1gFnPc4	malba
<g/>
:	:	kIx,	:
Bičování	bičování	k1gNnSc4	bičování
a	a	k8xC	a
Korunování	korunování	k1gNnSc4	korunování
trnovou	trnový	k2eAgFnSc7d1	Trnová
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
vlevo	vlevo	k6eAd1	vlevo
Přibíjení	přibíjení	k1gNnSc2	přibíjení
na	na	k7c4	na
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
Kristus	Kristus	k1gMnSc1	Kristus
před	před	k7c7	před
Pilátem	Pilát	k1gMnSc7	Pilát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
===	===	k?	===
</s>
</p>
<p>
<s>
Sochařská	sochařský	k2eAgFnSc1d1	sochařská
kompozice	kompozice	k1gFnSc1	kompozice
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInSc4d3	nejstarší
inventář	inventář	k1gInSc4	inventář
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
kříž	kříž	k1gInSc1	kříž
vysoký	vysoký	k2eAgInSc1d1	vysoký
4,34	[number]	k4	4,34
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
ukřižovaný	ukřižovaný	k2eAgMnSc1d1	ukřižovaný
Kristus	Kristus	k1gMnSc1	Kristus
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
3,12	[number]	k4	3,12
m	m	kA	m
<g/>
,	,	kIx,	,
vpravo	vpravo	k6eAd1	vpravo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
vysoká	vysoká	k1gFnSc1	vysoká
2,73	[number]	k4	2,73
m	m	kA	m
a	a	k8xC	a
nalevo	nalevo	k6eAd1	nalevo
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
vysoká	vysoký	k2eAgFnSc1d1	vysoká
2,50	[number]	k4	2,50
m.	m.	k?	m.
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
umístěna	umístit	k5eAaPmNgFnS	umístit
ve	v	k7c6	v
vítězném	vítězný	k2eAgInSc6d1	vítězný
oblouku	oblouk	k1gInSc6	oblouk
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
do	do	k7c2	do
královské	královský	k2eAgFnSc2d1	královská
empory	empora	k1gFnSc2	empora
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
monumentální	monumentální	k2eAgNnSc1d1	monumentální
řezbářské	řezbářský	k2eAgNnSc1d1	řezbářské
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
dramatičností	dramatičnost	k1gFnSc7	dramatičnost
emocionálního	emocionální	k2eAgInSc2d1	emocionální
projevu	projev	k1gInSc2	projev
dobové	dobový	k2eAgFnSc2d1	dobová
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtyři	čtyři	k4xCgFnPc1	čtyři
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
gotické	gotický	k2eAgFnPc1d1	gotická
polychromované	polychromovaný	k2eAgFnPc1d1	polychromovaná
plastiky	plastika	k1gFnPc1	plastika
===	===	k?	===
</s>
</p>
<p>
<s>
Slohovému	slohový	k2eAgNnSc3d1	slohové
období	období	k1gNnSc3	období
vzniku	vznik	k1gInSc2	vznik
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
čtyři	čtyři	k4xCgFnPc4	čtyři
dřevořezby	dřevořezba	k1gFnPc4	dřevořezba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
autorství	autorství	k1gNnSc1	autorství
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
prešovskému	prešovský	k2eAgMnSc3d1	prešovský
dřevořezbáři	dřevořezbář	k1gMnSc3	dřevořezbář
Jánu	Ján	k1gMnSc3	Ján
Weyszovi	Weysz	k1gMnSc3	Weysz
<g/>
.	.	kIx.	.
</s>
<s>
Plastiky	plastika	k1gFnPc1	plastika
jsou	být	k5eAaImIp3nP	být
vysoké	vysoká	k1gFnPc1	vysoká
108	[number]	k4	108
<g/>
–	–	k?	–
<g/>
112	[number]	k4	112
cm	cm	kA	cm
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
současné	současný	k2eAgNnSc4d1	současné
umístění	umístění	k1gNnSc4	umístění
je	být	k5eAaImIp3nS	být
sekundární	sekundární	k2eAgMnSc1d1	sekundární
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c6	na
pilířích	pilíř	k1gInPc6	pilíř
chóru	chór	k1gInSc2	chór
západního	západní	k2eAgInSc2d1	západní
portálu	portál	k1gInSc2	portál
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
tvořily	tvořit	k5eAaImAgFnP	tvořit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
součást	součást	k1gFnSc4	součást
bohaté	bohatý	k2eAgFnSc2d1	bohatá
plastické	plastický	k2eAgFnSc2d1	plastická
výzdoby	výzdoba	k1gFnSc2	výzdoba
nedochovaného	dochovaný	k2eNgInSc2d1	nedochovaný
nástavce	nástavec	k1gInSc2	nástavec
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
polychromované	polychromovaný	k2eAgFnPc1d1	polychromovaná
sochy	socha	k1gFnPc1	socha
představují	představovat	k5eAaImIp3nP	představovat
Štěpána	Štěpán	k1gMnSc4	Štěpán
I.	I.	kA	I.
svatého	svatý	k1gMnSc4	svatý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
syna	syn	k1gMnSc4	syn
svatého	svatý	k2eAgMnSc2d1	svatý
Imricha	Imrich	k1gMnSc2	Imrich
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Ladislava	Ladislav	k1gMnSc2	Ladislav
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Stanislava	Stanislav	k1gMnSc2	Stanislav
polského	polský	k2eAgMnSc2d1	polský
biskupa	biskup	k1gMnSc2	biskup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Panna	Panna	k1gFnSc1	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestná	bolestný	k2eAgFnSc1d1	bolestná
na	na	k7c6	na
sloupu	sloup	k1gInSc6	sloup
===	===	k?	===
</s>
</p>
<p>
<s>
polychromovaná	polychromovaný	k2eAgFnSc1d1	polychromovaná
dřevořezba	dřevořezba	k1gFnSc1	dřevořezba
z	z	k7c2	z
období	období	k1gNnSc2	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
112	[number]	k4	112
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
pozdně	pozdně	k6eAd1	pozdně
gotickém	gotický	k2eAgInSc6d1	gotický
točeném	točený	k2eAgInSc6d1	točený
sloupu	sloup	k1gInSc6	sloup
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
hlavici	hlavice	k1gFnSc6	hlavice
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
viditelná	viditelný	k2eAgFnSc1d1	viditelná
kamenická	kamenický	k2eAgFnSc1d1	kamenická
značka	značka	k1gFnSc1	značka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmen	písmeno	k1gNnPc2	písmeno
Y	Y	kA	Y
a	a	k8xC	a
A	A	kA	A
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
břevna	břevno	k1gNnSc2	břevno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
sochou	socha	k1gFnSc7	socha
je	být	k5eAaImIp3nS	být
gotický	gotický	k2eAgInSc1d1	gotický
baldachýn	baldachýn	k1gInSc1	baldachýn
vsunutý	vsunutý	k2eAgInSc1d1	vsunutý
dodatečně	dodatečně	k6eAd1	dodatečně
do	do	k7c2	do
boční	boční	k2eAgFnSc2d1	boční
jižní	jižní	k2eAgFnSc2d1	jižní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Plastika	plastika	k1gFnSc1	plastika
svojí	svůj	k3xOyFgFnSc7	svůj
expresívností	expresívnost	k1gFnSc7	expresívnost
a	a	k8xC	a
uspořádáním	uspořádání	k1gNnSc7	uspořádání
záhybů	záhyb	k1gInPc2	záhyb
roucha	roucho	k1gNnSc2	roucho
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
pozdně	pozdně	k6eAd1	pozdně
gotického	gotický	k2eAgNnSc2d1	gotické
řezbářství	řezbářství	k1gNnSc2	řezbářství
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lucerna	lucerna	k1gFnSc1	lucerna
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
===	===	k?	===
</s>
</p>
<p>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
kamenná	kamenný	k2eAgFnSc1d1	kamenná
věžička	věžička	k1gFnSc1	věžička
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fiály	fiála	k1gFnSc2	fiála
s	s	k7c7	s
výklenkem	výklenek	k1gInSc7	výklenek
pro	pro	k7c4	pro
lucernu	lucerna	k1gFnSc4	lucerna
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
kamenném	kamenný	k2eAgInSc6d1	kamenný
sloupu	sloup	k1gInSc6	sloup
s	s	k7c7	s
točeným	točený	k2eAgInSc7d1	točený
dříkem	dřík	k1gInSc7	dřík
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tympanony	tympanon	k1gInPc1	tympanon
věžiček	věžička	k1gFnPc2	věžička
zdobí	zdobit	k5eAaImIp3nP	zdobit
znaky	znak	k1gInPc1	znak
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc4	Košice
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
Uherského	uherský	k2eAgNnSc2d1	Uherské
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc2d1	český
<g/>
,	,	kIx,	,
a	a	k8xC	a
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
<g/>
.	.	kIx.	.
</s>
<s>
Přítomen	přítomen	k2eAgInSc1d1	přítomen
je	být	k5eAaImIp3nS	být
i	i	k9	i
erb	erb	k1gInSc4	erb
rodu	rod	k1gInSc2	rod
Hunyadi	Hunyad	k1gMnPc1	Hunyad
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
neidentifikovatelný	identifikovatelný	k2eNgInSc1d1	neidentifikovatelný
erb	erb	k1gInSc1	erb
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
lampa	lampa	k1gFnSc1	lampa
osvětlovala	osvětlovat	k5eAaImAgFnS	osvětlovat
prostranství	prostranství	k1gNnSc4	prostranství
před	před	k7c7	před
jižním	jižní	k2eAgInSc7d1	jižní
portálem	portál	k1gInSc7	portál
chrámu	chrámat	k5eAaImIp1nS	chrámat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
okolní	okolní	k2eAgInSc4d1	okolní
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sloužila	sloužit	k5eAaImAgFnS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
pro	pro	k7c4	pro
plynové	plynový	k2eAgNnSc4d1	plynové
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
stěny	stěna	k1gFnSc2	stěna
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chór	chór	k1gInSc4	chór
===	===	k?	===
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgInSc1d1	varhanní
chór	chór	k1gInSc1	chór
byl	být	k5eAaImAgInS	být
kompletně	kompletně	k6eAd1	kompletně
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
chór	chór	k1gInSc1	chór
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
delší	dlouhý	k2eAgFnSc1d2	delší
replikou	replika	k1gFnSc7	replika
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,	,
než	než	k8xS	než
měl	mít	k5eAaImAgInS	mít
originál	originál	k1gInSc1	originál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pilířích	pilíř	k1gInPc6	pilíř
jsou	být	k5eAaImIp3nP	být
druhotně	druhotně	k6eAd1	druhotně
umístěny	umístěn	k2eAgFnPc1d1	umístěna
čtyři	čtyři	k4xCgFnPc1	čtyři
gotické	gotický	k2eAgFnPc1d1	gotická
polychromované	polychromovaný	k2eAgFnPc1d1	polychromovaná
plastiky	plastika	k1gFnPc1	plastika
popsané	popsaný	k2eAgFnPc4d1	popsaná
výše	výše	k1gFnPc4	výše
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnPc4d1	doplněná
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
neogotické	ogotický	k2eNgFnPc4d1	neogotická
plastiky	plastika	k1gFnPc4	plastika
uherských	uherský	k2eAgMnPc2d1	uherský
králů	král	k1gMnPc2	král
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
Roberta	Robert	k1gMnSc2	Robert
<g/>
,	,	kIx,	,
Ludvíka	Ludvík	k1gMnSc2	Ludvík
I.	I.	kA	I.
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
varhany	varhany	k1gFnPc1	varhany
jsou	být	k5eAaImIp3nP	být
dílem	díl	k1gInSc7	díl
mistra	mistr	k1gMnSc2	mistr
Angstera	Angster	k1gMnSc2	Angster
z	z	k7c2	z
Pětikostelí	pětikostelí	k1gNnSc2	pětikostelí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
varhanám	varhany	k1gFnPc3	varhany
vede	vést	k5eAaImIp3nS	vést
přístup	přístup	k1gInSc4	přístup
skrz	skrz	k7c4	skrz
schodiště	schodiště	k1gNnSc4	schodiště
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
chór	chór	k1gInSc1	chór
<g/>
,	,	kIx,	,
po	po	k7c6	po
vybourání	vybourání	k1gNnSc6	vybourání
ponechaný	ponechaný	k2eAgMnSc1d1	ponechaný
jako	jako	k8xC	jako
druhotný	druhotný	k2eAgInSc1d1	druhotný
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
sběratel	sběratel	k1gMnSc1	sběratel
originálních	originální	k2eAgFnPc2d1	originální
středověkých	středověký	k2eAgFnPc2d1	středověká
stavebních	stavební	k2eAgFnPc2d1	stavební
částí	část	k1gFnPc2	část
rakouský	rakouský	k2eAgMnSc1d1	rakouský
hrabě	hrabě	k1gMnSc1	hrabě
Johann	Johann	k1gMnSc1	Johann
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Wilczek	Wilczek	k1gInSc4	Wilczek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
odvézt	odvézt	k5eAaPmF	odvézt
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
právě	právě	k9	právě
romanticky	romanticky	k6eAd1	romanticky
rekonstruovaného	rekonstruovaný	k2eAgInSc2d1	rekonstruovaný
hradu	hrad	k1gInSc2	hrad
Kreuzenstein	Kreuzenstein	k1gInSc1	Kreuzenstein
nedaleko	nedaleko	k7c2	nedaleko
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
do	do	k7c2	do
přechodového	přechodový	k2eAgInSc2d1	přechodový
mostu	most	k1gInSc2	most
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
hradními	hradní	k2eAgNnPc7d1	hradní
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
dostal	dostat	k5eAaPmAgMnS	dostat
také	také	k9	také
pojmenování	pojmenování	k1gNnSc4	pojmenování
Kaschauer	Kaschaura	k1gFnPc2	Kaschaura
Gang	Ganga	k1gFnPc2	Ganga
(	(	kIx(	(
<g/>
Košická	košický	k2eAgFnSc1d1	Košická
chodba	chodba	k1gFnSc1	chodba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pastoforium	Pastoforium	k1gNnSc1	Pastoforium
===	===	k?	===
</s>
</p>
<p>
<s>
Kamenné	kamenný	k2eAgNnSc1d1	kamenné
pastoforium	pastoforium	k1gNnSc1	pastoforium
k	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
eucharistie	eucharistie	k1gFnSc2	eucharistie
u	u	k7c2	u
severního	severní	k2eAgInSc2d1	severní
pilíře	pilíř	k1gInSc2	pilíř
vítězného	vítězný	k2eAgInSc2d1	vítězný
oblouku	oblouk	k1gInSc2	oblouk
je	být	k5eAaImIp3nS	být
nejpreciznějším	precizní	k2eAgNnSc7d3	nejpreciznější
kamenickým	kamenický	k2eAgNnSc7d1	kamenické
dílem	dílo	k1gNnSc7	dílo
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
jej	on	k3xPp3gMnSc4	on
mistr	mistr	k1gMnSc1	mistr
Štěpán	Štěpán	k1gMnSc1	Štěpán
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
<g/>
.	.	kIx.	.
</s>
<s>
Svatostánek	svatostánek	k1gInSc1	svatostánek
na	na	k7c6	na
šestiúhelníkovém	šestiúhelníkový	k2eAgInSc6d1	šestiúhelníkový
půdorysu	půdorys	k1gInSc6	půdorys
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
zdobený	zdobený	k2eAgInSc1d1	zdobený
složitou	složitý	k2eAgFnSc7d1	složitá
kompozicí	kompozice	k1gFnSc7	kompozice
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,	,
říms	římsa	k1gFnPc2	římsa
<g/>
,	,	kIx,	,
vlysů	vlys	k1gInPc2	vlys
<g/>
,	,	kIx,	,
arkád	arkáda	k1gFnPc2	arkáda
a	a	k8xC	a
oblouků	oblouk	k1gInPc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
poschodí	poschodí	k1gNnSc6	poschodí
pastoforia	pastoforium	k1gNnSc2	pastoforium
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zamřížovaný	zamřížovaný	k2eAgInSc1d1	zamřížovaný
výklenek	výklenek	k1gInSc1	výklenek
na	na	k7c6	na
ukládání	ukládání	k1gNnSc6	ukládání
svátosti	svátost	k1gFnSc2	svátost
oltářní	oltářní	k2eAgFnSc2d1	oltářní
<g/>
.	.	kIx.	.
</s>
<s>
Kovaná	kovaný	k2eAgNnPc1d1	kované
dvířka	dvířka	k1gNnPc1	dvířka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zdobí	zdobit	k5eAaImIp3nP	zdobit
je	on	k3xPp3gFnPc4	on
miniatury	miniatura	k1gFnPc4	miniatura
erbů	erb	k1gInPc2	erb
některých	některý	k3yIgMnPc2	některý
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
znaků	znak	k1gInPc2	znak
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgNnSc1d1	drobné
sádrové	sádrový	k2eAgNnSc1d1	sádrové
sousoší	sousoší	k1gNnSc1	sousoší
proroků	prorok	k1gMnPc2	prorok
a	a	k8xC	a
klečících	klečící	k2eAgMnPc2d1	klečící
andělů	anděl	k1gMnPc2	anděl
nahradili	nahradit	k5eAaPmAgMnP	nahradit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
starší	starý	k2eAgInPc4d2	starší
chybějící	chybějící	k2eAgInPc4d1	chybějící
díly	díl	k1gInPc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
pastoforium	pastoforium	k1gNnSc1	pastoforium
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
schody	schod	k1gInPc4	schod
vedoucí	vedoucí	k1gFnSc2	vedoucí
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
poschodí	poschodí	k1gNnSc4	poschodí
ztratily	ztratit	k5eAaPmAgFnP	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reliéf	reliéf	k1gInSc1	reliéf
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
===	===	k?	===
</s>
</p>
<p>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xC	jako
kamenné	kamenný	k2eAgNnSc1d1	kamenné
pastoforium	pastoforium	k1gNnSc1	pastoforium
a	a	k8xC	a
zhotovení	zhotovení	k1gNnSc1	zhotovení
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
rovněž	rovněž	k9	rovněž
mistru	mistr	k1gMnSc3	mistr
Štěpánovi	Štěpán	k1gMnSc3	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
svojí	svojit	k5eAaImIp3nP	svojit
kompozicí	kompozice	k1gFnSc7	kompozice
do	do	k7c2	do
sebe	se	k3xPyFc2	se
jakoby	jakoby	k8xS	jakoby
nezapadaly	zapadat	k5eNaImAgInP	zapadat
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
reliéf	reliéf	k1gInSc1	reliéf
patronky	patronka	k1gFnSc2	patronka
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stroze	stroze	k6eAd1	stroze
zdoben	zdobit	k5eAaImNgInS	zdobit
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
se	s	k7c7	s
šestiúhelníkovým	šestiúhelníkový	k2eAgInSc7d1	šestiúhelníkový
baldachýnem	baldachýn	k1gInSc7	baldachýn
se	s	k7c7	s
svorníkovými	svorníkový	k2eAgNnPc7d1	svorníkový
ukončeními	ukončení	k1gNnPc7	ukončení
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgNnPc6	který
jsou	být	k5eAaImIp3nP	být
filigránské	filigránský	k2eAgInPc1d1	filigránský
výjevy	výjev	k1gInPc1	výjev
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Špice	špice	k1gFnSc1	špice
baldachýnu	baldachýn	k1gInSc2	baldachýn
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
věžové	věžový	k2eAgFnSc2d1	věžová
fiály	fiála	k1gFnSc2	fiála
je	být	k5eAaImIp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
kompozicí	kompozice	k1gFnSc7	kompozice
uhnízděného	uhnízděný	k2eAgMnSc4d1	uhnízděný
pelikána	pelikán	k1gMnSc4	pelikán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
doplnili	doplnit	k5eAaPmAgMnP	doplnit
reliéf	reliéf	k1gInSc4	reliéf
o	o	k7c4	o
pole	pole	k1gNnSc4	pole
s	s	k7c7	s
latinským	latinský	k2eAgInSc7d1	latinský
nápisem	nápis	k1gInSc7	nápis
S.	S.	kA	S.
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
ora	ora	k?	ora
pro	pro	k7c4	pro
nobis	nobis	k1gFnSc4	nobis
<g/>
,	,	kIx,	,
umístěným	umístěný	k2eAgNnSc7d1	umístěné
nad	nad	k7c4	nad
konzolí	konzolí	k1gNnSc4	konzolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epitaf	epitaf	k1gInSc1	epitaf
Reinerů	Reiner	k1gMnPc2	Reiner
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
neogotických	ogotický	k2eNgFnPc2d1	neogotická
památek	památka	k1gFnPc2	památka
zachovaných	zachovaný	k2eAgFnPc2d1	zachovaná
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
patří	patřit	k5eAaImIp3nS	patřit
barokní	barokní	k2eAgInSc1d1	barokní
epitaf	epitaf	k1gInSc1	epitaf
rodiny	rodina	k1gFnSc2	rodina
městského	městský	k2eAgMnSc2d1	městský
rychtáře	rychtář	k1gMnSc2	rychtář
Melichara	Melichar	k1gMnSc2	Melichar
Reinera	Reiner	k1gMnSc2	Reiner
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
rodinnými	rodinný	k2eAgInPc7d1	rodinný
erby	erb	k1gInPc7	erb
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc4	obraz
Bičování	bičování	k1gNnSc2	bičování
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nám	my	k3xPp1nPc3	my
plastika	plastika	k1gFnSc1	plastika
Ukřižovaného	ukřižovaný	k2eAgNnSc2d1	ukřižované
a	a	k8xC	a
nad	nad	k7c7	nad
tympanonem	tympanon	k1gInSc7	tympanon
barokní	barokní	k2eAgNnSc1d1	barokní
sousoší	sousoší	k1gNnSc1	sousoší
dvou	dva	k4xCgMnPc2	dva
andělů	anděl	k1gMnPc2	anděl
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
Krista	Kristus	k1gMnSc2	Kristus
držícího	držící	k2eAgInSc2d1	držící
Zemi	zem	k1gFnSc3	zem
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mandorla	Mandorl	k1gMnSc4	Mandorl
s	s	k7c7	s
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
barokní	barokní	k2eAgFnSc7d1	barokní
památkou	památka	k1gFnSc7	památka
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vhodně	vhodně	k6eAd1	vhodně
umocňuje	umocňovat	k5eAaImIp3nS	umocňovat
estetickou	estetický	k2eAgFnSc4d1	estetická
výzdobu	výzdoba	k1gFnSc4	výzdoba
interiéru	interiér	k1gInSc2	interiér
je	být	k5eAaImIp3nS	být
závěsná	závěsný	k2eAgFnSc1d1	závěsná
mandorla	mandorla	k1gFnSc1	mandorla
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
pod	pod	k7c7	pod
vítězným	vítězný	k2eAgInSc7d1	vítězný
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
pohledy	pohled	k1gInPc4	pohled
(	(	kIx(	(
<g/>
zepředu	zepředu	k6eAd1	zepředu
i	i	k9	i
zezadu	zezadu	k6eAd1	zezadu
<g/>
)	)	kIx)	)
řešené	řešený	k2eAgNnSc1d1	řešené
sousoší	sousoší	k1gNnSc1	sousoší
Madony	Madona	k1gFnSc2	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lavice	lavice	k1gFnSc2	lavice
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
chrámové	chrámový	k2eAgFnPc1d1	chrámová
lavice	lavice	k1gFnPc1	lavice
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jsou	být	k5eAaImIp3nP	být
novější	nový	k2eAgFnPc1d2	novější
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
presbyteriu	presbyterium	k1gNnSc6	presbyterium
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
situované	situovaný	k2eAgFnPc4d1	situovaná
kanonické	kanonický	k2eAgFnPc4d1	kanonická
lavice	lavice	k1gFnPc4	lavice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nechaly	nechat	k5eAaPmAgFnP	nechat
zhotovit	zhotovit	k5eAaPmF	zhotovit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
hlavního	hlavní	k2eAgMnSc2d1	hlavní
architekta	architekt	k1gMnSc2	architekt
Imricha	Imrich	k1gMnSc2	Imrich
Steindla	Steindla	k1gMnSc2	Steindla
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
také	také	k9	také
patronátní	patronátní	k2eAgFnSc4d1	patronátní
lavici	lavice	k1gFnSc4	lavice
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
nádherně	nádherně	k6eAd1	nádherně
vyřezávaný	vyřezávaný	k2eAgInSc1d1	vyřezávaný
a	a	k8xC	a
vymalovaný	vymalovaný	k2eAgInSc1d1	vymalovaný
znak	znak	k1gInSc1	znak
Košic	Košice	k1gInPc2	Košice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kaple	kaple	k1gFnPc1	kaple
Mettercie	Mettercie	k1gFnPc1	Mettercie
–	–	k?	–
Zvěstování	zvěstování	k1gNnSc1	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
===	===	k?	===
</s>
</p>
<p>
<s>
Jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kaplí	kaple	k1gFnPc2	kaple
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
Kaple	kaple	k1gFnSc1	kaple
Zvěstování	zvěstování	k1gNnPc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
Matyášovou	Matyášův	k2eAgFnSc7d1	Matyášova
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
předsíní	předsíň	k1gFnSc7	předsíň
jižního	jižní	k2eAgInSc2d1	jižní
portálu	portál	k1gInSc2	portál
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1477	[number]	k4	1477
mistr	mistr	k1gMnSc1	mistr
Štěpán	Štěpán	k1gMnSc1	Štěpán
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
podobizna	podobizna	k1gFnSc1	podobizna
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
na	na	k7c6	na
konzole	konzola	k1gFnSc6	konzola
křížových	křížový	k2eAgNnPc2d1	křížové
žeber	žebro	k1gNnPc2	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Stavitel	stavitel	k1gMnSc1	stavitel
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
papírovou	papírový	k2eAgFnSc4d1	papírová
pásku	páska	k1gFnSc4	páska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc4d1	umístěný
oltář	oltář	k1gInSc4	oltář
Mettercie	Mettercie	k1gFnSc2	Mettercie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	můj	k3xOp1gFnSc1	můj
kaple	kaple	k1gFnSc1	kaple
také	také	k9	také
jiné	jiný	k2eAgNnSc4d1	jiné
jméno	jméno	k1gNnSc4	jméno
–	–	k?	–
kaple	kaple	k1gFnSc1	kaple
Mettercie	Mettercie	k1gFnSc1	Mettercie
<g/>
.	.	kIx.	.
</s>
<s>
Vybudovat	vybudovat	k5eAaPmF	vybudovat
ji	on	k3xPp3gFnSc4	on
nechali	nechat	k5eAaPmAgMnP	nechat
rodiče	rodič	k1gMnPc1	rodič
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
uherského	uherský	k2eAgMnSc2d1	uherský
primase	primas	k1gMnSc5	primas
Juraje	Juraj	k1gInPc4	Juraj
Satmáryho	Satmáry	k1gMnSc2	Satmáry
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
třetí	třetí	k4xOgNnSc1	třetí
jméno	jméno	k1gNnSc1	jméno
Satmáryho	Satmáry	k1gMnSc2	Satmáry
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
její	její	k3xOp3gFnSc7	její
podlahou	podlaha	k1gFnSc7	podlaha
zřídili	zřídit	k5eAaPmAgMnP	zřídit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kryptu	krypta	k1gFnSc4	krypta
košických	košický	k2eAgInPc2d1	košický
biskupů	biskup	k1gInPc2	biskup
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
náhrobky	náhrobek	k1gInPc1	náhrobek
jsou	být	k5eAaImIp3nP	být
vsazeny	vsadit	k5eAaPmNgInP	vsadit
do	do	k7c2	do
obvodových	obvodový	k2eAgFnPc2d1	obvodová
zdí	zeď	k1gFnPc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Pochování	pochování	k1gNnPc1	pochování
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
Ignác	Ignác	k1gMnSc1	Ignác
Fábry	Fábra	k1gFnSc2	Fábra
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Perger	Perger	k1gMnSc1	Perger
<g/>
,	,	kIx,	,
Žikmund	Žikmund	k1gMnSc1	Žikmund
Bubič	Bubič	k1gMnSc1	Bubič
<g/>
,	,	kIx,	,
Augustín	Augustín	k1gMnSc1	Augustín
Fischer-Colbrie	Fischer-Colbrie	k1gFnSc2	Fischer-Colbrie
a	a	k8xC	a
Jozef	Jozef	k1gMnSc1	Jozef
Čársky	Čárska	k1gFnSc2	Čárska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kaple	kaple	k1gFnPc4	kaple
Svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
===	===	k?	===
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
kaplí	kaple	k1gFnSc7	kaple
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
Kaple	kaple	k1gFnSc1	kaple
Svatého	svatý	k2eAgInSc2d1	svatý
kříže	kříž	k1gInSc2	kříž
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1475	[number]	k4	1475
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
donátorem	donátor	k1gMnSc7	donátor
byl	být	k5eAaImAgMnS	být
městský	městský	k2eAgMnSc1d1	městský
konzul	konzul	k1gMnSc1	konzul
a	a	k8xC	a
rychtář	rychtář	k1gMnSc1	rychtář
Augustin	Augustin	k1gMnSc1	Augustin
Cromer	Cromer	k1gMnSc1	Cromer
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
též	též	k9	též
nazývá	nazývat	k5eAaImIp3nS	nazývat
Cromerova	Cromerův	k2eAgFnSc1d1	Cromerův
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
ji	on	k3xPp3gFnSc4	on
využívá	využívat	k5eAaImIp3nS	využívat
košická	košický	k2eAgFnSc1d1	Košická
kapitula	kapitula	k1gFnSc1	kapitula
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc4	svůj
sakristii	sakristie	k1gFnSc4	sakristie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zamčená	zamčený	k2eAgFnSc1d1	zamčená
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc3	veřejnost
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Královská	královský	k2eAgFnSc1d1	královská
empora	empora	k1gFnSc1	empora
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
období	období	k1gNnSc2	období
druhé	druhý	k4xOgFnSc2	druhý
etapy	etapa	k1gFnSc2	etapa
výstavby	výstavba	k1gFnSc2	výstavba
chrámu	chrám	k1gInSc2	chrám
patří	patřit	k5eAaImIp3nS	patřit
výstavba	výstavba	k1gFnSc1	výstavba
královského	královský	k2eAgNnSc2d1	královské
oratoria	oratorium	k1gNnSc2	oratorium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
koncepci	koncepce	k1gFnSc4	koncepce
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vzor	vzor	k1gInSc1	vzor
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
tamější	tamější	k2eAgFnSc1d1	tamější
parléřovské	parléřovský	k2eAgFnPc4d1	parléřovská
stavební	stavební	k2eAgFnPc4d1	stavební
hutě	huť	k1gFnPc4	huť
<g/>
.	.	kIx.	.
</s>
<s>
Empora	empora	k1gFnSc1	empora
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
poschodí	poschodí	k1gNnSc6	poschodí
lomeného	lomený	k2eAgInSc2d1	lomený
oblouku	oblouk	k1gInSc2	oblouk
jižního	jižní	k2eAgNnSc2d1	jižní
křídla	křídlo	k1gNnSc2	křídlo
příčné	příčný	k2eAgFnSc2d1	příčná
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
zábradlí	zábradlí	k1gNnSc1	zábradlí
tvoří	tvořit	k5eAaImIp3nS	tvořit
složitě	složitě	k6eAd1	složitě
přelamovaná	přelamovaný	k2eAgFnSc1d1	přelamovaný
kamenná	kamenný	k2eAgFnSc1d1	kamenná
mříž	mříž	k1gFnSc1	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
pod	pod	k7c7	pod
emporou	empora	k1gFnSc7	empora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
významná	významný	k2eAgFnSc1d1	významná
epigrafická	epigrafický	k2eAgFnSc1d1	epigrafická
památka	památka	k1gFnSc1	památka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
působení	působení	k1gNnSc2	působení
Jana	Jan	k1gMnSc2	Jan
Jiskry	jiskra	k1gFnSc2	jiskra
z	z	k7c2	z
Brandýsa	Brandýs	k1gInSc2	Brandýs
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
košického	košický	k2eAgMnSc2d1	košický
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1441	[number]	k4	1441
proklamuje	proklamovat	k5eAaBmIp3nS	proklamovat
věrnost	věrnost	k1gFnSc1	věrnost
Košičanů	Košičan	k1gMnPc2	Košičan
králi	král	k1gMnSc6	král
Ladislavu	Ladislav	k1gMnSc6	Ladislav
Pohrobkovi	pohrobek	k1gMnSc6	pohrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Léta	léto	k1gNnSc2	léto
Páně	páně	k2eAgFnSc1d1	páně
1440	[number]	k4	1440
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
stolice	stolice	k1gFnSc2	stolice
blahoslaveného	blahoslavený	k2eAgMnSc2d1	blahoslavený
apoštola	apoštol	k1gMnSc2	apoštol
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
za	za	k7c2	za
ranních	ranní	k2eAgInPc2d1	ranní
červánků	červánek	k1gInPc2	červánek
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Komárno	Komárno	k1gNnSc1	Komárno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
od	od	k7c2	od
Pána	pán	k1gMnSc2	pán
Albrechta	Albrecht	k1gMnSc2	Albrecht
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
římského	římský	k2eAgMnSc2d1	římský
<g/>
,	,	kIx,	,
uherského	uherský	k2eAgMnSc2d1	uherský
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc2d1	český
<g/>
,	,	kIx,	,
dalmátského	dalmátský	k2eAgMnSc2d1	dalmátský
a	a	k8xC	a
chorvatského	chorvatský	k2eAgMnSc2d1	chorvatský
a	a	k8xC	a
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
paní	paní	k1gFnSc1	paní
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
královny	královna	k1gFnPc1	královna
těch	ten	k3xDgMnPc2	ten
samých	samý	k3xTgMnPc2	samý
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
kdysi	kdysi	k6eAd1	kdysi
nepřemožitelného	přemožitelný	k2eNgMnSc2d1	nepřemožitelný
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
Pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgMnSc1d1	pravý
král	král	k1gMnSc1	král
a	a	k8xC	a
dědic	dědic	k1gMnSc1	dědic
trůnu	trůn	k1gInSc2	trůn
těchto	tento	k3xDgNnPc2	tento
království	království	k1gNnPc2	království
a	a	k8xC	a
vojvodství	vojvodství	k1gNnPc2	vojvodství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastnit	vlastnit	k5eAaImF	vlastnit
dědičným	dědičný	k2eAgNnSc7d1	dědičné
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
též	též	k9	též
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
Letnic	letnice	k1gFnPc2	letnice
se	se	k3xPyFc4	se
vší	všecek	k3xTgFnSc7	všecek
pompézností	pompéznost	k1gFnSc7	pompéznost
a	a	k8xC	a
nádherou	nádhera	k1gFnSc7	nádhera
zákonitě	zákonitě	k6eAd1	zákonitě
korunovaný	korunovaný	k2eAgInSc1d1	korunovaný
svatou	svatý	k2eAgFnSc7d1	svatá
korunou	koruna	k1gFnSc7	koruna
uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
v	v	k7c6	v
Székesfehérváru	Székesfehérvár	k1gInSc6	Székesfehérvár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sdružené	sdružený	k2eAgNnSc1d1	sdružené
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
===	===	k?	===
</s>
</p>
<p>
<s>
Zdvojené	zdvojený	k2eAgNnSc1d1	zdvojené
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
z	z	k7c2	z
první	první	k4xOgFnSc2	první
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vedoucí	vedoucí	k1gFnSc2	vedoucí
na	na	k7c4	na
královskou	královský	k2eAgFnSc4d1	královská
emporu	empora	k1gFnSc4	empora
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
dokladem	doklad	k1gInSc7	doklad
vlivu	vliv	k1gInSc2	vliv
pražské	pražský	k2eAgFnSc2d1	Pražská
stavební	stavební	k2eAgFnSc2d1	stavební
huti	huť	k1gFnSc2	huť
v	v	k7c6	v
košickém	košický	k2eAgNnSc6d1	košické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc4d1	svatý
Víta	Vít	k1gMnSc4	Vít
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc4	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
postavili	postavit	k5eAaPmAgMnP	postavit
točité	točitý	k2eAgNnSc4d1	točité
schodiště	schodiště	k1gNnSc4	schodiště
též	též	k9	též
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
jižním	jižní	k2eAgNnSc6d1	jižní
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
,	,	kIx,	,
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
schodiště	schodiště	k1gNnSc2	schodiště
tvoří	tvořit	k5eAaImIp3nP	tvořit
až	až	k9	až
dvě	dva	k4xCgNnPc1	dva
točitá	točitý	k2eAgNnPc1d1	točité
ramena	rameno	k1gNnPc1	rameno
střetávající	střetávající	k2eAgNnPc1d1	střetávající
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
v	v	k7c6	v
pěti	pět	k4xCc6	pět
společných	společný	k2eAgInPc6d1	společný
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
rameno	rameno	k1gNnSc1	rameno
schodů	schod	k1gInPc2	schod
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
do	do	k7c2	do
podkroví	podkroví	k1gNnSc2	podkroví
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Schodiště	schodiště	k1gNnSc1	schodiště
je	být	k5eAaImIp3nS	být
navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
mistrovské	mistrovský	k2eAgFnSc3d1	mistrovská
konstrukci	konstrukce	k1gFnSc3	konstrukce
značně	značně	k6eAd1	značně
nepohodlné	pohodlný	k2eNgFnPc1d1	nepohodlná
a	a	k8xC	a
stísněné	stísněný	k2eAgFnPc1d1	stísněná
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mělo	mít	k5eAaImAgNnS	mít
především	především	k6eAd1	především
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sdružené	sdružený	k2eAgNnSc1d1	sdružené
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
stávající	stávající	k2eAgFnSc1d1	stávající
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
postavení	postavení	k1gNnSc6	postavení
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
podobné	podobný	k2eAgInPc4d1	podobný
projekty	projekt	k1gInPc4	projekt
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
chrámů	chrám	k1gInPc2	chrám
v	v	k7c6	v
sedmihradských	sedmihradský	k2eAgNnPc6d1	sedmihradské
městech	město	k1gNnPc6	město
Kluž	Kluž	k1gFnSc1	Kluž
</s>
</p>
<p>
<s>
a	a	k8xC	a
Sighişoara	Sighişoara	k1gFnSc1	Sighişoara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1499	[number]	k4	1499
<g/>
–	–	k?	–
<g/>
1500	[number]	k4	1500
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
košického	košický	k2eAgNnSc2d1	košické
schodiště	schodiště	k1gNnSc2	schodiště
postavili	postavit	k5eAaPmAgMnP	postavit
podobné	podobný	k2eAgInPc4d1	podobný
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
rezidenci	rezidence	k1gFnSc6	rezidence
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Grazu	Graz	k1gInSc6	Graz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fragment	fragment	k1gInSc1	fragment
středověkého	středověký	k2eAgInSc2d1	středověký
nápisu	nápis	k1gInSc2	nápis
===	===	k?	===
</s>
</p>
<p>
<s>
Vpravo	vpravo	k6eAd1	vpravo
u	u	k7c2	u
oltáře	oltář	k1gInSc2	oltář
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
boční	boční	k2eAgFnSc6d1	boční
apsidě	apsida	k1gFnSc6	apsida
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
místy	místy	k6eAd1	místy
čitelný	čitelný	k2eAgInSc1d1	čitelný
gotický	gotický	k2eAgInSc1d1	gotický
nápis	nápis	k1gInSc1	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
útržků	útržek	k1gInPc2	útržek
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
události	událost	k1gFnPc4	událost
období	období	k1gNnSc2	období
vlády	vláda	k1gFnSc2	vláda
králů	král	k1gMnPc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
a	a	k8xC	a
Alberta	Albert	k1gMnSc2	Albert
(	(	kIx(	(
<g/>
1387	[number]	k4	1387
<g/>
–	–	k?	–
<g/>
1439	[number]	k4	1439
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oltářní	oltářní	k2eAgInSc1d1	oltářní
obraz	obraz	k1gInSc1	obraz
Oplakávání	oplakávání	k1gNnSc2	oplakávání
Krista	Kristus	k1gMnSc2	Kristus
===	===	k?	===
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
obvodové	obvodový	k2eAgFnSc2d1	obvodová
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
za	za	k7c2	za
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oltářní	oltářní	k2eAgFnPc1d1	oltářní
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
===	===	k?	===
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
chóru	chór	k1gInSc2	chór
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
lucerny	lucerna	k1gFnSc2	lucerna
krále	král	k1gMnSc2	král
Matěje	Matěj	k1gMnSc2	Matěj
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
a	a	k8xC	a
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
nese	nést	k5eAaImIp3nS	nést
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
HAC	HAC	kA	HAC
<g/>
.	.	kIx.	.
<g/>
SVB	SVB	kA	SVB
<g/>
.	.	kIx.	.
<g/>
ICONE	ICONE	kA	ICONE
<g/>
.	.	kIx.	.
<g/>
IN	IN	kA	IN
<g/>
.	.	kIx.	.
<g/>
PRAGENSI	PRAGENSI	kA	PRAGENSI
<g/>
.	.	kIx.	.
<g/>
SEPVLCRO	SEPVLCRO	kA	SEPVLCRO
<g/>
.	.	kIx.	.
<g/>
COLITVR	COLITVR	kA	COLITVR
<g/>
.	.	kIx.	.
<g/>
SANCTVS	SANCTVS	kA	SANCTVS
<g/>
.	.	kIx.	.
<g/>
IOANNES	IOANNES	kA	IOANNES
<g/>
.	.	kIx.	.
<g/>
NEPOMVCENVS	NEPOMVCENVS	kA	NEPOMVCENVS
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
obraze	obraz	k1gInSc6	obraz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
hrobě	hrob	k1gInSc6	hrob
uctívaný	uctívaný	k2eAgMnSc1d1	uctívaný
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zpěvácká	zpěvácký	k2eAgFnSc1d1	zpěvácká
tribuna	tribuna	k1gFnSc1	tribuna
===	===	k?	===
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
balkónek	balkónek	k1gInSc1	balkónek
umístěný	umístěný	k2eAgInSc1d1	umístěný
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodě	loď	k1gFnSc2	loď
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
presbyteria	presbyterium	k1gNnSc2	presbyterium
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
vybavení	vybavení	k1gNnSc3	vybavení
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
pro	pro	k7c4	pro
spodobení	spodobení	k1gNnSc4	spodobení
nebes	nebesa	k1gNnPc2	nebesa
herci	herec	k1gMnPc7	herec
a	a	k8xC	a
zpěváky	zpěvák	k1gMnPc7	zpěvák
různých	různý	k2eAgFnPc2d1	různá
středověkých	středověký	k2eAgFnPc2d1	středověká
alegorií	alegorie	k1gFnPc2	alegorie
a	a	k8xC	a
mystérií	mystérium	k1gNnPc2	mystérium
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
tribunu	tribuna	k1gFnSc4	tribuna
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
malá	malý	k2eAgNnPc1d1	malé
dvířka	dvířka	k1gNnPc1	dvířka
v	v	k7c6	v
apsidě	apsida	k1gFnSc6	apsida
severní	severní	k2eAgFnSc2d1	severní
boční	boční	k2eAgFnSc2d1	boční
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vede	vést	k5eAaImIp3nS	vést
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
pilíře	pilíř	k1gInSc2	pilíř
do	do	k7c2	do
podkroví	podkroví	k1gNnSc2	podkroví
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
tribunu	tribuna	k1gFnSc4	tribuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
===	===	k?	===
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Florián	Florián	k1gMnSc1	Florián
byl	být	k5eAaImAgMnS	být
patronem	patron	k1gMnSc7	patron
hasičů	hasič	k1gMnPc2	hasič
a	a	k8xC	a
chránil	chránit	k5eAaImAgMnS	chránit
před	před	k7c7	před
požáry	požár	k1gInPc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nechalo	nechat	k5eAaPmAgNnS	nechat
postavit	postavit	k5eAaPmF	postavit
jeho	jeho	k3xOp3gFnSc4	jeho
sochu	socha	k1gFnSc4	socha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
stále	stále	k6eAd1	stále
nejprve	nejprve	k6eAd1	nejprve
u	u	k7c2	u
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
jižní	jižní	k2eAgFnSc2d1	jižní
stěny	stěna	k1gFnSc2	stěna
Urbanovy	Urbanův	k2eAgFnSc2d1	Urbanova
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
předsíně	předsíň	k1gFnSc2	předsíň
jižního	jižní	k2eAgInSc2d1	jižní
portálu	portál	k1gInSc2	portál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svatyně	svatyně	k1gFnSc2	svatyně
===	===	k?	===
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
svatyně	svatyně	k1gFnSc2	svatyně
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
etapy	etapa	k1gFnSc2	etapa
výstavby	výstavba	k1gFnSc2	výstavba
chrámu	chrám	k1gInSc2	chrám
1440	[number]	k4	1440
<g/>
–	–	k?	–
<g/>
1462	[number]	k4	1462
<g/>
.	.	kIx.	.
její	její	k3xOp3gFnSc4	její
interiérovou	interiérový	k2eAgFnSc4d1	interiérová
část	část	k1gFnSc4	část
nenarušila	narušit	k5eNaPmAgFnS	narušit
ani	ani	k8xC	ani
velká	velký	k2eAgFnSc1d1	velká
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
odkryly	odkrýt	k5eAaPmAgInP	odkrýt
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
klenbových	klenbový	k2eAgNnPc6d1	klenbové
polích	pole	k1gNnPc6	pole
svatyně	svatyně	k1gFnSc2	svatyně
erby	erb	k1gInPc1	erb
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
vlevo	vlevo	k6eAd1	vlevo
dvouhlavého	dvouhlavý	k2eAgMnSc2d1	dvouhlavý
císařského	císařský	k2eAgMnSc2d1	císařský
orla	orel	k1gMnSc2	orel
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
jednohlavého	jednohlavý	k2eAgMnSc4d1	jednohlavý
lucemburského	lucemburský	k2eAgMnSc4d1	lucemburský
orla	orel	k1gMnSc4	orel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
je	být	k5eAaImIp3nS	být
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
vzorek	vzorek	k1gInSc1	vzorek
původní	původní	k2eAgFnSc2d1	původní
barevnosti	barevnost	k1gFnSc2	barevnost
a	a	k8xC	a
polychromie	polychromie	k1gFnSc2	polychromie
chrámu	chrám	k1gInSc2	chrám
na	na	k7c6	na
části	část	k1gFnSc6	část
konzole	konzola	k1gFnSc6	konzola
a	a	k8xC	a
žebra	žebro	k1gNnSc2	žebro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svatyni	svatyně	k1gFnSc6	svatyně
se	se	k3xPyFc4	se
na	na	k7c6	na
konzolách	konzola	k1gFnPc6	konzola
nacházejí	nacházet	k5eAaImIp3nP	nacházet
neogotické	ogotický	k2eNgFnPc1d1	neogotická
sochy	socha	k1gFnPc1	socha
od	od	k7c2	od
Jána	Ján	k1gMnSc2	Ján
Marschalka	Marschalka	k1gFnSc1	Marschalka
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Představují	představovat	k5eAaImIp3nP	představovat
svaté	svatý	k2eAgNnSc4d1	svaté
<g/>
:	:	kIx,	:
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Alexandrijskou	alexandrijský	k2eAgFnSc4d1	Alexandrijská
<g/>
,	,	kIx,	,
Barboru	Barbora	k1gFnSc4	Barbora
<g/>
,	,	kIx,	,
Řehoře	Řehoř	k1gMnSc4	Řehoř
<g/>
,	,	kIx,	,
Ignáce	Ignác	k1gMnSc4	Ignác
biskupa	biskup	k1gMnSc4	biskup
<g/>
,	,	kIx,	,
Dominika	Dominik	k1gMnSc4	Dominik
<g/>
,	,	kIx,	,
Františka	František	k1gMnSc4	František
z	z	k7c2	z
Assisi	Assise	k1gFnSc6	Assise
<g/>
,	,	kIx,	,
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
,	,	kIx,	,
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
diakona	diakon	k1gMnSc2	diakon
<g/>
,	,	kIx,	,
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
,	,	kIx,	,
Ambrože	Ambrož	k1gMnSc2	Ambrož
<g/>
,	,	kIx,	,
Benedikta	Benedikt	k1gMnSc2	Benedikt
opata	opat	k1gMnSc2	opat
a	a	k8xC	a
Ignáce	Ignác	k1gMnSc2	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc4d1	současná
okenní	okenní	k2eAgFnPc4d1	okenní
vitráže	vitráž	k1gFnPc4	vitráž
vyhotovila	vyhotovit	k5eAaPmAgFnS	vyhotovit
dílna	dílna	k1gFnSc1	dílna
Karla	Karel	k1gMnSc2	Karel
Geylinga	Geyling	k1gMnSc2	Geyling
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vitrážích	vitráž	k1gFnPc6	vitráž
jsou	být	k5eAaImIp3nP	být
vyobrazeny	vyobrazit	k5eAaPmNgInP	vyobrazit
i	i	k9	i
znaky	znak	k1gInPc4	znak
kanovníků	kanovník	k1gMnPc2	kanovník
Sándora	Sándor	k1gMnSc2	Sándor
Dessewffyho	Dessewffy	k1gMnSc2	Dessewffy
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
jako	jako	k9	jako
čanádského	čanádský	k2eAgMnSc2d1	čanádský
biskupa	biskup	k1gMnSc2	biskup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Józsefa	József	k1gMnSc2	József
Repaszkeho	Repaszke	k1gMnSc2	Repaszke
a	a	k8xC	a
Jánosa	Jánosa	k1gFnSc1	Jánosa
Volneho	Volne	k1gMnSc2	Volne
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
pouze	pouze	k6eAd1	pouze
iniciálami	iniciála	k1gFnPc7	iniciála
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bartalana	Bartalana	k1gFnSc1	Bartalana
Merse	Merse	k1gFnSc1	Merse
(	(	kIx(	(
<g/>
úplný	úplný	k2eAgInSc1d1	úplný
rodový	rodový	k2eAgInSc1d1	rodový
erb	erb	k1gInSc1	erb
bez	bez	k7c2	bez
klobouku	klobouk	k1gInSc2	klobouk
a	a	k8xC	a
střapců	střapec	k1gInPc2	střapec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eduarda	Eduard	k1gMnSc2	Eduard
Karola	Karola	k1gFnSc1	Karola
Medveckého	Medvecký	k2eAgMnSc2d1	Medvecký
<g/>
,	,	kIx,	,
Štefana	Štefan	k1gMnSc2	Štefan
Lesska	Lessek	k1gMnSc2	Lessek
a	a	k8xC	a
Ferenca	Ferenca	k?	Ferenca
Pagácsa	Pagácsa	k1gFnSc1	Pagácsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obětní	obětní	k2eAgInSc4d1	obětní
stůl	stůl	k1gInSc4	stůl
===	===	k?	===
</s>
</p>
<p>
<s>
Nejmladší	mladý	k2eAgFnSc7d3	nejmladší
součástí	součást	k1gFnSc7	součást
chrámového	chrámový	k2eAgInSc2d1	chrámový
inventáře	inventář	k1gInSc2	inventář
je	být	k5eAaImIp3nS	být
obětní	obětní	k2eAgInSc4d1	obětní
stůl	stůl	k1gInSc4	stůl
celebrujícího	celebrující	k2eAgMnSc2d1	celebrující
kněze	kněz	k1gMnSc2	kněz
ve	v	k7c6	v
svatyni	svatyně	k1gFnSc6	svatyně
chrámu	chrám	k1gInSc2	chrám
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vytesán	vytesat	k5eAaPmNgInS	vytesat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
pískovce	pískovec	k1gInSc2	pískovec
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
dvou	dva	k4xCgNnPc2	dva
svrchních	svrchní	k2eAgNnPc2d1	svrchní
ramen	rameno	k1gNnPc2	rameno
vytvářejících	vytvářející	k2eAgNnPc2d1	vytvářející
elipsu	elipsa	k1gFnSc4	elipsa
<g/>
.	.	kIx.	.
</s>
<s>
Obětní	obětní	k2eAgInSc1d1	obětní
stůl	stůl	k1gInSc1	stůl
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
nově	nově	k6eAd1	nově
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
ambon	ambon	k1gInSc1	ambon
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
kázání	kázání	k1gNnSc4	kázání
<g/>
)	)	kIx)	)
a	a	k8xC	a
sedes	sedes	k1gInSc4	sedes
(	(	kIx(	(
<g/>
sedadla	sedadlo	k1gNnSc2	sedadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
objekty	objekt	k1gInPc4	objekt
zhotovili	zhotovit	k5eAaPmAgMnP	zhotovit
Michal	Michal	k1gMnSc1	Michal
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baníkovi	baníkův	k2eAgMnPc1d1	baníkův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kazatelna	kazatelna	k1gFnSc1	kazatelna
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvydařenějších	vydařený	k2eAgInPc2d3	nejvydařenější
počinů	počin	k1gInPc2	počin
neogotické	ogotický	k2eNgFnSc2d1	neogotická
přestavby	přestavba	k1gFnSc2	přestavba
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
kamenná	kamenný	k2eAgFnSc1d1	kamenná
kazatelna	kazatelna	k1gFnSc1	kazatelna
s	s	k7c7	s
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
stříškou	stříška	k1gFnSc7	stříška
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
sochařů	sochař	k1gMnPc2	sochař
W.	W.	kA	W.
Aubrama	Aubrama	k1gFnSc1	Aubrama
a	a	k8xC	a
R.	R.	kA	R.
Argentiho	Argenti	k1gMnSc2	Argenti
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
točitém	točitý	k2eAgNnSc6d1	točité
schodišti	schodiště	k1gNnSc6	schodiště
s	s	k7c7	s
kruhovým	kruhový	k2eAgNnSc7d1	kruhové
zábradlím	zábradlí	k1gNnSc7	zábradlí
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
parapetu	parapet	k1gInSc6	parapet
řečniště	řečniště	k1gNnSc2	řečniště
umístěny	umístěn	k2eAgFnPc1d1	umístěna
sochy	socha	k1gFnPc1	socha
proroků	prorok	k1gMnPc2	prorok
a	a	k8xC	a
církevních	církevní	k2eAgMnPc2d1	církevní
otců	otec	k1gMnPc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Mříž	mříž	k1gFnSc1	mříž
okolo	okolo	k7c2	okolo
kazatelny	kazatelna	k1gFnSc2	kazatelna
je	být	k5eAaImIp3nS	být
kovaná	kovaný	k2eAgFnSc1d1	kovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
krypta	krypta	k1gFnSc1	krypta
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
chrámu	chrám	k1gInSc2	chrám
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
vybudována	vybudován	k2eAgFnSc1d1	vybudována
krypta	krypta	k1gFnSc1	krypta
pro	pro	k7c4	pro
ostatky	ostatek	k1gInPc4	ostatek
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Kryptu	krypta	k1gFnSc4	krypta
i	i	k9	i
čtyři	čtyři	k4xCgInPc1	čtyři
kamenné	kamenný	k2eAgInPc1d1	kamenný
sarkofágy	sarkofág	k1gInPc1	sarkofág
projektoval	projektovat	k5eAaBmAgMnS	projektovat
budapešťský	budapešťský	k2eAgMnSc1d1	budapešťský
profesor	profesor	k1gMnSc1	profesor
Frigyes	Frigyesa	k1gFnPc2	Frigyesa
Schulek	Schulka	k1gFnPc2	Schulka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředním	prostřední	k2eAgNnSc6d1	prostřední
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
jsou	být	k5eAaImIp3nP	být
společně	společně	k6eAd1	společně
pochováni	pochován	k2eAgMnPc1d1	pochován
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Helena	Helena	k1gFnSc1	Helena
Zrinská	Zrinská	k1gFnSc1	Zrinská
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sarkofág	sarkofág	k1gInSc1	sarkofág
s	s	k7c7	s
tělesnými	tělesný	k2eAgInPc7d1	tělesný
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
generála	generál	k1gMnSc2	generál
hraběte	hrabě	k1gMnSc2	hrabě
Antona	Anton	k1gMnSc2	Anton
Esterháziho	Esterházi	k1gMnSc2	Esterházi
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
sarkofág	sarkofág	k1gInSc1	sarkofág
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
druha	druh	k1gMnSc2	druh
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Šibrika	Šibrik	k1gMnSc2	Šibrik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtím	čtvrtit	k5eAaImIp1nS	čtvrtit
sarkofágu	sarkofág	k1gInSc2	sarkofág
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
krypty	krypta	k1gFnSc2	krypta
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
hrabě	hrabě	k1gMnSc1	hrabě
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Berčéni	Berčéen	k2eAgMnPc1d1	Berčéen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
manželka	manželka	k1gFnSc1	manželka
Kristina	Kristina	k1gFnSc1	Kristina
Čákiová	Čákiový	k2eAgFnSc1d1	Čákiová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Apoteóza	apoteóza	k1gFnSc1	apoteóza
života	život	k1gInSc2	život
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
===	===	k?	===
</s>
</p>
<p>
<s>
Monumentální	monumentální	k2eAgFnSc1d1	monumentální
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
nad	nad	k7c7	nad
severním	severní	k2eAgInSc7d1	severní
vstupem	vstup	k1gInSc7	vstup
katedrály	katedrála	k1gFnSc2	katedrála
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Andor	Andor	k1gMnSc1	Andor
Dudics	Dudics	k1gInSc1	Dudics
<g/>
.	.	kIx.	.
</s>
<s>
Nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
oslavy	oslava	k1gFnSc2	oslava
života	život	k1gInSc2	život
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
je	být	k5eAaImIp3nS	být
řešena	řešit	k5eAaImNgFnS	řešit
jako	jako	k8xC	jako
triptych	triptych	k1gInSc1	triptych
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
mezi	mezi	k7c7	mezi
oknem	okno	k1gNnSc7	okno
severního	severní	k2eAgInSc2d1	severní
štítu	štít	k1gInSc2	štít
a	a	k8xC	a
římsou	římsa	k1gFnSc7	římsa
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
Rákócziho	Rákóczi	k1gMnSc4	Rákóczi
smuteční	smuteční	k2eAgInSc1d1	smuteční
pohřební	pohřební	k2eAgInSc4d1	pohřební
průvod	průvod	k1gInSc4	průvod
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
malba	malba	k1gFnSc1	malba
je	být	k5eAaImIp3nS	být
oslavou	oslava	k1gFnSc7	oslava
života	život	k1gInSc2	život
knížete	kníže	k1gMnSc2	kníže
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
zatčení	zatčení	k1gNnSc3	zatčení
na	na	k7c6	na
Šarišském	šarišský	k2eAgInSc6d1	šarišský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
v	v	k7c4	v
roli	role	k1gFnSc4	role
vůdce	vůdce	k1gMnSc2	vůdce
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
po	po	k7c6	po
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
v	v	k7c6	v
půloblouku	půloblouk	k1gInSc6	půloblouk
nad	nad	k7c7	nad
dveřmi	dveře	k1gFnPc7	dveře
severního	severní	k2eAgInSc2d1	severní
portálu	portál	k1gInSc2	portál
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
Rákócziho	Rákóczi	k1gMnSc4	Rákóczi
sarkofág	sarkofág	k1gInSc1	sarkofág
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Exteriér	exteriér	k1gInSc4	exteriér
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Severní	severní	k2eAgInSc1d1	severní
portál	portál	k1gInSc1	portál
===	===	k?	===
</s>
</p>
<p>
<s>
Řešení	řešení	k1gNnSc1	řešení
severního	severní	k2eAgInSc2d1	severní
portálu	portál	k1gInSc2	portál
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
se	se	k3xPyFc4	se
vymyká	vymykat	k5eAaImIp3nS	vymykat
dobovému	dobový	k2eAgInSc3d1	dobový
středověkému	středověký	k2eAgInSc3d1	středověký
úzu	úzus	k1gInSc3	úzus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
na	na	k7c6	na
severních	severní	k2eAgFnPc6d1	severní
stěnách	stěna	k1gFnPc6	stěna
kostelních	kostelní	k2eAgFnPc2d1	kostelní
staveb	stavba	k1gFnPc2	stavba
portály	portál	k1gInPc1	portál
nestavěly	stavět	k5eNaImAgInP	stavět
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gFnSc1	jeho
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
je	být	k5eAaImIp3nS	být
nejdekorativnější	dekorativní	k2eAgInSc1d3	dekorativní
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
třech	tři	k4xCgInPc2	tři
portálů	portál	k1gInPc2	portál
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
jeho	jeho	k3xOp3gNnSc4	jeho
obrácení	obrácení	k1gNnSc4	obrácení
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
prostoru	prostor	k1gInSc2	prostor
veřejného	veřejný	k2eAgNnSc2d1	veřejné
dění	dění	k1gNnSc2	dění
středověkých	středověký	k2eAgInPc2d1	středověký
Košic	Košice	k1gInPc2	Košice
<g/>
,	,	kIx,	,
k	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městskému	městský	k2eAgNnSc3d1	Městské
tržišti	tržiště	k1gNnSc3	tržiště
a	a	k8xC	a
k	k	k7c3	k
městské	městský	k2eAgFnSc3d1	městská
radnici	radnice	k1gFnSc3	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
se	se	k3xPyFc4	se
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
brána	brána	k1gFnSc1	brána
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
pozlacen	pozlatit	k5eAaPmNgInS	pozlatit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
portál	portál	k1gInSc1	portál
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvojice	dvojice	k1gFnSc1	dvojice
vstupních	vstupní	k2eAgFnPc2d1	vstupní
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oblouk	oblouk	k1gInSc1	oblouk
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
je	být	k5eAaImIp3nS	být
dělený	dělený	k2eAgInSc1d1	dělený
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
je	být	k5eAaImIp3nS	být
zobrazený	zobrazený	k2eAgInSc4d1	zobrazený
zástup	zástup	k1gInSc4	zástup
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
směřují	směřovat	k5eAaImIp3nP	směřovat
buď	buď	k8xC	buď
do	do	k7c2	do
nebeské	nebeský	k2eAgFnSc2d1	nebeská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spasené	spasený	k2eAgNnSc1d1	spasené
vítá	vítat	k5eAaImIp3nS	vítat
anděl	anděl	k1gMnSc1	anděl
nebo	nebo	k8xC	nebo
do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
leviatanovy	leviatanův	k2eAgFnSc2d1	leviatanův
tlamy	tlama	k1gFnSc2	tlama
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zatracené	zatracený	k2eAgFnPc4d1	zatracená
a	a	k8xC	a
spoutané	spoutaný	k2eAgFnPc4d1	spoutaná
v	v	k7c6	v
řetězech	řetěz	k1gInPc6	řetěz
vlečou	vléct	k5eAaImIp3nP	vléct
ďáblové	ďábel	k1gMnPc1	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
leviatanově	leviatanův	k2eAgFnSc6d1	leviatanův
tlamě	tlama	k1gFnSc6	tlama
končí	končit	k5eAaImIp3nS	končit
i	i	k9	i
vysoký	vysoký	k2eAgMnSc1d1	vysoký
církevní	církevní	k2eAgMnSc1d1	církevní
hodnostář	hodnostář	k1gMnSc1	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
spodní	spodní	k2eAgNnSc1d1	spodní
pole	pole	k1gNnSc1	pole
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
pozemskou	pozemský	k2eAgFnSc4d1	pozemská
realitu	realita	k1gFnSc4	realita
posledních	poslední	k2eAgFnPc2d1	poslední
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
horní	horní	k2eAgNnSc1d1	horní
pole	pole	k1gNnSc1	pole
zpodobňuje	zpodobňovat	k5eAaImIp3nS	zpodobňovat
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
realitu	realita	k1gFnSc4	realita
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
<g/>
:	:	kIx,	:
trůnící	trůnící	k2eAgMnSc1d1	trůnící
Kristus	Kristus	k1gMnSc1	Kristus
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
světu	svět	k1gInSc3	svět
(	(	kIx(	(
<g/>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
pod	pod	k7c7	pod
nohami	noha	k1gFnPc7	noha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
Pannou	Panna	k1gFnSc7	Panna
Marií	Maria	k1gFnPc2	Maria
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Evangelistou	evangelista	k1gMnSc7	evangelista
a	a	k8xC	a
dvěma	dva	k4xCgMnPc7	dva
anděly	anděl	k1gMnPc7	anděl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
drží	držet	k5eAaImIp3nP	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
nástroje	nástroj	k1gInPc1	nástroj
mučení	mučení	k1gNnSc1	mučení
(	(	kIx(	(
<g/>
kříž	kříž	k1gInSc1	kříž
a	a	k8xC	a
hřeby	hřeb	k1gInPc1	hřeb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
trojice	trojice	k1gFnSc1	trojice
andělů	anděl	k1gMnPc2	anděl
troubami	trouba	k1gFnPc7	trouba
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
figury	figura	k1gFnPc1	figura
představují	představovat	k5eAaImIp3nP	představovat
dvanáct	dvanáct	k4xCc4	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
ozdobného	ozdobný	k2eAgInSc2d1	ozdobný
štítu	štít	k1gInSc2	štít
(	(	kIx(	(
<g/>
vimperku	vimperk	k1gInSc2	vimperk
<g/>
)	)	kIx)	)
reliéfu	reliéf	k1gInSc2	reliéf
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
stupních	stupeň	k1gInPc6	stupeň
rozmístěných	rozmístěný	k2eAgInPc2d1	rozmístěný
pět	pět	k4xCc1	pět
rámcových	rámcový	k2eAgInPc2d1	rámcový
reliéfů	reliéf	k1gInPc2	reliéf
zdobených	zdobený	k2eAgInPc2d1	zdobený
fiálami	fiála	k1gFnPc7	fiála
<g/>
,	,	kIx,	,
profilací	profilace	k1gFnSc7	profilace
a	a	k8xC	a
kruhovými	kruhový	k2eAgInPc7d1	kruhový
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
spodní	spodní	k2eAgMnPc1d1	spodní
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
života	život	k1gInSc2	život
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
spodobněna	spodobněn	k2eAgFnSc1d1	spodobněn
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ošetřuje	ošetřovat	k5eAaImIp3nS	ošetřovat
<g/>
,	,	kIx,	,
umývá	umývat	k5eAaImIp3nS	umývat
a	a	k8xC	a
krmí	krmit	k5eAaImIp3nS	krmit
chudobné	chudobný	k2eAgNnSc1d1	chudobné
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
tři	tři	k4xCgMnPc1	tři
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
scénu	scéna	k1gFnSc4	scéna
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
je	být	k5eAaImIp3nS	být
ukřižovaný	ukřižovaný	k2eAgMnSc1d1	ukřižovaný
Kristus	Kristus	k1gMnSc1	Kristus
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
stromu	strom	k1gInSc2	strom
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
je	být	k5eAaImIp3nS	být
ztvárnění	ztvárnění	k1gNnSc1	ztvárnění
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
ukřižovaných	ukřižovaný	k2eAgMnPc2d1	ukřižovaný
na	na	k7c6	na
Golgotě	Golgota	k1gFnSc6	Golgota
<g/>
,	,	kIx,	,
nalevo	nalevo	k6eAd1	nalevo
spasená	spasený	k2eAgFnSc1d1	spasená
duše	duše	k1gFnSc1	duše
je	být	k5eAaImIp3nS	být
odnášena	odnášen	k2eAgFnSc1d1	odnášena
andělem	anděl	k1gMnSc7	anděl
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
napravo	napravo	k6eAd1	napravo
duši	duše	k1gFnSc4	duše
zatraceného	zatracený	k2eAgInSc2d1	zatracený
odnáší	odnášet	k5eAaImIp3nS	odnášet
ďábel	ďábel	k1gMnSc1	ďábel
do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
scénou	scéna	k1gFnSc7	scéna
Ukřižování	ukřižování	k1gNnSc2	ukřižování
jsou	být	k5eAaImIp3nP	být
nalevo	nalevo	k6eAd1	nalevo
plačící	plačící	k2eAgFnPc1d1	plačící
ženy	žena	k1gFnPc1	žena
okolo	okolo	k7c2	okolo
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
pod	pod	k7c7	pod
křížem	kříž	k1gInSc7	kříž
a	a	k8xC	a
napravo	napravo	k6eAd1	napravo
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
obklopen	obklopit	k5eAaPmNgMnS	obklopit
římskými	římský	k2eAgMnPc7d1	římský
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
byly	být	k5eAaImAgInP	být
doplněny	doplněn	k2eAgInPc1d1	doplněn
výklenky	výklenek	k1gInPc1	výklenek
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
obou	dva	k4xCgInPc2	dva
bran	brána	k1gFnPc2	brána
severního	severní	k2eAgInSc2d1	severní
portálu	portál	k1gInSc2	portál
neogotickými	ogotický	k2eNgFnPc7d1	neogotická
sochami	socha	k1gFnPc7	socha
<g/>
:	:	kIx,	:
svatý	svatý	k2eAgMnSc1d1	svatý
Imrich	Imrich	k1gMnSc1	Imrich
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgMnSc1d1	svatý
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
svatá	svatý	k2eAgFnSc1d1	svatá
Alžběta	Alžběta	k1gFnSc1	Alžběta
Uherská	uherský	k2eAgFnSc1d1	uherská
<g/>
,	,	kIx,	,
svatý	svatý	k1gMnSc1	svatý
Ladislav	Ladislav	k1gMnSc1	Ladislav
a	a	k8xC	a
svatý	svatý	k2eAgMnSc1d1	svatý
Henrich	Henrich	k1gMnSc1	Henrich
<g/>
.	.	kIx.	.
</s>
<s>
Výklenky	výklenek	k1gInPc1	výklenek
jsou	být	k5eAaImIp3nP	být
původní	původní	k2eAgNnSc4d1	původní
a	a	k8xC	a
neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
sochy	socha	k1gFnPc1	socha
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
stály	stát	k5eAaImAgFnP	stát
v	v	k7c6	v
předcházejících	předcházející	k2eAgNnPc6d1	předcházející
staletích	staletí	k1gNnPc6	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
neogotické	ogotický	k2eNgFnPc1d1	neogotická
sochy	socha	k1gFnPc1	socha
zdobí	zdobit	k5eAaImIp3nP	zdobit
průčelí	průčelí	k1gNnSc4	průčelí
západního	západní	k2eAgInSc2d1	západní
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
uherští	uherský	k2eAgMnPc1d1	uherský
králové	králová	k1gFnSc3	králová
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Alžběta	Alžběta	k1gFnSc1	Alžběta
Piastovská	Piastovský	k2eAgFnSc1d1	Piastovská
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Neogotické	Neogotický	k2eAgFnPc1d1	Neogotická
sochy	socha	k1gFnPc1	socha
severního	severní	k2eAgInSc2d1	severní
portálu	portál	k1gInSc2	portál
jsou	být	k5eAaImIp3nP	být
dílem	dílo	k1gNnSc7	dílo
budapešťského	budapešťský	k2eAgMnSc2d1	budapešťský
sochaře	sochař	k1gMnSc2	sochař
Ľudovíta	Ľudovít	k1gMnSc2	Ľudovít
Lantaye	Lantay	k1gMnSc2	Lantay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Západní	západní	k2eAgInSc1d1	západní
portál	portál	k1gInSc1	portál
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
liturgického	liturgický	k2eAgInSc2d1	liturgický
zvyku	zvyk	k1gInSc2	zvyk
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
západní	západní	k2eAgInSc4d1	západní
portál	portál	k1gInSc4	portál
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ho	on	k3xPp3gInSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
trojice	trojice	k1gFnSc1	trojice
bran	brána	k1gFnPc2	brána
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nejjednodušší	jednoduchý	k2eAgFnSc4d3	nejjednodušší
kamenickou	kamenický	k2eAgFnSc4d1	kamenická
výzdobu	výzdoba	k1gFnSc4	výzdoba
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
boční	boční	k2eAgInPc1d1	boční
vstupy	vstup	k1gInPc1	vstup
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
figurální	figurální	k2eAgFnSc2d1	figurální
dekorace	dekorace	k1gFnSc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ústřední	ústřední	k2eAgFnSc7d1	ústřední
bránou	brána	k1gFnSc7	brána
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgInPc1	dva
reliéfy	reliéf	k1gInPc1	reliéf
<g/>
,	,	kIx,	,
Jeden	jeden	k4xCgInSc1	jeden
bezprostředně	bezprostředně	k6eAd1	bezprostředně
nad	nad	k7c7	nad
bránou	brána	k1gFnSc7	brána
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
Krista	Krista	k1gFnSc1	Krista
v	v	k7c6	v
Getsemanské	getsemanský	k2eAgFnSc6d1	Getsemanská
zahradě	zahrada	k1gFnSc6	zahrada
úpěnlivě	úpěnlivě	k6eAd1	úpěnlivě
se	se	k3xPyFc4	se
modlícího	modlící	k2eAgInSc2d1	modlící
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
vykukujícímu	vykukující	k2eAgMnSc3d1	vykukující
z	z	k7c2	z
pravého	pravý	k2eAgInSc2d1	pravý
horního	horní	k2eAgInSc2d1	horní
rohu	roh	k1gInSc2	roh
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
Krista	Kristus	k1gMnSc4	Kristus
provázejí	provázet	k5eAaImIp3nP	provázet
apoštolové	apoštol	k1gMnPc1	apoštol
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
přichází	přicházet	k5eAaImIp3nS	přicházet
skupina	skupina	k1gFnSc1	skupina
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jidášem	jidáš	k1gInSc7	jidáš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedlovém	sedlový	k2eAgNnSc6d1	sedlové
ukončení	ukončení	k1gNnSc6	ukončení
portálu	portál	k1gInSc2	portál
je	být	k5eAaImIp3nS	být
scéna	scéna	k1gFnSc1	scéna
Piety	pieta	k1gFnSc2	pieta
<g/>
,	,	kIx,	,
Panna	Panna	k1gFnSc1	Panna
Marie	Maria	k1gFnSc2	Maria
držící	držící	k2eAgNnSc4d1	držící
tělo	tělo	k1gNnSc4	tělo
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
<g/>
,	,	kIx,	,
obklopená	obklopený	k2eAgNnPc4d1	obklopené
Máří	Máří	k?	Máří
Magdalénou	Magdaléna	k1gFnSc7	Magdaléna
a	a	k8xC	a
Marií	Maria	k1gFnSc7	Maria
Josefovou	Josefův	k2eAgFnSc7d1	Josefova
<g/>
.	.	kIx.	.
</s>
<s>
Nejvrchnější	vrchní	k2eAgInSc1d3	nejvrchnější
reliéf	reliéf	k1gInSc1	reliéf
zpodobňuje	zpodobňovat	k5eAaImIp3nS	zpodobňovat
anděly	anděl	k1gMnPc4	anděl
držící	držící	k2eAgFnSc4d1	držící
Veroničinu	Veroničin	k2eAgFnSc4d1	Veroničina
roušku	rouška	k1gFnSc4	rouška
s	s	k7c7	s
otiskem	otisk	k1gInSc7	otisk
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
symbolika	symbolika	k1gFnSc1	symbolika
portálu	portál	k1gInSc2	portál
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
tradicí	tradice	k1gFnSc7	tradice
Svaté	svatý	k2eAgFnSc2d1	svatá
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
výklenků	výklenek	k1gInPc2	výklenek
ústřední	ústřední	k2eAgFnSc2d1	ústřední
brány	brána	k1gFnSc2	brána
byly	být	k5eAaImAgFnP	být
usazeny	usadit	k5eAaPmNgFnP	usadit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dvě	dva	k4xCgFnPc1	dva
neogotické	ogotický	k2eNgFnPc1d1	neogotická
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
–	–	k?	–
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jižní	jižní	k2eAgInSc1d1	jižní
portál	portál	k1gInSc1	portál
===	===	k?	===
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
portál	portál	k1gInSc1	portál
se	se	k3xPyFc4	se
od	od	k7c2	od
předešlých	předešlý	k2eAgFnPc2d1	předešlá
dvou	dva	k4xCgFnPc2	dva
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zakomponován	zakomponovat	k5eAaPmNgMnS	zakomponovat
do	do	k7c2	do
předsíně	předsíň	k1gFnSc2	předsíň
pod	pod	k7c7	pod
královskou	královský	k2eAgFnSc7d1	královská
emporou	empora	k1gFnSc7	empora
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
severní	severní	k2eAgInSc1d1	severní
portál	portál	k1gInSc1	portál
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
brány	brána	k1gFnPc4	brána
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
figurativní	figurativní	k2eAgInPc4d1	figurativní
reliéfy	reliéf	k1gInPc4	reliéf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
ukončené	ukončená	k1gFnPc1	ukončená
sedly	sednout	k5eAaPmAgFnP	sednout
s	s	k7c7	s
trojúhelníkovými	trojúhelníkový	k2eAgInPc7d1	trojúhelníkový
kružbovými	kružbový	k2eAgInPc7d1	kružbový
štíty	štít	k1gInPc7	štít
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterými	který	k3yIgInPc7	který
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc1d1	další
řada	řada	k1gFnSc1	řada
sedel	sedlo	k1gNnPc2	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
tak	tak	k9	tak
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
dojem	dojem	k1gInSc4	dojem
trojitého	trojitý	k2eAgInSc2d1	trojitý
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
přicházelo	přicházet	k5eAaImAgNnS	přicházet
od	od	k7c2	od
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
městského	městský	k2eAgInSc2d1	městský
hřbitova	hřbitov	k1gInSc2	hřbitov
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncepčně	koncepčně	k6eAd1	koncepčně
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
prvkem	prvek	k1gInSc7	prvek
portálu	portál	k1gInSc2	portál
je	být	k5eAaImIp3nS	být
visutý	visutý	k2eAgInSc1d1	visutý
svorník	svorník	k1gInSc1	svorník
gotické	gotický	k2eAgFnSc2d1	gotická
klenby	klenba	k1gFnSc2	klenba
předsíně	předsíň	k1gFnSc2	předsíň
s	s	k7c7	s
listovou	listový	k2eAgFnSc7d1	listová
ornamentikou	ornamentika	k1gFnSc7	ornamentika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
inspiraci	inspirace	k1gFnSc4	inspirace
košických	košický	k2eAgMnPc2d1	košický
mistrů	mistr	k1gMnPc2	mistr
pražskou	pražský	k2eAgFnSc7d1	Pražská
parléřovskou	parléřovská	k1gFnSc7	parléřovská
hutí	huť	k1gFnPc2	huť
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodným	pozoruhodný	k2eAgMnSc7d1	pozoruhodný
je	být	k5eAaImIp3nS	být
i	i	k9	i
baldachýn	baldachýn	k1gInSc1	baldachýn
středověké	středověký	k2eAgFnSc2d1	středověká
sochy	socha	k1gFnSc2	socha
portálu	portál	k1gInSc2	portál
tvořený	tvořený	k2eAgMnSc1d1	tvořený
létajícími	létající	k2eAgMnPc7d1	létající
havrany	havran	k1gMnPc7	havran
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
havran	havran	k1gMnSc1	havran
a	a	k8xC	a
bestie	bestie	k1gFnSc1	bestie
drží	držet	k5eAaImIp3nS	držet
nástroje	nástroj	k1gInPc4	nástroj
umučení	umučení	k1gNnSc2	umučení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
baldachýnem	baldachýn	k1gInSc7	baldachýn
stála	stát	k5eAaImAgFnS	stát
původně	původně	k6eAd1	původně
socha	socha	k1gFnSc1	socha
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tu	tu	k6eAd1	tu
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
Immaculaty	Immacule	k1gNnPc7	Immacule
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Jána	Ján	k1gMnSc2	Ján
Marschalka	Marschalka	k1gFnSc1	Marschalka
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
neogotickými	ogotický	k2eNgFnPc7d1	neogotická
sochami	socha	k1gFnPc7	socha
portálu	portál	k1gInSc2	portál
jsou	být	k5eAaImIp3nP	být
světci	světec	k1gMnPc1	světec
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
Uherská	uherský	k2eAgFnSc1d1	uherská
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
apoštol	apoštol	k1gMnSc1	apoštol
Ondřej	Ondřej	k1gMnSc1	Ondřej
patron	patron	k1gMnSc1	patron
košické	košický	k2eAgFnSc2d1	Košická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
čánadský	čánadský	k2eAgMnSc1d1	čánadský
biskup	biskup	k1gMnSc1	biskup
Gerhard	Gerhard	k1gMnSc1	Gerhard
<g/>
,	,	kIx,	,
skotský	skotský	k2eAgMnSc1d1	skotský
kralevic	kralevic	k1gMnSc1	kralevic
Koloman	Koloman	k1gMnSc1	Koloman
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Xaverský	xaverský	k2eAgMnSc1d1	xaverský
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Teodor	Teodor	k1gMnSc1	Teodor
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
symbolikou	symbolika	k1gFnSc7	symbolika
portálu	portál	k1gInSc2	portál
nemají	mít	k5eNaImIp3nP	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
patrony	patrona	k1gFnPc1	patrona
donátorů	donátor	k1gMnPc2	donátor
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
římsou	římsa	k1gFnSc7	římsa
kamenné	kamenný	k2eAgFnSc2d1	kamenná
balustrády	balustráda	k1gFnSc2	balustráda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pět	pět	k4xCc1	pět
hlav	hlava	k1gFnPc2	hlava
restaurátorů	restaurátor	k1gMnPc2	restaurátor
chrámu	chrámat	k5eAaImIp1nS	chrámat
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc4d1	střední
portrét	portrét	k1gInSc4	portrét
patří	patřit	k5eAaImIp3nP	patřit
architektovy	architektův	k2eAgInPc1d1	architektův
Imrichu	Imrich	k1gInSc2	Imrich
Steindlovi	Steindlův	k2eAgMnPc1d1	Steindlův
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
patří	patřit	k5eAaImIp3nP	patřit
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Wilhelmovi	Wilhelm	k1gMnSc3	Wilhelm
Frödemu	Frödem	k1gMnSc3	Frödem
<g/>
,	,	kIx,	,
Imrichu	Imrich	k1gMnSc3	Imrich
Seidenovi	Seiden	k1gMnSc3	Seiden
<g/>
,	,	kIx,	,
Ľudovítu	Ľudovít	k1gMnSc3	Ľudovít
Steinhausovi	Steinhaus	k1gMnSc3	Steinhaus
a	a	k8xC	a
Ottovi	Otta	k1gMnSc3	Otta
Sztehlóvi	Sztehlóev	k1gFnSc3	Sztehlóev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
===	===	k?	===
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
věž	věž	k1gFnSc1	věž
stavěná	stavěný	k2eAgFnSc1d1	stavěná
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
výstavby	výstavba	k1gFnSc2	výstavba
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
letech	let	k1gInPc6	let
1420	[number]	k4	1420
<g/>
–	–	k?	–
<g/>
1440	[number]	k4	1440
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
za	za	k7c2	za
kapitanátu	kapitanát	k1gInSc2	kapitanát
Jana	Jan	k1gMnSc2	Jan
Jiskry	jiskra	k1gFnSc2	jiskra
z	z	k7c2	z
Brandýsa	Brandýs	k1gInSc2	Brandýs
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gFnPc4	její
zdi	zeď	k1gFnPc4	zeď
dovedli	dovést	k5eAaPmAgMnP	dovést
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
střešních	střešní	k2eAgFnPc2d1	střešní
říms	římsa	k1gFnPc2	římsa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
etapě	etapa	k1gFnSc6	etapa
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1462	[number]	k4	1462
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
tesaný	tesaný	k2eAgInSc1d1	tesaný
znak	znak	k1gInSc1	znak
Košic	Košice	k1gInPc2	Košice
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1462	[number]	k4	1462
nad	nad	k7c7	nad
portálem	portál	k1gInSc7	portál
západní	západní	k2eAgFnSc2d1	západní
fasády	fasáda	k1gFnSc2	fasáda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
vstupem	vstup	k1gInSc7	vstup
na	na	k7c4	na
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
půdorysu	půdorys	k1gInSc2	půdorys
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
se	se	k3xPyFc4	se
zeštíhluje	zeštíhlovat	k5eAaImIp3nS	zeštíhlovat
do	do	k7c2	do
osmiúhelníkového	osmiúhelníkový	k2eAgInSc2d1	osmiúhelníkový
půdorysu	půdorys	k1gInSc2	půdorys
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pravotočivé	pravotočivý	k2eAgNnSc1d1	pravotočivé
schodiště	schodiště	k1gNnSc1	schodiště
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
obvodových	obvodový	k2eAgFnPc2d1	obvodová
zdí	zeď	k1gFnPc2	zeď
třech	tři	k4xCgInPc2	tři
podlaží	podlaží	k1gNnPc2	podlaží
do	do	k7c2	do
postranní	postranní	k2eAgFnSc2d1	postranní
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgFnSc2d1	stejná
osmiúhelníkové	osmiúhelníkový	k2eAgFnSc2d1	osmiúhelníková
tubusové	tubusový	k2eAgFnSc2d1	tubusový
věžičky	věžička	k1gFnSc2	věžička
<g/>
.	.	kIx.	.
</s>
<s>
Stroze	stroze	k6eAd1	stroze
zdobená	zdobený	k2eAgFnSc1d1	zdobená
věž	věž	k1gFnSc1	věž
má	mít	k5eAaImIp3nS	mít
mezipodlažní	mezipodlažní	k2eAgFnPc4d1	mezipodlažní
římsy	římsa	k1gFnPc4	římsa
dekorované	dekorovaný	k2eAgFnPc4d1	dekorovaná
vlysem	vlys	k1gInSc7	vlys
s	s	k7c7	s
geometrickým	geometrický	k2eAgInSc7d1	geometrický
motivem	motiv	k1gInSc7	motiv
slepých	slepý	k2eAgFnPc2d1	slepá
kružeb	kružba	k1gFnPc2	kružba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
a	a	k8xC	a
pátým	pátý	k4xOgNnSc7	pátý
podlažím	podlaží	k1gNnSc7	podlaží
je	být	k5eAaImIp3nS	být
vlys	vlys	k1gInSc1	vlys
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
rozety	rozeta	k1gFnPc4	rozeta
–	–	k?	–
růžičky	růžička	k1gFnPc4	růžička
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
profilována	profilovat	k5eAaImNgFnS	profilovat
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
těžce	těžce	k6eAd1	těžce
poškozena	poškodit	k5eAaPmNgFnS	poškodit
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Košic	Košice	k1gInPc2	Košice
vojskem	vojsko	k1gNnSc7	vojsko
Jana	Jan	k1gMnSc2	Jan
I.	I.	kA	I.
Olbrachta	Olbracht	k1gMnSc2	Olbracht
v	v	k7c6	v
letech	let	k1gInPc6	let
1490	[number]	k4	1490
<g/>
–	–	k?	–
<g/>
1491	[number]	k4	1491
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc4	věž
opravoval	opravovat	k5eAaImAgMnS	opravovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1494	[number]	k4	1494
<g/>
–	–	k?	–
<g/>
1497	[number]	k4	1497
stavitel	stavitel	k1gMnSc1	stavitel
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Crompholz	Crompholz	k1gMnSc1	Crompholz
z	z	k7c2	z
Nisy	Nisa	k1gFnSc2	Nisa
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
věž	věž	k1gFnSc4	věž
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
instalovány	instalován	k2eAgFnPc4d1	instalována
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
mírně	mírně	k6eAd1	mírně
nadstavěna	nadstavěn	k2eAgFnSc1d1	nadstavěn
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
vrchol	vrchol	k1gInSc4	vrchol
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
rokoková	rokokový	k2eAgFnSc1d1	rokoková
přilbice	přilbice	k1gFnSc1	přilbice
s	s	k7c7	s
ochozem	ochoz	k1gInSc7	ochoz
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
šesté	šestý	k4xOgNnSc1	šestý
poschodí	poschodí	k1gNnSc1	poschodí
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Přilbice	přilbice	k1gFnSc1	přilbice
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
měděným	měděný	k2eAgInSc7d1	měděný
plechem	plech	k1gInSc7	plech
s	s	k7c7	s
pozlacenými	pozlacený	k2eAgInPc7d1	pozlacený
klempířskými	klempířský	k2eAgInPc7d1	klempířský
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
měděný	měděný	k2eAgInSc1d1	měděný
kříž	kříž	k1gInSc1	kříž
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
věž	věž	k1gFnSc1	věž
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výška	výška	k1gFnSc1	výška
se	s	k7c7	s
160	[number]	k4	160
schody	schod	k1gInPc7	schod
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
59,7	[number]	k4	59,7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
poschodí	poschodí	k1gNnSc6	poschodí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mechanismus	mechanismus	k1gInSc1	mechanismus
velkých	velký	k2eAgFnPc2d1	velká
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konstrukce	konstrukce	k1gFnSc2	konstrukce
na	na	k7c4	na
upevnění	upevnění	k1gNnSc4	upevnění
zvonů	zvon	k1gInPc2	zvon
<g/>
,	,	kIx,	,
na	na	k7c6	na
třetím	třetí	k4xOgMnSc6	třetí
dva	dva	k4xCgInPc1	dva
zvony	zvon	k1gInPc1	zvon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
zvony	zvon	k1gInPc1	zvon
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
5	[number]	k4	5
kusů	kus	k1gInPc2	kus
byly	být	k5eAaImAgFnP	být
roztaveny	roztavit	k5eAaPmNgFnP	roztavit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
použity	použít	k5eAaPmNgFnP	použít
k	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
odlil	odlít	k5eAaPmAgMnS	odlít
Alexandr	Alexandr	k1gMnSc1	Alexandr
Buchner	Buchner	k1gMnSc1	Buchner
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
od	od	k7c2	od
věřících	věřící	k1gFnPc2	věřící
nové	nový	k2eAgInPc1d1	nový
zvony	zvon	k1gInPc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Zvon	zvon	k1gInSc1	zvon
Božské	božská	k1gFnSc2	božská
srdce	srdce	k1gNnSc2	srdce
vážící	vážící	k2eAgFnSc1d1	vážící
1	[number]	k4	1
530	[number]	k4	530
kg	kg	kA	kg
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
plášti	plášť	k1gInSc6	plášť
vypsána	vypsán	k2eAgFnSc1d1	vypsána
jména	jméno	k1gNnPc1	jméno
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zvon	zvon	k1gInSc1	zvon
svatého	svatý	k2eAgMnSc2d1	svatý
Ondřeje	Ondřej	k1gMnSc2	Ondřej
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
košické	košický	k2eAgFnSc2d1	Košická
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
450	[number]	k4	450
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Dvojici	dvojice	k1gFnSc4	dvojice
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
umíráček	umíráček	k1gInSc1	umíráček
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
40	[number]	k4	40
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
poschodí	poschodí	k1gNnSc6	poschodí
je	být	k5eAaImIp3nS	být
instalovaná	instalovaný	k2eAgFnSc1d1	instalovaná
část	část	k1gFnSc1	část
hodinového	hodinový	k2eAgInSc2d1	hodinový
stroje	stroj	k1gInSc2	stroj
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
následně	následně	k6eAd1	následně
lanové	lanový	k2eAgNnSc4d1	lanové
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
místnost	místnost	k1gFnSc1	místnost
hasičské	hasičský	k2eAgFnSc2d1	hasičská
strážní	strážní	k2eAgFnSc2d1	strážní
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
až	až	k6eAd1	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Matyášova	Matyášův	k2eAgFnSc1d1	Matyášova
věž	věž	k1gFnSc1	věž
===	===	k?	===
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc4d1	jižní
věž	věž	k1gFnSc4	věž
začali	začít	k5eAaPmAgMnP	začít
stavět	stavět	k5eAaImF	stavět
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
výstavby	výstavba	k1gFnSc2	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
letech	let	k1gInPc6	let
1420	[number]	k4	1420
<g/>
–	–	k?	–
<g/>
1440	[number]	k4	1440
zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
severní	severní	k2eAgFnSc7d1	severní
věží	věž	k1gFnSc7	věž
taktéž	taktéž	k?	taktéž
na	na	k7c6	na
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
půdoryse	půdorys	k1gInSc6	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přerušení	přerušení	k1gNnSc6	přerušení
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
pracovat	pracovat	k5eAaImF	pracovat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1462	[number]	k4	1462
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
severní	severní	k2eAgFnSc1d1	severní
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
vedl	vést	k5eAaImAgMnS	vést
mistr	mistr	k1gMnSc1	mistr
Štěpán	Štěpán	k1gMnSc1	Štěpán
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
ozdobnějším	ozdobný	k2eAgMnSc7d2	ozdobný
a	a	k8xC	a
masivnějším	masivní	k2eAgInSc7d2	masivnější
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
severní	severní	k2eAgFnSc1d1	severní
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nebyla	být	k5eNaImAgFnS	být
dovedena	dovést	k5eAaPmNgFnS	dovést
do	do	k7c2	do
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Zakončena	zakončen	k2eAgFnSc1d1	zakončena
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
korunní	korunní	k2eAgFnSc2d1	korunní
římsy	římsa	k1gFnSc2	římsa
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodě	loď	k1gFnSc2	loď
dekorativním	dekorativní	k2eAgInSc7d1	dekorativní
ochozovým	ochozový	k2eAgInSc7d1	ochozový
věncem	věnec	k1gInSc7	věnec
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
pole	pole	k1gNnSc4	pole
se	se	k3xPyFc4	se
znaky	znak	k1gInPc7	znak
zemí	zem	k1gFnPc2	zem
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
a	a	k8xC	a
znakem	znak	k1gInSc7	znak
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zakrytá	zakrytý	k2eAgFnSc1d1	zakrytá
osmiúhelníkovou	osmiúhelníkový	k2eAgFnSc7d1	osmiúhelníková
plechovou	plechový	k2eAgFnSc7d1	plechová
stříškou	stříška	k1gFnSc7	stříška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
labyrint	labyrint	k1gInSc4	labyrint
točitých	točitý	k2eAgNnPc2d1	točité
schodišť	schodiště	k1gNnPc2	schodiště
navzájem	navzájem	k6eAd1	navzájem
propojených	propojený	k2eAgInPc2d1	propojený
chodbičkami	chodbička	k1gFnPc7	chodbička
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
význam	význam	k1gInSc1	význam
není	být	k5eNaImIp3nS	být
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
spolehlivě	spolehlivě	k6eAd1	spolehlivě
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
za	za	k7c2	za
západního	západní	k2eAgInSc2d1	západní
chóru	chór	k1gInSc2	chór
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
schodiště	schodiště	k1gNnSc1	schodiště
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rovněž	rovněž	k9	rovněž
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
varhanám	varhany	k1gFnPc3	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
původní	původní	k2eAgFnSc1d1	původní
kamenická	kamenický	k2eAgFnSc1d1	kamenická
výzdoba	výzdoba	k1gFnSc1	výzdoba
exteriéru	exteriér	k1gInSc2	exteriér
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
plastikami	plastika	k1gFnPc7	plastika
stoupenců	stoupenec	k1gMnPc2	stoupenec
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
v	v	k7c6	v
nárožních	nárožní	k2eAgInPc6d1	nárožní
výklencích	výklenek	k1gInPc6	výklenek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
od	od	k7c2	od
budapešťského	budapešťský	k2eAgMnSc2d1	budapešťský
sochaře	sochař	k1gMnSc2	sochař
Františka	František	k1gMnSc2	František
Mikula	Mikula	k1gMnSc1	Mikula
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
Štěpán	Štěpán	k1gMnSc1	Štěpán
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
<g/>
,	,	kIx,	,
Sebastian	Sebastian	k1gMnSc1	Sebastian
Rozgoni	Rozgoň	k1gFnSc6	Rozgoň
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Vitéz	Vitéza	k1gFnPc2	Vitéza
ze	z	k7c2	z
Sredny	Sredna	k1gFnSc2	Sredna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sluneční	sluneční	k2eAgFnPc4d1	sluneční
hodiny	hodina	k1gFnPc4	hodina
===	===	k?	===
</s>
</p>
<p>
<s>
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
jižní	jižní	k2eAgFnSc2d1	jižní
fasády	fasáda	k1gFnSc2	fasáda
chrámu	chrám	k1gInSc2	chrám
nad	nad	k7c7	nad
největším	veliký	k2eAgNnSc7d3	veliký
oknem	okno	k1gNnSc7	okno
Kaple	kaple	k1gFnSc2	kaple
Mettercie	Mettercie	k1gFnSc2	Mettercie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
svislé	svislý	k2eAgFnSc2d1	svislá
sluneční	sluneční	k2eAgFnSc2d1	sluneční
hodiny	hodina	k1gFnSc2	hodina
půlorlojového	půlorlojový	k2eAgInSc2d1	půlorlojový
typu	typ	k1gInSc2	typ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
goticko-arabské	gotickorabský	k2eAgFnPc4d1	goticko-arabský
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
čas	čas	k1gInSc4	čas
od	od	k7c2	od
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
večerní	večerní	k2eAgFnSc2d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
poškozené	poškozený	k2eAgMnPc4d1	poškozený
<g/>
,	,	kIx,	,
rekonstruovány	rekonstruován	k2eAgFnPc1d1	rekonstruována
byly	být	k5eAaImAgFnP	být
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chrliče	chrlič	k1gInPc4	chrlič
===	===	k?	===
</s>
</p>
<p>
<s>
Mohutná	mohutný	k2eAgFnSc1d1	mohutná
korunní	korunní	k2eAgFnSc1d1	korunní
římsa	římsa	k1gFnSc1	římsa
střešní	střešní	k2eAgFnSc2d1	střešní
úrovně	úroveň	k1gFnSc2	úroveň
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
kamennou	kamenný	k2eAgFnSc7d1	kamenná
balustrádou	balustráda	k1gFnSc7	balustráda
je	být	k5eAaImIp3nS	být
odvodňována	odvodňován	k2eAgFnSc1d1	odvodňován
pomocí	pomocí	k7c2	pomocí
početných	početný	k2eAgInPc2d1	početný
chrličů	chrlič	k1gInPc2	chrlič
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
zoomorfní	zoomorfní	k2eAgInPc1d1	zoomorfní
chrliče	chrlič	k1gInPc1	chrlič
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
původních	původní	k2eAgInPc2d1	původní
kusů	kus	k1gInPc2	kus
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Východoslovenského	východoslovenský	k2eAgNnSc2d1	Východoslovenské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
Zemplínského	Zemplínský	k2eAgNnSc2d1	Zemplínský
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Michalovcích	Michalovce	k1gInPc6	Michalovce
a	a	k8xC	a
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Kreuzenstein	Kreuzensteina	k1gFnPc2	Kreuzensteina
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
chrliče	chrlič	k1gInPc1	chrlič
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
originálních	originální	k2eAgFnPc2d1	originální
předloh	předloha	k1gFnPc2	předloha
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
i	i	k9	i
několik	několik	k4yIc1	několik
nejnovějších	nový	k2eAgInPc2d3	nejnovější
chrličů	chrlič	k1gInPc2	chrlič
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
při	při	k7c6	při
renovaci	renovace	k1gFnSc6	renovace
pilířového	pilířový	k2eAgInSc2d1	pilířový
systému	systém	k1gInSc2	systém
svatyně	svatyně	k1gFnSc2	svatyně
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
odvodu	odvod	k1gInSc2	odvod
dešťové	dešťový	k2eAgFnPc1d1	dešťová
vody	voda	k1gFnPc1	voda
měly	mít	k5eAaImAgFnP	mít
chrliče	chrlič	k1gInPc4	chrlič
rovněž	rovněž	k9	rovněž
odstrašující	odstrašující	k2eAgFnSc4d1	odstrašující
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zvířecích	zvířecí	k2eAgInPc2d1	zvířecí
motivů	motiv	k1gInPc2	motiv
(	(	kIx(	(
<g/>
nohy	noha	k1gFnSc2	noha
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
krkavce	krkavec	k1gMnSc2	krkavec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
motivy	motiv	k1gInPc4	motiv
příšer	příšera	k1gFnPc2	příšera
(	(	kIx(	(
<g/>
draci	drak	k1gMnPc1	drak
<g/>
,	,	kIx,	,
vlkodlaci	vlkodlak	k1gMnPc1	vlkodlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
chrlič	chrlič	k1gInSc1	chrlič
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
fasádě	fasáda	k1gFnSc6	fasáda
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
spodobněna	spodobněn	k2eAgFnSc1d1	spodobněn
opilá	opilý	k2eAgFnSc1d1	opilá
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
pohárem	pohár	k1gInSc7	pohár
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
schovávající	schovávající	k2eAgFnSc1d1	schovávající
láhev	láhev	k1gFnSc1	láhev
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
chrliči	chrlič	k1gInSc3	chrlič
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
nechal	nechat	k5eAaPmAgMnS	nechat
zvěčnit	zvěčnit	k5eAaPmF	zvěčnit
a	a	k8xC	a
potrestat	potrestat	k5eAaPmF	potrestat
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
mistr	mistr	k1gMnSc1	mistr
Štěpán	Štěpán	k1gMnSc1	Štěpán
za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
opilství	opilství	k1gNnSc4	opilství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sanktusník	sanktusník	k1gInSc4	sanktusník
===	===	k?	===
</s>
</p>
<p>
<s>
Věžička	věžička	k1gFnSc1	věžička
na	na	k7c6	na
křížení	křížení	k1gNnSc6	křížení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
a	a	k8xC	a
příčné	příčný	k2eAgFnSc2d1	příčná
lodě	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
neogotického	ogotický	k2eNgNnSc2d1	neogotické
zkrášlování	zkrášlování	k1gNnSc2	zkrášlování
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
chrámu	chrám	k1gInSc2	chrám
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
ji	on	k3xPp3gFnSc4	on
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
oplechována	oplechovat	k5eAaPmNgFnS	oplechovat
mědí	měď	k1gFnSc7	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Památník	památník	k1gInSc1	památník
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
druhů	druh	k1gInPc2	druh
přeneseny	přenést	k5eAaPmNgFnP	přenést
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
Košic	Košice	k1gInPc2	Košice
a	a	k8xC	a
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
katedrální	katedrální	k2eAgFnSc6d1	katedrální
kryptě	krypta	k1gFnSc6	krypta
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1938	[number]	k4	1938
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
první	první	k4xOgFnSc7	první
vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
arbitráží	arbitráž	k1gFnSc7	arbitráž
a	a	k8xC	a
připojením	připojení	k1gNnSc7	připojení
Košic	Košice	k1gInPc2	Košice
k	k	k7c3	k
horthyovskému	horthyovský	k2eAgNnSc3d1	horthyovský
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
památník	památník	k1gInSc1	památník
vůdci	vůdce	k1gMnSc3	vůdce
kuruckého	kurucký	k2eAgNnSc2d1	kurucké
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Sipos	Sipos	k1gMnSc1	Sipos
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Löffler	Löffler	k1gMnSc1	Löffler
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
ho	on	k3xPp3gInSc2	on
odlil	odlít	k5eAaPmAgMnS	odlít
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Buchner	Buchner	k1gMnSc1	Buchner
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
památníku	památník	k1gInSc6	památník
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
nápis	nápis	k1gInSc1	nápis
psaný	psaný	k2eAgInSc1d1	psaný
neutrálním	neutrální	k2eAgNnSc7d1	neutrální
latinským	latinský	k2eAgNnSc7d1	latinské
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
deponován	deponován	k2eAgMnSc1d1	deponován
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
a	a	k8xC	a
obnovený	obnovený	k2eAgMnSc1d1	obnovený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
překlad	překlad	k1gInSc1	překlad
zní	znět	k5eAaImIp3nS	znět
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
této	tento	k3xDgFnSc2	tento
starobylé	starobylý	k2eAgFnSc2d1	starobylá
katedrály	katedrála	k1gFnSc2	katedrála
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
kosti	kost	k1gFnSc2	kost
knížete	kníže	k1gMnSc2	kníže
Františka	František	k1gMnSc2	František
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
matky	matka	k1gFnPc1	matka
Heleny	Helena	k1gFnSc2	Helena
Zrinské	Zrinská	k1gFnSc2	Zrinská
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
syna	syn	k1gMnSc4	syn
Josefa	Josef	k1gMnSc4	Josef
a	a	k8xC	a
třech	tři	k4xCgInPc2	tři
věrných	věrný	k2eAgMnPc2d1	věrný
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vděčné	vděčný	k2eAgNnSc4d1	vděčné
potomstvo	potomstvo	k1gNnSc4	potomstvo
po	po	k7c6	po
dvousetletém	dvousetletý	k2eAgNnSc6d1	dvousetleté
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
uvaleném	uvalený	k2eAgNnSc6d1	uvalené
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nepříznivým	příznivý	k2eNgInSc7d1	nepříznivý
osudem	osud	k1gInSc7	osud
<g/>
,	,	kIx,	,
pietně	pietně	k6eAd1	pietně
přineslo	přinést	k5eAaPmAgNnS	přinést
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1906	[number]	k4	1906
s	s	k7c7	s
pohřebními	pohřební	k2eAgInPc7d1	pohřební
obřady	obřad	k1gInPc7	obřad
zbožně	zbožně	k6eAd1	zbožně
a	a	k8xC	a
důstojně	důstojně	k6eAd1	důstojně
uložilo	uložit	k5eAaPmAgNnS	uložit
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
posvátném	posvátný	k2eAgNnSc6d1	posvátné
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
pokojný	pokojný	k2eAgInSc4d1	pokojný
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Postůjte	postát	k5eAaPmRp2nP	postát
<g/>
,	,	kIx,	,
poutníci	poutník	k1gMnPc5	poutník
<g/>
,	,	kIx,	,
sláva	sláva	k1gFnSc1	sláva
těchto	tento	k3xDgMnPc2	tento
hrdinů	hrdina	k1gMnPc2	hrdina
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
památka	památka	k1gFnSc1	památka
skvělá	skvělý	k2eAgFnSc1d1	skvělá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
památník	památník	k1gInSc1	památník
dal	dát	k5eAaPmAgInS	dát
zřídit	zřídit	k5eAaPmF	zřídit
senát	senát	k1gInSc1	senát
a	a	k8xC	a
lid	lid	k1gInSc1	lid
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
léta	léto	k1gNnSc2	léto
Páně	páně	k2eAgNnSc2d1	páně
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
katedrály	katedrála	k1gFnSc2	katedrála
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Panovnické	panovnický	k2eAgFnSc2d1	panovnická
donace	donace	k1gFnSc2	donace
===	===	k?	===
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
budovaný	budovaný	k2eAgInSc1d1	budovaný
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
Košic	Košice	k1gInPc2	Košice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
200	[number]	k4	200
m2	m2	k4	m2
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
kostelem	kostel	k1gInSc7	kostel
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc2	novověk
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgInSc7d3	veliký
chrámem	chrám	k1gInSc7	chrám
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prestižní	prestižní	k2eAgFnSc7d1	prestižní
záležitostí	záležitost	k1gFnSc7	záležitost
košických	košický	k2eAgMnPc2d1	košický
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
,	,	kIx,	,
bohatých	bohatý	k2eAgMnPc2d1	bohatý
kupců	kupec	k1gMnPc2	kupec
a	a	k8xC	a
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
,	,	kIx,	,
symbolem	symbol	k1gInSc7	symbol
jejich	jejich	k3xOp3gFnSc2	jejich
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
reprezentativního	reprezentativní	k2eAgInSc2d1	reprezentativní
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
městě	město	k1gNnSc6	město
hospodářsky	hospodářsky	k6eAd1	hospodářsky
mimořádně	mimořádně	k6eAd1	mimořádně
rozvinutých	rozvinutý	k2eAgFnPc2d1	rozvinutá
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
byla	být	k5eAaImAgFnS	být
podporována	podporován	k2eAgFnSc1d1	podporována
uherskými	uherský	k2eAgMnPc7d1	uherský
králi	král	k1gMnPc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
kostel	kostel	k1gInSc1	kostel
stavět	stavět	k5eAaImF	stavět
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
polovinu	polovina	k1gFnSc4	polovina
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
košické	košický	k2eAgFnSc2d1	Košická
kontribuce	kontribuce	k1gFnSc2	kontribuce
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
obdařil	obdařit	k5eAaPmAgMnS	obdařit
Košice	Košice	k1gInPc4	Košice
mnohými	mnohý	k2eAgNnPc7d1	mnohé
privilegii	privilegium	k1gNnPc7	privilegium
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nepřímo	přímo	k6eNd1	přímo
napomáhaly	napomáhat	k5eAaBmAgFnP	napomáhat
financování	financování	k1gNnSc4	financování
stavby	stavba	k1gFnSc2	stavba
z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
pokladnice	pokladnice	k1gFnSc2	pokladnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
první	první	k4xOgFnSc4	první
stavební	stavební	k2eAgFnSc4d1	stavební
huť	huť	k1gFnSc4	huť
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
mistři	mistr	k1gMnPc1	mistr
byli	být	k5eAaImAgMnP	být
pozváni	pozvat	k5eAaPmNgMnP	pozvat
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
Zikmundovy	Zikmundův	k2eAgFnSc2d1	Zikmundova
rezidence	rezidence	k1gFnSc2	rezidence
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1468	[number]	k4	1468
věnoval	věnovat	k5eAaImAgInS	věnovat
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
novoroční	novoroční	k2eAgInPc4d1	novoroční
dary	dar	k1gInPc4	dar
od	od	k7c2	od
města	město	k1gNnSc2	město
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
této	tento	k3xDgFnSc2	tento
lhůty	lhůta	k1gFnSc2	lhůta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1472	[number]	k4	1472
bylo	být	k5eAaImAgNnS	být
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
prodloužení	prodloužení	k1gNnSc1	prodloužení
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
věnoval	věnovat	k5eAaPmAgMnS	věnovat
na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1476	[number]	k4	1476
celý	celý	k2eAgInSc4d1	celý
výnos	výnos	k1gInSc4	výnos
ze	z	k7c2	z
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Panovníkům	panovník	k1gMnPc3	panovník
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
dobrém	dobrý	k2eAgNnSc6d1	dobré
udržování	udržování	k1gNnSc6	udržování
stavby	stavba	k1gFnSc2	stavba
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
poškozen	poškodit	k5eAaPmNgInS	poškodit
i	i	k9	i
chrám	chrám	k1gInSc1	chrám
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Košice	Košice	k1gInPc1	Košice
od	od	k7c2	od
placení	placení	k1gNnSc2	placení
Daní	danit	k5eAaImIp3nS	danit
na	na	k7c4	na
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
z	z	k7c2	z
košické	košický	k2eAgFnSc2d1	Košická
kontribuce	kontribuce	k1gFnSc2	kontribuce
určil	určit	k5eAaPmAgInS	určit
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
Košic	Košice	k1gInPc2	Košice
"	"	kIx"	"
<g/>
v	v	k7c6	v
předešlé	předešlý	k2eAgFnSc6d1	předešlá
nádheře	nádhera	k1gFnSc6	nádhera
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1675	[number]	k4	1675
osobně	osobně	k6eAd1	osobně
intervenoval	intervenovat	k5eAaImAgMnS	intervenovat
u	u	k7c2	u
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
chrám	chrám	k1gInSc4	chrám
opravila	opravit	k5eAaPmAgFnS	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
nemá	mít	k5eNaImIp3nS	mít
dvojníka	dvojník	k1gMnSc4	dvojník
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Fábryho	Fábry	k1gMnSc2	Fábry
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
katedrály	katedrála	k1gFnSc2	katedrála
přispěl	přispět	k5eAaPmAgMnS	přispět
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
opravu	oprava	k1gFnSc4	oprava
císařský	císařský	k2eAgInSc4d1	císařský
manželský	manželský	k2eAgInSc4d1	manželský
pár	pár	k1gInSc4	pár
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Košic	Košice	k1gInPc2	Košice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
věnoval	věnovat	k5eAaImAgMnS	věnovat
5	[number]	k4	5
000	[number]	k4	000
a	a	k8xC	a
císařovna	císařovna	k1gFnSc1	císařovna
600	[number]	k4	600
forintů	forint	k1gInPc2	forint
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
nechala	nechat	k5eAaPmAgFnS	nechat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
zhotovit	zhotovit	k5eAaPmF	zhotovit
památní	památní	k2eAgFnSc4d1	památní
tabuli	tabule	k1gFnSc4	tabule
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
opřena	opřít	k5eAaPmNgFnS	opřít
o	o	k7c4	o
severní	severní	k2eAgFnSc4d1	severní
stěnu	stěna	k1gFnSc4	stěna
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
jejího	její	k3xOp3gInSc2	její
latinského	latinský	k2eAgInSc2d1	latinský
textu	text	k1gInSc2	text
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gNnSc4	jeho
Veličenstvo	veličenstvo	k1gNnSc4	veličenstvo
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
apoštolský	apoštolský	k2eAgMnSc1d1	apoštolský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c4	v
blaho	blaho	k1gNnSc4	blaho
svých	svůj	k3xOyFgInPc2	svůj
národů	národ	k1gInPc2	národ
navštívil	navštívit	k5eAaPmAgMnS	navštívit
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1852	[number]	k4	1852
a	a	k8xC	a
1857	[number]	k4	1857
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
Boží	boží	k2eAgFnSc7d1	boží
pomocí	pomoc	k1gFnSc7	pomoc
i	i	k8xC	i
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
opravu	oprava	k1gFnSc4	oprava
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jejím	její	k3xOp3gNnSc7	její
veličenstvem	veličenstvo	k1gNnSc7	veličenstvo
císařovnou	císařovna	k1gFnSc7	císařovna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
zbožně	zbožně	k6eAd1	zbožně
<g/>
,	,	kIx,	,
milostivě	milostivě	k6eAd1	milostivě
a	a	k8xC	a
velkodušně	velkodušně	k6eAd1	velkodušně
přispěl	přispět	k5eAaPmAgMnS	přispět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Košice	Košice	k1gInPc1	Košice
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
chrám	chrám	k1gInSc1	chrám
nebyl	být	k5eNaImAgInS	být
původně	původně	k6eAd1	původně
budován	budovat	k5eAaImNgInS	budovat
jako	jako	k8xS	jako
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
velkolepou	velkolepý	k2eAgFnSc7d1	velkolepá
koncepcí	koncepce	k1gFnSc7	koncepce
předčil	předčít	k5eAaPmAgInS	předčít
svatostánky	svatostánek	k1gInPc1	svatostánek
biskupských	biskupský	k2eAgNnPc2d1	Biskupské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Urychlení	urychlení	k1gNnSc1	urychlení
jeho	jeho	k3xOp3gFnSc2	jeho
výstavby	výstavba	k1gFnSc2	výstavba
aktivně	aktivně	k6eAd1	aktivně
podporovala	podporovat	k5eAaImAgFnS	podporovat
i	i	k9	i
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
odpustkovou	odpustkový	k2eAgFnSc4d1	odpustková
bulu	bula	k1gFnSc4	bula
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
písemným	písemný	k2eAgInSc7d1	písemný
pramenem	pramen	k1gInSc7	pramen
o	o	k7c6	o
chrámu	chrám	k1gInSc6	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Bula	bula	k1gFnSc1	bula
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
Košice	Košice	k1gInPc4	Košice
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
kostel	kostel	k1gInSc1	kostel
jako	jako	k8xC	jako
poutní	poutní	k2eAgNnSc1d1	poutní
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
slavná	slavný	k2eAgFnSc1d1	slavná
krev	krev	k1gFnSc1	krev
našeho	náš	k3xOp1gMnSc2	náš
Pána	pán	k1gMnSc2	pán
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
odpradávna	odpradávna	k6eAd1	odpradávna
přítomná	přítomný	k2eAgFnSc1d1	přítomná
zázračným	zázračný	k2eAgInSc7d1	zázračný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
veliké	veliký	k2eAgNnSc1d1	veliké
množství	množství	k1gNnSc1	množství
nevěřících	věřící	k2eNgMnPc2d1	nevěřící
sousedních	sousední	k2eAgMnPc2d1	sousední
Valachů	Valach	k1gMnPc2	Valach
a	a	k8xC	a
Rusínů	Rusín	k1gMnPc2	Rusín
pro	pro	k7c4	pro
často	často	k6eAd1	často
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
zázraky	zázrak	k1gInPc1	zázrak
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nevěřících	nevěřící	k1gMnPc2	nevěřící
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
přestoupili	přestoupit	k5eAaPmAgMnP	přestoupit
a	a	k8xC	a
každodenně	každodenně	k6eAd1	každodenně
přestupují	přestupovat	k5eAaImIp3nP	přestupovat
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
víru	víra	k1gFnSc4	víra
</s>
</p>
<p>
<s>
Účelem	účel	k1gInSc7	účel
buly	buly	k1gNnSc2	buly
bylo	být	k5eAaImAgNnS	být
udělení	udělení	k1gNnSc4	udělení
odpustků	odpustek	k1gInPc2	odpustek
těm	ten	k3xDgMnPc3	ten
hříšníkům	hříšník	k1gMnPc3	hříšník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vykonali	vykonat	k5eAaPmAgMnP	vykonat
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
přispěli	přispět	k5eAaPmAgMnP	přispět
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
dokončení	dokončení	k1gNnSc6	dokončení
<g/>
.	.	kIx.	.
</s>
<s>
Poutníci	poutník	k1gMnPc1	poutník
měli	mít	k5eAaImAgMnP	mít
dostat	dostat	k5eAaPmF	dostat
takové	takový	k3xDgNnSc4	takový
rozhřešení	rozhřešení	k1gNnSc4	rozhřešení
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
těm	ten	k3xDgMnPc3	ten
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Chrámu	chrámat	k5eAaImIp1nS	chrámat
svatého	svatý	k2eAgMnSc4d1	svatý
Marka	Marek	k1gMnSc4	Marek
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Kapli	kaple	k1gFnSc6	kaple
Porcinkuly	Porcinkula	k1gFnSc2	Porcinkula
v	v	k7c6	v
Assisi	Assise	k1gFnSc6	Assise
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
tehdejším	tehdejší	k2eAgNnPc3d1	tehdejší
poutním	poutní	k2eAgNnPc3d1	poutní
místům	místo	k1gNnPc3	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Cáchy	Cáchy	k1gFnPc1	Cáchy
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Mohuč	Mohuč	k1gFnSc1	Mohuč
<g/>
,	,	kIx,	,
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Compostela	Compostela	k1gFnSc1	Compostela
<g/>
,	,	kIx,	,
Darnov	Darnov	k1gInSc1	Darnov
či	či	k8xC	či
Oradea	Oradea	k1gFnSc1	Oradea
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
relikviích	relikvie	k1gFnPc6	relikvie
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc1	žádný
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tradice	tradice	k1gFnSc1	tradice
poutě	pouť	k1gFnSc2	pouť
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
farním	farní	k2eAgInSc6d1	farní
úřadě	úřad	k1gInSc6	úřad
se	se	k3xPyFc4	se
však	však	k9	však
zachoval	zachovat	k5eAaPmAgInS	zachovat
barokní	barokní	k2eAgInSc1d1	barokní
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
modlitbou	modlitba	k1gFnSc7	modlitba
znázorňující	znázorňující	k2eAgInSc1d1	znázorňující
vznik	vznik	k1gInSc1	vznik
košických	košický	k2eAgFnPc2d1	Košická
relikvií	relikvie	k1gFnPc2	relikvie
rozlitím	rozlití	k1gNnSc7	rozlití
konsekrovaného	konsekrovaný	k2eAgNnSc2d1	konsekrovaný
vína	víno	k1gNnSc2	víno
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Košická	košický	k2eAgFnSc1d1	Košická
gotická	gotický	k2eAgFnSc1d1	gotická
dílna	dílna	k1gFnSc1	dílna
===	===	k?	===
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
nadregionálního	nadregionální	k2eAgInSc2d1	nadregionální
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
svými	svůj	k3xOyFgFnPc7	svůj
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
hodnotami	hodnota	k1gFnPc7	hodnota
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
prvořadé	prvořadý	k2eAgNnSc1d1	prvořadé
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
středověké	středověký	k2eAgNnSc4d1	středověké
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
chrámu	chrám	k1gInSc2	chrám
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
koncentraci	koncentrace	k1gFnSc4	koncentrace
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
pracovní	pracovní	k2eAgFnSc2d1	pracovní
činnosti	činnost	k1gFnSc2	činnost
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
,	,	kIx,	,
kameníků	kameník	k1gMnPc2	kameník
<g/>
,	,	kIx,	,
malířů	malíř	k1gMnPc2	malíř
a	a	k8xC	a
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
košických	košický	k2eAgFnPc6d1	Košická
stavebních	stavební	k2eAgFnPc6d1	stavební
hutích	huť	k1gFnPc6	huť
se	se	k3xPyFc4	se
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
mistři	mistr	k1gMnPc1	mistr
s	s	k7c7	s
linkami	linka	k1gFnPc7	linka
z	z	k7c2	z
vícero	vícero	k4xRyIgNnSc4	vícero
měst	město	k1gNnPc2	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
stavební	stavební	k2eAgFnSc1d1	stavební
huť	huť	k1gFnSc1	huť
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c4	na
slezskou	slezský	k2eAgFnSc4d1	Slezská
Vratislav	Vratislav	k1gFnSc4	Vratislav
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
malopolský	malopolský	k2eAgInSc4d1	malopolský
Krakov	Krakov	k1gInSc4	Krakov
obohatila	obohatit	k5eAaPmAgFnS	obohatit
stavbu	stavba	k1gFnSc4	stavba
o	o	k7c4	o
nejvzácnější	vzácný	k2eAgInPc4d3	nejvzácnější
architektonické	architektonický	k2eAgInPc4d1	architektonický
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
lze	lze	k6eAd1	lze
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
tradici	tradice	k1gFnSc4	tradice
portálových	portálový	k2eAgInPc2d1	portálový
štítů	štít	k1gInPc2	štít
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
kamenickou	kamenický	k2eAgFnSc7d1	kamenická
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
představující	představující	k2eAgInSc4d1	představující
výtvarně	výtvarně	k6eAd1	výtvarně
nejhodnotnější	hodnotný	k2eAgInSc4d3	nejhodnotnější
přínos	přínos	k1gInSc4	přínos
chrámu	chrám	k1gInSc2	chrám
pro	pro	k7c4	pro
slovenské	slovenský	k2eAgNnSc4d1	slovenské
umění	umění	k1gNnSc4	umění
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
období	období	k1gNnSc6	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
široký	široký	k2eAgInSc4d1	široký
umělecký	umělecký	k2eAgInSc4d1	umělecký
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
specifikum	specifikum	k1gNnSc1	specifikum
bylo	být	k5eAaImAgNnS	být
prolínání	prolínání	k1gNnSc4	prolínání
vlivů	vliv	k1gInPc2	vliv
severoevropských	severoevropský	k2eAgNnPc2d1	severoevropské
ohnisek	ohnisko	k1gNnPc2	ohnisko
doznívajícího	doznívající	k2eAgInSc2d1	doznívající
gotického	gotický	k2eAgInSc2d1	gotický
stylu	styl	k1gInSc2	styl
s	s	k7c7	s
podnětnými	podnětný	k2eAgInPc7d1	podnětný
poznatky	poznatek	k1gInPc7	poznatek
nastupující	nastupující	k2eAgFnSc2d1	nastupující
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
přicházející	přicházející	k2eAgNnSc1d1	přicházející
ze	z	k7c2	z
dvora	dvůr	k1gInSc2	dvůr
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Budína	Budín	k1gInSc2	Budín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
stavba	stavba	k1gFnSc1	stavba
chrámu	chrám	k1gInSc2	chrám
podnítila	podnítit	k5eAaPmAgFnS	podnítit
i	i	k9	i
výstavbu	výstavba	k1gFnSc4	výstavba
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
doplňujících	doplňující	k2eAgInPc2d1	doplňující
stavebních	stavební	k2eAgInPc2d1	stavební
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterých	který	k3yRgFnPc2	který
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
nedokončeným	dokončený	k2eNgInSc7d1	nedokončený
kostelem	kostel	k1gInSc7	kostel
funkční	funkční	k2eAgInSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
a	a	k8xC	a
Urbanova	Urbanův	k2eAgFnSc1d1	Urbanova
věž	věž	k1gFnSc1	věž
jsou	být	k5eAaImIp3nP	být
samostatně	samostatně	k6eAd1	samostatně
stojící	stojící	k2eAgFnPc1d1	stojící
součásti	součást	k1gFnPc1	součást
komplexu	komplex	k1gInSc2	komplex
chrámu	chrám	k1gInSc2	chrám
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistři	mistr	k1gMnPc1	mistr
košického	košický	k2eAgInSc2d1	košický
uměleckého	umělecký	k2eAgInSc2d1	umělecký
okruhu	okruh	k1gInSc2	okruh
odcházeli	odcházet	k5eAaImAgMnP	odcházet
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
uherských	uherský	k2eAgNnPc2d1	Uherské
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Budín	Budín	k1gInSc1	Budín
<g/>
,	,	kIx,	,
Diósgyör	Diósgyör	k1gInSc1	Diósgyör
<g/>
,	,	kIx,	,
Bardejov	Bardejov	k1gInSc1	Bardejov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získávali	získávat	k5eAaImAgMnP	získávat
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
místních	místní	k2eAgFnPc2d1	místní
církevních	církevní	k2eAgFnPc2d1	církevní
i	i	k8xC	i
světských	světský	k2eAgFnPc2d1	světská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
značné	značný	k2eAgFnSc6d1	značná
popularitě	popularita	k1gFnSc6	popularita
košické	košický	k2eAgFnSc2d1	Košická
dílny	dílna	k1gFnSc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
košického	košický	k2eAgInSc2d1	košický
vzoru	vzor	k1gInSc2	vzor
se	se	k3xPyFc4	se
budovaly	budovat	k5eAaImAgInP	budovat
kostely	kostel	k1gInPc1	kostel
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
<g/>
,	,	kIx,	,
Sabinově	Sabinův	k2eAgNnSc6d1	Sabinovo
<g/>
,	,	kIx,	,
Rožňavě	Rožňava	k1gFnSc6	Rožňava
a	a	k8xC	a
Moldavě	Moldavě	k1gFnSc6	Moldavě
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
empora	empora	k1gFnSc1	empora
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
podobné	podobný	k2eAgInPc4d1	podobný
projekty	projekt	k1gInPc4	projekt
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
v	v	k7c6	v
Levoči	Levoča	k1gFnSc6	Levoča
a	a	k8xC	a
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
v	v	k7c6	v
Bardejově	Bardejov	k1gInSc6	Bardejov
<g/>
.	.	kIx.	.
</s>
<s>
Dosah	dosah	k1gInSc1	dosah
košické	košický	k2eAgFnSc2d1	Košická
dílny	dílna	k1gFnSc2	dílna
sahal	sahat	k5eAaImAgMnS	sahat
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
chrámy	chrám	k1gInPc4	chrám
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
(	(	kIx(	(
<g/>
sedmihradská	sedmihradský	k2eAgFnSc1d1	Sedmihradská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
Brašov	Brašov	k1gInSc1	Brašov
<g/>
,	,	kIx,	,
Kluž	Kluž	k1gFnSc1	Kluž
<g/>
,	,	kIx,	,
Sibiu	Sibius	k1gMnSc6	Sibius
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
pompézně	pompézně	k6eAd1	pompézně
jako	jako	k8xC	jako
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
chrámu	chrám	k1gInSc2	chrám
postupovala	postupovat	k5eAaImAgFnS	postupovat
košická	košický	k2eAgFnSc1d1	Košická
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
i	i	k9	i
při	při	k7c6	při
zabezpečování	zabezpečování	k1gNnSc6	zabezpečování
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
chrámu	chrám	k1gInSc2	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
má	mít	k5eAaImIp3nS	mít
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
malířskou	malířský	k2eAgFnSc4d1	malířská
výzdobu	výzdoba	k1gFnSc4	výzdoba
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
48	[number]	k4	48
maleb	malba	k1gFnPc2	malba
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
oltáři	oltář	k1gInSc6	oltář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
dalších	další	k2eAgInPc2d1	další
oltářů	oltář	k1gInPc2	oltář
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oltáře	oltář	k1gInPc4	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
Bardejově	Bardejov	k1gInSc6	Bardejov
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
košického	košický	k2eAgInSc2d1	košický
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
nejcennější	cenný	k2eAgFnSc1d3	nejcennější
památka	památka	k1gFnSc1	památka
slovenského	slovenský	k2eAgNnSc2d1	slovenské
pozdněgotického	pozdněgotický	k2eAgNnSc2d1	pozdněgotický
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
podobnosti	podobnost	k1gFnPc1	podobnost
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
Nicolause	Nicolause	k1gFnSc2	Nicolause
Gerhaerta	Gerhaerta	k1gFnSc1	Gerhaerta
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
a	a	k8xC	a
se	s	k7c7	s
švábským	švábský	k2eAgInSc7d1	švábský
uměleckým	umělecký	k2eAgInSc7d1	umělecký
okruhem	okruh	k1gInSc7	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Sloh	sloh	k1gInSc1	sloh
oltáře	oltář	k1gInSc2	oltář
převzal	převzít	k5eAaPmAgInS	převzít
mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Weiss	Weiss	k1gMnSc1	Weiss
při	při	k7c6	při
zhotovování	zhotovování	k1gNnSc6	zhotovování
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
Chrámu	chrámat	k5eAaImIp1nS	chrámat
svatého	svatý	k2eAgMnSc4d1	svatý
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
zmínku	zmínka	k1gFnSc4	zmínka
zasluhují	zasluhovat	k5eAaImIp3nP	zasluhovat
plastiky	plastika	k1gFnPc1	plastika
sousoší	sousoší	k1gNnSc2	sousoší
košické	košický	k2eAgFnPc1d1	Košická
Kalvárie	Kalvárie	k1gFnPc1	Kalvárie
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
expresívností	expresívnost	k1gFnSc7	expresívnost
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
rámec	rámec	k1gInSc4	rámec
krásného	krásný	k2eAgInSc2d1	krásný
slohu	sloh	k1gInSc2	sloh
a	a	k8xC	a
přinášejí	přinášet	k5eAaImIp3nP	přinášet
záblesky	záblesk	k1gInPc1	záblesk
nové	nový	k2eAgFnSc2d1	nová
orientace	orientace	k1gFnSc2	orientace
evropského	evropský	k2eAgNnSc2d1	Evropské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Romantický	romantický	k2eAgInSc1d1	romantický
mýtus	mýtus	k1gInSc1	mýtus
gotického	gotický	k2eAgInSc2d1	gotický
chrámu	chrám	k1gInSc2	chrám
===	===	k?	===
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
stavba	stavba	k1gFnSc1	stavba
uherské	uherský	k2eAgFnSc2d1	uherská
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
a	a	k8xC	a
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
romantických	romantický	k2eAgMnPc2d1	romantický
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Obdiv	obdiv	k1gInSc4	obdiv
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
chrámu	chrám	k1gInSc2	chrám
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
košický	košický	k2eAgMnSc1d1	košický
rodák	rodák	k1gMnSc1	rodák
Imrich	Imrich	k1gMnSc1	Imrich
Henszlmann	Henszlmann	k1gMnSc1	Henszlmann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
publikováním	publikování	k1gNnSc7	publikování
monografie	monografie	k1gFnSc2	monografie
o	o	k7c6	o
místních	místní	k2eAgInPc6d1	místní
středověkých	středověký	k2eAgInPc6d1	středověký
kostelích	kostel	k1gInPc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc4	dílo
Kassa	Kass	k1gMnSc2	Kass
városának	városának	k1gMnSc1	városának
ö-német	öémet	k1gMnSc1	ö-német
stylü	stylü	k?	stylü
templomairól	templomairól	k1gInSc1	templomairól
(	(	kIx(	(
<g/>
Košické	košický	k2eAgInPc1d1	košický
kostely	kostel	k1gInPc1	kostel
staroněmeckého	staroněmecký	k2eAgInSc2d1	staroněmecký
stylu	styl	k1gInSc2	styl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vyneslo	vynést	k5eAaPmAgNnS	vynést
Henszlmannovi	Henszlmannův	k2eAgMnPc1d1	Henszlmannův
titul	titul	k1gInSc4	titul
zakladatele	zakladatel	k1gMnSc2	zakladatel
uměnovědy	uměnověda	k1gFnSc2	uměnověda
a	a	k8xC	a
památkové	památkový	k2eAgFnSc2d1	památková
obnovy	obnova	k1gFnSc2	obnova
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
tajemníka	tajemník	k1gMnSc2	tajemník
Uherské	uherský	k2eAgFnSc2d1	uherská
dočasné	dočasný	k2eAgFnSc2d1	dočasná
památkové	památkový	k2eAgFnSc2d1	památková
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
restaurátorských	restaurátorský	k2eAgFnPc2d1	restaurátorská
akcí	akce	k1gFnPc2	akce
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
chrámu	chrámat	k5eAaImIp1nS	chrámat
v	v	k7c6	v
letech	let	k1gInPc6	let
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
památkovou	památkový	k2eAgFnSc7d1	památková
obnovou	obnova	k1gFnSc7	obnova
košické	košický	k2eAgFnSc2d1	Košická
katedrály	katedrála	k1gFnSc2	katedrála
byl	být	k5eAaImAgInS	být
svěřen	svěřit	k5eAaPmNgMnS	svěřit
rakouskému	rakouský	k2eAgMnSc3d1	rakouský
architektovi	architekt	k1gMnSc3	architekt
Friedrichu	Friedrich	k1gMnSc3	Friedrich
von	von	k1gInSc1	von
Schmidtovi	Schmidt	k1gMnSc3	Schmidt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
15	[number]	k4	15
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
dostavbě	dostavba	k1gFnSc6	dostavba
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
restaurace	restaurace	k1gFnSc2	restaurace
chrámu	chrám	k1gInSc2	chrám
vedl	vést	k5eAaImAgMnS	vést
restaurační	restaurační	k2eAgFnPc4d1	restaurační
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
Chrámu	chrám	k1gInSc6	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Imrich	Imrich	k1gMnSc1	Imrich
Steindl	Steindl	k1gMnSc1	Steindl
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
nové	nový	k2eAgFnSc2d1	nová
přestavby	přestavba	k1gFnSc2	přestavba
chrámu	chrámat	k5eAaImIp1nS	chrámat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
purismu	purismus	k1gInSc2	purismus
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
košickému	košický	k2eAgInSc3d1	košický
chrámu	chrám	k1gInSc3	chrám
<g/>
,	,	kIx,	,
restauroval	restaurovat	k5eAaBmAgInS	restaurovat
hrad	hrad	k1gInSc1	hrad
Vajdahunyad	Vajdahunyad	k1gInSc1	Vajdahunyad
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
nových	nový	k2eAgFnPc2d1	nová
neorenesančních	neorenesanční	k2eAgFnPc2d1	neorenesanční
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
Palác	palác	k1gInSc1	palác
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
komory	komora	k1gFnSc2	komora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
taktovkou	taktovka	k1gFnSc7	taktovka
neogoticky	ogoticky	k6eNd1	ogoticky
upraveny	upravit	k5eAaPmNgInP	upravit
chrámy	chrám	k1gInPc1	chrám
v	v	k7c6	v
Bardejově	Bardejov	k1gInSc6	Bardejov
a	a	k8xC	a
Spišské	spišský	k2eAgFnPc1d1	Spišská
Nové	Nové	k2eAgFnPc1d1	Nové
Vsi	ves	k1gFnPc1	ves
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
dílem	díl	k1gInSc7	díl
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
budapešťský	budapešťský	k2eAgInSc1d1	budapešťský
Parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
stavbyvedoucím	stavbyvedoucí	k1gMnSc7	stavbyvedoucí
restaurace	restaurace	k1gFnSc2	restaurace
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
stavitel	stavitel	k1gMnSc1	stavitel
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Fröde	Fröd	k1gInSc5	Fröd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
článcích	článek	k1gInPc6	článek
v	v	k7c6	v
denním	denní	k2eAgInSc6d1	denní
tisku	tisk	k1gInSc6	tisk
i	i	k8xC	i
odborných	odborný	k2eAgFnPc6d1	odborná
publikacích	publikace	k1gFnPc6	publikace
podal	podat	k5eAaPmAgInS	podat
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
probíhajících	probíhající	k2eAgFnPc6d1	probíhající
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
stal	stát	k5eAaPmAgInS	stát
učebnicovým	učebnicový	k2eAgInSc7d1	učebnicový
příkladem	příklad	k1gInSc7	příklad
památkové	památkový	k2eAgFnSc2d1	památková
obnovy	obnova	k1gFnSc2	obnova
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
architektura	architektura	k1gFnSc1	architektura
košické	košický	k2eAgFnSc2d1	Košická
katedrály	katedrála	k1gFnSc2	katedrála
byla	být	k5eAaImAgFnS	být
studována	studovat	k5eAaImNgFnS	studovat
jako	jako	k8xC	jako
norma	norma	k1gFnSc1	norma
uherského	uherský	k2eAgInSc2d1	uherský
středověkého	středověký	k2eAgInSc2d1	středověký
slohu	sloh	k1gInSc2	sloh
–	–	k?	–
národně	národně	k6eAd1	národně
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mnohé	mnohý	k2eAgFnPc1d1	mnohá
teoretické	teoretický	k2eAgFnPc1d1	teoretická
práce	práce	k1gFnPc1	práce
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
Sándora	Sándor	k1gMnSc4	Sándor
Nyáriho	Nyári	k1gMnSc4	Nyári
<g/>
,	,	kIx,	,
Kornela	Kornel	k1gMnSc4	Kornel
Divalda	Divald	k1gMnSc4	Divald
<g/>
,	,	kIx,	,
Otta	Otta	k1gMnSc1	Otta
Sztehlóa	Sztehlóa	k1gMnSc1	Sztehlóa
a	a	k8xC	a
nebo	nebo	k8xC	nebo
Ernó	Ernó	k1gMnSc4	Ernó
Marosiho	Marosi	k1gMnSc4	Marosi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
věnována	věnovat	k5eAaImNgFnS	věnovat
výstavbě	výstavba	k1gFnSc3	výstavba
nové	nový	k2eAgFnPc4d1	nová
krypty	krypta	k1gFnPc4	krypta
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
ostatků	ostatek	k1gInPc2	ostatek
uherského	uherský	k2eAgMnSc2d1	uherský
vůdce	vůdce	k1gMnSc2	vůdce
a	a	k8xC	a
knížete	kníže	k1gMnSc2	kníže
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
:	:	kIx,	:
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
byla	být	k5eAaImAgFnS	být
svěřena	svěřit	k5eAaPmNgFnS	svěřit
budapešťskému	budapešťský	k2eAgMnSc3d1	budapešťský
architektu	architekt	k1gMnSc3	architekt
Frigyesu	Frigyes	k1gInSc2	Frigyes
Schulekovi	Schuleek	k1gMnSc3	Schuleek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
neogotickou	ogotický	k2eNgFnSc7d1	neogotická
přestavbou	přestavba	k1gFnSc7	přestavba
Matyášova	Matyášův	k2eAgInSc2d1	Matyášův
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
Rybářskou	rybářský	k2eAgFnSc7d1	rybářská
baštou	bašta	k1gFnSc7	bašta
na	na	k7c6	na
budínském	budínský	k2eAgInSc6d1	budínský
hradním	hradní	k2eAgInSc6d1	hradní
kopci	kopec	k1gInSc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
realizoval	realizovat	k5eAaBmAgMnS	realizovat
i	i	k9	i
dotvoření	dotvoření	k1gNnSc4	dotvoření
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
Levoči	Levoča	k1gFnSc6	Levoča
<g/>
,	,	kIx,	,
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
františkánského	františkánský	k2eAgInSc2d1	františkánský
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
regotizaci	regotizace	k1gFnSc6	regotizace
věže	věž	k1gFnSc2	věž
hlavního	hlavní	k2eAgInSc2d1	hlavní
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozměrná	rozměrný	k2eAgFnSc1d1	rozměrná
hmota	hmota	k1gFnSc1	hmota
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
výjimečnost	výjimečnost	k1gFnSc1	výjimečnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jiných	jiný	k2eAgFnPc2d1	jiná
městských	městský	k2eAgFnPc2d1	městská
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
odrazily	odrazit	k5eAaPmAgInP	odrazit
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
místního	místní	k2eAgInSc2d1	místní
historizujícího	historizující	k2eAgInSc2d1	historizující
stylu	styl	k1gInSc2	styl
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
košických	košický	k2eAgFnPc2d1	Košická
novogotických	novogotický	k2eAgFnPc2d1	novogotická
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
architektury	architektura	k1gFnSc2	architektura
chrámu	chrám	k1gInSc2	chrám
vedlo	vést	k5eAaImAgNnS	vést
domácí	domácí	k2eAgMnPc4d1	domácí
architekty	architekt	k1gMnPc4	architekt
k	k	k7c3	k
uplatnění	uplatnění	k1gNnSc3	uplatnění
získaných	získaný	k2eAgInPc2d1	získaný
poznatků	poznatek	k1gInPc2	poznatek
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
další	další	k2eAgFnSc6d1	další
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vítézův	Vítézův	k2eAgInSc1d1	Vítézův
dům	dům	k1gInSc1	dům
na	na	k7c4	na
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
ulici	ulice	k1gFnSc4	ulice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
palácovým	palácový	k2eAgInSc7d1	palácový
domem	dům	k1gInSc7	dům
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
s	s	k7c7	s
neogotickou	ogotický	k2eNgFnSc7d1	neogotická
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
ho	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
Makranského	Makranský	k2eAgMnSc2d1	Makranský
nájemný	nájemný	k2eAgInSc4d1	nájemný
dům	dům	k1gInSc4	dům
na	na	k7c6	na
Roosweltově	Roosweltův	k2eAgFnSc6d1	Roosweltova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
Jakabova	Jakabův	k2eAgInSc2d1	Jakabův
paláce	palác	k1gInSc2	palác
byly	být	k5eAaImAgInP	být
dokonce	dokonce	k9	dokonce
použity	použít	k5eAaPmNgInP	použít
některé	některý	k3yIgInPc1	některý
vyřazené	vyřazený	k2eAgInPc1d1	vyřazený
chrliče	chrlič	k1gInPc1	chrlič
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
zakomponovaly	zakomponovat	k5eAaPmAgInP	zakomponovat
se	se	k3xPyFc4	se
do	do	k7c2	do
fasády	fasáda	k1gFnSc2	fasáda
této	tento	k3xDgFnSc2	tento
romanticko-eklektické	romantickoklektický	k2eAgFnSc2d1	romanticko-eklektický
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
architektonické	architektonický	k2eAgFnPc1d1	architektonická
části	část	k1gFnPc1	část
skončily	skončit	k5eAaPmAgFnP	skončit
jako	jako	k9	jako
náhrobky	náhrobek	k1gInPc4	náhrobek
a	a	k8xC	a
ozdoby	ozdoba	k1gFnPc4	ozdoba
na	na	k7c6	na
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k8xS	jako
dekorace	dekorace	k1gFnPc1	dekorace
v	v	k7c6	v
zahradých	zahradý	k2eAgFnPc6d1	zahradý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Legendy	legenda	k1gFnPc4	legenda
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
chrámem	chrám	k1gInSc7	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
chrámu	chrám	k1gInSc2	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
několik	několik	k4yIc4	několik
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
ve	v	k7c6	v
středověkých	středověký	k2eAgInPc6d1	středověký
časech	čas	k1gInPc6	čas
z	z	k7c2	z
období	období	k1gNnSc2	období
výstavby	výstavba	k1gFnSc2	výstavba
chrámu	chrám	k1gInSc2	chrám
konstrukce	konstrukce	k1gFnSc2	konstrukce
takovéto	takovýto	k3xDgFnPc4	takovýto
náročné	náročný	k2eAgFnPc4d1	náročná
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgFnPc4d1	trvající
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
dutém	dutý	k2eAgInSc6d1	dutý
kameni	kámen	k1gInSc6	kámen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
stavitelé	stavitel	k1gMnPc1	stavitel
umístili	umístit	k5eAaPmAgMnP	umístit
na	na	k7c4	na
neznámé	známý	k2eNgNnSc4d1	neznámé
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
vytáhnutý	vytáhnutý	k2eAgInSc1d1	vytáhnutý
<g/>
,	,	kIx,	,
způsobil	způsobit	k5eAaPmAgInS	způsobit
by	by	kYmCp3nS	by
zřícení	zřícení	k1gNnSc3	zřícení
celého	celý	k2eAgInSc2d1	celý
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Legendami	legenda	k1gFnPc7	legenda
je	být	k5eAaImIp3nS	být
také	také	k9	také
opředen	opředen	k2eAgMnSc1d1	opředen
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
chrličů	chrlič	k1gInPc2	chrlič
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
nezpodobňuje	zpodobňovat	k5eNaImIp3nS	zpodobňovat
zoomorfní	zoomorfní	k2eAgFnPc4d1	zoomorfní
bytosti	bytost	k1gFnPc4	bytost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ženu	žena	k1gFnSc4	žena
s	s	k7c7	s
vypoulenýma	vypoulený	k2eAgNnPc7d1	vypoulené
očima	oko	k1gNnPc7	oko
držící	držící	k2eAgNnSc4d1	držící
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
ukrývající	ukrývající	k2eAgFnSc4d1	ukrývající
láhev	láhev	k1gFnSc1	láhev
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
spodobnil	spodobnit	k5eAaPmAgMnS	spodobnit
mistr	mistr	k1gMnSc1	mistr
Matyášovy	Matyášův	k2eAgFnSc2d1	Matyášova
věže	věž	k1gFnSc2	věž
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
–	–	k?	–
alkoholičku	alkoholička	k1gFnSc4	alkoholička
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
dělala	dělat	k5eAaImAgFnS	dělat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
hanbu	hanba	k1gFnSc4	hanba
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
ji	on	k3xPp3gFnSc4	on
navěky	navěky	k6eAd1	navěky
chrlit	chrlit	k5eAaImF	chrlit
dešťovou	dešťový	k2eAgFnSc4d1	dešťová
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
i	i	k9	i
k	k	k7c3	k
lampě	lampa	k1gFnSc3	lampa
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Lampa	lampa	k1gFnSc1	lampa
prý	prý	k9	prý
má	mít	k5eAaImIp3nS	mít
takovou	takový	k3xDgFnSc4	takový
zázračnou	zázračný	k2eAgFnSc4d1	zázračná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
zločinu	zločin	k1gInSc2	zločin
<g/>
,	,	kIx,	,
nacpal	nacpat	k5eAaPmAgMnS	nacpat
do	do	k7c2	do
lampy	lampa	k1gFnSc2	lampa
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Matyášovu	Matyášův	k2eAgFnSc4d1	Matyášova
věž	věž	k1gFnSc4	věž
zase	zase	k9	zase
prý	prý	k9	prý
nedostavěli	dostavět	k5eNaPmAgMnP	dostavět
kvůli	kvůli	k7c3	kvůli
pádu	pád	k1gInSc3	pád
jednoho	jeden	k4xCgMnSc2	jeden
stavitele	stavitel	k1gMnSc2	stavitel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
zlé	zlý	k2eAgNnSc4d1	zlé
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
legend	legenda	k1gFnPc2	legenda
se	se	k3xPyFc4	se
posouvá	posouvat	k5eAaImIp3nS	posouvat
i	i	k9	i
tradice	tradice	k1gFnSc1	tradice
slavení	slavení	k1gNnSc2	slavení
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jedné	jeden	k4xCgFnSc2	jeden
nedělní	nedělní	k2eAgFnSc2d1	nedělní
mše	mše	k1gFnSc2	mše
<g/>
,	,	kIx,	,
když	když	k8xS	když
nepozorný	pozorný	k2eNgMnSc1d1	nepozorný
kněz	kněz	k1gMnSc1	kněz
při	při	k7c6	při
zakopnutí	zakopnutí	k1gNnSc6	zakopnutí
rozlil	rozlít	k5eAaPmAgMnS	rozlít
konsekrovanou	konsekrovaný	k2eAgFnSc4d1	konsekrovaná
číši	číše	k1gFnSc4	číše
s	s	k7c7	s
vínem	víno	k1gNnSc7	víno
na	na	k7c4	na
dlažbu	dlažba	k1gFnSc4	dlažba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
obraz	obraz	k1gInSc1	obraz
trpícího	trpící	k2eAgMnSc2d1	trpící
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
věřící	věřící	k1gMnPc1	věřící
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
slyšeli	slyšet	k5eAaImAgMnP	slyšet
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
sténání	sténání	k1gNnSc4	sténání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
nejstarší	starý	k2eAgFnPc1d3	nejstarší
veduty	veduta	k1gFnPc1	veduta
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
Košic	Košice	k1gInPc2	Košice
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
Chrám	chrám	k1gInSc4	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
jako	jako	k8xS	jako
nejvýraznější	výrazný	k2eAgInSc4d3	nejvýraznější
městský	městský	k2eAgInSc4d1	městský
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umělecké	umělecký	k2eAgNnSc4d1	umělecké
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
chrámu	chrám	k1gInSc2	chrám
přinesl	přinést	k5eAaPmAgInS	přinést
v	v	k7c6	v
období	období	k1gNnSc6	období
rozvíjejícího	rozvíjející	k2eAgInSc2d1	rozvíjející
se	se	k3xPyFc4	se
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
historismu	historismus	k1gInSc3	historismus
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
malíř	malíř	k1gMnSc1	malíř
Jakub	Jakub	k1gMnSc1	Jakub
Alt	Alt	kA	Alt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
akvarelových	akvarelový	k2eAgFnPc6d1	akvarelová
malbách	malba	k1gFnPc6	malba
Košický	košický	k2eAgInSc1d1	košický
dóm	dóm	k1gInSc1	dóm
a	a	k8xC	a
Náměstí	náměstí	k1gNnSc1	náměstí
před	před	k7c7	před
košickým	košický	k2eAgInSc7d1	košický
dómem	dóm	k1gInSc7	dóm
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
zachytil	zachytit	k5eAaPmAgMnS	zachytit
zákoutí	zákoutí	k1gNnSc4	zákoutí
okolo	okolo	k7c2	okolo
chrámu	chrám	k1gInSc2	chrám
s	s	k7c7	s
detailním	detailní	k2eAgNnSc7d1	detailní
zobrazením	zobrazení	k1gNnSc7	zobrazení
architektury	architektura	k1gFnSc2	architektura
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
okolního	okolní	k2eAgInSc2d1	okolní
městského	městský	k2eAgInSc2d1	městský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Věrným	věrný	k2eAgNnSc7d1	věrné
spodobněním	spodobnění	k1gNnSc7	spodobnění
chrámu	chrám	k1gInSc2	chrám
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
před	před	k7c7	před
velkou	velký	k2eAgFnSc7d1	velká
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
je	být	k5eAaImIp3nS	být
ocelorytina	ocelorytina	k1gFnSc1	ocelorytina
norimberského	norimberský	k2eAgMnSc2d1	norimberský
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
Ludwiga	Ludwiga	k1gFnSc1	Ludwiga
Rohbocka	Rohbocka	k1gFnSc1	Rohbocka
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Ungarn	Ungarn	k1gNnSc4	Ungarn
und	und	k?	und
Siebenburgen	Siebenburgen	k1gInSc1	Siebenburgen
in	in	k?	in
Malerische	Malerische	k1gInSc1	Malerische
Original-Ansichten	Original-Ansichten	k2eAgInSc1d1	Original-Ansichten
(	(	kIx(	(
<g/>
Darmstadt	Darmstadt	k1gInSc1	Darmstadt
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgMnSc1d1	domácí
představitel	představitel	k1gMnSc1	představitel
romantismu	romantismus	k1gInSc2	romantismus
František	František	k1gMnSc1	František
Klimkovič	Klimkovič	k1gMnSc1	Klimkovič
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
akvarelu	akvarel	k1gInSc2	akvarel
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
s	s	k7c7	s
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
chrám	chrám	k1gInSc4	chrám
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
představitelé	představitel	k1gMnPc1	představitel
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
avantgardy	avantgarda	k1gFnSc2	avantgarda
se	se	k3xPyFc4	se
nevyhnuli	vyhnout	k5eNaPmAgMnP	vyhnout
motivu	motiv	k1gInSc3	motiv
košické	košický	k2eAgFnSc2d1	Košická
perly	perla	k1gFnSc2	perla
gotické	gotický	k2eAgFnSc2d1	gotická
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
Eugen	Eugen	k1gInSc1	Eugen
Krón	Króna	k1gFnPc2	Króna
Dvůr	Dvůr	k1gInSc1	Dvůr
domu	dům	k1gInSc2	dům
č.	č.	k?	č.
7	[number]	k4	7
na	na	k7c6	na
Orlí	orlí	k2eAgFnSc6d1	orlí
ulici	ulice	k1gFnSc6	ulice
–	–	k?	–
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Elemír	Elemír	k1gMnSc1	Elemír
Halász-Hradil	Halász-Hradil	k1gMnSc1	Halász-Hradil
Večer	večer	k6eAd1	večer
před	před	k7c7	před
košickým	košický	k2eAgInSc7d1	košický
dómem	dóm	k1gInSc7	dóm
–	–	k?	–
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Janko	Janko	k1gMnSc1	Janko
Alexy	Alex	k1gMnPc4	Alex
Dóm	dóm	k1gInSc1	dóm
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
–	–	k?	–
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klasice	klasika	k1gFnSc3	klasika
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
košického	košický	k2eAgInSc2d1	košický
chrámu	chrám	k1gInSc2	chrám
patří	patřit	k5eAaImIp3nS	patřit
dílo	dílo	k1gNnSc4	dílo
Ľudovíta	Ľudovíto	k1gNnSc2	Ľudovíto
Felda	Feldo	k1gNnSc2	Feldo
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
košický	košický	k2eAgInSc4d1	košický
dóm	dóm	k1gInSc4	dóm
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
<g/>
Katedrálu	katedrála	k1gFnSc4	katedrála
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
nevynechal	vynechat	k5eNaPmAgMnS	vynechat
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Košic	Košice	k1gInPc2	Košice
žádný	žádný	k1gMnSc1	žádný
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc4	chrám
beletristicky	beletristicky	k6eAd1	beletristicky
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
cestopisném	cestopisný	k2eAgInSc6d1	cestopisný
románu	román	k1gInSc6	román
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
Daniel	Daniel	k1gMnSc1	Daniel
Speer	Speer	k1gMnSc1	Speer
alis	alis	k6eAd1	alis
Uherský	uherský	k2eAgInSc4d1	uherský
Simplicissimus	Simplicissimus	k1gInSc4	Simplicissimus
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
vskutku	vskutku	k9	vskutku
obdivuhodnou	obdivuhodný	k2eAgFnSc7d1	obdivuhodná
stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
také	také	k9	také
bez	bez	k7c2	bez
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
atiky	atika	k1gFnSc2	atika
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
čistě	čistě	k6eAd1	čistě
ze	z	k7c2	z
čtverhranných	čtverhranný	k2eAgInPc2d1	čtverhranný
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
drží	držet	k5eAaImIp3nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
železné	železný	k2eAgInPc1d1	železný
svorníky	svorník	k1gInPc1	svorník
zesilněné	zesilněný	k2eAgFnSc6d1	zesilněný
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
vyhotovené	vyhotovený	k2eAgFnSc2d1	vyhotovená
sochy	socha	k1gFnSc2	socha
a	a	k8xC	a
podivné	podivný	k2eAgFnSc2d1	podivná
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yRgInPc6	který
stéká	stékat	k5eAaImIp3nS	stékat
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
,	,	kIx,	,
i	i	k9	i
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
když	když	k8xS	když
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
dvě	dva	k4xCgFnPc1	dva
osoby	osoba	k1gFnPc1	osoba
stejnými	stejný	k2eAgFnPc7d1	stejná
dveřmi	dveře	k1gFnPc7	dveře
schody	schod	k1gInPc4	schod
napravo	napravo	k6eAd1	napravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
druhého	druhý	k4xOgMnSc4	druhý
nemůže	moct	k5eNaImIp3nS	moct
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zvenku	zvenku	k6eAd1	zvenku
je	on	k3xPp3gFnPc4	on
vidí	vidět	k5eAaImIp3nS	vidět
každý	každý	k3xTgMnSc1	každý
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
točitým	točitý	k2eAgNnSc7d1	točité
schodištěm	schodiště	k1gNnSc7	schodiště
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jít	jít	k5eAaImF	jít
nahoru	nahoru	k6eAd1	nahoru
dokud	dokud	k8xS	dokud
nakonec	nakonec	k6eAd1	nakonec
místo	místo	k1gNnSc1	místo
dveří	dveře	k1gFnPc2	dveře
dojde	dojít	k5eAaPmIp3nS	dojít
člověk	člověk	k1gMnSc1	člověk
jen	jen	k9	jen
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
soše	socha	k1gFnSc3	socha
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dívat	dívat	k5eAaImF	dívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
nějakým	nějaký	k3yIgNnSc7	nějaký
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
zabloudí	zabloudit	k5eAaPmIp3nP	zabloudit
na	na	k7c6	na
schodištích	schodiště	k1gNnPc6	schodiště
a	a	k8xC	a
chodbách	chodba	k1gFnPc6	chodba
<g/>
,	,	kIx,	,
že	že	k8xS	že
celé	celý	k2eAgFnPc1d1	celá
hodiny	hodina	k1gFnPc1	hodina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyjít	vyjít	k5eAaPmF	vyjít
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Svou	svůj	k3xOyFgFnSc4	svůj
úctu	úcta	k1gFnSc4	úcta
ke	k	k7c3	k
katedrále	katedrála	k1gFnSc3	katedrála
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k8xC	i
košický	košický	k2eAgMnSc1d1	košický
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
spisovatel	spisovatel	k1gMnSc1	spisovatel
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
Sándor	Sándora	k1gFnPc2	Sándora
Márai	Mára	k1gFnSc2	Mára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životopisném	životopisný	k2eAgInSc6d1	životopisný
románu	román	k1gInSc6	román
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
nepoměr	nepoměr	k1gInSc1	nepoměr
mezi	mezi	k7c7	mezi
výstavností	výstavnost	k1gFnSc7	výstavnost
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
ostatní	ostatní	k2eAgFnSc7d1	ostatní
zástavbou	zástavba	k1gFnSc7	zástavba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ji	on	k3xPp3gFnSc4	on
chrám	chrám	k1gInSc1	chrám
"	"	kIx"	"
<g/>
trochu	trochu	k6eAd1	trochu
terorizuje	terorizovat	k5eAaImIp3nS	terorizovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tyto	tento	k3xDgFnPc1	tento
katedrály	katedrála	k1gFnPc1	katedrála
se	se	k3xPyFc4	se
budovaly	budovat	k5eAaImAgFnP	budovat
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
vůlí	vůle	k1gFnSc7	vůle
a	a	k8xC	a
pokorou	pokora	k1gFnSc7	pokora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nechtěla	chtít	k5eNaImAgFnS	chtít
svět	svět	k1gInSc4	svět
ujařmit	ujařmit	k5eAaPmF	ujařmit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
ne	ne	k9	ne
panovat	panovat	k5eAaImF	panovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vést	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vyznal	vyznat	k5eAaBmAgMnS	vyznat
se	se	k3xPyFc4	se
Márai	Márai	k1gNnSc4	Márai
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Košická	košický	k2eAgFnSc1d1	Košická
pochůzka	pochůzka	k1gFnSc1	pochůzka
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
životě	život	k1gInSc6	život
města	město	k1gNnSc2	město
a	a	k8xC	a
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
erigování	erigování	k1gNnSc6	erigování
košické	košický	k2eAgFnSc2d1	Košická
diecéze	diecéze	k1gFnSc2	diecéze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
se	se	k3xPyFc4	se
chrám	chrám	k1gInSc1	chrám
stal	stát	k5eAaPmAgInS	stát
katedrálou	katedrála	k1gFnSc7	katedrála
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
východní	východní	k2eAgFnSc2d1	východní
provincie	provincie	k1gFnSc2	provincie
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
metropolitním	metropolitní	k2eAgInSc7d1	metropolitní
chrámem	chrám	k1gInSc7	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
chrámem	chrám	k1gInSc7	chrám
košické	košický	k2eAgFnSc2d1	Košická
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
farnosti	farnost	k1gFnSc2	farnost
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc4	tři
celodiecézní	celodiecézní	k2eAgFnPc4d1	celodiecézní
slavnosti	slavnost	k1gFnPc4	slavnost
<g/>
:	:	kIx,	:
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
svátek	svátek	k1gInSc1	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Ondřeje	Ondřej	k1gMnSc2	Ondřej
–	–	k?	–
patrona	patron	k1gMnSc4	patron
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
a	a	k8xC	a
svátek	svátek	k1gInSc4	svátek
Třech	tři	k4xCgMnPc2	tři
košických	košický	k2eAgMnPc2d1	košický
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
relikvie	relikvie	k1gFnPc1	relikvie
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
rovnoramenném	rovnoramenný	k2eAgInSc6d1	rovnoramenný
oltáři	oltář	k1gInSc6	oltář
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
relikvie	relikvie	k1gFnSc1	relikvie
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
patronky	patronka	k1gFnSc2	patronka
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
patronky	patronka	k1gFnSc2	patronka
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
relikviáři	relikviář	k1gInSc6	relikviář
chrámového	chrámový	k2eAgInSc2d1	chrámový
pokladu	poklad	k1gInSc2	poklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
běžný	běžný	k2eAgInSc4d1	běžný
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
konají	konat	k5eAaImIp3nP	konat
tři	tři	k4xCgFnPc4	tři
mše	mše	k1gFnPc4	mše
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
je	být	k5eAaImIp3nS	být
slouženo	sloužen	k2eAgNnSc1d1	slouženo
sedm	sedm	k4xCc1	sedm
mší	mše	k1gFnPc2	mše
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Liturgické	liturgický	k2eAgFnPc1d1	liturgická
slavnosti	slavnost	k1gFnPc1	slavnost
jsou	být	k5eAaImIp3nP	být
provázeny	provázet	k5eAaImNgFnP	provázet
Sborem	sborem	k6eAd1	sborem
svaté	svatý	k2eAgFnPc1d1	svatá
Cecílie	Cecílie	k1gFnPc1	Cecílie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
u	u	k7c2	u
chrámu	chrám	k1gInSc2	chrám
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
televize	televize	k1gFnSc1	televize
pravidelně	pravidelně	k6eAd1	pravidelně
vysílá	vysílat	k5eAaImIp3nS	vysílat
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
vánoční	vánoční	k2eAgFnSc4d1	vánoční
mši	mše	k1gFnSc4	mše
z	z	k7c2	z
košické	košický	k2eAgFnSc2d1	Košická
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
pořádání	pořádání	k1gNnSc2	pořádání
občasných	občasný	k2eAgInPc2d1	občasný
koncertů	koncert	k1gInPc2	koncert
Státní	státní	k2eAgFnSc2d1	státní
filharmonie	filharmonie	k1gFnSc2	filharmonie
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
varhanního	varhanní	k2eAgInSc2d1	varhanní
festivalu	festival	k1gInSc2	festival
Ivana	Ivan	k1gMnSc4	Ivan
Sokola	Sokol	k1gMnSc4	Sokol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupující	postupující	k2eAgFnSc7d1	postupující
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
byly	být	k5eAaImAgFnP	být
zpřístupněny	zpřístupnit	k5eAaPmNgFnP	zpřístupnit
turisticky	turisticky	k6eAd1	turisticky
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgFnPc4d1	atraktivní
prostory	prostora	k1gFnPc4	prostora
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
můžou	můžou	k?	můžou
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
navštívit	navštívit	k5eAaPmF	navštívit
tyto	tento	k3xDgInPc4	tento
objekty	objekt	k1gInPc4	objekt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
zvonového	zvonový	k2eAgNnSc2d1	zvonové
a	a	k8xC	a
hodinového	hodinový	k2eAgNnSc2d1	hodinové
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
vyhlídkovým	vyhlídkový	k2eAgInSc7d1	vyhlídkový
ochozem	ochoz	k1gInSc7	ochoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
krypta	krypta	k1gFnSc1	krypta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královské	královský	k2eAgNnSc1d1	královské
oratorium	oratorium	k1gNnSc1	oratorium
a	a	k8xC	a
sdružené	sdružený	k2eAgNnSc1d1	sdružené
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
<g/>
Katedrála	katedrála	k1gFnSc1	katedrála
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
kostel	kostel	k1gInSc4	kostel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
běžně	běžně	k6eAd1	běžně
nazývají	nazývat	k5eAaImIp3nP	nazývat
<g/>
,	,	kIx,	,
zvolili	zvolit	k5eAaPmAgMnP	zvolit
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
div	div	k1gInSc4	div
Košic	Košice	k1gInPc2	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
patronka	patronka	k1gFnSc1	patronka
<g/>
,	,	kIx,	,
svatá	svatý	k2eAgFnSc1d1	svatá
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
zase	zase	k9	zase
získala	získat	k5eAaPmAgFnS	získat
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Nej	Nej	k1gMnSc1	Nej
Košičan	Košičan	k1gMnSc1	Košičan
<g/>
/	/	kIx~	/
<g/>
ka	ka	k?	ka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dóm	dóm	k1gInSc1	dóm
svätej	svätat	k5eAaPmRp2nS	svätat
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BORODÁČ	BORODÁČ	kA	BORODÁČ
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Košický	košický	k2eAgInSc1d1	košický
dóm	dóm	k1gInSc1	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Národná	Národný	k2eAgFnSc1d1	Národná
kultúrna	kultúrna	k1gFnSc1	kultúrna
pamiatka	pamiatka	k1gFnSc1	pamiatka
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Východoslovenské	východoslovenský	k2eAgNnSc1d1	Východoslovenské
vydavateľstvo	vydavateľstvo	k1gNnSc1	vydavateľstvo
pre	pre	k?	pre
Mestskú	Mestskú	k1gFnSc1	Mestskú
správu	správa	k1gFnSc4	správa
pamiatok	pamiatok	k1gInSc4	pamiatok
v	v	k7c4	v
Košiciach	Košiciach	k1gInSc4	Košiciach
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dóm	dóm	k1gInSc1	dóm
sv.	sv.	kA	sv.
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
v	v	k7c6	v
Košiciach	Košicia	k1gFnPc6	Košicia
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
<g/>
:	:	kIx,	:
Sáša	Sáša	k1gFnSc1	Sáša
pre	pre	k?	pre
Arcibiskupstvo	Arcibiskupstvo	k1gNnSc1	Arcibiskupstvo
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
Farnosť	Farnosť	k1gFnPc1	Farnosť
sv.	sv.	kA	sv.
Alžbety	Alžbet	k2eAgInPc1d1	Alžbet
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
967096	[number]	k4	967096
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JUDÁK	JUDÁK	kA	JUDÁK
<g/>
,	,	kIx,	,
Viliam	Viliam	k1gMnSc1	Viliam
<g/>
.	.	kIx.	.
</s>
<s>
Pútnik	Pútnik	k1gInSc1	Pútnik
svätovojtešský	svätovojtešský	k2eAgInSc1d1	svätovojtešský
:	:	kIx,	:
kalendár	kalendár	k1gInSc1	kalendár
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Mária	Mária	k1gFnSc1	Mária
Vyskočová	Vyskočová	k1gFnSc1	Vyskočová
a	a	k8xC	a
Slavomír	Slavomír	k1gMnSc1	Slavomír
Ondica	Ondica	k1gMnSc1	Ondica
<g/>
.	.	kIx.	.
</s>
<s>
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
:	:	kIx,	:
Spolok	Spolok	k1gInSc1	Spolok
svätého	svätý	k2eAgMnSc2d1	svätý
Vojtecha	Vojtech	k1gMnSc2	Vojtech
<g/>
,	,	kIx,	,
ročník	ročník	k1gInSc1	ročník
139	[number]	k4	139
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7162	[number]	k4	7162
<g/>
-	-	kIx~	-
<g/>
824	[number]	k4	824
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Katedrála	katedrála	k1gFnSc1	katedrála
svätej	svätat	k5eAaPmRp2nS	svätat
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
v	v	k7c6	v
Košiciach	Košicia	k1gFnPc6	Košicia
<g/>
,	,	kIx,	,
s.	s.	k?	s.
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
56	[number]	k4	56
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LUKAČIN	LUKAČIN	kA	LUKAČIN
<g/>
,	,	kIx,	,
Alfonz	Alfonz	k1gInSc1	Alfonz
<g/>
.	.	kIx.	.
</s>
<s>
Staviteľ	Staviteľ	k?	Staviteľ
chrámu	chrámat	k5eAaImIp1nS	chrámat
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
<g/>
:	:	kIx,	:
PressPrint	PressPrint	k1gMnSc1	PressPrint
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
968272	[number]	k4	968272
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MARKUŠOVÁ	MARKUŠOVÁ	kA	MARKUŠOVÁ
<g/>
,	,	kIx,	,
Kristína	Kristína	k1gFnSc1	Kristína
<g/>
.	.	kIx.	.
</s>
<s>
Dóm	dóm	k1gInSc1	dóm
sv.	sv.	kA	sv.
Alžbety	Alžbet	k1gInPc1	Alžbet
<g/>
.	.	kIx.	.
</s>
<s>
Sprievodca	Sprievodca	k6eAd1	Sprievodca
po	po	k7c6	po
košických	košický	k2eAgFnPc6d1	Košická
kostoloch	kostoloch	k1gInSc1	kostoloch
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
<g/>
:	:	kIx,	:
Štroffek	Štroffka	k1gFnPc2	Štroffka
pre	pre	k?	pre
Historickú	Historickú	k1gMnSc1	Historickú
spoločnosť	spoločnostit	k5eAaPmRp2nS	spoločnostit
Imricha	Imrich	k1gMnSc4	Imrich
Henszlmanna	Henszlmanna	k1gFnSc1	Henszlmanna
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788096780006	[number]	k4	9788096780006
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
POLÁKOVÁ	Poláková	k1gFnSc1	Poláková
<g/>
,	,	kIx,	,
Mália	Mália	k1gFnSc1	Mália
<g/>
.	.	kIx.	.
</s>
<s>
Dóm	dóm	k1gInSc1	dóm
sv.	sv.	kA	sv.
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
v	v	k7c6	v
Košiciach	Košicia	k1gFnPc6	Košicia
<g/>
.	.	kIx.	.
</s>
<s>
Národná	Národný	k2eAgFnSc1d1	Národná
kultúrna	kultúrna	k1gFnSc1	kultúrna
pamiatka	pamiatka	k1gFnSc1	pamiatka
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Obzor	obzor	k1gInSc1	obzor
pre	pre	k?	pre
Východoslovenský	východoslovenský	k2eAgInSc1d1	východoslovenský
krajský	krajský	k2eAgInSc1d1	krajský
výbor	výbor	k1gInSc1	výbor
Združenia	Združenium	k1gNnSc2	Združenium
katolíckeho	katolíckeze	k6eAd1	katolíckeze
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
Pacem	pac	k1gInSc7	pac
in	in	k?	in
terris	terris	k1gFnSc1	terris
a	a	k8xC	a
Rímskokatolícky	Rímskokatolíck	k1gInPc1	Rímskokatolíck
farský	farský	k2eAgInSc1d1	farský
úrad	úrada	k1gFnPc2	úrada
sv.	sv.	kA	sv.
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
v	v	k7c6	v
Košiciach	Košiciach	k1gMnSc1	Košiciach
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WICK	WICK	kA	WICK
<g/>
,	,	kIx,	,
Vojtech	Vojta	k1gMnPc6	Vojta
<g/>
.	.	kIx.	.
</s>
<s>
Dóm	dóm	k1gInSc1	dóm
svätej	svätat	k5eAaPmRp2nS	svätat
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
v	v	k7c6	v
Košiciach	Košicia	k1gFnPc6	Košicia
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
<g/>
:	:	kIx,	:
Tlačiareň	Tlačiareň	k1gFnSc1	Tlačiareň
svätej	svätat	k5eAaImRp2nS	svätat
Alžbety	Alžbeta	k1gFnSc2	Alžbeta
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Katedrála	katedrál	k1gMnSc2	katedrál
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
farnosti	farnost	k1gFnSc2	farnost
</s>
</p>
<p>
<s>
Faktografické	faktografický	k2eAgInPc1d1	faktografický
údaje	údaj	k1gInPc1	údaj
na	na	k7c6	na
městských	městský	k2eAgFnPc6d1	městská
stránkách	stránka	k1gFnPc6	stránka
Cassovia	Cassovium	k1gNnSc2	Cassovium
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
