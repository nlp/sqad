<s>
Svatý	svatý	k2eAgInSc1d1
grál	grál	k1gInSc1
je	být	k5eAaImIp3nS
pohár	pohár	k1gInSc4
či	či	k8xC
nádoba	nádoba	k1gFnSc1
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
údajně	údajně	k6eAd1
pil	pít	k5eAaImAgMnS
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
při	při	k7c6
Poslední	poslední	k2eAgFnSc6d1
večeři	večeře	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>