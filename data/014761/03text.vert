<s>
Svatý	svatý	k2eAgInSc1d1
grál	grál	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
Arthura	Arthur	k1gMnSc2
Rackhama	Rackham	k1gMnSc2
<g/>
,	,	kIx,
1917	#num#	k4
</s>
<s>
Svatý	svatý	k2eAgInSc1d1
grál	grál	k1gInSc1
je	být	k5eAaImIp3nS
pohár	pohár	k1gInSc4
či	či	k8xC
nádoba	nádoba	k1gFnSc1
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
údajně	údajně	k6eAd1
pil	pít	k5eAaImAgMnS
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
při	při	k7c6
Poslední	poslední	k2eAgFnSc6d1
večeři	večeře	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legendy	legenda	k1gFnPc1
o	o	k7c6
Svatém	svatý	k2eAgInSc6d1
grálu	grál	k1gInSc6
</s>
<s>
Josef	Josef	k1gMnSc1
z	z	k7c2
Arimatie	Arimatie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
vyžádal	vyžádat	k5eAaPmAgMnS
od	od	k7c2
Piláta	Pilát	k1gMnSc2
mrtvé	mrtvý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
pohřbil	pohřbít	k5eAaPmAgInS
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
zachytit	zachytit	k5eAaPmF
krev	krev	k1gFnSc4
prýštící	prýštící	k2eAgFnSc4d1
z	z	k7c2
ukřižovaného	ukřižovaný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
legend	legenda	k1gFnPc2
Josef	Josef	k1gMnSc1
putoval	putovat	k5eAaImAgMnS
s	s	k7c7
Máří	Máří	k?
Magdalenou	Magdalena	k1gFnSc7
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grál	grál	k1gInSc1
zde	zde	k6eAd1
měl	mít	k5eAaImAgInS
chránit	chránit	k5eAaImF
rytířský	rytířský	k2eAgInSc1d1
řád	řád	k1gInSc1
templářů	templář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
Svatý	svatý	k2eAgInSc1d1
grál	grál	k1gInSc1
i	i	k9
s	s	k7c7
Josefovou	Josefův	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Glastonbury	Glastonbura	k1gFnSc2
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
britská	britský	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
přítomnost	přítomnost	k1gFnSc1
grálu	grál	k1gInSc2
na	na	k7c6
Artušově	Artušův	k2eAgInSc6d1
kulatém	kulatý	k2eAgInSc6d1
stole	stol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
teorií	teorie	k1gFnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
svatý	svatý	k2eAgInSc1d1
grál	grál	k1gInSc1
je	být	k5eAaImIp3nS
Máří	Máří	k?
Magdaléna	Magdaléna	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
některých	některý	k3yIgFnPc2
populárních	populární	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
Ježíšova	Ježíšův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kalich	kalich	k1gInSc1
znázorňuje	znázorňovat	k5eAaImIp3nS
starý	starý	k2eAgInSc1d1
symbol	symbol	k1gInSc1
ženství-ženského	ženství-ženský	k2eAgNnSc2d1
lůna	lůno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zřejmě	zřejmě	k6eAd1
nejstarší	starý	k2eAgFnSc4d3
verzi	verze	k1gFnSc4
dnes	dnes	k6eAd1
již	již	k6eAd1
běžně	běžně	k6eAd1
známé	známý	k2eAgFnPc1d1
pověsti	pověst	k1gFnPc1
o	o	k7c6
Grálu	grál	k1gInSc6
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
ve	v	k7c6
velšské	velšský	k2eAgFnSc6d1
básni	báseň	k1gFnSc6
vzniklé	vzniklý	k2eAgInPc1d1
přibližně	přibližně	k6eAd1
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Hovoří	hovořit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
o	o	k7c6
výpravě	výprava	k1gFnSc6
válečníků	válečník	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
legendárním	legendární	k2eAgMnSc7d1
anglickým	anglický	k2eAgMnSc7d1
králem	král	k1gMnSc7
Artušem	Artuš	k1gMnSc7
(	(	kIx(
<g/>
kolem	kolem	k7c2
roku	rok	k1gInSc2
500	#num#	k4
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
<g/>
)	)	kIx)
do	do	k7c2
Annwn	Annwna	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
keltský	keltský	k2eAgInSc4d1
onen	onen	k3xDgInSc4
svět	svět	k1gInSc4
(	(	kIx(
<g/>
zásvětí	zásvětí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc1
se	se	k3xPyFc4
plaví	plavit	k5eAaImIp3nP
po	po	k7c6
moři	moře	k1gNnSc6
a	a	k8xC
zaplétají	zaplétat	k5eAaImIp3nP
se	se	k3xPyFc4
do	do	k7c2
nebezpečných	bezpečný	k2eNgNnPc2d1
dobrodružství	dobrodružství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
jakousi	jakýsi	k3yIgFnSc4
kouzelnou	kouzelný	k2eAgFnSc4d1
nádobu	nádoba	k1gFnSc4
<g/>
,	,	kIx,
již	již	k6eAd1
střeží	střežit	k5eAaImIp3nS
devět	devět	k4xCc4
panen	panna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
příběhů	příběh	k1gInPc2
keltských	keltský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
z	z	k7c2
Británie	Británie	k1gFnSc2
a	a	k8xC
Bretaně	Bretaň	k1gFnSc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
v	v	k7c6
próze	próza	k1gFnSc6
či	či	k8xC
v	v	k7c6
básních	báseň	k1gFnPc6
<g/>
,	,	kIx,
vznikaly	vznikat	k5eAaImAgFnP
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
rozsáhlé	rozsáhlý	k2eAgInPc4d1
veršované	veršovaný	k2eAgInPc4d1
romány	román	k1gInPc4
francouzských	francouzský	k2eAgMnPc2d1
básníků	básník	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
keltský	keltský	k2eAgInSc1d1
kouzelný	kouzelný	k2eAgInSc1d1
svět	svět	k1gInSc1
přesadily	přesadit	k5eAaPmAgInP
do	do	k7c2
rámce	rámec	k1gInSc2
tamějšího	tamější	k2eAgInSc2d1
rytířského	rytířský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
tedy	tedy	k9
pověst	pověst	k1gFnSc1
o	o	k7c6
Grálu	grál	k1gInSc6
rozšířila	rozšířit	k5eAaPmAgFnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
básníky	básník	k1gMnPc4
jednoduché	jednoduchý	k2eAgFnSc2d1
hrdinu	hrdina	k1gMnSc4
této	tento	k3xDgFnSc3
pověsti	pověst	k1gFnSc3
Percevala	Percevala	k1gFnSc2
(	(	kIx(
<g/>
keltsky	keltsky	k6eAd1
Peredur	Peredura	k1gFnPc2
<g/>
)	)	kIx)
umístit	umístit	k5eAaPmF
na	na	k7c4
dvůr	dvůr	k1gInSc4
krále	král	k1gMnSc2
Artuše	Artuš	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgMnSc6
již	již	k6eAd1
žilo	žít	k5eAaImAgNnS
mnoho	mnoho	k4c1
vynikajících	vynikající	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
vytvořit	vytvořit	k5eAaPmF
z	z	k7c2
něj	on	k3xPp3gMnSc2
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
rytířů	rytíř	k1gMnPc2
pověstného	pověstný	k2eAgInSc2d1
kulatého	kulatý	k2eAgInSc2d1
stolu	stol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
dnešních	dnešní	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
ve	v	k7c4
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Chrétien	Chrétino	k1gNnPc2
de	de	k?
Troyes	Troyes	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
historicky	historicky	k6eAd1
jako	jako	k9
první	první	k4xOgMnSc1
zapracoval	zapracovat	k5eAaPmAgMnS
příběh	příběh	k1gInSc4
o	o	k7c6
Grálu	grál	k1gInSc6
do	do	k7c2
artušovských	artušovský	k2eAgFnPc2d1
legend	legenda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1185	#num#	k4
sepsal	sepsat	k5eAaPmAgMnS
dílo	dílo	k1gNnSc4
Perceval	Perceval	k1gFnSc2
li	li	k8xS
Galois	Galois	k1gFnSc2
ou	ou	k0
Les	les	k1gInSc4
Contes	Contes	k1gMnSc1
del	del	k?
Graal	Graal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
okolo	okolo	k7c2
roku	rok	k1gInSc2
1200	#num#	k4
<g/>
,	,	kIx,
složil	složit	k5eAaPmAgMnS
Robert	Robert	k1gMnSc1
de	de	k?
Boron	Boron	k1gMnSc1
na	na	k7c6
dvoře	dvůr	k1gInSc6
hraběte	hrabě	k1gMnSc2
Gautier	Gautier	k1gInSc1
de	de	k?
Montbéliard	Montbéliard	k1gInSc1
svoji	svůj	k3xOyFgFnSc4
Grand	grand	k1gMnSc1
estoire	estoir	k1gInSc5
dou	dou	k?
Graal	Graal	k1gInSc4
<g/>
,	,	kIx,
krátkou	krátký	k2eAgFnSc4d1
povídku	povídka	k1gFnSc4
o	o	k7c6
3514	#num#	k4
verších	verš	k1gInPc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nejdůležitější	důležitý	k2eAgNnSc4d3
starofrancouzské	starofrancouzský	k2eAgNnSc4d1
literární	literární	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
o	o	k7c6
Grálu	grál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Roberta	Robert	k1gMnSc4
de	de	k?
Boron	Boron	k1gMnSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
Grálem	grál	k1gInSc7
číše	číš	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
užita	užít	k5eAaPmNgFnS
u	u	k7c2
Poslední	poslední	k2eAgFnSc2d1
večeře	večeře	k1gFnSc2
a	a	k8xC
do	do	k7c2
níž	jenž	k3xRgFnSc2
později	pozdě	k6eAd2
Josef	Josef	k1gMnSc1
z	z	k7c2
Arimatie	Arimatie	k1gFnSc2
zachytil	zachytit	k5eAaPmAgMnS
Kristovu	Kristův	k2eAgFnSc4d1
vytékající	vytékající	k2eAgFnSc4d1
krev	krev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspirací	inspirace	k1gFnPc2
pro	pro	k7c4
Boronovu	Boronův	k2eAgFnSc4d1
báseň	báseň	k1gFnSc4
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
Nikodémův	Nikodémův	k2eAgInSc1d1
apokryf	apokryf	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Pilátovy	Pilátův	k2eAgInPc4d1
spisy	spis	k1gInPc4
<g/>
“	“	k?
–	–	k?
zpráva	zpráva	k1gFnSc1
o	o	k7c6
odsouzení	odsouzení	k1gNnSc6
a	a	k8xC
smrti	smrt	k1gFnSc6
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
římský	římský	k2eAgMnSc1d1
místodržitel	místodržitel	k1gMnSc1
Pilát	Pilát	k1gMnSc1
údajně	údajně	k6eAd1
poslal	poslat	k5eAaPmAgMnS
císaři	císař	k1gMnSc3
Tiberiovi	Tiberius	k1gMnSc3
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
ale	ale	k8xC
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
až	až	k9
koncem	koncem	k7c2
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1197	#num#	k4
a	a	k8xC
1210	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
Wolfram	wolfram	k1gInSc1
von	von	k1gInSc1
Eschenbach	Eschenbach	k1gInSc4
svého	svůj	k3xOyFgMnSc2
Parzivala	Parzivala	k1gMnSc2
<g/>
,	,	kIx,
dílo	dílo	k1gNnSc1
považované	považovaný	k2eAgNnSc1d1
za	za	k7c4
největší	veliký	k2eAgInSc4d3
epos	epos	k1gInSc4
německého	německý	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
předloha	předloha	k1gFnSc1
mu	on	k3xPp3gMnSc3
sloužilo	sloužit	k5eAaImAgNnS
básnické	básnický	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Chrétiena	Chrétieno	k1gNnSc2
de	de	k?
Troyes	Troyes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideální	ideální	k2eAgFnSc1d1
postava	postava	k1gFnSc1
Wolframova	Wolframův	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
Parzival	Parzival	k1gFnSc2
<g/>
,	,	kIx,
pozvolna	pozvolna	k6eAd1
přesáhne	přesáhnout	k5eAaPmIp3nS
pozemské	pozemský	k2eAgNnSc1d1
chápání	chápání	k1gNnSc1
pojmu	pojmout	k5eAaPmIp1nS
rytíře	rytíř	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nakonec	nakonec	k6eAd1
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
mnoha	mnoho	k4c2
pochybnostem	pochybnost	k1gFnPc3
<g/>
,	,	kIx,
bolesti	bolest	k1gFnSc3
a	a	k8xC
zkouškám	zkouška	k1gFnPc3
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
díky	díky	k7c3
svému	svůj	k3xOyFgNnSc3
čistému	čistý	k2eAgNnSc3d1
a	a	k8xC
hrdinnému	hrdinný	k2eAgNnSc3d1
snažení	snažení	k1gNnSc4
korunován	korunován	k2eAgMnSc1d1
za	za	k7c4
krále	král	k1gMnSc4
Grálu	grál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wolfram	wolfram	k1gInSc1
von	von	k1gInSc4
Eschenbach	Eschenbach	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
Grál	grál	k1gInSc4
jako	jako	k8xC,k8xS
kouzelný	kouzelný	k2eAgInSc4d1
<g/>
,	,	kIx,
zářící	zářící	k2eAgInSc4d1
drahokam	drahokam	k1gInSc4
s	s	k7c7
magickými	magický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
,	,	kIx,
seslaný	seslaný	k2eAgInSc1d1
na	na	k7c4
zem	zem	k1gFnSc4
anděly	anděl	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověku	člověk	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
pohlédne	pohlédnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
přinese	přinést	k5eAaPmIp3nS
nesmrtelnost	nesmrtelnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grál	grál	k1gInSc1
<g/>
,	,	kIx,
kámen	kámen	k1gInSc1
z	z	k7c2
jehož	jehož	k3xOyRp3gFnSc2
síly	síla	k1gFnSc2
rytíři	rytíř	k1gMnPc1
žijí	žít	k5eAaImIp3nP
<g/>
,	,	kIx,
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
„	„	k?
<g/>
lapsit	lapsit	k1gInSc1
exillis	exillis	k1gFnSc1
<g/>
“	“	k?
–	–	k?
mezi	mezi	k7c7
mnoha	mnoho	k4c7
významy	význam	k1gInPc7
převládá	převládat	k5eAaImIp3nS
„	„	k?
<g/>
z	z	k7c2
nebe	nebe	k1gNnSc2
spadlý	spadlý	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c4
Velký	velký	k2eAgInSc4d1
pátek	pátek	k1gInSc4
<g/>
,	,	kIx,
sestoupí	sestoupit	k5eAaPmIp3nS
z	z	k7c2
nebe	nebe	k1gNnSc2
holubice	holubice	k1gFnSc2
a	a	k8xC
položí	položit	k5eAaPmIp3nS
na	na	k7c4
kámen	kámen	k1gInSc4
hostii	hostie	k1gFnSc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
obnovení	obnovení	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přenesený	přenesený	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Jako	jako	k8xC,k8xS
svatý	svatý	k2eAgInSc1d1
grál	grál	k1gInSc1
bývají	bývat	k5eAaImIp3nP
označovány	označovat	k5eAaImNgInP
nedosažitelné	dosažitelný	k2eNgInPc1d1
předměty	předmět	k1gInPc1
nebo	nebo	k8xC
myšlenky	myšlenka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
člověk	člověk	k1gMnSc1
vyzkoumat	vyzkoumat	k5eAaPmF
nebo	nebo	k8xC
rozluštit	rozluštit	k5eAaPmF
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
dosáhnout	dosáhnout	k5eAaPmF
zásadního	zásadní	k2eAgInSc2d1
pokroku	pokrok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
různých	různý	k2eAgInPc6d1
vědních	vědní	k2eAgInPc6d1
oborech	obor	k1gInPc6
bývá	bývat	k5eAaImIp3nS
za	za	k7c4
svatý	svatý	k2eAgInSc4d1
grál	grál	k1gInSc4
označován	označován	k2eAgInSc4d1
např.	např.	kA
lék	lék	k1gInSc1
na	na	k7c4
rakovinu	rakovina	k1gFnSc4
<g/>
,	,	kIx,
porozumění	porozumění	k1gNnSc2
lidskému	lidský	k2eAgInSc3d1
genomu	genom	k1gInSc3
<g/>
,	,	kIx,
prokázání	prokázání	k1gNnSc3
mimozemského	mimozemský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
anti-gravitační	anti-gravitační	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
studená	studený	k2eAgFnSc1d1
fúze	fúze	k1gFnSc1
nebo	nebo	k8xC
teleportace	teleportace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Historie	historie	k1gFnPc4
Svatého	svatý	k2eAgInSc2d1
Grálu	grál	k1gInSc2
-	-	kIx~
SVATÝ	svatý	k2eAgInSc1d1
GRÁL	grál	k1gInSc1
A	a	k9
JEHO	jeho	k3xOp3gNnSc1
HLEDÁNÍ	hledání	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Král	Král	k1gMnSc1
Artuš	Artuš	k1gMnSc1
</s>
<s>
Parsifal	Parsifat	k5eAaPmAgMnS
<g/>
,	,	kIx,
opera	opera	k1gFnSc1
Richarda	Richard	k1gMnSc2
Wagnera	Wagner	k1gMnSc2
podle	podle	k7c2
legend	legenda	k1gFnPc2
o	o	k7c6
Grálu	grál	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4021799-1	4021799-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85056204	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85056204	#num#	k4
</s>
