<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
(	(	kIx(	(
<g/>
také	také	k9	také
neurčitek	neurčitek	k1gInSc1	neurčitek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neurčitý	určitý	k2eNgInSc4d1	neurčitý
tvar	tvar	k1gInSc4	tvar
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc4	způsob
ani	ani	k8xC	ani
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
základní	základní	k2eAgFnPc4d1	základní
(	(	kIx(	(
<g/>
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
<g/>
)	)	kIx)	)
tvar	tvar	k1gInSc4	tvar
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
makedonština	makedonština	k1gFnSc1	makedonština
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
infinitiv	infinitiv	k1gInSc1	infinitiv
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
musejí	muset	k5eAaImIp3nP	muset
ho	on	k3xPp3gInSc4	on
tedy	tedy	k9	tedy
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
opisně	opisně	k6eAd1	opisně
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc4	infinitiv
sice	sice	k8xC	sice
většinu	většina	k1gFnSc4	většina
mluvnických	mluvnický	k2eAgFnPc2d1	mluvnická
kategorií	kategorie	k1gFnPc2	kategorie
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gInSc4	jeho
název	název	k1gInSc4	název
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
schopnost	schopnost	k1gFnSc1	schopnost
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
např.	např.	kA	např.
slovesný	slovesný	k2eAgInSc4d1	slovesný
rod	rod	k1gInSc4	rod
-	-	kIx~	-
infinitiv	infinitiv	k1gInSc4	infinitiv
aktivní	aktivní	k2eAgInSc4d1	aktivní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vidět	vidět	k5eAaImF	vidět
<g/>
)	)	kIx)	)
x	x	k?	x
pasivní	pasivní	k2eAgInPc1d1	pasivní
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
viděn	viděn	k2eAgMnSc1d1	viděn
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vid	vid	k1gInSc1	vid
(	(	kIx(	(
<g/>
psát	psát	k5eAaImF	psát
x	x	k?	x
napsat	napsat	k5eAaBmF	napsat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
existují	existovat	k5eAaImIp3nP	existovat
infinitivy	infinitiv	k1gInPc1	infinitiv
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
infinitiv	infinitiv	k1gInSc4	infinitiv
minulý	minulý	k2eAgInSc4d1	minulý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začal	začít	k5eAaPmAgInS	začít
nebo	nebo	k8xC	nebo
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
před	před	k7c7	před
jiným	jiný	k2eAgInSc7d1	jiný
dějem	děj	k1gInSc7	děj
<g/>
.	.	kIx.	.
</s>
<s>
Infinitivy	infinitiv	k1gInPc1	infinitiv
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
různé	různý	k2eAgFnPc4d1	různá
využití	využití	k1gNnPc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
tvoření	tvoření	k1gNnSc4	tvoření
složených	složený	k2eAgInPc2d1	složený
časů	čas	k1gInPc2	čas
a	a	k8xC	a
způsobů	způsob	k1gInPc2	způsob
-	-	kIx~	-
např.	např.	kA	např.
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nebo	nebo	k8xC	nebo
němčině	němčina	k1gFnSc6	němčina
(	(	kIx(	(
<g/>
budu	být	k5eAaImBp1nS	být
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ich	ich	k?	ich
werde	werde	k6eAd1	werde
machen	machen	k?	machen
/	/	kIx~	/
ich	ich	k?	ich
werde	werde	k6eAd1	werde
gemacht	gemacht	k2eAgInSc4d1	gemacht
haben	haben	k1gInSc4	haben
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
<g/>
)	)	kIx)	)
kondenzace	kondenzace	k1gFnSc1	kondenzace
-	-	kIx~	-
krácení	krácení	k1gNnSc1	krácení
větných	větný	k2eAgFnPc2d1	větná
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
nahrazení	nahrazení	k1gNnSc4	nahrazení
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
věty	věta	k1gFnSc2	věta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
viděl	vidět	k5eAaImAgMnS	vidět
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
psát	psát	k5eAaImF	psát
dopis	dopis	k1gInSc1	dopis
místo	místo	k1gNnSc4	místo
viděl	vidět	k5eAaImAgInS	vidět
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
píše	psát	k5eAaImIp3nS	psát
dopis	dopis	k1gInSc1	dopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
využití	využití	k1gNnSc1	využití
méně	málo	k6eAd2	málo
běžné	běžný	k2eAgFnSc2d1	běžná
než	než	k8xS	než
např.	např.	kA	např.
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
;	;	kIx,	;
po	po	k7c6	po
způsobových	způsobový	k2eAgInPc6d1	způsobový
a	a	k8xC	a
neplnovýznamových	plnovýznamový	k2eNgInPc6d1	plnovýznamový
slovesech	sloveso	k1gNnPc6	sloveso
jakožto	jakožto	k8xS	jakožto
součást	součást	k1gFnSc1	součást
(	(	kIx(	(
<g/>
modifikace	modifikace	k1gFnSc1	modifikace
<g/>
)	)	kIx)	)
přísudku	přísudek	k1gInSc2	přísudek
(	(	kIx(	(
<g/>
chci	chtít	k5eAaImIp1nS	chtít
to	ten	k3xDgNnSc1	ten
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
umím	umět	k5eAaImIp1nS	umět
plavat	plavat	k5eAaImF	plavat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
forma	forma	k1gFnSc1	forma
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
lemma	lemma	k1gFnSc1	lemma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
imperativ	imperativ	k1gInSc4	imperativ
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
:	:	kIx,	:
Aufstehen	Aufstehen	k1gInSc1	Aufstehen
<g/>
!	!	kIx.	!
</s>
<s>
Vstávat	vstávat	k5eAaImF	vstávat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
infinitiv	infinitiv	k1gInSc1	infinitiv
tvoří	tvořit	k5eAaImIp3nS	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
-	-	kIx~	-
<g/>
t	t	k?	t
(	(	kIx(	(
<g/>
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
prosit	prosit	k5eAaImF	prosit
<g/>
,	,	kIx,	,
krýt	krýt	k5eAaImF	krýt
<g/>
,	,	kIx,	,
tisknout	tisknout	k5eAaImF	tisknout
<g/>
)	)	kIx)	)
a	a	k8xC	a
-	-	kIx~	-
<g/>
ci	ci	k0	ci
(	(	kIx(	(
<g/>
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
péci	péct	k5eAaImF	péct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
-	-	kIx~	-
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
na	na	k7c6	na
-	-	kIx~	-
<g/>
ti	ten	k3xDgMnPc1	ten
(	(	kIx(	(
<g/>
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
jako	jako	k8xS	jako
nésti	nést	k5eAaImF	nést
<g/>
,	,	kIx,	,
dělati	dělat	k5eAaImF	dělat
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
přidáním	přidání	k1gNnSc7	přidání
-	-	kIx~	-
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
knižní	knižní	k2eAgInSc4d1	knižní
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
ci	ci	k0	ci
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nahrazována	nahrazován	k2eAgFnSc1d1	nahrazována
-	-	kIx~	-
<g/>
ct	ct	k?	ct
(	(	kIx(	(
<g/>
moct	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
péct	péct	k5eAaImF	péct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
t	t	k?	t
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
supina	supinum	k1gNnSc2	supinum
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
tvořil	tvořit	k5eAaImAgInS	tvořit
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgNnPc2d1	nedokonavé
sloves	sloveso	k1gNnPc2	sloveso
a	a	k8xC	a
zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jen	jen	k9	jen
po	po	k7c6	po
slovesech	sloveso	k1gNnPc6	sloveso
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
supinum	supinum	k1gNnSc1	supinum
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
infinitivem	infinitiv	k1gInSc7	infinitiv
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
jsou	být	k5eAaImIp3nP	být
dubletní	dubletní	k2eAgInPc4d1	dubletní
tvary	tvar	k1gInPc4	tvar
k	k	k7c3	k
původním	původní	k2eAgInPc3d1	původní
infinitivním	infinitivní	k2eAgInPc3d1	infinitivní
tvarům	tvar	k1gInPc3	tvar
-	-	kIx~	-
ke	k	k7c3	k
tvarům	tvar	k1gInPc3	tvar
se	s	k7c7	s
zakončením	zakončení	k1gNnSc7	zakončení
-	-	kIx~	-
<g/>
ti	ti	k0	ti
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
i	i	k8xC	i
<g/>
)	)	kIx)	)
přibyly	přibýt	k5eAaPmAgInP	přibýt
tvary	tvar	k1gInPc1	tvar
zakončené	zakončený	k2eAgInPc1d1	zakončený
na	na	k7c4	na
-t	-t	k?	-t
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zcela	zcela	k6eAd1	zcela
<g />
.	.	kIx.	.
</s>
<s>
převládly	převládnout	k5eAaPmAgFnP	převládnout
i	i	k9	i
v	v	k7c6	v
psaném	psaný	k2eAgInSc6d1	psaný
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
n.	n.	k?	n.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Slovníku	slovník	k1gInSc2	slovník
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
opr	opr	k?	opr
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
základní	základní	k2eAgNnSc1d1	základní
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
koncovku	koncovka	k1gFnSc4	koncovka
s	s	k7c7	s
-i	-i	k?	-i
(	(	kIx(	(
<g/>
stříci	stříct	k5eAaPmF	stříct
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
nových	nový	k2eAgNnPc2d1	nové
sloves	sloveso	k1gNnPc2	sloveso
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
tvar	tvar	k1gInSc1	tvar
s	s	k7c7	s
-i	-i	k?	-i
umělým	umělý	k2eAgInSc7d1	umělý
konstruktem	konstrukt	k1gInSc7	konstrukt
(	(	kIx(	(
<g/>
mailovat	mailovat	k5eAaPmF	mailovat
<g/>
,	,	kIx,	,
chatovat	chatovat	k5eAaImF	chatovat
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
-ť	-ť	k?	-ť
(	(	kIx(	(
<g/>
т	т	k?	т
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
obvykle	obvykle	k6eAd1	obvykle
předchází	předcházet	k5eAaImIp3nS	předcházet
samohláska	samohláska	k1gFnSc1	samohláska
<g/>
;	;	kIx,	;
některá	některý	k3yIgFnSc1	některý
slovesa	sloveso	k1gNnSc2	sloveso
změnila	změnit	k5eAaPmAgFnS	změnit
ť	ť	k?	ť
na	na	k7c4	na
č	č	k0	č
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
ч	ч	k?	ч
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
*	*	kIx~	*
<g/>
м	м	k?	м
→	→	k?	→
м	м	k?	м
(	(	kIx(	(
<g/>
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
též	též	k9	též
-ti	i	k?	-ti
(	(	kIx(	(
<g/>
т	т	k?	т
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
и	и	k?	и
(	(	kIx(	(
<g/>
jít	jít	k5eAaImF	jít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polštině	polština	k1gFnSc6	polština
končí	končit	k5eAaImIp3nS	končit
infinitiv	infinitiv	k1gInSc1	infinitiv
na	na	k7c4	na
-ć	-ć	k?	-ć
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ť	ť	k?	ť
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
-c	-c	k?	-c
(	(	kIx(	(
<g/>
wlec	wlec	k1gInSc1	wlec
<g/>
,	,	kIx,	,
móc	móc	k?	móc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
končí	končit	k5eAaImIp3nS	končit
infinitiv	infinitiv	k1gInSc4	infinitiv
na	na	k7c4	na
-	-	kIx~	-
<g/>
ť.	ť.	k?	ť.
V	v	k7c6	v
bulharštině	bulharština	k1gFnSc6	bulharština
a	a	k8xC	a
makedonštině	makedonština	k1gFnSc6	makedonština
infinitiv	infinitiv	k1gInSc1	infinitiv
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
se	se	k3xPyFc4	se
opisně	opisně	k6eAd1	opisně
pomocí	pomocí	k7c2	pomocí
částice	částice	k1gFnSc2	částice
д	д	k?	д
a	a	k8xC	a
slovesa	sloveso	k1gNnSc2	sloveso
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
téže	tenže	k3xDgFnSc2	tenže
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
jako	jako	k9	jako
sloveso	sloveso	k1gNnSc4	sloveso
řídící	řídící	k2eAgFnSc2d1	řídící
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bulh.	bulh.	k?	bulh.
и	и	k?	и
д	д	k?	д
к	к	k?	к
=	=	kIx~	=
chceš	chtít	k5eAaImIp2nS	chtít
koupit	koupit	k5eAaPmF	koupit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
1	[number]	k4	1
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
model	model	k1gInSc4	model
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
novořečtině	novořečtina	k1gFnSc6	novořečtina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
částicí	částice	k1gFnSc7	částice
je	být	k5eAaImIp3nS	být
ν	ν	k?	ν
(	(	kIx(	(
<g/>
např.	např.	kA	např.
χ	χ	k?	χ
ν	ν	k?	ν
φ	φ	k?	φ
=	=	kIx~	=
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
se	se	k3xPyFc4	se
najíst	najíst	k5eAaPmF	najíst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srbštině	srbština	k1gFnSc6	srbština
sice	sice	k8xC	sice
infinitiv	infinitiv	k1gInSc1	infinitiv
oficiálně	oficiálně	k6eAd1	oficiálně
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přestává	přestávat	k5eAaImIp3nS	přestávat
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
infinitivu	infinitiv	k1gInSc2	infinitiv
částice	částice	k1gFnSc2	částice
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
wait	wait	k2eAgMnSc1d1	wait
=	=	kIx~	=
čekat	čekat	k5eAaImF	čekat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
slovníkový	slovníkový	k2eAgInSc1d1	slovníkový
tvar	tvar	k1gInSc1	tvar
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
imperativ	imperativ	k1gInSc1	imperativ
(	(	kIx(	(
<g/>
Wait	Wait	k1gInSc1	Wait
<g/>
.	.	kIx.	.
=	=	kIx~	=
Čekej	čekat	k5eAaImRp2nS	čekat
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
Čekejte	čekat	k5eAaImRp2nP	čekat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
užívání	užívání	k1gNnSc4	užívání
po	po	k7c6	po
modálních	modální	k2eAgFnPc6d1	modální
a	a	k8xC	a
některých	některý	k3yIgNnPc6	některý
dalších	další	k2eAgNnPc6d1	další
slovesech	sloveso	k1gNnPc6	sloveso
(	(	kIx(	(
<g/>
They	Thea	k1gFnSc2	Thea
may	may	k?	may
be	be	k?	be
here	herat	k5eAaPmIp3nS	herat
soon	soon	k1gInSc1	soon
<g/>
.	.	kIx.	.
=	=	kIx~	=
Možná	možná	k9	možná
zde	zde	k6eAd1	zde
budou	být	k5eAaImBp3nP	být
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
<s>
She	She	k?	She
saw	saw	k?	saw
them	them	k1gInSc1	them
sit	sit	k?	sit
in	in	k?	in
the	the	k?	the
garden	gardna	k1gFnPc2	gardna
<g/>
.	.	kIx.	.
=	=	kIx~	=
Viděla	vidět	k5eAaImAgFnS	vidět
je	on	k3xPp3gMnPc4	on
sedět	sedět	k5eAaImF	sedět
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
průběhový	průběhový	k2eAgInSc1d1	průběhový
infinitiv	infinitiv	k1gInSc1	infinitiv
(	(	kIx(	(
<g/>
It	It	k1gFnSc1	It
is	is	k?	is
not	nota	k1gFnPc2	nota
easy	easy	k1gInPc1	easy
to	ten	k3xDgNnSc1	ten
be	be	k?	be
swimming	swimming	k1gInSc1	swimming
so	so	k?	so
long	long	k1gInSc1	long
<g/>
.	.	kIx.	.
=	=	kIx~	=
Není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
být	být	k5eAaImF	být
plavající	plavající	k2eAgMnSc1d1	plavající
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
minulý	minulý	k2eAgInSc4d1	minulý
infinitiv	infinitiv	k1gInSc4	infinitiv
(	(	kIx(	(
<g/>
You	You	k1gMnSc1	You
must	must	k1gMnSc1	must
have	havat	k5eAaPmIp3nS	havat
seen	seen	k1gInSc4	seen
me	me	k?	me
<g/>
.	.	kIx.	.
=	=	kIx~	=
Určitě	určitě	k6eAd1	určitě
jsi	být	k5eAaImIp2nS	být
mě	já	k3xPp1nSc4	já
byl	být	k5eAaImAgInS	být
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
She	She	k?	She
seemed	seemed	k1gInSc1	seemed
to	ten	k3xDgNnSc4	ten
have	havat	k5eAaPmIp3nS	havat
been	been	k1gInSc1	been
crying	crying	k1gInSc1	crying
<g/>
.	.	kIx.	.
=	=	kIx~	=
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
plakala	plakat	k5eAaImAgFnS	plakat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
infinitiv	infinitiv	k1gInSc1	infinitiv
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Slovesa	sloveso	k1gNnSc2	sloveso
vzoru	vzor	k1gInSc2	vzor
péct	péct	k5eAaImF	péct
(	(	kIx(	(
<g/>
infinitiv	infinitiv	k1gInSc1	infinitiv
-ci	i	k?	-ci
-	-	kIx~	-
-ct	t	k?	-ct
<g/>
)	)	kIx)	)
v	v	k7c4	v
Internetové	internetový	k2eAgFnPc4d1	internetová
jazykové	jazykový	k2eAgFnPc4d1	jazyková
příruce	příruce	k1gFnPc4	příruce
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
