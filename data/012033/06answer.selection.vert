<s>
Fontána	fontána	k1gFnSc1	fontána
(	(	kIx(	(
<g/>
ital	ital	k1gInSc1	ital
<g/>
.	.	kIx.	.
fontana	fontana	k1gFnSc1	fontana
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
fons	fons	k1gInSc1	fons
<g/>
,	,	kIx,	,
pramen	pramen	k1gInSc1	pramen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
kamenná	kamenný	k2eAgFnSc1d1	kamenná
nebo	nebo	k8xC	nebo
kovová	kovový	k2eAgFnSc1d1	kovová
kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
nebo	nebo	k8xC	nebo
stříkající	stříkající	k2eAgFnSc7d1	stříkající
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doplněná	doplněný	k2eAgNnPc4d1	doplněné
vodotryskem	vodotrysk	k1gInSc7	vodotrysk
nebo	nebo	k8xC	nebo
různými	různý	k2eAgFnPc7d1	různá
umělými	umělý	k2eAgFnPc7d1	umělá
vodními	vodní	k2eAgFnPc7d1	vodní
kaskádami	kaskáda	k1gFnPc7	kaskáda
<g/>
.	.	kIx.	.
</s>
