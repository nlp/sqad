<p>
<s>
Fontána	fontána	k1gFnSc1	fontána
(	(	kIx(	(
<g/>
ital	ital	k1gInSc1	ital
<g/>
.	.	kIx.	.
fontana	fontana	k1gFnSc1	fontana
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
fons	fons	k1gInSc1	fons
<g/>
,	,	kIx,	,
pramen	pramen	k1gInSc1	pramen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
kamenná	kamenný	k2eAgFnSc1d1	kamenná
nebo	nebo	k8xC	nebo
kovová	kovový	k2eAgFnSc1d1	kovová
kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
nebo	nebo	k8xC	nebo
stříkající	stříkající	k2eAgFnSc7d1	stříkající
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doplněná	doplněný	k2eAgNnPc4d1	doplněné
vodotryskem	vodotrysk	k1gInSc7	vodotrysk
nebo	nebo	k8xC	nebo
různými	různý	k2eAgFnPc7d1	různá
umělými	umělý	k2eAgFnPc7d1	umělá
vodními	vodní	k2eAgFnPc7d1	vodní
kaskádami	kaskáda	k1gFnPc7	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zdobena	zdobit	k5eAaImNgFnS	zdobit
figurálními	figurální	k2eAgInPc7d1	figurální
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
plastikami	plastika	k1gFnPc7	plastika
<g/>
,	,	kIx,	,
mísovitými	mísovitý	k2eAgInPc7d1	mísovitý
nebo	nebo	k8xC	nebo
mušlovitými	mušlovitý	k2eAgInPc7d1	mušlovitý
přepady	přepad	k1gInPc7	přepad
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
umístění	umístění	k1gNnSc1	umístění
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
frekventovaných	frekventovaný	k2eAgNnPc6d1	frekventované
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zámeckých	zámecký	k2eAgFnPc2d1	zámecká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
fontány	fontána	k1gFnPc1	fontána
často	často	k6eAd1	často
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
různými	různý	k2eAgInPc7d1	různý
světelnými	světelný	k2eAgInPc7d1	světelný
efekty	efekt	k1gInPc7	efekt
a	a	k8xC	a
proud	proud	k1gInSc1	proud
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
řízen	řídit	k5eAaImNgInS	řídit
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Fontána	fontána	k1gFnSc1	fontána
je	být	k5eAaImIp3nS	být
také	také	k9	také
prvek	prvek	k1gInSc1	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
prosté	prostý	k2eAgNnSc1d1	prosté
zásobování	zásobování	k1gNnSc1	zásobování
vodou	voda	k1gFnSc7	voda
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
fontána	fontána	k1gFnSc1	fontána
luxus	luxus	k1gInSc4	luxus
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
technický	technický	k2eAgInSc1d1	technický
div	div	k1gInSc1	div
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
totiž	totiž	k9	totiž
tlakovou	tlakový	k2eAgFnSc4d1	tlaková
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
vynálezu	vynález	k1gInSc2	vynález
vodních	vodní	k2eAgNnPc2d1	vodní
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
a	a	k8xC	a
uzavřené	uzavřený	k2eAgNnSc4d1	uzavřené
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
trubkách	trubka	k1gFnPc6	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
Zpívající	zpívající	k2eAgFnSc1d1	zpívající
fontána	fontána	k1gFnSc1	fontána
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
byla	být	k5eAaImAgNnP	být
napájena	napájet	k5eAaImNgNnP	napájet
z	z	k7c2	z
nádrží	nádrž	k1gFnPc2	nádrž
ve	v	k7c6	v
Střešovicích	Střešovice	k1gFnPc6	Střešovice
a	a	k8xC	a
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
tlakové	tlakový	k2eAgInPc1d1	tlakový
vodovody	vodovod	k1gInPc1	vodovod
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
kašen	kašna	k1gFnPc2	kašna
<g/>
,	,	kIx,	,
napájené	napájený	k2eAgInPc1d1	napájený
z	z	k7c2	z
vodárenské	vodárenský	k2eAgFnSc2d1	vodárenská
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
čerpala	čerpat	k5eAaImAgFnS	čerpat
mlýnskými	mlýnský	k2eAgNnPc7d1	mlýnské
koly	kolo	k1gNnPc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
elektrických	elektrický	k2eAgNnPc2d1	elektrické
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vytvářet	vytvářet	k5eAaImF	vytvářet
potřebný	potřebný	k2eAgInSc4d1	potřebný
tlak	tlak	k1gInSc4	tlak
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
moderní	moderní	k2eAgInPc1d1	moderní
fontány	fontán	k1gInPc1	fontán
většinou	většinou	k6eAd1	většinou
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
oběhem	oběh	k1gInSc7	oběh
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fontány	fontána	k1gFnPc1	fontána
a	a	k8xC	a
kašny	kašna	k1gFnPc1	kašna
bývají	bývat	k5eAaImIp3nP	bývat
velkou	velký	k2eAgFnSc7d1	velká
chloubou	chlouba	k1gFnSc7	chlouba
a	a	k8xC	a
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
některých	některý	k3yIgNnPc2	některý
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
známé	známá	k1gFnPc1	známá
jsou	být	k5eAaImIp3nP	být
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
fontány	fontána	k1gFnPc1	fontána
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Římě	Řím	k1gInSc6	Řím
či	či	k8xC	či
pražská	pražský	k2eAgFnSc1d1	Pražská
Zpívající	zpívající	k2eAgFnSc1d1	zpívající
fontána	fontána	k1gFnSc1	fontána
či	či	k8xC	či
Křižíkova	Křižíkův	k2eAgFnSc1d1	Křižíkova
fontána	fontána	k1gFnSc1	fontána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kašna	kašna	k1gFnSc1	kašna
</s>
</p>
<p>
<s>
Zpívající	zpívající	k2eAgFnSc1d1	zpívající
fontána	fontána	k1gFnSc1	fontána
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
fontána	fontána	k1gFnSc1	fontána
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
fontána	fontána	k1gFnSc1	fontána
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
