<s>
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
nádherným	nádherný	k2eAgInSc7d1	nádherný
vzhledem	vzhled	k1gInSc7	vzhled
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
krásou	krása	k1gFnSc7	krása
jeho	on	k3xPp3gNnSc2	on
listí	listí	k1gNnSc2	listí
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
básně	báseň	k1gFnSc2	báseň
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Ginkgo	Ginkgo	k1gNnSc1	Ginkgo
biloba	biloba	k1gFnSc1	biloba
<g/>
.	.	kIx.	.
</s>
