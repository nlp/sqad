<s>
Síra	síra	k1gFnSc1	síra
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
S	s	k7c7	s
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Sulphur	Sulphura	k1gFnPc2	Sulphura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekovový	kovový	k2eNgInSc1d1	nekovový
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
stabilní	stabilní	k2eAgInPc1d1	stabilní
izotopy	izotop	k1gInPc1	izotop
<g/>
:	:	kIx,	:
32	[number]	k4	32
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
33	[number]	k4	33
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
34S	[number]	k4	34S
a	a	k8xC	a
36S	[number]	k4	36S
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
izotopů	izotop	k1gInPc2	izotop
je	být	k5eAaImIp3nS	být
nestabilních	stabilní	k2eNgFnPc2d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
síra	síra	k1gFnSc1	síra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
alotropických	alotropický	k2eAgFnPc6d1	alotropický
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
:	:	kIx,	:
Kosočtverečná	kosočtverečný	k2eAgFnSc1d1	kosočtverečná
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
)	)	kIx)	)
<g/>
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
modifikace	modifikace	k1gFnSc1	modifikace
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
postupně	postupně	k6eAd1	postupně
přecházejí	přecházet	k5eAaImIp3nP	přecházet
ostatní	ostatní	k2eAgFnPc4d1	ostatní
modifikace	modifikace	k1gFnPc4	modifikace
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
látka	látka	k1gFnSc1	látka
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
sirouhlíku	sirouhlík	k1gInSc6	sirouhlík
<g/>
,	,	kIx,	,
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
nebo	nebo	k8xC	nebo
etheru	ether	k1gInSc6	ether
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc4d1	dobrý
elektrický	elektrický	k2eAgInSc4d1	elektrický
a	a	k8xC	a
tepelný	tepelný	k2eAgInSc4d1	tepelný
izolant	izolant	k1gInSc4	izolant
<g/>
,	,	kIx,	,
molekula	molekula	k1gFnSc1	molekula
je	být	k5eAaImIp3nS	být
monocyklická	monocyklický	k2eAgFnSc1d1	monocyklický
<g/>
,	,	kIx,	,
oktaatomická	oktaatomický	k2eAgFnSc1d1	oktaatomický
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
95,3	[number]	k4	95,3
°	°	k?	°
<g/>
C	C	kA	C
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
modifikaci	modifikace	k1gFnSc4	modifikace
jednoklonnou	jednoklonný	k2eAgFnSc4d1	jednoklonná
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připraví	připravit	k5eAaPmIp3nS	připravit
se	se	k3xPyFc4	se
krystalizací	krystalizace	k1gFnSc7	krystalizace
kapalné	kapalný	k2eAgFnSc2d1	kapalná
síry	síra	k1gFnSc2	síra
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
rychlým	rychlý	k2eAgNnSc7d1	rychlé
ochlazením	ochlazení	k1gNnSc7	ochlazení
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
Jednoklonná	jednoklonný	k2eAgFnSc1d1	jednoklonná
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
říká	říkat	k5eAaImIp3nS	říkat
<g />
.	.	kIx.	.
</s>
<s>
perleťová	perleťový	k2eAgFnSc1d1	perleťová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
pomalým	pomalý	k2eAgNnSc7d1	pomalé
ochlazováním	ochlazování	k1gNnSc7	ochlazování
taveniny	tavenina	k1gFnSc2	tavenina
síry	síra	k1gFnSc2	síra
z	z	k7c2	z
teploty	teplota	k1gFnSc2	teplota
nad	nad	k7c7	nad
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
molekuly	molekula	k1gFnSc2	molekula
jsou	být	k5eAaImIp3nP	být
cyklické	cyklický	k2eAgFnSc3d1	cyklická
oktaatomické	oktaatomický	k2eAgFnSc3d1	oktaatomický
-	-	kIx~	-
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
těsnější	těsný	k2eAgFnPc4d2	těsnější
než	než	k8xS	než
u	u	k7c2	u
β	β	k?	β
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
formu	forma	k1gFnSc4	forma
α	α	k?	α
Homocyklické	homocyklický	k2eAgFnPc1d1	homocyklický
formy	forma	k1gFnPc1	forma
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc4d1	tvořen
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
6	[number]	k4	6
až	až	k9	až
20	[number]	k4	20
atomů	atom	k1gInPc2	atom
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
tato	tento	k3xDgFnSc1	tento
polysíra	polysír	k1gMnSc4	polysír
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
formách	forma	k1gFnPc6	forma
-	-	kIx~	-
kaučukovitá	kaučukovitý	k2eAgFnSc1d1	kaučukovitý
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
plastická	plastický	k2eAgFnSc1d1	plastická
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
polymerní	polymerní	k2eAgFnSc1d1	polymerní
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
supersublimovaná	supersublimovaný	k2eAgFnSc1d1	supersublimovaný
síra	síra	k1gFnSc1	síra
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
metastabilní	metastabilní	k2eAgFnPc1d1	metastabilní
alotropické	alotropický	k2eAgFnPc1d1	alotropický
směsi	směs	k1gFnPc1	směs
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
srážením	srážení	k1gNnSc7	srážení
síry	síra	k1gFnSc2	síra
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
nebo	nebo	k8xC	nebo
ochlazením	ochlazení	k1gNnSc7	ochlazení
horké	horký	k2eAgFnSc2d1	horká
kapalné	kapalný	k2eAgFnSc2d1	kapalná
síry	síra	k1gFnSc2	síra
z	z	k7c2	z
teploty	teplota	k1gFnSc2	teplota
okolo	okolo	k7c2	okolo
400	[number]	k4	400
°	°	k?	°
<g/>
C.	C.	kA	C.
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
šroubovice	šroubovice	k1gFnSc2	šroubovice
<g/>
,	,	kIx,	,
cyklo-S	cyklo-S	k?	cyklo-S
<g/>
8	[number]	k4	8
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
molekulové	molekulový	k2eAgFnPc1d1	molekulová
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
formy	forma	k1gFnPc1	forma
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c6	na
Sα	Sα	k1gFnSc6	Sα
<g/>
.	.	kIx.	.
</s>
<s>
Rychlým	rychlý	k2eAgNnSc7d1	rychlé
ochlazením	ochlazení	k1gNnSc7	ochlazení
par	para	k1gFnPc2	para
síry	síra	k1gFnSc2	síra
vzniká	vznikat	k5eAaImIp3nS	vznikat
sirný	sirný	k2eAgInSc4d1	sirný
květ	květ	k1gInSc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
taje	tát	k5eAaImIp3nS	tát
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
114	[number]	k4	114
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
žluté	žlutý	k2eAgFnPc4d1	žlutá
průhledné	průhledný	k2eAgFnPc4d1	průhledná
kapaliny	kapalina	k1gFnPc4	kapalina
<g/>
,	,	kIx,	,
kapalné	kapalný	k2eAgFnPc4d1	kapalná
síry	síra	k1gFnPc4	síra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc6	zvýšení
teploty	teplota	k1gFnSc2	teplota
nad	nad	k7c7	nad
160	[number]	k4	160
°	°	k?	°
<g/>
C	C	kA	C
kapalina	kapalina	k1gFnSc1	kapalina
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
viskóznější	viskózní	k2eAgMnSc1d2	viskóznější
a	a	k8xC	a
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
444,5	[number]	k4	444,5
°	°	k?	°
<g/>
C	C	kA	C
vře	vřít	k5eAaImIp3nS	vřít
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
oranžové	oranžový	k2eAgInPc4d1	oranžový
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
z	z	k7c2	z
osmi-	osmi-	k?	osmi-
a	a	k8xC	a
šestiatomových	šestiatomový	k2eAgFnPc2d1	šestiatomový
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
teplotou	teplota	k1gFnSc7	teplota
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
na	na	k7c4	na
čtyř-	čtyř-	k?	čtyř-
a	a	k8xC	a
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
860	[number]	k4	860
°	°	k?	°
<g/>
C	C	kA	C
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
parách	para	k1gFnPc6	para
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgInPc1d1	samostatný
atomy	atom	k1gInPc1	atom
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
až	až	k9	až
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
2	[number]	k4	2
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
Síra	síra	k1gFnSc1	síra
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
prvek	prvek	k1gInSc1	prvek
<g/>
;	;	kIx,	;
přímo	přímo	k6eAd1	přímo
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
prvky	prvek	k1gInPc7	prvek
kromě	kromě	k7c2	kromě
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
telluru	tellur	k1gInSc2	tellur
<g/>
,	,	kIx,	,
jodu	jod	k1gInSc2	jod
<g/>
,	,	kIx,	,
iridia	iridium	k1gNnSc2	iridium
<g/>
,	,	kIx,	,
platiny	platina	k1gFnSc2	platina
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
120	[number]	k4	120
°	°	k?	°
<g/>
C	C	kA	C
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
plynného	plynný	k2eAgInSc2d1	plynný
fluoru	fluor	k1gInSc2	fluor
se	se	k3xPyFc4	se
vznítí	vznítit	k5eAaPmIp3nS	vznítit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
SF	SF	kA	SF
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnPc1	reakce
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
halogeny	halogen	k1gInPc7	halogen
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
normální	normální	k2eAgFnSc6d1	normální
teplotě	teplota	k1gFnSc6	teplota
klidně	klidně	k6eAd1	klidně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
čistým	čistý	k2eAgInSc7d1	čistý
kyslíkem	kyslík	k1gInSc7	kyslík
síra	síra	k1gFnSc1	síra
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
nekovy	nekov	k1gInPc1	nekov
reagují	reagovat	k5eAaBmIp3nP	reagovat
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
až	až	k9	až
za	za	k7c4	za
zvýšené	zvýšený	k2eAgFnPc4d1	zvýšená
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1	přechodný
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
a	a	k8xC	a
aktinoidy	aktinoida	k1gFnPc1	aktinoida
reagují	reagovat	k5eAaBmIp3nP	reagovat
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
živě	živě	k6eAd1	živě
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
podvojných	podvojný	k2eAgInPc2d1	podvojný
sulfidů	sulfid	k1gInPc2	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
hoří	hořet	k5eAaImIp3nS	hořet
na	na	k7c4	na
vzduchu	vzduch	k1gInSc2	vzduch
modrým	modrý	k2eAgInSc7d1	modrý
plamenem	plamen	k1gInSc7	plamen
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
SO2	SO2	k1gFnPc7	SO2
a	a	k8xC	a
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
i	i	k9	i
oxidu	oxid	k1gInSc2	oxid
sírového	sírový	k2eAgInSc2d1	sírový
SO	So	kA	So
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
oxidační	oxidační	k2eAgFnPc1d1	oxidační
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
S	s	k7c7	s
+	+	kIx~	+
2	[number]	k4	2
HNO3	HNO3	k1gMnSc1	HNO3
→	→	k?	→
H2SO4	H2SO4	k1gMnSc1	H2SO4
+	+	kIx~	+
2	[number]	k4	2
NO	no	k9	no
Reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
hydroxidy	hydroxid	k1gInPc7	hydroxid
vzniká	vznikat	k5eAaImIp3nS	vznikat
thiosíran	thiosíran	k1gInSc1	thiosíran
a	a	k8xC	a
sulfid	sulfid	k1gInSc1	sulfid
<g/>
:	:	kIx,	:
4	[number]	k4	4
S	s	k7c7	s
+	+	kIx~	+
6	[number]	k4	6
KOH	KOH	kA	KOH
→	→	k?	→
K2S2O3	K2S2O3	k1gMnSc1	K2S2O3
+	+	kIx~	+
2	[number]	k4	2
K2S	K2S	k1gFnPc2	K2S
+	+	kIx~	+
3	[number]	k4	3
H2O	H2O	k1gFnPc2	H2O
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kyslíku	kyslík	k1gInSc2	kyslík
může	moct	k5eAaImIp3nS	moct
síra	síra	k1gFnSc1	síra
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
orbitalu	orbital	k1gInSc2	orbital
d.	d.	k?	d.
Atomy	atom	k1gInPc1	atom
síry	síra	k1gFnSc2	síra
můžou	můžou	k?	můžou
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
excitovaném	excitovaný	k2eAgInSc6d1	excitovaný
stavu	stav	k1gInSc6	stav
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
S	s	k7c7	s
<g/>
*	*	kIx~	*
(	(	kIx(	(
<g/>
čtyř-vazný	čtyřazný	k2eAgInSc1d1	čtyř-vazný
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
+	+	kIx~	+
<g/>
IV	IV	kA	IV
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
S	s	k7c7	s
<g/>
**	**	k?	**
(	(	kIx(	(
<g/>
šesti-vazný	šestiazný	k2eAgInSc1d1	šesti-vazný
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
+	+	kIx~	+
<g/>
VI	VI	kA	VI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
nejčastěji	často	k6eAd3	často
tvoří	tvořit	k5eAaImIp3nS	tvořit
kovalentní	kovalentní	k2eAgFnPc4d1	kovalentní
vazby	vazba	k1gFnPc4	vazba
(	(	kIx(	(
<g/>
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
dvojné	dvojný	k2eAgFnPc1d1	dvojná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
látkách	látka	k1gFnPc6	látka
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
vazba	vazba	k1gFnSc1	vazba
koordinačně-kovalentní	koordinačněovalentní	k2eAgFnSc1d1	koordinačně-kovalentní
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
atomy	atom	k1gInPc4	atom
síry	síra	k1gFnSc2	síra
mají	mít	k5eAaImIp3nP	mít
roli	role	k1gFnSc4	role
donoru	donor	k1gInSc2	donor
(	(	kIx(	(
<g/>
dárce	dárce	k1gMnSc1	dárce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iontová	iontový	k2eAgFnSc1d1	iontová
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
síry	síra	k1gFnSc2	síra
též	též	k9	též
možná	možná	k9	možná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
střední	střední	k2eAgFnSc3d1	střední
elektronegativitě	elektronegativita	k1gFnSc3	elektronegativita
síry	síra	k1gFnSc2	síra
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
vzácnější	vzácný	k2eAgFnSc1d2	vzácnější
-	-	kIx~	-
sloučeniny	sloučenina	k1gFnPc1	sloučenina
síry	síra	k1gFnSc2	síra
s	s	k7c7	s
iontovou	iontový	k2eAgFnSc7d1	iontová
vazbou	vazba	k1gFnSc7	vazba
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
Li	li	k8xS	li
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
nebo	nebo	k8xC	nebo
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
S.	S.	kA	S.
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
sirovodík	sirovodík	k1gInSc1	sirovodík
(	(	kIx(	(
<g/>
sulfan	sulfan	k1gInSc1	sulfan
<g/>
)	)	kIx)	)
H2S	H2S	k1gFnSc1	H2S
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
(	(	kIx(	(
<g/>
smrtelně	smrtelně	k6eAd1	smrtelně
toxická	toxický	k2eAgFnSc1d1	toxická
koncentrace	koncentrace	k1gFnSc1	koncentrace
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
0,15	[number]	k4	0,15
%	%	kIx~	%
<g/>
)	)	kIx)	)
plyn	plyn	k1gInSc1	plyn
silně	silně	k6eAd1	silně
zapáchající	zapáchající	k2eAgMnSc1d1	zapáchající
po	po	k7c6	po
zkažených	zkažený	k2eAgNnPc6d1	zkažené
vejcích	vejce	k1gNnPc6	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
soli	sůl	k1gFnPc1	sůl
odvozené	odvozený	k2eAgFnPc1d1	odvozená
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
kyseliny	kyselina	k1gFnSc2	kyselina
jsou	být	k5eAaImIp3nP	být
sulfidy	sulfid	k1gInPc4	sulfid
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
formální	formální	k2eAgInSc1d1	formální
náboj	náboj	k1gInSc1	náboj
<g/>
)	)	kIx)	)
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
mají	mít	k5eAaImIp3nP	mít
soli	sůl	k1gFnPc4	sůl
odvozené	odvozený	k2eAgFnPc4d1	odvozená
od	od	k7c2	od
kyseliny	kyselina	k1gFnSc2	kyselina
thiosírové	thiosírový	k2eAgFnSc2d1	thiosírový
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
thiosíran	thiosíran	k1gInSc1	thiosíran
sodný	sodný	k2eAgInSc1d1	sodný
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
složka	složka	k1gFnSc1	složka
fotografického	fotografický	k2eAgInSc2d1	fotografický
ustalovače	ustalovač	k1gInSc2	ustalovač
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
thiosírany	thiosíran	k1gInPc1	thiosíran
-	-	kIx~	-
thiosulfáty	thiosulfát	k1gInPc1	thiosulfát
dříve	dříve	k6eAd2	dříve
nazývány	nazýván	k2eAgInPc1d1	nazýván
sirnatany	sirnatan	k1gInPc1	sirnatan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
siřičitá	siřičitý	k2eAgFnSc1d1	siřičitá
H2SO3	H2SO3	k1gFnSc7	H2SO3
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
soli	sůl	k1gFnSc2	sůl
siřičitany	siřičitan	k1gInPc1	siřičitan
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
sulfity	sulfit	k1gInPc1	sulfit
<g/>
)	)	kIx)	)
S	s	k7c7	s
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
oxid	oxid	k1gInSc4	oxid
sírový	sírový	k2eAgInSc4d1	sírový
SO	So	kA	So
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
H2SO4	H2SO4	k1gFnSc7	H2SO4
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
soli	sůl	k1gFnSc2	sůl
sírany	síran	k1gInPc1	síran
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
sulfáty	sulfát	k1gInPc1	sulfát
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
thioly	thiol	k1gInPc1	thiol
(	(	kIx(	(
<g/>
merkaptany	merkaptan	k1gInPc1	merkaptan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-SH	-SH	k?	-SH
thioethery	thioethera	k1gFnSc2	thioethera
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-C	-C	k?	-C
<g/>
-	-	kIx~	-
<g/>
S	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
C-	C-	k1gMnSc1	C-
thioketony	thioketon	k1gInPc4	thioketon
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
S	s	k7c7	s
disulfidy	disulfid	k1gInPc7	disulfid
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-C-S-S-C-	-C-S-S-C-	k?	-C-S-S-C-
sulfoxidy	sulfoxida	k1gFnPc1	sulfoxida
<g/>
,	,	kIx,	,
sulfony	sulfon	k1gInPc1	sulfon
<g/>
,	,	kIx,	,
sulfonamidy	sulfonamid	k1gInPc1	sulfonamid
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
oxidované	oxidovaný	k2eAgFnPc1d1	oxidovaná
formy	forma	k1gFnPc1	forma
sulfonové	sulfonový	k2eAgFnSc2d1	sulfonová
kyseliny	kyselina	k1gFnSc2	kyselina
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
-SO3H	-SO3H	k4	-SO3H
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
bílkovinách	bílkovina	k1gFnPc6	bílkovina
-	-	kIx~	-
methionin	methionin	k1gInSc4	methionin
<g/>
,	,	kIx,	,
cystein	cystein	k1gInSc4	cystein
a	a	k8xC	a
cystin	cystin	k1gInSc4	cystin
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dvě	dva	k4xCgFnPc1	dva
molekuly	molekula	k1gFnPc1	molekula
cysteinu	cystein	k1gInSc2	cystein
spojené	spojený	k2eAgNnSc1d1	spojené
disulfidickým	disulfidický	k2eAgInSc7d1	disulfidický
můstkem	můstek	k1gInSc7	můstek
<g/>
)	)	kIx)	)
heterocyklické	heterocyklický	k2eAgFnPc4d1	heterocyklická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
jako	jako	k8xC	jako
např.	např.	kA	např.
thiofen	thiofen	k1gInSc1	thiofen
<g/>
,	,	kIx,	,
thiazol	thiazol	k1gInSc1	thiazol
apod.	apod.	kA	apod.
Síra	síra	k1gFnSc1	síra
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
0,03	[number]	k4	0,03
<g/>
-	-	kIx~	-
<g/>
0,09	[number]	k4	0,09
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
900	[number]	k4	900
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
síry	síra	k1gFnSc2	síra
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
60	[number]	k4	60
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
čistý	čistý	k2eAgInSc1d1	čistý
prvek	prvek	k1gInSc1	prvek
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
horkých	horký	k2eAgInPc2d1	horký
minerálních	minerální	k2eAgInPc2d1	minerální
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
oblasti	oblast	k1gFnPc1	oblast
těžby	těžba	k1gFnSc2	těžba
síry	síra	k1gFnSc2	síra
jsou	být	k5eAaImIp3nP	být
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Povolží	Povolží	k1gNnSc1	Povolží
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podrobněji	podrobně	k6eAd2	podrobně
viz	vidět	k5eAaImRp2nS	vidět
síra	síra	k1gFnSc1	síra
(	(	kIx(	(
<g/>
minerál	minerál	k1gInSc1	minerál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
rudách	ruda	k1gFnPc6	ruda
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
sulfidů	sulfid	k1gInPc2	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
-	-	kIx~	-
sfalerit	sfalerit	k1gInSc1	sfalerit
<g/>
,	,	kIx,	,
disulfid	disulfid	k1gInSc1	disulfid
železnatý	železnatý	k2eAgInSc1d1	železnatý
-	-	kIx~	-
pyrit	pyrit	k1gInSc1	pyrit
<g/>
,	,	kIx,	,
sulfid	sulfid	k1gInSc1	sulfid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
-	-	kIx~	-
galenit	galenit	k1gInSc1	galenit
<g/>
,	,	kIx,	,
sulfid	sulfid	k1gInSc1	sulfid
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
-	-	kIx~	-
cinabarit	cinabarit	k1gInSc1	cinabarit
(	(	kIx(	(
<g/>
rumělka	rumělka	k1gFnSc1	rumělka
<g/>
)	)	kIx)	)
a	a	k8xC	a
chalkopyrit	chalkopyrit	k1gInSc1	chalkopyrit
-	-	kIx~	-
směsný	směsný	k2eAgInSc1d1	směsný
sulfid	sulfid	k1gInSc1	sulfid
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
minerálem	minerál	k1gInSc7	minerál
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
síranů	síran	k1gInPc2	síran
je	být	k5eAaImIp3nS	být
sádrovec	sádrovec	k1gInSc1	sádrovec
-	-	kIx~	-
dihydrát	dihydrát	k1gInSc1	dihydrát
síranu	síran	k1gInSc2	síran
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
se	se	k3xPyFc4	se
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgNnSc6d1	značné
množství	množství	k1gNnSc6	množství
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
organického	organický	k2eAgInSc2d1	organický
původu	původ	k1gInSc2	původ
-	-	kIx~	-
v	v	k7c6	v
uhlí	uhlí	k1gNnSc6	uhlí
a	a	k8xC	a
ropě	ropa	k1gFnSc6	ropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
síra	síra	k1gFnSc1	síra
přítomna	přítomen	k2eAgFnSc1d1	přítomna
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
svých	svůj	k3xOyFgInPc2	svůj
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
především	především	k9	především
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sírového	sírový	k2eAgNnSc2d1	sírové
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
nekontrolované	kontrolovaný	k2eNgNnSc1d1	nekontrolované
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
<g/>
:	:	kIx,	:
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopek	sopka	k1gFnPc2	sopka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
emisi	emise	k1gFnSc3	emise
značných	značný	k2eAgInPc2d1	značný
množství	množství	k1gNnSc4	množství
sloučenin	sloučenina	k1gFnPc2	sloučenina
síry	síra	k1gFnPc4	síra
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
je	být	k5eAaImIp3nS	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
složkou	složka	k1gFnSc7	složka
organických	organický	k2eAgInPc2d1	organický
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
bílkovinách	bílkovina	k1gFnPc6	bílkovina
jako	jako	k8xS	jako
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
cystein	cystein	k1gInSc1	cystein
či	či	k8xC	či
metionin	metionina	k1gFnPc2	metionina
<g/>
,	,	kIx,	,
přítomných	přítomný	k2eAgFnPc2d1	přítomná
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
živých	živý	k2eAgInPc6d1	živý
organizmech	organizmus	k1gInPc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
proteinech	protein	k1gInPc6	protein
Fe-S	Fe-S	k1gFnSc2	Fe-S
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
koenzymu	koenzym	k1gInSc2	koenzym
A	A	kA	A
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
glutathionu	glutathion	k1gInSc6	glutathion
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
inaktivovat	inaktivovat	k5eAaBmF	inaktivovat
různé	různý	k2eAgInPc4d1	různý
toxiny	toxin	k1gInPc4	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Glutathion	Glutathion	k1gInSc1	Glutathion
je	být	k5eAaImIp3nS	být
složkou	složka	k1gFnSc7	složka
fytochelatinů	fytochelatin	k1gInPc2	fytochelatin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vyvazovat	vyvazovat	k5eAaImF	vyvazovat
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
glutathion	glutathion	k1gInSc1	glutathion
nevzniká	vznikat	k5eNaImIp3nS	vznikat
běžným	běžný	k2eAgInSc7d1	běžný
procesem	proces	k1gInSc7	proces
proteosyntézy	proteosyntéza	k1gFnSc2	proteosyntéza
na	na	k7c6	na
ribosomech	ribosom	k1gInPc6	ribosom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
činností	činnost	k1gFnSc7	činnost
speciálních	speciální	k2eAgInPc2d1	speciální
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
aktivovaných	aktivovaný	k2eAgFnPc2d1	aktivovaná
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
využívají	využívat	k5eAaImIp3nP	využívat
sloučeniny	sloučenina	k1gFnPc1	sloučenina
síry	síra	k1gFnSc2	síra
namísto	namísto	k7c2	namísto
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
přijímají	přijímat	k5eAaImIp3nP	přijímat
síru	síra	k1gFnSc4	síra
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
jako	jako	k8xS	jako
síranový	síranový	k2eAgInSc1d1	síranový
anion	anion	k1gInSc1	anion
SO	So	kA	So
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
symportem	symport	k1gInSc7	symport
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
protony	proton	k1gInPc7	proton
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
protony	proton	k1gInPc1	proton
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
poté	poté	k6eAd1	poté
zase	zase	k9	zase
vyčerpány	vyčerpán	k2eAgInPc1d1	vyčerpán
ATPásovými	ATPásův	k2eAgFnPc7d1	ATPásův
pumpami	pumpa	k1gFnPc7	pumpa
ven	ven	k6eAd1	ven
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
za	za	k7c4	za
investice	investice	k1gFnPc4	investice
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
jejich	jejich	k3xOp3gFnSc1	jejich
optimální	optimální	k2eAgFnSc1d1	optimální
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
síry	síra	k1gFnSc2	síra
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rostlinu	rostlina	k1gFnSc4	rostlina
energeticky	energeticky	k6eAd1	energeticky
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
rostlině	rostlina	k1gFnSc6	rostlina
transportována	transportován	k2eAgFnSc1d1	transportována
buď	buď	k8xC	buď
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
SO	So	kA	So
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
redukovaný	redukovaný	k2eAgInSc1d1	redukovaný
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
redukci	redukce	k1gFnSc4	redukce
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
ATP	atp	kA	atp
a	a	k8xC	a
redukovaný	redukovaný	k2eAgInSc1d1	redukovaný
feredoxin	feredoxin	k1gInSc1	feredoxin
<g/>
)	)	kIx)	)
sulfid	sulfid	k1gInSc1	sulfid
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vázaná	vázané	k1gNnPc1	vázané
v	v	k7c6	v
aminokyselinách	aminokyselina	k1gFnPc6	aminokyselina
či	či	k8xC	či
sulfolipidech	sulfolipid	k1gInPc6	sulfolipid
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
v	v	k7c6	v
dávnověku	dávnověk	k1gInSc6	dávnověk
a	a	k8xC	a
např.	např.	kA	např.
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
výbušnin	výbušnina	k1gFnPc2	výbušnina
a	a	k8xC	a
zábavní	zábavní	k2eAgFnSc2d1	zábavní
pyrotechniky	pyrotechnika	k1gFnSc2	pyrotechnika
se	se	k3xPyFc4	se
síra	síra	k1gFnSc1	síra
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
po	po	k7c6	po
vynálezu	vynález	k1gInSc6	vynález
dynamitu	dynamit	k1gInSc2	dynamit
význam	význam	k1gInSc4	význam
těchto	tento	k3xDgFnPc2	tento
směsí	směs	k1gFnPc2	směs
značně	značně	k6eAd1	značně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
elementární	elementární	k2eAgFnSc1d1	elementární
síra	síra	k1gFnSc1	síra
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
pro	pro	k7c4	pro
vulkanizaci	vulkanizace	k1gFnSc4	vulkanizace
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
síry	síra	k1gFnSc2	síra
přidané	přidaný	k2eAgFnSc2d1	přidaná
do	do	k7c2	do
směsi	směs	k1gFnSc2	směs
pak	pak	k6eAd1	pak
určuje	určovat	k5eAaImIp3nS	určovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
získaného	získaný	k2eAgInSc2d1	získaný
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
elementární	elementární	k2eAgFnSc1d1	elementární
síra	síra	k1gFnSc1	síra
základní	základní	k2eAgFnSc4d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
složkou	složka	k1gFnSc7	složka
různých	různý	k2eAgInPc2d1	různý
fungicidů	fungicid	k1gInPc2	fungicid
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prostředků	prostředek	k1gInPc2	prostředek
působících	působící	k2eAgFnPc2d1	působící
proti	proti	k7c3	proti
růstu	růst	k1gInSc3	růst
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
plísní	plíseň	k1gFnPc2	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Síření	síření	k1gNnSc1	síření
sklepů	sklep	k1gInPc2	sklep
i	i	k8xC	i
sudů	sud	k1gInPc2	sud
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
vína	víno	k1gNnSc2	víno
či	či	k8xC	či
piva	pivo	k1gNnSc2	pivo
efektivně	efektivně	k6eAd1	efektivně
brání	bránit	k5eAaImIp3nS	bránit
množení	množení	k1gNnSc4	množení
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
plísní	plíseň	k1gFnPc2	plíseň
a	a	k8xC	a
mikroorganizmů	mikroorganizmus	k1gInPc2	mikroorganizmus
<g/>
.	.	kIx.	.
</s>
<s>
Síra	síra	k1gFnSc1	síra
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
nezbytných	nezbytný	k2eAgFnPc2d1	nezbytná
pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
esenciální	esenciální	k2eAgFnPc1d1	esenciální
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jako	jako	k8xS	jako
cystein	cystein	k1gInSc1	cystein
a	a	k8xC	a
methionin	methionin	k1gInSc1	methionin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
přítomných	přítomný	k2eAgFnPc2d1	přítomná
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
živých	živý	k2eAgInPc6d1	živý
organizmech	organizmus	k1gInPc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
kvality	kvalita	k1gFnSc2	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
masivního	masivní	k2eAgNnSc2d1	masivní
spalování	spalování	k1gNnSc2	spalování
uhlí	uhlí	k1gNnSc2	uhlí
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
obsaženou	obsažený	k2eAgFnSc7d1	obsažená
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vznikají	vznikat	k5eAaImIp3nP	vznikat
kyseliny	kyselina	k1gFnPc1	kyselina
siřičitá	siřičitý	k2eAgFnSc1d1	siřičitá
a	a	k8xC	a
sírová	sírový	k2eAgFnSc1d1	sírová
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
kyselých	kyselý	k2eAgInPc2d1	kyselý
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
zničení	zničení	k1gNnSc6	zničení
smrkových	smrkový	k2eAgInPc2d1	smrkový
lesů	les	k1gInPc2	les
např.	např.	kA	např.
Jizerských	jizerský	k2eAgInPc2d1	jizerský
a	a	k8xC	a
Krušných	krušný	k2eAgInPc2d1	krušný
hor.	hor.	k?	hor.
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1990	[number]	k4	1990
až	až	k9	až
2006	[number]	k4	2006
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
emisí	emise	k1gFnPc2	emise
SO2	SO2	k1gMnPc2	SO2
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
90	[number]	k4	90
%	%	kIx~	%
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
instalaci	instalace	k1gFnSc4	instalace
účinných	účinný	k2eAgNnPc2d1	účinné
odsiřovacích	odsiřovací	k2eAgNnPc2d1	odsiřovací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
alkalických	alkalický	k2eAgInPc2d1	alkalický
sorbentů	sorbent	k1gInPc2	sorbent
(	(	kIx(	(
<g/>
mletý	mletý	k2eAgInSc1d1	mletý
vápenec	vápenec	k1gInSc1	vápenec
nebo	nebo	k8xC	nebo
magnezit	magnezit	k1gInSc1	magnezit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stoupají	stoupat	k5eAaImIp3nP	stoupat
emise	emise	k1gFnPc1	emise
SO2	SO2	k1gFnPc2	SO2
z	z	k7c2	z
malých	malý	k2eAgInPc2d1	malý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
představují	představovat	k5eAaImIp3nP	představovat
emise	emise	k1gFnPc1	emise
oxidů	oxid	k1gInPc2	oxid
síry	síra	k1gFnSc2	síra
problém	problém	k1gInSc4	problém
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
jako	jako	k9	jako
Čína	Čína	k1gFnSc1	Čína
nebo	nebo	k8xC	nebo
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
organizmu	organizmus	k1gInSc2	organizmus
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
bílkoviny	bílkovina	k1gFnPc4	bílkovina
(	(	kIx(	(
<g/>
sýry	sýr	k1gInPc4	sýr
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Síra	síra	k1gFnSc1	síra
je	být	k5eAaImIp3nS	být
složkou	složka	k1gFnSc7	složka
dvou	dva	k4xCgFnPc6	dva
esenciálních	esenciální	k2eAgFnPc2d1	esenciální
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
cysteinu	cystein	k1gInSc2	cystein
a	a	k8xC	a
methioninu	methionin	k1gInSc2	methionin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
buňkách	buňka	k1gFnPc6	buňka
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
ji	on	k3xPp3gFnSc4	on
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
nehtech	nehet	k1gInPc6	nehet
a	a	k8xC	a
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
nehrozí	hrozit	k5eNaImIp3nS	hrozit
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
dávka	dávka	k1gFnSc1	dávka
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
g.	g.	k?	g.
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
síra	síra	k1gFnSc1	síra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
síra	síra	k1gFnSc1	síra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
Crystalline	Crystallin	k1gInSc5	Crystallin
<g/>
,	,	kIx,	,
liquid	liquid	k1gInSc1	liquid
and	and	k?	and
polymerization	polymerization	k1gInSc1	polymerization
of	of	k?	of
sulphur	sulphura	k1gFnPc2	sulphura
on	on	k3xPp3gMnSc1	on
Vulcano	Vulcana	k1gFnSc5	Vulcana
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc4	Ital
Sulfur	Sulfura	k1gFnPc2	Sulfura
and	and	k?	and
its	its	k?	its
use	usus	k1gInSc5	usus
as	as	k9	as
a	a	k8xC	a
pesticide	pesticid	k1gInSc5	pesticid
</s>
