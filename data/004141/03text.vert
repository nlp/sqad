<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
akciová	akciový	k2eAgFnSc1d1	akciová
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Ruston	Ruston	k1gInSc1	Ruston
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
všeobecně	všeobecně	k6eAd1	všeobecně
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
lidovým	lidový	k2eAgInSc7d1	lidový
názvem	název	k1gInSc7	název
Rustonka	Rustonka	k1gFnSc1	Rustonka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
pražský	pražský	k2eAgInSc1d1	pražský
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stával	stávat	k5eAaImAgInS	stávat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Karlína	Karlín	k1gInSc2	Karlín
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Libní	Libeň	k1gFnSc7	Libeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
zvaném	zvaný	k2eAgInSc6d1	zvaný
Švábky	švábka	k1gFnPc1	švábka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
libeňskou	libeňský	k2eAgFnSc7d1	Libeňská
Palmovkou	Palmovka	k1gFnSc7	Palmovka
a	a	k8xC	a
karlínskou	karlínský	k2eAgFnSc7d1	Karlínská
Invalidovnou	invalidovna	k1gFnSc7	invalidovna
<g/>
,	,	kIx,	,
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
–	–	k?	–
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
č.	č.	k?	č.
p.	p.	k?	p.
268	[number]	k4	268
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
strojírenských	strojírenský	k2eAgInPc2d1	strojírenský
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
budova	budova	k1gFnSc1	budova
zanikajícího	zanikající	k2eAgInSc2d1	zanikající
areálu	areál	k1gInSc2	areál
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgFnSc1d1	někdejší
kotelna	kotelna	k1gFnSc1	kotelna
s	s	k7c7	s
osmibokým	osmiboký	k2eAgInSc7d1	osmiboký
cihlovým	cihlový	k2eAgInSc7d1	cihlový
komínem	komín	k1gInSc7	komín
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zdemolována	zdemolovat	k5eAaPmNgFnS	zdemolovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
prostorem	prostor	k1gInSc7	prostor
bývalé	bývalý	k2eAgFnSc2d1	bývalá
továrny	továrna	k1gFnSc2	továrna
dnes	dnes	k6eAd1	dnes
prochází	procházet	k5eAaImIp3nS	procházet
trasa	trasa	k1gFnSc1	trasa
B	B	kA	B
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
mezi	mezi	k7c7	mezi
stanicí	stanice	k1gFnSc7	stanice
Palmovka	Palmovka	k1gFnSc1	Palmovka
a	a	k8xC	a
stanicí	stanice	k1gFnSc7	stanice
Invalidovna	invalidovna	k1gFnSc1	invalidovna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kdysi	kdysi	k6eAd1	kdysi
bývala	bývat	k5eAaImAgFnS	bývat
přádelna	přádelna	k1gFnSc1	přádelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
zde	zde	k6eAd1	zde
přičiněním	přičinění	k1gNnSc7	přičinění
britských	britský	k2eAgMnPc2d1	britský
podnikatelů	podnikatel	k1gMnPc2	podnikatel
bratrů	bratr	k1gMnPc2	bratr
Thomasových	Thomasův	k2eAgFnPc2d1	Thomasova
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
parní	parní	k2eAgInPc4d1	parní
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
významného	významný	k2eAgMnSc2d1	významný
britského	britský	k2eAgMnSc2d1	britský
podnikatele	podnikatel	k1gMnSc2	podnikatel
Josepha	Joseph	k1gMnSc2	Joseph
Rustona	Ruston	k1gMnSc2	Ruston
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
příjmení	příjmení	k1gNnSc1	příjmení
pak	pak	k6eAd1	pak
dalo	dát	k5eAaPmAgNnS	dát
firmě	firma	k1gFnSc3	firma
její	její	k3xOp3gInSc1	její
trvalý	trvalý	k2eAgInSc1d1	trvalý
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
-	-	kIx~	-
komín	komín	k1gInSc1	komín
s	s	k7c7	s
ústřední	ústřední	k2eAgFnSc7d1	ústřední
kotelnou	kotelna	k1gFnSc7	kotelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
-	-	kIx~	-
1902	[number]	k4	1902
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
továrny	továrna	k1gFnSc2	továrna
postupně	postupně	k6eAd1	postupně
rozšiřován	rozšiřovat	k5eAaImNgInS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Stavitel	stavitel	k1gMnSc1	stavitel
František	František	k1gMnSc1	František
Schlaffer	Schlaffer	k1gMnSc1	Schlaffer
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
-	-	kIx~	-
1902	[number]	k4	1902
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
přístavbu	přístavba	k1gFnSc4	přístavba
strojovny	strojovna	k1gFnSc2	strojovna
a	a	k8xC	a
výstavbu	výstavba	k1gFnSc4	výstavba
největší	veliký	k2eAgFnSc2d3	veliký
budovy	budova	k1gFnSc2	budova
-	-	kIx~	-
mostárny	mostárna	k1gFnSc2	mostárna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulic	ulice	k1gFnPc2	ulice
Sokolovské	sokolovský	k2eAgFnSc2d1	Sokolovská
a	a	k8xC	a
Švábky	švábka	k1gFnSc2	švábka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
železné	železný	k2eAgFnPc1d1	železná
střešní	střešní	k2eAgFnPc1d1	střešní
konstrukce	konstrukce	k1gFnPc1	konstrukce
truhlárny	truhlárna	k1gFnSc2	truhlárna
a	a	k8xC	a
hlavních	hlavní	k2eAgFnPc2d1	hlavní
dílen	dílna	k1gFnPc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
zpočátku	zpočátku	k6eAd1	zpočátku
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
zejména	zejména	k9	zejména
kotle	kotel	k1gInPc1	kotel
a	a	k8xC	a
parní	parní	k2eAgInPc1d1	parní
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Ruston	Ruston	k1gInSc4	Ruston
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
lodní	lodní	k2eAgMnSc1d1	lodní
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c6	v
říční	říční	k2eAgFnSc6d1	říční
plavbě	plavba	k1gFnSc6	plavba
<g/>
,	,	kIx,	,
provozoval	provozovat	k5eAaImAgMnS	provozovat
Pražskou	pražský	k2eAgFnSc4d1	Pražská
paroplavební	paroplavební	k2eAgFnSc4d1	paroplavební
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
firma	firma	k1gFnSc1	firma
tehdy	tehdy	k6eAd1	tehdy
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zejména	zejména	k9	zejména
různá	různý	k2eAgNnPc4d1	různé
strojní	strojní	k2eAgNnPc4d1	strojní
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
díly	díl	k1gInPc4	díl
pro	pro	k7c4	pro
říční	říční	k2eAgFnPc4d1	říční
i	i	k8xC	i
zaoceánské	zaoceánský	k2eAgFnPc4d1	zaoceánská
lodě	loď	k1gFnPc4	loď
určená	určený	k2eAgNnPc1d1	určené
pro	pro	k7c4	pro
různé	různý	k2eAgMnPc4d1	různý
provozovatele	provozovatel	k1gMnPc4	provozovatel
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
i	i	k8xC	i
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Stavěla	stavět	k5eAaImAgFnS	stavět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jak	jak	k6eAd1	jak
říční	říční	k2eAgNnPc1d1	říční
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
a	a	k8xC	a
i	i	k9	i
námořní	námořní	k2eAgNnPc1d1	námořní
plavidla	plavidlo	k1gNnPc1	plavidlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
parníky	parník	k1gInPc1	parník
<g/>
,	,	kIx,	,
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
parní	parní	k2eAgInPc4d1	parní
stroje	stroj	k1gInPc4	stroj
určené	určený	k2eAgInPc4d1	určený
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
paroplavbu	paroplavba	k1gFnSc4	paroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
nýtované	nýtovaný	k2eAgFnPc4d1	nýtovaná
ocelové	ocelový	k2eAgFnPc4d1	ocelová
konstrukce	konstrukce	k1gFnPc4	konstrukce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
ocelové	ocelový	k2eAgInPc1d1	ocelový
mosty	most	k1gInPc1	most
a	a	k8xC	a
ocelové	ocelový	k2eAgFnSc2d1	ocelová
střešní	střešní	k2eAgFnSc2d1	střešní
příhradové	příhradový	k2eAgFnSc2d1	příhradová
konstrukce	konstrukce	k1gFnSc2	konstrukce
–	–	k?	–
pochází	pocházet	k5eAaImIp3nS	pocházet
odtud	odtud	k6eAd1	odtud
například	například	k6eAd1	například
střešní	střešní	k2eAgFnSc2d1	střešní
konstrukce	konstrukce	k1gFnSc2	konstrukce
pražského	pražský	k2eAgNnSc2d1	Pražské
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc4	jeden
pole	pole	k1gNnSc4	pole
Vyšehradského	vyšehradský	k2eAgInSc2d1	vyšehradský
železničního	železniční	k2eAgInSc2d1	železniční
mostu	most	k1gInSc2	most
či	či	k8xC	či
některé	některý	k3yIgInPc1	některý
objekty	objekt	k1gInPc1	objekt
na	na	k7c6	na
Pražském	pražský	k2eAgNnSc6d1	Pražské
výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
závodu	závod	k1gInSc2	závod
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
nadále	nadále	k6eAd1	nadále
rozšiřován	rozšiřovat	k5eAaImNgInS	rozšiřovat
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
výrobní	výrobní	k2eAgFnPc4d1	výrobní
haly	hala	k1gFnPc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podnikem	podnik	k1gInSc7	podnik
také	také	k9	také
úzce	úzko	k6eAd1	úzko
souvisel	souviset	k5eAaImAgInS	souviset
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
Karlínský	karlínský	k2eAgInSc1d1	karlínský
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
provozován	provozovat	k5eAaImNgInS	provozovat
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
ze	z	k7c2	z
slepých	slepý	k2eAgNnPc2d1	slepé
ramen	rameno	k1gNnPc2	rameno
blízké	blízký	k2eAgFnSc2d1	blízká
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
továrny	továrna	k1gFnSc2	továrna
také	také	k9	také
vedla	vést	k5eAaImAgFnS	vést
železniční	železniční	k2eAgFnSc1d1	železniční
vlečka	vlečka	k1gFnSc1	vlečka
z	z	k7c2	z
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Praha-Libeň	Praha-Libeň	k1gFnSc1	Praha-Libeň
dolní	dolní	k2eAgNnSc4d1	dolní
nádraží	nádraží	k1gNnSc4	nádraží
na	na	k7c4	na
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zrušeném	zrušený	k2eAgInSc6d1	zrušený
úseku	úsek	k1gInSc6	úsek
trati	trať	k1gFnSc2	trať
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
,	,	kIx,	,
oddělen	oddělen	k2eAgInSc1d1	oddělen
pouze	pouze	k6eAd1	pouze
ulicí	ulice	k1gFnSc7	ulice
Švábky	švábka	k1gFnSc2	švábka
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
areál	areál	k1gInSc1	areál
vozovny	vozovna	k1gFnSc2	vozovna
Křižíkovy	Křižíkův	k2eAgFnSc2d1	Křižíkova
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sloužil	sloužit	k5eAaImAgMnS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
jako	jako	k8xS	jako
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
vozovna	vozovna	k1gFnSc1	vozovna
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
propojen	propojit	k5eAaPmNgInS	propojit
spojovacími	spojovací	k2eAgFnPc7d1	spojovací
tratěmi	trať	k1gFnPc7	trať
jak	jak	k8xC	jak
s	s	k7c7	s
trolejbusovou	trolejbusový	k2eAgFnSc7d1	trolejbusová
tratí	trať	k1gFnSc7	trať
U	u	k7c2	u
kříže	kříž	k1gInSc2	kříž
<g/>
–	–	k?	–
<g/>
Čakovice	Čakovice	k1gFnSc2	Čakovice
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
tratí	tratit	k5eAaImIp3nP	tratit
Vysočany	Vysočany	k1gInPc4	Vysočany
<g/>
–	–	k?	–
<g/>
Ohrada	ohrada	k1gFnSc1	ohrada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
tratě	trať	k1gFnSc2	trať
do	do	k7c2	do
Čakovic	Čakovice	k1gFnPc2	Čakovice
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
i	i	k9	i
spojovací	spojovací	k2eAgFnSc1d1	spojovací
trať	trať	k1gFnSc1	trať
a	a	k8xC	a
areál	areál	k1gInSc1	areál
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xC	jako
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
pak	pak	k6eAd1	pak
celý	celý	k2eAgInSc1d1	celý
podnik	podnik	k1gInSc1	podnik
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Elektrických	elektrický	k2eAgInPc2d1	elektrický
podniků	podnik	k1gInPc2	podnik
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
účely	účel	k1gInPc4	účel
zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgInPc4d1	nový
provozy	provoz	k1gInPc4	provoz
–	–	k?	–
lakovna	lakovna	k1gFnSc1	lakovna
<g/>
,	,	kIx,	,
truhlárna	truhlárna	k1gFnSc1	truhlárna
<g/>
,	,	kIx,	,
gumárna	gumárna	k1gFnSc1	gumárna
<g/>
,	,	kIx,	,
krejčovské	krejčovský	k2eAgFnPc1d1	krejčovská
dílny	dílna	k1gFnPc1	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
hlavní	hlavní	k2eAgInSc1d1	hlavní
opravárenský	opravárenský	k2eAgInSc1d1	opravárenský
a	a	k8xC	a
údržbářský	údržbářský	k2eAgInSc1d1	údržbářský
podnik	podnik	k1gInSc1	podnik
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
opravy	oprava	k1gFnPc4	oprava
všech	všecek	k3xTgNnPc2	všecek
vozidel	vozidlo	k1gNnPc2	vozidlo
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
k	k	k7c3	k
napojení	napojení	k1gNnSc3	napojení
areálu	areál	k1gInSc2	areál
i	i	k9	i
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
síť	síť	k1gFnSc4	síť
ze	z	k7c2	z
Sokolovské	sokolovský	k2eAgFnSc2d1	Sokolovská
ulice	ulice	k1gFnSc2	ulice
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
Invalidovnou	invalidovna	k1gFnSc7	invalidovna
a	a	k8xC	a
Palmovkou	Palmovka	k1gFnSc7	Palmovka
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
kolejová	kolejový	k2eAgFnSc1d1	kolejová
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
propojka	propojka	k1gFnSc1	propojka
sloužila	sloužit	k5eAaImAgFnS	sloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1984	[number]	k4	1984
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
jak	jak	k6eAd1	jak
sousedící	sousedící	k2eAgFnSc1d1	sousedící
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
napojená	napojený	k2eAgFnSc1d1	napojená
železniční	železniční	k2eAgFnSc1d1	železniční
vlečka	vlečka	k1gFnSc1	vlečka
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
kompletně	kompletně	k6eAd1	kompletně
opravovaly	opravovat	k5eAaImAgFnP	opravovat
především	především	k9	především
tramvaje	tramvaj	k1gFnPc1	tramvaj
a	a	k8xC	a
tramvajové	tramvajový	k2eAgInPc1d1	tramvajový
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přibyly	přibýt	k5eAaPmAgInP	přibýt
i	i	k9	i
autobusy	autobus	k1gInPc1	autobus
a	a	k8xC	a
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
,	,	kIx,	,
úplně	úplně	k6eAd1	úplně
naposled	naposled	k6eAd1	naposled
pak	pak	k6eAd1	pak
konstrukce	konstrukce	k1gFnSc1	konstrukce
tramvajových	tramvajový	k2eAgNnPc2d1	tramvajové
kolejišť	kolejiště	k1gNnPc2	kolejiště
–	–	k?	–
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
výměny	výměna	k1gFnSc2	výměna
<g/>
,	,	kIx,	,
kolejová	kolejový	k2eAgNnPc1d1	kolejové
křížení	křížení	k1gNnPc1	křížení
<g/>
,	,	kIx,	,
kolejové	kolejový	k2eAgInPc1d1	kolejový
splítky	splítek	k1gInPc1	splítek
<g/>
,	,	kIx,	,
kolejové	kolejový	k2eAgInPc1d1	kolejový
oblouky	oblouk	k1gInPc1	oblouk
apod.	apod.	kA	apod.
Nacházely	nacházet	k5eAaImAgInP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
i	i	k8xC	i
krejčovské	krejčovský	k2eAgFnPc1d1	krejčovská
dílny	dílna	k1gFnPc1	dílna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
šily	šít	k5eAaImAgFnP	šít
služební	služební	k2eAgFnPc1d1	služební
uniformy	uniforma	k1gFnPc1	uniforma
pro	pro	k7c4	pro
pražské	pražský	k2eAgMnPc4d1	pražský
tramvajáky	tramvaják	k1gMnPc4	tramvaják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Elektrických	elektrický	k2eAgInPc2d1	elektrický
podniků	podnik	k1gInPc2	podnik
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
strojírna	strojírna	k1gFnSc1	strojírna
transformována	transformovat	k5eAaBmNgFnS	transformovat
na	na	k7c4	na
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pražská	pražský	k2eAgFnSc1d1	Pražská
strojírna	strojírna	k1gFnSc1	strojírna
a.s.	a.s.	k?	a.s.
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
v	v	k7c6	v
Libni	Libeň	k1gFnSc6	Libeň
kolejové	kolejový	k2eAgFnSc2d1	kolejová
konstrukce	konstrukce	k1gFnSc2	konstrukce
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
společnost	společnost	k1gFnSc1	společnost
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
zrekonstruovaného	zrekonstruovaný	k2eAgInSc2d1	zrekonstruovaný
areálu	areál	k1gInSc2	areál
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
9	[number]	k4	9
<g/>
-Vinoři	-Vinoř	k1gFnSc6	-Vinoř
a	a	k8xC	a
posílila	posílit	k5eAaPmAgFnS	posílit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
demolici	demolice	k1gFnSc3	demolice
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgInSc2d1	celý
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
nových	nový	k2eAgFnPc2d1	nová
kapacit	kapacita	k1gFnPc2	kapacita
a	a	k8xC	a
přesouvání	přesouvání	k1gNnSc4	přesouvání
výroby	výroba	k1gFnSc2	výroba
jinam	jinam	k6eAd1	jinam
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
utlumování	utlumování	k1gNnSc3	utlumování
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
definitivně	definitivně	k6eAd1	definitivně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
padlo	padnout	k5eAaPmAgNnS	padnout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
nové	nový	k2eAgFnSc2d1	nová
dopravní	dopravní	k2eAgFnSc2d1	dopravní
komunikace	komunikace	k1gFnSc2	komunikace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pobřežní	pobřežní	k1gMnPc1	pobřežní
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
procházet	procházet	k5eAaImF	procházet
právě	právě	k9	právě
těmito	tento	k3xDgNnPc7	tento
místy	místo	k1gNnPc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
závodu	závod	k1gInSc2	závod
kompletně	kompletně	k6eAd1	kompletně
demolován	demolován	k2eAgInSc1d1	demolován
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
kotelny	kotelna	k1gFnSc2	kotelna
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
osmibokým	osmiboký	k2eAgInSc7d1	osmiboký
komínem	komín	k1gInSc7	komín
pocházejícím	pocházející	k2eAgInSc7d1	pocházející
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poslední	poslední	k2eAgInSc1d1	poslední
cenný	cenný	k2eAgInSc1d1	cenný
zbytek	zbytek	k1gInSc1	zbytek
byl	být	k5eAaImAgInS	být
navzdory	navzdory	k7c3	navzdory
projektové	projektový	k2eAgFnSc3d1	projektová
studii	studie	k1gFnSc3	studie
a	a	k8xC	a
původní	původní	k2eAgFnSc3d1	původní
domluvě	domluva	k1gFnSc3	domluva
o	o	k7c6	o
památkové	památkový	k2eAgFnSc6d1	památková
ochraně	ochrana	k1gFnSc6	ochrana
zdemolován	zdemolovat	k5eAaPmNgInS	zdemolovat
developerskou	developerský	k2eAgFnSc7d1	developerská
společností	společnost	k1gFnSc7	společnost
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Real	Real	k1gInSc1	Real
Estate	Estat	k1gInSc5	Estat
koncem	konec	k1gInSc7	konec
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
továrny	továrna	k1gFnSc2	továrna
developer	developer	k1gMnSc1	developer
plánuje	plánovat	k5eAaImIp3nS	plánovat
výstavbu	výstavba	k1gFnSc4	výstavba
administrativního	administrativní	k2eAgNnSc2d1	administrativní
centra	centrum	k1gNnSc2	centrum
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rustonka	Rustonka	k1gFnSc1	Rustonka
<g/>
.	.	kIx.	.
</s>
<s>
BERAN	Beran	k1gMnSc1	Beran
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
a	a	k8xC	a
VALCHÁŘOVÁ	VALCHÁŘOVÁ	kA	VALCHÁŘOVÁ
<g/>
,	,	kIx,	,
Vladislava	Vladislava	k1gFnSc1	Vladislava
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
industriál	industriál	k1gInSc1	industriál
<g/>
:	:	kIx,	:
technické	technický	k2eAgFnSc2d1	technická
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
architektura	architektura	k1gFnSc1	architektura
Prahy	Praha	k1gFnSc2	Praha
<g/>
:	:	kIx,	:
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozš	rozš	k1gMnSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
303	[number]	k4	303
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3586	[number]	k4	3586
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
109	[number]	k4	109
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rustonka	Rustonka	k1gFnSc1	Rustonka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Jiří	Jiří	k1gMnSc1	Jiří
Novák	Novák	k1gMnSc1	Novák
<g/>
:	:	kIx,	:
Rustonka	Rustonka	k1gFnSc1	Rustonka
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
nebyla	být	k5eNaImAgNnP	být
jen	jen	k6eAd1	jen
továrnou	továrna	k1gFnSc7	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Věstník	věstník	k1gMnSc1	věstník
Klubu	klub	k1gInSc2	klub
Za	za	k7c4	za
starou	starý	k2eAgFnSc4d1	stará
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Z	z	k7c2	z
Rustonky	Rustonka	k1gFnSc2	Rustonka
nezbude	zbýt	k5eNaPmIp3nS	zbýt
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
demolice	demolice	k1gFnSc1	demolice
poslední	poslední	k2eAgFnSc2d1	poslední
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
</s>
