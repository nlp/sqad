<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
akciová	akciový	k2eAgFnSc1d1	akciová
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Ruston	Ruston	k1gInSc1	Ruston
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
všeobecně	všeobecně	k6eAd1	všeobecně
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
lidovým	lidový	k2eAgInSc7d1	lidový
názvem	název	k1gInSc7	název
Rustonka	Rustonka	k1gFnSc1	Rustonka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
pražský	pražský	k2eAgInSc1d1	pražský
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stával	stávat	k5eAaImAgInS	stávat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Karlína	Karlín	k1gInSc2	Karlín
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Libní	Libeň	k1gFnSc7	Libeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
zvaném	zvaný	k2eAgInSc6d1	zvaný
Švábky	švábka	k1gFnPc1	švábka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
libeňskou	libeňský	k2eAgFnSc7d1	Libeňská
Palmovkou	Palmovka	k1gFnSc7	Palmovka
a	a	k8xC	a
karlínskou	karlínský	k2eAgFnSc7d1	Karlínská
Invalidovnou	invalidovna	k1gFnSc7	invalidovna
<g/>
,	,	kIx,	,
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
–	–	k?	–
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
č.	č.	k?	č.
p.	p.	k?	p.
268	[number]	k4	268
<g/>
.	.	kIx.	.
</s>
