<s>
Třinec	Třinec	k1gInSc1	Třinec
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Trzyniec	Trzyniec	k1gInSc1	Trzyniec
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
Trzynietz	Trzynietz	k1gInSc1	Trzynietz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
32	[number]	k4	32
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
historického	historický	k2eAgNnSc2d1	historické
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
