<s>
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Brandeis	Brandeis	k1gInSc1	Brandeis
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Elbe	Elbus	k1gMnSc5	Elbus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalé	bývalý	k2eAgNnSc1d1	bývalé
samostatné	samostatný	k2eAgNnSc1d1	samostatné
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Praha-východ	Prahaýchod	k1gInSc1	Praha-východ
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
částí	část	k1gFnPc2	část
souměstí	souměstí	k1gNnSc2	souměstí
Brandýs	Brandýs	k1gInSc4	Brandýs
nad	nad	k7c4	nad
Labem-Stará	Labem-Starý	k2eAgNnPc4d1	Labem-Starý
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vyvýšené	vyvýšený	k2eAgFnSc6d1	vyvýšená
terase	terasa	k1gFnSc6	terasa
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vymodeloval	vymodelovat	k5eAaPmAgInS	vymodelovat
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
rokli	rokle	k1gFnSc4	rokle
se	s	k7c7	s
strmými	strmý	k2eAgInPc7d1	strmý
srázy	sráz	k1gInPc7	sráz
Vinořský	Vinořský	k2eAgInSc1d1	Vinořský
potok	potok	k1gInSc1	potok
vlévající	vlévající	k2eAgInSc1d1	vlévající
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
přechod	přechod	k1gInSc1	přechod
řeky	řeka	k1gFnSc2	řeka
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
již	již	k6eAd1	již
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
procházela	procházet	k5eAaImAgFnS	procházet
tudy	tudy	k6eAd1	tudy
významná	významný	k2eAgFnSc1d1	významná
zemská	zemský	k2eAgFnSc1d1	zemská
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
Brandýs	Brandýs	k1gInSc1	Brandýs
byla	být	k5eAaImAgFnS	být
jižně	jižně	k6eAd1	jižně
ležící	ležící	k2eAgFnSc1d1	ležící
ves	ves	k1gFnSc1	ves
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
nacházely	nacházet	k5eAaImAgInP	nacházet
dva	dva	k4xCgInPc1	dva
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
patřila	patřit	k5eAaImAgFnS	patřit
kolegiátní	kolegiátní	k2eAgFnSc6d1	kolegiátní
kapitule	kapitula	k1gFnSc6	kapitula
v	v	k7c6	v
Sadské	sadský	k2eAgFnSc6d1	Sadská
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
sv.	sv.	kA	sv.
Apolináře	Apolinář	k1gMnSc2	Apolinář
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
podací	podací	k2eAgNnSc4d1	podací
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
farnímu	farní	k2eAgInSc3d1	farní
kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
brandýských	brandýský	k2eAgMnPc2d1	brandýský
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
byl	být	k5eAaImAgInS	být
sloučen	sloučit	k5eAaPmNgInS	sloučit
s	s	k7c7	s
Brandýsem	Brandýs	k1gInSc7	Brandýs
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
městečko	městečko	k1gNnSc1	městečko
Brandýs	Brandýs	k1gInSc4	Brandýs
bylo	být	k5eAaImAgNnS	být
vysazeno	vysazen	k2eAgNnSc1d1	vysazeno
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pány	pan	k1gMnPc7	pan
z	z	k7c2	z
Michalovic	Michalovice	k1gFnPc2	Michalovice
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
drželi	držet	k5eAaImAgMnP	držet
jak	jak	k6eAd1	jak
část	část	k1gFnSc4	část
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
tak	tak	k9	tak
Boleslav	Boleslav	k1gMnSc1	Boleslav
Mladou	mladá	k1gFnSc4	mladá
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
trhová	trhový	k2eAgFnSc1d1	trhová
ves	ves	k1gFnSc1	ves
Brandýs	Brandýs	k1gInSc1	Brandýs
s	s	k7c7	s
mostem	most	k1gInSc7	most
a	a	k8xC	a
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ní	on	k3xPp3gFnSc3	on
zřídili	zřídit	k5eAaPmAgMnP	zřídit
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
majitelé	majitel	k1gMnPc1	majitel
panství	panství	k1gNnSc1	panství
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tato	tento	k3xDgFnSc1	tento
již	již	k6eAd1	již
nestála	stát	k5eNaImAgFnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
děkanství	děkanství	k1gNnSc1	děkanství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
ovládali	ovládat	k5eAaImAgMnP	ovládat
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
pražané	pražan	k1gMnPc1	pražan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1468	[number]	k4	1468
<g/>
-	-	kIx~	-
<g/>
1493	[number]	k4	1493
držel	držet	k5eAaImAgMnS	držet
panství	panství	k1gNnSc4	panství
Jan	Jan	k1gMnSc1	Jan
Tovačovský	Tovačovský	k1gMnSc1	Tovačovský
z	z	k7c2	z
Cimburka	Cimburek	k1gMnSc2	Cimburek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sem	sem	k6eAd1	sem
přemístil	přemístit	k5eAaPmAgInS	přemístit
správní	správní	k2eAgNnSc4d1	správní
centrum	centrum	k1gNnSc4	centrum
panství	panství	k1gNnSc2	panství
a	a	k8xC	a
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
také	také	k6eAd1	také
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
tvrz	tvrz	k1gFnSc4	tvrz
na	na	k7c4	na
reprezentativnější	reprezentativní	k2eAgNnSc4d2	reprezentativnější
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
Johanku	Johanka	k1gFnSc4	Johanka
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
provdanou	provdaný	k2eAgFnSc4d1	provdaná
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c4	za
Jana	Jan	k1gMnSc4	Jan
ze	z	k7c2	z
Šelmberka	Šelmberka	k1gFnSc1	Šelmberka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
městu	město	k1gNnSc3	město
některé	některý	k3yIgFnSc2	některý
výsady	výsada	k1gFnSc2	výsada
<g/>
,	,	kIx,	,
od	od	k7c2	od
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagelonského	jagelonský	k2eAgMnSc2d1	jagelonský
obdržela	obdržet	k5eAaPmAgFnS	obdržet
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
roku	rok	k1gInSc2	rok
1503	[number]	k4	1503
znak	znak	k1gInSc4	znak
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
přestavbě	přestavba	k1gFnSc6	přestavba
tvrze	tvrz	k1gFnSc2	tvrz
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
přešlo	přejít	k5eAaPmAgNnS	přejít
panství	panství	k1gNnSc4	panství
na	na	k7c4	na
původně	původně	k6eAd1	původně
rakouský	rakouský	k2eAgInSc4d1	rakouský
rod	rod	k1gInSc4	rod
Krajířů	Krajíř	k1gInPc2	Krajíř
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvelebování	zvelebování	k1gNnSc6	zvelebování
sídla	sídlo	k1gNnSc2	sídlo
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ona	onen	k3xDgFnSc1	onen
horlivý	horlivý	k2eAgMnSc1d1	horlivý
přívrženec	přívrženec	k1gMnSc1	přívrženec
jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
Konrád	Konrád	k1gMnSc1	Konrád
Krajíř	Krajíř	k1gFnSc1	Krajíř
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
i	i	k8xC	i
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
stává	stávat	k5eAaImIp3nS	stávat
majetkem	majetek	k1gInSc7	majetek
královským	královský	k2eAgInSc7d1	královský
a	a	k8xC	a
příležitostným	příležitostný	k2eAgNnSc7d1	příležitostné
venkovským	venkovský	k2eAgNnSc7d1	venkovské
rezidenčním	rezidenční	k2eAgNnSc7d1	rezidenční
sídlem	sídlo	k1gNnSc7	sídlo
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
značného	značný	k2eAgInSc2d1	značný
rozkvětu	rozkvět	k1gInSc2	rozkvět
a	a	k8xC	a
slávy	sláva	k1gFnSc2	sláva
zažíval	zažívat	k5eAaImAgInS	zažívat
Brandýs	Brandýs	k1gInSc1	Brandýs
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1581	[number]	k4	1581
povýšil	povýšit	k5eAaPmAgInS	povýšit
Brandýs	Brandýs	k1gInSc1	Brandýs
na	na	k7c4	na
královské	královský	k2eAgNnSc4d1	královské
komorní	komorní	k2eAgNnSc4d1	komorní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
přestavět	přestavět	k5eAaPmF	přestavět
zdejší	zdejší	k2eAgInSc4d1	zdejší
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
upravit	upravit	k5eAaPmF	upravit
terasovitou	terasovitý	k2eAgFnSc4d1	terasovitá
zahradu	zahrada	k1gFnSc4	zahrada
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeho	jeho	k3xOp3gMnSc1	jeho
komorní	komorní	k1gMnSc1	komorní
architekt	architekt	k1gMnSc1	architekt
Giovanni	Giovann	k1gMnPc1	Giovann
Maria	Mario	k1gMnSc2	Mario
Filippi	Filipp	k1gFnSc2	Filipp
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1508	[number]	k4	1508
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
město	město	k1gNnSc1	město
budovu	budova	k1gFnSc4	budova
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
pro	pro	k7c4	pro
sklad	sklad	k1gInSc4	sklad
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
záhy	záhy	k6eAd1	záhy
zřízena	zřízen	k2eAgFnSc1d1	zřízena
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
prošla	projít	k5eAaPmAgFnS	projít
mnoha	mnoho	k4c7	mnoho
úpravami	úprava	k1gFnPc7	úprava
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
do	do	k7c2	do
secesní	secesní	k2eAgFnSc2d1	secesní
podoby	podoba	k1gFnSc2	podoba
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
obdržela	obdržet	k5eAaPmAgFnS	obdržet
novou	nový	k2eAgFnSc4d1	nová
přístavbu	přístavba	k1gFnSc4	přístavba
<g/>
.	.	kIx.	.
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
freska	freska	k1gFnSc1	freska
na	na	k7c6	na
průčelí	průčelí	k1gNnSc6	průčelí
zpodobuje	zpodobovat	k5eAaImIp3nS	zpodobovat
akt	akt	k1gInSc1	akt
povýšení	povýšení	k1gNnSc2	povýšení
města	město	k1gNnSc2	město
Rudolfem	Rudolf	k1gMnSc7	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
jednoocasý	jednoocasý	k2eAgInSc1d1	jednoocasý
lev	lev	k1gInSc1	lev
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
asi	asi	k9	asi
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Michalovic	Michalovice	k1gFnPc2	Michalovice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
nahrazen	nahradit	k5eAaPmNgInS	nahradit
českým	český	k2eAgInSc7d1	český
dvouocasým	dvouocasý	k2eAgInSc7d1	dvouocasý
lvem	lev	k1gInSc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sídlila	sídlit	k5eAaImAgFnS	sídlit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
významná	významný	k2eAgFnSc1d1	významná
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
vzrůstající	vzrůstající	k2eAgFnSc1d1	vzrůstající
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tvořila	tvořit	k5eAaImAgFnS	tvořit
6	[number]	k4	6
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Potoce	potok	k1gInSc6	potok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starších	starý	k2eAgFnPc2d2	starší
synagog	synagoga	k1gFnPc2	synagoga
zničených	zničený	k2eAgFnPc2d1	zničená
požárem	požár	k1gInSc7	požár
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
synagoga	synagoga	k1gFnSc1	synagoga
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
doložena	doložen	k2eAgFnSc1d1	doložena
roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
synagoga	synagoga	k1gFnSc1	synagoga
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1657	[number]	k4	1657
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
přístupna	přístupen	k2eAgFnSc1d1	přístupna
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
nevyhnuly	vyhnout	k5eNaPmAgFnP	vyhnout
ani	ani	k8xC	ani
válečné	válečný	k2eAgFnPc1d1	válečná
útrapy	útrapa	k1gFnPc1	útrapa
a	a	k8xC	a
požáry	požár	k1gInPc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgFnSc7d3	nejtěžší
zkouškou	zkouška	k1gFnSc7	zkouška
byla	být	k5eAaImAgFnS	být
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
obsazen	obsazen	k2eAgInSc1d1	obsazen
Sasy	Sas	k1gMnPc7	Sas
i	i	k8xC	i
Švédy	Švéd	k1gMnPc7	Švéd
a	a	k8xC	a
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
pustošeno	pustošit	k5eAaImNgNnS	pustošit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
požár	požár	k1gInSc1	požár
postihl	postihnout	k5eAaPmAgInS	postihnout
Brandýs	Brandýs	k1gInSc4	Brandýs
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
shořela	shořet	k5eAaPmAgFnS	shořet
valná	valný	k2eAgFnSc1d1	valná
část	část	k1gFnSc1	část
domů	dům	k1gInPc2	dům
včetně	včetně	k7c2	včetně
židovské	židovský	k2eAgFnSc2d1	židovská
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
se	se	k3xPyFc4	se
na	na	k7c6	na
brandýském	brandýský	k2eAgInSc6d1	brandýský
zámku	zámek	k1gInSc6	zámek
sešli	sejít	k5eAaPmAgMnP	sejít
tři	tři	k4xCgMnPc1	tři
panovníci	panovník	k1gMnPc1	panovník
<g/>
:	:	kIx,	:
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
I.	I.	kA	I.
<g/>
,	,	kIx,	,
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tu	tu	k6eAd1	tu
připravovali	připravovat	k5eAaImAgMnP	připravovat
úder	úder	k1gInSc4	úder
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
I.	I.	kA	I.
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikaly	vznikat	k5eAaImAgInP	vznikat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
manufaktury	manufaktura	k1gFnSc2	manufaktura
a	a	k8xC	a
továrny	továrna	k1gFnSc2	továrna
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
i	i	k9	i
komunikace	komunikace	k1gFnPc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
železnice	železnice	k1gFnSc1	železnice
z	z	k7c2	z
Čelákovic	Čelákovice	k1gFnPc2	Čelákovice
do	do	k7c2	do
Neratovic	Neratovice	k1gFnPc2	Neratovice
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
č.	č.	k?	č.
0	[number]	k4	0
<g/>
74	[number]	k4	74
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Lysé	Lysá	k1gFnSc2	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc4	trať
č.	č.	k?	č.
0	[number]	k4	0
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
vykonával	vykonávat	k5eAaImAgInS	vykonávat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
i	i	k9	i
poslední	poslední	k2eAgMnSc1d1	poslední
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
i	i	k8xC	i
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
blahoslavený	blahoslavený	k2eAgMnSc1d1	blahoslavený
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
veslař	veslař	k1gMnSc1	veslař
Ondřej	Ondřej	k1gMnSc1	Ondřej
Synek	Synek	k1gMnSc1	Synek
či	či	k8xC	či
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
Oto	Oto	k1gMnSc1	Oto
Biederman	biederman	k1gMnSc1	biederman
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Brandýs	Brandýs	k1gInSc4	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
soukromě	soukromě	k6eAd1	soukromě
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
při	při	k7c6	při
prohlídce	prohlídka	k1gFnSc6	prohlídka
zámku	zámek	k1gInSc2	zámek
jako	jako	k8xC	jako
alternativy	alternativa	k1gFnSc2	alternativa
k	k	k7c3	k
Lánům	lán	k1gInPc3	lán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vojenských	vojenský	k2eAgInPc2d1	vojenský
manévrů	manévr	k1gInPc2	manévr
v	v	k7c6	v
Milovicích	Milovice	k1gFnPc6	Milovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
poklepal	poklepat	k5eAaPmAgInS	poklepat
na	na	k7c4	na
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
Mařatkova	Mařatkův	k2eAgInSc2d1	Mařatkův
pomníku	pomník	k1gInSc2	pomník
Památníku	památník	k1gInSc2	památník
národního	národní	k2eAgInSc2d1	národní
odboje	odboj	k1gInSc2	odboj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1923	[number]	k4	1923
při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
starosty	starosta	k1gMnPc7	starosta
regionu	region	k1gInSc2	region
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
oficiálně	oficiálně	k6eAd1	oficiálně
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
záležitostí	záležitost	k1gFnPc2	záležitost
města	město	k1gNnSc2	město
a	a	k8xC	a
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
města	město	k1gNnSc2	město
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
T.G.	T.G.	k1gMnSc1	T.G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
zámek	zámek	k1gInSc1	zámek
se	s	k7c7	s
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
na	na	k7c6	na
starších	starý	k2eAgInPc6d2	starší
základech	základ	k1gInPc6	základ
dobudoval	dobudovat	k5eAaPmAgInS	dobudovat
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Maria	Mario	k1gMnSc2	Mario
Filippi	Filipp	k1gFnSc2	Filipp
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1602-06	[number]	k4	1602-06
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
s	s	k7c7	s
praporcem	praporec	k1gInSc7	praporec
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
mlýnem	mlýn	k1gInSc7	mlýn
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
rampě	rampa	k1gFnSc6	rampa
zámku	zámek	k1gInSc2	zámek
mlýn	mlýn	k1gInSc4	mlýn
v	v	k7c6	v
podzámčí	podzámčí	k1gNnSc6	podzámčí
-	-	kIx~	-
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
svými	svůj	k3xOyFgInPc7	svůj
jedenácti	jedenáct	k4xCc7	jedenáct
mlýnskými	mlýnský	k2eAgNnPc7d1	mlýnské
koly	kolo	k1gNnPc7	kolo
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
barokní	barokní	k2eAgInSc4d1	barokní
pivovar	pivovar	k1gInSc4	pivovar
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
postil	postit	k5eAaImAgMnS	postit
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
-	-	kIx~	-
gotická	gotický	k2eAgFnSc1d1	gotická
památka	památka	k1gFnSc1	památka
s	s	k7c7	s
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
freskovou	freskový	k2eAgFnSc7d1	fresková
výzdobou	výzdoba	k1gFnSc7	výzdoba
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
barokně	barokně	k6eAd1	barokně
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
<g />
.	.	kIx.	.
</s>
<s>
Petra	Petr	k1gMnSc2	Petr
renesanční	renesanční	k2eAgMnSc1d1	renesanční
katolický	katolický	k2eAgMnSc1d1	katolický
kostel	kostel	k1gInSc1	kostel
Obrácení	obrácení	k1gNnSc4	obrácení
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
-	-	kIx~	-
původně	původně	k6eAd1	původně
sbor	sbor	k1gInSc1	sbor
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
Matteo	Matteo	k6eAd1	Matteo
Borgorelli	Borgorell	k1gMnSc3	Borgorell
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1541	[number]	k4	1541
<g/>
-	-	kIx~	-
<g/>
1542	[number]	k4	1542
některé	některý	k3yIgFnPc4	některý
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
katovna	katovna	k1gFnSc1	katovna
s	s	k7c7	s
psaníčkovými	psaníčkový	k2eAgNnPc7d1	psaníčkové
sgrafity	sgrafito	k1gNnPc7	sgrafito
Budova	budova	k1gFnSc1	budova
Městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Oblastní	oblastní	k2eAgNnSc1d1	oblastní
muzeum	muzeum	k1gNnSc1	muzeum
Praha	Praha	k1gFnSc1	Praha
východ	východ	k1gInSc4	východ
o.	o.	k?	o.
<g/>
p.	p.	k?	p.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
renesanční	renesanční	k2eAgFnSc7d1	renesanční
tzv.	tzv.	kA	tzv.
Arnoldinovský	Arnoldinovský	k2eAgInSc4d1	Arnoldinovský
dům	dům	k1gInSc4	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
budova	budova	k1gFnSc1	budova
muzea	muzeum	k1gNnSc2	muzeum
barokní	barokní	k2eAgFnSc7d1	barokní
děkanství	děkanství	k1gNnSc4	děkanství
a	a	k8xC	a
zvonici	zvonice	k1gFnSc4	zvonice
u	u	k7c2	u
katolického	katolický	k2eAgInSc2d1	katolický
kostela	kostel	k1gInSc2	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1568	[number]	k4	1568
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
židovská	židovský	k2eAgFnSc1d1	židovská
synagoga	synagoga	k1gFnSc1	synagoga
Obnovený	obnovený	k2eAgInSc4d1	obnovený
pomník	pomník	k1gInSc4	pomník
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
(	(	kIx(	(
<g/>
Břetislav	Břetislav	k1gMnSc1	Břetislav
Benda	Benda	k1gMnSc1	Benda
<g/>
)	)	kIx)	)
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
<g />
.	.	kIx.	.
</s>
<s>
Gymnázia	gymnázium	k1gNnPc1	gymnázium
(	(	kIx(	(
<g/>
socha	socha	k1gFnSc1	socha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
nalezena	nalezen	k2eAgFnSc1d1	nalezena
pod	pod	k7c7	pod
uhlím	uhlí	k1gNnSc7	uhlí
a	a	k8xC	a
starými	starý	k2eAgFnPc7d1	stará
pneumatikami	pneumatika	k1gFnPc7	pneumatika
ve	v	k7c6	v
sklepích	sklep	k1gInPc6	sklep
zámku	zámek	k1gInSc2	zámek
v	v	k7c4	v
Nelahozevsi	Nelahozevse	k1gFnSc4	Nelahozevse
a	a	k8xC	a
znovuodhalena	znovuodhalen	k2eAgFnSc1d1	znovuodhalen
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
UK	UK	kA	UK
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Barokní	barokní	k2eAgFnSc2d1	barokní
kaple	kaple	k1gFnSc2	kaple
vlevo	vlevo	k6eAd1	vlevo
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
silnici	silnice	k1gFnSc6	silnice
přes	přes	k7c4	přes
mosty	most	k1gInPc4	most
do	do	k7c2	do
Staré	Staré	k2eAgFnSc2d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
Zvony	zvon	k1gInPc1	zvon
z	z	k7c2	z
Klecan	Klecana	k1gFnPc2	Klecana
byly	být	k5eAaImAgInP	být
zapůjčeny	zapůjčit	k5eAaPmNgInP	zapůjčit
do	do	k7c2	do
Brandýsa	Brandýs	k1gInSc2	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
vysvěcení	vysvěcení	k1gNnSc2	vysvěcení
tamního	tamní	k2eAgInSc2d1	tamní
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
vracely	vracet	k5eAaImAgInP	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
si	se	k3xPyFc3	se
zpívaly	zpívat	k5eAaImAgInP	zpívat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hou	hou	k0	hou
<g/>
-	-	kIx~	-
<g/>
hou	hou	k0	hou
<g/>
!	!	kIx.	!
</s>
<s>
Klecanské	Klecanský	k2eAgInPc1d1	Klecanský
zvony	zvon	k1gInPc1	zvon
jdou	jít	k5eAaImIp3nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
Jdou	jít	k5eAaImIp3nP	jít
-	-	kIx~	-
jdou	jít	k5eAaImIp3nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Když	když	k8xS	když
letěly	letět	k5eAaImAgFnP	letět
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
jel	jet	k5eAaImAgMnS	jet
po	po	k7c6	po
mostě	most	k1gInSc6	most
bezbožný	bezbožný	k2eAgMnSc1d1	bezbožný
kočí	kočí	k1gMnSc1	kočí
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
bič	bič	k1gInSc1	bič
zapletl	zaplést	k5eAaPmAgInS	zaplést
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
zvonů	zvon	k1gInPc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Kočí	Kočí	k1gFnSc1	Kočí
zlostně	zlostně	k6eAd1	zlostně
zaklel	zaklít	k5eAaPmAgInS	zaklít
<g/>
,	,	kIx,	,
zvony	zvon	k1gInPc1	zvon
smutně	smutně	k6eAd1	smutně
zapěly	zapět	k5eAaPmAgInP	zapět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jdou	jít	k5eAaImIp3nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
Jdou	jít	k5eAaImIp3nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
Nedojdou	dojít	k5eNaPmIp3nP	dojít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
zapadly	zapadnout	k5eAaPmAgFnP	zapadnout
do	do	k7c2	do
tůně	tůně	k1gFnSc2	tůně
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
tam	tam	k6eAd1	tam
pradlena	pradlena	k1gFnSc1	pradlena
máchala	máchat	k5eAaImAgFnS	máchat
prádlo	prádlo	k1gNnSc4	prádlo
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
zvon	zvon	k1gInSc1	zvon
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Pradlena	pradlena	k1gFnSc1	pradlena
netušila	tušit	k5eNaImAgFnS	tušit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jí	jíst	k5eAaImIp3nS	jíst
to	ten	k3xDgNnSc1	ten
drží	držet	k5eAaImIp3nS	držet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vykřikla	vykřiknout	k5eAaPmAgFnS	vykřiknout
a	a	k8xC	a
zvon	zvon	k1gInSc1	zvon
se	se	k3xPyFc4	se
potopil	potopit	k5eAaPmAgInS	potopit
navždy	navždy	k6eAd1	navždy
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
v	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
starého	starý	k2eAgNnSc2d1	staré
Labe	Labe	k1gNnSc2	Labe
v	v	k7c6	v
tůni	tůně	k1gFnSc6	tůně
pod	pod	k7c7	pod
jezem	jez	k1gInSc7	jez
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
zvony	zvon	k1gInPc4	zvon
nebo	nebo	k8xC	nebo
při	při	k7c6	při
úplňku	úplněk	k1gInSc6	úplněk
slyšet	slyšet	k5eAaImF	slyšet
jejich	jejich	k3xOp3gNnSc4	jejich
smutné	smutný	k2eAgNnSc4d1	smutné
vyzvánění	vyzvánění	k1gNnSc4	vyzvánění
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jdou	jít	k5eAaImIp3nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
Jdou	jít	k5eAaImIp3nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
Nedojdou	dojít	k5eNaPmIp3nP	dojít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Haak	Haak	k1gMnSc1	Haak
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
Václav	Václav	k1gMnSc1	Václav
Husa	Husa	k1gMnSc1	Husa
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Karlach	Karlach	k1gMnSc1	Karlach
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšehradský	vyšehradský	k2eAgMnSc1d1	vyšehradský
probošt	probošt	k1gMnSc1	probošt
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kirs	Kirsa	k1gFnPc2	Kirsa
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violoncellista	violoncellista	k1gMnSc1	violoncellista
Adolf	Adolf	k1gMnSc1	Adolf
Kramenič	Kramenič	k1gMnSc1	Kramenič
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Jiří	Jiří	k1gMnSc1	Jiří
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Barbora	Barbora	k1gFnSc1	Barbora
Laláková	Laláková	k1gFnSc1	Laláková
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přebornice	přebornice	k1gFnSc1	přebornice
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
Karel	Karel	k1gMnSc1	Karel
Macháček	Macháček	k1gMnSc1	Macháček
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
lékař	lékař	k1gMnSc1	lékař
Dalibor	Dalibor	k1gMnSc1	Dalibor
Mlejnský	Mlejnský	k1gMnSc1	Mlejnský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
komunální	komunální	k2eAgMnSc1d1	komunální
politik	politik	k1gMnSc1	politik
Karel	Karel	k1gMnSc1	Karel
Plischke	Plischke	k1gFnSc1	Plischke
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
Justin	Justin	k1gMnSc1	Justin
Václav	Václav	k1gMnSc1	Václav
Prášek	Prášek	k1gMnSc1	Prášek
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
orientalista	orientalista	k1gMnSc1	orientalista
David	David	k1gMnSc1	David
Rikl	Rikl	k1gMnSc1	Rikl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Ludvík	Ludvík	k1gMnSc1	Ludvík
Seyvalter	Seyvalter	k1gMnSc1	Seyvalter
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
filantrop	filantrop	k1gMnSc1	filantrop
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g />
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Skála	Skála	k1gMnSc1	Skála
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
komunistický	komunistický	k2eAgMnSc1d1	komunistický
funkcionář	funkcionář	k1gMnSc1	funkcionář
Alžběta	Alžběta	k1gFnSc1	Alžběta
Skálová	Skálová	k1gFnSc1	Skálová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grafička	grafička	k1gFnSc1	grafička
a	a	k8xC	a
ilustrátorka	ilustrátorka	k1gFnSc1	ilustrátorka
Karel	Karel	k1gMnSc1	Karel
Šebor	Šebor	k1gMnSc1	Šebor
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Jan	Jan	k1gMnSc1	Jan
Tesánek	Tesánek	k1gMnSc1	Tesánek
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1728	[number]	k4	1728
<g/>
-	-	kIx~	-
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
Vendula	Vendula	k1gFnSc1	Vendula
Vartová-Eliášová	Vartová-Eliášová	k1gFnSc1	Vartová-Eliášová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
Žili	žít	k5eAaImAgMnP	žít
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Karel	Karla	k1gFnPc2	Karla
I.	I.	kA	I.
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
zde	zde	k6eAd1	zde
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
Ludvík	Ludvík	k1gMnSc1	Ludvík
Salvátor	Salvátor	k1gMnSc1	Salvátor
<g />
.	.	kIx.	.
</s>
<s>
Toskánský	toskánský	k2eAgMnSc1d1	toskánský
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
etnograf	etnograf	k1gMnSc1	etnograf
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
brandýského	brandýský	k2eAgInSc2d1	brandýský
zámku	zámek	k1gInSc2	zámek
<g/>
;	;	kIx,	;
strávil	strávit	k5eAaPmAgMnS	strávit
zde	zde	k6eAd1	zde
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
života	život	k1gInSc2	život
Leo	Leo	k1gMnSc1	Leo
Vaniš	Vaniš	k1gMnSc1	Vaniš
<g/>
,	,	kIx,	,
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
debutoval	debutovat	k5eAaBmAgInS	debutovat
v	v	k7c6	v
zdejším	zdejší	k2eAgNnSc6d1	zdejší
muzeu	muzeum	k1gNnSc6	muzeum
expozicí	expozice	k1gFnSc7	expozice
ilustrací	ilustrace	k1gFnSc7	ilustrace
<g/>
,	,	kIx,	,
strávil	strávit	k5eAaPmAgMnS	strávit
zde	zde	k6eAd1	zde
studijní	studijní	k2eAgNnPc4d1	studijní
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vanišova	Vanišův	k2eAgFnSc1d1	Vanišova
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Polabím	Polabí	k1gNnSc7	Polabí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
-	-	kIx~	-
pražský	pražský	k2eAgMnSc1d1	pražský
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
výtvarník	výtvarník	k1gMnSc1	výtvarník
Leo	Leo	k1gMnSc1	Leo
Vaniš	Vaniš	k1gMnSc1	Vaniš
zde	zde	k6eAd1	zde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
na	na	k7c6	na
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Pedagogickém	pedagogický	k2eAgInSc6d1	pedagogický
institutu	institut	k1gInSc6	institut
malbu	malba	k1gFnSc4	malba
<g/>
.	.	kIx.	.
</s>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čížková	Čížková	k1gFnSc1	Čížková
-	-	kIx~	-
sochařka	sochařka	k1gFnSc1	sochařka
a	a	k8xC	a
malířka	malířka	k1gFnSc1	malířka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
svůj	svůj	k3xOyFgInSc4	svůj
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
především	především	k9	především
dívčích	dívčí	k2eAgInPc2d1	dívčí
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
žije	žít	k5eAaImIp3nS	žít
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
také	také	k9	také
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Petiška	Petišek	k1gInSc2	Petišek
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Starých	Starých	k2eAgFnPc2d1	Starých
řeckých	řecký	k2eAgFnPc2d1	řecká
bájí	báj	k1gFnPc2	báj
a	a	k8xC	a
pověstí	pověst	k1gFnSc7	pověst
Martin	Martina	k1gFnPc2	Martina
Petiška	Petišek	k1gInSc2	Petišek
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
Antonín	Antonín	k1gMnSc1	Antonín
Bečvář	Bečvář	k1gMnSc1	Bečvář
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
klimatolog	klimatolog	k1gMnSc1	klimatolog
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
Drahoňovský	Drahoňovský	k1gMnSc1	Drahoňovský
<g/>
,	,	kIx,	,
obrozenecký	obrozenecký	k2eAgMnSc1d1	obrozenecký
humorista	humorista	k1gMnSc1	humorista
Souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
Louis	louis	k1gInPc7	louis
Brandeis	Brandeis	k1gFnSc2	Brandeis
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Brandeis	Brandeis	k1gFnSc1	Brandeis
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
