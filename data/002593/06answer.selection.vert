<s>
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Brandeis	Brandeis	k1gInSc1	Brandeis
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Elbe	Elbus	k1gMnSc5	Elbus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalé	bývalý	k2eAgNnSc1d1	bývalé
samostatné	samostatný	k2eAgNnSc1d1	samostatné
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Praha-východ	Prahaýchod	k1gInSc1	Praha-východ
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
částí	část	k1gFnPc2	část
souměstí	souměstí	k1gNnSc2	souměstí
Brandýs	Brandýs	k1gInSc4	Brandýs
nad	nad	k7c4	nad
Labem-Stará	Labem-Starý	k2eAgNnPc4d1	Labem-Starý
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
