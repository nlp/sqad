<s>
Cer	cer	k1gInSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Dub	dub	k1gInSc1
cer	cer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Cer	cer	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
1	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Ce	Ce	k?
</s>
<s>
58	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Cer	cer	k1gInSc1
<g/>
,	,	kIx,
Ce	Ce	k1gMnSc1
<g/>
,	,	kIx,
58	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Cerium	Cerium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
šedobílá	šedobílý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-45-1	7440-45-1	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
140,116	140,116	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
181,8	181,8	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
204	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
1	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,12	1,12	k4
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
krychová	krychová	k1gFnSc1
<g/>
,	,	kIx,
plošne	plošnout	k5eAaPmIp3nS,k5eAaImIp3nS
centrovaná	centrovaný	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
6,770	6,770	k4
kg	kg	kA
<g/>
·	·	k?
<g/>
dm	dm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
2,5	2,5	k4
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
11,3	11,3	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
794,85	794,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
068	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
3	#num#	k4
442,85	442,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
716	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
5,46	5,46	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
828	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetické	paramagnetický	k2eAgNnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lanthan	lanthan	k1gInSc1
≺	≺	k?
<g/>
Ce	Ce	k1gFnSc2
<g/>
≻	≻	k?
Praseodym	praseodym	k1gInSc1
</s>
<s>
⋎	⋎	k?
<g/>
Th	Th	k1gFnSc1
</s>
<s>
Cer	cer	k1gInSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ce	Ce	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Cerium	Cerium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedavě	šedavě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
vnitřně	vnitřně	k6eAd1
přechodný	přechodný	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
člen	člen	k1gInSc1
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
uplatnění	uplatnění	k1gNnSc1
nalézá	nalézat	k5eAaImIp3nS
v	v	k7c6
metalurgickém	metalurgický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
při	při	k7c6
výrobě	výroba	k1gFnSc6
speciálních	speciální	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
a	a	k8xC
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnSc4
deoxidaci	deoxidace	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
složkou	složka	k1gFnSc7
některých	některý	k3yIgNnPc2
skel	sklo	k1gNnPc2
a	a	k8xC
průmyslových	průmyslový	k2eAgInPc2d1
katalyzátorů	katalyzátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Cer	cer	k1gInSc1
vzhledově	vzhledově	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
železo	železo	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
šedavě	šedavě	k6eAd1
bílý	bílý	k2eAgInSc1d1
přechodný	přechodný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
však	však	k9
značně	značně	k6eAd1
měkký	měkký	k2eAgMnSc1d1
a	a	k8xC
snadno	snadno	k6eAd1
tvárný	tvárný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
cer	cer	k1gInSc1
značně	značně	k6eAd1
reaktivním	reaktivní	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
<g/>
,	,	kIx,
po	po	k7c6
europiu	europium	k1gNnSc6
nejreaktivnějším	reaktivní	k2eAgInSc7d3
lanthanoidem	lanthanoid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
mírně	mírně	k6eAd1
zvýšené	zvýšený	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
(	(	kIx(
<g/>
kolem	kolem	k7c2
80	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
reaguje	reagovat	k5eAaBmIp3nS
se	se	k3xPyFc4
vzdušným	vzdušný	k2eAgInSc7d1
kyslíkem	kyslík	k1gInSc7
(	(	kIx(
<g/>
hoří	hořet	k5eAaImIp3nS
<g/>
)	)	kIx)
za	za	k7c2
vzniku	vznik	k1gInSc2
velmi	velmi	k6eAd1
stabilního	stabilní	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
ceričitého	ceričitý	k2eAgInSc2d1
CeO	CeO	k1gFnPc7
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vodou	voda	k1gFnSc7
reaguje	reagovat	k5eAaBmIp3nS
cer	cer	k1gInSc1
za	za	k7c2
vzniku	vznik	k1gInSc2
plynného	plynný	k2eAgInSc2d1
vodíku	vodík	k1gInSc2
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
v	v	k7c6
běžných	běžný	k2eAgFnPc6d1
minerálních	minerální	k2eAgFnPc6d1
kyselinách	kyselina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Ce	Ce	k1gFnSc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
jako	jako	k9
jediný	jediný	k2eAgMnSc1d1
z	z	k7c2
lanthanoidů	lanthanoid	k1gInPc2
tvoří	tvořit	k5eAaImIp3nP
i	i	k9
stabilní	stabilní	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
s	s	k7c7
valencí	valence	k1gFnSc7
Ce	Ce	k1gFnSc7
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnPc4
Ce	Ce	k1gFnSc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
sloučeniny	sloučenina	k1gFnPc1
čtyřmocného	čtyřmocný	k2eAgInSc2d1
ceru	cer	k1gInSc2
mají	mít	k5eAaImIp3nP
barvu	barva	k1gFnSc4
žlutou	žlutý	k2eAgFnSc4d1
až	až	k9
oranžovou	oranžový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Objevili	objevit	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
současně	současně	k6eAd1
roku	rok	k1gInSc2
1803	#num#	k4
švédský	švédský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Jöns	Jönsa	k1gFnPc2
Jacob	Jacoba	k1gFnPc2
Berzelius	Berzelius	k1gInSc1
a	a	k8xC
Wilhelm	Wilhelm	k1gMnSc1
Hisinger	Hisinger	k1gMnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
v	v	k7c6
Německu	Německo	k1gNnSc6
Martin	Martin	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Klaproth	Klaproth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Cer	cer	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
nejvíce	nejvíce	k6eAd1,k6eAd3
zastoupeným	zastoupený	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
ze	z	k7c2
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
–	–	k?
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
v	v	k7c6
koncentraci	koncentrace	k1gFnSc6
asi	asi	k9
46	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
kolem	kolem	k7c2
0,0004	0,0004	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
ceru	cer	k1gInSc2
na	na	k7c4
30	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
cer	cer	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistují	existovat	k5eNaImIp3nP
však	však	k9
ani	ani	k9
minerály	minerál	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
by	by	kYmCp3nS
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
lanthanoidy	lanthanoida	k1gFnPc1
(	(	kIx(
<g/>
prvky	prvek	k1gInPc1
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
)	)	kIx)
vyskytovaly	vyskytovat	k5eAaImAgInP
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
minerály	minerál	k1gInPc4
směsné	směsný	k2eAgInPc4d1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
obsahují	obsahovat	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgInPc1
prvky	prvek	k1gInPc1
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
patří	patřit	k5eAaImIp3nS
monazity	monazit	k1gInPc4
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
<g/>
La	la	k1gNnSc1
<g/>
,	,	kIx,
<g/>
Th	Th	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Nd	Nd	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
4	#num#	k4
a	a	k8xC
xenotim	xenotim	k1gInSc1
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
fosforečnany	fosforečnan	k1gInPc1
lanthanoidů	lanthanoid	k1gInPc2
a	a	k8xC
dále	daleko	k6eAd2
bastnäsity	bastnäsit	k1gInPc1
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
<g/>
La	la	k1gNnSc1
<g/>
,	,	kIx,
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
CO	co	k9
<g/>
3	#num#	k4
<g/>
F	F	kA
–	–	k?
směsné	směsný	k2eAgInPc4d1
flourouhličitany	flourouhličitan	k1gInPc4
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
těchto	tento	k3xDgFnPc2
rud	ruda	k1gFnPc2
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
ve	v	k7c4
Skandinávii	Skandinávie	k1gFnSc4
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
a	a	k8xC
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
jsou	být	k5eAaImIp3nP
i	i	k9
fosfátové	fosfátový	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
–	–	k?
apatity	apatit	k1gInPc1
z	z	k7c2
poloostrova	poloostrov	k1gInSc2
Kola	kolo	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Při	při	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnPc1
rudy	ruda	k1gFnPc1
nejprve	nejprve	k6eAd1
louží	loužit	k5eAaImIp3nP
směsí	směs	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
a	a	k8xC
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1
a	a	k8xC
ze	z	k7c2
vzniklého	vzniklý	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
solí	solit	k5eAaImIp3nS
se	s	k7c7
přídavkem	přídavek	k1gInSc7
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
vysráží	vysrážet	k5eAaPmIp3nS
hydroxidy	hydroxid	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
separaci	separace	k1gFnSc3
ceru	cer	k1gInSc2
od	od	k7c2
zbylých	zbylý	k2eAgInPc2d1
lanthanoidů	lanthanoid	k1gInPc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
skutečnosti	skutečnost	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
hydroxid	hydroxid	k1gInSc1
ceričitý	ceričitý	k2eAgMnSc1d1
Ce	Ce	k1gMnSc1
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
podléhá	podléhat	k5eAaImIp3nS
hydrolýze	hydrolýza	k1gFnSc3
již	již	k6eAd1
v	v	k7c6
relativně	relativně	k6eAd1
kyselých	kyselý	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
(	(	kIx(
<g/>
kolem	kolem	k7c2
pH	ph	kA
=	=	kIx~
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směs	směs	k1gFnSc1
lanthanoidů	lanthanoid	k1gInPc2
se	se	k3xPyFc4
proto	proto	k8xC
nejprve	nejprve	k6eAd1
oxiduje	oxidovat	k5eAaBmIp3nS
působením	působení	k1gNnSc7
manganistanu	manganistan	k1gInSc2
draselného	draselný	k2eAgMnSc4d1
KMnO	KMnO	k1gMnSc4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
převede	převést	k5eAaPmIp3nS
veškerý	veškerý	k3xTgInSc4
cer	cer	k1gInSc4
do	do	k7c2
mocenství	mocenství	k1gNnSc2
Ce	Ce	k1gFnSc2
<g/>
4	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
postupnou	postupný	k2eAgFnSc7d1
neutralizací	neutralizace	k1gFnSc7
kyselého	kyselý	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
se	se	k3xPyFc4
vysráží	vysrážet	k5eAaPmIp3nS
prakticky	prakticky	k6eAd1
čistý	čistý	k2eAgInSc1d1
nerozpustný	rozpustný	k2eNgInSc1d1
hydroxid	hydroxid	k1gInSc1
ceričitý	ceričitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1
cer	cer	k1gInSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
elektrolýzou	elektrolýza	k1gFnSc7
taveniny	tavenina	k1gFnSc2
směsi	směs	k1gFnSc2
chloridu	chlorid	k1gInSc2
ceritého	ceritý	k2eAgInSc2d1
CeCl	CeCl	k1gInSc1
<g/>
3	#num#	k4
a	a	k8xC
chloridu	chlorid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
NaCl	NaCl	k1gInSc1
v	v	k7c6
grafitové	grafitový	k2eAgFnSc6d1
nádobě	nádoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cer	cer	k1gInSc1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
vylučuje	vylučovat	k5eAaImIp3nS
na	na	k7c6
grafitové	grafitový	k2eAgFnSc6d1
katodě	katoda	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
anodě	anoda	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
uvolňování	uvolňování	k1gNnSc3
plynného	plynný	k2eAgInSc2d1
chloru	chlor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Síran	síran	k1gInSc1
ceričitý	ceričitý	k2eAgInSc1d1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
vysokému	vysoký	k2eAgNnSc3d1
zastoupení	zastoupení	k1gNnSc3
ceru	cer	k1gInSc2
v	v	k7c6
rudách	ruda	k1gFnPc6
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
je	být	k5eAaImIp3nS
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
na	na	k7c6
trhu	trh	k1gInSc6
relativně	relativně	k6eAd1
nadbytek	nadbytek	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
vzniká	vznikat	k5eAaImIp3nS
částečně	částečně	k6eAd1
jako	jako	k8xS,k8xC
přebytek	přebytek	k1gInSc1
při	při	k7c6
výrobě	výroba	k1gFnSc6
vysoce	vysoce	k6eAd1
žádaných	žádaný	k2eAgInPc2d1
lanthanoidů	lanthanoid	k1gInPc2
–	–	k?
především	především	k6eAd1
europia	europium	k1gNnSc2
nebo	nebo	k8xC
samaria	samarium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgNnSc1d1
průmyslové	průmyslový	k2eAgNnSc1d1
využití	využití	k1gNnSc1
nalézá	nalézat	k5eAaImIp3nS
cer	cer	k1gInSc4
v	v	k7c6
metalurgii	metalurgie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
vysoká	vysoký	k2eAgFnSc1d1
afinita	afinita	k1gFnSc1
ke	k	k7c3
kyslíku	kyslík	k1gInSc3
a	a	k8xC
síře	síra	k1gFnSc3
se	se	k3xPyFc4
uplatní	uplatnit	k5eAaPmIp3nS
při	při	k7c6
odkysličování	odkysličování	k1gNnSc6
a	a	k8xC
desulfuraci	desulfurace	k1gFnSc6
vyráběných	vyráběný	k2eAgInPc2d1
kovů	kov	k1gInPc2
a	a	k8xC
slitin	slitina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Oceli	ocel	k1gFnPc1
nebo	nebo	k8xC
litina	litina	k1gFnSc1
s	s	k7c7
obsahem	obsah	k1gInSc7
malých	malý	k2eAgNnPc2d1
množství	množství	k1gNnPc2
ceru	cer	k1gInSc2
vykazují	vykazovat	k5eAaImIp3nP
vyšší	vysoký	k2eAgFnSc4d2
tvárnost	tvárnost	k1gFnSc4
a	a	k8xC
kujnost	kujnost	k1gFnSc4
a	a	k8xC
mají	mít	k5eAaImIp3nP
vyšší	vysoký	k2eAgFnSc4d2
mechanickou	mechanický	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
nárazu	náraz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Přídavek	přídavek	k1gInSc1
ceru	cer	k1gInSc2
do	do	k7c2
slitin	slitina	k1gFnPc2
na	na	k7c6
bázi	báze	k1gFnSc6
hořčíku	hořčík	k1gInSc2
a	a	k8xC
hliníku	hliník	k1gInSc2
zlepšuje	zlepšovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
teplotním	teplotní	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
a	a	k8xC
usnadňuje	usnadňovat	k5eAaImIp3nS
odlévání	odlévání	k1gNnSc1
složitějších	složitý	k2eAgInPc2d2
výrobků	výrobek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Slitina	slitina	k1gFnSc1
s	s	k7c7
wolframem	wolfram	k1gInSc7
slouží	sloužit	k5eAaImIp3nP
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
elektrod	elektroda	k1gFnPc2
pro	pro	k7c4
svařování	svařování	k1gNnSc4
a	a	k8xC
řezání	řezání	k1gNnSc4
kovů	kov	k1gInPc2
elektrickým	elektrický	k2eAgInSc7d1
obloukem	oblouk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obloukové	obloukový	k2eAgFnPc4d1
lampy	lampa	k1gFnPc4
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgFnPc4d1
především	především	k6eAd1
jako	jako	k8xS,k8xC
světelné	světelný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
při	při	k7c6
natáčení	natáčení	k1gNnSc6
filmů	film	k1gInPc2
<g/>
,	,	kIx,
mívají	mívat	k5eAaImIp3nP
často	často	k6eAd1
elektrody	elektroda	k1gFnPc4
ze	z	k7c2
slitin	slitina	k1gFnPc2
s	s	k7c7
obsahem	obsah	k1gInSc7
ceru	cer	k1gInSc2
a	a	k8xC
lanthanu	lanthan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgNnSc1d1
uplatnění	uplatnění	k1gNnPc1
nalézají	nalézat	k5eAaImIp3nP
sloučeniny	sloučenina	k1gFnPc1
ceru	cer	k1gInSc2
(	(	kIx(
<g/>
především	především	k9
oxid	oxid	k1gInSc4
ceričitý	ceričitý	k2eAgInSc4d1
CeO	CeO	k1gFnSc7
<g/>
2	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
sklářském	sklářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
přídavek	přídavek	k1gInSc4
do	do	k7c2
skloviny	sklovina	k1gFnSc2
slouží	sloužit	k5eAaImIp3nS
hlavně	hlavně	k9
k	k	k7c3
odbarvování	odbarvování	k1gNnSc3
vyrobeného	vyrobený	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
a	a	k8xC
snižuje	snižovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
propustnost	propustnost	k1gFnSc4
pro	pro	k7c4
ultrafialové	ultrafialový	k2eAgNnSc4d1
záření	záření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Katalyzátory	katalyzátor	k1gInPc1
s	s	k7c7
obsahem	obsah	k1gInSc7
ceru	cer	k1gInSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
petrochemii	petrochemie	k1gFnSc6
při	při	k7c6
krakování	krakování	k1gNnSc6
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brusné	brusný	k2eAgInPc1d1
a	a	k8xC
lešticí	lešticí	k2eAgInPc1d1
práškové	práškový	k2eAgInPc1d1
materiály	materiál	k1gInPc1
<g/>
,	,	kIx,
používané	používaný	k2eAgInPc1d1
při	při	k7c6
výrobě	výroba	k1gFnSc6
optických	optický	k2eAgFnPc2d1
součástek	součástka	k1gFnPc2
(	(	kIx(
<g/>
přesné	přesný	k2eAgFnSc2d1
čočky	čočka	k1gFnSc2
<g/>
,	,	kIx,
zrcadla	zrcadlo	k1gNnSc2
do	do	k7c2
dalekohledů	dalekohled	k1gInPc2
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obsahují	obsahovat	k5eAaImIp3nP
často	často	k6eAd1
významný	významný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
sloučenin	sloučenina	k1gFnPc2
ceru	cer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Soli	sůl	k1gFnPc1
čtyřmocného	čtyřmocný	k2eAgInSc2d1
ceru	cer	k1gInSc2
jsou	být	k5eAaImIp3nP
silná	silný	k2eAgNnPc1d1
oxidační	oxidační	k2eAgNnPc1d1
činidla	činidlo	k1gNnPc1
a	a	k8xC
především	především	k9
síran	síran	k1gInSc1
ceričitý	ceričitý	k2eAgInSc1d1
Ce	Ce	k1gMnSc7
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
často	často	k6eAd1
používán	používat	k5eAaImNgInS
v	v	k7c6
analytické	analytický	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
pro	pro	k7c4
oxidaci	oxidace	k1gFnSc4
analyzované	analyzovaný	k2eAgFnSc2d1
látky	látka	k1gFnSc2
v	v	k7c6
redoxních	redoxní	k2eAgFnPc6d1
titracích	titrace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
nalézá	nalézat	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
v	v	k7c6
preparativní	preparativní	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
při	při	k7c6
oxidační	oxidační	k2eAgFnSc6d1
syntéze	syntéza	k1gFnSc6
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
cer	cer	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
cer	cer	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4147480-6	4147480-6	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5763	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85022138	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85022138	#num#	k4
</s>
