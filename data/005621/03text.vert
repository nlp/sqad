<s>
Mouka	mouka	k1gFnSc1	mouka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
rozmělněná	rozmělněný	k2eAgFnSc1d1	rozmělněná
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
obilného	obilný	k2eAgNnSc2d1	obilné
zrna	zrno	k1gNnSc2	zrno
(	(	kIx(	(
<g/>
obilky	obilka	k1gFnSc2	obilka
<g/>
)	)	kIx)	)
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
podílem	podíl	k1gInSc7	podíl
otrubnatých	otrubnatý	k2eAgFnPc2d1	otrubnatý
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
jídel	jídlo	k1gNnPc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
obchodní	obchodní	k2eAgFnSc6d1	obchodní
síti	síť	k1gFnSc6	síť
se	se	k3xPyFc4	se
pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
(	(	kIx(	(
<g/>
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
běžně	běžně	k6eAd1	běžně
prodává	prodávat	k5eAaImIp3nS	prodávat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
druzích	druh	k1gInPc6	druh
rozlišených	rozlišený	k2eAgInPc6d1	rozlišený
podle	podle	k7c2	podle
tloušťky	tloušťka	k1gFnSc2	tloušťka
zrn	zrno	k1gNnPc2	zrno
<g/>
:	:	kIx,	:
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
nejjemnější	jemný	k2eAgFnSc1d3	nejjemnější
zrnitost	zrnitost	k1gFnSc1	zrnitost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polohrubá	polohrubý	k2eAgFnSc1d1	polohrubá
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
<g/>
,	,	kIx,	,
krupice	krupice	k1gFnSc1	krupice
(	(	kIx(	(
<g/>
nejhrubší	hrubý	k2eAgFnSc1d3	nejhrubší
zrnitost	zrnitost	k1gFnSc1	zrnitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mouka	mouka	k1gFnSc1	mouka
se	se	k3xPyFc4	se
mele	mlít	k5eAaImIp3nS	mlít
již	již	k6eAd1	již
od	od	k7c2	od
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Obilí	obilí	k1gNnSc1	obilí
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nemele	mlít	k5eNaImIp3nS	mlít
celé	celý	k2eAgNnSc1d1	celé
(	(	kIx(	(
<g/>
krom	krom	k7c2	krom
celozrnné	celozrnný	k2eAgFnSc2d1	celozrnná
mouky	mouka	k1gFnSc2	mouka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
od	od	k7c2	od
obalu	obal	k1gInSc2	obal
a	a	k8xC	a
zrno	zrno	k1gNnSc4	zrno
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
omílá	omílat	k5eAaImIp3nS	omílat
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hrubosti	hrubost	k1gFnSc2	hrubost
mletí	mletí	k1gNnSc2	mletí
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
u	u	k7c2	u
mouky	mouka	k1gFnSc2	mouka
uvádí	uvádět	k5eAaImIp3nS	uvádět
číselné	číselný	k2eAgNnSc1d1	číselné
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
popela	popel	k1gInSc2	popel
zbude	zbýt	k5eAaPmIp3nS	zbýt
po	po	k7c6	po
spálení	spálení	k1gNnSc6	spálení
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
mouky	mouka	k1gFnSc2	mouka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
část	část	k1gFnSc4	část
zrna	zrno	k1gNnSc2	zrno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nejvíce	nejvíce	k6eAd1	nejvíce
škrobů	škrob	k1gInPc2	škrob
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
popelovin	popelovina	k1gFnPc2	popelovina
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
větší	veliký	k2eAgInSc1d2	veliký
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
bylo	být	k5eAaImAgNnS	být
umleto	umlít	k5eAaPmNgNnS	umlít
z	z	k7c2	z
okrajové	okrajový	k2eAgFnSc2d1	okrajová
části	část	k1gFnSc2	část
zrna	zrno	k1gNnSc2	zrno
a	a	k8xC	a
mouka	mouka	k1gFnSc1	mouka
tedy	tedy	k9	tedy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
z	z	k7c2	z
nutričního	nutriční	k2eAgNnSc2d1	nutriční
hlediska	hledisko	k1gNnSc2	hledisko
kvalitnější	kvalitní	k2eAgNnSc1d2	kvalitnější
než	než	k8xS	než
například	například	k6eAd1	například
krupice	krupice	k1gFnSc1	krupice
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
nízkovymleté	nízkovymletý	k2eAgFnPc1d1	nízkovymletý
mouky	mouka	k1gFnPc1	mouka
s	s	k7c7	s
nižším	nízký	k2eAgNnSc7d2	nižší
číslem	číslo	k1gNnSc7	číslo
jsou	být	k5eAaImIp3nP	být
chutnější	chutný	k2eAgMnPc1d2	chutnější
a	a	k8xC	a
trvanlivější	trvanlivý	k2eAgMnPc1d2	trvanlivější
<g/>
.	.	kIx.	.
</s>
<s>
Slupka	slupka	k1gFnSc1	slupka
zrna	zrno	k1gNnSc2	zrno
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
L-cystein	Lystein	k1gInSc4	L-cystein
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
mouka	mouka	k1gFnSc1	mouka
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
L-cystein	Lystein	k1gInSc1	L-cystein
dodává	dodávat	k5eAaImIp3nS	dodávat
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
lidských	lidský	k2eAgInPc2d1	lidský
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Mouku	mouka	k1gFnSc4	mouka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
umlít	umlít	k5eAaPmF	umlít
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
obiloviny	obilovina	k1gFnSc2	obilovina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pšeničné	pšeničný	k2eAgFnSc2d1	pšeničná
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
také	také	k9	také
žitná	žitný	k2eAgFnSc1d1	Žitná
mouka	mouka	k1gFnSc1	mouka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k6eAd1	především
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
chleba	chléb	k1gInSc2	chléb
nebo	nebo	k8xC	nebo
perníku	perník	k1gInSc2	perník
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
také	také	k9	také
špaldová	špaldový	k2eAgFnSc1d1	špaldová
mouka	mouka	k1gFnSc1	mouka
<g/>
,	,	kIx,	,
vhodná	vhodný	k2eAgFnSc1d1	vhodná
do	do	k7c2	do
sladkých	sladký	k2eAgFnPc2d1	sladká
i	i	k8xC	i
slaných	slaný	k2eAgNnPc2d1	slané
těst	těsto	k1gNnPc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Mouka	mouka	k1gFnSc1	mouka
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
například	například	k6eAd1	například
z	z	k7c2	z
ječmene	ječmen	k1gInSc2	ječmen
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
pohanky	pohanka	k1gFnSc2	pohanka
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
sóji	sója	k1gFnSc2	sója
nebo	nebo	k8xC	nebo
amarantu	amarant	k1gInSc2	amarant
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgFnPc2d1	poslední
pět	pět	k4xCc1	pět
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
mouk	mouka	k1gFnPc2	mouka
jsou	být	k5eAaImIp3nP	být
mouky	mouka	k1gFnPc1	mouka
přirozeně	přirozeně	k6eAd1	přirozeně
bezlepkové	bezlepkový	k2eAgFnPc1d1	bezlepková
<g/>
,	,	kIx,	,
bezlepková	bezlepkový	k2eAgFnSc1d1	bezlepková
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
i	i	k9	i
deglutenizovaná	deglutenizovaný	k2eAgFnSc1d1	deglutenizovaný
pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
bližší	blízký	k2eAgNnSc1d2	bližší
běžné	běžný	k2eAgNnSc1d1	běžné
pšeničné	pšeničné	k1gNnSc1	pšeničné
mouce	mouka	k1gFnSc6	mouka
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Jizerka	Jizerka	k1gFnSc1	Jizerka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pšeničné	pšeničný	k2eAgFnPc1d1	pšeničná
mouky	mouka	k1gFnPc1	mouka
T	T	kA	T
400	[number]	k4	400
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
výběrová	výběrový	k2eAgFnSc1d1	výběrová
polohrubá	polohrubý	k2eAgFnSc1d1	polohrubá
T	T	kA	T
450	[number]	k4	450
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
hrubá	hrubá	k1gFnSc1	hrubá
(	(	kIx(	(
<g/>
krupice	krupice	k1gFnSc1	krupice
<g/>
)	)	kIx)	)
T	T	kA	T
512	[number]	k4	512
-	-	kIx~	-
Pšeničná	pšeničné	k1gNnPc4	pšeničné
pekařská	pekařský	k2eAgNnPc4d1	pekařské
speciál	speciál	k1gInSc4	speciál
T	T	kA	T
530	[number]	k4	530
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
hladká	hladkat	k5eAaImIp3nS	hladkat
světlá	světlý	k2eAgFnSc1d1	světlá
-	-	kIx~	-
pekařská	pekařský	k2eAgFnSc1d1	Pekařská
speciál	speciál	k1gInSc1	speciál
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
značení	značení	k1gNnSc1	značení
00	[number]	k4	00
Extra	extra	k6eAd1	extra
<g/>
)	)	kIx)	)
T	T	kA	T
550	[number]	k4	550
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
polohrubá	polohrubý	k2eAgFnSc1d1	polohrubá
světlá	světlý	k2eAgFnSc1d1	světlá
T	T	kA	T
650	[number]	k4	650
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
hladká	hladkat	k5eAaImIp3nS	hladkat
polosvětlá	polosvětlý	k2eAgFnSc1d1	polosvětlý
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pečení	pečení	k1gNnSc4	pečení
chleba	chléb	k1gInSc2	chléb
jsou	být	k5eAaImIp3nP	být
vhodnější	vhodný	k2eAgFnPc4d2	vhodnější
mouky	mouka	k1gFnPc4	mouka
s	s	k7c7	s
T	T	kA	T
<g/>
650	[number]	k4	650
<g/>
.	.	kIx.	.
</s>
<s>
T	T	kA	T
700	[number]	k4	700
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
,	,	kIx,	,
chlebová	chlebový	k2eAgFnSc1d1	chlebová
T	T	kA	T
1000	[number]	k4	1000
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
hladká	hladkat	k5eAaImIp3nS	hladkat
tmavá	tmavý	k2eAgFnSc1d1	tmavá
(	(	kIx(	(
<g/>
chlebová	chlebový	k2eAgFnSc1d1	chlebová
<g/>
)	)	kIx)	)
T	T	kA	T
1050	[number]	k4	1050
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
chlebová	chlebový	k2eAgFnSc1d1	chlebová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
tmavá	tmavý	k2eAgFnSc1d1	tmavá
T	T	kA	T
1150	[number]	k4	1150
-	-	kIx~	-
Chlebová	chlebový	k2eAgFnSc1d1	chlebová
mouka	mouka	k1gFnSc1	mouka
T	T	kA	T
1800	[number]	k4	1800
-	-	kIx~	-
Pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
celozrnná	celozrnný	k2eAgFnSc1d1	celozrnná
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
<g/>
;	;	kIx,	;
celozrnná	celozrnný	k2eAgFnSc1d1	celozrnná
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
Žitné	žitná	k1gFnSc2	žitná
mouky	mouka	k1gFnSc2	mouka
T	T	kA	T
960	[number]	k4	960
Žitná	žitný	k2eAgFnSc1d1	Žitná
chlebová	chlebový	k2eAgFnSc1d1	chlebová
T	T	kA	T
1700	[number]	k4	1700
Žitná	žitný	k2eAgFnSc1d1	Žitná
celozrnná	celozrnný	k2eAgFnSc1d1	celozrnná
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mouka	mouka	k1gFnSc1	mouka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mouka	mouka	k1gFnSc1	mouka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Zdravá-kuchařka	Zdraváuchařka	k1gFnSc1	Zdravá-kuchařka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Celozrnná	celozrnný	k2eAgFnSc1d1	celozrnná
mouka	mouka	k1gFnSc1	mouka
<g/>
:	:	kIx,	:
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
špalda	špalda	k1gFnSc1	špalda
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
</s>
