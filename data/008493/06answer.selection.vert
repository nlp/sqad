<s>
Údolí	údolí	k1gNnSc1	údolí
včel	včela	k1gFnPc2	včela
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
s	s	k7c7	s
psychologickými	psychologický	k2eAgInPc7d1	psychologický
prvky	prvek	k1gInPc7	prvek
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Körnera	Körner	k1gMnSc2	Körner
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
a	a	k8xC	a
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
Františka	František	k1gMnSc2	František
Vláčila	vláčit	k5eAaImAgFnS	vláčit
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Čepkem	Čepek	k1gMnSc7	Čepek
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
