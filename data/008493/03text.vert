<p>
<s>
Údolí	údolí	k1gNnSc1	údolí
včel	včela	k1gFnPc2	včela
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
s	s	k7c7	s
psychologickými	psychologický	k2eAgInPc7d1	psychologický
prvky	prvek	k1gInPc7	prvek
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Körnera	Körner	k1gMnSc2	Körner
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
a	a	k8xC	a
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
Františka	František	k1gMnSc2	František
Vláčila	vláčit	k5eAaImAgFnS	vláčit
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Čepkem	Čepek	k1gMnSc7	Čepek
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
Ondřej	Ondřej	k1gMnSc1	Ondřej
z	z	k7c2	z
Vlkova	Vlkův	k2eAgInSc2d1	Vlkův
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
matky	matka	k1gFnSc2	matka
nalézá	nalézat	k5eAaImIp3nS	nalézat
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
samotě	samota	k1gFnSc6	samota
lesa	les	k1gInSc2	les
mezi	mezi	k7c7	mezi
včelími	včelí	k2eAgInPc7d1	včelí
úly	úl	k1gInPc7	úl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
znovu	znovu	k6eAd1	znovu
žení	ženit	k5eAaImIp3nS	ženit
s	s	k7c7	s
mladičkou	mladičký	k2eAgFnSc7d1	mladičká
dívkou	dívka	k1gFnSc7	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
to	ten	k3xDgNnSc4	ten
nese	nést	k5eAaImIp3nS	nést
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Odváží	odvážet	k5eAaImIp3nS	odvážet
se	se	k3xPyFc4	se
přijít	přijít	k5eAaPmF	přijít
až	až	k9	až
na	na	k7c4	na
svatební	svatební	k2eAgFnSc4d1	svatební
hostinu	hostina	k1gFnSc4	hostina
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
maceše	macecha	k1gFnSc3	macecha
jako	jako	k8xS	jako
dar	dar	k1gInSc4	dar
květinový	květinový	k2eAgInSc1d1	květinový
koš	koš	k1gInSc1	koš
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
květy	květ	k1gInPc7	květ
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
ukryti	ukrýt	k5eAaPmNgMnP	ukrýt
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nevěstu	nevěsta	k1gFnSc4	nevěsta
vyděsí	vyděsit	k5eAaPmIp3nP	vyděsit
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
Ondřej	Ondřej	k1gMnSc1	Ondřej
udělal	udělat	k5eAaPmAgMnS	udělat
schválně	schválně	k6eAd1	schválně
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
vzteku	vztek	k1gInSc6	vztek
jím	on	k3xPp3gInSc7	on
praští	praštit	k5eAaPmIp3nS	praštit
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ondřej	Ondřej	k1gMnSc1	Ondřej
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
si	se	k3xPyFc3	se
však	však	k9	však
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
zasažen	zasažen	k2eAgInSc1d1	zasažen
strachem	strach	k1gInSc7	strach
o	o	k7c4	o
život	život	k1gInSc4	život
jediného	jediný	k2eAgNnSc2d1	jediné
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
modlitbě	modlitba	k1gFnSc6	modlitba
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
Ondřejova	Ondřejův	k2eAgInSc2d1	Ondřejův
života	život	k1gInSc2	život
jej	on	k3xPp3gMnSc4	on
přislíbí	přislíbit	k5eAaPmIp3nS	přislíbit
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
kapitole	kapitola	k1gFnSc6	kapitola
již	již	k9	již
Ondřej	Ondřej	k1gMnSc1	Ondřej
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
Pruska	Prusko	k1gNnSc2	Prusko
do	do	k7c2	do
křižáckého	křižácký	k2eAgInSc2d1	křižácký
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
do	do	k7c2	do
Stummu	Stumm	k1gInSc2	Stumm
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
ujímá	ujímat	k5eAaImIp3nS	ujímat
Armin	Armin	k2eAgInSc1d1	Armin
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Heide	Heid	k1gInSc5	Heid
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
zbožný	zbožný	k2eAgMnSc1d1	zbožný
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
a	a	k8xC	a
asketický	asketický	k2eAgMnSc1d1	asketický
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
víra	víra	k1gFnSc1	víra
místy	místy	k6eAd1	místy
přerůstá	přerůstat	k5eAaImIp3nS	přerůstat
ve	v	k7c4	v
fanatismus	fanatismus	k1gInSc4	fanatismus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
Ondřej	Ondřej	k1gMnSc1	Ondřej
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
na	na	k7c4	na
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
pevný	pevný	k2eAgInSc4d1	pevný
běh	běh	k1gInSc4	běh
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Zná	znát	k5eAaImIp3nS	znát
jen	jen	k9	jen
chlad	chlad	k1gInSc4	chlad
řádového	řádový	k2eAgInSc2d1	řádový
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
chlad	chlad	k1gInSc1	chlad
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
srdcí	srdce	k1gNnPc2	srdce
kolem	kolem	k7c2	kolem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
střetnutí	střetnutí	k1gNnSc1	střetnutí
s	s	k7c7	s
vroucí	vroucí	k2eAgFnSc7d1	vroucí
touhou	touha	k1gFnSc7	touha
a	a	k8xC	a
emocí	emoce	k1gFnSc7	emoce
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
osudným	osudný	k2eAgInSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
ze	z	k7c2	z
Stummu	Stumm	k1gInSc2	Stumm
uteče	utéct	k5eAaPmIp3nS	utéct
jeden	jeden	k4xCgMnSc1	jeden
s	s	k7c7	s
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
Rotgier	Rotgira	k1gFnPc2	Rotgira
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
útěk	útěk	k1gInSc1	útěk
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rytíře	rytíř	k1gMnSc4	rytíř
nepřípustný	přípustný	k2eNgInSc1d1	nepřípustný
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Armina	Armino	k1gNnSc2	Armino
vydávají	vydávat	k5eAaImIp3nP	vydávat
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
stopě	stopa	k1gFnSc6	stopa
<g/>
,	,	kIx,	,
štvou	štvát	k5eAaImIp3nP	štvát
ho	on	k3xPp3gInSc4	on
jako	jako	k9	jako
divou	divý	k2eAgFnSc4d1	divá
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Podivnou	podivný	k2eAgFnSc4d1	podivná
náhodou	náhodou	k6eAd1	náhodou
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
objeví	objevit	k5eAaPmIp3nS	objevit
právě	právě	k6eAd1	právě
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Rotgier	Rotgier	k1gInSc1	Rotgier
se	se	k3xPyFc4	se
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
nesnaží	snažit	k5eNaImIp3nP	snažit
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
sedí	sedit	k5eAaImIp3nP	sedit
v	v	k7c6	v
trávě	tráva	k1gFnSc6	tráva
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
domově	domov	k1gInSc6	domov
<g/>
,	,	kIx,	,
o	o	k7c6	o
životě	život	k1gInSc6	život
<g/>
...	...	k?	...
Zbožně	zbožně	k6eAd1	zbožně
hledí	hledět	k5eAaImIp3nS	hledět
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
řádového	řádový	k2eAgNnSc2d1	řádové
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
nadosah	nadosah	k6eAd1	nadosah
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
mu	on	k3xPp3gNnSc3	on
Ondřej	Ondřej	k1gMnSc1	Ondřej
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
nebrání	bránit	k5eNaImIp3nS	bránit
v	v	k7c6	v
útěku	útěk	k1gInSc6	útěk
<g/>
,	,	kIx,	,
omráčí	omráčit	k5eAaPmIp3nP	omráčit
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
utíká	utíkat	k5eAaImIp3nS	utíkat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
se	se	k3xPyFc4	se
však	však	k9	však
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajat	zajat	k2eAgMnSc1d1	zajat
a	a	k8xC	a
odveden	odveden	k2eAgMnSc1d1	odveden
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nakonec	nakonec	k6eAd1	nakonec
předhozen	předhodit	k5eAaPmNgInS	předhodit
zdivočelým	zdivočelý	k2eAgMnSc7d1	zdivočelý
psům	pes	k1gMnPc3	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
začíná	začínat	k5eAaImIp3nS	začínat
půst	půst	k1gInSc4	půst
a	a	k8xC	a
řádoví	řádový	k2eAgMnPc1d1	řádový
bratři	bratr	k1gMnPc1	bratr
jsou	být	k5eAaImIp3nP	být
uzamčeni	uzamčen	k2eAgMnPc1d1	uzamčen
do	do	k7c2	do
cel	clo	k1gNnPc2	clo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbožném	zbožný	k2eAgNnSc6d1	zbožné
rozjímání	rozjímání	k1gNnSc6	rozjímání
klíčí	klíčit	k5eAaImIp3nS	klíčit
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgNnSc6d1	Ondřejovo
srdci	srdce	k1gNnSc6	srdce
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
.	.	kIx.	.
</s>
<s>
Touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
teplu	teplo	k1gNnSc6	teplo
a	a	k8xC	a
po	po	k7c6	po
bzukotu	bzukot	k1gInSc6	bzukot
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
neslyšel	slyšet	k5eNaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
půstu	půst	k1gInSc2	půst
odhalí	odhalit	k5eAaPmIp3nS	odhalit
Armin	Armin	k1gInSc1	Armin
jeho	jeho	k3xOp3gFnSc4	jeho
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Stíhá	stíhat	k5eAaImIp3nS	stíhat
ho	on	k3xPp3gMnSc4	on
nemilosrdně	milosrdně	k6eNd1	milosrdně
až	až	k9	až
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
neohlíží	ohlížet	k5eNaImIp3nS	ohlížet
se	se	k3xPyFc4	se
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
<s>
Propuká	propukat	k5eAaImIp3nS	propukat
jeho	jeho	k3xOp3gNnSc1	jeho
fanatismus	fanatismus	k1gInSc1	fanatismus
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
strach	strach	k1gInSc4	strach
z	z	k7c2	z
čehokoliv	cokoliv	k3yInSc2	cokoliv
pohanského	pohanský	k2eAgNnSc2d1	pohanské
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
vidí	vidět	k5eAaImIp3nS	vidět
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
zatím	zatím	k6eAd1	zatím
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyhublý	vyhublý	k2eAgMnSc1d1	vyhublý
a	a	k8xC	a
oblečený	oblečený	k2eAgMnSc1d1	oblečený
jen	jen	k9	jen
v	v	k7c6	v
cárech	cár	k1gInPc6	cár
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
uhlíře	uhlíř	k1gMnPc4	uhlíř
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ty	k3xPp2nSc3	ty
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
ho	on	k3xPp3gMnSc4	on
zbít	zbít	k5eAaPmF	zbít
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
však	však	k9	však
brání	bránit	k5eAaImIp3nS	bránit
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
ošklivě	ošklivě	k6eAd1	ošklivě
poraní	poranit	k5eAaPmIp3nP	poranit
<g/>
.	.	kIx.	.
</s>
<s>
Získá	získat	k5eAaPmIp3nS	získat
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
respekt	respekt	k1gInSc4	respekt
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
přivedli	přivést	k5eAaPmAgMnP	přivést
koně	kůň	k1gMnPc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
domů	dům	k1gInPc2	dům
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Náhoda	náhoda	k1gFnSc1	náhoda
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc4	jejich
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
neznámém	známý	k2eNgMnSc6d1	neznámý
bojovníkovi	bojovník	k1gMnSc6	bojovník
vyslechne	vyslechnout	k5eAaPmIp3nS	vyslechnout
i	i	k9	i
Armin	Armin	k1gInSc1	Armin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
uhlíři	uhlíř	k1gMnPc1	uhlíř
vracejí	vracet	k5eAaImIp3nP	vracet
s	s	k7c7	s
koněm	kůň	k1gMnSc7	kůň
k	k	k7c3	k
Ondřejovi	Ondřej	k1gMnSc3	Ondřej
<g/>
,	,	kIx,	,
smluví	smluvit	k5eAaPmIp3nS	smluvit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přesile	přesila	k1gFnSc6	přesila
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
boje	boj	k1gInSc2	boj
se	se	k3xPyFc4	se
zapojí	zapojit	k5eAaPmIp3nS	zapojit
i	i	k9	i
Armin	Armin	k1gInSc4	Armin
a	a	k8xC	a
Ondřeje	Ondřej	k1gMnSc2	Ondřej
ochrání	ochránit	k5eAaPmIp3nS	ochránit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
odvádí	odvádět	k5eAaImIp3nS	odvádět
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
nebrání	bránit	k5eNaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
smířený	smířený	k2eAgMnSc1d1	smířený
a	a	k8xC	a
otupělý	otupělý	k2eAgMnSc1d1	otupělý
a	a	k8xC	a
Armina	Armina	k1gFnSc1	Armina
jeho	on	k3xPp3gNnSc2	on
chování	chování	k1gNnSc2	chování
ukolébá	ukolébat	k5eAaPmIp3nS	ukolébat
k	k	k7c3	k
nepozornosti	nepozornost	k1gFnSc3	nepozornost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
napít	napít	k5eAaBmF	napít
ze	z	k7c2	z
studánky	studánka	k1gFnSc2	studánka
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
jej	on	k3xPp3gMnSc4	on
udeří	udeřit	k5eAaPmIp3nP	udeřit
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
jeho	on	k3xPp3gMnSc4	on
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
ujíždí	ujíždět	k5eAaImIp3nS	ujíždět
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
do	do	k7c2	do
Vlkova	Vlkův	k2eAgNnSc2d1	Vlkovo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
si	se	k3xPyFc3	se
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Armina	Armina	k1gMnSc1	Armina
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trpí	trpět	k5eAaImIp3nP	trpět
neústalým	ústalý	k2eNgInSc7d1	ústalý
strachem	strach	k1gInSc7	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
rytíř	rytíř	k1gMnSc1	rytíř
nikdy	nikdy	k6eAd1	nikdy
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
nepřestane	přestat	k5eNaPmIp3nS	přestat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
(	(	kIx(	(
<g/>
vypíchl	vypíchnout	k5eAaPmAgMnS	vypíchnout
si	se	k3xPyFc3	se
oko	oko	k1gNnSc4	oko
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
a	a	k8xC	a
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
roztrhali	roztrhat	k5eAaPmAgMnP	roztrhat
vlastní	vlastní	k2eAgMnPc1d1	vlastní
psi	pes	k1gMnPc1	pes
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
zpustošil	zpustošit	k5eAaPmAgMnS	zpustošit
celý	celý	k2eAgInSc4d1	celý
statek	statek	k1gInSc4	statek
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
Ondřeje	Ondřej	k1gMnSc2	Ondřej
vykoupit	vykoupit	k5eAaPmF	vykoupit
zpět	zpět	k6eAd1	zpět
(	(	kIx(	(
<g/>
chtěl	chtít	k5eAaImAgMnS	chtít
ulevit	ulevit	k5eAaPmF	ulevit
svému	svůj	k3xOyFgNnSc3	svůj
svědomí	svědomí	k1gNnSc3	svědomí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
teď	teď	k6eAd1	teď
spravuje	spravovat	k5eAaImIp3nS	spravovat
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Lenora	Lenora	k1gFnSc1	Lenora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Ondřejova	Ondřejův	k2eAgMnSc2d1	Ondřejův
otce	otec	k1gMnSc2	otec
mnoho	mnoho	k6eAd1	mnoho
vytrpěla	vytrpět	k5eAaPmAgFnS	vytrpět
<g/>
.	.	kIx.	.
</s>
<s>
Bil	bít	k5eAaImAgMnS	bít
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
zavíral	zavírat	k5eAaImAgMnS	zavírat
k	k	k7c3	k
psům	pes	k1gMnPc3	pes
<g/>
.	.	kIx.	.
</s>
<s>
Nenáviděl	nenávidět	k5eAaImAgMnS	nenávidět
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
nedala	dát	k5eNaPmAgFnS	dát
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
přičítal	přičítat	k5eAaImAgMnS	přičítat
jí	jíst	k5eAaImIp3nS	jíst
k	k	k7c3	k
vině	vina	k1gFnSc3	vina
i	i	k8xC	i
odchod	odchod	k1gInSc4	odchod
Ondřejův	Ondřejův	k2eAgInSc4d1	Ondřejův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
coby	coby	k?	coby
pán	pán	k1gMnSc1	pán
Vlkova	Vlkův	k2eAgInSc2d1	Vlkův
<g/>
.	.	kIx.	.
</s>
<s>
Zakládá	zakládat	k5eAaImIp3nS	zakládat
novou	nový	k2eAgFnSc4d1	nová
psí	psí	k2eAgFnSc4d1	psí
smečku	smečka	k1gFnSc4	smečka
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
otcovou	otcův	k2eAgFnSc7d1	otcova
nechala	nechat	k5eAaPmAgFnS	nechat
paní	paní	k1gFnSc1	paní
domu	dům	k1gInSc2	dům
vyhubit	vyhubit	k5eAaPmF	vyhubit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přísný	přísný	k2eAgMnSc1d1	přísný
a	a	k8xC	a
s	s	k7c7	s
Lenorou	Lenora	k1gFnSc7	Lenora
zpočátku	zpočátku	k6eAd1	zpočátku
nezachází	zacházet	k5eNaImIp3nS	zacházet
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
přestává	přestávat	k5eAaImIp3nS	přestávat
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
macechu	macecha	k1gFnSc4	macecha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začne	začít	k5eAaPmIp3nS	začít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vidět	vidět	k5eAaImF	vidět
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
léta	léto	k1gNnPc4	léto
trpěla	trpět	k5eAaImAgFnS	trpět
vedle	vedle	k7c2	vedle
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
mladá	mladý	k2eAgFnSc1d1	mladá
a	a	k8xC	a
hezká	hezký	k2eAgFnSc1d1	hezká
<g/>
.	.	kIx.	.
</s>
<s>
Chtějí	chtít	k5eAaImIp3nP	chtít
se	se	k3xPyFc4	se
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
svatby	svatba	k1gFnSc2	svatba
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
lesa	les	k1gInSc2	les
a	a	k8xC	a
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
skalou	skala	k1gFnSc7	skala
mezi	mezi	k7c7	mezi
bažinami	bažina	k1gFnPc7	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
objeví	objevit	k5eAaPmIp3nS	objevit
Ondřej	Ondřej	k1gMnSc1	Ondřej
v	v	k7c6	v
kusu	kus	k1gInSc6	kus
dřeva	dřevo	k1gNnSc2	dřevo
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
řádový	řádový	k2eAgInSc1d1	řádový
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
obavy	obava	k1gFnPc1	obava
se	se	k3xPyFc4	se
naplňují	naplňovat	k5eAaImIp3nP	naplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
téměř	téměř	k6eAd1	téměř
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
Armin	Armin	k2eAgInSc1d1	Armin
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Heide	Heid	k1gInSc5	Heid
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
blízko	blízko	k6eAd1	blízko
Vlkova	Vlkův	k2eAgFnSc1d1	Vlkova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
svatební	svatební	k2eAgFnSc6d1	svatební
hostině	hostina	k1gFnSc6	hostina
se	se	k3xPyFc4	se
Armin	Armin	k1gMnSc1	Armin
skutečně	skutečně	k6eAd1	skutečně
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Beze	beze	k7c2	beze
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
i	i	k9	i
bez	bez	k7c2	bez
zlého	zlý	k2eAgInSc2d1	zlý
úmyslu	úmysl	k1gInSc2	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
ztrhaně	ztrhaně	k6eAd1	ztrhaně
a	a	k8xC	a
omšele	omšele	k6eAd1	omšele
<g/>
.	.	kIx.	.
</s>
<s>
Svatebčané	svatebčan	k1gMnPc1	svatebčan
jej	on	k3xPp3gMnSc4	on
zvou	zvát	k5eAaImIp3nP	zvát
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
,	,	kIx,	,
vida	vida	k?	vida
jeho	jeho	k3xOp3gInSc4	jeho
poklid	poklid	k1gInSc4	poklid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
návštěvou	návštěva	k1gFnSc7	návštěva
téměř	téměř	k6eAd1	téměř
potěšen	potěšit	k5eAaPmNgMnS	potěšit
<g/>
.	.	kIx.	.
</s>
<s>
Vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
veselil	veselit	k5eAaImAgMnS	veselit
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Armin	Armin	k2eAgMnSc1d1	Armin
věnuje	věnovat	k5eAaImIp3nS	věnovat
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
písek	písek	k1gInSc4	písek
z	z	k7c2	z
akkonské	akkonský	k2eAgFnSc2d1	akkonský
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
kdysi	kdysi	k6eAd1	kdysi
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
a	a	k8xC	a
který	který	k3yRgInSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
moc	moc	k6eAd1	moc
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
Lenora	Lenora	k1gFnSc1	Lenora
jej	on	k3xPp3gMnSc4	on
vysype	vysypat	k5eAaPmIp3nS	vysypat
do	do	k7c2	do
sukně	sukně	k1gFnSc2	sukně
a	a	k8xC	a
smete	smést	k5eAaPmIp3nS	smést
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
hostina	hostina	k1gFnSc1	hostina
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
nadchází	nadcházet	k5eAaImIp3nS	nadcházet
svatební	svatební	k2eAgFnSc1d1	svatební
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
Lenora	Lenora	k1gFnSc1	Lenora
sama	sám	k3xTgFnSc1	sám
v	v	k7c6	v
ložnici	ložnice	k1gFnSc6	ložnice
očekává	očekávat	k5eAaImIp3nS	očekávat
manželův	manželův	k2eAgInSc1d1	manželův
příchod	příchod	k1gInSc1	příchod
<g/>
.	.	kIx.	.
</s>
<s>
Dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
Ondřej	Ondřej	k1gMnSc1	Ondřej
se	se	k3xPyFc4	se
však	však	k9	však
objeví	objevit	k5eAaPmIp3nS	objevit
Armin	Armin	k2eAgMnSc1d1	Armin
a	a	k8xC	a
podřízne	podříznout	k5eAaPmIp3nS	podříznout
jí	on	k3xPp3gFnSc3	on
hrdlo	hrdnout	k5eAaImAgNnS	hrdnout
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
znesvěcení	znesvěcení	k1gNnSc4	znesvěcení
jeho	jeho	k3xOp3gInSc2	jeho
daru	dar	k1gInSc2	dar
<g/>
?	?	kIx.	?
</s>
<s>
Pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
Ondřeje	Ondřej	k1gMnSc2	Ondřej
<g/>
?	?	kIx.	?
</s>
<s>
S	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenora	Lenora	k1gFnSc1	Lenora
Armina	Armin	k1gMnSc2	Armin
na	na	k7c4	na
Ondřejův	Ondřejův	k2eAgInSc4d1	Ondřejův
popud	popud	k1gInSc4	popud
políbila	políbit	k5eAaPmAgFnS	políbit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řádových	řádový	k2eAgNnPc2d1	řádové
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Arminova	Arminův	k2eAgFnSc1d1	Arminova
mysl	mysl	k1gFnSc1	mysl
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
zastřená	zastřený	k2eAgFnSc1d1	zastřená
a	a	k8xC	a
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
vyvést	vyvést	k5eAaPmF	vyvést
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
bít	bít	k5eAaImF	bít
a	a	k8xC	a
ponižovat	ponižovat	k5eAaImF	ponižovat
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
svého	svůj	k3xOyFgMnSc4	svůj
dávného	dávný	k2eAgMnSc4d1	dávný
přítele	přítel	k1gMnSc4	přítel
v	v	k7c6	v
zoufalém	zoufalý	k2eAgInSc6d1	zoufalý
vzteku	vztek	k1gInSc6	vztek
nechá	nechat	k5eAaPmIp3nS	nechat
roztrhat	roztrhat	k5eAaPmF	roztrhat
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ale	ale	k9	ale
už	už	k6eAd1	už
těžko	těžko	k6eAd1	těžko
může	moct	k5eAaImIp3nS	moct
najít	najít	k5eAaPmF	najít
spočinutí	spočinutí	k1gNnSc4	spočinutí
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
A	a	k9	a
proto	proto	k8xC	proto
odchází	odcházet	k5eAaImIp3nS	odcházet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
</p>
