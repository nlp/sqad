<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
De	De	k?	De
Nachtwacht	Nachtwacht	k1gInSc1	Nachtwacht
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
holandského	holandský	k2eAgMnSc2d1	holandský
malíře	malíř	k1gMnSc2	malíř
Rembrandta	Rembrandta	k1gFnSc1	Rembrandta
van	van	k1gInSc1	van
Rijn	Rijn	k1gInSc1	Rijn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
tohoto	tento	k3xDgInSc2	tento
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
Posádka	posádka	k1gFnSc1	posádka
kapitána	kapitán	k1gMnSc2	kapitán
Franse	Frans	k1gMnSc2	Frans
Banninga	Banning	k1gMnSc2	Banning
Cocqa	Cocqus	k1gMnSc2	Cocqus
a	a	k8xC	a
Willema	Willem	k1gMnSc2	Willem
van	vana	k1gFnPc2	vana
Ruytenburcha	Ruytenburcha	k1gMnSc1	Ruytenburcha
avšak	avšak	k8xC	avšak
všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
především	především	k9	především
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
(	(	kIx(	(
<g/>
363	[number]	k4	363
x	x	k?	x
437	[number]	k4	437
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynikajícím	vynikající	k2eAgNnSc7d1	vynikající
vystižením	vystižení	k1gNnSc7	vystižení
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
stínu	stín	k1gInSc2	stín
a	a	k8xC	a
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
znázorněním	znázornění	k1gNnSc7	znázornění
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Rembrandt	Rembrandt	k1gInSc1	Rembrandt
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
dokončil	dokončit	k5eAaPmAgInS	dokončit
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholu	vrchol	k1gInSc2	vrchol
tzv.	tzv.	kA	tzv.
holandského	holandský	k2eAgInSc2d1	holandský
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
posádku	posádka	k1gFnSc4	posádka
vedenou	vedený	k2eAgFnSc4d1	vedená
kapitánem	kapitán	k1gMnSc7	kapitán
Fransem	Frans	k1gMnSc7	Frans
Banningaem	Banninga	k1gMnSc7	Banninga
Cocqem	Cocq	k1gInSc7	Cocq
(	(	kIx(	(
<g/>
oblečen	oblečen	k2eAgMnSc1d1	oblečen
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
šerpou	šerpa	k1gFnSc7	šerpa
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
poručíkem	poručík	k1gMnSc7	poručík
Willemem	Willem	k1gInSc7	Willem
van	vana	k1gFnPc2	vana
Ruytenburchem	Ruytenburch	k1gInSc7	Ruytenburch
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
nejvýraznější	výrazný	k2eAgInPc1d3	nejvýraznější
prvky	prvek	k1gInPc1	prvek
obrazu	obraz	k1gInSc2	obraz
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgMnPc1	dva
důstojníci	důstojník	k1gMnPc1	důstojník
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
dívka	dívka	k1gFnSc1	dívka
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
nese	nést	k5eAaImIp3nS	nést
vlajkonoš	vlajkonoš	k1gMnSc1	vlajkonoš
Jan	Jan	k1gMnSc1	Jan
Visscher	Visschra	k1gFnPc2	Visschra
Cornelissen	Cornelissna	k1gFnPc2	Cornelissna
posádkový	posádkový	k2eAgInSc4d1	posádkový
prapor	prapor	k1gInSc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
kuře	kuře	k1gNnSc4	kuře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
má	mít	k5eAaImIp3nS	mít
dívka	dívka	k1gFnSc1	dívka
upnuto	upnut	k2eAgNnSc1d1	upnuto
opaskem	opasek	k1gInSc7	opasek
k	k	k7c3	k
oděvu	oděv	k1gInSc3	oděv
<g/>
,	,	kIx,	,
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
poraženého	poražený	k2eAgMnSc4d1	poražený
protivníka	protivník	k1gMnSc4	protivník
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
pařát	pařát	k1gInSc1	pařát
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
vojáků	voják	k1gMnPc2	voják
bojujících	bojující	k2eAgMnPc2d1	bojující
s	s	k7c7	s
arkebuzou	arkebuza	k1gFnSc7	arkebuza
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
jejích	její	k3xOp3gInPc2	její
šatů	šat	k1gInPc2	šat
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
helmě	helma	k1gFnSc6	helma
dubový	dubový	k2eAgInSc4d1	dubový
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
motiv	motiv	k1gInSc1	motiv
vojáků	voják	k1gMnPc2	voják
bojujících	bojující	k2eAgMnPc2d1	bojující
s	s	k7c7	s
arkebuzou	arkebuza	k1gFnSc7	arkebuza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgInSc1d1	známý
název	název	k1gInSc1	název
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pod	pod	k7c7	pod
mylným	mylný	k2eAgInSc7d1	mylný
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
noční	noční	k2eAgFnSc4d1	noční
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dojem	dojem	k1gInSc1	dojem
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
tmavým	tmavý	k2eAgInSc7d1	tmavý
nátěrem	nátěr	k1gInSc7	nátěr
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
dodatečně	dodatečně	k6eAd1	dodatečně
natřen	natřít	k5eAaPmNgInS	natřít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
restaurování	restaurování	k1gNnSc6	restaurování
tento	tento	k3xDgInSc1	tento
nátěr	nátěr	k1gInSc1	nátěr
odstraněn	odstranit	k5eAaPmNgInS	odstranit
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
obraz	obraz	k1gInSc1	obraz
opět	opět	k6eAd1	opět
původní	původní	k2eAgInPc4d1	původní
odstíny	odstín	k1gInPc4	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tedy	tedy	k9	tedy
obraz	obraz	k1gInSc1	obraz
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
scénu	scéna	k1gFnSc4	scéna
za	za	k7c2	za
jasného	jasný	k2eAgInSc2d1	jasný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1715	[number]	k4	1715
při	při	k7c6	při
umisťování	umisťování	k1gNnSc6	umisťování
obrazu	obraz	k1gInSc2	obraz
na	na	k7c6	na
amsterodamské	amsterodamský	k2eAgFnSc6d1	Amsterodamská
radnici	radnice	k1gFnSc6	radnice
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
oříznut	oříznut	k2eAgInSc1d1	oříznut
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
čtyřech	čtyři	k4xCgFnPc6	čtyři
stranách	strana	k1gFnPc6	strana
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
umístění	umístění	k1gNnSc2	umístění
obrazu	obraz	k1gInSc2	obraz
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
sloupy	sloup	k1gInPc4	sloup
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nevešel	vejít	k5eNaPmAgMnS	vejít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
po	po	k7c6	po
oříznutí	oříznutí	k1gNnSc6	oříznutí
zmizely	zmizet	k5eAaPmAgInP	zmizet
dvě	dva	k4xCgFnPc4	dva
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
straně	strana	k1gFnSc6	strana
vrchol	vrchol	k1gInSc4	vrchol
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
balustráda	balustráda	k1gFnSc1	balustráda
a	a	k8xC	a
okraj	okraj	k1gInSc4	okraj
schodu	schod	k1gInSc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
balustráda	balustráda	k1gFnSc1	balustráda
a	a	k8xC	a
schod	schod	k1gInSc1	schod
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
obrazu	obraz	k1gInSc6	obraz
byly	být	k5eAaImAgFnP	být
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
vystižení	vystižení	k1gNnSc3	vystižení
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Kopie	kopie	k1gFnSc1	kopie
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
malíře	malíř	k1gMnSc2	malíř
Gerrita	Gerrita	k1gFnSc1	Gerrita
Ludense	Ludense	k1gFnSc1	Ludense
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
původně	původně	k6eAd1	původně
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Kopie	kopie	k1gFnSc1	kopie
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
necitlivých	citlivý	k2eNgInPc2d1	necitlivý
zásahů	zásah	k1gInPc2	zásah
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k6eAd1	také
obětí	oběť	k1gFnSc7	oběť
vandalismu	vandalismus	k1gInSc2	vandalismus
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Nůž	nůž	k1gInSc1	nůž
patřil	patřit	k5eAaImAgInS	patřit
jednomu	jeden	k4xCgNnSc3	jeden
učiteli	učitel	k1gMnSc6	učitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
potom	potom	k6eAd1	potom
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
ďábel	ďábel	k1gMnSc1	ďábel
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
Franz	Franz	k1gMnSc1	Franz
Banning	Banning	k1gInSc4	Banning
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gNnPc3	jeho
vyobrazením	vyobrazení	k1gNnPc3	vyobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
byl	být	k5eAaImAgInS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
úmyslně	úmyslně	k6eAd1	úmyslně
polit	polit	k2eAgInSc4d1	polit
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
okamžitě	okamžitě	k6eAd1	okamžitě
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
poléván	polévat	k5eAaImNgInS	polévat
množstvím	množství	k1gNnSc7	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
však	však	k9	však
pronikla	proniknout	k5eAaPmAgFnS	proniknout
jen	jen	k9	jen
svrchním	svrchní	k2eAgInSc7d1	svrchní
ochranným	ochranný	k2eAgInSc7d1	ochranný
lakem	lak	k1gInSc7	lak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
těchto	tento	k3xDgFnPc6	tento
aktech	akta	k1gNnPc6	akta
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
úspěšně	úspěšně	k6eAd1	úspěšně
restaurován	restaurovat	k5eAaBmNgInS	restaurovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgInPc1d1	alternativní
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
doplňující	doplňující	k2eAgInSc4d1	doplňující
náhled	náhled	k1gInSc4	náhled
<g/>
,	,	kIx,	,
k	k	k7c3	k
výše	vysoce	k6eAd2	vysoce
uvedenému	uvedený	k2eAgInSc3d1	uvedený
textu	text	k1gInSc3	text
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
výklad	výklad	k1gInSc4	výklad
samotného	samotný	k2eAgInSc2d1	samotný
obrazu	obraz	k1gInSc2	obraz
nabízí	nabízet	k5eAaImIp3nS	nabízet
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Petera	Peter	k1gMnSc2	Peter
Greenawaye	Greenaway	k1gMnSc2	Greenaway
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
hraný	hraný	k2eAgInSc1d1	hraný
dokument	dokument	k1gInSc1	dokument
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
evropskou	evropský	k2eAgFnSc7d1	Evropská
koprodukcí	koprodukce	k1gFnSc7	koprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
Martin	Martin	k1gMnSc1	Martin
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
Rembrandtovy	Rembrandtův	k2eAgFnSc2d1	Rembrandtova
sochy	socha	k1gFnSc2	socha
na	na	k7c4	na
Rembrandtplein	Rembrandtplein	k1gInSc4	Rembrandtplein
(	(	kIx(	(
<g/>
Rembrandtovo	Rembrandtův	k2eAgNnSc4d1	Rembrandtovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
)	)	kIx)	)
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
jeho	jeho	k3xOp3gInPc2	jeho
400	[number]	k4	400
narozenin	narozeniny	k1gFnPc2	narozeniny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
dle	dle	k7c2	dle
obrazu	obraz	k1gInSc2	obraz
rozestaveno	rozestaven	k2eAgNnSc1d1	rozestaveno
bronzové	bronzový	k2eAgNnSc1d1	bronzové
sousoší	sousoší	k1gNnSc1	sousoší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Night	Night	k1gMnSc1	Night
Watch	Watch	k1gMnSc1	Watch
na	na	k7c4	na
WebMuseum	WebMuseum	k1gNnSc4	WebMuseum
</s>
</p>
<p>
<s>
Night	Night	k2eAgInSc1d1	Night
Watch	Watch	k1gInSc1	Watch
v	v	k7c4	v
Rijksmuseum	Rijksmuseum	k1gInSc4	Rijksmuseum
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
</s>
</p>
<p>
<s>
Rembrandt	Rembrandt	k1gInSc1	Rembrandt
and	and	k?	and
the	the	k?	the
Night	Night	k1gMnSc1	Night
Watch	Watch	k1gMnSc1	Watch
</s>
</p>
<p>
<s>
The	The	k?	The
Night	Night	k2eAgInSc1d1	Night
Watch	Watch	k1gInSc1	Watch
by	by	kYmCp3nS	by
Rembrandt	Rembrandt	k1gInSc4	Rembrandt
van	vana	k1gFnPc2	vana
Rijn	Rijn	k1gMnSc1	Rijn
</s>
</p>
<p>
<s>
Night	Night	k2eAgInSc1d1	Night
Watch	Watch	k1gInSc1	Watch
Replica	Replicum	k1gNnSc2	Replicum
v	v	k7c4	v
Canajoharie	Canajoharie	k1gFnPc4	Canajoharie
Library	Librara	k1gFnSc2	Librara
and	and	k?	and
Art	Art	k1gFnSc1	Art
Gallery	Galler	k1gInPc4	Galler
</s>
</p>
