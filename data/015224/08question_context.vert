<s desamb="1">
Oscar	Oscar	k1gInSc1
Wilde	Wild	k1gInSc5
ho	on	k3xPp3gInSc4
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
„	„	k?
<g/>
novým	nový	k2eAgMnSc7d1
Kristem	Kristus	k1gMnSc7
přicházejícím	přicházející	k2eAgMnSc7d1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Kníže	kníže	k1gMnSc1
Petr	Petr	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
П	П	k?
А	А	k?
К	К	k?
<g/>
;	;	kIx,
27	#num#	k4
<g/>
.	.	kIx.
listopadujul	listopadujout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1842	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1921	#num#	k4
<g/>
,	,	kIx,
Dmitrov	Dmitrov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
revolucionář	revolucionář	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
geograf	geograf	k1gMnSc1
a	a	k8xC
geolog	geolog	k1gMnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
předních	přední	k2eAgMnPc2d1
ruských	ruský	k2eAgMnPc2d1
anarchistů	anarchista	k1gMnPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
zastánců	zastánce	k1gMnPc2
a	a	k8xC
teoretiků	teoretik	k1gMnPc2
anarchokomunismu	anarchokomunismus	k1gInSc2
<g/>
.	.	kIx.
</s>