<p>
<s>
Sir	sir	k1gMnSc1	sir
Edmund	Edmund	k1gMnSc1	Edmund
Percival	Percival	k1gMnSc1	Percival
Hillary	Hillara	k1gFnSc2	Hillara
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Tuakau	Tuakaus	k1gInSc2	Tuakaus
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Auckland	Auckland	k1gInSc1	Auckland
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
horolezec	horolezec	k1gMnSc1	horolezec
a	a	k8xC	a
průzkumník	průzkumník	k1gMnSc1	průzkumník
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Mount	Mounta	k1gFnPc2	Mounta
Everest	Everest	k1gInSc1	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholku	vrcholek	k1gInSc2	vrcholek
této	tento	k3xDgFnSc2	tento
hory	hora	k1gFnSc2	hora
vysoké	vysoký	k2eAgInPc1d1	vysoký
8848	[number]	k4	8848
metrů	metr	k1gInPc2	metr
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
se	s	k7c7	s
Šerpou	šerpa	k1gFnSc7	šerpa
Tenzingem	Tenzing	k1gInSc7	Tenzing
Norgayem	Norgayem	k1gInSc1	Norgayem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
držitelem	držitel	k1gMnSc7	držitel
následujících	následující	k2eAgNnPc2d1	následující
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
:	:	kIx,	:
Podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
Řád	řád	k1gInSc1	řád
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
až	až	k8xS	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
16	[number]	k4	16
let	léto	k1gNnPc2	léto
neviděl	vidět	k5eNaImAgMnS	vidět
hory	hora	k1gFnPc4	hora
ani	ani	k8xC	ani
sníh	sníh	k1gInSc4	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdě	tvrdě	k6eAd1	tvrdě
dřel	dřít	k5eAaImAgMnS	dřít
ve	v	k7c6	v
včelařském	včelařský	k2eAgInSc6d1	včelařský
podniku	podnik	k1gInSc6	podnik
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
byl	být	k5eAaImAgMnS	být
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
včelař	včelař	k1gMnSc1	včelař
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
školního	školní	k2eAgInSc2d1	školní
výletu	výlet	k1gInSc2	výlet
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nežil	žít	k5eNaImAgMnS	žít
pro	pro	k7c4	pro
takřka	takřka	k6eAd1	takřka
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
už	už	k6eAd1	už
měl	mít	k5eAaImAgMnS	mít
značné	značný	k2eAgFnPc4d1	značná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
také	také	k9	také
měl	mít	k5eAaImAgMnS	mít
slezeny	slezen	k2eAgFnPc1d1	slezena
všechny	všechen	k3xTgFnPc1	všechen
vysoké	vysoký	k2eAgFnPc1d1	vysoká
hory	hora	k1gFnPc1	hora
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k6eAd1	také
potkal	potkat	k5eAaPmAgMnS	potkat
horského	horský	k2eAgMnSc4d1	horský
průvodce	průvodce	k1gMnSc4	průvodce
Harryho	Harry	k1gMnSc4	Harry
Ayrese	Ayresa	k1gFnSc6	Ayresa
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yQgMnSc7	který
po	po	k7c4	po
3	[number]	k4	3
sezóny	sezóna	k1gFnSc2	sezóna
zlézal	zlézat	k5eAaImAgMnS	zlézat
horské	horský	k2eAgInPc4d1	horský
vrcholy	vrchol	k1gInPc4	vrchol
a	a	k8xC	a
který	který	k3yQgInSc1	který
ho	on	k3xPp3gInSc4	on
mnohému	mnohé	k1gNnSc3	mnohé
naučil	naučit	k5eAaPmAgInS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Georgem	Georg	k1gMnSc7	Georg
Lowem	Low	k1gMnSc7	Low
začali	začít	k5eAaPmAgMnP	začít
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
Himálaj	Himálaj	k1gFnSc4	Himálaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
výprava	výprava	k1gFnSc1	výprava
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
britské	britský	k2eAgFnSc2d1	britská
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
vedené	vedený	k2eAgFnSc2d1	vedená
velezkušeným	velezkušený	k2eAgMnSc7d1	velezkušený
Ericem	Erice	k1gMnSc7	Erice
Shiptonem	Shipton	k1gInSc7	Shipton
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
nebylo	být	k5eNaImAgNnS	být
dobýt	dobýt	k5eAaPmF	dobýt
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najít	najít	k5eAaPmF	najít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přes	přes	k7c4	přes
Nepál	Nepál	k1gInSc4	Nepál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
čerstvě	čerstvě	k6eAd1	čerstvě
otevřel	otevřít	k5eAaPmAgInS	otevřít
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
se	se	k3xPyFc4	se
nezdařila	zdařit	k5eNaPmAgFnS	zdařit
na	na	k7c4	na
100	[number]	k4	100
%	%	kIx~	%
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
však	však	k9	však
nalezena	nalézt	k5eAaBmNgFnS	nalézt
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pokusná	pokusný	k2eAgFnSc1d1	pokusná
výprava	výprava	k1gFnSc1	výprava
do	do	k7c2	do
Himálaje	Himálaj	k1gFnSc2	Himálaj
==	==	k?	==
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
nepálská	nepálský	k2eAgFnSc1d1	nepálská
vláda	vláda	k1gFnSc1	vláda
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1952	[number]	k4	1952
udělila	udělit	k5eAaPmAgFnS	udělit
povolení	povolení	k1gNnSc4	povolení
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
jen	jen	k9	jen
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
výpravě	výprava	k1gFnSc6	výprava
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
Královská	královský	k2eAgFnSc1d1	královská
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
Klub	klub	k1gInSc1	klub
alpinistů	alpinista	k1gMnPc2	alpinista
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypraví	vypravit	k5eAaPmIp3nS	vypravit
alespoň	alespoň	k9	alespoň
pokusnou	pokusný	k2eAgFnSc4d1	pokusná
výpravu	výprava	k1gFnSc4	výprava
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Výpravu	výprava	k1gFnSc4	výprava
vedl	vést	k5eAaImAgInS	vést
znovu	znovu	k6eAd1	znovu
Eric	Eric	k1gInSc1	Eric
Shipton	Shipton	k1gInSc1	Shipton
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Švýcarům	Švýcar	k1gMnPc3	Švýcar
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc1	Everest
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
všechna	všechen	k3xTgFnSc1	všechen
píle	píle	k1gFnSc1	píle
<g/>
,	,	kIx,	,
výstroj	výstroj	k1gFnSc1	výstroj
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
pokus	pokus	k1gInSc4	pokus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
1953	[number]	k4	1953
==	==	k?	==
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
výprava	výprava	k1gFnSc1	výprava
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
dorazila	dorazit	k5eAaPmAgFnS	dorazit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
aklimatizačním	aklimatizační	k2eAgNnSc6d1	aklimatizační
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1953	[number]	k4	1953
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
k	k	k7c3	k
úbočí	úbočí	k1gNnSc3	úbočí
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zdlouhavém	zdlouhavý	k2eAgNnSc6d1	zdlouhavé
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
namáhavém	namáhavý	k2eAgInSc6d1	namáhavý
postupu	postup	k1gInSc6	postup
vzhůru	vzhůru	k6eAd1	vzhůru
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
vydala	vydat	k5eAaPmAgFnS	vydat
1	[number]	k4	1
<g/>
.	.	kIx.	.
skupina	skupina	k1gFnSc1	skupina
Charles	Charles	k1gMnSc1	Charles
Evans	Evans	k1gInSc1	Evans
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
Bourdillon	Bourdillon	k1gInSc1	Bourdillon
na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
špatně	špatně	k6eAd1	špatně
fungujícímu	fungující	k2eAgInSc3d1	fungující
kyslíkovému	kyslíkový	k2eAgInSc3d1	kyslíkový
přístroji	přístroj	k1gInSc3	přístroj
Charlese	Charles	k1gMnSc2	Charles
Evanse	Evans	k1gMnSc2	Evans
dostali	dostat	k5eAaPmAgMnP	dostat
se	s	k7c7	s
"	"	kIx"	"
<g/>
jen	jen	k6eAd1	jen
<g/>
"	"	kIx"	"
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
8	[number]	k4	8
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Přesto	přesto	k8xC	přesto
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
člověk	člověk	k1gMnSc1	člověk
před	před	k7c7	před
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
tedy	tedy	k9	tedy
směřovalo	směřovat	k5eAaImAgNnS	směřovat
k	k	k7c3	k
2	[number]	k4	2
<g/>
.	.	kIx.	.
pokusu	pokus	k1gInSc2	pokus
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
právě	právě	k9	právě
Ed	Ed	k1gMnSc1	Ed
Hillary	Hillara	k1gFnSc2	Hillara
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
zkušený	zkušený	k2eAgMnSc1d1	zkušený
šerpa	šerpa	k1gFnSc1	šerpa
Tenzing	Tenzing	k1gInSc4	Tenzing
Norgay	Norgaa	k1gFnSc2	Norgaa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
neúspěšné	úspěšný	k2eNgFnPc4d1	neúspěšná
švýcarské	švýcarský	k2eAgFnPc4d1	švýcarská
výpravy	výprava	k1gFnPc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
začal	začít	k5eAaPmAgInS	začít
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
VIII	VIII	kA	VIII
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k6eAd1	až
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
IX	IX	kA	IX
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přečkali	přečkat	k5eAaPmAgMnP	přečkat
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
<g/>
,	,	kIx,	,
obtěžkáni	obtěžkat	k5eAaPmNgMnP	obtěžkat
kyslíkovými	kyslíkový	k2eAgInPc7d1	kyslíkový
přístroji	přístroj	k1gInPc7	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
pořídil	pořídit	k5eAaPmAgMnS	pořídit
několik	několik	k4yIc4	několik
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k8xC	i
slavnou	slavný	k2eAgFnSc4d1	slavná
fotografii	fotografia	k1gFnSc4	fotografia
Tenzinga	Tenzing	k1gMnSc2	Tenzing
držícího	držící	k2eAgNnSc2d1	držící
cepín	cepín	k1gInSc4	cepín
s	s	k7c7	s
nepálskou	nepálský	k2eAgFnSc7d1	nepálská
a	a	k8xC	a
britskou	britský	k2eAgFnSc7d1	britská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
fotografie	fotografie	k1gFnSc1	fotografie
Edmunda	Edmund	k1gMnSc2	Edmund
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Hillaryho	Hillary	k1gMnSc2	Hillary
<g/>
)	)	kIx)	)
Tenzing	Tenzing	k1gInSc1	Tenzing
neuměl	umět	k5eNaImAgInS	umět
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Everestu	Everest	k1gInSc2	Everest
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
neztrácejíce	ztrácet	k5eNaImSgFnP	ztrácet
drahocenný	drahocenný	k2eAgInSc4d1	drahocenný
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
strávili	strávit	k5eAaPmAgMnP	strávit
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
zpátky	zpátky	k6eAd1	zpátky
k	k	k7c3	k
táboru	tábor	k1gInSc3	tábor
IX	IX	kA	IX
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
až	až	k9	až
k	k	k7c3	k
táboru	tábor	k1gInSc3	tábor
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
domluveno	domluven	k2eAgNnSc1d1	domluveno
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
pokus	pokus	k1gInSc1	pokus
zdaří	zdařit	k5eAaPmIp3nS	zdařit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
VI	VI	kA	VI
udělá	udělat	k5eAaPmIp3nS	udělat
ze	z	k7c2	z
spacích	spací	k2eAgInPc2d1	spací
pytlů	pytel	k1gInPc2	pytel
písmeno	písmeno	k1gNnSc1	písmeno
T	T	kA	T
jako	jako	k8xS	jako
TOP	topit	k5eAaImRp2nS	topit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hlavní	hlavní	k2eAgFnSc1d1	hlavní
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
kotlině	kotlina	k1gFnSc6	kotlina
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
IV	IV	kA	IV
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokus	pokus	k1gInSc1	pokus
zdařil	zdařit	k5eAaPmAgInS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jelikož	jelikož	k8xS	jelikož
bylo	být	k5eAaImAgNnS	být
větrno	větrno	k6eAd1	větrno
a	a	k8xC	a
vánice	vánice	k1gFnSc1	vánice
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
nápadu	nápad	k1gInSc2	nápad
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
zpátky	zpátky	k6eAd1	zpátky
k	k	k7c3	k
táboru	tábor	k1gInSc3	tábor
IV	Iva	k1gFnPc2	Iva
a	a	k8xC	a
následně	následně	k6eAd1	následně
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
po	po	k7c6	po
zdolání	zdolání	k1gNnSc6	zdolání
Mount	Mount	k1gInSc4	Mount
Everestu	Everest	k1gInSc2	Everest
==	==	k?	==
</s>
</p>
<p>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
se	se	k3xPyFc4	se
také	také	k9	také
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
výpravy	výprava	k1gFnPc4	výprava
na	na	k7c4	na
Severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Neilem	Neil	k1gMnSc7	Neil
Armstrongem	Armstrong	k1gMnSc7	Armstrong
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
Jižní	jižní	k2eAgInSc4d1	jižní
pól	pól	k1gInSc4	pól
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stanul	stanout	k5eAaPmAgMnS	stanout
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
třech	tři	k4xCgInPc6	tři
pólech	pól	k1gInPc6	pól
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Three	Thre	k1gMnSc2	Thre
Poles	Poles	k1gMnSc1	Poles
Challenge	Challeng	k1gMnSc2	Challeng
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
podařilo	podařit	k5eAaPmAgNnS	podařit
také	také	k6eAd1	také
jeho	jeho	k3xOp3gMnSc3	jeho
synovi	syn	k1gMnSc3	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HILLARY	HILLARY	kA	HILLARY
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
High	High	k1gMnSc1	High
Adventure	Adventur	k1gMnSc5	Adventur
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Hodder	Hodder	k1gInSc1	Hodder
and	and	k?	and
Stoughton	Stoughton	k1gInSc1	Stoughton
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HILLARY	HILLARY	kA	HILLARY
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
High	High	k1gInSc1	High
Adventure	Adventur	k1gMnSc5	Adventur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Vladimír	Vladimír	k1gMnSc1	Vladimír
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
slovo	slovo	k1gNnSc1	slovo
-	-	kIx~	-
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
215	[number]	k4	215
s.	s.	k?	s.
56	[number]	k4	56
<g/>
/	/	kIx~	/
<g/>
VIII-	VIII-	k1gFnSc2	VIII-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HUNT	hunt	k1gInSc1	hunt
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Ascent	Ascent	k1gInSc1	Ascent
of	of	k?	of
Everest	Everest	k1gInSc1	Everest
<g/>
,	,	kIx,	,
Mountaineers	Mountaineers	k1gInSc1	Mountaineers
<g/>
'	'	kIx"	'
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
STN	STN	kA	STN
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ULLMAN	ULLMAN	kA	ULLMAN
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Ramsey	Ramsea	k1gFnSc2	Ramsea
<g/>
;	;	kIx,	;
NORGAY	NORGAY	kA	NORGAY
<g/>
,	,	kIx,	,
Tenzing	Tenzing	k1gInSc1	Tenzing
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
STN	STN	kA	STN
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
</s>
</p>
<p>
<s>
HERRLIGKOFFER	HERRLIGKOFFER	kA	HERRLIGKOFFER
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
dobývání	dobývání	k1gNnSc2	dobývání
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
203	[number]	k4	203
s.	s.	k?	s.
</s>
</p>
<p>
<s>
HILLARY	HILLARY	kA	HILLARY
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
oceánu	oceán	k1gInSc2	oceán
k	k	k7c3	k
oblakům	oblak	k1gInPc3	oblak
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
From	From	k1gMnSc1	From
The	The	k1gMnSc1	The
Ocean	Ocean	k1gMnSc1	Ocean
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Sky	Sky	k1gMnSc1	Sky
<g/>
,	,	kIx,	,
Hodder	Hodder	k1gMnSc1	Hodder
and	and	k?	and
Stoughton	Stoughton	k1gInSc1	Stoughton
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
264	[number]	k4	264
<g/>
+	+	kIx~	+
<g/>
24	[number]	k4	24
s.	s.	k?	s.
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
74	[number]	k4	74
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HILLARY	HILLARY	kA	HILLARY
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
neriskuje	riskovat	k5eNaBmIp3nS	riskovat
<g/>
,	,	kIx,	,
nevyhraje	vyhrát	k5eNaPmIp3nS	vyhrát
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Nothing	Nothing	k1gInSc1	Nothing
Venture	Ventur	k1gMnSc5	Ventur
<g/>
,	,	kIx,	,
Nothing	Nothing	k1gInSc1	Nothing
Win	Win	k1gFnSc1	Win
<g/>
,	,	kIx,	,
Hodder	Hodder	k1gInSc1	Hodder
and	and	k?	and
Stoughton	Stoughton	k1gInSc1	Stoughton
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Gita	Gita	k1gFnSc1	Gita
Zbavitelová	Zbavitelová	k1gFnSc1	Zbavitelová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
423	[number]	k4	423
<g/>
+	+	kIx~	+
<g/>
24	[number]	k4	24
s.	s.	k?	s.
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MESSNER	MESSNER	kA	MESSNER
<g/>
,	,	kIx,	,
Reinhold	Reinhold	k1gInSc1	Reinhold
<g/>
.	.	kIx.	.
</s>
<s>
Everest	Everest	k1gInSc4	Everest
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Everest	Everest	k1gInSc1	Everest
-	-	kIx~	-
Expedition	Expedition	k1gInSc1	Expedition
zum	zum	k?	zum
Endpunkt	Endpunkt	k1gInSc1	Endpunkt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Karlach	Karlach	k1gMnSc1	Karlach
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
168	[number]	k4	168
<g/>
+	+	kIx~	+
<g/>
32	[number]	k4	32
s.	s.	k?	s.
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DIEŠKA	DIEŠKA	kA	DIEŠKA
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Horolezectvo	Horolezectvo	k1gNnSc4	Horolezectvo
zblízka	zblízka	k6eAd1	zblízka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
304	[number]	k4	304
s.	s.	k?	s.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DIEŠKA	DIEŠKA	kA	DIEŠKA
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
;	;	kIx,	;
ŠIRL	ŠIRL	kA	ŠIRL
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Horolezectví	horolezectví	k1gNnSc4	horolezectví
zblízka	zblízka	k6eAd1	zblízka
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Horolezectvo	Horolezectvo	k1gNnSc1	Horolezectvo
zblízka	zblízka	k6eAd1	zblízka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Širl	Širl	k1gMnSc1	Širl
<g/>
;	;	kIx,	;
lektoroval	lektorovat	k5eAaImAgMnS	lektorovat
Jiří	Jiří	k1gMnSc1	Jiří
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
444	[number]	k4	444
s.	s.	k?	s.
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
187	[number]	k4	187
<g/>
-	-	kIx~	-
<g/>
189,190	[number]	k4	189,190
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DIEŠKA	DIEŠKA	kA	DIEŠKA
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
;	;	kIx,	;
ŠIRL	ŠIRL	kA	ŠIRL
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
HOROLEZECTVO	HOROLEZECTVO	kA	HOROLEZECTVO
encyklopédia	encyklopédium	k1gNnSc2	encyklopédium
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
254	[number]	k4	254
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7096	[number]	k4	7096
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HILLARY	HILLARY	kA	HILLARY
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
A	a	k9	a
View	View	k1gMnSc1	View
from	from	k1gMnSc1	from
the	the	k?	the
Summit	summit	k1gInSc1	summit
<g/>
,	,	kIx,	,
nevydaný	vydaný	k2eNgInSc1d1	nevydaný
originál	originál	k1gInSc1	originál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Fantys	Fantys	k1gInSc1	Fantys
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	rybka	k1gFnSc1	rybka
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
301	[number]	k4	301
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MESSNER	MESSNER	kA	MESSNER
<g/>
,	,	kIx,	,
Reinhold	Reinhold	k1gInSc1	Reinhold
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
264	[number]	k4	264
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8020406700	[number]	k4	8020406700
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnPc4	Hillara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
</s>
</p>
<p>
<s>
Ohlédnutí	ohlédnutí	k1gNnSc1	ohlédnutí
za	za	k7c7	za
legendou	legenda	k1gFnSc7	legenda
jménem	jméno	k1gNnSc7	jméno
Sir	sir	k1gMnSc1	sir
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
</s>
</p>
