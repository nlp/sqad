<s>
O.	O.	kA
k.	k.	k?
</s>
<s>
O.	O.	kA
k.	k.	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
neformální	formální	k2eNgFnSc6d1
podobě	podoba	k1gFnSc6
též	tenž	k3xDgFnSc2
O.K.	O.K.	k1gFnSc2
<g/>
,	,	kIx,
OK	oka	k1gFnPc2
<g/>
,	,	kIx,
ok	oka	k1gFnPc2
či	či	k8xC
okay	okaa	k1gFnSc2
(	(	kIx(
<g/>
oukej	oukej	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výraz	výraz	k1gInSc1
celosvětově	celosvětově	k6eAd1
užívaný	užívaný	k2eAgMnSc1d1
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
souhlasu	souhlas	k1gInSc2
<g/>
,	,	kIx,
znamenající	znamenající	k2eAgInSc1d1
„	„	k?
<g/>
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
dobře	dobře	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
ujednáno	ujednán	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
angličtině	angličtina	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
i	i	k9
jako	jako	k8xC,k8xS
sloveso	sloveso	k1gNnSc1
ve	v	k7c6
významu	význam	k1gInSc6
souhlasit	souhlasit	k5eAaImF
<g/>
,	,	kIx,
tolerovat	tolerovat	k5eAaImF
<g/>
,	,	kIx,
respektovat	respektovat	k5eAaImF
<g/>
,	,	kIx,
kvitovat	kvitovat	k5eAaBmF
<g/>
,	,	kIx,
zaujmout	zaujmout	k5eAaPmF
neutrální	neutrální	k2eAgInSc4d1
nebo	nebo	k8xC
nekonfliktní	konfliktní	k2eNgInSc4d1
postoj	postoj	k1gInSc4
vůči	vůči	k7c3
něčemu	něco	k3yInSc3
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
použít	použít	k5eAaPmF
i	i	k9
jako	jako	k9
pozitivní	pozitivní	k2eAgNnSc4d1
hodnocení	hodnocení	k1gNnSc4
(	(	kIx(
<g/>
nikoli	nikoli	k9
však	však	k9
excelentní	excelentní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
teorií	teorie	k1gFnPc2
vysvětlujících	vysvětlující	k2eAgFnPc2d1
etymologii	etymologie	k1gFnSc4
tohoto	tento	k3xDgNnSc2
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
u	u	k7c2
žádné	žádný	k3yNgFnSc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
nelze	lze	k6eNd1
s	s	k7c7
jistotou	jistota	k1gFnSc7
určit	určit	k5eAaPmF
její	její	k3xOp3gFnSc4
ověřitelnost	ověřitelnost	k1gFnSc4
a	a	k8xC
pravost	pravost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
</s>
<s>
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
dvojí	dvojí	k4xRgFnPc1
výslovnosti	výslovnost	k1gFnPc1
výrazu	výraz	k1gInSc2
o.	o.	k?
k.	k.	k?
<g/>
,	,	kIx,
či	či	k8xC
ok	oka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
výslovnost	výslovnost	k1gFnSc4
ó	ó	k0
ká	ká	k0
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgNnSc1d1
z	z	k7c2
české	český	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
písmen	písmeno	k1gNnPc2
O	o	k7c4
a	a	k8xC
K	k	k7c3
<g/>
;	;	kIx,
a	a	k8xC
výslovnost	výslovnost	k1gFnSc1
oukej	oukej	k1gInSc1
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
anglické	anglický	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
těchto	tento	k3xDgNnPc2
písmen	písmeno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
druhá	druhý	k4xOgFnSc1
výslovnost	výslovnost	k1gFnSc1
se	se	k3xPyFc4
též	též	k9
váže	vázat	k5eAaImIp3nS
k	k	k7c3
neformálnímu	formální	k2eNgInSc3d1
výrazu	výraz	k1gInSc3
okay	okaa	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
užíván	užívat	k5eAaImNgInS
také	také	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
Allen	Allen	k1gMnSc1
Walker	Walker	k1gMnSc1
Read	Read	k1gMnSc1
článek	článek	k1gInSc4
The	The	k1gMnSc3
Folklore	folklor	k1gInSc5
of	of	k?
"	"	kIx"
<g/>
O.	O.	kA
K.	K.	kA
<g/>
"	"	kIx"
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgMnPc4d1
se	se	k3xPyFc4
možnými	možný	k2eAgInPc7d1
etymologickými	etymologický	k2eAgInPc7d1
původy	původ	k1gInPc7
tohoto	tento	k3xDgInSc2
výrazu	výraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvíce	nejvíce	k6eAd1,k6eAd3
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
následující	následující	k2eAgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
Z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
Ola	Ola	k1gFnSc1
Kala	kala	k1gFnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
vše	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
)	)	kIx)
užívané	užívaný	k2eAgNnSc1d1
řeckými	řecký	k2eAgMnPc7d1
učiteli	učitel	k1gMnPc7
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
též	též	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
řečtí	řecký	k2eAgMnPc1d1
námořníci	námořník	k1gMnPc1
užívali	užívat	k5eAaImAgMnP
tento	tento	k3xDgInSc4
výraz	výraz	k1gInSc4
v	v	k7c6
amerických	americký	k2eAgInPc6d1
přístavech	přístav	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
lodích	loď	k1gFnPc6
byl	být	k5eAaImAgInS
nápis	nápis	k1gInSc1
O.K.	O.K.	k1gMnPc2
sloužící	sloužící	k2eAgMnSc1d1
jako	jako	k9
označení	označení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
v	v	k7c6
pořádku	pořádek	k1gInSc6
a	a	k8xC
připravena	připraven	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
užívání	užívání	k1gNnSc1
sahá	sahat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
špatně	špatně	k6eAd1
resp.	resp.	kA
foneticky	foneticky	k6eAd1
zapsaných	zapsaný	k2eAgNnPc2d1
anglických	anglický	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
all	all	k?
correct	correct	k1gMnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
vše	všechen	k3xTgNnSc1
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
/	/	kIx~
<g/>
oll	oll	k?
korrect	korrect	k1gInSc1
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
známka	známka	k1gFnSc1
o	o	k7c6
tomto	tento	k3xDgInSc6
užití	užití	k1gNnSc6
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1839	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
přezdívky	přezdívka	k1gFnSc2
Old	Olda	k1gFnPc2
Kinderhook	Kinderhook	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Starý	starý	k2eAgInSc1d1
Kinderhook	Kinderhook	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
užíval	užívat	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Martin	Martin	k1gMnSc1
Van	van	k1gInSc4
Buren	Buren	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
přezdívka	přezdívka	k1gFnSc1
odrážela	odrážet	k5eAaImAgFnS
místo	místo	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
narození	narození	k1gNnSc2
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
Kinderhook	Kinderhook	k1gInSc1
ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
indiánského	indiánský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
z	z	k7c2
jazykové	jazykový	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
muskogí	muskogí	k2eAgNnSc4d1
oke	oke	k1gNnSc4
<g/>
,	,	kIx,
či	či	k8xC
okeh	okeh	k1gMnSc1
(	(	kIx(
<g/>
ve	v	k7c6
významu	význam	k1gInSc6
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zaznamenané	zaznamenaný	k2eAgFnPc4d1
roku	rok	k1gInSc2
1812	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
slova	slovo	k1gNnSc2
afrického	africký	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
wolofštiny	wolofština	k1gFnSc2
waw-kay	waw-kaa	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
významu	význam	k1gInSc6
souhlasu	souhlas	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
z	z	k7c2
fráze	fráze	k1gFnSc2
z	z	k7c2
mandajských	mandajský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
o	o	k7c6
ke	k	k7c3
(	(	kIx(
<g/>
ve	v	k7c6
významu	význam	k1gInSc6
zajisté	zajisté	k9
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
slangu	slang	k1gInSc6
se	se	k3xPyFc4
výrazu	výraz	k1gInSc3
OK	oka	k1gFnPc2
používalo	používat	k5eAaImAgNnS
při	při	k7c6
rádiovém	rádiový	k2eAgInSc6d1
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
zkrácení	zkrácení	k1gNnSc4
výrazu	výraz	k1gInSc2
řečeného	řečený	k2eAgInSc2d1
při	při	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
bojových	bojový	k2eAgFnPc2d1
misí	mise	k1gFnPc2
<g/>
:	:	kIx,
zero	zero	k1gMnSc1
killed	killed	k1gMnSc1
(	(	kIx(
<g/>
nula	nula	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
či	či	k8xC
žádní	žádný	k3yNgMnPc1
mrtví	mrtvý	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
v	v	k7c6
angličtině	angličtina	k1gFnSc6
do	do	k7c2
vysílačky	vysílačka	k1gFnSc2
nevyslovuje	vyslovovat	k5eNaImIp3nS
číslice	číslice	k1gFnSc1
nula	nula	k1gFnSc1
jako	jako	k8xS,k8xC
zero	zero	k6eAd1
[	[	kIx(
<g/>
zírou	zírou	k6eAd1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xS,k8xC
písmeno	písmeno	k1gNnSc1
o	o	k7c6
[	[	kIx(
<g/>
ou	ou	k0
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
konečné	konečný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
se	se	k3xPyFc4
proto	proto	k8xC
pro	pro	k7c4
rychlejší	rychlý	k2eAgFnSc4d2
a	a	k8xC
srozumitelnější	srozumitelný	k2eAgFnSc4d2
komunikaci	komunikace	k1gFnSc4
používalo	používat	k5eAaImAgNnS
místo	místo	k1gNnSc1
zero	zero	k6eAd1
killed	killed	k1gInSc4
pouze	pouze	k6eAd1
o	o	k7c4
killed	killed	k1gInSc4
a	a	k8xC
nebo	nebo	k8xC
jen	jen	k9
zkratka	zkratka	k1gFnSc1
OK	OK	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
List	lista	k1gFnPc2
of	of	k?
proposed	proposed	k1gMnSc1
etymologies	etymologies	k1gMnSc1
of	of	k?
OK	oko	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Okay	Okaa	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
v.	v.	k?
v.	v.	k?
i	i	k9
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
o.	o.	k?
k.	k.	k?
<g/>
.	.	kIx.
↑	↑	k?
http://dictionary.cambridge.org/dictionary/british/ok_8	http://dictionary.cambridge.org/dictionary/british/ok_8	k4
<g/>
↑	↑	k?
http://www.thefreedictionary.com/okayed	http://www.thefreedictionary.com/okayed	k1gMnSc1
<g/>
↑	↑	k?
Zdeněk	Zdeněk	k1gMnSc1
Kos	Kos	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Zkratky	zkratka	k1gFnPc1
<g/>
,	,	kIx,
značky	značka	k1gFnPc1
<g/>
,	,	kIx,
akronymy	akronym	k1gInPc1
<g/>
,	,	kIx,
Horizont	horizont	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1983	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
o.	o.	k?
k.	k.	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
navržených	navržený	k2eAgFnPc2d1
etymologií	etymologie	k1gFnPc2
slovního	slovní	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
OK	oko	k1gNnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
o.	o.	k?
k.	k.	k?
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
http://www.e-kniha.com/proc-ceska-letadla-maji-oznaceni-ok.html	http://www.e-kniha.com/proc-ceska-letadla-maji-oznaceni-ok.html	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4699958-9	4699958-9	k4
</s>
