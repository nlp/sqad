<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
v	v	k7c6
angličtině	angličtina	k1gFnSc6
do	do	k7c2
vysílačky	vysílačka	k1gFnSc2
nevyslovuje	vyslovovat	k5eNaImIp3nS
číslice	číslice	k1gFnSc1
nula	nula	k1gFnSc1
jako	jako	k8xS,k8xC
zero	zero	k6eAd1
[	[	kIx(
<g/>
zírou	zírou	k6eAd1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xS,k8xC
písmeno	písmeno	k1gNnSc1
o	o	k7c6
[	[	kIx(
<g/>
ou	ou	k0
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
konečné	konečný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
se	se	k3xPyFc4
proto	proto	k8xC
pro	pro	k7c4
rychlejší	rychlý	k2eAgFnSc4d2
a	a	k8xC
srozumitelnější	srozumitelný	k2eAgFnSc4d2
komunikaci	komunikace	k1gFnSc4
používalo	používat	k5eAaImAgNnS
místo	místo	k1gNnSc1
zero	zero	k6eAd1
killed	killed	k1gInSc4
pouze	pouze	k6eAd1
o	o	k7c4
killed	killed	k1gInSc4
a	a	k8xC
nebo	nebo	k8xC
jen	jen	k9
zkratka	zkratka	k1gFnSc1
OK	OK	kA
<g/>
.	.	kIx.
</s>