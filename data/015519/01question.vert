<s>
Jaký	jaký	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
zkrácený	zkrácený	k2eAgInSc1d1
výraz	výraz	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
říkal	říkat	k5eAaImAgInS
při	při	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
bojových	bojový	k2eAgFnPc2d1
misí	mise	k1gFnPc2
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
nebyli	být	k5eNaImAgMnP
žádní	žádný	k3yNgMnPc1
mrtví	mrtvý	k2eAgMnPc1d1
a	a	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
souhlasu	souhlas	k1gInSc2
<g/>
?	?	kIx.
</s>