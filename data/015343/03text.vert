<s>
NGC	NGC	kA
64	#num#	k4
</s>
<s>
NGC	NGC	kA
64	#num#	k4
Galaxie	galaxie	k1gFnSc2
NGC	NGC	kA
64	#num#	k4
<g/>
Pozorovací	pozorovací	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
spirální	spirální	k2eAgFnPc1d1
galaxie	galaxie	k1gFnPc1
-	-	kIx~
SBbc	SBbc	k1gFnSc1
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Lewis	Lewis	k1gInSc1
Swift	Swift	k2eAgInSc1d1
Datum	datum	k1gNnSc4
objevu	objev	k1gInSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1886	#num#	k4
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
h	h	k?
17	#num#	k4
<g/>
m	m	kA
30,3	30,3	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
-6	-6	k4
<g/>
°	°	k?
49	#num#	k4
<g/>
′	′	k?
29	#num#	k4
<g/>
″	″	k?
Souhvězdí	souhvězdí	k1gNnSc1
</s>
<s>
Velryba	velryba	k1gFnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
Cet	ceta	k1gFnPc2
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
13,2	13,2	k4
Úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
1,7	1,7	k4
<g/>
′	′	k?
×	×	k?
1,2	1,2	k4
<g/>
′	′	k?
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
329	#num#	k4
Mly	Mly	k1gFnSc1
(	(	kIx(
<g/>
101	#num#	k4
Mpc	Mpc	k1gFnPc2
<g/>
)	)	kIx)
Rudý	rudý	k2eAgInSc1d1
posuv	posuv	k1gInSc1
</s>
<s>
0,024	0,024	k4
685	#num#	k4
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
Poloměr	poloměr	k1gInSc1
</s>
<s>
~	~	kIx~
90	#num#	k4
000	#num#	k4
ly	ly	k?
Označení	označení	k1gNnSc1
v	v	k7c6
katalozích	katalog	k1gInPc6
New	New	k1gMnSc2
General	General	k1gMnSc2
Catalogue	Catalogu	k1gMnSc2
</s>
<s>
NGC	NGC	kA
64	#num#	k4
Principal	Principal	k1gFnSc2
Galaxies	Galaxiesa	k1gFnPc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
PGC	PGC	kA
1149	#num#	k4
Jiná	jiná	k1gFnSc1
označení	označení	k1gNnSc2
</s>
<s>
PGC	PGC	kA
1149	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NGC	NGC	kA
64	#num#	k4
je	být	k5eAaImIp3nS
spirální	spirální	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
vzdálená	vzdálený	k2eAgFnSc1d1
od	od	k7c2
nás	my	k3xPp1nPc2
zhruba	zhruba	k6eAd1
329	#num#	k4
milionů	milion	k4xCgInPc2
světelných	světelný	k2eAgInPc2d1
let	léto	k1gNnPc2
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Velryby	velryba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
NGC	NGC	kA
objektů	objekt	k1gInPc2
1-250	1-250	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
