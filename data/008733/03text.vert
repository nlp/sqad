<p>
<s>
Ostašov	Ostašov	k1gInSc1	Ostašov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ostaschow	Ostaschow	k1gMnSc2	Ostaschow
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Staňkovice	Staňkovice	k1gFnSc2	Staňkovice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
1	[number]	k4	1
km	km	kA	km
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Staňkovic	Staňkovice	k1gFnPc2	Staňkovice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
12	[number]	k4	12
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Trvale	trvale	k6eAd1	trvale
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
27	[number]	k4	27
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Ostašov	Ostašov	k1gInSc1	Ostašov
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Staňkovice	Staňkovice	k1gFnSc2	Staňkovice
u	u	k7c2	u
Uhlířských	uhlířský	k2eAgFnPc2d1	uhlířská
Janovic	Janovice	k1gFnPc2	Janovice
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
8,37	[number]	k4	8,37
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ostašov	Ostašovo	k1gNnPc2	Ostašovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
<g/>
753611	[number]	k4	753611
<g/>
)	)	kIx)	)
</s>
</p>
