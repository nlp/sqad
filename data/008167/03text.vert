<p>
<s>
Suity	suita	k1gFnPc1	suita
pro	pro	k7c4	pro
sólové	sólový	k2eAgNnSc4d1	sólové
violoncello	violoncello	k1gNnSc4	violoncello
německého	německý	k2eAgMnSc2d1	německý
skladatele	skladatel	k1gMnSc2	skladatel
Johanna	Johann	k1gMnSc2	Johann
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc4	cyklus
šesti	šest	k4xCc2	šest
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1720	[number]	k4	1720
v	v	k7c6	v
Köthenu	Köthen	k1gInSc6	Köthen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
suita	suita	k1gFnSc1	suita
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1007	[number]	k4	1007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
v	v	k7c6	v
tónině	tónina	k1gFnSc6	tónina
G	G	kA	G
dur	dur	k1gNnPc4	dur
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1008	[number]	k4	1008
<g/>
)	)	kIx)	)
v	v	k7c6	v
d	d	k?	d
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1009	[number]	k4	1009
<g/>
)	)	kIx)	)
v	v	k7c6	v
C	C	kA	C
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1010	[number]	k4	1010
<g/>
)	)	kIx)	)
v	v	k7c6	v
Es	es	k1gNnSc6	es
dur	dur	k1gNnSc2	dur
<g/>
,	,	kIx,	,
pátá	pátá	k1gFnSc1	pátá
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1011	[number]	k4	1011
<g/>
)	)	kIx)	)
v	v	k7c6	v
c	c	k0	c
moll	moll	k1gNnSc6	moll
a	a	k8xC	a
šestá	šestý	k4xOgFnSc1	šestý
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1012	[number]	k4	1012
<g/>
)	)	kIx)	)
v	v	k7c6	v
D	D	kA	D
dur	dur	k1gNnSc1	dur
<g/>
.	.	kIx.	.
</s>
<s>
Pátá	pátý	k4xOgFnSc1	pátý
suita	suita	k1gFnSc1	suita
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
pro	pro	k7c4	pro
loutnu	loutna	k1gFnSc4	loutna
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
995	[number]	k4	995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
standardní	standardní	k2eAgFnSc4d1	standardní
formu	forma	k1gFnSc4	forma
suity	suita	k1gFnSc2	suita
(	(	kIx(	(
<g/>
allemanda	allemanda	k1gFnSc1	allemanda
–	–	k?	–
courante	courant	k1gMnSc5	courant
–	–	k?	–
sarabanda	sarabanda	k1gFnSc1	sarabanda
–	–	k?	–
gigue	gigue	k1gInSc1	gigue
<g/>
)	)	kIx)	)
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	dílo	k1gNnSc6	dílo
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
úvodní	úvodní	k2eAgFnSc4d1	úvodní
předehru	předehra	k1gFnSc4	předehra
(	(	kIx(	(
<g/>
preludium	preludium	k1gNnSc4	preludium
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
mezi	mezi	k7c4	mezi
sarabandu	sarabanda	k1gFnSc4	sarabanda
a	a	k8xC	a
gigue	gigue	k1gInSc4	gigue
vkládal	vkládat	k5eAaImAgMnS	vkládat
ještě	ještě	k9	ještě
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
galanterii	galanterie	k1gFnSc4	galanterie
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc4d1	taneční
větu	věta	k1gFnSc4	věta
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
A	a	k8xC	a
B	B	kA	B
A.	A.	kA	A.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
suitách	suita	k1gFnPc6	suita
jsou	být	k5eAaImIp3nP	být
galanteriemi	galanterie	k1gFnPc7	galanterie
menuety	menueta	k1gFnSc2	menueta
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
dvou	dva	k4xCgNnPc6	dva
autor	autor	k1gMnSc1	autor
použil	použít	k5eAaPmAgMnS	použít
bourrée	bourrée	k1gNnSc4	bourrée
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
dvou	dva	k4xCgFnPc6	dva
suitách	suita	k1gFnPc6	suita
tvoří	tvořit	k5eAaImIp3nP	tvořit
galanterii	galanterie	k1gFnSc4	galanterie
gavoty	gavota	k1gFnSc2	gavota
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
Bachových	Bachův	k2eAgFnPc2d1	Bachova
violoncellových	violoncellový	k2eAgFnPc2d1	violoncellová
suit	suita	k1gFnPc2	suita
tedy	tedy	k9	tedy
vypadá	vypadat	k5eAaImIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prélude	Prélude	k6eAd1	Prélude
</s>
</p>
<p>
<s>
Allemande	Allemand	k1gInSc5	Allemand
</s>
</p>
<p>
<s>
Courante	Courant	k1gMnSc5	Courant
</s>
</p>
<p>
<s>
Sarabande	Saraband	k1gInSc5	Saraband
</s>
</p>
<p>
<s>
Galanterie	galanterie	k1gFnSc1	galanterie
I	i	k8xC	i
–	–	k?	–
Galanterie	galanterie	k1gFnSc2	galanterie
II	II	kA	II
–	–	k?	–
Galanterie	galanterie	k1gFnSc1	galanterie
I	i	k8xC	i
da	da	k?	da
capo	capa	k1gFnSc5	capa
</s>
</p>
<p>
<s>
GigueProvedení	GigueProvedení	k1gNnSc1	GigueProvedení
každé	každý	k3xTgFnSc2	každý
následující	následující	k2eAgFnSc2d1	následující
suity	suita	k1gFnSc2	suita
zabere	zabrat	k5eAaPmIp3nS	zabrat
víc	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
než	než	k8xS	než
předchozí	předchozí	k2eAgInPc4d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
nahrávce	nahrávka	k1gFnSc6	nahrávka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pořídil	pořídit	k5eAaPmAgMnS	pořídit
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schiff	Schiff	k1gMnSc1	Schiff
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
trvání	trvání	k1gNnSc1	trvání
suit	suita	k1gFnPc2	suita
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
I.	I.	kA	I.
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
V.	V.	kA	V.
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
VI	VI	kA	VI
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
123	[number]	k4	123
minut	minuta	k1gFnPc2	minuta
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Suity	suita	k1gFnPc1	suita
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
dobových	dobový	k2eAgInPc6d1	dobový
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Anny	Anna	k1gFnSc2	Anna
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Bachové	Bachová	k1gFnSc2	Bachová
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
od	od	k7c2	od
Bachových	Bachův	k2eAgMnPc2d1	Bachův
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
spíše	spíše	k9	spíše
za	za	k7c4	za
technicky	technicky	k6eAd1	technicky
náročnou	náročný	k2eAgFnSc4d1	náročná
instruktivní	instruktivní	k2eAgFnSc4d1	instruktivní
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
než	než	k8xS	než
za	za	k7c4	za
díla	dílo	k1gNnPc4	dílo
vhodná	vhodný	k2eAgNnPc4d1	vhodné
ke	k	k7c3	k
koncertnímu	koncertní	k2eAgNnSc3d1	koncertní
provedení	provedení	k1gNnSc3	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
změnil	změnit	k5eAaPmAgInS	změnit
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Pablo	Pablo	k1gNnSc1	Pablo
Casals	Casals	k1gInSc1	Casals
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
suity	suita	k1gFnPc1	suita
veřejně	veřejně	k6eAd1	veřejně
hrál	hrát	k5eAaImAgMnS	hrát
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pořídil	pořídit	k5eAaPmAgMnS	pořídit
i	i	k9	i
jejich	jejich	k3xOp3gFnSc4	jejich
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
koncertního	koncertní	k2eAgInSc2d1	koncertní
repertoáru	repertoár	k1gInSc2	repertoár
violoncellistů	violoncellista	k1gMnPc2	violoncellista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
hudby	hudba	k1gFnSc2	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
suita	suita	k1gFnSc1	suita
pro	pro	k7c4	pro
sólové	sólový	k2eAgNnSc4d1	sólové
violoncello	violoncello	k1gNnSc4	violoncello
(	(	kIx(	(
<g/>
BWV	BWV	kA	BWV
1007	[number]	k4	1007
<g/>
)	)	kIx)	)
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
Johna	John	k1gMnSc2	John
Michela	Michel	k1gMnSc2	Michel
Preludium	preludium	k1gNnSc1	preludium
-	-	kIx~	-
Allemanda	allemanda	k1gFnSc1	allemanda
-	-	kIx~	-
Courante	Courant	k1gMnSc5	Courant
-	-	kIx~	-
Sarabanda	sarabanda	k1gFnSc1	sarabanda
-	-	kIx~	-
Menuety	menuet	k1gInPc1	menuet
-	-	kIx~	-
Gigue	Gigue	k1gFnSc1	Gigue
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
