<s>
Suity	suita	k1gFnPc1	suita
pro	pro	k7c4	pro
sólové	sólový	k2eAgNnSc4d1	sólové
violoncello	violoncello	k1gNnSc4	violoncello
německého	německý	k2eAgMnSc2d1	německý
skladatele	skladatel	k1gMnSc2	skladatel
Johanna	Johann	k1gMnSc2	Johann
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc4	cyklus
šesti	šest	k4xCc2	šest
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1720	[number]	k4	1720
v	v	k7c6	v
Köthenu	Köthen	k1gInSc6	Köthen
<g/>
.	.	kIx.	.
</s>
