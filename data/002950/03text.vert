<s>
C	C	kA	C
je	být	k5eAaImIp3nS	být
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
počátkem	počátkem	k7c2	počátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
Ken	Ken	k1gMnSc1	Ken
Thompson	Thompson	k1gMnSc1	Thompson
a	a	k8xC	a
Dennis	Dennis	k1gFnSc1	Dennis
Ritchie	Ritchie	k1gFnSc2	Ritchie
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Unix	Unix	k1gInSc1	Unix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
nejčastější	častý	k2eAgFnSc7d3	nejčastější
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
systémového	systémový	k2eAgInSc2d1	systémový
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
i	i	k9	i
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
je	být	k5eAaImIp3nS	být
nízkoúrovňový	nízkoúrovňový	k2eAgInSc1d1	nízkoúrovňový
<g/>
,	,	kIx,	,
kompilovaný	kompilovaný	k2eAgInSc1d1	kompilovaný
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
minimalistický	minimalistický	k2eAgInSc1d1	minimalistický
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
mocný	mocný	k2eAgMnSc1d1	mocný
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
systémového	systémový	k2eAgNnSc2d1	systémové
programování	programování	k1gNnSc2	programování
(	(	kIx(	(
<g/>
ovladače	ovladač	k1gInSc2	ovladač
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
OS	OS	kA	OS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zbytek	zbytek	k1gInSc1	zbytek
lze	lze	k6eAd1	lze
dořešit	dořešit	k5eAaPmF	dořešit
tzv.	tzv.	kA	tzv.
inline	inlinout	k5eAaPmIp3nS	inlinout
assemblerem	assembler	k1gInSc7	assembler
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
metodou	metoda	k1gFnSc7	metoda
zápisu	zápis	k1gInSc2	zápis
assembleru	assembler	k1gInSc2	assembler
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
C	C	kA	C
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
mnohem	mnohem	k6eAd1	mnohem
čitelnější	čitelný	k2eAgInSc1d2	čitelnější
než	než	k8xS	než
assembler	assembler	k1gInSc1	assembler
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
ho	on	k3xPp3gMnSc4	on
zapsat	zapsat	k5eAaPmF	zapsat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
snáze	snadno	k6eAd2	snadno
přenositelný	přenositelný	k2eAgInSc1d1	přenositelný
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
procesory	procesor	k1gInPc4	procesor
a	a	k8xC	a
počítačové	počítačový	k2eAgFnSc2d1	počítačová
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
překladače	překladač	k1gInPc1	překladač
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnPc1	knihovna
a	a	k8xC	a
interprety	interpret	k1gMnPc4	interpret
vysokoúrovňových	vysokoúrovňový	k2eAgInPc2d1	vysokoúrovňový
jazyků	jazyk	k1gInPc2	jazyk
implementovány	implementován	k2eAgInPc1d1	implementován
právě	právě	k6eAd1	právě
v	v	k7c6	v
C.	C.	kA	C.
Mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
moderních	moderní	k2eAgInPc2d1	moderní
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
přebírá	přebírat	k5eAaImIp3nS	přebírat
způsob	způsob	k1gInSc1	způsob
zápisu	zápis	k1gInSc2	zápis
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
syntaxi	syntaxe	k1gFnSc4	syntaxe
<g/>
)	)	kIx)	)
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
Java	Java	k1gFnSc1	Java
<g/>
,	,	kIx,	,
Perl	perl	k1gInSc1	perl
a	a	k8xC	a
PHP	PHP	kA	PHP
<g/>
.	.	kIx.	.
</s>
<s>
Ukládání	ukládání	k1gNnSc1	ukládání
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
C	C	kA	C
řešeno	řešit	k5eAaImNgNnS	řešit
třemi	tři	k4xCgInPc7	tři
základními	základní	k2eAgInPc7d1	základní
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
statickou	statický	k2eAgFnSc7d1	statická
alokací	alokace	k1gFnSc7	alokace
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automatickou	automatický	k2eAgFnSc7d1	automatická
alokací	alokace	k1gFnSc7	alokace
paměti	paměť	k1gFnSc2	paměť
na	na	k7c6	na
zásobníku	zásobník	k1gInSc2	zásobník
a	a	k8xC	a
dynamickou	dynamický	k2eAgFnSc7d1	dynamická
alokací	alokace	k1gFnSc7	alokace
na	na	k7c6	na
haldě	halda	k1gFnSc6	halda
(	(	kIx(	(
<g/>
heap	heap	k1gInSc1	heap
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
knihovních	knihovní	k2eAgFnPc2d1	knihovní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
disponuje	disponovat	k5eAaBmIp3nS	disponovat
jen	jen	k9	jen
minimální	minimální	k2eAgFnSc7d1	minimální
abstrakcí	abstrakce	k1gFnSc7	abstrakce
nad	nad	k7c7	nad
alokací	alokace	k1gFnSc7	alokace
<g/>
:	:	kIx,	:
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
přes	přes	k7c4	přes
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
zvaný	zvaný	k2eAgInSc1d1	zvaný
ukazatel	ukazatel	k1gInSc1	ukazatel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
drží	držet	k5eAaImIp3nS	držet
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
paměťový	paměťový	k2eAgInSc4d1	paměťový
prostor	prostor	k1gInSc4	prostor
daného	daný	k2eAgInSc2d1	daný
typu	typ	k1gInSc2	typ
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
aritmetické	aritmetický	k2eAgFnPc4d1	aritmetická
operace	operace	k1gFnPc4	operace
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
operace	operace	k1gFnPc1	operace
ale	ale	k8xC	ale
neoperují	operovat	k5eNaImIp3nP	operovat
s	s	k7c7	s
ukazateli	ukazatel	k1gMnPc7	ukazatel
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
bajtů	bajt	k1gInPc2	bajt
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
přihlíží	přihlížet	k5eAaImIp3nS	přihlížet
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
datového	datový	k2eAgInSc2d1	datový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
-	-	kIx~	-
existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
také	také	k9	také
ukazatele	ukazatel	k1gInPc1	ukazatel
typu	typ	k1gInSc2	typ
void	void	k6eAd1	void
*	*	kIx~	*
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
typ	typ	k1gInSc4	typ
dat	datum	k1gNnPc2	datum
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatele	ukazatel	k1gInPc1	ukazatel
tedy	tedy	k9	tedy
existují	existovat	k5eAaImIp3nP	existovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
proměnných	proměnná	k1gFnPc6	proměnná
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
odpovědnosti	odpovědnost	k1gFnSc6	odpovědnost
programátora	programátor	k1gMnSc2	programátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neukazovaly	ukazovat	k5eNaImAgFnP	ukazovat
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
nealokovanou	alokovaný	k2eNgFnSc4d1	alokovaný
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatele	ukazatel	k1gInPc1	ukazatel
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
mocným	mocný	k2eAgInSc7d1	mocný
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
C	C	kA	C
jazyk	jazyk	k1gInSc1	jazyk
povoluje	povolovat	k5eAaImIp3nS	povolovat
ukazatele	ukazatel	k1gInPc4	ukazatel
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
jsou	být	k5eAaImIp3nP	být
ukazatele	ukazatel	k1gInPc1	ukazatel
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přenositelnosti	přenositelnost	k1gFnSc2	přenositelnost
a	a	k8xC	a
rizika	riziko	k1gNnSc2	riziko
zhroucení	zhroucení	k1gNnSc2	zhroucení
programu	program	k1gInSc2	program
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
nesprávném	správný	k2eNgInSc6d1	nesprávný
použití	použití	k1gNnSc2	použití
Achillovou	Achillův	k2eAgFnSc7d1	Achillova
patou	pata	k1gFnSc7	pata
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
programátor	programátor	k1gMnSc1	programátor
má	mít	k5eAaImIp3nS	mít
plnou	plný	k2eAgFnSc4d1	plná
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
alokaci	alokace	k1gFnSc4	alokace
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
automatickém	automatický	k2eAgInSc6d1	automatický
dealokátoru	dealokátor	k1gInSc6	dealokátor
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
garbage	garbagat	k5eAaPmIp3nS	garbagat
collector	collector	k1gMnSc1	collector
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
Java	Javum	k1gNnSc2	Javum
a	a	k8xC	a
C	C	kA	C
<g/>
♯	♯	k?	♯
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
C	C	kA	C
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
méně	málo	k6eAd2	málo
univerzální	univerzální	k2eAgInSc4d1	univerzální
způsob	způsob	k1gInSc4	způsob
odkazování	odkazování	k1gNnSc2	odkazování
alokovaných	alokovaný	k2eAgFnPc2d1	alokovaná
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
chyby	chyba	k1gFnSc2	chyba
v	v	k7c6	v
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
rozšíření	rozšíření	k1gNnSc1	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
programátora	programátor	k1gMnSc2	programátor
za	za	k7c4	za
alokaci	alokace	k1gFnSc4	alokace
zachoval	zachovat	k5eAaPmAgMnS	zachovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
norem	norma	k1gFnPc2	norma
C	C	kA	C
<g/>
++	++	k?	++
<g/>
11	[number]	k4	11
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc6d2	vyšší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možnost	možnost	k1gFnSc1	možnost
volby	volba	k1gFnSc2	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
Bellových	Bellův	k2eAgFnPc6d1	Bellova
laboratořích	laboratoř	k1gFnPc6	laboratoř
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1969	[number]	k4	1969
a	a	k8xC	a
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Ritchie	Ritchie	k1gFnSc1	Ritchie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpřínosnější	přínosný	k2eAgNnSc1d3	nejpřínosnější
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
zvolili	zvolit	k5eAaPmAgMnP	zvolit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c4	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
přebírali	přebírat	k5eAaImAgMnP	přebírat
ze	z	k7c2	z
staršího	starý	k2eAgInSc2d2	starší
jazyka	jazyk	k1gInSc2	jazyk
zvaného	zvaný	k2eAgInSc2d1	zvaný
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
zase	zase	k9	zase
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jazyka	jazyk	k1gInSc2	jazyk
BCPL	BCPL	kA	BCPL
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Thompson	Thompson	k1gMnSc1	Thompson
také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jazyk	jazyk	k1gInSc4	jazyk
Bon	bona	k1gFnPc2	bona
na	na	k7c4	na
poctu	pocta	k1gFnSc4	pocta
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Bonnie	Bonnie	k1gFnSc2	Bonnie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
dostatečně	dostatečně	k6eAd1	dostatečně
stabilním	stabilní	k2eAgFnPc3d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
jádra	jádro	k1gNnSc2	jádro
Unixu	Unix	k1gInSc2	Unix
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
napsaného	napsaný	k2eAgInSc2d1	napsaný
v	v	k7c6	v
assembleru	assembler	k1gInSc6	assembler
PDP-	PDP-	k1gFnPc2	PDP-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přepsána	přepsat	k5eAaPmNgFnS	přepsat
do	do	k7c2	do
C.	C.	kA	C.
Unix	Unix	k1gInSc1	Unix
tedy	tedy	k9	tedy
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
napsané	napsaný	k2eAgInPc1d1	napsaný
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
než	než	k8xS	než
strojovém	strojový	k2eAgInSc6d1	strojový
jazyce	jazyk	k1gInSc6	jazyk
či	či	k8xC	či
assembleru	assembler	k1gInSc6	assembler
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInSc1d1	předchozí
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
systém	systém	k1gInSc1	systém
Multics	Multics	k1gInSc1	Multics
(	(	kIx(	(
<g/>
napsaný	napsaný	k2eAgInSc1d1	napsaný
v	v	k7c6	v
PL	PL	kA	PL
<g/>
/	/	kIx~	/
<g/>
I	I	kA	I
<g/>
)	)	kIx)	)
a	a	k8xC	a
TRIPOS	TRIPOS	kA	TRIPOS
(	(	kIx(	(
<g/>
napsaný	napsaný	k2eAgInSc1d1	napsaný
v	v	k7c6	v
BCPL	BCPL	kA	BCPL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Ritchie	Ritchie	k1gFnSc1	Ritchie
a	a	k8xC	a
Brian	Brian	k1gMnSc1	Brian
Kernighan	Kernighan	k1gMnSc1	Kernighan
vydali	vydat	k5eAaPmAgMnP	vydat
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
knihy	kniha	k1gFnSc2	kniha
The	The	k1gMnPc2	The
C	C	kA	C
Programming	Programming	k1gInSc4	Programming
Language	language	k1gFnSc2	language
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
programátory	programátor	k1gInPc7	programátor
C	C	kA	C
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
K	k	k7c3	k
<g/>
&	&	k?	&
<g/>
R	R	kA	R
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sloužila	sloužit	k5eAaImAgFnS	sloužit
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
neformální	formální	k2eNgFnSc2d1	neformální
specifikace	specifikace	k1gFnSc2	specifikace
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
C	C	kA	C
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
takto	takto	k6eAd1	takto
popsali	popsat	k5eAaPmAgMnP	popsat
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
K	k	k7c3	k
<g/>
&	&	k?	&
<g/>
R	R	kA	R
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
knihy	kniha	k1gFnSc2	kniha
popisovalo	popisovat	k5eAaImAgNnS	popisovat
novější	nový	k2eAgInSc4d2	novější
standard	standard	k1gInSc4	standard
ANSI	ANSI	kA	ANSI
C.	C.	kA	C.
<g/>
)	)	kIx)	)
K	k	k7c3	k
<g/>
&	&	k?	&
<g/>
R	R	kA	R
zavedli	zavést	k5eAaPmAgMnP	zavést
následující	následující	k2eAgFnPc4d1	následující
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc1	typ
struct	struct	k2eAgInSc1d1	struct
datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc1	typ
long	long	k1gInSc1	long
int	int	k?	int
datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc1	typ
unsigned	unsigned	k1gMnSc1	unsigned
int	int	k?	int
Operátor	operátor	k1gMnSc1	operátor
=	=	kIx~	=
<g/>
+	+	kIx~	+
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
+	+	kIx~	+
<g/>
=	=	kIx~	=
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
+	+	kIx~	+
mátl	mást	k5eAaImAgMnS	mást
lexikální	lexikální	k2eAgMnSc1d1	lexikální
analyzér	analyzér	k1gMnSc1	analyzér
překladače	překladač	k1gInSc2	překladač
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
<g/>
&	&	k?	&
<g/>
R	R	kA	R
C	C	kA	C
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
musejí	muset	k5eAaImIp3nP	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
všechny	všechen	k3xTgInPc4	všechen
překladače	překladač	k1gInPc4	překladač
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
Ještě	ještě	k6eAd1	ještě
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
ANSI	ANSI	kA	ANSI
C	C	kA	C
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
nejmenší	malý	k2eAgInSc1d3	nejmenší
společný	společný	k2eAgInSc1d1	společný
jmenovatel	jmenovatel	k1gInSc1	jmenovatel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
využívali	využívat	k5eAaPmAgMnP	využívat
programátoři	programátor	k1gMnPc1	programátor
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
kvůli	kvůli	k7c3	kvůli
maximální	maximální	k2eAgFnSc3d1	maximální
přenositelnosti	přenositelnost	k1gFnSc3	přenositelnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g />
.	.	kIx.	.
</s>
<s>
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
překladače	překladač	k1gInPc1	překladač
plně	plně	k6eAd1	plně
podporovaly	podporovat	k5eAaImAgInP	podporovat
ANSI	ANSI	kA	ANSI
C.	C.	kA	C.
V	v	k7c6	v
několika	několik	k4yIc2	několik
letech	léto	k1gNnPc6	léto
následujících	následující	k2eAgNnPc6d1	následující
po	po	k7c6	po
uvedení	uvedení	k1gNnSc1	uvedení
K	k	k7c3	k
<g/>
&	&	k?	&
<g/>
R	R	kA	R
C	C	kA	C
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
a	a	k8xC	a
přidáno	přidat	k5eAaPmNgNnS	přidat
několik	několik	k4yIc1	několik
"	"	kIx"	"
<g/>
neoficiálních	oficiální	k2eNgFnPc2d1	neoficiální
<g/>
"	"	kIx"	"
vlastností	vlastnost	k1gFnPc2	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
překladači	překladač	k1gInSc3	překladač
od	od	k7c2	od
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g/>
:	:	kIx,	:
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
void	void	k6eAd1	void
*	*	kIx~	*
a	a	k8xC	a
funkce	funkce	k1gFnPc1	funkce
vracející	vracející	k2eAgFnPc1d1	vracející
void	void	k1gInSc4	void
funkce	funkce	k1gFnSc2	funkce
vracející	vracející	k2eAgInSc4d1	vracející
typ	typ	k1gInSc4	typ
struct	structa	k1gFnPc2	structa
nebo	nebo	k8xC	nebo
union	union	k1gInSc1	union
položky	položka	k1gFnSc2	položka
ve	v	k7c4	v
struct	struct	k1gInSc4	struct
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
do	do	k7c2	do
odděleného	oddělený	k2eAgInSc2d1	oddělený
jmenného	jmenný	k2eAgInSc2d1	jmenný
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
struct	struct	k2eAgInSc4d1	struct
modifikátor	modifikátor	k1gInSc4	modifikátor
const	const	k5eAaPmF	const
standardní	standardní	k2eAgFnSc4d1	standardní
knihovnu	knihovna	k1gFnSc4	knihovna
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
většinu	většina	k1gFnSc4	většina
funkcí	funkce	k1gFnPc2	funkce
implementovaných	implementovaný	k2eAgFnPc2d1	implementovaná
různými	různý	k2eAgInPc7d1	různý
dodavateli	dodavatel	k1gMnPc7	dodavatel
výčtový	výčtový	k2eAgInSc4d1	výčtový
typ	typ	k1gInSc4	typ
enumeration	enumeration	k1gInSc1	enumeration
datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc4	typ
float	float	k5eAaPmF	float
V	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
začalo	začít	k5eAaPmAgNnS	začít
C	C	kA	C
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
BASIC	Basic	kA	Basic
jako	jako	k8xC	jako
přední	přední	k2eAgInSc1d1	přední
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
mikropočítače	mikropočítač	k1gInPc4	mikropočítač
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
IBM	IBM	kA	IBM
PC	PC	kA	PC
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gMnSc1	Stroustrup
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
v	v	k7c6	v
Bellových	Bellův	k2eAgFnPc6d1	Bellova
laboratořích	laboratoř	k1gFnPc6	laboratoř
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
C	C	kA	C
o	o	k7c4	o
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgInPc4d1	orientovaný
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
na	na	k7c6	na
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
;	;	kIx,	;
C	C	kA	C
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
populárnější	populární	k2eAgFnSc1d2	populárnější
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Unixu	Unix	k1gInSc2	Unix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
American	American	k1gInSc1	American
National	National	k1gFnSc2	National
Standards	Standardsa	k1gFnPc2	Standardsa
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
ANSI	ANSI	kA	ANSI
<g/>
)	)	kIx)	)
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
na	na	k7c4	na
sestavení	sestavení	k1gNnSc4	sestavení
komise	komise	k1gFnSc2	komise
X	X	kA	X
<g/>
3	[number]	k4	3
<g/>
J	J	kA	J
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
standardní	standardní	k2eAgFnSc4d1	standardní
specifikaci	specifikace	k1gFnSc4	specifikace
C.	C.	kA	C.
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
a	a	k8xC	a
pracném	pracný	k2eAgInSc6d1	pracný
procesu	proces	k1gInSc6	proces
byl	být	k5eAaImAgInS	být
standard	standard	k1gInSc1	standard
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
a	a	k8xC	a
schválen	schválit	k5eAaPmNgInS	schválit
jako	jako	k9	jako
ANSI	ANSI	kA	ANSI
X	X	kA	X
<g/>
3.159	[number]	k4	3.159
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
"	"	kIx"	"
<g/>
Programming	Programming	k1gInSc1	Programming
Language	language	k1gFnSc2	language
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
stále	stále	k6eAd1	stále
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
ANSI	ANSI	kA	ANSI
C.	C.	kA	C.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
standard	standard	k1gInSc1	standard
ANSI	ANSI	kA	ANSI
C	C	kA	C
(	(	kIx(	(
<g/>
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
změnami	změna	k1gFnPc7	změna
<g/>
)	)	kIx)	)
adoptován	adoptován	k2eAgMnSc1d1	adoptován
institucí	instituce	k1gFnSc7	instituce
International	International	k1gFnSc2	International
Organization	Organization	k1gInSc1	Organization
for	forum	k1gNnPc2	forum
Standardization	Standardization	k1gInSc1	Standardization
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
)	)	kIx)	)
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ISO	ISO	kA	ISO
9899	[number]	k4	9899
<g/>
|	|	kIx~	|
<g/>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
9899	[number]	k4	9899
<g/>
:	:	kIx,	:
<g/>
1990	[number]	k4	1990
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
standardizačního	standardizační	k2eAgInSc2d1	standardizační
procesu	proces	k1gInSc2	proces
ANSI	ANSI	kA	ANSI
C	C	kA	C
byl	být	k5eAaImAgInS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
nadmnožinu	nadmnožina	k1gFnSc4	nadmnožina
K	k	k7c3	k
<g/>
&	&	k?	&
<g/>
R	R	kA	R
C	C	kA	C
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
mnoho	mnoho	k4c1	mnoho
"	"	kIx"	"
<g/>
neoficiálních	neoficiální	k2eAgFnPc2d1	neoficiální
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
standardizační	standardizační	k2eAgFnSc1d1	standardizační
komise	komise	k1gFnSc1	komise
přidala	přidat	k5eAaPmAgFnS	přidat
několik	několik	k4yIc4	několik
vlastností	vlastnost	k1gFnPc2	vlastnost
jako	jako	k8xS	jako
funkční	funkční	k2eAgInPc4d1	funkční
prototypy	prototyp	k1gInPc4	prototyp
(	(	kIx(	(
<g/>
vypůjčené	vypůjčený	k2eAgInPc1d1	vypůjčený
z	z	k7c2	z
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
a	a	k8xC	a
schopnější	schopný	k2eAgInSc1d2	schopnější
preprocesor	preprocesor	k1gInSc1	preprocesor
<g/>
.	.	kIx.	.
</s>
<s>
ANSI	ANSI	kA	ANSI
C	C	kA	C
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
podporováno	podporován	k2eAgNnSc1d1	podporováno
téměř	téměř	k6eAd1	téměř
všemi	všecek	k3xTgInPc7	všecek
rozšířenými	rozšířený	k2eAgInPc7d1	rozšířený
překladači	překladač	k1gInPc7	překladač
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kódu	kód	k1gInSc2	kód
psaného	psaný	k2eAgInSc2d1	psaný
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
C	C	kA	C
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c4	na
ANSI	ANSI	kA	ANSI
C.	C.	kA	C.
Jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
program	program	k1gInSc4	program
napsaný	napsaný	k2eAgInSc4d1	napsaný
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
standardním	standardní	k2eAgNnSc6d1	standardní
C	C	kA	C
je	být	k5eAaImIp3nS	být
přeložitelný	přeložitelný	k2eAgInSc1d1	přeložitelný
a	a	k8xC	a
spustitelný	spustitelný	k2eAgMnSc1d1	spustitelný
na	na	k7c6	na
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
platformě	platforma	k1gFnSc6	platforma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tomuto	tento	k3xDgInSc3	tento
standardu	standard	k1gInSc3	standard
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k4c4	mnoho
programů	program	k1gInPc2	program
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
platformě	platforma	k1gFnSc6	platforma
nebo	nebo	k8xC	nebo
jedním	jeden	k4xCgInSc7	jeden
překladačem	překladač	k1gInSc7	překladač
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
použití	použití	k1gNnSc3	použití
nestandardních	standardní	k2eNgFnPc2d1	nestandardní
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
grafiku	grafika	k1gFnSc4	grafika
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
některé	některý	k3yIgInPc1	některý
překladače	překladač	k1gInPc1	překladač
v	v	k7c6	v
implicitním	implicitní	k2eAgInSc6d1	implicitní
módu	mód	k1gInSc6	mód
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
standardu	standard	k1gInSc2	standard
ANSI	ANSI	kA	ANSI
C.	C.	kA	C.
Po	po	k7c6	po
standardizaci	standardizace	k1gFnSc6	standardizace
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
vývoje	vývoj	k1gInSc2	vývoj
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
dokumentu	dokument	k1gInSc2	dokument
ISO	ISO	kA	ISO
9899	[number]	k4	9899
<g/>
:	:	kIx,	:
<g/>
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
C	C	kA	C
<g/>
99	[number]	k4	99
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2000	[number]	k4	2000
přijat	přijmout	k5eAaPmNgInS	přijmout
i	i	k8xC	i
jako	jako	k8xS	jako
ANSI	ANSI	kA	ANSI
standard	standard	k1gInSc1	standard
<g/>
.	.	kIx.	.
</s>
<s>
C99	C99	k4	C99
představil	představit	k5eAaPmAgMnS	představit
několik	několik	k4yIc4	několik
nových	nový	k2eAgFnPc2d1	nová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
mnohdy	mnohdy	k6eAd1	mnohdy
v	v	k7c6	v
překladačích	překladač	k1gInPc6	překladač
už	už	k6eAd1	už
implementovány	implementován	k2eAgFnPc1d1	implementována
jako	jako	k8xC	jako
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
:	:	kIx,	:
Inline	Inlin	k1gMnSc5	Inlin
funkce	funkce	k1gFnPc4	funkce
Proměnné	proměnná	k1gFnPc4	proměnná
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
deklarovány	deklarovat	k5eAaBmNgInP	deklarovat
kdekoli	kdekoli	k6eAd1	kdekoli
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
C89	C89	k1gFnSc6	C89
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
deklarovány	deklarovat	k5eAaBmNgFnP	deklarovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
bloku	blok	k1gInSc2	blok
Několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
long	longa	k1gFnPc2	longa
long	longa	k1gFnPc2	longa
int	int	k?	int
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
64	[number]	k4	64
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
integer	integer	k1gInSc1	integer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bool	bool	k1gMnSc1	bool
(	(	kIx(	(
<g/>
logický	logický	k2eAgMnSc1d1	logický
ano	ano	k9	ano
<g/>
/	/	kIx~	/
<g/>
ne	ne	k9	ne
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
typ	typ	k1gInSc1	typ
complex	complex	k1gInSc1	complex
určený	určený	k2eAgInSc1d1	určený
na	na	k7c4	na
reprezentaci	reprezentace	k1gFnSc4	reprezentace
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
s	s	k7c7	s
nekonstantní	konstantní	k2eNgFnSc7d1	nekonstantní
velikostí	velikost	k1gFnSc7	velikost
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
zakomentování	zakomentování	k1gNnSc4	zakomentování
jednoho	jeden	k4xCgMnSc2	jeden
řádku	řádek	k1gInSc2	řádek
//	//	k?	//
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
C	C	kA	C
<g/>
++	++	k?	++
nebo	nebo	k8xC	nebo
BCPL	BCPL	kA	BCPL
Nové	Nová	k1gFnPc4	Nová
knihovní	knihovní	k2eAgFnSc2d1	knihovní
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
náhrady	náhrada	k1gFnSc2	náhrada
za	za	k7c4	za
funkce	funkce	k1gFnPc4	funkce
náchylné	náchylný	k2eAgFnPc4d1	náchylná
na	na	k7c6	na
přetečení	přetečení	k1gNnSc6	přetečení
na	na	k7c6	na
zásobníku	zásobník	k1gInSc6	zásobník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
snprintf	snprintf	k1gInSc1	snprintf
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgInPc1d1	Nové
hlavičkové	hlavičkový	k2eAgInPc1d1	hlavičkový
soubory	soubor	k1gInPc1	soubor
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
stdint	stdint	k1gMnSc1	stdint
<g/>
.	.	kIx.	.
<g/>
h	h	k?	h
Variadická	Variadický	k2eAgNnPc1d1	Variadický
makra	makro	k1gNnPc1	makro
(	(	kIx(	(
<g/>
makra	makro	k1gNnSc2	makro
C	C	kA	C
preprocesoru	preprocesor	k1gInSc2	preprocesor
s	s	k7c7	s
proměnným	proměnný	k2eAgInSc7d1	proměnný
počtem	počet	k1gInSc7	počet
argumentů	argument	k1gInPc2	argument
<g/>
)	)	kIx)	)
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
slovo	slovo	k1gNnSc1	slovo
restrict	restricta	k1gFnPc2	restricta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
deklaraci	deklarace	k1gFnSc6	deklarace
ukazatele	ukazatel	k1gInSc2	ukazatel
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
odkazovanou	odkazovaný	k2eAgFnSc4d1	odkazovaná
tímto	tento	k3xDgNnSc7	tento
ukazatel	ukazatel	k1gInSc1	ukazatel
nepřistupuje	přistupovat	k5eNaImIp3nS	přistupovat
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
ukazatel	ukazatel	k1gInSc1	ukazatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
int	int	k?	int
<g/>
*	*	kIx~	*
restrict	restrict	k1gMnSc1	restrict
foo	foo	k?	foo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
překladač	překladač	k1gInSc1	překladač
může	moct	k5eAaImIp3nS	moct
produkovat	produkovat	k5eAaImF	produkovat
optimalizovanější	optimalizovaný	k2eAgInSc4d2	optimalizovaný
kód	kód	k1gInSc4	kód
<g/>
;	;	kIx,	;
zajištění	zajištění	k1gNnSc4	zajištění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tomu	ten	k3xDgNnSc3	ten
skutečně	skutečně	k6eAd1	skutečně
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
programátorovi	programátor	k1gMnSc6	programátor
(	(	kIx(	(
<g/>
při	při	k7c6	při
nedodržení	nedodržení	k1gNnSc6	nedodržení
je	být	k5eAaImIp3nS	být
chování	chování	k1gNnSc1	chování
programu	program	k1gInSc2	program
nedefinované	definovaný	k2eNgNnSc1d1	nedefinované
<g/>
)	)	kIx)	)
Standard	standard	k1gInSc1	standard
C99	C99	k1gFnSc2	C99
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
přísnější	přísný	k2eAgFnPc1d2	přísnější
než	než	k8xS	než
původní	původní	k2eAgInSc1d1	původní
standard	standard	k1gInSc1	standard
C	C	kA	C
<g/>
89	[number]	k4	89
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ukazatele	ukazatel	k1gInSc2	ukazatel
jiného	jiný	k2eAgInSc2d1	jiný
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
než	než	k8xS	než
pomocí	pomocí	k7c2	pomocí
jakého	jaký	k3yQgNnSc2	jaký
byla	být	k5eAaImAgFnS	být
zapsána	zapsán	k2eAgFnSc1d1	zapsána
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
překladači	překladač	k1gMnSc3	překladač
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
lepší	dobrý	k2eAgFnSc3d2	lepší
optimalizaci	optimalizace	k1gFnSc3	optimalizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
kompilací	kompilace	k1gFnSc7	kompilace
starších	starý	k2eAgInPc2d2	starší
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
kompilátor	kompilátor	k1gInSc1	kompilátor
zatím	zatím	k6eAd1	zatím
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
implementaci	implementace	k1gFnSc4	implementace
C	C	kA	C
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
(	(	kIx(	(
<g/>
GCC	GCC	kA	GCC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Firmy	firma	k1gFnPc1	firma
jako	jako	k8xS	jako
Microsoft	Microsoft	kA	Microsoft
nebo	nebo	k8xC	nebo
Borland	Borland	kA	Borland
neprojevily	projevit	k5eNaPmAgFnP	projevit
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
o	o	k7c6	o
implementaci	implementace	k1gFnSc6	implementace
C	C	kA	C
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
nových	nový	k2eAgFnPc2d1	nová
vlastností	vlastnost	k1gFnPc2	vlastnost
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
nekompatibilně	kompatibilně	k6eNd1	kompatibilně
s	s	k7c7	s
C99	C99	k1gFnPc7	C99
(	(	kIx(	(
<g/>
datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc1	typ
complex	complex	k1gInSc1	complex
v	v	k7c6	v
C99	C99	k1gFnSc6	C99
versus	versus	k7c1	versus
třída	třída	k1gFnSc1	třída
complex	complex	k1gInSc1	complex
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
aplikace	aplikace	k1gFnSc1	aplikace
vypíše	vypsat	k5eAaPmIp3nS	vypsat
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
na	na	k7c4	na
standardní	standardní	k2eAgInSc4d1	standardní
výstup	výstup	k1gInSc4	výstup
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
i	i	k9	i
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
nebo	nebo	k8xC	nebo
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
hardwarové	hardwarový	k2eAgNnSc4d1	hardwarové
zařízení	zařízení	k1gNnSc4	zařízení
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
bit	bit	k2eAgInSc1d1	bit
bucket	bucket	k1gInSc1	bucket
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
nastavení	nastavení	k1gNnSc4	nastavení
standardního	standardní	k2eAgInSc2d1	standardní
výstupu	výstup	k1gInSc2	výstup
v	v	k7c6	v
době	doba	k1gFnSc6	doba
spuštění	spuštění	k1gNnSc2	spuštění
programu	program	k1gInSc2	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
K	K	kA	K
<g/>
&	&	k?	&
<g/>
R.	R.	kA	R.
Programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
má	mít	k5eAaImIp3nS	mít
tyto	tento	k3xDgInPc1	tento
datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
<g/>
:	:	kIx,	:
char	char	k1gInSc1	char
-	-	kIx~	-
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
znaky	znak	k1gInPc4	znak
int	int	k?	int
-	-	kIx~	-
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
float	float	k5eAaBmF	float
-	-	kIx~	-
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
desetinné	desetinný	k2eAgNnSc4d1	desetinné
číslo	číslo	k1gNnSc4	číslo
s	s	k7c7	s
plovoucí	plovoucí	k2eAgFnSc7d1	plovoucí
řádovou	řádový	k2eAgFnSc7d1	řádová
čárkou	čárka	k1gFnSc7	čárka
double	double	k2eAgFnSc7d1	double
-	-	kIx~	-
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
desetinné	desetinný	k2eAgNnSc4d1	desetinné
číslo	číslo	k1gNnSc4	číslo
s	s	k7c7	s
plovoucí	plovoucí	k2eAgFnSc7d1	plovoucí
řádovou	řádový	k2eAgFnSc7d1	řádová
čárkou	čárka	k1gFnSc7	čárka
s	s	k7c7	s
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1	dvojnásobná
přesností	přesnost	k1gFnSc7	přesnost
void	void	k1gMnSc1	void
-	-	kIx~	-
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepotřebujeme	potřebovat	k5eNaImIp1nP	potřebovat
žádnou	žádný	k3yNgFnSc4	žádný
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
funkcí	funkce	k1gFnPc2	funkce
<g/>
)	)	kIx)	)
Řídící	řídící	k2eAgInPc1d1	řídící
příkazy	příkaz	k1gInPc1	příkaz
určují	určovat	k5eAaImIp3nP	určovat
průběh	průběh	k1gInSc1	průběh
zpracovávání	zpracovávání	k1gNnSc2	zpracovávání
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
takové	takový	k3xDgFnPc1	takový
tvoří	tvořit	k5eAaImIp3nP	tvořit
páteř	páteř	k1gFnSc4	páteř
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
if	if	k?	if
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkazů	příkaz	k1gInPc2	příkaz
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
pro	pro	k7c4	pro
větvení	větvení	k1gNnSc4	větvení
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
také	také	k9	také
jako	jako	k9	jako
podmíněné	podmíněný	k2eAgInPc4d1	podmíněný
příkazy	příkaz	k1gInPc4	příkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
výsledkem	výsledek	k1gInSc7	výsledek
testu	test	k1gInSc2	test
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vyhodnocena	vyhodnotit	k5eAaPmNgFnS	vyhodnotit
jako	jako	k9	jako
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduše	jednoduše	k6eAd1	jednoduše
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
činí	činit	k5eAaImIp3nS	činit
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
nějaké	nějaký	k3yIgFnSc2	nějaký
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
K	K	kA	K
příkazu	příkaz	k1gInSc2	příkaz
if	if	k?	if
můžete	moct	k5eAaImIp2nP	moct
přidat	přidat	k5eAaPmF	přidat
else	else	k6eAd1	else
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tak	tak	k6eAd1	tak
učiníte	učinit	k5eAaImIp2nP	učinit
<g/>
,	,	kIx,	,
tak	tak	k9	tak
programu	program	k1gInSc2	program
řeknete	říct	k5eAaPmIp2nP	říct
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
má	mít	k5eAaImIp3nS	mít
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
i	i	k8xC	i
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
podmínka	podmínka	k1gFnSc1	podmínka
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
poté	poté	k6eAd1	poté
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaPmF	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Vývojový	vývojový	k2eAgInSc1d1	vývojový
diagram	diagram	k1gInSc1	diagram
k	k	k7c3	k
příkladu	příklad	k1gInSc3	příklad
<g/>
:	:	kIx,	:
Příkaz	příkaz	k1gInSc1	příkaz
switch	switcha	k1gFnPc2	switcha
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
větví	větev	k1gFnPc2	větev
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
nějaké	nějaký	k3yIgFnSc6	nějaký
celočíselné	celočíselný	k2eAgFnSc6d1	celočíselná
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
formát	formát	k1gInSc1	formát
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
hodnoty	hodnota	k1gFnSc2	hodnota
celočíselného	celočíselný	k2eAgInSc2d1	celočíselný
výrazu	výraz	k1gInSc2	výraz
se	se	k3xPyFc4	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
case	cas	k1gFnSc2	cas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hodnota	hodnota	k1gFnSc1	hodnota
výrazu	výraz	k1gInSc2	výraz
rovna	roven	k2eAgFnSc1d1	rovna
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
provedou	provést	k5eAaPmIp3nP	provést
se	se	k3xPyFc4	se
příkazy	příkaz	k1gInPc1	příkaz
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
rovna	roven	k2eAgFnSc1d1	rovna
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
provedou	provést	k5eAaPmIp3nP	provést
se	se	k3xPyFc4	se
příkazy	příkaz	k1gInPc1	příkaz
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Příkaz	příkaz	k1gInSc1	příkaz
break	break	k1gInSc1	break
na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
větve	větev	k1gFnSc2	větev
není	být	k5eNaImIp3nS	být
povinný	povinný	k2eAgInSc1d1	povinný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
tam	tam	k6eAd1	tam
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
větve	větev	k1gFnSc2	větev
provádět	provádět	k5eAaImF	provádět
další	další	k2eAgFnSc4d1	další
větev	větev	k1gFnSc4	větev
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
hodnotu	hodnota	k1gFnSc4	hodnota
provádět	provádět	k5eAaImF	provádět
jedinou	jediný	k2eAgFnSc4d1	jediná
větev	větev	k1gFnSc4	větev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
break	break	k1gInSc1	break
nutný	nutný	k2eAgInSc1d1	nutný
na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
větve	větev	k1gFnSc2	větev
kromě	kromě	k7c2	kromě
poslední	poslední	k2eAgFnSc2d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Větev	větev	k1gFnSc1	větev
default	default	k1gInSc1	default
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
přítomna	přítomno	k1gNnPc1	přítomno
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vykoná	vykonat	k5eAaPmIp3nS	vykonat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
výrazu	výraz	k1gInSc2	výraz
není	být	k5eNaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
ani	ani	k9	ani
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
hodnot	hodnota	k1gFnPc2	hodnota
uvedených	uvedený	k2eAgFnPc2d1	uvedená
za	za	k7c4	za
příkazy	příkaz	k1gInPc4	příkaz
case	cas	k1gFnSc2	cas
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
Příkaz	příkaz	k1gInSc1	příkaz
cyklu	cyklus	k1gInSc6	cyklus
for	forum	k1gNnPc2	forum
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
příkazů	příkaz	k1gInPc2	příkaz
cyklu	cyklus	k1gInSc2	cyklus
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
C.	C.	kA	C.
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
opakovat	opakovat	k5eAaImF	opakovat
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
příkazů	příkaz	k1gInPc2	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
for	forum	k1gNnPc2	forum
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgInPc7d1	mnohý
programátory	programátor	k1gInPc7	programátor
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
C	C	kA	C
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
nejpružnější	pružný	k2eAgInSc4d3	nejpružnější
příkaz	příkaz	k1gInSc4	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
for	forum	k1gNnPc2	forum
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zadaný	zadaný	k2eAgInSc4d1	zadaný
počet	počet	k1gInSc4	počet
opakování	opakování	k1gNnSc4	opakování
příkazu	příkaz	k1gInSc2	příkaz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bloku	blok	k1gInSc2	blok
příkazu	příkaz	k1gInSc2	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obecný	obecný	k2eAgInSc1d1	obecný
formát	formát	k1gInSc1	formát
pro	pro	k7c4	pro
opakování	opakování	k1gNnSc4	opakování
jednoho	jeden	k4xCgInSc2	jeden
příkazu	příkaz	k1gInSc2	příkaz
vypadá	vypadat	k5eAaImIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Cyklus	cyklus	k1gInSc1	cyklus
while	while	k6eAd1	while
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc1	cyklus
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Napřed	napřed	k6eAd1	napřed
se	se	k3xPyFc4	se
testuje	testovat	k5eAaImIp3nS	testovat
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
platná	platný	k2eAgFnSc1d1	platná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
tělo	tělo	k1gNnSc1	tělo
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
testuje	testovat	k5eAaImIp3nS	testovat
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
platná	platný	k2eAgFnSc1d1	platná
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
za	za	k7c7	za
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
podmínka	podmínka	k1gFnSc1	podmínka
platná	platný	k2eAgFnSc1d1	platná
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
příchodu	příchod	k1gInSc6	příchod
na	na	k7c4	na
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
neprovede	provést	k5eNaPmIp3nS	provést
se	se	k3xPyFc4	se
cyklus	cyklus	k1gInSc4	cyklus
ani	ani	k9	ani
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
formát	formát	k1gInSc1	formát
vypadá	vypadat	k5eAaPmIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Cyklus	cyklus	k1gInSc1	cyklus
do-while	dohile	k6eAd1	do-while
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc1	cyklus
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Napřed	napřed	k6eAd1	napřed
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
tělo	tělo	k1gNnSc1	tělo
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
testuje	testovat	k5eAaImIp3nS	testovat
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
platná	platný	k2eAgFnSc1d1	platná
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
platná	platný	k2eAgFnSc1d1	platná
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
za	za	k7c7	za
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
provede	provést	k5eAaPmIp3nS	provést
vždy	vždy	k6eAd1	vždy
nejméně	málo	k6eAd3	málo
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
formát	formát	k1gInSc1	formát
vypadá	vypadat	k5eAaPmIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Příkaz	příkaz	k1gInSc1	příkaz
break	break	k1gInSc1	break
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
opuštění	opuštění	k1gNnSc3	opuštění
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
platnost	platnost	k1gFnSc4	platnost
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
continue	continue	k6eAd1	continue
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
skoku	skok	k1gInSc2	skok
na	na	k7c4	na
konec	konec	k1gInSc4	konec
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
testování	testování	k1gNnSc1	testování
podmínky	podmínka	k1gFnSc2	podmínka
(	(	kIx(	(
<g/>
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
for	forum	k1gNnPc2	forum
skočí	skočit	k5eAaPmIp3nS	skočit
na	na	k7c6	na
inkrementaci	inkrementace	k1gFnSc6	inkrementace
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
testuje	testovat	k5eAaImIp3nS	testovat
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
v	v	k7c6	v
cyklech	cyklus	k1gInPc6	cyklus
while	while	k6eAd1	while
a	a	k8xC	a
do-while	dohile	k6eAd1	do-while
skočí	skočit	k5eAaPmIp3nS	skočit
na	na	k7c4	na
test	test	k1gInSc4	test
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
syntaxe	syntaxe	k1gFnSc1	syntaxe
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Jazyky	jazyk	k1gInPc1	jazyk
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
Objective-C	Objective-C	k1gFnSc1	Objective-C
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
vlnu	vlna	k1gFnSc4	vlna
popularity	popularita	k1gFnSc2	popularita
objektově	objektově	k6eAd1	objektově
orientovaného	orientovaný	k2eAgNnSc2d1	orientované
programování	programování	k1gNnSc2	programování
(	(	kIx(	(
<g/>
OOP	OOP	kA	OOP
<g/>
)	)	kIx)	)
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
rozšíření	rozšíření	k1gNnSc2	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
implementována	implementovat	k5eAaImNgFnS	implementovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
preprocesoru	preprocesor	k1gInSc2	preprocesor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
byl	být	k5eAaImAgInS	být
nejprve	nejprve	k6eAd1	nejprve
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
C	C	kA	C
a	a	k8xC	a
poté	poté	k6eAd1	poté
zkompilován	zkompilován	k2eAgInSc1d1	zkompilován
kompilátorem	kompilátor	k1gInSc7	kompilátor
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sice	sice	k8xC	sice
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
C	C	kA	C
nejsou	být	k5eNaImIp3nP	být
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
které	který	k3yRgFnPc1	který
částečně	částečně	k6eAd1	částečně
přejaly	přejmout	k5eAaPmAgFnP	přejmout
syntaxi	syntaxe	k1gFnSc4	syntaxe
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
Java	Java	k1gMnSc1	Java
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
♯	♯	k?	♯
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jména	jméno	k1gNnPc4	jméno
některých	některý	k3yIgFnPc2	některý
knihovních	knihovní	k2eAgFnPc2d1	knihovní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
PHP	PHP	kA	PHP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
<g/>
++	++	k?	++
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
ale	ale	k9	ale
úplně	úplně	k6eAd1	úplně
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nejvíce	nejvíce	k6eAd1	nejvíce
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
kompilací	kompilace	k1gFnSc7	kompilace
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
C	C	kA	C
<g/>
++	++	k?	++
definuje	definovat	k5eAaBmIp3nS	definovat
mnohem	mnohem	k6eAd1	mnohem
striktnější	striktní	k2eAgNnPc4d2	striktnější
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
přetypování	přetypování	k1gNnSc4	přetypování
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
OOP	OOP	kA	OOP
implementuje	implementovat	k5eAaImIp3nS	implementovat
C	C	kA	C
<g/>
++	++	k?	++
také	také	k9	také
generické	generický	k2eAgNnSc1d1	generické
a	a	k8xC	a
procedurální	procedurální	k2eAgNnSc1d1	procedurální
programování	programování	k1gNnSc1	programování
<g/>
.	.	kIx.	.
</s>
<s>
Objective-C	Objective-C	k?	Objective-C
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
C	C	kA	C
a	a	k8xC	a
Smalltalk	Smalltalk	k1gInSc1	Smalltalk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
s	s	k7c7	s
C	C	kA	C
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zkompilovat	zkompilovat	k5eAaPmF	zkompilovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
zdrojový	zdrojový	k2eAgInSc4d1	zdrojový
text	text	k1gInSc4	text
s	s	k7c7	s
překladačem	překladač	k1gMnSc7	překladač
Objective-C	Objective-C	k1gMnSc7	Objective-C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntaxe	syntaxe	k1gFnSc1	syntaxe
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
preprocesoru	preprocesor	k1gInSc6	preprocesor
<g/>
,	,	kIx,	,
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
deklarací	deklarace	k1gFnPc2	deklarace
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
volání	volání	k1gNnPc2	volání
funkcí	funkce	k1gFnPc2	funkce
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
C	C	kA	C
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc1d1	orientované
programování	programování	k1gNnSc1	programování
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Smalltalk	Smalltalka	k1gFnPc2	Smalltalka
<g/>
.	.	kIx.	.
</s>
<s>
Preprocesor	preprocesor	k1gInSc1	preprocesor
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
Překladač	překladač	k1gInSc1	překladač
Sada	sada	k1gFnSc1	sada
kompilátorů	kompilátor	k1gMnPc2	kompilátor
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gMnSc1	gnu
Volatile	Volatil	k1gMnSc5	Volatil
Standardní	standardní	k2eAgFnSc1d1	standardní
knihovna	knihovna	k1gFnSc1	knihovna
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
Brian	Brian	k1gMnSc1	Brian
W.	W.	kA	W.
Kernighan	Kernighan	k1gMnSc1	Kernighan
<g/>
,	,	kIx,	,
Dennis	Dennis	k1gFnSc1	Dennis
M.	M.	kA	M.
Ritchie	Ritchie	k1gFnSc1	Ritchie
<g/>
:	:	kIx,	:
The	The	k1gMnSc3	The
C	C	kA	C
Programming	Programming	k1gInSc1	Programming
Language	language	k1gFnSc1	language
<g/>
,	,	kIx,	,
Second	Second	k1gInSc1	Second
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
Prentice-Hall	Prentice-Hall	k1gInSc1	Prentice-Hall
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-13-110370-9	[number]	k4	0-13-110370-9
Pavel	Pavel	k1gMnSc1	Pavel
Herout	Herout	k1gMnSc1	Herout
<g/>
:	:	kIx,	:
Učebnice	učebnice	k1gFnSc1	učebnice
jazyka	jazyk	k1gInSc2	jazyk
<g />
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
,	,	kIx,	,
KOPP	KOPP	kA	KOPP
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7232-220-6	[number]	k4	80-7232-220-6
Literatura	literatura	k1gFnSc1	literatura
k	k	k7c3	k
jazykům	jazyk	k1gInPc3	jazyk
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
C	C	kA	C
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Specifikace	specifikace	k1gFnSc2	specifikace
ANSI	ANSI	kA	ANSI
C	C	kA	C
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
Skripta	skriptum	k1gNnSc2	skriptum
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
doc.	doc.	kA	doc.
<g/>
Ing.	ing.	kA	ing.
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Vogel	Vogel	k1gMnSc1	Vogel
<g/>
,	,	kIx,	,
<g/>
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Fakultu	fakulta	k1gFnSc4	fakulta
strojní	strojní	k2eAgMnSc1d1	strojní
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
Seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
jazycích	jazyk	k1gInPc6	jazyk
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
na	na	k7c4	na
linuxsoft	linuxsoft	k1gInSc4	linuxsoft
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
Mgr.	Mgr.	kA	Mgr.
Jan	Jan	k1gMnSc1	Jan
Němec	Němec	k1gMnSc1	Němec
<g/>
)	)	kIx)	)
Objasnění	objasnění	k1gNnSc1	objasnění
klíčového	klíčový	k2eAgNnSc2d1	klíčové
slova	slovo	k1gNnSc2	slovo
restrict	restricta	k1gFnPc2	restricta
v	v	k7c6	v
C99	C99	k1gFnSc6	C99
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
C	C	kA	C
(	(	kIx(	(
<g/>
programming	programming	k1gInSc1	programming
language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
