<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vír	Vír	k1gInSc1	Vír
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
Vír	Vír	k1gInSc1	Vír
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tělesa	těleso	k1gNnSc2	těleso
hráze	hráz	k1gFnSc2	hráz
leží	ležet	k5eAaImIp3nS	ležet
osada	osada	k1gFnSc1	osada
Hamry	Hamry	k1gInPc1	Hamry
u	u	k7c2	u
Víru	Vír	k1gInSc2	Vír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
nádrží	nádrž	k1gFnSc7	nádrž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
torzo	torzo	k1gNnSc1	torzo
hradu	hrad	k1gInSc2	hrad
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
<g/>
.	.	kIx.	.
</s>
<s>
Hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
za	za	k7c7	za
meandrem	meandr	k1gInSc7	meandr
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
s	s	k7c7	s
přehradním	přehradní	k2eAgNnSc7d1	přehradní
jezerem	jezero	k1gNnSc7	jezero
je	být	k5eAaImIp3nS	být
propojena	propojit	k5eAaPmNgFnS	propojit
tunelem	tunel	k1gInSc7	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
stavby	stavba	k1gFnSc2	stavba
je	být	k5eAaImIp3nS	být
vyrovnávání	vyrovnávání	k1gNnSc1	vyrovnávání
toku	tok	k1gInSc2	tok
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
nádrže	nádrž	k1gFnSc2	nádrž
Vír	Vír	k1gInSc1	Vír
I.	I.	kA	I.
V	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
je	být	k5eAaImIp3nS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
jedna	jeden	k4xCgFnSc1	jeden
Kaplanova	Kaplanův	k2eAgFnSc1d1	Kaplanova
turbína	turbína	k1gFnSc1	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
0,730	[number]	k4	0,730
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vír	Vír	k1gInSc1	Vír
II	II	kA	II
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Vír	Vír	k1gInSc1	Vír
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Povodí	povodí	k1gNnSc2	povodí
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
</s>
