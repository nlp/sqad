<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bojovalo	bojovat	k5eAaImAgNnS	bojovat
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
mužů	muž	k1gMnPc2	muž
odvedených	odvedený	k2eAgMnPc2d1	odvedený
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
padlo	padnout	k5eAaImAgNnS	padnout
138	[number]	k4	138
000	[number]	k4	000
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
asi	asi	k9	asi
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
tisíce	tisíc	k4xCgInSc2	tisíc
(	(	kIx(	(
<g/>
jen	jen	k9	jen
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
v	v	k7c6	v
Československých	československý	k2eAgFnPc6d1	Československá
legiích	legie	k1gFnPc6	legie
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
válečného	válečný	k2eAgNnSc2d1	válečné
soupeření	soupeření	k1gNnSc2	soupeření
<g/>
.	.	kIx.	.
</s>
