<s>
Archie	Archie	k1gFnSc1
MacDonald	Macdonald	k1gMnSc1
(	(	kIx(
<g/>
zápasník	zápasník	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Archie	Archie	k1gFnSc1
MacDonald	Macdonald	k1gMnSc1
Narození	narození	k1gNnPc4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1895	#num#	k4
<g/>
Teangue	Teangue	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
1965	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
69	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Inverness	Inverness	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
zápasník	zápasník	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Zápas	zápas	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
LOH	LOH	kA
1924	#num#	k4
</s>
<s>
volný	volný	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
,	,	kIx,
těžká	těžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Donald	Donald	k1gMnSc1
Archibald	Archibald	k1gMnSc1
„	„	k?
<g/>
Archie	Archie	k1gFnSc2
<g/>
“	“	k?
MacDonald	Macdonald	k1gMnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1895	#num#	k4
Saasaig	Saasaig	k1gInSc1
–	–	k?
1965	#num#	k4
Inverness	Invernessa	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
zápasník	zápasník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
vybojoval	vybojovat	k5eAaPmAgInS
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
ve	v	k7c6
volném	volný	k2eAgInSc6d1
stylu	styl	k1gInSc6
v	v	k7c6
těžké	těžký	k2eAgFnSc6d1
váze	váha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
na	na	k7c6
hrách	hra	k1gFnPc6
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
kategorii	kategorie	k1gFnSc6
vypadl	vypadnout	k5eAaPmAgMnS
v	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
a	a	k8xC
obsadil	obsadit	k5eAaPmAgMnS
tak	tak	k9
dělené	dělený	k2eAgFnPc4d1
páté	pátá	k1gFnPc4
<g/>
,	,	kIx,
resp.	resp.	kA
poslední	poslední	k2eAgNnSc1d1
místo	místo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Archie	Archie	k1gFnSc1
MacDonald	Macdonald	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
