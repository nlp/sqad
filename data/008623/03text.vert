<p>
<s>
Mons	Mons	k1gInSc1	Mons
<g/>
.	.	kIx.	.
<g/>
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Apicella	Apicello	k1gNnPc4	Apicello
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
katolický	katolický	k2eAgMnSc1d1	katolický
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Velletri	Velletri	k1gNnSc2	Velletri
<g/>
–	–	k?	–
<g/>
Segni	Segň	k1gMnSc3	Segň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
tam	tam	k6eAd1	tam
také	také	k9	také
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Almo	alma	k1gFnSc5	alma
Collegio	Collegio	k1gMnSc1	Collegio
Caprinica	Caprinicus	k1gMnSc4	Caprinicus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Papežské	papežský	k2eAgFnSc6d1	Papežská
Gregoriánské	gregoriánský	k2eAgFnSc6d1	gregoriánská
univerzitě	univerzita	k1gFnSc6	univerzita
získal	získat	k5eAaPmAgMnS	získat
licentiát	licentiát	k1gInSc4	licentiát
z	z	k7c2	z
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1972	[number]	k4	1972
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
pro	pro	k7c4	pro
diecézi	diecéze	k1gFnSc4	diecéze
Řím	Řím	k1gInSc1	Řím
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
San	San	k1gFnSc2	San
Clemente	Clement	k1gInSc5	Clement
Papa	papa	k1gMnSc1	papa
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
titulárním	titulární	k2eAgMnSc7d1	titulární
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Novigradským	Novigradský	k2eAgFnPc3d1	Novigradský
Ugem	Ug	k1gFnPc3	Ug
Poletti	Poletť	k1gFnSc2	Poletť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
kardinálem	kardinál	k1gMnSc7	kardinál
a	a	k8xC	a
generálním	generální	k2eAgMnSc7d1	generální
vikářem	vikář	k1gMnSc7	vikář
diecéze	diecéze	k1gFnSc2	diecéze
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972-1977	[number]	k4	1972-1977
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
farní	farní	k2eAgMnSc1d1	farní
vikář	vikář	k1gMnSc1	vikář
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
San	San	k1gMnSc1	San
Giovanni	Giovann	k1gMnPc1	Giovann
Battista	Battista	k1gMnSc1	Battista
De	De	k?	De
Rossi	Rosse	k1gFnSc4	Rosse
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977-1985	[number]	k4	1977-1985
v	v	k7c6	v
San	San	k1gFnSc6	San
Filippo	Filippa	k1gFnSc5	Filippa
Neri	Ner	k1gFnSc2	Ner
alla	all	k2eAgNnPc4d1	all
Pineta	Pinet	k2eAgNnPc4d1	Pinet
Sacchetti	Sacchetti	k1gNnPc4	Sacchetti
<g/>
.	.	kIx.	.
</s>
<s>
Vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
náboženství	náboženství	k1gNnSc1	náboženství
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
Grazia	Grazius	k1gMnSc2	Grazius
Deledda	Deledda	k1gMnSc1	Deledda
a	a	k8xC	a
Antonio	Antonio	k1gMnSc1	Antonio
Rosmini	Rosmin	k2eAgMnPc1d1	Rosmin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
kněžích	kněz	k1gMnPc6	kněz
Římské	římský	k2eAgFnPc4d1	římská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1996	[number]	k4	1996
ho	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
pomocným	pomocný	k2eAgMnSc7d1	pomocný
biskupem	biskup	k1gMnSc7	biskup
Římské	římský	k2eAgFnSc2d1	římská
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
titulárním	titulární	k2eAgMnSc7d1	titulární
biskupem	biskup	k1gMnSc7	biskup
Ierafiským	Ierafiský	k1gMnSc7	Ierafiský
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byl	být	k5eAaImAgMnS	být
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
biskupa	biskup	k1gMnSc4	biskup
v	v	k7c6	v
Lateránské	lateránský	k2eAgFnSc6d1	Lateránská
bazilice	bazilika	k1gFnSc6	bazilika
kardinálem	kardinál	k1gMnSc7	kardinál
Camillem	Camill	k1gMnSc7	Camill
Ruinim	Ruini	k1gNnSc7	Ruini
a	a	k8xC	a
spolusvětiteli	spolusvětitel	k1gMnSc3	spolusvětitel
byli	být	k5eAaImAgMnP	být
Cesare	Cesar	k1gMnSc5	Cesar
Nosiglia	Nosiglium	k1gNnSc2	Nosiglium
titulární	titulární	k2eAgMnSc1d1	titulární
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Victoriánským	Victoriánský	k2eAgMnSc7d1	Victoriánský
a	a	k8xC	a
Diego	Diego	k6eAd1	Diego
Natale	Natal	k1gInSc5	Natal
Bona	bona	k1gFnSc1	bona
biskup	biskup	k1gInSc1	biskup
Saluzza	Saluzza	k1gFnSc1	Saluzza
<g/>
.	.	kIx.	.
</s>
<s>
Biskupem	biskup	k1gMnSc7	biskup
Velletri	Velletri	k1gNnSc2	Velletri
<g/>
–	–	k?	–
<g/>
Segni	Segň	k1gMnSc3	Segň
byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc1d1	zvolen
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
biskupa	biskup	k1gInSc2	biskup
Andrea	Andrea	k1gFnSc1	Andrea
Maria	Maria	k1gFnSc1	Maria
Erby	erb	k1gInPc1	erb
papežem	papež	k1gMnSc7	papež
Benediktem	Benedikt	k1gMnSc7	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Apicella	Apicello	k1gNnPc1	Apicello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
diecéze	diecéze	k1gFnSc2	diecéze
Velletri	Velletri	k1gNnSc2	Velletri
<g/>
–	–	k?	–
<g/>
Segni	Segň	k1gMnSc3	Segň
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
catholic-hierarchy	catholicierarch	k1gInPc1	catholic-hierarch
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
GCatholic	GCatholice	k1gFnPc2	GCatholice
</s>
</p>
