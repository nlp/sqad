<s>
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
komerční	komerční	k2eAgFnSc1d1	komerční
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
vysílání	vysílání	k1gNnSc4	vysílání
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
vysílání	vysílání	k1gNnSc4	vysílání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
prvním	první	k4xOgMnSc7	první
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
českou	český	k2eAgFnSc7d1	Česká
celostátní	celostátní	k2eAgFnSc7d1	celostátní
soukromou	soukromý	k2eAgFnSc7d1	soukromá
televizní	televizní	k2eAgFnSc7d1	televizní
stanicí	stanice	k1gFnSc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
léta	léto	k1gNnPc4	léto
je	být	k5eAaImIp3nS	být
nejsledovanější	sledovaný	k2eAgInSc1d3	nejsledovanější
českou	český	k2eAgFnSc7d1	Česká
televizní	televizní	k2eAgFnSc7d1	televizní
stanicí	stanice	k1gFnSc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
provozovatelem	provozovatel	k1gMnSc7	provozovatel
je	být	k5eAaImIp3nS	být
mediální	mediální	k2eAgFnSc1d1	mediální
společnost	společnost	k1gFnSc1	společnost
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
společnost	společnost	k1gFnSc4	společnost
CME	CME	kA	CME
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Kříženeckého	kříženecký	k2eAgNnSc2d1	kříženecký
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
5	[number]	k4	5
na	na	k7c6	na
Barrandově	Barrandov	k1gInSc6	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
i	i	k8xC	i
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
Christoph	Christoph	k1gMnSc1	Christoph
Mainusch	Mainusch	k1gMnSc1	Mainusch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
jednatelem	jednatel	k1gMnSc7	jednatel
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yQgFnSc4	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Američan	Američan	k1gMnSc1	Američan
Mark	Mark	k1gMnSc1	Mark
Palmer	Palmer	k1gMnSc1	Palmer
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Hunčík	Hunčík	k1gMnSc1	Hunčík
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
kolegy	kolega	k1gMnPc7	kolega
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
zakládají	zakládat	k5eAaImIp3nP	zakládat
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
ručením	ručení	k1gNnSc7	ručení
omezeným	omezený	k2eAgNnSc7d1	omezené
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
(	(	kIx(	(
<g/>
Central	Central	k1gFnSc1	Central
European	European	k1gMnSc1	European
Television	Television	k1gInSc1	Television
for	forum	k1gNnPc2	forum
21	[number]	k4	21
<g/>
st	st	kA	st
Century	Centura	k1gFnSc2	Centura
<g/>
)	)	kIx)	)
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
zřídit	zřídit	k5eAaPmF	zřídit
komerční	komerční	k2eAgFnSc2d1	komerční
televize	televize	k1gFnSc2	televize
v	v	k7c6	v
6	[number]	k4	6
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
finální	finální	k2eAgFnSc6d1	finální
žádosti	žádost	k1gFnSc6	žádost
o	o	k7c4	o
licenci	licence	k1gFnSc4	licence
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
<g/>
.	.	kIx.	.
</s>
<s>
Palmer	Palmer	k1gMnSc1	Palmer
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
Central	Central	k1gMnSc1	Central
European	European	k1gMnSc1	European
Development	Development	k1gMnSc1	Development
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
CEDC	CEDC	kA	CEDC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
spojitost	spojitost	k1gFnSc4	spojitost
i	i	k9	i
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
CME	CME	kA	CME
Ronaldem	Ronald	k1gMnSc7	Ronald
Lauderem	Lauder	k1gMnSc7	Lauder
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
CEDC	CEDC	kA	CEDC
a	a	k8xC	a
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
společně	společně	k6eAd1	společně
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
spořitelnou	spořitelna	k1gFnSc7	spořitelna
zakládají	zakládat	k5eAaImIp3nP	zakládat
Českou	český	k2eAgFnSc4d1	Česká
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
televizní	televizní	k2eAgFnSc4d1	televizní
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
ČNTS	ČNTS	kA	ČNTS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
CEDC	CEDC	kA	CEDC
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
66	[number]	k4	66
<g/>
%	%	kIx~	%
vlastnických	vlastnický	k2eAgNnPc2d1	vlastnické
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
spořitelna	spořitelna	k1gFnSc1	spořitelna
22	[number]	k4	22
<g/>
%	%	kIx~	%
a	a	k8xC	a
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
12	[number]	k4	12
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
ČNTS	ČNTS	kA	ČNTS
tedy	tedy	k9	tedy
začala	začít	k5eAaPmAgFnS	začít
provozovat	provozovat	k5eAaImF	provozovat
novou	nový	k2eAgFnSc4d1	nová
televizi	televize	k1gFnSc4	televize
Nova	nova	k1gFnSc1	nova
přes	přes	k7c4	přes
licenci	licence	k1gFnSc4	licence
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vydělávající	vydělávající	k2eAgFnSc1d1	vydělávající
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
CME	CME	kA	CME
lákavé	lákavý	k2eAgFnPc1d1	lákavá
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
získat	získat	k5eAaPmF	získat
licenci	licence	k1gFnSc4	licence
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
CME	CME	kA	CME
získala	získat	k5eAaPmAgFnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
společnost	společnost	k1gFnSc4	společnost
CEDC	CEDC	kA	CEDC
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
již	již	k6eAd1	již
figurovala	figurovat	k5eAaImAgFnS	figurovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
podíl	podíl	k1gInSc4	podíl
České	český	k2eAgFnSc2d1	Česká
spořitelny	spořitelna	k1gFnSc2	spořitelna
za	za	k7c4	za
36	[number]	k4	36
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
ČNTS	ČNTS	kA	ČNTS
88	[number]	k4	88
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
půjčila	půjčit	k5eAaPmAgFnS	půjčit
Vladimíru	Vladimíra	k1gFnSc4	Vladimíra
Železnému	Železný	k1gMnSc3	Železný
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
velkou	velký	k2eAgFnSc4d1	velká
částku	částka	k1gFnSc4	částka
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vyplatit	vyplatit	k5eAaPmF	vyplatit
zbylé	zbylý	k2eAgMnPc4d1	zbylý
vlastníky	vlastník	k1gMnPc4	vlastník
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
jediným	jediný	k2eAgMnSc7d1	jediný
vlastníkem	vlastník	k1gMnSc7	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
tak	tak	k9	tak
provedl	provést	k5eAaPmAgInS	provést
a	a	k8xC	a
CME	CME	kA	CME
mu	on	k3xPp3gMnSc3	on
dluh	dluh	k1gInSc4	dluh
odpustila	odpustit	k5eAaPmAgFnS	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Dvanácti	dvanáct	k4xCc3	dvanáct
procentní	procentní	k2eAgInPc4d1	procentní
podíl	podíl	k1gInSc4	podíl
společnosti	společnost	k1gFnSc2	společnost
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
podíl	podíl	k1gInSc4	podíl
jednoho	jeden	k4xCgNnSc2	jeden
procenta	procento	k1gNnSc2	procento
pro	pro	k7c4	pro
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
5,2	[number]	k4	5,2
<g/>
%	%	kIx~	%
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Železného	železný	k2eAgMnSc2d1	železný
-	-	kIx~	-
Nova	nova	k1gFnSc1	nova
Consulting	Consulting	k1gInSc1	Consulting
a	a	k8xC	a
5,2	[number]	k4	5,2
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
zbylých	zbylý	k2eAgMnPc2d1	zbylý
5	[number]	k4	5
společníků	společník	k1gMnPc2	společník
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
vzápětí	vzápětí	k6eAd1	vzápětí
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
CME	CME	kA	CME
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
i	i	k8xC	i
podíl	podíl	k1gInSc4	podíl
firmy	firma	k1gFnSc2	firma
Nova	nova	k1gFnSc1	nova
Consulting	Consulting	k1gInSc1	Consulting
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
vynášelo	vynášet	k5eAaImAgNnS	vynášet
Vladimíru	Vladimír	k1gMnSc3	Vladimír
Železnému	Železný	k1gMnSc3	Železný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
vysílání	vysílání	k1gNnSc4	vysílání
nejúspěšnější	úspěšný	k2eAgFnSc2d3	nejúspěšnější
komerční	komerční	k2eAgFnSc2d1	komerční
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
podpory	podpora	k1gFnPc4	podpora
Američanů	Američan	k1gMnPc2	Američan
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
podnikání	podnikání	k1gNnSc1	podnikání
prodají	prodat	k5eAaPmIp3nP	prodat
<g/>
.	.	kIx.	.
</s>
<s>
CME	CME	kA	CME
mu	on	k3xPp3gMnSc3	on
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
určitou	určitý	k2eAgFnSc4d1	určitá
částku	částka	k1gFnSc4	částka
může	moct	k5eAaImIp3nS	moct
dělat	dělat	k5eAaImF	dělat
pro	pro	k7c4	pro
nového	nový	k2eAgMnSc4d1	nový
majitele	majitel	k1gMnSc4	majitel
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
ale	ale	k8xC	ale
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
hospodaření	hospodaření	k1gNnSc1	hospodaření
firem	firma	k1gFnPc2	firma
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
ČNTS	ČNTS	kA	ČNTS
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
a	a	k8xC	a
CME	CME	kA	CME
si	se	k3xPyFc3	se
vyčíslilo	vyčíslit	k5eAaPmAgNnS	vyčíslit
škodu	škoda	k1gFnSc4	škoda
na	na	k7c4	na
69	[number]	k4	69
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
obrovský	obrovský	k2eAgInSc4d1	obrovský
konflikt	konflikt	k1gInSc4	konflikt
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
k	k	k7c3	k
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
se	se	k3xPyFc4	se
Železný	Železný	k1gMnSc1	Železný
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Novu	nova	k1gFnSc4	nova
CME	CME	kA	CME
nenechá	nechat	k5eNaPmIp3nS	nechat
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
MEF	MEF	kA	MEF
Holding	holding	k1gInSc1	holding
a	a	k8xC	a
Investiční	investiční	k2eAgFnSc7d1	investiční
a	a	k8xC	a
Poštovní	poštovní	k2eAgFnSc7d1	poštovní
bankou	banka	k1gFnSc7	banka
založit	založit	k5eAaPmF	založit
novou	nový	k2eAgFnSc4d1	nová
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nahradí	nahradit	k5eAaPmIp3nS	nahradit
ČNTS	ČNTS	kA	ČNTS
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc4d1	Česká
produkční	produkční	k2eAgNnSc1d1	produkční
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
dostal	dostat	k5eAaPmAgMnS	dostat
potřebné	potřebný	k2eAgFnPc4d1	potřebná
finance	finance	k1gFnPc4	finance
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
vysílání	vysílání	k1gNnSc2	vysílání
a	a	k8xC	a
čekal	čekat	k5eAaImAgInS	čekat
až	až	k9	až
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
vypovědět	vypovědět	k5eAaPmF	vypovědět
ČNTS	ČNTS	kA	ČNTS
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
když	když	k8xS	když
ČNSTS	ČNSTS	kA	ČNSTS
nedodala	dodat	k5eNaPmAgFnS	dodat
denní	denní	k2eAgInSc4d1	denní
vysílací	vysílací	k2eAgInSc4d1	vysílací
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
toho	ten	k3xDgNnSc2	ten
okamžitě	okamžitě	k6eAd1	okamžitě
přesunul	přesunout	k5eAaPmAgMnS	přesunout
vysílání	vysílání	k1gNnSc4	vysílání
Novy	nova	k1gFnSc2	nova
na	na	k7c4	na
Barrandov	Barrandov	k1gInSc4	Barrandov
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
bez	bez	k7c2	bez
ČNTS	ČNTS	kA	ČNTS
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
CME	CME	kA	CME
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
ovšem	ovšem	k9	ovšem
nenechala	nechat	k5eNaPmAgFnS	nechat
líbit	líbit	k5eAaImF	líbit
a	a	k8xC	a
podávala	podávat	k5eAaImAgFnS	podávat
různé	různý	k2eAgFnPc4d1	různá
žaloby	žaloba	k1gFnPc4	žaloba
a	a	k8xC	a
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
sledovanosti	sledovanost	k1gFnSc2	sledovanost
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
zažíval	zažívat	k5eAaImAgInS	zažívat
konflikty	konflikt	k1gInPc4	konflikt
mezi	mezi	k7c7	mezi
vlastníkem	vlastník	k1gMnSc7	vlastník
licence	licence	k1gFnSc2	licence
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
CME	CME	kA	CME
Ronalda	Ronalda	k1gMnSc1	Ronalda
S.	S.	kA	S.
Laudera	Laudera	k1gFnSc1	Laudera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozjezd	rozjezd	k1gInSc4	rozjezd
televize	televize	k1gFnSc1	televize
financovala	financovat	k5eAaBmAgFnS	financovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgInPc2	tento
sporů	spor	k1gInPc2	spor
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
arbitráži	arbitráž	k1gFnSc6	arbitráž
odsouzena	odsouzen	k2eAgFnSc1d1	odsouzena
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
k	k	k7c3	k
zaplacení	zaplacení	k1gNnSc3	zaplacení
odškodného	odškodné	k1gNnSc2	odškodné
společnosti	společnost	k1gFnSc2	společnost
CME	CME	kA	CME
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
10	[number]	k4	10
mld.	mld.	k?	mld.
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
RRTV	RRTV	kA	RRTV
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
licenci	licence	k1gFnSc6	licence
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
do	do	k7c2	do
firem	firma	k1gFnPc2	firma
kolem	kolem	k7c2	kolem
Novy	nova	k1gFnSc2	nova
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
investiční	investiční	k2eAgFnSc1d1	investiční
společnost	společnost	k1gFnSc1	společnost
PPF	PPF	kA	PPF
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
MEF	MEF	kA	MEF
Holding	holding	k1gInSc1	holding
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
vliv	vliv	k1gInSc4	vliv
nad	nad	k7c7	nad
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
odvolán	odvolat	k5eAaPmNgMnS	odvolat
z	z	k7c2	z
postu	post	k1gInSc2	post
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
z	z	k7c2	z
PPF	PPF	kA	PPF
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
kupuje	kupovat	k5eAaImIp3nS	kupovat
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
CME	CME	kA	CME
společnost	společnost	k1gFnSc4	společnost
PPF	PPF	kA	PPF
její	její	k3xOp3gInSc4	její
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
ČNTS	ČNTS	kA	ČNTS
a	a	k8xC	a
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
tím	ten	k3xDgNnSc7	ten
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
PPF	PPF	kA	PPF
později	pozdě	k6eAd2	pozdě
vyplácí	vyplácet	k5eAaImIp3nS	vyplácet
i	i	k9	i
MEF	MEF	kA	MEF
Holding	holding	k1gInSc1	holding
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
85	[number]	k4	85
<g/>
%	%	kIx~	%
vliv	vliv	k1gInSc4	vliv
nad	nad	k7c7	nad
Novou	Nová	k1gFnSc7	Nová
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
PPF	PPF	kA	PPF
prodává	prodávat	k5eAaImIp3nS	prodávat
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
opět	opět	k6eAd1	opět
společnosti	společnost	k1gFnSc2	společnost
CME	CME	kA	CME
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gInSc4	její
celý	celý	k2eAgInSc4d1	celý
podíl	podíl	k1gInSc4	podíl
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
CME	CME	kA	CME
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
i	i	k9	i
zbývající	zbývající	k2eAgInPc4d1	zbývající
podíly	podíl	k1gInPc4	podíl
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
vlastníkem	vlastník	k1gMnSc7	vlastník
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Generálním	generální	k2eAgInSc7d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zapnula	zapnout	k5eAaPmAgFnS	zapnout
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Železného	Železný	k1gMnSc2	Železný
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
start	start	k1gInSc1	start
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ovšem	ovšem	k9	ovšem
až	až	k9	až
v	v	k7c4	v
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
živým	živý	k2eAgInSc7d1	živý
přenosem	přenos	k1gInSc7	přenos
z	z	k7c2	z
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
vysílání	vysílání	k1gNnSc1	vysílání
v	v	k7c6	v
Měšťanské	měšťanský	k2eAgFnSc6d1	měšťanská
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
půl	půl	k1xP	půl
osmé	osmý	k4xOgFnSc2	osmý
večer	večer	k6eAd1	večer
odstartovaly	odstartovat	k5eAaPmAgFnP	odstartovat
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
televize	televize	k1gFnSc2	televize
Nova	novum	k1gNnSc2	novum
poprvé	poprvé	k6eAd1	poprvé
Televizní	televizní	k2eAgFnPc1d1	televizní
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zprávách	zpráva	k1gFnPc6	zpráva
následovaly	následovat	k5eAaImAgFnP	následovat
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
Počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
byla	být	k5eAaImAgFnS	být
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěrákových	Svěrákových	k2eAgMnSc2d1	Svěrákových
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
následoval	následovat	k5eAaImAgInS	následovat
hned	hned	k6eAd1	hned
i	i	k9	i
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
film	film	k1gInSc4	film
Krotitelé	krotitel	k1gMnPc1	krotitel
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
pak	pak	k6eAd1	pak
Nova	nova	k1gFnSc1	nova
uvedla	uvést	k5eAaPmAgFnS	uvést
erotický	erotický	k2eAgInSc4d1	erotický
magazín	magazín	k1gInSc4	magazín
Penthouse	Penthouse	k1gFnSc2	Penthouse
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vysílací	vysílací	k2eAgInSc1d1	vysílací
den	den	k1gInSc1	den
skončil	skončit	k5eAaPmAgInS	skončit
upoutávkou	upoutávka	k1gFnSc7	upoutávka
na	na	k7c4	na
sobotní	sobotní	k2eAgInSc4d1	sobotní
program	program	k1gInSc4	program
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
ranní	ranní	k2eAgFnSc4d1	ranní
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
Novy	nova	k1gFnSc2	nova
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
F1	F1	k1gFnSc2	F1
výpadky	výpadek	k1gInPc1	výpadek
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
Novou	Nová	k1gFnSc7	Nová
nebyl	být	k5eNaImAgMnS	být
zrovna	zrovna	k6eAd1	zrovna
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Novy	nova	k1gFnSc2	nova
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
ucházel	ucházet	k5eAaImAgMnS	ucházet
dříve	dříve	k6eAd2	dříve
o	o	k7c4	o
post	post	k1gInSc4	post
ředitele	ředitel	k1gMnSc2	ředitel
právě	právě	k6eAd1	právě
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Nova	nova	k1gFnSc1	nova
představila	představit	k5eAaPmAgFnS	představit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
začátcích	začátek	k1gInPc6	začátek
pořady	pořad	k1gInPc1	pořad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
až	až	k9	až
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
Televizní	televizní	k2eAgFnPc1d1	televizní
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
Počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
pouze	pouze	k6eAd1	pouze
vizuálně	vizuálně	k6eAd1	vizuálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
pevný	pevný	k2eAgInSc4d1	pevný
slot	slot	k1gInSc4	slot
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
vysílají	vysílat	k5eAaImIp3nP	vysílat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
pořady	pořad	k1gInPc7	pořad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
vysílány	vysílán	k2eAgInPc1d1	vysílán
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
investigativní	investigativní	k2eAgInSc1d1	investigativní
pořad	pořad	k1gInSc1	pořad
Snídaně	snídaně	k1gFnSc2	snídaně
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
<g/>
,	,	kIx,	,
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
Sedmička	sedmička	k1gFnSc1	sedmička
<g/>
,	,	kIx,	,
Občanské	občanský	k2eAgNnSc1d1	občanské
judo	judo	k1gNnSc1	judo
či	či	k8xC	či
pořad	pořad	k1gInSc1	pořad
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnPc7	ředitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uváděl	uvádět	k5eAaImAgMnS	uvádět
sám	sám	k3xTgMnSc1	sám
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Novy	nova	k1gFnSc2	nova
a	a	k8xC	a
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
zde	zde	k6eAd1	zde
divákům	divák	k1gMnPc3	divák
na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
dotazy	dotaz	k1gInPc4	dotaz
<g/>
,	,	kIx,	,
co	co	k8xS	co
televize	televize	k1gFnSc1	televize
bude	být	k5eAaImBp3nS	být
vysílat	vysílat	k5eAaImF	vysílat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
smí	smět	k5eAaImIp3nS	smět
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
i	i	k9	i
známé	známý	k2eAgInPc1d1	známý
americké	americký	k2eAgInPc1d1	americký
seriály	seriál	k1gInPc1	seriál
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
M.	M.	kA	M.
<g/>
A.S.	A.S.	k1gFnPc2	A.S.
<g/>
H.	H.	kA	H.
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
<g/>
,	,	kIx,	,
Tak	tak	k6eAd1	tak
jde	jít	k5eAaImIp3nS	jít
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
Colombo	Colomba	k1gMnSc5	Colomba
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vražda	vražda	k1gFnSc1	vražda
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaBmAgFnS	napsat
<g/>
,	,	kIx,	,
Dynastie	dynastie	k1gFnSc1	dynastie
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
potom	potom	k6eAd1	potom
Hospoda	Hospoda	k?	Hospoda
<g/>
,	,	kIx,	,
Nováci	Novák	k1gMnPc1	Novák
nebo	nebo	k8xC	nebo
Policajti	Policajti	k?	Policajti
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
Železný	Železný	k1gMnSc1	Železný
odvolán	odvolat	k5eAaPmNgMnS	odvolat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
shánět	shánět	k5eAaImF	shánět
si	se	k3xPyFc3	se
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
"	"	kIx"	"
<g/>
nové	nový	k2eAgFnSc2d1	nová
Novy	nova	k1gFnSc2	nova
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Železnému	Železný	k1gMnSc3	Železný
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
se	se	k3xPyFc4	se
odpojil	odpojit	k5eAaPmAgMnS	odpojit
od	od	k7c2	od
ČNST	ČNST	kA	ČNST
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
z	z	k7c2	z
garáží	garáž	k1gFnPc2	garáž
ve	v	k7c6	v
filmových	filmový	k2eAgInPc6d1	filmový
ateliérech	ateliér	k1gInPc6	ateliér
na	na	k7c6	na
Barrandově	Barrandov	k1gInSc6	Barrandov
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
vysílal	vysílat	k5eAaImAgInS	vysílat
dále	daleko	k6eAd2	daleko
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
pořady	pořad	k1gInPc4	pořad
Novy	nova	k1gFnSc2	nova
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
ČNST	ČNST	kA	ČNST
vlastnila	vlastnit	k5eAaImAgNnP	vlastnit
věškerá	věškerý	k2eAgNnPc1d1	věškeré
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
na	na	k7c4	na
divácky	divácky	k6eAd1	divácky
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k6eAd1	také
měla	mít	k5eAaImAgFnS	mít
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
všechny	všechen	k3xTgMnPc4	všechen
moderátory	moderátor	k1gMnPc4	moderátor
a	a	k8xC	a
redaktory	redaktor	k1gMnPc4	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
však	však	k9	však
nezahálel	zahálet	k5eNaImAgMnS	zahálet
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
začal	začít	k5eAaPmAgMnS	začít
moderátory	moderátor	k1gMnPc4	moderátor
přebírat	přebírat	k5eAaImF	přebírat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nahradil	nahradit	k5eAaPmAgMnS	nahradit
zcela	zcela	k6eAd1	zcela
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
s	s	k7c7	s
názvy	název	k1gInPc7	název
pořadů	pořad	k1gInPc2	pořad
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
přejmenováním	přejmenování	k1gNnSc7	přejmenování
-	-	kIx~	-
Televizní	televizní	k2eAgFnPc1d1	televizní
noviny	novina	k1gFnPc1	novina
změnily	změnit	k5eAaPmAgFnP	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
TN	TN	kA	TN
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
zase	zase	k9	zase
na	na	k7c4	na
Na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
noční	noční	k2eAgInSc4d1	noční
pořad	pořad	k1gInSc4	pořad
Tabu	tabu	k1gNnSc2	tabu
na	na	k7c4	na
Tabudka	Tabudek	k1gMnSc4	Tabudek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
jako	jako	k9	jako
moderátoři	moderátor	k1gMnPc1	moderátor
Televizních	televizní	k2eAgFnPc2d1	televizní
novin	novina	k1gFnPc2	novina
Lucie	Lucie	k1gFnSc1	Lucie
Borhyová	Borhyová	k1gFnSc1	Borhyová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
redaktorka	redaktorka	k1gFnSc1	redaktorka
a	a	k8xC	a
Rey	Rea	k1gFnPc1	Rea
Koranteng	Koranteng	k1gInSc1	Koranteng
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
Nově	nov	k1gInSc6	nov
uváděl	uvádět	k5eAaImAgMnS	uvádět
Počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vrátila	vrátit	k5eAaPmAgFnS	vrátit
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
bývalým	bývalý	k2eAgInPc3d1	bývalý
názvům	název	k1gInPc3	název
a	a	k8xC	a
nebo	nebo	k8xC	nebo
pořady	pořad	k1gInPc4	pořad
nahradila	nahradit	k5eAaPmAgFnS	nahradit
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbenými	oblíbený	k2eAgInPc7d1	oblíbený
pořady	pořad	k1gInPc7	pořad
byly	být	k5eAaImAgFnP	být
Ptákoviny	ptákovina	k1gFnPc4	ptákovina
<g/>
,	,	kIx,	,
Čundrcountry	Čundrcountr	k1gInPc4	Čundrcountr
show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
Eso	eso	k1gNnSc1	eso
<g/>
,	,	kIx,	,
Gumáci	Gumák	k1gMnPc1	Gumák
<g/>
,	,	kIx,	,
soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
pořad	pořad	k1gInSc1	pořad
Riskuj	riskovat	k5eAaBmRp2nS	riskovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Skopičiny	skopičina	k1gFnPc1	skopičina
<g/>
,	,	kIx,	,
Vabank	vabank	k1gInSc1	vabank
<g/>
,	,	kIx,	,
Tele	tele	k1gNnSc1	tele
Tele	tele	k1gNnSc1	tele
nebo	nebo	k8xC	nebo
Zlatíčka	zlatíčko	k1gNnSc2	zlatíčko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Nově	nov	k1gInSc6	nov
se	se	k3xPyFc4	se
objevovali	objevovat	k5eAaImAgMnP	objevovat
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
hvězdy	hvězda	k1gFnSc2	hvězda
jako	jako	k9	jako
Petr	Petr	k1gMnSc1	Petr
Nárožný	Nárožný	k2eAgMnSc1d1	Nárožný
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Sobota	Sobota	k1gMnSc1	Sobota
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Rychlý	Rychlý	k1gMnSc1	Rychlý
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Zedníček	Zedníček	k1gMnSc1	Zedníček
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Pergnerová	Pergnerová	k1gFnSc1	Pergnerová
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Mládek	Mládek	k1gMnSc1	Mládek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Trávníček	Trávníček	k1gMnSc1	Trávníček
nebo	nebo	k8xC	nebo
Sabina	Sabina	k1gFnSc1	Sabina
Laurinová	Laurinová	k1gFnSc1	Laurinová
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Sledovanost	sledovanost	k1gFnSc1	sledovanost
se	se	k3xPyFc4	se
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
rocích	rok	k1gInPc6	rok
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
60	[number]	k4	60
<g/>
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
divácích	divák	k1gMnPc6	divák
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
10	[number]	k4	10
<g/>
%	%	kIx~	%
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
s	s	k7c7	s
přehledem	přehled	k1gInSc7	přehled
byla	být	k5eAaImAgFnS	být
daleko	daleko	k6eAd1	daleko
nad	nad	k7c7	nad
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Éra	éra	k1gFnSc1	éra
pořadů	pořad	k1gInPc2	pořad
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Železného	Železný	k1gMnSc2	Železný
se	se	k3xPyFc4	se
nesla	nést	k5eAaImAgNnP	nést
v	v	k7c6	v
zábavném	zábavný	k2eAgMnSc6d1	zábavný
<g/>
,	,	kIx,	,
soutěžním	soutěžní	k2eAgMnSc6d1	soutěžní
či	či	k8xC	či
politickém	politický	k2eAgInSc6d1	politický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
odvolán	odvolat	k5eAaPmNgMnS	odvolat
z	z	k7c2	z
postu	post	k1gInSc2	post
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
ho	on	k3xPp3gMnSc4	on
dosazený	dosazený	k2eAgMnSc1d1	dosazený
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
PPF	PPF	kA	PPF
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pořady	pořad	k1gInPc1	pořad
vysílané	vysílaný	k2eAgInPc1d1	vysílaný
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Železného	Železný	k1gMnSc2	Železný
dále	daleko	k6eAd2	daleko
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
programu	program	k1gInSc6	program
již	již	k6eAd1	již
neobjevovaly	objevovat	k5eNaImAgFnP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc4	pořad
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnSc5	ředitel
se	se	k3xPyFc4	se
od	od	k7c2	od
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
vysílal	vysílat	k5eAaImAgInS	vysílat
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Volejte	volat	k5eAaImRp2nP	volat
Novu	nova	k1gFnSc4	nova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
nevystupoval	vystupovat	k5eNaImAgMnS	vystupovat
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
známé	známý	k2eAgFnPc1d1	známá
tváře	tvář	k1gFnPc1	tvář
spjaté	spjatý	k2eAgFnPc1d1	spjatá
s	s	k7c7	s
televizí	televize	k1gFnSc7	televize
Novou	Nová	k1gFnSc7	Nová
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přinesl	přinést	k5eAaPmAgInS	přinést
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
Nova	nova	k1gFnSc1	nova
první	první	k4xOgFnSc2	první
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
přibližněji	přibližně	k6eAd2	přibližně
první	první	k4xOgFnSc4	první
pěveckou	pěvecký	k2eAgFnSc4d1	pěvecká
soutěž	soutěž	k1gFnSc4	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc1	superstar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
obrovským	obrovský	k2eAgInSc7d1	obrovský
hitem	hit	k1gInSc7	hit
a	a	k8xC	a
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
konto	konto	k1gNnSc4	konto
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
hned	hned	k6eAd1	hned
3	[number]	k4	3
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
společně	společně	k6eAd1	společně
s	s	k7c7	s
TV	TV	kA	TV
Markíza	Markíz	k1gMnSc2	Markíz
společnou	společný	k2eAgFnSc4d1	společná
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
verzi	verze	k1gFnSc4	verze
Česko	Česko	k1gNnSc4	Česko
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
SuperStar	superstar	k1gFnSc1	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
ročníku	ročník	k1gInSc6	ročník
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc4	superstar
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xS	jako
moderátor	moderátor	k1gMnSc1	moderátor
Leoš	Leoš	k1gMnSc1	Leoš
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
moderátor	moderátor	k1gMnSc1	moderátor
dalších	další	k2eAgFnPc2d1	další
show	show	k1gFnPc2	show
i	i	k9	i
mimo	mimo	k7c4	mimo
SuperStar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Reality	realita	k1gFnPc1	realita
show	show	k1gFnPc2	show
diváky	divák	k1gMnPc7	divák
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
lákaly	lákat	k5eAaImAgFnP	lákat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
Novu	novum	k1gNnSc6	novum
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
další	další	k2eAgFnSc2d1	další
show	show	k1gFnSc2	show
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
Talentmania	Talentmanium	k1gNnSc2	Talentmanium
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
Bailando	Bailanda	k1gFnSc5	Bailanda
<g/>
,	,	kIx,	,
Vem	Vem	k?	Vem
si	se	k3xPyFc3	se
mě	já	k3xPp1nSc2	já
<g/>
!	!	kIx.	!
</s>
<s>
nebo	nebo	k8xC	nebo
X	X	kA	X
Factor	Factor	k1gInSc1	Factor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
taháku	tahák	k1gInSc2	tahák
Novy	nova	k1gFnSc2	nova
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
i	i	k9	i
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Big	Big	k1gFnSc2	Big
Brother	Brother	kA	Brother
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Nova	nova	k1gFnSc1	nova
spustila	spustit	k5eAaPmAgFnS	spustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
všimla	všimnout	k5eAaPmAgFnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
spouští	spouštět	k5eAaImIp3nS	spouštět
českou	český	k2eAgFnSc4d1	Česká
verzi	verze	k1gFnSc4	verze
VyVolených	vyvolený	k1gMnPc2	vyvolený
<g/>
.	.	kIx.	.
</s>
<s>
Nova	nova	k1gFnSc1	nova
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
show	show	k1gFnSc4	show
často	často	k6eAd1	často
vystavená	vystavený	k2eAgFnSc1d1	vystavená
placením	placení	k1gNnSc7	placení
pokut	pokuta	k1gFnPc2	pokuta
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Novu	nova	k1gFnSc4	nova
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
už	už	k6eAd1	už
ji	on	k3xPp3gFnSc4	on
neobnovila	obnovit	k5eNaPmAgFnS	obnovit
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
řadu	řada	k1gFnSc4	řada
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Prima	prima	k6eAd1	prima
VyVolené	vyvolený	k2eAgMnPc4d1	vyvolený
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc2	vedení
Novy	nova	k1gFnSc2	nova
pustilo	pustit	k5eAaPmAgNnS	pustit
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
opět	opět	k6eAd1	opět
k	k	k7c3	k
vlastní	vlastní	k2eAgFnSc3d1	vlastní
tvorbě	tvorba	k1gFnSc3	tvorba
seriálů	seriál	k1gInPc2	seriál
-	-	kIx~	-
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
Redakce	redakce	k1gFnSc1	redakce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přichází	přicházet	k5eAaImIp3nS	přicházet
s	s	k7c7	s
denním	denní	k2eAgInSc7d1	denní
seriálem	seriál	k1gInSc7	seriál
Ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
seriálem	seriál	k1gInSc7	seriál
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
nemocnice	nemocnice	k1gFnSc2	nemocnice
Ordinací	ordinace	k1gFnPc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
seriály	seriál	k1gInPc1	seriál
stanou	stanout	k5eAaPmIp3nP	stanout
oblíbenými	oblíbený	k2eAgFnPc7d1	oblíbená
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydrží	vydržet	k5eAaPmIp3nS	vydržet
jako	jako	k9	jako
nekonečné	konečný	k2eNgInPc4d1	nekonečný
seriály	seriál	k1gInPc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
seriály	seriál	k1gInPc7	seriál
byly	být	k5eAaImAgFnP	být
např.	např.	kA	např.
On	on	k3xPp3gMnSc1	on
je	on	k3xPp3gMnPc4	on
žena	žena	k1gFnSc1	žena
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Místo	místo	k1gNnSc1	místo
v	v	k7c6	v
životě	život	k1gInSc6	život
či	či	k8xC	či
Světla	světlo	k1gNnPc1	světlo
pasáže	pasáž	k1gFnSc2	pasáž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vydržely	vydržet	k5eAaPmAgFnP	vydržet
jednu	jeden	k4xCgFnSc4	jeden
či	či	k8xC	či
dvě	dva	k4xCgFnPc4	dva
řady	řada	k1gFnPc4	řada
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
pořadů	pořad	k1gInPc2	pořad
Nova	nova	k1gFnSc1	nova
spustila	spustit	k5eAaPmAgFnS	spustit
nové	nový	k2eAgInPc4d1	nový
pořady	pořad	k1gInPc4	pořad
jako	jako	k8xS	jako
Víkend	víkend	k1gInSc4	víkend
<g/>
,	,	kIx,	,
Koření	koření	k1gNnPc4	koření
<g/>
,	,	kIx,	,
Střepiny	střepina	k1gFnPc4	střepina
<g/>
,	,	kIx,	,
112	[number]	k4	112
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novu	nov	k1gInSc6	nov
přichází	přicházet	k5eAaImIp3nS	přicházet
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
seriály	seriál	k1gInPc4	seriál
jako	jako	k8xS	jako
Námořní	námořní	k2eAgFnSc1d1	námořní
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
pořádek	pořádek	k1gInSc1	pořádek
<g/>
:	:	kIx,	:
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
oběti	oběť	k1gFnPc4	oběť
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
začala	začít	k5eAaPmAgFnS	začít
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
vysílat	vysílat	k5eAaImF	vysílat
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
(	(	kIx(	(
<g/>
ranní	ranní	k2eAgNnSc4d1	ranní
<g/>
,	,	kIx,	,
odpolední	odpolední	k2eAgNnSc4d1	odpolední
i	i	k8xC	i
večerní	večerní	k2eAgNnSc4d1	večerní
<g/>
)	)	kIx)	)
v	v	k7c6	v
obrazovém	obrazový	k2eAgInSc6d1	obrazový
formátu	formát	k1gInSc6	formát
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
a	a	k8xC	a
v	v	k7c6	v
rozlišení	rozlišení	k1gNnSc6	rozlišení
HDTV	HDTV	kA	HDTV
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kompletní	kompletní	k2eAgInSc1d1	kompletní
přechod	přechod	k1gInSc1	přechod
u	u	k7c2	u
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
realizovala	realizovat	k5eAaBmAgFnS	realizovat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
televize	televize	k1gFnSc1	televize
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
začala	začít	k5eAaPmAgFnS	začít
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c6	na
vysílání	vysílání	k1gNnSc6	vysílání
druhého	druhý	k4xOgInSc2	druhý
celoplošného	celoplošný	k2eAgInSc2d1	celoplošný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
jí	on	k3xPp3gFnSc3	on
totiž	totiž	k9	totiž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zákonný	zákonný	k2eAgInSc4d1	zákonný
nárok	nárok	k1gInSc4	nárok
(	(	kIx(	(
<g/>
licence	licence	k1gFnPc4	licence
<g/>
)	)	kIx)	)
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
digitalizace	digitalizace	k1gFnSc2	digitalizace
a	a	k8xC	a
přechodu	přechod	k1gInSc2	přechod
z	z	k7c2	z
analogového	analogový	k2eAgInSc2d1	analogový
na	na	k7c4	na
digitální	digitální	k2eAgInSc4d1	digitální
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
do	do	k7c2	do
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vysílání	vysílání	k1gNnSc2	vysílání
tohoto	tento	k3xDgNnSc2	tento
druhého	druhý	k4xOgMnSc4	druhý
programu	program	k1gInSc6	program
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
nestanovil	stanovit	k5eNaPmAgInS	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
zahájila	zahájit	k5eAaPmAgFnS	zahájit
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
vysílání	vysílání	k1gNnSc2	vysílání
"	"	kIx"	"
<g/>
druhého	druhý	k4xOgInSc2	druhý
kanálu	kanál	k1gInSc2	kanál
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nova	nova	k1gFnSc1	nova
Cinema	Cinema	k1gFnSc1	Cinema
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
šířena	šířit	k5eAaImNgFnS	šířit
jen	jen	k9	jen
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
kabelových	kabelový	k2eAgInPc2d1	kabelový
operátorů	operátor	k1gInPc2	operátor
a	a	k8xC	a
na	na	k7c6	na
satelitní	satelitní	k2eAgFnSc6d1	satelitní
platformě	platforma	k1gFnSc6	platforma
CS	CS	kA	CS
Link	Link	k1gInSc1	Link
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
podala	podat	k5eAaPmAgFnS	podat
RRTV	RRTV	kA	RRTV
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
kompenzační	kompenzační	k2eAgFnSc4d1	kompenzační
licenci	licence	k1gFnSc4	licence
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgNnSc4d1	pozemní
digitální	digitální	k2eAgNnSc4d1	digitální
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
RRTV	RRTV	kA	RRTV
poté	poté	k6eAd1	poté
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
žádosti	žádost	k1gFnSc2	žádost
vyhověla	vyhovět	k5eAaPmAgFnS	vyhovět
a	a	k8xC	a
Nova	nova	k1gFnSc1	nova
Cinema	Cinema	k1gFnSc1	Cinema
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
v	v	k7c6	v
terestrickém	terestrický	k2eAgInSc6d1	terestrický
digitálním	digitální	k2eAgInSc6d1	digitální
multiplexu	multiplex	k1gInSc6	multiplex
2	[number]	k4	2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vysílá	vysílat	k5eAaImIp3nS	vysílat
i	i	k9	i
hlavní	hlavní	k2eAgInSc1d1	hlavní
kanál	kanál	k1gInSc1	kanál
Novy	nova	k1gFnSc2	nova
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
televize	televize	k1gFnSc1	televize
Barrandov	Barrandov	k1gInSc4	Barrandov
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
kanály	kanál	k1gInPc4	kanál
FTV	FTV	kA	FTV
Prima	prima	k1gFnSc1	prima
(	(	kIx(	(
<g/>
Prima	prima	k2eAgInSc1d1	prima
a	a	k8xC	a
Prima	prima	k2eAgInSc1d1	prima
Cool	Cool	k1gInSc1	Cool
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Nova	nova	k1gFnSc1	nova
nezapomněla	zapomnět	k5eNaImAgFnS	zapomnět
také	také	k9	také
na	na	k7c4	na
oslavy	oslava	k1gFnPc4	oslava
různých	různý	k2eAgFnPc2d1	různá
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
Mejdan	mejdan	k1gInSc1	mejdan
roku	rok	k1gInSc2	rok
z	z	k7c2	z
Václaváku	Václavák	k1gInSc2	Václavák
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
také	také	k9	také
Nova	nova	k1gFnSc1	nova
vysílala	vysílat	k5eAaImAgFnS	vysílat
galavečery	galavečer	k1gInPc4	galavečer
udílení	udílení	k1gNnSc2	udílení
cen	cena	k1gFnPc2	cena
TýTý	TýTý	k1gFnSc2	TýTý
<g/>
,	,	kIx,	,
Test	test	k1gInSc1	test
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
Miss	miss	k1gFnPc1	miss
ČR	ČR	kA	ČR
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
Miss	miss	k1gFnSc1	miss
nebo	nebo	k8xC	nebo
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
reality	realita	k1gFnPc4	realita
show	show	k1gFnSc1	show
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
a	a	k8xC	a
X	X	kA	X
Factor	Factor	k1gInSc1	Factor
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Dům	dům	k1gInSc1	dům
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
novinkou	novinka	k1gFnSc7	novinka
byl	být	k5eAaImAgInS	být
příchod	příchod	k1gInSc1	příchod
sitkomu	sitkom	k1gInSc2	sitkom
Comeback	Comebacka	k1gFnPc2	Comebacka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
nový	nový	k2eAgInSc4d1	nový
seriál	seriál	k1gInSc4	seriál
byla	být	k5eAaImAgFnS	být
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Anděl	Anděla	k1gFnPc2	Anděla
nebo	nebo	k8xC	nebo
cyklus	cyklus	k1gInSc1	cyklus
Soukromé	soukromý	k2eAgFnSc2d1	soukromá
pasti	past	k1gFnSc2	past
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Nova	novum	k1gNnSc2	novum
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
sbírku	sbírka	k1gFnSc4	sbírka
svých	svůj	k3xOyFgInPc2	svůj
seriálů	seriál	k1gInPc2	seriál
o	o	k7c4	o
Okresní	okresní	k2eAgInSc4d1	okresní
přebor	přebor	k1gInSc4	přebor
a	a	k8xC	a
Dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
sérii	série	k1gFnSc4	série
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
neprodloužila	prodloužit	k5eNaPmAgFnS	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Přinesla	přinést	k5eAaPmAgFnS	přinést
také	také	k9	také
nové	nový	k2eAgNnSc1d1	nové
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgMnPc2d1	nový
pořadů	pořad	k1gInPc2	pořad
jako	jako	k8xC	jako
Vizita	vizita	k1gFnSc1	vizita
<g/>
,	,	kIx,	,
Příběhy	příběh	k1gInPc1	příběh
bez	bez	k7c2	bez
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
Babicovy	Babicův	k2eAgFnPc1d1	Babicova
dobroty	dobrota	k1gFnPc1	dobrota
<g/>
,	,	kIx,	,
Stahovák	stahovák	k1gInSc1	stahovák
<g/>
,	,	kIx,	,
Přísně	přísně	k6eAd1	přísně
tajné	tajný	k2eAgFnPc1d1	tajná
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Neviňátka	neviňátko	k1gNnSc2	neviňátko
<g/>
,	,	kIx,	,
MR	MR	kA	MR
<g/>
.	.	kIx.	.
</s>
<s>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
k	k	k7c3	k
neuvěření	neuvěření	k1gNnSc3	neuvěření
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
hudební	hudební	k2eAgInSc1d1	hudební
pořad	pořad	k1gInSc1	pořad
Eso	eso	k1gNnSc4	eso
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
naposled	naposled	k6eAd1	naposled
moderoval	moderovat	k5eAaBmAgMnS	moderovat
Leoš	Leoš	k1gMnSc1	Leoš
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
a	a	k8xC	a
k	k	k7c3	k
Odpoledním	odpolední	k2eAgFnPc3d1	odpolední
a	a	k8xC	a
hlavním	hlavní	k2eAgFnPc3d1	hlavní
Televizním	televizní	k2eAgFnPc3d1	televizní
novinám	novina	k1gFnPc3	novina
přibývají	přibývat	k5eAaImIp3nP	přibývat
ještě	ještě	k6eAd1	ještě
Ranní	ranní	k2eAgFnPc1d1	ranní
<g/>
,	,	kIx,	,
Polední	polední	k2eAgFnPc1d1	polední
a	a	k8xC	a
Noční	noční	k2eAgFnPc1d1	noční
Televizní	televizní	k2eAgFnPc1d1	televizní
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
již	již	k6eAd1	již
za	za	k7c2	za
nového	nový	k2eAgMnSc2d1	nový
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Jana	Jana	k1gFnSc1	Jana
Andruška	Andruška	k1gMnSc1	Andruška
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c4	v
funkci	funkce	k1gFnSc4	funkce
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Petra	Petr	k1gMnSc4	Petr
Dvořáka	Dvořák	k1gMnSc4	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
také	také	k6eAd1	také
přišly	přijít	k5eAaPmAgFnP	přijít
nové	nový	k2eAgInPc4d1	nový
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
seriály	seriál	k1gInPc4	seriál
Mentalista	Mentalista	k1gMnSc1	Mentalista
nebo	nebo	k8xC	nebo
Tisíc	tisíc	k4xCgInSc1	tisíc
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
pak	pak	k6eAd1	pak
kriminální	kriminální	k2eAgInSc1d1	kriminální
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Expozitura	expozitura	k1gFnSc1	expozitura
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgNnSc7	jenž
stálo	stát	k5eAaImAgNnS	stát
duo	duo	k1gNnSc1	duo
Josef	Josef	k1gMnSc1	Josef
Klíma	Klíma	k1gMnSc1	Klíma
a	a	k8xC	a
Janek	Janek	k1gMnSc1	Janek
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Andruška	Andruška	k1gFnSc1	Andruška
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
televizi	televize	k1gFnSc6	televize
Nova	nova	k1gFnSc1	nova
vrací	vracet	k5eAaImIp3nS	vracet
některé	některý	k3yIgInPc4	některý
staré	starý	k2eAgInPc4d1	starý
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Železného	Železný	k1gMnSc2	Železný
<g/>
:	:	kIx,	:
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
Prásk	prásk	k0	prásk
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Investigativní	investigativní	k2eAgInSc1d1	investigativní
reportérský	reportérský	k2eAgInSc1d1	reportérský
pořad	pořad	k1gInSc1	pořad
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
také	také	k9	také
díky	díky	k7c3	díky
návratu	návrat	k1gInSc3	návrat
Josefa	Josef	k1gMnSc2	Josef
Klímy	Klíma	k1gMnSc2	Klíma
a	a	k8xC	a
Janka	Janek	k1gMnSc2	Janek
Kroupy	Kroupa	k1gMnSc2	Kroupa
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c6	na
Novu	nov	k1gInSc6	nov
<g/>
.	.	kIx.	.
</s>
<s>
Jarním	jarní	k2eAgInSc7d1	jarní
tahákem	tahák	k1gInSc7	tahák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Hlas	hlas	k1gInSc1	hlas
Česko	Česko	k1gNnSc1	Česko
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Nova	nova	k1gFnSc1	nova
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
seriálové	seriálový	k2eAgNnSc4d1	seriálové
portfolium	portfolium	k1gNnSc4	portfolium
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
o	o	k7c6	o
komedie	komedie	k1gFnPc4	komedie
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
Gympl	gympl	k1gInSc1	gympl
s	s	k7c7	s
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
učením	učení	k1gNnSc7	učení
omezeným	omezený	k2eAgNnSc7d1	omezené
nebo	nebo	k8xC	nebo
o	o	k7c4	o
seriál	seriál	k1gInSc4	seriál
z	z	k7c2	z
tanečního	taneční	k2eAgNnSc2d1	taneční
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
-	-	kIx~	-
První	první	k4xOgInSc1	první
krok	krok	k1gInSc1	krok
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
taháky	tahák	k1gInPc7	tahák
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nová	nový	k2eAgFnSc1d1	nová
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
kuchařská	kuchařský	k2eAgFnSc1d1	kuchařská
show	show	k1gFnSc1	show
MasterChef	MasterChef	k1gInSc1	MasterChef
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
uvedla	uvést	k5eAaPmAgFnS	uvést
novou	nova	k1gFnSc7	nova
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc1	show
Farma	farma	k1gFnSc1	farma
nebo	nebo	k8xC	nebo
svatební	svatební	k2eAgFnPc1d1	svatební
reality	realita	k1gFnSc2	realita
show	show	k1gNnSc2	show
4	[number]	k4	4
svatby	svatba	k1gFnSc2	svatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
spustit	spustit	k5eAaPmF	spustit
svůj	svůj	k3xOyFgInSc4	svůj
pátý	pátý	k4xOgInSc4	pátý
kanál	kanál	k1gInSc4	kanál
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Fanda	Fanda	k1gMnSc1	Fanda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
jako	jako	k8xC	jako
kanál	kanál	k1gInSc1	kanál
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
v	v	k7c6	v
terestrickém	terestrický	k2eAgInSc6d1	terestrický
digitálním	digitální	k2eAgInSc6d1	digitální
multiplexu	multiplex	k1gInSc6	multiplex
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Nova	nova	k1gFnSc1	nova
jej	on	k3xPp3gMnSc4	on
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
šířila	šířit	k5eAaImAgFnS	šířit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
satelitních	satelitní	k2eAgFnPc6d1	satelitní
<g/>
,	,	kIx,	,
kabelových	kabelový	k2eAgFnPc6d1	kabelová
a	a	k8xC	a
IPTV	IPTV	kA	IPTV
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
Nova	nova	k1gFnSc1	nova
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
spustila	spustit	k5eAaPmAgFnS	spustit
jako	jako	k9	jako
vánoční	vánoční	k2eAgInSc4d1	vánoční
dárek	dárek	k1gInSc4	dárek
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
diváky	divák	k1gMnPc4	divák
další	další	k2eAgInSc4d1	další
kanál	kanál	k1gInSc4	kanál
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
komediální	komediální	k2eAgInSc4d1	komediální
seriály	seriál	k1gInPc4	seriál
a	a	k8xC	a
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
staré	starý	k2eAgInPc4d1	starý
zábavné	zábavný	k2eAgInPc4d1	zábavný
pořady	pořad	k1gInPc4	pořad
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
-	-	kIx~	-
Tele	tele	k1gNnSc1	tele
Tele	tele	k1gNnSc1	tele
<g/>
,	,	kIx,	,
Ptákoviny	ptákovina	k1gFnPc1	ptákovina
či	či	k8xC	či
MR	MR	kA	MR
<g/>
.	.	kIx.	.
</s>
<s>
GS	GS	kA	GS
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybět	k5eNaImIp3nS	chybět
ani	ani	k9	ani
nové	nový	k2eAgInPc4d1	nový
premiérové	premiérový	k2eAgInPc4d1	premiérový
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
seriály	seriál	k1gInPc4	seriál
jako	jako	k9	jako
Franklin	Franklin	k2eAgMnSc1d1	Franklin
a	a	k8xC	a
Bash	Bash	k1gMnSc1	Bash
<g/>
,	,	kIx,	,
2	[number]	k4	2
Socky	Sock	k1gInPc1	Sock
<g/>
,	,	kIx,	,
Zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
nebo	nebo	k8xC	nebo
Mike	Mik	k1gFnSc2	Mik
a	a	k8xC	a
Molly	Molla	k1gFnSc2	Molla
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
je	být	k5eAaImIp3nS	být
šířen	šířen	k2eAgInSc1d1	šířen
pozemním	pozemní	k2eAgNnSc7d1	pozemní
digitálním	digitální	k2eAgNnSc7d1	digitální
vysíláním	vysílání	k1gNnSc7	vysílání
v	v	k7c6	v
multiplexu	multiplex	k1gInSc6	multiplex
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zavedl	zavést	k5eAaPmAgMnS	zavést
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Jan	Jan	k1gMnSc1	Jan
Andruško	Andruška	k1gFnSc5	Andruška
nové	nový	k2eAgFnPc4d1	nová
obchodní	obchodní	k2eAgFnPc4d1	obchodní
podmínky	podmínka	k1gFnPc4	podmínka
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
reklam	reklama	k1gFnPc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
zadavatelům	zadavatel	k1gMnPc3	zadavatel
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
si	se	k3xPyFc3	se
nakoupí	nakoupit	k5eAaPmIp3nP	nakoupit
za	za	k7c4	za
nižší	nízký	k2eAgFnSc4d2	nižší
cenu	cena	k1gFnSc4	cena
prostor	prostora	k1gFnPc2	prostora
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
ji	on	k3xPp3gFnSc4	on
inzerovat	inzerovat	k5eAaImF	inzerovat
rok	rok	k1gInSc4	rok
či	či	k8xC	či
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
za	za	k7c4	za
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
budou	být	k5eAaImBp3nP	být
kupovat	kupovat	k5eAaImF	kupovat
měsíční	měsíční	k2eAgFnSc4d1	měsíční
inzerci	inzerce	k1gFnSc4	inzerce
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zadavatelů	zadavatel	k1gMnPc2	zadavatel
to	ten	k3xDgNnSc1	ten
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
a	a	k8xC	a
přešla	přejít	k5eAaPmAgFnS	přejít
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
televizím	televize	k1gFnPc3	televize
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ředitele	ředitel	k1gMnSc2	ředitel
zadavatelé	zadavatel	k1gMnPc1	zadavatel
k	k	k7c3	k
Nově	nova	k1gFnSc3	nova
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
televize	televize	k1gFnSc1	televize
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
reklamu	reklama	k1gFnSc4	reklama
ještě	ještě	k6eAd1	ještě
zvedla	zvednout	k5eAaPmAgFnS	zvednout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zohlední	zohlednit	k5eAaPmIp3nS	zohlednit
dlouholeté	dlouholetý	k2eAgMnPc4d1	dlouholetý
zadavatele	zadavatel	k1gMnPc4	zadavatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Nova	nova	k1gFnSc1	nova
obnovila	obnovit	k5eAaPmAgFnS	obnovit
svoji	svůj	k3xOyFgFnSc4	svůj
stálici	stálice	k1gFnSc4	stálice
SuperStar	superstar	k1gFnSc4	superstar
a	a	k8xC	a
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
pozměněném	pozměněný	k2eAgInSc6d1	pozměněný
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
ostříleného	ostřílený	k2eAgMnSc4d1	ostřílený
moderátora	moderátor	k1gMnSc4	moderátor
Leoše	Leoš	k1gMnSc2	Leoš
Mareše	Mareš	k1gMnSc2	Mareš
za	za	k7c7	za
nováčky	nováček	k1gMnPc7	nováček
<g/>
:	:	kIx,	:
Zoru	Zora	k1gFnSc4	Zora
Kepkovou	Kepkový	k2eAgFnSc4d1	Kepková
ze	z	k7c2	z
Snídaně	snídaně	k1gFnSc2	snídaně
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
a	a	k8xC	a
Romana	Romana	k1gFnSc1	Romana
Juraška	Juraška	k1gFnSc1	Juraška
z	z	k7c2	z
Telerána	Telerán	k2eAgNnPc1d1	Teleráno
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
televize	televize	k1gFnSc1	televize
nezahálela	zahálet	k5eNaImAgFnS	zahálet
a	a	k8xC	a
spustila	spustit	k5eAaPmAgFnS	spustit
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k9	již
třetí	třetí	k4xOgInSc4	třetí
kanál	kanál	k1gInSc4	kanál
v	v	k7c6	v
období	období	k1gNnSc6	období
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oslavy	oslava	k1gFnSc2	oslava
svých	svůj	k3xOyFgNnPc2	svůj
19	[number]	k4	19
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
spustila	spustit	k5eAaPmAgFnS	spustit
Telku	Telku	k?	Telku
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
v	v	k7c6	v
multiplexu	multiplex	k1gInSc6	multiplex
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
plánován	plánovat	k5eAaImNgInS	plánovat
na	na	k7c4	na
přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
oslav	oslava	k1gFnPc2	oslava
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
nechtěla	chtít	k5eNaImAgFnS	chtít
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
licenci	licence	k1gFnSc4	licence
schválit	schválit	k5eAaPmF	schválit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
chyběla	chybět	k5eAaImAgFnS	chybět
doplnit	doplnit	k5eAaPmF	doplnit
nějaké	nějaký	k3yIgFnPc4	nějaký
potřebné	potřebný	k2eAgFnPc4d1	potřebná
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
vysílá	vysílat	k5eAaImIp3nS	vysílat
archivní	archivní	k2eAgInPc4d1	archivní
pořady	pořad	k1gInPc4	pořad
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
na	na	k7c4	na
obrazovky	obrazovka	k1gFnPc4	obrazovka
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vrátily	vrátit	k5eAaPmAgFnP	vrátit
reprízy	repríza	k1gFnPc1	repríza
původních	původní	k2eAgMnPc2d1	původní
seriálů	seriál	k1gInPc2	seriál
Ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
štěstí	štěstí	k1gNnSc2	štěstí
či	či	k8xC	či
zábavních	zábavní	k2eAgInPc2d1	zábavní
a	a	k8xC	a
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
pořadů	pořad	k1gInPc2	pořad
jako	jako	k8xS	jako
Horoskopičiny	Horoskopičina	k1gFnSc2	Horoskopičina
<g/>
,	,	kIx,	,
Riskuj	riskovat	k5eAaBmRp2nS	riskovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Hogo	Hogo	k1gMnSc1	Hogo
Fogo	Fogo	k1gMnSc1	Fogo
<g/>
,	,	kIx,	,
Novoty	novota	k1gFnPc1	novota
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnPc1	Lucie
na	na	k7c4	na
bílo	bílo	k1gNnSc4	bílo
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybit	k5eNaPmIp3nS	chybit
zde	zde	k6eAd1	zde
ani	ani	k8xC	ani
reprízy	repríza	k1gFnPc1	repríza
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc4	superstar
<g/>
,	,	kIx,	,
Jsi	být	k5eAaImIp2nS	být
chytřejší	chytrý	k2eAgMnSc1d2	chytřejší
než	než	k8xS	než
páťák	páťák	k1gMnSc1	páťák
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Talentmania	Talentmanium	k1gNnPc1	Talentmanium
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
snů	sen	k1gInPc2	sen
nebo	nebo	k8xC	nebo
Farma	farma	k1gFnSc1	farma
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
letní	letní	k2eAgFnPc4d1	letní
prázdniny	prázdniny	k1gFnPc4	prázdniny
přinesla	přinést	k5eAaPmAgFnS	přinést
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
nový	nový	k2eAgInSc4d1	nový
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
seriál	seriál	k1gInSc4	seriál
Doktoři	doktor	k1gMnPc1	doktor
z	z	k7c2	z
Počátků	počátek	k1gInPc2	počátek
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gNnSc7	jehož
vysíláním	vysílání	k1gNnSc7	vysílání
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
mimo	mimo	k7c4	mimo
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Uvedla	uvést	k5eAaPmAgFnS	uvést
také	také	k9	také
nový	nový	k2eAgInSc4d1	nový
pořad	pořad	k1gInSc4	pořad
Sígři	sígr	k1gMnPc1	sígr
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Holubovou	Holubův	k2eAgFnSc7d1	Holubova
a	a	k8xC	a
letní	letní	k2eAgFnSc4d1	letní
verzi	verze	k1gFnSc4	verze
pořadu	pořad	k1gInSc2	pořad
Víkend	víkend	k1gInSc1	víkend
-	-	kIx~	-
Víkendové	víkendový	k2eAgFnPc1d1	víkendová
prázdniny	prázdniny	k1gFnPc1	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
nahradila	nahradit	k5eAaPmAgFnS	nahradit
sitkom	sitkom	k1gInSc4	sitkom
Helena	Helena	k1gFnSc1	Helena
novým	nový	k2eAgInSc7d1	nový
sitkomem	sitkom	k1gInSc7	sitkom
s	s	k7c7	s
Milanem	Milan	k1gMnSc7	Milan
Šteindlerem	Šteindler	k1gMnSc7	Šteindler
-	-	kIx~	-
PanMáma	PanMám	k1gMnSc2	PanMám
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nasazenými	nasazený	k2eAgFnPc7d1	nasazená
novinkami	novinka	k1gFnPc7	novinka
slavila	slavit	k5eAaImAgFnS	slavit
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
personálních	personální	k2eAgFnPc6d1	personální
změnách	změna	k1gFnPc6	změna
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
CME	CME	kA	CME
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
majitelem	majitel	k1gMnSc7	majitel
Novy	nova	k1gFnSc2	nova
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
CME	CME	kA	CME
Adrian	Adrian	k1gMnSc1	Adrian
Sârbu	Sârb	k1gInSc2	Sârb
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
za	za	k7c4	za
nové	nový	k2eAgMnPc4d1	nový
ředitele	ředitel	k1gMnPc4	ředitel
Christopha	Christoph	k1gMnSc4	Christoph
Mainuscha	Mainusch	k1gMnSc4	Mainusch
a	a	k8xC	a
Michaela	Michael	k1gMnSc4	Michael
Del	Del	k1gFnSc2	Del
Nina	Nina	k1gFnSc1	Nina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
rezignovat	rezignovat	k5eAaBmF	rezignovat
i	i	k8xC	i
Jan	Jan	k1gMnSc1	Jan
Andruško	Andruška	k1gMnSc5	Andruška
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
plánem	plán	k1gInSc7	plán
TV	TV	kA	TV
Nova	novum	k1gNnSc2	novum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
nové	nový	k2eAgNnSc1d1	nové
vedení	vedení	k1gNnSc1	vedení
CME	CME	kA	CME
předložilo	předložit	k5eAaPmAgNnS	předložit
<g/>
.	.	kIx.	.
</s>
<s>
Andruško	Andruška	k1gMnSc5	Andruška
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
David	David	k1gMnSc1	David
Stogel	Stogel	k1gMnSc1	Stogel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
již	již	k6eAd1	již
na	na	k7c6	na
Nově	nova	k1gFnSc6	nova
dříve	dříve	k6eAd2	dříve
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
personálních	personální	k2eAgFnPc6d1	personální
změnách	změna	k1gFnPc6	změna
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
Nově	nově	k6eAd1	nově
z	z	k7c2	z
úsporných	úsporný	k2eAgInPc2d1	úsporný
důvodů	důvod	k1gInPc2	důvod
na	na	k7c6	na
rušení	rušení	k1gNnSc6	rušení
rušilo	rušit	k5eAaImAgNnS	rušit
mnoha	mnoho	k4c2	mnoho
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
ukončila	ukončit	k5eAaPmAgFnS	ukončit
natáčení	natáčení	k1gNnSc4	natáčení
svého	svůj	k3xOyFgInSc2	svůj
seriálu	seriál	k1gInSc2	seriál
Gympl	gympl	k1gInSc4	gympl
s	s	k7c7	s
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
učením	učení	k1gNnSc7	učení
omezeným	omezený	k2eAgNnSc7d1	omezené
a	a	k8xC	a
zrušila	zrušit	k5eAaPmAgFnS	zrušit
i	i	k9	i
noční	noční	k2eAgFnSc4d1	noční
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
relaci	relace	k1gFnSc4	relace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
exkluzivních	exkluzivní	k2eAgNnPc2d1	exkluzivní
vysílacích	vysílací	k2eAgNnPc2d1	vysílací
práv	právo	k1gNnPc2	právo
pro	pro	k7c4	pro
hokejový	hokejový	k2eAgInSc4d1	hokejový
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
když	když	k8xS	když
přeplatila	přeplatit	k5eAaPmAgFnS	přeplatit
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c4	na
konec	konec	k1gInSc4	konec
stalo	stát	k5eAaPmAgNnS	stát
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
se	se	k3xPyFc4	se
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
na	na	k7c6	na
výměně	výměna	k1gFnSc6	výměna
vysílacích	vysílací	k2eAgNnPc2d1	vysílací
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
olympijskému	olympijský	k2eAgInSc3d1	olympijský
hokeji	hokej	k1gInSc3	hokej
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
k	k	k7c3	k
českým	český	k2eAgInPc3d1	český
filmům	film	k1gInPc3	film
a	a	k8xC	a
seriálům	seriál	k1gInPc3	seriál
jako	jako	k9	jako
Chalupáři	chalupář	k1gMnPc7	chalupář
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
také	také	k9	také
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c4	o
neprodloužení	neprodloužení	k1gNnSc4	neprodloužení
smlouvy	smlouva	k1gFnSc2	smlouva
k	k	k7c3	k
vysílání	vysílání	k1gNnSc3	vysílání
české	český	k2eAgFnSc2d1	Česká
mutace	mutace	k1gFnSc2	mutace
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
také	také	k9	také
nakonec	nakonec	k6eAd1	nakonec
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
a	a	k8xC	a
MTV	MTV	kA	MTV
opustila	opustit	k5eAaPmAgFnS	opustit
skupinu	skupina	k1gFnSc4	skupina
Nova	novum	k1gNnSc2	novum
ke	k	k7c3	k
dni	den	k1gInSc3	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ji	on	k3xPp3gFnSc4	on
začala	začít	k5eAaPmAgFnS	začít
provozovat	provozovat	k5eAaImF	provozovat
společnost	společnost	k1gFnSc1	společnost
Viacom	Viacom	k1gInSc4	Viacom
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
evropskou	evropský	k2eAgFnSc7d1	Evropská
mutací	mutace	k1gFnSc7	mutace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Christoph	Christoph	k1gInSc1	Christoph
Mainusch	Mainusch	k1gInSc4	Mainusch
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
jednatelem	jednatel	k1gMnSc7	jednatel
společnosti	společnost	k1gFnSc2	společnost
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
začal	začít	k5eAaPmAgInS	začít
spravovat	spravovat	k5eAaImF	spravovat
Novu	nova	k1gFnSc4	nova
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
zůstal	zůstat	k5eAaPmAgMnS	zůstat
David	David	k1gMnSc1	David
Stogel	Stogel	k1gMnSc1	Stogel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
Mainusch	Mainusch	k1gMnSc1	Mainusch
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Stogel	Stoget	k5eAaBmAgMnS	Stoget
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
zastával	zastávat	k5eAaImAgMnS	zastávat
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
působení	působení	k1gNnSc2	působení
Petra	Petr	k1gMnSc2	Petr
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
Nova	nova	k1gFnSc1	nova
zrušila	zrušit	k5eAaPmAgFnS	zrušit
výrobu	výroba	k1gFnSc4	výroba
svého	svůj	k3xOyFgInSc2	svůj
znovuobnoveného	znovuobnovený	k2eAgInSc2d1	znovuobnovený
pořadu	pořad	k1gInSc2	pořad
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
dni	den	k1gInSc3	den
rozpustila	rozpustit	k5eAaPmAgFnS	rozpustit
i	i	k9	i
tým	tým	k1gInSc4	tým
Mediafaxu	Mediafax	k1gInSc2	Mediafax
<g/>
.	.	kIx.	.
</s>
<s>
Restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
dokončila	dokončit	k5eAaPmAgFnS	dokončit
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
podle	podle	k7c2	podle
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Christopha	Christoph	k1gMnSc2	Christoph
Mainuscha	Mainusch	k1gMnSc2	Mainusch
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
oslavila	oslavit	k5eAaPmAgFnS	oslavit
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
své	svůj	k3xOyFgFnPc4	svůj
dvacáté	dvacátý	k4xOgFnPc4	dvacátý
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
u	u	k7c2	u
jejich	jejich	k3xOp3gFnSc2	jejich
příležitosti	příležitost	k1gFnSc2	příležitost
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
"	"	kIx"	"
<g/>
velkou	velký	k2eAgFnSc4d1	velká
narozeninovou	narozeninový	k2eAgFnSc4d1	narozeninová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
rozmístila	rozmístit	k5eAaPmAgFnS	rozmístit
20	[number]	k4	20
dárků	dárek	k1gInPc2	dárek
pomocí	pomocí	k7c2	pomocí
balónů	balón	k1gInPc2	balón
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
také	také	k9	také
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
speciální	speciální	k2eAgInSc4d1	speciální
program	program	k1gInSc4	program
Všechno	všechen	k3xTgNnSc1	všechen
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shrnula	shrnout	k5eAaPmAgFnS	shrnout
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
na	na	k7c6	na
televizním	televizní	k2eAgInSc6d1	televizní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Pořadem	pořad	k1gInSc7	pořad
provázela	provázet	k5eAaImAgFnS	provázet
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
tvář	tvář	k1gFnSc1	tvář
televize	televize	k1gFnSc2	televize
Petr	Petr	k1gMnSc1	Petr
Rychlý	Rychlý	k1gMnSc1	Rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Oslavy	oslava	k1gFnPc1	oslava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
celý	celý	k2eAgInSc4d1	celý
měsíc	měsíc	k1gInSc4	měsíc
únor	únor	k1gInSc4	únor
<g/>
,	,	kIx,	,
přinesly	přinést	k5eAaPmAgInP	přinést
i	i	k9	i
obměnu	obměna	k1gFnSc4	obměna
televizní	televizní	k2eAgFnSc2d1	televizní
grafiky	grafika	k1gFnSc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
mohli	moct	k5eAaImAgMnP	moct
také	také	k9	také
využít	využít	k5eAaPmF	využít
možnosti	možnost	k1gFnSc2	možnost
navštívit	navštívit	k5eAaPmF	navštívit
prostory	prostor	k1gInPc4	prostor
televize	televize	k1gFnSc2	televize
Nova	novum	k1gNnSc2	novum
při	při	k7c6	při
Dnu	den	k1gInSc6	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgFnPc2	svůj
narozenin	narozeniny	k1gFnPc2	narozeniny
též	též	k9	též
spustila	spustit	k5eAaPmAgFnS	spustit
bezplatnou	bezplatný	k2eAgFnSc4d1	bezplatná
videotéku	videotéka	k1gFnSc4	videotéka
Nova	novum	k1gNnSc2	novum
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nabízí	nabízet	k5eAaImIp3nS	nabízet
své	svůj	k3xOyFgInPc4	svůj
pořady	pořad	k1gInPc4	pořad
až	až	k9	až
7	[number]	k4	7
dní	den	k1gInPc2	den
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
odvysílání	odvysílání	k1gNnSc6	odvysílání
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
představeno	představen	k2eAgNnSc4d1	představeno
nové	nový	k2eAgNnSc4d1	nové
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
grafiku	grafika	k1gFnSc4	grafika
<g/>
,	,	kIx,	,
znělky	znělka	k1gFnPc4	znělka
a	a	k8xC	a
logo	logo	k1gNnSc4	logo
Televizních	televizní	k2eAgFnPc2d1	televizní
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
zakřivená	zakřivený	k2eAgFnSc1d1	zakřivená
projekční	projekční	k2eAgFnSc1d1	projekční
stěna	stěna	k1gFnSc1	stěna
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
s	s	k7c7	s
Ultra	ultra	k2eAgFnSc7d1	ultra
HD	HD	kA	HD
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
posuvný	posuvný	k2eAgInSc1d1	posuvný
multimediální	multimediální	k2eAgInSc4d1	multimediální
moderátorský	moderátorský	k2eAgInSc4d1	moderátorský
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přemístit	přemístit	k5eAaPmF	přemístit
okolo	okolo	k7c2	okolo
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
až	až	k9	až
o	o	k7c4	o
270	[number]	k4	270
<g/>
°	°	k?	°
nebo	nebo	k8xC	nebo
obrovská	obrovský	k2eAgFnSc1d1	obrovská
projekční	projekční	k2eAgFnSc1d1	projekční
LED	LED	kA	LED
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Grafiku	grafika	k1gFnSc4	grafika
mohou	moct	k5eAaImIp3nP	moct
moderátoři	moderátor	k1gMnPc1	moderátor
navíc	navíc	k6eAd1	navíc
ovládat	ovládat	k5eAaImF	ovládat
pohybem	pohyb	k1gInSc7	pohyb
a	a	k8xC	a
gesty	gest	k1gInPc7	gest
pomocí	pomoc	k1gFnPc2	pomoc
konzole	konzola	k1gFnSc6	konzola
Kinect	Kinect	k2eAgMnSc1d1	Kinect
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
nad	nad	k7c7	nad
kamerou	kamera	k1gFnSc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
představeno	představen	k2eAgNnSc1d1	představeno
také	také	k9	také
nové	nový	k2eAgNnSc1d1	nové
studio	studio	k1gNnSc1	studio
o	o	k7c4	o
ranní	ranní	k2eAgInSc4d1	ranní
pořad	pořad	k1gInSc4	pořad
Snídaně	snídaně	k1gFnSc2	snídaně
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
natáčet	natáčet	k5eAaImF	natáčet
také	také	k9	také
pořady	pořad	k1gInPc1	pořad
Koření	kořenit	k5eAaImIp3nP	kořenit
<g/>
,	,	kIx,	,
Prásk	prásk	k0	prásk
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Víkend	víkend	k1gInSc4	víkend
a	a	k8xC	a
sportovní	sportovní	k2eAgInPc4d1	sportovní
přenosy	přenos	k1gInPc4	přenos
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
studio	studio	k1gNnSc1	studio
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
dle	dle	k7c2	dle
potřeb	potřeba	k1gFnPc2	potřeba
pořadu	pořad	k1gInSc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
pustila	pustit	k5eAaPmAgFnS	pustit
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
druhou	druhý	k4xOgFnSc4	druhý
řadu	řada	k1gFnSc4	řada
pěvecké	pěvecký	k2eAgFnSc2d1	pěvecká
show	show	k1gFnSc2	show
Hlas	hlas	k1gInSc4	hlas
Česko	Česko	k1gNnSc4	Česko
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Uvedla	uvést	k5eAaPmAgFnS	uvést
své	svůj	k3xOyFgInPc4	svůj
původní	původní	k2eAgInPc4d1	původní
seriály	seriál	k1gInPc4	seriál
od	od	k7c2	od
neděle	neděle	k1gFnSc2	neděle
až	až	k6eAd1	až
do	do	k7c2	do
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nově	nově	k6eAd1	nově
přesunula	přesunout	k5eAaPmAgFnS	přesunout
seriál	seriál	k1gInSc4	seriál
Doktoři	doktor	k1gMnPc1	doktor
z	z	k7c2	z
Počátků	počátek	k1gInPc2	počátek
pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c4	na
pondělí	pondělí	k1gNnSc4	pondělí
a	a	k8xC	a
středy	střed	k1gInPc4	střed
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
řadu	řada	k1gFnSc4	řada
seriálu	seriál	k1gInSc2	seriál
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Anděl	Anděla	k1gFnPc2	Anděla
nově	nově	k6eAd1	nově
představila	představit	k5eAaPmAgFnS	představit
v	v	k7c6	v
nedělním	nedělní	k2eAgInSc6d1	nedělní
prime	prim	k1gInSc5	prim
time	timus	k1gInSc5	timus
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
poprvé	poprvé	k6eAd1	poprvé
uvedla	uvést	k5eAaPmAgFnS	uvést
Nova	nova	k1gFnSc1	nova
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
Markíza	Markíz	k1gMnSc2	Markíz
Chart	charta	k1gFnPc2	charta
Show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
0,799	[number]	k4	0,799
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
premiérovou	premiérový	k2eAgFnSc4d1	premiérová
řadu	řada	k1gFnSc4	řada
Kriminálky	kriminálka	k1gFnSc2	kriminálka
Anděl	Anděla	k1gFnPc2	Anděla
nová	nový	k2eAgFnSc1d1	nová
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
nového	nový	k2eAgInSc2d1	nový
kriminálního	kriminální	k2eAgInSc2d1	kriminální
seriálu	seriál	k1gInSc2	seriál
Policie	policie	k1gFnSc1	policie
Modrava	Modrava	k1gFnSc1	Modrava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Nova	nova	k1gFnSc1	nova
obnovila	obnovit	k5eAaPmAgFnS	obnovit
po	po	k7c6	po
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
odmlce	odmlka	k1gFnSc6	odmlka
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
pilotním	pilotní	k2eAgNnSc6d1	pilotní
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
po	po	k7c6	po
odvysílání	odvysílání	k1gNnSc6	odvysílání
prvních	první	k4xOgInPc2	první
dílů	díl	k1gInPc2	díl
stal	stát	k5eAaPmAgInS	stát
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
seriálem	seriál	k1gInSc7	seriál
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
tak	tak	k9	tak
seriál	seriál	k1gInSc1	seriál
Ordinace	ordinace	k1gFnSc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
držela	držet	k5eAaImAgFnS	držet
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
sezóně	sezóna	k1gFnSc6	sezóna
stanice	stanice	k1gFnSc2	stanice
opět	opět	k6eAd1	opět
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Doktory	doktor	k1gMnPc4	doktor
z	z	k7c2	z
Počátků	počátek	k1gInPc2	počátek
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
vysílací	vysílací	k2eAgInSc4d1	vysílací
den	den	k1gInSc4	den
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středečních	středeční	k2eAgInPc6d1	středeční
večerech	večer	k1gInPc6	večer
seriál	seriál	k1gInSc4	seriál
nahradila	nahradit	k5eAaPmAgFnS	nahradit
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
televizní	televizní	k2eAgFnPc4d1	televizní
obrazovky	obrazovka	k1gFnPc4	obrazovka
přivedla	přivést	k5eAaPmAgFnS	přivést
novou	nový	k2eAgFnSc4d1	nová
podvečerní	podvečerní	k2eAgFnSc4d1	podvečerní
reality	realita	k1gFnPc4	realita
show	show	k1gFnSc1	show
Nejlíp	dobře	k6eAd3	dobře
vaří	vařit	k5eAaImIp3nS	vařit
moje	můj	k3xOp1gFnSc1	můj
máma	máma	k1gFnSc1	máma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
začala	začít	k5eAaPmAgFnS	začít
vysílal	vysílat	k5eAaImAgInS	vysílat
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
novou	nový	k2eAgFnSc4d1	nová
řadu	řada	k1gFnSc4	řada
SuperStar	superstar	k1gFnPc2	superstar
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
řadu	řada	k1gFnSc4	řada
seriálu	seriál	k1gInSc2	seriál
Expozitura	expozitura	k1gFnSc1	expozitura
s	s	k7c7	s
názvem	název	k1gInSc7	název
Atentát	atentát	k1gInSc1	atentát
a	a	k8xC	a
kuchařskou	kuchařský	k2eAgFnSc7d1	kuchařská
show	show	k1gFnSc7	show
MasterChef	MasterChef	k1gInSc1	MasterChef
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
produkci	produkce	k1gFnSc6	produkce
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
česko-slovenské	českolovenský	k2eAgInPc1d1	česko-slovenský
jako	jako	k8xS	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
zahájila	zahájit	k5eAaPmAgFnS	zahájit
vysílání	vysílání	k1gNnSc4	vysílání
nová	nový	k2eAgFnSc1d1	nová
kabelová	kabelový	k2eAgFnSc1d1	kabelová
stanice	stanice	k1gFnSc1	stanice
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sesterská	sesterský	k2eAgFnSc1d1	sesterská
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
1	[number]	k4	1
placená	placený	k2eAgFnSc1d1	placená
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zastavení	zastavení	k1gNnSc4	zastavení
nelegálního	legální	k2eNgNnSc2d1	nelegální
šíření	šíření	k1gNnSc2	šíření
obsahu	obsah	k1gInSc2	obsah
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spustí	spustit	k5eAaPmIp3nS	spustit
Nova	nova	k1gFnSc1	nova
International	International	k1gFnSc1	International
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
stanici	stanice	k1gFnSc6	stanice
přišly	přijít	k5eAaPmAgInP	přijít
již	již	k6eAd1	již
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
spustila	spustit	k5eAaPmAgFnS	spustit
stanici	stanice	k1gFnSc4	stanice
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
v	v	k7c4	v
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
nabízí	nabízet	k5eAaImIp3nS	nabízet
obdobné	obdobný	k2eAgNnSc4d1	obdobné
programové	programový	k2eAgNnSc4d1	programové
schéma	schéma	k1gNnSc4	schéma
jako	jako	k8xC	jako
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
pořady	pořad	k1gInPc7	pořad
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
společnost	společnost	k1gFnSc1	společnost
disponuje	disponovat	k5eAaBmIp3nS	disponovat
právy	právo	k1gNnPc7	právo
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Sesterská	sesterský	k2eAgFnSc1d1	sesterská
Markíza	Markíza	k1gFnSc1	Markíza
zase	zase	k9	zase
nabízí	nabízet	k5eAaImIp3nS	nabízet
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
kanál	kanál	k1gInSc4	kanál
Markíza	Markíz	k1gMnSc4	Markíz
International	International	k1gFnSc2	International
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
stanice	stanice	k1gFnSc1	stanice
oznámila	oznámit	k5eAaPmAgFnS	oznámit
start	start	k1gInSc4	start
nových	nový	k2eAgInPc2d1	nový
seriálů	seriál	k1gInPc2	seriál
Na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
Drazí	drahý	k2eAgMnPc1d1	drahý
sousedé	soused	k1gMnPc1	soused
<g/>
,	,	kIx,	,
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
sledovanosti	sledovanost	k1gFnSc3	sledovanost
přerušen	přerušen	k2eAgInSc4d1	přerušen
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
nábor	nábor	k1gInSc1	nábor
soutěžících	soutěžící	k1gMnPc2	soutěžící
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
soutěžních	soutěžní	k2eAgFnPc2d1	soutěžní
show	show	k1gFnPc2	show
Co	co	k9	co
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
začala	začít	k5eAaPmAgFnS	začít
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
je	být	k5eAaImIp3nS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
každý	každý	k3xTgInSc4	každý
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
a	a	k8xC	a
legendární	legendární	k2eAgNnSc4d1	legendární
soutěžní	soutěžní	k2eAgNnSc4d1	soutěžní
show	show	k1gNnSc4	show
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nasazena	nasadit	k5eAaPmNgFnS	nasadit
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
středu	středa	k1gFnSc4	středa
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Moderátorem	moderátor	k1gMnSc7	moderátor
je	být	k5eAaImIp3nS	být
Marek	Marek	k1gMnSc1	Marek
Vašut	Vašut	k1gMnSc1	Vašut
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
televize	televize	k1gFnSc1	televize
spustila	spustit	k5eAaPmAgFnS	spustit
novou	nový	k2eAgFnSc4d1	nová
zábavnou	zábavný	k2eAgFnSc4d1	zábavná
show	show	k1gFnSc4	show
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
tvář	tvář	k1gFnSc1	tvář
má	mít	k5eAaImIp3nS	mít
známý	známý	k2eAgInSc4d1	známý
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
české	český	k2eAgFnPc1d1	Česká
osobnosti	osobnost	k1gFnPc1	osobnost
mění	měnit	k5eAaImIp3nP	měnit
za	za	k7c4	za
české	český	k2eAgMnPc4d1	český
a	a	k8xC	a
světové	světový	k2eAgMnPc4d1	světový
zpěváky	zpěvák	k1gMnPc4	zpěvák
a	a	k8xC	a
zpěvačky	zpěvačka	k1gFnPc4	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysílána	vysílán	k2eAgFnSc1d1	vysílána
každou	každý	k3xTgFnSc4	každý
neděli	neděle	k1gFnSc4	neděle
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
nejsledovanější	sledovaný	k2eAgInSc1d3	nejsledovanější
pořad	pořad	k1gInSc1	pořad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podzimní	podzimní	k2eAgFnSc6d1	podzimní
části	část	k1gFnSc6	část
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
úspěchy	úspěch	k1gInPc4	úspěch
svých	svůj	k3xOyFgInPc2	svůj
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
věnu	věno	k1gNnSc3	věno
je	být	k5eAaImIp3nS	být
pořadům	pořad	k1gInPc3	pořad
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
než	než	k8xS	než
seriálům	seriál	k1gInPc3	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
show	show	k1gFnSc1	show
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
tvář	tvář	k1gFnSc1	tvář
má	mít	k5eAaImIp3nS	mít
známý	známý	k2eAgInSc4d1	známý
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
MasterChef	MasterChef	k1gInSc4	MasterChef
Česko	Česko	k1gNnSc4	Česko
<g/>
,	,	kIx,	,
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Co	co	k3yRnSc4	co
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Češi	Čech	k1gMnPc1	Čech
i	i	k8xC	i
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Nováčkem	Nováček	k1gMnSc7	Nováček
se	se	k3xPyFc4	se
v	v	k7c6	v
programu	program	k1gInSc6	program
stala	stát	k5eAaPmAgFnS	stát
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
Celebrity	celebrita	k1gFnSc2	celebrita
Game	game	k1gInSc1	game
Night	Night	k2eAgInSc1d1	Night
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svých	svůj	k3xOyFgNnPc2	svůj
23	[number]	k4	23
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
se	se	k3xPyFc4	se
Nova	nova	k1gFnSc1	nova
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
kanály	kanál	k1gInPc4	kanál
Fanda	Fanda	k1gFnSc1	Fanda
na	na	k7c4	na
Nova	nova	k1gFnSc1	nova
Action	Action	k1gInSc1	Action
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
na	na	k7c4	na
Nova	novum	k1gNnPc4	novum
2	[number]	k4	2
a	a	k8xC	a
Telka	Telka	k?	Telka
na	na	k7c4	na
Nova	novum	k1gNnPc4	novum
Gold	Golda	k1gFnPc2	Golda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jarního	jarní	k2eAgNnSc2d1	jarní
vysílání	vysílání	k1gNnSc2	vysílání
si	se	k3xPyFc3	se
Nova	nova	k1gFnSc1	nova
připravila	připravit	k5eAaPmAgFnS	připravit
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
seriál	seriál	k1gInSc1	seriál
Specialisté	specialista	k1gMnPc1	specialista
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vysílat	vysílat	k5eAaImF	vysílat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
každé	každý	k3xTgNnSc4	každý
pondělí	pondělí	k1gNnSc4	pondělí
od	od	k7c2	od
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Robinsonův	Robinsonův	k2eAgInSc4d1	Robinsonův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
bude	být	k5eAaImBp3nS	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
vyhrazeno	vyhrazen	k2eAgNnSc4d1	vyhrazeno
pondělí	pondělí	k1gNnSc4	pondělí
od	od	k7c2	od
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
a	a	k8xC	a
středa	středa	k1gFnSc1	středa
od	od	k7c2	od
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
řadou	řada	k1gFnSc7	řada
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
show	show	k1gFnSc1	show
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
tvář	tvář	k1gFnSc1	tvář
má	mít	k5eAaImIp3nS	mít
známý	známý	k2eAgInSc4d1	známý
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
se	se	k3xPyFc4	se
druhou	druhý	k4xOgFnSc7	druhý
řadou	řada	k1gFnSc7	řada
kriminální	kriminální	k2eAgInSc1d1	kriminální
seriál	seriál	k1gInSc1	seriál
Policie	policie	k1gFnSc1	policie
Modrava	Modrava	k1gFnSc1	Modrava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Televizní	televizní	k2eAgFnSc2d1	televizní
noviny	novina	k1gFnSc2	novina
(	(	kIx(	(
<g/>
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
vysílá	vysílat	k5eAaImIp3nS	vysílat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
nejsledovanější	sledovaný	k2eAgFnSc4d3	nejsledovanější
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
relaci	relace	k1gFnSc4	relace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Televizní	televizní	k2eAgFnSc2d1	televizní
noviny	novina	k1gFnSc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
program	program	k1gInSc1	program
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInPc4d1	aktuální
zpravodajské	zpravodajský	k2eAgInPc4d1	zpravodajský
pořady	pořad	k1gInPc4	pořad
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
:	:	kIx,	:
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
také	také	k9	také
informuje	informovat	k5eAaBmIp3nS	informovat
své	svůj	k3xOyFgMnPc4	svůj
diváky	divák	k1gMnPc4	divák
o	o	k7c6	o
zprávách	zpráva	k1gFnPc6	zpráva
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
pomocí	pomocí	k7c2	pomocí
internetového	internetový	k2eAgInSc2d1	internetový
portálu	portál	k1gInSc2	portál
tn	tn	k?	tn
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
internetový	internetový	k2eAgInSc1d1	internetový
zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
kanál	kanál	k1gInSc1	kanál
Nova	novum	k1gNnSc2	novum
News	Newsa	k1gFnPc2	Newsa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vysílal	vysílat	k5eAaImAgMnS	vysílat
živé	živý	k2eAgInPc4d1	živý
přenosy	přenos	k1gInPc4	přenos
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
relací	relace	k1gFnPc2	relace
<g/>
,	,	kIx,	,
staré	starý	k2eAgFnPc1d1	stará
zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
i	i	k9	i
staré	starý	k2eAgInPc4d1	starý
zpravodajské	zpravodajský	k2eAgInPc4d1	zpravodajský
pořady	pořad	k1gInPc4	pořad
jako	jako	k8xC	jako
Víkend	víkend	k1gInSc4	víkend
<g/>
,	,	kIx,	,
Střepiny	střepina	k1gFnPc4	střepina
<g/>
,	,	kIx,	,
112	[number]	k4	112
nebo	nebo	k8xC	nebo
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vrátila	vrátit	k5eAaPmAgFnS	vrátit
licenci	licence	k1gFnSc4	licence
k	k	k7c3	k
vysílání	vysílání	k1gNnSc3	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
události	událost	k1gFnPc1	událost
nechybí	chybět	k5eNaImIp3nP	chybět
také	také	k9	také
na	na	k7c6	na
placeném	placený	k2eAgInSc6d1	placený
kanálu	kanál	k1gInSc6	kanál
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
1	[number]	k4	1
a	a	k8xC	a
Nova	nova	k1gFnSc1	nova
Sport	sport	k1gInSc1	sport
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zprovoznila	zprovoznit	k5eAaPmAgFnS	zprovoznit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
barrandovských	barrandovský	k2eAgInPc2d1	barrandovský
ateliérů	ateliér	k1gInPc2	ateliér
novou	nový	k2eAgFnSc4d1	nová
budovu	budova	k1gFnSc4	budova
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
s	s	k7c7	s
nejmodernějším	moderní	k2eAgNnSc7d3	nejmodernější
vybavením	vybavení	k1gNnSc7	vybavení
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
vysílání	vysílání	k1gNnSc4	vysílání
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
v	v	k7c6	v
HDTV	HDTV	kA	HDTV
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
pořadů	pořad	k1gInPc2	pořad
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Plnoformátový	Plnoformátový	k2eAgInSc1d1	Plnoformátový
kanál	kanál	k1gInSc1	kanál
Nova	novum	k1gNnSc2	novum
vysílá	vysílat	k5eAaImIp3nS	vysílat
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vysílání	vysílání	k1gNnSc6	vysílání
nabízí	nabízet	k5eAaImIp3nP	nabízet
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
sitcomy	sitcom	k1gInPc1	sitcom
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
či	či	k8xC	či
publicistické	publicistický	k2eAgInPc1d1	publicistický
pořady	pořad	k1gInPc1	pořad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
nechybí	chybit	k5eNaPmIp3nS	chybit
původní	původní	k2eAgFnSc1d1	původní
tvorba	tvorba	k1gFnSc1	tvorba
ani	ani	k8xC	ani
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
akvizice	akvizice	k1gFnPc1	akvizice
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
dostává	dostávat	k5eAaImIp3nS	dostávat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
již	již	k6eAd1	již
ráno	ráno	k6eAd1	ráno
Ranními	ranní	k2eAgFnPc7d1	ranní
Televizními	televizní	k2eAgFnPc7d1	televizní
novinami	novina	k1gFnPc7	novina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pořadu	pořad	k1gInSc6	pořad
Snídaně	snídaně	k1gFnSc2	snídaně
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vysílají	vysílat	k5eAaImIp3nP	vysílat
o	o	k7c4	o
dvanácté	dvanáctý	k4xOgNnSc4	dvanáctý
Polední	poledne	k1gNnPc2	poledne
Televizní	televizní	k2eAgFnSc2d1	televizní
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
v	v	k7c4	v
odpoledne	odpoledne	k1gNnSc4	odpoledne
v	v	k7c4	v
pět	pět	k4xCc4	pět
Odpolední	odpoledne	k1gNnPc2	odpoledne
Televizní	televizní	k2eAgFnSc2d1	televizní
noviny	novina	k1gFnSc2	novina
a	a	k8xC	a
o	o	k7c4	o
půl	půl	k1xP	půl
osmé	osmý	k4xOgFnSc2	osmý
začínají	začínat	k5eAaImIp3nP	začínat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
Televizní	televizní	k2eAgFnSc4d1	televizní
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k1gNnSc1	dopoledne
vysílá	vysílat	k5eAaImIp3nS	vysílat
reprízu	repríza	k1gFnSc4	repríza
svého	svůj	k3xOyFgInSc2	svůj
nekonečného	konečný	k2eNgInSc2d1	nekonečný
seriálu	seriál	k1gInSc2	seriál
Ulice	ulice	k1gFnSc2	ulice
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
následují	následovat	k5eAaImIp3nP	následovat
reprízy	repríza	k1gFnPc1	repríza
filmu	film	k1gInSc2	film
<g/>
/	/	kIx~	/
<g/>
seriálu	seriál	k1gInSc2	seriál
z	z	k7c2	z
předešlého	předešlý	k2eAgInSc2d1	předešlý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
od	od	k7c2	od
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Zbylý	zbylý	k2eAgInSc4d1	zbylý
čas	čas	k1gInSc4	čas
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
různé	různý	k2eAgInPc1d1	různý
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
pořady	pořad	k1gInPc1	pořad
nebo	nebo	k8xC	nebo
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
Nova	nova	k1gFnSc1	nova
vysílá	vysílat	k5eAaImIp3nS	vysílat
staré	starý	k2eAgInPc4d1	starý
československé	československý	k2eAgInPc4d1	československý
<g/>
,	,	kIx,	,
české	český	k2eAgInPc4d1	český
či	či	k8xC	či
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
pořady	pořad	k1gInPc4	pořad
Rady	rada	k1gFnSc2	rada
ptáka	pták	k1gMnSc4	pták
Loskutáka	loskuták	k1gMnSc4	loskuták
<g/>
,	,	kIx,	,
Koření	koření	k1gNnSc4	koření
nebo	nebo	k8xC	nebo
Volejte	volat	k5eAaImRp2nP	volat
Novu	nova	k1gFnSc4	nova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
Nova	nova	k1gFnSc1	nova
nabízí	nabízet	k5eAaImIp3nS	nabízet
svou	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Pilířem	pilíř	k1gInSc7	pilíř
programu	program	k1gInSc2	program
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc4	její
seriály	seriál	k1gInPc4	seriál
Policie	policie	k1gFnSc1	policie
Modrava	Modrava	k1gFnSc1	Modrava
<g/>
,	,	kIx,	,
Ordinace	ordinace	k1gFnSc1	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
<g/>
,	,	kIx,	,
Na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
Drazí	drahý	k2eAgMnPc1d1	drahý
sousedi	soused	k1gMnPc1	soused
či	či	k8xC	či
Expozitura	expozitura	k1gFnSc1	expozitura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nichž	jenž	k3xRgFnPc6	jenž
většinou	většinou	k6eAd1	většinou
následují	následovat	k5eAaImIp3nP	následovat
reprízy	repríza	k1gFnPc4	repríza
již	již	k6eAd1	již
nevysílaných	vysílaný	k2eNgNnPc2d1	nevysílané
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
seriálů	seriál	k1gInPc2	seriál
nebo	nebo	k8xC	nebo
sitcomů	sitcom	k1gInPc2	sitcom
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
své	svůj	k3xOyFgInPc4	svůj
publicistické	publicistický	k2eAgInPc4d1	publicistický
pořady	pořad	k1gInPc4	pořad
Víkend	víkend	k1gInSc1	víkend
<g/>
,	,	kIx,	,
Střepiny	střepina	k1gFnPc1	střepina
<g/>
,	,	kIx,	,
Prásk	prásk	k0	prásk
<g/>
!	!	kIx.	!
</s>
<s>
apod.	apod.	kA	apod.
V	v	k7c6	v
méně	málo	k6eAd2	málo
sledovaných	sledovaný	k2eAgNnPc2d1	sledované
období	období	k1gNnPc2	období
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
léto	léto	k1gNnSc1	léto
vysílá	vysílat	k5eAaImIp3nS	vysílat
či	či	k8xC	či
vysílala	vysílat	k5eAaImAgFnS	vysílat
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
i	i	k8xC	i
seriály	seriál	k1gInPc1	seriál
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
-	-	kIx~	-
Plechová	plechový	k2eAgFnSc1d1	plechová
kavalérie	kavalérie	k1gFnSc1	kavalérie
<g/>
,	,	kIx,	,
Chalupáři	chalupář	k1gMnPc1	chalupář
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programové	programový	k2eAgFnSc6d1	programová
nabídce	nabídka	k1gFnSc6	nabídka
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
reality	realita	k1gFnSc2	realita
show	show	k1gFnPc2	show
jako	jako	k8xC	jako
SuperStar	superstar	k1gFnSc4	superstar
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc4	hlas
Česko	Česko	k1gNnSc4	Česko
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
MasterChef	MasterChef	k1gInSc1	MasterChef
Česko	Česko	k1gNnSc1	Česko
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
formáty	formát	k1gInPc1	formát
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
programu	program	k1gInSc6	program
objevují	objevovat	k5eAaImIp3nP	objevovat
také	také	k9	také
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
podvečerních	podvečerní	k2eAgInPc2d1	podvečerní
nebo	nebo	k8xC	nebo
večerních	večerní	k2eAgInPc2d1	večerní
slotů	slot	k1gInPc2	slot
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
i	i	k9	i
největší	veliký	k2eAgInPc4d3	veliký
seriálové	seriálový	k2eAgInPc4d1	seriálový
hity	hit	k1gInPc4	hit
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
kriminální	kriminální	k2eAgInPc1d1	kriminální
seriály	seriál	k1gInPc1	seriál
jako	jako	k8xS	jako
Námořní	námořní	k2eAgFnSc1d1	námořní
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
Stalker	Stalker	k1gInSc1	Stalker
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
Mentalista	Mentalista	k1gMnSc1	Mentalista
<g/>
,	,	kIx,	,
Zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
pořádek	pořádek	k1gInSc1	pořádek
<g/>
:	:	kIx,	:
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
oběti	oběť	k1gFnPc4	oběť
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kriminální	kriminální	k2eAgInPc1d1	kriminální
seriály	seriál	k1gInPc1	seriál
nejsou	být	k5eNaImIp3nP	být
jediné	jediný	k2eAgInPc1d1	jediný
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Nova	nova	k1gFnSc1	nova
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
nechybějí	chybět	k5eNaImIp3nP	chybět
také	také	k9	také
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
sitcomy	sitcom	k1gInPc1	sitcom
<g/>
,	,	kIx,	,
sci-fi	scii	k1gNnPc1	sci-fi
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
-	-	kIx~	-
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
půl	půl	k1xP	půl
chlapa	chlap	k1gMnSc2	chlap
<g/>
,	,	kIx,	,
Plastická	plastický	k2eAgFnSc1d1	plastická
chirurgie	chirurgie	k1gFnSc1	chirurgie
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Máma	máma	k1gFnSc1	máma
<g/>
,	,	kIx,	,
Superkočky	Superkočka	k1gFnPc1	Superkočka
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gMnSc1	Am
<g/>
,	,	kIx,	,
Pohotovost	pohotovost	k1gFnSc1	pohotovost
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
Nikita	Nikitum	k1gNnSc2	Nikitum
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
známé	známý	k2eAgInPc4d1	známý
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
seriály	seriál	k1gInPc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
americké	americký	k2eAgFnSc2d1	americká
produkce	produkce	k1gFnSc2	produkce
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
také	také	k6eAd1	také
práva	právo	k1gNnSc2	právo
např.	např.	kA	např.
na	na	k7c4	na
německé	německý	k2eAgInPc4d1	německý
nebo	nebo	k8xC	nebo
turecké	turecký	k2eAgInPc4d1	turecký
seriály	seriál	k1gInPc4	seriál
-	-	kIx~	-
Kobra	kobra	k1gFnSc1	kobra
11	[number]	k4	11
nebo	nebo	k8xC	nebo
telenovela	telenovela	k1gFnSc1	telenovela
Tisíc	tisíc	k4xCgInSc1	tisíc
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Vysíláním	vysílání	k1gNnSc7	vysílání
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
akvizice	akvizice	k1gFnSc2	akvizice
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
Nova	nova	k1gFnSc1	nova
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
komediální	komediální	k2eAgInPc1d1	komediální
seriály	seriál	k1gInPc1	seriál
a	a	k8xC	a
odpoledne	odpoledne	k1gNnSc1	odpoledne
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
objevují	objevovat	k5eAaImIp3nP	objevovat
spíše	spíše	k9	spíše
kriminální	kriminální	k2eAgInPc1d1	kriminální
seriály	seriál	k1gInPc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
seriály	seriál	k1gInPc1	seriál
však	však	k9	však
nechybí	chybit	k5eNaPmIp3nP	chybit
ani	ani	k8xC	ani
ve	v	k7c6	v
víkendovém	víkendový	k2eAgNnSc6d1	víkendové
dopoledni	dopoledne	k1gNnSc6	dopoledne
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
jako	jako	k8xC	jako
např.	např.	kA	např.
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Námořní	námořní	k2eAgFnSc1d1	námořní
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Miami	Miami	k1gNnSc2	Miami
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
také	také	k9	také
v	v	k7c6	v
prime	prim	k1gInSc5	prim
time	tim	k1gMnPc4	tim
<g/>
.	.	kIx.	.
</s>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
blok	blok	k1gInSc1	blok
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
a	a	k8xC	a
v	v	k7c4	v
neděli	neděle	k1gFnSc4	neděle
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
nabízené	nabízený	k2eAgInPc4d1	nabízený
známé	známý	k2eAgInPc4d1	známý
animované	animovaný	k2eAgInPc4d1	animovaný
či	či	k8xC	či
hrané	hraný	k2eAgInPc4d1	hraný
seriály	seriál	k1gInPc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
blok	blok	k1gInSc1	blok
začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
kolem	kolem	k7c2	kolem
šesté	šestý	k4xOgFnSc2	šestý
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
seriály	seriál	k1gInPc1	seriál
jako	jako	k8xS	jako
Oggy	Ogga	k1gFnPc1	Ogga
a	a	k8xC	a
škodíci	škodík	k1gMnPc1	škodík
<g/>
,	,	kIx,	,
Monstra	monstrum	k1gNnPc4	monstrum
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Vetřelci	vetřelec	k1gMnPc1	vetřelec
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
<g/>
,	,	kIx,	,
Bukugan	Bukugan	k1gInSc1	Bukugan
<g/>
,	,	kIx,	,
Tučňáci	tučňák	k1gMnPc1	tučňák
z	z	k7c2	z
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
,	,	kIx,	,
Dinofoz	Dinofoz	k1gInSc1	Dinofoz
<g/>
,	,	kIx,	,
Jak	jak	k8xS	jak
vycvičit	vycvičit	k5eAaPmF	vycvičit
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
Jake	Jake	k1gFnSc4	Jake
a	a	k8xC	a
piráti	pirát	k1gMnPc1	pirát
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
Nezemě	Nezema	k1gFnSc6	Nezema
nebo	nebo	k8xC	nebo
Hannah	Hannah	k1gMnSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Blok	blok	k1gInSc1	blok
kolem	kolem	k7c2	kolem
osmé	osmý	k4xOgFnSc2	osmý
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
rodinný	rodinný	k2eAgInSc4d1	rodinný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Voyo	Voyo	k6eAd1	Voyo
nabízí	nabízet	k5eAaImIp3nS	nabízet
placený	placený	k2eAgInSc1d1	placený
archív	archív	k1gInSc1	archív
všech	všecek	k3xTgInPc2	všecek
původních	původní	k2eAgInPc2d1	původní
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybit	k5eNaPmIp3nS	chybit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
také	také	k9	také
kompletní	kompletní	k2eAgInSc4d1	kompletní
rejstřík	rejstřík	k1gInSc4	rejstřík
českých	český	k2eAgInPc2d1	český
a	a	k8xC	a
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
nabízí	nabízet	k5eAaImIp3nS	nabízet
katalog	katalog	k1gInSc4	katalog
amerických	americký	k2eAgInPc2d1	americký
seriálů	seriál	k1gInPc2	seriál
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
titulky	titulek	k1gInPc7	titulek
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Předplatitelé	předplatitel	k1gMnPc1	předplatitel
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
seriály	seriál	k1gInPc4	seriál
původní	původní	k2eAgFnSc2d1	původní
tvorby	tvorba	k1gFnSc2	tvorba
před	před	k7c7	před
jejich	jejich	k3xOp3gFnSc7	jejich
premiérou	premiéra	k1gFnSc7	premiéra
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
20	[number]	k4	20
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
spustila	spustit	k5eAaPmAgFnS	spustit
Nova	nova	k1gFnSc1	nova
Group	Group	k1gInSc1	Group
video	video	k1gNnSc1	video
portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
nabídce	nabídka	k1gFnSc6	nabídka
je	být	k5eAaImIp3nS	být
archiv	archiv	k1gInSc1	archiv
původních	původní	k2eAgInPc2d1	původní
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
sedmi	sedm	k4xCc2	sedm
dní	den	k1gInPc2	den
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
je	on	k3xPp3gInPc4	on
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
placený	placený	k2eAgInSc4d1	placený
portál	portál	k1gInSc4	portál
Voyo	Voyo	k6eAd1	Voyo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
začala	začít	k5eAaPmAgFnS	začít
uvádět	uvádět	k5eAaImF	uvádět
také	také	k9	také
původní	původní	k2eAgInPc4d1	původní
internetové	internetový	k2eAgInPc4d1	internetový
pořady	pořad	k1gInPc4	pořad
jako	jako	k8xS	jako
Na	na	k7c6	na
baru	bar	k1gInSc6	bar
<g/>
,	,	kIx,	,
Maminky	maminka	k1gFnPc4	maminka
s	s	k7c7	s
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
Hravě	hravě	k6eAd1	hravě
zdravě	zdravě	k6eAd1	zdravě
s	s	k7c7	s
Kristinou	Kristina	k1gFnSc7	Kristina
<g/>
,	,	kIx,	,
Brána	brána	k1gFnSc1	brána
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
Vaše	váš	k3xOp2gNnSc1	váš
téma	téma	k1gNnSc4	téma
nebo	nebo	k8xC	nebo
Trendy	trend	k1gInPc4	trend
trendy	trend	k1gInPc4	trend
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybět	k5eNaImIp3nS	chybět
také	také	k9	také
přenosy	přenos	k1gInPc4	přenos
z	z	k7c2	z
exkluzivních	exkluzivní	k2eAgFnPc2d1	exkluzivní
hudebních	hudební	k2eAgFnPc2d1	hudební
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Nova	nova	k1gFnSc1	nova
Plus	plus	k1gInSc1	plus
také	také	k9	také
pořady	pořad	k1gInPc4	pořad
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
slovenské	slovenský	k2eAgFnSc2d1	slovenská
Markízy	Markíza	k1gFnSc2	Markíza
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
také	také	k6eAd1	také
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
verzi	verze	k1gFnSc4	verze
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc1	show
Farma	farma	k1gFnSc1	farma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
zahájila	zahájit	k5eAaPmAgFnS	zahájit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
ČR	ČR	kA	ČR
svůj	svůj	k3xOyFgInSc4	svůj
kanál	kanál	k1gInSc4	kanál
v	v	k7c6	v
HD	HD	kA	HD
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Nova	nova	k1gFnSc1	nova
již	již	k6eAd1	již
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
veletrhu	veletrh	k1gInSc2	veletrh
INVEX	INVEX	kA	INVEX
2007	[number]	k4	2007
experimentálně	experimentálně	k6eAd1	experimentálně
vysílala	vysílat	k5eAaImAgFnS	vysílat
HD	HD	kA	HD
na	na	k7c6	na
platformách	platforma	k1gFnPc6	platforma
DVB-T	DVB-T	k1gFnSc2	DVB-T
<g/>
,	,	kIx,	,
IPTV	IPTV	kA	IPTV
a	a	k8xC	a
satelitu	satelit	k1gInSc2	satelit
Astra	astra	k1gFnSc1	astra
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
se	s	k7c7	s
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nakoupit	nakoupit	k5eAaPmF	nakoupit
více	hodně	k6eAd2	hodně
techniky	technika	k1gFnSc2	technika
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
v	v	k7c6	v
HD	HD	kA	HD
a	a	k8xC	a
prvního	první	k4xOgInSc2	první
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
HD	HD	kA	HD
naostro	naostro	k6eAd1	naostro
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
zpravodajských	zpravodajský	k2eAgInPc2d1	zpravodajský
a	a	k8xC	a
publicistických	publicistický	k2eAgInPc2d1	publicistický
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
v	v	k7c6	v
prime	prim	k1gInSc5	prim
time	time	k1gInSc1	time
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Nova	nova	k1gFnSc1	nova
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c4	v
HD	HD	kA	HD
Soukromé	soukromý	k2eAgFnPc4d1	soukromá
pasti	past	k1gFnPc4	past
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Anděl	Anděla	k1gFnPc2	Anděla
<g/>
,	,	kIx,	,
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
štěstí	štěstí	k1gNnSc2	štěstí
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Ulice	ulice	k1gFnSc1	ulice
s	s	k7c7	s
Ordinací	ordinace	k1gFnSc7	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgFnP	být
představeny	představit	k5eAaPmNgInP	představit
také	také	k9	také
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
filmy	film	k1gInPc1	film
a	a	k8xC	a
seriály	seriál	k1gInPc1	seriál
v	v	k7c6	v
HD	HD	kA	HD
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
<g/>
,	,	kIx,	,
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
nebo	nebo	k8xC	nebo
Útěk	útěk	k1gInSc1	útěk
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pořadů	pořad	k1gInPc2	pořad
například	například	k6eAd1	například
Snídaně	snídaně	k1gFnSc1	snídaně
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
<g/>
,	,	kIx,	,
Víkend	víkend	k1gInSc1	víkend
<g/>
,	,	kIx,	,
Koření	koření	k1gNnPc1	koření
či	či	k8xC	či
Střepiny	střepina	k1gFnPc1	střepina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vysílala	vysílat	k5eAaImAgFnS	vysílat
Nova	nova	k1gFnSc1	nova
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
%	%	kIx~	%
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
poměrně	poměrně	k6eAd1	poměrně
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
HD	HD	kA	HD
vysílání	vysílání	k1gNnSc1	vysílání
je	být	k5eAaImIp3nS	být
šířeno	šířit	k5eAaImNgNnS	šířit
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vysílán	vysílán	k2eAgInSc4d1	vysílán
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
natočen	natočit	k5eAaBmNgInS	natočit
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
se	se	k3xPyFc4	se
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c6	na
satelitu	satelit	k1gInSc6	satelit
a	a	k8xC	a
v	v	k7c6	v
IPTV	IPTV	kA	IPTV
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
přestala	přestat	k5eAaPmAgFnS	přestat
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
multiplexu	multiplex	k1gInSc6	multiplex
4	[number]	k4	4
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Nova	nova	k1gFnSc1	nova
Cinema	Cinema	k1gFnSc1	Cinema
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nejsledovanější	sledovaný	k2eAgFnSc7d3	nejsledovanější
televizí	televize	k1gFnSc7	televize
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
jejím	její	k3xOp3gInSc7	její
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
pořadem	pořad	k1gInSc7	pořad
Televizní	televizní	k2eAgFnSc2d1	televizní
noviny	novina	k1gFnSc2	novina
a	a	k8xC	a
seriál	seriál	k1gInSc1	seriál
Ordinace	ordinace	k1gFnSc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
seriálem	seriál	k1gInSc7	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pravidelně	pravidelně	k6eAd1	pravidelně
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
sledovanost	sledovanost	k1gFnSc4	sledovanost
nad	nad	k7c7	nad
jedním	jeden	k4xCgInSc7	jeden
milionem	milion	k4xCgInSc7	milion
byl	být	k5eAaImAgInS	být
také	také	k9	také
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
lékařského	lékařský	k2eAgNnSc2d1	lékařské
prostředí	prostředí	k1gNnSc2	prostředí
spin-off	spinff	k1gInSc1	spin-off
Ordinace	ordinace	k1gFnSc2	ordinace
Doktoři	doktor	k1gMnPc1	doktor
z	z	k7c2	z
Počátků	počátek	k1gInPc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
data	datum	k1gNnPc1	datum
o	o	k7c6	o
sledovanosti	sledovanost	k1gFnSc6	sledovanost
dostaly	dostat	k5eAaPmAgFnP	dostat
české	český	k2eAgFnPc1d1	Česká
televize	televize	k1gFnPc1	televize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
TOP	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
nejsledovanějších	sledovaný	k2eAgInPc2d3	nejsledovanější
pořadů	pořad	k1gInPc2	pořad
právě	právě	k6eAd1	právě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
drží	držet	k5eAaImIp3nS	držet
Nova	nova	k1gFnSc1	nova
prvenství	prvenství	k1gNnSc2	prvenství
s	s	k7c7	s
Miss	miss	k1gFnSc7	miss
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
nenechalo	nechat	k5eNaPmAgNnS	nechat
ujít	ujít	k5eAaPmF	ujít
celkem	celkem	k6eAd1	celkem
4,992	[number]	k4	4,992
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
starších	starší	k1gMnPc2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
objevují	objevovat	k5eAaImIp3nP	objevovat
pořady	pořad	k1gInPc1	pořad
jako	jako	k8xS	jako
Pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
<g/>
,	,	kIx,	,
Novoty	novota	k1gFnPc1	novota
<g/>
,	,	kIx,	,
Silvestr	Silvestr	k1gInSc1	Silvestr
2000	[number]	k4	2000
nebo	nebo	k8xC	nebo
MS	MS	kA	MS
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
celoročního	celoroční	k2eAgInSc2d1	celoroční
podílu	podíl	k1gInSc2	podíl
40,95	[number]	k4	40,95
procenta	procento	k1gNnSc2	procento
na	na	k7c6	na
celodenní	celodenní	k2eAgFnSc6d1	celodenní
sledovanosti	sledovanost	k1gFnSc6	sledovanost
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
starších	starý	k2eAgNnPc2d2	starší
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
činil	činit	k5eAaImAgInS	činit
podíl	podíl	k1gInSc1	podíl
Novy	nova	k1gFnSc2	nova
na	na	k7c6	na
42,25	[number]	k4	42,25
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejsledovanější	sledovaný	k2eAgInPc4d3	nejsledovanější
pořady	pořad	k1gInPc4	pořad
patřily	patřit	k5eAaImAgFnP	patřit
Televizní	televizní	k2eAgFnPc4d1	televizní
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
Česko	Česko	k1gNnSc4	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc1	superstar
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
On	on	k3xPp3gInSc1	on
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
!	!	kIx.	!
</s>
<s>
nebo	nebo	k8xC	nebo
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
nasazení	nasazení	k1gNnSc1	nasazení
nových	nový	k2eAgInPc2d1	nový
seriálů	seriál	k1gInPc2	seriál
Ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
Ordinace	ordinace	k1gFnSc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Nova	nova	k1gFnSc1	nova
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejsledovanějších	sledovaný	k2eAgInPc2d3	nejsledovanější
pořadů	pořad	k1gInPc2	pořad
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
největšími	veliký	k2eAgInPc7d3	veliký
taháky	tahák	k1gInPc7	tahák
programu	program	k1gInSc2	program
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Ordinace	ordinace	k1gFnPc1	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
<g/>
,	,	kIx,	,
Televizní	televizní	k2eAgFnPc1d1	televizní
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Doktoři	doktor	k1gMnPc1	doktor
z	z	k7c2	z
Počátků	počátek	k1gInPc2	počátek
<g/>
,	,	kIx,	,
Počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Slavík	slavík	k1gInSc1	slavík
Mattoni	Matton	k1gMnPc1	Matton
<g/>
,	,	kIx,	,
Ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
Prásk	prásk	k0	prásk
<g/>
!	!	kIx.	!
</s>
<s>
z	z	k7c2	z
filmů	film	k1gInPc2	film
potom	potom	k6eAd1	potom
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
a	a	k8xC	a
Probudím	probudit	k5eAaPmIp1nS	probudit
se	se	k3xPyFc4	se
včera	včera	k6eAd1	včera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
stanice	stanice	k1gFnSc1	stanice
uvedla	uvést	k5eAaPmAgFnS	uvést
seriál	seriál	k1gInSc4	seriál
Policie	policie	k1gFnSc1	policie
Modrava	Modrava	k1gFnSc1	Modrava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
na	na	k7c6	na
postu	post	k1gInSc6	post
nejsledovanějšího	sledovaný	k2eAgInSc2d3	nejsledovanější
seriálu	seriál	k1gInSc2	seriál
současnosti	současnost	k1gFnSc2	současnost
seriál	seriál	k1gInSc1	seriál
Ordinace	ordinace	k1gFnSc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
drží	držet	k5eAaImIp3nS	držet
pozici	pozice	k1gFnSc4	pozice
nejsledovanějšího	sledovaný	k2eAgInSc2d3	nejsledovanější
nekonečného	konečný	k2eNgInSc2d1	nekonečný
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
pořady	pořad	k1gInPc7	pořad
byly	být	k5eAaImAgFnP	být
také	také	k9	také
Výměna	výměna	k1gFnSc1	výměna
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
Ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
Televizní	televizní	k2eAgFnPc4d1	televizní
noviny	novina	k1gFnPc4	novina
a	a	k8xC	a
mírný	mírný	k2eAgInSc4d1	mírný
pokles	pokles	k1gInSc4	pokles
zaregistrovali	zaregistrovat	k5eAaPmAgMnP	zaregistrovat
Doktoři	doktor	k1gMnPc1	doktor
z	z	k7c2	z
Počátků	počátek	k1gInPc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
udělil	udělit	k5eAaPmAgMnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
TV	TV	kA	TV
Nova	novum	k1gNnSc2	novum
a	a	k8xC	a
jejímu	její	k3xOp3gInSc3	její
pořadu	pořad	k1gInSc3	pořad
Věštírna	věštírna	k1gFnSc1	věštírna
anticenu	anticen	k2eAgFnSc4d1	anticena
bronzový	bronzový	k2eAgInSc4d1	bronzový
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
družstev	družstvo	k1gNnPc2	družstvo
za	za	k7c4	za
"	"	kIx"	"
<g/>
příkladné	příkladný	k2eAgNnSc4d1	příkladné
zvyšování	zvyšování	k1gNnSc4	zvyšování
sebevědomí	sebevědomí	k1gNnSc2	sebevědomí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
"	"	kIx"	"
<g/>
Věštírna	věštírna	k1gFnSc1	věštírna
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
zcela	zcela	k6eAd1	zcela
nevýrazný	výrazný	k2eNgMnSc1d1	nevýrazný
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
senzibilem	senzibil	k1gMnSc7	senzibil
či	či	k8xC	či
zázračníkem	zázračník	k1gMnSc7	zázračník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
Věštírně	věštírna	k1gFnSc6	věštírna
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
může	moct	k5eAaImIp3nS	moct
před	před	k7c7	před
kamerami	kamera	k1gFnPc7	kamera
stát	stát	k5eAaImF	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vysílání	vysílání	k1gNnSc4	vysílání
soutěže	soutěž	k1gFnSc2	soutěž
Big	Big	k1gFnSc2	Big
Brother	Brother	kA	Brother
mezi	mezi	k7c4	mezi
šestou	šestý	k4xOgFnSc4	šestý
ranní	ranní	k1gFnSc4	ranní
a	a	k8xC	a
dvaadvacátou	dvaadvacátý	k4xOgFnSc7	dvaadvacátý
hodinou	hodina	k1gFnSc7	hodina
večer	večer	k6eAd1	večer
udělila	udělit	k5eAaPmAgFnS	udělit
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
Nově	nově	k6eAd1	nově
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pokuty	pokuta	k1gFnSc2	pokuta
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
43,3	[number]	k4	43,3
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Rady	rada	k1gFnSc2	rada
byly	být	k5eAaImAgInP	být
díly	díl	k1gInPc1	díl
vysílané	vysílaný	k2eAgInPc1d1	vysílaný
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
čase	čas	k1gInSc6	čas
způsobilé	způsobilý	k2eAgFnSc2d1	způsobilá
ohrozit	ohrozit	k5eAaPmF	ohrozit
fyzický	fyzický	k2eAgInSc4d1	fyzický
<g/>
,	,	kIx,	,
psychický	psychický	k2eAgInSc4d1	psychický
i	i	k8xC	i
morální	morální	k2eAgInSc4d1	morální
vývoj	vývoj	k1gInSc4	vývoj
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
řadou	řada	k1gFnSc7	řada
manipulativních	manipulativní	k2eAgFnPc2d1	manipulativní
praktik	praktika	k1gFnPc2	praktika
<g/>
,	,	kIx,	,
agresí	agrese	k1gFnSc7	agrese
<g/>
,	,	kIx,	,
verbální	verbální	k2eAgFnSc7d1	verbální
vulgaritou	vulgarita	k1gFnSc7	vulgarita
<g/>
,	,	kIx,	,
jednáním	jednání	k1gNnSc7	jednání
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
uměle	uměle	k6eAd1	uměle
a	a	k8xC	a
samoúčelně	samoúčelně	k6eAd1	samoúčelně
exponovanou	exponovaný	k2eAgFnSc7d1	exponovaná
sexualitou	sexualita	k1gFnSc7	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
změnila	změnit	k5eAaPmAgFnS	změnit
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
vzhled	vzhled	k1gInSc1	vzhled
své	svůj	k3xOyFgFnSc2	svůj
oficiální	oficiální	k2eAgFnSc2d1	oficiální
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
prvcích	prvek	k1gInPc6	prvek
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
webem	web	k1gInSc7	web
americké	americký	k2eAgFnSc2d1	americká
stanice	stanice	k1gFnSc2	stanice
CBS	CBS	kA	CBS
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
upozornění	upozornění	k1gNnSc6	upozornění
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
odstranila	odstranit	k5eAaPmAgFnS	odstranit
některé	některý	k3yIgNnSc4	některý
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
CBS	CBS	kA	CBS
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
kroky	krok	k1gInPc7	krok
stanice	stanice	k1gFnSc2	stanice
Nova	nova	k1gFnSc1	nova
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc4	stránka
obou	dva	k4xCgFnPc2	dva
televizí	televize	k1gFnPc2	televize
byly	být	k5eAaImAgInP	být
založené	založený	k2eAgInPc1d1	založený
úplně	úplně	k6eAd1	úplně
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
kódech	kód	k1gInPc6	kód
při	při	k7c6	při
sestavování	sestavování	k1gNnSc6	sestavování
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
a	a	k8xC	a
současně	současně	k6eAd1	současně
první	první	k4xOgMnSc1	první
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
televize	televize	k1gFnSc2	televize
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
kanálu	kanál	k1gInSc3	kanál
Nova	novum	k1gNnSc2	novum
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
kanál	kanál	k1gInSc1	kanál
-	-	kIx~	-
Nova	nova	k1gFnSc1	nova
Cinema	Cinema	k1gFnSc1	Cinema
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
start	start	k1gInSc1	start
digitalizace	digitalizace	k1gFnSc2	digitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vysílat	vysílat	k5eAaImF	vysílat
dva	dva	k4xCgInPc1	dva
důležité	důležitý	k2eAgInPc1d1	důležitý
seriálu	seriál	k1gInSc2	seriál
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
Ordinace	ordinace	k1gFnSc1	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
drží	držet	k5eAaImIp3nS	držet
vysokou	vysoký	k2eAgFnSc4d1	vysoká
sledovanost	sledovanost	k1gFnSc4	sledovanost
i	i	k9	i
na	na	k7c4	na
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2010	[number]	k4	2010
odchází	odcházet	k5eAaImIp3nP	odcházet
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
TV	TV	kA	TV
Nova	novum	k1gNnSc2	novum
a	a	k8xC	a
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
jako	jako	k9	jako
viceprezident	viceprezident	k1gMnSc1	viceprezident
v	v	k7c6	v
CME	CME	kA	CME
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
jednatelem	jednatel	k1gMnSc7	jednatel
společnosti	společnost	k1gFnSc2	společnost
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
spravoval	spravovat	k5eAaImAgMnS	spravovat
přes	přes	k7c4	přes
svého	svůj	k3xOyFgMnSc4	svůj
zástupce	zástupce	k1gMnSc4	zástupce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Davida	David	k1gMnSc2	David
Stogela	Stogel	k1gMnSc2	Stogel
samotnou	samotný	k2eAgFnSc4d1	samotná
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
neobsadil	obsadit	k5eNaPmAgMnS	obsadit
místo	místo	k1gNnSc4	místo
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Jan	Jan	k1gMnSc1	Jan
Andruško	Andruška	k1gFnSc5	Andruška
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Andruško	Andruška	k1gFnSc5	Andruška
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
–	–	k?	–
Za	za	k7c2	za
úřadování	úřadování	k1gNnSc2	úřadování
Andruška	Andruška	k1gFnSc1	Andruška
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
čtyři	čtyři	k4xCgInPc1	čtyři
nové	nový	k2eAgInPc1d1	nový
kanály	kanál	k1gInPc1	kanál
-	-	kIx~	-
Fanda	Fanda	k1gFnSc1	Fanda
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
internetový	internetový	k2eAgInSc1d1	internetový
kanál	kanál	k1gInSc1	kanál
Nova	novum	k1gNnSc2	novum
News	Newsa	k1gFnPc2	Newsa
a	a	k8xC	a
naposled	naposled	k6eAd1	naposled
Telka	Telka	k?	Telka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Andruška	Andruška	k1gFnSc1	Andruška
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
televize	televize	k1gFnSc1	televize
ze	z	k7c2	z
sdružení	sdružení	k1gNnSc2	sdružení
ATO	ATO	kA	ATO
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
své	svůj	k3xOyFgNnSc4	svůj
sdružení	sdružení	k1gNnSc4	sdružení
SPMS	SPMS	kA	SPMS
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
sledovanosti	sledovanost	k1gFnSc2	sledovanost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
rozběhlo	rozběhnout	k5eAaPmAgNnS	rozběhnout
nový	nový	k2eAgInSc4d1	nový
výzkum	výzkum	k1gInSc4	výzkum
sledovanosti	sledovanost	k1gFnSc2	sledovanost
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
AC	AC	kA	AC
Nielsen	Nielsen	k1gInSc1	Nielsen
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
bylo	být	k5eAaImAgNnS	být
následujícím	následující	k2eAgInSc7d1	následující
ředitelem	ředitel	k1gMnSc7	ředitel
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
stopáž	stopáž	k1gFnSc4	stopáž
Televizních	televizní	k2eAgFnPc2d1	televizní
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
posunul	posunout	k5eAaPmAgInS	posunout
se	se	k3xPyFc4	se
start	start	k1gInSc1	start
hlavního	hlavní	k2eAgInSc2d1	hlavní
programu	program	k1gInSc2	program
na	na	k7c4	na
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
změnila	změnit	k5eAaPmAgFnS	změnit
Nova	nova	k1gFnSc1	nova
Group	Group	k1gInSc1	Group
svojí	svojit	k5eAaImIp3nS	svojit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
razantním	razantní	k2eAgNnSc6d1	razantní
zvýšení	zvýšení	k1gNnSc6	zvýšení
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
ve	v	k7c6	v
zrušení	zrušení	k1gNnSc6	zrušení
bonusů	bonus	k1gInPc2	bonus
mediálním	mediální	k2eAgFnPc3d1	mediální
agenturám	agentura	k1gFnPc3	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Andruško	Andruška	k1gFnSc5	Andruška
ukončil	ukončit	k5eAaPmAgInS	ukončit
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
říjnu	říjen	k1gInSc3	říjen
2013	[number]	k4	2013
své	svůj	k3xOyFgNnSc1	svůj
působení	působení	k1gNnSc1	působení
na	na	k7c6	na
postu	post	k1gInSc6	post
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Nova	nova	k1gFnSc1	nova
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Oznámení	oznámení	k1gNnSc1	oznámení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
personálních	personální	k2eAgFnPc2d1	personální
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
CME	CME	kA	CME
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Adriana	Adrian	k1gMnSc2	Adrian
Sârbua	Sârbuus	k1gMnSc2	Sârbuus
<g/>
.	.	kIx.	.
</s>
<s>
Christoph	Christoph	k1gMnSc1	Christoph
Mainusch	Mainusch	k1gMnSc1	Mainusch
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
–	–	k?	–
Mainusch	Mainusch	k1gInSc1	Mainusch
není	být	k5eNaImIp3nS	být
úplný	úplný	k2eAgMnSc1d1	úplný
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
pozici	pozice	k1gFnSc4	pozice
ko-prezidenta	korezident	k1gMnSc2	ko-prezident
CME	CME	kA	CME
a	a	k8xC	a
současně	současně	k6eAd1	současně
jednatele	jednatel	k1gMnSc2	jednatel
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
řídí	řídit	k5eAaImIp3nS	řídit
správu	správa	k1gFnSc4	správa
Nova	novum	k1gNnSc2	novum
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
mu	on	k3xPp3gMnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zástupce	zástupce	k1gMnSc1	zástupce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
David	David	k1gMnSc1	David
Stogel	Stogel	k1gMnSc1	Stogel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stejnou	stejný	k2eAgFnSc4d1	stejná
pozici	pozice	k1gFnSc4	pozice
zastával	zastávat	k5eAaImAgMnS	zastávat
již	již	k6eAd1	již
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Petra	Petr	k1gMnSc2	Petr
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Ukončil	ukončit	k5eAaPmAgMnS	ukončit
přípravy	příprava	k1gFnPc4	příprava
druhého	druhý	k4xOgNnSc2	druhý
měření	měření	k1gNnSc2	měření
sledovanosti	sledovanost	k1gFnSc2	sledovanost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
připravovat	připravovat	k5eAaImF	připravovat
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
Jana	Jana	k1gFnSc1	Jana
Andruška	Andruška	k1gFnSc1	Andruška
na	na	k7c6	na
Nově	nova	k1gFnSc6	nova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
restrukturalizace	restrukturalizace	k1gFnSc1	restrukturalizace
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
skončil	skončit	k5eAaPmAgInS	skončit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
licence	licence	k1gFnSc1	licence
pro	pro	k7c4	pro
internetový	internetový	k2eAgInSc4d1	internetový
kanál	kanál	k1gInSc4	kanál
Nova	nova	k1gFnSc1	nova
News	News	k1gInSc1	News
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
ukončil	ukončit	k5eAaPmAgMnS	ukončit
po	po	k7c6	po
domluvě	domluva	k1gFnSc6	domluva
zástupce	zástupce	k1gMnSc2	zástupce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
David	David	k1gMnSc1	David
Stogel	Stogel	k1gMnSc1	Stogel
svůj	svůj	k3xOyFgInSc4	svůj
pracovní	pracovní	k2eAgInSc4d1	pracovní
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Kašparová	Kašparová	k1gFnSc1	Kašparová
<g/>
:	:	kIx,	:
Pravda	pravda	k1gFnSc1	pravda
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Novy	nova	k1gFnSc2	nova
<g/>
,	,	kIx,	,
Formát	formát	k1gInSc1	formát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86155-16-1	[number]	k4	80-86155-16-1
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
<g/>
:	:	kIx,	:
Štvanice	Štvanice	k1gFnSc1	Štvanice
<g/>
:	:	kIx,	:
tu	ten	k3xDgFnSc4	ten
televizi	televize	k1gFnSc4	televize
nedáme	dát	k5eNaPmIp1nP	dát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Knihcentrum	Knihcentrum	k1gNnSc1	Knihcentrum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86054-85-3	[number]	k4	80-86054-85-3
Miroslava	Miroslava	k1gFnSc1	Miroslava
Besserová	Besserová	k1gFnSc1	Besserová
<g/>
:	:	kIx,	:
NOVA	nova	k1gFnSc1	nova
křížem	kříž	k1gInSc7	kříž
krážem	krážem	k6eAd1	krážem
<g/>
,	,	kIx,	,
Formát	formát	k1gInSc1	formát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86718-28-X	[number]	k4	80-86718-28-X
Petr	Petr	k1gMnSc1	Petr
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
:	:	kIx,	:
Válka	válka	k1gFnSc1	válka
o	o	k7c6	o
Novu	nov	k1gInSc6	nov
aneb	aneb	k?	aneb
Podvod	podvod	k1gInSc1	podvod
za	za	k7c4	za
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
,	,	kIx,	,
Formát	formát	k1gInSc1	formát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86718-48-4	[number]	k4	80-86718-48-4
Jan	Jan	k1gMnSc1	Jan
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
:	:	kIx,	:
Moc	moc	k1gFnSc1	moc
a	a	k8xC	a
nemoc	nemoc	k1gFnSc1	nemoc
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7260-282-7	[number]	k4	978-80-7260-282-7
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
TV	TV	kA	TV
Nova	novum	k1gNnSc2	novum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
Licenční	licenční	k2eAgFnSc2d1	licenční
podmínky	podmínka	k1gFnSc2	podmínka
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
</s>
