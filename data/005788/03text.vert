<s>
Ucho	ucho	k1gNnSc1	ucho
je	být	k5eAaImIp3nS	být
sluchový	sluchový	k2eAgInSc4d1	sluchový
orgán	orgán	k1gInSc4	orgán
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
základními	základní	k2eAgFnPc7d1	základní
částmi	část	k1gFnPc7	část
jsou	být	k5eAaImIp3nP	být
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
ucho	ucho	k1gNnSc1	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Ucho	ucho	k1gNnSc1	ucho
mladého	mladý	k2eAgMnSc2d1	mladý
člověka	člověk	k1gMnSc2	člověk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
zvuk	zvuk	k1gInSc4	zvuk
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
frekvencí	frekvence	k1gFnPc2	frekvence
20	[number]	k4	20
–	–	k?	–
20	[number]	k4	20
000	[number]	k4	000
hertzů	hertz	k1gInPc2	hertz
<g/>
,	,	kIx,	,
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
obvykle	obvykle	k6eAd1	obvykle
slyší	slyšet	k5eAaImIp3nP	slyšet
jen	jen	k9	jen
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
frekvencí	frekvence	k1gFnSc7	frekvence
50	[number]	k4	50
–	–	k?	–
8	[number]	k4	8
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
lidského	lidský	k2eAgInSc2d1	lidský
hlasu	hlas	k1gInSc2	hlas
běžného	běžný	k2eAgInSc2d1	běžný
hovoru	hovor	k1gInSc2	hovor
se	se	k3xPyFc4	se
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
80	[number]	k4	80
do	do	k7c2	do
120	[number]	k4	120
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
pak	pak	k6eAd1	pak
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
170	[number]	k4	170
až	až	k9	až
260	[number]	k4	260
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Delfíni	Delfín	k1gMnPc1	Delfín
<g/>
,	,	kIx,	,
netopýři	netopýr	k1gMnPc1	netopýr
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
zvuku	zvuk	k1gInSc2	zvuk
orientuje	orientovat	k5eAaBmIp3nS	orientovat
–	–	k?	–
tzv.	tzv.	kA	tzv.
echolokace	echolokace	k1gFnSc2	echolokace
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgNnSc1d1	vnější
ucho	ucho	k1gNnSc1	ucho
(	(	kIx(	(
<g/>
auris	auris	k1gFnSc1	auris
externa	externa	k1gFnSc1	externa
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
boltce	boltec	k1gInSc2	boltec
<g/>
,	,	kIx,	,
zvukovodu	zvukovod	k1gInSc2	zvukovod
a	a	k8xC	a
bubínku	bubínek	k1gInSc2	bubínek
<g/>
.	.	kIx.	.
</s>
<s>
Boltec	boltec	k1gInSc1	boltec
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
chrupavkou	chrupavka	k1gFnSc7	chrupavka
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
lalůček	lalůček	k1gInSc1	lalůček
chrupavčitou	chrupavčitý	k2eAgFnSc4d1	chrupavčitá
kostru	kostra	k1gFnSc4	kostra
nemá	mít	k5eNaImIp3nS	mít
<g/>
)	)	kIx)	)
a	a	k8xC	a
směřuje	směřovat	k5eAaImIp3nS	směřovat
akustické	akustický	k2eAgFnPc4d1	akustická
vlny	vlna	k1gFnPc4	vlna
do	do	k7c2	do
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
boltce	boltec	k1gInSc2	boltec
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
zvukovod	zvukovod	k1gInSc1	zvukovod
(	(	kIx(	(
<g/>
také	také	k9	také
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
sluchový	sluchový	k2eAgInSc1d1	sluchový
kanálek	kanálek	k1gInSc1	kanálek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
trubice	trubice	k1gFnSc1	trubice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
část	část	k1gFnSc4	část
chrupavčitou	chrupavčitý	k2eAgFnSc4d1	chrupavčitá
a	a	k8xC	a
kostěnou	kostěný	k2eAgFnSc4d1	kostěná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
zvukovodu	zvukovod	k1gInSc2	zvukovod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bubínek	bubínek	k1gInSc1	bubínek
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
zevním	zevní	k2eAgNnSc7d1	zevní
a	a	k8xC	a
středním	střední	k2eAgNnSc7d1	střední
uchem	ucho	k1gNnSc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
projde	projít	k5eAaPmIp3nS	projít
zvukovodem	zvukovod	k1gInSc7	zvukovod
<g/>
,	,	kIx,	,
naráží	narážet	k5eAaImIp3nS	narážet
do	do	k7c2	do
bubínku	bubínek	k1gInSc2	bubínek
a	a	k8xC	a
putuje	putovat	k5eAaImIp3nS	putovat
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
zvukovodu	zvukovod	k1gInSc2	zvukovod
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bubínek	bubínek	k1gInSc1	bubínek
je	být	k5eAaImIp3nS	být
vazivová	vazivový	k2eAgFnSc1d1	vazivová
blanka	blanka	k1gFnSc1	blanka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
,	,	kIx,	,
cca	cca	kA	cca
0,1	[number]	k4	0,1
mm	mm	kA	mm
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
vlna	vlna	k1gFnSc1	vlna
jej	on	k3xPp3gMnSc4	on
rozechvěje	rozechvět	k5eAaPmIp3nS	rozechvět
<g/>
,	,	kIx,	,
bubínek	bubínek	k1gInSc1	bubínek
ji	on	k3xPp3gFnSc4	on
zesílí	zesílit	k5eAaPmIp3nS	zesílit
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
do	do	k7c2	do
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Zdravý	zdravý	k2eAgInSc1d1	zdravý
bubínek	bubínek	k1gInSc1	bubínek
je	být	k5eAaImIp3nS	být
lesklý	lesklý	k2eAgInSc1d1	lesklý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
šedavou	šedavý	k2eAgFnSc4d1	šedavá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Výstelka	výstelka	k1gFnSc1	výstelka
zvukovodu	zvukovod	k1gInSc2	zvukovod
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mazové	mazový	k2eAgFnPc4d1	mazová
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
ušní	ušní	k2eAgInSc4d1	ušní
maz	maz	k1gInSc4	maz
<g/>
.	.	kIx.	.
</s>
<s>
Zvukovod	zvukovod	k1gInSc1	zvukovod
má	mít	k5eAaImIp3nS	mít
samočisticí	samočisticí	k2eAgFnSc4d1	samočisticí
schopnost	schopnost	k1gFnSc4	schopnost
–	–	k?	–
nečistoty	nečistota	k1gFnPc1	nečistota
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vypuzovány	vypuzován	k2eAgFnPc1d1	vypuzována
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Střední	střední	k2eAgNnSc1d1	střední
ucho	ucho	k1gNnSc1	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
ucho	ucho	k1gNnSc1	ucho
(	(	kIx(	(
<g/>
auris	auris	k1gInSc1	auris
media	medium	k1gNnSc2	medium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc4	systém
vzduchem	vzduch	k1gInSc7	vzduch
vyplněných	vyplněný	k2eAgFnPc2d1	vyplněná
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
vystlaných	vystlaný	k2eAgFnPc2d1	vystlaná
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
bubínkem	bubínek	k1gInSc7	bubínek
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
jsou	být	k5eAaImIp3nP	být
napojeny	napojit	k5eAaPmNgFnP	napojit
tři	tři	k4xCgFnPc1	tři
sluchové	sluchový	k2eAgFnPc1d1	sluchová
kůstky	kůstka	k1gFnPc1	kůstka
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
kladívko	kladívko	k1gNnSc4	kladívko
(	(	kIx(	(
<g/>
malleus	malleus	k1gInSc1	malleus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kovadlinka	kovadlinka	k1gFnSc1	kovadlinka
(	(	kIx(	(
<g/>
incus	incus	k1gInSc1	incus
<g/>
)	)	kIx)	)
a	a	k8xC	a
třmínek	třmínek	k1gInSc1	třmínek
(	(	kIx(	(
<g/>
stapes	stapes	k1gInSc1	stapes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řetěz	řetěz	k1gInSc1	řetěz
kůstek	kůstka	k1gFnPc2	kůstka
přenáší	přenášet	k5eAaImIp3nS	přenášet
zvuk	zvuk	k1gInSc1	zvuk
od	od	k7c2	od
bubínku	bubínek	k1gInSc2	bubínek
do	do	k7c2	do
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
ucha	ucho	k1gNnSc2	ucho
-	-	kIx~	-
ploténka	ploténka	k1gFnSc1	ploténka
třmínku	třmínek	k1gInSc2	třmínek
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
oválného	oválný	k2eAgNnSc2d1	oválné
okénka	okénko	k1gNnSc2	okénko
v	v	k7c6	v
labyrintu	labyrint	k1gInSc6	labyrint
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
ucho	ucho	k1gNnSc1	ucho
je	být	k5eAaImIp3nS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
ucha	ucho	k1gNnSc2	ucho
membránami	membrána	k1gFnPc7	membrána
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
oválné	oválný	k2eAgNnSc4d1	oválné
předsíňové	předsíňový	k2eAgNnSc4d1	předsíňový
okénko	okénko	k1gNnSc4	okénko
(	(	kIx(	(
<g/>
vestibulární	vestibulární	k2eAgMnPc4d1	vestibulární
<g/>
)	)	kIx)	)
a	a	k8xC	a
kruhového	kruhový	k2eAgInSc2d1	kruhový
hlemýžďové	hlemýžďový	k2eAgFnSc3d1	hlemýžďová
(	(	kIx(	(
<g/>
kochleární	kochleární	k2eAgFnSc3d1	kochleární
<g/>
)	)	kIx)	)
okénko	okénko	k1gNnSc1	okénko
<g/>
.	.	kIx.	.
</s>
<s>
Zesílení	zesílení	k1gNnSc1	zesílení
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
pákovou	pákový	k2eAgFnSc7d1	páková
funkcí	funkce	k1gFnSc7	funkce
sluchových	sluchový	k2eAgFnPc2d1	sluchová
kůstek	kůstka	k1gFnPc2	kůstka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přenášejí	přenášet	k5eAaImIp3nP	přenášet
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlny	vlna	k1gFnPc4	vlna
z	z	k7c2	z
většího	veliký	k2eAgInSc2d2	veliký
povrchu	povrch	k1gInSc2	povrch
bubínku	bubínek	k1gInSc2	bubínek
na	na	k7c4	na
menší	malý	k2eAgFnSc4d2	menší
plochu	plocha	k1gFnSc4	plocha
povrchu	povrch	k1gInSc2	povrch
membrány	membrána	k1gFnSc2	membrána
předsíňového	předsíňový	k2eAgNnSc2d1	předsíňový
okénka	okénko	k1gNnSc2	okénko
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrné	nadměrný	k2eAgInPc1d1	nadměrný
silné	silný	k2eAgInPc1d1	silný
zvuky	zvuk	k1gInPc1	zvuk
se	se	k3xPyFc4	se
tlumí	tlumit	k5eAaImIp3nP	tlumit
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
malých	malý	k2eAgInPc2d1	malý
kosterních	kosterní	k2eAgInPc2d1	kosterní
svalů	sval	k1gInPc2	sval
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
uchu	ucho	k1gNnSc6	ucho
(	(	kIx(	(
<g/>
napínač	napínač	k1gInSc1	napínač
bubínku	bubínek	k1gInSc2	bubínek
a	a	k8xC	a
třmínkový	třmínkový	k2eAgInSc4d1	třmínkový
sval	sval	k1gInSc4	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svalová	svalový	k2eAgNnPc4d1	svalové
vřeténka	vřeténko	k1gNnPc4	vřeténko
uvnitř	uvnitř	k7c2	uvnitř
těchto	tento	k3xDgInPc2	tento
svalů	sval	k1gInPc2	sval
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c6	na
protažení	protažení	k1gNnSc6	protažení
svalu	sval	k1gInSc2	sval
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spouští	spouštět	k5eAaImIp3nP	spouštět
tzv.	tzv.	kA	tzv.
akustický	akustický	k2eAgInSc4d1	akustický
reflex	reflex	k1gInSc4	reflex
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
smrštění	smrštění	k1gNnSc4	smrštění
těchto	tento	k3xDgInPc2	tento
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
protažení	protažení	k1gNnSc2	protažení
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
intenzitou	intenzita	k1gFnSc7	intenzita
zvuku	zvuk	k1gInSc2	zvuk
(	(	kIx(	(
<g/>
hlasitostí	hlasitost	k1gFnPc2	hlasitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlasité	hlasitý	k2eAgInPc1d1	hlasitý
zvuky	zvuk	k1gInPc1	zvuk
se	se	k3xPyFc4	se
tlumí	tlumit	k5eAaImIp3nP	tlumit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
natažení	natažení	k1gNnSc1	natažení
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
následná	následný	k2eAgFnSc1d1	následná
reflexní	reflexní	k2eAgFnSc1d1	reflexní
kontrakce	kontrakce	k1gFnSc1	kontrakce
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
nadměrnému	nadměrný	k2eAgInSc3d1	nadměrný
pohybu	pohyb	k1gInSc3	pohyb
sluchových	sluchový	k2eAgFnPc2d1	sluchová
kůstek	kůstka	k1gFnPc2	kůstka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
do	do	k7c2	do
nosohltanu	nosohltan	k1gInSc2	nosohltan
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
Eustachova	Eustachův	k2eAgFnSc1d1	Eustachova
trubice	trubice	k1gFnSc1	trubice
(	(	kIx(	(
<g/>
tuba	tuba	k1gFnSc1	tuba
Eustachi	Eustachi	k1gMnSc1	Eustachi
<g/>
,	,	kIx,	,
tuba	tuba	k1gFnSc1	tuba
auditiva	auditiva	k1gFnSc1	auditiva
<g/>
,	,	kIx,	,
sluchová	sluchový	k2eAgFnSc1d1	sluchová
středoušní	středoušní	k2eAgFnSc1d1	středoušní
trubice	trubice	k1gFnSc1	trubice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
tlak	tlak	k1gInSc4	tlak
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
uchu	ucho	k1gNnSc6	ucho
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
také	také	k9	také
čistit	čistit	k5eAaImF	čistit
středoušní	středoušní	k2eAgFnSc4d1	středoušní
dutinu	dutina	k1gFnSc4	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
ucho	ucho	k1gNnSc1	ucho
(	(	kIx(	(
<g/>
auris	auris	k1gFnSc1	auris
interna	interna	k1gFnSc1	interna
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kostěném	kostěný	k2eAgInSc6d1	kostěný
labyrintu	labyrint	k1gInSc6	labyrint
spánkové	spánkový	k2eAgFnSc2d1	spánková
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
temporalia	temporalium	k1gNnSc2	temporalium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kostěný	kostěný	k2eAgInSc1d1	kostěný
labyrint	labyrint	k1gInSc1	labyrint
částečně	částečně	k6eAd1	částečně
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
blanitý	blanitý	k2eAgInSc4d1	blanitý
labyrint	labyrint	k1gInSc4	labyrint
vyplněný	vyplněný	k2eAgInSc4d1	vyplněný
endolymfou	endolymfa	k1gFnSc7	endolymfa
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
kostěného	kostěný	k2eAgInSc2d1	kostěný
labyrintu	labyrint	k1gInSc2	labyrint
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
blanitý	blanitý	k2eAgInSc4d1	blanitý
labyrint	labyrint	k1gInSc4	labyrint
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
3	[number]	k4	3
polokruhovité	polokruhovitý	k2eAgFnSc2d1	polokruhovitá
kanálky	kanálka	k1gFnSc2	kanálka
<g/>
,	,	kIx,	,
vejčitý	vejčitý	k2eAgInSc1d1	vejčitý
váček	váček	k1gInSc1	váček
<g/>
,	,	kIx,	,
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
váček	váček	k1gInSc1	váček
a	a	k8xC	a
hlemýžď	hlemýžď	k1gMnSc1	hlemýžď
<g/>
.	.	kIx.	.
</s>
<s>
Hlemýžď	hlemýžď	k1gMnSc1	hlemýžď
je	být	k5eAaImIp3nS	být
stočená	stočený	k2eAgFnSc1d1	stočená
trubička	trubička	k1gFnSc1	trubička
naplněná	naplněný	k2eAgFnSc1d1	naplněná
tekutinou	tekutina	k1gFnSc7	tekutina
(	(	kIx(	(
<g/>
endolymfou	endolymfou	k6eAd1	endolymfou
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vibrace	vibrace	k1gFnSc1	vibrace
oválného	oválný	k2eAgNnSc2d1	oválné
okénka	okénko	k1gNnSc2	okénko
rozvlní	rozvlnit	k5eAaPmIp3nS	rozvlnit
endolymfu	endolymf	k1gMnSc3	endolymf
<g/>
.	.	kIx.	.
</s>
<s>
Vlnění	vlnění	k1gNnSc1	vlnění
endolymfy	endolymf	k1gInPc4	endolymf
rozechvěje	rozechvět	k5eAaPmIp3nS	rozechvět
krycí	krycí	k2eAgFnSc4d1	krycí
membránu	membrána	k1gFnSc4	membrána
Cortiho	Corti	k1gMnSc2	Corti
orgánu	orgán	k1gInSc2	orgán
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
vláskové	vláskový	k2eAgFnPc1d1	Vlásková
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
receptory	receptor	k1gInPc1	receptor
sluchu	sluch	k1gInSc2	sluch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
buňka	buňka	k1gFnSc1	buňka
má	mít	k5eAaImIp3nS	mít
vlásky	vlásek	k1gInPc4	vlásek
zapuštěné	zapuštěný	k2eAgInPc4d1	zapuštěný
do	do	k7c2	do
krycí	krycí	k2eAgFnSc2d1	krycí
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
její	její	k3xOp3gNnSc4	její
chvění	chvění	k1gNnSc4	chvění
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
signály	signál	k1gInPc4	signál
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
po	po	k7c6	po
sluchovém	sluchový	k2eAgInSc6d1	sluchový
nervu	nerv	k1gInSc6	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
jsou	být	k5eAaImIp3nP	být
vnímány	vnímán	k2eAgInPc1d1	vnímán
jako	jako	k8xS	jako
zvuk	zvuk	k1gInSc1	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Rovnovážný	rovnovážný	k2eAgInSc1d1	rovnovážný
(	(	kIx(	(
<g/>
vestibulární	vestibulární	k2eAgInSc1d1	vestibulární
<g/>
,	,	kIx,	,
statokinetický	statokinetický	k2eAgInSc1d1	statokinetický
<g/>
)	)	kIx)	)
orgán	orgán	k1gInSc1	orgán
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
vejčitého	vejčitý	k2eAgInSc2d1	vejčitý
a	a	k8xC	a
kulovitého	kulovitý	k2eAgInSc2d1	kulovitý
váčku	váček	k1gInSc2	váček
(	(	kIx(	(
<g/>
utriculus	utriculus	k1gMnSc1	utriculus
a	a	k8xC	a
sacculus	sacculus	k1gMnSc1	sacculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
detekují	detekovat	k5eAaImIp3nP	detekovat
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
a	a	k8xC	a
tří	tři	k4xCgInPc2	tři
polokruhovitých	polokruhovitý	k2eAgInPc2d1	polokruhovitý
kanálků	kanálek	k1gInPc2	kanálek
detekujících	detekující	k2eAgNnPc2d1	detekující
zrychlení	zrychlení	k1gNnPc2	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
váčcích	váček	k1gInPc6	váček
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmé	kolmý	k2eAgFnPc1d1	kolmá
vrstvy	vrstva	k1gFnPc1	vrstva
vláskových	vláskový	k2eAgFnPc2d1	Vlásková
buněk	buňka	k1gFnPc2	buňka
s	s	k7c7	s
vlásky	vlásek	k1gInPc7	vlásek
zapuštěnými	zapuštěný	k2eAgInPc7d1	zapuštěný
do	do	k7c2	do
rosolu	rosol	k1gInSc6	rosol
obsahujícímu	obsahující	k2eAgInSc3d1	obsahující
krystalky	krystalka	k1gFnPc1	krystalka
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgMnSc2d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vnímání	vnímání	k1gNnSc3	vnímání
zrychlení	zrychlení	k1gNnSc2	zrychlení
slouží	sloužit	k5eAaImIp3nP	sloužit
vláskové	vláskový	k2eAgFnPc1d1	Vlásková
buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
polokruhovitých	polokruhovitý	k2eAgInPc2d1	polokruhovitý
kanálků	kanálek	k1gInPc2	kanálek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
proudění	proudění	k1gNnSc6	proudění
endolymfy	endolymf	k1gInPc4	endolymf
v	v	k7c6	v
kanálcích	kanálek	k1gInPc6	kanálek
<g/>
.	.	kIx.	.
</s>
<s>
Předrážděním	předráždění	k1gNnSc7	předráždění
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
vzniká	vznikat	k5eAaImIp3nS	vznikat
mořská	mořský	k2eAgFnSc1d1	mořská
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
častým	častý	k2eAgFnPc3d1	častá
chorobám	choroba	k1gFnPc3	choroba
uší	ucho	k1gNnPc2	ucho
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Otitis	otitis	k1gFnSc1	otitis
media	medium	k1gNnSc2	medium
–	–	k?	–
zánět	zánět	k1gInSc4	zánět
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
<g/>
,	,	kIx,	,
častý	častý	k2eAgInSc1d1	častý
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
bolestivý	bolestivý	k2eAgInSc1d1	bolestivý
<g/>
.	.	kIx.	.
</s>
<s>
Tinnitus	Tinnitus	k1gMnSc1	Tinnitus
–	–	k?	–
neustálé	neustálý	k2eAgNnSc1d1	neustálé
zvonění	zvonění	k1gNnSc1	zvonění
nebo	nebo	k8xC	nebo
pískání	pískání	k1gNnSc1	pískání
v	v	k7c6	v
uchu	ucho	k1gNnSc6	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Vertigo	Vertigo	k6eAd1	Vertigo
–	–	k?	–
porucha	porucha	k1gFnSc1	porucha
polokruhovitých	polokruhovitý	k2eAgInPc2d1	polokruhovitý
kanálků	kanálek	k1gInPc2	kanálek
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
ucha	ucho	k1gNnSc2	ucho
způsobující	způsobující	k2eAgInSc4d1	způsobující
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
okolí	okolí	k1gNnSc1	okolí
točí	točit	k5eAaImIp3nS	točit
(	(	kIx(	(
<g/>
závrať	závrať	k1gFnSc1	závrať
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zalepené	zalepený	k2eAgNnSc1d1	zalepené
ucho	ucho	k1gNnSc1	ucho
–	–	k?	–
tvorba	tvorba	k1gFnSc1	tvorba
tekutiny	tekutina	k1gFnSc2	tekutina
ve	v	k7c6	v
středoušní	středoušní	k2eAgFnSc6d1	středoušní
dutině	dutina	k1gFnSc6	dutina
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
hluchotu	hluchota	k1gFnSc4	hluchota
<g/>
,	,	kIx,	,
časté	častý	k2eAgFnPc4d1	častá
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
