<s>
Otakar	Otakar	k1gMnSc1
Nekvasil	Nekvasil	k1gMnSc1
</s>
<s>
Otakar	Otakar	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Revolučního	revoluční	k2eAgNnSc2d1
nár	nár	k?
<g/>
.	.	kIx.
shromáždění	shromáždění	k1gNnSc1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1918	#num#	k4
–	–	k?
1920	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
staročeská	staročeský	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
<g/>
Česká	český	k2eAgFnSc1d1
státopráv	státopráva	k1gFnPc2
<g/>
.	.	kIx.
dem	dem	k?
<g/>
.	.	kIx.
<g/>
Čs	čs	kA
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1869	#num#	k4
Praha-KarlínRakousko-Uhersko	Praha-KarlínRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1933	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Choť	choť	k1gMnSc1
</s>
<s>
Libuše	Libuše	k1gFnSc1
Šámalová	Šámalová	k1gFnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
Alma	alma	k1gFnSc1
mater	mater	k1gFnPc2
</s>
<s>
česká	český	k2eAgFnSc1d1
technika	technika	k1gFnSc1
Praha	Praha	k1gFnSc1
Profese	profese	k1gFnSc1
</s>
<s>
stavební	stavební	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Otakar	Otakar	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Otakar	Otakar	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1869	#num#	k4
Praha-Karlín	Praha-Karlína	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1933	#num#	k4
Praha-Podolí	Praha-Podole	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
a	a	k8xC
československý	československý	k2eAgMnSc1d1
stavební	stavební	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
poslanec	poslanec	k1gMnSc1
Revolučního	revoluční	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
Republiky	republika	k1gFnSc2
československé	československý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
pražskou	pražský	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
synem	syn	k1gMnSc7
pražského	pražský	k2eAgInSc2d1
stavebního	stavební	k2eAgInSc2d1
podnikatele	podnikatel	k1gMnSc2
Václava	Václav	k1gMnSc2
Nekvasila	kvasit	k5eNaImAgFnS
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
-	-	kIx~
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
převzal	převzít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1906	#num#	k4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
podnik	podnik	k1gInSc1
a	a	k8xC
dále	daleko	k6eAd2
ho	on	k3xPp3gMnSc4
rozšířil	rozšířit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c6
jednu	jeden	k4xCgFnSc4
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
českých	český	k2eAgFnPc2d1
stavebních	stavební	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prováděla	provádět	k5eAaImAgFnS
velké	velký	k2eAgFnPc4d1
zakázky	zakázka	k1gFnPc4
jako	jako	k8xS,k8xC
Akciové	akciový	k2eAgFnPc4d1
ledárny	ledárna	k1gFnPc4
v	v	k7c6
Braníku	Braník	k1gInSc6
nebo	nebo	k8xC
palác	palác	k1gInSc1
Koruna	koruna	k1gFnSc1
<g/>
,	,	kIx,
palác	palác	k1gInSc1
Adria	Adria	k1gFnSc1
či	či	k8xC
budova	budova	k1gFnSc1
Městské	městský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
správní	správní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
výkonného	výkonný	k2eAgInSc2d1
výboru	výbor	k1gInSc2
České	český	k2eAgFnSc2d1
průmyslové	průmyslový	k2eAgFnSc2d1
banky	banka	k1gFnSc2
a	a	k8xC
členem	člen	k1gMnSc7
správní	správní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Českomoravské	českomoravský	k2eAgFnSc2d1
Kolben-Daněk	Kolben-Daněk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
domácího	domácí	k2eAgInSc2d1
protirakouského	protirakouský	k2eAgInSc2d1
odboje	odboj	k1gInSc2
(	(	kIx(
<g/>
Maffie	Maffie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
vzniku	vznik	k1gInSc6
republiky	republika	k1gFnSc2
zasedal	zasedat	k5eAaImAgInS
v	v	k7c6
Revolučním	revoluční	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
za	za	k7c4
Českou	český	k2eAgFnSc4d1
státoprávní	státoprávní	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
,	,	kIx,
respektive	respektive	k9
za	za	k7c2
z	z	k7c2
ní	on	k3xPp3gFnSc2
vzniklou	vzniklý	k2eAgFnSc4d1
Československou	československý	k2eAgFnSc4d1
národní	národní	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
strany	strana	k1gFnSc2
patřil	patřit	k5eAaImAgInS
původně	původně	k6eAd1
ke	k	k7c3
straně	strana	k1gFnSc3
staročeské	staročeský	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentu	parlament	k1gInSc6
byl	být	k5eAaImAgInS
referentem	referent	k1gMnSc7
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
prvního	první	k4xOgInSc2
československého	československý	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
podpoře	podpora	k1gFnSc6
stavebního	stavební	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Profesně	profesně	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
stavitel	stavitel	k1gMnSc1
a	a	k8xC
architekt	architekt	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Působil	působit	k5eAaImAgMnS
jako	jako	k9
místopředseda	místopředseda	k1gMnSc1
Spolku	spolek	k1gInSc2
československých	československý	k2eAgMnPc2d1
inženýrů	inženýr	k1gMnPc2
a	a	k8xC
architektů	architekt	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provedl	provést	k5eAaPmAgMnS
stavbu	stavba	k1gFnSc4
české	český	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
<g/>
,	,	kIx,
zemědělské	zemědělský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
a	a	k8xC
objekt	objekt	k1gInSc4
krajského	krajský	k2eAgInSc2d1
soudu	soud	k1gInSc2
tamtéž	tamtéž	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
firma	firma	k1gFnSc1
zbudovala	zbudovat	k5eAaPmAgFnS
rovněž	rovněž	k9
krajský	krajský	k2eAgInSc4d1
soud	soud	k1gInSc4
a	a	k8xC
elektrárnu	elektrárna	k1gFnSc4
v	v	k7c6
Užhorodě	Užhorod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
automobilismu	automobilismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
místopředsedou	místopředseda	k1gMnSc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
předsedou	předseda	k1gMnSc7
Autoklubu	autoklub	k1gInSc2
Republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
navazoval	navazovat	k5eAaImAgInS
kontakty	kontakt	k1gInPc4
s	s	k7c7
podobnými	podobný	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
na	na	k7c6
sklonku	sklonek	k1gInSc6
života	život	k1gInSc2
docílil	docílit	k5eAaPmAgInS
sjednocení	sjednocení	k1gNnSc4
všech	všecek	k3xTgInPc2
autoklubů	autoklub	k1gInPc2
v	v	k7c6
Československu	Československo	k1gNnSc6
do	do	k7c2
jednoho	jeden	k4xCgNnSc2
sdružení	sdružení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
onemocněl	onemocnět	k5eAaPmAgInS
ledvinovou	ledvinový	k2eAgFnSc7d1
chorobou	choroba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osmi	osm	k4xCc6
měsících	měsíc	k1gInPc6
ale	ale	k8xC
dočasně	dočasně	k6eAd1
překonal	překonat	k5eAaPmAgMnS
zdravotní	zdravotní	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
a	a	k8xC
vrátil	vrátit	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
aktivního	aktivní	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
své	svůj	k3xOyFgFnSc2
firmy	firma	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
tehdy	tehdy	k6eAd1
převedl	převést	k5eAaPmAgInS
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1933	#num#	k4
v	v	k7c6
podolském	podolský	k2eAgNnSc6d1
sanatoriu	sanatorium	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jen	jen	k6eAd1
necelé	celý	k2eNgInPc4d1
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
předtím	předtím	k6eAd1
zemřela	zemřít	k5eAaPmAgFnS
i	i	k9
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Libuše	Libuše	k1gFnSc2
rozená	rozený	k2eAgFnSc1d1
Šámalová	Šámalová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
<g/>
1	#num#	k4
2	#num#	k4
Archiv	archiv	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Matrika	matrika	k1gFnSc1
zemřelých	zemřelý	k1gMnPc2
v	v	k7c6
Podolí	Podolí	k1gNnSc6
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
POD	pod	k7c4
Z	Z	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
193	#num#	k4
<g/>
↑	↑	k?
Stavitel	stavitel	k1gMnSc1
bez	bez	k7c2
bázně	bázeň	k1gFnSc2
a	a	k8xC
hany	hana	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
profit	profit	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Ing.	ing.	kA
arch	archa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otakar	Otakar	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosinec	prosinec	k1gInSc1
1933	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
356	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Otakar	Otakar	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Otakar	Otakar	k1gMnSc1
Nekvasil	kvasit	k5eNaImAgMnS
v	v	k7c6
Revolučním	revoluční	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ntk	ntk	k?
<g/>
2017951838	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
9898149662201507020007	#num#	k4
</s>
