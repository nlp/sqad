<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
šachová	šachový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
šachová	šachový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
FIDE	FIDE	kA
Motto	motto	k1gNnSc1
</s>
<s>
Gens	Gens	k1gInSc1
una	una	k?
sumus	sumus	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1924	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
Typ	typ	k1gInSc1
</s>
<s>
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgInSc1d1
Účel	účel	k1gInSc1
</s>
<s>
Šachy	šach	k1gInPc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
AtényŘecko	AtényŘecko	k6eAd1
Řecko	Řecko	k1gNnSc4
Působnost	působnost	k1gFnSc1
</s>
<s>
celosvětová	celosvětový	k2eAgFnSc1d1
Prezident	prezident	k1gMnSc1
</s>
<s>
Arkadij	Arkadít	k5eAaPmRp2nS
DvorkovičRusko	DvorkovičRuska	k1gMnSc5
Rusko	Rusko	k1gNnSc1
Přidružení	přidružení	k1gNnSc3
</s>
<s>
SportAccord	SportAccord	k1gMnSc1
<g/>
,	,	kIx,
MOV	MOV	kA
<g/>
,	,	kIx,
ARISF	ARISF	kA
<g/>
,	,	kIx,
Šachový	šachový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.fide.com	www.fide.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
šachová	šachový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
příp	příp	kA
<g/>
.	.	kIx.
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
šachu	šach	k1gInSc2
(	(	kIx(
<g/>
FIDE	FIDE	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc2
des	des	k1gNnSc2
Échecs	Échecs	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
sdružující	sdružující	k2eAgFnSc2d1
jednotlivé	jednotlivý	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
šachové	šachový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1924	#num#	k4
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgNnSc1d1
Československo	Československo	k1gNnSc1
bylo	být	k5eAaImAgNnS
mezi	mezi	k7c7
15	#num#	k4
zakládajícími	zakládající	k2eAgInPc7d1
členy	člen	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jejím	její	k3xOp3gNnSc7
heslem	heslo	k1gNnSc7
je	být	k5eAaImIp3nS
Gens	Gens	k1gInSc1
una	una	k?
sumus	sumus	k1gInSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Jsme	být	k5eAaImIp1nP
jeden	jeden	k4xCgInSc4
lid	lid	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
října	říjen	k1gInSc2
2018	#num#	k4
je	být	k5eAaImIp3nS
předsedou	předseda	k1gMnSc7
FIDE	FIDE	kA
Arkadij	Arkadij	k1gMnSc7
Dvorkovič	Dvorkovič	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
2019	#num#	k4
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
195	#num#	k4
šachových	šachový	k2eAgFnPc2d1
federací	federace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předsedové	předseda	k1gMnPc1
FIDE	FIDE	kA
</s>
<s>
1924	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
Alexander	Alexandra	k1gFnPc2
Rueb	Rueba	k1gFnPc2
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
Folke	Folk	k1gMnSc2
Rogard	Rogard	k1gInSc4
</s>
<s>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
Max	max	kA
Euwe	Euwe	k1gFnPc2
</s>
<s>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
Frið	Frið	k1gFnPc2
Ólafsson	Ólafssona	k1gFnPc2
</s>
<s>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
Florencio	Florencio	k1gMnSc1
Campomanes	Campomanes	k1gMnSc1
</s>
<s>
1995	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
Kirsan	Kirsan	k1gInSc1
Iljumžinov	Iljumžinov	k1gInSc4
</s>
<s>
2018	#num#	k4
až	až	k6eAd1
dosud	dosud	k6eAd1
Arkadij	Arkadij	k1gMnSc1
Dvorkovič	Dvorkovič	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VESELÝ	Veselý	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
KALENDOVSKÝ	KALENDOVSKÝ	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
FORMÁNEK	Formánek	k1gMnSc1
<g/>
,	,	kIx,
Bedrich	Bedrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
šachu	šach	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
430	#num#	k4
s.	s.	k?
Heslo	heslo	k1gNnSc4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
federace	federace	k1gFnSc2
šachu	šach	k1gInSc2
<g/>
.	.	kIx.
↑	↑	k?
BBC	BBC	kA
News	News	k1gInSc1
<g/>
:	:	kIx,
Arkady	Arkada	k1gFnSc2
Dvorkovich	Dvorkovich	k1gMnSc1
<g/>
:	:	kIx,
Russian	Russian	k1gMnSc1
politician	politician	k1gMnSc1
crowned	crowned	k1gMnSc1
world	world	k1gMnSc1
chess	chess	k6eAd1
head	head	k6eAd1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
↑	↑	k?
Member	Member	k1gInSc1
Federations	Federations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIDE	FIDE	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
SportAccord	SportAccord	k6eAd1
</s>
<s>
Šachový	šachový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
ŠSČR	ŠSČR	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Šachového	šachový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ASOIF	ASOIF	kA
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
letních	letní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IAAF	IAAF	kA
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
BWF	BWF	kA
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIBA	FIBA	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
AIBA	AIBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UCI	UCI	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIFA	FIFA	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIG	FIG	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
IHF	IHF	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ISAF	ISAF	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FEI	FEI	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
IJF	IJF	kA
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICF	ICF	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
WA	WA	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UIPM	UIPM	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgInSc4d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIH	FIH	kA
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
WR	WR	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ISSF	ISSF	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ITTF	ITTF	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIE	FIE	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WTF	WTF	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ITF	ITF	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITU	ITU	kA
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FISA	FISA	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
FINA	Fina	k1gFnSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
FIVB	FIVB	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWF	IWF	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
UWW	UWW	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
AIOWF	AIOWF	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
zimních	zimní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IBU	IBU	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBSF	IBSF	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISU	ISU	kA
(	(	kIx(
<g/>
bruslení	bruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IIHF	IIHF	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIS	FIS	kA
(	(	kIx(
<g/>
lyžařské	lyžařský	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
sáňkařský	sáňkařský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
ARISF	ARISF	kA
(	(	kIx(
<g/>
37	#num#	k4
<g/>
)	)	kIx)
<g/>
Další	další	k2eAgFnSc1d1
federace	federace	k1gFnSc1
uznané	uznaný	k2eAgFnSc2d1
MOV	MOV	kA
</s>
<s>
IFAF	IFAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIA	FIA	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIB	FIB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WBSC	WBSC	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc1
a	a	k8xC
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WB	WB	kA
(	(	kIx(
<g/>
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WBF	WBF	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFF	IFF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIAA	UIAA	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICU	ICU	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WKF	WKF	kA
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICC	ICC	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIRS	FIRS	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CMSB	CMSB	kA
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
WCBS	WCBS	kA
(	(	kIx(
<g/>
kulečník	kulečník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAI	FAI	kA
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIM	FIM	kA
(	(	kIx(
<g/>
motocyklový	motocyklový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMA	IFMA	kA
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
IFNA	IFNA	kA
(	(	kIx(
<g/>
netball	netball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IOF	IOF	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIPV	FIPV	kA
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CMAS	CMAS	kA
(	(	kIx(
<g/>
podvodní	podvodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIP	FIP	kA
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
TWIF	TWIF	kA
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISMF	ISMF	kA
(	(	kIx(
<g/>
skialpinismus	skialpinismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFSC	IFSC	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSF	WSF	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFS	IFS	kA
(	(	kIx(
<g/>
sumo	suma	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
ISA	ISA	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIDE	FIDE	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
WDSF	WDSF	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WFDF	WFDF	kA
(	(	kIx(
<g/>
ultimate	ultimat	k1gInSc5
frisbee	frisbee	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
IWWF	IWWF	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
a	a	k8xC
wakeboarding	wakeboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIM	UIM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWUF	IWUF	kA
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
ILSF	ILSF	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ve	v	k7c6
SportAccordu	SportAccordo	k1gNnSc6
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IAF	IAF	kA
(	(	kIx(
<g/>
aikido	aikida	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FMJD	FMJD	kA
(	(	kIx(
<g/>
dáma	dáma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
IDBF	IDBF	kA
(	(	kIx(
<g/>
dračí	dračí	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
JJIF	JJIF	kA
(	(	kIx(
<g/>
džú-džucu	džú-džucu	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
IFA	IFA	kA
(	(	kIx(
<g/>
faustball	faustball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
go	go	k?
<g/>
)	)	kIx)
</s>
<s>
IFI	IFI	kA
(	(	kIx(
<g/>
ice	ice	k?
stock	stock	k1gInSc1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIK	fik	k0
(	(	kIx(
<g/>
kendó	kendó	k?
<g/>
)	)	kIx)
</s>
<s>
WAKO	WAKO	kA
(	(	kIx(
<g/>
kickboxing	kickboxing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBB	IFBB	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
a	a	k8xC
fitness	fitness	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WMF	WMF	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISFF	ISFF	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICSF	ICSF	kA
(	(	kIx(
<g/>
rybolovná	rybolovný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIAS	FIAS	kA
(	(	kIx(
<g/>
sambo	samba	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FISav	FISav	k1gInSc4
(	(	kIx(
<g/>
savate	savat	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
ISTAF	ISTAF	kA
(	(	kIx(
<g/>
sepak	sepak	k1gMnSc1
takraw	takraw	k?
<g/>
)	)	kIx)
</s>
<s>
IPF	IPF	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISTF	ISTF	kA
(	(	kIx(
<g/>
soft	soft	k?
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CIPS	CIPS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WDF	WDF	kA
(	(	kIx(
<g/>
šipky	šipka	k1gFnPc1
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
federace	federace	k1gFnPc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
WAF	WAF	kA
(	(	kIx(
<g/>
armwrestling	armwrestling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ARI	ARI	kA
(	(	kIx(
<g/>
Australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBA	iba	k6eAd1
(	(	kIx(
<g/>
bodyboarding	bodyboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PBA	PBA	kA
(	(	kIx(
<g/>
bowls	bowls	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBA	IFBA	kA
(	(	kIx(
<g/>
broomball	broomball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IKAEF	IKAEF	kA
(	(	kIx(
<g/>
eskrima	eskrim	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
WFA	WFA	kA
(	(	kIx(
<g/>
footbag	footbag	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ISBHF	ISBHF	kA
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
kroket	kroket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
kabaddi	kabaddit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
IMMAF	IMMAF	kA
(	(	kIx(
<g/>
MMA	MMA	kA
<g/>
)	)	kIx)
</s>
<s>
IFP	IFP	kA
(	(	kIx(
<g/>
poker	poker	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IPSC	IPSC	kA
(	(	kIx(
<g/>
practical	practicat	k5eAaPmAgInS
shooting	shooting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IQA	IQA	kA
(	(	kIx(
<g/>
mudlovský	mudlovský	k2eAgInSc1d1
famfrpál	famfrpál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMAR	IFMAR	kA
(	(	kIx(
<g/>
závody	závod	k1gInPc1
automobilových	automobilový	k2eAgInPc2d1
modelů	model	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
FIFTA	FIFTA	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
rogaining	rogaining	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
RLIF	RLIF	kA
(	(	kIx(
<g/>
třináctkové	třináctkový	k2eAgNnSc1d1
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSSA	WSSA	kA
(	(	kIx(
<g/>
sport	sport	k1gInSc1
stacking	stacking	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITPF	ITPF	kA
(	(	kIx(
<g/>
Tent	tent	k1gInSc1
pegging	pegging	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIT	fit	k2eAgInSc1d1
(	(	kIx(
<g/>
touch	touch	k1gInSc1
rugby	rugby	k1gNnSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Světových	světový	k2eAgFnPc2d1
her	hra	k1gFnPc2
•	•	k?
SportAccord	SportAccord	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2002152584	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1044932-2	1044932-2	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2151	#num#	k4
4527	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50067918	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125119978	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50067918	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šachy	šach	k1gInPc1
</s>
