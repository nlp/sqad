<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1867	[number]	k4	1867
Nechanice	Nechanice	k1gFnPc4	Nechanice
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1923	[number]	k4	1923
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
odsouzených	odsouzený	k1gMnPc2	odsouzený
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
s	s	k7c7	s
Omladinou	Omladina	k1gFnSc7	Omladina
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
1	[number]	k4	1
<g/>
.	.	kIx.	.
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
funkcionář	funkcionář	k1gMnSc1	funkcionář
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
atentátu	atentát	k1gInSc6	atentát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgMnS	být
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1867	[number]	k4	1867
v	v	k7c6	v
Nechanicích	Nechanice	k1gFnPc6	Nechanice
<g/>
,	,	kIx,	,
městě	město	k1gNnSc6	město
nedaleko	nedaleko	k7c2	nedaleko
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
deváté	devátý	k4xOgNnSc4	devátý
dítě	dítě	k1gNnSc4	dítě
Františka	František	k1gMnSc2	František
a	a	k8xC	a
Františky	Františka	k1gFnSc2	Františka
Rašínových	Rašínová	k1gFnPc2	Rašínová
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
František	František	k1gMnSc1	František
Rašín	Rašín	k1gMnSc1	Rašín
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
pekař	pekař	k1gMnSc1	pekař
a	a	k8xC	a
rolník	rolník	k1gMnSc1	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nejnadanější	nadaný	k2eAgMnPc1d3	nejnadanější
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
sourozenců	sourozenec	k1gMnPc2	sourozenec
studoval	studovat	k5eAaImAgMnS	studovat
Alois	Alois	k1gMnSc1	Alois
na	na	k7c6	na
gymnáziích	gymnázium	k1gNnPc6	gymnázium
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Bydžově	Bydžov	k1gInSc6	Bydžov
<g/>
,	,	kIx,	,
Broumově	Broumov	k1gInSc6	Broumov
a	a	k8xC	a
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Maturitu	maturita	k1gFnSc4	maturita
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
však	však	k9	však
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
přestoupil	přestoupit	k5eAaPmAgInS	přestoupit
na	na	k7c4	na
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
UK	UK	kA	UK
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
také	také	k9	také
mj.	mj.	kA	mj.
členem	člen	k1gMnSc7	člen
tradičního	tradiční	k2eAgInSc2d1	tradiční
Spolku	spolek	k1gInSc2	spolek
československých	československý	k2eAgMnPc2d1	československý
právníků	právník	k1gMnPc2	právník
Všehrd	Všehrda	k1gFnPc2	Všehrda
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
úspěšně	úspěšně	k6eAd1	úspěšně
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k9	také
napsal	napsat	k5eAaPmAgMnS	napsat
brožurku	brožurka	k1gFnSc4	brožurka
České	český	k2eAgFnSc2d1	Česká
státní	státní	k2eAgMnSc1d1	státní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
rakouské	rakouský	k2eAgInPc1d1	rakouský
úřady	úřad	k1gInPc1	úřad
zabavovaly	zabavovat	k5eAaImAgInP	zabavovat
a	a	k8xC	a
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
byl	být	k5eAaImAgMnS	být
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
vyšetřován	vyšetřovat	k5eAaImNgMnS	vyšetřovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgNnPc2	svůj
vysokoškolských	vysokoškolský	k2eAgNnPc2d1	vysokoškolské
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
politicky	politicky	k6eAd1	politicky
angažovat	angažovat	k5eAaBmF	angažovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
angažovanost	angažovanost	k1gFnSc1	angažovanost
mu	on	k3xPp3gMnSc3	on
posléze	posléze	k6eAd1	posléze
vynesla	vynést	k5eAaPmAgFnS	vynést
ztrátu	ztráta	k1gFnSc4	ztráta
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
žaláře	žalář	k1gInSc2	žalář
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgMnPc3	který
byl	být	k5eAaImAgInS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1894	[number]	k4	1894
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
s	s	k7c7	s
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
Omladinou	Omladina	k1gFnSc7	Omladina
<g/>
.	.	kIx.	.
</s>
<s>
Vězněn	vězněn	k2eAgMnSc1d1	vězněn
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
Bory	bor	k1gInPc1	bor
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
Rašín	Rašín	k1gInSc1	Rašín
amnestován	amnestovat	k5eAaBmNgInS	amnestovat
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
zpět	zpět	k6eAd1	zpět
i	i	k9	i
titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	práv	k2eAgInSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
sympatizoval	sympatizovat	k5eAaImAgMnS	sympatizovat
především	především	k9	především
s	s	k7c7	s
mladočechy	mladočech	k1gMnPc7	mladočech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Karolínu	Karolína	k1gFnSc4	Karolína
Jánskou	Jánská	k1gFnSc4	Jánská
ze	z	k7c2	z
Smíchova	Smíchov	k1gInSc2	Smíchov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
mladočechy	mladočech	k1gMnPc7	mladočech
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
rozešel	rozejít	k5eAaPmAgMnS	rozejít
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
Radikálně	radikálně	k6eAd1	radikálně
státoprávní	státoprávní	k2eAgFnSc2d1	státoprávní
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
státoprávní	státoprávní	k2eAgFnSc1d1	státoprávní
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeměně	přeměna	k1gFnSc6	přeměna
Radikálních	radikální	k2eAgInPc2d1	radikální
listů	list	k1gInPc2	list
v	v	k7c4	v
deník	deník	k1gInSc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
aktivní	aktivní	k2eAgFnSc2d1	aktivní
politiky	politika	k1gFnSc2	politika
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
advokátní	advokátní	k2eAgFnSc4d1	advokátní
kancelář	kancelář	k1gFnSc4	kancelář
(	(	kIx(	(
<g/>
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Živnobanku	Živnobanka	k1gFnSc4	Živnobanka
<g/>
)	)	kIx)	)
a	a	k8xC	a
politické	politický	k2eAgNnSc4d1	politické
dění	dění	k1gNnSc4	dění
komentoval	komentovat	k5eAaBmAgInS	komentovat
příležitostně	příležitostně	k6eAd1	příležitostně
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
deníku	deník	k1gInSc2	deník
Slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
díky	díky	k7c3	díky
změnám	změna	k1gFnPc3	změna
vedení	vedení	k1gNnSc2	vedení
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
do	do	k7c2	do
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgInSc1d1	celostátní
parlament	parlament	k1gInSc1	parlament
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
okrsek	okrsek	k1gInSc4	okrsek
Čechy	Čechy	k1gFnPc4	Čechy
31	[number]	k4	31
(	(	kIx(	(
<g/>
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Usedl	usednout	k5eAaPmAgMnS	usednout
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
frakce	frakce	k1gFnSc2	frakce
Český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
finančním	finanční	k2eAgInSc6d1	finanční
a	a	k8xC	a
rozpočtovém	rozpočtový	k2eAgInSc6d1	rozpočtový
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
ztratil	ztratit	k5eAaPmAgInS	ztratit
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1917	[number]	k4	1917
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pravomocného	pravomocný	k2eAgInSc2d1	pravomocný
rozsudku	rozsudek	k1gInSc2	rozsudek
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
odboje	odboj	k1gInSc2	odboj
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
organizace	organizace	k1gFnSc2	organizace
Maffie	Maffie	k1gFnSc2	Maffie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgInS	zatknout
a	a	k8xC	a
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1915	[number]	k4	1915
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
<g/>
,	,	kIx,	,
Vincencem	Vincenc	k1gMnSc7	Vincenc
Červinkou	Červinka	k1gMnSc7	Červinka
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Zamazalem	Zamazal	k1gMnSc7	Zamazal
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
a	a	k8xC	a
vyzvědačství	vyzvědačství	k1gNnSc4	vyzvědačství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědků	svědek	k1gMnPc2	svědek
vynesení	vynesení	k1gNnSc2	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
trest	trest	k1gInSc1	trest
přijal	přijmout	k5eAaPmAgInS	přijmout
nejstatečněji	statečně	k6eAd3	statečně
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
smál	smát	k5eAaImAgMnS	smát
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
jej	on	k3xPp3gMnSc4	on
zachránil	zachránit	k5eAaPmAgInS	zachránit
nástup	nástup	k1gInSc1	nástup
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
na	na	k7c4	na
rakousko-uherský	rakouskoherský	k2eAgInSc4d1	rakousko-uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
si	se	k3xPyFc3	se
odpykával	odpykávat	k5eAaImAgInS	odpykávat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
tvrdých	tvrdý	k2eAgFnPc6d1	tvrdá
podmínkách	podmínka	k1gFnPc6	podmínka
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Möllersdorfu	Möllersdorf	k1gInSc6	Möllersdorf
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sdílel	sdílet	k5eAaImAgMnS	sdílet
celu	cela	k1gFnSc4	cela
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc4	trest
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
nejprve	nejprve	k6eAd1	nejprve
zmírněn	zmírnit	k5eAaPmNgMnS	zmírnit
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1917	[number]	k4	1917
byl	být	k5eAaImAgMnS	být
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
odsouzenými	odsouzený	k1gMnPc7	odsouzený
amnestován	amnestovat	k5eAaBmNgMnS	amnestovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
opozičními	opoziční	k2eAgFnPc7d1	opoziční
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
císaře	císař	k1gMnSc4	císař
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
nepožádal	požádat	k5eNaPmAgMnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
české	český	k2eAgFnSc2d1	Česká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
národním	národní	k2eAgMnSc7d1	národní
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
se	se	k3xPyFc4	se
Rašín	Rašín	k1gInSc1	Rašín
okamžitě	okamžitě	k6eAd1	okamžitě
aktivně	aktivně	k6eAd1	aktivně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
promován	promovat	k5eAaBmNgMnS	promovat
doktorem	doktor	k1gMnSc7	doktor
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rozsudkem	rozsudek	k1gInSc7	rozsudek
z	z	k7c2	z
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
České	český	k2eAgFnSc2d1	Česká
státoprávní	státoprávní	k2eAgFnSc2d1	státoprávní
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Švehlou	Švehla	k1gMnSc7	Švehla
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Stříbrným	stříbrný	k1gInSc7	stříbrný
<g/>
,	,	kIx,	,
Františkem	František	k1gMnSc7	František
Soukupem	Soukup	k1gMnSc7	Soukup
a	a	k8xC	a
Vavrem	Vavr	k1gMnSc7	Vavr
Šrobárem	Šrobár	k1gMnSc7	Šrobár
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc4	výbor
československý	československý	k2eAgInSc4d1	československý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Rašín	Rašín	k1gMnSc1	Rašín
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
textu	text	k1gInSc2	text
prvního	první	k4xOgInSc2	první
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
s	s	k7c7	s
Rašínem	Rašín	k1gInSc7	Rašín
v	v	k7c6	v
Kramářově	kramářův	k2eAgFnSc6d1	Kramářova
vládě	vláda	k1gFnSc6	vláda
počítalo	počítat	k5eAaImAgNnS	počítat
na	na	k7c4	na
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
raději	rád	k6eAd2	rád
přijal	přijmout	k5eAaPmAgMnS	přijmout
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
úřadem	úřad	k1gInSc7	úřad
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
Rašín	Rašín	k1gMnSc1	Rašín
setrval	setrvat	k5eAaPmAgMnS	setrvat
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
až	až	k9	až
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
působení	působení	k1gNnSc3	působení
získal	získat	k5eAaPmAgInS	získat
Rašín	Rašín	k1gInSc1	Rašín
pověst	pověst	k1gFnSc1	pověst
budovatele	budovatel	k1gMnSc2	budovatel
státu	stát	k1gInSc2	stát
a	a	k8xC	a
stabilizátora	stabilizátor	k1gMnSc2	stabilizátor
rozvrácených	rozvrácený	k2eAgFnPc2d1	rozvrácená
poválečných	poválečný	k2eAgFnPc2d1	poválečná
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
taktéž	taktéž	k?	taktéž
tvůrcem	tvůrce	k1gMnSc7	tvůrce
měnové	měnový	k2eAgFnSc2d1	měnová
odluky	odluka	k1gFnSc2	odluka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
uděleny	udělen	k2eAgFnPc1d1	udělena
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
uzavřít	uzavřít	k5eAaPmF	uzavřít
státní	státní	k2eAgFnPc4d1	státní
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
okolkovat	okolkovat	k5eAaPmF	okolkovat
veškeré	veškerý	k3xTgNnSc4	veškerý
oběživo	oběživo	k1gNnSc4	oběživo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
nacházelo	nacházet	k5eAaImAgNnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
nechal	nechat	k5eAaPmAgMnS	nechat
zadržet	zadržet	k5eAaPmF	zadržet
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nucené	nucený	k2eAgFnSc2d1	nucená
státní	státní	k2eAgFnSc2d1	státní
půjčky	půjčka	k1gFnSc2	půjčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
Revolučním	revoluční	k2eAgNnSc6d1	revoluční
národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
státoprávní	státoprávní	k2eAgFnSc4d1	státoprávní
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
za	za	k7c2	za
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
Československou	československý	k2eAgFnSc4d1	Československá
národní	národní	k2eAgFnSc4d1	národní
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
národní	národní	k2eAgMnPc4d1	národní
demokraty	demokrat	k1gMnPc4	demokrat
poslanecké	poslanecký	k2eAgNnSc1d1	poslanecké
křeslo	křeslo	k1gNnSc1	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
poslaneckém	poslanecký	k2eAgInSc6d1	poslanecký
postu	post	k1gInSc6	post
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Jan	Jan	k1gMnSc1	Jan
Kamelský	Kamelský	k2eAgMnSc1d1	Kamelský
<g/>
.	.	kIx.	.
<g/>
Ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
první	první	k4xOgFnSc6	první
Švehlově	Švehlův	k2eAgFnSc6d1	Švehlova
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
nahradil	nahradit	k5eAaPmAgMnS	nahradit
svého	svůj	k3xOyFgMnSc4	svůj
stranického	stranický	k2eAgMnSc4d1	stranický
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
politického	politický	k2eAgMnSc4d1	politický
soka	sok	k1gMnSc4	sok
a	a	k8xC	a
odborného	odborný	k2eAgMnSc4d1	odborný
oponenta	oponent	k1gMnSc4	oponent
Karla	Karel	k1gMnSc4	Karel
Engliše	Engliš	k1gMnSc4	Engliš
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
podporoval	podporovat	k5eAaImAgInS	podporovat
politiku	politika	k1gFnSc4	politika
rozpočtové	rozpočtový	k2eAgFnSc2d1	rozpočtová
úspory	úspora	k1gFnSc2	úspora
a	a	k8xC	a
deflace	deflace	k1gFnSc2	deflace
<g/>
.	.	kIx.	.
</s>
<s>
Dostával	dostávat	k5eAaImAgInS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
legionáři	legionář	k1gMnPc7	legionář
<g/>
,	,	kIx,	,
když	když	k8xS	když
použil	použít	k5eAaPmAgInS	použít
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
práce	práce	k1gFnSc2	práce
pro	pro	k7c4	pro
národ	národ	k1gInSc4	národ
se	se	k3xPyFc4	se
neplatí	platit	k5eNaImIp3nS	platit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
ráno	ráno	k6eAd1	ráno
byl	být	k5eAaImAgMnS	být
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
u	u	k7c2	u
svého	svůj	k3xOyFgNnSc2	svůj
bydliště	bydliště	k1gNnSc2	bydliště
v	v	k7c6	v
Žitné	žitný	k2eAgFnSc6d1	Žitná
ulici	ulice	k1gFnSc6	ulice
střelen	střelen	k2eAgInSc4d1	střelen
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Pachatelem	pachatel	k1gMnSc7	pachatel
atentátu	atentát	k1gInSc2	atentát
byl	být	k5eAaImAgMnS	být
devatenáctiletý	devatenáctiletý	k2eAgMnSc1d1	devatenáctiletý
anarchokomunista	anarchokomunista	k1gMnSc1	anarchokomunista
Josef	Josef	k1gMnSc1	Josef
Šoupal	šoupat	k5eAaImAgMnS	šoupat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
odpykal	odpykat	k5eAaPmAgMnS	odpykat
18	[number]	k4	18
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
<g/>
Záchranka	záchranka	k1gFnSc1	záchranka
Rašína	Rašína	k1gFnSc1	Rašína
převezla	převézt	k5eAaPmAgFnS	převézt
do	do	k7c2	do
Podolského	podolský	k2eAgNnSc2d1	Podolské
sanatoria	sanatorium	k1gNnSc2	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
Lékařský	lékařský	k2eAgInSc1d1	lékařský
tým	tým	k1gInSc1	tým
vedl	vést	k5eAaImAgMnS	vést
prof.	prof.	kA	prof.
Julius	Julius	k1gMnSc1	Julius
Petřivalský	Petřivalský	k2eAgMnSc1d1	Petřivalský
<g/>
,	,	kIx,	,
asistovali	asistovat	k5eAaImAgMnP	asistovat
mu	on	k3xPp3gNnSc3	on
internista	internista	k1gMnSc1	internista
Ladislav	Ladislav	k1gMnSc1	Ladislav
Syllaba	Syllaba	k1gMnSc1	Syllaba
a	a	k8xC	a
chirurg	chirurg	k1gMnSc1	chirurg
Rudolf	Rudolf	k1gMnSc1	Rudolf
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
.	.	kIx.	.
</s>
<s>
Rašínův	Rašínův	k2eAgInSc1d1	Rašínův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
lékařů	lékař	k1gMnPc2	lékař
beznadějný	beznadějný	k2eAgInSc1d1	beznadějný
–	–	k?	–
kulka	kulka	k1gFnSc1	kulka
ho	on	k3xPp3gMnSc4	on
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
do	do	k7c2	do
jedenáctého	jedenáctý	k4xOgInSc2	jedenáctý
obratle	obratel	k1gInSc2	obratel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
uvízla	uvíznout	k5eAaPmAgFnS	uvíznout
<g/>
.	.	kIx.	.
</s>
<s>
Poranění	poranění	k1gNnSc1	poranění
míchy	mícha	k1gFnSc2	mícha
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
úplné	úplný	k2eAgNnSc1d1	úplné
ochrnutí	ochrnutí	k1gNnSc1	ochrnutí
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
těžké	těžký	k2eAgFnPc1d1	těžká
poruchy	porucha	k1gFnPc1	porucha
střev	střevo	k1gNnPc2	střevo
a	a	k8xC	a
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
;	;	kIx,	;
po	po	k7c6	po
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
utrpení	utrpení	k1gNnSc2	utrpení
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
,	,	kIx,	,
k	k	k7c3	k
bakteriální	bakteriální	k2eAgFnSc3d1	bakteriální
sněti	sněť	k1gFnSc3	sněť
a	a	k8xC	a
sepsi	sepse	k1gFnSc3	sepse
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
ráno	ráno	k6eAd1	ráno
Rašín	Rašín	k1gInSc4	Rašín
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze-Dejvicích	Praze-Dejvice	k1gFnPc6	Praze-Dejvice
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Šárka	Šárka	k1gFnSc1	Šárka
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Matěje	Matěj	k1gMnSc2	Matěj
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
posledních	poslední	k2eAgFnPc2d1	poslední
starostí	starost	k1gFnPc2	starost
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
atentát	atentát	k1gInSc1	atentát
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
sporem	spor	k1gInSc7	spor
s	s	k7c7	s
legionářskými	legionářský	k2eAgMnPc7d1	legionářský
představiteli	představitel	k1gMnPc7	představitel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
bylo	být	k5eAaImAgNnS	být
tragickým	tragický	k2eAgNnPc3d1	tragické
nedorozuměním	nedorozumění	k1gNnPc3	nedorozumění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
již	již	k6eAd1	již
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
kontroverznost	kontroverznost	k1gFnSc4	kontroverznost
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
nepopulární	populární	k2eNgInSc1d1	nepopulární
zákon	zákon	k1gInSc1	zákon
schválen	schválit	k5eAaPmNgInS	schválit
až	až	k6eAd1	až
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
posloužil	posloužit	k5eAaPmAgInS	posloužit
jako	jako	k8xC	jako
vhodná	vhodný	k2eAgFnSc1d1	vhodná
příležitost	příležitost	k1gFnSc1	příležitost
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
schválení	schválení	k1gNnSc3	schválení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
byl	být	k5eAaImAgMnS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
orientován	orientován	k2eAgInSc4d1	orientován
nacionalisticky	nacionalisticky	k6eAd1	nacionalisticky
<g/>
,	,	kIx,	,
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
zastával	zastávat	k5eAaImAgMnS	zastávat
radikální	radikální	k2eAgInPc4d1	radikální
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupem	postup	k1gInSc7	postup
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgMnS	stávat
konzervativcem	konzervativec	k1gMnSc7	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ekonom	ekonom	k1gMnSc1	ekonom
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
Albína	Albín	k1gMnSc2	Albín
Bráfa	Bráf	k1gMnSc2	Bráf
stoupencem	stoupenec	k1gMnSc7	stoupenec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
Carl	Carl	k1gMnSc1	Carl
Menger	Menger	k1gMnSc1	Menger
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
von	von	k1gInSc1	von
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1	Böhm-Bawerk
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Wieser	Wieser	k1gInSc1	Wieser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
striktní	striktní	k2eAgFnSc4d1	striktní
rozpočtovou	rozpočtový	k2eAgFnSc4d1	rozpočtová
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
posílit	posílit	k5eAaPmF	posílit
československou	československý	k2eAgFnSc4d1	Československá
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ovšem	ovšem	k9	ovšem
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
i	i	k9	i
dopad	dopad	k1gInSc4	dopad
následující	následující	k2eAgFnSc2d1	následující
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
na	na	k7c4	na
tuzemský	tuzemský	k2eAgInSc4d1	tuzemský
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1921	[number]	k4	1921
a	a	k8xC	a
1923	[number]	k4	1923
se	se	k3xPyFc4	se
cenová	cenový	k2eAgFnSc1d1	cenová
hladina	hladina	k1gFnSc1	hladina
snížila	snížit	k5eAaPmAgFnS	snížit
o	o	k7c4	o
43	[number]	k4	43
%	%	kIx~	%
<g/>
,	,	kIx,	,
export	export	k1gInSc1	export
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
53	[number]	k4	53
%	%	kIx~	%
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
ze	z	k7c2	z
72	[number]	k4	72
na	na	k7c4	na
207	[number]	k4	207
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
kriticky	kriticky	k6eAd1	kriticky
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
politiků	politik	k1gMnPc2	politik
okolo	okolo	k7c2	okolo
Hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
pravděpodobnému	pravděpodobný	k2eAgNnSc3d1	pravděpodobné
vyostření	vyostření	k1gNnSc3	vyostření
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Rašínově	Rašínův	k2eAgFnSc3d1	Rašínova
náhlé	náhlý	k2eAgFnSc3d1	náhlá
smrti	smrt	k1gFnSc3	smrt
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Manželství	manželství	k1gNnSc1	manželství
===	===	k?	===
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Karlou	Karla	k1gFnSc7	Karla
Janskou	janský	k2eAgFnSc7d1	Janská
<g/>
,	,	kIx,	,
dívkou	dívka	k1gFnSc7	dívka
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
bratří	bratr	k1gMnPc2	bratr
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Janský	janský	k2eAgMnSc1d1	janský
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
objevitel	objevitel	k1gMnSc1	objevitel
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
byla	být	k5eAaImAgFnS	být
Rašínovi	Rašín	k1gMnSc3	Rašín
vždy	vždy	k6eAd1	vždy
velkou	velký	k2eAgFnSc7d1	velká
oporou	opora	k1gFnSc7	opora
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šťastného	šťastný	k2eAgNnSc2d1	šťastné
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
–	–	k?	–
synové	syn	k1gMnPc1	syn
Ladislav	Ladislav	k1gMnSc1	Ladislav
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
státní	státní	k2eAgNnSc1d1	státní
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Politické	politický	k2eAgInPc1d1	politický
zločiny	zločin	k1gInPc1	zločin
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Můj	můj	k3xOp1gInSc1	můj
finanční	finanční	k2eAgInSc1d1	finanční
plán	plán	k1gInSc1	plán
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
hospodářství	hospodářství	k1gNnSc1	hospodářství
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaBmNgNnS	napsat
v	v	k7c6	v
letech	let	k1gInPc6	let
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finanční	finanční	k2eAgFnSc1d1	finanční
a	a	k8xC	a
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
politika	politika	k1gFnSc1	politika
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Inflace	inflace	k1gFnSc1	inflace
a	a	k8xC	a
deflace	deflace	k1gFnSc1	deflace
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mé	můj	k3xOp1gFnPc1	můj
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paměti	paměť	k1gFnPc1	paměť
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k9	již
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
desetiletí	desetiletí	k1gNnSc6	desetiletí
byl	být	k5eAaImAgInS	být
Rašín	Rašín	k1gInSc1	Rašín
několikrát	několikrát	k6eAd1	několikrát
portrétován	portrétován	k2eAgMnSc1d1	portrétován
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Olejomalba	olejomalba	k1gFnSc1	olejomalba
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Viktor	Viktor	k1gMnSc1	Viktor
Stretti	Stretti	k1gNnPc2	Stretti
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
</s>
</p>
<p>
<s>
Busta	busta	k1gFnSc1	busta
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Eduard	Eduard	k1gMnSc1	Eduard
Šaff	Šaff	k1gMnSc1	Šaff
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
</s>
</p>
<p>
<s>
Busta	busta	k1gFnSc1	busta
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
František	František	k1gMnSc1	František
Rous	Rous	k1gMnSc1	Rous
<g/>
,	,	kIx,	,
ml.	ml.	kA	ml.
</s>
</p>
<p>
<s>
Busta	busta	k1gFnSc1	busta
<g/>
,	,	kIx,	,
anonym	anonym	k1gInSc1	anonym
</s>
</p>
<p>
<s>
Plaketa	plaketa	k1gFnSc1	plaketa
(	(	kIx(	(
<g/>
též	též	k9	též
pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šejnost	Šejnost	k1gFnSc1	Šejnost
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
</s>
</p>
<p>
<s>
Dvacetikorunová	dvacetikorunový	k2eAgFnSc1d1	dvacetikorunová
bankovka	bankovka	k1gFnSc1	bankovka
československá	československý	k2eAgFnSc1d1	Československá
<g/>
,	,	kIx,	,
portrét	portrét	k1gInSc1	portrét
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
oběžná	oběžný	k2eAgFnSc1d1	oběžná
mince	mince	k1gFnSc1	mince
nominální	nominální	k2eAgFnSc2d1	nominální
hodnoty	hodnota	k1gFnSc2	hodnota
10	[number]	k4	10
Kčs	Kčs	kA	Kčs
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Aloise	Alois	k1gMnSc2	Alois
Rašína	Rašín	k1gMnSc2	Rašín
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
Jarmila	Jarmila	k1gFnSc1	Jarmila
Truhlíková-Spěváková	Truhlíková-Spěváková	k1gFnSc1	Truhlíková-Spěváková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Úprka	Úprka	k1gMnSc1	Úprka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
Milan	Milan	k1gMnSc1	Milan
Knobloch	Knobloch	k1gMnSc1	Knobloch
</s>
</p>
<p>
<s>
Medaile	medaile	k1gFnSc1	medaile
Aloise	Alois	k1gMnSc2	Alois
Rašína	Rašín	k1gMnSc2	Rašín
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ocenění	ocenění	k1gNnSc1	ocenění
udělované	udělovaný	k2eAgNnSc1d1	udělované
Vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
nebo	nebo	k8xC	nebo
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
.	.	kIx.	.
<g/>
Místní	místní	k2eAgInPc1d1	místní
názvy	název	k1gInPc1	název
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
Rašínovi	Rašín	k1gMnSc6	Rašín
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
významné	významný	k2eAgNnSc1d1	významné
pražské	pražský	k2eAgNnSc1d1	Pražské
nábřeží	nábřeží	k1gNnSc1	nábřeží
(	(	kIx(	(
<g/>
Rašínovo	Rašínův	k2eAgNnSc1d1	Rašínovo
nábřeží	nábřeží	k1gNnSc1	nábřeží
<g/>
,	,	kIx,	,
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
vedené	vedený	k2eAgFnSc6d1	vedená
od	od	k7c2	od
Palackého	Palackého	k2eAgNnSc2d1	Palackého
náměstí	náměstí	k1gNnSc2	náměstí
do	do	k7c2	do
Podolí	Podolí	k1gNnSc2	Podolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Lažnovského	Lažnovský	k2eAgMnSc4d1	Lažnovský
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
opět	opět	k6eAd1	opět
Rašínovo	Rašínův	k2eAgNnSc1d1	Rašínovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
nábřeží	nábřeží	k1gNnSc1	nábřeží
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Engelse	Engels	k1gMnSc2	Engels
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
Rašínovo	Rašínův	k2eAgNnSc1d1	Rašínovo
nábřeží	nábřeží	k1gNnSc1	nábřeží
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
mostu	most	k1gInSc2	most
k	k	k7c3	k
Výtoni	výtoň	k1gFnSc3	výtoň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rokycanech	Rokycany	k1gInPc6	Rokycany
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
nová	nový	k2eAgFnSc1d1	nová
čtvrť	čtvrť	k1gFnSc1	čtvrť
rodinných	rodinný	k2eAgInPc2d1	rodinný
domků	domek	k1gInPc2	domek
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Rašínov	Rašínov	k1gInSc1	Rašínov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Koderová	Koderová	k1gFnSc1	Koderová
-	-	kIx~	-
Sojka	Sojka	k1gMnSc1	Sojka
-	-	kIx~	-
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Teorie	teorie	k1gFnSc1	teorie
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Wolters	Wolters	k1gInSc1	Wolters
Kluwer	Kluwer	k1gInSc1	Kluwer
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
KOSATÍK	KOSATÍK	kA	KOSATÍK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
:	:	kIx,	:
50	[number]	k4	50
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2307	[number]	k4	2307
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lacina	Lacina	k1gMnSc1	Lacina
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
PRECLÍK	preclík	k1gInSc1	preclík
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
legie	legie	k1gFnSc1	legie
<g/>
,	,	kIx,	,
219	[number]	k4	219
str	str	kA	str
<g/>
.	.	kIx.	.
Karvinná	Karvinný	k2eAgFnSc1d1	Karvinná
<g/>
:	:	kIx,	:
Paris	Paris	k1gMnSc1	Paris
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
demokratickým	demokratický	k2eAgNnSc7d1	demokratické
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87173	[number]	k4	87173
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
;	;	kIx,	;
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
116	[number]	k4	116
<g/>
;	;	kIx,	;
125	[number]	k4	125
<g/>
-	-	kIx~	-
<g/>
148	[number]	k4	148
<g/>
;	;	kIx,	;
157	[number]	k4	157
<g/>
-	-	kIx~	-
<g/>
162	[number]	k4	162
<g/>
;	;	kIx,	;
165-169	[number]	k4	165-169
</s>
</p>
<p>
<s>
RAŠÍN	RAŠÍN	kA	RAŠÍN
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Paměti	paměť	k1gFnPc1	paměť
Dra	dřít	k5eAaImSgInS	dřít
Aloise	Alois	k1gMnSc4	Alois
Rašína	Rašín	k1gInSc2	Rašín
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
229	[number]	k4	229
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šetřilová	Šetřilová	k1gFnSc1	Šetřilová
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
:	:	kIx,	:
Dramatický	dramatický	k2eAgInSc1d1	dramatický
život	život	k1gInSc1	život
českého	český	k2eAgNnSc2d1	české
politika	politikum	k1gNnSc2	politikum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Šlechta	Šlechta	k1gMnSc1	Šlechta
<g/>
,	,	kIx,	,
O.	O.	kA	O.
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
in	in	k?	in
Národní	národní	k2eAgFnSc1d1	národní
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
:	:	kIx,	:
Osobnosti	osobnost	k1gFnPc1	osobnost
pravice	pravice	k1gFnSc2	pravice
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-903582-2-5	[number]	k4	80-903582-2-5
</s>
</p>
<p>
<s>
Olivová	olivový	k2eAgFnSc1d1	olivová
Věra	Věra	k1gFnSc1	Věra
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-86107-47-9	[number]	k4	978-80-86107-47-9
</s>
</p>
<p>
<s>
HOCH	hoch	k1gMnSc1	hoch
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
dostupné	dostupný	k2eAgFnPc1d1	dostupná
online	onlin	k1gMnSc5	onlin
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašína	k1gFnPc2	Rašína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Soupis	soupis	k1gInSc1	soupis
pražského	pražský	k2eAgNnSc2d1	Pražské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
1830-1910	[number]	k4	1830-1910
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rašín	Rašín	k1gMnSc1	Rašín
Alois	Alois	k1gMnSc1	Alois
*	*	kIx~	*
<g/>
1867	[number]	k4	1867
</s>
</p>
<p>
<s>
Příspěvky	příspěvek	k1gInPc1	příspěvek
přednesené	přednesený	k2eAgInPc1d1	přednesený
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
"	"	kIx"	"
<g/>
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
–	–	k?	–
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
nárohospodář	nárohospodář	k1gMnSc1	nárohospodář
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Janovec	janovec	k1gInSc1	janovec
<g/>
:	:	kIx,	:
Pocta	pocta	k1gFnSc1	pocta
velikánovi	velikán	k1gMnSc3	velikán
(	(	kIx(	(
<g/>
JUDr.	JUDr.	kA	JUDr.
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mgr.	Mgr.	kA	Mgr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Seidl	Seidl	k1gMnSc1	Seidl
(	(	kIx(	(
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
fond	fond	k1gInSc1	fond
českého	český	k2eAgNnSc2d1	české
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
</s>
</p>
<p>
<s>
Dokumenty	dokument	k1gInPc1	dokument
z	z	k7c2	z
Archivu	archiv	k1gInSc2	archiv
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Hoch	hoch	k1gMnSc1	hoch
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
online	onlinout	k5eAaPmIp3nS	onlinout
v	v	k7c6	v
eknihovně	eknihovně	k6eAd1	eknihovně
Bohemian	bohemian	k1gInSc1	bohemian
Library	Librara	k1gFnSc2	Librara
</s>
</p>
