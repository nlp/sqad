<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
enfants	enfants	k1gInSc1	enfants
du	du	k?	du
capitaine	capitainout	k5eAaPmIp3nS	capitainout
Grant	grant	k1gInSc1	grant
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
románů	román	k1gInPc2	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Verna	k1gFnSc1	Verna
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
cyklu	cyklus	k1gInSc2	cyklus
Podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Voyages	Voyages	k1gMnSc1	Voyages
extraordinaires	extraordinaires	k1gMnSc1	extraordinaires
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
tvoří	tvořit	k5eAaImIp3nS	tvořit
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
autorovy	autorův	k2eAgFnSc2d1	autorova
volné	volný	k2eAgFnSc2d1	volná
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
ještě	ještě	k6eAd1	ještě
patří	patřit	k5eAaImIp3nS	patřit
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
(	(	kIx(	(
<g/>
Vingt	Vingt	k2eAgInSc4d1	Vingt
mille	mille	k1gInSc4	mille
lieues	lieuesa	k1gFnPc2	lieuesa
sous	sous	k6eAd1	sous
les	les	k1gInSc1	les
mers	mersa	k1gFnPc2	mersa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
mystérieuse	mystérieuse	k1gFnSc1	mystérieuse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
díly	díl	k1gInPc7	díl
Vernovy	Vernův	k2eAgFnSc2d1	Vernova
trilogie	trilogie	k1gFnSc2	trilogie
však	však	k9	však
panuje	panovat	k5eAaImIp3nS	panovat
časový	časový	k2eAgInSc4d1	časový
nesoulad	nesoulad	k1gInSc4	nesoulad
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Časový	časový	k2eAgInSc1d1	časový
nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
díly	díl	k1gInPc7	díl
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
posádka	posádka	k1gFnSc1	posádka
jachty	jachta	k1gFnSc2	jachta
Duncan	Duncana	k1gFnPc2	Duncana
ulovila	ulovit	k5eAaPmAgFnS	ulovit
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
průlivu	průliv	k1gInSc6	průliv
mezi	mezi	k7c7	mezi
Irskem	Irsko	k1gNnSc7	Irsko
a	a	k8xC	a
Skotskem	Skotsko	k1gNnSc7	Skotsko
žraloka	žralok	k1gMnSc2	žralok
kladivouna	kladivoun	k1gMnSc2	kladivoun
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
útrobách	útroba	k1gFnPc6	útroba
objevila	objevit	k5eAaPmAgFnS	objevit
tři	tři	k4xCgFnPc4	tři
neúplné	úplný	k2eNgFnPc4d1	neúplná
listiny	listina	k1gFnPc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
napsány	napsat	k5eAaPmNgFnP	napsat
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
německy	německy	k6eAd1	německy
a	a	k8xC	a
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
ztroskotání	ztroskotání	k1gNnSc4	ztroskotání
lodi	loď	k1gFnSc2	loď
Britannia	Britannium	k1gNnSc2	Britannium
s	s	k7c7	s
kapitánem	kapitán	k1gMnSc7	kapitán
Grantem	grant	k1gInSc7	grant
a	a	k8xC	a
posádkou	posádka	k1gFnSc7	posádka
na	na	k7c4	na
37	[number]	k4	37
<g/>
°	°	k?	°
11	[number]	k4	11
<g/>
́	́	k?	́
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
délce	délka	k1gFnSc6	délka
byly	být	k5eAaImAgInP	být
bohužel	bohužel	k6eAd1	bohužel
nečitelné	čitelný	k2eNgInPc1d1	nečitelný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
jachty	jachta	k1gFnSc2	jachta
lord	lord	k1gMnSc1	lord
Glenarvan	Glenarvan	k1gMnSc1	Glenarvan
neváhal	váhat	k5eNaImAgMnS	váhat
ani	ani	k8xC	ani
okamžik	okamžik	k1gInSc4	okamžik
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
kapitánem	kapitán	k1gMnSc7	kapitán
Johnem	John	k1gMnSc7	John
Manglesem	Mangles	k1gMnSc7	Mangles
<g/>
,	,	kIx,	,
majorem	major	k1gMnSc7	major
Mac	Mac	kA	Mac
Nabbsem	Nabbs	k1gMnSc7	Nabbs
<g/>
,	,	kIx,	,
roztržitým	roztržitý	k2eAgMnSc7d1	roztržitý
zeměpiscem	zeměpisec	k1gMnSc7	zeměpisec
Jacquesem	Jacques	k1gMnSc7	Jacques
Paganelem	Paganel	k1gMnSc7	Paganel
a	a	k8xC	a
s	s	k7c7	s
Mary	Mary	k1gFnSc7	Mary
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Grantovými	grantový	k2eAgFnPc7d1	Grantová
<g/>
,	,	kIx,	,
dětmi	dítě	k1gFnPc7	dítě
ztraceného	ztracený	k2eAgInSc2d1	ztracený
skotského	skotský	k1gInSc2	skotský
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
,	,	kIx,	,
na	na	k7c4	na
dobrodružnou	dobrodružný	k2eAgFnSc4d1	dobrodružná
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamenala	znamenat	k5eAaImAgFnS	znamenat
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
po	po	k7c4	po
37	[number]	k4	37
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výprava	výprava	k1gFnSc1	výprava
začala	začít	k5eAaPmAgFnS	začít
pátráním	pátrání	k1gNnSc7	pátrání
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
ani	ani	k8xC	ani
pochod	pochod	k1gInSc1	pochod
přes	přes	k7c4	přes
Andy	Anda	k1gFnPc4	Anda
nepřinesl	přinést	k5eNaPmAgInS	přinést
žádaný	žádaný	k2eAgInSc1d1	žádaný
výsledek	výsledek	k1gInSc1	výsledek
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
naši	náš	k3xOp1gMnPc1	náš
přátelé	přítel	k1gMnPc1	přítel
zamířili	zamířit	k5eAaPmAgMnP	zamířit
k	k	k7c3	k
břehům	břeh	k1gInPc3	břeh
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
lodníkem	lodník	k1gMnSc7	lodník
Britannie	Britannie	k1gFnSc2	Britannie
Ayrtonem	Ayrton	k1gInSc7	Ayrton
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
provázel	provázet	k5eAaImAgMnS	provázet
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
vyklubal	vyklubat	k5eAaPmAgMnS	vyklubat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
však	však	k9	však
podvodník	podvodník	k1gMnSc1	podvodník
a	a	k8xC	a
vůdce	vůdce	k1gMnSc1	vůdce
trestanců	trestanec	k1gMnPc2	trestanec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
získat	získat	k5eAaPmF	získat
jejich	jejich	k3xOp3gInSc4	jejich
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
cestovatelé	cestovatel	k1gMnPc1	cestovatel
přes	přes	k7c4	přes
velké	velký	k2eAgFnPc4d1	velká
útrapy	útrapa	k1gFnPc4	útrapa
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
jachta	jachta	k1gFnSc1	jachta
tam	tam	k6eAd1	tam
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nečekala	čekat	k5eNaImAgFnS	čekat
<g/>
,	,	kIx,	,
omylem	omylem	k6eAd1	omylem
před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
příchodem	příchod	k1gInSc7	příchod
odplula	odplout	k5eAaPmAgFnS	odplout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
lodi	loď	k1gFnSc6	loď
dopluli	doplout	k5eAaPmAgMnP	doplout
Skotové	Skot	k1gMnPc1	Skot
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
padli	padnout	k5eAaImAgMnP	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
lidojedů	lidojed	k1gMnPc2	lidojed
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
je	on	k3xPp3gMnPc4	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Paganelovi	Paganel	k1gMnSc3	Paganel
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
zde	zde	k6eAd1	zde
je	on	k3xPp3gFnPc4	on
čekalo	čekat	k5eAaImAgNnS	čekat
velké	velký	k2eAgNnSc1d1	velké
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Vody	voda	k1gFnPc4	voda
zde	zde	k6eAd1	zde
křižoval	křižovat	k5eAaImAgMnS	křižovat
Duncan	Duncan	k1gInSc4	Duncan
s	s	k7c7	s
v	v	k7c6	v
podpalubí	podpalubí	k1gNnSc6	podpalubí
uvězněným	uvězněný	k2eAgInSc7d1	uvězněný
Ayrtonem	Ayrton	k1gInSc7	Ayrton
<g/>
.	.	kIx.	.
</s>
<s>
Náhoda	náhoda	k1gFnSc1	náhoda
přivedla	přivést	k5eAaPmAgFnS	přivést
jachtu	jachta	k1gFnSc4	jachta
k	k	k7c3	k
malému	malý	k2eAgInSc3d1	malý
ostrůvku	ostrůvek	k1gInSc3	ostrůvek
Tabor	Tabora	k1gFnPc2	Tabora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
lodník	lodník	k1gMnSc1	lodník
vysazen	vysazen	k2eAgMnSc1d1	vysazen
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yQgNnSc1	jaký
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
překvapení	překvapení	k1gNnSc3	překvapení
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
obydlen	obydlet	k5eAaPmNgInS	obydlet
třemi	tři	k4xCgInPc7	tři
trosečníky	trosečník	k1gMnPc7	trosečník
<g/>
,	,	kIx,	,
kapitánem	kapitán	k1gMnSc7	kapitán
Grantem	grant	k1gInSc7	grant
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
námořníky	námořník	k1gMnPc7	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
končí	končit	k5eAaImIp3nS	končit
ponecháním	ponechání	k1gNnSc7	ponechání
Ayrtona	Ayrton	k1gMnSc2	Ayrton
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
šťastným	šťastný	k2eAgInSc7d1	šťastný
návratem	návrat	k1gInSc7	návrat
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčtenějších	čtený	k2eAgFnPc2d3	Nejčtenější
Vernových	Vernův	k2eAgFnPc2d1	Vernova
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Verne	Vernout	k5eAaImIp3nS	Vernout
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
prostého	prostý	k2eAgNnSc2d1	prosté
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napínavého	napínavý	k2eAgInSc2d1	napínavý
příběhu	příběh	k1gInSc2	příběh
mnoho	mnoho	k4c4	mnoho
zeměpisných	zeměpisný	k2eAgMnPc2d1	zeměpisný
<g/>
,	,	kIx,	,
přírodovědných	přírodovědný	k2eAgInPc2d1	přírodovědný
i	i	k8xC	i
národopisných	národopisný	k2eAgInPc2d1	národopisný
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k8xC	jako
dobrý	dobrý	k2eAgMnSc1d1	dobrý
znalec	znalec	k1gMnSc1	znalec
lidských	lidský	k2eAgFnPc2d1	lidská
povah	povaha	k1gFnPc2	povaha
vykreslil	vykreslit	k5eAaPmAgInS	vykreslit
v	v	k7c6	v
dětech	dítě	k1gFnPc6	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
opravdové	opravdový	k2eAgFnPc4d1	opravdová
hrdiny	hrdina	k1gMnPc7	hrdina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
vzorem	vzor	k1gInSc7	vzor
statečnosti	statečnost	k1gFnSc2	statečnost
a	a	k8xC	a
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ilustrace	ilustrace	k1gFnSc2	ilustrace
==	==	k?	==
</s>
</p>
<p>
<s>
Knihu	kniha	k1gFnSc4	kniha
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
Édouard	Édouard	k1gInSc4	Édouard
Riou	Rious	k1gInSc2	Rious
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Vernova	Vernův	k2eAgMnSc2d1	Vernův
syna	syn	k1gMnSc2	syn
Michela	Michel	k1gMnSc2	Michel
natočen	natočen	k2eAgInSc1d1	natočen
francouzský	francouzský	k2eAgInSc1d1	francouzský
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
Les	les	k1gInSc1	les
Enfants	Enfants	k1gInSc1	Enfants
du	du	k?	du
capitaine	capitainout	k5eAaPmIp3nS	capitainout
Grant	grant	k1gInSc4	grant
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
dvaceti	dvacet	k4xCc2	dvacet
tří	tři	k4xCgInPc2	tři
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
známým	známý	k2eAgFnPc3d1	známá
filmovým	filmový	k2eAgFnPc3d1	filmová
adaptacím	adaptace	k1gFnPc3	adaptace
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Д	Д	k?	Д
к	к	k?	к
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgInSc1d1	sovětský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vajnshtok	Vajnshtok	k1gInSc1	Vajnshtok
a	a	k8xC	a
David	David	k1gMnSc1	David
Gutman	Gutman	k1gMnSc1	Gutman
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
trosečníků	trosečník	k1gMnPc2	trosečník
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
Search	Search	k1gMnSc1	Search
of	of	k?	of
the	the	k?	the
Castaways	Castaways	k1gInSc1	Castaways
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Robert	Robert	k1gMnSc1	Robert
Stevenson	Stevenson	k1gMnSc1	Stevenson
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hledání	hledání	k1gNnSc1	hledání
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
В	В	k?	В
п	п	k?	п
к	к	k?	к
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedmidílný	sedmidílný	k2eAgInSc1d1	sedmidílný
sovětský	sovětský	k2eAgInSc1d1	sovětský
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Stanislav	Stanislav	k1gMnSc1	Stanislav
Govoruchin	Govoruchin	k1gMnSc1	Govoruchin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Captain	Captain	k1gInSc1	Captain
Grant	grant	k1gInSc1	grant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Donovan	Donovan	k1gMnSc1	Donovan
Scott	Scott	k1gMnSc1	Scott
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Granta	k1gFnSc1	Granta
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
R.	R.	kA	R.
Lauermann	Lauermann	k1gMnSc1	Lauermann
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Granta	k1gFnSc1	Granta
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
svazky	svazek	k1gInPc1	svazek
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Granta	k1gFnSc1	Granta
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
svazky	svazek	k1gInPc4	svazek
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Pelant	Pelant	k1gMnSc1	Pelant
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Eduard	Eduard	k1gMnSc1	Eduard
Beaufort	Beaufort	k1gInSc4	Beaufort
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
Šumavský	šumavský	k2eAgMnSc1d1	šumavský
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
a	a	k8xC	a
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1947	[number]	k4	1947
a	a	k8xC	a
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Marie	Marie	k1gFnSc1	Marie
Sobotková	Sobotková	k1gFnSc1	Sobotková
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1955	[number]	k4	1955
a	a	k8xC	a
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Benjamin	Benjamin	k1gMnSc1	Benjamin
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Granta	k1gFnSc1	Granta
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
Šumavský	šumavský	k2eAgMnSc1d1	šumavský
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Granta	k1gFnSc1	Granta
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
Ondřej	Ondřej	k1gMnSc1	Ondřej
Neff	Neff	k1gMnSc1	Neff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
Omega	omega	k1gNnSc1	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
Šumavský	šumavský	k2eAgMnSc1d1	šumavský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
text	text	k1gInSc1	text
románu	román	k1gInSc2	román
</s>
</p>
<p>
<s>
Digitalizované	digitalizovaný	k2eAgNnSc1d1	digitalizované
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
od	od	k7c2	od
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Digitalizované	digitalizovaný	k2eAgNnSc1d1	digitalizované
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
od	od	k7c2	od
Václava	Václav	k1gMnSc2	Václav
Beneše	Beneš	k1gMnSc2	Beneš
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
</s>
</p>
