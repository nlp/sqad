<s>
Děti	dítě	k1gFnPc1
kapitána	kapitán	k1gMnSc2
Granta	Grant	k1gMnSc2
(	(	kIx(
<g/>
1867	[number]	k4
<g/>
-	-	kIx~
<g/>
1868	[number]	k4
<g/>
,	,	kIx,
Les	les	k1gInSc1
enfants	enfants	k1gInSc1
du	du	k?
capitaine	capitainout	k5eAaPmIp3nS
Grant	grant	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
románů	román	k1gInPc2
francouzského	francouzský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Julesa	Julesa	k1gFnSc1
Verna	Verna	k1gFnSc1
z	z	k7c2
jeho	jeho	k3xOp3gInSc2
cyklu	cyklus	k1gInSc2
Podivuhodné	podivuhodný	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
(	(	kIx(
<g/>
Les	les	k1gInSc1
Voyages	Voyages	k1gMnSc1
extraordinaires	extraordinaires	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>