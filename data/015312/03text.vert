<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
KřivoklátskoIUCN	KřivoklátskoIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Týřov	Týřov	k1gInSc1
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
od	od	k7c2
SkryjíZákladní	SkryjíZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1978	#num#	k4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
223	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
628	#num#	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
24	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.krivoklatsko.ochranaprirody.cz	www.krivoklatsko.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Křivoklátsko	Křivoklátsko	k1gNnSc4
je	být	k5eAaImIp3nS
chráněné	chráněný	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
628	#num#	k4
km²	km²	k?
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
původního	původní	k2eAgInSc2d1
krajinného	krajinný	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
včetně	včetně	k7c2
mimořádně	mimořádně	k6eAd1
cenných	cenný	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
západním	západní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
zhruba	zhruba	k6eAd1
30	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středem	středem	k7c2
CHKO	CHKO	kA
vede	vést	k5eAaImIp3nS
kaňonovité	kaňonovitý	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
řeky	řeka	k1gFnSc2
Berounky	Berounka	k1gFnSc2
<g/>
,	,	kIx,
odvodňující	odvodňující	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
je	být	k5eAaImIp3nS
Těchovín	Těchovín	k1gInSc1
(	(	kIx(
<g/>
616	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
nejnižším	nízký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Berounka	Berounka	k1gFnSc1
při	při	k7c6
výtoku	výtok	k1gInSc6
v	v	k7c6
Hýskově	Hýskův	k2eAgInSc6d1
(	(	kIx(
<g/>
217	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1977	#num#	k4
se	se	k3xPyFc4
Křivoklátsko	Křivoklátsko	k1gNnSc1
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
význam	význam	k1gInSc4
stalo	stát	k5eAaPmAgNnS
biosférickou	biosférický	k2eAgFnSc7d1
rezervací	rezervace	k1gFnSc7
UNESCO	Unesco	k1gNnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
i	i	k9
ptačí	ptačí	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
v	v	k7c6
rámci	rámec	k1gInSc6
soustavy	soustava	k1gFnSc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
několik	několik	k4yIc1
let	léto	k1gNnPc2
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
přechod	přechod	k1gInSc1
na	na	k7c4
vyšší	vysoký	k2eAgInSc4d2
stupeň	stupeň	k1gInSc4
ochrany	ochrana	k1gFnSc2
vytvořením	vytvoření	k1gNnSc7
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
z	z	k7c2
přírodně	přírodně	k6eAd1
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Území	území	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
západním	západní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
zhruba	zhruba	k6eAd1
30	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středem	středem	k7c2
CHKO	CHKO	kA
vede	vést	k5eAaImIp3nS
kaňonovité	kaňonovitý	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
řeky	řeka	k1gFnSc2
Berounky	Berounka	k1gFnSc2
<g/>
,	,	kIx,
odvodňující	odvodňující	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
je	být	k5eAaImIp3nS
Těchovín	Těchovín	k1gInSc1
(	(	kIx(
<g/>
616	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
nejnižším	nízký	k2eAgNnSc7d3
Berounka	Berounka	k1gFnSc1
při	při	k7c6
výtoku	výtok	k1gInSc6
v	v	k7c6
Hýskově	Hýskův	k2eAgInSc6d1
(	(	kIx(
<g/>
217	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
vrch	vrch	k1gInSc4
se	s	k7c7
zbytky	zbytek	k1gInPc7
oppida	oppidum	k1gNnSc2
Stradonice	Stradonice	k1gFnPc1
</s>
<s>
vnitřek	vnitřek	k1gInSc1
chlévu	chlév	k1gInSc3
u	u	k7c2
statku	statek	k1gInSc2
Karlov	Karlov	k1gInSc1
</s>
<s>
Na	na	k7c6
současném	současný	k2eAgInSc6d1
stavu	stav	k1gInSc6
zdejší	zdejší	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
se	se	k3xPyFc4
z	z	k7c2
největší	veliký	k2eAgFnSc2d3
míry	míra	k1gFnSc2
podílí	podílet	k5eAaImIp3nS
její	její	k3xOp3gInSc1
historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křivoklátsko	Křivoklátsko	k1gNnSc1
leží	ležet	k5eAaImIp3nS
mimo	mimo	k7c4
starou	starý	k2eAgFnSc4d1
sídelní	sídelní	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
jeho	jeho	k3xOp3gNnSc1
osídlení	osídlení	k1gNnSc1
až	až	k9
do	do	k7c2
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
řídké	řídký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
jen	jen	k6eAd1
pár	pár	k4xCyI
lokalit	lokalita	k1gFnPc2
pravěkého	pravěký	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
okolí	okolí	k1gNnSc1
Hýskova	Hýskův	k2eAgInSc2d1
<g/>
,	,	kIx,
pohřebištní	pohřebištní	k2eAgFnSc2d1
mohyly	mohyla	k1gFnSc2
v	v	k7c6
Lánské	Lánské	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
<g/>
,	,	kIx,
halštatské	halštatský	k2eAgNnSc1d1
hradiště	hradiště	k1gNnSc1
u	u	k7c2
Branova	Branov	k1gInSc2
nazývané	nazývaný	k2eAgFnSc2d1
„	„	k?
<g/>
Propadený	propadený	k2eAgInSc1d1
zámek	zámek	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
hradištní	hradištní	k2eAgMnSc1d1
předchůdce	předchůdce	k1gMnSc1
Křivoklátu	Křivoklát	k1gInSc2
z	z	k7c2
pozdní	pozdní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lokalitou	lokalita	k1gFnSc7
středoevropského	středoevropský	k2eAgInSc2d1
významu	význam	k1gInSc2
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
keltské	keltský	k2eAgNnSc1d1
oppidum	oppidum	k1gNnSc1
Stradonice	Stradonice	k1gFnSc2
z	z	k7c2
pozdní	pozdní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
laténské	laténský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
prvotní	prvotní	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
druhého	druhý	k4xOgNnSc2
století	století	k1gNnSc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
valové	valový	k2eAgNnSc1d1
opevnění	opevnění	k1gNnSc1
bylo	být	k5eAaImAgNnS
dostavěno	dostavět	k5eAaPmNgNnS
později	pozdě	k6eAd2
a	a	k8xC
celková	celkový	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
oppida	oppidum	k1gNnSc2
činila	činit	k5eAaImAgFnS
82	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nálezu	nález	k1gInSc6
dvou	dva	k4xCgNnPc2
set	set	k1gInSc4
zlatých	zlatý	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
roku	rok	k1gInSc2
1877	#num#	k4
bylo	být	k5eAaImAgNnS
návrší	návrší	k1gNnSc1
vystaveno	vystaven	k2eAgNnSc1d1
značnému	značný	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
hledačů	hledač	k1gMnPc2
pokladů	poklad	k1gInPc2
<g/>
,	,	kIx,
dočkalo	dočkat	k5eAaPmAgNnS
se	se	k3xPyFc4
ale	ale	k9
i	i	k9
archeologického	archeologický	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
lokalitou	lokalita	k1gFnSc7
spojenou	spojený	k2eAgFnSc7d1
s	s	k7c7
Kelty	Kelt	k1gMnPc7
je	být	k5eAaImIp3nS
ves	ves	k1gFnSc1
Podmokly	Podmokly	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
roku	rok	k1gInSc2
1771	#num#	k4
nalezen	nalezen	k2eAgInSc4d1
největší	veliký	k2eAgInSc4d3
keltský	keltský	k2eAgInSc4d1
poklad	poklad	k1gInSc4
vůbec	vůbec	k9
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
5000	#num#	k4
tisíc	tisíc	k4xCgInPc2
zlatých	zlatý	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
váze	váha	k1gFnSc6
40	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
jich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
po	po	k7c6
nálezu	nález	k1gInSc6
přetavena	přetaven	k2eAgFnSc1d1
<g/>
,	,	kIx,
pár	pár	k4xCyI
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
v	v	k7c6
muzeích	muzeum	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
hrálo	hrát	k5eAaImAgNnS
Křivoklátsko	Křivoklátsko	k1gNnSc1
úlohu	úloha	k1gFnSc4
loveckého	lovecký	k2eAgInSc2d1
hvozdu	hvozd	k1gInSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
spravován	spravovat	k5eAaImNgInS
nejprve	nejprve	k6eAd1
ze	z	k7c2
zaniklého	zaniklý	k2eAgInSc2d1
hradu	hrad	k1gInSc2
Hlavačov	Hlavačov	k1gInSc1
u	u	k7c2
Rakovníka	Rakovník	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
z	z	k7c2
Křivoklátu	Křivoklát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvislé	souvislý	k2eAgInPc1d1
lesní	lesní	k2eAgInPc1d1
celky	celek	k1gInPc1
byly	být	k5eAaImAgInP
záměrně	záměrně	k6eAd1
zachovávány	zachováván	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesnictví	lesnictví	k1gNnSc1
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
rozvíjet	rozvíjet	k5eAaImF
kolem	kolem	k7c2
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
za	za	k7c2
Fürstenberků	Fürstenberk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působili	působit	k5eAaImAgMnP
zde	zde	k6eAd1
odborníci	odborník	k1gMnPc1
z	z	k7c2
Čech	Čechy	k1gFnPc2
i	i	k8xC
Bavorska	Bavorsko	k1gNnSc2
<g/>
,	,	kIx,
vytěžené	vytěžený	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
osazovat	osazovat	k5eAaImF
novými	nový	k2eAgFnPc7d1
sazenicemi	sazenice	k1gFnPc7
a	a	k8xC
na	na	k7c6
Křivoklátě	Křivoklát	k1gInSc6
vznikla	vzniknout	k5eAaPmAgFnS
lesnická	lesnický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
dřevo	dřevo	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
těžit	těžit	k5eAaImF
podle	podle	k7c2
desetiletých	desetiletý	k2eAgInPc2d1
plánů	plán	k1gInPc2
a	a	k8xC
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
se	se	k3xPyFc4
zakládají	zakládat	k5eAaImIp3nP
smrkové	smrkový	k2eAgFnSc2d1
monokultury	monokultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
rychle	rychle	k6eAd1
změnila	změnit	k5eAaPmAgFnS
druhová	druhový	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
jednom	jeden	k4xCgNnSc6
zkoumaném	zkoumaný	k2eAgNnSc6d1
území	území	k1gNnSc6
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
roku	rok	k1gInSc2
1794	#num#	k4
65	#num#	k4
%	%	kIx~
listnatých	listnatý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1869	#num#	k4
už	už	k6eAd1
jen	jen	k9
15	#num#	k4
%	%	kIx~
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
dokonce	dokonce	k9
pouze	pouze	k6eAd1
4	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plošně	plošně	k6eAd1
sice	sice	k8xC
nebyla	být	k5eNaImAgFnS
tato	tento	k3xDgFnSc1
změna	změna	k1gFnSc1
tak	tak	k6eAd1
extrémní	extrémní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
množící	množící	k2eAgMnSc1d1
se	se	k3xPyFc4
smrkové	smrkový	k2eAgFnPc1d1
monokultury	monokultura	k1gFnPc1
přinášely	přinášet	k5eAaImAgFnP
stále	stále	k6eAd1
více	hodně	k6eAd2
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
největší	veliký	k2eAgFnSc3d3
kalamitě	kalamita	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vichřice	vichřice	k1gFnSc1
poškodila	poškodit	k5eAaPmAgFnS
přes	přes	k7c4
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
hektarů	hektar	k1gInPc2
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaha	snaha	k1gFnSc1
zlepšit	zlepšit	k5eAaPmF
druhovou	druhový	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
dřevin	dřevina	k1gFnPc2
se	se	k3xPyFc4
probouzí	probouzet	k5eAaImIp3nS
až	až	k9
na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělství	zemědělství	k1gNnSc1
se	se	k3xPyFc4
výrazněji	výrazně	k6eAd2
rozvíjí	rozvíjet	k5eAaImIp3nS
od	od	k7c2
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
třicetileté	třicetiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
ale	ale	k8xC
řada	řada	k1gFnSc1
vsí	ves	k1gFnPc2
zaniká	zanikat	k5eAaImIp3nS
a	a	k8xC
krajina	krajina	k1gFnSc1
pustne	pustnout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Fürstenberků	Fürstenberk	k1gInPc2
se	se	k3xPyFc4
zakládají	zakládat	k5eAaImIp3nP
zemědělské	zemědělský	k2eAgInPc1d1
dvory	dvůr	k1gInPc1
spravující	spravující	k2eAgFnSc2d1
polnosti	polnost	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
dvůr	dvůr	k1gInSc1
Karlov	Karlov	k1gInSc1
<g/>
,	,	kIx,
rozvíjí	rozvíjet	k5eAaImIp3nS
se	se	k3xPyFc4
pastva	pastva	k1gFnSc1
dobytka	dobytek	k1gInSc2
a	a	k8xC
podél	podél	k7c2
cest	cesta	k1gFnPc2
se	se	k3xPyFc4
zakládají	zakládat	k5eAaImIp3nP
aleje	alej	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodaření	hospodaření	k1gNnSc1
upadá	upadat	k5eAaPmIp3nS,k5eAaImIp3nS
od	od	k7c2
pozemkové	pozemkový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
zejména	zejména	k9
pak	pak	k6eAd1
po	po	k7c6
kolektivizaci	kolektivizace	k1gFnSc6
zemědělství	zemědělství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhled	vzhled	k1gInSc4
zdejší	zdejší	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
období	období	k1gNnSc6
komunismu	komunismus	k1gInSc2
značně	značně	k6eAd1
degradován	degradovat	k5eAaBmNgInS
nevhodnou	vhodný	k2eNgFnSc7d1
architekturou	architektura	k1gFnSc7
domů	dům	k1gInPc2
i	i	k8xC
stavbou	stavba	k1gFnSc7
předimenzovaných	předimenzovaný	k2eAgInPc2d1
kravínů	kravín	k1gInPc2
<g/>
,	,	kIx,
vepřínů	vepřín	k1gInPc2
<g/>
,	,	kIx,
silážních	silážní	k2eAgInPc2d1
žlabů	žlab	k1gInPc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
narušují	narušovat	k5eAaImIp3nP
ráz	ráz	k1gInSc4
jinak	jinak	k6eAd1
malebné	malebný	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
geologická	geologický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
okolí	okolí	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
oranžový	oranžový	k2eAgInSc1d1
pruh	pruh	k1gInSc1
vlevo	vlevo	k6eAd1
značí	značit	k5eAaImIp3nS
vyvřelé	vyvřelý	k2eAgNnSc1d1
křivoklátsko-rokycanské	křivoklátsko-rokycanský	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
Území	území	k1gNnSc1
je	být	k5eAaImIp3nS
geologicky	geologicky	k6eAd1
velmi	velmi	k6eAd1
staré	starý	k2eAgFnPc1d1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
zastoupené	zastoupený	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
starohorní	starohorní	k2eAgFnPc1d1
usazené	usazený	k2eAgFnPc1d1
břidlice	břidlice	k1gFnPc1
a	a	k8xC
droby	droba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedimenty	sediment	k1gInPc7
jsou	být	k5eAaImIp3nP
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
porušeny	porušen	k2eAgInPc4d1
podmořskými	podmořský	k2eAgInPc7d1
výlevy	výlev	k1gInPc7
bazaltového	bazaltový	k2eAgNnSc2d1
magmatu	magma	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
spilit	spilit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starších	starý	k2eAgFnPc6d2
prvohorách	prvohory	k1gFnPc6
(	(	kIx(
<g/>
kambrium	kambrium	k1gNnSc1
<g/>
)	)	kIx)
zasahovalo	zasahovat	k5eAaImAgNnS
moře	moře	k1gNnSc1
do	do	k7c2
okolí	okolí	k1gNnSc2
Skryjí	skrýt	k5eAaPmIp3nP
<g/>
,	,	kIx,
kde	kde	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
břidlice	břidlice	k1gFnPc1
velmi	velmi	k6eAd1
bohaté	bohatý	k2eAgFnPc1d1
na	na	k7c4
zkameněliny	zkamenělina	k1gFnPc4
mořských	mořský	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
trilobitů	trilobit	k1gMnPc2
<g/>
,	,	kIx,
ramenonožců	ramenonožec	k1gMnPc2
a	a	k8xC
ostnokožců	ostnokožec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
zkoumání	zkoumání	k1gNnSc1
se	se	k3xPyFc4
intenzivně	intenzivně	k6eAd1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
stavební	stavební	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
a	a	k8xC
paleontolog	paleontolog	k1gMnSc1
Joachim	Joachim	k1gMnSc1
Barrande	Barrand	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantou	dominanta	k1gFnSc7
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
křivoklátsko-rokycanské	křivoklátsko-rokycanský	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
vyvřelých	vyvřelý	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
od	od	k7c2
Zbečna	Zbečno	k1gNnSc2
k	k	k7c3
Rokycanům	Rokycany	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
projevy	projev	k1gInPc1
na	na	k7c6
povrchu	povrch	k1gInSc6
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgInPc1d1
v	v	k7c6
podobě	podoba	k1gFnSc6
erozí	eroze	k1gFnPc2
odhalených	odhalený	k2eAgInPc2d1
tvrdých	tvrdý	k2eAgInPc2d1
skalních	skalní	k2eAgInPc2d1
suků	suk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úzké	úzký	k2eAgInPc1d1
hřbety	hřbet	k1gInPc1
při	při	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgInP
ordovickými	ordovický	k2eAgInPc7d1
křemenci	křemenec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgFnSc1d2
horninotvorná	horninotvorný	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
a	a	k8xC
jen	jen	k6eAd1
lokální	lokální	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
údolí	údolí	k1gNnSc6
Berounky	Berounka	k1gFnSc2
jsou	být	k5eAaImIp3nP
říční	říční	k2eAgFnPc1d1
terasy	terasa	k1gFnPc1
naplavených	naplavený	k2eAgInPc2d1
štěrků	štěrk	k1gInPc2
a	a	k8xC
písků	písek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
třetihor	třetihory	k1gFnPc2
(	(	kIx(
<g/>
terciéru	terciér	k1gInSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
povrch	povrch	k1gInSc1
území	území	k1gNnPc2
srovnán	srovnat	k5eAaPmNgInS
do	do	k7c2
paroviny	parovina	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
většina	většina	k1gFnSc1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prudké	Prudké	k2eAgInPc4d1
klimatické	klimatický	k2eAgInPc4d1
výkyvy	výkyv	k1gInPc4
čtvrtohor	čtvrtohory	k1gFnPc2
však	však	k8xC
způsobily	způsobit	k5eAaPmAgInP
masivní	masivní	k2eAgFnSc4d1
erozi	eroze	k1gFnSc4
a	a	k8xC
holá	holý	k2eAgFnSc1d1
mrazová	mrazový	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
dob	doba	k1gFnPc2
ledových	ledový	k2eAgFnPc2d1
snadno	snadno	k6eAd1
podléhala	podléhat	k5eAaImAgFnS
vlivu	vliv	k1gInSc2
vody	voda	k1gFnSc2
i	i	k8xC
větru	vítr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říční	říční	k2eAgInPc1d1
toky	tok	k1gInPc1
se	se	k3xPyFc4
hluboce	hluboko	k6eAd1
zařízly	zaříznout	k5eAaPmAgFnP
i	i	k9
do	do	k7c2
tvrdých	tvrdý	k2eAgFnPc2d1
vyvřelých	vyvřelý	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
meziledové	meziledový	k2eAgFnPc1d1
se	se	k3xPyFc4
lokálně	lokálně	k6eAd1
vytvořila	vytvořit	k5eAaPmAgFnS
ložiska	ložisko	k1gNnSc2
pěnovců	pěnovec	k1gInPc2
(	(	kIx(
<g/>
sypký	sypký	k2eAgInSc1d1
travertin	travertin	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
svazích	svah	k1gInPc6
pramenů	pramen	k1gInPc2
bohatých	bohatý	k2eAgInPc2d1
na	na	k7c4
uhličitan	uhličitan	k1gInSc4
vápenatý	vápenatý	k2eAgInSc4d1
(	(	kIx(
<g/>
CaCO	CaCO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzorovou	vzorový	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
pěnovce	pěnovka	k1gFnSc3
je	on	k3xPp3gFnPc4
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
U	u	k7c2
Eremita	eremit	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
neobyčejná	obyčejný	k2eNgFnSc1d1
krása	krása	k1gFnSc1
křivoklátských	křivoklátský	k2eAgInPc2d1
lesů	les	k1gInPc2
</s>
<s>
Půdy	půda	k1gFnPc1
v	v	k7c6
celé	celá	k1gFnSc6
CHKO	CHKO	kA
odpovídají	odpovídat	k5eAaImIp3nP
vývojové	vývojový	k2eAgFnSc3d1
sérii	série	k1gFnSc3
hnědých	hnědý	k2eAgFnPc2d1
půd	půda	k1gFnPc2
(	(	kIx(
<g/>
kambizemí	kambizemí	k1gNnSc2
<g/>
)	)	kIx)
v	v	k7c6
různém	různý	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
živiny	živina	k1gFnPc4
jsou	být	k5eAaImIp3nP
spíše	spíše	k9
chudší	chudý	k2eAgMnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půda	půda	k1gFnSc1
na	na	k7c6
břidlicovém	břidlicový	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
bývá	bývat	k5eAaImIp3nS
špatně	špatně	k6eAd1
provzdušněná	provzdušněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
srážek	srážka	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
zbytkem	zbytek	k1gInSc7
republiky	republika	k1gFnSc2
mírné	mírný	k2eAgFnSc2d1
podprůměrné	podprůměrný	k2eAgFnSc2d1
<g/>
,	,	kIx,
protože	protože	k8xS
Křivoklátsko	Křivoklátsko	k1gNnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
okraji	okraj	k1gInSc6
srážkového	srážkový	k2eAgInSc2d1
stínu	stín	k1gInSc2
Krkonoš	Krkonoše	k1gFnPc2
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
v	v	k7c6
podzemí	podzemí	k1gNnSc6
neexistují	existovat	k5eNaImIp3nP
kolektory	kolektor	k1gInPc1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
podzemních	podzemní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výskyt	výskyt	k1gInSc1
je	být	k5eAaImIp3nS
vázán	vázat	k5eAaImNgInS
na	na	k7c4
zóny	zóna	k1gFnPc4
prasklin	prasklina	k1gFnPc2
v	v	k7c6
podloží	podloží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hydrologickou	hydrologický	k2eAgFnSc7d1
dominantou	dominanta	k1gFnSc7
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Berounka	Berounka	k1gFnSc1
s	s	k7c7
četnými	četný	k2eAgInPc7d1
přítoky	přítok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
v	v	k7c6
Křivoklátu	Křivoklát	k1gInSc2
činí	činit	k5eAaImIp3nS
32	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
při	při	k7c6
povodni	povodeň	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
však	však	k9
přesáhl	přesáhnout	k5eAaPmAgMnS
1400	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Na	na	k7c6
ploše	plocha	k1gFnSc6
CHKO	CHKO	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
340	#num#	k4
vodních	vodní	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
vodní	vodní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Jablečno	Jablečno	k1gNnSc1
<g/>
,	,	kIx,
Bartoň	Bartoň	k1gMnSc1
a	a	k8xC
Pilský	Pilský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
polovinu	polovina	k1gFnSc4
celkové	celkový	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
stojatých	stojatý	k2eAgFnPc2d1
vod	voda	k1gFnPc2
zaujímá	zaujímat	k5eAaImIp3nS
v.	v.	k?
n.	n.	k?
Klíčava	Klíčava	k1gFnSc1
(	(	kIx(
<g/>
72,5	72,5	k4
ha	ha	kA
<g/>
)	)	kIx)
na	na	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
říčce	říčka	k1gFnSc6
v	v	k7c6
Lánské	Lánské	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
netrpí	trpět	k5eNaImIp3nP
acidifikací	acidifikace	k1gFnSc7
díky	díky	k7c3
geologickému	geologický	k2eAgNnSc3d1
podloží	podloží	k1gNnSc3
a	a	k8xC
listnatým	listnatý	k2eAgInPc3d1
lesům	les	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klima	klima	k1gNnSc1
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
suché	suchý	k2eAgNnSc1d1
a	a	k8xC
mírně	mírně	k6eAd1
teplé	teplý	k2eAgNnSc1d1
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
roční	roční	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
úhrnem	úhrn	k1gInSc7
srážek	srážka	k1gFnPc2
530	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněhu	sníh	k1gInSc2
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
zimě	zima	k1gFnSc6
do	do	k7c2
20	#num#	k4
cm	cm	kA
a	a	k8xC
nevydrží	vydržet	k5eNaPmIp3nP
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
okolí	okolí	k1gNnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
svým	svůj	k3xOyFgInSc7
velmi	velmi	k6eAd1
členitým	členitý	k2eAgInSc7d1
terénem	terén	k1gInSc7
velkou	velký	k2eAgFnSc4d1
diverzitu	diverzita	k1gFnSc4
stanovišť	stanoviště	k1gNnPc2
a	a	k8xC
mikroklimat	mikroklima	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údolí	údolí	k1gNnPc1
potoků	potok	k1gInPc2
jsou	být	k5eAaImIp3nP
vlhká	vlhký	k2eAgNnPc1d1
a	a	k8xC
chladná	chladný	k2eAgNnPc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
stráně	stráň	k1gFnPc1
a	a	k8xC
vrcholy	vrchol	k1gInPc1
kopců	kopec	k1gInPc2
jsou	být	k5eAaImIp3nP
podstatně	podstatně	k6eAd1
teplejší	teplý	k2eAgFnPc1d2
a	a	k8xC
sušší	suchý	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejich	jejich	k3xOp3gNnPc6
jihozápadních	jihozápadní	k2eAgNnPc6d1
temenech	temeno	k1gNnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc1d1
bezlesí	bezlesí	k1gNnSc1
s	s	k7c7
keři	keř	k1gInPc7
a	a	k8xC
zakrslými	zakrslý	k2eAgInPc7d1
duby	dub	k1gInPc7
<g/>
,	,	kIx,
označované	označovaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
pleš	pleš	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
inverzi	inverze	k1gFnSc4
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
zvlněná	zvlněný	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
křivoklátské	křivoklátský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
Většina	většina	k1gFnSc1
území	území	k1gNnSc2
se	se	k3xPyFc4
překrývá	překrývat	k5eAaImIp3nS
s	s	k7c7
geomorfologickým	geomorfologický	k2eAgInSc7d1
celkem	celek	k1gInSc7
Křivoklátské	křivoklátský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východě	východ	k1gInSc6
sem	sem	k6eAd1
pak	pak	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
Plaská	plaskat	k5eAaImIp3nS
pahorkatina	pahorkatina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lánská	lánský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Berounky	Berounka	k1gFnSc2
má	mít	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc2d2
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc1
Tuchonín	Tuchonína	k1gFnPc2
(	(	kIx(
<g/>
487	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc4
geologické	geologický	k2eAgNnSc4d1
podloží	podloží	k1gNnSc4
jsou	být	k5eAaImIp3nP
sedimentované	sedimentovaný	k2eAgFnPc1d1
břidlice	břidlice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
předurčují	předurčovat	k5eAaImIp3nP
krajinu	krajina	k1gFnSc4
k	k	k7c3
zaobleným	zaoblený	k2eAgInPc3d1
tvarům	tvar	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
ovšem	ovšem	k9
hluboce	hluboko	k6eAd1
zařezávají	zařezávat	k5eAaImIp3nP
potoky	potok	k1gInPc1
tekoucí	tekoucí	k2eAgInPc1d1
k	k	k7c3
Berounce	Berounka	k1gFnSc3
(	(	kIx(
<g/>
Rakovnický	rakovnický	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
Javornice	Javornice	k1gFnSc1
<g/>
,	,	kIx,
Tyterský	Tyterský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
Klíčava	Klíčava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
se	se	k3xPyFc4
na	na	k7c6
vyvřelém	vyvřelý	k2eAgNnSc6d1
křivoklátsko-rokycanském	křivoklátsko-rokycanský	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
rozkládá	rozkládat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
a	a	k8xC
členitější	členitý	k2eAgFnSc1d2
Zbirožská	zbirožský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
Těchovín	Těchovína	k1gFnPc2
(	(	kIx(
<g/>
616	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
významu	význam	k1gInSc2
jádrem	jádro	k1gNnSc7
celé	celý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraznými	výrazný	k2eAgInPc7d1
prvky	prvek	k1gInPc7
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
vymodelované	vymodelovaný	k2eAgInPc4d1
meandry	meandr	k1gInPc4
Berounky	Berounka	k1gFnSc2
a	a	k8xC
četné	četný	k2eAgInPc1d1
buližníkové	buližníkový	k2eAgInPc1d1
skalní	skalní	k2eAgInPc1d1
výchozy	výchoz	k1gInPc1
(	(	kIx(
<g/>
Krušná	krušný	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Velíz	Velíz	k1gInSc1
<g/>
,	,	kIx,
Zámecký	zámecký	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
svažitý	svažitý	k2eAgInSc1d1
terén	terén	k1gInSc1
v	v	k7c6
údolích	údolí	k1gNnPc6
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
Dlouholetým	dlouholetý	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
pod	pod	k7c7
vedením	vedení	k1gNnSc7
RNDr.	RNDr.	kA
J.	J.	kA
Kolbeka	Kolbeka	k1gFnSc1
byla	být	k5eAaImAgFnS
prokázána	prokázat	k5eAaPmNgFnS
přítomnost	přítomnost	k1gFnSc1
1800	#num#	k4
rostlinných	rostlinný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
činí	činit	k5eAaImIp3nS
60	#num#	k4
%	%	kIx~
celkového	celkový	k2eAgNnSc2d1
druhového	druhový	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
celé	celý	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tak	tak	k6eAd1
nebývalou	bývalý	k2eNgFnSc4d1,k2eAgFnSc4d1
diverzitu	diverzita	k1gFnSc4
vděčí	vděčit	k5eAaImIp3nS
Křivoklátsko	Křivoklátsko	k1gNnSc1
zejména	zejména	k6eAd1
velkému	velký	k2eAgInSc3d1
gradientu	gradient	k1gInSc3
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nabízí	nabízet	k5eAaImIp3nS
mnoho	mnoho	k4c4
typů	typ	k1gInPc2
přírodních	přírodní	k2eAgNnPc2d1
stanovišť	stanoviště	k1gNnPc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
menším	malý	k2eAgNnSc7d2
narušením	narušení	k1gNnSc7
lidskou	lidský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
.	.	kIx.
62	#num#	k4
procentní	procentní	k2eAgFnSc4d1
lesnatost	lesnatost	k1gFnSc4
je	být	k5eAaImIp3nS
vysoko	vysoko	k6eAd1
nad	nad	k7c7
celostátním	celostátní	k2eAgInSc7d1
průměrem	průměr	k1gInSc7
<g/>
,	,	kIx,
lesy	les	k1gInPc1
mají	mít	k5eAaImIp3nP
navíc	navíc	k6eAd1
na	na	k7c6
většině	většina	k1gFnSc6
své	svůj	k3xOyFgFnSc2
rozlohy	rozloha	k1gFnSc2
zachovanou	zachovaný	k2eAgFnSc4d1
příznivou	příznivý	k2eAgFnSc4d1
druhovou	druhový	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
s	s	k7c7
velkým	velký	k2eAgInSc7d1
podílem	podíl	k1gInSc7
listnatých	listnatý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
vlivu	vliv	k1gInSc2
člověka	člověk	k1gMnSc2
by	by	kYmCp3nS
podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
oblasti	oblast	k1gFnSc2
dominovala	dominovat	k5eAaImAgFnS
černýšová	černýšový	k2eAgFnSc1d1
dubohabřina	dubohabřina	k1gFnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
biková	bikový	k2eAgFnSc1d1
a	a	k8xC
jedlová	jedlový	k2eAgFnSc1d1
doubrava	doubrava	k1gFnSc1
a	a	k8xC
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
a	a	k8xC
severních	severní	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
biková	bikovat	k5eAaPmIp3nS,k5eAaImIp3nS
nebo	nebo	k8xC
lipová	lipový	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
lokální	lokální	k2eAgInPc4d1
porosty	porost	k1gInPc4
jedlin	jedlina	k1gFnPc2
a	a	k8xC
svahy	svah	k1gInPc4
s	s	k7c7
tisem	tis	k1gInSc7
červeným	červený	k2eAgInSc7d1
(	(	kIx(
<g/>
Taxus	Taxus	k1gInSc1
baccata	baccat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vratička	vratička	k1gFnSc1
měsíční	měsíční	k2eAgFnSc1d1
</s>
<s>
Na	na	k7c6
vrcholových	vrcholový	k2eAgFnPc6d1
partiích	partie	k1gFnPc6
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
reliktně	reliktně	k6eAd1
rozšířeným	rozšířený	k2eAgInPc3d1
teplomilným	teplomilný	k2eAgInPc3d1
druhům	druh	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštností	zvláštnost	k1gFnPc2
jsou	být	k5eAaImIp3nP
zakrslé	zakrslý	k2eAgInPc1d1
duby	dub	k1gInPc1
zimní	zimní	k2eAgInPc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
patraea	patrae	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
bylin	bylina	k1gFnPc2
např.	např.	kA
třemdava	třemdava	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dictamnus	Dictamnus	k1gMnSc1
albus	albus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koniklec	koniklec	k1gInSc4
luční	luční	k2eAgInSc4d1
(	(	kIx(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1
pratensis	pratensis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chrpa	chrpa	k1gFnSc1
chlumní	chlumní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cyanus	Cyanus	k1gMnSc1
triumfetti	triumfetť	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
kavyl	kavyl	k1gInSc1
Ivanův	Ivanův	k2eAgInSc1d1
(	(	kIx(
<g/>
Stipa	Stipa	k1gFnSc1
joannis	joannis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
skalách	skála	k1gFnPc6
roste	růst	k5eAaImIp3nS
tařice	tařice	k1gFnSc1
skalní	skalní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Aurinia	Aurinium	k1gNnPc1
saxatilis	saxatilis	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
lomikámen	lomikámen	k1gInSc1
vždyživý	vždyživý	k2eAgInSc1d1
(	(	kIx(
<g/>
Saxifragapaniculata	Saxifragapanicule	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svažitém	svažitý	k2eAgInSc6d1
terénu	terén	k1gInSc6
najdeme	najít	k5eAaPmIp1nP
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
suťových	suťový	k2eAgInPc2d1
lesů	les	k1gInPc2
s	s	k7c7
přirozeným	přirozený	k2eAgInSc7d1
výskytem	výskyt	k1gInSc7
konopičky	konopička	k1gFnSc2
úzkolisté	úzkolistý	k2eAgNnSc4d1
(	(	kIx(
<g/>
Dalanum	Dalanum	k1gNnSc4
angustifolium	angustifolium	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
před	před	k7c7
olistěním	olistění	k1gNnSc7
stromů	strom	k1gInPc2
rozkvetou	rozkvést	k5eAaPmIp3nP
tyto	tento	k3xDgInPc1
lesy	les	k1gInPc1
celými	celá	k1gFnPc7
koberci	koberec	k1gInSc6
tří	tři	k4xCgInPc2
druhů	druh	k1gInPc2
běžných	běžný	k2eAgFnPc2d1
sasanek	sasanka	k1gFnPc2
(	(	kIx(
<g/>
anemone	anemon	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bledule	bledule	k1gFnSc1
jarní	jarní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leucojum	Leucojum	k1gInSc1
vernum	vernum	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgInPc2d1
časných	časný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nivách	niva	k1gFnPc6
potoků	potok	k1gInPc2
jsou	být	k5eAaImIp3nP
od	od	k7c2
baroka	baroko	k1gNnSc2
obhospodařované	obhospodařovaný	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
luk	louka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
chráněné	chráněný	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
z	z	k7c2
Červeného	Červeného	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
ČR	ČR	kA
se	se	k3xPyFc4
počítají	počítat	k5eAaImIp3nP
na	na	k7c4
desítky	desítka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenovat	jmenovat	k5eAaBmF,k5eAaImF
lze	lze	k6eAd1
kriticky	kriticky	k6eAd1
ohroženou	ohrožený	k2eAgFnSc4d1
kapradinu	kapradina	k1gFnSc4
vratičku	vratička	k1gFnSc4
měsíční	měsíční	k2eAgMnSc1d1
(	(	kIx(
<g/>
Botrichium	Botrichium	k1gNnSc1
matricariifolia	matricariifolium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
výskyt	výskyt	k1gInSc1
je	být	k5eAaImIp3nS
omezen	omezen	k2eAgInSc1d1
na	na	k7c4
několik	několik	k4yIc4
oddělených	oddělený	k2eAgFnPc2d1
populací	populace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
rostlin	rostlina	k1gFnPc2
vrcholových	vrcholový	k2eAgFnPc2d1
pleší	pleš	k1gFnPc2
jmenujme	jmenovat	k5eAaBmRp1nP,k5eAaImRp1nP
drobnou	drobný	k2eAgFnSc4d1
kapradinku	kapradinka	k1gFnSc4
skalní	skalní	k2eAgInPc1d1
(	(	kIx(
<g/>
Woodsia	Woodsia	k1gFnSc1
ilvensis	ilvensis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
koniklec	koniklec	k1gInSc1
luční	luční	k2eAgInSc1d1
český	český	k2eAgInSc1d1
(	(	kIx(
<g/>
Pulsatilla	Pulsatilla	k1gMnSc1
pratensis	pratensis	k1gFnSc2
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
bohemica	bohemica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
lokalitě	lokalita	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
hořeček	hořeček	k1gInSc1
ladní	ladní	k2eAgInSc1d1
pobaltský	pobaltský	k2eAgInSc1d1
(	(	kIx(
<g/>
Gentianella	Gentianella	k1gMnSc1
campestris	campestris	k1gFnSc2
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
baltica	baltica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
česnek	česnek	k1gInSc1
tuhý	tuhý	k2eAgInSc1d1
(	(	kIx(
<g/>
Allium	Allium	k1gNnSc1
strictum	strictum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vstavač	vstavač	k1gInSc4
nachový	nachový	k2eAgInSc4d1
(	(	kIx(
<g/>
Orchis	Orchis	k1gInSc4
purpurea	purpure	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vstavač	vstavač	k1gInSc4
osmahlý	osmahlý	k2eAgInSc4d1
(	(	kIx(
<g/>
Orchis	Orchis	k1gInSc4
ustulata	ustule	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kruštík	kruštík	k1gInSc4
Greuterův	Greuterův	k2eAgInSc4d1
(	(	kIx(
<g/>
Epipactis	Epipactis	k1gInSc4
greuterii	greuterie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vlhkomilná	vlhkomilný	k2eAgFnSc1d1
ostřice	ostřice	k1gFnSc1
Davallova	Davallův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
davallina	davallina	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
světlomilný	světlomilný	k2eAgInSc1d1
hvozdík	hvozdík	k1gInSc1
pyšný	pyšný	k2eAgInSc1d1
(	(	kIx(
<g/>
Dianthus	Dianthus	k1gInSc1
superbus	superbus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hvozdík	hvozdík	k1gInSc1
pyšný	pyšný	k2eAgInSc1d1
</s>
<s>
Na	na	k7c6
pokraji	pokraj	k1gInSc6
vymření	vymření	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
hořec	hořec	k1gInSc1
hořepník	hořepník	k1gInSc1
(	(	kIx(
<g/>
Gentiana	Gentiana	k1gFnSc1
pneumonanthe	pneumonanth	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
nesvědčí	svědčit	k5eNaImIp3nS
změna	změna	k1gFnSc1
obhospodařování	obhospodařování	k1gNnSc2
luk	louka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgInPc1d1
listnaté	listnatý	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
velká	velký	k2eAgFnSc1d1
diverzita	diverzita	k1gFnSc1
stanovišť	stanoviště	k1gNnPc2
skýtají	skýtat	k5eAaImIp3nP
dobré	dobrý	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
výskyt	výskyt	k1gInSc4
mnoha	mnoho	k4c2
druhů	druh	k1gInPc2
měkkýšů	měkkýš	k1gMnPc2
<g/>
,	,	kIx,
hmyzu	hmyz	k1gInSc2
<g/>
,	,	kIx,
pavoukovců	pavoukovec	k1gMnPc2
i	i	k8xC
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
najdeme	najít	k5eAaPmIp1nP
v	v	k7c6
CHKO	CHKO	kA
24	#num#	k4
kriticky	kriticky	k6eAd1
ohrožených	ohrožený	k2eAgMnPc2d1
<g/>
,	,	kIx,
60	#num#	k4
silně	silně	k6eAd1
ohrožených	ohrožený	k2eAgMnPc2d1
a	a	k8xC
60	#num#	k4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Stepník	Stepník	k1gInSc1
černonohý	černonohý	k2eAgInSc1d1
</s>
<s>
Bezobratlí	bezobratlí	k1gMnPc1
<g/>
:	:	kIx,
Na	na	k7c6
skalních	skalní	k2eAgInPc6d1
a	a	k8xC
stepních	stepní	k2eAgInPc6d1
biotopech	biotop	k1gInPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
měkkýši	měkkýš	k1gMnPc1
Vertigo	Vertigo	k6eAd1
alpestris	alpestris	k1gFnSc4
<g/>
,	,	kIx,
Pupilla	Pupilla	k1gFnSc1
triplicata	triplicata	k1gFnSc1
a	a	k8xC
vřetenatka	vřetenatka	k1gFnSc1
česká	český	k2eAgFnSc1d1
(	(	kIx(
<g/>
Alinda	Alinda	k1gFnSc1
biplicata	biplicata	k1gFnSc1
bohemica	bohemica	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pavouci	pavouk	k1gMnPc1
skálovka	skálovka	k1gFnSc1
(	(	kIx(
<g/>
Zetoles	Zetoles	k1gMnSc1
puritanus	puritanus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sklípkánek	sklípkánek	k1gMnSc1
(	(	kIx(
<g/>
Atypus	Atypus	k1gMnSc1
affinis	affinis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
stepník	stepník	k1gMnSc1
(	(	kIx(
<g/>
Eresus	Eresus	k1gMnSc1
sandaliatus	sandaliatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
brouků	brouk	k1gMnPc2
několik	několik	k4yIc4
druhů	druh	k1gInPc2
mandelinek	mandelinka	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Chrysolina	Chrysolina	k1gFnSc1
aurichalcea	aurichalcea	k1gFnSc1
<g/>
,	,	kIx,
dřepčík	dřepčík	k1gMnSc1
Dibolia	Dibolius	k1gMnSc4
cynoglossi	cynoglosse	k1gFnSc6
<g/>
,	,	kIx,
krytohlavové	krytohlavový	k2eAgFnSc6d1
<g/>
,	,	kIx,
např.	např.	kA
Cryptocephalus	Cryptocephalus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
a	a	k8xC
vzácně	vzácně	k6eAd1
vruboun	vruboun	k1gMnSc1
Sisyphus	Sisyphus	k1gMnSc1
schaefferi	schaefferi	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lánské	Lánské	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
je	být	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1
známá	známý	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
krasečka	krasečko	k1gNnSc2
Aphanisticus	Aphanisticus	k1gMnSc1
pusillus	pusillus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
bezobratlých	bezobratlí	k1gMnPc2
jsou	být	k5eAaImIp3nP
vázány	vázán	k2eAgFnPc1d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
potravně	potravně	k6eAd1
<g/>
,	,	kIx,
na	na	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
rostlinu	rostlina	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
nosatci	nosatec	k1gMnPc1
na	na	k7c4
tařici	tařice	k1gFnSc4
skalní	skalní	k2eAgFnPc1d1
a	a	k8xC
nosateček	nosateček	k1gInSc1
(	(	kIx(
<g/>
Oprohinus	Oprohinus	k1gInSc1
suturalis	suturalis	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c4
česnek	česnek	k1gInSc4
chlumní	chlumní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stepích	step	k1gFnPc6
můžeme	moct	k5eAaImIp1nP
spatřit	spatřit	k5eAaPmF
motýly	motýl	k1gMnPc4
okáče	okáč	k1gMnSc2
skalního	skalní	k2eAgMnSc2d1
(	(	kIx(
<g/>
Chazara	Chazar	k1gMnSc2
briseis	briseis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bělopáska	bělopásek	k1gMnSc2
dvouřadého	dvouřadý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Limenitis	Limenitis	k1gFnSc1
camilla	camilla	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
můru	můra	k1gFnSc4
Staurophora	Staurophor	k1gMnSc2
celsia	celsius	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
jinak	jinak	k6eAd1
známe	znát	k5eAaImIp1nP
jen	jen	k9
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kamení	kamení	k1gNnSc6
na	na	k7c6
suťových	suťový	k2eAgInPc6d1
svazích	svah	k1gInPc6
se	se	k3xPyFc4
reliktně	reliktně	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
vzácné	vzácný	k2eAgInPc1d1
druhy	druh	k1gInPc1
sekáčů	sekáč	k1gMnPc2
–	–	k?
žláznatka	žláznatka	k1gFnSc1
Nemastoma	Nemastoma	k1gFnSc1
triste	trisit	k5eAaImRp2nP,k5eAaPmRp2nP
a	a	k8xC
klepítník	klepítník	k1gInSc4
členěný	členěný	k2eAgInSc4d1
(	(	kIx(
<g/>
Ischyropsalis	Ischyropsalis	k1gInSc4
hellwigi	hellwig	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesy	les	k1gInPc7
jsou	být	k5eAaImIp3nP
domovem	domov	k1gInSc7
137	#num#	k4
ze	z	k7c2
184	#num#	k4
druhů	druh	k1gInPc2
tesaříkovitých	tesaříkovitý	k2eAgMnPc2d1
brouků	brouk	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
tesaříka	tesařík	k1gMnSc2
obrovského	obrovský	k2eAgMnSc2d1
(	(	kIx(
<g/>
Cerambyx	Cerambyx	k1gInSc1
cerdo	cerdo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
t.	t.	k?
broskvoňového	broskvoňový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Purpuricenus	Purpuricenus	k1gInSc1
kaehleri	kaehler	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
střevlíků	střevlík	k1gMnPc2
Licinus	Licinus	k1gMnSc1
hoffmannsegi	hoffmannseg	k1gFnSc2
a	a	k8xC
Cychrus	Cychrus	k1gMnSc1
attentuatus	attentuatus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existence	existence	k1gFnSc1
mnoha	mnoho	k4c2
starých	starý	k2eAgInPc2d1
stromů	strom	k1gInPc2
umožňuje	umožňovat	k5eAaImIp3nS
přítomnost	přítomnost	k1gFnSc1
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
na	na	k7c4
ně	on	k3xPp3gMnPc4
vázaných	vázaný	k2eAgNnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ně	on	k3xPp3gNnSc4
patří	patřit	k5eAaImIp3nP
kovaříci	kovařík	k1gMnPc1
Hemicrepidius	Hemicrepidius	k1gMnSc1
mutilatus	mutilatus	k1gMnSc1
a	a	k8xC
Megapenthes	Megapenthes	k1gMnSc1
lugens	lugens	k1gInSc1
<g/>
,	,	kIx,
páchník	páchník	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Osmoderma	Osmoderm	k1gMnSc4
eremita	eremit	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
brouci	brouk	k1gMnPc1
čeledi	čeleď	k1gFnSc2
Lycidae	Lycidae	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
povrch	povrch	k6eAd1wR
kmenů	kmen	k1gInPc2
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
vázáni	vázat	k5eAaImNgMnP
např.	např.	kA
mandelinky	mandelinka	k1gFnSc2
Lachnaia	Lachnaius	k1gMnSc4
sexpunctata	sexpunctat	k1gMnSc4
a	a	k8xC
Pachybrachis	Pachybrachis	k1gFnSc4
picus	picus	k1gInSc4
a	a	k8xC
z	z	k7c2
pavouků	pavouk	k1gMnPc2
Atyphaena	Atyphaen	k1gMnSc4
furva	furv	k1gMnSc4
a	a	k8xC
Cetonana	Cetonan	k1gMnSc4
laticeps	laticeps	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listnatých	listnatý	k2eAgInPc6d1
lesích	les	k1gInPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
roháč	roháč	k1gInSc1
obrovský	obrovský	k2eAgInSc1d1
(	(	kIx(
<g/>
Lucanus	Lucanus	k1gInSc1
servus	servus	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
motýlů	motýl	k1gMnPc2
můžeme	moct	k5eAaImIp1nP
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
ohroženého	ohrožený	k2eAgMnSc4d1
batolce	batolec	k1gMnSc4
duhového	duhový	k2eAgInSc2d1
<g/>
(	(	kIx(
<g/>
Apatura	Apatura	k1gFnSc1
iris	iris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
b.	b.	k?
červeného	červené	k1gNnSc2
(	(	kIx(
<g/>
A.	A.	kA
ilia	ilia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nočních	noční	k2eAgFnPc2d1
pak	pak	k9
strakáče	strakáč	k1gMnSc2
březového	březový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Endromis	Endromis	k1gFnSc1
versicolora	versicolora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hřbetozubce	hřbetozubce	k1gMnSc1
Odontosia	Odontosia	k1gFnSc1
carmelita	carmelita	k1gFnSc1
a	a	k8xC
různorožce	různorožka	k1gFnSc3
černopásého	černopásý	k2eAgNnSc2d1
(	(	kIx(
<g/>
Fagivorina	Fagivorin	k2eAgNnSc2d1
arenaria	arenarium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhově	druhově	k6eAd1
bohaté	bohatý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
lužní	lužní	k2eAgInPc1d1
a	a	k8xC
vodní	vodní	k2eAgInPc1d1
biotopy	biotop	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Klíčavském	Klíčavský	k2eAgInSc6d1
potoce	potok	k1gInSc6
byl	být	k5eAaImAgMnS
objeven	objeven	k2eAgMnSc1d1
velevrub	velevrub	k1gMnSc1
tupý	tupý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Unio	Unio	k1gMnSc1
crassus	crassus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
korýšů	korýš	k1gMnPc2
lze	lze	k6eAd1
v	v	k7c6
čistých	čistý	k2eAgInPc6d1
potocích	potok	k1gInPc6
nalézt	nalézt	k5eAaBmF,k5eAaPmF
rak	rak	k1gMnSc1
kamenáč	kamenáč	k1gMnSc1
(	(	kIx(
<g/>
Austropotamobilus	Austropotamobilus	k1gInSc1
torrentium	torrentium	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
r.	r.	kA
říční	říční	k2eAgMnSc1d1
(	(	kIx(
<g/>
Astacus	Astacus	k1gMnSc1
astacus	astacus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
stojatých	stojatý	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
rak	rak	k1gMnSc1
bahenní	bahenní	k2eAgMnSc1d1
(	(	kIx(
<g/>
A.	A.	kA
leptodactylus	leptodactylus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výr	výr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
hnízdí	hnízdit	k5eAaImIp3nS
na	na	k7c6
skalních	skalní	k2eAgFnPc6d1
stěnách	stěna	k1gFnPc6
</s>
<s>
Obratlovci	obratlovec	k1gMnPc1
<g/>
:	:	kIx,
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
význačná	význačný	k2eAgFnSc1d1
přítomností	přítomnost	k1gFnSc7
mnoha	mnoho	k4c2
ohrožených	ohrožený	k2eAgMnPc2d1
a	a	k8xC
málo	málo	k6eAd1
častých	častý	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
význačných	význačný	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
žijí	žít	k5eAaImIp3nP
v	v	k7c6
některých	některý	k3yIgInPc6
potocích	potok	k1gInPc6
vranka	vranka	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cottus	Cottus	k1gMnSc1
gobio	gobio	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
pstruh	pstruh	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
potoční	potoční	k2eAgFnSc4d1
(	(	kIx(
<g/>
Salmo	Salma	k1gFnSc5
trutta	trutta	k1gMnSc1
m.	m.	k?
fario	fario	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tůňky	tůňka	k1gFnPc1
<g/>
,	,	kIx,
mokřady	mokřad	k1gInPc1
a	a	k8xC
potoky	potok	k1gInPc1
skýtají	skýtat	k5eAaImIp3nP
životní	životní	k2eAgInSc4d1
prostor	prostor	k1gInSc4
mnoha	mnoho	k4c3
zajímavým	zajímavý	k2eAgMnPc3d1
obojživelníkům	obojživelník	k1gMnPc3
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
silně	silně	k6eAd1
nebo	nebo	k8xC
kriticky	kriticky	k6eAd1
ohroženým	ohrožený	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
jmenujme	jmenovat	k5eAaBmRp1nP,k5eAaImRp1nP
především	především	k9
krásného	krásný	k2eAgMnSc2d1
mloka	mlok	k1gMnSc2
skvrnitého	skvrnitý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Salamandra	salamandr	k1gMnSc2
salamandra	salamandr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
větších	veliký	k2eAgFnPc6d2
tůních	tůně	k1gFnPc6
čolka	čolek	k1gMnSc2
horského	horský	k2eAgMnSc2d1
(	(	kIx(
<g/>
Triturus	Triturus	k1gInSc1
alpestris	alpestris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
č.	č.	k?
velkého	velký	k2eAgNnSc2d1
(	(	kIx(
<g/>
T.	T.	kA
Cristatus	Cristatus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
žáby	žába	k1gFnPc1
kuňku	kuňka	k1gFnSc4
žlutobřichou	žlutobřichý	k2eAgFnSc4d1
(	(	kIx(
<g/>
Bombina	Bombin	k2eAgMnSc4d1
variegata	variegat	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
k.	k.	k?
obecou	obecá	k1gFnSc4
(	(	kIx(
<g/>
B.	B.	kA
Bombina	Bombina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
teplých	teplý	k2eAgFnPc6d1
stráních	stráň	k1gFnPc6
žije	žít	k5eAaImIp3nS
ještěrka	ještěrka	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
viridis	viridis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
užovka	užovka	k1gFnSc1
hladká	hladký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Coronella	Coronella	k1gFnSc1
austriaca	austriaca	k1gFnSc1
<g/>
)	)	kIx)
i	i	k9
zmije	zmije	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Vipera	Vipera	k1gFnSc1
berus	berus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
běžná	běžný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c6
Křivoklátsku	Křivoklátsek	k1gInSc6
i	i	k9
užovka	užovka	k1gFnSc1
podplamatá	podplamatá	k1gFnSc1
(	(	kIx(
<g/>
Natrix	Natrix	k1gInSc1
tessellata	tesselle	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skalní	skalní	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
skýtají	skýtat	k5eAaImIp3nP
hnízdiště	hnízdiště	k1gNnSc2
největší	veliký	k2eAgFnSc6d3
sově	sova	k1gFnSc6
vůbec	vůbec	k9
<g/>
:	:	kIx,
výru	výr	k1gMnSc3
velkému	velký	k2eAgMnSc3d1
(	(	kIx(
<g/>
Bubo	Bubo	k1gMnSc1
bubo	bubo	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listnatých	listnatý	k2eAgInPc6d1
lesích	les	k1gInPc6
hnízdí	hnízdit	k5eAaImIp3nS
velký	velký	k2eAgMnSc1d1
dravec	dravec	k1gMnSc1
luňák	luňák	k1gMnSc1
červený	červený	k2eAgMnSc1d1
(	(	kIx(
<g/>
Milvus	Milvus	k1gMnSc1
milvus	milvus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hnízdy	hnízdo	k1gNnPc7
sociálního	sociální	k2eAgInSc2d1
hmyzu	hmyz	k1gInSc2
se	se	k3xPyFc4
živící	živící	k2eAgMnSc1d1
včelojed	včelojed	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pernis	Pernis	k1gInSc1
apivorus	apivorus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skrytě	skrytě	k6eAd1
žijící	žijící	k2eAgMnSc1d1
jestřáb	jestřáb	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Accipiter	Accipiter	k1gInSc1
gentilis	gentilis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
krahujec	krahujec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Accipiter	Accipiter	k1gMnSc1
nisus	nisus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výskyt	výskyt	k1gInSc4
velkých	velký	k2eAgInPc2d1
a	a	k8xC
starých	starý	k2eAgInPc2d1
stromů	strom	k1gInPc2
umožňuje	umožňovat	k5eAaImIp3nS
přítomnost	přítomnost	k1gFnSc1
datla	datel	k1gMnSc2
černého	černý	k1gMnSc2
(	(	kIx(
<g/>
Dryocopus	Dryocopus	k1gMnSc1
martius	martius	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
strakapoudů	strakapoud	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
vysekávají	vysekávat	k5eAaImIp3nP
v	v	k7c6
kmenech	kmen	k1gInPc6
hnízdní	hnízdní	k2eAgFnPc1d1
dutiny	dutina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
po	po	k7c6
nich	on	k3xPp3gFnPc6
obsazují	obsazovat	k5eAaImIp3nP
další	další	k2eAgMnPc1d1
dutinoví	dutinový	k2eAgMnPc1d1
hnízdiči	hnízdič	k1gMnPc1
<g/>
,	,	kIx,
zejména	zejména	k9
sovy	sova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najdeme	najít	k5eAaPmIp1nP
zde	zde	k6eAd1
obvyklého	obvyklý	k2eAgMnSc2d1
puštíka	puštík	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
(	(	kIx(
<g/>
Strix	Strix	k1gInSc1
aluco	aluco	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezi	mezi	k7c4
mnohem	mnohem	k6eAd1
vzácnější	vzácný	k2eAgMnSc1d2
patří	patřit	k5eAaImIp3nS
kulíšek	kulíšek	k1gMnSc1
nejmenší	malý	k2eAgMnSc1d3
(	(	kIx(
<g/>
Glaucidium	Glaucidium	k1gNnSc1
passerinum	passerinum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
sýc	sýc	k1gMnSc1
rousný	rousný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Aegolius	Aegolius	k1gMnSc1
funereus	funereus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
velké	velký	k2eAgInPc4d1
lesy	les	k1gInPc4
je	být	k5eAaImIp3nS
vázaný	vázaný	k2eAgInSc1d1
výrazný	výrazný	k2eAgInSc1d1
druh	druh	k1gInSc1
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Ciconia	Ciconium	k1gNnSc2
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
hnízdí	hnízdit	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
NPR	NPR	kA
Týřov	Týřov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pěvců	pěvec	k1gMnPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
CHKO	CHKO	kA
např.	např.	kA
žluva	žluva	k1gFnSc1
hajní	hajní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Oriolus	Oriolus	k1gMnSc1
oriolus	oriolus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lejsek	lejsek	k1gMnSc1
bělokrký	bělokrký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Ficedula	Ficedula	k1gFnSc1
albicollis	albicollis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
křovinách	křovina	k1gFnPc6
pěnice	pěnice	k1gFnSc2
vlašská	vlašský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sylvia	Sylvia	k1gFnSc1
nisoria	nisorium	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
tažný	tažný	k2eAgMnSc1d1
druh	druh	k1gMnSc1
ťuhýk	ťuhýk	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Lanius	Lanius	k1gMnSc1
collurio	collurio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
keřích	keř	k1gInPc6
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
vysedávají	vysedávat	k5eAaImIp3nP
strnadi	strnad	k1gMnPc1
a	a	k8xC
na	na	k7c6
loukách	louka	k1gFnPc6
hnízdí	hnízdit	k5eAaImIp3nS
skřivan	skřivan	k1gMnSc1
polní	polní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Alauda	Alauda	k1gMnSc1
arvensis	arvensis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káně	káně	k1gNnSc1
lesní	lesní	k2eAgFnSc2d1
(	(	kIx(
<g/>
Buteo	Butea	k1gMnSc5
falcone	falcon	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejběžnějším	běžný	k2eAgMnSc7d3
dravcem	dravec	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
na	na	k7c6
polích	pole	k1gNnPc6
nelze	lze	k6eNd1
minout	minout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ledňáčka	ledňáček	k1gMnSc2
říčního	říční	k2eAgMnSc2d1
(	(	kIx(
<g/>
Alcedo	Alcedo	k1gNnSc1
atthis	atthis	k1gFnSc2
<g/>
)	)	kIx)
spatříme	spatřit	k5eAaPmIp1nP
jen	jen	k9
vzácně	vzácně	k6eAd1
<g/>
,	,	kIx,
zato	zato	k6eAd1
skorec	skorec	k1gMnSc1
vodní	vodní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cinclus	Cinclus	k1gMnSc1
cinclus	cinclus	k1gMnSc1
<g/>
)	)	kIx)
loví	lovit	k5eAaImIp3nS
bezobratlé	bezobratlý	k2eAgNnSc1d1
na	na	k7c6
většině	většina	k1gFnSc6
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vody	voda	k1gFnSc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc1d1
(	(	kIx(
<g/>
Lutra	Lutra	k1gFnSc1
lutra	lutra	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
invazivní	invazivní	k2eAgMnSc1d1
norek	norek	k1gMnSc1
americký	americký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Mustela	Mustela	k1gFnSc1
vison	vison	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
na	na	k7c6
sněhu	sníh	k1gInSc6
jsou	být	k5eAaImIp3nP
vidět	vidět	k5eAaImF
charakteristické	charakteristický	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
lovící	lovící	k2eAgFnSc2d1
lasice	lasice	k1gFnSc2
hranostaje	hranostaj	k1gMnSc2
(	(	kIx(
<g/>
Mustela	Mustel	k1gMnSc2
erminea	ermineus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skalách	skála	k1gFnPc6
mají	mít	k5eAaImIp3nP
nory	nora	k1gFnPc1
liška	liška	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Vulpes	Vulpes	k1gMnSc1
vulpes	vulpes	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
jezevec	jezevec	k1gMnSc1
lesní	lesní	k2eAgMnPc1d1
(	(	kIx(
<g/>
Meles	Meles	k1gMnSc1
meles	meles	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
zdejší	zdejší	k2eAgMnPc4d1
malé	malý	k2eAgMnPc4d1
savce	savec	k1gMnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
myška	myška	k1gFnSc1
drobná	drobná	k1gFnSc1
(	(	kIx(
<g/>
Micromys	Micromys	k1gInSc1
minutus	minutus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hraboš	hraboš	k1gMnSc1
mokřadní	mokřadní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Microtus	Microtus	k1gInSc1
agrestis	agrestis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plch	plch	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glis	Glis	k1gInSc1
glis	glis	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
plšík	plšík	k1gMnSc1
lískový	lískový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Muscardinus	Muscardinus	k1gMnSc1
avellanarius	avellanarius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantní	dominantní	k2eAgInPc1d1
druhy	druh	k1gInPc1
jsou	být	k5eAaImIp3nP
jelen	jelen	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cervus	Cervus	k1gMnSc1
elaphus	elaphus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
srnec	srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Capreolus	Capreolus	k1gMnSc1
capreolus	capreolus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
prase	prase	k1gNnSc1
divoké	divoký	k2eAgNnSc1d1
(	(	kIx(
<g/>
Sus	Sus	k1gMnSc4
scrofa	scrof	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepůvodními	původní	k2eNgInPc7d1
druhy	druh	k1gInPc7
jsou	být	k5eAaImIp3nP
daněk	daněk	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dama	Dama	k1gMnSc1
dama	dama	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
muflon	muflon	k1gMnSc1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
musimon	musimon	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plán	plán	k1gInSc1
na	na	k7c4
zřízení	zřízení	k1gNnSc4
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
jedinečnosti	jedinečnost	k1gFnSc3
území	území	k1gNnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
řeší	řešit	k5eAaImIp3nS
přechod	přechod	k1gInSc4
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
CHKO	CHKO	kA
na	na	k7c4
vyšší	vysoký	k2eAgInSc4d2
stupeň	stupeň	k1gInSc4
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
zachování	zachování	k1gNnSc4
výjimečně	výjimečně	k6eAd1
rozsáhlých	rozsáhlý	k2eAgInPc2d1
listnatých	listnatý	k2eAgInPc2d1
lesních	lesní	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
2008	#num#	k4
po	po	k7c6
vypracování	vypracování	k1gNnSc6
studií	studio	k1gNnPc2
ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
schválilo	schválit	k5eAaPmAgNnS
záměr	záměr	k1gInSc4
na	na	k7c4
zřízení	zřízení	k1gNnSc4
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
,	,	kIx,
současně	současně	k6eAd1
se	s	k7c7
záměrem	záměr	k1gInSc7
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Jeseníky	Jeseník	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2009	#num#	k4
pak	pak	k6eAd1
byly	být	k5eAaImAgFnP
poprvé	poprvé	k6eAd1
zveřejněny	zveřejnit	k5eAaPmNgFnP
plánované	plánovaný	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
parku	park	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
navrženého	navržený	k2eAgNnSc2d1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
102	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
šestina	šestina	k1gFnSc1
území	území	k1gNnSc2
CHKO	CHKO	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Pozn	pozn	kA
<g/>
:	:	kIx,
Na	na	k7c6
internetu	internet	k1gInSc6
v	v	k7c6
článcích	článek	k1gInPc6
z	z	k7c2
nedávné	dávný	k2eNgFnSc2d1
doby	doba	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
záměně	záměna	k1gFnSc3
km²	km²	k?
za	za	k7c4
hektary	hektar	k1gInPc4
a	a	k8xC
tento	tento	k3xDgInSc1
omyl	omyl	k1gInSc1
je	být	k5eAaImIp3nS
přepisován	přepisovat	k5eAaImNgInS
dále	daleko	k6eAd2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Jde	jít	k5eAaImIp3nS
z	z	k7c2
drtivé	drtivý	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
o	o	k7c4
zalesněné	zalesněný	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Berounky	Berounka	k1gFnSc2
sahá	sahat	k5eAaImIp3nS
od	od	k7c2
Zbirožského	zbirožský	k2eAgInSc2d1
potoka	potok	k1gInSc2
k	k	k7c3
Dlouhé	Dlouhé	k2eAgFnSc3d1
skále	skála	k1gFnSc3
u	u	k7c2
Kublova	Kublův	k2eAgInSc2d1
a	a	k8xC
pak	pak	k6eAd1
směrem	směr	k1gInSc7
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
až	až	k9
ke	k	k7c3
vsi	ves	k1gFnSc3
Žloukovice	Žloukovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
jde	jít	k5eAaImIp3nS
o	o	k7c6
okolí	okolí	k1gNnSc6
Čertovy	čertův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
a	a	k8xC
širší	široký	k2eAgNnSc4d2
okolí	okolí	k1gNnSc4
Vůznice	Vůznice	k1gFnSc2
a	a	k8xC
Žlubineckého	Žlubinecký	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
NP	NP	kA
nezahrnovala	zahrnovat	k5eNaImAgFnS
pole	pole	k1gNnSc4
a	a	k8xC
lidské	lidský	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
<g/>
,	,	kIx,
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
19	#num#	k4
obcemi	obec	k1gFnPc7
<g/>
,	,	kIx,
uvnitř	uvnitř	k6eAd1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
Karlova	Karlův	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
nešlo	jít	k5eNaImAgNnS
vyhnout	vyhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
2010	#num#	k4
končí	končit	k5eAaImIp3nP
kolo	kolo	k1gNnSc4
připomínek	připomínka	k1gFnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
následuje	následovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc4
řešení	řešení	k1gNnSc4
a	a	k8xC
návrh	návrh	k1gInSc4
je	být	k5eAaImIp3nS
připraven	připravit	k5eAaPmNgMnS
postoupit	postoupit	k5eAaPmF
do	do	k7c2
meziresortního	meziresortní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
však	však	k9
nebyl	být	k5eNaImAgMnS
poslán	poslat	k5eAaPmNgMnS
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
tlaku	tlak	k1gInSc2
lobbistických	lobbistický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
a	a	k8xC
nezájmu	nezájem	k1gInSc2
některých	některý	k3yIgMnPc2
politiků	politik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
až	až	k9
v	v	k7c6
březnu	březen	k1gInSc6
2013	#num#	k4
díky	díky	k7c3
ministru	ministr	k1gMnSc3
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Chalupovi	Chalupa	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
návrh	návrh	k1gInSc1
předložen	předložen	k2eAgInSc1d1
vládě	vláda	k1gFnSc3
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
ale	ale	k8xC
shodou	shoda	k1gFnSc7
okolností	okolnost	k1gFnPc2
podala	podat	k5eAaPmAgFnS
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
demisi	demise	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministr	ministr	k1gMnSc1
úřednické	úřednický	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
Tomáš	Tomáš	k1gMnSc1
Podivínský	podivínský	k2eAgMnSc1d1
ve	v	k7c6
snahách	snaha	k1gFnPc6
dále	daleko	k6eAd2
pokračoval	pokračovat	k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
Mezitím	mezitím	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
volbám	volba	k1gFnPc3
<g/>
,	,	kIx,
ministrem	ministr	k1gMnSc7
zemědělství	zemědělství	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Miroslav	Miroslav	k1gMnSc1
Toman	Toman	k1gMnSc1
a	a	k8xC
ten	ten	k3xDgInSc1
celý	celý	k2eAgInSc1d1
návrh	návrh	k1gInSc1
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stávající	stávající	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
dostatečnou	dostatečná	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ministerstvem	ministerstvo	k1gNnSc7
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
zcela	zcela	k6eAd1
připraven	připravit	k5eAaPmNgInS
včetně	včetně	k7c2
finančního	finanční	k2eAgNnSc2d1
zajištění	zajištění	k1gNnSc2
<g/>
,	,	kIx,
zbývalo	zbývat	k5eAaImAgNnS
schválení	schválení	k1gNnSc1
vládou	vláda	k1gFnSc7
a	a	k8xC
parlamentem	parlament	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
průzkumy	průzkum	k1gInPc1
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
a	a	k8xC
2010	#num#	k4
zjistily	zjistit	k5eAaPmAgFnP
70-	70-	k4
a	a	k8xC
78	#num#	k4
<g/>
procentní	procentní	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Proti	proti	k7c3
zřízení	zřízení	k1gNnSc3
parku	park	k1gInSc2
vystupují	vystupovat	k5eAaImIp3nP
zejména	zejména	k9
zástupci	zástupce	k1gMnPc1
myslivců	myslivec	k1gMnPc2
a	a	k8xC
lesníků	lesník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
zorganizovali	zorganizovat	k5eAaPmAgMnP
petici	petice	k1gFnSc4
proti	proti	k7c3
zřízení	zřízení	k1gNnSc3
parku	park	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
podepsalo	podepsat	k5eAaPmAgNnS
14000	#num#	k4
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
bojí	bát	k5eAaImIp3nP
například	například	k6eAd1
omezení	omezení	k1gNnSc4
vstupu	vstup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
návrhu	návrh	k1gInSc2
má	mít	k5eAaImIp3nS
omezení	omezení	k1gNnSc4
vstupu	vstup	k1gInSc2
platit	platit	k5eAaImF
pouze	pouze	k6eAd1
pro	pro	k7c4
I.	I.	kA
zónu	zóna	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
zhruba	zhruba	k6eAd1
3000	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
na	na	k7c4
třetinu	třetina	k1gFnSc4
je	být	k5eAaImIp3nS
vstup	vstup	k1gInSc1
zakázán	zakázat	k5eAaPmNgInS
již	již	k6eAd1
nyní	nyní	k6eAd1
a	a	k8xC
zbytek	zbytek	k1gInSc1
je	být	k5eAaImIp3nS
většinou	většina	k1gFnSc7
ve	v	k7c6
špatně	špatně	k6eAd1
přístupných	přístupný	k2eAgFnPc6d1
částech	část	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stávající	stávající	k2eAgFnSc1d1
síť	síť	k1gFnSc1
turistických	turistický	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
zůstane	zůstat	k5eAaPmIp3nS
přístupná	přístupný	k2eAgFnSc1d1
cyklistům	cyklista	k1gMnPc3
i	i	k8xC
pěším	pěší	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
Těžba	těžba	k1gFnSc1
dřeva	dřevo	k1gNnSc2
3	#num#	k4
</s>
<s>
V	v	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Křivoklátsko	Křivoklátsko	k1gNnSc1
byl	být	k5eAaImAgMnS
ministrem	ministr	k1gMnSc7
zemědělství	zemědělství	k1gNnSc2
Jakubem	Jakub	k1gMnSc7
Šebestou	Šebesta	k1gMnSc7
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
vyhlášen	vyhlásit	k5eAaPmNgInS
první	první	k4xOgInSc1
lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
založení	založení	k1gNnSc2
iniciovala	iniciovat	k5eAaBmAgFnS
Česká	český	k2eAgFnSc1d1
lesnická	lesnický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
(	(	kIx(
<g/>
LPK	LPK	kA
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
rozlohou	rozloha	k1gFnSc7
jen	jen	k9
o	o	k7c4
něco	něco	k3yInSc4
málo	málo	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
160	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
hranice	hranice	k1gFnPc1
určují	určovat	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
obce	obec	k1gFnSc2
Zbečno	Zbečno	k1gNnSc1
<g/>
,	,	kIx,
Ostrovec-Lhotka	Ostrovec-Lhotka	k1gFnSc1
a	a	k8xC
Hudlice	Hudlice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Jakuba	Jakub	k1gMnSc2
Šebesty	Šebesta	k1gMnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
prostřednictvím	prostřednictvím	k7c2
fungujících	fungující	k2eAgInPc2d1
lesnických	lesnický	k2eAgInPc2d1
parků	park	k1gInPc2
mělo	mít	k5eAaImAgNnS
prokázat	prokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
přírodní	přírodní	k2eAgFnPc1d1
a	a	k8xC
kulturní	kulturní	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
určité	určitý	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
účinně	účinně	k6eAd1
chránit	chránit	k5eAaImF
i	i	k9
bez	bez	k7c2
zpřísňování	zpřísňování	k1gNnSc2
speciálního	speciální	k2eAgInSc2d1
režimu	režim	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založení	založení	k1gNnSc1
Lesnického	lesnický	k2eAgInSc2d1
parku	park	k1gInSc2
Křivoklátsko	Křivoklátsko	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
ředitele	ředitel	k1gMnSc2
Agentury	agentura	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
Františka	František	k1gMnSc2
Pelce	Pelc	k1gMnSc2
aktivitou	aktivita	k1gFnSc7
namířenou	namířený	k2eAgFnSc7d1
proti	proti	k7c3
chystanému	chystaný	k2eAgNnSc3d1
vyhlášení	vyhlášení	k1gNnSc3
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
v	v	k7c6
této	tento	k3xDgFnSc6
lokalitě	lokalita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
To	to	k9
lesníci	lesník	k1gMnPc1
sice	sice	k8xC
popírají	popírat	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
situace	situace	k1gFnSc1
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
jasná	jasný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
to	ten	k3xDgNnSc4
být	být	k5eAaImF
součástí	součást	k1gFnSc7
Mezinárodní	mezinárodní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
modelových	modelový	k2eAgInPc2d1
lesů	les	k1gInPc2
(	(	kIx(
<g/>
angl.	angl.	k?
The	The	k1gFnSc1
International	International	k1gFnSc1
Model	model	k1gInSc4
Forest	Forest	k1gInSc1
Network	network	k1gInSc1
–	–	k?
IMFN	IMFN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
projektu	projekt	k1gInSc2
zaměřeného	zaměřený	k2eAgNnSc2d1
na	na	k7c4
trvale	trvale	k6eAd1
udržitelné	udržitelný	k2eAgNnSc4d1
využití	využití	k1gNnSc4
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemá	mít	k5eNaImIp3nS
však	však	k9
žádnou	žádný	k3yNgFnSc4
oporu	opora	k1gFnSc4
v	v	k7c6
zákonech	zákon	k1gInPc6
a	a	k8xC
podle	podle	k7c2
slov	slovo	k1gNnPc2
samotných	samotný	k2eAgMnPc2d1
zakladatelů	zakladatel	k1gMnPc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
dohodu	dohoda	k1gFnSc4
majitelů	majitel	k1gMnPc2
a	a	k8xC
správců	správce	k1gMnPc2
pozemků	pozemek	k1gInPc2
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
hospodařit	hospodařit	k5eAaImF
podle	podle	k7c2
určitých	určitý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Problémy	problém	k1gInPc1
současné	současný	k2eAgFnSc2d1
CHKO	CHKO	kA
</s>
<s>
Les	les	k1gInSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
okusu	okus	k1gInSc2
zvěří	zvěř	k1gFnSc7
nemůže	moct	k5eNaImIp3nS
obnovovat	obnovovat	k5eAaImF
</s>
<s>
Subjekty	subjekt	k1gInPc1
hospodařící	hospodařící	k2eAgInPc1d1
ve	v	k7c6
zdejších	zdejší	k2eAgInPc6d1
lesích	les	k1gInPc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
především	především	k6eAd1
Lesy	les	k1gInPc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
proklamují	proklamovat	k5eAaBmIp3nP
dodržování	dodržování	k1gNnSc4
zásad	zásada	k1gFnPc2
trvale	trvale	k6eAd1
udržitelného	udržitelný	k2eAgNnSc2d1
hospodaření	hospodaření	k1gNnSc2
a	a	k8xC
zvyšování	zvyšování	k1gNnSc2
hodnoty	hodnota	k1gFnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
prezentuje	prezentovat	k5eAaBmIp3nS
i	i	k9
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Otázkou	otázka	k1gFnSc7
je	být	k5eAaImIp3nS
však	však	k9
reálné	reálný	k2eAgNnSc1d1
plnění	plnění	k1gNnSc1
těchto	tento	k3xDgFnPc2
zásad	zásada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
LČR	LČR	kA
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
založena	založit	k5eAaPmNgFnS
obecně	obecně	k6eAd1
prospěšná	prospěšný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc2
Dolní	dolní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
působící	působící	k2eAgInPc1d1
ve	v	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
rezervaci	rezervace	k1gFnSc6
na	na	k7c6
jihu	jih	k1gInSc6
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
jejich	jejich	k3xOp3gFnSc2
správy	správa	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
značné	značný	k2eAgFnSc3d1
devastaci	devastace	k1gFnSc3
tamního	tamní	k2eAgInSc2d1
lužního	lužní	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
patrná	patrný	k2eAgFnSc1d1
už	už	k6eAd1
při	při	k7c6
pohledu	pohled	k1gInSc6
na	na	k7c4
letecké	letecký	k2eAgInPc4d1
snímky	snímek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
hnutí	hnutí	k1gNnSc2
Duha	duha	k1gFnSc1
zde	zde	k6eAd1
za	za	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
zlikvidováno	zlikvidovat	k5eAaPmNgNnS
přes	přes	k7c4
650	#num#	k4
ha	ha	kA
starých	starý	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
eufemicky	eufemicky	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k9
„	„	k?
<g/>
celoplošná	celoplošný	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
půdy	půda	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
plošné	plošný	k2eAgFnSc6d1
holoseči	holoseč	k1gFnSc6
a	a	k8xC
rozmetání	rozmetání	k1gNnSc6
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
vzácných	vzácný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
pařezů	pařez	k1gMnPc2
těžkou	těžký	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
argumentem	argument	k1gInSc7
LČR	LČR	kA
jsou	být	k5eAaImIp3nP
nízké	nízký	k2eAgInPc1d1
náklady	náklad	k1gInPc1
<g/>
,	,	kIx,
při	při	k7c6
tom	ten	k3xDgMnSc6
ale	ale	k8xC
zřejmě	zřejmě	k6eAd1
cenu	cena	k1gFnSc4
stejně	stejně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
podhodnocenou	podhodnocený	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zhruba	zhruba	k6eAd1
od	od	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
projednávat	projednávat	k5eAaImF
návrh	návrh	k1gInSc4
na	na	k7c4
zřízení	zřízení	k1gNnSc4
NP	NP	kA
<g/>
,	,	kIx,
umísťují	umísťovat	k5eAaImIp3nP
hospodařící	hospodařící	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
výrazně	výrazně	k6eAd1
větší	veliký	k2eAgInPc1d2
podíl	podíl	k1gInSc1
těžby	těžba	k1gFnSc2
do	do	k7c2
velmi	velmi	k6eAd1
cenných	cenný	k2eAgInPc2d1
starých	starý	k2eAgInPc2d1
bukových	bukový	k2eAgInPc2d1
porostů	porost	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
by	by	kYmCp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
zachovány	zachován	k2eAgFnPc1d1
a	a	k8xC
chráněny	chráněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
celkovému	celkový	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
těžby	těžba	k1gFnSc2
ve	v	k7c6
snaze	snaha	k1gFnSc6
dotěžit	dotěžit	k5eAaPmF
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
dovoluje	dovolovat	k5eAaImIp3nS
končící	končící	k2eAgInSc1d1
desetiletý	desetiletý	k2eAgInSc4d1
lesnický	lesnický	k2eAgInSc4d1
plán	plán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
velmi	velmi	k6eAd1
utrpěla	utrpět	k5eAaPmAgFnS
estetická	estetický	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
zdejších	zdejší	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
2012	#num#	k4
v	v	k7c6
anketě	anketa	k1gFnSc6
J.	J.	kA
Veselské	Veselská	k1gFnPc4
uvedlo	uvést	k5eAaPmAgNnS
40	#num#	k4
<g/>
%	%	kIx~
z	z	k7c2
200	#num#	k4
dotazovaných	dotazovaný	k2eAgMnPc2d1
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
nespokojeni	spokojit	k5eNaPmNgMnP
s	s	k7c7
lesním	lesní	k2eAgNnSc7d1
hospodařením	hospodaření	k1gNnSc7
na	na	k7c6
Křivoklátsku	Křivoklátsek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Značnou	značný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
mizí	mizet	k5eAaImIp3nP
staré	starý	k2eAgInPc1d1
stromy	strom	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
klíčové	klíčový	k2eAgMnPc4d1
pro	pro	k7c4
mnoho	mnoho	k4c4
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
a	a	k8xC
také	také	k9
dodávají	dodávat	k5eAaImIp3nP
lesu	les	k1gInSc3
jeho	jeho	k3xOp3gFnSc1
impozantní	impozantní	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
především	především	k9
na	na	k7c6
území	území	k1gNnSc6
připravovaného	připravovaný	k2eAgInSc2d1
NP	NP	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
záměrem	záměr	k1gInSc7
nechat	nechat	k5eAaPmF
přírodu	příroda	k1gFnSc4
přirozenému	přirozený	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
<g/>
,	,	kIx,
protože	protože	k8xS
právě	právě	k9
tam	tam	k6eAd1
bylo	být	k5eAaImAgNnS
těchto	tento	k3xDgInPc2
stromů	strom	k1gInPc2
nejvíc	hodně	k6eAd3,k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
diskutovanějším	diskutovaný	k2eAgInSc7d2
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
přezvěření	přezvěření	k1gNnSc1
lesů	les	k1gInPc2
<g/>
,	,	kIx,
podle	podle	k7c2
Petra	Petr	k1gMnSc2
Bendla	Bendla	k1gFnSc7
se	se	k3xPyFc4
lesy	les	k1gInPc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
nadměrného	nadměrný	k2eAgInSc2d1
okusu	okus	k1gInSc2
zvěří	zvěř	k1gFnPc2
nemohou	moct	k5eNaImIp3nP
přirozeně	přirozeně	k6eAd1
obnovovat	obnovovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
pravdu	pravda	k1gFnSc4
mu	on	k3xPp3gMnSc3
dává	dávat	k5eAaImIp3nS
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
pohled	pohled	k1gInSc4
na	na	k7c4
mladé	mladý	k2eAgInPc4d1
okousané	okousaný	k2eAgInPc4d1
stromy	strom	k1gInPc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
vysázenými	vysázený	k2eAgInPc7d1
porosty	porost	k1gInPc7
se	se	k3xPyFc4
najdou	najít	k5eAaPmIp3nP
i	i	k9
takové	takový	k3xDgInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
úplně	úplně	k6eAd1
všechny	všechen	k3xTgInPc4
stromky	stromek	k1gInPc4
zničené	zničený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bendl	Bendl	k1gMnSc1
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c4
věcí	věc	k1gFnPc2
zanedbaných	zanedbaný	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
uvedených	uvedený	k2eAgFnPc2d1
informací	informace	k1gFnPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stávající	stávající	k2eAgFnSc1d1
míra	míra	k1gFnSc1
ochrany	ochrana	k1gFnSc2
pro	pro	k7c4
území	území	k1gNnSc4
není	být	k5eNaImIp3nS
dostatečná	dostatečný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
se	se	k3xPyFc4
založení	založení	k1gNnSc1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
jeví	jevit	k5eAaImIp3nS
jako	jako	k9
jediná	jediný	k2eAgFnSc1d1
naděje	naděje	k1gFnSc1
na	na	k7c4
změnu	změna	k1gFnSc4
poměrů	poměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Turismus	turismus	k1gInSc1
</s>
<s>
Staročeské	staročeský	k2eAgFnPc1d1
máje	máj	k1gFnPc1
v	v	k7c6
obci	obec	k1gFnSc6
Kublov	Kublovo	k1gNnPc2
</s>
<s>
nazdobený	nazdobený	k2eAgInSc1d1
povoz	povoz	k1gInSc1
při	při	k7c6
májích	máj	k1gFnPc6
v	v	k7c6
Kublově	Kublův	k2eAgFnSc6d1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
středověké	středověký	k2eAgInPc4d1
hrady	hrad	k1gInPc4
Křivoklát	Křivoklát	k1gInSc1
<g/>
,	,	kIx,
Točník	Točník	k1gInSc1
<g/>
,	,	kIx,
Žebrák	Žebrák	k1gInSc1
a	a	k8xC
Krakovec	krakovec	k1gInSc1
<g/>
,	,	kIx,
zříceniny	zřícenina	k1gFnPc1
Týřov	Týřov	k1gInSc1
<g/>
,	,	kIx,
Jenčov	Jenčov	k1gInSc1
a	a	k8xC
Jivno	Jivno	k6eAd1
a	a	k8xC
zámky	zámek	k1gInPc4
Zbiroh	Zbiroh	k1gInSc1
<g/>
,	,	kIx,
Lány	lán	k1gInPc1
a	a	k8xC
Nižbor	Nižbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Zbečně	Zbečně	k1gFnSc6
lze	lze	k6eAd1
navštívit	navštívit	k5eAaPmF
lidovou	lidový	k2eAgFnSc4d1
památku	památka	k1gFnSc4
Hamousův	Hamousův	k2eAgInSc4d1
statek	statek	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
červenci	červenec	k1gInSc6
koná	konat	k5eAaImIp3nS
řemeslnický	řemeslnický	k2eAgInSc1d1
jarmark	jarmark	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvu	návštěva	k1gFnSc4
zámku	zámek	k1gInSc2
Nižbor	Nižbor	k1gInSc4
s	s	k7c7
keltskou	keltský	k2eAgFnSc7d1
expozicí	expozice	k1gFnSc7
lze	lze	k6eAd1
spojit	spojit	k5eAaPmF
s	s	k7c7
prohlídkou	prohlídka	k1gFnSc7
místní	místní	k2eAgFnSc2d1
sklárny	sklárna	k1gFnSc2
RÜCKL	RÜCKL	kA
CRYSTAL	CRYSTAL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Točníku	Točník	k1gInSc6
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
koná	konat	k5eAaImIp3nS
historická	historický	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
a	a	k8xC
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrad	hrad	k1gInSc1
Křivoklát	Křivoklát	k1gInSc1
nabízí	nabízet	k5eAaImIp3nS
dva	dva	k4xCgInPc4
prohlídkové	prohlídkový	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
a	a	k8xC
historicky	historicky	k6eAd1
laděnou	laděný	k2eAgFnSc4d1
akci	akce	k1gFnSc4
Křivoklání	Křivoklání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
vsích	ves	k1gFnPc6
se	se	k3xPyFc4
dosud	dosud	k6eAd1
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
slaví	slavit	k5eAaImIp3nP
staročeské	staročeský	k2eAgFnPc1d1
máje	máj	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
i	i	k9
zajímavější	zajímavý	k2eAgFnSc1d2
tzv.	tzv.	kA
suché	suchý	k2eAgInPc1d1
máje	máj	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Kublov	Kublov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Křivoklátsko	Křivoklátsko	k1gNnSc4
zavítá	zavítat	k5eAaPmIp3nS
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
mnoho	mnoho	k6eAd1
turistů	turist	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
letních	letní	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berounka	Berounka	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
tradiční	tradiční	k2eAgFnPc4d1
vodácké	vodácký	k2eAgFnPc4d1
řeky	řeka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skrz	skrz	k7c4
lesy	les	k1gInPc4
vedou	vést	k5eAaImIp3nP
pozoruhodné	pozoruhodný	k2eAgFnPc4d1
značené	značený	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
a	a	k8xC
za	za	k7c4
návštěvu	návštěva	k1gFnSc4
stojí	stát	k5eAaImIp3nS
rovněž	rovněž	k9
zaříznutá	zaříznutý	k2eAgNnPc4d1
údolí	údolí	k1gNnPc4
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zvláště	zvláště	k6eAd1
chráněné	chráněný	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
</s>
<s>
les	les	k1gInSc1
v	v	k7c6
NPR	NPR	kA
Týřov	Týřov	k1gInSc4
</s>
<s>
PR	pr	k0
Jouglovka	Jouglovka	k1gFnSc1
</s>
<s>
PR	pr	k0
Brdatka	Brdatka	k1gFnSc1
<g/>
:	:	kIx,
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
ve	v	k7c6
stráni	stráň	k1gFnSc6
proti	proti	k7c3
Újezdu	Újezd	k1gInSc3
nad	nad	k7c7
Zbečnem	Zbečno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachovaná	zachovaný	k2eAgNnPc1d1
přirozená	přirozený	k2eAgNnPc1d1
lesní	lesní	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Čertova	čertův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
<g/>
:	:	kIx,
lávový	lávový	k2eAgInSc1d1
skalní	skalní	k2eAgInSc1d1
výchoz	výchoz	k1gInSc1
poblíž	poblíž	k7c2
Týřovic	Týřovice	k1gFnPc2
<g/>
,	,	kIx,
xerotermní	xerotermní	k2eAgFnSc1d1
květena	květena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc4
<g/>
:	:	kIx,
výjimečně	výjimečně	k6eAd1
zachovalá	zachovalý	k2eAgFnSc1d1
doubrava	doubrava	k1gFnSc1
s	s	k7c7
mochnou	mochna	k1gFnSc7
bílou	bílý	k2eAgFnSc7d1
(	(	kIx(
<g/>
Potentilla	Potentilla	k1gFnSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přes	přes	k7c4
150	#num#	k4
druhů	druh	k1gInPc2
cévnatých	cévnatý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Dubensko	Dubensko	k1gNnSc1
<g/>
:	:	kIx,
suťový	suťový	k2eAgInSc1d1
svah	svah	k1gInSc1
s	s	k7c7
tisem	tis	k1gInSc7
červeným	červený	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Jezírka	jezírko	k1gNnSc2
<g/>
:	:	kIx,
strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
dolního	dolní	k2eAgInSc2d1
toku	tok	k1gInSc2
Zbirožského	zbirožský	k2eAgInSc2d1
potoka	potok	k1gInSc2
s	s	k7c7
rozmanitými	rozmanitý	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Jouglovka	Jouglovka	k1gFnSc1
<g/>
:	:	kIx,
buližníkový	buližníkový	k2eAgInSc1d1
skalní	skalní	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
beze	beze	k7c2
stop	stop	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Kabečnice	Kabečnice	k1gFnSc1
<g/>
:	:	kIx,
strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Berounky	Berounka	k1gFnSc2
s	s	k7c7
teplomilnou	teplomilný	k2eAgFnSc7d1
vegetací	vegetace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Kohoutov	Kohoutov	k1gInSc1
<g/>
:	:	kIx,
bučina	bučina	k1gFnSc1
pralesovitého	pralesovitý	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
s	s	k7c7
přirozenou	přirozený	k2eAgFnSc7d1
druhovou	druhový	k2eAgFnSc7d1
skladbou	skladba	k1gFnSc7
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
obce	obec	k1gFnSc2
Ostrovec	Ostrovec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Lípa	lípa	k1gFnSc1
<g/>
:	:	kIx,
vrch	vrch	k1gInSc1
se	s	k7c7
suťovými	suťový	k2eAgInPc7d1
svahy	svah	k1gInPc7
a	a	k8xC
přirozenou	přirozený	k2eAgFnSc7d1
skladbou	skladba	k1gFnSc7
dřevin	dřevina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Na	na	k7c6
Babě	baba	k1gFnSc6
<g/>
:	:	kIx,
nejvýznamnější	významný	k2eAgFnSc1d3
křivoklátská	křivoklátský	k2eAgFnSc1d1
pleš	pleš	k1gFnSc1
s	s	k7c7
trávníky	trávník	k1gInPc7
a	a	k8xC
teplomilnou	teplomilný	k2eAgFnSc4d1
doubravu	doubrava	k1gFnSc4
<g/>
,	,	kIx,
nahází	naházet	k5eAaBmIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
jižně	jižně	k6eAd1
od	od	k7c2
Křivoklátu	Křivoklát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Nezabudické	Nezabudický	k2eAgFnPc4d1
skály	skála	k1gFnSc2
<g/>
:	:	kIx,
skalnatý	skalnatý	k2eAgInSc1d1
svah	svah	k1gInSc1
údolí	údolí	k1gNnSc2
Berounky	Berounka	k1gFnSc2
se	s	k7c7
sedmi	sedm	k4xCc2
z	z	k7c2
devíti	devět	k4xCc2
českých	český	k2eAgInPc2d1
druhů	druh	k1gInPc2
plazů	plaz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Prameny	pramen	k1gInPc1
Klíčavy	Klíčava	k1gFnSc2
<g/>
:	:	kIx,
lužní	lužní	k2eAgFnSc2d1
olšiny	olšina	k1gFnSc2
a	a	k8xC
mokřadní	mokřadní	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
prameniště	prameniště	k1gNnSc2
Klíčavy	Klíčava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
PP	PP	kA
Stará	starý	k2eAgFnSc1d1
ves	ves	k1gFnSc1
<g/>
:	:	kIx,
vulkanický	vulkanický	k2eAgInSc1d1
výhoz	výhoz	k1gInSc1
u	u	k7c2
obce	obec	k1gFnSc2
Trubská	Trubský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Stříbrný	stříbrný	k1gInSc4
luh	luh	k1gInSc1
<g/>
:	:	kIx,
stráň	stráň	k1gFnSc1
v	v	k7c6
údolí	údolí	k1gNnSc6
Berounky	Berounka	k1gFnSc2
u	u	k7c2
Častonic	Častonice	k1gFnPc2
porostlá	porostlý	k2eAgFnSc1d1
javorem	javor	k1gInSc7
a	a	k8xC
tisem	tis	k1gInSc7
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
diverzitou	diverzita	k1gFnSc7
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Svatá	svatý	k2eAgFnSc1d1
Alžběta	Alžběta	k1gFnSc1
<g/>
:	:	kIx,
svah	svah	k1gInSc1
údolí	údolí	k1gNnSc2
Klíčavy	Klíčava	k1gFnSc2
se	s	k7c7
zachovalým	zachovalý	k2eAgInSc7d1
listnatým	listnatý	k2eAgInSc7d1
lesem	les	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
PP	PP	kA
Trubínský	Trubínský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
:	:	kIx,
bohatá	bohatý	k2eAgFnSc1d1
skalní	skalní	k2eAgFnSc1d1
step	step	k1gFnSc1
na	na	k7c6
bazaltovém	bazaltový	k2eAgInSc6d1
podkladu	podklad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Týřov	Týřov	k1gInSc1
<g/>
:	:	kIx,
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
zachované	zachovaný	k2eAgNnSc1d1
území	území	k1gNnSc1
kolem	kolem	k7c2
ústí	ústí	k1gNnSc2
Úpořského	Úpořský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
druhů	druh	k1gInPc2
stanovišť	stanoviště	k1gNnPc2
a	a	k8xC
zřícenina	zřícenina	k1gFnSc1
přemyslovského	přemyslovský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
U	u	k7c2
Eremita	eremita	k1gMnSc1
<g/>
:	:	kIx,
původní	původní	k2eAgFnSc2d1
suťové	suťový	k2eAgFnSc2d1
javořiny	javořina	k1gFnSc2
s	s	k7c7
tisem	tis	k1gInSc7
u	u	k7c2
Branova	Branov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
PP	PP	kA
Valachov	Valachov	k1gInSc1
<g/>
:	:	kIx,
umělá	umělý	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
těžbou	těžba	k1gFnSc7
<g/>
,	,	kIx,
uvnitř	uvnitř	k7c2
vznikají	vznikat	k5eAaImIp3nP
minerály	minerál	k1gInPc1
síry	síra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Velká	velká	k1gFnSc1
Pleš	pleš	k1gFnSc1
<g/>
:	:	kIx,
přirozené	přirozený	k2eAgNnSc1d1
bezlesí	bezlesí	k1gNnSc1
na	na	k7c6
temeni	temeno	k1gNnSc6
kopců	kopec	k1gInPc2
s	s	k7c7
navazujícími	navazující	k2eAgInPc7d1
suťovými	suťový	k2eAgInPc7d1
lesy	les	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
druh	druh	k1gInSc4
bezobratlých	bezobratlý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
PP	PP	kA
Vraní	vraní	k2eAgFnSc1d1
skála	skála	k1gFnSc1
<g/>
:	:	kIx,
přístupný	přístupný	k2eAgInSc1d1
buližníkový	buližníkový	k2eAgInSc1d1
hřbet	hřbet	k1gInSc1
s	s	k7c7
výhledem	výhled	k1gInSc7
do	do	k7c2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Vůznice	Vůznice	k1gFnSc1
<g/>
:	:	kIx,
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
území	území	k1gNnSc1
potoka	potok	k1gInSc2
Vůznice	Vůznice	k1gFnSc2
mezi	mezi	k7c7
Bělčí	Bělč	k1gFnSc7
a	a	k8xC
Nižborem	Nižbor	k1gInSc7
obsahuje	obsahovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
typů	typ	k1gInPc2
lesního	lesní	k2eAgInSc2d1
porostu	porost	k1gInSc2
a	a	k8xC
víc	hodně	k6eAd2
než	než	k8xS
450	#num#	k4
druhů	druh	k1gInPc2
cévnatých	cévnatý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PR	pr	k0
Vysoký	vysoký	k2eAgInSc1d1
tok	tok	k1gInSc1
<g/>
:	:	kIx,
další	další	k2eAgInPc1d1
z	z	k7c2
křivoklátských	křivoklátský	k2eAgFnPc2d1
pleší	pleš	k1gFnPc2
s	s	k7c7
projevem	projev	k1gInSc7
vrcholového	vrcholový	k2eAgInSc2d1
fenoménu	fenomén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
PP	PP	kA
Zdická	zdický	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
u	u	k7c2
Kublova	Kublův	k2eAgInSc2d1
<g/>
:	:	kIx,
erozí	eroze	k1gFnSc7
obnažená	obnažený	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
chráněná	chráněný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
výrazný	výrazný	k2eAgInSc1d1
krajinný	krajinný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Vojen	vojna	k1gFnPc2
Ložek	Ložek	k6eAd1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
Kubíková	Kubíková	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Špryňar	Špryňar	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
:	:	kIx,
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
ČR	ČR	kA
;	;	kIx,
sv.	sv.	kA
13	#num#	k4
-	-	kIx~
Střední	střední	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
<g/>
,	,	kIx,
Vyd	Vyd	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
750	#num#	k4
<g/>
–	–	k?
<g/>
757	#num#	k4
<g/>
↑	↑	k?
Vojen	vojna	k1gFnPc2
Ložek	Ložek	k6eAd1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
738	#num#	k4
<g/>
–	–	k?
<g/>
741.1	741.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Křivoklátsko	Křivoklátsko	k1gNnSc1
-	-	kIx~
charakteristika	charakteristika	k1gFnSc1
oblasti	oblast	k1gFnSc2
ke	k	k7c3
stažení	stažení	k1gNnSc3
-	-	kIx~
ze	z	k7c2
stránek	stránka	k1gFnPc2
http://www.ochranaprirody.cz/	http://www.ochranaprirody.cz/	k?
<g/>
↑	↑	k?
Vojen	vojna	k1gFnPc2
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
741	#num#	k4
<g/>
–	–	k?
<g/>
742.1	742.1	k4
2	#num#	k4
3	#num#	k4
Vojen	vojna	k1gFnPc2
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
743	#num#	k4
<g/>
-	-	kIx~
<g/>
745.1	745.1	k4
2	#num#	k4
3	#num#	k4
Vojen	vojna	k1gFnPc2
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
746	#num#	k4
<g/>
-	-	kIx~
<g/>
750.1	750.1	k4
2	#num#	k4
3	#num#	k4
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc1
-	-	kIx~
charakteristika	charakteristika	k1gFnSc1
oblasti	oblast	k1gFnSc2
-	-	kIx~
fauna	fauna	k1gFnSc1
ke	k	k7c3
stažení	stažení	k1gNnSc3
-	-	kIx~
ze	z	k7c2
stránek	stránka	k1gFnPc2
http://www.ochranaprirody.cz/	http://www.ochranaprirody.cz/	k?
<g/>
↑	↑	k?
Stát	stát	k1gInSc1
chystá	chystat	k5eAaImIp3nS
nové	nový	k2eAgInPc4d1
národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
<g/>
:	:	kIx,
Křivoklátsko	Křivoklátsko	k1gNnSc1
a	a	k8xC
Jeseníky	Jeseník	k1gInPc1
<g/>
↑	↑	k?
Stát	stát	k1gInSc1
poprvé	poprvé	k6eAd1
ukázal	ukázat	k5eAaPmAgInS
hranice	hranice	k1gFnPc4
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Křivoklátsko	Křivoklátsko	k1gNnSc4
<g/>
↑	↑	k?
stav	stav	k1gInSc1
k	k	k7c3
létu	léto	k1gNnSc3
2013	#num#	k4
+	+	kIx~
historie	historie	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Ministr	ministr	k1gMnSc1
Toman	Toman	k1gMnSc1
chce	chtít	k5eAaImIp3nS
zastavit	zastavit	k5eAaPmF
přípravu	příprava	k1gFnSc4
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
na	na	k7c6
Křivoklátsku	Křivoklátsek	k1gInSc6
<g/>
.	.	kIx.
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
e	e	k0
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Jiří	Jiří	k1gMnSc1
Oberfalzer	Oberfalzer	k1gMnSc1
-	-	kIx~
Křivoklátsko	Křivoklátsko	k1gNnSc1
a	a	k8xC
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
.	.	kIx.
www.oberfalzer.cz	www.oberfalzer.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Komise	komise	k1gFnSc1
Asociace	asociace	k1gFnSc2
krajů	kraj	k1gInPc2
je	být	k5eAaImIp3nS
proti	proti	k7c3
vyhlášení	vyhlášení	k1gNnSc3
NP	NP	kA
Křivoklátsko	Křivoklátsko	k1gNnSc4
<g/>
.	.	kIx.
www.krajskelisty.cz	www.krajskelisty.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
.	.	kIx.
www.lesycr.cz	www.lesycr.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Pár	pár	k4xCyI
poznámek	poznámka	k1gFnPc2
k	k	k7c3
Lesnickému	lesnický	k2eAgInSc3d1
parku	park	k1gInSc2
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
je	být	k5eAaImIp3nS
akcí	akce	k1gFnSc7
proti	proti	k7c3
národnímu	národní	k2eAgInSc3d1
parku	park	k1gInSc3
<g/>
.	.	kIx.
ekolist	ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
obecně	obecně	k6eAd1
prospěšná	prospěšný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc2
Dolní	dolní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
↑	↑	k?
Inspekce	inspekce	k1gFnSc2
nařídila	nařídit	k5eAaPmAgFnS
státním	státní	k2eAgInPc3d1
lesům	les	k1gInPc3
těžit	těžit	k5eAaImF
dřevo	dřevo	k1gNnSc1
citlivěji	citlivě	k6eAd2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc4
anketa	anketa	k1gFnSc1
<g/>
↑	↑	k?
Křivoklátské	křivoklátský	k2eAgInPc4d1
lesy	les	k1gInPc4
ničí	ničit	k5eAaImIp3nS
přemnožená	přemnožený	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
<g/>
,	,	kIx,
škody	škoda	k1gFnPc1
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
největší	veliký	k2eAgFnPc1d3
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DUDKOVÁ	Dudková	k1gFnSc1
<g/>
,	,	kIx,
Marta	Marta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraj	kraj	k1gInSc4
černých	černý	k2eAgMnPc2d1
čápů	čáp	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Asco	Asco	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
87	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
737	#num#	k4
<g/>
–	–	k?
<g/>
804	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Křivoklátsko	Křivoklátsko	k1gNnSc4
na	na	k7c6
OpenStreetMap	OpenStreetMap	k1gInSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Turistický	turistický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
Skryjská	Skryjský	k2eAgFnSc1d1
jezírka	jezírko	k1gNnSc2
</s>
<s>
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
zřejmě	zřejmě	k6eAd1
bude	být	k5eAaImBp3nS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
obce	obec	k1gFnPc1
už	už	k6eAd1
rezignovaly	rezignovat	k5eAaBmAgFnP
</s>
<s>
Lesnický	lesnický	k2eAgInSc1d1
park	park	k1gInSc1
Křivoklátsko	Křivoklátsko	k1gNnSc1
</s>
<s>
číslo	číslo	k1gNnSc1
dvě	dva	k4xCgNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Křivoklátsku	Křivoklátsek	k1gInSc6
se	se	k3xPyFc4
opět	opět	k6eAd1
rýsuje	rýsovat	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
týkající	týkající	k2eAgInSc4d1
se	se	k3xPyFc4
připravovaného	připravovaný	k2eAgInSc2d1
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
</s>
<s>
je	být	k5eAaImIp3nS
blíž	blízce	k6eAd2
národnímu	národní	k2eAgInSc3d1
parku	park	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
finišuje	finišovat	k5eAaImIp3nS
<g/>
,	,	kIx,
proti	proti	k7c3
jsou	být	k5eAaImIp3nP
myslivci	myslivec	k1gMnPc1
</s>
<s>
lesy	les	k1gInPc4
ničí	ničit	k5eAaImIp3nS
přemnožená	přemnožený	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
<g/>
,	,	kIx,
škody	škoda	k1gFnPc1
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
největší	veliký	k2eAgFnPc1d3
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Beroun	Beroun	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hřebeny	hřeben	k1gInPc1
•	•	k?
Povodí	povodí	k1gNnSc1
Kačáku	Kačák	k1gInSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1
•	•	k?
Koda	Kod	k1gInSc2
•	•	k?
Týřov	Týřov	k1gInSc1
•	•	k?
Vůznice	Vůznice	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Klonk	Klonk	k1gMnSc1
•	•	k?
Kotýz	Kotýz	k1gMnSc1
•	•	k?
Zlatý	zlatý	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Jouglovka	Jouglovka	k1gFnSc1
•	•	k?
Karlické	Karlický	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Kobyla	kobyla	k1gFnSc1
•	•	k?
Na	na	k7c6
Voskopě	Voskopa	k1gFnSc6
•	•	k?
Tetínské	Tetínský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Voškov	Voškov	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
památky	památka	k1gFnSc2
</s>
<s>
Branžovy	Branžův	k2eAgInPc1d1
•	•	k?
Housina	Housin	k2eAgFnSc1d1
•	•	k?
Jindřichova	Jindřichův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Lom	lom	k1gInSc1
Kozolupy	Kozolupa	k1gFnSc2
•	•	k?
Lounín	Lounín	k1gMnSc1
•	•	k?
Otmíčská	Otmíčský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Stará	starý	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Stroupínský	Stroupínský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Studánky	studánka	k1gFnSc2
u	u	k7c2
Cerhovic	Cerhovice	k1gFnPc2
•	•	k?
Syslí	syslí	k2eAgFnSc2d1
louky	louka	k1gFnSc2
u	u	k7c2
Loděnice	loděnice	k1gFnSc2
•	•	k?
Špičatý	špičatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
–	–	k?
Barrandovy	Barrandov	k1gInPc4
jámy	jáma	k1gFnSc2
•	•	k?
Trubínský	Trubínský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Vraní	vraní	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Zahořanský	Zahořanský	k2eAgMnSc1d1
stratotyp	stratotyp	k1gMnSc1
•	•	k?
Zdická	zdický	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
u	u	k7c2
Kublova	Kublův	k2eAgInSc2d1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Kladno	Kladno	k1gNnSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Džbán	džbán	k1gInSc1
•	•	k?
Povodí	povodí	k1gNnSc1
Kačáku	Kačák	k1gInSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Křivoklátsko	Křivoklátsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Vůznice	Vůznice	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílichovské	Bílichovský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Cikánský	cikánský	k2eAgInSc4d1
dolík	dolík	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Cikánský	cikánský	k2eAgInSc1d1
dolík	dolík	k1gInSc1
•	•	k?
Údolí	údolí	k1gNnSc1
Klíčavy	Klíčava	k1gFnSc2
•	•	k?
Záplavy	záplava	k1gFnSc2
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bohouškova	Bohouškův	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Červené	Červené	k2eAgInPc7d1
dolíky	dolík	k1gInPc7
•	•	k?
Hradiště	Hradiště	k1gNnSc2
•	•	k?
Kalspot	Kalspota	k1gFnPc2
•	•	k?
Kovárské	Kovárský	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Krnčí	Krnčí	k1gMnSc1
a	a	k8xC
Voleška	Voleška	k1gFnSc1
•	•	k?
Kyšice	Kyšice	k1gFnSc1
–	–	k?
Kobyla	kobyla	k1gFnSc1
•	•	k?
Markův	Markův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Mokřiny	mokřina	k1gFnSc2
u	u	k7c2
Beřovic	Beřovice	k1gFnPc2
•	•	k?
Na	na	k7c6
Pilavě	Pilava	k1gFnSc6
•	•	k?
Ostrov	ostrov	k1gInSc1
u	u	k7c2
Jedomělic	Jedomělice	k1gFnPc2
•	•	k?
Otvovická	Otvovický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Šibenicí	šibenice	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Veselovem	Veselov	k1gInSc7
•	•	k?
Podlešínská	Podlešínský	k2eAgFnSc1d1
skalní	skalní	k2eAgFnSc1d1
jehla	jehla	k1gFnSc1
•	•	k?
Slánská	slánský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Smečenská	Smečenský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Smečno	Smečno	k1gNnSc1
•	•	k?
Smradovna	Smradovna	k1gFnSc1
•	•	k?
Třebichovická	Třebichovický	k2eAgFnSc1d1
olšinka	olšinka	k1gFnSc1
•	•	k?
Ve	v	k7c6
Šperkotně	Šperkotně	k1gFnSc6
•	•	k?
Vinařická	Vinařický	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Zákolanský	Zákolanský	k2eAgInSc4d1
potok	potok	k1gInSc4
•	•	k?
Žraločí	žraločí	k2eAgInPc4d1
zuby	zub	k1gInPc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Plzeň-sever	Plzeň-sevra	k1gFnPc2
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Berounka	Berounka	k1gFnSc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Střela	střela	k1gFnSc1
•	•	k?
Hřešihlavská	Hřešihlavský	k2eAgFnSc1d1
•	•	k?
Rohatiny	rohatina	k1gFnSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Křivoklátsko	Křivoklátsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Odlezelské	Odlezelský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bažantnice	bažantnice	k1gFnSc1
•	•	k?
Dubensko	Dubensko	k1gNnSc1
•	•	k?
Hůrky	hůrka	k1gFnSc2
•	•	k?
Kozelka	Kozelek	k1gMnSc2
•	•	k?
Krašov	Krašov	k1gInSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Petrovka	Petrovka	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
u	u	k7c2
Polínek	polínko	k1gNnPc2
•	•	k?
Střela	střela	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Čerňovice	Čerňovice	k1gFnSc1
•	•	k?
Čertova	čertův	k2eAgFnSc1d1
hráz	hráz	k1gFnSc1
•	•	k?
Hromnické	Hromnický	k2eAgNnSc4d1
jezírko	jezírko	k1gNnSc4
•	•	k?
Malenický	Malenický	k2eAgInSc4d1
pramen	pramen	k1gInSc4
•	•	k?
Malochova	Malochův	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Osojno	Osojno	k6eAd1
•	•	k?
Příšovská	Příšovský	k2eAgFnSc1d1
homolka	homolka	k1gFnSc1
•	•	k?
U	u	k7c2
Báby	bába	k1gFnSc2
–	–	k?
U	u	k7c2
Lomu	lom	k1gInSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Rakovník	Rakovník	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Džbán	džbán	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Křivoklátsko	Křivoklátsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Pochválovská	Pochválovský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Týřov	Týřov	k1gInSc1
•	•	k?
Velká	velká	k1gFnSc1
Pleš	pleš	k1gFnSc1
•	•	k?
Vůznice	Vůznice	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Brdatka	Brdatka	k1gFnSc1
•	•	k?
Čertova	čertův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Červená	červený	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
•	•	k?
Skryjská	Skryjský	k2eAgFnSc1d1
jezírka	jezírko	k1gNnSc2
•	•	k?
Kabečnice	Kabečnice	k1gFnSc2
•	•	k?
Louky	louka	k1gFnSc2
v	v	k7c6
oboře	obora	k1gFnSc6
Libeň	Libeň	k1gFnSc4
•	•	k?
Luční	luční	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Milská	Milská	k1gFnSc1
stráň	stráň	k1gFnSc1
•	•	k?
Na	na	k7c6
Babě	baba	k1gFnSc6
•	•	k?
Nezabudické	Nezabudický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Podhůrka	Podhůrka	k1gMnSc1
•	•	k?
Prameny	pramen	k1gInPc1
Klíčavy	Klíčava	k1gFnSc2
•	•	k?
Rybníčky	rybníček	k1gInPc4
u	u	k7c2
Podbořánek	Podbořánka	k1gFnPc2
•	•	k?
Stříbrný	stříbrný	k1gInSc1
luh	luh	k1gInSc1
•	•	k?
Svatá	svatat	k5eAaImIp3nS
Alžběta	Alžběta	k1gFnSc1
•	•	k?
Tankodrom	tankodrom	k1gInSc1
•	•	k?
U	u	k7c2
Eremita	eremit	k1gMnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Klíčavy	Klíčava	k1gFnSc2
•	•	k?
V	v	k7c6
Bahnách	Bahnách	k?
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
Tok	tok	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Kalivody	Kalivoda	k1gMnPc4
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Krtské	Krtský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Malý	malý	k2eAgInSc1d1
Uran	Uran	k1gInSc1
•	•	k?
Na	na	k7c6
Novém	nový	k2eAgInSc6d1
rybníce	rybník	k1gInSc6
•	•	k?
Ostrovecká	Ostrovecký	k2eAgFnSc1d1
olšina	olšina	k1gFnSc1
•	•	k?
Plaviště	plaviště	k1gNnSc2
•	•	k?
Prameny	pramen	k1gInPc1
Javornice	Javornice	k1gFnSc2
•	•	k?
Přílepská	Přílepský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Skryjsko-týřovické	Skryjsko-týřovický	k2eAgNnSc4d1
kambrium	kambrium	k1gNnSc4
•	•	k?
Soseňský	Soseňský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Valachov	Valachov	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Rokycany	Rokycany	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Berounka	Berounka	k1gFnSc1
•	•	k?
Trhoň	Trhoň	k1gFnSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Chlumská	chlumský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kohoutov	Kohoutov	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Vosek	Vosek	k6eAd1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Skryjská	Skryjský	k2eAgNnPc1d1
jezírka	jezírko	k1gNnPc1
•	•	k?
Lípa	lípa	k1gFnSc1
•	•	k?
Louky	louka	k1gFnSc2
pod	pod	k7c7
Palcířem	Palcíř	k1gInSc7
•	•	k?
Třímanské	třímanský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
V	v	k7c6
Horách	hora	k1gFnPc6
•	•	k?
Zvoníčkovna	Zvoníčkovna	k1gFnSc1
•	•	k?
Žďár	Žďár	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bašta	Bašta	k1gMnSc1
•	•	k?
Biskoupky	Biskoupka	k1gFnSc2
•	•	k?
Ejpovické	ejpovický	k2eAgInPc4d1
útesy	útes	k1gInPc4
•	•	k?
Hrádecká	hrádecký	k2eAgFnSc1d1
bahna	bahno	k1gNnSc2
•	•	k?
Jalovce	jalovec	k1gInPc1
na	na	k7c6
Světovině	Světovina	k1gFnSc6
•	•	k?
Kakejcov	Kakejcov	k1gInSc1
•	•	k?
Kamenec	Kamenec	k1gInSc4
•	•	k?
Kařezské	Kařezský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Kašparův	Kašparův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Kateřina	Kateřina	k1gFnSc1
•	•	k?
Medový	medový	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Niva	niva	k1gFnSc1
u	u	k7c2
Volduch	Volducha	k1gFnPc2
•	•	k?
Pod	pod	k7c7
Starým	starý	k2eAgInSc7d1
hradem	hrad	k1gInSc7
•	•	k?
Rokycanská	rokycanský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Rumpál	rumpál	k1gInSc1
•	•	k?
Štěpánský	štěpánský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
U	u	k7c2
hřbitova	hřbitov	k1gInSc2
•	•	k?
Zavírka	zavírka	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128447	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
</s>
