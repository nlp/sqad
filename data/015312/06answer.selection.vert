<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Křivoklátsko	Křivoklátsko	k1gNnSc4
je	být	k5eAaImIp3nS
chráněné	chráněný	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
628	#num#	k4
km²	km²	k?
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
původního	původní	k2eAgInSc2d1
krajinného	krajinný	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
včetně	včetně	k7c2
mimořádně	mimořádně	k6eAd1
cenných	cenný	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
.	.	kIx.
</s>