<s>
Antarktida	Antarktida	k1gFnSc1
(	(	kIx(
<g/>
název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řečtiny	řečtina	k1gFnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
"	"	kIx"
<g/>
naproti	naproti	k7c3
Arktidě	Arktida	k1gFnSc3
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
přeneseném	přenesený	k2eAgInSc6d1
významu	význam	k1gInSc6
"	"	kIx"
<g/>
naproti	naproti	k7c3
severu	sever	k1gInSc3
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtvrtý	čtvrtý	k4xOgInSc4
největší	veliký	k2eAgInSc4d3
kontinent	kontinent	k1gInSc4
<g/>
.	.	kIx.
</s>