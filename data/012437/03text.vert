<p>
<s>
Husitství	husitství	k1gNnSc1	husitství
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
v	v	k7c6	v
marxistické	marxistický	k2eAgFnSc6d1	marxistická
historiografii	historiografie	k1gFnSc6	historiografie
též	též	k9	též
husitské	husitský	k2eAgNnSc1d1	husitské
revoluční	revoluční	k2eAgNnSc1d1	revoluční
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
nábožensky	nábožensky	k6eAd1	nábožensky
–	–	k?	–
a	a	k8xC	a
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
ohledech	ohled	k1gInPc6	ohled
i	i	k9	i
národnostně	národnostně	k6eAd1	národnostně
<g/>
,	,	kIx,	,
sociálně	sociálně	k6eAd1	sociálně
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
–	–	k?	–
motivované	motivovaný	k2eAgNnSc4d1	motivované
hnutí	hnutí	k1gNnSc4	hnutí
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
vzešlé	vzešlý	k2eAgInPc1d1	vzešlý
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
dalekosáhlou	dalekosáhlý	k2eAgFnSc4d1	dalekosáhlá
reformu	reforma	k1gFnSc4	reforma
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Husitství	husitství	k1gNnSc1	husitství
se	se	k3xPyFc4	se
zrodilo	zrodit	k5eAaPmAgNnS	zrodit
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
stoupenců	stoupenec	k1gMnPc2	stoupenec
pražského	pražský	k2eAgMnSc2d1	pražský
univerzitního	univerzitní	k2eAgMnSc2d1	univerzitní
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gNnSc6	jehož
upálení	upálení	k1gNnSc6	upálení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
se	se	k3xPyFc4	se
masově	masově	k6eAd1	masově
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
i	i	k8xC	i
Moravském	moravský	k2eAgNnSc6d1	Moravské
markrabství	markrabství	k1gNnSc6	markrabství
(	(	kIx(	(
<g/>
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
pak	pak	k6eAd1	pak
i	i	k9	i
v	v	k7c6	v
hornoslezských	hornoslezský	k2eAgNnPc6d1	hornoslezské
vévodstvích	vévodství	k1gNnPc6	vévodství
<g/>
)	)	kIx)	)
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
citelně	citelně	k6eAd1	citelně
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Husité	husita	k1gMnPc1	husita
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
nazývali	nazývat	k5eAaImAgMnP	nazývat
kališníky	kališník	k1gMnPc7	kališník
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
označení	označený	k2eAgMnPc1d1	označený
husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zprvu	zprvu	k6eAd1	zprvu
chápáno	chápat	k5eAaImNgNnS	chápat
pejorativně	pejorativně	k6eAd1	pejorativně
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
utrakvisté	utrakvista	k1gMnPc1	utrakvista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologie	chronologie	k1gFnSc2	chronologie
==	==	k?	==
</s>
</p>
<p>
<s>
Chronologicky	chronologicky	k6eAd1	chronologicky
se	se	k3xPyFc4	se
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
době	doba	k1gFnSc6	doba
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1419	[number]	k4	1419
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
jakési	jakýsi	k3yIgFnSc6	jakýsi
předehře	předehra	k1gFnSc6	předehra
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
další	další	k2eAgInSc1d1	další
předěl	předěl	k1gInSc1	předěl
je	být	k5eAaImIp3nS	být
chápán	chápat	k5eAaImNgInS	chápat
rok	rok	k1gInSc1	rok
1427	[number]	k4	1427
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
upevnila	upevnit	k5eAaPmAgFnS	upevnit
hegemonie	hegemonie	k1gFnSc1	hegemonie
polních	polní	k2eAgFnPc2d1	polní
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
převrat	převrat	k1gInSc1	převrat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
radikálnímu	radikální	k2eAgInSc3d1	radikální
husitismu	husitismus	k1gInSc3	husitismus
pražského	pražský	k2eAgNnSc2d1	Pražské
souměstí	souměstí	k1gNnSc2	souměstí
<g/>
.	.	kIx.	.
</s>
<s>
Radikálové	radikál	k1gMnPc1	radikál
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
drželi	držet	k5eAaImAgMnP	držet
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
až	až	k9	až
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
konec	konec	k1gInSc1	konec
další	další	k2eAgFnSc2d1	další
etapy	etapa	k1gFnSc2	etapa
je	být	k5eAaImIp3nS	být
chápán	chápat	k5eAaImNgInS	chápat
rok	rok	k1gInSc1	rok
1436	[number]	k4	1436
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
basilejských	basilejský	k2eAgNnPc2d1	Basilejské
kompaktát	kompaktáta	k1gNnPc2	kompaktáta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
i	i	k9	i
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
konec	konec	k1gInSc1	konec
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
František	František	k1gMnSc1	František
Šmahel	Šmahel	k1gMnSc1	Šmahel
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Čornej	Čornej	k1gMnSc1	Čornej
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
novějších	nový	k2eAgFnPc6d2	novější
pracích	práce	k1gFnPc6	práce
ovšem	ovšem	k9	ovšem
nepovažují	považovat	k5eNaImIp3nP	považovat
rok	rok	k1gInSc4	rok
1436	[number]	k4	1436
za	za	k7c4	za
konec	konec	k1gInSc4	konec
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
souvislost	souvislost	k1gFnSc4	souvislost
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1436	[number]	k4	1436
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Františka	František	k1gMnSc4	František
Šmahela	Šmahel	k1gMnSc4	Šmahel
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
končí	končit	k5eAaImIp3nS	končit
až	až	k6eAd1	až
kutnohorským	kutnohorský	k2eAgInSc7d1	kutnohorský
náboženským	náboženský	k2eAgInSc7d1	náboženský
mírem	mír	k1gInSc7	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Čornej	Čornej	k1gFnSc2	Čornej
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
etapy	etapa	k1gFnPc4	etapa
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1436	[number]	k4	1436
a	a	k8xC	a
1467	[number]	k4	1467
<g/>
–	–	k?	–
<g/>
1479	[number]	k4	1479
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Situace	situace	k1gFnSc1	situace
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
revoluce	revoluce	k1gFnSc2	revoluce
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Důvody	důvod	k1gInPc1	důvod
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
Česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgInPc2d3	nejmocnější
středoevropských	středoevropský	k2eAgInPc2d1	středoevropský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
tento	tento	k3xDgInSc1	tento
prudký	prudký	k2eAgInSc1d1	prudký
vzestup	vzestup	k1gInSc1	vzestup
měl	mít	k5eAaImAgInS	mít
své	svůj	k3xOyFgFnPc4	svůj
stinné	stinný	k2eAgFnPc4d1	stinná
stránky	stránka	k1gFnPc4	stránka
<g/>
:	:	kIx,	:
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
produkce	produkce	k1gFnSc1	produkce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hranice	hranice	k1gFnSc1	hranice
svých	svůj	k3xOyFgFnPc2	svůj
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
sice	sice	k8xC	sice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
ani	ani	k8xC	ani
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
nenabyl	nabýt	k5eNaPmAgInS	nabýt
katastrofických	katastrofický	k2eAgInPc2d1	katastrofický
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc4	jeho
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
mentalitu	mentalita	k1gFnSc4	mentalita
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byly	být	k5eAaImAgInP	být
stále	stále	k6eAd1	stále
dosti	dosti	k6eAd1	dosti
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
původně	původně	k6eAd1	původně
příznivé	příznivý	k2eAgNnSc1d1	příznivé
klima	klima	k1gNnSc1	klima
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
značně	značně	k6eAd1	značně
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
malá	malý	k2eAgFnSc1d1	malá
doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
hladomorů	hladomor	k1gMnPc2	hladomor
<g/>
.	.	kIx.	.
<g/>
Založení	založení	k1gNnSc1	založení
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
sice	sice	k8xC	sice
pozvedlo	pozvednout	k5eAaPmAgNnS	pozvednout
Prahu	Praha	k1gFnSc4	Praha
kulturně	kulturně	k6eAd1	kulturně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohli	moct	k5eNaImAgMnP	moct
najít	najít	k5eAaPmF	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
vlivu	vliv	k1gInSc2	vliv
cizinců	cizinec	k1gMnPc2	cizinec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
Kutnohorským	kutnohorský	k2eAgInSc7d1	kutnohorský
dekretem	dekret	k1gInSc7	dekret
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
vlivu	vliv	k1gInSc2	vliv
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
ke	k	k7c3	k
kulturní	kulturní	k2eAgFnSc3d1	kulturní
izolaci	izolace	k1gFnSc3	izolace
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
vznikalo	vznikat	k5eAaImAgNnS	vznikat
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
sociálním	sociální	k2eAgInPc3d1	sociální
rozdílům	rozdíl	k1gInPc3	rozdíl
mezi	mezi	k7c7	mezi
duchovními	duchovní	k2eAgFnPc7d1	duchovní
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
kazatelé	kazatel	k1gMnPc1	kazatel
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
na	na	k7c4	na
neutěšené	utěšený	k2eNgInPc4d1	neutěšený
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
reformy	reforma	k1gFnSc2	reforma
této	tento	k3xDgFnSc2	tento
instituce	instituce	k1gFnSc2	instituce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vzdálila	vzdálit	k5eAaPmAgFnS	vzdálit
původním	původní	k2eAgInPc3d1	původní
ideálům	ideál	k1gInPc3	ideál
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
představitelé	představitel	k1gMnPc1	představitel
často	často	k6eAd1	často
jednali	jednat	k5eAaImAgMnP	jednat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
učením	učení	k1gNnSc7	učení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
stala	stát	k5eAaPmAgFnS	stát
místem	místo	k1gNnSc7	místo
teologicko-filosofického	teologickoilosofický	k2eAgInSc2d1	teologicko-filosofický
boje	boj	k1gInSc2	boj
–	–	k?	–
čeští	český	k2eAgMnPc1d1	český
vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
dostali	dostat	k5eAaPmAgMnP	dostat
přijetím	přijetí	k1gNnSc7	přijetí
myšlenek	myšlenka	k1gFnPc2	myšlenka
Johna	John	k1gMnSc2	John
Wycliffa	Wycliff	k1gMnSc2	Wycliff
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
přinesl	přinést	k5eAaPmAgMnS	přinést
Jeroným	Jeroným	k1gMnSc1	Jeroným
Pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
odlišit	odlišit	k5eAaPmF	odlišit
se	se	k3xPyFc4	se
od	od	k7c2	od
německých	německý	k2eAgMnPc2d1	německý
nominalistů	nominalista	k1gMnPc2	nominalista
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
diskuse	diskuse	k1gFnSc1	diskuse
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
odborného	odborný	k2eAgInSc2d1	odborný
diskurzu	diskurz	k1gInSc2	diskurz
<g/>
,	,	kIx,	,
když	když	k8xS	když
však	však	k9	však
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc3	rok
1403	[number]	k4	1403
Viklefovy	Viklefův	k2eAgInPc4d1	Viklefův
články	článek	k1gInPc4	článek
shromážděním	shromáždění	k1gNnSc7	shromáždění
univerzitních	univerzitní	k2eAgMnPc2d1	univerzitní
mistrů	mistr	k1gMnPc2	mistr
odsouzeny	odsouzen	k2eAgInPc1d1	odsouzen
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
jen	jen	k9	jen
vyhrotilo	vyhrotit	k5eAaPmAgNnS	vyhrotit
a	a	k8xC	a
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
zášť	zášť	k1gFnSc4	zášť
vůči	vůči	k7c3	vůči
cizím	cizí	k2eAgMnPc3d1	cizí
vzdělancům	vzdělanec	k1gMnPc3	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
myšlenky	myšlenka	k1gFnPc1	myšlenka
nacházely	nacházet	k5eAaImAgFnP	nacházet
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
půdu	půda	k1gFnSc4	půda
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
princezny	princezna	k1gFnSc2	princezna
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
Richardem	Richard	k1gMnSc7	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Viclefovy	Viclefův	k2eAgInPc1d1	Viclefův
názory	názor	k1gInPc1	názor
však	však	k9	však
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nebyly	být	k5eNaImAgFnP	být
nové	nový	k2eAgFnPc1d1	nová
<g/>
,	,	kIx,	,
již	již	k9	již
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
názory	názor	k1gInPc7	názor
na	na	k7c4	na
eucharistii	eucharistie	k1gFnSc4	eucharistie
polemizoval	polemizovat	k5eAaImAgMnS	polemizovat
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Biskupec	biskupec	k1gMnSc1	biskupec
a	a	k8xC	a
na	na	k7c4	na
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
majetku	majetek	k1gInSc2	majetek
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
pak	pak	k6eAd1	pak
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhal	probíhat	k5eAaImAgInS	probíhat
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
filozofickými	filozofický	k2eAgMnPc7d1	filozofický
a	a	k8xC	a
teologickými	teologický	k2eAgMnPc7d1	teologický
zastánci	zastánce	k1gMnPc7	zastánce
nominalismu	nominalismus	k1gInSc2	nominalismus
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
)	)	kIx)	)
a	a	k8xC	a
přívrženci	přívrženec	k1gMnPc1	přívrženec
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
reprezentovanými	reprezentovaný	k2eAgInPc7d1	reprezentovaný
především	především	k9	především
českou	český	k2eAgFnSc7d1	Česká
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
Viklefovými	Viklefův	k2eAgMnPc7d1	Viklefův
přívrženci	přívrženec	k1gMnPc7	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
držení	držení	k1gNnSc6	držení
se	se	k3xPyFc4	se
Viklefa	Viklef	k1gMnSc2	Viklef
sledoval	sledovat	k5eAaImAgMnS	sledovat
své	své	k1gNnSc4	své
učitele	učitel	k1gMnSc2	učitel
Stanislava	Stanislav	k1gMnSc2	Stanislav
ze	z	k7c2	z
Znojma	Znojmo	k1gNnSc2	Znojmo
a	a	k8xC	a
Štěpána	Štěpána	k1gFnSc1	Štěpána
z	z	k7c2	z
Pálče	Páleč	k1gFnSc2	Páleč
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1403	[number]	k4	1403
německá	německý	k2eAgFnSc1d1	německá
část	část	k1gFnSc1	část
univerzity	univerzita	k1gFnSc2	univerzita
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
Viklefovy	Viklefův	k2eAgInPc4d1	Viklefův
názory	názor	k1gInPc4	názor
sepsané	sepsaný	k2eAgFnSc2d1	sepsaná
dominikánem	dominikán	k1gMnSc7	dominikán
Janem	Jan	k1gMnSc7	Jan
Hübnerem	Hübner	k1gMnSc7	Hübner
<g/>
.	.	kIx.	.
</s>
<s>
Viklefovy	Viklefův	k2eAgFnPc4d1	Viklefova
myšlenky	myšlenka	k1gFnPc4	myšlenka
užíval	užívat	k5eAaImAgMnS	užívat
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
i	i	k9	i
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
kázáních	kázání	k1gNnPc6	kázání
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
mistři	mistr	k1gMnPc1	mistr
včetně	včetně	k7c2	včetně
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
měli	mít	k5eAaImAgMnP	mít
podporu	podpora	k1gFnSc4	podpora
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1405	[number]	k4	1405
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
Stanislav	Stanislav	k1gMnSc1	Stanislav
ze	z	k7c2	z
Znojma	Znojmo	k1gNnSc2	Znojmo
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
ohledně	ohledně	k7c2	ohledně
remanenční	remanenční	k2eAgFnSc2d1	remanenční
nauky	nauka	k1gFnSc2	nauka
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
dané	daný	k2eAgFnSc2d1	daná
pražskému	pražský	k2eAgMnSc3d1	pražský
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
Zbyňku	Zbyněk	k1gMnSc3	Zbyněk
Zajíci	Zajíc	k1gMnSc3	Zajíc
z	z	k7c2	z
Házmburka	Házmburka	k1gFnSc1	Házmburka
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
osobností	osobnost	k1gFnPc2	osobnost
české	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
právě	právě	k9	právě
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
vybízel	vybízet	k5eAaImAgInS	vybízet
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
aby	aby	k9	aby
Viklefovo	Viklefův	k2eAgNnSc4d1	Viklefovo
učení	učení	k1gNnSc4	učení
zavrhla	zavrhnout	k5eAaPmAgFnS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
národ	národ	k1gInSc1	národ
sice	sice	k8xC	sice
soupis	soupis	k1gInSc1	soupis
z	z	k7c2	z
Viklefa	Viklef	k1gMnSc2	Viklef
vybraných	vybraný	k2eAgFnPc2d1	vybraná
vět	věta	k1gFnPc2	věta
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1408	[number]	k4	1408
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
svými	svůj	k3xOyFgFnPc7	svůj
formulacemi	formulace	k1gFnPc7	formulace
dal	dát	k5eAaPmAgInS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Viklef	Viklef	k1gInSc1	Viklef
chápán	chápat	k5eAaImNgInS	chápat
správně	správně	k6eAd1	správně
a	a	k8xC	a
v	v	k7c6	v
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
jeho	jeho	k3xOp3gFnPc3	jeho
názory	názor	k1gInPc4	názor
kacířské	kacířský	k2eAgNnSc1d1	kacířské
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
na	na	k7c4	na
králův	králův	k2eAgInSc4d1	králův
nátlak	nátlak	k1gInSc4	nátlak
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nebylo	být	k5eNaImAgNnS	být
shledáno	shledat	k5eAaPmNgNnS	shledat
bludů	blud	k1gInPc2	blud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
navíc	navíc	k6eAd1	navíc
nebyl	být	k5eNaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgInS	dokázat
úspěšně	úspěšně	k6eAd1	úspěšně
překonat	překonat	k5eAaPmF	překonat
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
či	či	k8xC	či
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
nebyla	být	k5eNaImAgNnP	být
spokojena	spokojen	k2eAgFnSc1d1	spokojena
ani	ani	k8xC	ani
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
,	,	kIx,	,
důsledkem	důsledek	k1gInSc7	důsledek
čehož	což	k3yRnSc2	což
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
zajat	zajmout	k5eAaPmNgInS	zajmout
a	a	k8xC	a
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
navíc	navíc	k6eAd1	navíc
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
rostl	růst	k5eAaImAgInS	růst
neklid	neklid	k1gInSc1	neklid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
Joštem	Jošt	k1gMnSc7	Jošt
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratry	bratr	k1gMnPc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
tyto	tento	k3xDgInPc1	tento
boje	boj	k1gInPc1	boj
skončily	skončit	k5eAaPmAgInP	skončit
<g/>
,	,	kIx,	,
utvořili	utvořit	k5eAaPmAgMnP	utvořit
nyní	nyní	k6eAd1	nyní
nezaměstnaní	zaměstnaný	k2eNgMnPc1d1	nezaměstnaný
bojovníci	bojovník	k1gMnPc1	bojovník
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
loupily	loupit	k5eAaImAgFnP	loupit
a	a	k8xC	a
hledaly	hledat	k5eAaImAgFnP	hledat
nové	nový	k2eAgNnSc4d1	nové
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hlasatelé	hlasatel	k1gMnPc1	hlasatel
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
====	====	k?	====
</s>
</p>
<p>
<s>
Kazatelé	kazatel	k1gMnPc1	kazatel
požadující	požadující	k2eAgFnSc4d1	požadující
nápravu	náprava	k1gFnSc4	náprava
stávajících	stávající	k2eAgInPc2d1	stávající
poměrů	poměr	k1gInPc2	poměr
se	se	k3xPyFc4	se
objevovali	objevovat	k5eAaImAgMnP	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Konrád	Konrád	k1gMnSc1	Konrád
Waldhauser	Waldhauser	k1gMnSc1	Waldhauser
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Milíč	Milíč	k1gMnSc1	Milíč
z	z	k7c2	z
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
módní	módní	k2eAgFnSc4d1	módní
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
,	,	kIx,	,
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
však	však	k9	však
kritika	kritika	k1gFnSc1	kritika
začala	začít	k5eAaPmAgFnS	začít
nabývat	nabývat	k5eAaImF	nabývat
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
důrazu	důraz	k1gInSc6	důraz
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
restrikce	restrikce	k1gFnSc1	restrikce
(	(	kIx(	(
<g/>
1403	[number]	k4	1403
zákaz	zákaz	k1gInSc4	zákaz
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
1412	[number]	k4	1412
uvalení	uvalení	k1gNnSc2	uvalení
klatby	klatba	k1gFnSc2	klatba
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Husa	Hus	k1gMnSc4	Hus
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
v	v	k7c6	v
kališnických	kališnický	k2eAgMnPc6d1	kališnický
kazatelích	kazatel	k1gMnPc6	kazatel
jen	jen	k6eAd1	jen
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
pocit	pocit	k1gInSc4	pocit
mučedníků	mučedník	k1gMnPc2	mučedník
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
zostřovala	zostřovat	k5eAaImAgFnS	zostřovat
jejich	jejich	k3xOp3gFnSc4	jejich
rétoriku	rétorika	k1gFnSc4	rétorika
<g/>
.	.	kIx.	.
<g/>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
kazatelů	kazatel	k1gMnPc2	kazatel
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Jeroným	Jeroným	k1gMnSc1	Jeroným
Pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
)	)	kIx)	)
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
pevně	pevně	k6eAd1	pevně
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc4d1	současný
řád	řád	k1gInSc4	řád
si	se	k3xPyFc3	se
přáli	přát	k5eAaImAgMnP	přát
vylepšit	vylepšit	k5eAaPmF	vylepšit
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
změnit	změnit	k5eAaPmF	změnit
zcela	zcela	k6eAd1	zcela
od	od	k7c2	od
základů	základ	k1gInPc2	základ
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgMnS	být
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
sjednocené	sjednocený	k2eAgFnSc3d1	sjednocená
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
znovu	znovu	k6eAd1	znovu
naplňovala	naplňovat	k5eAaImAgFnS	naplňovat
ideály	ideál	k1gInPc4	ideál
rané	raný	k2eAgFnSc2d1	raná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Poddaní	poddaný	k1gMnPc1	poddaný
mají	mít	k5eAaImIp3nP	mít
autority	autorita	k1gFnPc4	autorita
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jejich	jejich	k3xOp3gNnSc1	jejich
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
nestojí	stát	k5eNaImIp3nS	stát
proti	proti	k7c3	proti
Boží	boží	k2eAgFnSc3d1	boží
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Kritizovány	kritizován	k2eAgInPc1d1	kritizován
byly	být	k5eAaImAgInP	být
také	také	k9	také
odpustky	odpustek	k1gInPc1	odpustek
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
prodeji	prodej	k1gInSc6	prodej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1412	[number]	k4	1412
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pouliční	pouliční	k2eAgFnSc2d1	pouliční
bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
myšlenky	myšlenka	k1gFnPc1	myšlenka
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
šířily	šířit	k5eAaImAgFnP	šířit
výhradně	výhradně	k6eAd1	výhradně
mezi	mezi	k7c7	mezi
měšťany	měšťan	k1gMnPc7	měšťan
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spojily	spojit	k5eAaPmAgFnP	spojit
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
chiliasmu	chiliasmus	k1gInSc2	chiliasmus
a	a	k8xC	a
nabyly	nabýt	k5eAaPmAgFnP	nabýt
bojovné	bojovný	k2eAgFnPc1d1	bojovná
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Upálení	upálení	k1gNnSc4	upálení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
následky	následek	k1gInPc7	následek
====	====	k?	====
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
byla	být	k5eAaImAgFnS	být
neklidná	klidný	k2eNgFnSc1d1	neklidná
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
komplikována	komplikovat	k5eAaBmNgFnS	komplikovat
papežským	papežský	k2eAgNnSc7d1	papežské
schizmatem	schizma	k1gNnSc7	schizma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Zikmundovo	Zikmundův	k2eAgNnSc4d1	Zikmundovo
naléhání	naléhání	k1gNnSc4	naléhání
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
svolán	svolat	k5eAaPmNgInS	svolat
kostnický	kostnický	k2eAgInSc1d1	kostnický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
trojpapežství	trojpapežství	k1gNnSc2	trojpapežství
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
koncilu	koncil	k1gInSc2	koncil
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
prodiskutovat	prodiskutovat	k5eAaPmF	prodiskutovat
reformu	reforma	k1gFnSc4	reforma
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
herezemi	hereze	k1gFnPc7	hereze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
koncil	koncil	k1gInSc4	koncil
povolán	povolat	k5eAaPmNgMnS	povolat
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
již	již	k6eAd1	již
předem	předem	k6eAd1	předem
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
papeže	papež	k1gMnSc4	papež
za	za	k7c4	za
antikrista	antikrist	k1gMnSc4	antikrist
<g/>
,	,	kIx,	,
autoritu	autorita	k1gFnSc4	autorita
církevního	církevní	k2eAgInSc2d1	církevní
soudu	soud	k1gInSc2	soud
neuznal	uznat	k5eNaPmAgInS	uznat
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
se	se	k3xPyFc4	se
odvolal	odvolat	k5eAaPmAgMnS	odvolat
ke	k	k7c3	k
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Husovo	Husův	k2eAgNnSc4d1	Husovo
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
spis	spis	k1gInSc1	spis
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
(	(	kIx(	(
<g/>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
za	za	k7c4	za
hlavu	hlava	k1gFnSc4	hlava
církve	církev	k1gFnSc2	církev
pokládá	pokládat	k5eAaImIp3nS	pokládat
Krista	Krista	k1gFnSc1	Krista
a	a	k8xC	a
za	za	k7c4	za
její	její	k3xOp3gMnPc4	její
členy	člen	k1gMnPc4	člen
lidi	člověk	k1gMnPc4	člověk
toužící	toužící	k2eAgMnPc1d1	toužící
po	po	k7c6	po
spasení	spasení	k1gNnSc6	spasení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
učení	učení	k1gNnSc1	učení
o	o	k7c6	o
příslušnosti	příslušnost	k1gFnSc6	příslušnost
<g/>
/	/	kIx~	/
<g/>
nepříslušnosti	nepříslušnost	k1gFnSc2	nepříslušnost
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
lidí	člověk	k1gMnPc2	člověk
predestinovaných	predestinovaný	k2eAgMnPc2d1	predestinovaný
k	k	k7c3	k
spáse	spása	k1gFnSc3	spása
<g/>
,	,	kIx,	,
či	či	k8xC	či
zatracení	zatracený	k2eAgMnPc1d1	zatracený
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c4	za
herezi	hereze	k1gFnSc4	hereze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Husova	Husův	k2eAgInSc2d1	Husův
spisu	spis	k1gInSc2	spis
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jedná	jednat	k5eAaImIp3nS	jednat
zle	zle	k6eAd1	zle
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
nejen	nejen	k6eAd1	nejen
hlavou	hlava	k1gFnSc7	hlava
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
její	její	k3xOp3gFnSc7	její
částí	část	k1gFnSc7	část
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
XXIII	XXIII	kA	XXIII
<g/>
,	,	kIx,	,
nařčenému	nařčený	k2eAgInSc3d1	nařčený
z	z	k7c2	z
pirátství	pirátství	k1gNnSc2	pirátství
a	a	k8xC	a
sodomie	sodomie	k1gFnSc2	sodomie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
Hus	Hus	k1gMnSc1	Hus
stále	stále	k6eAd1	stále
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
předán	předán	k2eAgInSc1d1	předán
světské	světský	k2eAgFnSc3d1	světská
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1415	[number]	k4	1415
před	před	k7c7	před
kostnickými	kostnický	k2eAgFnPc7d1	Kostnická
hradbami	hradba	k1gFnPc7	hradba
upálen	upálit	k5eAaPmNgInS	upálit
<g/>
.	.	kIx.	.
</s>
<s>
Zbylý	zbylý	k2eAgInSc4d1	zbylý
popel	popel	k1gInSc4	popel
vhodili	vhodit	k5eAaPmAgMnP	vhodit
katovi	katův	k2eAgMnPc1d1	katův
pacholci	pacholek	k1gMnPc1	pacholek
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Rýna	Rýn	k1gInSc2	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Husovi	Hus	k1gMnSc6	Hus
tak	tak	k6eAd1	tak
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
jeho	jeho	k3xOp3gMnPc1	jeho
přívrženci	přívrženec	k1gMnPc1	přívrženec
uctívat	uctívat	k5eAaImF	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
koncilem	koncil	k1gInSc7	koncil
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
také	také	k9	také
Mistr	mistr	k1gMnSc1	mistr
Jeroným	Jeroným	k1gMnSc1	Jeroným
Pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
postava	postava	k1gFnSc1	postava
pražského	pražský	k2eAgInSc2d1	pražský
reformního	reformní	k2eAgInSc2d1	reformní
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Husovo	Husův	k2eAgNnSc4d1	Husovo
upálení	upálení	k1gNnSc4	upálení
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
zcela	zcela	k6eAd1	zcela
opačný	opačný	k2eAgInSc4d1	opačný
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
než	než	k8xS	než
koncil	koncil	k1gInSc1	koncil
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
–	–	k?	–
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kacířské	kacířský	k2eAgNnSc1d1	kacířské
učení	učení	k1gNnSc1	učení
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
a	a	k8xC	a
Hus	Hus	k1gMnSc1	Hus
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k9	jako
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
požadavků	požadavek	k1gInPc2	požadavek
hnutí	hnutí	k1gNnSc2	hnutí
–	–	k?	–
přijímání	přijímání	k1gNnSc2	přijímání
podobojí	podobojí	k6eAd1	podobojí
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
výrazu	výraz	k1gInSc3	výraz
rovnosti	rovnost	k1gFnSc2	rovnost
duchovních	duchovní	k1gMnPc2	duchovní
a	a	k8xC	a
laiků	laik	k1gMnPc2	laik
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
mezi	mezi	k7c7	mezi
kališníky	kališník	k1gMnPc7	kališník
začaly	začít	k5eAaPmAgFnP	začít
projevovat	projevovat	k5eAaImF	projevovat
i	i	k8xC	i
rozdíly	rozdíl	k1gInPc4	rozdíl
(	(	kIx(	(
<g/>
sociální	sociální	k2eAgInPc4d1	sociální
<g/>
,	,	kIx,	,
názorové	názorový	k2eAgInPc4d1	názorový
i	i	k8xC	i
zájmové	zájmový	k2eAgInPc4d1	zájmový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
několika	několik	k4yIc2	několik
husitských	husitský	k2eAgInPc2d1	husitský
proudů	proud	k1gInPc2	proud
–	–	k?	–
od	od	k7c2	od
umírněného	umírněný	k2eAgInSc2d1	umírněný
proudu	proud	k1gInSc2	proud
až	až	k9	až
po	po	k7c4	po
zcela	zcela	k6eAd1	zcela
radikální	radikální	k2eAgMnPc4d1	radikální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výbuch	výbuch	k1gInSc1	výbuch
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
===	===	k?	===
</s>
</p>
<p>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
vyhrotila	vyhrotit	k5eAaPmAgFnS	vyhrotit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vůdcem	vůdce	k1gMnSc7	vůdce
radikálně	radikálně	k6eAd1	radikálně
smýšlejících	smýšlející	k2eAgMnPc2d1	smýšlející
husitů	husita	k1gMnPc2	husita
stal	stát	k5eAaPmAgMnS	stát
kazatel	kazatel	k1gMnSc1	kazatel
Jan	Jan	k1gMnSc1	Jan
Želivský	želivský	k2eAgMnSc1d1	želivský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svými	svůj	k3xOyFgNnPc7	svůj
strhujícími	strhující	k2eAgNnPc7d1	strhující
kázáními	kázání	k1gNnPc7	kázání
rychle	rychle	k6eAd1	rychle
velké	velký	k2eAgFnSc2d1	velká
obliby	obliba	k1gFnSc2	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vzestupu	vzestup	k1gInSc3	vzestup
revolučních	revoluční	k2eAgFnPc2d1	revoluční
nálad	nálada	k1gFnPc2	nálada
zabránit	zabránit	k5eAaPmF	zabránit
a	a	k8xC	a
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
dosadil	dosadit	k5eAaPmAgInS	dosadit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
června	červen	k1gInSc2	červen
1419	[number]	k4	1419
nové	nový	k2eAgFnPc4d1	nová
konšely	konšel	k1gMnPc7	konšel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zatkli	zatknout	k5eAaPmAgMnP	zatknout
několik	několik	k4yIc4	několik
novoměstských	novoměstský	k2eAgMnPc2d1	novoměstský
husitů	husita	k1gMnPc2	husita
<g/>
.	.	kIx.	.
</s>
<s>
Netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
husité	husita	k1gMnPc1	husita
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Želivského	želivský	k2eAgInSc2d1	želivský
napadli	napadnout	k5eAaPmAgMnP	napadnout
Novoměstskou	novoměstský	k2eAgFnSc4d1	Novoměstská
radnici	radnice	k1gFnSc4	radnice
a	a	k8xC	a
konšely	konšel	k1gMnPc7	konšel
vyházeli	vyházet	k5eAaPmAgMnP	vyházet
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
okno	okno	k1gNnSc4	okno
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
nazývá	nazývat	k5eAaImIp3nS	nazývat
defenestrace	defenestrace	k1gFnSc1	defenestrace
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ubili	ubít	k5eAaPmAgMnP	ubít
<g/>
.	.	kIx.	.
</s>
<s>
Kronikáři	kronikář	k1gMnPc1	kronikář
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
postihl	postihnout	k5eAaPmAgMnS	postihnout
Václava	Václav	k1gMnSc4	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
hradu	hrad	k1gInSc6	hrad
záchvat	záchvat	k1gInSc1	záchvat
hněvu	hněv	k1gInSc2	hněv
(	(	kIx(	(
<g/>
asi	asi	k9	asi
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gInSc7	jehož
vlivem	vliv	k1gInSc7	vliv
mu	on	k3xPp3gMnSc3	on
rychle	rychle	k6eAd1	rychle
ubývalo	ubývat	k5eAaImAgNnS	ubývat
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
během	během	k7c2	během
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
zemřel	zemřít	k5eAaPmAgInS	zemřít
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c4	v
dlouholeté	dlouholetý	k2eAgInPc4d1	dlouholetý
války	válek	k1gInPc4	válek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
jediným	jediný	k2eAgMnSc7d1	jediný
právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
dědicem	dědic	k1gMnSc7	dědic
českého	český	k2eAgInSc2d1	český
trůnu	trůn	k1gInSc2	trůn
stal	stát	k5eAaPmAgMnS	stát
uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
katolická	katolický	k2eAgFnSc1d1	katolická
či	či	k8xC	či
husitská	husitský	k2eAgFnSc1d1	husitská
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
byl	být	k5eAaImAgInS	být
vypracován	vypracován	k2eAgInSc1d1	vypracován
seznam	seznam	k1gInSc1	seznam
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
za	za	k7c2	za
jakých	jaký	k3yRgFnPc2	jaký
měl	mít	k5eAaImAgMnS	mít
Zikmund	Zikmund	k1gMnSc1	Zikmund
usednout	usednout	k5eAaPmF	usednout
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Zikmunda	Zikmund	k1gMnSc4	Zikmund
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
vládnout	vládnout	k5eAaImF	vládnout
jako	jako	k8xS	jako
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
nepřijatelné	přijatelný	k2eNgInPc1d1	nepřijatelný
<g/>
.	.	kIx.	.
</s>
<s>
Odjel	odjet	k5eAaPmAgMnS	odjet
tedy	tedy	k9	tedy
do	do	k7c2	do
Vratislavi	Vratislav	k1gFnSc2	Vratislav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
Tábora	Tábor	k1gInSc2	Tábor
===	===	k?	===
</s>
</p>
<p>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
zachvátily	zachvátit	k5eAaPmAgFnP	zachvátit
revoluční	revoluční	k2eAgInPc4d1	revoluční
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
Lužice	Lužice	k1gFnSc1	Lužice
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
věrné	věrný	k2eAgFnSc3d1	věrná
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
radikálních	radikální	k2eAgMnPc2d1	radikální
husitů	husita	k1gMnPc2	husita
se	se	k3xPyFc4	se
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
na	na	k7c4	na
strategicky	strategicky	k6eAd1	strategicky
výhodný	výhodný	k2eAgInSc4d1	výhodný
ostroh	ostroh	k1gInSc4	ostroh
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
hradiště	hradiště	k1gNnSc1	hradiště
(	(	kIx(	(
<g/>
snad	snad	k9	snad
bývalé	bývalý	k2eAgNnSc1d1	bývalé
opidum	opidum	k1gNnSc1	opidum
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Lužnicí	Lužnice	k1gFnSc7	Lužnice
a	a	k8xC	a
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
tady	tady	k6eAd1	tady
nové	nový	k2eAgNnSc4d1	nové
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
biblický	biblický	k2eAgInSc1d1	biblický
název	název	k1gInSc1	název
Tábor	Tábor	k1gInSc1	Tábor
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
hory	hora	k1gFnSc2	hora
Tabor	Tabora	k1gFnPc2	Tabora
blízko	blízko	k7c2	blízko
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Táborité	táborita	k1gMnPc1	táborita
(	(	kIx(	(
<g/>
či	či	k8xC	či
také	také	k9	také
táboři	tábor	k1gMnPc1	tábor
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vlivných	vlivný	k2eAgFnPc2d1	vlivná
chiliastických	chiliastický	k2eAgFnPc2d1	chiliastická
představ	představa	k1gFnPc2	představa
nechtěli	chtít	k5eNaImAgMnP	chtít
žít	žít	k5eAaImF	žít
podle	podle	k7c2	podle
dosud	dosud	k6eAd1	dosud
platných	platný	k2eAgInPc2d1	platný
lidských	lidský	k2eAgInPc2d1	lidský
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nedokonalé	dokonalý	k2eNgNnSc4d1	nedokonalé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
Božího	boží	k2eAgInSc2d1	boží
zákona	zákon	k1gInSc2	zákon
–	–	k?	–
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
bratry	bratr	k1gMnPc7	bratr
a	a	k8xC	a
sestrami	sestra	k1gFnPc7	sestra
a	a	k8xC	a
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
města	město	k1gNnSc2	město
odevzdávali	odevzdávat	k5eAaImAgMnP	odevzdávat
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
do	do	k7c2	do
kádí	káď	k1gFnPc2	káď
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
posloužit	posloužit	k5eAaPmF	posloužit
celé	celý	k2eAgFnSc3d1	celá
komunitě	komunita	k1gFnSc3	komunita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázala	dokázat	k5eAaPmAgFnS	dokázat
překonat	překonat	k5eAaPmF	překonat
první	první	k4xOgInSc4	první
kritický	kritický	k2eAgInSc4d1	kritický
rok	rok	k1gInSc4	rok
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgInP	být
ukotveny	ukotven	k2eAgInPc1d1	ukotven
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
venkovem	venkov	k1gInSc7	venkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
táborů	tábor	k1gInPc2	tábor
stáli	stát	k5eAaImAgMnP	stát
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
o	o	k7c6	o
vojenské	vojenský	k2eAgFnSc6d1	vojenská
záležitosti	záležitost	k1gFnSc6	záležitost
se	se	k3xPyFc4	se
starali	starat	k5eAaImAgMnP	starat
čtyři	čtyři	k4xCgFnPc4	čtyři
hejtmané	hejtmaný	k2eAgFnPc4d1	hejtmaný
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
muž	muž	k1gMnSc1	muž
se	s	k7c7	s
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
ze	z	k7c2	z
záškodnických	záškodnický	k2eAgFnPc2d1	záškodnická
válek	válka	k1gFnPc2	válka
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
jako	jako	k9	jako
vynikající	vynikající	k2eAgMnPc4d1	vynikající
vojevůdce	vojevůdce	k1gMnPc4	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
velel	velet	k5eAaImAgInS	velet
vojenským	vojenský	k2eAgFnPc3d1	vojenská
akcím	akce	k1gFnPc3	akce
husitů	husita	k1gMnPc2	husita
jako	jako	k9	jako
úplně	úplně	k6eAd1	úplně
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Tábora	Tábor	k1gInSc2	Tábor
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1420	[number]	k4	1420
začal	začít	k5eAaPmAgMnS	začít
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
sbory	sbor	k1gInPc7	sbor
úspěšně	úspěšně	k6eAd1	úspěšně
operovat	operovat	k5eAaImF	operovat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
jižních	jižní	k2eAgFnPc2d1	jižní
a	a	k8xC	a
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgInS	začít
systematicky	systematicky	k6eAd1	systematicky
podmaňovat	podmaňovat	k5eAaImF	podmaňovat
bašty	bašta	k1gFnPc4	bašta
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Vítězně	vítězně	k6eAd1	vítězně
skončila	skončit	k5eAaPmAgFnS	skončit
i	i	k9	i
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokusila	pokusit	k5eAaPmAgFnS	pokusit
porazit	porazit	k5eAaPmF	porazit
západočeská	západočeský	k2eAgFnSc1d1	Západočeská
katolická	katolický	k2eAgFnSc1d1	katolická
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtyři	čtyři	k4xCgInPc1	čtyři
artikuly	artikul	k1gInPc1	artikul
pražské	pražský	k2eAgFnSc2d1	Pražská
===	===	k?	===
</s>
</p>
<p>
<s>
Společným	společný	k2eAgInSc7d1	společný
programem	program	k1gInSc7	program
husitů	husita	k1gMnPc2	husita
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
čtyři	čtyři	k4xCgInPc1	čtyři
artikuly	artikul	k1gInPc1	artikul
pražské	pražský	k2eAgInPc1d1	pražský
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaly	požadovat	k5eAaImAgFnP	požadovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přijímání	přijímání	k1gNnSc1	přijímání
z	z	k7c2	z
kalicha	kalich	k1gInSc2	kalich
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
způsobou	způsoba	k1gFnSc7	způsoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
svobodu	svoboda	k1gFnSc4	svoboda
kázání	kázání	k1gNnSc2	kázání
slova	slovo	k1gNnSc2	slovo
Božího	boží	k2eAgNnSc2d1	boží
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
církev	církev	k1gFnSc4	církev
zbavenou	zbavený	k2eAgFnSc4d1	zbavená
majetku	majetek	k1gInSc3	majetek
a	a	k8xC	a
světské	světský	k2eAgFnSc3d1	světská
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
trestání	trestání	k1gNnSc1	trestání
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
postavení	postavení	k1gNnSc4	postavení
hříšníků	hříšník	k1gMnPc2	hříšník
<g/>
.	.	kIx.	.
<g/>
Splněním	splnění	k1gNnSc7	splnění
těchto	tento	k3xDgInPc2	tento
požadavků	požadavek	k1gInPc2	požadavek
podmiňovala	podmiňovat	k5eAaImAgFnS	podmiňovat
kališnická	kališnický	k2eAgFnSc1d1	kališnická
šlechta	šlechta	k1gFnSc1	šlechta
přijetí	přijetí	k1gNnSc4	přijetí
Zikmunda	Zikmund	k1gMnSc2	Zikmund
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
názorech	názor	k1gInPc6	názor
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
husitské	husitský	k2eAgFnPc1d1	husitská
skupiny	skupina	k1gFnPc1	skupina
výrazně	výrazně	k6eAd1	výrazně
lišily	lišit	k5eAaImAgFnP	lišit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
příčinou	příčina	k1gFnSc7	příčina
vleklých	vleklý	k2eAgFnPc2d1	vleklá
neshod	neshoda	k1gFnPc2	neshoda
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
a	a	k8xC	a
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
===	===	k?	===
</s>
</p>
<p>
<s>
Sjednotit	sjednotit	k5eAaPmF	sjednotit
se	se	k3xPyFc4	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vnějšího	vnější	k2eAgNnSc2d1	vnější
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc3	rok
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
Martin	Martin	k1gMnSc1	Martin
V.	V.	kA	V.
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
na	na	k7c4	na
Zikmundovu	Zikmundův	k2eAgFnSc4d1	Zikmundova
žádost	žádost	k1gFnSc4	žádost
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
sebraným	sebraný	k2eAgNnSc7d1	sebrané
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc2	Vratislav
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
moravská	moravský	k2eAgFnSc1d1	Moravská
katolická	katolický	k2eAgFnSc1d1	katolická
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
napadl	napadnout	k5eAaPmAgMnS	napadnout
vzápětí	vzápětí	k6eAd1	vzápětí
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
dorazil	dorazit	k5eAaPmAgInS	dorazit
až	až	k9	až
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
činilo	činit	k5eAaImAgNnS	činit
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obklíčil	obklíčit	k5eAaPmAgMnS	obklíčit
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgInS	obsadit
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
tísni	tíseň	k1gFnSc6	tíseň
požádali	požádat	k5eAaPmAgMnP	požádat
pražští	pražský	k2eAgMnPc1d1	pražský
husité	husita	k1gMnPc1	husita
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
Táboru	tábor	k1gInSc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Táboři	tábor	k1gMnPc1	tábor
vedení	vedení	k1gNnSc2	vedení
Žižkou	Žižka	k1gMnSc7	Žižka
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
zvratem	zvrat	k1gInSc7	zvrat
v	v	k7c6	v
obléhání	obléhání	k1gNnSc6	obléhání
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
celé	celý	k2eAgFnSc2d1	celá
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
odražení	odražení	k1gNnSc1	odražení
Zikmundových	Zikmundových	k2eAgFnPc2d1	Zikmundových
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pokusily	pokusit	k5eAaPmAgFnP	pokusit
obsadit	obsadit	k5eAaPmF	obsadit
vrch	vrch	k1gInSc4	vrch
Vítkov	Vítkov	k1gInSc4	Vítkov
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
úplně	úplně	k6eAd1	úplně
odřízly	odříznout	k5eAaPmAgFnP	odříznout
Staré	Staré	k2eAgFnPc1d1	Staré
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
od	od	k7c2	od
možnosti	možnost	k1gFnSc2	možnost
zásobování	zásobování	k1gNnSc2	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
fiasku	fiasko	k1gNnSc6	fiasko
finančně	finančně	k6eAd1	finančně
vyčerpaný	vyčerpaný	k2eAgMnSc1d1	vyčerpaný
Zikmund	Zikmund	k1gMnSc1	Zikmund
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
odtáhl	odtáhnout	k5eAaPmAgInS	odtáhnout
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnPc1	jeho
posádky	posádka	k1gFnPc1	posádka
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
a	a	k8xC	a
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Spojenému	spojený	k2eAgNnSc3d1	spojené
vojsku	vojsko	k1gNnSc3	vojsko
táborů	tábor	k1gInPc2	tábor
a	a	k8xC	a
pražanů	pražan	k1gMnPc2	pražan
se	se	k3xPyFc4	se
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
jeho	jeho	k3xOp3gFnPc2	jeho
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
porazili	porazit	k5eAaPmAgMnP	porazit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Zikmundovo	Zikmundův	k2eAgNnSc1d1	Zikmundovo
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1421	[number]	k4	1421
husité	husita	k1gMnPc1	husita
převzali	převzít	k5eAaPmAgMnP	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
královských	královský	k2eAgNnPc2d1	královské
a	a	k8xC	a
velkých	velký	k2eAgNnPc2d1	velké
poddanských	poddanský	k2eAgNnPc2d1	poddanské
měst	město	k1gNnPc2	město
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
východních	východní	k2eAgFnPc6d1	východní
a	a	k8xC	a
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
,	,	kIx,	,
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc1	Kadaň
<g/>
,	,	kIx,	,
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
Louny	Louny	k1gInPc1	Louny
<g/>
,	,	kIx,	,
Slaný	Slaný	k1gInSc1	Slaný
<g/>
,	,	kIx,	,
Beroun	Beroun	k1gInSc1	Beroun
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Kouřim	Kouřim	k1gFnSc1	Kouřim
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
,	,	kIx,	,
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
pražského	pražský	k2eAgInSc2d1	pražský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1421	[number]	k4	1421
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
i	i	k9	i
Zikmundova	Zikmundův	k2eAgFnSc1d1	Zikmundova
posádka	posádka	k1gFnSc1	posádka
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1421	[number]	k4	1421
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
sněm	sněm	k1gInSc1	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
účastníci	účastník	k1gMnPc1	účastník
přijali	přijmout	k5eAaPmAgMnP	přijmout
čtyři	čtyři	k4xCgInPc4	čtyři
artikuly	artikul	k1gInPc4	artikul
pražské	pražský	k2eAgFnSc2d1	Pražská
a	a	k8xC	a
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
Zikmunda	Zikmund	k1gMnSc4	Zikmund
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
obvinili	obvinit	k5eAaPmAgMnP	obvinit
ze	z	k7c2	z
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
království	království	k1gNnSc3	království
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
obyvatelům	obyvatel	k1gMnPc3	obyvatel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc4	svůj
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
od	od	k7c2	od
1421	[number]	k4	1421
do	do	k7c2	do
Žižkovy	Žižkův	k2eAgFnSc2d1	Žižkova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1424	[number]	k4	1424
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
1421	[number]	k4	1421
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
konsolidovala	konsolidovat	k5eAaBmAgFnS	konsolidovat
táborská	táborský	k2eAgFnSc1d1	táborská
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rázně	rázně	k6eAd1	rázně
skoncovala	skoncovat	k5eAaPmAgFnS	skoncovat
s	s	k7c7	s
ultraradikálními	ultraradikální	k2eAgFnPc7d1	ultraradikální
sektami	sekta	k1gFnPc7	sekta
pikartů	pikart	k1gMnPc2	pikart
a	a	k8xC	a
adamitů	adamita	k1gMnPc2	adamita
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
ideály	ideál	k1gInPc1	ideál
a	a	k8xC	a
praktiky	praktika	k1gFnPc1	praktika
hrozily	hrozit	k5eAaImAgFnP	hrozit
ochromit	ochromit	k5eAaPmF	ochromit
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
Tábora	Tábor	k1gInSc2	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1421	[number]	k4	1421
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostal	dostat	k5eAaPmAgMnS	dostat
radikální	radikální	k2eAgMnSc1d1	radikální
kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
Želivský	želivský	k2eAgMnSc1d1	želivský
<g/>
,	,	kIx,	,
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1421	[number]	k4	1421
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
pádu	pád	k1gInSc2	pád
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1422	[number]	k4	1422
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
otevřené	otevřený	k2eAgFnPc4d1	otevřená
Želivského	želivský	k2eAgInSc2d1	želivský
diktatuře	diktatura	k1gFnSc3	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1421	[number]	k4	1421
také	také	k9	také
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
druhá	druhý	k4xOgFnSc1	druhý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
kardinál	kardinál	k1gMnSc1	kardinál
Branda	Brand	k1gMnSc2	Brand
di	di	k?	di
Castiglione	Castiglion	k1gInSc5	Castiglion
a	a	k8xC	a
říšská	říšský	k2eAgNnPc4d1	říšské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
výpravě	výprava	k1gFnSc3	výprava
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
i	i	k9	i
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
a	a	k8xC	a
Albrecht	Albrecht	k1gMnSc1	Albrecht
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
husitstvím	husitství	k1gNnSc7	husitství
ovlivněnou	ovlivněný	k2eAgFnSc7d1	ovlivněná
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vytáhl	vytáhnout	k5eAaPmAgInS	vytáhnout
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kutnou	kutný	k2eAgFnSc4d1	Kutná
Horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
spojeným	spojený	k2eAgNnSc7d1	spojené
husitským	husitský	k2eAgNnSc7d1	husitské
vojskem	vojsko	k1gNnSc7	vojsko
pražanů	pražan	k1gMnPc2	pražan
a	a	k8xC	a
táborů	tábor	k1gMnPc2	tábor
vedeným	vedený	k2eAgMnSc7d1	vedený
Žižkou	Žižka	k1gMnSc7	Žižka
ale	ale	k8xC	ale
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
husity	husita	k1gMnPc4	husita
dobyt	dobyt	k2eAgMnSc1d1	dobyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1423	[number]	k4	1423
vyvrcholily	vyvrcholit	k5eAaPmAgInP	vyvrcholit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spory	spor	k1gInPc1	spor
nekompromisního	kompromisní	k2eNgMnSc2d1	nekompromisní
Žižky	Žižka	k1gMnSc2	Žižka
s	s	k7c7	s
táborskými	táborský	k2eAgMnPc7d1	táborský
kněžími	kněz	k1gMnPc7	kněz
jeho	jeho	k3xOp3gInSc7	jeho
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
východních	východní	k2eAgFnPc2d1	východní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
věrných	věrný	k2eAgMnPc2d1	věrný
a	a	k8xC	a
početných	početný	k2eAgMnPc2d1	početný
východočeských	východočeský	k2eAgMnPc2d1	východočeský
husitů	husita	k1gMnPc2	husita
(	(	kIx(	(
<g/>
orebitů	orebita	k1gMnPc2	orebita
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
velká	velký	k2eAgNnPc4d1	velké
východočeská	východočeský	k2eAgNnPc4d1	Východočeské
města	město	k1gNnPc4	město
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
svého	svůj	k3xOyFgMnSc2	svůj
spojence	spojenec	k1gMnSc2	spojenec
Diviše	Diviš	k1gMnSc2	Diviš
Bořka	Bořek	k1gMnSc2	Bořek
z	z	k7c2	z
Miletínka	Miletínko	k1gNnSc2	Miletínko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Jaroměř	Jaroměř	k1gFnSc4	Jaroměř
a	a	k8xC	a
Čáslav	Čáslav	k1gFnSc4	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
bojovně	bojovně	k6eAd1	bojovně
naladěnému	naladěný	k2eAgMnSc3d1	naladěný
Žižkovi	Žižka	k1gMnSc3	Žižka
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1423	[number]	k4	1423
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
svatohavelská	svatohavelský	k2eAgFnSc1d1	Svatohavelská
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
spojili	spojit	k5eAaPmAgMnP	spojit
umírnění	umírněný	k2eAgMnPc1d1	umírněný
kališníci	kališník	k1gMnPc1	kališník
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vojsko	vojsko	k1gNnSc1	vojsko
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
Žižkou	Žižka	k1gMnSc7	Žižka
poraženo	porazit	k5eAaPmNgNnS	porazit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1424	[number]	k4	1424
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Malešova	Malešův	k2eAgInSc2d1	Malešův
a	a	k8xC	a
přestože	přestože	k8xS	přestože
ten	ten	k3xDgMnSc1	ten
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
zakončených	zakončený	k2eAgInPc2d1	zakončený
tzv.	tzv.	kA	tzv.
libeňskou	libeňský	k2eAgFnSc7d1	Libeňská
smlouvou	smlouva	k1gFnSc7	smlouva
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
zdickými	zdický	k2eAgFnPc7d1	Zdická
úmluvami	úmluva	k1gFnPc7	úmluva
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1424	[number]	k4	1424
Žižka	Žižka	k1gMnSc1	Žižka
během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
na	na	k7c4	na
Zikmundem	Zikmund	k1gMnSc7	Zikmund
ovládanou	ovládaný	k2eAgFnSc4d1	ovládaná
Moravu	Morava	k1gFnSc4	Morava
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léta	léto	k1gNnSc2	léto
1424	[number]	k4	1424
<g/>
–	–	k?	–
<g/>
1427	[number]	k4	1427
===	===	k?	===
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Žižkově	Žižkův	k2eAgFnSc6d1	Žižkova
smrti	smrt	k1gFnSc6	smrt
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
zdických	zdický	k2eAgFnPc2d1	Zdická
dohod	dohoda	k1gFnPc2	dohoda
a	a	k8xC	a
táborský	táborský	k2eAgInSc4d1	táborský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
Žižkovi	Žižkův	k2eAgMnPc1d1	Žižkův
sirotci	sirotek	k1gMnPc1	sirotek
teď	teď	k6eAd1	teď
vedení	vedení	k1gNnSc2	vedení
Janem	Jan	k1gMnSc7	Jan
Hvězdou	Hvězda	k1gMnSc7	Hvězda
<g/>
,	,	kIx,	,
řečeným	řečený	k2eAgNnSc7d1	řečené
Bzdinkou	bzdinka	k1gFnSc7	bzdinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
pražský	pražský	k2eAgInSc4d1	pražský
svaz	svaz	k1gInSc4	svaz
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
vystoupily	vystoupit	k5eAaPmAgFnP	vystoupit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
sféry	sféra	k1gFnPc4	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Bzdinka	bzdinka	k1gFnSc1	bzdinka
dokázal	dokázat	k5eAaPmAgInS	dokázat
spojit	spojit	k5eAaPmF	spojit
táborský	táborský	k2eAgInSc1d1	táborský
a	a	k8xC	a
východočeský	východočeský	k2eAgInSc1d1	východočeský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
táborským	táborský	k2eAgMnSc7d1	táborský
hejtmanem	hejtman	k1gMnSc7	hejtman
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1425	[number]	k4	1425
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
i	i	k9	i
s	s	k7c7	s
pražským	pražský	k2eAgInSc7d1	pražský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyprofiloval	vyprofilovat	k5eAaPmAgInS	vyprofilovat
i	i	k9	i
poslední	poslední	k2eAgInSc1d1	poslední
velký	velký	k2eAgInSc1d1	velký
husitský	husitský	k2eAgInSc1d1	husitský
svaz	svaz	k1gInSc1	svaz
<g/>
:	:	kIx,	:
žatecko-lounský	žateckoounský	k2eAgInSc1d1	žatecko-lounský
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
válek	válka	k1gFnPc2	válka
schopný	schopný	k2eAgMnSc1d1	schopný
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
z	z	k7c2	z
Vřesovic	Vřesovice	k1gFnPc2	Vřesovice
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
svazy	svaz	k1gInPc7	svaz
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
vládlo	vládnout	k5eAaImAgNnS	vládnout
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
vytáhla	vytáhnout	k5eAaPmAgFnS	vytáhnout
jejich	jejich	k3xOp3gNnPc4	jejich
spojená	spojený	k2eAgNnPc4d1	spojené
vojska	vojsko	k1gNnPc4	vojsko
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1426	[number]	k4	1426
proti	proti	k7c3	proti
Ústí	ústí	k1gNnSc3	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
drženému	držený	k2eAgInSc3d1	držený
saským	saský	k2eAgMnSc7d1	saský
markrabětem	markrabě	k1gMnSc7	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Říše	říš	k1gFnSc2	říš
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
vojsko	vojsko	k1gNnSc1	vojsko
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
saských	saský	k2eAgMnPc2d1	saský
<g/>
,	,	kIx,	,
durynských	durynský	k2eAgMnPc2d1	durynský
a	a	k8xC	a
míšeňských	míšeňský	k2eAgMnPc2d1	míšeňský
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1426	[number]	k4	1426
krutě	krutě	k6eAd1	krutě
poraženo	porazit	k5eAaPmNgNnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
Na	na	k7c4	na
Běhání	běhání	k1gNnSc4	běhání
a	a	k8xC	a
město	město	k1gNnSc4	město
vzápětí	vzápětí	k6eAd1	vzápětí
dobyto	dobyt	k2eAgNnSc4d1	dobyto
a	a	k8xC	a
vypleněno	vypleněn	k2eAgNnSc4d1	vypleněno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1427	[number]	k4	1427
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
další	další	k2eAgInSc4d1	další
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
podnícený	podnícený	k2eAgInSc1d1	podnícený
snahou	snaha	k1gFnSc7	snaha
litevského	litevský	k2eAgMnSc2d1	litevský
knížete	kníže	k1gMnSc2	kníže
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Korybutoviče	Korybutovič	k1gMnSc2	Korybutovič
o	o	k7c4	o
upevnění	upevnění	k1gNnSc4	upevnění
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
byli	být	k5eAaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
či	či	k8xC	či
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
odstaveni	odstaven	k2eAgMnPc1d1	odstaven
umírnění	umírněný	k2eAgMnPc1d1	umírněný
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
mistři	mistr	k1gMnPc1	mistr
Křišťan	Křišťan	k1gMnSc1	Křišťan
z	z	k7c2	z
Prachatic	Prachatice	k1gFnPc2	Prachatice
<g/>
,	,	kIx,	,
Prokop	Prokop	k1gMnSc1	Prokop
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Mladoňovic	Mladoňovice	k1gFnPc2	Mladoňovice
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Příbram	Příbram	k1gFnSc1	Příbram
a	a	k8xC	a
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
Korybutovič	Korybutovič	k1gMnSc1	Korybutovič
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Valdštejn	Valdštejna	k1gFnPc2	Valdštejna
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
pražského	pražský	k2eAgNnSc2d1	Pražské
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
Rokycana	Rokycan	k1gMnSc2	Rokycan
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
souměstí	souměstí	k1gNnSc1	souměstí
se	se	k3xPyFc4	se
tak	tak	k9	tak
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přiklonilo	přiklonit	k5eAaPmAgNnS	přiklonit
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
radikálními	radikální	k2eAgInPc7d1	radikální
husitskými	husitský	k2eAgInPc7d1	husitský
svazy	svaz	k1gInPc7	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vrchol	vrchol	k1gInSc1	vrchol
moci	moc	k1gFnSc2	moc
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
1427	[number]	k4	1427
<g/>
–	–	k?	–
<g/>
1431	[number]	k4	1431
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
léta	léto	k1gNnPc4	léto
1427	[number]	k4	1427
až	až	k8xS	až
1431	[number]	k4	1431
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
jakási	jakýsi	k3yIgFnSc1	jakýsi
mocenská	mocenský	k2eAgFnSc1d1	mocenská
stabilita	stabilita	k1gFnSc1	stabilita
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
součinností	součinnost	k1gFnSc7	součinnost
všech	všecek	k3xTgInPc2	všecek
husitských	husitský	k2eAgInPc2d1	husitský
svazů	svaz	k1gInPc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1426	[number]	k4	1426
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
táborského	táborský	k2eAgInSc2d1	táborský
svazu	svaz	k1gInSc2	svaz
objevila	objevit	k5eAaPmAgFnS	objevit
výrazná	výrazný	k2eAgFnSc1d1	výrazná
osobnost	osobnost	k1gFnSc1	osobnost
kněze	kněz	k1gMnSc2	kněz
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
i	i	k9	i
vlivem	vlivem	k7c2	vlivem
svého	svůj	k3xOyFgInSc2	svůj
původu	původ	k1gInSc2	původ
udržovat	udržovat	k5eAaImF	udržovat
živé	živý	k2eAgInPc4d1	živý
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čelní	čelní	k2eAgFnSc6d1	čelní
pozici	pozice	k1gFnSc6	pozice
u	u	k7c2	u
východočeského	východočeský	k2eAgInSc2d1	východočeský
husitského	husitský	k2eAgInSc2d1	husitský
svazu	svaz	k1gInSc2	svaz
byl	být	k5eAaImAgInS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
kněz	kněz	k1gMnSc1	kněz
Ambrožek	ambrožka	k1gFnPc2	ambrožka
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
výrazné	výrazný	k2eAgFnSc2d1	výrazná
duchovní	duchovní	k2eAgFnSc2d1	duchovní
mocenské	mocenský	k2eAgFnSc2d1	mocenská
složky	složka	k1gFnSc2	složka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
husitské	husitský	k2eAgFnSc6d1	husitská
teokracii	teokracie	k1gFnSc6	teokracie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
vydělila	vydělit	k5eAaPmAgFnS	vydělit
složka	složka	k1gFnSc1	složka
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
počátečních	počáteční	k2eAgNnPc2d1	počáteční
let	léto	k1gNnPc2	léto
revoluce	revoluce	k1gFnSc2	revoluce
disponovala	disponovat	k5eAaBmAgNnP	disponovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
profesionální	profesionální	k2eAgFnSc7d1	profesionální
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
stálo	stát	k5eAaImAgNnS	stát
několik	několik	k4yIc1	několik
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
každý	každý	k3xTgInSc1	každý
husitský	husitský	k2eAgInSc1d1	husitský
svaz	svaz	k1gInSc1	svaz
disponoval	disponovat	k5eAaBmAgInS	disponovat
množstvím	množství	k1gNnSc7	množství
ovládaných	ovládaný	k2eAgNnPc2d1	ovládané
měst	město	k1gNnPc2	město
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
posádkami	posádka	k1gFnPc7	posádka
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
šlechticů	šlechtic	k1gMnPc2	šlechtic
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
družinami	družina	k1gFnPc7	družina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
katolická	katolický	k2eAgFnSc1d1	katolická
šlechta	šlechta	k1gFnSc1	šlechta
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
husitským	husitský	k2eAgMnPc3d1	husitský
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
,	,	kIx,	,
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
výraznýma	výrazný	k2eAgFnPc7d1	výrazná
katolickýma	katolický	k2eAgFnPc7d1	katolická
enklávami	enkláva	k1gFnPc7	enkláva
kromě	kromě	k7c2	kromě
panství	panství	k1gNnSc2	panství
Oldřicha	Oldřich	k1gMnSc2	Oldřich
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Plzeň	Plzeň	k1gFnSc1	Plzeň
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
zázemím	zázemí	k1gNnSc7	zázemí
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
i	i	k9	i
oblast	oblast	k1gFnSc1	oblast
katolického	katolický	k2eAgNnSc2d1	katolické
lužického	lužický	k2eAgNnSc2d1	Lužické
Šestiměstí	Šestiměstí	k1gNnSc2	Šestiměstí
s	s	k7c7	s
Žitavou	Žitava	k1gFnSc7	Žitava
a	a	k8xC	a
Zhořelcem	Zhořelec	k1gInSc7	Zhořelec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
plnilo	plnit	k5eAaImAgNnS	plnit
funkci	funkce	k1gFnSc4	funkce
výzvědnou	výzvědný	k2eAgFnSc4d1	výzvědná
a	a	k8xC	a
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
husitských	husitský	k2eAgNnPc2d1	husitské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Výzvědné	výzvědný	k2eAgFnPc1d1	výzvědná
zprávy	zpráva	k1gFnPc1	zpráva
byly	být	k5eAaImAgFnP	být
posílány	posílat	k5eAaImNgFnP	posílat
ze	z	k7c2	z
severočeských	severočeský	k2eAgInPc2d1	severočeský
hradů	hrad	k1gInPc2	hrad
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
<g/>
,	,	kIx,	,
Grabštejn	Grabštejn	k1gNnSc1	Grabštejn
<g/>
,	,	kIx,	,
Hamrštejn	Hamrštejn	k1gNnSc1	Hamrštejn
<g/>
,	,	kIx,	,
Jablonné	Jablonné	k1gNnSc1	Jablonné
<g/>
,	,	kIx,	,
Stráž	stráž	k1gFnSc1	stráž
pod	pod	k7c7	pod
Ralskem	Ralsko	k1gNnSc7	Ralsko
<g/>
,	,	kIx,	,
Děvín	Děvín	k1gInSc1	Děvín
<g/>
,	,	kIx,	,
Ralsko	Ralsko	k1gNnSc1	Ralsko
<g/>
,	,	kIx,	,
Milštejn	Milštejn	k1gMnSc1	Milštejn
<g/>
,	,	kIx,	,
Fredevald	Fredevald	k1gMnSc1	Fredevald
<g/>
,	,	kIx,	,
Tolštejn	Tolštejn	k1gMnSc1	Tolštejn
a	a	k8xC	a
Lipý	Lipý	k1gMnSc1	Lipý
<g/>
.	.	kIx.	.
</s>
<s>
Stabilizace	stabilizace	k1gFnSc1	stabilizace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
umožnila	umožnit	k5eAaPmAgFnS	umožnit
husitům	husita	k1gMnPc3	husita
změnit	změnit	k5eAaPmF	změnit
taktiku	taktika	k1gFnSc4	taktika
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
bránění	bránění	k1gNnSc2	bránění
husitství	husitství	k1gNnSc2	husitství
na	na	k7c4	na
území	území	k1gNnSc4	území
Čech	Čechy	k1gFnPc2	Čechy
začali	začít	k5eAaPmAgMnP	začít
husité	husita	k1gMnPc1	husita
podnikat	podnikat	k5eAaImF	podnikat
rejsy	rejs	k1gInPc4	rejs
(	(	kIx(	(
<g/>
spanilé	spanilý	k2eAgFnPc4d1	spanilá
jízdy	jízda	k1gFnPc4	jízda
<g/>
)	)	kIx)	)
do	do	k7c2	do
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
,	,	kIx,	,
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
umožnily	umožnit	k5eAaPmAgInP	umožnit
hospodářsky	hospodářsky	k6eAd1	hospodářsky
odlehčit	odlehčit	k5eAaPmF	odlehčit
neúrodami	neúroda	k1gFnPc7	neúroda
a	a	k8xC	a
válečnými	válečný	k2eAgFnPc7d1	válečná
útrapami	útrapa	k1gFnPc7	útrapa
zničené	zničený	k2eAgFnSc6d1	zničená
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1427	[number]	k4	1427
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
organizaci	organizace	k1gFnSc3	organizace
již	již	k9	již
třetí	třetí	k4xOgFnSc2	třetí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
braniborského	braniborský	k2eAgMnSc2d1	braniborský
markraběte	markrabě	k1gMnSc2	markrabě
Fridricha	Fridrich	k1gMnSc2	Fridrich
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
Fridricha	Fridrich	k1gMnSc2	Fridrich
postavil	postavit	k5eAaPmAgMnS	postavit
trevírský	trevírský	k2eAgMnSc1d1	trevírský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Otto	Otto	k1gMnSc1	Otto
z	z	k7c2	z
Ziegenheimu	Ziegenheim	k1gInSc2	Ziegenheim
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1427	[number]	k4	1427
až	až	k6eAd1	až
ke	k	k7c3	k
Stříbru	stříbro	k1gNnSc3	stříbro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
vojsko	vojsko	k1gNnSc1	vojsko
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
střetem	střet	k1gInSc7	střet
s	s	k7c7	s
husitským	husitský	k2eAgNnSc7d1	husitské
vojskem	vojsko	k1gNnSc7	vojsko
zpanikařilo	zpanikařit	k5eAaPmAgNnS	zpanikařit
a	a	k8xC	a
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
i	i	k9	i
do	do	k7c2	do
Tachova	Tachov	k1gInSc2	Tachov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
vypleněn	vyplenit	k5eAaPmNgInS	vyplenit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
neúspěch	neúspěch	k1gInSc1	neúspěch
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
stal	stát	k5eAaPmAgInS	stát
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
organizaci	organizace	k1gFnSc3	organizace
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
výpravy	výprava	k1gFnSc2	výprava
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
papeže	papež	k1gMnSc2	papež
Martina	Martin	k1gMnSc2	Martin
V.	V.	kA	V.
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
Evžena	Evžen	k1gMnSc2	Evžen
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
kardinála	kardinál	k1gMnSc2	kardinál
Giuliana	Giulian	k1gMnSc2	Giulian
Cesariniho	Cesarini	k1gMnSc2	Cesarini
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Fridricha	Fridrich	k1gMnSc2	Fridrich
Hohenzollernského	hohenzollernský	k2eAgInSc2d1	hohenzollernský
<g/>
,	,	kIx,	,
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1431	[number]	k4	1431
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgNnSc1d1	početné
vojsko	vojsko	k1gNnSc1	vojsko
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
Domažlice	Domažlice	k1gFnPc4	Domažlice
a	a	k8xC	a
chystalo	chystat	k5eAaImAgNnS	chystat
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
však	však	k9	však
křižáci	křižák	k1gMnPc1	křižák
zaslechli	zaslechnout	k5eAaPmAgMnP	zaslechnout
hřmot	hřmot	k1gInSc4	hřmot
kol	kol	k7c2	kol
blížících	blížící	k2eAgInPc2d1	blížící
se	se	k3xPyFc4	se
vozů	vůz	k1gInPc2	vůz
husitského	husitský	k2eAgNnSc2d1	husitské
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
Ktož	Ktož	k1gFnSc1	Ktož
jsú	jsú	k?	jsú
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
,	,	kIx,	,
obrátili	obrátit	k5eAaPmAgMnP	obrátit
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
v	v	k7c6	v
panice	panika	k1gFnSc6	panika
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
českým	český	k2eAgMnPc3d1	český
husitům	husita	k1gMnPc3	husita
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1430	[number]	k4	1430
dokonce	dokonce	k9	dokonce
i	i	k9	i
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1431	[number]	k4	1431
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
neúspěšným	úspěšný	k2eNgMnPc3d1	neúspěšný
<g/>
,	,	kIx,	,
domluvám	domluva	k1gFnPc3	domluva
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
mezi	mezi	k7c7	mezi
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
a	a	k8xC	a
husitskou	husitský	k2eAgFnSc7d1	husitská
delegací	delegace	k1gFnSc7	delegace
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
husitské	husitský	k2eAgFnSc2d1	husitská
účasti	účast	k1gFnSc2	účast
na	na	k7c4	na
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
vyhlášeném	vyhlášený	k2eAgInSc6d1	vyhlášený
církevním	církevní	k2eAgInSc6d1	církevní
koncilu	koncil	k1gInSc6	koncil
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ovšem	ovšem	k9	ovšem
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
na	na	k7c6	na
papežské	papežský	k2eAgFnSc6d1	Papežská
straně	strana	k1gFnSc6	strana
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Norimberští	norimberský	k2eAgMnPc1d1	norimberský
napsali	napsat	k5eAaBmAgMnP	napsat
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1431	[number]	k4	1431
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
jsme	být	k5eAaImIp1nP	být
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
k	k	k7c3	k
Tachovu	Tachov	k1gInSc3	Tachov
a	a	k8xC	a
Domažlicím	Domažlice	k1gFnPc3	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
pustoší	pustošit	k5eAaImIp3nP	pustošit
a	a	k8xC	a
zapalují	zapalovat	k5eAaImIp3nP	zapalovat
široko	široko	k6eAd1	široko
daleko	daleko	k6eAd1	daleko
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
české	český	k2eAgFnSc6d1	Česká
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jen	jen	k9	jen
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Rozpad	rozpad	k1gInSc1	rozpad
husitského	husitský	k2eAgInSc2d1	husitský
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
radikálů	radikál	k1gMnPc2	radikál
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
1434	[number]	k4	1434
===	===	k?	===
</s>
</p>
<p>
<s>
Porážka	porážka	k1gFnSc1	porážka
poslední	poslední	k2eAgFnSc2d1	poslední
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
vedla	vést	k5eAaImAgFnS	vést
kardinála	kardinál	k1gMnSc4	kardinál
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
i	i	k9	i
hlavního	hlavní	k2eAgMnSc2d1	hlavní
předsedajícího	předsedající	k1gMnSc2	předsedající
koncilu	koncil	k1gInSc2	koncil
Giuliana	Giulian	k1gMnSc4	Giulian
Cesariniho	Cesarini	k1gMnSc4	Cesarini
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
účastníky	účastník	k1gMnPc4	účastník
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgMnPc4d1	nutný
husity	husita	k1gMnPc4	husita
pozvat	pozvat	k5eAaPmF	pozvat
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
návrh	návrh	k1gInSc4	návrh
přijali	přijmout	k5eAaPmAgMnP	přijmout
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stanovily	stanovit	k5eAaPmAgFnP	stanovit
podmínky	podmínka	k1gFnPc1	podmínka
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
úspěšně	úspěšně	k6eAd1	úspěšně
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
1432	[number]	k4	1432
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
chebské	chebský	k2eAgFnPc4d1	Chebská
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husitskému	husitský	k2eAgNnSc3d1	husitské
poselstvu	poselstvo	k1gNnSc3	poselstvo
složenému	složený	k2eAgInSc3d1	složený
z	z	k7c2	z
duchovních	duchovní	k2eAgMnPc2d1	duchovní
i	i	k8xC	i
světských	světský	k2eAgMnPc2d1	světský
zástupců	zástupce	k1gMnPc2	zástupce
všech	všecek	k3xTgInPc2	všecek
svazů	svaz	k1gInPc2	svaz
se	se	k3xPyFc4	se
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
(	(	kIx(	(
<g/>
leden-duben	ledenuben	k2eAgMnSc1d1	leden-duben
1433	[number]	k4	1433
<g/>
)	)	kIx)	)
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
svůj	svůj	k3xOyFgInSc4	svůj
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
uznání	uznání	k1gNnSc4	uznání
čtyř	čtyři	k4xCgFnPc2	čtyři
artikul	artikul	k1gInSc1	artikul
v	v	k7c6	v
celoevropském	celoevropský	k2eAgNnSc6d1	celoevropské
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
celokřesťanském	celokřesťanský	k2eAgMnSc6d1	celokřesťanský
<g/>
)	)	kIx)	)
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
spíše	spíše	k9	spíše
sondovaly	sondovat	k5eAaImAgFnP	sondovat
pozici	pozice	k1gFnSc4	pozice
svého	svůj	k3xOyFgMnSc2	svůj
protivníka	protivník	k1gMnSc2	protivník
a	a	k8xC	a
v	v	k7c6	v
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
během	během	k7c2	během
svatotrojického	svatotrojický	k2eAgInSc2d1	svatotrojický
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1433	[number]	k4	1433
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
husitů	husita	k1gMnPc2	husita
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
změnili	změnit	k5eAaPmAgMnP	změnit
strategii	strategie	k1gFnSc4	strategie
a	a	k8xC	a
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
závaznost	závaznost	k1gFnSc4	závaznost
artikul	artikul	k1gInSc4	artikul
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
obyvatele	obyvatel	k1gMnPc4	obyvatel
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznějšímu	výrazný	k2eAgInSc3d2	výraznější
rozkolu	rozkol	k1gInSc3	rozkol
mezi	mezi	k7c7	mezi
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
husity	husita	k1gMnPc7	husita
a	a	k8xC	a
radikálními	radikální	k2eAgInPc7d1	radikální
bloky	blok	k1gInPc7	blok
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
husitskou	husitský	k2eAgFnSc7d1	husitská
delegací	delegace	k1gFnSc7	delegace
vracející	vracející	k2eAgFnSc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
Basileje	Basilej	k1gFnSc2	Basilej
přijelo	přijet	k5eAaPmAgNnS	přijet
poselstvo	poselstvo	k1gNnSc1	poselstvo
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
husitskými	husitský	k2eAgMnPc7d1	husitský
Čechy	Čech	k1gMnPc7	Čech
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
a	a	k8xC	a
že	že	k8xS	že
největší	veliký	k2eAgFnSc7d3	veliký
překážkou	překážka	k1gFnSc7	překážka
v	v	k7c6	v
dosažení	dosažení	k1gNnSc6	dosažení
dohody	dohoda	k1gFnSc2	dohoda
je	být	k5eAaImIp3nS	být
nejradikálnější	radikální	k2eAgFnSc1d3	nejradikálnější
složka	složka	k1gFnSc1	složka
husitských	husitský	k2eAgFnPc2d1	husitská
Čech	Čechy	k1gFnPc2	Čechy
–	–	k?	–
polní	polní	k2eAgFnSc2d1	polní
vojska	vojsko	k1gNnSc2	vojsko
bratrstev	bratrstvo	k1gNnPc2	bratrstvo
–	–	k?	–
jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
táborského	táborský	k2eAgInSc2d1	táborský
a	a	k8xC	a
východočeského	východočeský	k2eAgInSc2d1	východočeský
sirotčího	sirotčí	k2eAgInSc2d1	sirotčí
<g/>
.	.	kIx.	.
</s>
<s>
Obratný	obratný	k2eAgMnSc1d1	obratný
diplomat	diplomat	k1gMnSc1	diplomat
Jan	Jan	k1gMnSc1	Jan
Polomar	Polomar	k1gMnSc1	Polomar
pak	pak	k6eAd1	pak
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
prosadil	prosadit	k5eAaPmAgMnS	prosadit
novou	nový	k2eAgFnSc4d1	nová
slibnou	slibný	k2eAgFnSc4d1	slibná
linii	linie	k1gFnSc4	linie
postupu	postup	k1gInSc2	postup
proti	proti	k7c3	proti
Čechům	Čech	k1gMnPc3	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Ústupky	ústupek	k1gInPc4	ústupek
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
otázkách	otázka	k1gFnPc6	otázka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
povolení	povolení	k1gNnSc4	povolení
přijímání	přijímání	k1gNnSc2	přijímání
z	z	k7c2	z
kalicha	kalich	k1gInSc2	kalich
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
rozvrátit	rozvrátit	k5eAaPmF	rozvrátit
husitský	husitský	k2eAgInSc4d1	husitský
blok	blok	k1gInSc4	blok
a	a	k8xC	a
tak	tak	k6eAd1	tak
připravit	připravit	k5eAaPmF	připravit
půdu	půda	k1gFnSc4	půda
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
bratrstev	bratrstvo	k1gNnPc2	bratrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Polomarovu	Polomarův	k2eAgFnSc4d1	Polomarův
taktiku	taktika	k1gFnSc4	taktika
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
národem	národ	k1gInSc7	národ
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zacházet	zacházet	k5eAaImF	zacházet
jako	jako	k9	jako
s	s	k7c7	s
koněm	kůň	k1gMnSc7	kůň
nebo	nebo	k8xC	nebo
mezkem	mezek	k1gMnSc7	mezek
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
dlouho	dlouho	k6eAd1	dlouho
mírně	mírně	k6eAd1	mírně
a	a	k8xC	a
pěkně	pěkně	k6eAd1	pěkně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
přehodit	přehodit	k5eAaPmF	přehodit
ohlávka	ohlávka	k1gFnSc1	ohlávka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ho	on	k3xPp3gMnSc4	on
umožní	umožnit	k5eAaPmIp3nS	umožnit
přivázat	přivázat	k5eAaPmF	přivázat
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
koncilem	koncil	k1gInSc7	koncil
svou	svůj	k3xOyFgFnSc4	svůj
šanci	šance	k1gFnSc4	šance
vidělo	vidět	k5eAaImAgNnS	vidět
i	i	k9	i
kališnické	kališnický	k2eAgNnSc1d1	kališnické
panstvo	panstvo	k1gNnSc1	panstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mohlo	moct	k5eAaImAgNnS	moct
upevnit	upevnit	k5eAaPmF	upevnit
své	svůj	k3xOyFgFnPc4	svůj
mocenské	mocenský	k2eAgFnPc4d1	mocenská
pozice	pozice	k1gFnPc4	pozice
vůči	vůči	k7c3	vůči
radikálům	radikál	k1gMnPc3	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
naopak	naopak	k6eAd1	naopak
jednak	jednak	k8xC	jednak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
svých	svůj	k3xOyFgMnPc2	svůj
ideových	ideový	k2eAgMnPc2d1	ideový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
čistě	čistě	k6eAd1	čistě
praktických	praktický	k2eAgFnPc2d1	praktická
pozic	pozice	k1gFnPc2	pozice
(	(	kIx(	(
<g/>
profesionální	profesionální	k2eAgMnPc1d1	profesionální
vojáci	voják	k1gMnPc1	voják
si	se	k3xPyFc3	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
představit	představit	k5eAaPmF	představit
život	život	k1gInSc4	život
bez	bez	k7c2	bez
válečných	válečný	k2eAgInPc2d1	válečný
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
pozice	pozice	k1gFnSc1	pozice
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
již	již	k9	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
zoufalá	zoufalý	k2eAgFnSc1d1	zoufalá
<g/>
)	)	kIx)	)
nebyli	být	k5eNaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
vyjít	vyjít	k5eAaPmF	vyjít
vstříc	vstříc	k7c3	vstříc
požadavkům	požadavek	k1gInPc3	požadavek
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
spanilých	spanilý	k2eAgFnPc6d1	spanilá
jízdách	jízda	k1gFnPc6	jízda
do	do	k7c2	do
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Žiliny	Žilina	k1gFnPc4	Žilina
<g/>
,	,	kIx,	,
Trnavy	Trnava	k1gFnSc2	Trnava
a	a	k8xC	a
Kežmarku	Kežmarok	k1gInSc2	Kežmarok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1433	[number]	k4	1433
bylo	být	k5eAaImAgNnS	být
podniknuto	podniknut	k2eAgNnSc1d1	podniknuto
husitské	husitský	k2eAgNnSc1d1	husitské
tažení	tažení	k1gNnSc1	tažení
k	k	k7c3	k
Baltu	Balt	k1gInSc3	Balt
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Polsku	Polsko	k1gNnSc3	Polsko
proti	proti	k7c3	proti
Řádu	řád	k1gInSc3	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
sirotčí	sirotčí	k2eAgNnSc1d1	sirotčí
vojsko	vojsko	k1gNnSc1	vojsko
ihned	ihned	k6eAd1	ihned
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
obléhání	obléhání	k1gNnSc2	obléhání
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poselstvo	poselstvo	k1gNnSc1	poselstvo
koncilu	koncil	k1gInSc2	koncil
opouštělo	opouštět	k5eAaImAgNnS	opouštět
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
vracelo	vracet	k5eAaImAgNnS	vracet
se	se	k3xPyFc4	se
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
,	,	kIx,	,
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1433	[number]	k4	1433
táborské	táborský	k2eAgNnSc4d1	táborské
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
doplněné	doplněný	k2eAgInPc4d1	doplněný
oddíly	oddíl	k1gInPc4	oddíl
některých	některý	k3yIgMnPc2	některý
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
pilíř	pilíř	k1gInSc4	pilíř
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
tím	ten	k3xDgNnSc7	ten
chtěl	chtít	k5eAaImAgInS	chtít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
sílu	síla	k1gFnSc4	síla
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
vystrašit	vystrašit	k5eAaPmF	vystrašit
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
protivníky	protivník	k1gMnPc4	protivník
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
k	k	k7c3	k
zásadním	zásadní	k2eAgMnPc3d1	zásadní
ústupkům	ústupek	k1gInPc3	ústupek
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
neúrodných	úrodný	k2eNgNnPc2d1	neúrodné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
válečných	válečný	k2eAgFnPc2d1	válečná
útrap	útrapa	k1gFnPc2	útrapa
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
blokády	blokáda	k1gFnSc2	blokáda
sužoval	sužovat	k5eAaImAgInS	sužovat
hlad	hlad	k1gInSc4	hlad
a	a	k8xC	a
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
hospodářství	hospodářství	k1gNnSc1	hospodářství
ani	ani	k8xC	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
úrody	úroda	k1gFnSc2	úroda
a	a	k8xC	a
míru	mír	k1gInSc2	mír
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
trvale	trvale	k6eAd1	trvale
vydržovat	vydržovat	k5eAaImF	vydržovat
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
vojáků	voják	k1gMnPc2	voják
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Táborské	Táborské	k2eAgNnSc1d1	Táborské
vojsko	vojsko	k1gNnSc1	vojsko
před	před	k7c7	před
Plzní	Plzeň	k1gFnSc7	Plzeň
tak	tak	k9	tak
trpělo	trpět	k5eAaImAgNnS	trpět
hlady	hlady	k6eAd1	hlady
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
výprava	výprava	k1gFnSc1	výprava
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
byla	být	k5eAaImAgFnS	být
nedaleko	nedaleko	k7c2	nedaleko
hranic	hranice	k1gFnPc2	hranice
u	u	k7c2	u
Hiltersriedu	Hiltersried	k1gMnSc6	Hiltersried
znenadání	znenadání	k6eAd1	znenadání
napadena	napaden	k2eAgFnSc1d1	napadena
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
mužů	muž	k1gMnPc2	muž
zničena	zničen	k2eAgFnSc1d1	zničena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ležení	ležení	k1gNnSc6	ležení
před	před	k7c7	před
Plzní	Plzeň	k1gFnSc7	Plzeň
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
nepokoje	nepokoj	k1gInSc2	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Bojovníci	bojovník	k1gMnPc1	bojovník
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
z	z	k7c2	z
neúspěchu	neúspěch	k1gInSc2	neúspěch
hejtmany	hejtman	k1gMnPc4	hejtman
<g/>
,	,	kIx,	,
fyzicky	fyzicky	k6eAd1	fyzicky
byl	být	k5eAaImAgInS	být
napaden	napadnout	k5eAaPmNgMnS	napadnout
i	i	k9	i
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poté	poté	k6eAd1	poté
opustil	opustit	k5eAaPmAgMnS	opustit
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skleslé	skleslý	k2eAgFnSc6d1	skleslá
náladě	nálada	k1gFnSc6	nálada
nic	nic	k3yNnSc4	nic
nezměnil	změnit	k5eNaPmAgInS	změnit
ani	ani	k8xC	ani
příchod	příchod	k1gInSc1	příchod
druhého	druhý	k4xOgNnSc2	druhý
polního	polní	k2eAgNnSc2d1	polní
vojska	vojsko	k1gNnSc2	vojsko
–	–	k?	–
sirotků	sirotek	k1gMnPc2	sirotek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1433	[number]	k4	1433
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
výpravy	výprava	k1gFnSc2	výprava
k	k	k7c3	k
Baltu	Balt	k1gInSc3	Balt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
bratrstev	bratrstvo	k1gNnPc2	bratrstvo
se	se	k3xPyFc4	se
odvracel	odvracet	k5eAaImAgInS	odvracet
i	i	k9	i
selský	selský	k2eAgInSc1d1	selský
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
sužovaný	sužovaný	k2eAgInSc1d1	sužovaný
polními	polní	k2eAgNnPc7d1	polní
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
po	po	k7c6	po
venkově	venkov	k1gInSc6	venkov
sháněla	shánět	k5eAaImAgFnS	shánět
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
píci	píce	k1gFnSc4	píce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podzimní	podzimní	k2eAgInSc1d1	podzimní
svatomartinský	svatomartinský	k2eAgInSc1d1	svatomartinský
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zvolil	zvolit	k5eAaPmAgMnS	zvolit
novou	nový	k2eAgFnSc4d1	nová
zemskou	zemský	k2eAgFnSc4d1	zemská
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Alešem	Aleš	k1gMnSc7	Aleš
Vřešťovským	Vřešťovský	k1gMnSc7	Vřešťovský
z	z	k7c2	z
Rýzmburka	Rýzmburek	k1gMnSc2	Rýzmburek
<g/>
.	.	kIx.	.
</s>
<s>
Nezávazně	závazně	k6eNd1	závazně
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
také	také	k9	také
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
opět	opět	k6eAd1	opět
dleli	dlít	k5eAaImAgMnP	dlít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Sílily	sílit	k5eAaImAgFnP	sílit
hlasy	hlas	k1gInPc4	hlas
požadující	požadující	k2eAgNnSc4d1	požadující
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
překážku	překážka	k1gFnSc4	překážka
mírových	mírový	k2eAgFnPc2d1	mírová
dohod	dohoda	k1gFnPc2	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozkol	rozkol	k1gInSc1	rozkol
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
během	během	k7c2	během
jara	jaro	k1gNnSc2	jaro
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
husitů	husita	k1gMnPc2	husita
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
obléhání	obléhání	k1gNnSc1	obléhání
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
pošramotilo	pošramotit	k5eAaPmAgNnS	pošramotit
pověst	pověst	k1gFnSc4	pověst
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
pravice	pravice	k1gFnSc1	pravice
a	a	k8xC	a
střed	střed	k1gInSc1	střed
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nedávno	nedávno	k6eAd1	nedávno
zvoleného	zvolený	k2eAgMnSc4d1	zvolený
zemského	zemský	k2eAgMnSc4d1	zemský
správce	správce	k1gMnSc4	správce
Aleše	Aleš	k1gMnSc4	Aleš
Vřešťovského	Vřešťovský	k1gMnSc4	Vřešťovský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
koaličně	koaličně	k6eAd1	koaličně
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tzv.	tzv.	kA	tzv.
panskou	panská	k1gFnSc4	panská
jednotu	jednota	k1gFnSc4	jednota
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
i	i	k9	i
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Podplacený	podplacený	k2eAgMnSc1d1	podplacený
husitský	husitský	k2eAgMnSc1d1	husitský
hejtman	hejtman	k1gMnSc1	hejtman
Přibík	Přibík	k1gMnSc1	Přibík
z	z	k7c2	z
Klenové	klenový	k2eAgFnSc2d1	Klenová
zrádně	zrádně	k6eAd1	zrádně
zásobil	zásobit	k5eAaPmAgMnS	zásobit
obleženou	obležený	k2eAgFnSc4d1	obležená
Plzeň	Plzeň	k1gFnSc4	Plzeň
a	a	k8xC	a
naděje	nadát	k5eAaBmIp3nS	nadát
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
dobytí	dobytí	k1gNnSc4	dobytí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nedohlednu	nedohledno	k1gNnSc3	nedohledno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
shromažďovalo	shromažďovat	k5eAaImAgNnS	shromažďovat
u	u	k7c2	u
Kačiny	Kačina	k1gFnSc2	Kačina
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
vojsko	vojsko	k1gNnSc1	vojsko
panské	panská	k1gFnSc2	panská
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1434	[number]	k4	1434
mu	on	k3xPp3gMnSc3	on
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
své	svůj	k3xOyFgFnPc4	svůj
brány	brána	k1gFnPc4	brána
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgFnSc2d1	Pražská
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
přepadeno	přepadnout	k5eAaPmNgNnS	přepadnout
a	a	k8xC	a
dobyto	dobýt	k5eAaPmNgNnS	dobýt
také	také	k6eAd1	také
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
i	i	k9	i
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
k	k	k7c3	k
polním	polní	k2eAgNnPc3d1	polní
vojskům	vojsko	k1gNnPc3	vojsko
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgInS	ujmout
se	se	k3xPyFc4	se
organizování	organizování	k1gNnSc3	organizování
odvetných	odvetný	k2eAgFnPc2d1	odvetná
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
bratrská	bratrský	k2eAgNnPc1d1	bratrské
polní	polní	k2eAgNnPc1d1	polní
vojska	vojsko	k1gNnPc1	vojsko
zrušila	zrušit	k5eAaPmAgFnS	zrušit
obléhání	obléhání	k1gNnSc4	obléhání
a	a	k8xC	a
odtáhla	odtáhnout	k5eAaPmAgFnS	odtáhnout
od	od	k7c2	od
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělila	rozdělit	k5eAaPmAgNnP	rozdělit
se	se	k3xPyFc4	se
a	a	k8xC	a
směřovala	směřovat	k5eAaImAgFnS	směřovat
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ovládala	ovládat	k5eAaImAgFnS	ovládat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
doplnila	doplnit	k5eAaPmAgFnS	doplnit
prořídlé	prořídlý	k2eAgFnPc4d1	prořídlá
řady	řada	k1gFnPc4	řada
svých	svůj	k3xOyFgMnPc2	svůj
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
protivníkem	protivník	k1gMnSc7	protivník
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
spojená	spojený	k2eAgNnPc4d1	spojené
polní	polní	k2eAgNnPc4d1	polní
vojska	vojsko	k1gNnPc4	vojsko
setkala	setkat	k5eAaPmAgFnS	setkat
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
však	však	k9	však
byla	být	k5eAaImAgFnS	být
poražena	porazit	k5eAaPmNgFnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
porážkou	porážka	k1gFnSc7	porážka
byla	být	k5eAaImAgFnS	být
radikální	radikální	k2eAgFnSc1d1	radikální
polní	polní	k2eAgFnSc1d1	polní
vojska	vojsko	k1gNnSc2	vojsko
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
rozhodování	rozhodování	k1gNnSc2	rozhodování
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
s	s	k7c7	s
basilejským	basilejský	k2eAgInSc7d1	basilejský
koncilem	koncil	k1gInSc7	koncil
a	a	k8xC	a
se	s	k7c7	s
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
;	;	kIx,	;
kompaktáta	kompaktáta	k1gNnPc4	kompaktáta
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
umírněné	umírněný	k2eAgFnSc6d1	umírněná
podobě	podoba	k1gFnSc6	podoba
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
přijata	přijmout	k5eAaPmNgFnS	přijmout
a	a	k8xC	a
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1436	[number]	k4	1436
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
(	(	kIx(	(
<g/>
Basilejská	basilejský	k2eAgNnPc4d1	Basilejské
kompaktáta	kompaktáta	k1gNnPc4	kompaktáta
<g/>
)	)	kIx)	)
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
koncilního	koncilní	k2eAgMnSc2d1	koncilní
legáta	legát	k1gMnSc2	legát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Důsledky	důsledek	k1gInPc1	důsledek
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
války	válka	k1gFnPc1	válka
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
období	období	k1gNnSc4	období
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
kontroverze	kontroverze	k1gFnPc4	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válečných	válečný	k2eAgNnPc6d1	válečné
letech	léto	k1gNnPc6	léto
upadlo	upadnout	k5eAaPmAgNnS	upadnout
hospodářství	hospodářství	k1gNnSc1	hospodářství
do	do	k7c2	do
katastrofálního	katastrofální	k2eAgInSc2d1	katastrofální
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInPc4d1	obchodní
styky	styk	k1gInPc4	styk
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
byly	být	k5eAaImAgFnP	být
oficiálně	oficiálně	k6eAd1	oficiálně
přerušeny	přerušen	k2eAgFnPc1d1	přerušena
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
výrazně	výrazně	k6eAd1	výrazně
oslabeny	oslaben	k2eAgFnPc1d1	oslabena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
domácí	domácí	k2eAgFnSc1d1	domácí
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
hodnota	hodnota	k1gFnSc1	hodnota
mince	mince	k1gFnSc1	mince
výrazně	výrazně	k6eAd1	výrazně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
oslabení	oslabení	k1gNnSc2	oslabení
těžby	těžba	k1gFnSc2	těžba
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyhnáním	vyhnání	k1gNnSc7	vyhnání
místních	místní	k2eAgMnPc2d1	místní
Němců	Němec	k1gMnPc2	Němec
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Těžkou	těžký	k2eAgFnSc7d1	těžká
zátěží	zátěž	k1gFnSc7	zátěž
bylo	být	k5eAaImAgNnS	být
uživení	uživení	k1gNnSc1	uživení
stálých	stálý	k2eAgNnPc2d1	stálé
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
izolace	izolace	k1gFnSc1	izolace
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
oddálení	oddálení	k1gNnSc1	oddálení
příchodu	příchod	k1gInSc2	příchod
sociálně-ekonomických	sociálněkonomický	k2eAgFnPc2d1	sociálně-ekonomická
změn	změna	k1gFnPc2	změna
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
probíhala	probíhat	k5eAaImAgFnS	probíhat
fáze	fáze	k1gFnSc1	fáze
nástupu	nástup	k1gInSc2	nástup
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
ekonomika	ekonomika	k1gFnSc1	ekonomika
zaostávala	zaostávat	k5eAaImAgFnS	zaostávat
i	i	k9	i
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
ani	ani	k8xC	ani
v	v	k7c6	v
začátku	začátek	k1gInSc6	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
vývoj	vývoj	k1gInSc4	vývoj
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
integritu	integrita	k1gFnSc4	integrita
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
oslabily	oslabit	k5eAaPmAgInP	oslabit
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
animozita	animozita	k1gFnSc1	animozita
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
především	především	k9	především
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
katolickými	katolický	k2eAgMnPc7d1	katolický
Horní	horní	k2eAgFnSc7d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc7d1	dolní
Lužicí	Lužice	k1gFnSc7	Lužice
a	a	k8xC	a
Slezskem	Slezsko	k1gNnSc7	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
úbytku	úbytek	k1gInSc3	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
jak	jak	k6eAd1	jak
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojené	spojený	k2eAgInPc1d1	spojený
hladomory	hladomor	k1gInPc1	hladomor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
epidemie	epidemie	k1gFnSc1	epidemie
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zavlečen	zavleknout	k5eAaPmNgInS	zavleknout
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
neustále	neustále	k6eAd1	neustále
vracel	vracet	k5eAaImAgInS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
2,80	[number]	k4	2,80
<g/>
–	–	k?	–
<g/>
3,37	[number]	k4	3,37
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
odhadů	odhad	k1gInPc2	odhad
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
1,50	[number]	k4	1,50
<g/>
–	–	k?	–
<g/>
1,85	[number]	k4	1,85
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
<g/>
Válčící	válčící	k2eAgFnPc1d1	válčící
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
dopouštěly	dopouštět	k5eAaImAgFnP	dopouštět
masakrů	masakr	k1gInPc2	masakr
zajatých	zajatý	k2eAgMnPc2d1	zajatý
vojáků	voják	k1gMnPc2	voják
i	i	k8xC	i
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
dobytých	dobytý	k2eAgNnPc6d1	dobyté
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
obléhané	obléhaný	k2eAgNnSc1d1	obléhané
město	město	k1gNnSc1	město
odmítalo	odmítat	k5eAaImAgNnS	odmítat
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklady	příklad	k1gInPc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
masakr	masakr	k1gInSc4	masakr
katolických	katolický	k2eAgMnPc2d1	katolický
obyvatel	obyvatel	k1gMnPc2	obyvatel
Chomutova	Chomutov	k1gInSc2	Chomutov
roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
<g/>
,	,	kIx,	,
vhazování	vhazování	k1gNnSc1	vhazování
zajatých	zajatá	k1gFnPc2	zajatá
husitů	husita	k1gMnPc2	husita
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
sympatizantů	sympatizant	k1gMnPc2	sympatizant
do	do	k7c2	do
šachet	šachta	k1gFnPc2	šachta
v	v	k7c6	v
kutnohorských	kutnohorský	k2eAgInPc6d1	kutnohorský
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
masakr	masakr	k1gInSc1	masakr
obránců	obránce	k1gMnPc2	obránce
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1422	[number]	k4	1422
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
upálení	upálení	k1gNnSc1	upálení
700	[number]	k4	700
zajatců	zajatec	k1gMnPc2	zajatec
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
ve	v	k7c6	v
stodolách	stodola	k1gFnPc6	stodola
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
roku	rok	k1gInSc2	rok
1434	[number]	k4	1434
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duchovní	duchovní	k2eAgFnSc1d1	duchovní
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
procházely	procházet	k5eAaImAgFnP	procházet
velkou	velký	k2eAgFnSc7d1	velká
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
venkovských	venkovský	k2eAgFnPc2d1	venkovská
a	a	k8xC	a
městských	městský	k2eAgFnPc2d1	městská
škol	škola	k1gFnPc2	škola
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
<g/>
,	,	kIx,	,
vážně	vážně	k6eAd1	vážně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
i	i	k9	i
význam	význam	k1gInSc1	význam
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
odchodem	odchod	k1gInSc7	odchod
nečeské	český	k2eNgFnSc3d1	nečeská
části	část	k1gFnSc3	část
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
zavřením	zavření	k1gNnSc7	zavření
několika	několik	k4yIc2	několik
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Úlohu	úloha	k1gFnSc4	úloha
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
jako	jako	k8xS	jako
regionálního	regionální	k2eAgNnSc2d1	regionální
centra	centrum	k1gNnSc2	centrum
vzdělání	vzdělání	k1gNnSc2	vzdělání
tak	tak	k8xC	tak
převzala	převzít	k5eAaPmAgFnS	převzít
např.	např.	kA	např.
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
či	či	k8xC	či
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příchod	příchod	k1gInSc1	příchod
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
znamenal	znamenat	k5eAaImAgInS	znamenat
konec	konec	k1gInSc1	konec
stavebního	stavební	k2eAgInSc2d1	stavební
rozmachu	rozmach	k1gInSc2	rozmach
i	i	k8xC	i
rozvoje	rozvoj	k1gInSc2	rozvoj
gotického	gotický	k2eAgNnSc2d1	gotické
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neklidných	klidný	k2eNgFnPc6d1	neklidná
dobách	doba	k1gFnPc6	doba
klesl	klesnout	k5eAaPmAgInS	klesnout
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
budování	budování	k1gNnSc4	budování
nových	nový	k2eAgFnPc2d1	nová
staveb	stavba	k1gFnPc2	stavba
i	i	k8xC	i
tvorbu	tvorba	k1gFnSc4	tvorba
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Chyběly	chybět	k5eAaImAgInP	chybět
také	také	k9	také
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
panovnický	panovnický	k2eAgInSc1d1	panovnický
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zmizel	zmizet	k5eAaPmAgMnS	zmizet
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
zákazníků	zákazník	k1gMnPc2	zákazník
stavebníků	stavebník	k1gMnPc2	stavebník
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
poté	poté	k6eAd1	poté
opustili	opustit	k5eAaPmAgMnP	opustit
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1	stavební
činnost	činnost	k1gFnSc1	činnost
upadala	upadat	k5eAaImAgFnS	upadat
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ustaly	ustat	k5eAaPmAgFnP	ustat
i	i	k9	i
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
rozestavěných	rozestavěný	k2eAgInPc6d1	rozestavěný
objektech	objekt	k1gInPc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
také	také	k9	také
s	s	k7c7	s
katedrálou	katedrála	k1gFnSc7	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
někteří	některý	k3yIgMnPc1	některý
stavitelé	stavitel	k1gMnPc1	stavitel
se	se	k3xPyFc4	se
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
a	a	k8xC	a
zdokonalování	zdokonalování	k1gNnSc6	zdokonalování
opevnění	opevnění	k1gNnPc2	opevnění
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
také	také	k9	také
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
znamenaly	znamenat	k5eAaImAgInP	znamenat
izolaci	izolace	k1gFnSc4	izolace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
oddálení	oddálení	k1gNnSc1	oddálení
příchodu	příchod	k1gInSc2	příchod
renesanční	renesanční	k2eAgFnSc2d1	renesanční
kultury	kultura	k1gFnSc2	kultura
z	z	k7c2	z
(	(	kIx(	(
<g/>
katolické	katolický	k2eAgFnSc2d1	katolická
<g/>
)	)	kIx)	)
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Málokterou	málokterý	k3yIgFnSc4	málokterý
oblast	oblast	k1gFnSc4	oblast
postihly	postihnout	k5eAaPmAgFnP	postihnout
husitské	husitský	k2eAgFnPc1d1	husitská
války	válka	k1gFnPc1	válka
tak	tak	k6eAd1	tak
těžce	těžce	k6eAd1	těžce
jako	jako	k8xS	jako
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
početné	početný	k2eAgInPc4d1	početný
kláštery	klášter	k1gInPc4	klášter
a	a	k8xC	a
kostely	kostel	k1gInPc4	kostel
a	a	k8xC	a
během	během	k7c2	během
obrazoboreckých	obrazoborecký	k2eAgFnPc2d1	obrazoborecká
bouří	bouř	k1gFnPc2	bouř
zničili	zničit	k5eAaPmAgMnP	zničit
mnoho	mnoho	k4c4	mnoho
vynikajících	vynikající	k2eAgNnPc2d1	vynikající
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
a	a	k8xC	a
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1421	[number]	k4	1421
<g/>
–	–	k?	–
<g/>
1424	[number]	k4	1424
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zničeno	zničit	k5eAaPmNgNnS	zničit
14	[number]	k4	14
benediktinských	benediktinský	k2eAgInPc2d1	benediktinský
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
18	[number]	k4	18
proboštství	proboštství	k1gNnPc2	proboštství
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bylo	být	k5eAaImAgNnS	být
usmrceno	usmrtit	k5eAaPmNgNnS	usmrtit
přibližně	přibližně	k6eAd1	přibližně
674	[number]	k4	674
řeholníků	řeholník	k1gMnPc2	řeholník
a	a	k8xC	a
řeholnic	řeholnice	k1gFnPc2	řeholnice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
husitská	husitský	k2eAgNnPc4d1	husitské
vojska	vojsko	k1gNnPc4	vojsko
zničila	zničit	k5eAaPmAgFnS	zničit
16	[number]	k4	16
premonstrátských	premonstrátský	k2eAgInPc2d1	premonstrátský
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
snad	snad	k9	snad
575	[number]	k4	575
řeholníků	řeholník	k1gMnPc2	řeholník
a	a	k8xC	a
řeholnic	řeholnice	k1gFnPc2	řeholnice
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciáckých	cisterciácký	k2eAgInPc2d1	cisterciácký
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
proboštství	proboštství	k1gNnSc1	proboštství
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
33	[number]	k4	33
<g/>
,	,	kIx,	,
pobito	pobít	k5eAaPmNgNnS	pobít
bylo	být	k5eAaImAgNnS	být
3884	[number]	k4	3884
řeholníků	řeholník	k1gMnPc2	řeholník
a	a	k8xC	a
řeholnic	řeholnice	k1gFnPc2	řeholnice
<g/>
.	.	kIx.	.
</s>
<s>
Augustiniáni	augustinián	k1gMnPc1	augustinián
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
40	[number]	k4	40
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
450	[number]	k4	450
členů	člen	k1gMnPc2	člen
řehole	řehole	k1gFnSc2	řehole
<g/>
.	.	kIx.	.
</s>
<s>
Zpustošeno	zpustošen	k2eAgNnSc1d1	zpustošeno
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
44	[number]	k4	44
klášterů	klášter	k1gInPc2	klášter
minoritských	minoritský	k2eAgInPc2d1	minoritský
<g/>
,	,	kIx,	,
37	[number]	k4	37
dominikánských	dominikánský	k2eAgFnPc2d1	Dominikánská
a	a	k8xC	a
3	[number]	k4	3
další	další	k2eAgInPc4d1	další
kláštery	klášter	k1gInPc4	klášter
jiných	jiný	k2eAgInPc2d1	jiný
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
bylo	být	k5eAaImAgNnS	být
pobito	pobít	k5eAaPmNgNnS	pobít
550	[number]	k4	550
řeholníků	řeholník	k1gMnPc2	řeholník
a	a	k8xC	a
řeholnic	řeholnice	k1gFnPc2	řeholnice
<g/>
.	.	kIx.	.
</s>
<s>
Plenění	plenění	k1gNnSc1	plenění
klášterů	klášter	k1gInPc2	klášter
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
stojí	stát	k5eAaImIp3nS	stát
velké	velký	k2eAgNnSc1d1	velké
bohatství	bohatství	k1gNnSc1	bohatství
husitského	husitský	k2eAgInSc2d1	husitský
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
odkazu	odkaz	k1gInSc2	odkaz
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
obohacena	obohacen	k2eAgFnSc1d1	obohacena
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
reformní	reformní	k2eAgFnSc1d1	reformní
část	část	k1gFnSc1	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Husitské	husitský	k2eAgNnSc1d1	husitské
válečnictví	válečnictví	k1gNnSc1	válečnictví
přineslo	přinést	k5eAaPmAgNnS	přinést
do	do	k7c2	do
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
vojenství	vojenství	k1gNnSc2	vojenství
mnoho	mnoho	k4c1	mnoho
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
žoldnéři	žoldnér	k1gMnPc1	žoldnér
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
používali	používat	k5eAaImAgMnP	používat
husitskou	husitský	k2eAgFnSc4d1	husitská
bojovou	bojový	k2eAgFnSc4d1	bojová
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
výzbroj	výzbroj	k1gFnSc4	výzbroj
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
vyhledávaným	vyhledávaný	k2eAgMnPc3d1	vyhledávaný
válečníkům	válečník	k1gMnPc3	válečník
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Jiskra	jiskra	k1gFnSc1	jiskra
z	z	k7c2	z
Brandýsa	Brandýs	k1gInSc2	Brandýs
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
bratříků	bratřík	k1gMnPc2	bratřík
válčil	válčit	k5eAaImAgInS	válčit
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
i	i	k8xC	i
proti	proti	k7c3	proti
expanzi	expanze	k1gFnSc3	expanze
Turků	Turek	k1gMnPc2	Turek
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varny	Varna	k1gFnSc2	Varna
roku	rok	k1gInSc2	rok
1444	[number]	k4	1444
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Schönbergu	Schönberg	k1gInSc2	Schönberg
roku	rok	k1gInSc2	rok
1504	[number]	k4	1504
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
svých	svůj	k3xOyFgMnPc2	svůj
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
lancknechtů	lancknecht	k1gMnPc2	lancknecht
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
na	na	k7c4	na
české	český	k2eAgMnPc4d1	český
žoldnéře	žoldnéř	k1gMnPc4	žoldnéř
vracející	vracející	k2eAgMnPc4d1	vracející
se	se	k3xPyFc4	se
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc7d1	poslední
bitvou	bitva	k1gFnSc7	bitva
ve	v	k7c4	v
které	který	k3yQgMnPc4	který
čeští	český	k2eAgMnPc1d1	český
bojovníci	bojovník	k1gMnPc1	bojovník
použili	použít	k5eAaPmAgMnP	použít
husitskou	husitský	k2eAgFnSc4d1	husitská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
taktiku	taktika	k1gFnSc4	taktika
včetně	včetně	k7c2	včetně
tzv.	tzv.	kA	tzv.
vozových	vozový	k2eAgFnPc2d1	vozová
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
na	na	k7c4	na
1600	[number]	k4	1600
českých	český	k2eAgMnPc2d1	český
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
ocenil	ocenit	k5eAaPmAgMnS	ocenit
jejich	jejich	k3xOp3gFnSc4	jejich
statečnost	statečnost	k1gFnSc4	statečnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
700	[number]	k4	700
přeživších	přeživší	k2eAgMnPc2d1	přeživší
zajatců	zajatec	k1gMnPc2	zajatec
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
propustil	propustit	k5eAaPmAgMnS	propustit
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
požadoval	požadovat	k5eAaImAgMnS	požadovat
výkupné	výkupné	k1gNnSc4	výkupné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
interpretace	interpretace	k1gFnSc1	interpretace
významu	význam	k1gInSc2	význam
husitství	husitství	k1gNnSc2	husitství
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
historiografie	historiografie	k1gFnSc2	historiografie
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
chápal	chápat	k5eAaImAgMnS	chápat
husitství	husitství	k1gNnSc4	husitství
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
českou	český	k2eAgFnSc7d1	Česká
reformací	reformace	k1gFnSc7	reformace
jako	jako	k8xC	jako
vrchol	vrchol	k1gInSc4	vrchol
národních	národní	k2eAgFnPc2d1	národní
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
Josefa	Josef	k1gMnSc4	Josef
Pekaře	Pekař	k1gMnSc4	Pekař
<g/>
,	,	kIx,	,
posuzoval	posuzovat	k5eAaImAgInS	posuzovat
ale	ale	k9	ale
husitskou	husitský	k2eAgFnSc4d1	husitská
revoluci	revoluce	k1gFnSc4	revoluce
příliš	příliš	k6eAd1	příliš
nekriticky	kriticky	k6eNd1	kriticky
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
umělá	umělý	k2eAgNnPc4d1	umělé
a	a	k8xC	a
účelová	účelový	k2eAgNnPc4d1	účelové
spojení	spojení	k1gNnPc4	spojení
se	s	k7c7	s
soudobou	soudobý	k2eAgFnSc7d1	soudobá
sociálně-politickou	sociálněolitický	k2eAgFnSc7d1	sociálně-politická
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgInSc1d1	Palackého
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
husitskou	husitský	k2eAgFnSc4d1	husitská
revoluci	revoluce	k1gFnSc4	revoluce
naopak	naopak	k6eAd1	naopak
podpořila	podpořit	k5eAaPmAgFnS	podpořit
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
hledající	hledající	k2eAgFnSc1d1	hledající
v	v	k7c6	v
husitství	husitství	k1gNnSc6	husitství
společenský	společenský	k2eAgInSc1d1	společenský
zápas	zápas	k1gInSc1	zápas
o	o	k7c4	o
humanitu	humanita	k1gFnSc4	humanita
a	a	k8xC	a
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
moderní	moderní	k2eAgInSc4d1	moderní
český	český	k2eAgInSc4d1	český
národní	národní	k2eAgInSc4d1	národní
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
právě	právě	k9	právě
na	na	k7c6	na
husitském	husitský	k2eAgInSc6d1	husitský
odkazu	odkaz	k1gInSc6	odkaz
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
názorový	názorový	k2eAgInSc1d1	názorový
proud	proud	k1gInSc1	proud
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
učení	učení	k1gNnSc2	učení
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
husitský	husitský	k2eAgInSc4d1	husitský
odkaz	odkaz	k1gInSc4	odkaz
odmítala	odmítat	k5eAaImAgFnS	odmítat
jako	jako	k9	jako
rozrušení	rozrušení	k1gNnSc4	rozrušení
kontinuity	kontinuita	k1gFnSc2	kontinuita
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
korigován	korigován	k2eAgInSc1d1	korigován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Husovi	Hus	k1gMnSc3	Hus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prosazování	prosazování	k1gNnSc1	prosazování
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
vize	vize	k1gFnSc2	vize
husitského	husitský	k2eAgInSc2d1	husitský
odkazu	odkaz	k1gInSc2	odkaz
se	se	k3xPyFc4	se
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
společnosti	společnost	k1gFnSc2	společnost
projevilo	projevit	k5eAaPmAgNnS	projevit
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
měla	mít	k5eAaImAgFnS	mít
podíl	podíl	k1gInSc4	podíl
i	i	k9	i
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
vztahu	vztah	k1gInSc2	vztah
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
menšinou	menšina	k1gFnSc7	menšina
a	a	k8xC	a
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
demokratickém	demokratický	k2eAgNnSc6d1	demokratické
prostředí	prostředí	k1gNnSc6	prostředí
první	první	k4xOgFnPc1	první
republiky	republika	k1gFnPc1	republika
se	se	k3xPyFc4	se
však	však	k9	však
vedle	vedle	k7c2	vedle
masarykovských	masarykovský	k2eAgInPc2d1	masarykovský
pohledů	pohled	k1gInPc2	pohled
např.	např.	kA	např.
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Urbánka	Urbánek	k1gMnSc2	Urbánek
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
i	i	k9	i
četní	četný	k2eAgMnPc1d1	četný
ideoví	ideový	k2eAgMnPc1d1	ideový
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
husitství	husitství	k1gNnSc4	husitství
ideově	ideově	k6eAd1	ideově
navázala	navázat	k5eAaPmAgFnS	navázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zapadají	zapadat	k5eAaPmIp3nP	zapadat
do	do	k7c2	do
kontextu	kontext	k1gInSc2	kontext
českého	český	k2eAgInSc2d1	český
reformního	reformní	k2eAgInSc2d1	reformní
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
sehrály	sehrát	k5eAaPmAgFnP	sehrát
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
české	český	k2eAgFnSc6d1	Česká
historiografii	historiografie	k1gFnSc6	historiografie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
interpretace	interpretace	k1gFnPc1	interpretace
se	se	k3xPyFc4	se
především	především	k6eAd1	především
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zřetelně	zřetelně	k6eAd1	zřetelně
promítly	promítnout	k5eAaPmAgInP	promítnout
i	i	k9	i
do	do	k7c2	do
česko-německých	českoěmecký	k2eAgInPc2d1	česko-německý
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prosazení	prosazení	k1gNnSc6	prosazení
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
do	do	k7c2	do
československé	československý	k2eAgFnSc2d1	Československá
historiografie	historiografie	k1gFnSc2	historiografie
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
husitský	husitský	k2eAgInSc4d1	husitský
odkaz	odkaz	k1gInSc4	odkaz
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
nově	nově	k6eAd1	nově
zpolitizován	zpolitizován	k2eAgMnSc1d1	zpolitizován
a	a	k8xC	a
ideologicky	ideologicky	k6eAd1	ideologicky
zcela	zcela	k6eAd1	zcela
zdeformován	zdeformovat	k5eAaPmNgMnS	zdeformovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
sociálního	sociální	k2eAgNnSc2d1	sociální
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
revoluce	revoluce	k1gFnSc2	revoluce
utlačovaných	utlačovaný	k2eAgMnPc2d1	utlačovaný
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prostého	prostý	k2eAgInSc2d1	prostý
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
utlačovatelům	utlačovatel	k1gMnPc3	utlačovatel
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
,	,	kIx,	,
šlechtě	šlechta	k1gFnSc3	šlechta
a	a	k8xC	a
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
hluboce	hluboko	k6eAd1	hluboko
a	a	k8xC	a
negativně	negativně	k6eAd1	negativně
změnil	změnit	k5eAaPmAgInS	změnit
obecné	obecný	k2eAgNnSc4d1	obecné
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
husitské	husitský	k2eAgFnSc6d1	husitská
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
historického	historický	k2eAgNnSc2d1	historické
bádání	bádání	k1gNnSc2	bádání
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
podařilo	podařit	k5eAaPmAgNnS	podařit
znovuotevřít	znovuotevřít	k5eAaPmF	znovuotevřít
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
diskusi	diskuse	k1gFnSc4	diskuse
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
obléhání	obléhání	k1gNnSc6	obléhání
Naumburgu	Naumburg	k1gInSc2	Naumburg
==	==	k?	==
</s>
</p>
<p>
<s>
Legenda	legenda	k1gFnSc1	legenda
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
husitech	husita	k1gMnPc6	husita
obléhajících	obléhající	k2eAgInPc2d1	obléhající
město	město	k1gNnSc4	město
Naumburg	Naumburg	k1gMnSc1	Naumburg
roku	rok	k1gInSc2	rok
1432	[number]	k4	1432
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
prý	prý	k9	prý
tehdy	tehdy	k6eAd1	tehdy
vzkázali	vzkázat	k5eAaPmAgMnP	vzkázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřistupují	přistupovat	k5eNaImIp3nP	přistupovat
na	na	k7c4	na
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
šetření	šetření	k1gNnSc4	šetření
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
obležené	obležený	k2eAgNnSc1d1	obležené
město	město	k1gNnSc1	město
nepoddá	poddat	k5eNaPmIp3nS	poddat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
do	do	k7c2	do
základů	základ	k1gInPc2	základ
vypáleno	vypálen	k2eAgNnSc1d1	vypáleno
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
pobiti	pobít	k5eAaPmNgMnP	pobít
<g/>
.	.	kIx.	.
</s>
<s>
Měšťané	měšťan	k1gMnPc1	měšťan
se	se	k3xPyFc4	se
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
podrobit	podrobit	k5eAaPmF	podrobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
městského	městský	k2eAgMnSc4d1	městský
zámečníka	zámečník	k1gMnSc4	zámečník
a	a	k8xC	a
do	do	k7c2	do
husitského	husitský	k2eAgNnSc2d1	husitské
ležení	ležení	k1gNnSc2	ležení
vyslali	vyslat	k5eAaPmAgMnP	vyslat
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Husitům	husita	k1gMnPc3	husita
se	se	k3xPyFc4	se
dětí	dítě	k1gFnPc2	dítě
zželelo	zželet	k5eAaPmAgNnS	zželet
<g/>
,	,	kIx,	,
pohostili	pohostit	k5eAaPmAgMnP	pohostit
je	on	k3xPp3gFnPc4	on
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
natrhali	natrhat	k5eAaBmAgMnP	natrhat
třešně	třešeň	k1gFnSc2	třešeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
slaví	slavit	k5eAaImIp3nP	slavit
Kirschenfest	Kirschenfest	k1gFnSc4	Kirschenfest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
1391	[number]	k4	1391
založení	založení	k1gNnSc1	založení
Betlémské	betlémský	k2eAgFnSc2d1	Betlémská
kaple	kaple	k1gFnSc2	kaple
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
1392	[number]	k4	1392
Pravidla	pravidlo	k1gNnPc4	pravidlo
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
Matěje	Matěj	k1gMnSc2	Matěj
z	z	k7c2	z
Janova	Janov	k1gInSc2	Janov
</s>
</p>
<p>
<s>
1402	[number]	k4	1402
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
kazatelem	kazatel	k1gMnSc7	kazatel
v	v	k7c6	v
Betlémské	betlémský	k2eAgFnSc6d1	Betlémská
kapli	kaple	k1gFnSc6	kaple
</s>
</p>
<p>
<s>
1403	[number]	k4	1403
odsouzení	odsouzení	k1gNnSc1	odsouzení
45	[number]	k4	45
článků	článek	k1gInPc2	článek
v	v	k7c6	v
Betlémské	betlémský	k2eAgFnSc6d1	Betlémská
kapli	kaple	k1gFnSc6	kaple
</s>
</p>
<p>
<s>
1408	[number]	k4	1408
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
se	se	k3xPyFc4	se
distancuje	distancovat	k5eAaBmIp3nS	distancovat
od	od	k7c2	od
reformního	reformní	k2eAgInSc2d1	reformní
kruhu	kruh	k1gInSc2	kruh
</s>
</p>
<p>
<s>
1409	[number]	k4	1409
Dekret	dekret	k1gInSc1	dekret
kutnohorský	kutnohorský	k2eAgInSc1d1	kutnohorský
–	–	k?	–
vydaný	vydaný	k2eAgInSc1d1	vydaný
Václavem	Václav	k1gMnSc7	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1412	[number]	k4	1412
odpustkové	odpustkový	k2eAgFnPc4d1	odpustková
bouře	bouř	k1gFnPc4	bouř
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
poprava	poprava	k1gFnSc1	poprava
tří	tři	k4xCgMnPc2	tři
mladíků	mladík	k1gMnPc2	mladík
<g/>
;	;	kIx,	;
Husův	Husův	k2eAgInSc4d1	Husův
odchod	odchod	k1gInSc4	odchod
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
</s>
</p>
<p>
<s>
1413	[number]	k4	1413
Husův	Husův	k2eAgInSc4d1	Husův
spis	spis	k1gInSc4	spis
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
</s>
</p>
<p>
<s>
1414	[number]	k4	1414
počátek	počátek	k1gInSc1	počátek
přijímání	přijímání	k1gNnSc2	přijímání
podobojí	podobojí	k2eAgInPc2d1	podobojí
</s>
</p>
<p>
<s>
1415	[number]	k4	1415
Husova	Husův	k2eAgFnSc1d1	Husova
smrt	smrt	k1gFnSc1	smrt
<g/>
;	;	kIx,	;
protesty	protest	k1gInPc1	protest
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
</s>
</p>
<p>
<s>
1416	[number]	k4	1416
upálení	upálení	k1gNnSc1	upálení
Jeronýma	Jeroným	k1gMnSc2	Jeroným
Pražského	pražský	k2eAgMnSc2d1	pražský
</s>
</p>
<p>
<s>
1417	[number]	k4	1417
radikalizace	radikalizace	k1gFnSc1	radikalizace
</s>
</p>
<p>
<s>
1419	[number]	k4	1419
poutě	pouť	k1gFnPc4	pouť
na	na	k7c4	na
hory	hora	k1gFnPc4	hora
<g/>
;	;	kIx,	;
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
defenestrace	defenestrace	k1gFnSc1	defenestrace
konšelů	konšel	k1gMnPc2	konšel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
smrt	smrt	k1gFnSc1	smrt
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
1420	[number]	k4	1420
založení	založení	k1gNnSc1	založení
Tábora	Tábor	k1gInSc2	Tábor
<g/>
;	;	kIx,	;
Čtyři	čtyři	k4xCgInPc1	čtyři
artikuly	artikul	k1gInPc1	artikul
pražské	pražský	k2eAgInPc1d1	pražský
<g/>
;	;	kIx,	;
porážka	porážka	k1gFnSc1	porážka
první	první	k4xOgFnSc2	první
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
;	;	kIx,	;
smrt	smrt	k1gFnSc1	smrt
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
z	z	k7c2	z
Husi	Hus	k1gFnSc2	Hus
</s>
</p>
<p>
<s>
1421	[number]	k4	1421
vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
táborských	táborský	k2eAgMnPc2d1	táborský
pikartů	pikart	k1gMnPc2	pikart
a	a	k8xC	a
adamitů	adamita	k1gMnPc2	adamita
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
;	;	kIx,	;
čáslavský	čáslavský	k2eAgInSc1d1	čáslavský
sněm	sněm	k1gInSc1	sněm
zvolil	zvolit	k5eAaPmAgInS	zvolit
prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
<g/>
;	;	kIx,	;
porážka	porážka	k1gFnSc1	porážka
druhé	druhý	k4xOgFnSc2	druhý
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
u	u	k7c2	u
Žatce	Žatec	k1gInSc2	Žatec
</s>
</p>
<p>
<s>
1422	[number]	k4	1422
<g/>
–	–	k?	–
<g/>
1427	[number]	k4	1427
litevský	litevský	k2eAgMnSc1d1	litevský
princ	princ	k1gMnSc1	princ
Zikmund	Zikmund	k1gMnSc1	Zikmund
Korybutovič	Korybutovič	k1gMnSc1	Korybutovič
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
správcem	správce	k1gMnSc7	správce
Čech	Čechy	k1gFnPc2	Čechy
</s>
</p>
<p>
<s>
1422	[number]	k4	1422
poprava	poprava	k1gFnSc1	poprava
Jana	Jana	k1gFnSc1	Jana
Želivského	želivský	k2eAgNnSc2d1	Želivské
</s>
</p>
<p>
<s>
1423	[number]	k4	1423
"	"	kIx"	"
<g/>
Menší	malý	k2eAgInSc1d2	menší
Tábor	Tábor	k1gInSc1	Tábor
<g/>
"	"	kIx"	"
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
;	;	kIx,	;
vznik	vznik	k1gInSc1	vznik
panské	panský	k2eAgFnSc2d1	Panská
jednoty	jednota	k1gFnSc2	jednota
</s>
</p>
<p>
<s>
1424	[number]	k4	1424
porážka	porážka	k1gFnSc1	porážka
panské	panský	k2eAgFnSc2d1	Panská
jednoty	jednota	k1gFnSc2	jednota
u	u	k7c2	u
Malešova	Malešův	k2eAgInSc2d1	Malešův
<g/>
;	;	kIx,	;
smrt	smrt	k1gFnSc1	smrt
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
</s>
</p>
<p>
<s>
1426	[number]	k4	1426
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
husitských	husitský	k2eAgNnPc2d1	husitské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
;	;	kIx,	;
porážka	porážka	k1gFnSc1	porážka
Říšského	říšský	k2eAgNnSc2d1	říšské
vojska	vojsko	k1gNnSc2	vojsko
u	u	k7c2	u
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
1427	[number]	k4	1427
zmatený	zmatený	k2eAgInSc4d1	zmatený
útěk	útěk	k1gInSc4	útěk
třetí	třetí	k4xOgFnSc2	třetí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
od	od	k7c2	od
Tachova	Tachov	k1gInSc2	Tachov
<g/>
;	;	kIx,	;
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
období	období	k1gNnSc1	období
spanilých	spanilý	k2eAgFnPc2d1	spanilá
jízd	jízda	k1gFnPc2	jízda
</s>
</p>
<p>
<s>
1429	[number]	k4	1429
schůzka	schůzka	k1gFnSc1	schůzka
se	s	k7c7	s
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
</s>
</p>
<p>
<s>
1431	[number]	k4	1431
porážka	porážka	k1gFnSc1	porážka
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
</s>
</p>
<p>
<s>
1433	[number]	k4	1433
veřejné	veřejný	k2eAgNnSc1d1	veřejné
slyšení	slyšení	k1gNnSc3	slyšení
husitů	husita	k1gMnPc2	husita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
;	;	kIx,	;
výprava	výprava	k1gFnSc1	výprava
sirotků	sirotek	k1gMnPc2	sirotek
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltu	Balt	k1gInSc2	Balt
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1434	[number]	k4	1434
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
<g/>
;	;	kIx,	;
smrt	smrt	k1gFnSc1	smrt
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
<g/>
;	;	kIx,	;
zánik	zánik	k1gInSc1	zánik
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
</s>
</p>
<p>
<s>
1436	[number]	k4	1436
přijetí	přijetí	k1gNnSc1	přijetí
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
<g/>
;	;	kIx,	;
odboj	odboj	k1gInSc1	odboj
Jana	Jan	k1gMnSc2	Jan
Roháče	roháč	k1gMnSc2	roháč
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BARTOŠ	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Husově	Husův	k2eAgFnSc6d1	Husova
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
-	-	kIx~	-
<g/>
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
515	[number]	k4	515
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BARTOŠ	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
Žižkova	Žižkov	k1gInSc2	Žižkov
1415	[number]	k4	1415
<g/>
-	-	kIx~	-
<g/>
1426	[number]	k4	1426
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
236	[number]	k4	236
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BARTOŠ	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
bratrstev	bratrstvo	k1gNnPc2	bratrstvo
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
pád	pád	k1gInSc4	pád
1426	[number]	k4	1426
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BAUM	BAUM	kA	BAUM
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
:	:	kIx,	:
Kostnice	Kostnice	k1gFnSc1	Kostnice
<g/>
,	,	kIx,	,
Hus	Hus	k1gMnSc1	Hus
a	a	k8xC	a
války	válka	k1gFnPc1	válka
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
405	[number]	k4	405
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
543	[number]	k4	543
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BEZOLD	BEZOLD	kA	BEZOLD
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc1	von
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
husitství	husitství	k1gNnSc2	husitství
:	:	kIx,	:
Kulturně-historická	Kulturněistorický	k2eAgFnSc1d1	Kulturně-historická
studie	studie	k1gFnSc1	studie
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc4	překlad
A.	A.	kA	A.
Chytil	chytit	k5eAaPmAgMnS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Pelcl	Pelcl	k1gMnSc1	Pelcl
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úvodní	úvodní	k2eAgFnSc7d1	úvodní
statí	stať	k1gFnSc7	stať
Jos	Jos	k1gFnSc2	Jos
<g/>
.	.	kIx.	.
</s>
<s>
Pekaře	Pekař	k1gMnSc4	Pekař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BYSTRICKÝ	bystrický	k2eAgMnSc1d1	bystrický
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
WASKA	WASKA	kA	WASKA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vyhnání	vyhnání	k1gNnSc6	vyhnání
křižáků	křižák	k1gMnPc2	křižák
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
roku	rok	k1gInSc2	rok
1427	[number]	k4	1427
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
vítězství	vítězství	k1gNnSc1	vítězství
u	u	k7c2	u
Stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
Tachova	Tachov	k1gInSc2	Tachov
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
57	[number]	k4	57
s.	s.	k?	s.
</s>
</p>
<p>
<s>
CIRONIS	CIRONIS	kA	CIRONIS
<g/>
,	,	kIx,	,
Petros	Petrosa	k1gFnPc2	Petrosa
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Rokycan	Rokycany	k1gInPc2	Rokycany
<g/>
.	.	kIx.	.
</s>
<s>
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
97	[number]	k4	97
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1378-1437	[number]	k4	1378-1437
:	:	kIx,	:
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
438	[number]	k4	438
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Lipanská	lipanský	k2eAgFnSc1d1	Lipanská
křižovatka	křižovatka	k1gFnSc1	křižovatka
:	:	kIx,	:
příčiny	příčina	k1gFnPc1	příčina
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc1	průběh
a	a	k8xC	a
historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
jedné	jeden	k4xCgFnSc2	jeden
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
277	[number]	k4	277
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7038	[number]	k4	7038
<g/>
-	-	kIx~	-
<g/>
183	[number]	k4	183
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Lipanské	lipanský	k2eAgFnPc1d1	Lipanská
ozvěny	ozvěna	k1gFnPc1	ozvěna
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
:	:	kIx,	:
H	H	kA	H
&	&	k?	&
H	H	kA	H
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
203	[number]	k4	203
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85787	[number]	k4	85787
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1419	[number]	k4	1419
-	-	kIx~	-
První	první	k4xOgFnSc1	první
pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
:	:	kIx,	:
krvavá	krvavý	k2eAgFnSc1d1	krvavá
neděle	neděle	k1gFnSc1	neděle
uprostřed	uprostřed	k7c2	uprostřed
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87341	[number]	k4	87341
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
stíny	stín	k1gInPc1	stín
husitství	husitství	k1gNnSc2	husitství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
482	[number]	k4	482
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
84	[number]	k4	84
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
českých	český	k2eAgFnPc2d1	Česká
kronik	kronika	k1gFnPc2	kronika
:	:	kIx,	:
cesty	cesta	k1gFnSc2	cesta
ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
husitské	husitský	k2eAgFnSc2d1	husitská
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
456	[number]	k4	456
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
590	[number]	k4	590
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
V.	V.	kA	V.
1402	[number]	k4	1402
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
790	[number]	k4	790
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
296	[number]	k4	296
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DOLEJŠÍ	Dolejší	k1gMnSc1	Dolejší
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
KŘÍŽEK	křížek	k1gInSc1	křížek
<g/>
,	,	kIx,	,
Leonid	Leonid	k1gInSc1	Leonid
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
:	:	kIx,	:
vrchol	vrchol	k1gInSc1	vrchol
válečného	válečný	k2eAgNnSc2d1	válečné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
1419	[number]	k4	1419
<g/>
-	-	kIx~	-
<g/>
1434	[number]	k4	1434
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
361	[number]	k4	361
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87057	[number]	k4	87057
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Předhusitské	předhusitský	k2eAgFnPc1d1	předhusitská
Čechy	Čechy	k1gFnPc1	Čechy
1310	[number]	k4	1310
<g/>
-	-	kIx~	-
<g/>
1419	[number]	k4	1419
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
421	[number]	k4	421
s.	s.	k?	s.
</s>
</p>
<p>
<s>
HAZARD	hazard	k1gInSc1	hazard
<g/>
,	,	kIx,	,
Harry	Harr	k1gInPc1	Harr
W.	W.	kA	W.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Crusades	Crusades	k1gInSc1	Crusades
<g/>
.	.	kIx.	.
</s>
<s>
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
fourteenth	fourteenth	k1gMnSc1	fourteenth
and	and	k?	and
fifteenth	fifteenth	k1gMnSc1	fifteenth
centuries	centuries	k1gMnSc1	centuries
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gInSc1	Madison
<g/>
:	:	kIx,	:
University	universita	k1gFnSc2	universita
of	of	k?	of
Wisconsin	Wisconsin	k2eAgInSc1d1	Wisconsin
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
813	[number]	k4	813
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
299	[number]	k4	299
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6670	[number]	k4	6670
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOENSCH	HOENSCH	kA	HOENSCH
<g/>
,	,	kIx,	,
Jörg	Jörg	k1gInSc4	Jörg
Konrad	Konrada	k1gFnPc2	Konrada
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
:	:	kIx,	:
pozdně	pozdně	k6eAd1	pozdně
středověká	středověký	k2eAgFnSc1d1	středověká
dynastie	dynastie	k1gFnSc1	dynastie
celoevropského	celoevropský	k2eAgInSc2d1	celoevropský
významu	význam	k1gInSc2	význam
1308	[number]	k4	1308
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
304	[number]	k4	304
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
518	[number]	k4	518
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KALIVODA	Kalivoda	k1gMnSc1	Kalivoda
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladtelství	Nakladtelství	k1gNnSc1	Nakladtelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
560	[number]	k4	560
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KALIVODA	Kalivoda	k1gMnSc1	Kalivoda
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Filosofia	Filosofia	k1gFnSc1	Filosofia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
367	[number]	k4	367
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7007	[number]	k4	7007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAVKA	Kavka	k1gMnSc1	Kavka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
uprostřed	uprostřed	k7c2	uprostřed
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
290	[number]	k4	290
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
680	[number]	k4	680
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KEJŘ	KEJŘ	kA	KEJŘ
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
265	[number]	k4	265
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KEJŘ	KEJŘ	kA	KEJŘ
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Husův	Husův	k2eAgInSc4d1	Husův
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
235	[number]	k4	235
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KONRAD	KONRAD	kA	KONRAD
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
326	[number]	k4	326
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KOTYK	KOTYK	kA	KOTYK
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
revizi	revize	k1gFnSc4	revize
Husova	Husův	k2eAgInSc2d1	Husův
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
488	[number]	k4	488
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MEZNÍK	mezník	k1gInSc1	mezník
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
Morava	Morava	k1gFnSc1	Morava
1310	[number]	k4	1310
<g/>
-	-	kIx~	-
<g/>
1423	[number]	k4	1423
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
562	[number]	k4	562
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
363	[number]	k4	363
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
;	;	kIx,	;
ŠÁMAL	Šámal	k1gMnSc1	Šámal
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zrození	zrození	k1gNnSc1	zrození
mýtu	mýtus	k1gInSc2	mýtus
:	:	kIx,	:
dva	dva	k4xCgInPc1	dva
životy	život	k1gInPc1	život
husitské	husitský	k2eAgFnSc2d1	husitská
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
504	[number]	k4	504
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
1191	[number]	k4	1191
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOUKUP	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
SVÁTEK	Svátek	k1gMnSc1	Svátek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1	Křížové
výpravy	výprava	k1gFnPc1	výprava
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
vymknutá	vymknutý	k2eAgFnSc1d1	vymknutá
z	z	k7c2	z
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
498	[number]	k4	498
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
české	český	k2eAgFnSc2d1	Česká
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
364	[number]	k4	364
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
74	[number]	k4	74
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
420	[number]	k4	420
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Epilog	epilog	k1gInSc1	epilog
bouřlivého	bouřlivý	k2eAgInSc2d1	bouřlivý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
550	[number]	k4	550
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
76	[number]	k4	76
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
Čechy	Čechy	k1gFnPc1	Čechy
:	:	kIx,	:
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
ideje	idea	k1gFnPc1	idea
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
758	[number]	k4	758
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
468	[number]	k4	468
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Idea	idea	k1gFnSc1	idea
národa	národ	k1gInSc2	národ
v	v	k7c6	v
husitských	husitský	k2eAgFnPc6d1	husitská
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
229	[number]	k4	229
s.	s.	k?	s.
</s>
</p>
<p>
<s>
URBÁNEK	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Lipany	lipan	k1gMnPc4	lipan
a	a	k8xC	a
konec	konec	k1gInSc4	konec
polních	polní	k2eAgNnPc2d1	polní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
262	[number]	k4	262
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Utrakvismus	utrakvismus	k1gInSc1	utrakvismus
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
reformace	reformace	k1gFnSc1	reformace
</s>
</p>
<p>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
války	válka	k1gFnPc1	válka
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Husitství	husitství	k1gNnSc2	husitství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Husitství	husitství	k1gNnSc2	husitství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
O	o	k7c6	o
době	doba	k1gFnSc6	doba
husitské	husitský	k2eAgFnSc2d1	husitská
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Husitství	husitství	k1gNnSc2	husitství
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Husité	husita	k1gMnPc1	husita
–	–	k?	–
Ktož	Ktož	k1gFnSc1	Ktož
jsú	jsú	k?	jsú
boží	boží	k2eAgMnPc1d1	boží
bojovníci	bojovník	k1gMnPc1	bojovník
</s>
</p>
