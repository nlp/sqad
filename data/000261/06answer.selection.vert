<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégr	k1gMnSc1	Šlégr
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1971	[number]	k4	1971
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
prestižního	prestižní	k2eAgInSc2d1	prestižní
spolku	spolek	k1gInSc2	spolek
Triple	tripl	k1gInSc5	tripl
Gold	Gold	k1gMnSc1	Gold
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
OH	OH	kA	OH
a	a	k8xC	a
zisk	zisk	k1gInSc4	zisk
Stanleyova	Stanleyův	k2eAgInSc2d1	Stanleyův
poháru	pohár	k1gInSc2	pohár
<g/>
)	)	kIx)	)
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
člen	člen	k1gInSc1	člen
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
do	do	k7c2	do
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
.	.	kIx.	.
</s>
