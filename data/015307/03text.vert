<s>
Anděla	Anděla	k1gFnSc1
Čisáriková	Čisárikový	k2eAgFnSc1d1
</s>
<s>
Anděla	Anděla	k1gFnSc1
Čisáriková	Čisárikový	k2eAgFnSc1d1
Narození	narození	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
Domamil	Domamil	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Brno	Brno	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
do	do	k7c2
1962	#num#	k4
<g/>
)	)	kIx)
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Čisárik	Čisárik	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Čisárik	Čisárik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Anděla	Anděla	k1gFnSc1
Čisáriková	Čisárikový	k2eAgFnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
Domamil	Domamil	k1gFnSc2
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
loutkoherečka	loutkoherečka	k1gFnSc1
a	a	k8xC
dabérka	dabérka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
v	v	k7c6
Domamili	Domamili	k1gFnSc6
do	do	k7c2
rodiny	rodina	k1gFnSc2
Nechvátalů	Nechvátal	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
absolvovala	absolvovat	k5eAaPmAgFnS
katedru	katedra	k1gFnSc4
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
na	na	k7c6
Divadelní	divadelní	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
slovenským	slovenský	k2eAgMnSc7d1
manželem	manžel	k1gMnSc7
a	a	k8xC
překladatelem	překladatel	k1gMnSc7
Jánem	Ján	k1gMnSc7
Čisárikem	Čisárik	k1gMnSc7
<g/>
,	,	kIx,
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ukončila	ukončit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
divadelní	divadelní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dabingu	dabing	k1gInSc2
se	se	k3xPyFc4
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Anděla	Anděla	k1gFnSc1
Čisáriková	Čisárikový	k2eAgFnSc1d1
zemřela	zemřít	k5eAaPmAgFnS
náhle	náhle	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
brněnském	brněnský	k2eAgInSc6d1
bytě	byt	k1gInSc6
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
svým	svůj	k3xOyFgMnSc7
manželem	manžel	k1gMnSc7
Jánem	Ján	k1gMnSc7
Čisárikem	Čisárik	k1gMnSc7
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
syna	syn	k1gMnSc4
Tomáše	Tomáš	k1gMnSc4
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dcery	dcera	k1gFnSc2
Janu	Jana	k1gFnSc4
a	a	k8xC
Hanu	Hana	k1gFnSc4
Čisárikovy	Čisárikův	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželem	manžel	k1gMnSc7
Tomáše	Tomáš	k1gMnSc2
Čisárika	Čisárik	k1gMnSc2
je	být	k5eAaImIp3nS
herečka	herečka	k1gFnSc1
a	a	k8xC
dabérka	dabérka	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
Slavíková	Slavíková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
taky	taky	k6eAd1
v	v	k7c6
Četnických	četnický	k2eAgFnPc6d1
humoreskách	humoreska	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Encyklopedie	encyklopedie	k1gFnSc2
dějin	dějiny	k1gFnPc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
encyklopedie	encyklopedie	k1gFnPc1
<g/>
.	.	kIx.
<g/>
brna	brna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zemřela	zemřít	k5eAaPmAgFnS
brněnská	brněnský	k2eAgFnSc1d1
loutkoherečka	loutkoherečka	k1gFnSc1
a	a	k8xC
dabérka	dabérka	k1gFnSc1
Anděla	Anděla	k1gFnSc1
Čisáriková	Čisárikový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-29	2018-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Anděla	Anděla	k1gFnSc1
Čisáriková	Čisárikový	k2eAgFnSc1d1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
232451	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
36155190005982131805	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Umění	umění	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
