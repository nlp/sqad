<s>
Seaborgium	Seaborgium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Sg	Sg	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtrnáctým	čtrnáctý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc4d1
uměle	uměle	k6eAd1
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
nebo	nebo	k8xC
urychlovači	urychlovač	k1gInSc6
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>