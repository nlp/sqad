<p>
<s>
Aisha	Aisha	k1gFnSc1	Aisha
Kandisha	Kandisha	k1gFnSc1	Kandisha
je	být	k5eAaImIp3nS	být
bájná	bájný	k2eAgFnSc1d1	bájná
postava	postava	k1gFnSc1	postava
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
severní	severní	k2eAgFnSc2d1	severní
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ženského	ženský	k2eAgMnSc4d1	ženský
démona	démon	k1gMnSc4	démon
<g/>
,	,	kIx,	,
sukuba	sukub	k1gMnSc4	sukub
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
láká	lákat	k5eAaImIp3nS	lákat
muže	muž	k1gMnSc4	muž
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
postavu	postava	k1gFnSc4	postava
nádherné	nádherný	k2eAgFnSc2d1	nádherná
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
drápy	dráp	k1gInPc7	dráp
na	na	k7c6	na
rukách	ruka	k1gFnPc6	ruka
a	a	k8xC	a
s	s	k7c7	s
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
