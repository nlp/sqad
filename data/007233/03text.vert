<s>
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
(	(	kIx(	(
<g/>
Rubroboletus	Rubroboletus	k1gMnSc1	Rubroboletus
satanas	satanas	k1gMnSc1	satanas
(	(	kIx(	(
<g/>
Lenz	Lenz	k1gMnSc1	Lenz
<g/>
)	)	kIx)	)
Kuan	Kuan	k1gMnSc1	Kuan
Zhao	Zhao	k1gMnSc1	Zhao
et	et	k?	et
Zhu	Zhu	k1gFnSc2	Zhu
L.	L.	kA	L.
Yang	Yang	k1gInSc4	Yang
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
houba	houba	k1gFnSc1	houba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hřibovitých	hřibovitý	k2eAgMnPc2d1	hřibovitý
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
modrajících	modrající	k2eAgInPc2d1	modrající
a	a	k8xC	a
barevných	barevný	k2eAgInPc2d1	barevný
hřibů	hřib	k1gInPc2	hřib
<g/>
.	.	kIx.	.
</s>
<s>
Satan	satan	k1gInSc1	satan
je	být	k5eAaImIp3nS	být
také	také	k9	také
největší	veliký	k2eAgInSc1d3	veliký
hřib	hřib	k1gInSc1	hřib
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Boletus	Boletus	k1gMnSc1	Boletus
erythropus	erythropus	k1gMnSc1	erythropus
Krombh	Krombh	k1gMnSc1	Krombh
<g/>
.	.	kIx.	.
1846	[number]	k4	1846
non	non	k?	non
Pers.	Pers.	k1gMnSc1	Pers.
nec	nec	k?	nec
Fr.	Fr.	k1gMnSc1	Fr.
Boletus	Boletus	k1gMnSc1	Boletus
lupinus	lupinus	k1gMnSc1	lupinus
Fries	Fries	k1gMnSc1	Fries
1838	[number]	k4	1838
(	(	kIx(	(
<g/>
non	non	k?	non
Bres	Bres	k1gInSc1	Bres
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Boletus	Boletus	k1gMnSc1	Boletus
lupinus	lupinus	k1gMnSc1	lupinus
Rick	Rick	k1gMnSc1	Rick
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
non	non	k?	non
Bres	Bres	k1gInSc1	Bres
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Boletus	Boletus	k1gInSc1	Boletus
luridus	luridus	k1gInSc1	luridus
Barla	Barla	k1gFnSc1	Barla
non	non	k?	non
Fr.	Fr.	k1gFnSc2	Fr.
nec	nec	k?	nec
Schaeff	Schaeff	k1gInSc1	Schaeff
nec	nec	k?	nec
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Boletus	Boletus	k1gMnSc1	Boletus
marmoreus	marmoreus	k1gMnSc1	marmoreus
Roques	Roques	k1gMnSc1	Roques
Boletus	Boletus	k1gMnSc1	Boletus
sanguineus	sanguineus	k1gMnSc1	sanguineus
Krombh	Krombh	k1gMnSc1	Krombh
<g/>
.	.	kIx.	.
1846	[number]	k4	1846
non	non	k?	non
With	With	k1gMnSc1	With
<g/>
.	.	kIx.	.
</s>
<s>
Boletus	Boletus	k1gMnSc1	Boletus
satanas	satanas	k1gMnSc1	satanas
Lenz	Lenz	k1gInSc4	Lenz
1831	[number]	k4	1831
Boletus	Boletus	k1gMnSc1	Boletus
tuberosus	tuberosus	k1gMnSc1	tuberosus
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
1791	[number]	k4	1791
Tubiporus	Tubiporus	k1gInSc1	Tubiporus
satanas	satanas	k1gInSc4	satanas
(	(	kIx(	(
<g/>
Lenz	Lenz	k1gInSc4	Lenz
<g/>
)	)	kIx)	)
Maire	Mair	k1gMnSc5	Mair
1937	[number]	k4	1937
české	český	k2eAgInPc4d1	český
názvy	název	k1gInPc4	název
hřib	hřib	k1gInSc1	hřib
krvavý	krvavý	k2eAgInSc4d1	krvavý
hřib	hřib	k1gInSc4	hřib
satan	satan	k1gMnSc1	satan
lidové	lidový	k2eAgInPc4d1	lidový
názvy	název	k1gInPc4	název
Červenáč	Červenáč	k1gInSc1	Červenáč
(	(	kIx(	(
<g/>
na	na	k7c6	na
Holicku	Holick	k1gInSc6	Holick
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červenka	červenka	k1gFnSc1	červenka
<g/>
,	,	kIx,	,
bliják	bliják	k1gMnSc1	bliják
<g/>
,	,	kIx,	,
blivák	blivák	k1gMnSc1	blivák
(	(	kIx(	(
<g/>
na	na	k7c6	na
Chlumecku	Chlumeck	k1gInSc6	Chlumeck
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeduvatyj	jeduvatyj	k1gMnSc1	jeduvatyj
doubravník	doubravník	k1gMnSc1	doubravník
<g/>
,	,	kIx,	,
jenerál	jenerál	k?	jenerál
(	(	kIx(	(
<g/>
na	na	k7c6	na
Karlštejnsku	Karlštejnsko	k1gNnSc6	Karlštejnsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
(	(	kIx(	(
<g/>
na	na	k7c6	na
Karlštejnsku	Karlštejnsko	k1gNnSc6	Karlštejnsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karmazín	karmazín	k1gInSc1	karmazín
<g/>
,	,	kIx,	,
krousťák	krousťák	k1gInSc1	krousťák
<g/>
,	,	kIx,	,
krvák	krvák	k1gInSc1	krvák
<g/>
,	,	kIx,	,
růžovák	růžovák	k1gInSc1	růžovák
<g/>
,	,	kIx,	,
safián	safián	k1gInSc1	safián
<g/>
,	,	kIx,	,
umrdlák	umrdlák	k1gInSc1	umrdlák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
byl	být	k5eAaImAgInS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
název	název	k1gInSc1	název
hřib	hřib	k1gInSc1	hřib
krvavý	krvavý	k2eAgInSc1d1	krvavý
nebo	nebo	k8xC	nebo
červenka	červenka	k1gFnSc1	červenka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Čelakovský	Čelakovský	k2eAgInSc4d1	Čelakovský
německý	německý	k2eAgInSc4d1	německý
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
der	drát	k5eAaImRp2nS	drát
Satanspilz	Satanspilz	k1gInSc1	Satanspilz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
české	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
hřib	hřib	k1gInSc1	hřib
satan	satan	k1gMnSc1	satan
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
odborného	odborný	k2eAgInSc2d1	odborný
názvu	název	k1gInSc2	název
Boletus	Boletus	k1gMnSc1	Boletus
satanas	satanas	k1gMnSc1	satanas
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
hřib	hřib	k1gInSc1	hřib
satanův	satanův	k2eAgInSc1d1	satanův
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
houbu	houba	k1gFnSc4	houba
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
německý	německý	k2eAgMnSc1d1	německý
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Harald	Harald	k1gMnSc1	Harald
Othmar	Othmar	k1gMnSc1	Othmar
Lenz	Lenz	k1gMnSc1	Lenz
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
označil	označit	k5eAaPmAgMnS	označit
hřib	hřib	k1gInSc4	hřib
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
společně	společně	k6eAd1	společně
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Karlem	Karel	k1gMnSc7	Karel
Salzmannem	Salzmann	k1gMnSc7	Salzmann
<g/>
,	,	kIx,	,
služkou	služka	k1gFnSc7	služka
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
dámou	dáma	k1gFnSc7	dáma
v	v	k7c6	v
září	září	k1gNnSc6	září
1830	[number]	k4	1830
otrávil	otrávit	k5eAaPmAgMnS	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
(	(	kIx(	(
<g/>
350	[number]	k4	350
<g/>
)	)	kIx)	)
milimetrů	milimetr	k1gInPc2	milimetr
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
sametový	sametový	k2eAgMnSc1d1	sametový
<g/>
,	,	kIx,	,
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
polštářkovitý	polštářkovitý	k2eAgInSc1d1	polštářkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Rourky	rourka	k1gFnPc1	rourka
jsou	být	k5eAaImIp3nP	být
zprvu	zprvu	k6eAd1	zprvu
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
žlutozelené	žlutozelený	k2eAgInPc1d1	žlutozelený
<g/>
,	,	kIx,	,
póry	pór	k1gInPc1	pór
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
zpočátku	zpočátku	k6eAd1	zpočátku
stejně	stejně	k6eAd1	stejně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
jako	jako	k8xC	jako
rourky	rourka	k1gFnPc1	rourka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
klobouku	klobouk	k1gInSc2	klobouk
získávají	získávat	k5eAaImIp3nP	získávat
vínově	vínově	k6eAd1	vínově
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
bývají	bývat	k5eAaImIp3nP	bývat
oranžovočervené	oranžovočervený	k2eAgFnPc1d1	oranžovočervená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
tvoří	tvořit	k5eAaImIp3nP	tvořit
žlutý	žlutý	k2eAgInSc4d1	žlutý
pásek	pásek	k1gInSc4	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Třeň	třeň	k1gInSc1	třeň
je	být	k5eAaImIp3nS	být
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
červený	červený	k2eAgMnSc1d1	červený
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
ke	k	k7c3	k
klobouku	klobouk	k1gInSc3	klobouk
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
do	do	k7c2	do
červenožluta	červenožlut	k1gMnSc2	červenožlut
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
kryje	krýt	k5eAaImIp3nS	krýt
síťka	síťka	k1gFnSc1	síťka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
shora	shora	k6eAd1	shora
až	až	k9	až
do	do	k7c2	do
dolní	dolní	k2eAgFnSc2d1	dolní
poloviny	polovina	k1gFnSc2	polovina
třeně	třeň	k1gInSc2	třeň
<g/>
;	;	kIx,	;
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
je	být	k5eAaImIp3nS	být
oranžově	oranžově	k6eAd1	oranžově
červeně	červeně	k6eAd1	červeně
nebo	nebo	k8xC	nebo
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
podklad	podklad	k1gInSc4	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Dužina	dužina	k1gFnSc1	dužina
je	být	k5eAaImIp3nS	být
bělavá	bělavý	k2eAgFnSc1d1	bělavá
<g/>
,	,	kIx,	,
na	na	k7c6	na
řezu	řez	k1gInSc6	řez
nepatrně	nepatrně	k6eAd1	nepatrně
modrající	modrající	k2eAgFnSc1d1	modrající
<g/>
.	.	kIx.	.
</s>
<s>
Chutná	chutnat	k5eAaImIp3nS	chutnat
jemně	jemně	k6eAd1	jemně
<g/>
,	,	kIx,	,
ořechově	ořechově	k6eAd1	ořechově
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
houba	houba	k1gFnSc1	houba
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
syrova	syrov	k1gInSc2	syrov
silně	silně	k6eAd1	silně
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
dužiny	dužina	k1gFnSc2	dužina
se	se	k3xPyFc4	se
skoro	skoro	k6eAd1	skoro
vůbec	vůbec	k9	vůbec
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
plodnice	plodnice	k1gFnSc1	plodnice
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
výraznějšího	výrazný	k2eAgInSc2d2	výraznější
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
staré	starý	k2eAgFnPc1d1	stará
plodnice	plodnice	k1gFnPc1	plodnice
páchnou	páchnout	k5eAaImIp3nP	páchnout
-	-	kIx~	-
pach	pach	k1gInSc1	pach
bývá	bývat	k5eAaImIp3nS	bývat
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
mršině	mršina	k1gFnSc3	mršina
nebo	nebo	k8xC	nebo
zpoceným	zpocený	k2eAgFnPc3d1	zpocená
nohám	noha	k1gFnPc3	noha
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k7c2wR	povrch
klobouku	klobouk	k1gInSc2	klobouk
kryjí	krýt	k5eAaImIp3nP	krýt
trichodermové	trichodermová	k1gFnPc1	trichodermová
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
μ	μ	k?	μ
široké	široký	k2eAgFnSc2d1	široká
hyfy	hyfa	k1gFnSc2	hyfa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kryté	krytý	k2eAgInPc1d1	krytý
tenkou	tenký	k2eAgFnSc7d1	tenká
gelatinózní	gelatinózní	k2eAgFnSc7d1	gelatinózní
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusy	výtrus	k1gInPc1	výtrus
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
×	×	k?	×
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
μ	μ	k?	μ
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
boletoidního	boletoidní	k2eAgInSc2d1	boletoidní
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
vřetenovité	vřetenovitý	k2eAgInPc1d1	vřetenovitý
až	až	k9	až
elipsovitě	elipsovitě	k6eAd1	elipsovitě
vřetenovité	vřetenovitý	k2eAgInPc4d1	vřetenovitý
se	se	k3xPyFc4	se
suprahilární	suprahilární	k2eAgFnSc7d1	suprahilární
depresí	deprese	k1gFnSc7	deprese
patrnou	patrný	k2eAgFnSc7d1	patrná
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
<g/>
.	.	kIx.	.
</s>
<s>
Satan	satan	k1gInSc1	satan
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vzácně	vzácně	k6eAd1	vzácně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
na	na	k7c6	na
alkalických	alkalický	k2eAgInPc6d1	alkalický
podkladech	podklad	k1gInPc6	podklad
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
teplomilné	teplomilný	k2eAgFnSc2d1	teplomilná
květeny	květena	k1gFnSc2	květena
<g/>
.	.	kIx.	.
</s>
<s>
Pilát	pilát	k1gInSc1	pilát
jej	on	k3xPp3gMnSc4	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
druh	druh	k1gInSc1	druh
teplomilných	teplomilný	k2eAgInPc2d1	teplomilný
lesů	les	k1gInPc2	les
na	na	k7c6	na
vápenatých	vápenatý	k2eAgFnPc6d1	vápenatá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
pak	pak	k6eAd1	pak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
xerotermních	xerotermní	k2eAgInPc2d1	xerotermní
dubohabrových	dubohabrův	k2eAgInPc2d1	dubohabrův
hájů	háj	k1gInPc2	háj
na	na	k7c6	na
vápencích	vápenec	k1gInPc6	vápenec
<g/>
,	,	kIx,	,
skalních	skalní	k2eAgFnPc2d1	skalní
hadcových	hadcový	k2eAgFnPc2d1	hadcová
stepí	step	k1gFnPc2	step
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
listnáči	listnáč	k1gInPc7	listnáč
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nálezů	nález	k1gInPc2	nález
řídícího	řídící	k2eAgMnSc2d1	řídící
učitele	učitel	k1gMnSc2	učitel
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kyselých	kyselý	k2eAgFnPc2d1	kyselá
doubrav	doubrava	k1gFnPc2	doubrava
a	a	k8xC	a
oligotrofních	oligotrofní	k2eAgFnPc2d1	oligotrofní
habrových	habrový	k2eAgFnPc2d1	Habrová
doubrav	doubrava	k1gFnPc2	doubrava
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
především	především	k9	především
pod	pod	k7c7	pod
duby	dub	k1gInPc7	dub
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pod	pod	k7c7	pod
jinými	jiný	k2eAgInPc7d1	jiný
listnáči	listnáč	k1gInPc7	listnáč
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovat	jmenovat	k5eAaImF	jmenovat
lze	lze	k6eAd1	lze
například	například	k6eAd1	například
buk	buk	k1gInSc4	buk
<g/>
,	,	kIx,	,
lípu	lípa	k1gFnSc4	lípa
nebo	nebo	k8xC	nebo
břízu	bříza	k1gFnSc4	bříza
<g/>
.	.	kIx.	.
</s>
<s>
Fruktifikuje	fruktifikovat	k5eAaBmIp3nS	fruktifikovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
popsán	popsán	k2eAgInSc1d1	popsán
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
na	na	k7c6	na
následujících	následující	k2eAgFnPc6d1	následující
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
:	:	kIx,	:
Báň	báň	k1gFnSc1	báň
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
)	)	kIx)	)
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
(	(	kIx(	(
<g/>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
a	a	k8xC	a
Zlínský	zlínský	k2eAgInSc1d1	zlínský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
kras	kras	k1gInSc1	kras
(	(	kIx(	(
<g/>
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
Klapice	Klapice	k1gFnSc1	Klapice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Praha-východ	Prahaýchod	k1gInSc1	Praha-východ
<g/>
)	)	kIx)	)
Džbán	džbán	k1gInSc1	džbán
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc1	okres
Louny	Louny	k1gInPc1	Louny
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
a	a	k8xC	a
Rakovník	Rakovník	k1gInSc1	Rakovník
<g/>
)	)	kIx)	)
Kněžičky	kněžička	k1gFnPc1	kněžička
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc1	okres
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
Žehuňská	Žehuňský	k2eAgFnSc1d1	Žehuňská
obora	obora	k1gFnSc1	obora
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
Podyjí	Podyjí	k1gNnSc1	Podyjí
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
Morava	Morava	k1gFnSc1	Morava
<g/>
)	)	kIx)	)
Vyšenské	Vyšenský	k2eAgInPc1d1	Vyšenský
kopce	kopec	k1gInPc1	kopec
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc1d1	velký
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Louny	Louny	k1gInPc1	Louny
<g/>
)	)	kIx)	)
Žernov	žernov	k1gInSc1	žernov
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Polabí	Polabí	k1gNnSc6	Polabí
a	a	k8xC	a
na	na	k7c6	na
Brněnsku	Brněnsko	k1gNnSc6	Brněnsko
<g/>
.	.	kIx.	.
</s>
<s>
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zaměnit	zaměnit	k5eAaPmF	zaměnit
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
hřiby	hřib	k1gInPc7	hřib
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
póry	pór	k1gInPc7	pór
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hřibem	hřib	k1gInSc7	hřib
kovářem	kovář	k1gMnSc7	kovář
<g/>
,	,	kIx,	,
hřibem	hřib	k1gInSc7	hřib
kolodějem	koloděj	k1gMnSc7	koloděj
<g/>
,	,	kIx,	,
hřibem	hřib	k1gInSc7	hřib
nachovým	nachový	k2eAgInSc7d1	nachový
nebo	nebo	k8xC	nebo
hřibem	hřib	k1gInSc7	hřib
kříštěm	kříšť	k1gInSc7	kříšť
<g/>
.	.	kIx.	.
hřib	hřib	k1gInSc1	hřib
kovář	kovář	k1gMnSc1	kovář
(	(	kIx(	(
<g/>
Neoboletus	Neoboletus	k1gMnSc1	Neoboletus
luridiformis	luridiformis	k1gFnSc2	luridiformis
<g/>
)	)	kIx)	)
-	-	kIx~	-
hnědý	hnědý	k2eAgInSc1d1	hnědý
až	až	k8xS	až
hnědorezavý	hnědorezavý	k2eAgInSc1d1	hnědorezavý
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
třeň	třeň	k1gInSc1	třeň
nemá	mít	k5eNaImIp3nS	mít
síťku	síťka	k1gFnSc4	síťka
hřib	hřib	k1gInSc1	hřib
koloděj	koloděj	k1gMnSc1	koloděj
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Suillellus	Suillellus	k1gMnSc1	Suillellus
luridus	luridus	k1gMnSc1	luridus
<g/>
)	)	kIx)	)
-	-	kIx~	-
krémový	krémový	k2eAgInSc1d1	krémový
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
síťky	síťka	k1gFnSc2	síťka
a	a	k8xC	a
podkladu	podklad	k1gInSc2	podklad
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
výrazněji	výrazně	k6eAd2	výrazně
liší	lišit	k5eAaImIp3nP	lišit
hřib	hřib	k1gInSc4	hřib
kříšť	kříšť	k1gInSc1	kříšť
(	(	kIx(	(
<g/>
Caloboletus	Caloboletus	k1gMnSc1	Caloboletus
calopus	calopus	k1gMnSc1	calopus
<g/>
)	)	kIx)	)
-	-	kIx~	-
žluté	žlutý	k2eAgInPc4d1	žlutý
póry	pór	k1gInPc4	pór
<g/>
,	,	kIx,	,
výskyt	výskyt	k1gInSc4	výskyt
často	často	k6eAd1	často
pod	pod	k7c4	pod
jehličnany	jehličnan	k1gInPc4	jehličnan
spíše	spíše	k9	spíše
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
hřib	hřib	k1gInSc4	hřib
nachový	nachový	k2eAgInSc4d1	nachový
(	(	kIx(	(
<g/>
Rubroboletus	Rubroboletus	k1gInSc1	Rubroboletus
rhodoxanthus	rhodoxanthus	k1gInSc1	rhodoxanthus
<g/>
)	)	kIx)	)
-	-	kIx~	-
drobnější	drobný	k2eAgInSc1d2	drobnější
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
nepáchne	páchnout	k5eNaImIp3nS	páchnout
<g/>
,	,	kIx,	,
třeň	třeň	k1gInSc1	třeň
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
s	s	k7c7	s
výraznější	výrazný	k2eAgFnSc7d2	výraznější
síťkou	síťka	k1gFnSc7	síťka
hřib	hřib	k1gInSc4	hřib
Moserův	Moserův	k2eAgInSc4d1	Moserův
(	(	kIx(	(
<g/>
Rubroboletus	Rubroboletus	k1gInSc1	Rubroboletus
rubrosanguineus	rubrosanguineus	k1gInSc1	rubrosanguineus
<g/>
)	)	kIx)	)
-	-	kIx~	-
roste	růst	k5eAaImIp3nS	růst
vzácně	vzácně	k6eAd1	vzácně
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
pod	pod	k7c7	pod
jehličnany	jehličnan	k1gInPc4	jehličnan
hřib	hřib	k1gInSc1	hřib
Le	Le	k1gMnPc2	Le
Galové	Galová	k1gFnSc2	Galová
(	(	kIx(	(
<g/>
Rubroboletus	Rubroboletus	k1gInSc1	Rubroboletus
legaliae	legalia	k1gInSc2	legalia
<g/>
)	)	kIx)	)
-	-	kIx~	-
výraznější	výrazný	k2eAgFnSc1d2	výraznější
síťka	síťka	k1gFnSc1	síťka
<g/>
,	,	kIx,	,
při	při	k7c6	při
zasychání	zasychání	k1gNnSc6	zasychání
voní	vonět	k5eAaImIp3nS	vonět
po	po	k7c6	po
Maggi	maggi	k1gNnSc6	maggi
<g/>
,	,	kIx,	,
i	i	k8xC	i
hráze	hráze	k1gFnSc1	hráze
<g />
.	.	kIx.	.
</s>
<s>
rybníků	rybník	k1gInPc2	rybník
hřib	hřib	k1gInSc1	hřib
satanovitý	satanovitý	k2eAgInSc1d1	satanovitý
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
satanoides	satanoides	k1gInSc1	satanoides
<g/>
)	)	kIx)	)
-	-	kIx~	-
hráze	hráze	k1gFnSc1	hráze
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
žlutější	žlutý	k2eAgFnSc1d2	žlutější
dužnina	dužnina	k1gFnSc1	dužnina
<g/>
,	,	kIx,	,
kyjovitý	kyjovitý	k2eAgInSc1d1	kyjovitý
třeň	třeň	k1gInSc1	třeň
K	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc4	satan
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
kolodějů	koloděj	k1gMnPc2	koloděj
a	a	k8xC	a
hřibu	hřib	k1gInSc2	hřib
Le	Le	k1gFnSc2	Le
Galové	Galová	k1gFnSc2	Galová
až	až	k9	až
na	na	k7c4	na
ojedinělé	ojedinělý	k2eAgFnPc4d1	ojedinělá
výjimky	výjimka	k1gFnPc4	výjimka
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
na	na	k7c6	na
hrázích	hráz	k1gFnPc6	hráz
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
podobných	podobný	k2eAgNnPc6d1	podobné
synantropních	synantropní	k2eAgNnPc6d1	synantropní
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hřib	hřib	k1gInSc1	hřib
hlohový	hlohový	k2eAgInSc1d1	hlohový
<g/>
.	.	kIx.	.
</s>
<s>
Rubroboletus	Rubroboletus	k1gMnSc1	Rubroboletus
satanas	satanas	k1gMnSc1	satanas
f.	f.	k?	f.
crataegi	crataeg	k1gFnSc2	crataeg
Smotl	Smotl	k1gFnSc2	Smotl
<g/>
.	.	kIx.	.
ex	ex	k6eAd1	ex
Mikšík	Mikšík	k1gMnSc1	Mikšík
<g/>
.	.	kIx.	.
</s>
<s>
Xanthoidní	Xanthoidní	k2eAgFnSc1d1	Xanthoidní
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chybí	chybit	k5eAaPmIp3nP	chybit
červené	červený	k2eAgNnSc4d1	červené
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
hřib	hřib	k1gInSc4	hřib
hlohový	hlohový	k2eAgInSc4d1	hlohový
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
však	však	k9	však
neodráží	odrážet	k5eNaImIp3nS	odrážet
symbiózu	symbióza	k1gFnSc4	symbióza
s	s	k7c7	s
hlohem	hloh	k1gInSc7	hloh
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
světlé	světlý	k2eAgNnSc4d1	světlé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
je	být	k5eAaImIp3nS	být
bělavý	bělavý	k2eAgInSc1d1	bělavý
<g/>
,	,	kIx,	,
rourky	rourka	k1gFnPc4	rourka
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
třeň	třeň	k1gInSc1	třeň
bledě	bledě	k6eAd1	bledě
žlutý	žlutý	k2eAgMnSc1d1	žlutý
<g/>
,	,	kIx,	,
dužnina	dužnina	k1gFnSc1	dužnina
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
staré	starý	k2eAgFnPc1d1	stará
plodnice	plodnice	k1gFnPc1	plodnice
páchnou	páchnout	k5eAaImIp3nP	páchnout
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
houbu	houba	k1gFnSc4	houba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
evidována	evidovat	k5eAaImNgFnS	evidovat
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
hub	houba	k1gFnPc2	houba
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jako	jako	k8xC	jako
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
VU	VU	kA	VU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
jej	on	k3xPp3gMnSc4	on
chránit	chránit	k5eAaImF	chránit
<g/>
,	,	kIx,	,
nálezy	nález	k1gInPc4	nález
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
hlásit	hlásit	k5eAaImF	hlásit
mykologickým	mykologický	k2eAgNnPc3d1	mykologické
pracovištím	pracoviště	k1gNnPc3	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Otrava	otrava	k1gMnSc1	otrava
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gMnSc7	satan
<g/>
.	.	kIx.	.
</s>
<s>
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
syrova	syrov	k1gInSc2	syrov
(	(	kIx(	(
<g/>
či	či	k8xC	či
po	po	k7c6	po
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
tepelné	tepelný	k2eAgFnSc6d1	tepelná
úpravě	úprava	k1gFnSc6	úprava
<g/>
)	)	kIx)	)
silně	silně	k6eAd1	silně
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
neutišitelné	utišitelný	k2eNgNnSc1d1	neutišitelné
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
stačí	stačit	k5eAaBmIp3nS	stačit
ochutnat	ochutnat	k5eAaPmF	ochutnat
malý	malý	k2eAgInSc1d1	malý
kousek	kousek	k1gInSc1	kousek
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
potíže	potíž	k1gFnPc1	potíž
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
výpary	výpar	k1gInPc4	výpar
ze	z	k7c2	z
syrové	syrový	k2eAgFnSc2d1	syrová
houby	houba	k1gFnSc2	houba
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kvalitním	kvalitní	k2eAgNnSc6d1	kvalitní
tepelném	tepelný	k2eAgNnSc6d1	tepelné
zpracování	zpracování	k1gNnSc6	zpracování
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
varu	var	k1gInSc2	var
<g/>
)	)	kIx)	)
otrava	otrava	k1gFnSc1	otrava
nehrozí	hrozit	k5eNaImIp3nS	hrozit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
satanu	satan	k1gInSc6	satan
byly	být	k5eAaImAgInP	být
zjištěny	zjištěn	k2eAgInPc1d1	zjištěn
i	i	k8xC	i
indolové	indolový	k2eAgInPc1d1	indolový
deriváty	derivát	k1gInPc1	derivát
(	(	kIx(	(
<g/>
psychotoxické	psychotoxický	k2eAgFnPc1d1	psychotoxický
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vařením	vaření	k1gNnSc7	vaření
nerozkládají	rozkládat	k5eNaImIp3nP	rozkládat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
jej	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
doporučit	doporučit	k5eAaPmF	doporučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
umístila	umístit	k5eAaPmAgFnS	umístit
obec	obec	k1gFnSc1	obec
Vršovice	Vršovice	k1gFnPc4	Vršovice
na	na	k7c6	na
svoji	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
erb	erb	k1gInSc4	erb
figuru	figura	k1gFnSc4	figura
hřibu	hřib	k1gInSc2	hřib
satanu	satan	k1gInSc2	satan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
národní	národní	k2eAgFnSc4d1	národní
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
Velký	velký	k2eAgInSc4d1	velký
Vrch	vrch	k1gInSc4	vrch
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
českých	český	k2eAgFnPc2d1	Česká
lokalit	lokalita	k1gFnPc2	lokalita
této	tento	k3xDgFnSc2	tento
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Plodnice	plodnice	k1gFnPc1	plodnice
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
nalezeny	naleznout	k5eAaPmNgInP	naleznout
exempláře	exemplář	k1gInPc1	exemplář
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
vážil	vážit	k5eAaImAgMnS	vážit
2	[number]	k4	2
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
