<s>
Obsazení	obsazení	k1gNnSc1
Říma	Řím	k1gInSc2
</s>
<s>
Obsazení	obsazení	k1gNnSc1
Říma	Řím	k1gInSc2
(	(	kIx(
<g/>
Presa	Presa	k1gFnSc1
di	di	k?
Roma	Rom	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Risorgimento	Risorgimento	k1gNnSc1
</s>
<s>
Proražení	proražení	k1gNnSc1
brány	brána	k1gFnSc2
Porta	porto	k1gNnSc2
Pia	Pius	k1gMnSc2
<g/>
,	,	kIx,
Carlo	Carla	k1gMnSc5
Ademollo	Ademolla	k1gMnSc5
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1870	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vítězství	vítězství	k1gNnSc1
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
konec	konec	k1gInSc1
Papežského	papežský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
Risorgimenta	Risorgimento	k1gNnSc2
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
anexe	anexe	k1gFnSc1
Říma	Řím	k1gInSc2
a	a	k8xC
Lazia	Lazium	k1gNnSc2
Italským	italský	k2eAgMnPc3d1
královstvím	království	k1gNnSc7
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
Papežský	papežský	k2eAgInSc1d1
stát	stát	k5eAaImF,k5eAaPmF
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raffaele	Raffael	k1gInSc2
Cadorna	Cadorna	k1gFnSc1
</s>
<s>
Pius	Pius	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hermann	Hermann	k1gMnSc1
Kanzler	Kanzler	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
50	#num#	k4
000	#num#	k4
</s>
<s>
13	#num#	k4
157	#num#	k4
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
49	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
</s>
<s>
19	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
</s>
<s>
Obsazení	obsazení	k1gNnSc1
Říma	Řím	k1gInSc2
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Presa	Pres	k1gMnSc2
di	di	k?
Roma	Rom	k1gMnSc2
<g/>
)	)	kIx)
italskou	italský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1870	#num#	k4
znamenalo	znamenat	k5eAaImAgNnS
zánik	zánik	k1gInSc4
Papežského	papežský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
symbolický	symbolický	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
procesu	proces	k1gInSc2
sjednocení	sjednocení	k1gNnSc2
Itálie	Itálie	k1gFnSc2
(	(	kIx(
<g/>
risorgimenta	risorgimenta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
tak	tak	k6eAd1
získala	získat	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
tradiční	tradiční	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
předčasně	předčasně	k6eAd1
ukončen	ukončen	k2eAgInSc4d1
probíhající	probíhající	k2eAgInSc4d1
první	první	k4xOgInSc4
vatikánský	vatikánský	k2eAgInSc4d1
koncil	koncil	k1gInSc4
a	a	k8xC
papežové	papež	k1gMnPc1
byli	být	k5eAaImAgMnP
až	až	k9
do	do	k7c2
lateránských	lateránský	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
s	s	k7c7
Mussolinim	Mussolini	k1gNnSc7
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
zbaveni	zbavit	k5eAaPmNgMnP
výsostného	výsostný	k2eAgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
postavení	postavení	k1gNnSc2
faktických	faktický	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Římská	římský	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
<g/>
“	“	k?
byla	být	k5eAaImAgFnS
předmětem	předmět	k1gInSc7
úsilí	úsilí	k1gNnSc1
Italů	Ital	k1gMnPc2
již	již	k6eAd1
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
byl	být	k5eAaImAgInS
pod	pod	k7c7
ochranou	ochrana	k1gFnSc7
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giuseppe	Giusepp	k1gInSc5
Garibaldi	Garibald	k1gMnPc1
na	na	k7c4
Řím	Řím	k1gInSc4
vyrazil	vyrazit	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
armády	armáda	k1gFnSc2
dobrovolníků	dobrovolník	k1gMnPc2
v	v	k7c6
srpnu	srpen	k1gInSc6
1862	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
Francie	Francie	k1gFnSc1
donutila	donutit	k5eAaPmAgFnS
diplomatickým	diplomatický	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
Itálii	Itálie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
proti	proti	k7c3
dobrovolníkům	dobrovolník	k1gMnPc3
vyslala	vyslat	k5eAaPmAgFnS
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
postup	postup	k1gInSc1
garibaldiovců	garibaldiovec	k1gMnPc2
zastavilo	zastavit	k5eAaPmAgNnS
bitvou	bitva	k1gFnSc7
u	u	k7c2
Aspromonte	Aspromont	k1gInSc5
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1862	#num#	k4
a	a	k8xC
Garibaldi	Garibald	k1gMnPc1
byl	být	k5eAaImAgInS
zraněn	zranit	k5eAaPmNgMnS
a	a	k8xC
zajat	zajmout	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záříjovou	Záříjový	k2eAgFnSc7d1
úmluvou	úmluva	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1864	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
Itálie	Itálie	k1gFnSc1
stažení	stažení	k1gNnSc2
francouzských	francouzský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
z	z	k7c2
Říma	Řím	k1gInSc2
a	a	k8xC
zavázala	zavázat	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
chránit	chránit	k5eAaImF
sama	sám	k3xTgMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
Garibaldi	Garibald	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
znovu	znovu	k6eAd1
pokusil	pokusit	k5eAaPmAgInS
Řím	Řím	k1gInSc1
obsadit	obsadit	k5eAaPmF
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
tam	tam	k6eAd1
opět	opět	k6eAd1
své	svůj	k3xOyFgNnSc4
vojsko	vojsko	k1gNnSc4
umístila	umístit	k5eAaPmAgFnS
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
u	u	k7c2
Mentany	Mentan	k1gInPc1
italské	italský	k2eAgMnPc4d1
dobrovolníky	dobrovolník	k1gMnPc4
rozprášila	rozprášit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Papež	Papež	k1gMnSc1
Pius	Pius	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
kolem	kolem	k7c2
roku	rok	k1gInSc2
1864	#num#	k4
</s>
<s>
Nakonec	nakonec	k6eAd1
k	k	k7c3
získání	získání	k1gNnSc3
Říma	Řím	k1gInSc2
Italům	Ital	k1gMnPc3
pomohla	pomoct	k5eAaPmAgFnS
porážka	porážka	k1gFnSc1
Francie	Francie	k1gFnSc2
v	v	k7c6
prusko-francouzské	prusko-francouzský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porážka	porážka	k1gFnSc1
u	u	k7c2
Sedanu	sedan	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1870	#num#	k4
a	a	k8xC
zajetí	zajetí	k1gNnSc2
Napoleona	Napoleon	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
Italům	Ital	k1gMnPc3
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ze	z	k7c2
strany	strana	k1gFnSc2
Francie	Francie	k1gFnSc2
již	již	k6eAd1
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
ničeho	nic	k3yNnSc2
obávat	obávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Viktor	Viktor	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
nejprve	nejprve	k6eAd1
poslal	poslat	k5eAaPmAgMnS
k	k	k7c3
papeži	papež	k1gMnSc3
Piovi	Pius	k1gMnSc3
IX	IX	kA
<g/>
.	.	kIx.
vyslance	vyslanec	k1gMnSc2
Gustava	Gustav	k1gMnSc2
Ponzu	Ponz	k1gInSc2
di	di	k?
San	San	k1gFnSc2
Martino	Martin	k2eAgNnSc1d1
s	s	k7c7
návrhem	návrh	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
papež	papež	k1gMnSc1
dobrovolně	dobrovolně	k6eAd1
postoupil	postoupit	k5eAaPmAgMnS
většinu	většina	k1gFnSc4
Říma	Řím	k1gInSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
Itálii	Itálie	k1gFnSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
formálně	formálně	k6eAd1
zachován	zachovat	k5eAaPmNgInS
v	v	k7c6
podobě	podoba	k1gFnSc6
římské	římský	k2eAgFnSc3d1
čtvrti	čtvrt	k1gFnSc3
uvnitř	uvnitř	k7c2
Leonských	leonský	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
a	a	k8xC
Itálie	Itálie	k1gFnSc1
by	by	kYmCp3nS
papežům	papež	k1gMnPc3
trvale	trvale	k6eAd1
vyplácela	vyplácet	k5eAaImAgFnS
roční	roční	k2eAgFnSc4d1
náhradu	náhrada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pius	Pius	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
návrh	návrh	k1gInSc1
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
zamítl	zamítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
tak	tak	k6eAd1
překročila	překročit	k5eAaPmAgFnS
hranice	hranice	k1gFnSc1
Papežského	papežský	k2eAgInSc2d1
státu	stát	k1gInSc2
italská	italský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
čele	čelo	k1gNnSc6
stál	stát	k5eAaImAgMnS
generál	generál	k1gMnSc1
Raffaele	Raffael	k1gInSc2
Cadorna	Cadorno	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
pomalu	pomalu	k6eAd1
postupovala	postupovat	k5eAaImAgFnS
k	k	k7c3
Římu	Řím	k1gInSc3
s	s	k7c7
nadějí	naděje	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ještě	ještě	k6eAd1
podaří	podařit	k5eAaPmIp3nS
vyjednat	vyjednat	k5eAaPmF
mírové	mírový	k2eAgNnSc4d1
převzetí	převzetí	k1gNnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Papež	Papež	k1gMnSc1
měl	mít	k5eAaImAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
obranu	obrana	k1gFnSc4
13	#num#	k4
157	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Cadorna	Cadorno	k1gNnSc2
velel	velet	k5eAaImAgInS
asi	asi	k9
50	#num#	k4
000	#num#	k4
Italů	Ital	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papežskou	papežský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
vedl	vést	k5eAaImAgMnS
generál	generál	k1gMnSc1
Hermann	Hermann	k1gMnSc1
Kanzler	Kanzler	k1gMnSc1
a	a	k8xC
sestávala	sestávat	k5eAaImAgFnS
ze	z	k7c2
Švýcarské	švýcarský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
a	a	k8xC
dobrovolníků	dobrovolník	k1gMnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
zuávů	zuáv	k1gMnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
z	z	k7c2
katolických	katolický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pius	Pius	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bez	bez	k7c2
francouzské	francouzský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
Řím	Řím	k1gInSc1
podlehne	podlehnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
avšak	avšak	k8xC
přesto	přesto	k8xC
nařídil	nařídit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
aspoň	aspoň	k9
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
bráněno	bráněn	k2eAgNnSc1d1
–	–	k?
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
obsazení	obsazení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
násilím	násilí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Italové	Ital	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
k	k	k7c3
Aurelianovým	Aurelianův	k2eAgFnPc3d1
hradbám	hradba	k1gFnPc3
Říma	Řím	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
tříhodinovou	tříhodinový	k2eAgFnSc7d1
dělostřelbou	dělostřelba	k1gFnSc7
hradbu	hradba	k1gFnSc4
prorazili	prorazit	k5eAaPmAgMnP
poblíž	poblíž	k7c2
brány	brána	k1gFnSc2
Porta	porto	k1gNnSc2
Pia	Pius	k1gMnSc2
a	a	k8xC
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
celkem	celkem	k6eAd1
49	#num#	k4
italských	italský	k2eAgMnPc2d1
a	a	k8xC
19	#num#	k4
papežských	papežský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
Řím	Řím	k1gInSc1
byl	být	k5eAaImAgInS
obsazen	obsadit	k5eAaPmNgInS
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
a	a	k8xC
po	po	k7c6
plebiscitu	plebiscit	k1gInSc6
byl	být	k5eAaImAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Laziem	Lazium	k1gNnSc7
připojen	připojen	k2eAgInSc1d1
k	k	k7c3
Italskému	italský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ulice	ulice	k1gFnSc1
Via	via	k7c4
Pia	Pius	k1gMnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vedla	vést	k5eAaImAgFnS
od	od	k7c2
brány	brána	k1gFnSc2
Porta	porto	k1gNnSc2
Pia	Pius	k1gMnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c6
Ulici	ulice	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
(	(	kIx(
<g/>
Via	via	k7c4
XX	XX	kA
Settembre	Settembr	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
podobně	podobně	k6eAd1
byly	být	k5eAaImAgFnP
pojmenovány	pojmenován	k2eAgFnPc1d1
důležité	důležitý	k2eAgFnPc1d1
ulice	ulice	k1gFnPc1
v	v	k7c6
řadě	řada	k1gFnSc6
italských	italský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
De	De	k?
Cesare	Cesar	k1gMnSc5
<g/>
,	,	kIx,
Raffaele	Raffael	k1gInPc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
The	The	k1gFnSc1
Last	Last	k1gMnSc1
Days	Days	k1gInSc1
of	of	k?
Papal	papat	k5eAaImAgInS
Rome	Rom	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Archibald	Archibald	k1gMnSc1
Constable	Constable	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
443	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7556416-6	7556416-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85115188	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85115188	#num#	k4
</s>
