<s>
Karel	Karel	k1gMnSc1	Karel
Schinzel	Schinzel	k1gMnSc1	Schinzel
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1886	[number]	k4	1886
Edrovice	Edrovice	k1gFnSc2	Edrovice
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1951	[number]	k4	1951
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
narozený	narozený	k2eAgInSc4d1	narozený
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnou	rozhodný	k2eAgFnSc7d1	rozhodná
měrou	míra	k1gFnSc7wR	míra
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
zabýval	zabývat	k5eAaImAgMnS	zabývat
se	se	k3xPyFc4	se
také	také	k9	také
reprodukčními	reprodukční	k2eAgFnPc7d1	reprodukční
technikami	technika	k1gFnPc7	technika
a	a	k8xC	a
zvukovým	zvukový	k2eAgInSc7d1	zvukový
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Edrovicích	Edrovice	k1gFnPc6	Edrovice
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Rýmařova	rýmařův	k2eAgFnSc1d1	Rýmařova
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Schinzelových	Schinzelová	k1gFnPc2	Schinzelová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Opavy	Opava	k1gFnSc2	Opava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
otec	otec	k1gMnSc1	otec
otevřel	otevřít	k5eAaPmAgMnS	otevřít
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
hlouběji	hluboko	k6eAd2	hluboko
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
chemické	chemický	k2eAgInPc4d1	chemický
procesy	proces	k1gInPc4	proces
vyvíjení	vyvíjení	k1gNnSc2	vyvíjení
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
jako	jako	k8xS	jako
účetní	účetní	k2eAgFnSc4d1	účetní
v	v	k7c6	v
opavské	opavský	k2eAgFnSc6d1	Opavská
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
léky	lék	k1gInPc4	lék
a	a	k8xC	a
chemikálie	chemikálie	k1gFnPc4	chemikálie
Hell	Hella	k1gFnPc2	Hella
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Apotheke	Apotheke	k1gFnSc4	Apotheke
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
si	se	k3xPyFc3	se
zřídil	zřídit	k5eAaPmAgMnS	zřídit
soukromou	soukromý	k2eAgFnSc4d1	soukromá
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trávil	trávit	k5eAaImAgInS	trávit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
<g/>
;	;	kIx,	;
své	svůj	k3xOyFgFnPc4	svůj
znalosti	znalost	k1gFnPc4	znalost
si	se	k3xPyFc3	se
doplňoval	doplňovat	k5eAaImAgInS	doplňovat
studiem	studio	k1gNnSc7	studio
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
psaných	psaný	k2eAgInPc2d1	psaný
časopisů	časopis	k1gInPc2	časopis
o	o	k7c6	o
chemických	chemický	k2eAgInPc6d1	chemický
a	a	k8xC	a
fotografických	fotografický	k2eAgInPc6d1	fotografický
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
podal	podat	k5eAaPmAgMnS	podat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
patent	patent	k1gInSc4	patent
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
"	"	kIx"	"
<g/>
katachromie	katachromie	k1gFnSc1	katachromie
<g/>
"	"	kIx"	"
–	–	k?	–
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
postup	postup	k1gInSc4	postup
vyvolávání	vyvolávání	k1gNnSc2	vyvolávání
snímků	snímek	k1gInPc2	snímek
na	na	k7c4	na
fotografický	fotografický	k2eAgInSc4d1	fotografický
materiál	materiál	k1gInSc4	materiál
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
citlivými	citlivý	k2eAgFnPc7d1	citlivá
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vynález	vynález	k1gInSc1	vynález
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
později	pozdě	k6eAd2	pozdě
důležitým	důležitý	k2eAgInSc7d1	důležitý
základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgInSc3	který
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
vyráběny	vyráběn	k2eAgInPc4d1	vyráběn
filmy	film	k1gInPc4	film
Kodachrome	Kodachrom	k1gInSc5	Kodachrom
a	a	k8xC	a
Agfacolor	Agfacolora	k1gFnPc2	Agfacolora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
úspěších	úspěch	k1gInPc6	úspěch
mu	on	k3xPp3gMnSc3	on
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
pomohl	pomoct	k5eAaPmAgMnS	pomoct
pokračovat	pokračovat	k5eAaImF	pokračovat
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
Technische	Technische	k1gFnSc6	Technische
Hochschule	Hochschule	k1gFnSc2	Hochschule
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
večerní	večerní	k2eAgInPc4d1	večerní
kurzy	kurz	k1gInPc4	kurz
na	na	k7c6	na
reálné	reálný	k2eAgFnSc6d1	reálná
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Schattenfeldu	Schattenfeld	k1gInSc6	Schattenfeld
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
získal	získat	k5eAaPmAgInS	získat
inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
diplom	diplom	k1gInSc1	diplom
<g/>
;	;	kIx,	;
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
fotograf	fotograf	k1gMnSc1	fotograf
–	–	k?	–
pořizoval	pořizovat	k5eAaImAgMnS	pořizovat
snímky	snímek	k1gInPc4	snímek
vojáků	voják	k1gMnPc2	voják
pózujích	pózuj	k1gInPc6	pózuj
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyvolával	vyvolávat	k5eAaImAgMnS	vyvolávat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
rok	rok	k1gInSc1	rok
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
doktorskou	doktorský	k2eAgFnSc7d1	doktorská
prací	práce	k1gFnSc7	práce
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
výroby	výroba	k1gFnSc2	výroba
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
dále	daleko	k6eAd2	daleko
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
svého	svůj	k3xOyFgInSc2	svůj
patentu	patent	k1gInSc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
návrh	návrh	k1gInSc1	návrh
předložil	předložit	k5eAaPmAgInS	předložit
komisi	komise	k1gFnSc3	komise
expertů	expert	k1gMnPc2	expert
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
nerealistický	realistický	k2eNgInSc4d1	nerealistický
<g/>
.	.	kIx.	.
</s>
<s>
Schinzel	Schinzet	k5eAaImAgMnS	Schinzet
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Opavy	Opava	k1gFnSc2	Opava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zařídil	zařídit	k5eAaPmAgMnS	zařídit
novou	nový	k2eAgFnSc4d1	nová
laboratoř	laboratoř	k1gFnSc4	laboratoř
v	v	k7c6	v
domě	dům	k1gInSc6	dům
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Morální	morální	k2eAgFnSc4d1	morální
podporu	podpora	k1gFnSc4	podpora
mu	on	k3xPp3gMnSc3	on
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
bratr	bratr	k1gMnSc1	bratr
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Schinzel	Schinzet	k5eAaImAgMnS	Schinzet
sám	sám	k3xTgMnSc1	sám
v	v	k7c4	v
ústraní	ústraní	k1gNnSc4	ústraní
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
finančně	finančně	k6eAd1	finančně
nákladném	nákladný	k2eAgInSc6d1	nákladný
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
výsledků	výsledek	k1gInPc2	výsledek
patentoval	patentovat	k5eAaBmAgInS	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
na	na	k7c4	na
bratrovo	bratrův	k2eAgNnSc4d1	bratrovo
naléhání	naléhání	k1gNnSc4	naléhání
začal	začít	k5eAaPmAgInS	začít
své	svůj	k3xOyFgInPc4	svůj
výsledky	výsledek	k1gInPc4	výsledek
publikovat	publikovat	k5eAaBmF	publikovat
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
časopisech	časopis	k1gInPc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
jeho	jeho	k3xOp3gInPc2	jeho
článků	článek	k1gInPc2	článek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Das	Das	k1gMnPc2	Das
Lichtbild	Lichtbilda	k1gFnPc2	Lichtbilda
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
pozornost	pozornost	k1gFnSc1	pozornost
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
Kodak	Kodak	kA	Kodak
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Agfa	Agfa	k1gFnSc1	Agfa
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postřehli	postřehnout	k5eAaPmAgMnP	postřehnout
význam	význam	k1gInSc4	význam
Schinzelova	Schinzelův	k2eAgInSc2d1	Schinzelův
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
jej	on	k3xPp3gMnSc4	on
společnost	společnost	k1gFnSc1	společnost
Eastman	Eastman	k1gMnSc1	Eastman
Kodak	Kodak	kA	Kodak
pozvala	pozvat	k5eAaPmAgFnS	pozvat
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
továren	továrna	k1gFnPc2	továrna
do	do	k7c2	do
Rochesteru	Rochester	k1gInSc2	Rochester
<g/>
,	,	kIx,	,
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delších	dlouhý	k2eAgNnPc6d2	delší
vyjednáváních	vyjednávání	k1gNnPc6	vyjednávání
firma	firma	k1gFnSc1	firma
od	od	k7c2	od
Schinzela	Schinzela	k1gFnSc2	Schinzela
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
za	za	k7c4	za
nízkou	nízký	k2eAgFnSc4d1	nízká
částku	částka	k1gFnSc4	částka
práva	právo	k1gNnSc2	právo
k	k	k7c3	k
27	[number]	k4	27
patentům	patent	k1gInPc3	patent
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k9	i
patent	patent	k1gInSc4	patent
na	na	k7c4	na
barevné	barevný	k2eAgNnSc4d1	barevné
vyvíjení	vyvíjení	k1gNnSc4	vyvíjení
s	s	k7c7	s
postupnou	postupný	k2eAgFnSc7d1	postupná
spektrální	spektrální	k2eAgFnSc7d1	spektrální
expozicí	expozice	k1gFnSc7	expozice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
převrat	převrat	k1gInSc4	převrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Schinzel	Schinzet	k5eAaImAgMnS	Schinzet
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
nevalné	valný	k2eNgFnSc6d1	nevalná
finanční	finanční	k2eAgFnSc6d1	finanční
situaci	situace	k1gFnSc6	situace
do	do	k7c2	do
Opavy	Opava	k1gFnSc2	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
Kodak	kodak	k1gInSc1	kodak
na	na	k7c4	na
Schinzela	Schinzela	k1gFnSc4	Schinzela
obrátil	obrátit	k5eAaPmAgMnS	obrátit
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
masové	masový	k2eAgFnSc2d1	masová
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Schinzel	Schinzet	k5eAaPmAgMnS	Schinzet
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
váhání	váhání	k1gNnSc6	váhání
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
práce	práce	k1gFnSc2	práce
byly	být	k5eAaImAgInP	být
problémy	problém	k1gInPc1	problém
vyřešeny	vyřešit	k5eAaPmNgInP	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
unavený	unavený	k2eAgMnSc1d1	unavený
a	a	k8xC	a
ve	v	k7c6	v
špatné	špatný	k2eAgFnSc6d1	špatná
finanční	finanční	k2eAgFnSc6d1	finanční
situaci	situace	k1gFnSc6	situace
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942	[number]	k4	1942
až	až	k9	až
1943	[number]	k4	1943
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
mladší	mladý	k2eAgFnSc7d2	mladší
Hermínou	Hermína	k1gFnSc7	Hermína
Marií	Maria	k1gFnSc7	Maria
Würstovou	Würstová	k1gFnSc7	Würstová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přišla	přijít	k5eAaPmAgFnS	přijít
další	další	k2eAgFnSc1d1	další
rána	rána	k1gFnSc1	rána
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
laboratoř	laboratoř	k1gFnSc1	laboratoř
v	v	k7c6	v
Badenu	Baden	k1gInSc6	Baden
byla	být	k5eAaImAgFnS	být
vyrabována	vyrabován	k2eAgFnSc1d1	vyrabován
spojeneckými	spojenecký	k2eAgNnPc7d1	spojenecké
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
všechno	všechen	k3xTgNnSc4	všechen
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc4	poznámka
i	i	k8xC	i
osobní	osobní	k2eAgInSc4d1	osobní
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Zdrcen	zdrcen	k2eAgMnSc1d1	zdrcen
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
poznámek	poznámka	k1gFnPc2	poznámka
sepsal	sepsat	k5eAaPmAgMnS	sepsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
barevné	barevný	k2eAgFnSc6d1	barevná
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1951	[number]	k4	1951
náhle	náhle	k6eAd1	náhle
oslepl	oslepnout	k5eAaPmAgInS	oslepnout
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
