<p>
<s>
Glee	Glee	k1gInSc1	Glee
je	být	k5eAaImIp3nS	být
muzikálový	muzikálový	k2eAgInSc1d1	muzikálový
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
produkovala	produkovat	k5eAaImAgFnS	produkovat
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Fox	fox	k1gInSc1	fox
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
letech	let	k1gInPc6	let
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
v	v	k7c6	v
šesti	šest	k4xCc6	šest
řadách	řada	k1gFnPc6	řada
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
celkem	celkem	k6eAd1	celkem
121	[number]	k4	121
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
vysílaly	vysílat	k5eAaImAgFnP	vysílat
první	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
sezóny	sezóna	k1gFnPc1	sezóna
v	v	k7c6	v
letech	let	k1gInPc6	let
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
a	a	k8xC	a
Prima	prima	k6eAd1	prima
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
středoškolský	středoškolský	k2eAgInSc1d1	středoškolský
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
soutěže	soutěž	k1gFnSc2	soutěž
školních	školní	k2eAgInPc2d1	školní
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
glee	glee	k1gNnSc1	glee
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
seriál	seriál	k1gInSc1	seriál
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
sboru	sbor	k1gInSc2	sbor
musejí	muset	k5eAaImIp3nP	muset
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
kolem	kolem	k7c2	kolem
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
sexuality	sexualita	k1gFnSc2	sexualita
a	a	k8xC	a
se	s	k7c7	s
sociálními	sociální	k2eAgFnPc7d1	sociální
otázkami	otázka	k1gFnPc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
těžké	těžký	k2eAgNnSc1d1	těžké
je	být	k5eAaImIp3nS	být
býti	být	k5eAaImF	být
studentem	student	k1gMnSc7	student
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herci	herec	k1gMnPc5	herec
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Hostující	hostující	k2eAgFnSc2d1	hostující
role	role	k1gFnSc2	role
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Will	Will	k1gMnSc1	Will
Schuester	Schuester	k1gMnSc1	Schuester
(	(	kIx(	(
<g/>
Matthew	Matthew	k1gMnSc1	Matthew
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
učitel	učitel	k1gMnSc1	učitel
španělštiny	španělština	k1gFnSc2	španělština
na	na	k7c6	na
fiktivní	fiktivní	k2eAgFnSc6d1	fiktivní
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
William	William	k1gInSc4	William
McKinley	McKinlea	k1gMnSc2	McKinlea
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
učitel	učitel	k1gMnSc1	učitel
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
vedoucí	vedoucí	k1gMnSc1	vedoucí
hudebního	hudební	k2eAgInSc2d1	hudební
souboru	soubor	k1gInSc2	soubor
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
ze	z	k7c2	z
zneužívání	zneužívání	k1gNnSc2	zneužívání
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
převezme	převzít	k5eAaPmIp3nS	převzít
vedení	vedení	k1gNnSc1	vedení
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
žák	žák	k1gMnSc1	žák
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
členem	člen	k1gMnSc7	člen
tehdy	tehdy	k6eAd1	tehdy
významného	významný	k2eAgInSc2d1	významný
a	a	k8xC	a
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
by	by	kYmCp3nS	by
nyní	nyní	k6eAd1	nyní
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
dávné	dávný	k2eAgInPc4d1	dávný
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
potýkat	potýkat	k5eAaImF	potýkat
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
těžkostmi	těžkost	k1gFnPc7	těžkost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
napnutý	napnutý	k2eAgInSc1d1	napnutý
rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
se	s	k7c7	s
Sue	Sue	k1gFnSc7	Sue
Sylvesterovou	Sylvesterův	k2eAgFnSc7d1	Sylvesterův
(	(	kIx(	(
<g/>
Jane	Jan	k1gMnSc5	Jan
Lynch	Lyncha	k1gFnPc2	Lyncha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
trenérkou	trenérka	k1gFnSc7	trenérka
místních	místní	k2eAgFnPc2d1	místní
roztleskávaček	roztleskávačka	k1gFnPc2	roztleskávačka
a	a	k8xC	a
obává	obávat	k5eAaImIp3nS	obávat
se	se	k3xPyFc4	se
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
přednostní	přednostní	k2eAgNnSc4d1	přednostní
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
sbor	sbor	k1gInSc1	sbor
sabotovat	sabotovat	k5eAaImF	sabotovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
problémy	problém	k1gInPc4	problém
mu	on	k3xPp3gMnSc3	on
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
manželka	manželka	k1gFnSc1	manželka
Terri	Terr	k1gFnSc2	Terr
(	(	kIx(	(
<g/>
Jessalyn	Jessalyn	k1gMnSc1	Jessalyn
Gilsig	Gilsig	k1gMnSc1	Gilsig
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
výchovná	výchovný	k2eAgFnSc1d1	výchovná
poradkyně	poradkyně	k1gFnSc1	poradkyně
Emma	Emma	k1gFnSc1	Emma
Pilsburyová	Pilsburyová	k1gFnSc1	Pilsburyová
(	(	kIx(	(
<g/>
Jayma	Jayma	k1gNnSc1	Jayma
Mays	Maysa	k1gFnPc2	Maysa
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
osm	osm	k4xCc1	osm
členů	člen	k1gInPc2	člen
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Dianna	Diann	k1gMnSc4	Diann
Agronová	Agronová	k1gFnSc1	Agronová
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Colfer	Colfer	k1gInSc1	Colfer
<g/>
,	,	kIx,	,
Kevin	Kevin	k1gInSc1	Kevin
McHale	McHala	k1gFnSc3	McHala
<g/>
,	,	kIx,	,
Lea	Lea	k1gFnSc1	Lea
Michele	Michel	k1gInSc2	Michel
<g/>
,	,	kIx,	,
Cory	Cora	k1gFnSc2	Cora
Monteith	Monteitha	k1gFnPc2	Monteitha
<g/>
,	,	kIx,	,
Amber	ambra	k1gFnPc2	ambra
Riley	Rilea	k1gFnSc2	Rilea
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Salling	Salling	k1gInSc1	Salling
a	a	k8xC	a
Jenna	Jenna	k1gFnSc1	Jenna
Ushkowitz	Ushkowitza	k1gFnPc2	Ushkowitza
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oni	onen	k3xDgMnPc1	onen
řeší	řešit	k5eAaImIp3nP	řešit
rozličné	rozličný	k2eAgInPc4d1	rozličný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Rachel	Rachel	k1gInSc1	Rachel
Berryová	Berryová	k1gFnSc1	Berryová
(	(	kIx(	(
<g/>
Lea	Lea	k1gFnSc1	Lea
Michele	Michel	k1gInSc2	Michel
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
stát	stát	k5eAaPmF	stát
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	kurt	k1gInSc1	kurt
(	(	kIx(	(
<g/>
Chris	Chris	k1gInSc1	Chris
Colfer	Colfer	k1gInSc1	Colfer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
gay	gay	k1gMnSc1	gay
<g/>
,	,	kIx,	,
čelí	čelit	k5eAaImIp3nS	čelit
šikaně	šikana	k1gFnSc6	šikana
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
sexuální	sexuální	k2eAgFnSc3d1	sexuální
orientaci	orientace	k1gFnSc3	orientace
a	a	k8xC	a
bojí	bát	k5eAaImIp3nS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
otec	otec	k1gMnSc1	otec
přijme	přijmout	k5eAaPmIp3nS	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Finn	Finn	k1gMnSc1	Finn
(	(	kIx(	(
<g/>
Cory	Cory	k1gInPc1	Cory
Monteith	Monteitha	k1gFnPc2	Monteitha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Puck	Puck	k1gMnSc1	Puck
(	(	kIx(	(
<g/>
Mark	Mark	k1gMnSc1	Mark
Salling	Salling	k1gInSc1	Salling
<g/>
)	)	kIx)	)
a	a	k8xC	a
roztleskávačka	roztleskávačka	k1gFnSc1	roztleskávačka
Quinn	Quinna	k1gFnPc2	Quinna
(	(	kIx(	(
<g/>
Dianna	Dianen	k2eAgFnSc1d1	Dianen
Agronová	Agronová	k1gFnSc1	Agronová
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zapletou	zaplést	k5eAaPmIp3nP	zaplést
do	do	k7c2	do
výbušného	výbušný	k2eAgInSc2d1	výbušný
milostného	milostný	k2eAgInSc2d1	milostný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Quinn	Quinn	k1gMnSc1	Quinn
otěhotní	otěhotnět	k5eAaPmIp3nS	otěhotnět
s	s	k7c7	s
Puckem	Puckem	k?	Puckem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dítě	dítě	k1gNnSc1	dítě
Finnovo	Finnův	k2eAgNnSc1d1	Finnův
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pravda	pravda	k1gFnSc1	pravda
nakonec	nakonec	k6eAd1	nakonec
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sérii	série	k1gFnSc6	série
se	s	k7c7	s
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
stali	stát	k5eAaPmAgMnP	stát
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
členky	členka	k1gFnPc4	členka
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
členů	člen	k1gInPc2	člen
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
Naya	Nay	k2eAgNnPc4d1	Nay
Rivera	Rivero	k1gNnPc4	Rivero
<g/>
,	,	kIx,	,
Heather	Heathra	k1gFnPc2	Heathra
Morris	Morris	k1gFnSc2	Morris
a	a	k8xC	a
Mike	Mike	k1gNnSc2	Mike
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Malley	Mallea	k1gFnPc4	Mallea
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sérii	série	k1gFnSc6	série
k	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
postavám	postava	k1gFnPc3	postava
přibyli	přibýt	k5eAaPmAgMnP	přibýt
Mike	Mike	k1gFnSc7	Mike
Chang	Chang	k1gMnSc1	Chang
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Shum	Shum	k1gMnSc1	Shum
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
)	)	kIx)	)
a	a	k8xC	a
Blaine	Blain	k1gInSc5	Blain
Anderson	Anderson	k1gMnSc1	Anderson
(	(	kIx(	(
<g/>
Darren	Darrna	k1gFnPc2	Darrna
Criss	Criss	k1gInSc1	Criss
<g/>
)	)	kIx)	)
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
seriál	seriál	k1gInSc4	seriál
opustili	opustit	k5eAaPmAgMnP	opustit
Gilsig	Gilsig	k1gInSc4	Gilsig
a	a	k8xC	a
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Malley	Mallea	k1gFnPc4	Mallea
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sérii	série	k1gFnSc6	série
již	již	k9	již
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
nepatřily	patřit	k5eNaImAgInP	patřit
Quinn	Quinn	k1gNnSc4	Quinn
Fabrayová	Fabrayová	k1gFnSc1	Fabrayová
a	a	k8xC	a
Emma	Emma	k1gFnSc1	Emma
Pillsburyová	Pillsburyový	k2eAgFnSc1d1	Pillsburyový
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
Sam	Sam	k1gMnSc1	Sam
Evans	Evansa	k1gFnPc2	Evansa
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Chorda	chorda	k1gFnSc1	chorda
Overstreeta	Overstreeta	k1gFnSc1	Overstreeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc4	seriál
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Ryan	Ryan	k1gInSc4	Ryan
Murphy	Murpha	k1gFnSc2	Murpha
<g/>
,	,	kIx,	,
Brad	brada	k1gFnPc2	brada
Falchuk	Falchuka	k1gFnPc2	Falchuka
a	a	k8xC	a
Ian	Ian	k1gMnSc1	Ian
Brennan	Brennan	k1gMnSc1	Brennan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zprvu	zprvu	k6eAd1	zprvu
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
natočit	natočit	k5eAaBmF	natočit
Glee	Glee	k1gInSc4	Glee
jako	jako	k8xS	jako
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
a	a	k8xC	a
první	první	k4xOgFnSc1	první
řada	řada	k1gFnSc1	řada
běžela	běžet	k5eAaImAgFnS	běžet
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
následovaly	následovat	k5eAaImAgFnP	následovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
dílu	díl	k1gInSc6	díl
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
hudebních	hudební	k2eAgNnPc2d1	hudební
vystoupení	vystoupení	k1gNnPc2	vystoupení
vybíraných	vybíraný	k2eAgNnPc2d1	vybírané
Murphym	Murphymum	k1gNnPc2	Murphymum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
vyrovnanost	vyrovnanost	k1gFnSc4	vyrovnanost
muzikálových	muzikálový	k2eAgInPc2d1	muzikálový
hitů	hit	k1gInPc2	hit
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vydávány	vydávat	k5eAaPmNgInP	vydávat
přes	přes	k7c4	přes
iTunes	iTunes	k1gInSc4	iTunes
Store	Stor	k1gInSc5	Stor
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
CD	CD	kA	CD
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
z	z	k7c2	z
Glee	Gle	k1gFnSc2	Gle
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
vydávány	vydávat	k5eAaPmNgInP	vydávat
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Columbia	Columbia	k1gFnSc1	Columbia
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Glee	Gle	k1gInSc2	Gle
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
komerčním	komerční	k2eAgInSc7d1	komerční
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
:	:	kIx,	:
singlů	singl	k1gInPc2	singl
se	se	k3xPyFc4	se
digitálně	digitálně	k6eAd1	digitálně
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
CD	CD	kA	CD
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
vydán	vydat	k5eAaPmNgInS	vydat
také	také	k9	také
na	na	k7c6	na
DVD	DVD	kA	DVD
a	a	k8xC	a
Blu-ray	Blua	k2eAgFnPc1d1	Blu-ra
<g/>
,	,	kIx,	,
vyšly	vyjít	k5eAaPmAgFnP	vyjít
knihy	kniha	k1gFnPc1	kniha
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
Glee	Gle	k1gFnSc2	Gle
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc2	aplikace
pro	pro	k7c4	pro
iPad	iPad	k1gInSc4	iPad
a	a	k8xC	a
karaoke	karaokat	k5eAaPmIp3nS	karaokat
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
herní	herní	k2eAgFnSc4d1	herní
konzoli	konzole	k1gFnSc4	konzole
Wii	Wii	k1gFnSc2	Wii
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
představitelů	představitel	k1gMnPc2	představitel
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
Cory	Cora	k1gFnSc2	Cora
Monteith	Monteitha	k1gFnPc2	Monteitha
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
tvůrci	tvůrce	k1gMnPc1	tvůrce
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
zmínil	zmínit	k5eAaPmAgMnS	zmínit
Ryan	Ryan	k1gMnSc1	Ryan
Murphy	Murpha	k1gFnSc2	Murpha
<g/>
)	)	kIx)	)
seriálu	seriál	k1gInSc2	seriál
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vážně	vážně	k6eAd1	vážně
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
okamžitém	okamžitý	k2eAgNnSc6d1	okamžité
zrušení	zrušení	k1gNnSc6	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Monteithovově	Monteithovův	k2eAgFnSc3d1	Monteithovův
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
štáb	štáb	k1gInSc4	štáb
a	a	k8xC	a
herce	herc	k1gInPc4	herc
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
pokračovat	pokračovat	k5eAaImF	pokračovat
nechtějí	chtít	k5eNaImIp3nP	chtít
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
seriál	seriál	k1gInSc1	seriál
ukončen	ukončit	k5eAaPmNgInS	ukončit
pátou	pátá	k1gFnSc7	pátá
sérií	série	k1gFnPc2	série
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
ohlášení	ohlášení	k1gNnSc4	ohlášení
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
objednána	objednat	k5eAaPmNgFnS	objednat
šestá	šestý	k4xOgFnSc1	šestý
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílat	k5eAaPmNgFnS	odvysílat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
také	také	k9	také
díky	díky	k7c3	díky
časopisu	časopis	k1gInSc3	časopis
Entertainment	Entertainment	k1gInSc1	Entertainment
Weekly	Weekl	k1gInPc4	Weekl
dostala	dostat	k5eAaPmAgFnS	dostat
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
Monteith	Monteith	k1gMnSc1	Monteith
žil	žít	k5eAaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
Glee	Glee	k1gNnSc1	Glee
dostávalo	dostávat	k5eAaImAgNnS	dostávat
převážně	převážně	k6eAd1	převážně
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Metacritic	Metacritice	k1gFnPc2	Metacritice
dostal	dostat	k5eAaPmAgMnS	dostat
z	z	k7c2	z
18	[number]	k4	18
hodnocení	hodnocení	k1gNnSc2	hodnocení
průměr	průměr	k1gInSc4	průměr
77	[number]	k4	77
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
řada	řada	k1gFnSc1	řada
získala	získat	k5eAaPmAgFnS	získat
devatenáct	devatenáct	k4xCc4	devatenáct
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgMnPc1	čtyři
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
na	na	k7c4	na
Satellite	Satellit	k1gInSc5	Satellit
Awards	Awardsa	k1gFnPc2	Awardsa
a	a	k8xC	a
57	[number]	k4	57
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nominací	nominace	k1gFnPc2	nominace
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
získán	získat	k5eAaPmNgInS	získat
jeden	jeden	k4xCgInSc1	jeden
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
seriál	seriál	k1gInSc4	seriál
(	(	kIx(	(
<g/>
komedie	komedie	k1gFnSc1	komedie
<g/>
/	/	kIx~	/
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
ceny	cena	k1gFnPc4	cena
Emmy	Emma	k1gFnSc2	Emma
-	-	kIx~	-
z	z	k7c2	z
herců	herc	k1gInPc2	herc
pro	pro	k7c4	pro
Jane	Jan	k1gMnSc5	Jan
Lynch	Lynch	k1gInSc1	Lynch
a	a	k8xC	a
Neila	Neila	k1gFnSc1	Neila
Patricka	Patricka	k1gFnSc1	Patricka
Harrise	Harrise	k1gFnSc1	Harrise
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
epizodní	epizodní	k2eAgFnSc6d1	epizodní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
ze	z	k7c2	z
štábu	štáb	k1gInSc2	štáb
pro	pro	k7c4	pro
Ryana	Ryan	k1gMnSc4	Ryan
Murphyho	Murphy	k1gMnSc4	Murphy
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
pilotního	pilotní	k2eAgInSc2d1	pilotní
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
pět	pět	k4xCc4	pět
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kategorie	kategorie	k1gFnSc2	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
seriál	seriál	k1gInSc4	seriál
(	(	kIx(	(
<g/>
komedie	komedie	k1gFnSc2	komedie
<g/>
)	)	kIx)	)
a	a	k8xC	a
nominací	nominace	k1gFnSc7	nominace
pro	pro	k7c4	pro
Matthewa	Matthew	k2eAgMnSc4d1	Matthew
Morrisona	Morrison	k1gMnSc4	Morrison
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Lynch	Lynch	k1gInSc4	Lynch
<g/>
,	,	kIx,	,
Leu	Lea	k1gFnSc4	Lea
Michele	Michel	k1gInSc2	Michel
a	a	k8xC	a
Chrise	Chrise	k1gFnSc2	Chrise
Colfera	Colfero	k1gNnSc2	Colfero
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
cenu	cena	k1gFnSc4	cena
2	[number]	k4	2
<g/>
.	.	kIx.	.
série	série	k1gFnSc2	série
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
seriál	seriál	k1gInSc4	seriál
(	(	kIx(	(
<g/>
komedie	komedie	k1gFnSc2	komedie
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Lynch	Lynch	k1gInSc1	Lynch
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
seriálu	seriál	k1gInSc2	seriál
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chris	Chris	k1gFnSc1	Chris
Colfer	Colfer	k1gInSc1	Colfer
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
seriálu	seriál	k1gInSc2	seriál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Písně	píseň	k1gFnPc1	píseň
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
seriálu	seriál	k1gInSc2	seriál
Glee	Gle	k1gFnSc2	Gle
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Glee	Gle	k1gFnSc2	Gle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
seriálu	seriál	k1gInSc2	seriál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glee	Glee	k1gFnSc1	Glee
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glee	Gleat	k5eAaPmIp3nS	Gleat
na	na	k7c4	na
SerialZone	SerialZon	k1gInSc5	SerialZon
</s>
</p>
<p>
<s>
Glee	Glee	k6eAd1	Glee
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
</s>
</p>
<p>
<s>
Glee	Gleat	k5eAaPmIp3nS	Gleat
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glee	Glee	k1gFnSc1	Glee
na	na	k7c4	na
TV	TV	kA	TV
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
