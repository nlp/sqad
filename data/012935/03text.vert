<p>
<s>
Soběšovice	Soběšovice	k1gFnSc1	Soběšovice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Sobieschowitz	Sobieschowitz	k1gInSc1	Sobieschowitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Chrášťany	Chrášťan	k1gMnPc7	Chrášťan
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Benešov	Benešov	k1gInSc1	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Chrášťan	Chrášťan	k1gMnSc1	Chrášťan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
13	[number]	k4	13
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Osadou	osada	k1gFnSc7	osada
protéká	protékat	k5eAaImIp3nS	protékat
Tloskovský	Tloskovský	k2eAgInSc1d1	Tloskovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
levostranným	levostranný	k2eAgInSc7d1	levostranný
přítokem	přítok	k1gInSc7	přítok
Janovického	janovický	k2eAgInSc2d1	janovický
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soběšovice	Soběšovice	k1gFnSc1	Soběšovice
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Chrášťany	Chrášťan	k1gMnPc4	Chrášťan
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
11,54	[number]	k4	11,54
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1310	[number]	k4	1310
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ves	ves	k1gFnSc1	ves
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
vojenského	vojenský	k2eAgNnSc2d1	vojenské
cvičiště	cvičiště	k1gNnSc2	cvičiště
Zbraní	zbraň	k1gFnPc2	zbraň
SS	SS	kA	SS
Benešov	Benešov	k1gInSc1	Benešov
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1943	[number]	k4	1943
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
dvůr	dvůr	k1gInSc1	dvůr
čp.	čp.	k?	čp.
1	[number]	k4	1
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Soběšovice	Soběšovice	k1gFnSc2	Soběšovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Chrášťany	Chrášťan	k1gMnPc4	Chrášťan
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
