<p>
<s>
Zbraní	zbraň	k1gFnSc7	zbraň
se	se	k3xPyFc4	se
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
právu	právo	k1gNnSc6	právo
rozumí	rozumět	k5eAaImIp3nS	rozumět
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
učinit	učinit	k5eAaImF	učinit
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
tělu	tělo	k1gNnSc3	tělo
důraznějším	důrazný	k2eAgMnSc6d2	důraznější
<g/>
.	.	kIx.	.
</s>
<s>
Zbraní	zbraň	k1gFnSc7	zbraň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
i	i	k8xC	i
pes	pes	k1gMnSc1	pes
nebo	nebo	k8xC	nebo
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
judikováno	judikován	k2eAgNnSc1d1	judikován
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbraní	zbraň	k1gFnSc7	zbraň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
i	i	k8xC	i
propisovací	propisovací	k2eAgFnSc1d1	propisovací
tužka	tužka	k1gFnSc1	tužka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Některé	některý	k3yIgInPc1	některý
trestné	trestný	k2eAgInPc1d1	trestný
činy	čin	k1gInPc1	čin
jsou	být	k5eAaImIp3nP	být
posuzovány	posuzovat	k5eAaImNgInP	posuzovat
přísněji	přísně	k6eAd2	přísně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
spáchány	spáchat	k5eAaPmNgInP	spáchat
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
útok	útok	k1gInSc1	útok
na	na	k7c4	na
úřední	úřední	k2eAgFnSc4d1	úřední
osobu	osoba	k1gFnSc4	osoba
</s>
</p>
<p>
<s>
nedovolené	dovolený	k2eNgNnSc1d1	nedovolené
překročení	překročení	k1gNnSc1	překročení
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
</s>
</p>
<p>
<s>
vydírání	vydírání	k1gNnSc1	vydírání
</s>
</p>
<p>
<s>
porušování	porušování	k1gNnSc1	porušování
domovní	domovní	k2eAgFnSc2d1	domovní
svobody	svoboda	k1gFnSc2	svoboda
</s>
</p>
<p>
<s>
některé	některý	k3yIgInPc1	některý
trestné	trestný	k2eAgInPc1d1	trestný
činy	čin	k1gInPc1	čin
vojenské	vojenský	k2eAgInPc1d1	vojenský
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zběhnutíProtože	zběhnutíProtože	k6eAd1	zběhnutíProtože
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
právním	právní	k2eAgInSc6d1	právní
řádu	řád	k1gInSc6	řád
není	být	k5eNaImIp3nS	být
obecná	obecný	k2eAgFnSc1d1	obecná
definice	definice	k1gFnSc1	definice
zbraně	zbraň	k1gFnSc2	zbraň
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
její	její	k3xOp3gFnSc1	její
aplikovatelnost	aplikovatelnost	k1gFnSc1	aplikovatelnost
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
výkladu	výklad	k1gInSc6	výklad
ustanovení	ustanovení	k1gNnSc2	ustanovení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
svobodě	svoboda	k1gFnSc6	svoboda
shromažďování	shromažďování	k1gNnSc2	shromažďování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střelné	střelný	k2eAgFnPc1d1	střelná
zbraně	zbraň	k1gFnPc1	zbraň
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
několika	několik	k4yIc2	několik
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mohou	moct	k5eAaImIp3nP	moct
lidé	člověk	k1gMnPc1	člověk
kromě	kromě	k7c2	kromě
policistů	policista	k1gMnPc2	policista
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
nosit	nosit	k5eAaImF	nosit
zbraň	zbraň	k1gFnSc4	zbraň
pouze	pouze	k6eAd1	pouze
skrytou	skrytý	k2eAgFnSc7d1	skrytá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
