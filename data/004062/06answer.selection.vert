<s>
Pomerančovník	pomerančovník	k1gInSc1	pomerančovník
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
mělce	mělce	k6eAd1	mělce
kořenící	kořenící	k2eAgInSc1d1	kořenící
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kulovitou	kulovitý	k2eAgFnSc4d1	kulovitá
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
trnité	trnitý	k2eAgFnSc2d1	trnitá
větvičky	větvička	k1gFnSc2	větvička
<g/>
.	.	kIx.	.
</s>
