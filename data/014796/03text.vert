<s>
Sklenice	sklenice	k1gFnPc1
</s>
<s>
Sklenice	sklenice	k1gFnPc1
s	s	k7c7
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Sklenice	sklenice	k1gFnPc1
se	s	k7c7
lžící	lžíce	k1gFnSc7
na	na	k7c4
absint	absint	k1gInSc4
</s>
<s>
Silnostěnná	silnostěnný	k2eAgFnSc1d1
koktejlová	koktejlový	k2eAgFnSc1d1
sklenka	sklenka	k1gFnSc1
</s>
<s>
Sklenice	sklenice	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
zdrobněle	zdrobněle	k6eAd1
sklenička	sklenička	k1gFnSc1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
sklenka	sklenka	k1gFnSc1
<g/>
,	,	kIx,
sklinka	sklinka	k1gFnSc1
<g/>
,	,	kIx,
sklínka	sklínka	k1gFnSc1
nebo	nebo	k8xC
jen	jen	k9
sklo	sklo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nádoba	nádoba	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
při	při	k7c6
stolování	stolování	k1gNnSc6
k	k	k7c3
servírování	servírování	k1gNnSc3
a	a	k8xC
pití	pití	k1gNnSc3
nápojů	nápoj	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
jídle	jídlo	k1gNnSc6
pochutin	pochutina	k1gFnPc2
nebo	nebo	k8xC
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
uchovávání	uchovávání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
většinou	většinou	k6eAd1
foukáním	foukání	k1gNnSc7
nebo	nebo	k8xC
lisováním	lisování	k1gNnSc7
z	z	k7c2
průhledného	průhledný	k2eAgNnSc2d1
nebo	nebo	k8xC
průsvitného	průsvitný	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
o	o	k7c6
různé	různý	k2eAgFnSc6d1
tloušťce	tloušťka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
druhu	druh	k1gInSc6
nápoje	nápoj	k1gInSc2
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
je	být	k5eAaImIp3nS
určena	určen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklo	sklo	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
pohled	pohled	k1gInSc4
na	na	k7c4
konzumovaný	konzumovaný	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
slovo	slovo	k1gNnSc1
sklenička	sklenička	k1gFnSc1
užívá	užívat	k5eAaImIp3nS
expresivně	expresivně	k6eAd1
pro	pro	k7c4
vypití	vypití	k1gNnSc4
1	#num#	k4
sklenky	sklenka	k1gFnSc2
alkoholického	alkoholický	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
pozvání	pozvánět	k5eAaImIp3nS
k	k	k7c3
pití	pití	k1gNnSc3
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nezajdeme	zajít	k5eNaPmIp1nP
na	na	k7c4
skleničku	sklenička	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Jednodušší	jednoduchý	k2eAgFnPc1d2
skleničky	sklenička	k1gFnPc1
mají	mít	k5eAaImIp3nP
tvar	tvar	k1gInSc4
válcovité	válcovitý	k2eAgFnSc2d1
nádoby	nádoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěny	stěn	k1gInPc4
a	a	k8xC
dno	dno	k1gNnSc4
uvnitř	uvnitř	k6eAd1
jsou	být	k5eAaImIp3nP
hladké	hladký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plášť	plášť	k1gInSc1
(	(	kIx(
<g/>
stěny	stěna	k1gFnPc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
různě	různě	k6eAd1
tvarován	tvarován	k2eAgInSc4d1
(	(	kIx(
<g/>
vydutý	vydutý	k2eAgInSc4d1
<g/>
,	,	kIx,
kónický	kónický	k2eAgInSc4d1
<g/>
,	,	kIx,
zvlněný	zvlněný	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
opatřen	opatřen	k2eAgInSc1d1
plastickou	plastický	k2eAgFnSc7d1
dekorací	dekorace	k1gFnSc7
(	(	kIx(
<g/>
žebrování	žebrování	k1gNnSc1
<g/>
,	,	kIx,
zrnění	zrnění	k1gNnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
ouškem	ouško	k1gNnSc7
pro	pro	k7c4
uchopení	uchopení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Značková	značkový	k2eAgFnSc1d1
sklenka	sklenka	k1gFnSc1
Durit	Durita	k1gFnPc2
z	z	k7c2
Inwaldovy	Inwaldův	k2eAgFnSc2d1
sklárny	sklárna	k1gFnSc2
na	na	k7c6
Zlíchově	Zlíchův	k2eAgFnSc6d1
1914-1940	1914-1940	k4
</s>
<s>
Barevné	barevný	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
druhotných	druhotný	k2eAgFnPc2d1
duritek	duritka	k1gFnPc2
ze	z	k7c2
státní	státní	k2eAgFnSc2d1
sklárny	sklárna	k1gFnSc2
</s>
<s>
Dno	dno	k1gNnSc1
sklenice	sklenice	k1gFnSc2
se	s	k7c7
značkou	značka	k1gFnSc7
Skláren	sklárna	k1gFnPc2
Moravia	Moravius	k1gMnSc2
n.	n.	k?
<g/>
p.	p.	k?
</s>
<s>
Dno	dno	k1gNnSc1
sklenice	sklenice	k1gFnSc2
se	s	k7c7
značkou	značka	k1gFnSc7
Rudolfovy	Rudolfův	k2eAgFnSc2d1
huti	huť	k1gFnSc2
</s>
<s>
Sklenice	sklenice	k1gFnPc1
od	od	k7c2
hořčice	hořčice	k1gFnSc2
výrobce	výrobce	k1gMnSc2
Seliko	Selika	k1gFnSc5
n.	n.	k?
<g/>
p.	p.	k?
Ostrava	Ostrava	k1gFnSc1
Třebovice	Třebovice	k1gFnSc1
s	s	k7c7
víčky	víčko	k1gNnPc7
z	z	k7c2
let	léto	k1gNnPc2
1967	#num#	k4
-	-	kIx~
1969	#num#	k4
</s>
<s>
Dva	dva	k4xCgInPc1
typy	typ	k1gInPc1
českých	český	k2eAgFnPc2d1
sklenic	sklenice	k1gFnPc2
od	od	k7c2
hořčice	hořčice	k1gFnSc2
</s>
<s>
Sklenka	sklenka	k1gFnSc1
od	od	k7c2
hořčice	hořčice	k1gFnSc2
značky	značka	k1gFnSc2
Amora	Amor	k1gMnSc2
<g/>
,	,	kIx,
lisované	lisovaný	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
1985	#num#	k4
<g/>
-dosud	-dosuda	k1gFnPc2
</s>
<s>
Druhy	druh	k1gInPc1
sklenic	sklenice	k1gFnPc2
</s>
<s>
sklenka	sklenka	k1gFnSc1
či	či	k8xC
sklenička	sklenička	k1gFnSc1
–	–	k?
nejčastější	častý	k2eAgFnSc1d3
picí	picí	k2eAgFnSc1d1
nádobka	nádobka	k1gFnSc1
o	o	k7c6
objemu	objem	k1gInSc6
cca	cca	kA
0,2	0,2	k4
litru	litr	k1gInSc2
pro	pro	k7c4
konzumaci	konzumace	k1gFnSc4
nápojů	nápoj	k1gInPc2
</s>
<s>
odlivka	odlivka	k1gFnSc1
–	–	k?
sklenka	sklenka	k1gFnSc1
poloviční	poloviční	k2eAgFnSc2d1
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
objemu	objem	k1gInSc6
cca	cca	kA
0,1	0,1	k4
litru	litr	k1gInSc2
</s>
<s>
půllitr	půllitr	k1gInSc1
–	–	k?
silnostěnná	silnostěnný	k2eAgFnSc1d1
nádoba	nádoba	k1gFnSc1
o	o	k7c6
obsahu	obsah	k1gInSc6
0,5	0,5	k4
litru	litr	k1gInSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
uchem	ucho	k1gNnSc7
<g/>
,	,	kIx,
ke	k	k7c3
konzumaci	konzumace	k1gFnSc3
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
míchaných	míchaný	k2eAgFnPc2d1
limonád	limonáda	k1gFnPc2
<g/>
;	;	kIx,
název	název	k1gInSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
přenáší	přenášet	k5eAaImIp3nS
i	i	k9
na	na	k7c4
menší	malý	k2eAgFnPc4d2
sklenice	sklenice	k1gFnPc4
(	(	kIx(
<g/>
půllitřík	půllitřík	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
větší	veliký	k2eAgFnSc1d2
sklenice	sklenice	k1gFnSc1
podobného	podobný	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
</s>
<s>
tuplák	tuplák	k1gInSc1
-	-	kIx~
pivní	pivní	k2eAgFnSc1d1
sklenice	sklenice	k1gFnSc1
dvojité	dvojitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
úzká	úzký	k2eAgFnSc1d1
a	a	k8xC
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
1	#num#	k4
litrová	litrový	k2eAgFnSc1d1
</s>
<s>
třetinka	třetinka	k1gFnSc1
-	-	kIx~
nádoba	nádoba	k1gFnSc1
o	o	k7c6
obsahu	obsah	k1gInSc6
0,3	0,3	k4
litru	litr	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
tradiční	tradiční	k2eAgFnSc1d1
míra	míra	k1gFnSc1
pro	pro	k7c4
malé	malý	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
</s>
<s>
likérka	likérka	k1gFnSc1
(	(	kIx(
<g/>
panáková	panákový	k2eAgFnSc1d1
sklenička	sklenička	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
malá	malý	k2eAgFnSc1d1
sklenička	sklenička	k1gFnSc1
o	o	k7c6
objemu	objem	k1gInSc6
0,04	0,04	k4
<g/>
/	/	kIx~
<g/>
0,05	0,05	k4
litru	litr	k1gInSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
velký	velký	k2eAgInSc1d1
panák	panák	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
0,02	0,02	k4
litru	litr	k1gInSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
malý	malý	k2eAgInSc1d1
panák	panák	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převážně	převážně	k6eAd1
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
konzumaci	konzumace	k1gFnSc4
tvrdého	tvrdý	k2eAgInSc2d1
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
odměrka	odměrka	k1gFnSc1
–	–	k?
s	s	k7c7
vyznačenými	vyznačený	k2eAgFnPc7d1
objemovými	objemový	k2eAgFnPc7d1
mírami	míra	k1gFnPc7,k1gFnPc7wR
</s>
<s>
sklenice	sklenice	k1gFnSc1
na	na	k7c4
víno	víno	k1gNnSc4
–	–	k?
má	mít	k5eAaImIp3nS
nožku	nožka	k1gFnSc4
na	na	k7c6
kruhové	kruhový	k2eAgFnSc6d1
podstavě	podstava	k1gFnSc6
<g/>
,	,	kIx,
soudkový	soudkový	k2eAgInSc4d1
tvar	tvar	k1gInSc4
a	a	k8xC
rozšířené	rozšířený	k2eAgNnSc4d1
hrdlo	hrdlo	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
usnadňovala	usnadňovat	k5eAaImAgFnS
vychutnání	vychutnání	k1gNnSc2
vína	víno	k1gNnSc2
pomocí	pomocí	k7c2
čichu	čich	k1gInSc2
</s>
<s>
sklenice	sklenice	k1gFnSc1
na	na	k7c4
brandy	brandy	k1gFnSc4
či	či	k8xC
koňak	koňak	k1gInSc4
–	–	k?
typ	typ	k1gInSc1
koktejlový	koktejlový	k2eAgInSc1d1
bez	bez	k7c2
stopky	stopka	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
tzv.	tzv.	kA
napoleonka	napoleonka	k1gFnSc1
<g/>
,	,	kIx,
široká	široký	k2eAgFnSc1d1
se	s	k7c7
zužujícím	zužující	k2eAgMnSc7d1
se	se	k3xPyFc4
hrdlem	hrdlo	k1gNnSc7
<g/>
,	,	kIx,
na	na	k7c6
nízké	nízký	k2eAgFnSc6d1
stopce	stopka	k1gFnSc6
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
"	"	kIx"
<g/>
zahřívání	zahřívání	k1gNnSc4
<g/>
"	"	kIx"
v	v	k7c6
dlani	dlaň	k1gFnSc6
</s>
<s>
sklenice	sklenice	k1gFnSc1
na	na	k7c4
grog	grog	k1gInSc4
<g/>
,	,	kIx,
punč	punč	k1gInSc4
<g/>
,	,	kIx,
absint	absint	k1gInSc4
či	či	k8xC
svařené	svařený	k2eAgNnSc4d1
víno	víno	k1gNnSc4
-	-	kIx~
silnostěnné	silnostěnný	k2eAgFnSc2d1
lisované	lisovaný	k2eAgFnSc2d1
sklenice	sklenice	k1gFnSc2
<g/>
;	;	kIx,
nalévání	nalévání	k1gNnSc2
horkého	horký	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
se	se	k3xPyFc4
doporučuje	doporučovat	k5eAaImIp3nS
přes	přes	k7c4
vloženou	vložený	k2eAgFnSc4d1
lžičku	lžička	k1gFnSc4
</s>
<s>
varná	varný	k2eAgFnSc1d1
sklenka	sklenka	k1gFnSc1
-	-	kIx~
čirá	čirý	k2eAgFnSc1d1
tenkostěnná	tenkostěnný	k2eAgFnSc1d1
na	na	k7c4
horké	horký	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
<g/>
,	,	kIx,
často	často	k6eAd1
v	v	k7c6
kovovém	kovový	k2eAgInSc6d1
stojánku	stojánek	k1gInSc6
s	s	k7c7
ouškem	ouško	k1gNnSc7
</s>
<s>
sklenice	sklenice	k1gFnSc1
na	na	k7c4
sekt	sekt	k1gInSc4
nebo	nebo	k8xC
na	na	k7c4
šampus	šampus	k1gInSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
šampuska	šampuska	k1gFnSc1
</s>
<s>
kónická	kónický	k2eAgFnSc1d1
číše	číše	k1gFnSc1
<g/>
,	,	kIx,
užívaná	užívaný	k2eAgFnSc1d1
také	také	k9
v	v	k7c6
lékařství	lékařství	k1gNnSc6
<g/>
,	,	kIx,
např.	např.	kA
na	na	k7c4
moč	moč	k1gFnSc4
</s>
<s>
úzká	úzký	k2eAgFnSc1d1
a	a	k8xC
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
píšťala	píšťala	k1gFnSc1
<g/>
–	–	k?
určená	určený	k2eAgFnSc1d1
ke	k	k7c3
zvýraznění	zvýraznění	k1gNnSc3
tzv.	tzv.	kA
řetízkování	řetízkování	k1gNnSc3
bublinek	bublinka	k1gFnPc2
sektu	sekt	k1gInSc2
ze	z	k7c2
dna	dno	k1gNnSc2
sklenky	sklenka	k1gFnSc2
</s>
<s>
lékovka	lékovka	k1gFnSc1
–	–	k?
malá	malý	k2eAgFnSc1d1
sklenička	sklenička	k1gFnSc1
na	na	k7c4
léky	lék	k1gInPc4
</s>
<s>
s	s	k7c7
rozšířeným	rozšířený	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
hrdla	hrdlo	k1gNnSc2
pro	pro	k7c4
namačkávání	namačkávání	k1gNnSc4
pružného	pružný	k2eAgNnSc2d1
víčka	víčko	k1gNnSc2
</s>
<s>
se	s	k7c7
závitem	závit	k1gInSc7
pro	pro	k7c4
našroubování	našroubování	k1gNnSc4
tvrdého	tvrdý	k2eAgNnSc2d1
víčka	víčko	k1gNnSc2
</s>
<s>
kojenecká	kojenecký	k2eAgFnSc1d1
sklenice	sklenice	k1gFnSc1
–	–	k?
speciální	speciální	k2eAgFnPc4d1
sklenice	sklenice	k1gFnPc4
<g/>
,	,	kIx,
resp	resp	kA
<g/>
,	,	kIx,
láhev	láhev	k1gFnSc1
pro	pro	k7c4
výživu	výživa	k1gFnSc4
kojenců	kojenec	k1gMnPc2
přes	přes	k7c4
dudlík	dudlík	k1gInSc4
</s>
<s>
zavařovací	zavařovací	k2eAgFnSc1d1
sklenice	sklenice	k1gFnSc1
<g/>
,	,	kIx,
zavařovačka	zavařovačka	k1gFnSc1
pro	pro	k7c4
kompoty	kompot	k1gInPc4
<g/>
,	,	kIx,
zavařeniny	zavařenina	k1gFnPc4
nebo	nebo	k8xC
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
se	s	k7c7
skleněným	skleněný	k2eAgNnSc7d1
víčkem	víčko	k1gNnSc7
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
s	s	k7c7
plechovým	plechový	k2eAgInSc7d1
klipsem	klips	k1gInSc7
nebo	nebo	k8xC
s	s	k7c7
drátěnou	drátěný	k2eAgFnSc7d1
svorkou	svorka	k1gFnSc7
<g/>
;	;	kIx,
k	k	k7c3
nejstarším	starý	k2eAgFnPc3d3
středoevropským	středoevropský	k2eAgFnPc3d1
značkám	značka	k1gFnPc3
dosud	dosud	k6eAd1
vyráběným	vyráběný	k2eAgMnPc3d1
patří	patřit	k5eAaImIp3nS
Weckovky	Weckovka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zavedl	zavést	k5eAaPmAgMnS
Johann	Johann	k1gMnSc1
Weck	Weck	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
masťovka	masťovka	k1gFnSc1
–	–	k?
sklenice	sklenice	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
skladování	skladování	k1gNnSc4
škvařeného	škvařený	k2eAgNnSc2d1
sádla	sádlo	k1gNnSc2
</s>
<s>
Duritka	Duritka	k1gFnSc1
</s>
<s>
Duritka	Duritka	k1gFnSc1
je	být	k5eAaImIp3nS
typ	typ	k1gInSc4
stolní	stolní	k2eAgFnSc2d1
sklenky	sklenka	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
zavedla	zavést	k5eAaPmAgFnS
Sklárna	sklárna	k1gFnSc1
a	a	k8xC
rafinerie	rafinerie	k1gFnSc1
Josef	Josef	k1gMnSc1
Inwald	Inwald	k1gMnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Zlíchově	Zlíchův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
také	také	k9
ze	z	k7c2
skláren	sklárna	k1gFnPc2
Rudolfova	Rudolfův	k2eAgFnSc1d1
huť	huť	k1gFnSc1
v	v	k7c6
Dubí	dubí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značkové	značkový	k2eAgInPc1d1
Duritky	Duritek	k1gInPc1
se	se	k3xPyFc4
vyráběly	vyrábět	k5eAaImAgInP
v	v	k7c6
letech	léto	k1gNnPc6
1914	#num#	k4
<g/>
-	-	kIx~
<g/>
1940	#num#	k4
<g/>
,	,	kIx,
neznačkové	značkový	k2eNgNnSc4d1
státní	státní	k2eAgInSc4d1
až	až	k8xS
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
předností	přednost	k1gFnSc7
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
dána	dát	k5eAaPmNgFnS
způsobem	způsob	k1gInSc7
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
předpjaté	předpjatý	k2eAgNnSc4d1
sklo	sklo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžné	běžný	k2eAgFnPc4d1
sklenice	sklenice	k1gFnPc4
se	se	k3xPyFc4
vyfoukly	vyfouknout	k5eAaPmAgFnP
nebo	nebo	k8xC
vylisovaly	vylisovat	k5eAaPmAgFnP
a	a	k8xC
potom	potom	k6eAd1
pomalu	pomalu	k6eAd1
ochlazovaly	ochlazovat	k5eAaImAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
popraskání	popraskání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpjaté	předpjatý	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
se	se	k3xPyFc4
však	však	k9
po	po	k7c6
vytavení	vytavení	k1gNnSc6
a	a	k8xC
vylisování	vylisování	k1gNnSc6
zchladilo	zchladit	k5eAaPmAgNnS
prudce	prudko	k6eAd1
(	(	kIx(
<g/>
kalení	kalení	k1gNnSc1
skla	sklo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rudolfově	Rudolfův	k2eAgFnSc6d1
huti	huť	k1gFnSc6
používali	používat	k5eAaImAgMnP
k	k	k7c3
chlazení	chlazení	k1gNnSc3
olej	olej	k1gInSc1
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
vodu	voda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přívlastek	přívlastek	k1gInSc1
„	„	k?
<g/>
nerozbitná	rozbitný	k2eNgFnSc1d1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
jen	jen	k9
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rozbití	rozbití	k1gNnSc3
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
tlakem	tlak	k1gInSc7
<g/>
,	,	kIx,
nárazem	náraz	k1gInSc7
na	na	k7c4
horní	horní	k2eAgFnSc4d1
hranu	hrana	k1gFnSc4
<g/>
,	,	kIx,
pádem	pád	k1gInSc7
na	na	k7c4
kamennou	kamenný	k2eAgFnSc4d1
podlahu	podlaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střepy	střep	k1gInPc7
se	se	k3xPyFc4
tříští	tříštit	k5eAaImIp3nS
na	na	k7c4
tisíce	tisíc	k4xCgInPc4
kousků	kousek	k1gInPc2
<g/>
;	;	kIx,
nebývají	bývat	k5eNaImIp3nP
ostrohranné	ostrohranný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Hořčičák	Hořčičák	k1gMnSc1
</s>
<s>
Hořčičák	Hořčičák	k1gInSc1
je	být	k5eAaImIp3nS
hovorové	hovorový	k2eAgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
skleničku	sklenička	k1gFnSc4
od	od	k7c2
hořčice	hořčice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklenice	sklenice	k1gFnSc1
pro	pro	k7c4
výrobce	výrobce	k1gMnPc4
hořčice	hořčice	k1gFnSc2
vyrábějí	vyrábět	k5eAaImIp3nP
sklárny	sklárna	k1gFnPc1
v	v	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Čechách	Čechy	k1gFnPc6
byly	být	k5eAaImAgFnP
tradičními	tradiční	k2eAgInPc7d1
výrobci	výrobce	k1gMnPc7
sklářské	sklářský	k2eAgInPc4d1
závody	závod	k1gInPc4
Rudolfova	Rudolfův	k2eAgFnSc1d1
huť	huť	k1gFnSc1
v	v	k7c6
Dubí	dubí	k1gNnSc6
nebo	nebo	k8xC
Sklárny	sklárna	k1gFnSc2
Moravia	Moravium	k1gNnSc2
n.	n.	k?
<g/>
p.	p.	k?
Objem	objem	k1gInSc1
sklenic	sklenice	k1gFnPc2
byl	být	k5eAaImAgInS
0,2	0,2	k4
litru	litr	k1gInSc2
a	a	k8xC
vyskytovaly	vyskytovat	k5eAaImAgInP
se	se	k3xPyFc4
čiré	čirý	k2eAgInPc1d1
i	i	k8xC
tónované	tónovaný	k2eAgInPc1d1
(	(	kIx(
<g/>
např.	např.	kA
namodralé	namodralý	k2eAgInPc1d1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
několika	několik	k4yIc6
tvarových	tvarový	k2eAgFnPc6d1
variantách	varianta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
naplnění	naplnění	k1gNnSc6
hořčicí	hořčice	k1gFnSc7
se	se	k3xPyFc4
sklenička	sklenička	k1gFnSc1
uzavírala	uzavírat	k5eAaPmAgFnS,k5eAaImAgFnS
papírovým	papírový	k2eAgMnPc3d1
a	a	k8xC
později	pozdě	k6eAd2
plastovým	plastový	k2eAgNnSc7d1
víčkem	víčko	k1gNnSc7
opatřeným	opatřený	k2eAgInSc7d1
potiskem	potisk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
hořčičné	hořčičný	k2eAgFnSc2d1
sklenice	sklenice	k1gFnSc2
designových	designový	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
<g/>
,	,	kIx,
často	často	k6eAd1
z	z	k7c2
Francie	Francie	k1gFnSc2
nebo	nebo	k8xC
z	z	k7c2
Nizozemí	Nizozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
a	a	k8xC
cenová	cenový	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
hořčičných	hořčičný	k2eAgFnPc2d1
sklenic	sklenice	k1gFnPc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
masovému	masový	k2eAgNnSc3d1
rozšíření	rozšíření	k1gNnSc3
v	v	k7c6
domácnostech	domácnost	k1gFnPc6
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
k	k	k7c3
pití	pití	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
kuchyni	kuchyně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
jako	jako	k9
odměrky	odměrka	k1gFnPc1
v	v	k7c6
kuchyňských	kuchyňský	k2eAgInPc6d1
receptech	recept	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.mcm-europe.fr/en/content/15-the-weck-glassware-compagny	https://www.mcm-europe.fr/en/content/15-the-weck-glassware-compagn	k1gInPc1
<g/>
↑	↑	k?
http://ekonomika.idnes.cz/duritky-nerozbitne-jen-nekdy-exploduji-f01-/ekonomika.aspx?c=A050628_101941_ekonomika_plz	http://ekonomika.idnes.cz/duritky-nerozbitne-jen-nekdy-exploduji-f01-/ekonomika.aspx?c=A050628_101941_ekonomika_plz	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
lisované	lisovaný	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
sympozium	sympozium	k1gNnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostislav	Rostislav	k1gMnSc1
Vajdák	Vajdák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gottwaldov	Gottwaldov	k1gInSc1
1972	#num#	k4
</s>
<s>
Olga	Olga	k1gFnSc1
Drahotová	Drahotový	k2eAgFnSc1d1
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
1980	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Číše	číše	k1gFnSc1
</s>
<s>
hrnek	hrnek	k1gInSc1
</s>
<s>
láhev	láhev	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
sklenice	sklenice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
sklenice	sklenice	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4442245-3	4442245-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
87003624	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
87003624	#num#	k4
</s>
