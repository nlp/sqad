<p>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
snídaně	snídaně	k1gFnSc1	snídaně
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
full	full	k1gMnSc1	full
breakfast	breakfast	k1gMnSc1	breakfast
nebo	nebo	k8xC	nebo
full	full	k1gMnSc1	full
English	English	k1gMnSc1	English
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
teplé	teplý	k2eAgNnSc1d1	teplé
ranní	ranní	k2eAgNnSc1d1	ranní
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snídaňový	Snídaňový	k2eAgInSc4d1	Snídaňový
talíř	talíř	k1gInSc4	talíř
tvoří	tvořit	k5eAaImIp3nS	tvořit
opečená	opečený	k2eAgFnSc1d1	opečená
slanina	slanina	k1gFnSc1	slanina
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
masové	masový	k2eAgInPc1d1	masový
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
klobásy	klobása	k1gFnPc1	klobása
<g/>
,	,	kIx,	,
párky	párek	k1gInPc1	párek
nebo	nebo	k8xC	nebo
jelito	jelito	k1gNnSc1	jelito
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
vejci	vejce	k1gNnSc3	vejce
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
upravenými	upravený	k2eAgFnPc7d1	upravená
jako	jako	k8xS	jako
volské	volský	k2eAgNnSc1d1	volské
oko	oko	k1gNnSc1	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kousky	kousek	k1gInPc4	kousek
rajčat	rajče	k1gNnPc2	rajče
a	a	k8xC	a
žampionů	žampion	k1gInPc2	žampion
orestovaných	orestovaný	k2eAgInPc2d1	orestovaný
na	na	k7c6	na
tuku	tuk	k1gInSc6	tuk
a	a	k8xC	a
fazolemi	fazole	k1gFnPc7	fazole
v	v	k7c6	v
tomatové	tomatový	k2eAgFnSc6d1	tomatová
omáčce	omáčka	k1gFnSc6	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Přílohou	příloha	k1gFnSc7	příloha
jsou	být	k5eAaImIp3nP	být
opečené	opečený	k2eAgInPc1d1	opečený
toasty	toast	k1gInPc1	toast
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
především	především	k9	především
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hash	hasha	k1gFnPc2	hasha
browns	browns	k1gInSc1	browns
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
bramboráku	bramborák	k1gInSc2	bramborák
z	z	k7c2	z
uvařených	uvařený	k2eAgFnPc2d1	uvařená
<g/>
,	,	kIx,	,
nastrouhaných	nastrouhaný	k2eAgFnPc2d1	nastrouhaná
a	a	k8xC	a
osmažených	osmažený	k2eAgFnPc2d1	osmažená
brambor	brambora	k1gFnPc2	brambora
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lívance	lívanec	k1gInPc1	lívanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dochucení	dochucení	k1gNnSc6	dochucení
jídla	jídlo	k1gNnSc2	jídlo
bývá	bývat	k5eAaImIp3nS	bývat
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
kečup	kečup	k1gInSc1	kečup
nebo	nebo	k8xC	nebo
hnědá	hnědý	k2eAgFnSc1d1	hnědá
omáčka	omáčka	k1gFnSc1	omáčka
HP	HP	kA	HP
sauce	sauce	k1gFnPc1	sauce
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
a	a	k8xC	a
sladového	sladový	k2eAgInSc2d1	sladový
octa	ocet	k1gInSc2	ocet
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
krajových	krajový	k2eAgFnPc2d1	krajová
obměn	obměna	k1gFnPc2	obměna
základního	základní	k2eAgInSc2d1	základní
receptu	recept	k1gInSc2	recept
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
snídani	snídaně	k1gFnSc4	snídaně
haggis	haggis	k1gFnSc2	haggis
a	a	k8xC	a
ovesné	ovesný	k2eAgInPc1d1	ovesný
suchary	suchar	k1gInPc1	suchar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
zase	zase	k9	zase
laverbread	laverbread	k6eAd1	laverbread
(	(	kIx(	(
<g/>
dušené	dušený	k2eAgFnPc1d1	dušená
mořské	mořský	k2eAgFnPc1d1	mořská
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
uzený	uzený	k2eAgMnSc1d1	uzený
sleď	sleď	k1gMnSc1	sleď
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nápojů	nápoj	k1gInPc2	nápoj
se	se	k3xPyFc4	se
k	k	k7c3	k
anglické	anglický	k2eAgFnSc3d1	anglická
snídani	snídaně	k1gFnSc3	snídaně
podává	podávat	k5eAaImIp3nS	podávat
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
nebo	nebo	k8xC	nebo
ovocné	ovocný	k2eAgInPc4d1	ovocný
džusy	džus	k1gInPc4	džus
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
vegetariánská	vegetariánský	k2eAgFnSc1d1	vegetariánská
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgMnPc1d1	využívající
náhražek	náhražka	k1gFnPc2	náhražka
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
<g/>
Tradice	tradice	k1gFnSc1	tradice
vydatných	vydatný	k2eAgFnPc2d1	vydatná
snídaní	snídaně	k1gFnPc2	snídaně
"	"	kIx"	"
<g/>
na	na	k7c4	na
vidličku	vidlička	k1gFnSc4	vidlička
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
u	u	k7c2	u
anglické	anglický	k2eAgFnSc2d1	anglická
venkovské	venkovský	k2eAgFnSc2d1	venkovská
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
vrstev	vrstva	k1gFnPc2	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
zvláště	zvláště	k6eAd1	zvláště
manuálně	manuálně	k6eAd1	manuálně
pracující	pracující	k2eAgMnPc1d1	pracující
<g/>
.	.	kIx.	.
</s>
<s>
Isabella	Isabelt	k5eAaBmAgFnS	Isabelt
Beetonová	Beetonový	k2eAgFnSc1d1	Beetonový
tuto	tento	k3xDgFnSc4	tento
snídani	snídaně	k1gFnSc4	snídaně
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
klasické	klasický	k2eAgFnSc6d1	klasická
kuchařské	kuchařský	k2eAgFnSc6d1	kuchařská
knize	kniha	k1gFnSc6	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
viktoriánského	viktoriánský	k2eAgInSc2d1	viktoriánský
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
její	její	k3xOp3gFnSc2	její
konzumace	konzumace	k1gFnSc2	konzumace
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
víkendy	víkend	k1gInPc4	víkend
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nP	chodit
jíst	jíst	k5eAaImF	jíst
do	do	k7c2	do
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
anglická	anglický	k2eAgFnSc1d1	anglická
snídaně	snídaně	k1gFnSc1	snídaně
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1190	[number]	k4	1190
kalorií	kalorie	k1gFnPc2	kalorie
<g/>
.	.	kIx.	.
<g/>
Protipólem	protipól	k1gInSc7	protipól
anglické	anglický	k2eAgFnSc2d1	anglická
snídaně	snídaně	k1gFnSc2	snídaně
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
snídaně	snídaně	k1gFnSc1	snídaně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
studená	studený	k2eAgFnSc1d1	studená
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
pečivo	pečivo	k1gNnSc1	pečivo
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgNnSc1d1	doplněné
máslem	máslo	k1gNnSc7	máslo
<g/>
,	,	kIx,	,
sýrem	sýr	k1gInSc7	sýr
<g/>
,	,	kIx,	,
medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
marmeládou	marmeláda	k1gFnSc7	marmeláda
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
uzenin	uzenina	k1gFnPc2	uzenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
a	a	k8xC	a
restauracích	restaurace	k1gFnPc6	restaurace
zpravidla	zpravidla	k6eAd1	zpravidla
dostávají	dostávat	k5eAaImIp3nP	dostávat
hosté	host	k1gMnPc1	host
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Snídaně	snídaně	k1gFnSc1	snídaně
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anglická	anglický	k2eAgFnSc1d1	anglická
snídaně	snídaně	k1gFnSc1	snídaně
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.englishbreakfastsociety.com/full-english-breakfast.html	[url]	k1gMnSc1	http://www.englishbreakfastsociety.com/full-english-breakfast.html
</s>
</p>
<p>
<s>
http://www.tastejourney.cz/2014/03/anglicka-snidane-v-britskych-hospodach-podavaji-i-k-veceri/	[url]	k4	http://www.tastejourney.cz/2014/03/anglicka-snidane-v-britskych-hospodach-podavaji-i-k-veceri/
</s>
</p>
