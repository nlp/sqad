<s>
Přílohou	příloha	k1gFnSc7	příloha
jsou	být	k5eAaImIp3nP	být
opečené	opečený	k2eAgInPc1d1	opečený
toasty	toast	k1gInPc1	toast
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
především	především	k9	především
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hash	hasha	k1gFnPc2	hasha
browns	browns	k1gInSc1	browns
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
bramboráku	bramborák	k1gInSc2	bramborák
z	z	k7c2	z
uvařených	uvařený	k2eAgFnPc2d1	uvařená
<g/>
,	,	kIx,	,
nastrouhaných	nastrouhaný	k2eAgFnPc2d1	nastrouhaná
a	a	k8xC	a
osmažených	osmažený	k2eAgFnPc2d1	osmažená
brambor	brambora	k1gFnPc2	brambora
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lívance	lívanec	k1gInPc1	lívanec
<g/>
.	.	kIx.	.
</s>
