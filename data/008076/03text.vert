<s>
Těhotenství	těhotenství	k1gNnSc1	těhotenství
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
žena	žena	k1gFnSc1	žena
vynosí	vynosit	k5eAaPmIp3nS	vynosit
živé	živý	k2eAgMnPc4d1	živý
potomky	potomek	k1gMnPc4	potomek
od	od	k7c2	od
početí	početí	k1gNnSc2	početí
(	(	kIx(	(
<g/>
koncepce	koncepce	k1gFnSc1	koncepce
<g/>
)	)	kIx)	)
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
potomstvo	potomstvo	k1gNnSc4	potomstvo
schopno	schopen	k2eAgNnSc1d1	schopno
života	život	k1gInSc2	život
mimo	mimo	k7c4	mimo
dělohu	děloha	k1gFnSc4	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
početím	početí	k1gNnSc7	početí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
oplodnění	oplodnění	k1gNnSc2	oplodnění
a	a	k8xC	a
formování	formování	k1gNnSc2	formování
zygoty	zygota	k1gFnSc2	zygota
(	(	kIx(	(
<g/>
oplozená	oplozený	k2eAgFnSc1d1	oplozená
vaječná	vaječný	k2eAgFnSc1d1	vaječná
buňka	buňka	k1gFnSc1	buňka
po	po	k7c6	po
splynutí	splynutí	k1gNnSc6	splynutí
samčí	samčí	k2eAgFnSc2d1	samčí
a	a	k8xC	a
samičí	samičí	k2eAgFnSc2d1	samičí
gamety	gameta	k1gFnSc2	gameta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
porodem	porod	k1gInSc7	porod
<g/>
,	,	kIx,	,
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
,	,	kIx,	,
potratem	potrat	k1gInSc7	potrat
nebo	nebo	k8xC	nebo
interrupcí	interrupce	k1gFnSc7	interrupce
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
těhotenství	těhotenství	k1gNnSc2	těhotenství
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
i	i	k9	i
na	na	k7c4	na
některé	některý	k3yIgMnPc4	některý
další	další	k2eAgMnPc4d1	další
savce	savec	k1gMnPc4	savec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
primáty	primát	k1gInPc4	primát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
se	se	k3xPyFc4	se
ale	ale	k9	ale
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
březost	březost	k1gFnSc4	březost
<g/>
.	.	kIx.	.
</s>
<s>
Těhotenství	těhotenství	k1gNnSc1	těhotenství
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
40	[number]	k4	40
týdnů	týden	k1gInPc2	týden
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
poslední	poslední	k2eAgFnSc2d1	poslední
menstruace	menstruace	k1gFnSc2	menstruace
do	do	k7c2	do
porodu	porod	k1gInSc2	porod
(	(	kIx(	(
<g/>
38	[number]	k4	38
týdnů	týden	k1gInPc2	týden
od	od	k7c2	od
oplodnění	oplodnění	k1gNnSc2	oplodnění
v	v	k7c6	v
případě	případ	k1gInSc6	případ
28	[number]	k4	28
<g/>
denního	denní	k2eAgInSc2d1	denní
menstruačního	menstruační	k2eAgInSc2d1	menstruační
cyklu	cyklus	k1gInSc2	cyklus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
trimestrů	trimestr	k1gInPc2	trimestr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
trimestru	trimestr	k1gInSc6	trimestr
hrozí	hrozit	k5eAaImIp3nS	hrozit
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
riziko	riziko	k1gNnSc4	riziko
spontánního	spontánní	k2eAgInSc2d1	spontánní
či	či	k8xC	či
zamlklého	zamlklý	k2eAgInSc2d1	zamlklý
potratu	potrat	k1gInSc2	potrat
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Potrat	potrat	k1gInSc1	potrat
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
výsledkem	výsledek	k1gInSc7	výsledek
defektů	defekt	k1gInPc2	defekt
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poškození	poškození	k1gNnSc1	poškození
způsobeného	způsobený	k2eAgNnSc2d1	způsobené
po	po	k7c6	po
početí	početí	k1gNnSc6	početí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
oplodnění	oplodnění	k1gNnSc1	oplodnění
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
krok	krok	k1gInSc1	krok
těhotenství	těhotenství	k1gNnSc2	těhotenství
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
sexuálním	sexuální	k2eAgInSc7d1	sexuální
stykem	styk	k1gInSc7	styk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
mužské	mužský	k2eAgFnPc1d1	mužská
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
spermie	spermie	k1gFnSc1	spermie
<g/>
)	)	kIx)	)
vpraveny	vpraven	k2eAgInPc1d1	vpraven
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Mužské	mužský	k2eAgNnSc1d1	mužské
sperma	sperma	k1gNnSc1	sperma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
pohlavní	pohlavní	k2eAgFnPc4d1	pohlavní
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
cukry	cukr	k1gInPc1	cukr
<g/>
,	,	kIx,	,
proteiny	protein	k1gInPc1	protein
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
látky	látka	k1gFnPc1	látka
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
životaschopnosti	životaschopnost	k1gFnSc2	životaschopnost
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
spermie	spermie	k1gFnPc1	spermie
přežijí	přežít	k5eAaPmIp3nP	přežít
v	v	k7c6	v
ženském	ženský	k2eAgNnSc6d1	ženské
těle	tělo	k1gNnSc6	tělo
obvykle	obvykle	k6eAd1	obvykle
asi	asi	k9	asi
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Spermie	spermie	k1gFnPc1	spermie
mají	mít	k5eAaImIp3nP	mít
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
bičík	bičík	k1gInSc4	bičík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgFnPc1d1	jediná
lidské	lidský	k2eAgFnPc1d1	lidská
buňky	buňka	k1gFnPc1	buňka
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
vlastností	vlastnost	k1gFnSc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
haploidy	haploid	k1gInPc4	haploid
(	(	kIx(	(
<g/>
buňky	buňka	k1gFnPc4	buňka
s	s	k7c7	s
jednou	jednou	k6eAd1	jednou
sadou	sada	k1gFnSc7	sada
chromozómů	chromozóm	k1gInPc2	chromozóm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
meiózou	meióza	k1gFnSc7	meióza
ze	z	k7c2	z
zárodečných	zárodečný	k2eAgFnPc2d1	zárodečná
buněk	buňka	k1gFnPc2	buňka
ve	v	k7c6	v
varlatech	varle	k1gNnPc6	varle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ejakulaci	ejakulace	k1gFnSc6	ejakulace
(	(	kIx(	(
<g/>
výronu	výron	k1gInSc2	výron
semene	semeno	k1gNnSc2	semeno
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
100	[number]	k4	100
až	až	k9	až
300	[number]	k4	300
miliónů	milión	k4xCgInPc2	milión
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
(	(	kIx(	(
<g/>
oocyty	oocyt	k1gInPc1	oocyt
II	II	kA	II
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
haploidní	haploidní	k2eAgFnPc1d1	haploidní
ženské	ženská	k1gFnPc1	ženská
reprodukční	reprodukční	k2eAgFnSc2d1	reprodukční
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
sloučit	sloučit	k5eAaPmF	sloučit
se	se	k3xPyFc4	se
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
spermií	spermie	k1gFnSc7	spermie
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
vytvoření	vytvoření	k1gNnSc1	vytvoření
oplodněné	oplodněný	k2eAgFnSc2d1	oplodněná
zygoty	zygota	k1gFnSc2	zygota
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgInP	produkovat
meiózou	meióza	k1gFnSc7	meióza
ve	v	k7c6	v
vaječnících	vaječník	k1gInPc6	vaječník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
aktivovány	aktivovat	k5eAaBmNgInP	aktivovat
a	a	k8xC	a
uvolněny	uvolnit	k5eAaPmNgInP	uvolnit
hormonálními	hormonální	k2eAgFnPc7d1	hormonální
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
menstruačním	menstruační	k2eAgInSc6d1	menstruační
cyklu	cyklus	k1gInSc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
menstruačního	menstruační	k2eAgInSc2d1	menstruační
cyklu	cyklus	k1gInSc2	cyklus
uvolněno	uvolnit	k5eAaPmNgNnS	uvolnit
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ovulace	ovulace	k1gFnSc2	ovulace
okraj	okraj	k1gInSc1	okraj
(	(	kIx(	(
<g/>
fimbrie	fimbrie	k1gFnSc1	fimbrie
<g/>
)	)	kIx)	)
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vejcovodu	vejcovod	k1gInSc2	vejcovod
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
přes	přes	k7c4	přes
vaječník	vaječník	k1gInSc4	vaječník
na	na	k7c4	na
zachycení	zachycení	k1gNnSc4	zachycení
uvolněného	uvolněný	k2eAgNnSc2d1	uvolněné
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oplodnění	oplodnění	k1gNnSc6	oplodnění
spermie	spermie	k1gFnSc2	spermie
obvykle	obvykle	k6eAd1	obvykle
potkají	potkat	k5eAaPmIp3nP	potkat
vajíčka	vajíčko	k1gNnPc1	vajíčko
ve	v	k7c6	v
vejcovodu	vejcovod	k1gInSc6	vejcovod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
od	od	k7c2	od
spermiových	spermiový	k2eAgFnPc2d1	spermiová
buněk	buňka	k1gFnPc2	buňka
před	před	k7c7	před
dosažením	dosažení	k1gNnSc7	dosažení
vejcovodu	vejcovod	k1gInSc2	vejcovod
plavat	plavat	k5eAaImF	plavat
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
vaginy	vagina	k1gFnSc2	vagina
přes	přes	k7c4	přes
děložní	děložní	k2eAgNnSc4d1	děložní
hrdlo	hrdlo	k1gNnSc4	hrdlo
(	(	kIx(	(
<g/>
cervix	cervix	k1gInSc1	cervix
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
délku	délka	k1gFnSc4	délka
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
spermie	spermie	k1gFnSc2	spermie
značná	značný	k2eAgFnSc1d1	značná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
vejcovodu	vejcovod	k1gInSc2	vejcovod
spermie	spermie	k1gFnSc2	spermie
plavou	plavat	k5eAaImIp3nP	plavat
k	k	k7c3	k
vajíčku	vajíčko	k1gNnSc3	vajíčko
(	(	kIx(	(
<g/>
orientují	orientovat	k5eAaBmIp3nP	orientovat
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
chemotaxe	chemotaxe	k1gFnSc2	chemotaxe
<g/>
)	)	kIx)	)
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
ho	on	k3xPp3gInSc4	on
oplodnit	oplodnit	k5eAaPmF	oplodnit
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
spermiová	spermiový	k2eAgFnSc1d1	spermiová
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
malý	malý	k2eAgInSc4d1	malý
váček	váček	k1gInSc4	váček
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
povrchu	povrch	k1gInSc2	povrch
vajíčka	vajíčko	k1gNnSc2	vajíčko
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tak	tak	k9	tak
proniknutí	proniknutí	k1gNnSc4	proniknutí
spermie	spermie	k1gFnSc2	spermie
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vajíčko	vajíčko	k1gNnSc1	vajíčko
sloučí	sloučit	k5eAaPmIp3nS	sloučit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
spermií	spermie	k1gFnSc7	spermie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabrání	zabránit	k5eAaPmIp3nS	zabránit
proniknutí	proniknutí	k1gNnSc4	proniknutí
dalších	další	k2eAgFnPc2d1	další
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Sloučení	sloučení	k1gNnSc1	sloučení
buněčných	buněčný	k2eAgNnPc2d1	buněčné
jader	jádro	k1gNnPc2	jádro
vajíčka	vajíčko	k1gNnSc2	vajíčko
a	a	k8xC	a
spermie	spermie	k1gFnSc2	spermie
na	na	k7c6	na
formování	formování	k1gNnSc6	formování
diploidní	diploidní	k2eAgFnSc2d1	diploidní
buňky	buňka	k1gFnSc2	buňka
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
první	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
metody	metoda	k1gFnPc1	metoda
oplodnění	oplodnění	k1gNnSc2	oplodnění
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
označované	označovaný	k2eAgFnPc1d1	označovaná
pojmem	pojem	k1gInSc7	pojem
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
neplodnosti	neplodnost	k1gFnSc6	neplodnost
nebo	nebo	k8xC	nebo
k	k	k7c3	k
oplodnění	oplodnění	k1gNnSc3	oplodnění
žen	žena	k1gFnPc2	žena
bez	bez	k7c2	bez
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
faktory	faktor	k1gInPc1	faktor
jako	jako	k8xC	jako
polycystická	polycystický	k2eAgNnPc1d1	polycystické
ovaria	ovarium	k1gNnPc1	ovarium
<g/>
,	,	kIx,	,
obezita	obezita	k1gFnSc1	obezita
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
nízká	nízký	k2eAgFnSc1d1	nízká
hmotnost	hmotnost	k1gFnSc1	hmotnost
ženy	žena	k1gFnSc2	žena
či	či	k8xC	či
obézní	obézní	k2eAgMnSc1d1	obézní
muž	muž	k1gMnSc1	muž
snižují	snižovat	k5eAaImIp3nP	snižovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
oplodnění	oplodnění	k1gNnSc4	oplodnění
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc4	příznak
těhotenství	těhotenství	k1gNnSc2	těhotenství
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
podle	podle	k7c2	podle
stádia	stádium	k1gNnSc2	stádium
na	na	k7c6	na
rané	raný	k2eAgFnSc6d1	raná
a	a	k8xC	a
pozdní	pozdní	k2eAgFnSc6d1	pozdní
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
příznakem	příznak	k1gInSc7	příznak
raného	raný	k2eAgNnSc2d1	rané
těhotenství	těhotenství	k1gNnSc2	těhotenství
je	být	k5eAaImIp3nS	být
vynechání	vynechání	k1gNnSc4	vynechání
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
menstruace	menstruace	k1gFnSc2	menstruace
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
časté	častý	k2eAgNnSc1d1	časté
nutkání	nutkání	k1gNnSc1	nutkání
k	k	k7c3	k
močení	močení	k1gNnSc3	močení
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
přecitlivělost	přecitlivělost	k1gFnSc1	přecitlivělost
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
nejčastější	častý	k2eAgInPc1d3	nejčastější
příznaky	příznak	k1gInPc1	příznak
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
:	:	kIx,	:
Citlivé	citlivý	k2eAgInPc4d1	citlivý
<g/>
,	,	kIx,	,
nateklé	nateklý	k2eAgInPc4d1	nateklý
prsy	prs	k1gInPc4	prs
Únava	únava	k1gFnSc1	únava
Implantační	implantační	k2eAgNnSc4d1	implantační
krvácení	krvácení	k1gNnSc4	krvácení
Nevolnost	nevolnost	k1gFnSc4	nevolnost
anebo	anebo	k8xC	anebo
zvracení	zvracení	k1gNnSc4	zvracení
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
vnímavost	vnímavost	k1gFnSc1	vnímavost
pachů	pach	k1gInPc2	pach
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
chutí	chuť	k1gFnSc7	chuť
Nafouklé	nafouklý	k2eAgNnSc4d1	nafouklé
břicho	břicho	k1gNnSc4	břicho
Časté	častý	k2eAgNnSc1d1	časté
močení	močení	k1gNnSc1	močení
Vynechání	vynechání	k1gNnSc2	vynechání
menstruace	menstruace	k1gFnSc2	menstruace
Bazální	bazální	k2eAgFnSc1d1	bazální
teplota	teplota	k1gFnSc1	teplota
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
těhotenský	těhotenský	k2eAgInSc1d1	těhotenský
test	test	k1gInSc1	test
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
čase	čas	k1gInSc6	čas
existuje	existovat	k5eAaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
totipotentní	totipotentní	k2eAgFnSc1d1	totipotentní
(	(	kIx(	(
<g/>
schopna	schopen	k2eAgFnSc1d1	schopna
dělením	dělení	k1gNnSc7	dělení
produkovat	produkovat	k5eAaImF	produkovat
každý	každý	k3xTgInSc4	každý
buněčný	buněčný	k2eAgInSc4d1	buněčný
typ	typ	k1gInSc4	typ
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
)	)	kIx)	)
buňka	buňka	k1gFnSc1	buňka
–	–	k?	–
zygota	zygota	k1gFnSc1	zygota
–	–	k?	–
geneticky	geneticky	k6eAd1	geneticky
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
nový	nový	k2eAgInSc1d1	nový
organismus	organismus	k1gInSc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mitotické	mitotický	k2eAgNnSc1d1	mitotické
buněčné	buněčný	k2eAgNnSc1d1	buněčné
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc4d1	následující
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
buňka	buňka	k1gFnSc1	buňka
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
diploidní	diploidní	k2eAgFnPc4d1	diploidní
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Zygota	zygota	k1gFnSc1	zygota
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
<g/>
,	,	kIx,	,
produkujíc	produkovat	k5eAaImSgFnS	produkovat
2	[number]	k4	2
menší	malý	k2eAgFnPc1d2	menší
buňky	buňka	k1gFnPc1	buňka
zvané	zvaný	k2eAgFnPc4d1	zvaná
blastomery	blastomera	k1gFnPc4	blastomera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
taktéž	taktéž	k?	taktéž
dělí	dělit	k5eAaImIp3nS	dělit
každých	každý	k3xTgNnPc2	každý
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
asi	asi	k9	asi
4	[number]	k4	4
děleních	dělení	k1gNnPc6	dělení
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
16	[number]	k4	16
individuálních	individuální	k2eAgFnPc2d1	individuální
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
16	[number]	k4	16
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
morula	morula	k1gFnSc1	morula
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
opouští	opouštět	k5eAaImIp3nS	opouštět
vejcovod	vejcovod	k1gInSc4	vejcovod
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
ektopické	ektopický	k2eAgNnSc1d1	ektopické
těhotenství	těhotenství	k1gNnSc1	těhotenství
(	(	kIx(	(
<g/>
mimoděložní	mimoděložní	k2eAgFnSc1d1	mimoděložní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blastocel	Blastocet	k5eAaPmAgMnS	Blastocet
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
dutina	dutina	k1gFnSc1	dutina
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
vyvíjející	vyvíjející	k2eAgFnPc1d1	vyvíjející
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
rostou	růst	k5eAaImIp3nP	růst
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
této	tento	k3xDgFnSc2	tento
dutiny	dutina	k1gFnSc2	dutina
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
zona	zon	k2eAgFnSc1d1	zona
pellucida	pellucida	k1gFnSc1	pellucida
(	(	kIx(	(
<g/>
blanka	blanka	k1gFnSc1	blanka
kryjící	kryjící	k2eAgFnSc1d1	kryjící
zralé	zralý	k2eAgNnSc4d1	zralé
vajíčko	vajíčko	k1gNnSc4	vajíčko
<g/>
)	)	kIx)	)
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
velikosti	velikost	k1gFnSc6	velikost
jako	jako	k9	jako
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
rostou	růst	k5eAaImIp3nP	růst
stále	stále	k6eAd1	stále
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vešly	vejít	k5eAaPmAgFnP	vejít
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
struktura	struktura	k1gFnSc1	struktura
s	s	k7c7	s
dutinou	dutina	k1gFnSc7	dutina
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
a	a	k8xC	a
vyvíjejícími	vyvíjející	k2eAgFnPc7d1	vyvíjející
se	se	k3xPyFc4	se
buňkami	buňka	k1gFnPc7	buňka
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
blastocysta	blastocysta	k1gFnSc1	blastocysta
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
blastocysty	blastocysta	k1gFnSc2	blastocysta
značí	značit	k5eAaImIp3nS	značit
formování	formování	k1gNnSc4	formování
2	[number]	k4	2
typů	typ	k1gInPc2	typ
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
buňky	buňka	k1gFnPc1	buňka
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
ve	v	k7c6	v
vnitru	vnitro	k1gNnSc6	vnitro
blastocelu	blastocel	k1gInSc2	blastocel
a	a	k8xC	a
buňky	buňka	k1gFnSc2	buňka
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vnějšku	vnějšek	k1gInSc6	vnějšek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
24	[number]	k4	24
-	-	kIx~	-
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
blanka	blanka	k1gFnSc1	blanka
blastocysty	blastocysta	k1gFnSc2	blastocysta
(	(	kIx(	(
<g/>
zona	zon	k2eAgFnSc1d1	zona
pellucida	pellucida	k1gFnSc1	pellucida
<g/>
)	)	kIx)	)
přetrhne	přetrhnout	k5eAaPmIp3nS	přetrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
blastocysty	blastocysta	k1gFnSc2	blastocysta
začnou	začít	k5eAaPmIp3nP	začít
vylučovat	vylučovat	k5eAaImF	vylučovat
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
naruší	narušit	k5eAaPmIp3nS	narušit
výstelku	výstelka	k1gFnSc4	výstelka
(	(	kIx(	(
<g/>
epitel	epitel	k1gInSc4	epitel
<g/>
)	)	kIx)	)
dělohy	děloha	k1gFnSc2	děloha
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
uhnízdění	uhnízdění	k1gNnSc4	uhnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
obklopující	obklopující	k2eAgFnSc4d1	obklopující
blastocystu	blastocysta	k1gFnSc4	blastocysta
teď	teď	k6eAd1	teď
naruší	narušit	k5eAaPmIp3nP	narušit
buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
formujíce	formovat	k5eAaImSgMnP	formovat
malé	malý	k2eAgNnSc4d1	malé
nádržky	nádržka	k1gFnPc4	nádržka
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stimulují	stimulovat	k5eAaImIp3nP	stimulovat
vznik	vznik	k1gInSc4	vznik
kapilár	kapilára	k1gFnPc2	kapilára
(	(	kIx(	(
<g/>
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
růstu	růst	k1gInSc2	růst
placenty	placenta	k1gFnSc2	placenta
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
buňky	buňka	k1gFnPc1	buňka
blastocysty	blastocysta	k1gFnSc2	blastocysta
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
dělí	dělit	k5eAaImIp3nS	dělit
<g/>
,	,	kIx,	,
formujíce	formovat	k5eAaImSgFnP	formovat
2	[number]	k4	2
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
vrstva	vrstva	k1gFnSc1	vrstva
bude	být	k5eAaImBp3nS	být
embryo	embryo	k1gNnSc4	embryo
(	(	kIx(	(
<g/>
zárodek	zárodek	k1gInSc1	zárodek
<g/>
)	)	kIx)	)
a	a	k8xC	a
buňky	buňka	k1gFnPc4	buňka
odtud	odtud	k6eAd1	odtud
budou	být	k5eAaImBp3nP	být
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
amniotické	amniotický	k2eAgFnSc6d1	amniotický
(	(	kIx(	(
<g/>
plodové	plodový	k2eAgFnSc6d1	plodová
<g/>
)	)	kIx)	)
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
spodní	spodní	k2eAgFnSc1d1	spodní
vrstva	vrstva	k1gFnSc1	vrstva
formuje	formovat	k5eAaImIp3nS	formovat
malý	malý	k2eAgInSc4d1	malý
vak	vak	k1gInSc4	vak
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Začnou	začít	k5eAaPmIp3nP	začít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc4	buňka
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
v	v	k7c6	v
abnormální	abnormální	k2eAgFnSc6d1	abnormální
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
teď	teď	k6eAd1	teď
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
ektopické	ektopický	k2eAgNnSc4d1	ektopické
(	(	kIx(	(
<g/>
mimoděložní	mimoděložní	k2eAgNnSc4d1	mimoděložní
<g/>
)	)	kIx)	)
těhotenství	těhotenství	k1gNnSc4	těhotenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
tkáně	tkáň	k1gFnSc2	tkáň
chorionu	chorion	k1gInSc2	chorion
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgInSc1d1	vnější
zárodečný	zárodečný	k2eAgInSc1d1	zárodečný
obal	obal	k1gInSc1	obal
plodu	plod	k1gInSc2	plod
<g/>
)	)	kIx)	)
ve	v	k7c6	v
formující	formující	k2eAgFnSc6d1	formující
se	se	k3xPyFc4	se
placentě	placenta	k1gFnSc6	placenta
ukotví	ukotvit	k5eAaPmIp3nS	ukotvit
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
část	část	k1gFnSc1	část
na	na	k7c4	na
dělohu	děloha	k1gFnSc4	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
v	v	k7c6	v
bodu	bod	k1gInSc6	bod
formující	formující	k2eAgFnSc2d1	formující
se	se	k3xPyFc4	se
placenty	placenta	k1gFnSc2	placenta
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
při	při	k7c6	při
místě	místo	k1gNnSc6	místo
uhnízdění	uhnízdění	k1gNnSc2	uhnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
vak	vak	k1gInSc1	vak
uvnitř	uvnitř	k7c2	uvnitř
blastocysty	blastocysta	k1gFnSc2	blastocysta
začne	začít	k5eAaPmIp3nS	začít
produkovat	produkovat	k5eAaImF	produkovat
červené	červený	k2eAgFnSc2d1	červená
krevní	krevní	k2eAgFnSc2d1	krevní
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgFnPc2d1	další
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
pojivá	pojivý	k2eAgFnSc1d1	pojivá
tkáň	tkáň	k1gFnSc1	tkáň
mezi	mezi	k7c7	mezi
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
placentou	placenta	k1gFnSc7	placenta
a	a	k8xC	a
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
plodem	plod	k1gInSc7	plod
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
do	do	k7c2	do
pupeční	pupeční	k2eAgFnSc2d1	pupeční
šňůry	šňůra	k1gFnSc2	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
úzká	úzký	k2eAgFnSc1d1	úzká
linie	linie	k1gFnSc1	linie
buněk	buňka	k1gFnPc2	buňka
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
růst	růst	k1gInSc1	růst
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
plod	plod	k1gInSc4	plod
čeká	čekat	k5eAaImIp3nS	čekat
gastrulace	gastrulace	k1gFnSc1	gastrulace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
3	[number]	k4	3
vrstvy	vrstva	k1gFnPc4	vrstva
plodu	plod	k1gInSc2	plod
-	-	kIx~	-
ektoderm	ektoderm	k1gInSc1	ektoderm
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgMnSc1d1	vnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezoderm	mezoderm	k1gInSc1	mezoderm
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc6d1	střední
<g/>
)	)	kIx)	)
a	a	k8xC	a
endoderm	endoderm	k1gInSc1	endoderm
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úzké	úzký	k2eAgFnPc1d1	úzká
linie	linie	k1gFnPc1	linie
buněk	buňka	k1gFnPc2	buňka
začnou	začít	k5eAaPmIp3nP	začít
formovat	formovat	k5eAaImF	formovat
endoderm	endoderm	k1gInSc4	endoderm
a	a	k8xC	a
mezoderm	mezoderm	k1gInSc4	mezoderm
<g/>
.	.	kIx.	.
</s>
<s>
Ektoderm	ektoderm	k1gInSc1	ektoderm
začne	začít	k5eAaPmIp3nS	začít
rychle	rychle	k6eAd1	rychle
růst	růst	k1gInSc4	růst
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc4	výsledek
chemických	chemický	k2eAgFnPc2d1	chemická
složek	složka	k1gFnPc2	složka
produkovaných	produkovaný	k2eAgMnPc2d1	produkovaný
mezodermem	mezoderm	k1gInSc7	mezoderm
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
3	[number]	k4	3
vrstvy	vrstva	k1gFnPc1	vrstva
dají	dát	k5eAaPmIp3nP	dát
základ	základ	k1gInSc4	základ
všem	všecek	k3xTgInPc3	všecek
různým	různý	k2eAgInPc3d1	různý
typům	typ	k1gInPc3	typ
tkání	tkáň	k1gFnPc2	tkáň
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Endoderm	Endoderm	k1gInSc1	Endoderm
později	pozdě	k6eAd2	pozdě
vyformuje	vyformovat	k5eAaPmIp3nS	vyformovat
linii	linie	k1gFnSc4	linie
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
několika	několik	k4yIc2	několik
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Mezoderm	mezoderm	k1gInSc1	mezoderm
vyformuje	vyformovat	k5eAaPmIp3nS	vyformovat
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
lymfatickou	lymfatický	k2eAgFnSc4d1	lymfatická
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
,	,	kIx,	,
vnitřek	vnitřek	k1gInSc4	vnitřek
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
reprodukční	reprodukční	k2eAgInPc4d1	reprodukční
a	a	k8xC	a
vylučovací	vylučovací	k2eAgInPc4d1	vylučovací
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
dá	dát	k5eAaPmIp3nS	dát
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
slezinu	slezina	k1gFnSc4	slezina
a	a	k8xC	a
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
krevních	krevní	k2eAgFnPc2d1	krevní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Ektoderm	ektoderm	k1gInSc1	ektoderm
vyformuje	vyformovat	k5eAaPmIp3nS	vyformovat
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
rohovku	rohovka	k1gFnSc4	rohovka
<g/>
,	,	kIx,	,
linii	linie	k1gFnSc4	linie
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
a	a	k8xC	a
vnějšího	vnější	k2eAgNnSc2d1	vnější
ucha	ucho	k1gNnSc2	ucho
<g/>
,	,	kIx,	,
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
dutinu	dutina	k1gFnSc4	dutina
<g/>
,	,	kIx,	,
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
konečník	konečník	k1gInSc4	konečník
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
hypofýzu	hypofýza	k1gFnSc4	hypofýza
<g/>
,	,	kIx,	,
prsní	prsní	k2eAgFnPc4d1	prsní
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
části	část	k1gFnPc4	část
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
18	[number]	k4	18
dní	den	k1gInPc2	den
po	po	k7c6	po
oplodnění	oplodnění	k1gNnSc6	oplodnění
je	být	k5eAaImIp3nS	být
embryo	embryo	k1gNnSc1	embryo
poděleno	podělit	k5eAaPmNgNnS	podělit
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
množství	množství	k1gNnSc2	množství
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bude	být	k5eAaImBp3nS	být
potřebovat	potřebovat	k5eAaImF	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
jako	jako	k8xC	jako
hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oblast	oblast	k1gFnSc1	oblast
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Nervový	nervový	k2eAgInSc1d1	nervový
systém	systém	k1gInSc1	systém
embrya	embryo	k1gNnSc2	embryo
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
růst	růst	k1gInSc4	růst
v	v	k7c6	v
duté	dutý	k2eAgFnSc6d1	dutá
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgFnSc6d1	zvaná
nervová	nervový	k2eAgFnSc1d1	nervová
brázda	brázda	k1gFnSc1	brázda
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgInSc1d1	krevní
systém	systém	k1gInSc1	systém
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
oběh	oběh	k1gInSc4	oběh
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
embryu	embryo	k1gNnSc6	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
a	a	k8xC	a
proudí	proudit	k5eAaImIp3nP	proudit
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
tvořících	tvořící	k2eAgFnPc6d1	tvořící
se	se	k3xPyFc4	se
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
placenty	placenta	k1gFnSc2	placenta
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
sekundární	sekundární	k2eAgFnPc1d1	sekundární
krevní	krevní	k2eAgFnPc1d1	krevní
cévy	céva	k1gFnPc1	céva
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
zásobování	zásobování	k1gNnSc2	zásobování
živinami	živina	k1gFnPc7	živina
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
formovat	formovat	k5eAaImF	formovat
na	na	k7c6	na
vaku	vak	k1gInSc6	vak
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
embrya	embryo	k1gNnSc2	embryo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
diferencovat	diferencovat	k5eAaImF	diferencovat
do	do	k7c2	do
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Endokardiální	Endokardiální	k2eAgInSc1d1	Endokardiální
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
srdeční	srdeční	k2eAgInSc1d1	srdeční
<g/>
)	)	kIx)	)
buňky	buňka	k1gFnPc1	buňka
začnou	začít	k5eAaPmIp3nP	začít
formovat	formovat	k5eAaImF	formovat
svalstvo	svalstvo	k1gNnSc4	svalstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
asi	asi	k9	asi
24	[number]	k4	24
dní	den	k1gInPc2	den
po	po	k7c6	po
oplodnění	oplodnění	k1gNnSc6	oplodnění
existuje	existovat	k5eAaImIp3nS	existovat
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
rourkové	rourkový	k2eAgNnSc1d1	rourkový
srdce	srdce	k1gNnSc1	srdce
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
S	s	k7c7	s
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
začne	začít	k5eAaPmIp3nS	začít
pulsovat	pulsovat	k5eAaImF	pulsovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
začne	začít	k5eAaPmIp3nS	začít
proudit	proudit	k5eAaImF	proudit
krev	krev	k1gFnSc1	krev
embryem	embryo	k1gNnSc7	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
embrya	embryo	k1gNnSc2	embryo
rostou	růst	k5eAaImIp3nP	růst
<g/>
,	,	kIx,	,
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
se	se	k3xPyFc4	se
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
uši	ucho	k1gNnPc1	ucho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucím	budoucí	k2eAgInSc6d1	budoucí
hrudníku	hrudník	k1gInSc6	hrudník
bije	bít	k5eAaImIp3nS	bít
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
plíce	plíce	k1gFnPc1	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Nenarozené	narozený	k2eNgNnSc1d1	nenarozené
dítě	dítě	k1gNnSc1	dítě
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
embryem	embryo	k1gNnSc7	embryo
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
plodem	plod	k1gInSc7	plod
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
už	už	k6eAd1	už
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgInPc4	všechen
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
čelist	čelist	k1gFnSc1	čelist
včetně	včetně	k7c2	včetně
zárodků	zárodek	k1gInPc2	zárodek
zubů	zub	k1gInPc2	zub
v	v	k7c6	v
dásních	dáseň	k1gFnPc6	dáseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
probíhá	probíhat	k5eAaImIp3nS	probíhat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
2	[number]	k4	2
cm	cm	kA	cm
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
srdce	srdce	k1gNnSc1	srdce
tepe	tepat	k5eAaImIp3nS	tepat
asi	asi	k9	asi
140	[number]	k4	140
až	až	k9	až
150	[number]	k4	150
krát	krát	k6eAd1	krát
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tlukot	tlukot	k1gInSc1	tlukot
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
stetoskopem	stetoskop	k1gInSc7	stetoskop
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
se	s	k7c7	s
prsty	prst	k1gInPc7	prst
s	s	k7c7	s
měkkými	měkký	k2eAgInPc7d1	měkký
nehty	nehet	k1gInPc7	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prstech	prst	k1gInPc6	prst
jsou	být	k5eAaImIp3nP	být
zřejmé	zřejmý	k2eAgInPc1d1	zřejmý
otisky	otisk	k1gInPc1	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
dotyk	dotyk	k1gInSc4	dotyk
<g/>
.	.	kIx.	.
</s>
<s>
Vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
se	se	k3xPyFc4	se
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určení	určení	k1gNnSc1	určení
pohlaví	pohlaví	k1gNnSc2	pohlaví
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
svírá	svírat	k5eAaImIp3nS	svírat
pěst	pěst	k1gFnSc4	pěst
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
třetího	třetí	k4xOgInSc2	třetí
měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
prvního	první	k4xOgMnSc2	první
trimestru	trimestr	k1gInSc2	trimestr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
asi	asi	k9	asi
10	[number]	k4	10
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
150	[number]	k4	150
g.	g.	k?	g.
Plod	plod	k1gInSc1	plod
slyší	slyšet	k5eAaImIp3nS	slyšet
matčin	matčin	k2eAgInSc4d1	matčin
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
tlukot	tlukot	k1gInSc4	tlukot
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
okolní	okolní	k2eAgInPc4d1	okolní
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Plodová	plodový	k2eAgFnSc1d1	plodová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
obměněna	obměněn	k2eAgFnSc1d1	obměněna
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
přibývá	přibývat	k5eAaImIp3nS	přibývat
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
asi	asi	k9	asi
čtvrt	čtvrt	k1gFnSc4	čtvrt
kg	kg	kA	kg
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
16	[number]	k4	16
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
250	[number]	k4	250
g.	g.	k?	g.
Matce	matka	k1gFnSc6	matka
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
prsa	prsa	k1gNnPc1	prsa
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
první	první	k4xOgInPc4	první
pohyby	pohyb	k1gInPc4	pohyb
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
19	[number]	k4	19
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
350	[number]	k4	350
g.	g.	k?	g.
Začínají	začínat	k5eAaImIp3nP	začínat
mu	on	k3xPp3gInSc3	on
růst	růst	k5eAaImF	růst
vlasy	vlas	k1gInPc1	vlas
a	a	k8xC	a
oční	oční	k2eAgFnPc1d1	oční
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ultrazvukovém	ultrazvukový	k2eAgNnSc6d1	ultrazvukové
vyšetření	vyšetření	k1gNnSc6	vyšetření
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
pohyby	pohyb	k1gInPc4	pohyb
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
možno	možno	k6eAd1	možno
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
otáčí	otáčet	k5eAaImIp3nS	otáčet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Saje	sát	k5eAaImIp3nS	sát
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
palec	palec	k1gInSc4	palec
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nemá	mít	k5eNaImIp3nS	mít
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
sací	sací	k2eAgInSc1d1	sací
reflex	reflex	k1gInSc1	reflex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
reálnou	reálný	k2eAgFnSc4d1	reálná
šanci	šance	k1gFnSc4	šance
(	(	kIx(	(
<g/>
asi	asi	k9	asi
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
v	v	k7c6	v
případě	případ	k1gInSc6	případ
předčasného	předčasný	k2eAgInSc2d1	předčasný
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
přes	přes	k7c4	přes
30	[number]	k4	30
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
900	[number]	k4	900
g.	g.	k?	g.
Plod	plod	k1gInSc1	plod
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
přibývá	přibývat	k5eAaImIp3nS	přibývat
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
Kope	kopat	k5eAaImIp3nS	kopat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
škytat	škytat	k5eAaImF	škytat
a	a	k8xC	a
plakat	plakat	k5eAaImF	plakat
<g/>
.	.	kIx.	.
</s>
<s>
Chutí	chuť	k1gFnSc7	chuť
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
sladkým	sladký	k2eAgInSc7d1	sladký
a	a	k8xC	a
kyselým	kyselý	k2eAgInSc7d1	kyselý
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
všech	všecek	k3xTgInPc2	všecek
pět	pět	k4xCc4	pět
smyslů	smysl	k1gInPc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
1,8	[number]	k4	1,8
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Plodu	plod	k1gInSc3	plod
rychle	rychle	k6eAd1	rychle
narostl	narůst	k5eAaPmAgInS	narůst
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
předčasného	předčasný	k2eAgInSc2d1	předčasný
porodu	porod	k1gInSc2	porod
má	mít	k5eAaImIp3nS	mít
dítě	dítě	k1gNnSc4	dítě
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
42	[number]	k4	42
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
2,3	[number]	k4	2,3
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
obvykle	obvykle	k6eAd1	obvykle
měří	měřit	k5eAaImIp3nS	měřit
od	od	k7c2	od
48	[number]	k4	48
do	do	k7c2	do
54	[number]	k4	54
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
od	od	k7c2	od
2,8	[number]	k4	2,8
do	do	k7c2	do
4	[number]	k4	4
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
jeho	jeho	k3xOp3gFnSc2	jeho
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
9,5	[number]	k4	9,5
až	až	k9	až
10,5	[number]	k4	10,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Požívání	požívání	k1gNnSc1	požívání
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
fetální	fetální	k2eAgInSc1d1	fetální
alkoholový	alkoholový	k2eAgInSc1d1	alkoholový
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Kouření	kouření	k1gNnSc1	kouření
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
také	také	k9	také
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
obezita	obezita	k1gFnSc1	obezita
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
matky	matka	k1gFnSc2	matka
pak	pak	k6eAd1	pak
významně	významně	k6eAd1	významně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
autismu	autismus	k1gInSc2	autismus
<g/>
.	.	kIx.	.
</s>
