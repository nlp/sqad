<p>
<s>
Drozd	Drozd	k1gMnSc1	Drozd
cvrčala	cvrčala	k1gFnSc1	cvrčala
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
iliacus	iliacus	k1gMnSc1	iliacus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
drozdovití	drozdovitý	k2eAgMnPc1d1	drozdovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
biotop	biotop	k1gInSc1	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
lesích	les	k1gInPc6	les
a	a	k8xC	a
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c4	po
Kamčatku	Kamčatka	k1gFnSc4	Kamčatka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
poddruzích	poddruh	k1gInPc6	poddruh
<g/>
:	:	kIx,	:
T.	T.	kA	T.
i.	i.	k?	i.
iliacus	iliacus	k1gInSc1	iliacus
a	a	k8xC	a
T.	T.	kA	T.
i.	i.	k?	i.
coburni	coburnit	k5eAaPmRp2nS	coburnit
<g/>
.	.	kIx.	.
</s>
<s>
Severoevropské	severoevropský	k2eAgFnPc1d1	severoevropská
populace	populace	k1gFnPc1	populace
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
početně	početně	k6eAd1	početně
migrují	migrovat	k5eAaImIp3nP	migrovat
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nepravidelně	pravidelně	k6eNd1	pravidelně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
maximálně	maximálně	k6eAd1	maximálně
několika	několik	k4yIc2	několik
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
31	[number]	k4	31
–	–	k?	–
42	[number]	k4	42
000	[number]	k4	000
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Drozd	Drozd	k1gMnSc1	Drozd
cvrčala	cvrčala	k1gFnSc1	cvrčala
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
21	[number]	k4	21
–	–	k?	–
23	[number]	k4	23
cm	cm	kA	cm
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
kolem	kolem	k7c2	kolem
50	[number]	k4	50
–	–	k?	–
80	[number]	k4	80
g	g	kA	g
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
hojnější	hojný	k2eAgMnSc1d2	hojnější
příbuzný	příbuzný	k1gMnSc1	příbuzný
drozd	drozd	k1gMnSc1	drozd
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
středoevropských	středoevropský	k2eAgInPc2d1	středoevropský
druhů	druh	k1gInPc2	druh
drozdů	drozd	k1gMnPc2	drozd
jej	on	k3xPp3gMnSc4	on
rozpoznáme	rozpoznat	k5eAaPmIp1nP	rozpoznat
díky	díky	k7c3	díky
výraznému	výrazný	k2eAgInSc3d1	výrazný
bílému	bílý	k2eAgInSc3d1	bílý
pruhu	pruh	k1gInSc3	pruh
nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
načervenalému	načervenalý	k2eAgNnSc3d1	načervenalé
opeření	opeření	k1gNnSc3	opeření
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
krátký	krátký	k2eAgInSc4d1	krátký
ocas	ocas	k1gInSc4	ocas
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
bílé	bílý	k2eAgNnSc1d1	bílé
s	s	k7c7	s
hnědým	hnědý	k2eAgNnSc7d1	hnědé
čárkováním	čárkování	k1gNnSc7	čárkování
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnSc2	končetina
a	a	k8xC	a
štíhlý	štíhlý	k2eAgInSc1d1	štíhlý
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
zobák	zobák	k1gInSc1	zobák
žlutohnědý	žlutohnědý	k2eAgInSc1d1	žlutohnědý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
okrově	okrově	k6eAd1	okrově
hnědí	hnědit	k5eAaImIp3nS	hnědit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Drozd	Drozd	k1gMnSc1	Drozd
cvrčala	cvrčala	k1gFnSc1	cvrčala
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
při	při	k7c6	při
tazích	tag	k1gInPc6	tag
se	se	k3xPyFc4	se
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
do	do	k7c2	do
větších	veliký	k2eAgNnPc2d2	veliký
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
200	[number]	k4	200
členných	členný	k2eAgNnPc2d1	členné
hejn	hejno	k1gNnPc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
také	také	k9	také
ve	v	k7c4	v
společnosti	společnost	k1gFnPc4	společnost
drozdů	drozd	k1gMnPc2	drozd
zpěvných	zpěvný	k2eAgMnPc2d1	zpěvný
<g/>
,	,	kIx,	,
kvíčal	kvíčala	k1gFnPc2	kvíčala
a	a	k8xC	a
brávníků	brávník	k1gMnPc2	brávník
<g/>
,	,	kIx,	,
kosů	kos	k1gMnPc2	kos
či	či	k8xC	či
špačků	špaček	k1gMnPc2	špaček
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
plachého	plachý	k2eAgMnSc4d1	plachý
ptáka	pták	k1gMnSc4	pták
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
zaslechnout	zaslechnout	k5eAaPmF	zaslechnout
než	než	k8xS	než
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgInSc1d1	složený
se	s	k7c7	s
švitořivých	švitořivý	k2eAgInPc2d1	švitořivý
a	a	k8xC	a
flétnovitých	flétnovitý	k2eAgInPc2d1	flétnovitý
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
drobným	drobný	k2eAgInSc7d1	drobný
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
žížalami	žížala	k1gFnPc7	žížala
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
plody	plod	k1gInPc1	plod
hlohu	hloh	k1gInSc2	hloh
a	a	k8xC	a
jeřábu	jeřáb	k1gInSc2	jeřáb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
z	z	k7c2	z
travin	travina	k1gFnPc2	travina
a	a	k8xC	a
větviček	větvička	k1gFnPc2	větvička
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
asi	asi	k9	asi
1,5	[number]	k4	1,5
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
mívá	mívat	k5eAaImIp3nS	mívat
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
jednu	jeden	k4xCgFnSc4	jeden
snůšku	snůška	k1gFnSc4	snůška
po	po	k7c6	po
5	[number]	k4	5
–	–	k?	–
6	[number]	k4	6
žlutohnědých	žlutohnědý	k2eAgNnPc6d1	žlutohnědé
vejcích	vejce	k1gNnPc6	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
kolem	kolem	k7c2	kolem
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Redwing	Redwing	k1gInSc4	Redwing
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Droździk	Droździk	k1gInSc4	Droździk
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Drozd	Drozd	k1gMnSc1	Drozd
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
</s>
</p>
<p>
<s>
Drozd	Drozd	k1gMnSc1	Drozd
kvíčala	kvíčala	k1gFnSc1	kvíčala
</s>
</p>
<p>
<s>
Drozd	Drozd	k1gMnSc1	Drozd
brávník	brávník	k1gMnSc1	brávník
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
drozd	drozd	k1gMnSc1	drozd
cvrčala	cvrčala	k1gFnSc1	cvrčala
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
drozd	drozd	k1gMnSc1	drozd
cvrčala	cvrčala	k1gFnSc1	cvrčala
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
