<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
část	část	k1gFnSc1	část
historie	historie	k1gFnSc2	historie
automobilů	automobil	k1gInPc2	automobil
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
realizovány	realizován	k2eAgInPc1d1	realizován
první	první	k4xOgInPc1	první
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
vozidly	vozidlo	k1gNnPc7	vozidlo
poháněnými	poháněný	k2eAgNnPc7d1	poháněné
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
