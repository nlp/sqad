<s>
Automobil	automobil	k1gInSc1	automobil
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
auto	auto	k1gNnSc1	auto
,	,	kIx,	,
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
α	α	k?	α
autos	autos	k1gInSc1	autos
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dvoustopé	dvoustopý	k2eAgNnSc4d1	dvoustopé
osobní	osobní	k2eAgNnSc4d1	osobní
nebo	nebo	k8xC	nebo
nákladní	nákladní	k2eAgNnSc4d1	nákladní
silniční	silniční	k2eAgNnSc4d1	silniční
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
této	tento	k3xDgFnSc3	tento
definici	definice	k1gFnSc3	definice
mezi	mezi	k7c4	mezi
automobily	automobil	k1gInPc4	automobil
obvykle	obvykle	k6eAd1	obvykle
neřadíme	řadit	k5eNaImIp1nP	řadit
autobusy	autobus	k1gInPc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
druhu	druh	k1gInSc2	druh
pohonu	pohon	k1gInSc2	pohon
<g/>
,	,	kIx,	,
např.	např.	kA	např.
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
dieselové	dieselový	k2eAgFnPc1d1	dieselová
<g/>
,	,	kIx,	,
zážehové	zážehový	k2eAgFnPc1d1	zážehová
<g/>
,	,	kIx,	,
elektro	elektro	k1gNnSc1	elektro
aj.	aj.	kA	aj.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
590	[number]	k4	590
000	[number]	k4	000
000	[number]	k4	000
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
registrováno	registrován	k2eAgNnSc1d1	registrováno
980	[number]	k4	980
milionů	milion	k4xCgInPc2	milion
aut	auto	k1gNnPc2	auto
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
registrováno	registrovat	k5eAaBmNgNnS	registrovat
1	[number]	k4	1
015	[number]	k4	015
000	[number]	k4	000
000	[number]	k4	000
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
automobil	automobil	k1gInSc1	automobil
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
kolojezd	kolojezd	k6eAd1	kolojezd
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ά	ά	k?	ά
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
áuto	áuto	k6eAd1	áuto
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
a	a	k8xC	a
latinského	latinský	k2eAgNnSc2d1	latinské
mobilis	mobilis	k1gFnSc7	mobilis
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zkrácený	zkrácený	k2eAgInSc4d1	zkrácený
tvar	tvar	k1gInSc4	tvar
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
češtině	čeština	k1gFnSc6	čeština
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
užíván	užíván	k2eAgInSc4d1	užíván
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
slova	slovo	k1gNnSc2	slovo
automobil	automobil	k1gInSc1	automobil
–	–	k?	–
samohyb	samohyb	k1gInSc1	samohyb
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc1	automobil
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
etymologicky	etymologicky	k6eAd1	etymologicky
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgInSc1d1	pohybující
pozemní	pozemní	k2eAgInSc1d1	pozemní
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
nebo	nebo	k8xC	nebo
trolejích	trolej	k1gFnPc6	trolej
a	a	k8xC	a
k	k	k7c3	k
jehož	jehož	k3xOyRp3gInSc3	jehož
pohybu	pohyb	k1gInSc3	pohyb
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
tažných	tažný	k2eAgNnPc2d1	tažné
zvířat	zvíře	k1gNnPc2	zvíře
či	či	k8xC	či
lidské	lidský	k2eAgFnSc2d1	lidská
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
pohybovat	pohybovat	k5eAaImF	pohybovat
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
pohonu	pohon	k1gInSc3	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
definici	definice	k1gFnSc3	definice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
motorová	motorový	k2eAgNnPc4d1	motorové
jednostopá	jednostopý	k2eAgNnPc4d1	jednostopé
vozidla	vozidlo	k1gNnPc4	vozidlo
(	(	kIx(	(
<g/>
motorky	motorek	k1gInPc1	motorek
<g/>
,	,	kIx,	,
mopedy	moped	k1gInPc1	moped
<g/>
,	,	kIx,	,
motorová	motorový	k2eAgNnPc1d1	motorové
jízdní	jízdní	k2eAgNnPc1d1	jízdní
kola	kolo	k1gNnPc1	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
a	a	k8xC	a
pojízdné	pojízdný	k2eAgInPc1d1	pojízdný
pracovní	pracovní	k2eAgInPc1d1	pracovní
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
však	však	k9	však
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
právních	právní	k2eAgInPc6d1	právní
předpisech	předpis	k1gInPc6	předpis
termín	termín	k1gInSc4	termín
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
automobil	automobil	k1gInSc1	automobil
(	(	kIx(	(
<g/>
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
část	část	k1gFnSc1	část
historie	historie	k1gFnSc2	historie
automobilů	automobil	k1gInPc2	automobil
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
realizovány	realizován	k2eAgInPc1d1	realizován
první	první	k4xOgInPc1	první
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
vozidly	vozidlo	k1gNnPc7	vozidlo
poháněnými	poháněný	k2eAgNnPc7d1	poháněné
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gFnPc3	jejich
prvním	první	k4xOgFnPc3	první
konstruktérům	konstruktér	k1gMnPc3	konstruktér
patřili	patřit	k5eAaImAgMnP	patřit
Skot	skot	k1gInSc1	skot
James	James	k1gMnSc1	James
Watt	watt	k1gInSc1	watt
a	a	k8xC	a
nebo	nebo	k8xC	nebo
Francouz	Francouz	k1gMnSc1	Francouz
Nicolas	Nicolas	k1gMnSc1	Nicolas
Joseph	Joseph	k1gMnSc1	Joseph
Cugnot	Cugnota	k1gFnPc2	Cugnota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
uvezl	uvézt	k5eAaPmAgInS	uvézt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1769	[number]	k4	1769
čtyři	čtyři	k4xCgMnPc4	čtyři
pasažéry	pasažér	k1gMnPc4	pasažér
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
9	[number]	k4	9
km	km	kA	km
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
doménou	doména	k1gFnSc7	doména
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zlepšovaly	zlepšovat	k5eAaImAgInP	zlepšovat
a	a	k8xC	a
zrychlovaly	zrychlovat	k5eAaImAgInP	zrychlovat
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neměnilo	měnit	k5eNaImAgNnS	měnit
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
provozní	provozní	k2eAgFnSc6d1	provozní
náročnosti	náročnost	k1gFnSc6	náročnost
a	a	k8xC	a
těžkopádnosti	těžkopádnost	k1gFnSc6	těžkopádnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvrat	zvrat	k1gInSc1	zvrat
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konstruktérům	konstruktér	k1gMnPc3	konstruktér
podařilo	podařit	k5eAaPmAgNnS	podařit
zprovoznit	zprovoznit	k5eAaPmF	zprovoznit
první	první	k4xOgInPc4	první
spalovací	spalovací	k2eAgInPc4d1	spalovací
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1862	[number]	k4	1862
až	až	k9	až
1866	[number]	k4	1866
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Nicolaus	Nicolaus	k1gInSc1	Nicolaus
Otto	Otto	k1gMnSc1	Otto
první	první	k4xOgMnSc1	první
čtyřdobý	čtyřdobý	k2eAgInSc1d1	čtyřdobý
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
vývoj	vývoj	k1gInSc1	vývoj
dnešních	dnešní	k2eAgInPc2d1	dnešní
automobilů	automobil	k1gInPc2	automobil
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Mannheimu	Mannheim	k1gInSc6	Mannheim
u	u	k7c2	u
Karla	Karel	k1gMnSc2	Karel
Benze	Benze	k1gFnSc2	Benze
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
svoji	svůj	k3xOyFgFnSc4	svůj
motorovou	motorový	k2eAgFnSc4d1	motorová
tříkolku	tříkolka	k1gFnSc4	tříkolka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
dálkovou	dálkový	k2eAgFnSc4d1	dálková
jízdu	jízda	k1gFnSc4	jízda
s	s	k7c7	s
automobilem	automobil	k1gInSc7	automobil
podnikla	podniknout	k5eAaPmAgFnS	podniknout
Bertha	Bertha	k1gFnSc1	Bertha
Benzová	Benzová	k1gFnSc1	Benzová
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
Mannheimu	Mannheim	k1gInSc2	Mannheim
do	do	k7c2	do
Pforzheimu	Pforzheim	k1gInSc2	Pforzheim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
zcela	zcela	k6eAd1	zcela
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Karlu	Karel	k1gMnSc6	Karel
Benzovi	Benz	k1gMnSc6	Benz
začal	začít	k5eAaPmAgMnS	začít
automobily	automobil	k1gInPc4	automobil
stavět	stavět	k5eAaImF	stavět
také	také	k9	také
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gMnSc1	Daimler
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
motorů	motor	k1gInPc2	motor
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Maybachem	Maybach	k1gMnSc7	Maybach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
pak	pak	k6eAd1	pak
Němec	Němec	k1gMnSc1	Němec
Rudolf	Rudolf	k1gMnSc1	Rudolf
Diesel	diesel	k1gInSc4	diesel
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
první	první	k4xOgInSc4	první
provozuschopný	provozuschopný	k2eAgInSc4d1	provozuschopný
vznětový	vznětový	k2eAgInSc4d1	vznětový
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
automobilem	automobil	k1gInSc7	automobil
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1888-1889	[number]	k4	1888-1889
druhý	druhý	k4xOgInSc4	druhý
Marcusův	Marcusův	k2eAgInSc4d1	Marcusův
automobil	automobil	k1gInSc4	automobil
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
v	v	k7c6	v
adamovském	adamovský	k2eAgInSc6d1	adamovský
podniku	podnik	k1gInSc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
funkčním	funkční	k2eAgInSc7d1	funkční
automobilem	automobil	k1gInSc7	automobil
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
Präsident	Präsident	k1gMnSc1	Präsident
(	(	kIx(	(
<g/>
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
prezidenta	prezident	k1gMnSc2	prezident
rakouského	rakouský	k2eAgInSc2d1	rakouský
autoklubu	autoklub	k1gInSc2	autoklub
<g/>
)	)	kIx)	)
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
Kopřivnické	kopřivnický	k2eAgFnSc6d1	kopřivnická
vozovce	vozovka	k1gFnSc6	vozovka
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Nesselsdorfer	Nesselsdorfer	k1gMnSc1	Nesselsdorfer
Wagenbaufabriksgesellschaft	Wagenbaufabriksgesellschaft	k1gMnSc1	Wagenbaufabriksgesellschaft
–	–	k?	–
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
NW	NW	kA	NW
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
automobilka	automobilka	k1gFnSc1	automobilka
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
následoval	následovat	k5eAaImAgInS	následovat
první	první	k4xOgInSc4	první
nákladní	nákladní	k2eAgInSc4d1	nákladní
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Automobily	automobil	k1gInPc1	automobil
Benz	Benza	k1gFnPc2	Benza
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
vybavovány	vybavován	k2eAgMnPc4d1	vybavován
otočným	otočný	k2eAgInSc7d1	otočný
řídicím	řídicí	k2eAgInSc7d1	řídicí
čepem	čep	k1gInSc7	čep
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
elektromobily	elektromobil	k1gInPc1	elektromobil
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
mezi	mezi	k7c7	mezi
automobily	automobil	k1gInPc7	automobil
s	s	k7c7	s
parním	parní	k2eAgInSc7d1	parní
<g/>
,	,	kIx,	,
elektrickým	elektrický	k2eAgInSc7d1	elektrický
a	a	k8xC	a
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
trvala	trvalo	k1gNnSc2	trvalo
téměř	téměř	k6eAd1	téměř
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
prvního	první	k4xOgNnSc2	první
desetiletí	desetiletí	k1gNnSc2	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začaly	začít	k5eAaPmAgInP	začít
dominovat	dominovat	k5eAaImF	dominovat
automobily	automobil	k1gInPc4	automobil
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
i	i	k8xC	i
když	když	k8xS	když
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
efektivity	efektivita	k1gFnSc2	efektivita
přenosu	přenos	k1gInSc2	přenos
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
i	i	k9	i
po	po	k7c6	po
století	století	k1gNnSc6	století
vývoje	vývoj	k1gInSc2	vývoj
dvakrát	dvakrát	k6eAd1	dvakrát
výhodnější	výhodný	k2eAgInSc1d2	výhodnější
elektromobil	elektromobil	k1gInSc1	elektromobil
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
benzínem	benzín	k1gInSc7	benzín
či	či	k8xC	či
naftou	nafta	k1gFnSc7	nafta
poháněné	poháněný	k2eAgInPc1d1	poháněný
automobily	automobil	k1gInPc1	automobil
staly	stát	k5eAaPmAgInP	stát
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Revoluci	revoluce	k1gFnSc4	revoluce
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
masové	masový	k2eAgNnSc1d1	masové
rozšíření	rozšíření	k1gNnSc1	rozšíření
automobilů	automobil	k1gInPc2	automobil
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
v	v	k7c6	v
USA	USA	kA	USA
Henry	Henry	k1gMnSc1	Henry
Ford	ford	k1gInSc4	ford
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
lidově	lidově	k6eAd1	lidově
dostupný	dostupný	k2eAgInSc4d1	dostupný
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
Ford	ford	k1gInSc1	ford
model	model	k1gInSc1	model
T	T	kA	T
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
os	osa	k1gFnPc2	osa
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
středů	střed	k1gInPc2	střed
otisků	otisk	k1gInPc2	otisk
pneumatik	pneumatika	k1gFnPc2	pneumatika
jedné	jeden	k4xCgFnSc2	jeden
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Rozchod	rozchod	k1gInSc1	rozchod
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
nápravy	náprava	k1gFnSc2	náprava
vozidla	vozidlo	k1gNnSc2	vozidlo
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
svislých	svislý	k2eAgFnPc2d1	svislá
rovin	rovina	k1gFnPc2	rovina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
předního	přední	k2eAgInSc2d1	přední
a	a	k8xC	a
zadního	zadní	k2eAgInSc2d1	zadní
konce	konec	k1gInSc2	konec
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
se	se	k3xPyFc4	se
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
zpětná	zpětný	k2eAgNnPc1d1	zpětné
zrcátka	zrcátko	k1gNnPc1	zrcátko
<g/>
,	,	kIx,	,
obrysová	obrysový	k2eAgNnPc1d1	obrysové
a	a	k8xC	a
směrová	směrový	k2eAgNnPc1d1	směrové
světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
pružné	pružný	k2eAgFnPc1d1	pružná
části	část	k1gFnPc1	část
apod.	apod.	kA	apod.
Výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
při	při	k7c6	při
pohotovostní	pohotovostní	k2eAgFnSc6d1	pohotovostní
hmotnosti	hmotnost	k1gFnSc6	hmotnost
automobilu	automobil	k1gInSc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
svislé	svislý	k2eAgFnSc2d1	svislá
roviny	rovina	k1gFnSc2	rovina
procházející	procházející	k2eAgFnSc2d1	procházející
osou	osa	k1gFnSc7	osa
kola	kolo	k1gNnSc2	kolo
k	k	k7c3	k
nejvzdálenějšímu	vzdálený	k2eAgInSc3d3	nejvzdálenější
bodu	bod	k1gInSc3	bod
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
<g/>
/	/	kIx~	/
<g/>
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Určuje	určovat	k5eAaImIp3nS	určovat
se	se	k3xPyFc4	se
při	při	k7c6	při
maximálním	maximální	k2eAgNnSc6d1	maximální
zatížení	zatížení	k1gNnSc6	zatížení
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
podložkou	podložka	k1gFnSc7	podložka
a	a	k8xC	a
rovinou	rovina	k1gFnSc7	rovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tečná	tečný	k2eAgFnSc1d1	tečná
k	k	k7c3	k
pneumatikám	pneumatika	k1gFnPc3	pneumatika
a	a	k8xC	a
neleží	ležet	k5eNaImIp3nS	ležet
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
žádný	žádný	k3yNgInSc4	žádný
bod	bod	k1gInSc4	bod
karoserie	karoserie	k1gFnSc2	karoserie
před	před	k7c7	před
<g/>
/	/	kIx~	/
<g/>
za	za	k7c7	za
nápravou	náprava	k1gFnSc7	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
bodu	bod	k1gInSc2	bod
podvozku	podvozek	k1gInSc2	podvozek
nebo	nebo	k8xC	nebo
karoserie	karoserie	k1gFnSc2	karoserie
od	od	k7c2	od
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
při	při	k7c6	při
zatížení	zatížení	k1gNnSc6	zatížení
automobilu	automobil	k1gInSc2	automobil
maximální	maximální	k2eAgFnSc7d1	maximální
povolenou	povolený	k2eAgFnSc7d1	povolená
užitečnou	užitečný	k2eAgFnSc7d1	užitečná
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
rozměrů	rozměr	k1gInPc2	rozměr
jsou	být	k5eAaImIp3nP	být
osobní	osobní	k2eAgInPc1d1	osobní
automobily	automobil	k1gInPc1	automobil
rozděleny	rozdělen	k2eAgInPc1d1	rozdělen
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
tendenci	tendence	k1gFnSc4	tendence
neustálého	neustálý	k2eAgInSc2d1	neustálý
nárůstu	nárůst	k1gInSc2	nárůst
jak	jak	k8xS	jak
velikosti	velikost	k1gFnSc2	velikost
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
výkonu	výkon	k1gInSc2	výkon
jejich	jejich	k3xOp3gInPc2	jejich
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
tříd	třída	k1gFnPc2	třída
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
stále	stále	k6eAd1	stále
posunují	posunovat	k5eAaImIp3nP	posunovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
modelů	model	k1gInPc2	model
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Základními	základní	k2eAgFnPc7d1	základní
technickými	technický	k2eAgFnPc7d1	technická
částmi	část	k1gFnPc7	část
současných	současný	k2eAgMnPc2d1	současný
osobních	osobní	k2eAgMnPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
jsou	být	k5eAaImIp3nP	být
karoserie	karoserie	k1gFnPc1	karoserie
<g/>
,	,	kIx,	,
podvozek	podvozek	k1gInSc1	podvozek
<g/>
,	,	kIx,	,
hnací	hnací	k2eAgFnSc1d1	hnací
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
příslušenství	příslušenství	k1gNnSc1	příslušenství
<g/>
,	,	kIx,	,
výstroj	výstroj	k1gFnSc1	výstroj
a	a	k8xC	a
výbava	výbava	k1gFnSc1	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Karoserie	karoserie	k1gFnSc1	karoserie
představuje	představovat	k5eAaImIp3nS	představovat
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
současných	současný	k2eAgInPc2d1	současný
automobilů	automobil	k1gInPc2	automobil
jeho	jeho	k3xOp3gFnSc4	jeho
nosnou	nosný	k2eAgFnSc4d1	nosná
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
náklad	náklad	k1gInSc4	náklad
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
montáž	montáž	k1gFnSc4	montáž
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Karoserie	karoserie	k1gFnSc1	karoserie
historicky	historicky	k6eAd1	historicky
starších	starý	k2eAgNnPc2d2	starší
vozidel	vozidlo	k1gNnPc2	vozidlo
byla	být	k5eAaImAgFnS	být
pojata	pojat	k2eAgFnSc1d1	pojata
jako	jako	k8xC	jako
podvozková	podvozkový	k2eAgFnSc1d1	podvozková
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
nosným	nosný	k2eAgInSc7d1	nosný
rámem	rám	k1gInSc7	rám
z	z	k7c2	z
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
ocelových	ocelový	k2eAgInPc2d1	ocelový
<g/>
)	)	kIx)	)
nosníků	nosník	k1gInPc2	nosník
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
byly	být	k5eAaImAgInP	být
přivařeny	přivařen	k2eAgInPc1d1	přivařen
kapotovací	kapotovací	k2eAgInPc1d1	kapotovací
plechy	plech	k1gInPc1	plech
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
prostor	prostor	k1gInSc4	prostor
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Karoserie	karoserie	k1gFnSc1	karoserie
dnešních	dnešní	k2eAgNnPc2d1	dnešní
vozidel	vozidlo	k1gNnPc2	vozidlo
je	být	k5eAaImIp3nS	být
koncipována	koncipován	k2eAgFnSc1d1	koncipována
jako	jako	k8xS	jako
samonosná	samonosný	k2eAgFnSc1d1	samonosná
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
nosný	nosný	k2eAgInSc4d1	nosný
rám	rám	k1gInSc4	rám
<g/>
.	.	kIx.	.
</s>
<s>
Nosnou	nosný	k2eAgFnSc4d1	nosná
funkci	funkce	k1gFnSc4	funkce
přebírají	přebírat	k5eAaImIp3nP	přebírat
samotné	samotný	k2eAgInPc1d1	samotný
kapotovací	kapotovací	k2eAgInPc1d1	kapotovací
plechy	plech	k1gInPc1	plech
<g/>
.	.	kIx.	.
</s>
<s>
Mezistupněm	mezistupeň	k1gInSc7	mezistupeň
je	být	k5eAaImIp3nS	být
karoserie	karoserie	k1gFnSc1	karoserie
polonosná	polonosný	k2eAgFnSc1d1	polonosný
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
části	část	k1gFnPc1	část
karoserie	karoserie	k1gFnSc2	karoserie
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Používány	používat	k5eAaImNgFnP	používat
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc1d1	běžná
konstrukční	konstrukční	k2eAgFnPc1d1	konstrukční
oceli	ocel	k1gFnPc1	ocel
<g/>
,	,	kIx,	,
nízko	nízko	k6eAd1	nízko
a	a	k8xC	a
vysokolegované	vysokolegovaný	k2eAgFnPc4d1	vysokolegovaná
oceli	ocel	k1gFnPc4	ocel
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnPc4	ocel
s	s	k7c7	s
transformačně	transformačně	k6eAd1	transformačně
indukovanou	indukovaný	k2eAgFnSc7d1	indukovaná
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
tvárná	tvárný	k2eAgFnSc1d1	tvárná
litina	litina	k1gFnSc1	litina
<g/>
,	,	kIx,	,
hliníkové	hliníkový	k2eAgFnPc1d1	hliníková
slitiny	slitina	k1gFnPc1	slitina
<g/>
,	,	kIx,	,
plasty	plast	k1gInPc1	plast
aj.	aj.	kA	aj.
Karoserie	karoserie	k1gFnSc2	karoserie
hraje	hrát	k5eAaImIp3nS	hrát
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
zajišťování	zajišťování	k1gNnSc6	zajišťování
aktivní	aktivní	k2eAgFnSc2d1	aktivní
i	i	k8xC	i
pasivní	pasivní	k2eAgFnSc2d1	pasivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
deformační	deformační	k2eAgFnPc4d1	deformační
zóny	zóna	k1gFnPc4	zóna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
pohltit	pohltit	k5eAaPmF	pohltit
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
co	co	k9	co
největší	veliký	k2eAgNnSc1d3	veliký
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
karoserii	karoserie	k1gFnSc6	karoserie
odděleny	oddělen	k2eAgInPc4d1	oddělen
prostory	prostor	k1gInPc4	prostor
pro	pro	k7c4	pro
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
náklad	náklad	k1gInSc4	náklad
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
osobní	osobní	k2eAgInPc1d1	osobní
vozy	vůz	k1gInPc1	vůz
na	na	k7c6	na
<g/>
:	:	kIx,	:
Motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
i	i	k8xC	i
náklad	náklad	k1gInSc4	náklad
od	od	k7c2	od
sebe	se	k3xPyFc2	se
nejsou	být	k5eNaImIp3nP	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
pevnými	pevný	k2eAgFnPc7d1	pevná
příčkami	příčka	k1gFnPc7	příčka
karoserie	karoserie	k1gFnSc2	karoserie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
motor	motor	k1gInSc4	motor
je	být	k5eAaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
náklad	náklad	k1gInSc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Oddělené	oddělený	k2eAgFnPc1d1	oddělená
prostory	prostora	k1gFnPc1	prostora
pro	pro	k7c4	pro
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
posádku	posádka	k1gFnSc4	posádka
i	i	k8xC	i
náklad	náklad	k1gInSc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
karoserie	karoserie	k1gFnSc2	karoserie
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tyto	tento	k3xDgInPc4	tento
typy	typ	k1gInPc4	typ
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
<g/>
:	:	kIx,	:
Podvozek	podvozek	k1gInSc1	podvozek
vozidla	vozidlo	k1gNnSc2	vozidlo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
nápravy	náprava	k1gFnSc2	náprava
<g/>
,	,	kIx,	,
odpružení	odpružení	k1gNnSc2	odpružení
<g/>
,	,	kIx,	,
vozidlových	vozidlový	k2eAgFnPc2d1	vozidlová
kol	kola	k1gFnPc2	kola
<g/>
,	,	kIx,	,
brzdové	brzdový	k2eAgFnSc2d1	brzdová
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Podvozek	podvozek	k1gInSc4	podvozek
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jízdní	jízdní	k2eAgFnPc4d1	jízdní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
náprava	náprava	k1gFnSc1	náprava
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
je	být	k5eAaImIp3nS	být
řídící	řídící	k2eAgNnSc1d1	řídící
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
náprava	náprava	k1gFnSc1	náprava
je	být	k5eAaImIp3nS	být
hnací	hnací	k2eAgFnSc1d1	hnací
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
koncepci	koncepce	k1gFnSc4	koncepce
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
:	:	kIx,	:
Motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
spojka	spojka	k1gFnSc1	spojka
a	a	k8xC	a
převodovka	převodovka	k1gFnSc1	převodovka
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
vpředu	vpředu	k6eAd1	vpředu
<g/>
,	,	kIx,	,
rozvodovka	rozvodovka	k1gFnSc1	rozvodovka
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnSc1d1	hnací
náprava	náprava	k1gFnSc1	náprava
je	být	k5eAaImIp3nS	být
zadní	zadní	k2eAgFnSc1d1	zadní
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
hnacího	hnací	k2eAgInSc2d1	hnací
momentu	moment	k1gInSc2	moment
z	z	k7c2	z
převodovky	převodovka	k1gFnSc2	převodovka
na	na	k7c4	na
rozvodovku	rozvodovka	k1gFnSc4	rozvodovka
je	být	k5eAaImIp3nS	být
kardanovým	kardanový	k2eAgInSc7d1	kardanový
hřídelem	hřídel	k1gInSc7	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
koncepci	koncepce	k1gFnSc4	koncepce
používají	používat	k5eAaImIp3nP	používat
např.	např.	kA	např.
vozy	vůz	k1gInPc7	vůz
BMW	BMW	kA	BMW
a	a	k8xC	a
také	také	k6eAd1	také
např.	např.	kA	např.
vozy	vůz	k1gInPc1	vůz
VAZ	vaz	k1gInSc1	vaz
Lada	Lada	k1gFnSc1	Lada
2101	[number]	k4	2101
<g/>
-	-	kIx~	-
<g/>
2107	[number]	k4	2107
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
pohonu	pohon	k1gInSc2	pohon
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
u	u	k7c2	u
přední	přední	k2eAgFnSc2d1	přední
hnací	hnací	k2eAgFnSc2d1	hnací
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
koncepci	koncepce	k1gFnSc4	koncepce
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
většinou	většina	k1gFnSc7	většina
napříč	napříč	k6eAd1	napříč
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
podélně	podélně	k6eAd1	podélně
(	(	kIx(	(
<g/>
Audi	Audi	k1gNnSc1	Audi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
pohonu	pohon	k1gInSc2	pohon
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
u	u	k7c2	u
zadní	zadní	k2eAgFnSc2d1	zadní
hnací	hnací	k2eAgFnSc2d1	hnací
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
motor	motor	k1gInSc1	motor
uložen	uložit	k5eAaPmNgInS	uložit
napříč	napříč	k6eAd1	napříč
před	před	k7c7	před
zadní	zadní	k2eAgFnSc7d1	zadní
nápravou	náprava	k1gFnSc7	náprava
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
provedení	provedení	k1gNnSc1	provedení
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
uprostřed	uprostřed	k7c2	uprostřed
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Porsche	Porsche	k1gNnSc1	Porsche
Cayman	Cayman	k1gMnSc1	Cayman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenášejí	přenášet	k5eAaImIp3nP	přenášet
tíhovou	tíhový	k2eAgFnSc4d1	tíhová
sílu	síla	k1gFnSc4	síla
karoserie	karoserie	k1gFnSc2	karoserie
<g/>
,	,	kIx,	,
hnací	hnací	k2eAgFnSc2d1	hnací
<g/>
,	,	kIx,	,
brzdné	brzdný	k2eAgFnSc2d1	brzdná
a	a	k8xC	a
setrvačné	setrvačný	k2eAgFnSc2d1	setrvačná
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
pohybem	pohyb	k1gInSc7	pohyb
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
řízení	řízení	k1gNnSc4	řízení
vozidla	vozidlo	k1gNnSc2	vozidlo
a	a	k8xC	a
odpružení	odpružení	k1gNnSc2	odpružení
<g/>
.	.	kIx.	.
</s>
<s>
Nápravy	náprava	k1gFnPc1	náprava
podle	podle	k7c2	podle
řiditelnosti	řiditelnost	k1gFnSc2	řiditelnost
<g/>
:	:	kIx,	:
řízená	řízený	k2eAgFnSc1d1	řízená
(	(	kIx(	(
<g/>
řídící	řídící	k2eAgFnSc1d1	řídící
<g/>
,	,	kIx,	,
řiditelná	řiditelný	k2eAgFnSc1d1	řiditelná
<g/>
)	)	kIx)	)
neřízená	řízený	k2eNgFnSc1d1	neřízená
Nápravy	náprava	k1gFnPc1	náprava
podle	podle	k7c2	podle
brzditelnosti	brzditelnost	k1gFnSc2	brzditelnost
<g/>
:	:	kIx,	:
brzděná	brzděný	k2eAgFnSc1d1	brzděná
nebrzděná	brzděný	k2eNgFnSc1d1	nebrzděná
Nápravy	náprava	k1gFnPc1	náprava
podle	podle	k7c2	podle
pohonu	pohon	k1gInSc2	pohon
<g/>
:	:	kIx,	:
hnací	hnací	k2eAgFnSc1d1	hnací
(	(	kIx(	(
<g/>
hnaná	hnaný	k2eAgFnSc1d1	hnaná
<g/>
,	,	kIx,	,
poháněná	poháněný	k2eAgFnSc1d1	poháněná
<g/>
,	,	kIx,	,
záběrová	záběrový	k2eAgFnSc1d1	záběrová
<g/>
)	)	kIx)	)
volná	volný	k2eAgFnSc1d1	volná
(	(	kIx(	(
<g/>
nehnaná	hnaný	k2eNgFnSc1d1	hnaný
<g/>
,	,	kIx,	,
nepoháněná	poháněný	k2eNgFnSc1d1	nepoháněná
<g/>
)	)	kIx)	)
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
náprav	náprava	k1gFnPc2	náprava
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
znak	znak	k1gInSc4	znak
náprav	náprava	k1gFnPc2	náprava
obsahující	obsahující	k2eAgFnSc4d1	obsahující
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
náprav	náprava	k1gFnPc2	náprava
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
jich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
hnaných	hnaný	k2eAgFnPc2d1	hnaná
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
uspořádání	uspořádání	k1gNnSc1	uspořádání
má	mít	k5eAaImIp3nS	mít
znak	znak	k1gInSc4	znak
náprav	náprava	k1gFnPc2	náprava
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgNnPc4	čtyři
kola	kolo	k1gNnPc4	kolo
celkem	celkem	k6eAd1	celkem
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
hnaná	hnaný	k2eAgNnPc1d1	hnané
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
obou	dva	k4xCgFnPc2	dva
náprav	náprava	k1gFnPc2	náprava
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Zmatení	zmatení	k1gNnSc1	zmatení
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
u	u	k7c2	u
náprav	náprava	k1gFnPc2	náprava
s	s	k7c7	s
dvojmontáží	dvojmontáž	k1gFnSc7	dvojmontáž
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zaužívané	zaužívaný	k2eAgNnSc1d1	zaužívané
označování	označování	k1gNnSc1	označování
tedy	tedy	k9	tedy
používá	používat	k5eAaImIp3nS	používat
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
1	[number]	k4	1
náprava	náprava	k1gFnSc1	náprava
=	=	kIx~	=
2	[number]	k4	2
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
konstrukce	konstrukce	k1gFnSc2	konstrukce
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
Nápravu	náprava	k1gFnSc4	náprava
McPherson	McPhersona	k1gFnPc2	McPhersona
Víceprvkovou	Víceprvkový	k2eAgFnSc4d1	Víceprvková
nápravu	náprava	k1gFnSc4	náprava
Lichoběžníkovou	lichoběžníkový	k2eAgFnSc4d1	lichoběžníková
nápravu	náprava	k1gFnSc4	náprava
Klikovou	klikový	k2eAgFnSc4d1	kliková
nápravu	náprava	k1gFnSc4	náprava
Úhlovou	úhlový	k2eAgFnSc4d1	úhlová
nápravu	náprava	k1gFnSc4	náprava
Kyvadlovou	kyvadlový	k2eAgFnSc4d1	kyvadlová
náprava	náprava	k1gFnSc1	náprava
Tuhou	tuhý	k2eAgFnSc4d1	tuhá
nápravu	náprava	k1gFnSc4	náprava
Nápravu	náprava	k1gFnSc4	náprava
De	De	k?	De
Dion	Dion	k1gMnSc1	Dion
Chapmanovu	Chapmanův	k2eAgFnSc4d1	Chapmanova
napravu	naprava	k1gFnSc4	naprava
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Brzda	brzda	k1gFnSc1	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Brzdová	brzdový	k2eAgFnSc1d1	brzdová
soustava	soustava	k1gFnSc1	soustava
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
rychlosti	rychlost	k1gFnSc2	rychlost
pohybujícího	pohybující	k2eAgInSc2d1	pohybující
se	se	k3xPyFc4	se
automobilu	automobil	k1gInSc2	automobil
na	na	k7c4	na
nižší	nízký	k2eAgFnSc4d2	nižší
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
hodnotě	hodnota	k1gFnSc6	hodnota
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
parkujícího	parkující	k2eAgNnSc2d1	parkující
vozidla	vozidlo	k1gNnSc2	vozidlo
před	před	k7c7	před
samovolným	samovolný	k2eAgNnSc7d1	samovolné
rozjetím	rozjetí	k1gNnSc7	rozjetí
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
nebo	nebo	k8xC	nebo
odtlačením	odtlačení	k1gNnSc7	odtlačení
nenechavci	nenechavec	k1gMnPc7	nenechavec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
brzdění	brzdění	k1gNnSc2	brzdění
vozidla	vozidlo	k1gNnSc2	vozidlo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
jeho	jeho	k3xOp3gFnSc2	jeho
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
disipací	disipace	k1gFnPc2	disipace
nebo	nebo	k8xC	nebo
rekuperací	rekuperace	k1gFnPc2	rekuperace
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rekuperace	rekuperace	k1gFnSc1	rekuperace
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
disipační	disipační	k2eAgFnPc1d1	disipační
třecí	třecí	k2eAgFnPc1d1	třecí
brzdy	brzda	k1gFnPc1	brzda
kotoučové	kotoučový	k2eAgFnPc1d1	kotoučová
a	a	k8xC	a
bubnové	bubnový	k2eAgFnPc1d1	bubnová
<g/>
.	.	kIx.	.
</s>
<s>
Pohybová	pohybový	k2eAgFnSc1d1	pohybová
energie	energie	k1gFnSc1	energie
vozidla	vozidlo	k1gNnSc2	vozidlo
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
teplo	teplo	k1gNnSc4	teplo
projevující	projevující	k2eAgNnPc4d1	projevující
se	s	k7c7	s
vzrůstem	vzrůst	k1gInSc7	vzrůst
teploty	teplota	k1gFnSc2	teplota
činných	činný	k2eAgFnPc2d1	činná
ploch	plocha	k1gFnPc2	plocha
třecích	třecí	k2eAgInPc2d1	třecí
elementů	element	k1gInPc2	element
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dále	daleko	k6eAd2	daleko
teplo	teplo	k6eAd1	teplo
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Brzdy	brzda	k1gFnPc1	brzda
bývají	bývat	k5eAaImIp3nP	bývat
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dimenzovány	dimenzovat	k5eAaBmNgFnP	dimenzovat
pro	pro	k7c4	pro
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
brzdný	brzdný	k2eAgInSc4d1	brzdný
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
lze	lze	k6eAd1	lze
intenzivněji	intenzivně	k6eAd2	intenzivně
brzdit	brzdit	k5eAaImF	brzdit
přední	přední	k2eAgNnPc4d1	přední
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
točivý	točivý	k2eAgInSc1d1	točivý
moment	moment	k1gInSc1	moment
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
převrací	převracet	k5eAaImIp3nP	převracet
vozidlo	vozidlo	k1gNnSc4	vozidlo
na	na	k7c4	na
předek	předek	k1gInSc4	předek
a	a	k8xC	a
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
odlehčuje	odlehčovat	k5eAaImIp3nS	odlehčovat
<g/>
.	.	kIx.	.
</s>
<s>
Zatížené	zatížený	k2eAgNnSc1d1	zatížené
kolo	kolo	k1gNnSc1	kolo
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
smýkat	smýkat	k5eAaImF	smýkat
při	při	k7c6	při
větší	veliký	k2eAgFnSc6d2	veliký
brzdné	brzdný	k2eAgFnSc6d1	brzdná
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolo	kolo	k1gNnSc1	kolo
odlehčené	odlehčený	k2eAgNnSc1d1	odlehčené
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
odlehčené	odlehčený	k2eAgNnSc1d1	odlehčené
kolo	kolo	k1gNnSc1	kolo
nelze	lze	k6eNd1	lze
příliš	příliš	k6eAd1	příliš
brzdit	brzdit	k5eAaImF	brzdit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
snadněji	snadno	k6eAd2	snadno
smekne	smeknout	k5eAaPmIp3nS	smeknout
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
umístění	umístění	k1gNnSc4	umístění
hnacího	hnací	k2eAgInSc2d1	hnací
agregátu	agregát	k1gInSc2	agregát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podstatně	podstatně	k6eAd1	podstatně
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dnes	dnes	k6eAd1	dnes
nejběžnějšího	běžný	k2eAgNnSc2d3	nejběžnější
uspořádání	uspořádání	k1gNnSc2	uspořádání
motoru	motor	k1gInSc2	motor
vpředu	vpředu	k6eAd1	vpředu
vycházejí	vycházet	k5eAaImIp3nP	vycházet
přední	přední	k2eAgFnPc1d1	přední
brzdy	brzda	k1gFnPc1	brzda
mnohem	mnohem	k6eAd1	mnohem
výkonnější	výkonný	k2eAgFnPc1d2	výkonnější
než	než	k8xS	než
brzdy	brzda	k1gFnPc1	brzda
zadních	zadní	k2eAgNnPc2d1	zadní
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kotoučové	kotoučový	k2eAgFnPc1d1	kotoučová
brzdy	brzda	k1gFnPc1	brzda
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
přední	přední	k2eAgFnPc4d1	přední
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c4	na
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Holé	Holé	k2eAgInPc1d1	Holé
kotouče	kotouč	k1gInPc1	kotouč
se	se	k3xPyFc4	se
snáze	snadno	k6eAd2	snadno
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
okolním	okolní	k2eAgInSc7d1	okolní
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
tak	tak	k6eAd1	tak
při	při	k7c6	při
stejném	stejný	k2eAgInSc6d1	stejný
výkonu	výkon	k1gInSc6	výkon
vycházejí	vycházet	k5eAaImIp3nP	vycházet
rozměrově	rozměrově	k6eAd1	rozměrově
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
bubnové	bubnový	k2eAgFnPc1d1	bubnová
<g/>
.	.	kIx.	.
</s>
<s>
Výhodná	výhodný	k2eAgFnSc1d1	výhodná
je	být	k5eAaImIp3nS	být
i	i	k9	i
snadná	snadný	k2eAgFnSc1d1	snadná
výměna	výměna	k1gFnSc1	výměna
brzdných	brzdný	k2eAgInPc2d1	brzdný
elementů	element	k1gInPc2	element
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
brzdění	brzdění	k1gNnSc4	brzdění
zadních	zadní	k2eAgNnPc2d1	zadní
kol	kolo	k1gNnPc2	kolo
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodná	vhodný	k2eAgFnSc1d1	vhodná
(	(	kIx(	(
<g/>
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
konstrukce	konstrukce	k1gFnSc1	konstrukce
ruční	ruční	k2eAgFnSc2d1	ruční
brzdy	brzda	k1gFnSc2	brzda
<g/>
,	,	kIx,	,
koroze	koroze	k1gFnSc1	koroze
kotoučů	kotouč	k1gInPc2	kotouč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bubnové	bubnový	k2eAgFnPc1d1	bubnová
brzdy	brzda	k1gFnPc1	brzda
mají	mít	k5eAaImIp3nP	mít
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
konstrukci	konstrukce	k1gFnSc6	konstrukce
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
chráněné	chráněný	k2eAgFnSc2d1	chráněná
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
aut	auto	k1gNnPc2	auto
k	k	k7c3	k
brzdění	brzdění	k1gNnSc3	brzdění
zadních	zadní	k2eAgNnPc2d1	zadní
kol	kolo	k1gNnPc2	kolo
i	i	k9	i
pro	pro	k7c4	pro
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
realizace	realizace	k1gFnSc2	realizace
mechanizmu	mechanizmus	k1gInSc2	mechanizmus
ruční	ruční	k2eAgFnPc4d1	ruční
parkovací	parkovací	k2eAgFnPc4d1	parkovací
brzdy	brzda	k1gFnPc4	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnPc1d1	zadní
bubnové	bubnový	k2eAgFnPc1d1	bubnová
brzdy	brzda	k1gFnPc1	brzda
se	se	k3xPyFc4	se
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
rozměrově	rozměrově	k6eAd1	rozměrově
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
provozní	provozní	k2eAgNnSc4d1	provozní
brzdění	brzdění	k1gNnSc4	brzdění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ruční	ruční	k2eAgFnSc1d1	ruční
mechanická	mechanický	k2eAgFnSc1d1	mechanická
(	(	kIx(	(
<g/>
parkovací	parkovací	k2eAgFnSc1d1	parkovací
<g/>
)	)	kIx)	)
brzda	brzda	k1gFnSc1	brzda
byla	být	k5eAaImAgFnS	být
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
provoznímu	provozní	k2eAgNnSc3d1	provozní
brzdění	brzdění	k1gNnSc3	brzdění
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
hydraulický	hydraulický	k2eAgInSc1d1	hydraulický
tlak	tlak	k1gInSc1	tlak
snížený	snížený	k2eAgInSc1d1	snížený
redukčními	redukční	k2eAgInPc7d1	redukční
ventily	ventil	k1gInPc7	ventil
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
výkon	výkon	k1gInSc4	výkon
zadních	zadní	k2eAgFnPc2d1	zadní
brzd	brzda	k1gFnPc2	brzda
snížen	snížit	k5eAaPmNgInS	snížit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
životnosti	životnost	k1gFnSc2	životnost
jejich	jejich	k3xOp3gNnSc4	jejich
obložení	obložení	k1gNnSc4	obložení
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
užitkových	užitkový	k2eAgNnPc2d1	užitkové
vozidel	vozidlo	k1gNnPc2	vozidlo
je	být	k5eAaImIp3nS	být
zadní	zadní	k2eAgFnSc1d1	zadní
náprava	náprava	k1gFnSc1	náprava
zatížena	zatížit	k5eAaPmNgFnS	zatížit
různě	různě	k6eAd1	různě
podle	podle	k7c2	podle
hmotnosti	hmotnost	k1gFnSc2	hmotnost
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
brzdného	brzdný	k2eAgInSc2d1	brzdný
tlaku	tlak	k1gInSc2	tlak
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nP	sloužit
redukční	redukční	k2eAgInPc1d1	redukční
ventily	ventil	k1gInPc1	ventil
s	s	k7c7	s
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
regulací	regulace	k1gFnSc7	regulace
redukčního	redukční	k2eAgInSc2d1	redukční
účinku	účinek	k1gInSc2	účinek
podle	podle	k7c2	podle
zatížení	zatížení	k1gNnSc2	zatížení
zadní	zadní	k2eAgFnSc2d1	zadní
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
vůlí	vůle	k1gFnPc2	vůle
<g/>
,	,	kIx,	,
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
a	a	k8xC	a
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
realizace	realizace	k1gFnSc2	realizace
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
brzdy	brzda	k1gFnPc1	brzda
s	s	k7c7	s
hydraulickým	hydraulický	k2eAgInSc7d1	hydraulický
rozvodem	rozvod	k1gInSc7	rozvod
brzdné	brzdný	k2eAgFnSc2d1	brzdná
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
se	se	k3xPyFc4	se
rozvody	rozvod	k1gInPc7	rozvod
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
dvou	dva	k4xCgMnPc2	dva
relativně	relativně	k6eAd1	relativně
samostatných	samostatný	k2eAgMnPc2d1	samostatný
okruhů	okruh	k1gInPc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okruhu	okruh	k1gInSc6	okruh
nezpůsobí	způsobit	k5eNaPmIp3nS	způsobit
úplné	úplný	k2eAgNnSc4d1	úplné
selhání	selhání	k1gNnSc4	selhání
brzd	brzda	k1gFnPc2	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okruhu	okruh	k1gInSc6	okruh
levé	levý	k2eAgFnSc2d1	levá
přední	přední	k2eAgNnPc4d1	přední
s	s	k7c7	s
pravým	pravý	k2eAgNnSc7d1	pravé
zadním	zadní	k2eAgNnSc7d1	zadní
a	a	k8xC	a
pravé	pravý	k2eAgFnPc4d1	pravá
přední	přední	k2eAgNnPc4d1	přední
s	s	k7c7	s
levým	levý	k2eAgNnSc7d1	levé
zadním	zadní	k2eAgNnSc7d1	zadní
kolem	kolo	k1gNnSc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
selhání	selhání	k1gNnSc6	selhání
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
okruhů	okruh	k1gInPc2	okruh
je	být	k5eAaImIp3nS	být
brzdění	brzdění	k1gNnSc1	brzdění
dost	dost	k6eAd1	dost
nesymetrické	symetrický	k2eNgNnSc1d1	nesymetrické
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
poruchu	porucha	k1gFnSc4	porucha
nelze	lze	k6eNd1	lze
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okruhu	okruh	k1gInSc6	okruh
zadní	zadní	k2eAgFnSc2d1	zadní
a	a	k8xC	a
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
přední	přední	k2eAgFnPc4d1	přední
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
nákladní	nákladní	k2eAgNnPc1d1	nákladní
vozidla	vozidlo	k1gNnPc1	vozidlo
a	a	k8xC	a
traktory	traktor	k1gInPc1	traktor
mají	mít	k5eAaImIp3nP	mít
vzduchový	vzduchový	k2eAgInSc4d1	vzduchový
kompresor	kompresor	k1gInSc4	kompresor
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
tlaku	tlak	k1gInSc2	tlak
pro	pro	k7c4	pro
pneumatické	pneumatický	k2eAgInPc4d1	pneumatický
brzdové	brzdový	k2eAgInPc4d1	brzdový
rozvody	rozvod	k1gInPc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
brzdy	brzda	k1gFnPc1	brzda
plně	plně	k6eAd1	plně
brzdí	brzdit	k5eAaImIp3nP	brzdit
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšením	zvýšení	k1gNnSc7	zvýšení
tlaku	tlak	k1gInSc2	tlak
se	se	k3xPyFc4	se
odbrzdí	odbrzdit	k5eAaPmIp3nS	odbrzdit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
se	se	k3xPyFc4	se
přibrzďuje	přibrzďovat	k5eAaImIp3nS	přibrzďovat
odpouštěním	odpouštění	k1gNnSc7	odpouštění
tlaku	tlak	k1gInSc2	tlak
z	z	k7c2	z
pracovních	pracovní	k2eAgInPc2d1	pracovní
válců	válec	k1gInPc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Vozidlo	vozidlo	k1gNnSc1	vozidlo
může	moct	k5eAaImIp3nS	moct
zahájit	zahájit	k5eAaPmF	zahájit
jízdu	jízda	k1gFnSc4	jízda
až	až	k9	až
po	po	k7c6	po
natlakování	natlakování	k1gNnSc6	natlakování
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
ekologické	ekologický	k2eAgInPc4d1	ekologický
a	a	k8xC	a
stále	stále	k6eAd1	stále
dostupné	dostupný	k2eAgNnSc4d1	dostupné
médium	médium	k1gNnSc4	médium
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInSc1	jeho
únik	únik	k1gInSc1	únik
při	při	k7c6	při
zapojování	zapojování	k1gNnSc6	zapojování
a	a	k8xC	a
odpojování	odpojování	k1gNnSc6	odpojování
vlečných	vlečný	k2eAgFnPc2d1	vlečná
souprav	souprava	k1gFnPc2	souprava
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vzduchové	vzduchový	k2eAgFnPc1d1	vzduchová
brzdy	brzda	k1gFnPc1	brzda
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
vozidel	vozidlo	k1gNnPc2	vozidlo
určených	určený	k2eAgNnPc2d1	určené
k	k	k7c3	k
vlečení	vlečení	k1gNnSc3	vlečení
přívěsů	přívěs	k1gInPc2	přívěs
a	a	k8xC	a
návěsů	návěs	k1gInPc2	návěs
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tato	tento	k3xDgNnPc1	tento
vozidla	vozidlo	k1gNnPc1	vozidlo
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
brzdovou	brzdový	k2eAgFnSc4d1	brzdová
soustavu	soustava	k1gFnSc4	soustava
hydraulickou	hydraulický	k2eAgFnSc4d1	hydraulická
a	a	k8xC	a
soustavu	soustava	k1gFnSc4	soustava
pneumatickou	pneumatický	k2eAgFnSc7d1	pneumatická
mají	mít	k5eAaImIp3nP	mít
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
brzdění	brzdění	k1gNnSc4	brzdění
přívěsů	přívěs	k1gInPc2	přívěs
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
brzd	brzda	k1gFnPc2	brzda
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
zamrzání	zamrzání	k1gNnSc2	zamrzání
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
tlaku	tlak	k1gInSc2	tlak
ze	z	k7c2	z
soustavy	soustava	k1gFnSc2	soustava
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kola	kolo	k1gNnPc1	kolo
přibrzďována	přibrzďován	k2eAgNnPc1d1	přibrzďován
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
toho	ten	k3xDgMnSc4	ten
řidič	řidič	k1gMnSc1	řidič
včas	včas	k6eAd1	včas
nevšimne	všimnout	k5eNaPmIp3nS	všimnout
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
takovému	takový	k3xDgInSc3	takový
zahřátí	zahřátý	k2eAgMnPc1d1	zahřátý
brzd	brzda	k1gFnPc2	brzda
<g/>
,	,	kIx,	,
že	že	k8xS	že
explodují	explodovat	k5eAaBmIp3nP	explodovat
pneumatiky	pneumatika	k1gFnPc4	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
vozidel	vozidlo	k1gNnPc2	vozidlo
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
podtlakový	podtlakový	k2eAgInSc1d1	podtlakový
posilovač	posilovač	k1gInSc1	posilovač
brzd	brzda	k1gFnPc2	brzda
využívající	využívající	k2eAgFnSc1d1	využívající
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
sacím	sací	k2eAgNnSc6d1	sací
potrubí	potrubí	k1gNnSc6	potrubí
motoru	motor	k1gInSc2	motor
k	k	k7c3	k
usnadnění	usnadnění	k1gNnSc3	usnadnění
ovládání	ovládání	k1gNnSc2	ovládání
brzdového	brzdový	k2eAgInSc2d1	brzdový
pedálu	pedál	k1gInSc2	pedál
řidičem	řidič	k1gMnSc7	řidič
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
převoz	převoz	k1gInSc4	převoz
těžkých	těžký	k2eAgInPc2d1	těžký
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
neekonomické	ekonomický	k2eNgNnSc1d1	neekonomické
dimenzovat	dimenzovat	k5eAaBmF	dimenzovat
běžné	běžný	k2eAgFnPc4d1	běžná
brzdy	brzda	k1gFnPc4	brzda
na	na	k7c4	na
tak	tak	k6eAd1	tak
enormní	enormní	k2eAgNnSc4d1	enormní
zatížení	zatížení	k1gNnSc4	zatížení
<g/>
,	,	kIx,	,
jakému	jaký	k3yQgNnSc3	jaký
by	by	k9	by
byly	být	k5eAaImAgFnP	být
vystaveny	vystaven	k2eAgFnPc1d1	vystavena
při	při	k7c6	při
sjíždění	sjíždění	k1gNnSc6	sjíždění
táhlých	táhlý	k2eAgInPc2d1	táhlý
svahů	svah	k1gInPc2	svah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
takové	takový	k3xDgFnPc4	takový
situace	situace	k1gFnPc4	situace
jsou	být	k5eAaImIp3nP	být
nákladní	nákladní	k2eAgInPc1d1	nákladní
automobily	automobil	k1gInPc1	automobil
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
vybaveny	vybavit	k5eAaPmNgInP	vybavit
motorovým	motorový	k2eAgInSc7d1	motorový
retardérem	retardér	k1gInSc7	retardér
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
běžný	běžný	k2eAgInSc1d1	běžný
čtyřdobý	čtyřdobý	k2eAgInSc1d1	čtyřdobý
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
deceleraci	decelerace	k1gFnSc6	decelerace
brzdící	brzdící	k2eAgInSc4d1	brzdící
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
ABS	ABS	kA	ABS
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
systém	systém	k1gInSc1	systém
ABS	ABS	kA	ABS
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
zablokování	zablokování	k1gNnSc4	zablokování
kola	kolo	k1gNnSc2	kolo
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
uplatnit	uplatnit	k5eAaPmF	uplatnit
elektroniku	elektronika	k1gFnSc4	elektronika
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
brzdného	brzdný	k2eAgInSc2d1	brzdný
procesu	proces	k1gInSc2	proces
vedly	vést	k5eAaImAgFnP	vést
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
systémů	systém	k1gInPc2	systém
ABS	ABS	kA	ABS
ve	v	k7c6	v
výbavě	výbava	k1gFnSc6	výbava
běžných	běžný	k2eAgInPc2d1	běžný
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
elektronika	elektronika	k1gFnSc1	elektronika
systému	systém	k1gInSc2	systém
ABS	ABS	kA	ABS
nefunkční	funkční	k2eNgFnSc1d1	nefunkční
<g/>
,	,	kIx,	,
brzdová	brzdový	k2eAgFnSc1d1	brzdová
soustava	soustava	k1gFnSc1	soustava
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
fungovat	fungovat	k5eAaImF	fungovat
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
ABS	ABS	kA	ABS
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
EBD	EBD	kA	EBD
jako	jako	k8xS	jako
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
náhradu	náhrada	k1gFnSc4	náhrada
mechanických	mechanický	k2eAgInPc2d1	mechanický
reduktorů	reduktor	k1gInPc2	reduktor
tlaku	tlak	k1gInSc2	tlak
pro	pro	k7c4	pro
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysazení	vysazení	k1gNnSc6	vysazení
elektroniky	elektronika	k1gFnSc2	elektronika
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
přebrzďována	přebrzďován	k2eAgFnSc1d1	přebrzďován
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
elektronické	elektronický	k2eAgInPc1d1	elektronický
systémy	systém	k1gInPc1	systém
řízení	řízení	k1gNnSc2	řízení
brzd	brzda	k1gFnPc2	brzda
jsou	být	k5eAaImIp3nP	být
nadstavbami	nadstavba	k1gFnPc7	nadstavba
ABS	ABS	kA	ABS
a	a	k8xC	a
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
řídícími	řídící	k2eAgInPc7d1	řídící
systémy	systém	k1gInPc7	systém
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
systémy	systém	k1gInPc1	systém
MSR	MSR	kA	MSR
<g/>
,	,	kIx,	,
ASR	ASR	kA	ASR
a	a	k8xC	a
ESP	ESP	kA	ESP
<g/>
.	.	kIx.	.
</s>
<s>
MSR	MSR	kA	MSR
brání	bránit	k5eAaImIp3nS	bránit
smyku	smyk	k1gInSc2	smyk
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
ASR	ASR	kA	ASR
brání	bránit	k5eAaImIp3nP	bránit
prokluzu	prokluz	k1gInSc3	prokluz
kol	kolo	k1gNnPc2	kolo
při	při	k7c6	při
akceleraci	akcelerace	k1gFnSc6	akcelerace
<g/>
.	.	kIx.	.
</s>
<s>
ESP	ESP	kA	ESP
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
vozidlo	vozidlo	k1gNnSc1	vozidlo
podle	podle	k7c2	podle
čidel	čidlo	k1gNnPc2	čidlo
zrychlení	zrychlení	k1gNnPc2	zrychlení
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
EDL	EDL	kA	EDL
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
EDS	EDS	kA	EDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
ABS	ABS	kA	ABS
funkci	funkce	k1gFnSc4	funkce
elektronické	elektronický	k2eAgFnSc2d1	elektronická
závěrky	závěrka	k1gFnSc2	závěrka
diferenciálu	diferenciál	k1gInSc2	diferenciál
<g/>
.	.	kIx.	.
</s>
<s>
Přibrzdí	přibrzdit	k5eAaPmIp3nS	přibrzdit
prokluzující	prokluzující	k2eAgNnSc1d1	prokluzující
se	se	k3xPyFc4	se
kolo	kolo	k1gNnSc1	kolo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
umožní	umožnit	k5eAaPmIp3nS	umožnit
přenos	přenos	k1gInSc4	přenos
hnacího	hnací	k2eAgInSc2d1	hnací
momentu	moment	k1gInSc2	moment
na	na	k7c4	na
protější	protější	k2eAgNnSc4d1	protější
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
přidanou	přidaný	k2eAgFnSc7d1	přidaná
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
HSA	HSA	kA	HSA
(	(	kIx(	(
<g/>
Hill	Hill	k1gInSc1	Hill
Start	start	k1gInSc1	start
Assist	Assist	k1gInSc1	Assist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgNnSc1d1	bránící
couvnutí	couvnutí	k1gNnSc1	couvnutí
vozidla	vozidlo	k1gNnSc2	vozidlo
při	při	k7c6	při
rozjezdu	rozjezd	k1gInSc6	rozjezd
do	do	k7c2	do
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
Spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
a	a	k8xC	a
Čtyřdobý	čtyřdobý	k2eAgInSc1d1	čtyřdobý
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnSc4d1	hnací
soustavu	soustava	k1gFnSc4	soustava
tvoří	tvořit	k5eAaImIp3nS	tvořit
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
spojka	spojka	k1gFnSc1	spojka
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
<g/>
,	,	kIx,	,
rozvodovka	rozvodovka	k1gFnSc1	rozvodovka
a	a	k8xC	a
hnací	hnací	k2eAgInPc1d1	hnací
hřídele	hřídel	k1gInPc1	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
tyto	tento	k3xDgFnPc1	tento
součásti	součást	k1gFnPc1	součást
tvoří	tvořit	k5eAaImIp3nP	tvořit
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
celek	celek	k1gInSc4	celek
pohánějící	pohánějící	k2eAgInSc4d1	pohánějící
blízkou	blízký	k2eAgFnSc4d1	blízká
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vozidel	vozidlo	k1gNnPc2	vozidlo
s	s	k7c7	s
náhonem	náhon	k1gInSc7	náhon
na	na	k7c4	na
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
nápravu	náprava	k1gFnSc4	náprava
rozvodovka	rozvodovka	k1gFnSc1	rozvodovka
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
převodovky	převodovka	k1gFnSc2	převodovka
a	a	k8xC	a
točivý	točivý	k2eAgInSc1d1	točivý
moment	moment	k1gInSc1	moment
z	z	k7c2	z
převodovky	převodovka	k1gFnSc2	převodovka
se	se	k3xPyFc4	se
do	do	k7c2	do
rozvodovky	rozvodovka	k1gFnSc2	rozvodovka
přenáší	přenášet	k5eAaImIp3nS	přenášet
torzní	torzní	k2eAgFnSc7d1	torzní
tyčí	tyč	k1gFnSc7	tyč
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kardanovou	kardanový	k2eAgFnSc7d1	Kardanová
hřídelí	hřídel	k1gFnSc7	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
motor	motor	k1gInSc4	motor
a	a	k8xC	a
převodovku	převodovka	k1gFnSc4	převodovka
je	být	k5eAaImIp3nS	být
vložena	vložit	k5eAaPmNgFnS	vložit
spojka	spojka	k1gFnSc1	spojka
zabezpečující	zabezpečující	k2eAgFnSc1d1	zabezpečující
bezrázové	bezrázový	k2eAgNnSc4d1	bezrázové
připojení	připojení	k1gNnSc4	připojení
motoru	motor	k1gInSc2	motor
ke	k	k7c3	k
zbytku	zbytek	k1gInSc3	zbytek
hnací	hnací	k2eAgFnSc2d1	hnací
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
automobily	automobil	k1gInPc7	automobil
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
převážně	převážně	k6eAd1	převážně
kotoučovou	kotoučový	k2eAgFnSc7d1	kotoučová
spojkou	spojka	k1gFnSc7	spojka
s	s	k7c7	s
talířovou	talířový	k2eAgFnSc7d1	talířová
pružinou	pružina	k1gFnSc7	pružina
<g/>
.	.	kIx.	.
</s>
<s>
Kotouč	kotouč	k1gInSc1	kotouč
takové	takový	k3xDgFnSc2	takový
spojky	spojka	k1gFnSc2	spojka
je	být	k5eAaImIp3nS	být
unášen	unášet	k5eAaImNgInS	unášet
čelní	čelní	k2eAgFnSc7d1	čelní
plochou	plocha	k1gFnSc7	plocha
setrvačníku	setrvačník	k1gInSc2	setrvačník
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
pohání	pohánět	k5eAaImIp3nP	pohánět
vstupní	vstupní	k2eAgInSc4d1	vstupní
drážkovaný	drážkovaný	k2eAgInSc4d1	drážkovaný
hřídel	hřídel	k1gInSc4	hřídel
převodovky	převodovka	k1gFnSc2	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Převodovka	převodovka	k1gFnSc1	převodovka
ať	ať	k9	ať
již	již	k6eAd1	již
manuální	manuální	k2eAgNnSc1d1	manuální
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
automatická	automatický	k2eAgFnSc1d1	automatická
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
převodu	převod	k1gInSc3	převod
otáček	otáčka	k1gFnPc2	otáčka
motoru	motor	k1gInSc2	motor
na	na	k7c4	na
otáčky	otáčka	k1gFnPc4	otáčka
kol	kol	k7c2	kol
automobilu	automobil	k1gInSc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
automobily	automobil	k1gInPc7	automobil
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
převážně	převážně	k6eAd1	převážně
manuální	manuální	k2eAgFnSc7d1	manuální
synchronizovanou	synchronizovaný	k2eAgFnSc7d1	synchronizovaná
převodovkou	převodovka	k1gFnSc7	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Automatické	automatický	k2eAgFnPc1d1	automatická
převodovky	převodovka	k1gFnPc1	převodovka
se	se	k3xPyFc4	se
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
tradici	tradice	k1gFnSc4	tradice
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Převodovka	převodovka	k1gFnSc1	převodovka
skutečných	skutečný	k2eAgMnPc2d1	skutečný
terénních	terénní	k2eAgMnPc2d1	terénní
a	a	k8xC	a
některých	některý	k3yIgNnPc2	některý
nákladních	nákladní	k2eAgNnPc2d1	nákladní
vozidel	vozidlo	k1gNnPc2	vozidlo
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
redukcí	redukce	k1gFnSc7	redukce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
převod	převod	k1gInSc1	převod
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
zařazením	zařazení	k1gNnSc7	zařazení
lze	lze	k6eAd1	lze
vynásobit	vynásobit	k5eAaPmF	vynásobit
převodový	převodový	k2eAgInSc4d1	převodový
poměr	poměr	k1gInSc4	poměr
celé	celý	k2eAgFnSc2d1	celá
převodovky	převodovka	k1gFnSc2	převodovka
a	a	k8xC	a
zdvojnásobit	zdvojnásobit	k5eAaPmF	zdvojnásobit
tak	tak	k9	tak
počet	počet	k1gInSc4	počet
převodových	převodový	k2eAgInPc2d1	převodový
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
náročném	náročný	k2eAgInSc6d1	náročný
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
převážení	převážení	k1gNnSc6	převážení
těžkých	těžký	k2eAgInPc2d1	těžký
nákladů	náklad	k1gInPc2	náklad
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
řešení	řešení	k1gNnSc1	řešení
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
.	.	kIx.	.
</s>
<s>
Dvoudobému	dvoudobý	k2eAgInSc3d1	dvoudobý
motoru	motor	k1gInSc3	motor
mazanému	mazaný	k2eAgInSc3d1	mazaný
olejem	olej	k1gInSc7	olej
v	v	k7c6	v
palivové	palivový	k2eAgFnSc6d1	palivová
směsi	směs	k1gFnSc6	směs
neprospívá	prospívat	k5eNaImIp3nS	prospívat
brzdění	brzdění	k1gNnSc1	brzdění
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
motoru	motor	k1gInSc2	motor
pak	pak	k6eAd1	pak
nepřichází	přicházet	k5eNaImIp3nS	přicházet
benzín	benzín	k1gInSc1	benzín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
žádný	žádný	k3yNgInSc4	žádný
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Motoru	motor	k1gInSc3	motor
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
hrozit	hrozit	k5eAaImF	hrozit
zadření	zadření	k1gNnSc4	zadření
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
automobily	automobil	k1gInPc1	automobil
s	s	k7c7	s
dvoudobými	dvoudobý	k2eAgInPc7d1	dvoudobý
motory	motor	k1gInPc7	motor
opatřovaly	opatřovat	k5eAaImAgFnP	opatřovat
volnoběžkou	volnoběžka	k1gFnSc7	volnoběžka
účinkující	účinkující	k2eAgFnPc1d1	účinkující
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
pokročilejších	pokročilý	k2eAgFnPc2d2	pokročilejší
konstrukcí	konstrukce	k1gFnPc2	konstrukce
jen	jen	k9	jen
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
rychlostní	rychlostní	k2eAgInSc4d1	rychlostní
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
volnoběžkou	volnoběžka	k1gFnSc7	volnoběžka
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyřazovat	vyřazovat	k5eAaImF	vyřazovat
rychlost	rychlost	k1gFnSc4	rychlost
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
z	z	k7c2	z
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc7d1	určitá
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
nemožnost	nemožnost	k1gFnSc1	nemožnost
brzdit	brzdit	k5eAaImF	brzdit
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nahodit	nahodit	k5eAaPmF	nahodit
motor	motor	k1gInSc4	motor
roztlačením	roztlačení	k1gNnSc7	roztlačení
auta	auto	k1gNnSc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
automobilech	automobil	k1gInPc6	automobil
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
převodovky	převodovka	k1gFnPc4	převodovka
minimálně	minimálně	k6eAd1	minimálně
hlučné	hlučný	k2eAgFnPc4d1	hlučná
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
účinné	účinný	k2eAgInPc1d1	účinný
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
výrobně	výrobně	k6eAd1	výrobně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Ozubená	ozubený	k2eAgNnPc1d1	ozubené
kola	kolo	k1gNnPc1	kolo
převodů	převod	k1gInPc2	převod
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
čelní	čelní	k2eAgFnSc7d1	čelní
s	s	k7c7	s
šikmým	šikmý	k2eAgNnSc7d1	šikmé
evolventním	evolventní	k2eAgNnSc7d1	evolventní
ozubením	ozubení	k1gNnSc7	ozubení
<g/>
.	.	kIx.	.
</s>
<s>
Přímá	přímý	k2eAgNnPc1d1	přímé
ozubení	ozubení	k1gNnPc1	ozubení
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
hlučnost	hlučnost	k1gFnSc4	hlučnost
používají	používat	k5eAaImIp3nP	používat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
zpátečky	zpátečka	k1gFnSc2	zpátečka
a	a	k8xC	a
na	na	k7c6	na
věnci	věnec	k1gInSc6	věnec
setrvačníku	setrvačník	k1gInSc2	setrvačník
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
u	u	k7c2	u
ozubení	ozubení	k1gNnPc2	ozubení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
jen	jen	k9	jen
po	po	k7c4	po
omezenou	omezený	k2eAgFnSc4d1	omezená
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
obrážením	obrážení	k1gNnSc7	obrážení
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
šikmá	šikmý	k2eAgNnPc1d1	šikmé
ozubení	ozubení	k1gNnPc1	ozubení
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
metodou	metoda	k1gFnSc7	metoda
odvalovacího	odvalovací	k2eAgNnSc2d1	odvalovací
frézování	frézování	k1gNnSc2	frézování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
omezení	omezení	k1gNnSc4	omezení
hlučnosti	hlučnost	k1gFnSc2	hlučnost
převodů	převod	k1gInPc2	převod
se	se	k3xPyFc4	se
na	na	k7c6	na
ozubení	ozubení	k1gNnSc6	ozubení
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
shaving	shaving	k1gInSc1	shaving
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgMnSc3	jenž
předtím	předtím	k6eAd1	předtím
kosodélníkový	kosodélníkový	k2eAgInSc1d1	kosodélníkový
průřez	průřez	k1gInSc1	průřez
zubu	zub	k1gInSc2	zub
získá	získat	k5eAaPmIp3nS	získat
mírně	mírně	k6eAd1	mírně
soudkovitý	soudkovitý	k2eAgInSc4d1	soudkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Rozvodovka	rozvodovka	k1gFnSc1	rozvodovka
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
přenos	přenos	k1gInSc4	přenos
hnacího	hnací	k2eAgInSc2d1	hnací
momentu	moment	k1gInSc2	moment
od	od	k7c2	od
převodovky	převodovka	k1gFnSc2	převodovka
na	na	k7c4	na
hnací	hnací	k2eAgInPc4d1	hnací
hřídele	hřídel	k1gInPc4	hřídel
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
základní	základní	k2eAgInSc4d1	základní
převodový	převodový	k2eAgInSc4d1	převodový
poměr	poměr	k1gInSc4	poměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
násobením	násobení	k1gNnSc7	násobení
skládá	skládat	k5eAaImIp3nS	skládat
s	s	k7c7	s
převodovým	převodový	k2eAgInSc7d1	převodový
poměrem	poměr	k1gInSc7	poměr
převodovky	převodovka	k1gFnSc2	převodovka
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
celkový	celkový	k2eAgInSc4d1	celkový
převodový	převodový	k2eAgInSc4d1	převodový
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
rozvodovky	rozvodovka	k1gFnSc2	rozvodovka
je	být	k5eAaImIp3nS	být
diferenciál	diferenciál	k1gInSc4	diferenciál
zabezpečující	zabezpečující	k2eAgNnSc1d1	zabezpečující
rozdělení	rozdělení	k1gNnSc1	rozdělení
hnacího	hnací	k2eAgInSc2d1	hnací
momentu	moment	k1gInSc2	moment
mezi	mezi	k7c4	mezi
levé	levý	k2eAgNnSc4d1	levé
a	a	k8xC	a
pravé	pravý	k2eAgNnSc4d1	pravé
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pohonů	pohon	k1gInPc2	pohon
obou	dva	k4xCgFnPc2	dva
náprav	náprava	k1gFnPc2	náprava
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
mezinápravový	mezinápravový	k2eAgInSc1d1	mezinápravový
diferenciál	diferenciál	k1gInSc1	diferenciál
se	s	k7c7	s
spojkou	spojka	k1gFnSc7	spojka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vypnout	vypnout	k5eAaPmF	vypnout
pohon	pohon	k1gInSc1	pohon
druhé	druhý	k4xOgFnSc2	druhý
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prokluzu	prokluz	k1gInSc6	prokluz
kola	kolo	k1gNnSc2	kolo
je	být	k5eAaImIp3nS	být
znemožněn	znemožněn	k2eAgInSc1d1	znemožněn
přenos	přenos	k1gInSc1	přenos
hnací	hnací	k2eAgFnSc2d1	hnací
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zamezení	zamezení	k1gNnSc3	zamezení
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
slouží	sloužit	k5eAaImIp3nS	sloužit
uzávěrka	uzávěrka	k1gFnSc1	uzávěrka
diferenciálu	diferenciál	k1gInSc2	diferenciál
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
terénních	terénní	k2eAgInPc2d1	terénní
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciál	diferenciál	k1gInSc1	diferenciál
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
samosvorný	samosvorný	k2eAgMnSc1d1	samosvorný
(	(	kIx(	(
<g/>
obejde	obejít	k5eAaPmIp3nS	obejít
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
použita	použít	k5eAaPmNgNnP	použít
výrobně	výrobně	k6eAd1	výrobně
náročná	náročný	k2eAgNnPc1d1	náročné
šneková	šnekový	k2eAgNnPc1d1	šnekové
ozubená	ozubený	k2eAgNnPc1d1	ozubené
kola	kolo	k1gNnPc1	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
závěrky	závěrka	k1gFnSc2	závěrka
může	moct	k5eAaImIp3nS	moct
úspěšně	úspěšně	k6eAd1	úspěšně
suplovat	suplovat	k5eAaImF	suplovat
systém	systém	k1gInSc4	systém
EDL	EDL	kA	EDL
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
EDS	EDS	kA	EDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nadstavba	nadstavba	k1gFnSc1	nadstavba
elektronického	elektronický	k2eAgNnSc2d1	elektronické
řízení	řízení	k1gNnSc2	řízení
brzd	brzda	k1gFnPc2	brzda
ABS	ABS	kA	ABS
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnPc1d1	hnací
hřídele	hřídel	k1gFnPc1	hřídel
(	(	kIx(	(
<g/>
též	též	k9	též
poloosy	poloosa	k1gFnSc2	poloosa
<g/>
)	)	kIx)	)
přenášejí	přenášet	k5eAaImIp3nP	přenášet
točivý	točivý	k2eAgInSc4d1	točivý
moment	moment	k1gInSc4	moment
od	od	k7c2	od
rozvodovky	rozvodovka	k1gFnSc2	rozvodovka
na	na	k7c4	na
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
poháněna	poháněn	k2eAgFnSc1d1	poháněna
řiditelná	řiditelný	k2eAgFnSc1d1	řiditelná
náprava	náprava	k1gFnSc1	náprava
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hřídele	hřídel	k1gInPc4	hřídel
opatřeny	opatřen	k2eAgInPc4d1	opatřen
stejnoběžnými	stejnoběžný	k2eAgInPc7d1	stejnoběžný
klouby	kloub	k1gInPc7	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
klouby	kloub	k1gInPc4	kloub
(	(	kIx(	(
<g/>
u	u	k7c2	u
převodovky	převodovka	k1gFnSc2	převodovka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
stejnoběžné	stejnoběžný	k2eAgInPc1d1	stejnoběžný
klouby	kloub	k1gInPc1	kloub
typu	typ	k1gInSc2	typ
tripode	tripod	k1gMnSc5	tripod
a	a	k8xC	a
jako	jako	k9	jako
takové	takový	k3xDgFnPc1	takový
nepřenášejí	přenášet	k5eNaImIp3nP	přenášet
axiální	axiální	k2eAgFnSc4d1	axiální
sílu	síla	k1gFnSc4	síla
na	na	k7c4	na
ložiska	ložisko	k1gNnPc4	ložisko
rozvodovky	rozvodovka	k1gFnSc2	rozvodovka
<g/>
.	.	kIx.	.
</s>
<s>
Klouby	kloub	k1gInPc1	kloub
jsou	být	k5eAaImIp3nP	být
naplněny	naplnit	k5eAaPmNgFnP	naplnit
plastickým	plastický	k2eAgNnSc7d1	plastické
mazivem	mazivo	k1gNnSc7	mazivo
a	a	k8xC	a
zakryty	zakrýt	k5eAaPmNgFnP	zakrýt
prachovkami	prachovka	k1gFnPc7	prachovka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
prachovky	prachovka	k1gFnPc1	prachovka
brání	bránit	k5eAaImIp3nP	bránit
kloub	kloub	k1gInSc4	kloub
proti	proti	k7c3	proti
prachu	prach	k1gInSc3	prach
a	a	k8xC	a
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
prachovky	prachovka	k1gFnSc2	prachovka
dojde	dojít	k5eAaPmIp3nS	dojít
brzy	brzy	k6eAd1	brzy
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
drahého	drahý	k2eAgInSc2d1	drahý
kloubu	kloub	k1gInSc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
prachovky	prachovka	k1gFnPc1	prachovka
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
často	často	k6eAd1	často
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
výfukového	výfukový	k2eAgNnSc2d1	výfukové
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
mechanicky	mechanicky	k6eAd1	mechanicky
hodně	hodně	k6eAd1	hodně
namáhány	namáhat	k5eAaImNgFnP	namáhat
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
výměna	výměna	k1gFnSc1	výměna
není	být	k5eNaImIp3nS	být
snadná	snadný	k2eAgFnSc1d1	snadná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Homokinetické	Homokinetický	k2eAgInPc1d1	Homokinetický
klouby	kloub	k1gInPc1	kloub
jsou	být	k5eAaImIp3nP	být
výrobně	výrobně	k6eAd1	výrobně
náročné	náročný	k2eAgInPc1d1	náročný
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
produkuje	produkovat	k5eAaImIp3nS	produkovat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
známé	známý	k2eAgFnSc2d1	známá
značky	značka	k1gFnSc2	značka
Löbro	Löbro	k1gNnSc1	Löbro
(	(	kIx(	(
<g/>
Löhr	Löhr	k1gMnSc1	Löhr
&	&	k?	&
Bromkamp	Bromkamp	k1gMnSc1	Bromkamp
GmbH	GmbH	k1gMnSc1	GmbH
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
GKN	GKN	kA	GKN
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
slangový	slangový	k2eAgInSc4d1	slangový
název	název	k1gInSc4	název
pro	pro	k7c4	pro
hnací	hnací	k2eAgFnSc4d1	hnací
hřídel	hřídel	k1gFnSc4	hřídel
se	s	k7c7	s
stejnoběžným	stejnoběžný	k2eAgInSc7d1	stejnoběžný
kloubem	kloub	k1gInSc7	kloub
'	'	kIx"	'
<g/>
lebro	lebro	k6eAd1	lebro
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
ještě	ještě	k9	ještě
CIFAM	CIFAM	kA	CIFAM
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Cologne	Cologn	k1gInSc5	Cologn
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc4	součást
Metelli	Metell	k1gMnSc3	Metell
S.	S.	kA	S.
<g/>
p.	p.	k?	p.
<g/>
A.	A.	kA	A.
<g/>
)	)	kIx)	)
a	a	k8xC	a
značka	značka	k1gFnSc1	značka
KAMOKA	KAMOKA	kA	KAMOKA
<g/>
®	®	k?	®
zastupující	zastupující	k2eAgFnSc3d1	zastupující
produkci	produkce	k1gFnSc3	produkce
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Hong	Hong	k1gMnSc1	Hong
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
Dálného	dálný	k2eAgInSc2d1	dálný
Východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
běžných	běžný	k2eAgInPc2d1	běžný
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
poloosa	poloosa	k1gFnSc1	poloosa
pohání	pohánět	k5eAaImIp3nS	pohánět
přímo	přímo	k6eAd1	přímo
kolo	kolo	k1gNnSc1	kolo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
konstrukcí	konstrukce	k1gFnPc2	konstrukce
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
terénních	terénní	k2eAgNnPc2d1	terénní
vozidel	vozidlo	k1gNnPc2	vozidlo
poloosa	poloosa	k1gFnSc1	poloosa
pohání	pohánět	k5eAaImIp3nP	pohánět
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
ozubený	ozubený	k2eAgInSc4d1	ozubený
převod	převod	k1gInSc4	převod
umístěný	umístěný	k2eAgInSc4d1	umístěný
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
umístit	umístit	k5eAaPmF	umístit
poloosu	poloosa	k1gFnSc4	poloosa
výš	vysoce	k6eAd2	vysoce
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
samotná	samotný	k2eAgFnSc1d1	samotná
osa	osa	k1gFnSc1	osa
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
vozidlo	vozidlo	k1gNnSc1	vozidlo
má	mít	k5eAaImIp3nS	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
průchodnost	průchodnost	k1gFnSc4	průchodnost
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnPc1d1	hnací
hřídele	hřídel	k1gFnPc1	hřídel
také	také	k9	také
vycházejí	vycházet	k5eAaImIp3nP	vycházet
tenčí	tenký	k2eAgFnPc1d2	tenčí
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
automobilu	automobil	k1gInSc2	automobil
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
funkcí	funkce	k1gFnSc7	funkce
tempomat	tempomat	k5eAaPmF	tempomat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hnací	hnací	k2eAgFnSc6d1	hnací
soustavě	soustava	k1gFnSc6	soustava
plní	plnit	k5eAaImIp3nP	plnit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
maziva	mazivo	k1gNnSc2	mazivo
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
překonal	překonat	k5eAaPmAgInS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
elektromobil	elektromobil	k1gInSc1	elektromobil
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Jamais	Jamais	k1gFnSc2	Jamais
Contente	Content	k1gInSc5	Content
<g/>
"	"	kIx"	"
Camille	Camill	k1gMnSc5	Camill
Jenatzyho	Jenatzyha	k1gMnSc5	Jenatzyha
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
překonal	překonat	k5eAaPmAgInS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
parní	parní	k2eAgInSc1d1	parní
automobil	automobil	k1gInSc1	automobil
firmy	firma	k1gFnSc2	firma
Stanley	Stanlea	k1gFnSc2	Stanlea
Brothers	Brothers	k1gInSc1	Brothers
řízený	řízený	k2eAgInSc1d1	řízený
Fredem	Fred	k1gMnSc7	Fred
Marriottem	Marriott	k1gMnSc7	Marriott
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
1000	[number]	k4	1000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
překonal	překonat	k5eAaPmAgInS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
automobil	automobil	k1gInSc1	automobil
s	s	k7c7	s
raketovým	raketový	k2eAgInSc7d1	raketový
motorem	motor	k1gInSc7	motor
The	The	k1gMnSc2	The
Blue	Blu	k1gMnSc2	Blu
Flame	Flam	k1gInSc5	Flam
firmy	firma	k1gFnSc2	firma
Reaction	Reaction	k1gInSc1	Reaction
Dynamics	Dynamics	k1gInSc1	Dynamics
řízený	řízený	k2eAgInSc1d1	řízený
Gary	Gara	k1gFnSc2	Gara
Gabelichem	Gabelich	k1gInSc7	Gabelich
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
překonal	překonat	k5eAaPmAgMnS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
automobil	automobil	k1gInSc4	automobil
ThrustSSC	ThrustSSC	k1gFnSc2	ThrustSSC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
2008	[number]	k4	2008
celkově	celkově	k6eAd1	celkově
prudce	prudko	k6eAd1	prudko
klesla	klesnout	k5eAaPmAgFnS	klesnout
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
nových	nový	k2eAgInPc6d1	nový
automobilech	automobil	k1gInPc6	automobil
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
nejmenších	malý	k2eAgInPc6d3	nejmenší
vozech	vůz	k1gInPc6	vůz
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
o	o	k7c4	o
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
