<s>
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
(	(	kIx(
<g/>
Polná	Polná	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Lokalizace	lokalizace	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
41408	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5133	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
v	v	k7c6
Polné	Polná	k1gFnSc6
na	na	k7c6
Jihlavsku	Jihlavsko	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
města	město	k1gNnSc2
na	na	k7c6
výšině	výšina	k1gFnSc6
Kalvárie	Kalvárie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
čtyřmi	čtyři	k4xCgInPc7
většími	veliký	k2eAgInPc7d2
zděnými	zděný	k2eAgFnPc7d1
výklenkovými	výklenkový	k2eAgFnPc7d1
kapličkami	kaplička	k1gFnPc7
<g/>
,	,	kIx,
třemi	tři	k4xCgFnPc7
kříži	kříž	k1gInSc6
Kalvárie	Kalvárie	k1gFnSc2
a	a	k8xC
Božím	boží	k2eAgInSc7d1
hrobem	hrob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1894	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
místě	místo	k1gNnSc6
starší	starý	k2eAgFnSc2d2
dřevěné	dřevěný	k2eAgFnSc2d1
Kalvárie	Kalvárie	k1gFnSc2
postavena	postaven	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Roberta	Robert	k1gMnSc2
Niklíčka	Niklíček	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalvárie	Kalvárie	k1gFnSc1
se	s	k7c7
třemi	tři	k4xCgInPc7
kamennými	kamenný	k2eAgInPc7d1
kříži	kříž	k1gInPc7
a	a	k8xC
čtyřmi	čtyři	k4xCgFnPc7
kapličkami	kaplička	k1gFnPc7
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
zásluhou	zásluhou	k7c2
děkana	děkan	k1gMnSc2
Františka	František	k1gMnSc4
Pojmona	Pojmon	k1gMnSc4
<g/>
,	,	kIx,
rodáka	rodák	k1gMnSc4
z	z	k7c2
Polné	Polná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapličky	kaplička	k1gFnPc1
jsou	být	k5eAaImIp3nP
volně	volně	k6eAd1
rozestavěné	rozestavěný	k2eAgFnPc4d1
ve	v	k7c6
svahu	svah	k1gInSc6
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
obdélníkový	obdélníkový	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
a	a	k8xC
podezdívku	podezdívka	k1gFnSc4
<g/>
,	,	kIx,
ukončenou	ukončená	k1gFnSc4
soklovou	soklový	k2eAgFnSc4d1
římsou	římsa	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
všechny	všechen	k3xTgInPc1
mají	mít	k5eAaImIp3nP
klekátko	klekátko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celé	celý	k2eAgNnSc1d1
poutní	poutní	k2eAgNnSc1d1
místo	místo	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
opraveno	opraven	k2eAgNnSc1d1
svépomocí	svépomoc	k1gFnSc7
místními	místní	k2eAgMnPc7d1
občany	občan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
je	být	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Kalvárií	Kalvárie	k1gFnSc7
<g/>
,	,	kIx,
Božím	boží	k2eAgInSc7d1
hrobem	hrob	k1gInSc7
a	a	k8xC
pilířem	pilíř	k1gInSc7
s	s	k7c7
Nejsvětější	nejsvětější	k2eAgFnSc7d1
trojicí	trojice	k1gFnSc7
chráněna	chránit	k5eAaImNgFnS
jako	jako	k9
nemovitá	movitý	k2eNgFnSc1d1
Kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Polná	Polná	k1gFnSc1
<g/>
:	:	kIx,
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
a	a	k8xC
Boží	boží	k2eAgInSc1d1
hrob	hrob	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
1958	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
153518	#num#	k4
:	:	kIx,
křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Polná	Polnat	k5eAaPmIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Křížová	křížový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
