<s>
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
či	či	k8xC	či
fotoefekt	fotoefekt	k1gInSc1	fotoefekt
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
jsou	být	k5eAaImIp3nP	být
elektrony	elektron	k1gInPc1	elektron
uvolňovány	uvolňován	k2eAgInPc1d1	uvolňován
(	(	kIx(	(
<g/>
vyzařovány	vyzařován	k2eAgInPc1d1	vyzařován
<g/>
,	,	kIx,	,
emitovány	emitován	k2eAgInPc1d1	emitován
<g/>
)	)	kIx)	)
z	z	k7c2	z
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
)	)	kIx)	)
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
absorpce	absorpce	k1gFnSc2	absorpce
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
nebo	nebo	k8xC	nebo
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Emitované	emitovaný	k2eAgInPc1d1	emitovaný
elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
fotoelektrony	fotoelektron	k1gInPc1	fotoelektron
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
uvolňování	uvolňování	k1gNnSc1	uvolňování
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
fotoelektrická	fotoelektrický	k2eAgFnSc1d1	fotoelektrická
emise	emise	k1gFnSc1	emise
(	(	kIx(	(
<g/>
fotoemise	fotoemise	k1gFnSc1	fotoemise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jev	jev	k1gInSc1	jev
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
působením	působení	k1gNnSc7	působení
vnějšího	vnější	k2eAgNnSc2d1	vnější
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
elektrony	elektron	k1gInPc1	elektron
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
vnějším	vnější	k2eAgInSc6d1	vnější
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uvolněné	uvolněný	k2eAgInPc4d1	uvolněný
elektrony	elektron	k1gInPc4	elektron
látku	látka	k1gFnSc4	látka
neopouští	opouštět	k5eNaImIp3nS	opouštět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jako	jako	k8xC	jako
vodivostní	vodivostnit	k5eAaPmIp3nP	vodivostnit
elektrony	elektron	k1gInPc1	elektron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c4	na
látku	látka	k1gFnSc4	látka
dopadají	dopadat	k5eAaImIp3nP	dopadat
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vyzařování	vyzařování	k1gNnSc3	vyzařování
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
inverzním	inverzní	k2eAgMnSc6d1	inverzní
(	(	kIx(	(
<g/>
obráceném	obrácený	k2eAgInSc6d1	obrácený
<g/>
)	)	kIx)	)
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pochopení	pochopení	k1gNnSc4	pochopení
duality	dualita	k1gFnSc2	dualita
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
některých	některý	k3yIgFnPc2	některý
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
kovy	kov	k1gInPc1	kov
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
nabijí	nabít	k5eAaPmIp3nP	nabít
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
zinek	zinek	k1gInSc4	zinek
osvětlený	osvětlený	k2eAgInSc1d1	osvětlený
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
světlem	světlo	k1gNnSc7	světlo
se	se	k3xPyFc4	se
nabije	nabít	k5eAaBmIp3nS	nabít
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ozáření	ozáření	k1gNnSc6	ozáření
vzorku	vzorek	k1gInSc2	vzorek
spektrem	spektrum	k1gNnSc7	spektrum
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
byly	být	k5eAaImAgFnP	být
přitom	přitom	k6eAd1	přitom
pohlceny	pohlcen	k2eAgFnPc1d1	pohlcena
krátké	krátká	k1gFnPc1	krátká
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
a	a	k8xC	a
delší	dlouhý	k2eAgFnPc4d2	delší
vlny	vlna	k1gFnPc4	vlna
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
krátké	krátké	k1gNnSc4	krátké
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
emisi	emise	k1gFnSc3	emise
vodivostních	vodivostní	k2eAgInPc2d1	vodivostní
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
těchto	tento	k3xDgInPc2	tento
elektronů	elektron	k1gInPc2	elektron
rostl	růst	k5eAaImAgInS	růst
s	s	k7c7	s
intenzitou	intenzita	k1gFnSc7	intenzita
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
pozorován	pozorovat	k5eAaImNgInS	pozorovat
jen	jen	k9	jen
pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
velké	velká	k1gFnPc4	velká
délky	délka	k1gFnSc2	délka
vln	vlna	k1gFnPc2	vlna
jev	jev	k1gInSc1	jev
nenastal	nastat	k5eNaPmAgInS	nastat
při	při	k7c6	při
libovolné	libovolný	k2eAgFnSc6d1	libovolná
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
krátké	krátké	k1gNnSc4	krátké
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
se	se	k3xPyFc4	se
se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
intenzity	intenzita	k1gFnSc2	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
počet	počet	k1gInSc1	počet
uvolněných	uvolněný	k2eAgMnPc2d1	uvolněný
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
intenzita	intenzita	k1gFnSc1	intenzita
neovlivnila	ovlivnit	k5eNaPmAgFnS	ovlivnit
energii	energie	k1gFnSc4	energie
těchto	tento	k3xDgInPc2	tento
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
představ	představa	k1gFnPc2	představa
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
by	by	kYmCp3nS	by
elektronům	elektron	k1gInPc3	elektron
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
předána	předat	k5eAaPmNgFnS	předat
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
intenzitou	intenzita	k1gFnSc7	intenzita
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
energie	energie	k1gFnSc1	energie
vyzařovaných	vyzařovaný	k2eAgInPc2d1	vyzařovaný
elektronů	elektron	k1gInPc2	elektron
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
vyzařovaných	vyzařovaný	k2eAgInPc2d1	vyzařovaný
elektronů	elektron	k1gInPc2	elektron
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Experimentálně	experimentálně	k6eAd1	experimentálně
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
frekvence	frekvence	k1gFnSc1	frekvence
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
klesne	klesnout	k5eAaPmIp3nS	klesnout
pod	pod	k7c7	pod
tzv.	tzv.	kA	tzv.
mezní	mezní	k2eAgMnSc1d1	mezní
(	(	kIx(	(
<g/>
prahový	prahový	k2eAgInSc1d1	prahový
<g/>
)	)	kIx)	)
kmitočet	kmitočet	k1gInSc1	kmitočet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
fotoemise	fotoemise	k1gFnSc1	fotoemise
se	se	k3xPyFc4	se
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezní	mezní	k2eAgFnSc1d1	mezní
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
vlastností	vlastnost	k1gFnSc7	vlastnost
každé	každý	k3xTgFnSc2	každý
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
frekvence	frekvence	k1gFnSc1	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
mezní	mezní	k2eAgFnSc2d1	mezní
frekvence	frekvence	k1gFnSc2	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
fotoelektrony	fotoelektron	k1gInPc7	fotoelektron
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
maximální	maximální	k2eAgFnSc2d1	maximální
hodnoty	hodnota	k1gFnSc2	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hodnota	hodnota	k1gFnSc1	hodnota
energie	energie	k1gFnSc2	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgFnSc7d1	lineární
funkcí	funkce	k1gFnSc7	funkce
frekvence	frekvence	k1gFnSc2	frekvence
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
h	h	k?	h
(	(	kIx(	(
ν	ν	k?	ν
-	-	kIx~	-
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
h	h	k?	h
ν	ν	k?	ν
-	-	kIx~	-
h	h	k?	h
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
nu	nu	k9	nu
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
-h	-h	k?	-h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
není	být	k5eNaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
fyzika	fyzika	k1gFnSc1	fyzika
schopná	schopný	k2eAgFnSc1d1	schopná
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Podivné	podivný	k2eAgNnSc1d1	podivné
chování	chování	k1gNnSc1	chování
světla	světlo	k1gNnSc2	světlo
při	při	k7c6	při
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
vlněním	vlnění	k1gNnSc7	vlnění
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
až	až	k6eAd1	až
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
poznatků	poznatek	k1gInPc2	poznatek
právě	právě	k9	právě
se	se	k3xPyFc4	se
rodící	rodící	k2eAgFnSc2d1	rodící
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
Planckem	Planck	k1gInSc7	Planck
prezentovaná	prezentovaný	k2eAgFnSc1d1	prezentovaná
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
vlnění	vlnění	k1gNnSc1	vlnění
předává	předávat	k5eAaImIp3nS	předávat
svou	svůj	k3xOyFgFnSc4	svůj
energii	energie	k1gFnSc4	energie
při	při	k7c6	při
interakcích	interakce	k1gFnPc6	interakce
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
částicemi	částice	k1gFnPc7	částice
nespojitě	spojitě	k6eNd1	spojitě
<g/>
,	,	kIx,	,
po	po	k7c6	po
takzvaných	takzvaný	k2eAgNnPc6d1	takzvané
kvantech	kvantum	k1gNnPc6	kvantum
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
kvanta	kvantum	k1gNnSc2	kvantum
energie	energie	k1gFnSc2	energie
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
(	(	kIx(	(
<g/>
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
)	)	kIx)	)
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
h	h	k?	h
ν	ν	k?	ν
=	=	kIx~	=
ħ	ħ	k?	ħ
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
omega	omega	k1gNnSc1	omega
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
frekvence	frekvence	k1gFnSc1	frekvence
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
=	=	kIx~	=
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
π	π	k?	π
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc6	omega
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
kruhová	kruhový	k2eAgFnSc1d1	kruhová
frekvence	frekvence	k1gFnSc1	frekvence
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
=	=	kIx~	=
h	h	k?	h
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
2	[number]	k4	2
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbara	k1gFnPc2	hbara
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
kvantum	kvantum	k1gNnSc4	kvantum
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc1	název
foton	foton	k1gInSc1	foton
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
předává	předávat	k5eAaImIp3nS	předávat
energii	energie	k1gFnSc4	energie
elektronům	elektron	k1gInPc3	elektron
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zkoumané	zkoumaný	k2eAgFnSc2d1	zkoumaná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc2	lambda
}	}	kIx)	}
světla	světlo	k1gNnSc2	světlo
dostatečně	dostatečně	k6eAd1	dostatečně
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
frekvence	frekvence	k1gFnSc1	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
h	h	k?	h
ν	ν	k?	ν
=	=	kIx~	=
h	h	k?	h
c	c	k0	c
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
hc	hc	k?	hc
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
záření	záření	k1gNnSc1	záření
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
předá	předat	k5eAaPmIp3nS	předat
elektronu	elektron	k1gInSc3	elektron
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
hodnoty	hodnota	k1gFnPc4	hodnota
pro	pro	k7c4	pro
uvolnění	uvolnění	k1gNnSc4	uvolnění
tohoto	tento	k3xDgInSc2	tento
elektronu	elektron	k1gInSc2	elektron
z	z	k7c2	z
vazby	vazba	k1gFnSc2	vazba
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
této	tento	k3xDgFnSc2	tento
energie	energie	k1gFnSc2	energie
potřebné	potřebný	k2eAgFnSc2d1	potřebná
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
elektronu	elektron	k1gInSc2	elektron
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
ionizační	ionizační	k2eAgFnSc1d1	ionizační
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
ionizační	ionizační	k2eAgFnSc2d1	ionizační
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
elektrony	elektron	k1gInPc1	elektron
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
z	z	k7c2	z
látky	látka	k1gFnSc2	látka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
fotoelektrická	fotoelektrický	k2eAgFnSc1d1	fotoelektrická
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Předáním	předání	k1gNnSc7	předání
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
energie	energie	k1gFnSc2	energie
elektronům	elektron	k1gInPc3	elektron
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tuto	tento	k3xDgFnSc4	tento
bariéru	bariéra	k1gFnSc4	bariéra
překonat	překonat	k5eAaPmF	překonat
(	(	kIx(	(
<g/>
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
výstupní	výstupní	k2eAgFnSc6d1	výstupní
práci	práce	k1gFnSc6	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dopadající	dopadající	k2eAgInPc1d1	dopadající
fotony	foton	k1gInPc4	foton
předávají	předávat	k5eAaImIp3nP	předávat
elektronům	elektron	k1gInPc3	elektron
energii	energie	k1gFnSc4	energie
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
této	tento	k3xDgFnSc2	tento
bariéry	bariéra	k1gFnSc2	bariéra
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
prahová	prahový	k2eAgFnSc1d1	prahová
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkých	velký	k2eAgFnPc6d1	velká
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
(	(	kIx(	(
<g/>
nízkých	nízký	k2eAgFnPc6d1	nízká
frekvencích	frekvence	k1gFnPc6	frekvence
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k8xC	i
energiích	energie	k1gFnPc6	energie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jev	jev	k1gInSc1	jev
neprojeví	projevit	k5eNaPmIp3nS	projevit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
energie	energie	k1gFnSc1	energie
fotonu	foton	k1gInSc2	foton
nestačí	stačit	k5eNaBmIp3nS	stačit
na	na	k7c4	na
uvolnění	uvolnění	k1gNnSc4	uvolnění
elektronu	elektron	k1gInSc2	elektron
z	z	k7c2	z
obalu	obal	k1gInSc2	obal
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
předaná	předaný	k2eAgFnSc1d1	předaná
elektronu	elektron	k1gInSc2	elektron
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
uvolnění	uvolnění	k1gNnSc4	uvolnění
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
výstupní	výstupní	k2eAgFnSc2d1	výstupní
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
fotoelektronu	fotoelektron	k1gInSc2	fotoelektron
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
látky	látka	k1gFnSc2	látka
část	část	k1gFnSc1	část
energie	energie	k1gFnSc2	energie
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
má	mít	k5eAaImIp3nS	mít
formu	forma	k1gFnSc4	forma
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
úvah	úvaha	k1gFnPc2	úvaha
získal	získat	k5eAaPmAgMnS	získat
Einstein	Einstein	k1gMnSc1	Einstein
rovnici	rovnice	k1gFnSc4	rovnice
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
ν	ν	k?	ν
=	=	kIx~	=
h	h	k?	h
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
dopadajícího	dopadající	k2eAgInSc2d1	dopadající
fotonu	foton	k1gInSc2	foton
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgFnSc1d1	minimální
energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
elektronu	elektron	k1gInSc2	elektron
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
výstupní	výstupní	k2eAgFnSc2d1	výstupní
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
možná	možný	k2eAgFnSc1d1	možná
energie	energie	k1gFnSc1	energie
uvolněného	uvolněný	k2eAgInSc2d1	uvolněný
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uvedené	uvedený	k2eAgFnSc2d1	uvedená
rovnice	rovnice	k1gFnSc2	rovnice
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
uvolněného	uvolněný	k2eAgInSc2d1	uvolněný
elektronu	elektron	k1gInSc2	elektron
závisí	záviset	k5eAaImIp3nS	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
intenzitu	intenzita	k1gFnSc4	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
nemůže	moct	k5eNaImIp3nS	moct
při	při	k7c6	při
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
<	<	kIx(	<
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
fotoefektu	fotoefekt	k1gInSc3	fotoefekt
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
mohou	moct	k5eAaImIp3nP	moct
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
detektoru	detektor	k1gInSc3	detektor
prodělat	prodělat	k5eAaPmF	prodělat
neelastickou	elastický	k2eNgFnSc4d1	neelastická
srážku	srážka	k1gFnSc4	srážka
a	a	k8xC	a
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
energie	energie	k1gFnSc2	energie
ztratit	ztratit	k5eAaPmF	ztratit
(	(	kIx(	(
<g/>
zvlášť	zvlášť	k6eAd1	zvlášť
elektrony	elektron	k1gInPc4	elektron
vyražené	vyražený	k2eAgInPc4d1	vyražený
z	z	k7c2	z
vnitřku	vnitřek	k1gInSc2	vnitřek
látky	látka	k1gFnSc2	látka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gFnSc1	jejich
energie	energie	k1gFnSc1	energie
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
do	do	k7c2	do
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
intenzita	intenzita	k1gFnSc1	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
uvolněných	uvolněný	k2eAgMnPc2d1	uvolněný
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
větší	veliký	k2eAgFnSc6d2	veliký
intenzitě	intenzita	k1gFnSc6	intenzita
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
také	také	k9	také
počet	počet	k1gInSc1	počet
uvolněných	uvolněný	k2eAgInPc2d1	uvolněný
elektronů	elektron	k1gInPc2	elektron
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c4	na
látku	látka	k1gFnSc4	látka
dopadají	dopadat	k5eAaImIp3nP	dopadat
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vyzařování	vyzařování	k1gNnSc3	vyzařování
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
inverzním	inverzní	k2eAgMnSc6d1	inverzní
(	(	kIx(	(
<g/>
obráceném	obrácený	k2eAgInSc6d1	obrácený
<g/>
)	)	kIx)	)
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
potenciálová	potenciálový	k2eAgFnSc1d1	potenciálová
hráz	hráz	k1gFnSc1	hráz
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
hodnotu	hodnota	k1gFnSc4	hodnota
výstupní	výstupní	k2eAgFnSc2d1	výstupní
práce	práce	k1gFnSc2	práce
zanedbat	zanedbat	k5eAaPmF	zanedbat
proti	proti	k7c3	proti
kinetické	kinetický	k2eAgFnSc3d1	kinetická
energii	energie	k1gFnSc3	energie
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
h	h	k?	h
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gFnPc7	E_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
:	:	kIx,	:
Při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
elektronu	elektron	k1gInSc2	elektron
na	na	k7c4	na
kov	kov	k1gInSc4	kov
dochází	docházet	k5eAaImIp3nS	docházet
obvykle	obvykle	k6eAd1	obvykle
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
jeho	jeho	k3xOp3gFnSc2	jeho
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
několika	několik	k4yIc7	několik
srážkami	srážka	k1gFnPc7	srážka
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
postupně	postupně	k6eAd1	postupně
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
svoji	svůj	k3xOyFgFnSc4	svůj
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
elektrony	elektron	k1gInPc1	elektron
však	však	k9	však
všechnu	všechen	k3xTgFnSc4	všechen
svoji	svůj	k3xOyFgFnSc4	svůj
energii	energie	k1gFnSc4	energie
ztratí	ztratit	k5eAaPmIp3nS	ztratit
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
nárazu	náraz	k1gInSc6	náraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
všechna	všechen	k3xTgFnSc1	všechen
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
elektronu	elektron	k1gInSc2	elektron
může	moct	k5eAaImIp3nS	moct
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c6	v
částici	částice	k1gFnSc6	částice
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
ve	v	k7c4	v
foton	foton	k1gInSc4	foton
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
fotony	foton	k1gInPc4	foton
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
energie	energie	k1gFnSc2	energie
elektronu	elektron	k1gInSc2	elektron
ve	v	k7c6	v
foton	foton	k1gInSc1	foton
jedním	jeden	k4xCgInSc7	jeden
nárazem	náraz	k1gInSc7	náraz
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
fotonu	foton	k1gInSc2	foton
dodáno	dodán	k2eAgNnSc1d1	dodáno
největší	veliký	k2eAgNnSc1d3	veliký
možné	možný	k2eAgNnSc1d1	možné
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
h	h	k?	h
:	:	kIx,	:
ν	ν	k?	ν
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
e	e	k0	e
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc6	E_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
eU	eU	k?	eU
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
elektronu	elektron	k1gInSc2	elektron
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
urychlující	urychlující	k2eAgInSc4d1	urychlující
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřením	vyjádření	k1gNnSc7	vyjádření
pomocí	pomocí	k7c2	pomocí
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
získáme	získat	k5eAaPmIp1nP	získat
tzv.	tzv.	kA	tzv.
Duane-Huntův	Duane-Huntův	k2eAgInSc4d1	Duane-Huntův
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
min	mina	k1gFnPc2	mina
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
c	c	k0	c
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
konst	konst	k5eAaPmF	konst
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}}	}}	k?	}}
<g/>
U	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
hc	hc	k?	hc
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
konst	konst	k1gInSc1	konst
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zvyšováním	zvyšování	k1gNnSc7	zvyšování
urychlujícího	urychlující	k2eAgInSc2d1	urychlující
potenciálu	potenciál	k1gInSc2	potenciál
se	se	k3xPyFc4	se
maximum	maximum	k1gNnSc1	maximum
energie	energie	k1gFnSc2	energie
posouvá	posouvat	k5eAaImIp3nS	posouvat
ke	k	k7c3	k
kratším	krátký	k2eAgFnPc3d2	kratší
vlnovým	vlnový	k2eAgFnPc3d1	vlnová
délkám	délka	k1gFnPc3	délka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
experimentálně	experimentálně	k6eAd1	experimentálně
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
Heinrich	Heinrich	k1gMnSc1	Heinrich
Hertz	hertz	k1gInSc4	hertz
<g/>
.	.	kIx.	.
</s>
<s>
Pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
fyziky	fyzika	k1gFnSc2	fyzika
nevysvětlitelné	vysvětlitelný	k2eNgNnSc4d1	nevysvětlitelné
chování	chování	k1gNnSc4	chování
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Kvantové	kvantový	k2eAgNnSc4d1	kvantové
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
za	za	k7c4	za
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
a	a	k8xC	a
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
teoretické	teoretický	k2eAgFnSc3d1	teoretická
fyzice	fyzika	k1gFnSc3	fyzika
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vedla	vést	k5eAaImAgFnS	vést
spíše	spíše	k9	spíše
politická	politický	k2eAgFnSc1d1	politická
pohnutka	pohnutka	k1gFnSc1	pohnutka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
nebyla	být	k5eNaImAgFnS	být
ještě	ještě	k9	ještě
všeobecně	všeobecně	k6eAd1	všeobecně
přijatá	přijatý	k2eAgFnSc1d1	přijatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
komise	komise	k1gFnSc1	komise
obdařila	obdařit	k5eAaPmAgFnS	obdařit
Einsteina	Einstein	k1gMnSc4	Einstein
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
dílčí	dílčí	k2eAgInSc4d1	dílčí
obecně	obecně	k6eAd1	obecně
přijatý	přijatý	k2eAgInSc4d1	přijatý
výsledek	výsledek	k1gInSc4	výsledek
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
přidala	přidat	k5eAaPmAgFnS	přidat
komentář	komentář	k1gInSc4	komentář
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
o	o	k7c4	o
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
snad	snad	k9	snad
na	na	k7c4	na
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
něco	něco	k6eAd1	něco
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využití	využití	k1gNnSc4	využití
solární	solární	k2eAgFnSc2d1	solární
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
fotočlánků	fotočlánek	k1gInPc2	fotočlánek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
fotodiody	fotodioda	k1gFnSc2	fotodioda
nebo	nebo	k8xC	nebo
fototranzistoru	fototranzistor	k1gInSc2	fototranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
našel	najít	k5eAaPmAgInS	najít
uplatnění	uplatnění	k1gNnSc3	uplatnění
především	především	k9	především
i	i	k9	i
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
citlivých	citlivý	k2eAgInPc2d1	citlivý
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
v	v	k7c6	v
polovodičích	polovodič	k1gInPc6	polovodič
elektrony	elektron	k1gInPc1	elektron
z	z	k7c2	z
atomových	atomový	k2eAgInPc2d1	atomový
orbitalů	orbital	k1gInPc2	orbital
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
jako	jako	k8xS	jako
nosiče	nosič	k1gInPc4	nosič
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Fotodiody	fotodioda	k1gFnPc1	fotodioda
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
například	například	k6eAd1	například
v	v	k7c6	v
solárních	solární	k2eAgFnPc6d1	solární
kalkulačkách	kalkulačka	k1gFnPc6	kalkulačka
<g/>
.	.	kIx.	.
</s>
<s>
Foton	foton	k1gInSc1	foton
Elektron	elektron	k1gInSc1	elektron
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
Fotovoltaický	fotovoltaický	k2eAgInSc1d1	fotovoltaický
článek	článek	k1gInSc1	článek
Fotočlánek	fotočlánek	k1gInSc1	fotočlánek
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
[	[	kIx(	[
<g/>
http://www.math.muni.cz/~xokrajek/fotoefekt.pdf	[url]	k1gInSc1	http://www.math.muni.cz/~xokrajek/fotoefekt.pdf
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
<g/>
]	]	kIx)	]
seminární	seminární	k2eAgFnSc1d1	seminární
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Bc.	Bc.	k1gFnSc1	Bc.
Petr	Petr	k1gMnSc1	Petr
Okrajek	okrajek	k1gInSc1	okrajek
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
Fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
</s>
