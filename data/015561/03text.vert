<s>
Jaromír	Jaromír	k1gMnSc1
Plocek	Plocka	k1gFnPc2
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
PlocekOsobní	PlocekOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Plocek	Plocka	k1gFnPc2
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1974	#num#	k4
(	(	kIx(
<g/>
46	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
174	#num#	k4
cm	cm	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
70	#num#	k4
kg	kg	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
záložník	záložník	k1gMnSc1
Mládežnické	mládežnický	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
1980-1992	1980-1992	k4
</s>
<s>
VCHZ	VCHZ	kA
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1993-19961996-19981998-20012001-200320032003-20082008-20112011-2012FC	1993-19961996-19981998-20012001-200320032003-20082008-20112011-2012FC	k4
Viktoria	Viktoria	k1gFnSc1
PlzeňAFK	PlzeňAFK	k1gFnSc2
Atlantic	Atlantice	k1gFnPc2
Lázně	lázeň	k1gFnSc2
BohdanečFK	BohdanečFK	k1gFnSc1
Viktoria	Viktoria	k1gFnSc1
ŽižkovSK	ŽižkovSK	k1gFnSc2
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
MŠK	MŠK	kA
ŽilinaSK	ŽilinaSK	k1gFnSc7
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
SC	SC	kA
Sparkasse	Sparkass	k1gMnSc2
Zwettl	Zwettl	k1gMnSc2
SV	sv	kA
Waidhofen	Waidhofen	k1gInSc1
<g/>
/	/	kIx~
<g/>
Thaya	Thaya	k1gFnSc1
<g/>
51	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
37	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
57	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
61	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
105	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Plocek	Plocka	k1gFnPc2
(	(	kIx(
<g/>
*	*	kIx~
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1974	#num#	k4
Pardubice	Pardubice	k1gInPc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
záložník	záložník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
otec	otec	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Plocek	Plocko	k1gNnPc2
byl	být	k5eAaImAgMnS
ligový	ligový	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
Spartaku	Spartak	k1gInSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Začínal	začínat	k5eAaImAgInS
v	v	k7c6
rodných	rodný	k2eAgInPc6d1
Pardubicích	Pardubice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
hrál	hrát	k5eAaImAgMnS
za	za	k7c2
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
AFK	AFK	kA
Atlantic	Atlantice	k1gFnPc2
Lázně	lázeň	k1gFnPc1
Bohdaneč	Bohdaneč	k1gInSc1
<g/>
,	,	kIx,
FK	FK	kA
Viktoria	Viktoria	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
<g/>
,	,	kIx,
SK	Sk	kA
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
MŠK	MŠK	kA
Žilina	Žilina	k1gFnSc1
<g/>
,	,	kIx,
SC	SC	kA
Sparkasse	Sparkasse	k1gFnSc1
Zwettl	Zwettl	k1gFnSc2
a	a	k8xC
hráčskou	hráčský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
končil	končit	k5eAaImAgInS
v	v	k7c6
SV	sv	kA
Waidhofen	Waidhofen	k1gInSc4
<g/>
/	/	kIx~
<g/>
Thaya	Thaya	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
české	český	k2eAgFnSc6d1
lize	liga	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
248	#num#	k4
utkáních	utkání	k1gNnPc6
a	a	k8xC
dal	dát	k5eAaPmAgMnS
4	#num#	k4
góly	gól	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
nastoupil	nastoupit	k5eAaPmAgMnS
ve	v	k7c6
2	#num#	k4
utkáních	utkání	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
transfermarkt	transfermarkt	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
</s>
<s>
Worldfootball	Worldfootball	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c6
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
SK	Sk	kA
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Eurofotbal	Eurofotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
