<s>
Ertar	Ertar	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
READERCONu	READERCONus	k1gInSc2
</s>
<s>
Ergea	Ergea	k1gFnSc1
-	-	kIx~
svět	svět	k1gInSc1
Ertaru	Ertar	k1gInSc2
(	(	kIx(
<g/>
Stát	stát	k1gInSc1
Republika	republika	k1gFnSc1
Ertar	Ertar	k1gMnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
zobrazené	zobrazený	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
velmi	velmi	k6eAd1
přibližně	přibližně	k6eAd1
uprostřed	uprostřed	k7c2
obrázku	obrázek	k1gInSc2
na	na	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
kontinentu	kontinent	k1gInSc2
Taros	Tarosa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Ertar	Ertar	k1gMnSc1
je	být	k5eAaImIp3nS
sci-fi	sci-fi	k1gFnSc3
projekt	projekt	k1gInSc4
zabývající	zabývající	k2eAgInSc4d1
se	se	k3xPyFc4
alternativní	alternativní	k2eAgFnSc7d1
realitou	realita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popisuje	popisovat	k5eAaImIp3nS
pokročilou	pokročilý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
především	především	k6eAd1
vědecky	vědecky	k6eAd1
a	a	k8xC
technologicky	technologicky	k6eAd1
v	v	k7c6
předstihu	předstih	k1gInSc6
před	před	k7c7
sousedními	sousední	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
naší	náš	k3xOp1gFnSc7
realitou	realita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledá	hledat	k5eAaImIp3nS
kořeny	kořen	k1gInPc4
a	a	k8xC
důsledky	důsledek	k1gInPc4
takového	takový	k3xDgInSc2
vývoje	vývoj	k1gInSc2
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
pokud	pokud	k8xS
možno	možno	k6eAd1
zábavnou	zábavný	k2eAgFnSc7d1
formou	forma	k1gFnSc7
prezentovat	prezentovat	k5eAaBmF
jevy	jev	k1gInPc4
vzniklé	vzniklý	k2eAgInPc4d1
vazbami	vazba	k1gFnPc7
na	na	k7c4
prvoplánový	prvoplánový	k2eAgInSc4d1
pozitivní	pozitivní	k2eAgInSc4d1
vědeckotechnický	vědeckotechnický	k2eAgInSc4d1
pokrok	pokrok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
skupinou	skupina	k1gFnSc7
nadšenců	nadšenec	k1gMnPc2
pro	pro	k7c4
sci-fi	sci-fi	k1gFnSc4
vedenou	vedený	k2eAgFnSc4d1
Daliborem	Dalibor	k1gMnSc7
Čápem	Čáp	k1gMnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Téma	téma	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
při	při	k7c6
práci	práce	k1gFnSc6
na	na	k7c6
neoficiálním	neoficiální	k2eAgNnSc6d1,k2eNgNnSc6d1
fanzinu	fanzino	k1gNnSc6
Světelné	světelný	k2eAgInPc4d1
roky	rok	k1gInPc4
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
posláním	poslání	k1gNnSc7
byla	být	k5eAaImAgFnS
vzájemná	vzájemný	k2eAgFnSc1d1
informovanost	informovanost	k1gFnSc1
o	o	k7c4
SF	SF	kA
filmech	film	k1gInPc6
<g/>
,	,	kIx,
především	především	k9
o	o	k7c6
těch	ten	k3xDgInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
někomu	někdo	k3yInSc3
podařilo	podařit	k5eAaPmAgNnS
zhlédnout	zhlédnout	k5eAaPmF
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
se	se	k3xPyFc4
začala	začít	k5eAaPmAgNnP
konat	konat	k5eAaImF
pravidelná	pravidelný	k2eAgNnPc1d1
setkání	setkání	k1gNnPc1
čtenářů	čtenář	k1gMnPc2
fanzinu	fanzin	k1gInSc2
(	(	kIx(
<g/>
READERCON	READERCON	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
listopadu	listopad	k1gInSc6
1989	#num#	k4
(	(	kIx(
<g/>
Sametová	sametový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
)	)	kIx)
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
SF	SF	kA
filmy	film	k1gInPc4
v	v	k7c6
Československu	Československo	k1gNnSc6
dostupnější	dostupný	k2eAgFnPc1d2
a	a	k8xC
klub	klub	k1gInSc1
se	se	k3xPyFc4
zaměřil	zaměřit	k5eAaPmAgInS
na	na	k7c4
širší	široký	k2eAgNnSc4d2
spektrum	spektrum	k1gNnSc4
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
filmový	filmový	k2eAgInSc1d1
fanzin	fanzin	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
i	i	k9
nadále	nadále	k6eAd1
<g/>
,	,	kIx,
klub	klub	k1gInSc1
se	se	k3xPyFc4
soustřeďuje	soustřeďovat	k5eAaImIp3nS
hlavně	hlavně	k9
na	na	k7c4
vytváření	vytváření	k1gNnSc4
komplexního	komplexní	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
paralelního	paralelní	k2eAgInSc2d1
světa	svět	k1gInSc2
Ertaru	Ertar	k1gInSc2
</s>
<s>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
pořádají	pořádat	k5eAaImIp3nP
speciální	speciální	k2eAgNnPc1d1
setkání	setkání	k1gNnPc1
(	(	kIx(
<g/>
ERCON	ERCON	kA
<g/>
)	)	kIx)
a	a	k8xC
novoroční	novoroční	k2eAgFnPc4d1
oslavy	oslava	k1gFnPc4
(	(	kIx(
<g/>
KVIDO	Kvido	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
účastníci	účastník	k1gMnPc1
přidávají	přidávat	k5eAaImIp3nP
další	další	k2eAgInPc4d1
kaménky	kamének	k1gInPc4
do	do	k7c2
mozaiky	mozaika	k1gFnSc2
tohoto	tento	k3xDgInSc2
paralelního	paralelní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Aktivity	aktivita	k1gFnPc1
(	(	kIx(
<g/>
ertologie	ertologie	k1gFnPc1
<g/>
)	)	kIx)
spočívají	spočívat	k5eAaImIp3nP
ve	v	k7c6
zprávách	zpráva	k1gFnPc6
o	o	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
paralelního	paralelní	k2eAgInSc2d1
světa	svět	k1gInSc2
Ertaru	Ertar	k1gInSc2
na	na	k7c6
planetě	planeta	k1gFnSc6
Ergea	Erge	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
podloženy	podložit	k5eAaPmNgInP
„	„	k?
<g/>
přivezenými	přivezený	k2eAgFnPc7d1
<g/>
“	“	k?
artefakty	artefakt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
klubu	klub	k1gInSc2
se	se	k3xPyFc4
také	také	k9
účastňují	účastňovat	k5eAaImIp3nP
jiných	jiný	k2eAgInPc2d1
SF	SF	kA
setkání	setkání	k1gNnSc2
a	a	k8xC
přiměřeně	přiměřeně	k6eAd1
propagují	propagovat	k5eAaImIp3nP
ideu	idea	k1gFnSc4
Ertaru	Ertar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
čestným	čestný	k2eAgMnPc3d1
členům	člen	k1gMnPc3
klubu	klub	k1gInSc2
patřil	patřit	k5eAaImAgMnS
známý	známý	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
písničkář	písničkář	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Velinský	velinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
písně	píseň	k1gFnPc1
se	se	k3xPyFc4
s	s	k7c7
jeho	jeho	k3xOp3gInSc7
souhlasem	souhlas	k1gInSc7
používaly	používat	k5eAaImAgFnP
při	při	k7c6
„	„	k?
<g/>
restauraci	restaurace	k1gFnSc6
<g/>
“	“	k?
některých	některý	k3yIgFnPc2
ertarských	ertarský	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zeměpis	zeměpis	k1gInSc1
a	a	k8xC
historie	historie	k1gFnSc1
</s>
<s>
Republika	republika	k1gFnSc1
Ertar	Ertara	k1gFnPc2
(	(	kIx(
<g/>
rozloha	rozloha	k1gFnSc1
593	#num#	k4
750	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
prostírá	prostírat	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
střední	střední	k2eAgFnSc2d1
části	část	k1gFnSc2
kontinentu	kontinent	k1gInSc2
Táros	Tárosa	k1gFnPc2
v	v	k7c6
podobě	podoba	k1gFnSc6
téměř	téměř	k6eAd1
pravidelného	pravidelný	k2eAgInSc2d1
obdélníku	obdélník	k1gInSc2
protáhlého	protáhlý	k2eAgInSc2d1
směrem	směr	k1gInSc7
k	k	k7c3
severu	sever	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obklíčena	obklíčen	k2eAgFnSc1d1
jest	být	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
pohořím	pohořet	k5eAaPmIp1nS
Dunaris	Dunaris	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
vybíhají	vybíhat	k5eAaImIp3nP
vrchy	vrch	k1gInPc1
pásma	pásmo	k1gNnSc2
Alkaratan	Alkaratan	k1gInSc1
a	a	k8xC
zasahují	zasahovat	k5eAaImIp3nP
asi	asi	k9
do	do	k7c2
třetiny	třetina	k1gFnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
krajina	krajina	k1gFnSc1
k	k	k7c3
moři	moře	k1gNnSc3
<g/>
,	,	kIx,
ke	k	k7c3
břehům	břeh	k1gInPc3
Sargasového	sargasový	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
odděluje	oddělovat	k5eAaImIp3nS
čtvrtinu	čtvrtina	k1gFnSc4
země	zem	k1gFnSc2
řeka	řeka	k1gFnSc1
Diana	Diana	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgInSc7d1
tokem	tok	k1gInSc7
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
význačnou	význačný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
kraj	kraj	k1gInSc1
kolem	kolem	k7c2
řeky	řeka	k1gFnSc2
Rio	Rio	k1gFnPc1
Quark	Quark	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kmen	kmen	k1gInSc1
Ertů	Ert	k1gInPc2
podle	podle	k7c2
pověstí	pověst	k1gFnPc2
přišel	přijít	k5eAaPmAgInS
z	z	k7c2
jihu	jih	k1gInSc2
a	a	k8xC
usadil	usadit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
úrodné	úrodný	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
kolem	kolem	k7c2
Sargasového	sargasový	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
v	v	k7c6
prvním	první	k4xOgNnSc6
tisíciletí	tisíciletí	k1gNnSc6
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Následovala	následovat	k5eAaImAgFnS
Doba	doba	k1gFnSc1
knížat	kníže	k1gMnPc2wR
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
až	až	k9
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Doba	doba	k1gFnSc1
králů	král	k1gMnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Doba	doba	k1gFnSc1
cizí	cizí	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
až	až	k9
9	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
a	a	k8xC
Nová	nový	k2eAgFnSc1d1
doba	doba	k1gFnSc1
královská	královský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
až	až	k9
17	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
začínají	začínat	k5eAaImIp3nP
v	v	k7c6
roce	rok	k1gInSc6
1698	#num#	k4
ustanovením	ustanovení	k1gNnSc7
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
a	a	k8xC
politické	politický	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
</s>
<s>
Celkem	celkem	k6eAd1
10	#num#	k4
186	#num#	k4
300	#num#	k4
obyvatel	obyvatel	k1gMnPc2
/	/	kIx~
<g/>
údaj	údaj	k1gInSc4
k	k	k7c3
30.12	30.12	k4
<g/>
.1987	.1987	k4
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národnostní	národnostní	k2eAgInSc4d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
Ertové	Ertové	k2eAgFnSc1d1
86	#num#	k4
<g/>
,	,	kIx,
Tulánci	Tulánec	k1gMnPc7
8	#num#	k4
<g/>
,	,	kIx,
Tegové	Tegové	k2eAgFnSc1d1
1	#num#	k4
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInSc1d1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ertar	Ertar	k1gInSc1
je	být	k5eAaImIp3nS
republikou	republika	k1gFnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Koordinátorem	koordinátor	k1gMnSc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
8	#num#	k4
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
celkem	celkem	k6eAd1
na	na	k7c4
47	#num#	k4
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Věda	věda	k1gFnSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
</s>
<s>
Znak	znak	k1gInSc1
Ertaru	Ertar	k1gInSc2
</s>
<s>
První	první	k4xOgFnSc1
READERCON	READERCON	kA
Světelných	světelný	k2eAgInPc2d1
roků	rok	k1gInPc2
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
V	v	k7c6
Ertaru	Ertar	k1gInSc6
se	se	k3xPyFc4
důsledně	důsledně	k6eAd1
používá	používat	k5eAaImIp3nS
desetinný	desetinný	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
měření	měření	k1gNnSc4
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
má	mít	k5eAaImIp3nS
10	#num#	k4
sekcí	sekce	k1gFnPc2
dělených	dělený	k2eAgFnPc2d1
na	na	k7c6
100	#num#	k4
period	perioda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
period	perioda	k1gFnPc2
má	mít	k5eAaImIp3nS
100	#num#	k4
pulsů	puls	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
puls	puls	k1gInSc1
=	=	kIx~
cca	cca	kA
0,8	0,8	k4
sec	sec	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
délková	délkový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
1	#num#	k4
yankabal	yankabal	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1	#num#	k4
anglickému	anglický	k2eAgInSc3d1
yardu	yard	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Ertská	Ertský	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
je	být	k5eAaImIp3nS
námi	my	k3xPp1nPc7
vnímána	vnímat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
velmi	velmi	k6eAd1
výrazná	výrazný	k2eAgFnSc1d1
až	až	k9
agresivní	agresivní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fanoušci	Fanoušek	k1gMnPc1
techna	techna	k1gFnSc1
a	a	k8xC
někteří	některý	k3yIgMnPc1
rockeři	rocker	k1gMnPc1
s	s	k7c7
tímto	tento	k3xDgInSc7
názorem	názor	k1gInSc7
nesouhlasí	souhlasit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
Středověké	středověký	k2eAgFnPc1d1
a	a	k8xC
moderní	moderní	k2eAgFnPc1d1
ekologické	ekologický	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
tradiční	tradiční	k2eAgInSc4d1
tzv.	tzv.	kA
okapový	okapový	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
střechy	střecha	k1gFnPc4
se	s	k7c7
spádem	spád	k1gInSc7
doprostřed	doprostřed	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dešťová	dešťový	k2eAgFnSc1d1
voda	voda	k1gFnSc1
jímá	jímat	k5eAaImIp3nS
a	a	k8xC
svádí	svádět	k5eAaImIp3nS
dolů	dolů	k6eAd1
do	do	k7c2
stavby	stavba	k1gFnSc2
jako	jako	k8xC,k8xS
voda	voda	k1gFnSc1
užitková	užitkový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
přímořskému	přímořský	k2eAgNnSc3d1
podnebí	podnebí	k1gNnSc3
s	s	k7c7
hojnými	hojný	k2eAgFnPc7d1
a	a	k8xC
pravidelnými	pravidelný	k2eAgFnPc7d1
srážkami	srážka	k1gFnPc7
jde	jít	k5eAaImIp3nS
o	o	k7c4
praktické	praktický	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
začíná	začínat	k5eAaImIp3nS
prosazovat	prosazovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Historie	historie	k1gFnSc1
filmu	film	k1gInSc2
začala	začít	k5eAaPmAgFnS
v	v	k7c6
Ertaru	Ertar	k1gInSc6
před	před	k7c7
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
lety	let	k1gInPc7
barevnými	barevný	k2eAgInPc7d1
filmy	film	k1gInPc7
a	a	k8xC
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zážitkovými	zážitkový	k2eAgNnPc7d1
kiny	kino	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
archivech	archiv	k1gInPc6
lze	lze	k6eAd1
najít	najít	k5eAaPmF
několik	několik	k4yIc4
černobílých	černobílý	k2eAgInPc2d1
filmů	film	k1gInPc2
natočených	natočený	k2eAgInPc2d1
jako	jako	k8xS,k8xC
experimenty	experiment	k1gInPc1
pro	pro	k7c4
zdůraznění	zdůraznění	k1gNnSc4
uměleckého	umělecký	k2eAgInSc2d1
záměru	záměr	k1gInSc2
autora	autor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pohraniční	pohraniční	k2eAgInSc4d1
styk	styk	k1gInSc4
</s>
<s>
Republika	republika	k1gFnSc1
Ertar	Ertara	k1gFnPc2
udržuje	udržovat	k5eAaImIp3nS
se	s	k7c7
sousedními	sousední	k2eAgInPc7d1
státy	stát	k1gInPc7
dobré	dobrý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
pohraničních	pohraniční	k2eAgInPc2d1
přechodů	přechod	k1gInPc2
zavedla	zavést	k5eAaPmAgFnS
i	i	k9
zvláštní	zvláštní	k2eAgInSc4d1
režim	režim	k1gInSc4
na	na	k7c6
Sargasové	sargasový	k2eAgFnSc6d1
dálnici	dálnice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
tranzit	tranzit	k1gInSc4
vozidel	vozidlo	k1gNnPc2
mezi	mezi	k7c7
sousedními	sousední	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
bez	bez	k7c2
celního	celní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohraniční	pohraniční	k2eAgInSc4d1
styk	styk	k1gInSc4
se	s	k7c7
Zemí	zem	k1gFnSc7
je	být	k5eAaImIp3nS
kvantový	kvantový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
objem	objem	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
hospodářského	hospodářský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
zanedbatelný	zanedbatelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
technickým	technický	k2eAgFnPc3d1
potížím	potíž	k1gFnPc3
a	a	k8xC
častým	častý	k2eAgNnPc3d1
zkreslením	zkreslení	k1gNnPc3
při	při	k7c6
přenosu	přenos	k1gInSc6
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
význam	význam	k1gInSc4
spočívá	spočívat	k5eAaImIp3nS
spíše	spíše	k9
v	v	k7c6
kulturní	kulturní	k2eAgFnSc6d1
než	než	k8xS
v	v	k7c6
informační	informační	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Závěr	závěr	k1gInSc1
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
opět	opět	k6eAd1
roste	růst	k5eAaImIp3nS
význam	význam	k1gInSc1
fanzinu	fanzinout	k5eAaPmIp1nS
Světelné	světelný	k2eAgInPc4d1
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
počátků	počátek	k1gInPc2
fanzinu	fanzin	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
poskytoval	poskytovat	k5eAaImAgInS
necenzurované	cenzurovaný	k2eNgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
nedostupných	dostupný	k2eNgFnPc6d1
SF	SF	kA
filmech	film	k1gInPc6
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
nyní	nyní	k6eAd1
spíše	spíše	k9
vedení	vedení	k1gNnSc3
při	při	k7c6
orientaci	orientace	k1gFnSc6
v	v	k7c6
záplavě	záplava	k1gFnSc6
dostupných	dostupný	k2eAgFnPc2d1
SF	SF	kA
filmů	film	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
ukončilo	ukončit	k5eAaPmAgNnS
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
velvyslanectví	velvyslanectví	k1gNnSc2
Republiky	republika	k1gFnSc2
Ertar	Ertara	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájemci	zájemce	k1gMnPc7
o	o	k7c6
seznámení	seznámení	k1gNnSc6
s	s	k7c7
historií	historie	k1gFnSc7
<g/>
,	,	kIx,
vědou	věda	k1gFnSc7
a	a	k8xC
kulturou	kultura	k1gFnSc7
Ertaru	Ertar	k1gInSc2
mohou	moct	k5eAaImIp3nP
získat	získat	k5eAaPmF
základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
prostřednictvím	prostřednictvím	k7c2
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
jeho	jeho	k3xOp3gMnPc2
příznivců	příznivec	k1gMnPc2
-	-	kIx~
viz	vidět	k5eAaImRp2nS
cestovní	cestovní	k2eAgFnPc4d1
kancelář	kancelář	k1gFnSc1
Erore	Eror	k1gInSc5
(	(	kIx(
<g/>
www.ertologove.cz	www.ertologove.cza	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Stránky	stránka	k1gFnPc4
CK	CK	kA
Erore	Eror	k1gInSc5
-	-	kIx~
klub	klub	k1gInSc1
příznivců	příznivec	k1gMnPc2
Ertaru	Ertar	k1gInSc2
v	v	k7c6
ČR	ČR	kA
http://ertologove.cz/	http://ertologove.cz/	k?
<g/>
↑	↑	k?
http://amber.zine.cz/AZOld/wizw/cc.htm	http://amber.zine.cz/AZOld/wizw/cc.htm	k1gInSc1
<g/>
↑	↑	k?
Mysli	mysl	k1gFnSc2
si	se	k3xPyFc3
svět	svět	k1gInSc4
<g/>
,	,	kIx,
Magazin	Magazin	k1gInSc1
SEDMIČKA	sedmička	k1gFnSc1
<g/>
,	,	kIx,
č.	č.	k?
<g/>
52	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
http://www.mf.cz/	http://www.mf.cz/	k?
↑	↑	k?
Beneš	Beneš	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Jak	jak	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
žilo	žít	k5eAaImAgNnS
uvnitř	uvnitř	k7c2
duté	dutý	k2eAgFnSc2d1
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Magazin	Magazin	k1gMnSc1
EPOCHA	epocha	k1gFnSc1
<g/>
,	,	kIx,
č.	č.	k?
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
RF	RF	kA
HOBBY	hobby	k1gNnSc1
s.	s.	k?
r.	r.	kA
o.	o.	k?
http://rf-hobby.cz/	http://rf-hobby.cz/	k?
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ertar	Ertara	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
