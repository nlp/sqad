<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
period	perioda	k1gFnPc2
má	mít	k5eAaImIp3nS
100	#num#	k4
pulsů	puls	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
puls	puls	k1gInSc1
=	=	kIx~
cca	cca	kA
0,8	0,8	k4
sec	sec	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
délková	délkový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
1	#num#	k4
yankabal	yankabal	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1	#num#	k4
anglickému	anglický	k2eAgInSc3d1
yardu	yard	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
má	mít	k5eAaImIp3nS
10	#num#	k4
sekcí	sekce	k1gFnPc2
dělených	dělený	k2eAgFnPc2d1
na	na	k7c6
100	#num#	k4
period	perioda	k1gFnPc2
<g/>
.	.	kIx.
</s>