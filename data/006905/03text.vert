<s>
Loving	Loving	k1gInSc1	Loving
Hut	Hut	k1gFnSc2	Hut
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
řetězec	řetězec	k1gInSc1	řetězec
veganských	veganský	k2eAgFnPc2d1	veganská
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
přes	přes	k7c4	přes
200	[number]	k4	200
poboček	pobočka	k1gFnPc2	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
restaurační	restaurační	k2eAgNnSc4d1	restaurační
zařízení	zařízení	k1gNnSc4	zařízení
typu	typ	k1gInSc2	typ
fast	fast	k1gMnSc1	fast
food	food	k1gMnSc1	food
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
buď	buď	k8xC	buď
objednat	objednat	k5eAaPmF	objednat
jídlo	jídlo	k1gNnSc4	jídlo
z	z	k7c2	z
menu	menu	k1gNnSc2	menu
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
oběda	oběd	k1gInSc2	oběd
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stravovat	stravovat	k5eAaImF	stravovat
formou	forma	k1gFnSc7	forma
bufetu	bufet	k1gInSc2	bufet
-	-	kIx~	-
platí	platit	k5eAaImIp3nS	platit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
zde	zde	k6eAd1	zde
podávané	podávaný	k2eAgInPc1d1	podávaný
pokrmy	pokrm	k1gInPc1	pokrm
jsou	být	k5eAaImIp3nP	být
připravovány	připravovat	k5eAaImNgInP	připravovat
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
také	také	k9	také
čehokoliv	cokoliv	k3yInSc2	cokoliv
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
-	-	kIx~	-
vegansky	vegansky	k6eAd1	vegansky
<g/>
.	.	kIx.	.
</s>
<s>
Nepodávají	podávat	k5eNaImIp3nP	podávat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zákaz	zákaz	k1gInSc4	zákaz
kouření	kouření	k1gNnSc2	kouření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
veganskou	veganský	k2eAgFnSc7d1	veganská
restaurací	restaurace	k1gFnSc7	restaurace
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
VegNews	VegNewsa	k1gFnPc2	VegNewsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
pobočky	pobočka	k1gFnSc2	pobočka
v	v	k7c6	v
deseti	deset	k4xCc6	deset
zemích	zem	k1gFnPc6	zem
-	-	kIx~	-
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
aplikace	aplikace	k1gFnPc4	aplikace
pro	pro	k7c4	pro
iOS	iOS	k?	iOS
a	a	k8xC	a
Android	android	k1gInSc1	android
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
pobočky	pobočka	k1gFnPc1	pobočka
Loving	Loving	k1gInSc1	Loving
hut	hut	k?	hut
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
poboček	pobočka	k1gFnPc2	pobočka
<g/>
:	:	kIx,	:
Truhlářská	truhlářský	k2eAgFnSc1d1	truhlářská
20	[number]	k4	20
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
35	[number]	k4	35
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
Plzeňská	plzeňská	k1gFnSc1	plzeňská
8	[number]	k4	8
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
)	)	kIx)	)
-	-	kIx~	-
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
Radlická	radlický	k2eAgFnSc1d1	Radlická
117	[number]	k4	117
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
(	(	kIx(	(
<g/>
OC	OC	kA	OC
Butovice	Butovice	k1gFnSc1	Butovice
<g/>
)	)	kIx)	)
-	-	kIx~	-
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
Neklanova	Neklanův	k2eAgFnSc1d1	Neklanova
30	[number]	k4	30
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
-	-	kIx~	-
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
Dukelských	dukelský	k2eAgMnPc2d1	dukelský
hrdinů	hrdina	k1gMnPc2	hrdina
18	[number]	k4	18
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
Zborovská	Zborovský	k2eAgFnSc1d1	Zborovská
19	[number]	k4	19
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
Koubkova	Koubkův	k2eAgFnSc1d1	Koubkova
8	[number]	k4	8
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
-	-	kIx~	-
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
Radčická	Radčická	k1gFnSc1	Radčická
2	[number]	k4	2
<g/>
,	,	kIx,	,
301	[number]	k4	301
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
OC	OC	kA	OC
Plaza	plaz	k1gMnSc2	plaz
<g/>
)	)	kIx)	)
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
50	[number]	k4	50
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
-	-	kIx~	-
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
Veganství	Veganství	k1gNnPc2	Veganství
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Loving	Loving	k1gInSc1	Loving
Hut	Hut	k1gMnPc2	Hut
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
restaurace	restaurace	k1gFnSc2	restaurace
</s>
