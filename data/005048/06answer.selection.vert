<s>
Největší	veliký	k2eAgFnSc7d3	veliký
památkou	památka	k1gFnSc7	památka
mezopotámského	mezopotámský	k2eAgNnSc2d1	mezopotámský
písemnictví	písemnictví	k1gNnSc2	písemnictví
je	být	k5eAaImIp3nS	být
Epos	epos	k1gInSc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k8xC	ale
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
nebyl	být	k5eNaImAgMnS	být
nejspíše	nejspíše	k9	nejspíše
moc	moc	k6eAd1	moc
znám	znám	k2eAgMnSc1d1	znám
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
veliký	veliký	k2eAgInSc4d1	veliký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
tamější	tamější	k2eAgFnSc6d1	tamější
literární	literární	k2eAgFnSc6d1	literární
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
</s>
