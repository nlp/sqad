<s>
Hungarosaurus	Hungarosaurus	k1gInSc1	Hungarosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Maďarský	maďarský	k2eAgMnSc1d1	maďarský
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
ptakopánvého	ptakopánvý	k2eAgMnSc2d1	ptakopánvý
obrněného	obrněný	k2eAgMnSc2d1	obrněný
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
(	(	kIx(	(
<g/>
tyreoforana	tyreoforan	k1gMnSc2	tyreoforan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fosílie	fosílie	k1gFnPc1	fosílie
tohoto	tento	k3xDgNnSc2	tento
středně	středně	k6eAd1	středně
velkého	velký	k2eAgMnSc2d1	velký
obrněného	obrněný	k2eAgMnSc2d1	obrněný
býložravce	býložravec	k1gMnSc2	býložravec
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc4d1	znám
ze	z	k7c2	z
souvrství	souvrství	k1gNnSc2	souvrství
Csehbánya	Csehbányus	k1gMnSc2	Csehbányus
<g/>
,	,	kIx,	,
rozkládajícího	rozkládající	k2eAgMnSc2d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
západního	západní	k2eAgNnSc2d1	západní
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Stářím	stáří	k1gNnSc7	stáří
jsou	být	k5eAaImIp3nP	být
svrchnokřídové	svrchnokřída	k1gMnPc1	svrchnokřída
a	a	k8xC	a
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
geologického	geologický	k2eAgInSc2d1	geologický
stupně	stupeň	k1gInSc2	stupeň
santon	santon	k1gInSc1	santon
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
86-83	[number]	k4	86-83
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hungarosaurus	Hungarosaurus	k1gMnSc1	Hungarosaurus
byl	být	k5eAaImAgMnS	být
zavalitý	zavalitý	k2eAgMnSc1d1	zavalitý
čtvernohý	čtvernohý	k2eAgMnSc1d1	čtvernohý
býložravec	býložravec	k1gMnSc1	býložravec
se	s	k7c7	s
zobákovitými	zobákovitý	k2eAgFnPc7d1	zobákovitá
čelistmi	čelist	k1gFnPc7	čelist
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
tělo	tělo	k1gNnSc4	tělo
chránil	chránit	k5eAaImAgMnS	chránit
pancíř	pancíř	k1gInSc4	pancíř
ze	z	k7c2	z
zkostnatělých	zkostnatělý	k2eAgFnPc2d1	zkostnatělá
kožních	kožní	k2eAgFnPc2d1	kožní
destiček	destička	k1gFnPc2	destička
(	(	kIx(	(
<g/>
osteodermů	osteoderm	k1gInPc2	osteoderm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nejlépe	dobře	k6eAd3	dobře
zachovaným	zachovaný	k2eAgInSc7d1	zachovaný
ankylosaurem	ankylosaur	k1gInSc7	ankylosaur
svrchnokřídové	svrchnokřídový	k2eAgFnSc2d1	svrchnokřídový
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kladistická	Kladistický	k2eAgFnSc1d1	Kladistický
analýza	analýza	k1gFnSc1	analýza
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bazálního	bazální	k2eAgMnSc4d1	bazální
zástupce	zástupce	k1gMnSc4	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Nodosauridae	Nodosaurida	k1gFnSc2	Nodosaurida
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
vývojově	vývojově	k6eAd1	vývojově
pokročilejší	pokročilý	k2eAgMnSc1d2	pokročilejší
než	než	k8xS	než
evropský	evropský	k2eAgInSc1d1	evropský
Struthiosaurus	Struthiosaurus	k1gInSc1	Struthiosaurus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
primitivnější	primitivní	k2eAgFnSc1d2	primitivnější
než	než	k8xS	než
třeba	třeba	k6eAd1	třeba
severoamerické	severoamerický	k2eAgInPc4d1	severoamerický
rody	rod	k1gInPc4	rod
Silvisaurus	Silvisaurus	k1gMnSc1	Silvisaurus
<g/>
,	,	kIx,	,
Sauropelta	Sauropelta	k1gMnSc1	Sauropelta
nebo	nebo	k8xC	nebo
Pawpawsaurus	Pawpawsaurus	k1gMnSc1	Pawpawsaurus
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
hungarosaura	hungarosaura	k1gFnSc1	hungarosaura
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
4	[number]	k4	4
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
lebka	lebka	k1gFnSc1	lebka
měřila	měřit	k5eAaImAgFnS	měřit
asi	asi	k9	asi
32-36	[number]	k4	32-36
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hungarosaurus	Hungarosaurus	k1gInSc1	Hungarosaurus
tormai	tormai	k6eAd1	tormai
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
čtyř	čtyři	k4xCgMnPc2	čtyři
fosilních	fosilní	k2eAgMnPc2d1	fosilní
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
bauxitovém	bauxitový	k2eAgInSc6d1	bauxitový
lomu	lom	k1gInSc6	lom
nedaleko	nedaleko	k7c2	nedaleko
maďarské	maďarský	k2eAgFnSc2d1	maďarská
vsi	ves	k1gFnSc2	ves
Iharkút	Iharkúta	k1gFnPc2	Iharkúta
<g/>
.	.	kIx.	.
</s>
<s>
Holotyp	Holotyp	k1gMnSc1	Holotyp
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc4	označení
MTM	MTM	kA	MTM
Gyn	Gyn	k1gMnSc2	Gyn
<g/>
/	/	kIx~	/
<g/>
404	[number]	k4	404
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgMnS	uložit
v	v	k7c6	v
přírodovědeckém	přírodovědecký	k2eAgNnSc6d1	Přírodovědecké
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
asi	asi	k9	asi
450	[number]	k4	450
zkamenělých	zkamenělý	k2eAgFnPc2d1	zkamenělá
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
částí	část	k1gFnPc2	část
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
lokalitě	lokalita	k1gFnSc6	lokalita
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
také	také	k9	také
fosílie	fosílie	k1gFnPc1	fosílie
jiných	jiný	k2eAgMnPc2d1	jiný
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
ptakoještěrů	ptakoještěr	k1gMnPc2	ptakoještěr
<g/>
,	,	kIx,	,
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
,	,	kIx,	,
ještěrek	ještěrka	k1gFnPc2	ještěrka
<g/>
,	,	kIx,	,
želv	želva	k1gFnPc2	želva
a	a	k8xC	a
kostnatých	kostnatý	k2eAgFnPc2d1	kostnatá
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
výzkumem	výzkum	k1gInSc7	výzkum
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zabývá	zabývat	k5eAaImIp3nS	zabývat
maďarský	maďarský	k2eAgMnSc1d1	maďarský
paleontolog	paleontolog	k1gMnSc1	paleontolog
Attila	Attila	k1gMnSc1	Attila
Ösi	Ösi	k1gMnSc1	Ösi
<g/>
.	.	kIx.	.
</s>
<s>
Ősi	Ősi	k?	Ősi
<g/>
,	,	kIx,	,
Attila	Attila	k1gMnSc1	Attila
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Hungarosaurus	Hungarosaurus	k1gInSc1	Hungarosaurus
tormai	torma	k1gFnSc2	torma
<g/>
,	,	kIx,	,
a	a	k8xC	a
new	new	k?	new
ankylosaur	ankylosaura	k1gFnPc2	ankylosaura
(	(	kIx(	(
<g/>
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
)	)	kIx)	)
from	from	k6eAd1	from
the	the	k?	the
Upper	Upper	k1gMnSc1	Upper
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
<g/>
.	.	kIx.	.
</s>
<s>
Journal	Journat	k5eAaImAgMnS	Journat
of	of	k?	of
Vertebrate	Vertebrat	k1gInSc5	Vertebrat
Paleontology	paleontolog	k1gMnPc4	paleontolog
25	[number]	k4	25
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
370	[number]	k4	370
<g/>
-	-	kIx~	-
<g/>
383	[number]	k4	383
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
2005	[number]	k4	2005
</s>
