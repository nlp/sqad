<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
kulového	kulový	k2eAgNnSc2d1	kulové
rotujícího	rotující	k2eAgNnSc2d1	rotující
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc1	měsíc
nebo	nebo	k8xC	nebo
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
na	na	k7c6	na
půli	půle	k1gFnSc6	půle
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
geografickými	geografický	k2eAgInPc7d1	geografický
póly	pól	k1gInPc7	pól
<g/>
?	?	kIx.	?
</s>
