<p>
<s>
Atacama	Atacamum	k1gNnPc1	Atacamum
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgNnPc1d1	zvané
také	také	k9	také
Puna	Pun	k1gMnSc2	Pun
de	de	k?	de
Atacama	Atacam	k1gMnSc2	Atacam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejsušší	suchý	k2eAgFnSc1d3	nejsušší
poušť	poušť	k1gFnSc1	poušť
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Poušť	poušť	k1gFnSc1	poušť
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
okolo	okolo	k7c2	okolo
Pacifiku	Pacifik	k1gInSc2	Pacifik
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
horského	horský	k2eAgInSc2d1	horský
státu	stát	k1gInSc2	stát
Peru	prát	k5eAaImIp1nS	prát
až	až	k9	až
do	do	k7c2	do
centrálních	centrální	k2eAgFnPc2d1	centrální
oblastí	oblast	k1gFnPc2	oblast
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místy	k6eAd1	místy
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
až	až	k9	až
na	na	k7c4	na
úpatí	úpatí	k1gNnPc4	úpatí
Západních	západní	k2eAgFnPc2d1	západní
Kordiller	Kordillery	k1gFnPc2	Kordillery
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
5000	[number]	k4	5000
m.	m.	k?	m.
Prší	pršet	k5eAaImIp3nS	pršet
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
udáváno	udáván	k2eAgNnSc1d1	udáváno
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Calama	Calama	k1gFnSc1	Calama
<g/>
,	,	kIx,	,
Copiapó	Copiapó	k1gFnSc1	Copiapó
<g/>
,	,	kIx,	,
Antofagasta	Antofagasta	k1gFnSc1	Antofagasta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
měření	měření	k1gNnSc2	měření
významně	významně	k6eAd1	významně
nepršelo	pršet	k5eNaImAgNnS	pršet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1570	[number]	k4	1570
až	až	k9	až
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Atacama	Atacamum	k1gNnSc2	Atacamum
žije	žít	k5eAaImIp3nS	žít
okolo	okolo	k7c2	okolo
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
i	i	k9	i
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
<g/>
.	.	kIx.	.
</s>
<s>
Obrovská	obrovský	k2eAgNnPc1d1	obrovské
naleziště	naleziště	k1gNnPc1	naleziště
železné	železný	k2eAgFnSc2d1	železná
a	a	k8xC	a
měděné	měděný	k2eAgFnSc2d1	měděná
rudy	ruda	k1gFnSc2	ruda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
významným	významný	k2eAgInSc7d1	významný
artiklem	artikl	k1gInSc7	artikl
této	tento	k3xDgFnSc2	tento
nehostinné	hostinný	k2eNgFnSc2d1	nehostinná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
důležité	důležitý	k2eAgNnSc4d1	důležité
nerostné	nerostný	k2eAgNnSc4d1	nerostné
bohatství	bohatství	k1gNnSc4	bohatství
patří	patřit	k5eAaImIp3nS	patřit
největší	veliký	k2eAgNnSc4d3	veliký
naleziště	naleziště	k1gNnSc4	naleziště
dusičnanu	dusičnan	k1gInSc2	dusičnan
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
minerál	minerál	k1gInSc4	minerál
nitronatrit	nitronatrita	k1gFnPc2	nitronatrita
neboli	neboli	k8xC	neboli
chilský	chilský	k2eAgInSc1d1	chilský
ledek	ledek	k1gInSc1	ledek
<g/>
)	)	kIx)	)
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
se	se	k3xPyFc4	se
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
těžby	těžba	k1gFnSc2	těžba
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
produkce	produkce	k1gFnSc1	produkce
z	z	k7c2	z
Atacamy	Atacam	k1gInPc7	Atacam
až	až	k9	až
čtyři	čtyři	k4xCgFnPc1	čtyři
pětiny	pětina	k1gFnPc1	pětina
světové	světový	k2eAgFnSc2d1	světová
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Atacama	Atacama	k1gFnSc1	Atacama
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
náhlými	náhlý	k2eAgInPc7d1	náhlý
teplotními	teplotní	k2eAgInPc7d1	teplotní
výkyvy	výkyv	k1gInPc7	výkyv
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
až	až	k9	až
po	po	k7c4	po
50	[number]	k4	50
°	°	k?	°
<g/>
C	C	kA	C
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
teplotní	teplotní	k2eAgInSc1d1	teplotní
gradient	gradient	k1gInSc1	gradient
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
erozní	erozní	k2eAgFnSc4d1	erozní
činnost	činnost	k1gFnSc4	činnost
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Vlhké	vlhký	k2eAgInPc1d1	vlhký
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
překonat	překonat	k5eAaPmF	překonat
morfologickou	morfologický	k2eAgFnSc4d1	morfologická
hráz	hráz	k1gFnSc4	hráz
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
Andami	Anda	k1gFnPc7	Anda
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
sem	sem	k6eAd1	sem
nedosáhnou	dosáhnout	k5eNaPmIp3nP	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vláha	vláha	k1gFnSc1	vláha
není	být	k5eNaImIp3nS	být
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
transportována	transportovat	k5eAaBmNgFnS	transportovat
ani	ani	k8xC	ani
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
východního	východní	k2eAgInSc2d1	východní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
proudí	proudit	k5eAaPmIp3nS	proudit
studený	studený	k2eAgInSc1d1	studený
Peruánský	peruánský	k2eAgInSc1d1	peruánský
oceánský	oceánský	k2eAgInSc1d1	oceánský
proud	proud	k1gInSc1	proud
a	a	k8xC	a
vlivem	vliv	k1gInSc7	vliv
síly	síla	k1gFnSc2	síla
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
se	se	k3xPyFc4	se
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
řídká	řídký	k2eAgNnPc1d1	řídké
mračna	mračno	k1gNnPc1	mračno
stáčí	stáčet	k5eAaImIp3nP	stáčet
zpět	zpět	k6eAd1	zpět
nad	nad	k7c4	nad
oceán	oceán	k1gInSc4	oceán
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
nad	nad	k7c4	nad
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Srážkový	srážkový	k2eAgInSc1d1	srážkový
úhrn	úhrn	k1gInSc1	úhrn
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
1	[number]	k4	1
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Atacama	Atacama	k1gFnSc1	Atacama
i	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
extrémy	extrém	k1gInPc4	extrém
má	mít	k5eAaImIp3nS	mít
co	co	k9	co
nabídnout	nabídnout	k5eAaPmF	nabídnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Národní	národní	k2eAgFnSc2d1	národní
rezervace	rezervace	k1gFnSc2	rezervace
Los	los	k1gMnSc1	los
Flamencos	Flamencos	k1gMnSc1	Flamencos
<g/>
,	,	kIx,	,
Valle	Valle	k1gFnSc1	Valle
de	de	k?	de
la	la	k1gNnSc2	la
Luna	luna	k1gFnSc1	luna
či	či	k8xC	či
gejzíru	gejzír	k1gInSc6	gejzír
El	Ela	k1gFnPc2	Ela
Tatio	Tatio	k1gMnSc1	Tatio
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nejvýše	nejvýše	k6eAd1	nejvýše
položeným	položený	k2eAgInSc7d1	položený
gejzírem	gejzír	k1gInSc7	gejzír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
pracovišť	pracoviště	k1gNnPc2	pracoviště
Evropské	evropský	k2eAgFnSc2d1	Evropská
jižní	jižní	k2eAgFnSc2d1	jižní
observatoře	observatoř	k1gFnSc2	observatoř
(	(	kIx(	(
<g/>
ESO	eso	k1gNnSc1	eso
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
Chajnantor	Chajnantor	k1gInSc1	Chajnantor
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
největší	veliký	k2eAgInSc1d3	veliký
astronomický	astronomický	k2eAgInSc1d1	astronomický
přístroj	přístroj	k1gInSc1	přístroj
současnosti	současnost	k1gFnSc2	současnost
<g/>
:	:	kIx,	:
soustava	soustava	k1gFnSc1	soustava
66	[number]	k4	66
radioteleskopů	radioteleskop	k1gInPc2	radioteleskop
ALMA	alma	k1gFnSc1	alma
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
Paranal	Paranal	k1gFnPc2	Paranal
s	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
hlavními	hlavní	k2eAgInPc7d1	hlavní
dalekohledy	dalekohled	k1gInPc7	dalekohled
Very	Vera	k1gMnSc2	Vera
large	larg	k1gMnSc2	larg
telescope	telescop	k1gInSc5	telescop
(	(	kIx(	(
<g/>
VLT	VLT	kA	VLT
<g/>
)	)	kIx)	)
se	s	k7c7	s
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
8,2	[number]	k4	8,2
m	m	kA	m
umístěnými	umístěný	k2eAgInPc7d1	umístěný
na	na	k7c6	na
umělé	umělý	k2eAgFnSc6d1	umělá
plošině	plošina	k1gFnSc6	plošina
hory	hora	k1gFnSc2	hora
Paranal	Paranal	k1gFnSc2	Paranal
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
La	la	k1gNnSc2	la
Silla	Silla	k1gFnSc1	Silla
–	–	k?	–
nejstarší	starý	k2eAgNnSc4d3	nejstarší
pracoviště	pracoviště	k1gNnSc4	pracoviště
ESO	eso	k1gNnSc1	eso
s	s	k7c7	s
15	[number]	k4	15
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
3,58	[number]	k4	3,58
<g/>
m	m	kA	m
New	New	k1gFnPc2	New
Technology	technolog	k1gMnPc4	technolog
Telescope	Telescop	k1gInSc5	Telescop
(	(	kIx(	(
<g/>
dalekohled	dalekohled	k1gInSc4	dalekohled
nové	nový	k2eAgFnSc2d1	nová
technologie	technologie	k1gFnSc2	technologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
extrémně	extrémně	k6eAd1	extrémně
velký	velký	k2eAgInSc1d1	velký
dalekohled	dalekohled	k1gInSc1	dalekohled
–	–	k?	–
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
optický	optický	k2eAgInSc1d1	optický
dalekohled	dalekohled	k1gInSc1	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
39,3	[number]	k4	39,3
m	m	kA	m
s	s	k7c7	s
plánovaným	plánovaný	k2eAgNnSc7d1	plánované
uvedením	uvedení	k1gNnSc7	uvedení
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2024	[number]	k4	2024
<g/>
.	.	kIx.	.
<g/>
Důvodem	důvod	k1gInSc7	důvod
jejich	jejich	k3xOp3gNnSc4	jejich
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
poušti	poušť	k1gFnSc6	poušť
je	být	k5eAaImIp3nS	být
především	především	k9	především
extrémně	extrémně	k6eAd1	extrémně
suché	suchý	k2eAgNnSc1d1	suché
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
pozorování	pozorování	k1gNnPc4	pozorování
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
paprskům	paprsek	k1gInPc3	paprsek
některých	některý	k3yIgFnPc2	některý
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
proniknout	proniknout	k5eAaPmF	proniknout
až	až	k9	až
k	k	k7c3	k
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
bezoblačných	bezoblačný	k2eAgInPc2d1	bezoblačný
dní	den	k1gInPc2	den
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gNnSc1	jejich
světelné	světelný	k2eAgNnSc1d1	světelné
znečištění	znečištění	k1gNnSc1	znečištění
neruší	rušit	k5eNaImIp3nS	rušit
pozorování	pozorování	k1gNnSc4	pozorování
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
přírodním	přírodní	k2eAgFnPc3d1	přírodní
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
charakteru	charakter	k1gInSc6	charakter
povrchu	povrch	k1gInSc2	povrch
využívá	využívat	k5eAaPmIp3nS	využívat
oblast	oblast	k1gFnSc1	oblast
také	také	k9	také
NASA	NASA	kA	NASA
k	k	k7c3	k
testům	test	k1gMnPc3	test
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
mise	mise	k1gFnPc4	mise
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Chilské	chilský	k2eAgInPc1d1	chilský
kaktusy	kaktus	k1gInPc1	kaktus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Atacama	Atacamum	k1gNnSc2	Atacamum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Atacama	Atacamum	k1gNnSc2	Atacamum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Atacama	Atacamum	k1gNnSc2	Atacamum
</s>
</p>
