<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
trojbarevná	trojbarevný	k2eAgFnSc1d1	trojbarevná
trikolóra	trikolóra	k1gFnSc1	trikolóra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
šířkou	šířka	k1gFnSc7	šířka
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
Ruské	ruský	k2eAgFnSc2d1	ruská
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přebírala	přebírat	k5eAaImAgFnS	přebírat
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
národní	národní	k2eAgNnSc4d1	národní
hnutí	hnutí	k1gNnSc4	hnutí
symboliku	symbolika	k1gFnSc4	symbolika
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
jediného	jediný	k2eAgInSc2d1	jediný
tehdy	tehdy	k6eAd1	tehdy
samostatného	samostatný	k2eAgInSc2d1	samostatný
slovanského	slovanský	k2eAgInSc2d1	slovanský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
ruské	ruský	k2eAgFnSc2d1	ruská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
tehdy	tehdy	k6eAd1	tehdy
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
panslovanské	panslovanský	k2eAgFnPc4d1	panslovanská
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
barvám	barva	k1gFnPc3	barva
přisuzován	přisuzován	k2eAgInSc4d1	přisuzován
následující	následující	k2eAgInSc4d1	následující
význam	význam	k1gInSc4	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
neposkvrněnost	neposkvrněnost	k1gFnSc4	neposkvrněnost
<g/>
,	,	kIx,	,
dokonalost	dokonalost	k1gFnSc4	dokonalost
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
představuje	představovat	k5eAaImIp3nS	představovat
stálost	stálost	k1gFnSc1	stálost
<g/>
,	,	kIx,	,
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
věrnost	věrnost	k1gFnSc4	věrnost
a	a	k8xC	a
Bohorodičku	Bohorodička	k1gFnSc4	Bohorodička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červená	Červená	k1gFnSc1	Červená
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
krve	krev	k1gFnSc2	krev
prolité	prolitý	k2eAgFnSc2d1	prolitá
za	za	k7c4	za
vlast	vlast	k1gFnSc4	vlast
a	a	k8xC	a
absolutistické	absolutistický	k2eAgFnPc4d1	absolutistická
vlády	vláda	k1gFnPc4	vláda
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
patřily	patřit	k5eAaImAgFnP	patřit
původně	původně	k6eAd1	původně
Moskvě	Moskva	k1gFnSc3	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
vlajky	vlajka	k1gFnSc2	vlajka
SSSR	SSSR	kA	SSSR
==	==	k?	==
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
srp	srp	k1gInSc1	srp
a	a	k8xC	a
kladivo	kladivo	k1gNnSc1	kladivo
symbolizovaly	symbolizovat	k5eAaImAgInP	symbolizovat
proletariát	proletariát	k1gInSc4	proletariát
a	a	k8xC	a
rolníky	rolník	k1gMnPc4	rolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červená	červený	k2eAgFnSc1d1	červená
hvězda	hvězda	k1gFnSc1	hvězda
se	s	k7c7	s
zlatým	zlatý	k2eAgInSc7d1	zlatý
okrajem	okraj	k1gInSc7	okraj
představovala	představovat	k5eAaImAgFnS	představovat
jednotu	jednota	k1gFnSc4	jednota
sovětských	sovětský	k2eAgInPc2d1	sovětský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Rusko	Rusko	k1gNnSc1	Rusko
nemělo	mít	k5eNaImAgNnS	mít
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
cara	car	k1gMnSc2	car
Alexeje	Alexej	k1gMnSc2	Alexej
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1668	[number]	k4	1668
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
první	první	k4xOgFnSc1	první
vojenská	vojenský	k2eAgFnSc1d1	vojenská
loď	loď	k1gFnSc1	loď
Orel	Orel	k1gMnSc1	Orel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
stavitel	stavitel	k1gMnSc1	stavitel
lodě	loď	k1gFnSc2	loď
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
šlechtickou	šlechtický	k2eAgFnSc4d1	šlechtická
dumu	duma	k1gFnSc4	duma
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
loď	loď	k1gFnSc1	loď
mohla	moct	k5eAaImAgFnS	moct
plout	plout	k5eAaImF	plout
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
s	s	k7c7	s
nizozemskou	nizozemský	k2eAgFnSc7d1	nizozemská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
bývalo	bývat	k5eAaImAgNnS	bývat
zvykem	zvyk	k1gInSc7	zvyk
u	u	k7c2	u
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
prosbu	prosba	k1gFnSc4	prosba
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
kladně	kladně	k6eAd1	kladně
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zavést	zavést	k5eAaPmF	zavést
obměnu	obměna	k1gFnSc4	obměna
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
vlajky	vlajka	k1gFnSc2	vlajka
jako	jako	k8xS	jako
civilní	civilní	k2eAgFnSc4d1	civilní
vlajku	vlajka	k1gFnSc4	vlajka
pro	pro	k7c4	pro
ruské	ruský	k2eAgFnPc4d1	ruská
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
horizontální	horizontální	k2eAgFnSc1d1	horizontální
bílo-modro-červená	bíloodro-červený	k2eAgFnSc1d1	bílo-modro-červená
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vybraná	vybraný	k2eAgNnPc1d1	vybrané
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výběru	výběr	k1gInSc2	výběr
Alexeje	Alexej	k1gMnSc2	Alexej
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
uznaná	uznaný	k2eAgFnSc1d1	uznaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
měly	mít	k5eAaImAgFnP	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
ruské	ruský	k2eAgFnPc1d1	ruská
národní	národní	k2eAgFnPc1d1	národní
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
zastoupené	zastoupený	k2eAgInPc1d1	zastoupený
i	i	k9	i
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Bílo-modro-červená	Bíloodro-červený	k2eAgFnSc1d1	Bílo-modro-červený
trikolóra	trikolóra	k1gFnSc1	trikolóra
jak	jak	k6eAd1	jak
píše	psát	k5eAaImIp3nS	psát
Alexander	Alexandra	k1gFnPc2	Alexandra
Putjatin	Putjatina	k1gFnPc2	Putjatina
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
O	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
národní	národní	k2eAgFnSc6d1	národní
vlajce	vlajka	k1gFnSc6	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc4	první
státní	státní	k2eAgInSc4d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
východoevropské	východoevropský	k2eAgFnPc4d1	východoevropská
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obracely	obracet	k5eAaImAgFnP	obracet
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
národně-osvobozovacích	národněsvobozovací	k2eAgNnPc6d1	národně-osvobozovací
povstáních	povstání	k1gNnPc6	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
nazývají	nazývat	k5eAaImIp3nP	nazývat
panslovanskými	panslovanský	k2eAgFnPc7d1	panslovanská
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hypotézy	hypotéza	k1gFnSc2	hypotéza
o	o	k7c6	o
původu	původ	k1gInSc6	původ
ruské	ruský	k2eAgFnSc2d1	ruská
vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
historici	historik	k1gMnPc1	historik
zastávají	zastávat	k5eAaImIp3nP	zastávat
několik	několik	k4yIc4	několik
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
původ	původ	k1gInSc4	původ
ruské	ruský	k2eAgFnSc2d1	ruská
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
díla	dílo	k1gNnSc2	dílo
Úryvky	úryvek	k1gInPc4	úryvek
ruských	ruský	k2eAgFnPc2d1	ruská
námořních	námořní	k2eAgFnPc2d1	námořní
dějin	dějiny	k1gFnPc2	dějiny
F.	F.	kA	F.
F.	F.	kA	F.
Veselago	Veselago	k1gMnSc1	Veselago
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
barvy	barva	k1gFnPc1	barva
použité	použitý	k2eAgFnPc1d1	použitá
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Orel	Orel	k1gMnSc1	Orel
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
stavitelů	stavitel	k1gMnPc2	stavitel
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
mírou	míra	k1gFnSc7	míra
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
ruská	ruský	k2eAgFnSc1d1	ruská
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
podobala	podobat	k5eAaImAgFnS	podobat
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
vlajce	vlajka	k1gFnSc3	vlajka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
bílo-modro-červená	bíloodro-červený	k2eAgFnSc1d1	bílo-modro-červená
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
jeho	on	k3xPp3gNnSc2	on
tvrzení	tvrzení	k1gNnSc2	tvrzení
je	být	k5eAaImIp3nS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
–	–	k?	–
budoucího	budoucí	k2eAgMnSc4d1	budoucí
ruského	ruský	k2eAgMnSc4d1	ruský
panovníka	panovník	k1gMnSc4	panovník
Petra	Petr	k1gMnSc4	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgMnSc4d1	veliký
–	–	k?	–
dal	dát	k5eAaPmAgInS	dát
vyrobit	vyrobit	k5eAaPmF	vyrobit
štíty	štít	k1gInPc4	štít
s	s	k7c7	s
bílo-modro-červenými	bíloodro-červený	k2eAgFnPc7d1	bílo-modro-červená
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
tvrzením	tvrzení	k1gNnSc7	tvrzení
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
druhý	druhý	k4xOgMnSc1	druhý
významný	významný	k2eAgMnSc1d1	významný
historik	historik	k1gMnSc1	historik
námořnictví	námořnictví	k1gNnSc2	námořnictví
P.	P.	kA	P.
I.	I.	kA	I.
Belavecev	Belavecva	k1gFnPc2	Belavecva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Barvy	barva	k1gFnSc2	barva
ruské	ruský	k2eAgFnSc2d1	ruská
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
známou	známý	k2eAgFnSc4d1	známá
kresbu	kresba	k1gFnSc4	kresba
Obsazení	obsazení	k1gNnSc2	obsazení
pevnosti	pevnost	k1gFnSc2	pevnost
Azov	Azov	k1gInSc1	Azov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1696	[number]	k4	1696
od	od	k7c2	od
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
malíře	malíř	k1gMnSc2	malíř
Adriaana	Adriaan	k1gMnSc2	Adriaan
Schoonebeeka	Schoonebeeek	k1gMnSc2	Schoonebeeek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
obraze	obraz	k1gInSc6	obraz
mají	mít	k5eAaImIp3nP	mít
ruské	ruský	k2eAgFnPc1d1	ruská
vlajky	vlajka	k1gFnPc1	vlajka
podobu	podoba	k1gFnSc4	podoba
čtverců	čtverec	k1gInPc2	čtverec
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
křížem	křížem	k6eAd1	křížem
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
obsazení	obsazení	k1gNnSc1	obsazení
pevnosti	pevnost	k1gFnSc2	pevnost
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc3	období
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
i	i	k9	i
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
bílo-modro-červené	bíloodro-červený	k2eAgFnSc6d1	bílo-modro-červená
vlajce	vlajka	k1gFnSc6	vlajka
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgFnSc2d1	blízká
k	k	k7c3	k
dnešní	dnešní	k2eAgFnSc3d1	dnešní
podobě	podoba	k1gFnSc3	podoba
vlajky	vlajka	k1gFnSc2	vlajka
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
Moskevského	moskevský	k2eAgMnSc2d1	moskevský
cara	car	k1gMnSc2	car
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
byla	být	k5eAaImAgFnS	být
carská	carský	k2eAgFnSc1d1	carská
standarta	standarta	k1gFnSc1	standarta
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
tzv.	tzv.	kA	tzv.
vlajka	vlajka	k1gFnSc1	vlajka
moskevského	moskevský	k2eAgMnSc2d1	moskevský
cara	car	k1gMnSc2	car
vztyčená	vztyčený	k2eAgFnSc1d1	vztyčená
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1693	[number]	k4	1693
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
plavby	plavba	k1gFnSc2	plavba
po	po	k7c6	po
Bílém	bílý	k2eAgNnSc6d1	bílé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
bílo-modro-červené	bíloodro-červený	k2eAgFnSc2d1	bílo-modro-červená
trikolóry	trikolóra	k1gFnSc2	trikolóra
byl	být	k5eAaImAgMnS	být
umístěný	umístěný	k2eAgMnSc1d1	umístěný
zlatý	zlatý	k2eAgMnSc1d1	zlatý
dvojhlavý	dvojhlavý	k2eAgMnSc1d1	dvojhlavý
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
používala	používat	k5eAaImAgFnS	používat
jako	jako	k9	jako
námořní	námořní	k2eAgFnSc1d1	námořní
vlajka	vlajka	k1gFnSc1	vlajka
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1699	[number]	k4	1699
<g/>
–	–	k?	–
<g/>
1700	[number]	k4	1700
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
nové	nový	k2eAgFnPc4d1	nová
varianty	varianta	k1gFnPc4	varianta
vojensko-námořní	vojenskoámořní	k2eAgFnSc2d1	vojensko-námořní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1705	[number]	k4	1705
vydal	vydat	k5eAaPmAgInS	vydat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc4	který
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
obchodníci	obchodník	k1gMnPc1	obchodník
povinni	povinen	k2eAgMnPc1d1	povinen
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
obchodními	obchodní	k2eAgFnPc7d1	obchodní
loděmi	loď	k1gFnPc7	loď
plavat	plavat	k5eAaImF	plavat
pod	pod	k7c7	pod
bílo-modro-červenou	bíloodro-červený	k2eAgFnSc7d1	bílo-modro-červená
ruskou	ruský	k2eAgFnSc7d1	ruská
trikolórou	trikolóra	k1gFnSc7	trikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
na	na	k7c6	na
vojenských	vojenský	k2eAgFnPc6d1	vojenská
lodích	loď	k1gFnPc6	loď
používala	používat	k5eAaImAgFnS	používat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Ondřejská	ondřejský	k2eAgFnSc1d1	Ondřejská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
trikolóra	trikolóra	k1gFnSc1	trikolóra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajkou	vlajka	k1gFnSc7	vlajka
občanských	občanský	k2eAgInPc2d1	občanský
soudů	soud	k1gInPc2	soud
v	v	k7c6	v
Ruském	ruský	k2eAgNnSc6d1	ruské
impériu	impérium	k1gNnSc6	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
Ruska	Rusko	k1gNnSc2	Rusko
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
dnešní	dnešní	k2eAgFnSc3d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
modrá	modrat	k5eAaImIp3nS	modrat
Bohorodičku	Bohorodička	k1gFnSc4	Bohorodička
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
absolutismus	absolutismus	k1gInSc4	absolutismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
barev	barva	k1gFnPc2	barva
vlajky	vlajka	k1gFnSc2	vlajka
bylo	být	k5eAaImAgNnS	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modrá	modrat	k5eAaImIp3nS	modrat
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
dnešní	dnešní	k2eAgNnSc4d1	dnešní
území	území	k1gNnSc4	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Malé	Malé	k2eAgNnSc1d1	Malé
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
М	М	k?	М
Р	Р	k?	Р
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
;	;	kIx,	;
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červená	Červená	k1gFnSc1	Červená
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
Velké	velký	k2eAgNnSc4d1	velké
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
Ruské	ruský	k2eAgFnSc2d1	ruská
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
ustanovená	ustanovený	k2eAgFnSc1d1	ustanovená
Prozatimní	Prozatimní	k2eAgFnSc1d1	Prozatimní
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
demokracii	demokracie	k1gFnSc3	demokracie
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
použití	použití	k1gNnSc1	použití
červené	červený	k2eAgFnSc2d1	červená
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
Ruské	ruský	k2eAgFnSc2d1	ruská
republiky	republika	k1gFnSc2	republika
znovu	znovu	k6eAd1	znovu
ruská	ruský	k2eAgFnSc1d1	ruská
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
Sovětského	sovětský	k2eAgNnSc2d1	sovětské
Ruska	Rusko	k1gNnSc2	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zavedená	zavedený	k2eAgFnSc1d1	zavedená
červená	červený	k2eAgFnSc1d1	červená
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
iniciálami	iniciála	k1gFnPc7	iniciála
RSFSR	RSFSR	kA	RSFSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
komunisté	komunista	k1gMnPc1	komunista
zrušili	zrušit	k5eAaPmAgMnP	zrušit
všechny	všechen	k3xTgFnPc4	všechen
předcházející	předcházející	k2eAgFnPc4d1	předcházející
vlajky	vlajka	k1gFnPc4	vlajka
a	a	k8xC	a
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
zavedli	zavést	k5eAaPmAgMnP	zavést
červenou	červený	k2eAgFnSc4d1	červená
vlajku	vlajka	k1gFnSc4	vlajka
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1954	[number]	k4	1954
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
ruská	ruský	k2eAgFnSc1d1	ruská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vlajku	vlajka	k1gFnSc4	vlajka
SSSR	SSSR	kA	SSSR
doplněnou	doplněná	k1gFnSc4	doplněná
modrým	modrý	k2eAgInSc7d1	modrý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
===	===	k?	===
</s>
</p>
<p>
<s>
Opětovně	opětovně	k6eAd1	opětovně
byla	být	k5eAaImAgFnS	být
ruská	ruský	k2eAgFnSc1d1	ruská
zástava	zástava	k1gFnSc1	zástava
zavedena	zaveden	k2eAgFnSc1d1	zavedena
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
schválil	schválit	k5eAaPmAgInS	schválit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
Sovět	sovět	k1gInSc1	sovět
RSFSR	RSFSR	kA	RSFSR
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
na	na	k7c4	na
kupoli	kupole	k1gFnSc4	kupole
Senátního	senátní	k2eAgInSc2d1	senátní
paláce	palác	k1gInSc2	palác
Moskevského	moskevský	k2eAgInSc2d1	moskevský
kremlu	kreml	k1gInSc2	kreml
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
odstín	odstín	k1gInSc1	odstín
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Ruska	Ruska	k1gFnSc1	Ruska
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Ruska	Rusko	k1gNnSc2	Rusko
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vlajek	vlajka	k1gFnPc2	vlajka
subjektů	subjekt	k1gInPc2	subjekt
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ruská	ruský	k2eAgFnSc1d1	ruská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
