<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
trojbarevná	trojbarevný	k2eAgFnSc1d1	trojbarevná
trikolóra	trikolóra	k1gFnSc1	trikolóra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
šířkou	šířka	k1gFnSc7	šířka
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
