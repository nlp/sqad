<p>
<s>
Porto	porto	k1gNnSc1	porto
da	da	k?	da
Cruz	Cruza	k1gFnPc2	Cruza
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Machico	Machico	k6eAd1	Machico
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
portugalského	portugalský	k2eAgInSc2d1	portugalský
ostrova	ostrov	k1gInSc2	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
Machico	Machico	k6eAd1	Machico
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
Porto	porto	k1gNnSc1	porto
da	da	k?	da
Cruz	Cruza	k1gFnPc2	Cruza
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
severním	severní	k2eAgNnSc6d1	severní
a	a	k8xC	a
obě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
dělí	dělit	k5eAaImIp3nP	dělit
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
jen	jen	k6eAd1	jen
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
horského	horský	k2eAgInSc2d1	horský
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Porto	porto	k1gNnSc1	porto
da	da	k?	da
Cruz	Cruza	k1gFnPc2	Cruza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
2793	[number]	k4	2793
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
městu	město	k1gNnSc3	město
dal	dát	k5eAaPmAgInS	dát
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
vztyčili	vztyčit	k5eAaPmAgMnP	vztyčit
první	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
(	(	kIx(	(
<g/>
porto	porto	k1gNnSc1	porto
=	=	kIx~	=
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
cruz	cruz	k1gInSc1	cruz
=	=	kIx~	=
kříž	kříž	k1gInSc1	kříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
pracuje	pracovat	k5eAaImIp3nS	pracovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
cukrovar	cukrovar	k1gInSc1	cukrovar
zpracovávající	zpracovávající	k2eAgFnSc4d1	zpracovávající
cukrovou	cukrový	k2eAgFnSc4d1	cukrová
třtinu	třtina	k1gFnSc4	třtina
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
vinařství	vinařství	k1gNnSc1	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
odrůda	odrůda	k1gFnSc1	odrůda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
americana	american	k1gMnSc2	american
(	(	kIx(	(
<g/>
vinho	vin	k1gMnSc2	vin
seco	seco	k1gMnSc1	seco
americana	american	k1gMnSc2	american
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
nový	nový	k2eAgInSc1d1	nový
kostel	kostel	k1gInSc1	kostel
ozdobený	ozdobený	k2eAgInSc1d1	ozdobený
sochami	socha	k1gFnPc7	socha
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
<g/>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
dominantou	dominanta	k1gFnSc7	dominanta
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
"	"	kIx"	"
<g/>
orlí	orlí	k2eAgFnSc1d1	orlí
skála	skála	k1gFnSc1	skála
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Penha	Penha	k1gFnSc1	Penha
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Águia	Águia	k1gFnSc1	Águia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgNnSc1d1	oddělující
Porto	porto	k1gNnSc1	porto
da	da	k?	da
Cruz	Cruza	k1gFnPc2	Cruza
od	od	k7c2	od
sousedního	sousední	k2eAgNnSc2d1	sousední
města	město	k1gNnSc2	město
Faial	Faial	k1gInSc1	Faial
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
zřízena	zřídit	k5eAaPmNgFnS	zřídit
pro	pro	k7c4	pro
místní	místní	k2eAgMnPc4d1	místní
zemědělce	zemědělec	k1gMnPc4	zemědělec
kabinová	kabinový	k2eAgFnSc1d1	kabinová
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
značně	značně	k6eAd1	značně
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Porto	porto	k1gNnSc1	porto
da	da	k?	da
Cruz	Cruza	k1gFnPc2	Cruza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
