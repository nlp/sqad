<p>
<s>
Matthew	Matthew	k?	Matthew
Henry	Henry	k1gMnSc1	Henry
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1662	[number]	k4	1662
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1714	[number]	k4	1714
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
komentátor	komentátor	k1gMnSc1	komentátor
Bible	bible	k1gFnSc2	bible
a	a	k8xC	a
presbyteriánský	presbyteriánský	k2eAgMnSc1d1	presbyteriánský
farář	farář	k1gMnSc1	farář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1662	[number]	k4	1662
na	na	k7c4	na
Broad	Broad	k1gInSc4	Broad
Oak	Oak	k1gFnSc2	Oak
<g/>
,	,	kIx,	,
statku	statek	k1gInSc2	statek
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
mezi	mezi	k7c7	mezi
Flintshirem	Flintshir	k1gMnSc7	Flintshir
a	a	k8xC	a
Shropshirem	Shropshir	k1gMnSc7	Shropshir
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Philip	Philip	k1gMnSc1	Philip
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
také	také	k9	také
duchovní	duchovní	k2eAgFnSc4d1	duchovní
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
zákonu	zákon	k1gInSc3	zákon
Act	Act	k1gFnSc2	Act
of	of	k?	of
Uniformity	uniformita	k1gFnSc2	uniformita
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
státní	státní	k2eAgFnSc2d1	státní
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
dokázal	dokázat	k5eAaPmAgMnS	dokázat
získat	získat	k5eAaPmF	získat
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
synovi	syn	k1gMnSc3	syn
dobré	dobrý	k2eAgNnSc1d1	dobré
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Matthew	Matthew	k?	Matthew
studoval	studovat	k5eAaImAgMnS	studovat
nejdřív	dříve	k6eAd3	dříve
v	v	k7c6	v
Islingtonu	Islington	k1gInSc6	Islington
a	a	k8xC	a
poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
právo	právo	k1gNnSc4	právo
v	v	k7c4	v
Gray	Graa	k1gFnPc4	Graa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Inn	Inn	k1gFnSc7	Inn
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
práva	právo	k1gNnSc2	právo
ovšem	ovšem	k9	ovšem
zanechal	zanechat	k5eAaPmAgMnS	zanechat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
duchovním	duchovní	k2eAgInSc7d1	duchovní
presbyteriánů	presbyterián	k1gMnPc2	presbyterián
v	v	k7c6	v
Chesteru	Chester	k1gInSc6	Chester
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Mare	Mar	k1gInSc2	Mar
Street	Streeta	k1gFnPc2	Streeta
v	v	k7c4	v
Hackney	Hackne	k1gMnPc4	Hackne
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1714	[number]	k4	1714
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
v	v	k7c6	v
Nantwichi	Nantwich	k1gInSc6	Nantwich
v	v	k7c4	v
Queen	Queen	k1gInSc4	Queen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Aid	Aida	k1gFnPc2	Aida
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Chesteru	Chester	k1gInSc2	Chester
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnPc1	práce
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
dílem	díl	k1gInSc7	díl
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
šestidílné	šestidílný	k2eAgFnSc2d1	šestidílná
Exposition	Exposition	k1gInSc4	Exposition
of	of	k?	of
the	the	k?	the
Old	Olda	k1gFnPc2	Olda
and	and	k?	and
New	New	k1gFnSc2	New
Testaments	Testamentsa	k1gFnPc2	Testamentsa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Výklad	výklad	k1gInSc1	výklad
Nového	Nového	k2eAgInSc2d1	Nového
a	a	k8xC	a
Starého	Starého	k2eAgInSc2d1	Starého
Zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
Complete	Comple	k1gNnSc2	Comple
Commentary	Commentara	k1gFnSc2	Commentara
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1706	[number]	k4	1706
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výklad	výklad	k1gInSc1	výklad
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
verš	verš	k1gInSc1	verš
po	po	k7c6	po
verši	verš	k1gInSc6	verš
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Starý	starý	k2eAgInSc1d1	starý
Zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
okomentován	okomentovat	k5eAaPmNgInS	okomentovat
celý	celý	k2eAgInSc1d1	celý
<g/>
,	,	kIx,	,
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
okomentována	okomentován	k2eAgFnSc1d1	okomentován
pouze	pouze	k6eAd1	pouze
evangelia	evangelium	k1gNnPc4	evangelium
a	a	k8xC	a
Skutky	skutek	k1gInPc4	skutek
apoštolské	apoštolský	k2eAgInPc4d1	apoštolský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
třinácti	třináct	k4xCc7	třináct
jinými	jiný	k2eAgMnPc7d1	jiný
autory	autor	k1gMnPc7	autor
dokončen	dokončen	k2eAgInSc4d1	dokončen
komentář	komentář	k1gInSc4	komentář
i	i	k9	i
k	k	k7c3	k
zbytku	zbytek	k1gInSc3	zbytek
Nového	Nového	k2eAgInSc2d1	Nového
Zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
podle	podle	k7c2	podle
poznámek	poznámka	k1gFnPc2	poznámka
Henryho	Henry	k1gMnSc2	Henry
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
exegenetický	exegenetický	k2eAgMnSc1d1	exegenetický
<g/>
,	,	kIx,	,
s	s	k7c7	s
textem	text	k1gInSc7	text
nakládá	nakládat	k5eAaImIp3nS	nakládat
jako	jako	k9	jako
s	s	k7c7	s
hotovým	hotové	k1gNnSc7	hotové
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
vyložit	vyložit	k5eAaPmF	vyložit
pro	pro	k7c4	pro
praktické	praktický	k2eAgInPc4d1	praktický
a	a	k8xC	a
zbožné	zbožný	k2eAgInPc4d1	zbožný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
anglický	anglický	k2eAgInSc4d1	anglický
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
Bibli	bible	k1gFnSc3	bible
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
mnohé	mnohý	k2eAgMnPc4d1	mnohý
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavěli	stavět	k5eAaImAgMnP	stavět
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
evangelikální	evangelikální	k2eAgMnPc1d1	evangelikální
kazatelé	kazatel	k1gMnPc1	kazatel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
George	George	k1gFnSc1	George
Whitefield	Whitefield	k1gMnSc1	Whitefield
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Spurgeon	Spurgeon	k1gMnSc1	Spurgeon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
přečetl	přečíst	k5eAaPmAgMnS	přečíst
Henryho	Henry	k1gMnSc2	Henry
dílo	dílo	k1gNnSc1	dílo
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
každý	každý	k3xTgMnSc1	každý
duchovní	duchovní	k1gMnSc1	duchovní
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
měl	mít	k5eAaImAgMnS	mít
celé	celá	k1gFnSc2	celá
pozorně	pozorně	k6eAd1	pozorně
přečíst	přečíst	k5eAaPmF	přečíst
alespoň	alespoň	k9	alespoň
jednou	jednou	k6eAd1	jednou
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matthew	Matthew	k1gFnSc2	Matthew
Henry	henry	k1gInPc2	henry
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Matthew	Matthew	k1gMnSc1	Matthew
Henry	Henry	k1gMnSc1	Henry
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Commentary	Commentar	k1gMnPc7	Commentar
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Whole	Whole	k1gNnSc1	Whole
Bible	bible	k1gFnSc1	bible
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Matthew	Matthew	k1gMnSc1	Matthew
Henry	Henry	k1gMnSc1	Henry
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
