<p>
<s>
Kanton	Kanton	k1gInSc4	Kanton
Lomme	lomit	k5eAaImRp1nP	lomit
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Lomme	lomit	k5eAaImRp1nP	lomit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
kanton	kanton	k1gInSc4	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Nord	Norda	k1gFnPc2	Norda
v	v	k7c6	v
regionu	region	k1gInSc6	region
Nord-Pas-de-Calais	Nord-Pase-Calais	k1gFnSc2	Nord-Pas-de-Calais
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
ho	on	k3xPp3gNnSc4	on
10	[number]	k4	10
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnSc2	obec
kantonu	kanton	k1gInSc2	kanton
==	==	k?	==
</s>
</p>
<p>
<s>
Beaucamps-Ligny	Beaucamps-Ligna	k1gFnPc1	Beaucamps-Ligna
</s>
</p>
<p>
<s>
Englos	Englos	k1gMnSc1	Englos
</s>
</p>
<p>
<s>
Ennetiè	Ennetiè	k?	Ennetiè
</s>
</p>
<p>
<s>
Erquinghem-le-Sec	Erquingheme-Sec	k1gMnSc1	Erquinghem-le-Sec
</s>
</p>
<p>
<s>
Escobecques	Escobecques	k1gMnSc1	Escobecques
</s>
</p>
<p>
<s>
Hallennes-lez-Haubourdin	Hallennesez-Haubourdin	k2eAgInSc1d1	Hallennes-lez-Haubourdin
</s>
</p>
<p>
<s>
Lomme	lomit	k5eAaImRp1nP	lomit
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
města	město	k1gNnSc2	město
Lille	Lille	k1gFnSc2	Lille
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Maisnil	Maisnil	k1gFnSc1	Maisnil
</s>
</p>
<p>
<s>
Radinghem-en-Weppes	Radinghemn-Weppes	k1gMnSc1	Radinghem-en-Weppes
</s>
</p>
<p>
<s>
Sequedin	Sequedin	k2eAgInSc1d1	Sequedin
</s>
</p>
