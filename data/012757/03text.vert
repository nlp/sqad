<p>
<s>
Milion	milion	k4xCgInSc1	milion
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Il	Il	k1gFnSc1	Il
Milione	milion	k4xCgInSc5	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cestopis	cestopis	k1gInSc1	cestopis
Marca	Marc	k2eAgFnSc1d1	Marca
Pola	pola	k1gFnSc1	pola
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uveřejnění	uveřejnění	k1gNnSc6	uveřejnění
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
popisovala	popisovat	k5eAaImAgFnS	popisovat
exotické	exotický	k2eAgNnSc4d1	exotické
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rytířskými	rytířský	k2eAgInPc7d1	rytířský
příběhy	příběh	k1gInPc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Marco	Marco	k1gMnSc1	Marco
Polo	polo	k6eAd1	polo
diktoval	diktovat	k5eAaImAgMnS	diktovat
příběh	příběh	k1gInSc4	příběh
románovému	románový	k2eAgMnSc3d1	románový
spisovateli	spisovatel	k1gMnSc3	spisovatel
Rustichellovi	Rustichell	k1gMnSc3	Rustichell
z	z	k7c2	z
Pisy	Pisa	k1gFnSc2	Pisa
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1298	[number]	k4	1298
<g/>
–	–	k?	–
<g/>
1299	[number]	k4	1299
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
vydání	vydání	k1gNnSc2	vydání
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
neznám	znám	k2eNgMnSc1d1	neznám
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
dílo	dílo	k1gNnSc4	dílo
tohoto	tento	k3xDgMnSc2	tento
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
příručku	příručka	k1gFnSc4	příručka
obchodní	obchodní	k2eAgFnSc2d1	obchodní
geografie	geografie	k1gFnSc2	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
zde	zde	k6eAd1	zde
najít	najít	k5eAaPmF	najít
popis	popis	k1gInSc4	popis
flóry	flóra	k1gFnSc2	flóra
a	a	k8xC	a
fauny	fauna	k1gFnSc2	fauna
<g/>
,	,	kIx,	,
přírodního	přírodní	k2eAgNnSc2d1	přírodní
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
a	a	k8xC	a
způsoby	způsob	k1gInPc1	způsob
života	život	k1gInSc2	život
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
nekritizuje	kritizovat	k5eNaImIp3nS	kritizovat
všechna	všechen	k3xTgNnPc4	všechen
náboženství	náboženství	k1gNnPc4	náboženství
kromě	kromě	k7c2	kromě
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
zdát	zdát	k5eAaPmF	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
mystifikaci	mystifikace	k1gFnSc4	mystifikace
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
dominikánský	dominikánský	k2eAgMnSc1d1	dominikánský
mnich	mnich	k1gMnSc1	mnich
Francesco	Francesco	k1gMnSc1	Francesco
Pipino	pipina	k1gFnSc5	pipina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Milion	milion	k4xCgInSc1	milion
přeložil	přeložit	k5eAaPmAgMnS	přeložit
z	z	k7c2	z
dialektu	dialekt	k1gInSc2	dialekt
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dílo	dílo	k1gNnSc1	dílo
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
dalších	další	k2eAgMnPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
úpravami	úprava	k1gFnPc7	úprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
posílit	posílit	k5eAaPmF	posílit
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
Milionu	milion	k4xCgInSc2	milion
tedy	tedy	k8xC	tedy
nenajdeme	najít	k5eNaPmIp1nP	najít
žádné	žádný	k3yNgInPc4	žádný
soudy	soud	k1gInPc4	soud
jiných	jiný	k1gMnPc2	jiný
náboženství	náboženství	k1gNnSc2	náboženství
či	či	k8xC	či
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Letovicích	Letovice	k1gFnPc6	Letovice
</s>
</p>
<p>
<s>
==	==	k?	==
Díla	dílo	k1gNnPc4	dílo
inspirovaná	inspirovaný	k2eAgNnPc4d1	inspirované
cestopisem	cestopis	k1gInSc7	cestopis
Milion	milion	k4xCgInSc1	milion
==	==	k?	==
</s>
</p>
<p>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
:	:	kIx,	:
Ken	Ken	k1gMnSc1	Ken
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Giuliano	Giuliana	k1gFnSc5	Giuliana
Montaldo	Montaldo	k1gNnSc1	Montaldo
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Košlerová	Košlerová	k1gFnSc1	Košlerová
<g/>
:	:	kIx,	:
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
cestopisu	cestopis	k1gInSc2	cestopis
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Weinlich	Weinlich	k1gMnSc1	Weinlich
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
:	:	kIx,	:
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Richelmy	Richelm	k1gInPc1	Richelm
<g/>
,	,	kIx,	,
Benedict	Benedict	k2eAgInSc1d1	Benedict
Wong	Wong	k1gInSc1	Wong
<g/>
,	,	kIx,	,
Netflix	Netflix	k1gInSc1	Netflix
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Starší	starý	k2eAgFnSc1d2	starší
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
/	/	kIx~	/
Redaktor	redaktor	k1gMnSc1	redaktor
svazku	svazek	k1gInSc2	svazek
Josef	Josef	k1gMnSc1	Josef
Hrabák	Hrabák	k1gMnSc1	Hrabák
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
531	[number]	k4	531
s.	s.	k?	s.
S.	S.	kA	S.
138	[number]	k4	138
<g/>
,	,	kIx,	,
154	[number]	k4	154
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OPELÍK	OPELÍK	kA	OPELÍK
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
M	M	kA	M
<g/>
–	–	k?	–
<g/>
O.	O.	kA	O.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
728	[number]	k4	728
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
708	[number]	k4	708
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
277	[number]	k4	277
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POLO	pola	k1gFnSc5	pola
<g/>
,	,	kIx,	,
Marco	Marca	k1gFnSc5	Marca
<g/>
.	.	kIx.	.
</s>
<s>
Marka	marka	k1gFnSc1	marka
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
Milion	milion	k4xCgInSc1	milion
:	:	kIx,	:
o	o	k7c6	o
záležitostech	záležitost	k1gFnPc6	záležitost
Tatarů	Tatar	k1gMnPc2	Tatar
a	a	k8xC	a
Východních	východní	k2eAgFnPc2d1	východní
Indií	Indie	k1gFnPc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Justin	Justina	k1gFnPc2	Justina
Václav	Václav	k1gMnSc1	Václav
Prášek	Prášek	k1gMnSc1	Prášek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
slovesnost	slovesnost	k1gFnSc1	slovesnost
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
305	[number]	k4	305
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Milion	milion	k4xCgInSc1	milion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
<g/>
:	:	kIx,	:
Milion	milion	k4xCgInSc1	milion
–	–	k?	–
starší	starý	k2eAgInSc4d2	starší
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
cestopisu	cestopis	k1gInSc2	cestopis
</s>
</p>
<p>
<s>
Četba	četba	k1gFnSc1	četba
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
:	:	kIx,	:
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
–	–	k?	–
Milión	milión	k4xCgInSc1	milión
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
záležitostech	záležitost	k1gFnPc6	záležitost
Tatarů	Tatar	k1gMnPc2	Tatar
a	a	k8xC	a
východních	východní	k2eAgInPc2d1	východní
Indií	Indie	k1gFnSc7	Indie
–	–	k?	–
pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
