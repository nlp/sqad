<s>
Další	další	k2eAgFnSc7d1	další
pamětihodností	pamětihodnost	k1gFnSc7	pamětihodnost
v	v	k7c6	v
Paršovicích	Paršovice	k1gFnPc6	Paršovice
je	být	k5eAaImIp3nS	být
školní	školní	k2eAgFnSc1d1	školní
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
škola	škola	k1gFnSc1	škola
císaře	císař	k1gMnSc2	císař
pána	pán	k1gMnSc2	pán
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
"	"	kIx"	"
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
mužského	mužský	k2eAgNnSc2d1	mužské
jména	jméno	k1gNnSc2	jméno
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
–	–	k?	–
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
staročesky	staročesky	k6eAd1	staročesky
Pareš	Pareš	k1gFnSc1	Pareš
<g/>
.	.	kIx.	.
</s>
