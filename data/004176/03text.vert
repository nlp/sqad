<s>
Knoflíkáři	knoflíkář	k1gMnSc3	knoflíkář
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
režiséra	režisér	k1gMnSc2	režisér
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
vlastního	vlastní	k2eAgInSc2d1	vlastní
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
scénáře	scénář	k1gInSc2	scénář
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
šest	šest	k4xCc4	šest
povídek	povídka	k1gFnPc2	povídka
<g/>
:	:	kIx,	:
Štěstí	štěstí	k1gNnSc1	štěstí
z	z	k7c2	z
Kokury	Kokura	k1gFnSc2	Kokura
<g/>
,	,	kIx,	,
Taxíkář	taxíkář	k1gMnSc1	taxíkář
<g/>
,	,	kIx,	,
Civilizační	civilizační	k2eAgInPc4d1	civilizační
návyky	návyk	k1gInPc4	návyk
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
slušná	slušný	k2eAgFnSc1d1	slušná
generace	generace	k1gFnPc1	generace
<g/>
,	,	kIx,	,
Pitomci	pitomec	k1gMnPc1	pitomec
<g/>
,	,	kIx,	,
Duch	duch	k1gMnSc1	duch
amerického	americký	k2eAgMnSc2d1	americký
pilota	pilot	k1gMnSc2	pilot
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
navzájem	navzájem	k6eAd1	navzájem
provázaných	provázaný	k2eAgFnPc6d1	provázaná
povídkách	povídka	k1gFnPc6	povídka
jsou	být	k5eAaImIp3nP	být
ukázány	ukázat	k5eAaPmNgInP	ukázat
různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
lidiček	lidičky	k1gMnPc2	lidičky
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
životní	životní	k2eAgInPc1d1	životní
osudy	osud	k1gInPc1	osud
–	–	k?	–
od	od	k7c2	od
taxikáře	taxikář	k1gMnSc2	taxikář
s	s	k7c7	s
nevěrnou	věrný	k2eNgFnSc7d1	nevěrná
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
psychologa	psycholog	k1gMnSc4	psycholog
s	s	k7c7	s
každodenními	každodenní	k2eAgInPc7d1	každodenní
rituály	rituál	k1gInPc7	rituál
<g/>
,	,	kIx,	,
pitomce	pitomec	k1gMnSc4	pitomec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umí	umět	k5eAaImIp3nS	umět
doplivnout	doplivnout	k5eAaPmF	doplivnout
na	na	k7c4	na
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
,	,	kIx,	,
až	až	k8xS	až
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pomocí	pomocí	k7c2	pomocí
umělého	umělý	k2eAgInSc2d1	umělý
chrupu	chrup	k1gInSc2	chrup
v	v	k7c6	v
zadnici	zadnice	k1gFnSc6	zadnice
ucvakávají	ucvakávat	k5eAaPmIp3nP	ucvakávat
knoflíky	knoflík	k1gInPc7	knoflík
z	z	k7c2	z
gaučů	gauč	k1gInPc2	gauč
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
Tverpové	Tverp	k1gMnPc1	Tverp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
především	především	k9	především
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
o	o	k7c6	o
atomové	atomový	k2eAgFnSc6d1	atomová
bombě	bomba	k1gFnSc6	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Knoflíkáři	knoflíkář	k1gMnPc1	knoflíkář
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Knoflíkáři	knoflíkář	k1gMnPc1	knoflíkář
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc5	Databas
Knoflíkáři	knoflíkář	k1gMnSc6	knoflíkář
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
