<s>
Knoflíkáři	knoflíkář	k1gMnSc3	knoflíkář
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
režiséra	režisér	k1gMnSc2	režisér
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
vlastního	vlastní	k2eAgInSc2d1	vlastní
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
scénáře	scénář	k1gInSc2	scénář
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
