<p>
<s>
Ploutvenky	Ploutvenka	k1gFnPc1	Ploutvenka
(	(	kIx(	(
<g/>
Chaetognatha	Chaetognatha	k1gFnSc1	Chaetognatha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kmen	kmen	k1gInSc1	kmen
mořských	mořský	k2eAgMnPc2d1	mořský
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
0,5	[number]	k4	0,5
–	–	k?	–
10	[number]	k4	10
cm	cm	kA	cm
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
trup	trup	k1gInSc4	trup
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
plavání	plavání	k1gNnSc4	plavání
jim	on	k3xPp3gMnPc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
horizontální	horizontální	k2eAgFnPc1d1	horizontální
ploutvičky	ploutvička	k1gFnPc1	ploutvička
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
koncová	koncový	k2eAgFnSc1d1	koncová
a	a	k8xC	a
několik	několik	k4yIc1	několik
postranních	postranní	k2eAgFnPc2d1	postranní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
mají	mít	k5eAaImIp3nP	mít
chitinové	chitinový	k2eAgInPc1d1	chitinový
ostny	osten	k1gInPc1	osten
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
lapání	lapání	k1gNnSc3	lapání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
nebo	nebo	k8xC	nebo
celá	celý	k2eAgFnSc1d1	celá
hlava	hlava	k1gFnSc1	hlava
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
zakryty	zakryt	k2eAgFnPc4d1	zakryta
kápí	kápě	k1gFnSc7	kápě
(	(	kIx(	(
<g/>
hood	hood	k6eAd1	hood
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ploutvenky	Ploutvenka	k1gFnPc4	Ploutvenka
jsou	být	k5eAaImIp3nP	být
dravci	dravec	k1gMnPc1	dravec
<g/>
,	,	kIx,	,
kořist	kořist	k1gFnSc4	kořist
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
tetrodotoxinem	tetrodotoxin	k1gInSc7	tetrodotoxin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
hlavě	hlava	k1gFnSc6	hlava
produkují	produkovat	k5eAaImIp3nP	produkovat
symbiotické	symbiotický	k2eAgFnPc1d1	symbiotická
bakterie	bakterie	k1gFnPc1	bakterie
rodu	rod	k1gInSc2	rod
Vibrio	vibrio	k1gNnSc1	vibrio
<g/>
.	.	kIx.	.
</s>
<s>
Vylučovací	vylučovací	k2eAgFnSc1d1	vylučovací
soustava	soustava	k1gFnSc1	soustava
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
cévní	cévní	k2eAgFnSc1d1	cévní
soustava	soustava	k1gFnSc1	soustava
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
hermafroditi	hermafrodit	k1gMnPc1	hermafrodit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
unikátní	unikátní	k2eAgInSc4d1	unikátní
embryonální	embryonální	k2eAgInSc4d1	embryonální
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
heterocelie	heterocelie	k1gFnSc1	heterocelie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
==	==	k?	==
</s>
</p>
<p>
<s>
Příbuzenské	příbuzenský	k2eAgInPc4d1	příbuzenský
vztahy	vztah	k1gInPc4	vztah
ploutvenek	ploutvenka	k1gFnPc2	ploutvenka
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
skupinami	skupina	k1gFnPc7	skupina
živočichů	živočich	k1gMnPc2	živočich
byly	být	k5eAaImAgFnP	být
dlouho	dlouho	k6eAd1	dlouho
nejasné	jasný	k2eNgFnPc1d1	nejasná
nejasné	jasný	k2eNgFnPc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
druhoústé	druhoústý	k2eAgNnSc4d1	druhoústý
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
molekulárních	molekulární	k2eAgNnPc2d1	molekulární
studií	studio	k1gNnPc2	studio
naznačovaly	naznačovat	k5eAaImAgInP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Ecdysozoa	Ecdysozoum	k1gNnSc2	Ecdysozoum
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Hox	Hox	k1gFnSc2	Hox
genů	gen	k1gInPc2	gen
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
bazální	bazální	k2eAgFnSc4d1	bazální
skupinu	skupina	k1gFnSc4	skupina
Eubilaterií	Eubilaterie	k1gFnPc2	Eubilaterie
nebo	nebo	k8xC	nebo
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
prvoústých	prvoústý	k2eAgMnPc2d1	prvoústý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
řazené	řazený	k2eAgFnPc1d1	řazená
jako	jako	k8xS	jako
bazální	bazální	k2eAgFnSc1d1	bazální
skupina	skupina	k1gFnSc1	skupina
prvoústých	prvoústý	k2eAgFnPc2d1	prvoústý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
před	před	k7c7	před
oddělením	oddělení	k1gNnSc7	oddělení
skupin	skupina	k1gFnPc2	skupina
Lophotrochozoa	Lophotrochozo	k1gInSc2	Lophotrochozo
a	a	k8xC	a
Ecdysozoa	Ecdysozo	k1gInSc2	Ecdysozo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nových	nový	k2eAgFnPc2d1	nová
prací	práce	k1gFnPc2	práce
by	by	kYmCp3nP	by
však	však	k9	však
mohly	moct	k5eAaImAgFnP	moct
patřit	patřit	k5eAaImF	patřit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Lophotrochozoa	Lophotrochozo	k1gInSc2	Lophotrochozo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Hox	Hox	k1gFnSc2	Hox
genů	gen	k1gInPc2	gen
do	do	k7c2	do
kladu	klad	k1gInSc2	klad
čelistovci	čelistovec	k1gMnPc5	čelistovec
(	(	kIx(	(
<g/>
Gnathifera	Gnathifera	k1gFnSc1	Gnathifera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
ploutvenkám	ploutvenka	k1gFnPc3	ploutvenka
býval	bývat	k5eAaImAgInS	bývat
řazen	řazen	k2eAgInSc1d1	řazen
kambrický	kambrický	k2eAgInSc1d1	kambrický
rod	rod	k1gInSc1	rod
z	z	k7c2	z
Burgesských	Burgesský	k2eAgFnPc2d1	Burgesský
břidlic	břidlice	k1gFnPc2	břidlice
Amiskwia	Amiskwia	k1gFnSc1	Amiskwia
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
Archisagittoidea	Archisagittoidea	k1gMnSc1	Archisagittoidea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
není	být	k5eNaImIp3nS	být
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novějších	nový	k2eAgInPc2d2	novější
názorů	názor	k1gInPc2	názor
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
blízký	blízký	k2eAgMnSc1d1	blízký
pásnicím	pásnice	k1gFnPc3	pásnice
(	(	kIx(	(
<g/>
Nemertea	Nemertea	k1gFnSc1	Nemertea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Recentní	recentní	k2eAgFnPc1d1	recentní
ploutvenky	ploutvenka	k1gFnPc1	ploutvenka
tvoří	tvořit	k5eAaImIp3nP	tvořit
třídu	třída	k1gFnSc4	třída
Sagittoidea	Sagittoide	k1gInSc2	Sagittoide
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
řády	řád	k1gInPc7	řád
–	–	k?	–
Aphragmophora	Aphragmophor	k1gMnSc4	Aphragmophor
a	a	k8xC	a
Phragmophora	Phragmophor	k1gMnSc4	Phragmophor
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
molekulárních	molekulární	k2eAgFnPc6d1	molekulární
metodách	metoda	k1gFnPc6	metoda
(	(	kIx(	(
<g/>
analýze	analýza	k1gFnSc3	analýza
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Papillon	Papillon	k1gInSc4	Papillon
a	a	k8xC	a
spol	spol	k1gInSc4	spol
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Sagittoidea	Sagittoidea	k1gFnSc1	Sagittoidea
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Aphragmophora	Aphragmophora	k1gFnSc1	Aphragmophora
</s>
</p>
<p>
<s>
Podřád	podřád	k1gInSc1	podřád
<g/>
:	:	kIx,	:
Ctinodontina	Ctinodontina	k1gFnSc1	Ctinodontina
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Bathybelosidae	Bathybelosidae	k1gFnSc1	Bathybelosidae
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Pterosagittidae	Pterosagittidae	k1gFnSc1	Pterosagittidae
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Sagittidae	Sagittidae	k1gFnSc1	Sagittidae
</s>
</p>
<p>
<s>
Podřád	podřád	k1gInSc1	podřád
<g/>
:	:	kIx,	:
Flabellodontina	Flabellodontina	k1gFnSc1	Flabellodontina
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Krohnittidae	Krohnittidae	k1gFnSc1	Krohnittidae
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Phragmophora	Phragmophora	k1gFnSc1	Phragmophora
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Bathyspadellidae	Bathyspadellidae	k1gFnSc1	Bathyspadellidae
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Eukrohniidae	Eukrohniidae	k1gFnSc1	Eukrohniidae
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Krohnittellidae	Krohnittellidae	k1gFnSc1	Krohnittellidae
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Spadellidae	Spadellidae	k1gFnSc1	Spadellidae
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ploutvenky	Ploutvenka	k1gFnSc2	Ploutvenka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Chaetognatha	Chaetognath	k1gMnSc2	Chaetognath
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
