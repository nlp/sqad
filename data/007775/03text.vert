<s>
Želva	želva	k1gFnSc1	želva
sloní	slonit	k5eAaImIp3nS	slonit
(	(	kIx(	(
<g/>
Geochelone	Geochelon	k1gInSc5	Geochelon
nigra	nigr	k1gMnSc2	nigr
complex	complex	k1gInSc1	complex
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
želvou	želva	k1gFnSc7	želva
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
největší	veliký	k2eAgFnSc7d3	veliký
žijící	žijící	k2eAgMnPc1d1	žijící
pozemní	pozemní	k2eAgMnPc1d1	pozemní
druh	druh	k1gMnSc1	druh
želvy	želva	k1gFnSc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Chelonoidis	Chelonoidis	k1gFnSc7	Chelonoidis
nigra	nigr	k1gMnSc2	nigr
Harlan	Harlana	k1gFnPc2	Harlana
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
Želva	želva	k1gFnSc1	želva
sloní	sloň	k1gFnPc2	sloň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
1,2	[number]	k4	1,2
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
200	[number]	k4	200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
krunýře	krunýř	k1gInSc2	krunýř
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
poddruhu	poddruh	k1gInSc2	poddruh
želvy	želva	k1gFnSc2	želva
<g/>
:	:	kIx,	:
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
krunýř	krunýř	k1gInSc4	krunýř
sedlovitý	sedlovitý	k2eAgInSc4d1	sedlovitý
a	a	k8xC	a
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
klenutý	klenutý	k2eAgInSc1d1	klenutý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zvednout	zvednout	k5eAaPmF	zvednout
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c4	na
vyšší	vysoký	k2eAgInPc4d2	vyšší
zdroje	zdroj	k1gInPc4	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
mají	mít	k5eAaImIp3nP	mít
krunýř	krunýř	k1gInSc1	krunýř
klenutý	klenutý	k2eAgInSc1d1	klenutý
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
nižšími	nízký	k2eAgFnPc7d2	nižší
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
černě	černě	k6eAd1	černě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
robustní	robustní	k2eAgFnPc1d1	robustní
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkými	krátký	k2eAgInPc7d1	krátký
prsty	prst	k1gInPc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
endemit	endemit	k1gInSc1	endemit
souostroví	souostroví	k1gNnSc2	souostroví
Galapágy	Galapágy	k1gFnPc1	Galapágy
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
tak	tak	k9	tak
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
úrodné	úrodný	k2eAgFnSc6d1	úrodná
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
také	také	k9	také
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
sopečném	sopečný	k2eAgInSc6d1	sopečný
popelu	popel	k1gInSc6	popel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
výhřevný	výhřevný	k2eAgInSc1d1	výhřevný
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
tráví	trávit	k5eAaImIp3nP	trávit
pastvou	pastva	k1gFnSc7	pastva
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
vyhříváním	vyhřívání	k1gNnPc3	vyhřívání
se	se	k3xPyFc4	se
v	v	k7c6	v
loužích	louž	k1gFnPc6	louž
nebo	nebo	k8xC	nebo
válením	válení	k1gNnSc7	válení
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
samci	samec	k1gMnPc1	samec
stávají	stávat	k5eAaImIp3nP	stávat
teritoriální	teritoriální	k2eAgMnPc1d1	teritoriální
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
partnerky	partnerka	k1gFnPc4	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
zásnub	zásnuba	k1gFnPc2	zásnuba
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
želv	želva	k1gFnPc2	želva
sloních	sloní	k2eAgFnPc2d1	sloní
nekompromisní	kompromisní	k2eNgFnSc7d1	nekompromisní
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
samec	samec	k1gMnSc1	samec
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
příhodnou	příhodný	k2eAgFnSc4d1	příhodná
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
drsně	drsně	k6eAd1	drsně
jí	on	k3xPp3gFnSc3	on
vnutí	vnutit	k5eAaPmIp3nS	vnutit
podřízené	podřízený	k2eAgNnSc1d1	podřízené
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
kouše	kousat	k5eAaImIp3nS	kousat
a	a	k8xC	a
štípe	štípat	k5eAaImIp3nS	štípat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
přinutil	přinutit	k5eAaPmAgMnS	přinutit
k	k	k7c3	k
nehybnosti	nehybnost	k1gFnSc3	nehybnost
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
vysune	vysunout	k5eAaPmIp3nS	vysunout
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vejcorodé	vejcorodý	k2eAgFnPc1d1	vejcorodá
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jámy	jáma	k1gFnSc2	jáma
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
pak	pak	k6eAd1	pak
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
některé	některý	k3yIgFnPc4	některý
želvy	želva	k1gFnPc4	želva
sloní	sloň	k1gFnPc2	sloň
žijí	žít	k5eAaImIp3nP	žít
zaručeně	zaručeně	k6eAd1	zaručeně
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ochrany	ochrana	k1gFnSc2	ochrana
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
existenci	existence	k1gFnSc6	existence
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
želvy	želva	k1gFnPc1	želva
jsou	být	k5eAaImIp3nP	být
ohrožovány	ohrožovat	k5eAaImNgFnP	ohrožovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
věku	věk	k1gInSc6	věk
introdukovanými	introdukovaný	k2eAgNnPc7d1	introdukovaný
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
potkany	potkan	k1gMnPc7	potkan
<g/>
,	,	kIx,	,
kočkami	kočka	k1gFnPc7	kočka
a	a	k8xC	a
potravní	potravní	k2eAgFnSc7d1	potravní
konkurencí	konkurence	k1gFnSc7	konkurence
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
stanice	stanice	k1gFnSc1	stanice
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
Research	Research	k1gMnSc1	Research
Station	station	k1gInSc1	station
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
programu	program	k1gInSc2	program
a	a	k8xC	a
repatriace	repatriace	k1gFnSc2	repatriace
želv	želva	k1gFnPc2	želva
sloních	sloní	k2eAgFnPc2d1	sloní
pozvednout	pozvednout	k5eAaPmF	pozvednout
jejich	jejich	k3xOp3gFnSc4	jejich
upadávající	upadávající	k2eAgFnSc4d1	upadávající
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
1000	[number]	k4	1000
želv	želva	k1gFnPc2	želva
sloních	sloní	k2eAgFnPc2d1	sloní
<g/>
.	.	kIx.	.
</s>
<s>
G.	G.	kA	G.
abingdoni	abingdoň	k1gFnSc6	abingdoň
(	(	kIx(	(
<g/>
Gunther	Gunthra	k1gFnPc2	Gunthra
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
G.	G.	kA	G.
becki	beck	k1gFnSc2	beck
(	(	kIx(	(
<g/>
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
G.	G.	kA	G.
darwini	darwin	k1gMnPc1	darwin
(	(	kIx(	(
<g/>
Van	van	k1gInSc1	van
Denburgh	Denburgha	k1gFnPc2	Denburgha
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
G.	G.	kA	G.
ephippium	ephippium	k1gNnSc1	ephippium
(	(	kIx(	(
<g/>
Gunther	Gunthra	k1gFnPc2	Gunthra
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
G.	G.	kA	G.
galapagoensis	galapagoensis	k1gFnSc1	galapagoensis
(	(	kIx(	(
<g/>
Baur	Baur	k1gMnSc1	Baur
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
G.	G.	kA	G.
guntheri	gunther	k1gFnSc2	gunther
(	(	kIx(	(
<g/>
Baur	Baur	k1gMnSc1	Baur
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
G.	G.	kA	G.
hoodensis	hoodensis	k1gFnSc1	hoodensis
(	(	kIx(	(
<g/>
Van	van	k1gInSc1	van
Denburgh	Denburgha	k1gFnPc2	Denburgha
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
G.	G.	kA	G.
chathamensis	chathamensis	k1gFnSc1	chathamensis
(	(	kIx(	(
<g/>
Van	van	k1gInSc1	van
Denburgh	Denburgha	k1gFnPc2	Denburgha
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
G.	G.	kA	G.
micropyes	micropyesa	k1gFnPc2	micropyesa
(	(	kIx(	(
<g/>
Gunther	Gunthra	k1gFnPc2	Gunthra
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
G.	G.	kA	G.
nigra	nigr	k1gMnSc2	nigr
(	(	kIx(	(
<g/>
Harlan	Harlan	k1gMnSc1	Harlan
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
G.	G.	kA	G.
phantastica	phantastica	k1gFnSc1	phantastica
(	(	kIx(	(
<g/>
Van	van	k1gInSc1	van
Denburgh	Denburgha	k1gFnPc2	Denburgha
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
G.	G.	kA	G.
porteri	porter	k1gFnSc2	porter
(	(	kIx(	(
<g/>
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
G.	G.	kA	G.
vandenburghi	vandenburgh	k1gFnSc6	vandenburgh
(	(	kIx(	(
<g/>
de	de	k?	de
Sola	Sola	k1gMnSc1	Sola
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Želvu	želva	k1gFnSc4	želva
sloní	sloň	k1gFnPc2	sloň
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
chová	chovat	k5eAaImIp3nS	chovat
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
Velké	velký	k2eAgFnSc2d1	velká
želvy	želva	k1gFnSc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
Želva	želva	k1gFnSc1	želva
sloní	slonit	k5eAaImIp3nS	slonit
santacruzská	santacruzský	k2eAgFnSc1d1	santacruzský
<g/>
,	,	kIx,	,
Želva	želva	k1gFnSc1	želva
sloní	slonit	k5eAaImIp3nS	slonit
pinzónská	pinzónský	k2eAgFnSc1d1	pinzónský
a	a	k8xC	a
také	také	k9	také
Želva	želva	k1gFnSc1	želva
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Harriet	Harriet	k1gInSc1	Harriet
(	(	kIx(	(
<g/>
želva	želva	k1gFnSc1	želva
<g/>
)	)	kIx)	)
Želva	želva	k1gFnSc1	želva
obrovská	obrovský	k2eAgFnSc1d1	obrovská
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Lexikon	lexikon	k1gNnSc1	lexikon
zvířat	zvíře	k1gNnPc2	zvíře
-	-	kIx~	-
Želva	želva	k1gFnSc1	želva
sloní	sloň	k1gFnPc2	sloň
(	(	kIx(	(
<g/>
Geochelone	Geochelon	k1gInSc5	Geochelon
nigra	nigr	k1gMnSc2	nigr
<g/>
)	)	kIx)	)
Fernando	Fernanda	k1gFnSc5	Fernanda
<g/>
,	,	kIx,	,
C.	C.	kA	C.
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Geochelone	Geochelon	k1gInSc5	Geochelon
nigra	nigr	k1gMnSc4	nigr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
On-line	Onin	k1gInSc5	On-lin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc1	Diversit
Web	web	k1gInSc4	web
<g/>
.	.	kIx.	.
</s>
<s>
Accessed	Accessed	k1gMnSc1	Accessed
November	November	k1gMnSc1	November
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
