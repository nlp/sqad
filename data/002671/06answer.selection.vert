<s>
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Mährisch	Mährisch	k1gInSc1	Mährisch
Kromau	Kromaus	k1gInSc2	Kromaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Znojmo	Znojmo	k1gNnSc4	Znojmo
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
27	[number]	k4	27
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Rokytná	Rokytný	k2eAgFnSc1d1	Rokytná
<g/>
.	.	kIx.	.
</s>
