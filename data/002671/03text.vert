<s>
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Mährisch	Mährisch	k1gInSc1	Mährisch
Kromau	Kromaus	k1gInSc2	Kromaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Znojmo	Znojmo	k1gNnSc4	Znojmo
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
27	[number]	k4	27
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Rokytná	Rokytný	k2eAgFnSc1d1	Rokytná
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
49,57	[number]	k4	49,57
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
těžké	těžký	k2eAgNnSc4d1	těžké
poškození	poškození	k1gNnSc4	poškození
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
vinařské	vinařský	k2eAgFnSc6d1	vinařská
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
viniční	viniční	k2eAgFnSc2d1	viniční
tratě	trať	k1gFnSc2	trať
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Jalovcová	jalovcový	k2eAgFnSc1d1	Jalovcová
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
vysočinou	vysočina	k1gFnSc7	vysočina
a	a	k8xC	a
Dyjsko-svrateckým	dyjskovratecký	k2eAgInSc7d1	dyjsko-svratecký
úvalem	úval	k1gInSc7	úval
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obtékána	obtékat	k5eAaImNgFnS	obtékat
řekou	řeka	k1gFnSc7	řeka
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
meandr	meandr	k1gInSc1	meandr
řeky	řeka	k1gFnSc2	řeka
Rokytné	Rokytný	k2eAgFnSc2d1	Rokytná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
Moravskokrumlovska	Moravskokrumlovsko	k1gNnSc2	Moravskokrumlovsko
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jedněm	jeden	k4xCgInPc3	jeden
ze	z	k7c2	z
stabilně	stabilně	k6eAd1	stabilně
osídleným	osídlený	k2eAgNnSc7d1	osídlené
územím	území	k1gNnSc7	území
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc1	doklad
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
bohaté	bohatý	k2eAgNnSc1d1	bohaté
archeologické	archeologický	k2eAgNnSc1d1	Archeologické
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
obdobích	období	k1gNnPc6	období
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgNnSc1d1	středověké
osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
Rokyten	Rokytno	k1gNnPc2	Rokytno
v	v	k7c6	v
Rokytné	Rokytný	k2eAgFnSc6d1	Rokytná
<g/>
,	,	kIx,	,
osídlení	osídlení	k1gNnSc1	osídlení
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
archeologickými	archeologický	k2eAgInPc7d1	archeologický
nálezy	nález	k1gInPc7	nález
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
panství	panství	k1gNnSc4	panství
a	a	k8xC	a
hradu	hrad	k1gInSc2	hrad
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1289	[number]	k4	1289
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založen	k2eAgNnSc1d1	založeno
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
opěrný	opěrný	k2eAgInSc1d1	opěrný
bod	bod	k1gInSc1	bod
jeho	jeho	k3xOp3gFnSc2	jeho
domény	doména	k1gFnSc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
jméno	jméno	k1gNnSc1	jméno
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
v	v	k7c6	v
latinsky	latinsky	k6eAd1	latinsky
psaném	psaný	k2eAgInSc6d1	psaný
listě	list	k1gInSc6	list
Rudolfa	Rudolf	k1gMnSc4	Rudolf
Habsburského	habsburský	k2eAgMnSc4d1	habsburský
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1277	[number]	k4	1277
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
Moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
přidán	přidán	k2eAgMnSc1d1	přidán
kvůli	kvůli	k7c3	kvůli
odlišení	odlišení	k1gNnSc3	odlišení
od	od	k7c2	od
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
krumm	krumm	k1gInSc1	krumm
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
křivý	křivý	k2eAgInSc1d1	křivý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
širokého	široký	k2eAgInSc2d1	široký
meandru	meandr	k1gInSc2	meandr
řeky	řeka	k1gFnSc2	řeka
Rokytné	Rokytný	k2eAgFnSc2d1	Rokytná
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1289	[number]	k4	1289
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
majetkem	majetek	k1gInSc7	majetek
Bočka	Boček	k1gMnSc2	Boček
z	z	k7c2	z
Obřan	Obřana	k1gFnPc2	Obřana
<g/>
.	.	kIx.	.
</s>
<s>
Smrtí	smrtit	k5eAaImIp3nS	smrtit
jeho	on	k3xPp3gMnSc4	on
vnuka	vnuk	k1gMnSc4	vnuk
Smila	Smil	k1gMnSc4	Smil
z	z	k7c2	z
Obřan	Obřana	k1gFnPc2	Obřana
roku	rok	k1gInSc2	rok
1312	[number]	k4	1312
jejich	jejich	k3xOp3gNnSc2	jejich
rod	rod	k1gInSc1	rod
vymřel	vymřít	k5eAaPmAgInS	vymřít
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
přešel	přejít	k5eAaPmAgInS	přejít
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jej	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1312	[number]	k4	1312
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
rodům	rod	k1gInPc3	rod
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1319	[number]	k4	1319
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
stal	stát	k5eAaPmAgMnS	stát
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
maršálkem	maršálek	k1gMnSc7	maršálek
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
úřad	úřad	k1gInSc4	úřad
zastávali	zastávat	k5eAaImAgMnP	zastávat
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
a	a	k8xC	a
Moravský	moravský	k2eAgInSc4d1	moravský
Krumlov	Krumlov	k1gInSc4	Krumlov
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
jejich	jejich	k3xOp3gFnPc3	jejich
hlavním	hlavní	k2eAgFnPc3d1	hlavní
mocenským	mocenský	k2eAgFnPc3d1	mocenská
a	a	k8xC	a
hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
oporám	opora	k1gFnPc3	opora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1368	[number]	k4	1368
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
město	město	k1gNnSc4	město
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
Ješek	Ješek	k6eAd1	Ješek
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
ho	on	k3xPp3gInSc4	on
zdědil	zdědit	k5eAaPmAgMnS	zdědit
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Beneš	Beneš	k1gMnSc1	Beneš
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
drželi	držet	k5eAaImAgMnP	držet
Moravský	moravský	k2eAgInSc4d1	moravský
Krumlov	Krumlov	k1gInSc4	Krumlov
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1437	[number]	k4	1437
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
získali	získat	k5eAaPmAgMnP	získat
zpět	zpět	k6eAd1	zpět
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
jej	on	k3xPp3gMnSc4	on
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
věhlasný	věhlasný	k2eAgMnSc1d1	věhlasný
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Paracelsus	Paracelsus	k1gMnSc1	Paracelsus
<g/>
.	.	kIx.	.
</s>
<s>
Sídlil	sídlit	k5eAaImAgMnS	sídlit
v	v	k7c6	v
zámecké	zámecký	k2eAgFnSc6d1	zámecká
věži	věž	k1gFnSc6	věž
a	a	k8xC	a
léčil	léčit	k5eAaImAgMnS	léčit
Jana	Jan	k1gMnSc4	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jej	on	k3xPp3gNnSc4	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
uzdravit	uzdravit	k5eAaPmF	uzdravit
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
zde	zde	k6eAd1	zde
však	však	k9	však
několik	několik	k4yIc4	několik
významných	významný	k2eAgInPc2d1	významný
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
např.	např.	kA	např.
třetí	třetí	k4xOgInSc4	třetí
svazek	svazek	k1gInSc4	svazek
díla	dílo	k1gNnSc2	dílo
Die	Die	k1gFnSc2	Die
grosse	grosse	k1gFnSc2	grosse
Wundersarznei	Wundersarzne	k1gFnSc2	Wundersarzne
a	a	k8xC	a
Astrologia	Astrologium	k1gNnSc2	Astrologium
magna	magn	k1gInSc2	magn
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
ošetřoval	ošetřovat	k5eAaImAgInS	ošetřovat
Čeňka	Čeněk	k1gMnSc4	Čeněk
z	z	k7c2	z
Lipé	Lipé	k1gNnSc2	Lipé
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
Johannes	Johannes	k1gMnSc1	Johannes
Crato	Crato	k1gNnSc4	Crato
z	z	k7c2	z
Kraftheimu	Kraftheim	k1gInSc2	Kraftheim
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
<g/>
-	-	kIx~	-
<g/>
1585	[number]	k4	1585
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
císařů	císař	k1gMnPc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
..	..	k?	..
Za	za	k7c2	za
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
renesančně	renesančně	k6eAd1	renesančně
přestavěn	přestavět	k5eAaPmNgInS	přestavět
s	s	k7c7	s
arkádovým	arkádový	k2eAgNnSc7d1	arkádové
nádvořím	nádvoří	k1gNnSc7	nádvoří
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
podlažích	podlaží	k1gNnPc6	podlaží
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
italského	italský	k2eAgMnSc2d1	italský
architekta	architekt	k1gMnSc2	architekt
Leonarda	Leonardo	k1gMnSc2	Leonardo
Gara	Garus	k1gMnSc2	Garus
di	di	k?	di
Bisono	Bisona	k1gFnSc5	Bisona
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
vlastníkem	vlastník	k1gMnSc7	vlastník
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
z	z	k7c2	z
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
byl	být	k5eAaImAgMnS	být
Pertold	Pertold	k1gMnSc1	Pertold
Bohobud	Bohobud	k1gMnSc1	Bohobud
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
rozmařilého	rozmařilý	k2eAgInSc2d1	rozmařilý
života	život	k1gInSc2	život
upadlo	upadnout	k5eAaPmAgNnS	upadnout
panství	panství	k1gNnSc1	panství
do	do	k7c2	do
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
byly	být	k5eAaImAgFnP	být
chovány	chován	k2eAgFnPc1d1	chována
desítky	desítka	k1gFnPc1	desítka
ušlechtilých	ušlechtilý	k2eAgMnPc2d1	ušlechtilý
koní	kůň	k1gMnPc2	kůň
i	i	k8xC	i
exotických	exotický	k2eAgNnPc2d1	exotické
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tu	tu	k6eAd1	tu
krytá	krytý	k2eAgFnSc1d1	krytá
jízdárna	jízdárna	k1gFnSc1	jízdárna
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
obora	obora	k1gFnSc1	obora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgMnS	být
Pertold	Pertold	k1gMnSc1	Pertold
jako	jako	k8xS	jako
moravský	moravský	k2eAgMnSc1d1	moravský
direktor	direktor	k1gMnSc1	direktor
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
konfiskován	konfiskován	k2eAgInSc1d1	konfiskován
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozsudkem	rozsudek	k1gInSc7	rozsudek
však	však	k8xC	však
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
žebrák	žebrák	k1gMnSc1	žebrák
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
koupil	koupit	k5eAaPmAgInS	koupit
krumlovský	krumlovský	k2eAgInSc1d1	krumlovský
majetek	majetek	k1gInSc1	majetek
od	od	k7c2	od
císařské	císařský	k2eAgFnSc2d1	císařská
komory	komora	k1gFnSc2	komora
za	za	k7c4	za
600	[number]	k4	600
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
zlatých	zlatý	k2eAgFnPc2d1	zlatá
Gundakar	Gundakara	k1gFnPc2	Gundakara
z	z	k7c2	z
Liechtensteina	Liechtenstein	k1gMnSc2	Liechtenstein
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnům	Lichtenštejn	k1gMnPc3	Lichtenštejn
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
krumlovské	krumlovský	k2eAgNnSc1d1	krumlovské
panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c2	za
jejich	jejich	k3xOp3gInSc2	jejich
dědičné	dědičný	k2eAgNnSc1d1	dědičné
knížectví	knížectví	k1gNnSc1	knížectví
(	(	kIx(	(
<g/>
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
pak	pak	k6eAd1	pak
drželi	držet	k5eAaImAgMnP	držet
Moravský	moravský	k2eAgInSc4d1	moravský
Krumlov	Krumlov	k1gInSc4	Krumlov
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Listinou	listina	k1gFnSc7	listina
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1644	[number]	k4	1644
upravili	upravit	k5eAaPmAgMnP	upravit
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vylepšili	vylepšit	k5eAaPmAgMnP	vylepšit
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
erb	erb	k1gInSc4	erb
a	a	k8xC	a
knížecí	knížecí	k2eAgFnSc4d1	knížecí
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zavedli	zavést	k5eAaPmAgMnP	zavést
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
přáli	přát	k5eAaImAgMnP	přát
však	však	k9	však
rozvoji	rozvoj	k1gInSc3	rozvoj
řemeslnických	řemeslnický	k2eAgInPc2d1	řemeslnický
cechů	cech	k1gInPc2	cech
<g/>
.	.	kIx.	.
</s>
<s>
Krumlovští	krumlovský	k2eAgMnPc1d1	krumlovský
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
si	se	k3xPyFc3	se
vedli	vést	k5eAaImAgMnP	vést
jako	jako	k9	jako
dobří	dobrý	k2eAgMnPc1d1	dobrý
hospodáři	hospodář	k1gMnPc1	hospodář
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
panství	panství	k1gNnSc6	panství
<g/>
.	.	kIx.	.
</s>
<s>
Gundakar	Gundakar	k1gMnSc1	Gundakar
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
město	město	k1gNnSc4	město
společně	společně	k6eAd1	společně
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
Uherský	uherský	k2eAgInSc1d1	uherský
Ostroh	Ostroh	k1gInSc1	Ostroh
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
suverénní	suverénní	k2eAgNnSc4d1	suverénní
knížectví	knížectví	k1gNnSc4	knížectví
Liechtenstein	Liechtenstein	k1gMnSc1	Liechtenstein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
knížectví	knížectví	k1gNnSc1	knížectví
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
moravských	moravský	k2eAgInPc2d1	moravský
stavů	stav	k1gInPc2	stav
nepřečkalo	přečkat	k5eNaPmAgNnS	přečkat
Gundakarovy	Gundakarův	k2eAgMnPc4d1	Gundakarův
nástupce	nástupce	k1gMnPc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
přetvořili	přetvořit	k5eAaPmAgMnP	přetvořit
zámecký	zámecký	k2eAgInSc4d1	zámecký
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
prováděli	provádět	k5eAaImAgMnP	provádět
dílčí	dílčí	k2eAgFnPc4d1	dílčí
úpravy	úprava	k1gFnPc4	úprava
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
interiérech	interiér	k1gInPc6	interiér
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
modernizovali	modernizovat	k5eAaBmAgMnP	modernizovat
obytné	obytný	k2eAgInPc4d1	obytný
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
přistavěli	přistavět	k5eAaPmAgMnP	přistavět
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
kapli	kaple	k1gFnSc4	kaple
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
pak	pak	k6eAd1	pak
nechali	nechat	k5eAaPmAgMnP	nechat
vystavět	vystavět	k5eAaPmF	vystavět
kapli	kaple	k1gFnSc4	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
<g/>
,	,	kIx,	,
opravili	opravit	k5eAaPmAgMnP	opravit
farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgInS	být
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
vylidněný	vylidněný	k2eAgInSc1d1	vylidněný
<g/>
,	,	kIx,	,
vypálený	vypálený	k2eAgInSc1d1	vypálený
a	a	k8xC	a
rozbořený	rozbořený	k2eAgInSc1d1	rozbořený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
zde	zde	k6eAd1	zde
drancovala	drancovat	k5eAaImAgFnS	drancovat
26	[number]	k4	26
týdnů	týden	k1gInPc2	týden
švédská	švédský	k2eAgNnPc4d1	švédské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1690	[number]	k4	1690
zachvátil	zachvátit	k5eAaPmAgMnS	zachvátit
město	město	k1gNnSc4	město
požár	požár	k1gInSc1	požár
a	a	k8xC	a
skoro	skoro	k6eAd1	skoro
celý	celý	k2eAgInSc1d1	celý
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
byl	být	k5eAaImAgInS	být
patronem	patron	k1gInSc7	patron
svatý	svatý	k2eAgMnSc1d1	svatý
Florián	Florián	k1gMnSc1	Florián
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
i	i	k8xC	i
nově	nově	k6eAd1	nově
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1695	[number]	k4	1695
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1805	[number]	k4	1805
navštívil	navštívit	k5eAaPmAgInS	navštívit
zámek	zámek	k1gInSc1	zámek
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
ke	k	k7c3	k
Slavkovu	Slavkov	k1gInSc3	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1809	[number]	k4	1809
tábořilo	tábořit	k5eAaImAgNnS	tábořit
u	u	k7c2	u
Krumlova	Krumlov	k1gInSc2	Krumlov
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1866	[number]	k4	1866
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
sem	sem	k6eAd1	sem
vtrhla	vtrhnout	k5eAaPmAgNnP	vtrhnout
pruská	pruský	k2eAgNnPc1d1	pruské
vojska	vojsko	k1gNnPc1	vojsko
při	při	k7c6	při
prusko-rakouské	pruskoakouský	k2eAgFnSc6d1	prusko-rakouská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
Lichtenštejnů	Lichtenštejn	k1gInPc2	Lichtenštejn
prodáno	prodat	k5eAaPmNgNnS	prodat
spřízněnému	spřízněný	k2eAgInSc3d1	spřízněný
rodu	rod	k1gInSc3	rod
Kinských	Kinský	k1gMnPc2	Kinský
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
město	město	k1gNnSc4	město
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
při	při	k7c6	při
náletu	nálet	k1gInSc6	nálet
bylo	být	k5eAaImAgNnS	být
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
383	[number]	k4	383
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
113	[number]	k4	113
zničeno	zničit	k5eAaPmNgNnS	zničit
do	do	k7c2	do
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
soustředily	soustředit	k5eAaPmAgInP	soustředit
úřady	úřad	k1gInPc1	úřad
z	z	k7c2	z
rozbořeného	rozbořený	k2eAgNnSc2d1	rozbořené
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
se	se	k3xPyFc4	se
nastěhoval	nastěhovat	k5eAaPmAgInS	nastěhovat
místní	místní	k2eAgInSc1d1	místní
okresní	okresní	k2eAgInSc1d1	okresní
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
sídlily	sídlit	k5eAaImAgFnP	sídlit
zde	zde	k6eAd1	zde
další	další	k2eAgInPc1d1	další
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Úředníci	úředník	k1gMnPc1	úředník
úřadovali	úřadovat	k5eAaImAgMnP	úřadovat
v	v	k7c6	v
zámeckém	zámecký	k2eAgNnSc6d1	zámecké
vybavení	vybavení	k1gNnSc6	vybavení
po	po	k7c6	po
Kinských	Kinská	k1gFnPc6	Kinská
<g/>
.	.	kIx.	.
</s>
<s>
Mobiliář	mobiliář	k1gInSc1	mobiliář
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
později	pozdě	k6eAd2	pozdě
svezen	svézt	k5eAaPmNgMnS	svézt
na	na	k7c4	na
několik	několik	k4yIc4	několik
zámků	zámek	k1gInPc2	zámek
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
vybavení	vybavení	k1gNnSc2	vybavení
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lysicích	Lysice	k1gFnPc6	Lysice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krumlovském	krumlovský	k2eAgInSc6d1	krumlovský
zámku	zámek	k1gInSc6	zámek
pak	pak	k6eAd1	pak
sídlila	sídlit	k5eAaImAgFnS	sídlit
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgFnPc4	který
hořelo	hořet	k5eAaImAgNnS	hořet
v	v	k7c6	v
několika	několik	k4yIc6	několik
různých	různý	k2eAgFnPc6d1	různá
zámeckých	zámecký	k2eAgFnPc6d1	zámecká
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
pak	pak	k6eAd1	pak
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
Konekta	Konekt	k1gMnSc2	Konekt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
zpracováváním	zpracovávání	k1gNnSc7	zpracovávání
zahradního	zahradní	k2eAgInSc2d1	zahradní
nábytku	nábytek	k1gInSc2	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
učiliště	učiliště	k1gNnSc1	učiliště
s	s	k7c7	s
internátem	internát	k1gInSc7	internát
Železničního	železniční	k2eAgNnSc2d1	železniční
stavitelství	stavitelství	k1gNnSc2	stavitelství
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zde	zde	k6eAd1	zde
sídlilo	sídlit	k5eAaImAgNnS	sídlit
až	až	k9	až
do	do	k7c2	do
převratu	převrat	k1gInSc2	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
trvale	trvale	k6eAd1	trvale
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
rukách	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
zapůjčena	zapůjčit	k5eAaPmNgNnP	zapůjčit
plátna	plátno	k1gNnPc1	plátno
Muchovy	Muchův	k2eAgFnSc2d1	Muchova
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
epopeje	epopej	k1gFnSc2	epopej
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
vystavena	vystavit	k5eAaPmNgFnS	vystavit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Rytířském	rytířský	k2eAgInSc6d1	rytířský
sále	sál	k1gInSc6	sál
a	a	k8xC	a
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
Kostel	kostel	k1gInSc1	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
-	-	kIx~	-
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1248	[number]	k4	1248
Lichtenštejnská	lichtenštejnský	k2eAgFnSc1d1	lichtenštejnská
hrobka	hrobka	k1gFnSc1	hrobka
-	-	kIx~	-
empírová	empírový	k2eAgFnSc1d1	empírová
stavba	stavba	k1gFnSc1	stavba
opatřená	opatřený	k2eAgFnSc1d1	opatřená
lichtenštejnským	lichtenštejnský	k2eAgInSc7d1	lichtenštejnský
erby	erb	k1gInPc7	erb
a	a	k8xC	a
stromem	strom	k1gInSc7	strom
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Hrobka	hrobka	k1gFnSc1	hrobka
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
Eleonorou	Eleonora	k1gFnSc7	Eleonora
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc3	Liechtenstein
pro	pro	k7c4	pro
ostatky	ostatek	k1gInPc7	ostatek
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
knížete	kníže	k1gMnSc2	kníže
Karla	Karel	k1gMnSc2	Karel
z	z	k7c2	z
Lichtensteina	Lichtenstein	k1gMnSc2	Lichtenstein
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Olejomalbu	olejomalba	k1gFnSc4	olejomalba
Krista	Kristus	k1gMnSc2	Kristus
Pána	pán	k1gMnSc2	pán
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
malíř	malíř	k1gMnSc1	malíř
Hubert	Hubert	k1gMnSc1	Hubert
Maurer	Maurer	k1gMnSc1	Maurer
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
barokního	barokní	k2eAgMnSc2d1	barokní
světce	světec	k1gMnSc2	světec
v	v	k7c6	v
podživotní	podživotní	k2eAgFnSc6d1	podživotní
velikosti	velikost	k1gFnSc6	velikost
z	z	k7c2	z
mušlového	mušlový	k2eAgInSc2d1	mušlový
vápence	vápenec	k1gInSc2	vápenec
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
restaurována	restaurovat	k5eAaBmNgFnS	restaurovat
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
-	-	kIx~	-
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
Klášterním	klášterní	k2eAgNnSc6d1	klášterní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
součástí	součást	k1gFnSc7	součást
klášter	klášter	k1gInSc4	klášter
Augustiniánů	augustinián	k1gMnPc2	augustinián
<g/>
.	.	kIx.	.
</s>
<s>
Knížecí	knížecí	k2eAgInSc1d1	knížecí
dům	dům	k1gInSc1	dům
-	-	kIx~	-
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
dům	dům	k1gInSc1	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
reprezentativní	reprezentativní	k2eAgInSc4d1	reprezentativní
městský	městský	k2eAgInSc4d1	městský
palác	palác	k1gInSc4	palác
nižší	nízký	k2eAgFnSc2d2	nižší
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgInPc2	dva
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1614	[number]	k4	1614
připadl	připadnout	k5eAaPmAgInS	připadnout
dům	dům	k1gInSc1	dům
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
manželky	manželka	k1gFnSc2	manželka
majitele	majitel	k1gMnSc2	majitel
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
Marii	Maria	k1gFnSc4	Maria
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
přešel	přejít	k5eAaPmAgInS	přejít
dům	dům	k1gInSc1	dům
do	do	k7c2	do
držby	držba	k1gFnSc2	držba
knížete	kníže	k1gMnSc2	kníže
Gundakara	Gundakar	k1gMnSc2	Gundakar
z	z	k7c2	z
Liechtensteina	Liechtenstein	k1gMnSc2	Liechtenstein
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zřídil	zřídit	k5eAaPmAgMnS	zřídit
sídlo	sídlo	k1gNnSc4	sídlo
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
úředníky	úředník	k1gMnPc4	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1692	[number]	k4	1692
byl	být	k5eAaImAgInS	být
opraven	opravit	k5eAaPmNgInS	opravit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
sídlem	sídlo	k1gNnSc7	sídlo
Městské	městský	k2eAgFnSc2d1	městská
spořitelny	spořitelna	k1gFnSc2	spořitelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
prošel	projít	k5eAaPmAgInS	projít
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
umístěno	umístěn	k2eAgNnSc4d1	umístěno
muzeum	muzeum	k1gNnSc4	muzeum
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prostor	prostor	k1gInSc1	prostor
využívá	využívat	k5eAaPmIp3nS	využívat
dodnes	dodnes	k6eAd1	dodnes
společně	společně	k6eAd1	společně
s	s	k7c7	s
Městským	městský	k2eAgNnSc7d1	Městské
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
a	a	k8xC	a
soukromými	soukromý	k2eAgFnPc7d1	soukromá
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
-	-	kIx~	-
výrazná	výrazný	k2eAgFnSc1d1	výrazná
dominanta	dominanta	k1gFnSc1	dominanta
východně	východně	k6eAd1	východně
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
je	být	k5eAaImIp3nS	být
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
sv.	sv.	kA	sv.
Floriánovi	Florián	k1gMnSc3	Florián
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
si	se	k3xPyFc3	se
občané	občan	k1gMnPc1	občan
města	město	k1gNnSc2	město
zvolili	zvolit	k5eAaPmAgMnP	zvolit
po	po	k7c6	po
zhoubném	zhoubný	k2eAgInSc6d1	zhoubný
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
za	za	k7c4	za
patrona	patron	k1gMnSc4	patron
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
ve	v	k7c6	v
slohu	sloh	k1gInSc6	sloh
klasicizujícího	klasicizující	k2eAgNnSc2d1	klasicizující
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
je	být	k5eAaImIp3nS	být
Gutweinův	Gutweinův	k2eAgInSc1d1	Gutweinův
obraz	obraz	k1gInSc1	obraz
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gFnPc7	jehož
nohami	noha	k1gFnPc7	noha
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vedutě	veduta	k1gFnSc6	veduta
vyobrazeno	vyobrazen	k2eAgNnSc1d1	vyobrazeno
město	město	k1gNnSc1	město
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kapli	kaple	k1gFnSc3	kaple
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
záchraně	záchrana	k1gFnSc6	záchrana
života	život	k1gInSc2	život
knížete	kníže	k1gMnSc2	kníže
Antonína	Antonín	k1gMnSc2	Antonín
Floriána	Florián	k1gMnSc2	Florián
z	z	k7c2	z
Liechtensteina	Liechtenstein	k1gMnSc2	Liechtenstein
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
splašil	splašit	k5eAaPmAgMnS	splašit
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
díkůvzdání	díkůvzdání	k1gNnSc1	díkůvzdání
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
života	život	k1gInSc2	život
dal	dát	k5eAaPmAgInS	dát
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
kníže	kníže	k1gMnSc1	kníže
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
postavit	postavit	k5eAaPmF	postavit
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
prováděna	provádět	k5eAaImNgFnS	provádět
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1695	[number]	k4	1695
až	až	k9	až
1697	[number]	k4	1697
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
byla	být	k5eAaImAgFnS	být
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
zpustošena	zpustošen	k2eAgFnSc1d1	zpustošena
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
byla	být	k5eAaImAgFnS	být
znova	znova	k6eAd1	znova
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
<g/>
.	.	kIx.	.
</s>
<s>
Prvorepublikovou	prvorepublikový	k2eAgFnSc4d1	prvorepubliková
tradici	tradice	k1gFnSc4	tradice
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
tradiční	tradiční	k2eAgFnPc4d1	tradiční
poutě	pouť	k1gFnPc4	pouť
na	na	k7c6	na
první	první	k4xOgFnSc6	první
neděli	neděle	k1gFnSc6	neděle
po	po	k7c6	po
svátku	svátek	k1gInSc6	svátek
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
jsou	být	k5eAaImIp3nP	být
uchovávány	uchováván	k2eAgFnPc4d1	uchovávána
listiny	listina	k1gFnPc4	listina
a	a	k8xC	a
písemnosti	písemnost	k1gFnPc4	písemnost
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
-	-	kIx~	-
částečně	částečně	k6eAd1	částečně
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
hradební	hradební	k2eAgInSc1d1	hradební
systém	systém	k1gInSc1	systém
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
pozdějšími	pozdní	k2eAgFnPc7d2	pozdější
úpravami	úprava	k1gFnPc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
bylo	být	k5eAaImAgNnS	být
pobořeno	pobořit	k5eAaPmNgNnS	pobořit
Švédy	švéda	k1gFnPc4	švéda
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
úsek	úsek	k1gInSc1	úsek
je	být	k5eAaImIp3nS	být
parkánové	parkánový	k2eAgFnSc3d1	Parkánová
zdi	zeď	k1gFnSc3	zeď
s	s	k7c7	s
poloválcovitou	poloválcovitý	k2eAgFnSc7d1	poloválcovitý
baštou	bašta	k1gFnSc7	bašta
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
klíčovými	klíčový	k2eAgFnPc7d1	klíčová
střílnami	střílna	k1gFnPc7	střílna
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
parkán	parkán	k1gInSc1	parkán
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
obvodu	obvod	k1gInSc6	obvod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výše	vysoce	k6eAd2	vysoce
4	[number]	k4	4
-	-	kIx~	-
5	[number]	k4	5
m	m	kA	m
<g/>
,	,	kIx,	,
zeď	zeď	k1gFnSc1	zeď
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
lomového	lomový	k2eAgInSc2d1	lomový
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
kvádrů	kvádr	k1gInPc2	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
opevnění	opevnění	k1gNnSc2	opevnění
byly	být	k5eAaImAgFnP	být
i	i	k9	i
tři	tři	k4xCgFnPc1	tři
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
dochovala	dochovat	k5eAaPmAgFnS	dochovat
tzv.	tzv.	kA	tzv.
tulešická	tulešická	k1gFnSc1	tulešická
brána	brána	k1gFnSc1	brána
se	s	k7c7	s
znatelným	znatelný	k2eAgInSc7d1	znatelný
padacím	padací	k2eAgInSc7d1	padací
mostem	most	k1gInSc7	most
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
tzv.	tzv.	kA	tzv.
severní	severní	k2eAgFnSc1d1	severní
brána	brána	k1gFnSc1	brána
při	při	k7c6	při
pěšině	pěšina	k1gFnSc6	pěšina
na	na	k7c4	na
střelnici	střelnice	k1gFnSc4	střelnice
v	v	k7c6	v
čp.	čp.	k?	čp.
113	[number]	k4	113
a	a	k8xC	a
nedochovaná	dochovaný	k2eNgFnSc1d1	nedochovaná
tzv.	tzv.	kA	tzv.
rakšická	rakšická	k1gFnSc1	rakšická
brána	brána	k1gFnSc1	brána
s	s	k7c7	s
hranolovou	hranolový	k2eAgFnSc7d1	hranolová
věží	věž	k1gFnSc7	věž
mezi	mezi	k7c7	mezi
budovami	budova	k1gFnPc7	budova
dnešního	dnešní	k2eAgNnSc2d1	dnešní
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rozšíření	rozšíření	k1gNnSc2	rozšíření
dopravy	doprava	k1gFnSc2	doprava
zrušena	zrušen	k2eAgFnSc1d1	zrušena
<g/>
,	,	kIx,	,
hranolová	hranolový	k2eAgFnSc1d1	hranolová
pozdě	pozdě	k6eAd1	pozdě
gotická	gotický	k2eAgFnSc1d1	gotická
pětipodlažní	pětipodlažní	k2eAgFnSc1d1	pětipodlažní
věž	věž	k1gFnSc1	věž
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgFnS	stát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Rokytné	Rokytný	k2eAgFnSc6d1	Rokytná
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
studánka	studánka	k1gFnSc1	studánka
-	-	kIx~	-
romantická	romantický	k2eAgFnSc1d1	romantická
centrální	centrální	k2eAgFnSc1d1	centrální
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
kapli	kaple	k1gFnSc6	kaple
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
výrazných	výrazný	k2eAgFnPc2d1	výrazná
gotizujících	gotizující	k2eAgFnPc2d1	gotizující
forem	forma	k1gFnPc2	forma
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
úvalu	úval	k1gInSc2	úval
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
nádraží	nádraží	k1gNnSc3	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kaple	kaple	k1gFnSc2	kaple
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
gotický	gotický	k2eAgInSc1d1	gotický
almužní	almužní	k2eAgInSc1d1	almužní
sloupek	sloupek	k1gInSc1	sloupek
z	z	k7c2	z
mušlového	mušlový	k2eAgInSc2d1	mušlový
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Leopolda	Leopold	k1gMnSc2	Leopold
v	v	k7c4	v
Rokytné	Rokytný	k2eAgMnPc4d1	Rokytný
-	-	kIx~	-
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
pozdně	pozdně	k6eAd1	pozdně
románská	románský	k2eAgFnSc1d1	románská
jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
podélná	podélný	k2eAgFnSc1d1	podélná
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
pokročilého	pokročilý	k1gMnSc2	pokročilý
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
barokizována	barokizován	k2eAgFnSc1d1	barokizována
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1734	[number]	k4	1734
<g/>
,	,	kIx,	,
s	s	k7c7	s
mocnou	mocný	k2eAgFnSc7d1	mocná
hranolovou	hranolový	k2eAgFnSc7d1	hranolová
jednopatrovou	jednopatrový	k2eAgFnSc7d1	jednopatrová
věží	věž	k1gFnSc7	věž
zakončenou	zakončený	k2eAgFnSc4d1	zakončená
jehlanem	jehlan	k1gInSc7	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
obehnán	obehnat	k5eAaPmNgInS	obehnat
ohradní	ohradní	k2eAgFnSc7d1	ohradní
barokizovanou	barokizovaný	k2eAgFnSc7d1	barokizovaná
zídkou	zídka	k1gFnSc7	zídka
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
-	-	kIx~	-
Na	na	k7c4	na
asi	asi	k9	asi
82	[number]	k4	82
arech	ar	k1gInPc6	ar
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
407	[number]	k4	407
náhrobků	náhrobek	k1gInPc2	náhrobek
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
i	i	k8xC	i
poslední	poslední	k2eAgMnSc1d1	poslední
krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
rabín	rabín	k1gMnSc1	rabín
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Adolf	Adolf	k1gMnSc1	Adolf
Hahn	Hahn	k1gMnSc1	Hahn
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Hradisko	hradisko	k1gNnSc1	hradisko
Rokyten	Rokytno	k1gNnPc2	Rokytno
v	v	k7c4	v
Rokytné	Rokytný	k2eAgMnPc4d1	Rokytný
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
znojemského	znojemský	k2eAgNnSc2d1	Znojemské
údělného	údělný	k2eAgNnSc2d1	údělné
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rokytné	Rokytný	k2eAgFnSc6d1	Rokytná
sídlil	sídlit	k5eAaImAgInS	sídlit
velmož	velmož	k1gMnSc1	velmož
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
podléhal	podléhat	k5eAaImAgMnS	podléhat
znojemskému	znojemský	k2eAgNnSc3d1	Znojemské
údělnému	údělný	k2eAgNnSc3d1	údělné
knížeti	kníže	k1gNnSc3wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k9	jistě
zde	zde	k6eAd1	zde
pobývala	pobývat	k5eAaImAgFnS	pobývat
přiměřená	přiměřený	k2eAgFnSc1d1	přiměřená
vojenská	vojenský	k2eAgFnSc1d1	vojenská
družina	družina	k1gFnSc1	družina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hradu	hrad	k1gInSc6	hrad
bývalo	bývat	k5eAaImAgNnS	bývat
tržiště	tržiště	k1gNnSc1	tržiště
<g/>
,	,	kIx,	,
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
se	se	k3xPyFc4	se
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc1	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zde	zde	k6eAd1	zde
býval	bývat	k5eAaImAgInS	bývat
také	také	k9	také
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
jedinou	jediný	k2eAgFnSc7d1	jediná
kamennou	kamenný	k2eAgFnSc7d1	kamenná
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Zánik	zánik	k1gInSc1	zánik
hradu	hrad	k1gInSc2	hrad
bývá	bývat	k5eAaImIp3nS	bývat
spojován	spojován	k2eAgInSc1d1	spojován
s	s	k7c7	s
trestnou	trestný	k2eAgFnSc7d1	trestná
výpravou	výprava	k1gFnSc7	výprava
pražského	pražský	k2eAgMnSc2d1	pražský
knížete	kníže	k1gMnSc2	kníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
proti	proti	k7c3	proti
Konrádu	Konrád	k1gMnSc3	Konrád
Otovi	Ota	k1gMnSc3	Ota
Znojemskému	znojemský	k2eAgMnSc3d1	znojemský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1146	[number]	k4	1146
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrácení	vyvrácení	k1gNnSc1	vyvrácení
hradiště	hradiště	k1gNnSc2	hradiště
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
biskupa	biskup	k1gMnSc2	biskup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
"	"	kIx"	"
<g/>
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
hradu	hrad	k1gInSc2	hrad
Rokytná	Rokytný	k2eAgFnSc1d1	Rokytná
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1146	[number]	k4	1146
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
bylo	být	k5eAaImAgNnS	být
hradiště	hradiště	k1gNnSc1	hradiště
ještě	ještě	k6eAd1	ještě
nakrátko	nakrátko	k6eAd1	nakrátko
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
až	až	k9	až
roku	rok	k1gInSc2	rok
1185	[number]	k4	1185
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Krumlovsko-rokytenské	krumlovskookytenský	k2eAgInPc1d1	krumlovsko-rokytenský
slepence	slepenec	k1gInPc1	slepenec
-	-	kIx~	-
rezervace	rezervace	k1gFnPc1	rezervace
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
výměře	výměra	k1gFnSc6	výměra
86,5	[number]	k4	86,5
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
unikátní	unikátní	k2eAgInPc4d1	unikátní
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
a	a	k8xC	a
živočišné	živočišný	k2eAgNnSc4d1	živočišné
společenstva	společenstvo	k1gNnPc4	společenstvo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
na	na	k7c4	na
geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
a	a	k8xC	a
mikroklimaticky	mikroklimaticky	k6eAd1	mikroklimaticky
mimořádně	mimořádně	k6eAd1	mimořádně
zajímavém	zajímavý	k2eAgNnSc6d1	zajímavé
členitém	členitý	k2eAgNnSc6d1	členité
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Rokytné	Rokytný	k2eAgFnSc2d1	Rokytná
zahloubené	zahloubený	k2eAgFnPc4d1	zahloubená
v	v	k7c6	v
permských	permský	k2eAgInPc6d1	permský
slepencích	slepenec	k1gInPc6	slepenec
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
Boskovické	boskovický	k2eAgNnSc4d1	boskovické
brázda	brázda	k1gFnSc1	brázda
a	a	k8xC	a
vyvřelinového	vyvřelinový	k2eAgInSc2d1	vyvřelinový
masivu	masiv	k1gInSc2	masiv
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Boskovická	boskovický	k2eAgFnSc1d1	boskovická
brázda	brázda	k1gFnSc1	brázda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tvořena	tvořit	k5eAaImNgFnS	tvořit
Moravskokrumlovskou	moravskokrumlovský	k2eAgFnSc7d1	moravskokrumlovská
a	a	k8xC	a
Ivančickou	ivančický	k2eAgFnSc7d1	Ivančická
kotlinou	kotlina	k1gFnSc7	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
Narození	narození	k1gNnPc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
-	-	kIx~	-
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Polánky	Polánka	k1gFnSc2	Polánka
<g/>
.	.	kIx.	.
</s>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Krumlova	Krumlov	k1gInSc2	Krumlov
(	(	kIx(	(
<g/>
†	†	k?	†
1398	[number]	k4	1398
<g/>
)	)	kIx)	)
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Jakub	Jakub	k1gMnSc1	Jakub
Mořic	Mořic	k1gMnSc1	Mořic
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1641	[number]	k4	1641
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1709	[number]	k4	1709
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochován	pochován	k2eAgMnSc1d1	pochován
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
loretánské	loretánský	k2eAgFnSc6d1	Loretánská
kapli	kaple	k1gFnSc6	kaple
klášterního	klášterní	k2eAgInSc2d1	klášterní
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
kaple	kaple	k1gFnSc2	kaple
realizoval	realizovat	k5eAaBmAgMnS	realizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
pochována	pochovat	k5eAaPmNgFnS	pochovat
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
manželka	manželka	k1gFnSc1	manželka
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Eleonora	Eleonora	k1gFnSc1	Eleonora
Markéta	Markéta	k1gFnSc1	Markéta
Holštejnská	Holštejnská	k1gFnSc1	Holštejnská
<g/>
.	.	kIx.	.
</s>
<s>
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Bohumír	Bohumír	k1gMnSc1	Bohumír
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Čermák	Čermák	k1gMnSc1	Čermák
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
projektů	projekt	k1gInPc2	projekt
i	i	k8xC	i
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
Gundakar	Gundakara	k1gFnPc2	Gundakara
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
(	(	kIx(	(
<g/>
1586	[number]	k4	1586
<g/>
-	-	kIx~	-
<g/>
1658	[number]	k4	1658
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
tzv.	tzv.	kA	tzv.
malého	malé	k1gNnSc2	malé
(	(	kIx(	(
<g/>
Gundakarova	Gundakarův	k2eAgInSc2d1	Gundakarův
<g/>
)	)	kIx)	)
majorátu	majorát	k1gInSc2	majorát
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Guttwein	Guttwein	k1gMnSc1	Guttwein
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1718	[number]	k4	1718
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
mědirytec	mědirytec	k1gMnSc1	mědirytec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1698	[number]	k4	1698
<g/>
-	-	kIx~	-
<g/>
1714	[number]	k4	1714
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
oltářního	oltářní	k2eAgInSc2d1	oltářní
obrazu	obraz	k1gInSc2	obraz
Umučení	umučení	k1gNnSc4	umučení
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc4	Bartoloměj
a	a	k8xC	a
veduty	veduta	k1gFnPc4	veduta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
MUDr.	MUDr.	kA	MUDr.
Emanuel	Emanuel	k1gMnSc1	Emanuel
Haizl	Haizl	k1gMnSc1	Haizl
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
Jezbořice	Jezbořice	k1gFnSc1	Jezbořice
-	-	kIx~	-
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastenecký	vlastenecký	k2eAgMnSc1d1	vlastenecký
činitel	činitel	k1gMnSc1	činitel
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Vilém	Vilém	k1gMnSc1	Vilém
Haňák	Haňák	k1gMnSc1	Haňák
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
pracovník	pracovník	k1gMnSc1	pracovník
v	v	k7c6	v
regionu	region	k1gInSc6	region
Moravskokrumlovska	Moravskokrumlovsko	k1gNnSc2	Moravskokrumlovsko
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
svazku	svazek	k1gInSc2	svazek
Vlastivědy	vlastivěda	k1gFnSc2	vlastivěda
moravské	moravský	k2eAgFnSc2d1	Moravská
(	(	kIx(	(
<g/>
topografické	topografický	k2eAgFnSc2d1	topografická
řady	řada	k1gFnSc2	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
soudnímu	soudní	k2eAgNnSc3d1	soudní
okresu	okres	k1gInSc2	okres
Moravský	moravský	k2eAgInSc4d1	moravský
Krumlov	Krumlov	k1gInSc4	Krumlov
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Jonáš	Jonáš	k1gMnSc1	Jonáš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Jiří	Jiří	k1gMnSc1	Jiří
Kacetl	Kacetl	k1gMnSc1	Kacetl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
znojemský	znojemský	k2eAgMnSc1d1	znojemský
politik	politik	k1gMnSc1	politik
JUDr.	JUDr.	kA	JUDr.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kaniak	Kaniak	k1gMnSc1	Kaniak
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
německé	německý	k2eAgFnSc2d1	německá
politické	politický	k2eAgFnSc2d1	politická
reprezentace	reprezentace	k1gFnSc2	reprezentace
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Kašpárek	Kašpárek	k1gMnSc1	Kašpárek
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stolařský	stolařský	k2eAgMnSc1d1	stolařský
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
člen	člen	k1gMnSc1	člen
správní	správní	k2eAgFnSc2d1	správní
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
lidovou	lidový	k2eAgFnSc4d1	lidová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1923	[number]	k4	1923
až	až	k8xS	až
1938	[number]	k4	1938
starostou	starosta	k1gMnSc7	starosta
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kaufman	Kaufman	k1gMnSc1	Kaufman
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klempíř	klempíř	k1gMnSc1	klempíř
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
numismatik	numismatik	k1gMnSc1	numismatik
<g/>
,	,	kIx,	,
sběratel	sběratel	k1gMnSc1	sběratel
<g/>
,	,	kIx,	,
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Okresního	okresní	k2eAgNnSc2d1	okresní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
průvodce	průvodce	k1gMnSc1	průvodce
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
Muchova	Muchův	k2eAgFnSc1d1	Muchova
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
člen	člen	k1gMnSc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
komise	komise	k1gFnSc2	komise
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
Kočí	Kočí	k1gFnSc1	Kočí
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
<g/>
,	,	kIx,	,
správkyně	správkyně	k1gFnPc1	správkyně
DPS	DPS	kA	DPS
JUDr.	JUDr.	kA	JUDr.
Josef	Josef	k1gMnSc1	Josef
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
funkcionář	funkcionář	k1gMnSc1	funkcionář
spolků	spolek	k1gInPc2	spolek
Národní	národní	k2eAgFnSc1d1	národní
jednota	jednota	k1gFnSc1	jednota
pro	pro	k7c4	pro
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
Matice	matice	k1gFnSc1	matice
Krumlovské	krumlovský	k2eAgFnSc2d1	Krumlovská
aj.	aj.	kA	aj.
Podporovatel	podporovatel	k1gMnSc1	podporovatel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
české	český	k2eAgFnSc2d1	Česká
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Organizátor	organizátor	k1gMnSc1	organizátor
prohlášení	prohlášení	k1gNnSc2	prohlášení
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
ČSR	ČSR	kA	ČSR
členem	člen	k1gMnSc7	člen
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
vládním	vládní	k2eAgMnSc7d1	vládní
komisařem	komisař	k1gMnSc7	komisař
a	a	k8xC	a
prvním	první	k4xOgMnPc3	první
meziválečným	meziválečný	k2eAgMnPc3d1	meziválečný
starostou	starosta	k1gMnSc7	starosta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Eleonora	Eleonora	k1gFnSc1	Eleonora
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc6	Liechtenstein
(	(	kIx(	(
<g/>
1745	[number]	k4	1745
<g/>
-	-	kIx~	-
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozená	rozený	k2eAgFnSc1d1	rozená
princezna	princezna	k1gFnSc1	princezna
Oettingen-Spielberk	Oettingen-Spielberk	k1gInSc1	Oettingen-Spielberk
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
knížete	kníže	k1gMnSc4	kníže
Karla	Karel	k1gMnSc4	Karel
I.	I.	kA	I.
Boromejského	Boromejský	k2eAgMnSc4d1	Boromejský
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc6	Liechtenstein
<g/>
.	.	kIx.	.
</s>
<s>
Dvorní	dvorní	k2eAgFnSc1d1	dvorní
dáma	dáma	k1gFnSc1	dáma
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Reorganizátorka	Reorganizátorka	k1gFnSc1	Reorganizátorka
zámeckého	zámecký	k2eAgInSc2d1	zámecký
areálu	areál	k1gInSc2	areál
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
stavitelka	stavitelka	k1gFnSc1	stavitelka
lichtenštejnské	lichtenštejnský	k2eAgFnSc2d1	lichtenštejnská
hrobky	hrobka	k1gFnSc2	hrobka
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
černého	černý	k2eAgInSc2d1	černý
tunelu	tunel	k1gInSc2	tunel
pod	pod	k7c7	pod
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Nechala	nechat	k5eAaPmAgFnS	nechat
upravit	upravit	k5eAaPmF	upravit
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Členka	členka	k1gFnSc1	členka
dámského	dámský	k2eAgInSc2d1	dámský
kroužku	kroužek	k1gInSc2	kroužek
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
si	se	k3xPyFc3	se
korespondovala	korespondovat	k5eAaImAgFnS	korespondovat
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Málek	Málek	k1gMnSc1	Málek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
střelec	střelec	k1gMnSc1	střelec
a	a	k8xC	a
olympionik	olympionik	k1gMnSc1	olympionik
Stanislav	Stanislav	k1gMnSc1	Stanislav
Marek	Marek	k1gMnSc1	Marek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zemědělec	zemědělec	k1gMnSc1	zemědělec
<g/>
,	,	kIx,	,
komunální	komunální	k2eAgMnSc1d1	komunální
politik	politik	k1gMnSc1	politik
Gustav	Gustav	k1gMnSc1	Gustav
Obrlik	Obrlik	k1gMnSc1	Obrlik
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
MUDr.	MUDr.	kA	MUDr.
Mořic	Mořic	k1gMnSc1	Mořic
Odstrčil	Odstrčil	k1gMnSc1	Odstrčil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
Bezdězí	Bezdězí	k1gNnSc1	Bezdězí
-	-	kIx~	-
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
vlastenecký	vlastenecký	k2eAgMnSc1d1	vlastenecký
činitel	činitel	k1gMnSc1	činitel
Plukovník	plukovník	k1gMnSc1	plukovník
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
Adolf	Adolf	k1gMnSc1	Adolf
Opálka	opálka	k1gFnSc1	opálka
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1915	[number]	k4	1915
-	-	kIx~	-
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
provedení	provedení	k1gNnSc6	provedení
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
zastupujícího	zastupující	k2eAgMnSc4d1	zastupující
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
JUDr.	JUDr.	kA	JUDr.
Václav	Václav	k1gMnSc1	Václav
Perek	perko	k1gNnPc2	perko
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
činitel	činitel	k1gMnSc1	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
české	český	k2eAgInPc4d1	český
spolky	spolek	k1gInPc4	spolek
a	a	k8xC	a
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
českou	český	k2eAgFnSc4d1	Česká
komunitu	komunita	k1gFnSc4	komunita
v	v	k7c6	v
etnicky	etnicky	k6eAd1	etnicky
smíšeném	smíšený	k2eAgInSc6d1	smíšený
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
místního	místní	k2eAgMnSc2d1	místní
Sokola	Sokol	k1gMnSc2	Sokol
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
místního	místní	k2eAgInSc2d1	místní
odboru	odbor	k1gInSc2	odbor
Národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
pro	pro	k7c4	pro
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Rydval	Rydval	k1gMnSc1	Rydval
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
Marie	Maria	k1gFnSc2	Maria
Eleonora	Eleonora	k1gFnSc1	Eleonora
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
jako	jako	k8xS	jako
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc3	Liechtenstein
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
podmaršálka	podmaršálek	k1gMnSc2	podmaršálek
Mořice	Mořic	k1gMnSc2	Mořic
Josefa	Josef	k1gMnSc2	Josef
knížete	kníže	k1gMnSc2	kníže
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc3	Liechtenstein
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Leopoldiny	Leopoldin	k2eAgFnSc2d1	Leopoldina
kněžny	kněžna	k1gFnSc2	kněžna
Esterházy	Esterháza	k1gFnSc2	Esterháza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
knížete	kníže	k1gMnSc2	kníže
Jana	Jan	k1gMnSc2	Jan
Adolfa	Adolf	k1gMnSc2	Adolf
II	II	kA	II
<g/>
.	.	kIx.	.
knížete	kníže	k1gMnSc2	kníže
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
a	a	k8xC	a
o	o	k7c6	o
vybudování	vybudování	k1gNnSc6	vybudování
rodinné	rodinný	k2eAgFnSc2d1	rodinná
hrobky	hrobka	k1gFnSc2	hrobka
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
<g/>
.	.	kIx.	.
</s>
<s>
Erich	Erich	k1gMnSc1	Erich
Sloschek	Sloschek	k1gMnSc1	Sloschek
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgMnSc1d1	regionální
německý	německý	k2eAgMnSc1d1	německý
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
první	první	k4xOgFnSc2	první
historické	historický	k2eAgFnSc2d1	historická
syntézy	syntéza	k1gFnSc2	syntéza
o	o	k7c6	o
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
kurátor	kurátor	k1gMnSc1	kurátor
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
pedagogický	pedagogický	k2eAgMnSc1d1	pedagogický
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
úředník	úředník	k1gMnSc1	úředník
Josef	Josef	k1gMnSc1	Josef
Staroštík	Staroštík	k1gMnSc1	Staroštík
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
osvětový	osvětový	k2eAgMnSc1d1	osvětový
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Moravskokrumlovského	moravskokrumlovský	k2eAgInSc2d1	moravskokrumlovský
zpravodaje	zpravodaj	k1gInSc2	zpravodaj
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
kroužku	kroužek	k1gInSc2	kroužek
Moravskokrumlovských	moravskokrumlovský	k2eAgMnPc2d1	moravskokrumlovský
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc2	správce
galerie	galerie	k1gFnSc2	galerie
Muchova	Muchův	k2eAgFnSc1d1	Muchova
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
třech	tři	k4xCgFnPc6	tři
jejích	její	k3xOp3gMnPc2	její
katalogů	katalog	k1gInPc2	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Průmyslu	průmysl	k1gInSc2	průmysl
mléčné	mléčný	k2eAgFnSc2d1	mléčná
výživy	výživa	k1gFnSc2	výživa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c4	na
Lacrum	lacrum	k1gInSc4	lacrum
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Stehlík	Stehlík	k1gMnSc1	Stehlík
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
paleontolog	paleontolog	k1gMnSc1	paleontolog
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
člen	člen	k1gMnSc1	člen
správní	správní	k2eAgFnSc2d1	správní
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
následně	následně	k6eAd1	následně
člen	člen	k1gInSc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
rady	rada	k1gFnSc2	rada
za	za	k7c4	za
Čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
stranu	strana	k1gFnSc4	strana
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
</s>
<s>
Odborný	odborný	k2eAgMnSc1d1	odborný
pracovník	pracovník	k1gMnSc1	pracovník
diluviálního	diluviální	k2eAgNnSc2d1	diluviální
oddělení	oddělení	k1gNnSc2	oddělení
Moravského	moravský	k2eAgNnSc2d1	Moravské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
memoriam	memoriam	k6eAd1	memoriam
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Antonín	Antonín	k1gMnSc1	Antonín
Ugwitz	Ugwitz	k1gMnSc1	Ugwitz
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
spolkový	spolkový	k2eAgMnSc1d1	spolkový
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
"	"	kIx"	"
<g/>
Příběhů	příběh	k1gInPc2	příběh
města	město	k1gNnSc2	město
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
a	a	k8xC	a
sousední	sousední	k2eAgFnSc2d1	sousední
vesnice	vesnice	k1gFnSc2	vesnice
Rokytné	Rokytný	k2eAgFnSc2d1	Rokytná
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Vihanová	Vihanová	k1gFnSc1	Vihanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
scenáristka	scenáristka	k1gFnSc1	scenáristka
Libor	Libor	k1gMnSc1	Libor
Zelníček	zelníček	k1gInSc1	zelníček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
Dagmar	Dagmar	k1gFnSc1	Dagmar
Zvěřinová	Zvěřinová	k1gFnSc1	Zvěřinová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manažerka	manažerka	k1gFnSc1	manažerka
<g/>
,	,	kIx,	,
senátorka	senátorka	k1gFnSc1	senátorka
Michael	Michael	k1gMnSc1	Michael
Warneke	Warneke	k1gInSc1	Warneke
-	-	kIx~	-
německý	německý	k2eAgMnSc1d1	německý
starosta	starosta	k1gMnSc1	starosta
v	v	k7c6	v
letech	let	k1gInPc6	let
1902	[number]	k4	1902
<g />
.	.	kIx.	.
</s>
<s>
až	až	k9	až
1909	[number]	k4	1909
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kaniak	Kaniak	k1gMnSc1	Kaniak
-	-	kIx~	-
kategoricky	kategoricky	k6eAd1	kategoricky
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
zřízení	zřízení	k1gNnSc3	zřízení
české	český	k2eAgFnSc2d1	Česká
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
starosty	starosta	k1gMnSc2	starosta
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
r.	r.	kA	r.
1909	[number]	k4	1909
do	do	k7c2	do
r.	r.	kA	r.
1919	[number]	k4	1919
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josef	Josef	k1gMnSc1	Josef
Kopřiva	Kopřiva	k1gMnSc1	Kopřiva
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
první	první	k4xOgMnSc1	první
poválečný	poválečný	k2eAgMnSc1d1	poválečný
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
intervenci	intervence	k1gFnSc3	intervence
německých	německý	k2eAgMnPc2d1	německý
radních	radní	k1gMnPc2	radní
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Brothánek	Brothánek	k1gMnSc1	Brothánek
Richard	Richard	k1gMnSc1	Richard
Filla	Filla	k1gMnSc1	Filla
Antonín	Antonín	k1gMnSc1	Antonín
Kašpárek	Kašpárek	k1gMnSc1	Kašpárek
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
-	-	kIx~	-
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Kašpárek	Kašpárek	k1gMnSc1	Kašpárek
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
-	-	kIx~	-
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Kašpárek	Kašpárek	k1gMnSc1	Kašpárek
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
JUDr.	JUDr.	kA	JUDr.
Štěpán	Štěpán	k1gMnSc1	Štěpán
Grohschmiedt	Grohschmiedt	k1gMnSc1	Grohschmiedt
<g/>
,	,	kIx,	,
povoláním	povolání	k1gNnPc3	povolání
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
právni	právnit	k5eAaPmRp2nS	právnit
poradce	poradce	k1gMnSc2	poradce
hraběte	hrabě	k1gMnSc4	hrabě
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Kinského	Kinské	k1gNnSc2	Kinské
K.	K.	kA	K.
Balcar	Balcar	k1gMnSc1	Balcar
Jiří	Jiří	k1gMnSc1	Jiří
Brauner	Brauner	k1gMnSc1	Brauner
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
Pitlach	Pitlach	k1gMnSc1	Pitlach
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
-	-	kIx~	-
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
Pitlach	Pitlach	k1gMnSc1	Pitlach
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Smíšeného	smíšený	k2eAgInSc2d1	smíšený
pěveckého	pěvecký	k2eAgInSc2d1	pěvecký
sboru	sbor	k1gInSc2	sbor
Karla	Karel	k1gMnSc4	Karel
Němečka	Němeček	k1gMnSc4	Němeček
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Mokrý	Mokrý	k1gMnSc1	Mokrý
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Současným	současný	k2eAgMnSc7d1	současný
starostou	starosta	k1gMnSc7	starosta
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Mgr.	Mgr.	kA	Mgr.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Třetina	třetina	k1gFnSc1	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Předchozím	předchozí	k2eAgMnSc7d1	předchozí
starostou	starosta	k1gMnSc7	starosta
byl	být	k5eAaImAgMnS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Mokrý	Mokrý	k1gMnSc1	Mokrý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
petiční	petiční	k2eAgFnSc2d1	petiční
akce	akce	k1gFnSc2	akce
proti	proti	k7c3	proti
přestěhování	přestěhování	k1gNnSc3	přestěhování
Muchovy	Muchův	k2eAgFnSc2d1	Muchova
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
epopeje	epopej	k1gFnSc2	epopej
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
krajského	krajský	k2eAgNnSc2d1	krajské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
za	za	k7c4	za
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
Polánka	Polánka	k1gFnSc1	Polánka
Rakšice	Rakšice	k1gFnSc1	Rakšice
Rokytná	Rokytný	k2eAgFnSc1d1	Rokytná
Až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgInS	být
Moravský	moravský	k2eAgInSc4d1	moravský
Krumlov	Krumlov	k1gInSc4	Krumlov
okresem	okres	k1gInSc7	okres
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
čítal	čítat	k5eAaImAgInS	čítat
následující	následující	k2eAgFnPc4d1	následující
obce	obec	k1gFnPc4	obec
:	:	kIx,	:
Běhařovice	Běhařovice	k1gFnSc1	Běhařovice
•	•	k?	•
Biskoupky	Biskoupka	k1gFnSc2	Biskoupka
•	•	k?	•
Bohutice	Bohutice	k1gFnSc2	Bohutice
•	•	k?	•
Branišovice	Branišovice	k1gFnSc2	Branišovice
(	(	kIx(	(
<g/>
Vinohrádky	vinohrádek	k1gInPc1	vinohrádek
<g/>
)	)	kIx)	)
•	•	k?	•
Budkovice	Budkovice	k1gFnSc2	Budkovice
•	•	k?	•
Čermákovice	Čermákovice	k1gFnSc2	Čermákovice
•	•	k?	•
Damnice	Damnice	k1gFnSc2	Damnice
•	•	k?	•
Dobelice	Dobelice	k1gFnSc2	Dobelice
•	•	k?	•
Dobronice	Dobronice	k1gFnSc2	Dobronice
•	•	k?	•
Dobřínsko	Dobřínsko	k1gNnSc1	Dobřínsko
•	•	k?	•
Dolenice	Dolenice	k1gFnSc2	Dolenice
•	•	k?	•
Dolní	dolní	k2eAgMnPc4d1	dolní
Dubňany	Dubňan	k1gMnPc4	Dubňan
•	•	k?	•
<g />
.	.	kIx.	.
</s>
<s>
Dukovany	Dukovany	k1gInPc1	Dukovany
•	•	k?	•
Džbánice	Džbánice	k1gFnSc2	Džbánice
•	•	k?	•
Heřmanice	Heřmanice	k1gFnSc2	Heřmanice
u	u	k7c2	u
Rouchovan	Rouchovan	k1gMnSc1	Rouchovan
•	•	k?	•
Horní	horní	k2eAgMnPc4d1	horní
Dubňany	Dubňan	k1gMnPc4	Dubňan
•	•	k?	•
Horní	horní	k2eAgFnSc1d1	horní
Kounice	Kounice	k1gFnSc1	Kounice
•	•	k?	•
Hostěradice	Hostěradice	k1gFnSc2	Hostěradice
•	•	k?	•
Hrubšice	Hrubšice	k1gFnSc2	Hrubšice
•	•	k?	•
Chlupice	Chlupice	k1gFnSc2	Chlupice
•	•	k?	•
Jamolice	Jamolice	k1gFnSc2	Jamolice
•	•	k?	•
Jezeřany	jezeřan	k1gMnPc4	jezeřan
•	•	k?	•
Jiřice	Jiřice	k1gFnSc1	Jiřice
•	•	k?	•
Kadov	Kadov	k1gInSc1	Kadov
•	•	k?	•
Kašenec	Kašenec	k1gInSc1	Kašenec
•	•	k?	•
Křepice	Křepice	k1gFnSc2	Křepice
•	•	k?	•
Kubšice	Kubšice	k1gFnSc2	Kubšice
•	•	k?	•
Lesonice	Lesonice	k1gFnSc2	Lesonice
•	•	k?	•
Loděnice	loděnice	k1gFnSc2	loděnice
•	•	k?	•
Maršovice	Maršovice	k1gFnSc2	Maršovice
•	•	k?	•
Medlice	Medlice	k1gFnSc2	Medlice
•	•	k?	•
Miroslav	Miroslav	k1gMnSc1	Miroslav
(	(	kIx(	(
<g/>
Václavov	Václavov	k1gInSc1	Václavov
<g/>
)	)	kIx)	)
•	•	k?	•
Miroslavské	Miroslavský	k2eAgFnSc2d1	Miroslavská
<g />
.	.	kIx.	.
</s>
<s>
Knínice	Knínice	k1gFnSc1	Knínice
•	•	k?	•
Míšovice	Míšovice	k1gFnSc2	Míšovice
•	•	k?	•
Morašice	Morašice	k1gFnSc2	Morašice
•	•	k?	•
Našiměřice	Našiměřice	k1gFnSc2	Našiměřice
•	•	k?	•
Olbramovice	Olbramovice	k1gFnSc1	Olbramovice
(	(	kIx(	(
<g/>
Babice	babice	k1gFnSc1	babice
•	•	k?	•
<g/>
Lidměřice	Lidměřice	k1gFnSc2	Lidměřice
•	•	k?	•
<g/>
Želovice	Želovice	k1gFnSc2	Želovice
<g/>
)	)	kIx)	)
•	•	k?	•
Petrovice	Petrovice	k1gFnSc1	Petrovice
•	•	k?	•
Polánka	Polánka	k1gFnSc1	Polánka
(	(	kIx(	(
<g/>
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
•	•	k?	•
Přeskače	Přeskač	k1gInSc2	Přeskač
•	•	k?	•
Ratišovice	Ratišovice	k1gFnSc2	Ratišovice
•	•	k?	•
Rešice	Rešice	k1gFnSc1	Rešice
(	(	kIx(	(
<g/>
Kordula	Kordula	k1gFnSc1	Kordula
<g/>
)	)	kIx)	)
•	•	k?	•
Rokytná	Rokytný	k2eAgFnSc1d1	Rokytná
•	•	k?	•
Rouchovany	Rouchovan	k1gMnPc4	Rouchovan
•	•	k?	•
Rybníky	rybník	k1gInPc4	rybník
•	•	k?	•
Řeznovice	Řeznovice	k1gFnSc2	Řeznovice
•	•	k?	•
Skalice	Skalice	k1gFnSc2	Skalice
•	•	k?	•
Skryje	skrýt	k5eAaPmIp3nS	skrýt
nad	nad	k7c7	nad
Jihlavou	Jihlava	k1gFnSc7	Jihlava
(	(	kIx(	(
<g/>
Lipňany	Lipňan	k1gMnPc4	Lipňan
<g/>
)	)	kIx)	)
•	•	k?	•
Stupešice	Stupešice	k1gFnSc2	Stupešice
•	•	k?	•
Suchohrdly	Suchohrdly	k1gFnSc2	Suchohrdly
•	•	k?	•
Šemíkovice	Šemíkovice	k1gFnSc2	Šemíkovice
•	•	k?	•
Šumice	Šumice	k1gFnSc1	Šumice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
)	)	kIx)	)
•	•	k?	•
Tavíkovice	Tavíkovice	k1gFnSc2	Tavíkovice
•	•	k?	•
Trnové	trnový	k2eAgFnSc2d1	Trnová
Pole	pole	k1gFnSc2	pole
•	•	k?	•
Trstěnice	trstěnice	k1gFnSc2	trstěnice
•	•	k?	•
Tulešice	Tulešice	k1gFnSc2	Tulešice
•	•	k?	•
Vedrovice	Vedrovice	k1gFnSc2	Vedrovice
•	•	k?	•
Vémyslice	Vémyslice	k1gFnSc2	Vémyslice
•	•	k?	•
Višňové	višňový	k2eAgNnSc1d1	Višňové
•	•	k?	•
</s>
