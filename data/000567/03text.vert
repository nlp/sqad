<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
nebo	nebo	k8xC	nebo
také	také	k9	také
Pražský	pražský	k2eAgInSc4d1	pražský
orloj	orloj	k1gInSc4	orloj
jsou	být	k5eAaImIp3nP	být
středověké	středověký	k2eAgFnPc1d1	středověká
astronomické	astronomický	k2eAgFnPc1d1	astronomická
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
věže	věž	k1gFnSc2	věž
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doložen	doložit	k5eAaPmNgInS	doložit
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1410	[number]	k4	1410
<g/>
.	.	kIx.	.
</s>
<s>
Orloji	orloj	k1gInSc3	orloj
uprostřed	uprostřed	k6eAd1	uprostřed
dominuje	dominovat	k5eAaImIp3nS	dominovat
astronomický	astronomický	k2eAgInSc1d1	astronomický
ciferník	ciferník	k1gInSc1	ciferník
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
kalendářní	kalendářní	k2eAgFnSc1d1	kalendářní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
astronomickém	astronomický	k2eAgInSc6d1	astronomický
ciferníku	ciferník	k1gInSc6	ciferník
<g/>
,	,	kIx,	,
odvozeném	odvozený	k2eAgInSc6d1	odvozený
od	od	k7c2	od
astrolábu	astroláb	k1gInSc6	astroláb
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
odečíst	odečíst	k5eAaPmF	odečíst
různé	různý	k2eAgInPc4d1	různý
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
astronomické	astronomický	k2eAgInPc4d1	astronomický
cykly	cyklus	k1gInPc4	cyklus
<g/>
,	,	kIx,	,
polohu	poloha	k1gFnSc4	poloha
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
kterým	který	k3yQgNnSc7	který
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
zvířetníku	zvířetník	k1gInSc2	zvířetník
právě	právě	k6eAd1	právě
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
polohu	poloha	k1gFnSc4	poloha
Měsíce	měsíc	k1gInSc2	měsíc
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
fázi	fáze	k1gFnSc4	fáze
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
lze	lze	k6eAd1	lze
odečíst	odečíst	k5eAaPmF	odečíst
aktuální	aktuální	k2eAgInSc4d1	aktuální
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
a	a	k8xC	a
nepohyblivé	pohyblivý	k2eNgInPc4d1	nepohyblivý
svátky	svátek	k1gInPc4	svátek
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
astronomickým	astronomický	k2eAgInSc7d1	astronomický
ciferníkem	ciferník	k1gInSc7	ciferník
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
apoštolové	apoštol	k1gMnPc1	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
doplněn	doplněn	k2eAgInSc1d1	doplněn
sochami	socha	k1gFnPc7	socha
po	po	k7c6	po
okrajích	okraj	k1gInPc6	okraj
<g/>
,	,	kIx,	,
bustou	busta	k1gFnSc7	busta
anděla	anděl	k1gMnSc2	anděl
mezi	mezi	k7c7	mezi
okny	okno	k1gNnPc7	okno
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
ozvučeným	ozvučený	k2eAgInSc7d1	ozvučený
kohoutem	kohout	k1gInSc7	kohout
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
nad	nad	k7c7	nad
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
orloje	orloj	k1gInSc2	orloj
<g/>
,	,	kIx,	,
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
astrolábu	astroláb	k1gInSc2	astroláb
<g/>
,	,	kIx,	,
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
pohybů	pohyb	k1gInPc2	pohyb
soch	socha	k1gFnPc2	socha
je	být	k5eAaImIp3nS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
mechanickým	mechanický	k2eAgInSc7d1	mechanický
hodinovým	hodinový	k2eAgInSc7d1	hodinový
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
prošel	projít	k5eAaPmAgMnS	projít
několika	několik	k4yIc7	několik
úpravami	úprava	k1gFnPc7	úprava
a	a	k8xC	a
zlepšeními	zlepšení	k1gNnPc7	zlepšení
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1402	[number]	k4	1402
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňován	k2eAgFnPc1d1	zmiňována
hodiny	hodina	k1gFnPc1	hodina
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
hodinářem	hodinář	k1gMnSc7	hodinář
Mikulášem	mikuláš	k1gInSc7	mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
astronomem	astronom	k1gMnSc7	astronom
Janem	Jan	k1gMnSc7	Jan
Šindelem	šindel	k1gInSc7	šindel
postaven	postaven	k2eAgInSc1d1	postaven
současný	současný	k2eAgInSc1d1	současný
orloj	orloj	k1gInSc1	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
architektonická	architektonický	k2eAgFnSc1d1	architektonická
a	a	k8xC	a
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
orloj	orloj	k1gInSc1	orloj
upravil	upravit	k5eAaPmAgMnS	upravit
hodinář	hodinář	k1gMnSc1	hodinář
mistr	mistr	k1gMnSc1	mistr
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
upravil	upravit	k5eAaPmAgMnS	upravit
a	a	k8xC	a
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
Staroměstský	staroměstský	k2eAgInSc4d1	staroměstský
orloj	orloj	k1gInSc4	orloj
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
z	z	k7c2	z
Klokotské	Klokotský	k2eAgFnSc2d1	Klokotská
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
opravy	oprava	k1gFnPc1	oprava
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
až	až	k9	až
1866	[number]	k4	1866
byla	být	k5eAaImAgFnS	být
také	také	k9	také
osazena	osazen	k2eAgFnSc1d1	osazena
nová	nový	k2eAgFnSc1d1	nová
kalendářní	kalendářní	k2eAgFnSc1d1	kalendářní
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Mánesa	Mánes	k1gMnSc2	Mánes
se	s	k7c7	s
symboly	symbol	k1gInPc7	symbol
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgInSc2d1	pražský
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
během	během	k7c2	během
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
byl	být	k5eAaImAgInS	být
orloj	orloj	k1gInSc1	orloj
značně	značně	k6eAd1	značně
poničen	poničen	k2eAgInSc1d1	poničen
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
obnovení	obnovení	k1gNnSc1	obnovení
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
celkovou	celkový	k2eAgFnSc4d1	celková
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejlépe	dobře	k6eAd3	dobře
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
středověký	středověký	k2eAgInSc1d1	středověký
orloj	orloj	k1gInSc1	orloj
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
turistických	turistický	k2eAgInPc2d1	turistický
objektů	objekt	k1gInPc2	objekt
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
UNESCO	UNESCO	kA	UNESCO
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1338	[number]	k4	1338
udělil	udělit	k5eAaPmAgMnS	udělit
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
povolení	povolení	k1gNnSc2	povolení
staroměstským	staroměstský	k2eAgMnPc3d1	staroměstský
měšťanům	měšťan	k1gMnPc3	měšťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zřídili	zřídit	k5eAaPmAgMnP	zřídit
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
patricijském	patricijský	k2eAgInSc6d1	patricijský
domě	dům	k1gInSc6	dům
Wolflina	Wolflina	k1gFnSc1	Wolflina
od	od	k7c2	od
Kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
domu	dům	k1gInSc2	dům
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
na	na	k7c6	na
starších	starý	k2eAgInPc6d2	starší
základech	základ	k1gInPc6	základ
zahájena	zahájen	k2eAgFnSc1d1	zahájena
stavba	stavba	k1gFnSc1	stavba
věže	věž	k1gFnSc2	věž
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
umístěny	umístěn	k2eAgFnPc4d1	umístěna
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
i	i	k9	i
zvon	zvon	k1gInSc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
také	také	k9	také
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
vysvěcená	vysvěcený	k2eAgFnSc1d1	vysvěcená
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1805	[number]	k4	1805
až	až	k9	až
1807	[number]	k4	1807
byl	být	k5eAaImAgInS	být
dostavěn	dostavět	k5eAaPmNgInS	dostavět
i	i	k9	i
ochoz	ochoz	k1gInSc1	ochoz
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
patrně	patrně	k6eAd1	patrně
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
nechal	nechat	k5eAaPmAgMnS	nechat
sestavit	sestavit	k5eAaPmF	sestavit
u	u	k7c2	u
hodináře-horologisty	hodinářeorologista	k1gMnSc2	hodináře-horologista
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
později	pozdě	k6eAd2	pozdě
pracovali	pracovat	k5eAaImAgMnP	pracovat
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
a	a	k8xC	a
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
dílo	dílo	k1gNnSc4	dílo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
vznik	vznik	k1gInSc1	vznik
orloje	orloj	k1gInSc2	orloj
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
orloje	orloj	k1gInSc2	orloj
je	být	k5eAaImIp3nS	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
a	a	k8xC	a
astronomický	astronomický	k2eAgInSc1d1	astronomický
číselník	číselník	k1gInSc1	číselník
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
hodinář	hodinář	k1gMnSc1	hodinář
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Šindela	Šindel	k1gMnSc2	Šindel
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
rektora	rektor	k1gMnSc2	rektor
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
pražský	pražský	k2eAgInSc4d1	pražský
orloj	orloj	k1gInSc4	orloj
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
starší	starý	k2eAgInSc1d2	starší
orloj	orloj	k1gInSc1	orloj
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
další	další	k2eAgInPc4d1	další
evropské	evropský	k2eAgInPc4d1	evropský
orloje	orloj	k1gInPc4	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
bezprostředně	bezprostředně	k6eAd1	bezprostředně
kolem	kolem	k7c2	kolem
ciferníku	ciferník	k1gInSc2	ciferník
orloje	orloj	k1gInSc2	orloj
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dílen	dílna	k1gFnPc2	dílna
kamenické	kamenický	k2eAgFnSc2d1	kamenická
huti	huť	k1gFnSc2	huť
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stavěla	stavět	k5eAaImAgFnS	stavět
i	i	k9	i
sousední	sousední	k2eAgFnSc6d1	sousední
kapli	kaple	k1gFnSc6	kaple
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
působila	působit	k5eAaImAgFnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1	okolní
bohatá	bohatý	k2eAgFnSc1d1	bohatá
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
orloj	orloj	k1gInSc1	orloj
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1490	[number]	k4	1490
hodinář	hodinář	k1gMnSc1	hodinář
Jan	Jan	k1gMnSc1	Jan
Růže	růž	k1gFnSc2	růž
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgMnSc1d1	zvaný
také	také	k9	také
mistr	mistr	k1gMnSc1	mistr
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
)	)	kIx)	)
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pomocníkem	pomocník	k1gMnSc7	pomocník
Jakubem	Jakub	k1gMnSc7	Jakub
Čechem	Čech	k1gMnSc7	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
byl	být	k5eAaImAgMnS	být
Hanuš	Hanuš	k1gMnSc1	Hanuš
nakonec	nakonec	k6eAd1	nakonec
oslepen	oslepen	k2eAgMnSc1d1	oslepen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nemohl	moct	k5eNaImAgMnS	moct
postavit	postavit	k5eAaPmF	postavit
podobné	podobný	k2eAgFnPc4d1	podobná
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
omyl	omyl	k1gInSc1	omyl
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
nesprávné	správný	k2eNgFnSc2d1	nesprávná
interpretace	interpretace	k1gFnSc2	interpretace
záznamů	záznam	k1gInPc2	záznam
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
spravoval	spravovat	k5eAaImAgMnS	spravovat
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
z	z	k7c2	z
Klokotské	Klokotský	k2eAgFnSc2d1	Klokotská
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Omyl	omyl	k1gInSc1	omyl
později	pozdě	k6eAd2	pozdě
vyvrátil	vyvrátit	k5eAaPmAgInS	vyvrátit
až	až	k9	až
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horský	Horský	k1gMnSc1	Horský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
objevila	objevit	k5eAaPmAgFnS	objevit
orlojnická	orlojnický	k2eAgFnSc1d1	orlojnický
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
opisem	opis	k1gInSc7	opis
německé	německý	k2eAgFnSc2d1	německá
listiny	listina	k1gFnSc2	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
radnice	radnice	k1gFnSc1	radnice
děkuje	děkovat	k5eAaImIp3nS	děkovat
Mikulášovi	Mikuláš	k1gMnSc3	Mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
za	za	k7c4	za
dobrou	dobrý	k2eAgFnSc4d1	dobrá
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orloj	orloj	k1gInSc1	orloj
stručně	stručně	k6eAd1	stručně
ale	ale	k8xC	ale
přesně	přesně	k6eAd1	přesně
popisuje	popisovat	k5eAaImIp3nS	popisovat
a	a	k8xC	a
za	za	k7c4	za
odměnu	odměna	k1gFnSc4	odměna
dává	dávat	k5eAaImIp3nS	dávat
hodináři	hodinář	k1gMnSc3	hodinář
dům	dům	k1gInSc4	dům
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
roční	roční	k2eAgInSc4d1	roční
plat	plat	k1gInSc4	plat
<g/>
.	.	kIx.	.
</s>
<s>
Předpoklad	předpoklad	k1gInSc1	předpoklad
o	o	k7c6	o
autorství	autorství	k1gNnSc6	autorství
mistra	mistr	k1gMnSc2	mistr
Hanuše	Hanuš	k1gMnSc2	Hanuš
patrně	patrně	k6eAd1	patrně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
přestavbou	přestavba	k1gFnSc7	přestavba
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1470	[number]	k4	1470
až	až	k9	až
1473	[number]	k4	1473
<g/>
,	,	kIx,	,
úpravami	úprava	k1gFnPc7	úprava
a	a	k8xC	a
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mistr	mistr	k1gMnSc1	mistr
Hanuš	Hanuš	k1gMnSc1	Hanuš
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
provedl	provést	k5eAaPmAgMnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
orloj	orloj	k1gInSc1	orloj
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zastavil	zastavit	k5eAaPmAgInS	zastavit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
opravován	opravovat	k5eAaImNgInS	opravovat
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
nebo	nebo	k8xC	nebo
1659	[number]	k4	1659
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
orloji	orloj	k1gInSc3	orloj
přenesen	přenést	k5eAaPmNgInS	přenést
i	i	k9	i
středověký	středověký	k2eAgInSc1d1	středověký
bicí	bicí	k2eAgInSc1d1	bicí
stroj	stroj	k1gInSc1	stroj
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
přidány	přidán	k2eAgFnPc4d1	přidána
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
sošky	soška	k1gFnPc4	soška
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
orloj	orloj	k1gInSc4	orloj
upadal	upadat	k5eAaPmAgMnS	upadat
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgMnS	přestat
lidi	člověk	k1gMnPc4	člověk
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
kritickém	kritický	k2eAgInSc6d1	kritický
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
pražský	pražský	k2eAgInSc1d1	pražský
magistrát	magistrát	k1gInSc1	magistrát
uvažoval	uvažovat	k5eAaImAgInS	uvažovat
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
orloje	orloj	k1gInSc2	orloj
do	do	k7c2	do
starého	starý	k2eAgNnSc2d1	staré
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
záchranu	záchrana	k1gFnSc4	záchrana
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
a	a	k8xC	a
meteorolog	meteorolog	k1gMnSc1	meteorolog
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Antonín	Antonín	k1gMnSc1	Antonín
Strnad	Strnad	k1gMnSc1	Strnad
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chápal	chápat	k5eAaImAgMnS	chápat
jeho	jeho	k3xOp3gFnSc4	jeho
historickou	historický	k2eAgFnSc4d1	historická
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
opravu	oprava	k1gFnSc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delším	dlouhý	k2eAgNnSc6d2	delší
úsilí	úsilí	k1gNnSc6	úsilí
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
magistrátního	magistrátní	k2eAgMnSc4d1	magistrátní
radu	rada	k1gMnSc4	rada
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
magistrát	magistrát	k1gInSc4	magistrát
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
finanční	finanční	k2eAgFnSc2d1	finanční
částky	částka	k1gFnSc2	částka
potřebné	potřebný	k2eAgFnSc2d1	potřebná
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Strnadova	Strnadův	k2eAgInSc2d1	Strnadův
odborného	odborný	k2eAgInSc2d1	odborný
dohledu	dohled	k1gInSc2	dohled
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
oprava	oprava	k1gFnSc1	oprava
za	za	k7c4	za
793	[number]	k4	793
zlatých	zlatá	k1gFnPc2	zlatá
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1787	[number]	k4	1787
až	až	k9	až
1791	[number]	k4	1791
hodinářem	hodinář	k1gMnSc7	hodinář
Šimonem	Šimon	k1gMnSc7	Šimon
Landspergerem	Landsperger	k1gMnSc7	Landsperger
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
patrně	patrně	k6eAd1	patrně
přibyly	přibýt	k5eAaPmAgFnP	přibýt
i	i	k9	i
figurky	figurka	k1gFnPc1	figurka
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
další	další	k2eAgFnSc2d1	další
velké	velký	k2eAgFnSc2d1	velká
opravy	oprava	k1gFnSc2	oprava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
až	až	k9	až
1866	[number]	k4	1866
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
do	do	k7c2	do
chodu	chod	k1gInSc2	chod
i	i	k8xC	i
astronomické	astronomický	k2eAgFnSc2d1	astronomická
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
přidána	přidán	k2eAgFnSc1d1	přidána
ozvučená	ozvučený	k2eAgFnSc1d1	ozvučená
soška	soška	k1gFnSc1	soška
kohouta	kohout	k1gMnSc2	kohout
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
pohon	pohon	k1gInSc1	pohon
a	a	k8xC	a
krok	krok	k1gInSc1	krok
stroje	stroj	k1gInSc2	stroj
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
daleko	daleko	k6eAd1	daleko
dokonalejším	dokonalý	k2eAgInSc7d2	dokonalejší
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
orloj	orloj	k1gInSc1	orloj
začal	začít	k5eAaPmAgInS	začít
ukazovat	ukazovat	k5eAaImF	ukazovat
přesný	přesný	k2eAgInSc1d1	přesný
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
oprava	oprava	k1gFnSc1	oprava
a	a	k8xC	a
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
orloje	orloj	k1gInSc2	orloj
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
vedle	vedle	k7c2	vedle
orloje	orloj	k1gInSc2	orloj
zasazena	zasazen	k2eAgFnSc1d1	zasazena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
s	s	k7c7	s
mylně	mylně	k6eAd1	mylně
uvedeným	uvedený	k2eAgNnSc7d1	uvedené
autorstvím	autorství	k1gNnSc7	autorství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
druhé	druhý	k4xOgFnPc4	druhý
světové	světový	k2eAgFnPc4d1	světová
války	válka	k1gFnPc4	válka
byl	být	k5eAaImAgInS	být
orloj	orloj	k1gInSc1	orloj
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
zasažené	zasažený	k2eAgNnSc1d1	zasažené
dělostřeleckým	dělostřelecký	k2eAgInSc7d1	dělostřelecký
granátem	granát	k1gInSc7	granát
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
cihelná	cihelný	k2eAgFnSc1d1	cihelná
obezdívka	obezdívka	k1gFnSc1	obezdívka
ochránila	ochránit	k5eAaPmAgFnS	ochránit
stroj	stroj	k1gInSc4	stroj
před	před	k7c7	před
větším	veliký	k2eAgNnSc7d2	veliký
poškozením	poškození	k1gNnSc7	poškození
<g/>
,	,	kIx,	,
shořela	shořet	k5eAaPmAgFnS	shořet
však	však	k9	však
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
stál	stát	k5eAaImAgMnS	stát
<g/>
,	,	kIx,	,
i	i	k8xC	i
mechanismus	mechanismus	k1gInSc1	mechanismus
s	s	k7c7	s
apoštoly	apoštol	k1gMnPc7	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zdála	zdát	k5eAaImAgFnS	zdát
nenapravitelná	napravitelný	k2eNgFnSc1d1	nenapravitelná
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
náhradě	náhrada	k1gFnSc6	náhrada
orloje	orloj	k1gInSc2	orloj
moderním	moderní	k2eAgInSc7d1	moderní
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgNnSc6d1	velké
úsilí	úsilí	k1gNnSc6	úsilí
zejména	zejména	k9	zejména
bratří	bratřit	k5eAaImIp3nS	bratřit
Veseckých	Vesecká	k1gFnPc2	Vesecká
byl	být	k5eAaImAgInS	být
opravený	opravený	k2eAgInSc1d1	opravený
orloj	orloj	k1gInSc1	orloj
uveden	uveden	k2eAgInSc1d1	uveden
do	do	k7c2	do
chodu	chod	k1gInSc2	chod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
opravě	oprava	k1gFnSc6	oprava
však	však	k9	však
byly	být	k5eAaImAgInP	být
nesprávně	správně	k6eNd1	správně
namalovány	namalován	k2eAgInPc4d1	namalován
barevné	barevný	k2eAgInPc4d1	barevný
horizonty	horizont	k1gInPc4	horizont
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
desce	deska	k1gFnSc6	deska
astronomického	astronomický	k2eAgInSc2d1	astronomický
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
chybě	chyba	k1gFnSc3	chyba
orloj	orloj	k1gInSc1	orloj
nesprávně	správně	k6eNd1	správně
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
přechod	přechod	k1gInSc1	přechod
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
svítání	svítání	k1gNnSc2	svítání
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
soumraku	soumrak	k1gInSc2	soumrak
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
odchylkou	odchylka	k1gFnSc7	odchylka
až	až	k9	až
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Chyba	chyba	k1gFnSc1	chyba
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
byl	být	k5eAaImAgInS	být
orloj	orloj	k1gInSc1	orloj
rekonstruován	rekonstruovat	k5eAaBmNgInS	rekonstruovat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrestaurování	zrestaurování	k1gNnSc3	zrestaurování
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
spodního	spodní	k2eAgInSc2d1	spodní
kruhu	kruh	k1gInSc2	kruh
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Mánesa	Mánes	k1gMnSc2	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
zakryty	zakrýt	k5eAaPmNgFnP	zakrýt
sítí	síť	k1gFnSc7	síť
proti	proti	k7c3	proti
holubům	holub	k1gMnPc3	holub
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
orlojníkem	orlojník	k1gMnSc7	orlojník
byl	být	k5eAaImAgMnS	být
hodinář	hodinář	k1gMnSc1	hodinář
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
je	být	k5eAaImIp3nS	být
přisuzována	přisuzován	k2eAgFnSc1d1	přisuzována
stavba	stavba	k1gFnSc1	stavba
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1475	[number]	k4	1475
převzal	převzít	k5eAaPmAgInS	převzít
orloj	orloj	k1gInSc1	orloj
mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
)	)	kIx)	)
a	a	k8xC	a
spravoval	spravovat	k5eAaImAgMnS	spravovat
orloj	orloj	k1gInSc4	orloj
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roce	rok	k1gInSc6	rok
1497	[number]	k4	1497
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
převzal	převzít	k5eAaPmAgMnS	převzít
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
mistr	mistr	k1gMnSc1	mistr
Jakub	Jakub	k1gMnSc1	Jakub
příjmením	příjmení	k1gNnSc7	příjmení
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
synem	syn	k1gMnSc7	syn
Jana	Jan	k1gMnSc2	Jan
Růže	růž	k1gFnSc2	růž
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
dobře	dobře	k6eAd1	dobře
seznámen	seznámen	k2eAgInSc1d1	seznámen
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Jakub	Jakub	k1gMnSc1	Jakub
pak	pak	k6eAd1	pak
spravoval	spravovat	k5eAaImAgMnS	spravovat
orloj	orloj	k1gInSc4	orloj
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
převzal	převzít	k5eAaPmAgMnS	převzít
funkci	funkce	k1gFnSc4	funkce
orlojníka	orlojník	k1gMnSc2	orlojník
Václav	Václav	k1gMnSc1	Václav
Zvůnek	Zvůnek	k1gMnSc1	Zvůnek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
složitý	složitý	k2eAgInSc4d1	složitý
stroj	stroj	k1gInSc4	stroj
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
uspokojivém	uspokojivý	k2eAgInSc6d1	uspokojivý
chodu	chod	k1gInSc6	chod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1552	[number]	k4	1552
byly	být	k5eAaImAgInP	být
klíče	klíč	k1gInPc1	klíč
od	od	k7c2	od
orloje	orloj	k1gInSc2	orloj
dány	dát	k5eAaPmNgFnP	dát
mistru	mistr	k1gMnSc3	mistr
Janu	Jan	k1gMnSc3	Jan
Šteinmeisseovi	Šteinmeisseus	k1gMnSc3	Šteinmeisseus
<g/>
,	,	kIx,	,
hodináři	hodinář	k1gMnSc3	hodinář
z	z	k7c2	z
Plantnéřské	Plantnéřský	k2eAgFnSc2d1	Plantnéřský
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nechtěl	chtít	k5eNaImAgMnS	chtít
o	o	k7c4	o
orloj	orloj	k1gInSc4	orloj
starat	starat	k5eAaImF	starat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
předala	předat	k5eAaPmAgFnS	předat
správcovství	správcovství	k1gNnSc4	správcovství
orloje	orloj	k1gInSc2	orloj
Janu	Jan	k1gMnSc3	Jan
Táborskému	Táborský	k1gMnSc3	Táborský
z	z	k7c2	z
Klokotské	Klokotský	k2eAgFnSc2d1	Klokotská
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Pomocníkem	pomocník	k1gMnSc7	pomocník
Táborského	Táborského	k2eAgFnSc1d1	Táborského
v	v	k7c6	v
údržbě	údržba	k1gFnSc6	údržba
orloje	orloj	k1gInSc2	orloj
byl	být	k5eAaImAgInS	být
Daniel	Daniel	k1gMnSc1	Daniel
Skřivan	Skřivan	k1gMnSc1	Skřivan
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
správce	správce	k1gMnSc1	správce
orloje	orloj	k1gInSc2	orloj
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1556	[number]	k4	1556
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
správa	správa	k1gFnSc1	správa
odňata	odnít	k5eAaPmNgFnS	odnít
a	a	k8xC	a
orloj	orloj	k1gInSc1	orloj
byl	být	k5eAaImAgInS	být
svěřen	svěřit	k5eAaPmNgInS	svěřit
Václavu	Václav	k1gMnSc3	Václav
Tobiášovi	Tobiáš	k1gMnSc3	Tobiáš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
spravoval	spravovat	k5eAaImAgMnS	spravovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1560	[number]	k4	1560
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
správcem	správce	k1gMnSc7	správce
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
Táborský	táborský	k2eAgMnSc1d1	táborský
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vyučil	vyučit	k5eAaPmAgMnS	vyučit
svého	svůj	k3xOyFgMnSc4	svůj
zástupce	zástupce	k1gMnSc4	zástupce
Jakuba	Jakub	k1gMnSc2	Jakub
Špačka	Špaček	k1gMnSc2	Špaček
a	a	k8xC	a
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
tajů	taj	k1gInPc2	taj
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
,	,	kIx,	,
spravoval	spravovat	k5eAaImAgInS	spravovat
orloj	orloj	k1gInSc1	orloj
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
vůle	vůle	k1gFnSc2	vůle
Jakub	Jakub	k1gMnSc1	Jakub
Špaček	Špaček	k1gMnSc1	Špaček
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
orlojníkem	orlojník	k1gMnSc7	orlojník
byl	být	k5eAaImAgMnS	být
mistr	mistr	k1gMnSc1	mistr
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1593	[number]	k4	1593
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Šimon	Šimon	k1gMnSc1	Šimon
Podolský	podolský	k2eAgMnSc1d1	podolský
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
Jiřík	Jiřík	k1gMnSc1	Jiřík
Švorcpach	Švorcpach	k1gMnSc1	Švorcpach
(	(	kIx(	(
<g/>
Šworcpach	Šworcpach	k1gMnSc1	Šworcpach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
souvislá	souvislý	k2eAgFnSc1d1	souvislá
řada	řada	k1gFnSc1	řada
známých	známý	k2eAgMnPc2d1	známý
starých	starý	k2eAgMnPc2d1	starý
orlojníků	orlojník	k1gMnPc2	orlojník
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
dalším	další	k2eAgMnSc7d1	další
orlojníkem	orlojník	k1gMnSc7	orlojník
byl	být	k5eAaImAgMnS	být
až	až	k9	až
P.	P.	kA	P.
Jan	Jan	k1gMnSc1	Jan
Klein	Klein	k1gMnSc1	Klein
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
o	o	k7c4	o
orloj	orloj	k1gInSc4	orloj
stará	starat	k5eAaImIp3nS	starat
Petr	Petr	k1gMnSc1	Petr
Skála	Skála	k1gMnSc1	Skála
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
doma	doma	k6eAd1	doma
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
přenos	přenos	k1gInSc4	přenos
z	z	k7c2	z
kamery	kamera	k1gFnSc2	kamera
z	z	k7c2	z
protějšího	protější	k2eAgInSc2d1	protější
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
orlojníka	orlojník	k1gMnSc4	orlojník
popsal	popsat	k5eAaPmAgInS	popsat
orloj	orloj	k1gInSc1	orloj
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
z	z	k7c2	z
Klokotské	Klokotský	k2eAgFnSc2d1	Klokotská
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
orloji	orloj	k1gInSc6	orloj
staroměstském	staroměstský	k2eAgInSc6d1	staroměstský
1570	[number]	k4	1570
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
orloj	orloj	k1gInSc1	orloj
poháněn	poháněn	k2eAgInSc1d1	poháněn
jediným	jediný	k2eAgInSc7d1	jediný
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obstarával	obstarávat	k5eAaImAgMnS	obstarávat
všechny	všechen	k3xTgInPc4	všechen
čtyři	čtyři	k4xCgInPc4	čtyři
pohyby	pohyb	k1gInPc4	pohyb
astronomického	astronomický	k2eAgInSc2d1	astronomický
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
,	,	kIx,	,
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
pohyb	pohyb	k1gInSc4	pohyb
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
i	i	k8xC	i
bicí	bicí	k2eAgInSc1d1	bicí
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
snesený	snesený	k2eAgInSc1d1	snesený
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
orloj	orloj	k1gInSc1	orloj
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
otáčivý	otáčivý	k2eAgInSc4d1	otáčivý
pohon	pohon	k1gInSc4	pohon
měsíční	měsíční	k2eAgFnSc2d1	měsíční
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
byl	být	k5eAaImAgInS	být
nepřesný	přesný	k2eNgInSc1d1	nepřesný
krok	krok	k1gInSc1	krok
nahrazen	nahradit	k5eAaPmNgInS	nahradit
samostatným	samostatný	k2eAgInSc7d1	samostatný
chronometrem	chronometr	k1gInSc7	chronometr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hlavní	hlavní	k2eAgInSc1d1	hlavní
stroj	stroj	k1gInSc1	stroj
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dostaly	dostat	k5eAaPmAgInP	dostat
měsíční	měsíční	k2eAgFnSc1d1	měsíční
ručka	ručka	k1gFnSc1	ručka
a	a	k8xC	a
čtyřiadvacetník	čtyřiadvacetník	k1gMnSc1	čtyřiadvacetník
samostatné	samostatný	k2eAgInPc4d1	samostatný
pohony	pohon	k1gInPc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
orloje	orloj	k1gInSc2	orloj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
strojů	stroj	k1gInPc2	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každá	každý	k3xTgFnSc1	každý
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
dílčí	dílčí	k2eAgInSc1d1	dílčí
stroj	stroj	k1gInSc1	stroj
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
ovládá	ovládat	k5eAaImIp3nS	ovládat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
orloje	orloj	k1gInSc2	orloj
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgInSc4d1	tvořící
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
:	:	kIx,	:
stroj	stroj	k1gInSc1	stroj
hlavní	hlavní	k2eAgInSc1d1	hlavní
(	(	kIx(	(
<g/>
jicí	jicí	k2eAgInSc1d1	jicí
nebo	nebo	k8xC	nebo
také	také	k9	také
minutní	minutní	k2eAgInSc1d1	minutní
stroj	stroj	k1gInSc1	stroj
<g/>
)	)	kIx)	)
stroj	stroj	k1gInSc1	stroj
<g />
.	.	kIx.	.
</s>
<s>
ukazovací	ukazovací	k2eAgInSc4d1	ukazovací
stroj	stroj	k1gInSc4	stroj
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
stroj	stroj	k1gInSc4	stroj
bicí	bicí	k2eAgInSc4d1	bicí
stroj	stroj	k1gInSc4	stroj
měsíce	měsíc	k1gInSc2	měsíc
stroj	stroj	k1gInSc1	stroj
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
stroj	stroj	k1gInSc1	stroj
kalendářní	kalendářní	k2eAgInSc1d1	kalendářní
Božkův	Božkův	k2eAgInSc1d1	Božkův
chronometr	chronometr	k1gInSc1	chronometr
Stroj	stroj	k1gInSc1	stroj
hlavní	hlavní	k2eAgInSc1d1	hlavní
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
jicí	jicí	k2eAgInSc1d1	jicí
(	(	kIx(	(
<g/>
jdoucí	jdoucí	k2eAgFnPc1d1	jdoucí
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Gehwerk	Gehwerk	k1gInSc1	Gehwerk
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojený	spojený	k2eAgInSc1d1	spojený
stroj	stroj	k1gInSc4	stroj
ukazovací	ukazovací	k2eAgInSc4d1	ukazovací
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
zvířetníku	zvířetník	k1gInSc2	zvířetník
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
závažím	závaží	k1gNnSc7	závaží
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
80	[number]	k4	80
kg	kg	kA	kg
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uvádělo	uvádět	k5eAaImAgNnS	uvádět
celý	celý	k2eAgInSc4d1	celý
stroj	stroj	k1gInSc4	stroj
v	v	k7c4	v
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
řízen	řídit	k5eAaImNgInS	řídit
vřetenovým	vřetenový	k2eAgInSc7d1	vřetenový
krokem	krok	k1gInSc7	krok
s	s	k7c7	s
lihýřem	lihýř	k1gInSc7	lihýř
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
kroku	krok	k1gInSc2	krok
hodin	hodina	k1gFnPc2	hodina
bylo	být	k5eAaImAgNnS	být
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
(	(	kIx(	(
<g/>
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
až	až	k9	až
čtvrthodinovou	čtvrthodinový	k2eAgFnSc4d1	čtvrthodinová
odchylku	odchylka	k1gFnSc4	odchylka
od	od	k7c2	od
přesného	přesný	k2eAgInSc2d1	přesný
času	čas	k1gInSc2	čas
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
byl	být	k5eAaImAgInS	být
krok	krok	k1gInSc1	krok
nahrazen	nahradit	k5eAaPmNgInS	nahradit
velmi	velmi	k6eAd1	velmi
dokonalým	dokonalý	k2eAgInSc7d1	dokonalý
mechanickým	mechanický	k2eAgInSc7d1	mechanický
chronometrem	chronometr	k1gInSc7	chronometr
Romualda	Romuald	k1gMnSc2	Romuald
Božka	Božek	k1gMnSc2	Božek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hlavní	hlavní	k2eAgInSc1d1	hlavní
stroj	stroj	k1gInSc1	stroj
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
a	a	k8xC	a
posouvá	posouvat	k5eAaImIp3nS	posouvat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
orloj	orloj	k1gInSc1	orloj
maximálně	maximálně	k6eAd1	maximálně
třísekundovou	třísekundový	k2eAgFnSc4d1	třísekundová
odchylku	odchylka	k1gFnSc4	odchylka
od	od	k7c2	od
přesného	přesný	k2eAgInSc2d1	přesný
času	čas	k1gInSc2	čas
za	za	k7c4	za
týden	týden	k1gInSc4	týden
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
jen	jen	k6eAd1	jen
půl	půl	k1xP	půl
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
poháněny	poháněn	k2eAgFnPc1d1	poháněna
závažími	závaží	k1gNnPc7	závaží
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ale	ale	k8xC	ale
automatické	automatický	k2eAgNnSc4d1	automatické
natahování	natahování	k1gNnSc4	natahování
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc4	tři
kola	kolo	k1gNnPc4	kolo
mají	mít	k5eAaImIp3nP	mít
365	[number]	k4	365
(	(	kIx(	(
<g/>
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
366	[number]	k4	366
(	(	kIx(	(
<g/>
Zodiak-Zvěrokruh	Zodiak-Zvěrokruh	k1gInSc1	Zodiak-Zvěrokruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
379	[number]	k4	379
(	(	kIx(	(
<g/>
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgNnPc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
pohání	pohánět	k5eAaImIp3nP	pohánět
pastorek	pastorek	k1gInSc4	pastorek
s	s	k7c7	s
24	[number]	k4	24
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
propojený	propojený	k2eAgInSc1d1	propojený
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Ukazovací	ukazovací	k2eAgInSc1d1	ukazovací
stroj	stroj	k1gInSc1	stroj
vede	vést	k5eAaImIp3nS	vést
pomocí	pomocí	k7c2	pomocí
tří	tři	k4xCgInPc2	tři
souosých	souosý	k2eAgInPc2d1	souosý
hřídelů	hřídel	k1gInPc2	hřídel
tři	tři	k4xCgInPc1	tři
ukazatele	ukazatel	k1gInPc1	ukazatel
pro	pro	k7c4	pro
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
se	s	k7c7	s
zvířetníkem	zvířetník	k1gInSc7	zvířetník
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
spouští	spouštět	k5eAaImIp3nS	spouštět
v	v	k7c6	v
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
30	[number]	k4	30
a	a	k8xC	a
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
další	další	k2eAgInPc4d1	další
pomocné	pomocný	k2eAgInPc4d1	pomocný
stroje	stroj	k1gInPc4	stroj
<g/>
:	:	kIx,	:
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
zvonící	zvonící	k2eAgFnSc1d1	zvonící
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc1	stroj
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
stroj	stroj	k1gInSc1	stroj
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
tyto	tento	k3xDgInPc4	tento
stroje	stroj	k1gInPc4	stroj
vykonají	vykonat	k5eAaPmIp3nP	vykonat
určené	určený	k2eAgInPc4d1	určený
pohyby	pohyb	k1gInPc4	pohyb
s	s	k7c7	s
přidruženým	přidružený	k2eAgInSc7d1	přidružený
mechanismem	mechanismus	k1gInSc7	mechanismus
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
svého	svůj	k3xOyFgInSc2	svůj
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zastaví	zastavit	k5eAaPmIp3nS	zastavit
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
uzamknou	uzamknout	k5eAaPmIp3nP	uzamknout
<g/>
.	.	kIx.	.
</s>
<s>
Zvonící	zvonící	k2eAgInSc1d1	zvonící
stroj	stroj	k1gInSc1	stroj
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
zvoněním	zvonění	k1gNnSc7	zvonění
na	na	k7c4	na
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
odbíjení	odbíjení	k1gNnSc4	odbíjení
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
bicí	bicí	k2eAgInSc4d1	bicí
stoj	stoj	k1gInSc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Kalendářní	kalendářní	k2eAgInSc1d1	kalendářní
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
také	také	k9	také
vypouštěn	vypouštět	k5eAaImNgInS	vypouštět
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
a	a	k8xC	a
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
každých	každý	k3xTgFnPc2	každý
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
posune	posunout	k5eAaPmIp3nS	posunout
kalendářní	kalendářní	k2eAgFnSc4d1	kalendářní
desku	deska	k1gFnSc4	deska
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ciferníkem	ciferník	k1gInSc7	ciferník
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
novodobý	novodobý	k2eAgInSc1d1	novodobý
stroj	stroj	k1gInSc1	stroj
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
samostatný	samostatný	k2eAgInSc1d1	samostatný
pohon	pohon	k1gInSc1	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
původní	původní	k2eAgFnSc2d1	původní
mechaniky	mechanika	k1gFnSc2	mechanika
orloje	orloj	k1gInSc2	orloj
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc4	tři
kovaná	kovaný	k2eAgNnPc4d1	kované
ozubená	ozubený	k2eAgNnPc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
116	[number]	k4	116
cm	cm	kA	cm
poháněná	poháněný	k2eAgFnSc1d1	poháněná
jedním	jeden	k4xCgInSc7	jeden
společným	společný	k2eAgInSc7d1	společný
pastorkem	pastorek	k1gInSc7	pastorek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Slunce	slunce	k1gNnSc4	slunce
má	mít	k5eAaImIp3nS	mít
kolo	kolo	k1gNnSc4	kolo
366	[number]	k4	366
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Měsíc	měsíc	k1gInSc4	měsíc
379	[number]	k4	379
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
pro	pro	k7c4	pro
zvířetník	zvířetník	k1gInSc4	zvířetník
365	[number]	k4	365
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
opravný	opravný	k2eAgInSc1d1	opravný
mechanismus	mechanismus	k1gInSc1	mechanismus
pro	pro	k7c4	pro
chod	chod	k1gInSc4	chod
měsíce	měsíc	k1gInSc2	měsíc
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
převodového	převodový	k2eAgInSc2d1	převodový
poměru	poměr	k1gInSc2	poměr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
měsíc	měsíc	k1gInSc4	měsíc
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
správně	správně	k6eAd1	správně
378,8	[number]	k4	378,8
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřiadvacetník	Čtyřiadvacetník	k1gInSc1	Čtyřiadvacetník
je	být	k5eAaImIp3nS	být
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
pootáčení	pootáčení	k1gNnSc3	pootáčení
vnějšího	vnější	k2eAgNnSc2d1	vnější
"	"	kIx"	"
<g/>
staročeského	staročeský	k2eAgMnSc2d1	staročeský
<g/>
"	"	kIx"	"
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
24	[number]	k4	24
gotických	gotický	k2eAgFnPc2d1	gotická
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
odečítat	odečítat	k5eAaImF	odečítat
staročeský	staročeský	k2eAgInSc4d1	staročeský
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
den	den	k1gInSc1	den
začínal	začínat	k5eAaImAgInS	začínat
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kývavý	kývavý	k2eAgInSc1d1	kývavý
pohyb	pohyb	k1gInSc1	pohyb
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
realizován	realizovat	k5eAaBmNgInS	realizovat
klikovým	klikový	k2eAgInSc7d1	klikový
mechanismem	mechanismus	k1gInSc7	mechanismus
od	od	k7c2	od
kalendářního	kalendářní	k2eAgNnSc2d1	kalendářní
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1570	[number]	k4	1570
popisuje	popisovat	k5eAaImIp3nS	popisovat
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
střední	střední	k2eAgFnSc1d1	střední
odchylka	odchylka	k1gFnSc1	odchylka
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
činila	činit	k5eAaImAgFnS	činit
jen	jen	k9	jen
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
maximální	maximální	k2eAgFnPc4d1	maximální
hodnoty	hodnota	k1gFnPc4	hodnota
11	[number]	k4	11
minut	minuta	k1gFnPc2	minuta
koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
klikový	klikový	k2eAgInSc1d1	klikový
mechanismus	mechanismus	k1gInSc1	mechanismus
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
odstraněn	odstranit	k5eAaPmNgInS	odstranit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
strojkem	strojek	k1gInSc7	strojek
s	s	k7c7	s
reverzací	reverzace	k1gFnSc7	reverzace
chodu	chod	k1gInSc2	chod
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
velice	velice	k6eAd1	velice
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
neosvědčilo	osvědčit	k5eNaPmAgNnS	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Spouštěcí	spouštěcí	k2eAgInSc1d1	spouštěcí
strojek	strojek	k1gInSc1	strojek
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
uvádět	uvádět	k5eAaImF	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
orloje	orloj	k1gInSc2	orloj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
úplně	úplně	k6eAd1	úplně
vynechán	vynechat	k5eAaPmNgInS	vynechat
a	a	k8xC	a
čtyřiadvacetník	čtyřiadvacetník	k1gInSc1	čtyřiadvacetník
byl	být	k5eAaImAgInS	být
trvale	trvale	k6eAd1	trvale
nastaven	nastavit	k5eAaPmNgInS	nastavit
do	do	k7c2	do
polohy	poloha	k1gFnSc2	poloha
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
pohybový	pohybový	k2eAgInSc1d1	pohybový
mechanismus	mechanismus	k1gInSc1	mechanismus
pro	pro	k7c4	pro
samočinné	samočinný	k2eAgNnSc4d1	samočinné
natáčení	natáčení	k1gNnSc4	natáčení
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Emanuel	Emanuel	k1gMnSc1	Emanuel
Procházka	Procházka	k1gMnSc1	Procházka
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ing.	ing.	kA	ing.
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Krajníkem	krajník	k1gMnSc7	krajník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
se	s	k7c7	s
samostatným	samostatný	k2eAgInSc7d1	samostatný
hodinovým	hodinový	k2eAgInSc7d1	hodinový
strojkem	strojek	k1gInSc7	strojek
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
orloj	orloj	k1gInSc4	orloj
instalován	instalovat	k5eAaBmNgInS	instalovat
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Bicí	bicí	k2eAgInSc1d1	bicí
stroj	stroj	k1gInSc1	stroj
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
orloje	orloj	k1gInSc2	orloj
umístěn	umístit	k5eAaPmNgInS	umístit
přímo	přímo	k6eAd1	přímo
za	za	k7c7	za
ciferníkem	ciferník	k1gInSc7	ciferník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samostatně	samostatně	k6eAd1	samostatně
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
přestavbách	přestavba	k1gFnPc6	přestavba
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
nebo	nebo	k8xC	nebo
1659	[number]	k4	1659
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
za	za	k7c4	za
ciferník	ciferník	k1gInSc4	ciferník
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
konstrukčně	konstrukčně	k6eAd1	konstrukčně
upraven	upravit	k5eAaPmNgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Bicí	bicí	k2eAgInSc1d1	bicí
stroj	stroj	k1gInSc1	stroj
se	se	k3xPyFc4	se
spouští	spouštět	k5eAaImIp3nS	spouštět
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
a	a	k8xC	a
odpočítává	odpočítávat	k5eAaImIp3nS	odpočítávat
údery	úder	k1gInPc4	úder
<g/>
.	.	kIx.	.
</s>
<s>
Použitý	použitý	k2eAgInSc1d1	použitý
"	"	kIx"	"
<g/>
švarcvaldský	švarcvaldský	k2eAgInSc1d1	švarcvaldský
<g/>
"	"	kIx"	"
mechanismus	mechanismus	k1gInSc1	mechanismus
se	s	k7c7	s
závěrkovým	závěrkový	k2eAgNnSc7d1	závěrkové
kolem	kolo	k1gNnSc7	kolo
s	s	k7c7	s
různě	různě	k6eAd1	různě
vzdálenými	vzdálený	k2eAgInPc7d1	vzdálený
zářezy	zářez	k1gInPc7	zářez
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užíval	užívat	k5eAaImAgInS	užívat
u	u	k7c2	u
věžních	věžní	k2eAgFnPc2d1	věžní
i	i	k8xC	i
kukačkových	kukačkový	k2eAgFnPc2d1	Kukačková
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
odpočítávat	odpočítávat	k5eAaImF	odpočítávat
až	až	k9	až
do	do	k7c2	do
24	[number]	k4	24
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hlavního	hlavní	k2eAgNnSc2d1	hlavní
závěrkového	závěrkový	k2eAgNnSc2d1	závěrkové
kola	kolo	k1gNnSc2	kolo
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
malé	malý	k2eAgNnSc1d1	malé
závěrkové	závěrkový	k2eAgNnSc1d1	závěrkové
kolo	kolo	k1gNnSc1	kolo
a	a	k8xC	a
odbíjení	odbíjení	k1gNnSc1	odbíjení
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
až	až	k9	až
když	když	k8xS	když
se	se	k3xPyFc4	se
zářezy	zářez	k1gInPc1	zářez
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
sejdou	sejít	k5eAaPmIp3nP	sejít
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odpočítávání	odpočítávání	k1gNnSc3	odpočítávání
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
využívá	využívat	k5eAaPmIp3nS	využívat
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
matematická	matematický	k2eAgFnSc1d1	matematická
hříčka	hříčka	k1gFnSc1	hříčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
předpokládaného	předpokládaný	k2eAgMnSc2d1	předpokládaný
duchovního	duchovní	k1gMnSc2	duchovní
otce	otec	k1gMnSc2	otec
orloje	orloj	k1gInSc2	orloj
Jana	Jan	k1gMnSc2	Jan
Šindela	Šindel	k1gMnSc2	Šindel
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
šindelovská	šindelovský	k2eAgFnSc1d1	šindelovský
posloupnost	posloupnost	k1gFnSc1	posloupnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
zdvojení	zdvojení	k1gNnSc1	zdvojení
závěrkových	závěrkový	k2eAgFnPc2d1	Závěrková
kol	kola	k1gFnPc2	kola
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
věžních	věžní	k2eAgFnPc2d1	věžní
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Pražský	pražský	k2eAgInSc1d1	pražský
orloj	orloj	k1gInSc1	orloj
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosud	dosud	k6eAd1	dosud
toto	tento	k3xDgNnSc4	tento
řešení	řešení	k1gNnSc4	řešení
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazování	zobrazování	k1gNnSc1	zobrazování
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
fází	fáze	k1gFnPc2	fáze
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
řešilo	řešit	k5eAaImAgNnS	řešit
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
hřídelem	hřídel	k1gInSc7	hřídel
v	v	k7c6	v
ručce	ručka	k1gFnSc6	ručka
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
otáčel	otáčet	k5eAaImAgInS	otáčet
měsíční	měsíční	k2eAgFnSc4d1	měsíční
koulí	koule	k1gFnSc7	koule
nebo	nebo	k8xC	nebo
clonou	clona	k1gFnSc7	clona
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
fáze	fáze	k1gFnPc1	fáze
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Důmyslné	důmyslný	k2eAgNnSc1d1	důmyslné
mechanické	mechanický	k2eAgNnSc1d1	mechanické
řešení	řešení	k1gNnSc1	řešení
pro	pro	k7c4	pro
znázornění	znázornění	k1gNnSc4	znázornění
fází	fáze	k1gFnPc2	fáze
Měsíce	měsíc	k1gInSc2	měsíc
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
orloji	orloj	k1gInSc6	orloj
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
i	i	k8xC	i
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
konstrukcí	konstrukce	k1gFnPc2	konstrukce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
unikátní	unikátní	k2eAgFnSc7d1	unikátní
součástí	součást	k1gFnSc7	součást
orloje	orloj	k1gInSc2	orloj
i	i	k8xC	i
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
koule	koule	k1gFnSc1	koule
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
130	[number]	k4	130
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
povrch	povrch	k1gInSc1	povrch
má	mít	k5eAaImIp3nS	mít
tmavou	tmavý	k2eAgFnSc4d1	tmavá
a	a	k8xC	a
světlou	světlý	k2eAgFnSc4d1	světlá
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
natočení	natočení	k1gNnSc2	natočení
polokoulí	polokoule	k1gFnPc2	polokoule
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
jsou	být	k5eAaImIp3nP	být
vizuálně	vizuálně	k6eAd1	vizuálně
vytvářeny	vytvářen	k2eAgFnPc1d1	vytvářena
fáze	fáze	k1gFnPc1	fáze
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
originální	originální	k2eAgFnSc7d1	originální
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
konstrukcí	konstrukce	k1gFnSc7	konstrukce
bez	bez	k7c2	bez
vnějšího	vnější	k2eAgInSc2d1	vnější
pohonu	pohon	k1gInSc2	pohon
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
koule	koule	k1gFnSc2	koule
je	být	k5eAaImIp3nS	být
ozubený	ozubený	k2eAgInSc4d1	ozubený
věnec	věnec	k1gInSc4	věnec
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
ozubením	ozubení	k1gNnSc7	ozubení
57	[number]	k4	57
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
zabírají	zabírat	k5eAaImIp3nP	zabírat
dva	dva	k4xCgInPc4	dva
půlzávity	půlzávit	k1gInPc4	půlzávit
dvouchodého	dvouchodý	k2eAgInSc2d1	dvouchodý
šneku	šnek	k1gInSc2	šnek
<g/>
.	.	kIx.	.
</s>
<s>
Šnek	šnek	k1gInSc1	šnek
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
závažím	závaží	k1gNnSc7	závaží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
kolmé	kolmý	k2eAgFnSc2d1	kolmá
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
ručka	ručka	k1gFnSc1	ručka
Měsíce	měsíc	k1gInSc2	měsíc
během	během	k7c2	během
dne	den	k1gInSc2	den
mění	měnit	k5eAaImIp3nS	měnit
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
závaží	závaží	k1gNnPc1	závaží
se	se	k3xPyFc4	se
šnekem	šnek	k1gInSc7	šnek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
díky	díky	k7c3	díky
gravitaci	gravitace	k1gFnSc3	gravitace
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
poloze	poloha	k1gFnSc6	poloha
a	a	k8xC	a
šnek	šnek	k1gInSc4	šnek
tak	tak	k6eAd1	tak
pomalu	pomalu	k6eAd1	pomalu
pootáčí	pootáčet	k5eAaImIp3nP	pootáčet
měsíční	měsíční	k2eAgFnPc1d1	měsíční
koulí	koule	k1gFnSc7	koule
kolem	kolem	k7c2	kolem
její	její	k3xOp3gFnSc2	její
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
pro	pro	k7c4	pro
znázorňování	znázorňování	k1gNnSc4	znázorňování
fází	fáze	k1gFnSc7	fáze
měsíce	měsíc	k1gInSc2	měsíc
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
přesný	přesný	k2eAgInSc1d1	přesný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
chyba	chyba	k1gFnSc1	chyba
je	být	k5eAaImIp3nS	být
však	však	k9	však
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
korekci	korekce	k1gFnSc4	korekce
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
4	[number]	k4	4
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
bez	bez	k7c2	bez
korekce	korekce	k1gFnSc2	korekce
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mechanismus	mechanismus	k1gInSc1	mechanismus
předbíhal	předbíhat	k5eAaImAgInS	předbíhat
o	o	k7c4	o
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
<g/>
,	,	kIx,	,
když	když	k8xS	když
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
koule	koule	k1gFnSc1	koule
například	například	k6eAd1	například
přimrzne	přimrznout	k5eAaPmIp3nS	přimrznout
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
poloha	poloha	k1gFnSc1	poloha
opravit	opravit	k5eAaPmF	opravit
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
v	v	k7c4	v
měsíční	měsíční	k2eAgFnSc4d1	měsíční
kouli	koule	k1gFnSc4	koule
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
popsán	popsat	k5eAaPmNgInS	popsat
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1864	[number]	k4	1864
až	až	k9	až
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
a	a	k8xC	a
na	na	k7c4	na
orloj	orloj	k1gInSc4	orloj
osazen	osadit	k5eAaPmNgInS	osadit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
technologie	technologie	k1gFnSc2	technologie
provedení	provedení	k1gNnSc2	provedení
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1659	[number]	k4	1659
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
řešení	řešení	k1gNnSc1	řešení
zobrazování	zobrazování	k1gNnSc2	zobrazování
fází	fáze	k1gFnPc2	fáze
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
známé	známý	k2eAgInPc1d1	známý
ze	z	k7c2	z
zprávy	zpráva	k1gFnSc2	zpráva
Táborského	Táborský	k1gMnSc2	Táborský
<g/>
)	)	kIx)	)
patrně	patrně	k6eAd1	patrně
užívalo	užívat	k5eAaImAgNnS	užívat
převod	převod	k1gInSc4	převod
mezi	mezi	k7c7	mezi
hlavní	hlavní	k2eAgFnSc7d1	hlavní
osou	osa	k1gFnSc7	osa
ukazatele	ukazatel	k1gInPc1	ukazatel
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
ukazatelem	ukazatel	k1gInSc7	ukazatel
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
přes	přes	k7c4	přes
ozubený	ozubený	k2eAgInSc4d1	ozubený
převod	převod	k1gInSc4	převod
odvaloval	odvalovat	k5eAaImAgMnS	odvalovat
po	po	k7c6	po
hlavní	hlavní	k2eAgFnSc6d1	hlavní
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
pevně	pevně	k6eAd1	pevně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
ukazatelem	ukazatel	k1gInSc7	ukazatel
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
natáčel	natáčet	k5eAaImAgMnS	natáčet
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
natáčela	natáčet	k5eAaImAgFnS	natáčet
se	se	k3xPyFc4	se
i	i	k9	i
koule	koule	k1gFnSc1	koule
s	s	k7c7	s
fázemi	fáze	k1gFnPc7	fáze
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
mechanismus	mechanismus	k1gInSc1	mechanismus
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
orloje	orloj	k1gInPc1	orloj
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
odchylku	odchylka	k1gFnSc4	odchylka
až	až	k8xS	až
4	[number]	k4	4
dny	den	k1gInPc7	den
od	od	k7c2	od
skutečné	skutečný	k2eAgFnSc2d1	skutečná
fáze	fáze	k1gFnSc2	fáze
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
cyklické	cyklický	k2eAgFnSc2d1	cyklická
odchylky	odchylka	k1gFnSc2	odchylka
produkuje	produkovat	k5eAaImIp3nS	produkovat
také	také	k9	také
odchylku	odchylka	k1gFnSc4	odchylka
narůstající	narůstající	k2eAgFnSc4d1	narůstající
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
přesně	přesně	k6eAd1	přesně
opačnou	opačný	k2eAgFnSc4d1	opačná
fázi	fáze	k1gFnSc4	fáze
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
byl	být	k5eAaImAgInS	být
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
neznámý	známý	k2eNgMnSc1d1	neznámý
mistr	mistr	k1gMnSc1	mistr
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
unikátní	unikátní	k2eAgInSc1d1	unikátní
nový	nový	k2eAgInSc1d1	nový
mechanismus	mechanismus	k1gInSc1	mechanismus
poháněný	poháněný	k2eAgInSc1d1	poháněný
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
odchylku	odchylka	k1gFnSc4	odchylka
jen	jen	k9	jen
poloviční	poloviční	k2eAgMnSc1d1	poloviční
(	(	kIx(	(
<g/>
a	a	k8xC	a
cyklickou	cyklický	k2eAgFnSc4d1	cyklická
odchylku	odchylka	k1gFnSc4	odchylka
nemá	mít	k5eNaImIp3nS	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
sošek	soška	k1gFnPc2	soška
je	být	k5eAaImIp3nS	být
pomocný	pomocný	k2eAgInSc1d1	pomocný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
spouští	spouštět	k5eAaImIp3nS	spouštět
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
v	v	k7c6	v
denní	denní	k2eAgFnSc6d1	denní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
stroji	stroj	k1gInPc7	stroj
a	a	k8xC	a
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
přenášen	přenášet	k5eAaImNgInS	přenášet
nekonečným	konečný	k2eNgInSc7d1	nekonečný
řetězem	řetěz	k1gInSc7	řetěz
do	do	k7c2	do
komůrky	komůrka	k1gFnSc2	komůrka
o	o	k7c4	o
patro	patro	k1gNnSc4	patro
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Apoštolové	apoštol	k1gMnPc1	apoštol
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
na	na	k7c6	na
spřažených	spřažený	k2eAgInPc6d1	spřažený
vozících	vozík	k1gInPc6	vozík
a	a	k8xC	a
pákový	pákový	k2eAgInSc4d1	pákový
mechanismus	mechanismus	k1gInSc4	mechanismus
s	s	k7c7	s
vratnou	vratný	k2eAgFnSc7d1	vratná
pružinou	pružina	k1gFnSc7	pružina
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
jejich	jejich	k3xOp3gNnSc1	jejich
pootáčení	pootáčení	k1gNnSc1	pootáčení
za	za	k7c7	za
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
pohyby	pohyb	k1gInPc1	pohyb
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
zvedá	zvedat	k5eAaImIp3nS	zvedat
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
žehná	žehnat	k5eAaImIp3nS	žehnat
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
kývne	kývnout	k5eAaPmIp3nS	kývnout
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
souhlasu	souhlas	k1gInSc2	souhlas
a	a	k8xC	a
sv.	sv.	kA	sv.
Tomáš	Tomáš	k1gMnSc1	Tomáš
zavrtí	zavrtit	k5eAaPmIp3nS	zavrtit
hlavou	hlava	k1gFnSc7	hlava
jako	jako	k8xS	jako
nevěřící	nevěřící	k1gFnSc7	nevěřící
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
zarážkovým	zarážkový	k2eAgInSc7d1	zarážkový
systémem	systém	k1gInSc7	systém
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pohybu	pohyb	k1gInSc2	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
otevírání	otevírání	k1gNnSc1	otevírání
okenic	okenice	k1gFnPc2	okenice
i	i	k9	i
pohyby	pohyb	k1gInPc1	pohyb
soch	socha	k1gFnPc2	socha
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
plášti	plášť	k1gInSc6	plášť
orloje	orloj	k1gInSc2	orloj
(	(	kIx(	(
<g/>
Smrtka	Smrtka	k1gFnSc1	Smrtka
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Marnivec	marnivec	k1gMnSc1	marnivec
<g/>
,	,	kIx,	,
Turek	Turek	k1gMnSc1	Turek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
svátků	svátek	k1gInPc2	svátek
(	(	kIx(	(
<g/>
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
-	-	kIx~	-
Bílá	bílý	k2eAgFnSc1d1	bílá
sobota	sobota	k1gFnSc1	sobota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Smrtka	Smrtka	k1gFnSc1	Smrtka
nemá	mít	k5eNaImIp3nS	mít
zvonit	zvonit	k5eAaImF	zvonit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovládající	ovládající	k2eAgInSc1d1	ovládající
drát	drát	k1gInSc1	drát
odpojen	odpojit	k5eAaPmNgInS	odpojit
z	z	k7c2	z
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
dále	daleko	k6eAd2	daleko
ovládá	ovládat	k5eAaImIp3nS	ovládat
pohyb	pohyb	k1gInSc4	pohyb
křídel	křídlo	k1gNnPc2	křídlo
kohouta	kohout	k1gMnSc2	kohout
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
kokrhání	kokrhání	k1gNnSc1	kokrhání
je	být	k5eAaImIp3nS	být
řešeno	řešit	k5eAaImNgNnS	řešit
měchem	měch	k1gInSc7	měch
<g/>
,	,	kIx,	,
stlačovaným	stlačovaný	k2eAgNnSc7d1	stlačované
závažím	závaží	k1gNnSc7	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
se	se	k3xPyFc4	se
závaží	závaží	k1gNnSc1	závaží
nadzvedne	nadzvednout	k5eAaPmIp3nS	nadzvednout
a	a	k8xC	a
měch	měch	k1gInSc1	měch
naplní	naplnit	k5eAaPmIp3nS	naplnit
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
produkce	produkce	k1gFnSc1	produkce
apoštolů	apoštol	k1gMnPc2	apoštol
je	být	k5eAaImIp3nS	být
vzduch	vzduch	k1gInSc1	vzduch
závažím	závaží	k1gNnSc7	závaží
z	z	k7c2	z
měchu	měch	k1gInSc2	měch
hnán	hnát	k5eAaImNgInS	hnát
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
trubek	trubka	k1gFnPc2	trubka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
imitují	imitovat	k5eAaBmIp3nP	imitovat
kokrhání	kokrhání	k1gNnSc4	kokrhání
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
předchází	předcházet	k5eAaImIp3nS	předcházet
odbíjení	odbíjení	k1gNnSc1	odbíjení
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sošky	soška	k1gFnSc2	soška
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Kalendářní	kalendářní	k2eAgInSc1d1	kalendářní
stroj	stroj	k1gInSc1	stroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
kolo	kolo	k1gNnSc4	kolo
s	s	k7c7	s
365	[number]	k4	365
zuby	zub	k1gInPc7	zub
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kalendářní	kalendářní	k2eAgFnSc7d1	kalendářní
deskou	deska	k1gFnSc7	deska
každou	každý	k3xTgFnSc4	každý
půlnoc	půlnoc	k1gFnSc4	půlnoc
pootočí	pootočit	k5eAaPmIp3nS	pootočit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
zub	zub	k1gInSc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
reference	reference	k1gFnSc2	reference
<g/>
)	)	kIx)	)
zůstane	zůstat	k5eAaPmIp3nS	zůstat
kolo	kolo	k1gNnSc4	kolo
jeden	jeden	k4xCgInSc1	jeden
den	den	k1gInSc1	den
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
správcem	správce	k1gMnSc7	správce
orloje	orloj	k1gInSc2	orloj
opět	opět	k6eAd1	opět
Jan	Jan	k1gMnSc1	Jan
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
mechanismus	mechanismus	k1gInSc1	mechanismus
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
den	den	k1gInSc4	den
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
a	a	k8xC	a
pootočí	pootočit	k5eAaPmIp3nS	pootočit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
to	ten	k3xDgNnSc4	ten
musel	muset	k5eAaImAgMnS	muset
orlojník	orlojník	k1gMnSc1	orlojník
vykonávat	vykonávat	k5eAaImF	vykonávat
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Božkův	Božkův	k2eAgInSc1d1	Božkův
chronometr	chronometr	k1gInSc1	chronometr
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
do	do	k7c2	do
soustrojí	soustrojí	k1gNnSc2	soustrojí
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
přesnost	přesnost	k1gFnSc1	přesnost
a	a	k8xC	a
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
chodu	chod	k1gInSc2	chod
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
už	už	k6eAd1	už
neodpovídal	odpovídat	k5eNaImAgInS	odpovídat
potřebám	potřeba	k1gFnPc3	potřeba
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Náhrada	náhrada	k1gFnSc1	náhrada
původního	původní	k2eAgInSc2d1	původní
lihýře	lihýř	k1gInSc2	lihýř
s	s	k7c7	s
vřetenovým	vřetenový	k2eAgInSc7d1	vřetenový
krokem	krok	k1gInSc7	krok
za	za	k7c4	za
přesný	přesný	k2eAgInSc4d1	přesný
řídící	řídící	k2eAgInSc4d1	řídící
chronometr	chronometr	k1gInSc4	chronometr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
spouštět	spouštět	k5eAaImF	spouštět
starý	starý	k2eAgInSc4d1	starý
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
Romualdu	Romuald	k1gMnSc3	Romuald
Božkovi	Božek	k1gMnSc3	Božek
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
až	až	k9	až
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
synu	syn	k1gMnSc3	syn
hodináře	hodinář	k1gMnSc2	hodinář
a	a	k8xC	a
konstruktéra	konstruktér	k1gMnSc2	konstruktér
Josefa	Josef	k1gMnSc2	Josef
Božka	Božek	k1gMnSc2	Božek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
regulátor	regulátor	k1gInSc1	regulátor
se	s	k7c7	s
sekundovým	sekundový	k2eAgNnSc7d1	sekundové
kyvadlem	kyvadlo	k1gNnSc7	kyvadlo
a	a	k8xC	a
rtuťovou	rtuťový	k2eAgFnSc7d1	rtuťová
kompenzací	kompenzace	k1gFnSc7	kompenzace
teplotních	teplotní	k2eAgFnPc2d1	teplotní
změn	změna	k1gFnPc2	změna
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
přesnost	přesnost	k1gFnSc4	přesnost
chodu	chod	k1gInSc2	chod
<g/>
.	.	kIx.	.
</s>
<s>
Chronometr	chronometr	k1gInSc1	chronometr
má	mít	k5eAaImIp3nS	mít
Denisonův	Denisonův	k2eAgInSc1d1	Denisonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
krok	krok	k1gInSc1	krok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dokladem	doklad	k1gInSc7	doklad
vynikající	vynikající	k2eAgFnSc2d1	vynikající
konstruktérské	konstruktérský	k2eAgFnSc2d1	konstruktérská
i	i	k8xC	i
hodinářské	hodinářský	k2eAgFnSc2d1	hodinářská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
je	být	k5eAaImIp3nS	být
chod	chod	k1gInSc4	chod
pražského	pražský	k2eAgInSc2d1	pražský
orloje	orloj	k1gInSc2	orloj
definitivně	definitivně	k6eAd1	definitivně
řízen	řídit	k5eAaImNgInS	řídit
tímto	tento	k3xDgInSc7	tento
chronometrem	chronometr	k1gInSc7	chronometr
<g/>
.	.	kIx.	.
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1	astronomická
část	část	k1gFnSc1	část
orloje	orloj	k1gInSc2	orloj
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
model	model	k1gInSc1	model
ptolemaiovské	ptolemaiovský	k2eAgFnSc2d1	ptolemaiovský
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
Ptolemaiova	Ptolemaiův	k2eAgFnSc1d1	Ptolemaiova
zeměstředná	zeměstředný	k2eAgFnSc1d1	zeměstředný
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
jako	jako	k8xC	jako
astroláb	astroláb	k1gInSc4	astroláb
poháněný	poháněný	k2eAgInSc4d1	poháněný
hodinovým	hodinový	k2eAgInSc7d1	hodinový
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
stroj	stroj	k1gInSc4	stroj
orloje	orloj	k1gInSc2	orloj
pohání	pohánět	k5eAaImIp3nP	pohánět
tři	tři	k4xCgFnPc1	tři
souosé	souosý	k2eAgFnPc1d1	souosá
hřídele	hřídel	k1gFnPc1	hřídel
přes	přes	k7c4	přes
ozubená	ozubený	k2eAgNnPc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
počtem	počet	k1gInSc7	počet
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
nesou	nést	k5eAaImIp3nP	nést
tři	tři	k4xCgInPc4	tři
ukazatele	ukazatel	k1gInPc4	ukazatel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
sluneční	sluneční	k2eAgInSc4d1	sluneční
a	a	k8xC	a
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
a	a	k8xC	a
pohyb	pohyb	k1gInSc4	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Důmyslný	důmyslný	k2eAgInSc1d1	důmyslný
systém	systém	k1gInSc1	systém
táhel	táhlo	k1gNnPc2	táhlo
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
přitom	přitom	k6eAd1	přitom
posouvá	posouvat	k5eAaImIp3nS	posouvat
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
)	)	kIx)	)
a	a	k8xC	a
měsíční	měsíční	k2eAgFnSc4d1	měsíční
kouli	koule	k1gFnSc4	koule
po	po	k7c6	po
jejich	jejich	k3xOp3gFnPc6	jejich
ručkách	ručka	k1gFnPc6	ručka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
polohu	poloha	k1gFnSc4	poloha
Slunce	slunce	k1gNnSc2	slunce
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
horizontu	horizont	k1gInSc3	horizont
i	i	k9	i
ve	v	k7c6	v
zvěrokruhu	zvěrokruh	k1gInSc6	zvěrokruh
a	a	k8xC	a
polohu	poloh	k1gInSc6	poloh
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
viditelné	viditelný	k2eAgFnSc2d1	viditelná
části	část	k1gFnSc2	část
orloje	orloj	k1gInSc2	orloj
je	být	k5eAaImIp3nS	být
nepohyblivý	pohyblivý	k2eNgInSc1d1	nepohyblivý
velký	velký	k2eAgInSc1d1	velký
kruh	kruh	k1gInSc1	kruh
s	s	k7c7	s
barevným	barevný	k2eAgNnSc7d1	barevné
pozadím	pozadí	k1gNnSc7	pozadí
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2,6	[number]	k4	2,6
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modrém	modrý	k2eAgInSc6d1	modrý
středovém	středový	k2eAgInSc6d1	středový
kruhu	kruh	k1gInSc6	kruh
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgInSc4d1	vyobrazen
glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netradičně	tradičně	k6eNd1	tradičně
severním	severní	k2eAgInSc7d1	severní
pólem	pól	k1gInSc7	pól
dolu	dol	k1gInSc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
určení	určení	k1gNnSc2	určení
místa	místo	k1gNnSc2	místo
pozorování	pozorování	k1gNnSc2	pozorování
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
středová	středový	k2eAgFnSc1d1	středová
hlavní	hlavní	k2eAgFnSc1d1	hlavní
osa	osa	k1gFnSc1	osa
ciferníku	ciferník	k1gInSc2	ciferník
prochází	procházet	k5eAaImIp3nS	procházet
právě	právě	k9	právě
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
jasně	jasně	k6eAd1	jasně
modrá	modrý	k2eAgFnSc1d1	modrá
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
obloha	obloha	k1gFnSc1	obloha
nad	nad	k7c7	nad
horizontem	horizont	k1gInSc7	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
plocha	plocha	k1gFnSc1	plocha
pak	pak	k6eAd1	pak
představují	představovat	k5eAaImIp3nP	představovat
část	část	k1gFnSc4	část
oblohy	obloha	k1gFnSc2	obloha
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
modré	modrý	k2eAgFnSc6d1	modrá
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
pak	pak	k6eAd1	pak
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svítání	svítání	k1gNnSc2	svítání
a	a	k8xC	a
soumraku	soumrak	k1gInSc2	soumrak
prochází	procházet	k5eAaImIp3nS	procházet
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
červenou	červený	k2eAgFnSc7d1	červená
částí	část	k1gFnSc7	část
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
černém	černý	k2eAgNnSc6d1	černé
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
během	během	k7c2	během
astronomické	astronomický	k2eAgFnSc2d1	astronomická
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osmnáct	osmnáct	k4xCc4	osmnáct
stupňů	stupeň	k1gInPc2	stupeň
pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
na	na	k7c6	na
51	[number]	k4	51
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sluneční	sluneční	k2eAgInSc1d1	sluneční
symbol	symbol	k1gInSc1	symbol
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
červnem	červen	k1gInSc7	červen
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
části	část	k1gFnSc2	část
vůbec	vůbec	k9	vůbec
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
červené	červený	k2eAgFnSc6d1	červená
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
latinské	latinský	k2eAgInPc1d1	latinský
nápisy	nápis	k1gInPc1	nápis
AVRORA	AVRORA	kA	AVRORA
(	(	kIx(	(
<g/>
svítání	svítání	k1gNnPc2	svítání
<g/>
)	)	kIx)	)
a	a	k8xC	a
ORTVS	ORTVS	kA	ORTVS
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nápisy	nápis	k1gInPc1	nápis
OCCASVS	OCCASVS	kA	OCCASVS
(	(	kIx(	(
<g/>
západ	západ	k1gInSc1	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
CREPVSCVLVM	CREPVSCVLVM	kA	CREPVSCVLVM
(	(	kIx(	(
<g/>
soumrak	soumrak	k1gInSc1	soumrak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgFnPc4d1	zlatá
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
modrého	modrý	k2eAgInSc2d1	modrý
kruhu	kruh	k1gInSc2	kruh
tvoří	tvořit	k5eAaImIp3nS	tvořit
ciferník	ciferník	k1gInSc1	ciferník
běžného	běžný	k2eAgInSc2d1	běžný
24	[number]	k4	24
<g/>
hodinového	hodinový	k2eAgInSc2d1	hodinový
dne	den	k1gInSc2	den
a	a	k8xC	a
pozlacená	pozlacený	k2eAgFnSc1d1	pozlacená
ručka	ručka	k1gFnSc1	ručka
zde	zde	k6eAd1	zde
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
sluneční	sluneční	k2eAgInSc1d1	sluneční
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Polední	polední	k2eAgFnSc1d1	polední
dvanáctka	dvanáctka	k1gFnSc1	dvanáctka
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c6	v
jasně	jasně	k6eAd1	jasně
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
půlnoční	půlnoční	k2eAgFnSc1d1	půlnoční
dvanáctka	dvanáctka	k1gFnSc1	dvanáctka
dole	dole	k6eAd1	dole
v	v	k7c6	v
černém	černý	k2eAgNnSc6d1	černé
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
seřízen	seřídit	k5eAaPmNgInS	seřídit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
neukazuje	ukazovat	k5eNaImIp3nS	ukazovat
místní	místní	k2eAgInSc1d1	místní
sluneční	sluneční	k2eAgInSc1d1	sluneční
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
občanský	občanský	k2eAgInSc1d1	občanský
středoevropský	středoevropský	k2eAgInSc1d1	středoevropský
čas	čas	k1gInSc1	čas
UT	UT	kA	UT
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
15	[number]	k4	15
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
místnímu	místní	k2eAgInSc3d1	místní
času	čas	k1gInSc3	čas
se	se	k3xPyFc4	se
předchází	předcházet	k5eAaImIp3nS	předcházet
o	o	k7c4	o
2	[number]	k4	2
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
18	[number]	k4	18
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivené	zakřivený	k2eAgFnPc1d1	zakřivená
zlaté	zlatý	k2eAgFnPc1d1	zlatá
čáry	čára	k1gFnPc1	čára
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
modrou	modrý	k2eAgFnSc4d1	modrá
plochu	plocha	k1gFnSc4	plocha
ciferníku	ciferník	k1gInSc2	ciferník
na	na	k7c4	na
12	[number]	k4	12
částí	část	k1gFnPc2	část
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
"	"	kIx"	"
<g/>
přirozené	přirozený	k2eAgFnPc1d1	přirozená
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
babylonské	babylonský	k2eAgFnPc4d1	Babylonská
<g/>
"	"	kIx"	"
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
času	čas	k1gInSc2	čas
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnPc1d1	denní
hodiny	hodina	k1gFnPc1	hodina
pak	pak	k6eAd1	pak
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
noční	noční	k2eAgFnSc6d1	noční
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
tma	tma	k6eAd1	tma
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
této	tento	k3xDgFnSc2	tento
hodiny	hodina	k1gFnSc2	hodina
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
mění	měnit	k5eAaImIp3nS	měnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
během	během	k7c2	během
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
ručce	ručka	k1gFnSc6	ručka
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
posouvá	posouvat	k5eAaImIp3nS	posouvat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
"	"	kIx"	"
<g/>
přirozený	přirozený	k2eAgInSc4d1	přirozený
<g/>
"	"	kIx"	"
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
ručka	ručka	k1gFnSc1	ručka
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
ciferníku	ciferník	k1gInSc2	ciferník
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
"	"	kIx"	"
<g/>
mechanický	mechanický	k2eAgInSc1d1	mechanický
<g/>
"	"	kIx"	"
čas	čas	k1gInSc1	čas
se	s	k7c7	s
stejně	stejně	k6eAd1	stejně
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
hodinami	hodina	k1gFnPc7	hodina
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
užívány	užívat	k5eAaImNgInP	užívat
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
údaj	údaj	k1gInSc1	údaj
času	čas	k1gInSc2	čas
neříká	říkat	k5eNaImIp3nS	říkat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
nebo	nebo	k8xC	nebo
tma	tma	k1gFnSc1	tma
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
v	v	k7c4	v
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
tma	tma	k6eAd1	tma
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
lidem	lid	k1gInSc7	lid
na	na	k7c6	na
mechanickém	mechanický	k2eAgInSc6d1	mechanický
času	čas	k1gInSc6	čas
dlouho	dlouho	k6eAd1	dlouho
vadilo	vadit	k5eAaImAgNnS	vadit
a	a	k8xC	a
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
vzácným	vzácný	k2eAgNnSc7d1	vzácné
svědectvím	svědectví	k1gNnSc7	svědectví
tohoto	tento	k3xDgInSc2	tento
důležitého	důležitý	k2eAgInSc2d1	důležitý
přechodu	přechod	k1gInSc2	přechod
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zlaceného	zlacený	k2eAgInSc2d1	zlacený
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
posouvá	posouvat	k5eAaImIp3nS	posouvat
se	se	k3xPyFc4	se
po	po	k7c6	po
hlavní	hlavní	k2eAgFnSc6d1	hlavní
hodinové	hodinový	k2eAgFnSc6d1	hodinová
ručce	ručka	k1gFnSc6	ručka
táhlem	táhlo	k1gNnSc7	táhlo
<g/>
,	,	kIx,	,
spojeným	spojený	k2eAgMnSc7d1	spojený
se	se	k3xPyFc4	se
zvířetníkem	zvířetník	k1gInSc7	zvířetník
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
ciferník	ciferník	k1gInSc4	ciferník
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
posouvá	posouvat	k5eAaImIp3nS	posouvat
se	se	k3xPyFc4	se
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
zvířetníku	zvířetník	k1gInSc2	zvířetník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
:	:	kIx,	:
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
úsvit	úsvit	k1gInSc4	úsvit
<g/>
,	,	kIx,	,
soumrak	soumrak	k1gInSc4	soumrak
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
výšku	výška	k1gFnSc4	výška
Slunce	slunce	k1gNnSc2	slunce
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
postavení	postavení	k1gNnSc2	postavení
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
zvěrokruhu	zvěrokruh	k1gInSc6	zvěrokruh
přibližné	přibližný	k2eAgNnSc1d1	přibližné
kalendářní	kalendářní	k2eAgNnSc1d1	kalendářní
datum	datum	k1gNnSc1	datum
(	(	kIx(	(
<g/>
dílek	dílek	k1gInSc1	dílek
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
zvířetníku	zvířetník	k1gInSc2	zvířetník
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
5	[number]	k4	5
dnům	den	k1gInPc3	den
<g/>
)	)	kIx)	)
na	na	k7c6	na
zakřivených	zakřivený	k2eAgFnPc6d1	zakřivená
zlatých	zlatý	k2eAgFnPc6d1	zlatá
liniích	linie	k1gFnPc6	linie
na	na	k7c6	na
modré	modrý	k2eAgFnSc6d1	modrá
ploše	plocha	k1gFnSc6	plocha
ciferníku	ciferník	k1gInSc2	ciferník
lze	lze	k6eAd1	lze
odečítat	odečítat	k5eAaImF	odečítat
"	"	kIx"	"
<g/>
přirozený	přirozený	k2eAgInSc4d1	přirozený
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
babylonský	babylonský	k2eAgInSc4d1	babylonský
<g/>
"	"	kIx"	"
čas	čas	k1gInSc4	čas
s	s	k7c7	s
různě	různě	k6eAd1	různě
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
hodinami	hodina	k1gFnPc7	hodina
(	(	kIx(	(
<g/>
černé	černý	k2eAgFnPc1d1	černá
arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
<g/>
)	)	kIx)	)
Měsíc	měsíc	k1gInSc1	měsíc
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
ciferníku	ciferník	k1gInSc6	ciferník
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
cyklu	cyklus	k1gInSc6	cyklus
27	[number]	k4	27
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
43	[number]	k4	43
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
posuvu	posuv	k1gInSc2	posuv
po	po	k7c6	po
ručce	ručka	k1gFnSc6	ručka
je	být	k5eAaImIp3nS	být
skrytým	skrytý	k2eAgInSc7d1	skrytý
mechanismem	mechanismus	k1gInSc7	mechanismus
v	v	k7c6	v
měsíční	měsíční	k2eAgFnSc6d1	měsíční
kouli	koule	k1gFnSc6	koule
znázorňována	znázorňován	k2eAgFnSc1d1	znázorňována
i	i	k8xC	i
fáze	fáze	k1gFnSc1	fáze
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
světlá	světlý	k2eAgFnSc1d1	světlá
a	a	k8xC	a
tmavá	tmavý	k2eAgFnSc1d1	tmavá
polokoule	polokoule	k1gFnSc1	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ručce	ručka	k1gFnSc6	ručka
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
upevněn	upevněn	k2eAgInSc1d1	upevněn
excentrický	excentrický	k2eAgInSc1d1	excentrický
kruh	kruh	k1gInSc1	kruh
se	s	k7c7	s
znameními	znamení	k1gNnPc7	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Ručka	ručka	k1gFnSc1	ručka
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
času	čas	k1gInSc2	čas
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
ciferník	ciferník	k1gInSc1	ciferník
za	za	k7c4	za
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
56	[number]	k4	56
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
4	[number]	k4	4
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
něco	něco	k3yInSc4	něco
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
ručka	ručka	k1gFnSc1	ručka
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Táhlo	táhlo	k1gNnSc1	táhlo
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
přitom	přitom	k6eAd1	přitom
posouvá	posouvat	k5eAaImIp3nS	posouvat
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
ručce	ručka	k1gFnSc6	ručka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
aktuální	aktuální	k2eAgNnSc4d1	aktuální
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
znamení	znamení	k1gNnSc1	znamení
má	mít	k5eAaImIp3nS	mít
úhlově	úhlově	k6eAd1	úhlově
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
excentrickému	excentrický	k2eAgNnSc3d1	excentrické
usazení	usazení	k1gNnSc3	usazení
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc4	tento
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
nestejně	stejně	k6eNd1	stejně
veliké	veliký	k2eAgInPc1d1	veliký
úseky	úsek	k1gInPc1	úsek
v	v	k7c6	v
dělení	dělení	k1gNnSc6	dělení
obvodu	obvod	k1gInSc2	obvod
zvířetníku	zvířetník	k1gInSc2	zvířetník
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
každého	každý	k3xTgNnSc2	každý
znamení	znamení	k1gNnSc2	znamení
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
paprsky	paprsek	k1gInPc4	paprsek
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
zvířetníku	zvířetník	k1gInSc2	zvířetník
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
6	[number]	k4	6
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přibližné	přibližný	k2eAgNnSc4d1	přibližné
odečítání	odečítání	k1gNnSc4	odečítání
kalendářního	kalendářní	k2eAgNnSc2d1	kalendářní
data	datum	k1gNnSc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
vada	vada	k1gFnSc1	vada
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
během	během	k7c2	během
úprav	úprava	k1gFnPc2	úprava
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
nepohyboval	pohybovat	k5eNaImAgInS	pohybovat
nad	nad	k7c7	nad
zvířetníkem	zvířetník	k1gInSc7	zvířetník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vně	vně	k6eAd1	vně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
opravě	oprava	k1gFnSc6	oprava
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
zvířetník	zvířetník	k1gInSc1	zvířetník
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c4	o
zlacený	zlacený	k2eAgInSc4d1	zlacený
okraj	okraj	k1gInSc4	okraj
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
obvodu	obvod	k1gInSc6	obvod
se	se	k3xPyFc4	se
symbol	symbol	k1gInSc1	symbol
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězdička	hvězdička	k1gFnSc1	hvězdička
představuje	představovat	k5eAaImIp3nS	představovat
jarní	jarní	k2eAgInSc4d1	jarní
bod	bod	k1gInSc4	bod
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřiadvacetník	Čtyřiadvacetník	k1gInSc1	Čtyřiadvacetník
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
staročeském	staročeský	k2eAgMnSc6d1	staročeský
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k6eAd1	také
italském	italský	k2eAgInSc6d1	italský
<g/>
)	)	kIx)	)
způsobu	způsob	k1gInSc6	způsob
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
označen	označit	k5eAaPmNgInS	označit
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
gotického	gotický	k2eAgInSc2d1	gotický
tvaru	tvar	k1gInSc2	tvar
na	na	k7c6	na
samostatném	samostatný	k2eAgNnSc6d1	samostatné
mezikruží	mezikruží	k1gNnSc6	mezikruží
<g/>
.	.	kIx.	.
</s>
<s>
Mezikruží	mezikruží	k1gNnSc1	mezikruží
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
okraji	okraj	k1gInSc6	okraj
hlavního	hlavní	k2eAgInSc2d1	hlavní
ciferníku	ciferník	k1gInSc2	ciferník
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
podkladem	podklad	k1gInSc7	podklad
a	a	k8xC	a
zlaceným	zlacený	k2eAgNnSc7d1	zlacené
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odečítání	odečítání	k1gNnSc3	odečítání
času	čas	k1gInSc2	čas
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
slouží	sloužit	k5eAaImIp3nP	sloužit
dva	dva	k4xCgInPc1	dva
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Kolik	kolik	k4yQc4	kolik
je	být	k5eAaImIp3nS	být
hodin	hodina	k1gFnPc2	hodina
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
zlatá	zlatý	k2eAgFnSc1d1	zlatá
ruka	ruka	k1gFnSc1	ruka
na	na	k7c6	na
čtyřiadvaceníku	čtyřiadvaceník	k1gMnSc6	čtyřiadvaceník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
roku	rok	k1gInSc2	rok
také	také	k9	také
natáčí	natáčet	k5eAaImIp3nS	natáčet
<g/>
.	.	kIx.	.
</s>
<s>
Staročeský	staročeský	k2eAgInSc1d1	staročeský
den	den	k1gInSc1	den
totiž	totiž	k9	totiž
začínal	začínat	k5eAaImAgInS	začínat
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
dodnes	dodnes	k6eAd1	dodnes
například	například	k6eAd1	například
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
liturgii	liturgie	k1gFnSc6	liturgie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
konstruktéři	konstruktér	k1gMnPc1	konstruktér
orloje	orloj	k1gInSc2	orloj
vyřešili	vyřešit	k5eAaPmAgMnP	vyřešit
natáčením	natáčení	k1gNnSc7	natáčení
ciferníku	ciferník	k1gInSc2	ciferník
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
60	[number]	k4	60
<g/>
°	°	k?	°
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
údaj	údaj	k1gInSc1	údaj
na	na	k7c6	na
ciferníku	ciferník	k1gInSc6	ciferník
tak	tak	k6eAd1	tak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
roční	roční	k2eAgFnSc6d1	roční
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
krátký	krátký	k2eAgInSc4d1	krátký
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c4	v
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
odbíjeno	odbíjen	k2eAgNnSc1d1	odbíjen
v	v	k7c6	v
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
SEČ	SEČ	kA	SEČ
<g/>
)	)	kIx)	)
večer	večer	k1gInSc1	večer
<g/>
,	,	kIx,	,
při	při	k7c6	při
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
v	v	k7c4	v
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
a	a	k8xC	a
v	v	k7c4	v
nejkratší	krátký	k2eAgInSc4d3	nejkratší
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
o	o	k7c4	o
4	[number]	k4	4
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
odpolední	odpolední	k2eAgFnSc6d1	odpolední
<g/>
.	.	kIx.	.
</s>
<s>
Staročeský	staročeský	k2eAgInSc1d1	staročeský
čas	čas	k1gInSc1	čas
tak	tak	k6eAd1	tak
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc1	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
uplynuly	uplynout	k5eAaPmAgFnP	uplynout
od	od	k7c2	od
posledního	poslední	k2eAgInSc2d1	poslední
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc4d1	hlavní
ukazatel	ukazatel	k1gInSc4	ukazatel
orloje	orloj	k1gInSc2	orloj
a	a	k8xC	a
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
kruh	kruh	k1gInSc1	kruh
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
průměr	průměr	k1gInSc4	průměr
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
kývavou	kývavý	k2eAgFnSc4d1	kývavá
stupnici	stupnice	k1gFnSc4	stupnice
čtyřiadvacetníku	čtyřiadvacetník	k1gInSc2	čtyřiadvacetník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
staročeský	staročeský	k2eAgInSc4d1	staročeský
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
občanský	občanský	k2eAgInSc1d1	občanský
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
také	také	k9	také
"	"	kIx"	"
<g/>
německý	německý	k2eAgMnSc1d1	německý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
den	den	k1gInSc1	den
začíná	začínat	k5eAaImIp3nS	začínat
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
×	×	k?	×
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ciferník	ciferník	k1gInSc1	ciferník
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
I	i	k8xC	i
až	až	k9	až
XII	XII	kA	XII
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
i	i	k9	i
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
dvanáctka	dvanáctka	k1gFnSc1	dvanáctka
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
znamená	znamenat	k5eAaImIp3nS	znamenat
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
dvanáctka	dvanáctka	k1gFnSc1	dvanáctka
v	v	k7c6	v
černém	černý	k2eAgNnSc6d1	černé
poli	pole	k1gNnSc6	pole
půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
části	část	k1gFnPc1	část
ciferníku	ciferník	k1gInSc2	ciferník
mohou	moct	k5eAaImIp3nP	moct
číslice	číslice	k1gFnPc4	číslice
překrývat	překrývat	k5eAaImF	překrývat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ručka	ručka	k1gFnSc1	ručka
protažena	protažen	k2eAgFnSc1d1	protažena
i	i	k9	i
k	k	k7c3	k
opačnému	opačný	k2eAgInSc3d1	opačný
okraji	okraj	k1gInSc3	okraj
ciferníku	ciferník	k1gInSc2	ciferník
se	s	k7c7	s
shodnými	shodný	k2eAgNnPc7d1	shodné
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
německého	německý	k2eAgInSc2d1	německý
času	čas	k1gInSc2	čas
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zavedl	zavést	k5eAaPmAgMnS	zavést
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
<g/>
.	.	kIx.	.
12	[number]	k4	12
pozlacených	pozlacený	k2eAgFnPc2d1	pozlacená
křivek	křivka	k1gFnPc2	křivka
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
obloukem	oblouk	k1gInSc7	oblouk
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
od	od	k7c2	od
obvodu	obvod	k1gInSc2	obvod
ke	k	k7c3	k
středu	střed	k1gInSc2	střed
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgFnP	popsat
černými	černý	k2eAgFnPc7d1	černá
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
označují	označovat	k5eAaImIp3nP	označovat
hodiny	hodina	k1gFnPc1	hodina
babylonské	babylonský	k2eAgFnPc1d1	Babylonská
nebo	nebo	k8xC	nebo
také	také	k9	také
planetní	planetní	k2eAgFnSc1d1	planetní
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
den	den	k1gInSc4	den
od	od	k7c2	od
východu	východ	k1gInSc2	východ
po	po	k7c4	po
západ	západ	k1gInSc4	západ
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
12	[number]	k4	12
stejných	stejný	k2eAgInPc2d1	stejný
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
různě	různě	k6eAd1	různě
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
planetu	planeta	k1gFnSc4	planeta
ke	k	k7c3	k
dni	den	k1gInSc3	den
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
umístěna	umístit	k5eAaPmNgFnS	umístit
i	i	k8xC	i
planetní	planetní	k2eAgFnSc1d1	planetní
tabulka	tabulka	k1gFnSc1	tabulka
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
kohout	kohout	k1gMnSc1	kohout
<g/>
.	.	kIx.	.
</s>
<s>
Křivky	křivka	k1gFnPc1	křivka
protínají	protínat	k5eAaImIp3nP	protínat
tři	tři	k4xCgFnPc1	tři
soustředné	soustředný	k2eAgFnPc1d1	soustředná
kružnice	kružnice	k1gFnPc1	kružnice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
představují	představovat	k5eAaImIp3nP	představovat
obratníky	obratník	k1gInPc4	obratník
Raka	rak	k1gMnSc4	rak
<g/>
,	,	kIx,	,
Kozoroha	Kozoroh	k1gMnSc4	Kozoroh
a	a	k8xC	a
rovník	rovník	k1gInSc4	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
dělení	dělení	k1gNnSc1	dělení
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
počítání	počítání	k1gNnSc1	počítání
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
východu	východ	k1gInSc6	východ
slunce	slunce	k1gNnSc2	slunce
používali	používat	k5eAaImAgMnP	používat
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
<g/>
,	,	kIx,	,
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
národy	národ	k1gInPc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
číselník	číselník	k1gInSc1	číselník
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
XII	XII	kA	XII
<g/>
.	.	kIx.	.
hodinu	hodina	k1gFnSc4	hodina
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
XXIV	XXIV	kA	XXIV
<g/>
.	.	kIx.	.
dole	dole	k6eAd1	dole
a	a	k8xC	a
VI	VI	kA	VI
<g/>
.	.	kIx.	.
respektive	respektive	k9	respektive
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
hodinu	hodina	k1gFnSc4	hodina
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
též	též	k9	též
siderický	siderický	k2eAgMnSc1d1	siderický
<g/>
)	)	kIx)	)
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězdička	hvězdička	k1gFnSc1	hvězdička
na	na	k7c6	na
ručce	ručka	k1gFnSc6	ručka
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
spojen	spojit	k5eAaPmNgInS	spojit
i	i	k9	i
zvířetník	zvířetník	k1gInSc1	zvířetník
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
i	i	k8xC	i
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
polohy	poloha	k1gFnSc2	poloha
"	"	kIx"	"
<g/>
stálic	stálice	k1gFnPc2	stálice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
asi	asi	k9	asi
o	o	k7c4	o
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
kratší	krátký	k2eAgFnSc2d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc4	rozdíl
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Země	zem	k1gFnPc1	zem
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
otáčce	otáčka	k1gFnSc6	otáčka
vůči	vůči	k7c3	vůči
hvězdám	hvězda	k1gFnPc3	hvězda
posune	posunout	k5eAaPmIp3nS	posunout
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
již	již	k6eAd1	již
také	také	k9	také
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
poloze	poloha	k1gFnSc6	poloha
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
jako	jako	k8xS	jako
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
činí	činit	k5eAaImIp3nS	činit
tento	tento	k3xDgInSc4	tento
rozdíl	rozdíl	k1gInSc4	rozdíl
celý	celý	k2eAgInSc4d1	celý
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
čas	čas	k1gInSc1	čas
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
minut	minuta	k1gFnPc2	minuta
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
od	od	k7c2	od
posledního	poslední	k2eAgInSc2d1	poslední
průchodu	průchod	k1gInSc2	průchod
jarního	jarní	k2eAgInSc2d1	jarní
bodu	bod	k1gInSc2	bod
poledníkem	poledník	k1gMnSc7	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
odečítá	odečítat	k5eAaImIp3nS	odečítat
z	z	k7c2	z
římských	římský	k2eAgFnPc2d1	římská
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
k	k	k7c3	k
odečtenému	odečtený	k2eAgInSc3d1	odečtený
času	čas	k1gInSc3	čas
připočte	připočíst	k5eAaPmIp3nS	připočíst
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
až	až	k9	až
1982	[number]	k4	1982
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Přestavení	přestavení	k1gNnSc1	přestavení
orloje	orloj	k1gInSc2	orloj
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
mechanický	mechanický	k2eAgInSc1d1	mechanický
zásah	zásah	k1gInSc1	zásah
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
astroláb	astroláb	k1gInSc1	astroláb
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
polohu	poloha	k1gFnSc4	poloha
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
pro	pro	k7c4	pro
jiné	jiný	k2eAgNnSc4d1	jiné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
pro	pro	k7c4	pro
30	[number]	k4	30
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
jen	jen	k9	jen
čas	čas	k1gInSc4	čas
na	na	k7c6	na
malých	malý	k2eAgFnPc6d1	malá
boční	boční	k2eAgNnPc4d1	boční
cifernících	ciferník	k1gInPc6	ciferník
a	a	k8xC	a
věžních	věžní	k2eAgFnPc6d1	věžní
hodinách	hodina	k1gFnPc6	hodina
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Kalendářní	kalendářní	k2eAgFnSc1d1	kalendářní
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Mánesa	Mánes	k1gMnSc2	Mánes
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kopií	kopie	k1gFnSc7	kopie
od	od	k7c2	od
J.	J.	kA	J.
K.	K.	kA	K.
Lišky	Liška	k1gMnSc2	Liška
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazen	k2eAgFnPc4d1	zobrazena
alegorie	alegorie	k1gFnPc4	alegorie
měsíců	měsíc	k1gInPc2	měsíc
s	s	k7c7	s
venkovskou	venkovský	k2eAgFnSc7d1	venkovská
a	a	k8xC	a
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
obrázky	obrázek	k1gInPc1	obrázek
představují	představovat	k5eAaImIp3nP	představovat
znamení	znamení	k1gNnSc4	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
obrázek	obrázek	k1gInSc1	obrázek
se	s	k7c7	s
zlatými	zlatý	k2eAgFnPc7d1	zlatá
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
věžemi	věž	k1gFnPc7	věž
je	být	k5eAaImIp3nS	být
středověký	středověký	k2eAgInSc4d1	středověký
znak	znak	k1gInSc4	znak
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
kruhu	kruh	k1gInSc2	kruh
je	být	k5eAaImIp3nS	být
církevní	církevní	k2eAgNnSc1d1	církevní
kalendárium	kalendárium	k1gNnSc1	kalendárium
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
365	[number]	k4	365
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
s	s	k7c7	s
názvy	název	k1gInPc7	název
pevných	pevný	k2eAgInPc2d1	pevný
svátků	svátek	k1gInPc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gMnSc1	originál
Mánesovy	Mánesův	k2eAgFnSc2d1	Mánesova
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
kopie	kopie	k1gFnSc1	kopie
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
Suchardovým	Suchardův	k2eAgMnSc7d1	Suchardův
přítelem	přítel	k1gMnSc7	přítel
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
akademickým	akademický	k2eAgMnSc7d1	akademický
malířem	malíř	k1gMnSc7	malíř
Bohumírem	Bohumír	k1gMnSc7	Bohumír
Čílou	Číla	k1gMnSc7	Číla
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Bohumír	Bohumír	k1gMnSc1	Bohumír
Číla	Číla	k1gMnSc1	Číla
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
až	až	k9	až
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
vynikal	vynikat	k5eAaImAgMnS	vynikat
především	především	k6eAd1	především
jako	jako	k9	jako
restaurátor	restaurátor	k1gMnSc1	restaurátor
a	a	k8xC	a
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
byl	být	k5eAaImAgInS	být
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
restaurován	restaurován	k2eAgInSc1d1	restaurován
právě	právě	k9	právě
jím	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
<g/>
,	,	kIx,	,
akademickou	akademický	k2eAgFnSc7d1	akademická
malířkou	malířka	k1gFnSc7	malířka
a	a	k8xC	a
restaurátorkou	restaurátorka	k1gFnSc7	restaurátorka
Bohumírou	Bohumíra	k1gFnSc7	Bohumíra
Míšovou-Čílovou	Míšovou-Čílová	k1gFnSc7	Míšovou-Čílová
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
až	až	k9	až
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
označeny	označit	k5eAaPmNgInP	označit
písmeny	písmeno	k1gNnPc7	písmeno
A	A	kA	A
až	až	k8xS	až
G	G	kA	G
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
cyklicky	cyklicky	k6eAd1	cyklicky
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
písmena	písmeno	k1gNnPc1	písmeno
sloužila	sloužit	k5eAaImAgNnP	sloužit
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
nedělí	neděle	k1gFnPc2	neděle
<g/>
:	:	kIx,	:
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
roce	rok	k1gInSc6	rok
platí	platit	k5eAaImIp3nS	platit
určité	určitý	k2eAgNnSc1d1	určité
"	"	kIx"	"
<g/>
nedělní	nedělní	k2eAgNnSc4d1	nedělní
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
"	"	kIx"	"
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
jím	jíst	k5eAaImIp1nS	jíst
označené	označený	k2eAgInPc1d1	označený
dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
neděle	neděle	k1gFnSc2	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Cisiojan	cisiojan	k1gInSc1	cisiojan
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
známá	známý	k2eAgFnSc1d1	známá
část	část	k1gFnSc1	část
kalendária	kalendárium	k1gNnSc2	kalendárium
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
po	po	k7c6	po
vnějším	vnější	k2eAgInSc6d1	vnější
obvodu	obvod	k1gInSc6	obvod
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stará	starý	k2eAgFnSc1d1	stará
mnemotechnická	mnemotechnický	k2eAgFnSc1d1	mnemotechnická
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
zapamatování	zapamatování	k1gNnSc4	zapamatování
nepohyblivých	pohyblivý	k2eNgInPc2d1	nepohyblivý
svátků	svátek	k1gInPc2	svátek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
svátků	svátek	k1gInPc2	svátek
významných	významný	k2eAgMnPc2d1	významný
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
cisiojánu	cisioján	k1gInSc2	cisioján
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
slabik	slabika	k1gFnPc2	slabika
jmen	jméno	k1gNnPc2	jméno
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
výplňkových	výplňkový	k2eAgFnPc2d1	výplňková
slabik	slabika	k1gFnPc2	slabika
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Složený	složený	k2eAgInSc1d1	složený
veršovaný	veršovaný	k2eAgInSc1d1	veršovaný
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
poněkud	poněkud	k6eAd1	poněkud
kostrbatý	kostrbatý	k2eAgMnSc1d1	kostrbatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
a	a	k8xC	a
zapamatovatelný	zapamatovatelný	k2eAgInSc1d1	zapamatovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
roční	roční	k2eAgInSc1d1	roční
cisioján	cisioján	k1gInSc1	cisioján
má	mít	k5eAaImIp3nS	mít
365	[number]	k4	365
slabik	slabika	k1gFnPc2	slabika
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
dnů	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
přepisu	přepis	k1gInSc2	přepis
prosincové	prosincový	k2eAgFnSc2d1	prosincová
části	část	k1gFnSc2	část
cisiojánu	cisioján	k1gInSc2	cisioján
(	(	kIx(	(
<g/>
významné	významný	k2eAgFnPc1d1	významná
slabiky	slabika	k1gFnPc1	slabika
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
kurzívou	kurzíva	k1gFnSc7	kurzíva
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Po	po	k7c6	po
-	-	kIx~	-
sně	snít	k5eAaImSgMnS	snít
-	-	kIx~	-
hu	hu	k0	hu
-	-	kIx~	-
Bá	Bá	k1gMnPc5	Bá
-	-	kIx~	-
ra	ra	k0	ra
-	-	kIx~	-
s	s	k7c7	s
Mi	já	k3xPp1nSc3	já
-	-	kIx~	-
ku	k	k7c3	k
-	-	kIx~	-
lá	lá	k0	lá
-	-	kIx~	-
šem	šem	k?	šem
-	-	kIx~	-
šla	jít	k5eAaImAgFnS	jít
<g/>
,	,	kIx,	,
v	v	k7c4	v
no	no	k9	no
-	-	kIx~	-
ci	ci	k0	ci
-	-	kIx~	-
Luc	Luc	k1gFnSc6	Luc
-	-	kIx~	-
ka	ka	k?	ka
-	-	kIx~	-
len	len	k1gInSc1	len
-	-	kIx~	-
pře	pře	k1gFnSc1	pře
-	-	kIx~	-
dla	dla	k?	dla
<g/>
,	,	kIx,	,
-	-	kIx~	-
po	po	k7c4	po
-	-	kIx~	-
vě	vě	k?	vě
-	-	kIx~	-
děl	dělo	k1gNnPc2	dělo
-	-	kIx~	-
To	to	k9	to
-	-	kIx~	-
máš	mít	k5eAaImIp2nS	mít
-	-	kIx~	-
tre	tre	k?	tre
-	-	kIx~	-
stán	stán	k?	stán
<g/>
:	:	kIx,	:
Na	na	k7c4	na
-	-	kIx~	-
ro	ro	k?	ro
-	-	kIx~	-
dil	dil	k?	dil
-	-	kIx~	-
se	s	k7c7	s
-	-	kIx~	-
Kri	Kri	k1gMnSc1	Kri
-	-	kIx~	-
stus	stus	k1gInSc1	stus
-	-	kIx~	-
Pán	pán	k1gMnSc1	pán
<g/>
.	.	kIx.	.
</s>
<s>
Cisiojány	cisioján	k1gInPc1	cisioján
vznikaly	vznikat	k5eAaImAgInP	vznikat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
používaly	používat	k5eAaImAgFnP	používat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
tištěných	tištěný	k2eAgInPc2d1	tištěný
kalendářů	kalendář	k1gInPc2	kalendář
však	však	k9	však
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
upadly	upadnout	k5eAaPmAgFnP	upadnout
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc6	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
českého	český	k2eAgInSc2d1	český
cisiojánu	cisioján	k1gInSc2	cisioján
na	na	k7c6	na
kalendářní	kalendářní	k2eAgFnSc6d1	kalendářní
desce	deska	k1gFnSc6	deska
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
se	se	k3xPyFc4	se
přičinil	přičinit	k5eAaPmAgMnS	přičinit
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
v	v	k7c4	v
denní	denní	k2eAgFnSc4d1	denní
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
okénkách	okénko	k1gNnPc6	okénko
nad	nad	k7c7	nad
ciferníkem	ciferník	k1gInSc7	ciferník
objevuje	objevovat	k5eAaImIp3nS	objevovat
12	[number]	k4	12
apoštolů	apoštol	k1gMnPc2	apoštol
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
atributy	atribut	k1gInPc7	atribut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
zobrazování	zobrazování	k1gNnSc2	zobrazování
apoštolů	apoštol	k1gMnPc2	apoštol
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
odměřováním	odměřování	k1gNnSc7	odměřování
času	čas	k1gInSc2	čas
žádnou	žádný	k3yNgFnSc4	žádný
spojitost	spojitost	k1gFnSc4	spojitost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
náboženský	náboženský	k2eAgMnSc1d1	náboženský
a	a	k8xC	a
pro	pro	k7c4	pro
zaujetí	zaujetí	k1gNnSc4	zaujetí
kolemjdoucích	kolemjdoucí	k1gMnPc2	kolemjdoucí
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
evangelijním	evangelijní	k2eAgInPc3d1	evangelijní
výčtům	výčet	k1gInPc3	výčet
dvanácti	dvanáct	k4xCc2	dvanáct
tedy	tedy	k8xC	tedy
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
chybí	chybět	k5eAaImIp3nS	chybět
evangelista	evangelista	k1gMnSc1	evangelista
Matouš	Matouš	k1gMnSc1	Matouš
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Jakubů	Jakub	k1gMnPc2	Jakub
(	(	kIx(	(
<g/>
zdroje	zdroj	k1gInPc1	zdroj
se	se	k3xPyFc4	se
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jidáš	jidáš	k1gInSc1	jidáš
Iškariotský	iškariotský	k2eAgInSc1d1	iškariotský
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
svatý	svatý	k1gMnSc1	svatý
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
,	,	kIx,	,
přibraný	přibraný	k2eAgMnSc1d1	přibraný
podle	podle	k7c2	podle
Skutků	skutek	k1gInPc2	skutek
apoštolských	apoštolský	k2eAgInPc2d1	apoštolský
místo	místo	k1gNnSc4	místo
Jidáše	jidáš	k1gInSc2	jidáš
<g/>
,	,	kIx,	,
a	a	k8xC	a
svatí	svatý	k1gMnPc1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
a	a	k8xC	a
Barnabáš	Barnabáš	k1gMnPc1	Barnabáš
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
apoštoly	apoštol	k1gMnPc7	apoštol
stali	stát	k5eAaPmAgMnP	stát
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnSc2d1	současná
sošky	soška	k1gFnSc2	soška
dvanácti	dvanáct	k4xCc2	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
loutkář	loutkář	k1gMnSc1	loutkář
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
sochy	socha	k1gFnPc1	socha
apoštolů	apoštol	k1gMnPc2	apoštol
shořely	shořet	k5eAaPmAgFnP	shořet
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
figury	figura	k1gFnPc1	figura
apoštolů	apoštol	k1gMnPc2	apoštol
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
objevily	objevit	k5eAaPmAgFnP	objevit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
celý	celý	k2eAgInSc1d1	celý
městský	městský	k2eAgInSc1d1	městský
archiv	archiv	k1gInSc1	archiv
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
taktéž	taktéž	k?	taktéž
shořel	shořet	k5eAaPmAgInS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
na	na	k7c4	na
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
prázdná	prázdný	k2eAgNnPc1d1	prázdné
okénka	okénko	k1gNnPc1	okénko
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
figury	figura	k1gFnPc1	figura
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
duté	dutý	k2eAgFnPc1d1	dutá
a	a	k8xC	a
vykonávaly	vykonávat	k5eAaImAgFnP	vykonávat
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
pohyb	pohyb	k1gInSc4	pohyb
natočení	natočení	k1gNnSc2	natočení
do	do	k7c2	do
okénka	okénko	k1gNnSc2	okénko
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
navíc	navíc	k6eAd1	navíc
vykonávaly	vykonávat	k5eAaImAgInP	vykonávat
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInSc1d1	další
pohyb	pohyb	k1gInSc1	pohyb
<g/>
;	;	kIx,	;
zdvihání	zdvihání	k1gNnSc1	zdvihání
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
vrtění	vrtění	k1gNnSc1	vrtění
a	a	k8xC	a
přikyvování	přikyvování	k1gNnSc1	přikyvování
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
současných	současný	k2eAgMnPc2d1	současný
apoštolů	apoštol	k1gMnPc2	apoštol
Z	z	k7c2	z
Dvanácti	dvanáct	k4xCc2	dvanáct
chyběl	chybět	k5eAaImAgInS	chybět
Juda	judo	k1gNnPc1	judo
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgMnS	být
svatý	svatý	k1gMnSc1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
apoštolů	apoštol	k1gMnPc2	apoštol
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
orloji	orloj	k1gInSc6	orloj
dalších	další	k2eAgFnPc2d1	další
9	[number]	k4	9
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
konají	konat	k5eAaImIp3nP	konat
drobné	drobný	k2eAgInPc4d1	drobný
pohyby	pohyb	k1gInPc4	pohyb
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
:	:	kIx,	:
zakokrháním	zakokrhání	k1gNnSc7	zakokrhání
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
představení	představení	k1gNnSc1	představení
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Soška	soška	k1gFnSc1	soška
je	být	k5eAaImIp3nS	být
pozlacená	pozlacený	k2eAgFnSc1d1	pozlacená
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
komůrce	komůrka	k1gFnSc6	komůrka
nad	nad	k7c7	nad
okny	okno	k1gNnPc7	okno
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Kokrhání	kokrhání	k1gNnSc1	kokrhání
je	být	k5eAaImIp3nS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
stlačeným	stlačený	k2eAgInSc7d1	stlačený
vzduchem	vzduch	k1gInSc7	vzduch
z	z	k7c2	z
měchu	měch	k1gInSc2	měch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
hnán	hnát	k5eAaImNgInS	hnát
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
trubek	trubka	k1gFnPc2	trubka
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
kohouta	kohout	k1gMnSc2	kohout
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
orloj	orloj	k1gInSc4	orloj
osazena	osadit	k5eAaPmNgFnS	osadit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
řada	řada	k1gFnSc1	řada
vlevo	vlevo	k6eAd1	vlevo
<g/>
:	:	kIx,	:
Marnivec	marnivec	k1gMnSc1	marnivec
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
socha	socha	k1gFnSc1	socha
horní	horní	k2eAgFnSc2d1	horní
řady	řada	k1gFnSc2	řada
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
si	se	k3xPyFc3	se
prohlíží	prohlížet	k5eAaImIp3nP	prohlížet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tvář	tvář	k1gFnSc4	tvář
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
postava	postava	k1gFnSc1	postava
vedle	vedle	k7c2	vedle
marnivce	marnivec	k1gMnSc2	marnivec
<g/>
,	,	kIx,	,
potřásá	potřásat	k5eAaImIp3nS	potřásat
měšcem	měšec	k1gInSc7	měšec
a	a	k8xC	a
kývá	kývat	k5eAaImIp3nS	kývat
holí	hole	k1gFnSc7	hole
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
řada	řada	k1gFnSc1	řada
vpravo	vpravo	k6eAd1	vpravo
<g/>
:	:	kIx,	:
Smrtka	Smrtka	k1gFnSc1	Smrtka
(	(	kIx(	(
<g/>
kostlivec	kostlivec	k1gMnSc1	kostlivec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
soška	soška	k1gFnSc1	soška
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
otáčí	otáčet	k5eAaImIp3nS	otáčet
přesýpací	přesýpací	k2eAgFnPc4d1	přesýpací
hodiny	hodina	k1gFnPc4	hodina
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
odměřování	odměřování	k1gNnSc1	odměřování
času	čas	k1gInSc2	čas
života	život	k1gInSc2	život
a	a	k8xC	a
tahá	tahat	k5eAaImIp3nS	tahat
za	za	k7c4	za
provaz	provaz	k1gInSc4	provaz
umíráčku	umíráček	k1gInSc2	umíráček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
nahoře	nahoře	k6eAd1	nahoře
ve	v	k7c6	v
věžičce	věžička	k1gFnSc6	věžička
nad	nad	k7c7	nad
orlojem	orloj	k1gInSc7	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Umíráček	umíráček	k1gInSc1	umíráček
před	před	k7c7	před
opětovným	opětovný	k2eAgNnSc7d1	opětovné
spuštěním	spuštění	k1gNnSc7	spuštění
času	čas	k1gInSc2	čas
zazvoní	zazvonit	k5eAaPmIp3nS	zazvonit
73	[number]	k4	73
<g/>
x.	x.	k?	x.
Turek	Turek	k1gMnSc1	Turek
<g/>
:	:	kIx,	:
soška	soška	k1gFnSc1	soška
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
rozkoše	rozkoš	k1gFnSc2	rozkoš
jako	jako	k8xS	jako
neřesti	neřest	k1gFnSc2	neřest
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnPc1	síť
kolem	kolem	k7c2	kolem
soch	socha	k1gFnPc2	socha
mají	mít	k5eAaImIp3nP	mít
zamezit	zamezit	k5eAaPmF	zamezit
dosedání	dosedání	k1gNnSc4	dosedání
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
znečisťování	znečisťování	k1gNnSc4	znečisťování
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
řada	řada	k1gFnSc1	řada
vlevo	vlevo	k6eAd1	vlevo
<g/>
:	:	kIx,	:
Filosof	filosof	k1gMnSc1	filosof
zkoumající	zkoumající	k2eAgInSc4d1	zkoumající
svět	svět	k1gInSc4	svět
Archanděl	archanděl	k1gMnSc1	archanděl
Michael	Michael	k1gMnSc1	Michael
<g/>
:	:	kIx,	:
socha	socha	k1gFnSc1	socha
archanděla	archanděl	k1gMnSc2	archanděl
nejprve	nejprve	k6eAd1	nejprve
svým	svůj	k3xOyFgNnSc7	svůj
kopím	kopit	k5eAaImIp1nS	kopit
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
na	na	k7c4	na
platný	platný	k2eAgInSc4d1	platný
údaj	údaj	k1gInSc4	údaj
na	na	k7c6	na
kalendářní	kalendářní	k2eAgFnSc6d1	kalendářní
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
Mánesova	Mánesův	k2eAgNnSc2d1	Mánesovo
pootočení	pootočení	k1gNnSc2	pootočení
kalendária	kalendárium	k1gNnSc2	kalendárium
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
ukazatel	ukazatel	k1gInSc4	ukazatel
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
řada	řada	k1gFnSc1	řada
vpravo	vpravo	k6eAd1	vpravo
<g/>
:	:	kIx,	:
Astronom	astronom	k1gMnSc1	astronom
<g/>
/	/	kIx~	/
<g/>
Hvězdář	hvězdář	k1gMnSc1	hvězdář
sledující	sledující	k2eAgFnSc4d1	sledující
oblohu	obloha	k1gFnSc4	obloha
Kronikář	kronikář	k1gMnSc1	kronikář
zaznamenávající	zaznamenávající	k2eAgFnSc2d1	zaznamenávající
události	událost	k1gFnSc2	událost
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
Asi	asi	k9	asi
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
pověst	pověst	k1gFnSc4	pověst
o	o	k7c6	o
mistru	mistr	k1gMnSc6	mistr
Hanušovi	Hanuš	k1gMnSc6	Hanuš
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Julius	Julius	k1gMnSc1	Julius
Košnář	Košnář	k1gMnSc1	Košnář
<g/>
,	,	kIx,	,
Další	další	k2eAgFnPc1d1	další
pověsti	pověst	k1gFnPc1	pověst
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
následujícím	následující	k2eAgInPc3d1	následující
výjevům	výjev	k1gInPc3	výjev
orloje	orloj	k1gInSc2	orloj
<g/>
:	:	kIx,	:
Starý	starý	k2eAgInSc1d1	starý
orloj	orloj	k1gInSc1	orloj
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
radnice	radnice	k1gFnSc2	radnice
Kostlivec	kostlivec	k1gMnSc1	kostlivec
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
<g />
.	.	kIx.	.
</s>
<s>
orloji	orloj	k1gInSc6	orloj
Vrabec	Vrabec	k1gMnSc1	Vrabec
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
kostlivcově	kostlivcův	k2eAgFnSc6d1	kostlivcova
Ďáblovo	ďáblův	k2eAgNnSc1d1	ďáblovo
oko	oko	k1gNnSc1	oko
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
kostlivci	kostlivec	k1gMnSc6	kostlivec
na	na	k7c6	na
staroměstském	staroměstský	k2eAgInSc6d1	staroměstský
orloji	orloj	k1gInSc6	orloj
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
sedmadvaceti	sedmadvacet	k4xCc6	sedmadvacet
popravených	popravený	k2eAgFnPc6d1	popravená
Staroměstského	staroměstský	k2eAgNnSc2d1	Staroměstské
orloje	orloj	k1gInPc1	orloj
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
i	i	k9	i
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
když	když	k8xS	když
se	se	k3xPyFc4	se
stroj	stroj	k1gInSc1	stroj
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
do	do	k7c2	do
orloje	orloj	k1gInSc2	orloj
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
nebo	nebo	k8xC	nebo
zešílí	zešílet	k5eAaPmIp3nS	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
nové	nový	k2eAgFnSc2d1	nová
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
zešílel	zešílet	k5eAaPmAgMnS	zešílet
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
i	i	k8xC	i
staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
historickou	historický	k2eAgFnSc4d1	historická
významnost	významnost	k1gFnSc4	významnost
stále	stále	k6eAd1	stále
prezentovány	prezentovat	k5eAaBmNgFnP	prezentovat
jako	jako	k8xS	jako
hodnotná	hodnotný	k2eAgFnSc1d1	hodnotná
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
600	[number]	k4	600
letům	léto	k1gNnPc3	léto
pražského	pražský	k2eAgInSc2d1	pražský
staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
byla	být	k5eAaImAgFnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
provedena	provést	k5eAaPmNgFnS	provést
společností	společnost	k1gFnPc2	společnost
Tomato	Tomat	k2eAgNnSc1d1	Tomato
production	production	k1gInSc4	production
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
The	The	k1gMnSc2	The
Macula	Macul	k1gMnSc2	Macul
večerní	večerní	k2eAgFnSc1d1	večerní
videomappingová	videomappingový	k2eAgFnSc1d1	videomappingový
show	show	k1gFnSc1	show
osvětlující	osvětlující	k2eAgFnSc1d1	osvětlující
orloj	orloj	k1gInSc1	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Motivy	motiv	k1gInPc1	motiv
z	z	k7c2	z
orloje	orloj	k1gInSc2	orloj
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
československých	československý	k2eAgFnPc6d1	Československá
poštovních	poštovní	k2eAgFnPc6d1	poštovní
známkách	známka	k1gFnPc6	známka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
známkách	známka	k1gFnPc6	známka
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
"	"	kIx"	"
<g/>
Dětem	dítě	k1gFnPc3	dítě
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
motiv	motiv	k1gInSc1	motiv
Ledna	ledno	k1gNnSc2wB	ledno
z	z	k7c2	z
Mánesovy	Mánesův	k2eAgFnSc2d1	Mánesova
kalendářní	kalendářní	k2eAgFnSc2d1	kalendářní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vyšel	vyjít	k5eAaPmAgInS	vyjít
aršík	aršík	k1gInSc1	aršík
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
orloje	orloj	k1gInSc2	orloj
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Astoláb	Astoláb	k1gInSc1	Astoláb
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
i	i	k9	i
na	na	k7c6	na
známce	známka	k1gFnSc6	známka
s	s	k7c7	s
nominální	nominální	k2eAgFnSc7d1	nominální
hodnotou	hodnota	k1gFnSc7	hodnota
0,50	[number]	k4	0,50
Kčs	Kčs	kA	Kčs
ze	z	k7c2	z
série	série	k1gFnSc2	série
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
jednoty	jednota	k1gFnSc2	jednota
československých	československý	k2eAgMnPc2d1	československý
matematiků	matematik	k1gMnPc2	matematik
a	a	k8xC	a
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
600	[number]	k4	600
let	léto	k1gNnPc2	léto
od	od	k7c2	od
první	první	k4xOgFnSc2	první
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
orloji	orloj	k1gInSc6	orloj
vyšla	vyjít	k5eAaPmAgFnS	vyjít
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
česká	český	k2eAgFnSc1d1	Česká
známka	známka	k1gFnSc1	známka
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
21	[number]	k4	21
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
vydala	vydat	k5eAaPmAgFnS	vydat
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
pamětní	pamětní	k2eAgFnSc4d1	pamětní
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
dvousetkorunu	dvousetkoruna	k1gFnSc4	dvousetkoruna
k	k	k7c3	k
600	[number]	k4	600
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
sestrojení	sestrojení	k1gNnPc2	sestrojení
Staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
téma	téma	k1gNnSc4	téma
orloje	orloj	k1gInSc2	orloj
a	a	k8xC	a
mistra	mistr	k1gMnSc2	mistr
Hanuše	Hanuš	k1gMnSc2	Hanuš
je	být	k5eAaImIp3nS	být
také	také	k9	také
český	český	k2eAgInSc1d1	český
animovaný	animovaný	k2eAgInSc1d1	animovaný
celovečerní	celovečerní	k2eAgInSc1d1	celovečerní
film	film	k1gInSc1	film
Kozí	kozí	k2eAgInSc1d1	kozí
příběh	příběh	k1gInSc1	příběh
-	-	kIx~	-
pověsti	pověst	k1gFnSc2	pověst
staré	starý	k2eAgFnSc2d1	stará
Prahy	Praha	k1gFnSc2	Praha
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Tománka	Tománek	k1gMnSc2	Tománek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
s	s	k7c7	s
orlojem	orloj	k1gInSc7	orloj
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
česká	český	k2eAgFnSc1d1	Česká
zajímavost	zajímavost	k1gFnSc1	zajímavost
v	v	k7c6	v
parku	park	k1gInSc6	park
miniatur	miniatura	k1gFnPc2	miniatura
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
Mini-Europe	Mini-Europ	k1gInSc5	Mini-Europ
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihokorejském	jihokorejský	k2eAgInSc6d1	jihokorejský
Soulu	Soul	k1gInSc6	Soul
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Hongde	Hongd	k1gInSc5	Hongd
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
obklopena	obklopit	k5eAaPmNgFnS	obklopit
moderními	moderní	k2eAgInPc7d1	moderní
domy	dům	k1gInPc7	dům
a	a	k8xC	a
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
vedením	vedení	k1gNnSc7	vedení
<g/>
,	,	kIx,	,
replika	replika	k1gFnSc1	replika
části	část	k1gFnSc2	část
pražské	pražský	k2eAgFnSc2d1	Pražská
staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
věrné	věrný	k2eAgFnPc4d1	věrná
a	a	k8xC	a
funkční	funkční	k2eAgFnPc4d1	funkční
repliky	replika	k1gFnPc4	replika
orloje	orloj	k1gInSc2	orloj
(	(	kIx(	(
<g/>
zmenšené	zmenšený	k2eAgInPc4d1	zmenšený
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
restaurace	restaurace	k1gFnSc1	restaurace
řetězce	řetězec	k1gInSc2	řetězec
Castle	Castle	k1gFnSc1	Castle
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Pražským	pražský	k2eAgInSc7d1	pražský
orlojem	orloj	k1gInSc7	orloj
je	být	k5eAaImIp3nS	být
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
i	i	k8xC	i
orloj	orloj	k1gInSc1	orloj
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
z	z	k7c2	z
distribuční	distribuční	k2eAgFnSc2d1	distribuční
trafostanice	trafostanice	k1gFnSc2	trafostanice
v	v	k7c6	v
Kryštofově	Kryštofův	k2eAgNnSc6d1	Kryštofovo
Údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
dominantou	dominanta	k1gFnSc7	dominanta
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
dvě	dva	k4xCgNnPc1	dva
okénka	okénko	k1gNnPc1	okénko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc2	který
přes	přes	k7c4	přes
den	den	k1gInSc4	den
pravidelně	pravidelně	k6eAd1	pravidelně
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
obíhá	obíhat	k5eAaImIp3nS	obíhat
12	[number]	k4	12
vyřezávaných	vyřezávaný	k2eAgMnPc2d1	vyřezávaný
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
doplněné	doplněný	k2eAgInPc4d1	doplněný
dalšími	další	k2eAgFnPc7d1	další
statickými	statický	k2eAgFnPc7d1	statická
i	i	k8xC	i
pohyblivými	pohyblivý	k2eAgFnPc7d1	pohyblivá
sochami	socha	k1gFnPc7	socha
a	a	k8xC	a
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
i	i	k9	i
zařízení	zařízení	k1gNnSc1	zařízení
zobrazující	zobrazující	k2eAgFnSc2d1	zobrazující
fáze	fáze	k1gFnSc2	fáze
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
znamení	znamení	k1gNnSc2	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
