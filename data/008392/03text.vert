<p>
<s>
PETROF	PETROF	kA	PETROF
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
především	především	k6eAd1	především
výrobou	výroba	k1gFnSc7	výroba
klavírů	klavír	k1gInPc2	klavír
a	a	k8xC	a
pianin	pianino	k1gNnPc2	pianino
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Petrof	Petrof	k1gMnSc1	Petrof
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
evropským	evropský	k2eAgMnSc7d1	evropský
výrobcem	výrobce	k1gMnSc7	výrobce
akustických	akustický	k2eAgInPc2d1	akustický
klavírů	klavír	k1gInPc2	klavír
a	a	k8xC	a
pianin	pianino	k1gNnPc2	pianino
<g/>
,	,	kIx,	,
exportující	exportující	k2eAgMnPc1d1	exportující
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
společnosti	společnost	k1gFnSc2	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Antonín	Antonín	k1gMnSc1	Antonín
Petrof	Petrof	k1gMnSc1	Petrof
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	s	k7c7	s
truhlářem	truhlář	k1gMnSc7	truhlář
v	v	k7c6	v
dílně	dílna	k1gFnSc6	dílna
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Jana	Jan	k1gMnSc2	Jan
Petrofa	Petrof	k1gMnSc2	Petrof
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
se	se	k3xPyFc4	se
odjel	odjet	k5eAaPmAgMnS	odjet
učit	učit	k5eAaImF	učit
stavitelem	stavitel	k1gMnSc7	stavitel
klavírů	klavír	k1gInPc2	klavír
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
k	k	k7c3	k
firmám	firma	k1gFnPc3	firma
Heitzman	Heitzman	k1gMnSc1	Heitzman
<g/>
,	,	kIx,	,
Ehrbar	Ehrbar	k1gMnSc1	Ehrbar
a	a	k8xC	a
Schweighofer	Schweighofer	k1gMnSc1	Schweighofer
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncertní	koncertní	k2eAgInSc4d1	koncertní
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
otcovu	otcův	k2eAgFnSc4d1	otcova
truhlářskou	truhlářský	k2eAgFnSc4d1	truhlářská
dílnu	dílna	k1gFnSc4	dílna
na	na	k7c4	na
dílnu	dílna	k1gFnSc4	dílna
klavírnickou	klavírnický	k2eAgFnSc4d1	klavírnický
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgMnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
výrobce	výrobce	k1gMnPc4	výrobce
klavírů	klavír	k1gInPc2	klavír
a	a	k8xC	a
pian	piano	k1gNnPc2	piano
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zákazníky	zákazník	k1gMnPc7	zákazník
firmy	firma	k1gFnSc2	firma
Petrof	Petrof	k1gInSc1	Petrof
patřil	patřit	k5eAaImAgInS	patřit
také	také	k9	také
císařský	císařský	k2eAgInSc1d1	císařský
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
Antonín	Antonín	k1gMnSc1	Antonín
Petrof	Petrof	k1gMnSc1	Petrof
obdržel	obdržet	k5eAaPmAgMnS	obdržet
privilegium	privilegium	k1gNnSc4	privilegium
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
dvorního	dvorní	k2eAgMnSc4d1	dvorní
dodavatele	dodavatel	k1gMnSc4	dodavatel
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
propůjčen	propůjčen	k2eAgInSc4d1	propůjčen
titul	titul	k1gInSc4	titul
tajného	tajný	k2eAgMnSc2d1	tajný
dvorního	dvorní	k2eAgMnSc2d1	dvorní
rady	rada	k1gMnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oba	dva	k4xCgInPc1	dva
jeho	jeho	k3xOp3gMnPc6	jeho
synové	syn	k1gMnPc1	syn
později	pozdě	k6eAd2	pozdě
získali	získat	k5eAaPmAgMnP	získat
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
vyvážet	vyvážet	k5eAaImF	vyvážet
své	svůj	k3xOyFgInPc4	svůj
výrobky	výrobek	k1gInPc4	výrobek
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
obchodní	obchodní	k2eAgFnSc4d1	obchodní
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Petrofova	Petrofův	k2eAgFnSc1d1	Petrofův
manželka	manželka	k1gFnSc1	manželka
Marie	Marie	k1gFnSc1	Marie
stala	stát	k5eAaPmAgFnS	stát
prokuristkou	prokuristka	k1gFnSc7	prokuristka
<g/>
,	,	kIx,	,
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
dostali	dostat	k5eAaPmAgMnP	dostat
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
firmě	firma	k1gFnSc6	firma
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Petrofovi	Petrofův	k2eAgMnPc1d1	Petrofův
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
vedení	vedení	k1gNnSc2	vedení
podniku	podnik	k1gInSc2	podnik
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Vladimír	Vladimír	k1gMnSc1	Vladimír
Petrof	Petrof	k1gMnSc1	Petrof
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
starší	starý	k2eAgMnPc1d2	starší
bratři	bratr	k1gMnPc1	bratr
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
získala	získat	k5eAaPmAgFnS	získat
firma	firma	k1gFnSc1	firma
Petrof	Petrof	k1gInSc1	Petrof
licenci	licence	k1gFnSc4	licence
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
elektroakustických	elektroakustický	k2eAgInPc2d1	elektroakustický
klavírů	klavír	k1gInPc2	klavír
značky	značka	k1gFnSc2	značka
Neo-Bechstein	Neo-Bechsteina	k1gFnPc2	Neo-Bechsteina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenované	přejmenovaný	k2eAgInPc1d1	přejmenovaný
na	na	k7c4	na
Neo-Petrof	Neo-Petrof	k1gInSc4	Neo-Petrof
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Petrof	Petrof	k1gInSc4	Petrof
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
také	také	k9	také
nástroje	nástroj	k1gInPc1	nástroj
jiných	jiný	k2eAgFnPc2d1	jiná
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
v	v	k7c6	v
severočeském	severočeský	k2eAgInSc6d1	severočeský
Jiříkově	Jiříkův	k2eAgInSc6d1	Jiříkův
firma	firma	k1gFnSc1	firma
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
znárodněný	znárodněný	k2eAgInSc1d1	znárodněný
pobočný	pobočný	k2eAgInSc1d1	pobočný
závod	závod	k1gInSc1	závod
firmy	firma	k1gFnSc2	firma
August	August	k1gMnSc1	August
Förster	Förster	k1gMnSc1	Förster
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
klavírů	klavír	k1gInPc2	klavír
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
saském	saský	k2eAgNnSc6d1	Saské
městě	město	k1gNnSc6	město
Löbau	Löbaus	k1gInSc2	Löbaus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pobočce	pobočka	k1gFnSc6	pobočka
kompletují	kompletovat	k5eAaBmIp3nP	kompletovat
piana	piano	k1gNnPc1	piano
značky	značka	k1gFnSc2	značka
Weinbach	Weinbacha	k1gFnPc2	Weinbacha
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
podnik	podnik	k1gInSc1	podnik
Petrof	Petrof	k1gInSc1	Petrof
znárodněn	znárodněn	k2eAgInSc1d1	znárodněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
až	až	k9	až
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
podnik	podnik	k1gInSc1	podnik
reprivatizován	reprivatizovat	k5eAaBmNgInS	reprivatizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Petrof	Petrof	k1gInSc1	Petrof
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vedený	vedený	k2eAgInSc1d1	vedený
dvěma	dva	k4xCgFnPc7	dva
sestrami	sestra	k1gFnPc7	sestra
páté	pátý	k4xOgFnSc2	pátý
generace	generace	k1gFnSc2	generace
rodu	rod	k1gInSc2	rod
Petrofových	Petrofův	k2eAgFnPc2d1	Petrofův
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
roční	roční	k2eAgFnSc7d1	roční
produkcí	produkce	k1gFnSc7	produkce
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
největší	veliký	k2eAgMnSc1d3	veliký
výrobce	výrobce	k1gMnSc1	výrobce
klavírů	klavír	k1gInPc2	klavír
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
produkuje	produkovat	k5eAaImIp3nS	produkovat
ročně	ročně	k6eAd1	ročně
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
000	[number]	k4	000
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
12	[number]	k4	12
000	[number]	k4	000
pianin	pianino	k1gNnPc2	pianino
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
kapacitami	kapacita	k1gFnPc7	kapacita
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
klavírů	klavír	k1gInPc2	klavír
a	a	k8xC	a
pianin	pianino	k1gNnPc2	pianino
i	i	k8xC	i
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
a	a	k8xC	a
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
laboratoří	laboratoř	k1gFnSc7	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
206	[number]	k4	206
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
evropské	evropský	k2eAgFnPc4d1	Evropská
výrobce	výrobce	k1gMnSc2	výrobce
pian	piano	k1gNnPc2	piano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
i	i	k9	i
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
dotací	dotace	k1gFnPc2	dotace
z	z	k7c2	z
Evropského	evropský	k2eAgInSc2d1	evropský
sociálního	sociální	k2eAgInSc2d1	sociální
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
šest	šest	k4xCc1	šest
základních	základní	k2eAgInPc2d1	základní
modelů	model	k1gInPc2	model
křídel	křídlo	k1gNnPc2	křídlo
–	–	k?	–
dle	dle	k7c2	dle
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgInPc1d1	koncertní
mistrovské	mistrovský	k2eAgInPc1d1	mistrovský
nástroje	nástroj	k1gInPc1	nástroj
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
typech	typ	k1gInPc6	typ
–	–	k?	–
P	P	kA	P
284	[number]	k4	284
Mistral	mistral	k1gInSc4	mistral
a	a	k8xC	a
P	P	kA	P
237	[number]	k4	237
Monsoon	Monsoon	k1gInSc4	Monsoon
a	a	k8xC	a
P	P	kA	P
210	[number]	k4	210
Pasat	Pasat	k2eAgInSc1d1	Pasat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
individuálně	individuálně	k6eAd1	individuálně
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
odborníky	odborník	k1gMnPc7	odborník
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
typy	typ	k1gInPc4	typ
P	P	kA	P
194	[number]	k4	194
Storm	Storm	k1gInSc4	Storm
<g/>
,	,	kIx,	,
P	P	kA	P
173	[number]	k4	173
Breeze	Breeze	k1gFnSc2	Breeze
a	a	k8xC	a
P	P	kA	P
159	[number]	k4	159
Bora	Bor	k1gInSc2	Bor
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
standardním	standardní	k2eAgInSc7d1	standardní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stylových	stylový	k2eAgFnPc6d1	stylová
variantách	varianta	k1gFnPc6	varianta
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
typy	typ	k1gInPc1	typ
P	P	kA	P
IV	IV	kA	IV
CHIPP	CHIPP	kA	CHIPP
a	a	k8xC	a
P	P	kA	P
IV	IV	kA	IV
Rokoko	rokoko	k1gNnSc1	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
křídla	křídlo	k1gNnPc1	křídlo
mají	mít	k5eAaImIp3nP	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
standardní	standardní	k2eAgFnPc4d1	standardní
úpravy	úprava	k1gFnPc4	úprava
<g/>
:	:	kIx,	:
nejžádanější	žádaný	k2eAgInSc4d3	nejžádanější
černý	černý	k2eAgInSc4d1	černý
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
ořech	ořech	k1gInSc4	ořech
a	a	k8xC	a
leštěný	leštěný	k2eAgInSc4d1	leštěný
mahagon	mahagon	k1gInSc4	mahagon
<g/>
.	.	kIx.	.
</s>
<s>
Pianina	pianino	k1gNnPc1	pianino
mají	mít	k5eAaImIp3nP	mít
šest	šest	k4xCc1	šest
základních	základní	k2eAgInPc2d1	základní
výškových	výškový	k2eAgInPc2d1	výškový
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
118	[number]	k4	118
<g/>
,	,	kIx,	,
122	[number]	k4	122
<g/>
,	,	kIx,	,
125	[number]	k4	125
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
,	,	kIx,	,
131	[number]	k4	131
a	a	k8xC	a
135	[number]	k4	135
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Obohacením	obohacení	k1gNnSc7	obohacení
sortimentu	sortiment	k1gInSc2	sortiment
jsou	být	k5eAaImIp3nP	být
nástroje	nástroj	k1gInPc1	nástroj
stylového	stylový	k2eAgNnSc2d1	stylové
provedení	provedení	k1gNnSc2	provedení
<g/>
:	:	kIx,	:
CHIPP	CHIPP	kA	CHIPP
<g/>
,	,	kIx,	,
DCHIPP	DCHIPP	kA	DCHIPP
a	a	k8xC	a
Rokoko	rokoko	k1gNnSc4	rokoko
s	s	k7c7	s
ručním	ruční	k2eAgNnSc7d1	ruční
řemeslným	řemeslný	k2eAgNnSc7d1	řemeslné
zpracováním	zpracování	k1gNnSc7	zpracování
povrchů	povrch	k1gInPc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
staví	stavit	k5eAaImIp3nS	stavit
také	také	k9	také
pianina	pianino	k1gNnPc1	pianino
podle	podle	k7c2	podle
přání	přání	k1gNnPc2	přání
a	a	k8xC	a
individuálních	individuální	k2eAgInPc2d1	individuální
požadavků	požadavek	k1gInPc2	požadavek
zákazníka	zákazník	k1gMnSc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
Petrof	Petrof	k1gInSc1	Petrof
také	také	k9	také
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
nábytku	nábytek	k1gInSc2	nábytek
a	a	k8xC	a
kuchyní	kuchyně	k1gFnPc2	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důležitá	důležitý	k2eAgNnPc1d1	důležité
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
1839	[number]	k4	1839
–	–	k?	–
Narození	narození	k1gNnSc2	narození
zakladatele	zakladatel	k1gMnSc2	zakladatel
firmy	firma	k1gFnSc2	firma
Antonína	Antonín	k1gMnSc2	Antonín
Petrofa	Petrof	k1gMnSc2	Petrof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1857	[number]	k4	1857
–	–	k?	–
Antonín	Antonín	k1gMnSc1	Antonín
Petrof	Petrof	k1gMnSc1	Petrof
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgMnS	učit
stavitelem	stavitel	k1gMnSc7	stavitel
klavírů	klavír	k1gInPc2	klavír
u	u	k7c2	u
firem	firma	k1gFnPc2	firma
Heitzman	Heitzman	k1gMnSc1	Heitzman
<g/>
,	,	kIx,	,
Ehrbar	Ehrbar	k1gMnSc1	Ehrbar
a	a	k8xC	a
Schweighofer	Schweighofer	k1gMnSc1	Schweighofer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1864	[number]	k4	1864
–	–	k?	–
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
prvního	první	k4xOgInSc2	první
koncertního	koncertní	k2eAgInSc2d1	koncertní
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1865	[number]	k4	1865
–	–	k?	–
Antonín	Antonín	k1gMnSc1	Antonín
Petrof	Petrof	k1gMnSc1	Petrof
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
otcovu	otcův	k2eAgFnSc4d1	otcova
truhlářskou	truhlářský	k2eAgFnSc4d1	truhlářská
dílnu	dílna	k1gFnSc4	dílna
na	na	k7c4	na
dílnu	dílna	k1gFnSc4	dílna
klavírnickou	klavírnický	k2eAgFnSc4d1	klavírnický
na	na	k7c6	na
starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
prvního	první	k4xOgNnSc2	první
piana	piano	k1gNnSc2	piano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1866	[number]	k4	1866
–	–	k?	–
Přerušení	přerušení	k1gNnSc4	přerušení
výroby	výroba	k1gFnSc2	výroba
kvůli	kvůli	k7c3	kvůli
rakousko-pruské	rakouskoruský	k2eAgFnSc3d1	rakousko-pruská
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1874	[number]	k4	1874
–	–	k?	–
Přemístění	přemístění	k1gNnSc1	přemístění
firmy	firma	k1gFnSc2	firma
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
prostoru	prostor	k1gInSc2	prostor
za	za	k7c7	za
městem	město	k1gNnSc7	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
pianin	pianino	k1gNnPc2	pianino
také	také	k9	také
výroba	výroba	k1gFnSc1	výroba
harmonií	harmonie	k1gFnPc2	harmonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1880	[number]	k4	1880
–	–	k?	–
Otevření	otevření	k1gNnSc1	otevření
pobočného	pobočný	k2eAgInSc2d1	pobočný
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
Temešváru	Temešvár	k1gInSc6	Temešvár
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1881	[number]	k4	1881
–	–	k?	–
Zahájena	zahájen	k2eAgFnSc1d1	zahájena
vlastní	vlastní	k2eAgFnSc1d1	vlastní
výroba	výroba	k1gFnSc1	výroba
klávesnic	klávesnice	k1gFnPc2	klávesnice
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc2	konstrukce
mechanik	mechanika	k1gFnPc2	mechanika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1883	[number]	k4	1883
–	–	k?	–
Zavedena	zaveden	k2eAgFnSc1d1	zavedena
výroba	výroba	k1gFnSc1	výroba
pianin	pianino	k1gNnPc2	pianino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1895	[number]	k4	1895
–	–	k?	–
Začátek	začátek	k1gInSc1	začátek
vývozu	vývoz	k1gInSc2	vývoz
nástrojů	nástroj	k1gInPc2	nástroj
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
,	,	kIx,	,
sklad	sklad	k1gInSc4	sklad
a	a	k8xC	a
servisní	servisní	k2eAgNnSc4d1	servisní
středisko	středisko	k1gNnSc4	středisko
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1899	[number]	k4	1899
–	–	k?	–
Antonín	Antonín	k1gMnSc1	Antonín
Petrof	Petrof	k1gMnSc1	Petrof
byl	být	k5eAaImAgMnS	být
císařem	císař	k1gMnSc7	císař
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
dvorním	dvorní	k2eAgMnSc7d1	dvorní
továrníkem	továrník	k1gMnSc7	továrník
pian	piano	k1gNnPc2	piano
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
a	a	k8xC	a
tajným	tajný	k2eAgMnSc7d1	tajný
radou	rada	k1gMnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
později	pozdě	k6eAd2	pozdě
přešly	přejít	k5eAaPmAgInP	přejít
na	na	k7c4	na
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
–	–	k?	–
Firma	firma	k1gFnSc1	firma
přeměněna	přeměněn	k2eAgFnSc1d1	přeměněna
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
obchodní	obchodní	k2eAgFnSc4d1	obchodní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Petrofová	Petrofový	k2eAgFnSc1d1	Petrofová
je	být	k5eAaImIp3nS	být
prokuristkou	prokuristka	k1gFnSc7	prokuristka
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zakladatele	zakladatel	k1gMnSc2	zakladatel
firmy	firma	k1gFnSc2	firma
zde	zde	k6eAd1	zde
pracují	pracovat	k5eAaImIp3nP	pracovat
i	i	k9	i
synové	syn	k1gMnPc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
i	i	k8xC	i
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
Vladimír	Vladimír	k1gMnSc1	Vladimír
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1915	[number]	k4	1915
–	–	k?	–
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
Antonín	Antonín	k1gMnSc1	Antonín
Petrof	Petrof	k1gMnSc1	Petrof
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válečných	válečný	k2eAgNnPc6d1	válečné
letech	léto	k1gNnPc6	léto
řídí	řídit	k5eAaImIp3nS	řídit
společnost	společnost	k1gFnSc1	společnost
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
Vladimír	Vladimíra	k1gFnPc2	Vladimíra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
–	–	k?	–
Zavedena	zaveden	k2eAgFnSc1d1	zavedena
výroba	výroba	k1gFnSc1	výroba
elektropneumatických	elektropneumatický	k2eAgNnPc2d1	elektropneumatické
pian	piano	k1gNnPc2	piano
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
radioakustických	radioakustický	k2eAgMnPc2d1	radioakustický
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc4	export
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
–	–	k?	–
Firma	firma	k1gFnSc1	firma
Petrof	Petrof	k1gMnSc1	Petrof
společně	společně	k6eAd1	společně
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
americkou	americký	k2eAgFnSc7d1	americká
firmou	firma	k1gFnSc7	firma
Steinway	Steinwaa	k1gFnSc2	Steinwaa
otevřela	otevřít	k5eAaPmAgFnS	otevřít
pobočku	pobočka	k1gFnSc4	pobočka
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c4	na
Wigmore	Wigmor	k1gInSc5	Wigmor
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
podniku	podnik	k1gInSc2	podnik
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
správu	správa	k1gFnSc4	správa
podniku	podnik	k1gInSc2	podnik
o	o	k7c6	o
III	III	kA	III
<g/>
.	.	kIx.	.
generaci	generace	k1gFnSc4	generace
Petrofů	Petrof	k1gInPc2	Petrof
<g/>
:	:	kIx,	:
Dimitrije	Dimitrije	k1gMnPc2	Dimitrije
<g/>
,	,	kIx,	,
Eduarda	Eduard	k1gMnSc2	Eduard
a	a	k8xC	a
Eugena	Eugen	k1gMnSc2	Eugen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
–	–	k?	–
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
získaly	získat	k5eAaPmAgInP	získat
nástroje	nástroj	k1gInPc1	nástroj
Petrof	Petrof	k1gInSc1	Petrof
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
pracovalo	pracovat	k5eAaImAgNnS	pracovat
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
–	–	k?	–
Znárodnění	znárodnění	k1gNnSc4	znárodnění
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
Petrofovi	Petrofův	k2eAgMnPc1d1	Petrofův
zbaveni	zbaven	k2eAgMnPc1d1	zbaven
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
Založeno	založit	k5eAaPmNgNnS	založit
vývojové	vývojový	k2eAgNnSc1d1	vývojové
oddělení	oddělení	k1gNnSc1	oddělení
pianin	pianino	k1gNnPc2	pianino
a	a	k8xC	a
klavírů	klavír	k1gInPc2	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
–	–	k?	–
Ukončena	ukončen	k2eAgFnSc1d1	ukončena
výroba	výroba	k1gFnSc1	výroba
harmonií	harmonie	k1gFnPc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
EXPO	Expo	k1gNnSc4	Expo
Brusel	Brusel	k1gInSc4	Brusel
ocenění	ocenění	k1gNnSc2	ocenění
zlatou	zlatá	k1gFnSc4	zlatá
medailí	medaile	k1gFnPc2	medaile
za	za	k7c4	za
koncertní	koncertní	k2eAgNnSc4d1	koncertní
křídlo	křídlo	k1gNnSc4	křídlo
PETROF	PETROF	kA	PETROF
Mondial	Mondial	k1gInSc1	Mondial
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Začátek	začátek	k1gInSc1	začátek
procesu	proces	k1gInSc2	proces
privatizace	privatizace	k1gFnSc2	privatizace
a	a	k8xC	a
nástup	nástup	k1gInSc1	nástup
Ing.	ing.	kA	ing.
Jana	Jan	k1gMnSc2	Jan
Petrofa	Petrof	k1gMnSc2	Petrof
(	(	kIx(	(
<g/>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
po	po	k7c6	po
43	[number]	k4	43
letech	léto	k1gNnPc6	léto
totalitního	totalitní	k2eAgInSc2d1	totalitní
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Otevření	otevření	k1gNnSc2	otevření
nového	nový	k2eAgNnSc2d1	nové
moderního	moderní	k2eAgNnSc2d1	moderní
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
centra	centrum	k1gNnSc2	centrum
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
velkoobjemovou	velkoobjemový	k2eAgFnSc7d1	velkoobjemová
bezodrazovou	bezodrazový	k2eAgFnSc7d1	bezodrazová
měřící	měřící	k2eAgFnSc7d1	měřící
komorou	komora	k1gFnSc7	komora
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc7d3	veliký
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Založena	založen	k2eAgFnSc1d1	založena
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
pianina	pianino	k1gNnPc4	pianino
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Dokončení	dokončení	k1gNnSc4	dokončení
privatizace	privatizace	k1gFnSc2	privatizace
firmy	firma	k1gFnSc2	firma
společností	společnost	k1gFnPc2	společnost
PETROF	PETROF	kA	PETROF
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
135	[number]	k4	135
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
firmy	firma	k1gFnSc2	firma
Petrof	Petrof	k1gInSc1	Petrof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Továrna	továrna	k1gFnSc1	továrna
přešla	přejít	k5eAaPmAgFnS	přejít
plně	plně	k6eAd1	plně
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
rodině	rodina	k1gFnSc3	rodina
Petrofů	Petrof	k1gMnPc2	Petrof
<g/>
,	,	kIx,	,
nástup	nástup	k1gInSc1	nástup
V.	V.	kA	V.
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
patentovaná	patentovaný	k2eAgFnSc1d1	patentovaná
magneticky	magneticky	k6eAd1	magneticky
akcelerovaná	akcelerovaný	k2eAgFnSc1d1	akcelerovaná
mechanika	mechanika	k1gFnSc1	mechanika
(	(	kIx(	(
<g/>
MAA	MAA	kA	MAA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
první	první	k4xOgInSc1	první
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
mechaniky	mechanika	k1gFnSc2	mechanika
MAA	MAA	kA	MAA
a	a	k8xC	a
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
na	na	k7c6	na
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
2004	[number]	k4	2004
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
140	[number]	k4	140
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
firmy	firma	k1gFnSc2	firma
PETROF	PETROF	kA	PETROF
–	–	k?	–
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
Mgr.	Mgr.	kA	Mgr.
Zuzana	Zuzana	k1gFnSc1	Zuzana
Ceralová	Ceralová	k1gFnSc1	Ceralová
Petrofová	Petrofový	k2eAgFnSc1d1	Petrofová
(	(	kIx(	(
<g/>
V.	V.	kA	V.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obnovena	obnoven	k2eAgFnSc1d1	obnovena
výroba	výroba	k1gFnSc1	výroba
pianinových	pianinový	k2eAgFnPc2d1	pianinový
a	a	k8xC	a
klavírových	klavírový	k2eAgFnPc2d1	klavírová
mechanik	mechanika	k1gFnPc2	mechanika
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Zuzana	Zuzana	k1gFnSc1	Zuzana
Ceralová	Ceralová	k1gFnSc1	Ceralová
Petrofová	Petrofový	k2eAgFnSc1d1	Petrofová
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
prezidentkou	prezidentka	k1gFnSc7	prezidentka
Evropské	evropský	k2eAgFnSc2d1	Evropská
konfederace	konfederace	k1gFnSc2	konfederace
výrobců	výrobce	k1gMnPc2	výrobce
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
CAFIM	CAFIM	kA	CAFIM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
řada	řada	k1gFnSc1	řada
pěti	pět	k4xCc2	pět
délek	délka	k1gFnPc2	délka
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
doplnění	doplnění	k1gNnSc1	doplnění
o	o	k7c4	o
koncertní	koncertní	k2eAgNnSc4d1	koncertní
křídlo	křídlo	k1gNnSc4	křídlo
P	P	kA	P
210	[number]	k4	210
Pasat	Pasat	k2eAgMnSc1d1	Pasat
a	a	k8xC	a
PETROF	PETROF	kA	PETROF
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
zákazníka	zákazník	k1gMnSc4	zákazník
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
opatřit	opatřit	k5eAaPmF	opatřit
nástroj	nástroj	k1gInSc1	nástroj
vlastní	vlastní	k2eAgInSc1d1	vlastní
magneticky	magneticky	k6eAd1	magneticky
vyvažovanou	vyvažovaný	k2eAgFnSc7d1	vyvažovaná
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Nástroje	nástroj	k1gInSc2	nástroj
značky	značka	k1gFnSc2	značka
Petrof	Petrof	k1gInSc1	Petrof
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgFnP	označit
novou	nový	k2eAgFnSc7d1	nová
pečetí	pečeť	k1gFnSc7	pečeť
European	European	k1gMnSc1	European
Excellence	Excellence	k1gFnSc2	Excellence
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
EEX	EEX	kA	EEX
<g/>
,	,	kIx,	,
důkazem	důkaz	k1gInSc7	důkaz
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
zárukou	záruka	k1gFnSc7	záruka
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
životnosti	životnost	k1gFnSc2	životnost
<g/>
,	,	kIx,	,
odborného	odborný	k2eAgInSc2d1	odborný
servisu	servis	k1gInSc2	servis
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc4d1	vysoká
užitné	užitný	k2eAgFnPc4d1	užitná
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
klavírů	klavír	k1gInPc2	klavír
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
pro	pro	k7c4	pro
nejnáročnější	náročný	k2eAgMnPc4d3	nejnáročnější
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgNnSc4d1	koncertní
křídlo	křídlo	k1gNnSc4	křídlo
P	P	kA	P
194	[number]	k4	194
Storm	Storm	k1gInSc4	Storm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Otevření	otevření	k1gNnSc2	otevření
vlastní	vlastní	k2eAgFnSc2d1	vlastní
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
firmy	firma	k1gFnSc2	firma
PETROF	PETROF	kA	PETROF
USA	USA	kA	USA
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
koncertní	koncertní	k2eAgNnPc4d1	koncertní
křídla	křídlo	k1gNnPc4	křídlo
P	P	kA	P
237	[number]	k4	237
Monsoon	Monsoona	k1gFnPc2	Monsoona
<g/>
,	,	kIx,	,
P	P	kA	P
173	[number]	k4	173
Breeze	Breeze	k1gFnSc2	Breeze
a	a	k8xC	a
P	P	kA	P
159	[number]	k4	159
Bora	Bor	k1gInSc2	Bor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Dokončení	dokončení	k1gNnSc4	dokončení
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
klavírů	klavír	k1gInPc2	klavír
Petrof	Petrof	k1gInSc1	Petrof
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
audio-legendou	audioegenda	k1gFnSc7	audio-legenda
Markem	Marek	k1gMnSc7	Marek
Levinsonem	Levinson	k1gMnSc7	Levinson
(	(	kIx(	(
<g/>
reprobedny	reprobedna	k1gFnSc2	reprobedna
pro	pro	k7c4	pro
high-endový	highndový	k2eAgInSc4d1	high-endový
audio	audio	k2eAgInSc4d1	audio
systém	systém	k1gInSc4	systém
DANIEL	Daniela	k1gFnPc2	Daniela
HERTZ	hertz	k1gInSc1	hertz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Otevření	otevření	k1gNnSc1	otevření
Muzea	muzeum	k1gNnSc2	muzeum
PETROF	PETROF	kA	PETROF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Představena	představen	k2eAgFnSc1d1	představena
nová	nový	k2eAgFnSc1d1	nová
prémiová	prémiový	k2eAgFnSc1d1	prémiová
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
ANT	Ant	k1gMnSc1	Ant
<g/>
.	.	kIx.	.
</s>
<s>
PETROF	PETROF	kA	PETROF
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
křídlo	křídlo	k1gNnSc1	křídlo
AP	ap	kA	ap
275	[number]	k4	275
<g/>
,	,	kIx,	,
AP	ap	kA	ap
225	[number]	k4	225
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Zuzana	Zuzana	k1gFnSc1	Zuzana
Ceralová	Ceralová	k1gFnSc1	Ceralová
Petrofová	Petrofový	k2eAgFnSc1d1	Petrofová
oceněna	ocenit	k5eAaPmNgFnS	ocenit
titulem	titul	k1gInSc7	titul
Manažerka	manažerka	k1gFnSc1	manažerka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Projekt	projekt	k1gInSc1	projekt
Petrof	Petrof	k1gMnSc1	Petrof
Art	Art	k1gFnSc1	Art
Family	Famila	k1gFnSc2	Famila
(	(	kIx(	(
<g/>
PAF	paf	k2eAgFnSc1d1	paf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
představení	představení	k1gNnSc1	představení
pianina	pianino	k1gNnSc2	pianino
AP	ap	kA	ap
136	[number]	k4	136
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
stavby	stavba	k1gFnSc2	stavba
Petrof	Petrof	k1gInSc1	Petrof
Gallery	Galler	k1gInPc1	Galler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Otevření	otevření	k1gNnSc1	otevření
PETROF	PETROF	kA	PETROF
Gallery	Galler	k1gInPc4	Galler
a	a	k8xC	a
PETROF	PETROF	kA	PETROF
Café	café	k1gNnSc4	café
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PETR	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
1853-1930	[number]	k4	1853-1930
:	:	kIx,	:
Em	Ema	k1gFnPc2	Ema
<g/>
.	.	kIx.	.
</s>
<s>
Š.	Š.	kA	Š.
Petr	Petr	k1gMnSc1	Petr
&	&	k?	&
Novák	Novák	k1gMnSc1	Novák
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
piana	piano	k1gNnPc4	piano
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Emanuel	Emanuel	k1gMnSc1	Emanuel
Štěpán	Štěpán	k1gMnSc1	Štěpán
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ca	ca	kA	ca
1895	[number]	k4	1895
<g/>
]	]	kIx)	]
--	--	k?	--
15	[number]	k4	15
s.	s.	k?	s.
Zdigitalizováno	Zdigitalizován	k2eAgNnSc4d1	Zdigitalizován
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
služby	služba	k1gFnSc2	služba
Elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
(	(	kIx(	(
<g/>
EOD	EOD	kA	EOD
<g/>
)	)	kIx)	)
Moravskou	moravský	k2eAgFnSc7d1	Moravská
zemskou	zemský	k2eAgFnSc7d1	zemská
knihovnou	knihovna	k1gFnSc7	knihovna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETROF	PETROF	kA	PETROF
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
150	[number]	k4	150
years	years	k1gInSc1	years
of	of	k?	of
great	great	k2eAgInSc1d1	great
pianos	pianos	k1gInSc1	pianos
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
:	:	kIx,	:
Studio	studio	k1gNnSc1	studio
Artefakt	artefakt	k1gInSc1	artefakt
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-260-5951-6	[number]	k4	978-80-260-5951-6
(	(	kIx(	(
<g/>
záznam	záznam	k1gInSc1	záznam
ve	v	k7c6	v
SVKHK	SVKHK	kA	SVKHK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PETROF	PETROF	kA	PETROF
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
CZ	CZ	kA	CZ
<g/>
:	:	kIx,	:
půvab	půvab	k1gInSc1	půvab
<g/>
,	,	kIx,	,
elegance	elegance	k1gFnSc1	elegance
<g/>
,	,	kIx,	,
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
opojení	opojení	k1gNnSc1	opojení
<g/>
,	,	kIx,	,
fantazie	fantazie	k1gFnSc1	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
:	:	kIx,	:
Studio	studio	k1gNnSc1	studio
Artefakt	artefakt	k1gInSc1	artefakt
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-270-5318-6	[number]	k4	978-80-270-5318-6
(	(	kIx(	(
<g/>
záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
SVK	SVK	kA	SVK
HK	HK	kA	HK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petrof	Petrof	k1gInSc1	Petrof
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.petrof.cz	www.petrof.cz	k1gMnSc1	www.petrof.cz
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
