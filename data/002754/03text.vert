<s>
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
(	(	kIx(	(
<g/>
1344	[number]	k4	1344
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skotskou	skotský	k2eAgFnSc7d1	skotská
gaelštinou	gaelština	k1gFnSc7	gaelština
Beinn	Beinna	k1gFnPc2	Beinna
Nibheis	Nibheis	k1gFnPc2	Nibheis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
pohoří	pohoří	k1gNnSc2	pohoří
Grampiany	Grampiana	k1gFnSc2	Grampiana
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Fort	Fort	k?	Fort
William	William	k1gInSc1	William
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
1344	[number]	k4	1344
m	m	kA	m
(	(	kIx(	(
<g/>
4409	[number]	k4	4409
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
Munros	Munrosa	k1gFnPc2	Munrosa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
skotské	skotský	k2eAgFnPc1d1	skotská
hory	hora	k1gFnPc1	hora
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
3000	[number]	k4	3000
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
turisty	turist	k1gMnPc7	turist
a	a	k8xC	a
horolezci	horolezec	k1gMnPc7	horolezec
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Ben	Ben	k1gInSc1	Ben
<g/>
"	"	kIx"	"
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
přiláká	přilákat	k5eAaPmIp3nS	přilákat
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
asi	asi	k9	asi
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
výstupu	výstup	k1gInSc2	výstup
zrekonstruovanou	zrekonstruovaný	k2eAgFnSc4d1	zrekonstruovaná
koňskou	koňský	k2eAgFnSc4d1	koňská
stezku	stezka	k1gFnSc4	stezka
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
Glen	Glena	k1gFnPc2	Glena
Nevis	viset	k5eNaImRp2nS	viset
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Horolezce	horolezec	k1gMnPc4	horolezec
nejvíce	nejvíce	k6eAd1	nejvíce
láká	lákat	k5eAaImIp3nS	lákat
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
stěna	stěna	k1gFnSc1	stěna
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
skalní	skalní	k2eAgFnPc4d1	skalní
stěny	stěna	k1gFnPc4	stěna
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
severovýchodním	severovýchodní	k2eAgMnSc7d1	severovýchodní
sousedem	soused	k1gMnSc7	soused
Carn	Carna	k1gFnPc2	Carna
Mor	mor	k1gInSc1	mor
Dearg	Dearg	k1gMnSc1	Dearg
propojený	propojený	k2eAgInSc4d1	propojený
masiv	masiv	k1gInSc4	masiv
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
hory	hora	k1gFnPc1	hora
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
8	[number]	k4	8
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
britských	britský	k2eAgFnPc2d1	britská
hor	hora	k1gFnPc2	hora
přes	přes	k7c4	přes
4	[number]	k4	4
000	[number]	k4	000
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
1	[number]	k4	1
219	[number]	k4	219
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
svahy	svah	k1gInPc1	svah
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
1	[number]	k4	1
200	[number]	k4	200
m	m	kA	m
na	na	k7c4	na
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
dna	dno	k1gNnSc2	dno
údolí	údolí	k1gNnSc2	údolí
Glen	Glen	k1gMnSc1	Glen
Nevis	viset	k5eNaImRp2nS	viset
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
a	a	k8xC	a
nejprudší	prudký	k2eAgInPc1d3	nejprudší
svahy	svah	k1gInPc1	svah
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
severní	severní	k2eAgInSc1d1	severní
svah	svah	k1gInSc1	svah
spadá	spadat	k5eAaImIp3nS	spadat
o	o	k7c4	o
pouhých	pouhý	k2eAgInPc2d1	pouhý
600	[number]	k4	600
m	m	kA	m
do	do	k7c2	do
Coire	Coir	k1gInSc5	Coir
Leis	Leisa	k1gFnPc2	Leisa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hlavního	hlavní	k2eAgInSc2d1	hlavní
vrcholu	vrchol	k1gInSc2	vrchol
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
1	[number]	k4	1
344	[number]	k4	344
m	m	kA	m
má	mít	k5eAaImIp3nS	mít
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Carn	Carn	k1gMnSc1	Carn
Dearg	Dearg	k1gInSc1	Dearg
(	(	kIx(	(
<g/>
červený	červený	k2eAgInSc1d1	červený
kopec	kopec	k1gInSc1	kopec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgMnPc1d2	vyšší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
(	(	kIx(	(
<g/>
1	[number]	k4	1
221	[number]	k4	221
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
Fort	Fort	k?	Fort
William	William	k1gInSc1	William
často	často	k6eAd1	často
zaměňován	zaměňovat	k5eAaImNgInS	zaměňovat
za	za	k7c4	za
vlastní	vlastní	k2eAgInSc4d1	vlastní
Ben	Ben	k1gInSc4	Ben
Nevis	viset	k5eNaImRp2nS	viset
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Carn	Carn	k1gInSc1	Carn
Dearg	Dearg	k1gInSc1	Dearg
(	(	kIx(	(
<g/>
1	[number]	k4	1
020	[number]	k4	020
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
ční	čnět	k5eAaImIp3nS	čnět
nad	nad	k7c7	nad
údolím	údolí	k1gNnSc7	údolí
Glen	Glena	k1gFnPc2	Glena
Nevis	viset	k5eNaImRp2nS	viset
na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
leží	ležet	k5eAaImIp3nS	ležet
nižší	nízký	k2eAgFnSc1d2	nižší
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Meall	Meall	k1gInSc1	Meall
an	an	k?	an
t-Suidhe	t-Suidhe	k1gInSc1	t-Suidhe
(	(	kIx(	(
<g/>
711	[number]	k4	711
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
leží	ležet	k5eAaImIp3nP	ležet
malé	malý	k2eAgNnSc1d1	malé
jezero	jezero	k1gNnSc1	jezero
Lochan	Lochana	k1gFnPc2	Lochana
an	an	k?	an
t-Suidhe	t-Suidhe	k1gFnPc2	t-Suidhe
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
Glen	Glena	k1gFnPc2	Glena
Nevis	viset	k5eNaImRp2nS	viset
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Meall	Meall	k1gMnSc1	Meall
an	an	k?	an
t-Suidhe	t-Suidhe	k6eAd1	t-Suidhe
a	a	k8xC	a
poté	poté	k6eAd1	poté
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
západní	západní	k2eAgInPc4d1	západní
svahy	svah	k1gInPc4	svah
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tvořen	tvořit	k5eAaImNgInS	tvořit
vyvřelou	vyvřelý	k2eAgFnSc7d1	vyvřelá
žulou	žula	k1gFnSc7	žula
z	z	k7c2	z
devonu	devon	k1gInSc2	devon
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
400	[number]	k4	400
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
s	s	k7c7	s
příměsemi	příměse	k1gFnPc7	příměse
krystalických	krystalický	k2eAgFnPc2d1	krystalická
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
reliéf	reliéf	k1gInSc1	reliéf
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
časté	častý	k2eAgFnPc4d1	častá
špatné	špatná	k1gFnPc4	špatná
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
špatně	špatně	k6eAd1	špatně
vybavené	vybavený	k2eAgMnPc4d1	vybavený
turisty	turist	k1gMnPc4	turist
zaskočit	zaskočit	k5eAaPmF	zaskočit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozorování	pozorování	k1gNnSc2	pozorování
observatoře	observatoř	k1gFnSc2	observatoř
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mlha	mlha	k1gFnSc1	mlha
přítomná	přítomný	k2eAgFnSc1d1	přítomná
asi	asi	k9	asi
80	[number]	k4	80
<g/>
%	%	kIx~	%
času	čas	k1gInSc2	čas
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
55	[number]	k4	55
<g/>
%	%	kIx~	%
času	čas	k1gInSc2	čas
mezi	mezi	k7c7	mezi
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
červnem	červen	k1gInSc7	červen
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
zimní	zimní	k2eAgFnPc1d1	zimní
teploty	teplota	k1gFnPc1	teplota
byly	být	k5eAaImAgFnP	být
kolem	kolem	k7c2	kolem
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
kolem	kolem	k7c2	kolem
-	-	kIx~	-
<g/>
0,5	[number]	k4	0,5
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
každoročně	každoročně	k6eAd1	každoročně
261	[number]	k4	261
bouřek	bouřka	k1gFnPc2	bouřka
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
zde	zde	k6eAd1	zde
asi	asi	k9	asi
4350	[number]	k4	4350
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
2050	[number]	k4	2050
mm	mm	kA	mm
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
městě	město	k1gNnSc6	město
Fort	Fort	k?	Fort
William	William	k1gInSc4	William
a	a	k8xC	a
600	[number]	k4	600
mm	mm	kA	mm
v	v	k7c4	v
Inverness	Inverness	k1gInSc4	Inverness
a	a	k8xC	a
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jarem	jaro	k1gNnSc7	jaro
a	a	k8xC	a
létem	léto	k1gNnSc7	léto
<g/>
,	,	kIx,	,
dvojnásobné	dvojnásobný	k2eAgInPc1d1	dvojnásobný
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
doložený	doložený	k2eAgInSc4d1	doložený
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
botanik	botanik	k1gMnSc1	botanik
James	James	k1gMnSc1	James
Robertson	Robertson	k1gMnSc1	Robertson
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1771	[number]	k4	1771
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
sbíral	sbírat	k5eAaImAgMnS	sbírat
botanické	botanický	k2eAgInPc4d1	botanický
vzorky	vzorek	k1gInPc4	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
raný	raný	k2eAgInSc1d1	raný
výstup	výstup	k1gInSc1	výstup
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
Johnu	John	k1gMnSc3	John
Williamsu	Williams	k1gMnSc3	Williams
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přinesl	přinést	k5eAaPmAgMnS	přinést
první	první	k4xOgFnPc4	první
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
geologickém	geologický	k2eAgNnSc6d1	geologické
složení	složení	k1gNnSc6	složení
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
byla	být	k5eAaImAgFnS	být
hora	hora	k1gFnSc1	hora
oficiálně	oficiálně	k6eAd1	oficiálně
uznána	uznán	k2eAgFnSc1d1	uznána
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
její	její	k3xOp3gMnSc1	její
jiný	jiný	k2eAgMnSc1d1	jiný
skotský	skotský	k2eAgMnSc1d1	skotský
rival	rival	k1gMnSc1	rival
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
Macdhui	Macdhu	k1gFnSc2	Macdhu
<g/>
.	.	kIx.	.
</s>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
byla	být	k5eAaImAgNnP	být
zbudována	zbudovat	k5eAaPmNgNnP	zbudovat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1883	[number]	k4	1883
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
byla	být	k5eAaImAgFnS	být
zbudována	zbudovat	k5eAaPmNgFnS	zbudovat
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
observatoří	observatoř	k1gFnSc7	observatoř
a	a	k8xC	a
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
dopravu	doprava	k1gFnSc4	doprava
zásob	zásoba	k1gFnPc2	zásoba
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
stezky	stezka	k1gFnSc2	stezka
a	a	k8xC	a
observatoře	observatoř	k1gFnSc2	observatoř
učinilo	učinit	k5eAaImAgNnS	učinit
výstupy	výstup	k1gInPc4	výstup
na	na	k7c4	na
Ben	Ben	k1gInSc4	Ben
Nevis	viset	k5eNaImRp2nS	viset
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
železnice	železnice	k1gFnSc2	železnice
do	do	k7c2	do
Fort	Fort	k?	Fort
William	William	k1gInSc1	William
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
ozubnicovou	ozubnicový	k2eAgFnSc4d1	ozubnicová
železnici	železnice	k1gFnSc4	železnice
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
uskutečněn	uskutečnit	k5eAaPmNgMnS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgInP	být
pozemky	pozemek	k1gInPc1	pozemek
<g/>
,	,	kIx,	,
pokrývající	pokrývající	k2eAgInPc1d1	pokrývající
jižní	jižní	k2eAgFnSc4d1	jižní
stranu	strana	k1gFnSc4	strana
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
vrchol	vrchol	k1gInSc1	vrchol
<g/>
,	,	kIx,	,
zakoupeny	zakoupen	k2eAgInPc1d1	zakoupen
charitativní	charitativní	k2eAgFnSc7d1	charitativní
organizací	organizace	k1gFnSc7	organizace
Scottish	Scottish	k1gMnSc1	Scottish
conservation	conservation	k1gInSc4	conservation
charity	charita	k1gFnSc2	charita
<g/>
.	.	kIx.	.
</s>
<s>
Koňská	koňský	k2eAgFnSc1d1	koňská
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
zbudovaná	zbudovaný	k2eAgFnSc1d1	zbudovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k9	jako
Ben	Ben	k1gInSc1	Ben
Path	Patha	k1gFnPc2	Patha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
a	a	k8xC	a
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
způsobem	způsob	k1gInSc7	způsob
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Achintee	Achintee	k1gFnSc6	Achintee
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
údolí	údolí	k1gNnSc2	údolí
Glen	Glena	k1gFnPc2	Glena
Nevis	viset	k5eNaImRp2nS	viset
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Fort	Fort	k?	Fort
William	William	k1gInSc4	William
a	a	k8xC	a
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
20	[number]	k4	20
m.	m.	k?	m.
Mosty	most	k1gInPc1	most
z	z	k7c2	z
informačního	informační	k2eAgNnSc2d1	informační
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
youth	youth	k1gInSc1	youth
hostelu	hostel	k1gInSc2	hostel
nyní	nyní	k6eAd1	nyní
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přístup	přístup	k1gInSc4	přístup
i	i	k9	i
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
strany	strana	k1gFnSc2	strana
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
prudce	prudko	k6eAd1	prudko
stoupá	stoupat	k5eAaImIp3nS	stoupat
k	k	k7c3	k
sedlu	sedlo	k1gNnSc3	sedlo
Lochan	Lochana	k1gFnPc2	Lochana
Meall	Meallum	k1gNnPc2	Meallum
an	an	k?	an
t-Suidhe	t-Suidh	k1gFnSc2	t-Suidh
(	(	kIx(	(
<g/>
570	[number]	k4	570
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
cik-cak	cikak	k6eAd1	cik-cak
vychází	vycházet	k5eAaImIp3nS	vycházet
zbývajících	zbývající	k2eAgInPc2d1	zbývající
700	[number]	k4	700
m	m	kA	m
po	po	k7c6	po
kamenném	kamenný	k2eAgNnSc6d1	kamenné
západním	západní	k2eAgNnSc6d1	západní
úbočí	úbočí	k1gNnSc6	úbočí
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
zbudovanou	zbudovaný	k2eAgFnSc7d1	zbudovaná
kamennou	kamenný	k2eAgFnSc7d1	kamenná
mohylou	mohyla	k1gFnSc7	mohyla
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
triangulační	triangulační	k2eAgInSc1d1	triangulační
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Ben	Ben	k1gInSc4	Ben
Nevis	viset	k5eNaImRp2nS	viset
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
výzvy	výzva	k1gFnSc2	výzva
Three	Thre	k1gMnSc2	Thre
Peaks	Peaksa	k1gFnPc2	Peaksa
Challenge	Challeng	k1gFnSc2	Challeng
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
ve	v	k7c6	v
výstupu	výstup	k1gInSc6	výstup
na	na	k7c4	na
3	[number]	k4	3
vrcholy	vrchol	k1gInPc4	vrchol
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
britských	britský	k2eAgFnPc2d1	britská
zemí	zem	k1gFnPc2	zem
-	-	kIx~	-
skotský	skotský	k2eAgInSc1d1	skotský
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
(	(	kIx(	(
<g/>
1344	[number]	k4	1344
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
Scafell	Scafell	k1gMnSc1	Scafell
Pike	Pik	k1gFnSc2	Pik
(	(	kIx(	(
<g/>
978	[number]	k4	978
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
velšský	velšský	k2eAgInSc1d1	velšský
Snowdon	Snowdon	k1gInSc1	Snowdon
(	(	kIx(	(
<g/>
1085	[number]	k4	1085
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstupové	výstupový	k2eAgFnPc1d1	výstupová
trasy	trasa	k1gFnPc1	trasa
jsou	být	k5eAaImIp3nP	být
předepsané	předepsaný	k2eAgFnPc1d1	předepsaná
a	a	k8xC	a
pro	pro	k7c4	pro
splnění	splnění	k1gNnSc4	splnění
výzvy	výzva	k1gFnSc2	výzva
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
stihnout	stihnout	k5eAaPmF	stihnout
všechny	všechen	k3xTgInPc4	všechen
3	[number]	k4	3
výstupy	výstup	k1gInPc7	výstup
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
započítávají	započítávat	k5eAaImIp3nP	započítávat
i	i	k9	i
přesuny	přesun	k1gInPc4	přesun
autem	auto	k1gNnSc7	auto
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
tvoří	tvořit	k5eAaImIp3nS	tvořit
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
kamenná	kamenný	k2eAgFnSc1d1	kamenná
plošina	plošina	k1gFnSc1	plošina
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
asi	asi	k9	asi
40	[number]	k4	40
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
jsou	být	k5eAaImIp3nP	být
nápadné	nápadný	k2eAgInPc4d1	nápadný
zbytky	zbytek	k1gInPc4	zbytek
budovy	budova	k1gFnSc2	budova
meteorologicke	meteorologick	k1gFnSc2	meteorologick
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
věže	věž	k1gFnSc2	věž
observatoře	observatoř	k1gFnSc2	observatoř
byl	být	k5eAaImAgInS	být
zbudován	zbudován	k2eAgInSc1d1	zbudován
nouzový	nouzový	k2eAgInSc1d1	nouzový
přístřešek	přístřešek	k1gInSc1	přístřešek
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
chycené	chycený	k2eAgFnSc2d1	chycená
špatným	špatný	k2eAgNnSc7d1	špatné
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
observatoře	observatoř	k1gFnSc2	observatoř
je	být	k5eAaImIp3nS	být
památník	památník	k1gInSc1	památník
obětem	oběť	k1gFnPc3	oběť
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
daleký	daleký	k2eAgInSc4d1	daleký
rozhled	rozhled	k1gInSc4	rozhled
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
190	[number]	k4	190
km	km	kA	km
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
poangličtěnou	poangličtěný	k2eAgFnSc7d1	poangličtěná
verzí	verze	k1gFnSc7	verze
jména	jméno	k1gNnSc2	jméno
Beinn	Beinn	k1gMnSc1	Beinn
Nibheis	Nibheis	k1gFnSc2	Nibheis
ze	z	k7c2	z
skotské	skotský	k2eAgFnSc2d1	skotská
gaelštiny	gaelština	k1gFnSc2	gaelština
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
beinn	beinn	k1gInSc1	beinn
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgNnSc7d3	nejčastější
gaelským	gaelský	k2eAgNnSc7d1	gaelské
slovem	slovo	k1gNnSc7	slovo
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
horu	hora	k1gFnSc4	hora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nibheis	nibheis	k1gInSc1	nibheis
je	být	k5eAaImIp3nS	být
chápáno	chápat	k5eAaImNgNnS	chápat
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
obvykle	obvykle	k6eAd1	obvykle
překládáno	překládat	k5eAaImNgNnS	překládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zlomyslná	zlomyslný	k2eAgFnSc1d1	zlomyslná
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
vzteklá	vzteklý	k2eAgFnSc1d1	vzteklá
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
výkladu	výklad	k1gInSc2	výklad
je	být	k5eAaImIp3nS	být
Beinn	Beinn	k1gNnSc1	Beinn
Nibheis	Nibheis	k1gFnSc2	Nibheis
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
beinn-neamh-bhathais	beinneamhhathais	k1gFnSc2	beinn-neamh-bhathais
<g/>
,	,	kIx,	,
z	z	k7c2	z
Neamh	Neamha	k1gFnPc2	Neamha
"	"	kIx"	"
<g/>
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
mraky	mrak	k1gInPc1	mrak
<g/>
"	"	kIx"	"
a	a	k8xC	a
bathais	bathais	k1gFnSc1	bathais
"	"	kIx"	"
<g/>
vrchol	vrchol	k1gInSc1	vrchol
lidské	lidský	k2eAgFnSc2d1	lidská
hlavy	hlava	k1gFnSc2	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
překlad	překlad	k1gInSc1	překlad
"	"	kIx"	"
<g/>
nebeská	nebeský	k2eAgFnSc1d1	nebeská
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
užíván	užíván	k2eAgMnSc1d1	užíván
<g/>
.	.	kIx.	.
</s>
