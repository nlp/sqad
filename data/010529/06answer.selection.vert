<s>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
podobně	podobně	k6eAd1	podobně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jako	jako	k9	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
39	[number]	k4	39
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
35,244	[number]	k4	35,244
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Sol	sol	k1gNnSc2	sol
<g/>
.	.	kIx.	.
</s>
