<s>
Hrnek	hrnek	k1gInSc1	hrnek
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc1	sloužící
pro	pro	k7c4	pro
rozlévání	rozlévání	k1gNnSc4	rozlévání
nápojů	nápoj	k1gInPc2	nápoj
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
nádob	nádoba	k1gFnPc2	nádoba
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
konzumaci	konzumace	k1gFnSc4	konzumace
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
skleničce	sklenička	k1gFnSc3	sklenička
má	mít	k5eAaImIp3nS	mít
úchyt	úchyt	k1gInSc1	úchyt
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
ucho	ucho	k1gNnSc1	ucho
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
snadnější	snadný	k2eAgNnSc4d2	snazší
přenášení	přenášení	k1gNnSc4	přenášení
studených	studený	k2eAgInPc2d1	studený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
horkých	horký	k2eAgInPc2d1	horký
nápojů	nápoj	k1gInPc2	nápoj
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
či	či	k8xC	či
kakao	kakao	k1gNnSc1	kakao
k	k	k7c3	k
ústům	ústa	k1gNnPc3	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
subtilnější	subtilní	k2eAgFnSc1d2	subtilnější
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
šálek	šálek	k1gInSc1	šálek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
podávání	podávání	k1gNnSc6	podávání
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
nápojů	nápoj	k1gInPc2	nápoj
již	již	k6eAd1	již
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
více	hodně	k6eAd2	hodně
zdobený	zdobený	k2eAgInSc1d1	zdobený
<g/>
.	.	kIx.	.
</s>
<s>
Hrnek	hrnek	k1gInSc1	hrnek
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
z	z	k7c2	z
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
<g/>
,	,	kIx,	,
či	či	k8xC	či
plechové	plechový	k2eAgInPc1d1	plechový
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
plecháčky	plecháček	k1gInPc1	plecháček
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
i	i	k9	i
plastové	plastový	k2eAgInPc1d1	plastový
nebo	nebo	k8xC	nebo
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
termohrnku	termohrnka	k1gMnSc4	termohrnka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
izoluje	izolovat	k5eAaBmIp3nS	izolovat
horký	horký	k2eAgInSc1d1	horký
obsah	obsah	k1gInSc1	obsah
od	od	k7c2	od
chladnějšího	chladný	k2eAgNnSc2d2	chladnější
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Hrnky	hrnek	k1gInPc1	hrnek
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nevyrábějí	vyrábět	k5eNaImIp3nP	vyrábět
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pro	pro	k7c4	pro
skleněné	skleněný	k2eAgFnPc4d1	skleněná
nádoby	nádoba	k1gFnPc4	nádoba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jiné	jiný	k2eAgNnSc4d1	jiné
pojmenování	pojmenování	k1gNnSc4	pojmenování
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
půllitr	půllitr	k1gInSc4	půllitr
<g/>
,	,	kIx,	,
či	či	k8xC	či
třetinka	třetinka	k1gFnSc1	třetinka
nebo	nebo	k8xC	nebo
sklenička	sklenička	k1gFnSc1	sklenička
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
konzumaci	konzumace	k1gFnSc4	konzumace
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
limonád	limonáda	k1gFnPc2	limonáda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nevydrží	vydržet	k5eNaPmIp3nP	vydržet
velkou	velký	k2eAgFnSc4d1	velká
teplotu	teplota	k1gFnSc4	teplota
jako	jako	k8xS	jako
hrnek	hrnek	k1gInSc4	hrnek
<g/>
.	.	kIx.	.
</s>
<s>
Hrnek	hrnek	k1gInSc1	hrnek
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
válcovitá	válcovitý	k2eAgFnSc1d1	válcovitá
nádobka	nádobka	k1gFnSc1	nádobka
různé	různý	k2eAgInPc1d1	různý
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
a	a	k8xC	a
stěny	stěna	k1gFnPc1	stěna
uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
stěny	stěna	k1gFnPc1	stěna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ozdobeny	ozdoben	k2eAgInPc4d1	ozdoben
různými	různý	k2eAgInPc7d1	různý
obrázky	obrázek	k1gInPc7	obrázek
(	(	kIx(	(
<g/>
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
zvířátka	zvířátko	k1gNnPc4	zvířátko
<g/>
,	,	kIx,	,
nápisy	nápis	k1gInPc1	nápis
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
různě	různě	k6eAd1	různě
tvarovány	tvarován	k2eAgFnPc1d1	tvarována
<g/>
.	.	kIx.	.
</s>
<s>
Hrnky	hrnek	k1gInPc1	hrnek
bývají	bývat	k5eAaImIp3nP	bývat
opatřeny	opatřen	k2eAgInPc1d1	opatřen
uchy	ucha	k1gFnPc4	ucha
či	či	k8xC	či
oušky	ouško	k1gNnPc7	ouško
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
uchopení	uchopení	k1gNnSc4	uchopení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
ucha	ucho	k1gNnPc1	ucho
i	i	k8xC	i
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
hrnků	hrnek	k1gInPc2	hrnek
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
hrnečky	hrneček	k1gInPc1	hrneček
mají	mít	k5eAaImIp3nP	mít
přidány	přidat	k5eAaPmNgInP	přidat
dekorační	dekorační	k2eAgInPc1d1	dekorační
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ozdoby	ozdoba	k1gFnPc1	ozdoba
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
či	či	k8xC	či
libovolného	libovolný	k2eAgInSc2d1	libovolný
jiného	jiný	k2eAgInSc2d1	jiný
předmětu	předmět	k1gInSc2	předmět
a	a	k8xC	a
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
nabarveny	nabarven	k2eAgFnPc1d1	nabarvena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
estetický	estetický	k2eAgInSc4d1	estetický
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
ne	ne	k9	ne
o	o	k7c4	o
funkční	funkční	k2eAgInSc4d1	funkční
doplněk	doplněk	k1gInSc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Hrnec	hrnec	k1gInSc1	hrnec
Šálek	šálek	k1gInSc1	šálek
Sklenice	sklenice	k1gFnSc1	sklenice
Termoska	termoska	k1gFnSc1	termoska
Hrnčířský	hrnčířský	k2eAgInSc4d1	hrnčířský
kruh	kruh	k1gInSc4	kruh
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hrnek	hrnek	k1gInSc1	hrnek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hrnek	hrnek	k1gInSc1	hrnek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
