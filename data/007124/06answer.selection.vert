<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
subtilnější	subtilní	k2eAgFnSc1d2	subtilnější
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
šálek	šálek	k1gInSc1	šálek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
podávání	podávání	k1gNnSc6	podávání
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
nápojů	nápoj	k1gInPc2	nápoj
již	již	k6eAd1	již
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
více	hodně	k6eAd2	hodně
zdobený	zdobený	k2eAgInSc1d1	zdobený
<g/>
.	.	kIx.	.
</s>
