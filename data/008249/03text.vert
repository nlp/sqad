<p>
<s>
Mikroregion	mikroregion	k1gInSc1	mikroregion
Bouřlivák	bouřlivák	k1gMnSc1	bouřlivák
je	být	k5eAaImIp3nS	být
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
svazek	svazek	k1gInSc4	svazek
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
sídlem	sídlo	k1gNnSc7	sídlo
jsou	být	k5eAaImIp3nP	být
Košťany	Košťan	k1gMnPc4	Košťan
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
regionálního	regionální	k2eAgInSc2d1	regionální
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
sdružené	sdružený	k2eAgFnPc1d1	sdružená
v	v	k7c6	v
mikroregionu	mikroregion	k1gInSc6	mikroregion
==	==	k?	==
</s>
</p>
<p>
<s>
Hrob	hrob	k1gInSc1	hrob
</s>
</p>
<p>
<s>
Moldava	Moldava	k1gFnSc1	Moldava
</s>
</p>
<p>
<s>
Mikulov	Mikulov	k1gInSc1	Mikulov
</s>
</p>
<p>
<s>
Košťany	Košťan	k1gMnPc4	Košťan
</s>
</p>
<p>
<s>
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Mikroregion	mikroregion	k1gInSc1	mikroregion
Bouřlivák	bouřlivák	k1gMnSc1	bouřlivák
na	na	k7c6	na
Regionálním	regionální	k2eAgInSc6d1	regionální
informačním	informační	k2eAgInSc6d1	informační
servisu	servis	k1gInSc6	servis
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
