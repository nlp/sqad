<s>
Omiš	Omiš	k1gMnSc1	Omiš
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Splitsko-dalmatské	splitskoalmatský	k2eAgFnSc6d1	splitsko-dalmatský
župě	župa	k1gFnSc6	župa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
15	[number]	k4	15
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
město	město	k1gNnSc1	město
patří	patřit	k5eAaImIp3nS	patřit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
turistické	turistický	k2eAgInPc4d1	turistický
cíle	cíl	k1gInPc4	cíl
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
dalmatského	dalmatský	k2eAgNnSc2d1	dalmatské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Omiš	Omiš	k1gMnSc1	Omiš
leží	ležet	k5eAaImIp3nS	ležet
necelých	celý	k2eNgInPc2d1	necelý
30	[number]	k4	30
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Splitu	Split	k1gInSc2	Split
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Cetiny	Cetina	k1gFnSc2	Cetina
do	do	k7c2	do
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
hluboký	hluboký	k2eAgInSc1d1	hluboký
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
kaňon	kaňon	k1gInSc4	kaňon
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
hřebeny	hřeben	k1gInPc4	hřeben
pohoří	pohoří	k1gNnSc3	pohoří
Mosor	Mosor	k1gInSc4	Mosor
a	a	k8xC	a
Omišské	Omišský	k2eAgInPc4d1	Omišský
Dináry	dinár	k1gInPc4	dinár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vypínají	vypínat	k5eAaImIp3nP	vypínat
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
můžeme	moct	k5eAaImIp1nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
i	i	k9	i
pevnost	pevnost	k1gFnSc4	pevnost
Starigrad	Starigrada	k1gFnPc2	Starigrada
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
těžký	těžký	k2eAgInSc4d1	těžký
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výhled	výhled	k1gInSc1	výhled
určitě	určitě	k6eAd1	určitě
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Omiš	Omiš	k1gInSc1	Omiš
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
osídlen	osídlen	k2eAgInSc1d1	osídlen
ilyrskými	ilyrský	k2eAgInPc7d1	ilyrský
kmeny	kmen	k1gInPc7	kmen
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
neretvanských	retvanský	k2eNgMnPc2d1	retvanský
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
obávanými	obávaný	k2eAgMnPc7d1	obávaný
protivníky	protivník	k1gMnPc7	protivník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Omiše	Omiš	k1gInSc2	Omiš
vojenské	vojenský	k2eAgInPc1d1	vojenský
střety	střet	k1gInPc1	střet
mezi	mezi	k7c7	mezi
Benátskou	benátský	k2eAgFnSc7d1	Benátská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
potvrzení	potvrzení	k1gNnSc1	potvrzení
benátské	benátský	k2eAgFnSc2d1	Benátská
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
Omišem	Omiš	k1gInSc7	Omiš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Omiš	Omiš	k1gInSc1	Omiš
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
celé	celý	k2eAgNnSc4d1	celé
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vliv	vliv	k1gInSc1	vliv
těsně	těsně	k6eAd1	těsně
sousedící	sousedící	k2eAgFnSc1d1	sousedící
Poljičská	Poljičský	k2eAgFnSc1d1	Poljičský
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zrušil	zrušit	k5eAaPmAgMnS	zrušit
až	až	k9	až
Napoleon	Napoleon	k1gMnSc1	Napoleon
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
dal	dát	k5eAaPmAgInS	dát
povel	povel	k1gInSc1	povel
pro	pro	k7c4	pro
zánik	zánik	k1gInSc4	zánik
Benátské	benátský	k2eAgFnSc2d1	Benátská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
a	a	k8xC	a
Dubrovnické	dubrovnický	k2eAgFnSc2d1	Dubrovnická
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
.	.	kIx.	.
</s>
<s>
Patronem	patron	k1gMnSc7	patron
města	město	k1gNnSc2	město
Omiš	Omiš	k1gMnSc1	Omiš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
(	(	kIx(	(
<g/>
Sveti	Sveti	k1gNnSc7	Sveti
Ivan	Ivan	k1gMnSc1	Ivan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Nepomuku	Nepomuk	k1gInSc6	Nepomuk
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
notář	notář	k1gMnSc1	notář
arcibiskupské	arcibiskupský	k2eAgFnSc2d1	arcibiskupská
soudní	soudní	k2eAgFnSc2d1	soudní
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
,	,	kIx,	,
farář	farář	k1gMnSc1	farář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálním	generální	k2eAgMnSc7d1	generální
vikářem	vikář	k1gMnSc7	vikář
pražské	pražský	k2eAgFnSc2d1	Pražská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
nátlaku	nátlak	k1gInSc3	nátlak
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
moci	moc	k1gFnSc2	moc
mučen	mučen	k2eAgInSc4d1	mučen
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
utopen	utopen	k2eAgMnSc1d1	utopen
ve	v	k7c6	v
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1393	[number]	k4	1393
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svatého	svatý	k2eAgNnSc2d1	svaté
ho	on	k3xPp3gInSc4	on
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1729	[number]	k4	1729
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
památka	památka	k1gFnSc1	památka
je	být	k5eAaImIp3nS	být
připomínána	připomínán	k2eAgFnSc1d1	připomínána
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
světových	světový	k2eAgFnPc6d1	světová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
tradičně	tradičně	k6eAd1	tradičně
pořádá	pořádat	k5eAaImIp3nS	pořádat
Den	den	k1gInSc1	den
města	město	k1gNnSc2	město
Omiš	Omiš	k1gInSc1	Omiš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
a	a	k8xC	a
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
se	se	k3xPyFc4	se
vypravují	vypravovat	k5eAaImIp3nP	vypravovat
i	i	k9	i
různá	různý	k2eAgNnPc4d1	různé
procesí	procesí	k1gNnPc4	procesí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
malém	malý	k2eAgNnSc6d1	malé
přístavním	přístavní	k2eAgNnSc6d1	přístavní
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
a	a	k8xC	a
paláců	palác	k1gInPc2	palác
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
stavebních	stavební	k2eAgNnPc2d1	stavební
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Michala	Michala	k1gFnSc1	Michala
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1629	[number]	k4	1629
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
ulici	ulice	k1gFnSc6	ulice
staré	starý	k2eAgFnSc2d1	stará
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
kostelem	kostel	k1gInSc7	kostel
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
část	část	k1gFnSc1	část
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
Peovica	Peovic	k1gInSc2	Peovic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
311	[number]	k4	311
m	m	kA	m
pak	pak	k6eAd1	pak
trosky	troska	k1gFnPc1	troska
pevnosti	pevnost	k1gFnSc2	pevnost
Stari	Star	k1gFnSc2	Star
grad	grad	k1gInSc1	grad
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
také	také	k9	také
jako	jako	k8xS	jako
Fortica	Fortica	k1gMnSc1	Fortica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
na	na	k7c6	na
Priku	Priko	k1gNnSc6	Priko
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poljički	Poljički	k6eAd1	Poljički
muzej	muzej	k1gInSc1	muzej
(	(	kIx(	(
<g/>
Poljicské	Poljicský	k2eAgNnSc1d1	Poljicský
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
sbírka	sbírka	k1gFnSc1	sbírka
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
Poljické	Poljický	k2eAgFnSc2d1	Poljický
republiky	republika	k1gFnSc2	republika
Zbirka	Zbirk	k1gInSc2	Zbirk
obitelji	obitelt	k5eAaPmIp1nS	obitelt
Radman	Radman	k1gMnSc1	Radman
(	(	kIx(	(
<g/>
Sbírka	sbírka	k1gFnSc1	sbírka
rodiny	rodina	k1gFnSc2	rodina
Radmanů	Radman	k1gInPc2	Radman
<g/>
)	)	kIx)	)
Muzej	Muzej	k1gInSc1	Muzej
grada	grad	k1gMnSc2	grad
Omiša	Omišus	k1gMnSc2	Omišus
(	(	kIx(	(
<g/>
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
Tradiční	tradiční	k2eAgInSc1d1	tradiční
festival	festival	k1gInSc1	festival
dalmatských	dalmatský	k2eAgInPc2d1	dalmatský
pěveckých	pěvecký	k2eAgInPc2d1	pěvecký
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
4	[number]	k4	4
<g/>
.	.	kIx.	.
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Omiško	Omiška	k1gFnSc5	Omiška
<g />
.	.	kIx.	.
</s>
<s>
kulturno	kulturno	k1gNnSc1	kulturno
ljeto	ljeto	k1gNnSc1	ljeto
(	(	kIx(	(
<g/>
Festival	festival	k1gInSc4	festival
Omišské	Omišský	k2eAgNnSc4d1	Omišský
kulturní	kulturní	k2eAgNnSc4d1	kulturní
léto	léto	k1gNnSc4	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
různých	různý	k2eAgFnPc2d1	různá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
období	období	k1gNnSc6	období
červenec	červenec	k1gInSc4	červenec
až	až	k8xS	až
srpen	srpen	k1gInSc4	srpen
Kromě	kromě	k7c2	kromě
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
akcí	akce	k1gFnPc2	akce
probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
řada	řada	k1gFnSc1	řada
zábavních	zábavní	k2eAgFnPc2d1	zábavní
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
večerů	večer	k1gInPc2	večer
Lidová	lidový	k2eAgFnSc1d1	lidová
slavnost	slavnost	k1gFnSc1	slavnost
Sveti	Sveť	k1gFnSc2	Sveť
Ivan	Ivan	k1gMnSc1	Ivan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
16	[number]	k4	16
května	květen	k1gInSc2	květen
Festival	festival	k1gInSc1	festival
Ribarska	Ribarska	k1gFnSc1	Ribarska
noć	noć	k?	noć
(	(	kIx(	(
<g/>
Rybářská	rybářský	k2eAgFnSc1d1	rybářská
noc	noc	k1gFnSc1	noc
<g/>
)	)	kIx)	)
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
Gusarske	Gusarsk	k1gFnSc2	Gusarsk
večeri	večer	k1gFnSc2	večer
(	(	kIx(	(
<g/>
Pirátské	pirátský	k2eAgInPc1d1	pirátský
večery	večer	k1gInPc1	večer
<g/>
)	)	kIx)	)
během	běh	k1gInSc7	běh
červenceš	červencat	k5eAaBmIp2nS	červencat
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
K	k	k7c3	k
městu	město	k1gNnSc3	město
náleží	náležet	k5eAaImIp3nP	náležet
sídla	sídlo	k1gNnPc1	sídlo
Blato	Blato	k1gNnSc1	Blato
na	na	k7c4	na
Cetini	Cetin	k1gMnPc1	Cetin
<g/>
,	,	kIx,	,
Borak	borak	k1gMnSc1	borak
<g/>
,	,	kIx,	,
Čelina	Čelina	k1gMnSc1	Čelina
<g/>
,	,	kIx,	,
Čisla	Čisla	k1gMnSc1	Čisla
<g/>
,	,	kIx,	,
Donji	Donje	k1gFnSc4	Donje
Dolac	Dolac	k1gFnSc4	Dolac
<g/>
,	,	kIx,	,
Dubrava	Dubrava	k1gFnSc1	Dubrava
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Gata	Gatum	k1gNnPc4	Gatum
<g/>
,	,	kIx,	,
Gornji	Gornje	k1gFnSc4	Gornje
Dolac	Dolac	k1gFnSc4	Dolac
<g/>
,	,	kIx,	,
Kostanje	Kostanj	k1gMnSc4	Kostanj
<g/>
,	,	kIx,	,
Kučiće	Kučić	k1gMnSc4	Kučić
<g/>
,	,	kIx,	,
Lokva	Lokv	k1gMnSc4	Lokv
Rogoznica	Rogoznic	k1gInSc2	Rogoznic
<g/>
,	,	kIx,	,
Marušići	Marušić	k1gFnSc6	Marušić
<g/>
,	,	kIx,	,
Mimice	mimika	k1gFnSc6	mimika
<g/>
,	,	kIx,	,
Naklice	Naklice	k1gFnSc1	Naklice
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Sela	selo	k1gNnSc2	selo
<g/>
,	,	kIx,	,
Nemira	Nemira	k1gMnSc1	Nemira
<g/>
,	,	kIx,	,
Ostrvica	Ostrvica	k1gMnSc1	Ostrvica
<g/>
,	,	kIx,	,
Pisak	Pisak	k1gMnSc1	Pisak
<g/>
,	,	kIx,	,
Podašpilje	Podašpilje	k1gFnSc5	Podašpilje
<g/>
,	,	kIx,	,
Podgrađe	Podgrađe	k1gFnSc5	Podgrađe
<g/>
,	,	kIx,	,
Putišići	Putišić	k1gFnSc5	Putišić
<g/>
,	,	kIx,	,
Seoca	Seoca	k1gMnSc1	Seoca
<g/>
,	,	kIx,	,
Slime	Slim	k1gMnSc5	Slim
<g/>
,	,	kIx,	,
Smolonje	Smolonje	k1gFnSc5	Smolonje
<g/>
,	,	kIx,	,
Srijane	Srijan	k1gMnSc5	Srijan
<g/>
,	,	kIx,	,
Stanići	Stanići	k1gNnSc6	Stanići
<g/>
,	,	kIx,	,
Svinišće	Svinišće	k1gNnSc6	Svinišće
<g/>
,	,	kIx,	,
Trnbusi	Trnbuse	k1gFnSc6	Trnbuse
<g/>
,	,	kIx,	,
Tugare	Tugar	k1gMnSc5	Tugar
<g/>
,	,	kIx,	,
Zakučac	Zakučac	k1gFnSc1	Zakučac
a	a	k8xC	a
Zvečanje	Zvečanje	k1gFnSc1	Zvečanje
<g/>
.	.	kIx.	.
</s>
<s>
Diksmuide	Diksmuid	k1gMnSc5	Diksmuid
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
Havířov	Havířov	k1gInSc1	Havířov
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
Rjazaň	Rjazaň	k1gFnSc1	Rjazaň
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Omiš	Omiš	k1gInSc4	Omiš
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Omiše	Omiše	k1gFnSc2	Omiše
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
městě	město	k1gNnSc6	město
Omiš	Omiš	k1gInSc4	Omiš
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
