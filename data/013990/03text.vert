<s>
Klávesa	klávesa	k1gFnSc1
Ctrl	Ctrl	kA
</s>
<s>
Klávesy	kláves	k1gInPc1
Ctrl	Ctrl	kA
<g/>
,	,	kIx,
Windows	Windows	kA
<g/>
,	,	kIx,
Alt	Alt	kA
</s>
<s>
Na	na	k7c6
počítačové	počítačový	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
je	být	k5eAaImIp3nS
klávesa	klávesa	k1gFnSc1
Ctrl	Ctrl	kA
používána	používat	k5eAaImNgFnS
společně	společně	k6eAd1
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
klávesou	klávesa	k1gFnSc7
(	(	kIx(
<g/>
jinými	jiný	k2eAgFnPc7d1
klávesami	klávesa	k1gFnPc7
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
modifikační	modifikační	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
<g/>
;	;	kIx,
výjimku	výjimek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
počítačové	počítačový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
provádění	provádění	k1gNnSc3
speciálních	speciální	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
jako	jako	k9
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
mění	měnit	k5eAaImIp3nS
standardní	standardní	k2eAgInSc4d1
význam	význam	k1gInSc4
té	ten	k3xDgFnSc2
klávesy	klávesa	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
stisknuta	stisknout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
podobně	podobně	k6eAd1
jako	jako	k9
klávesa	klávesa	k1gFnSc1
Shift	Shift	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klávesa	klávesa	k1gFnSc1
Ctrl	Ctrl	kA
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
klávesnice	klávesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
označena	označit	k5eAaPmNgFnS
jako	jako	k9
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
kbd	kbd	k?
<g/>
.	.	kIx.
<g/>
Sablona__Klavesa	Sablona__Klavesa	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
;	;	kIx,
<g/>
background-image	background-imag	k1gInSc2
<g/>
:	:	kIx,
<g/>
linear-gradient	linear-gradient	k1gMnSc1
<g/>
(	(	kIx(
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.4	.4	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
,0	,0	k4
<g/>
,	,	kIx,
<g/>
.1	.1	k4
<g/>
))	))	k?
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
DDD	DDD	kA
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
b	b	k?
<g/>
1	#num#	k4
#	#	kIx~
<g/>
888	#num#	k4
#	#	kIx~
<g/>
CCC	CCC	kA
<g/>
;	;	kIx,
<g/>
border-radius	border-radius	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.4	.4	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
text-shadow	text-shadow	k?
<g/>
:	:	kIx,
<g/>
0	#num#	k4
1	#num#	k4
<g/>
px	px	k?
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.5	.5	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
}	}	kIx)
<g/>
Ctrl	Ctrl	kA
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
Control	Control	k1gInSc4
nebo	nebo	k8xC
Ctl	Ctl	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německému	německý	k2eAgInSc3d1
rozložení	rozložení	k1gNnSc1
kláves	klávesa	k1gFnPc2
odpovídá	odpovídat	k5eAaImIp3nS
Strg	Strg	k1gInSc1
dle	dle	k7c2
normy	norma	k1gFnSc2
DIN	din	k1gInSc1
2137	#num#	k4
<g/>
:	:	kIx,
<g/>
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
textu	text	k1gInSc6
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
reprezentována	reprezentovat	k5eAaImNgFnS
znakem	znak	k1gInSc7
„	„	k?
<g/>
šipka	šipka	k1gFnSc1
nahoru	nahoru	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
U	u	k7c2
<g/>
+	+	kIx~
<g/>
2303	#num#	k4
<g/>
,	,	kIx,
⌃	⌃	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
znakem	znak	k1gInSc7
caret	careta	k1gFnPc2
čili	čili	k8xC
krokev	krokev	k1gFnSc4
(	(	kIx(
<g/>
^	^	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Označování	označování	k1gNnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
několik	několik	k4yIc1
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
označovat	označovat	k5eAaImF
stisk	stisk	k1gInSc4
klávesy	klávesa	k1gFnSc2
Ctrl	Ctrl	kA
společně	společně	k6eAd1
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
klávesou	klávesa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
uvedené	uvedený	k2eAgInPc4d1
příklady	příklad	k1gInPc4
znamenají	znamenat	k5eAaImIp3nP
»	»	k?
<g/>
drž	držet	k5eAaImRp2nS
stisknutou	stisknutý	k2eAgFnSc4d1
klávesu	klávesa	k1gFnSc4
Ctrl	Ctrl	kA
a	a	k8xC
současně	současně	k6eAd1
s	s	k7c7
ní	on	k3xPp3gFnSc7
stiskni	stisknout	k5eAaPmRp2nS
klávesu	klávesa	k1gFnSc4
X	X	kA
<g/>
«	«	k?
<g/>
.	.	kIx.
</s>
<s>
^	^	kIx~
<g/>
XTradiční	XTradiční	k2eAgInSc1d1
zápis	zápis	k1gInSc1
</s>
<s>
C-xEmacs	C-xEmacs	k6eAd1
</s>
<s>
CTRL-Xstarý	CTRL-Xstarý	k2eAgInSc1d1
způsob	způsob	k1gInSc1
zápisu	zápis	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
používal	používat	k5eAaImAgInS
Microsoft	Microsoft	kA
</s>
<s>
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Xnový	Xnový	k2eAgInSc4d1
způsob	způsob	k1gInSc4
používaný	používaný	k2eAgInSc4d1
společností	společnost	k1gFnSc7
Microsoft	Microsoft	kA
</s>
<s>
Funkce	funkce	k1gFnSc1
</s>
<s>
Následující	následující	k2eAgInPc1d1
příklady	příklad	k1gInPc1
klávesových	klávesový	k2eAgFnPc2d1
zkratek	zkratka	k1gFnPc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
lišit	lišit	k5eAaImF
<g/>
,	,	kIx,
uvedené	uvedený	k2eAgFnPc1d1
kombinace	kombinace	k1gFnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
systému	systém	k1gInSc6
macOS	macOS	k?
na	na	k7c6
počítačích	počítač	k1gInPc6
Apple	Apple	kA
se	se	k3xPyFc4
většina	většina	k1gFnSc1
těchto	tento	k3xDgFnPc2
zkratek	zkratka	k1gFnPc2
používá	používat	k5eAaImIp3nS
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
modifikační	modifikační	k2eAgFnSc7d1
klávesou	klávesa	k1gFnSc7
Command	Commanda	k1gFnPc2
(	(	kIx(
<g/>
Cmd	Cmd	k1gMnSc1
<g/>
,	,	kIx,
⌘	⌘	k?
<g/>
)	)	kIx)
a	a	k8xC
klávesa	klávesa	k1gFnSc1
Ctrl	Ctrl	kA
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
jednotlačítkovou	jednotlačítkový	k2eAgFnSc7d1
myší	myš	k1gFnSc7
k	k	k7c3
vyvolání	vyvolání	k1gNnSc3
kontextové	kontextový	k2eAgFnSc2d1
nabídky	nabídka	k1gFnSc2
a	a	k8xC
k	k	k7c3
měně	měna	k1gFnSc3
používaným	používaný	k2eAgFnPc3d1
klávesovým	klávesový	k2eAgFnPc3d1
zkratkám	zkratka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
AVybrat	AVybrat	k1gMnSc1
vše	všechen	k3xTgNnSc4
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
BTučně	BTučně	k1gMnSc1
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
CKopíruj	CKopíruj	k1gMnSc1
do	do	k7c2
schránky	schránka	k1gFnSc2
(	(	kIx(
<g/>
Copy	cop	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
DDialogové	DDialogový	k2eAgNnSc1d1
okno	okno	k1gNnSc1
Písmo	písmo	k1gNnSc1
(	(	kIx(
<g/>
textový	textový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Přidat	přidat	k5eAaPmF
k	k	k7c3
oblíbeným	oblíbený	k2eAgFnPc3d1
položkám	položka	k1gFnPc3
(	(	kIx(
<g/>
webový	webový	k2eAgInSc1d1
prohlížeč	prohlížeč	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
EZarovnání	EZarovnání	k1gNnSc1
na	na	k7c4
střed	střed	k1gInSc4
(	(	kIx(
<g/>
textový	textový	k2eAgInSc4d1
procesor	procesor	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
FNajít	FNajít	k1gMnSc1
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
GDialogové	GDialogový	k2eAgNnSc4d1
okno	okno	k1gNnSc4
Přejít	přejít	k5eAaPmF
na	na	k7c4
(	(	kIx(
<g/>
textový	textový	k2eAgInSc4d1
procesor	procesor	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
HNajít	HNajít	k1gMnSc2
a	a	k8xC
nahradit	nahradit	k5eAaPmF
(	(	kIx(
<g/>
textový	textový	k2eAgInSc4d1
procesor	procesor	k1gInSc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Historie	historie	k1gFnSc1
(	(	kIx(
<g/>
webový	webový	k2eAgInSc1d1
prohlížeč	prohlížeč	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
IKurzíva	IKurzív	k1gMnSc2
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
JZarovnání	JZarovnání	k1gNnSc1
do	do	k7c2
bloku	blok	k1gInSc2
(	(	kIx(
<g/>
textový	textový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
KVložit	KVložit	k1gFnSc7
hypertextový	hypertextový	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
LZarovnání	LZarovnání	k1gNnSc4
vlevo	vlevo	k6eAd1
(	(	kIx(
<g/>
textový	textový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
MOdsazení	MOdsazení	k1gNnSc1
odstavce	odstavec	k1gInSc2
zleva	zleva	k6eAd1
(	(	kIx(
<g/>
textový	textový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
NNové	NNový	k2eAgNnSc1d1
(	(	kIx(
<g/>
okno	okno	k1gNnSc1
<g/>
,	,	kIx,
dokument	dokument	k1gInSc1
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
OOtevřít	OOtevřít	k1gMnSc1
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
PTisk	PTisk	k1gInSc1
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
QZavřít	QZavřít	k1gMnSc3
aplikaci	aplikace	k1gFnSc4
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
RZarovnání	RZarovnání	k1gNnSc4
vpravo	vpravo	k6eAd1
(	(	kIx(
<g/>
textový	textový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Aktualizovat	aktualizovat	k5eAaBmF
(	(	kIx(
<g/>
webový	webový	k2eAgMnSc1d1
prohlížeč	prohlížeč	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
SUložit	SUložit	k1gMnSc1
(	(	kIx(
<g/>
Save	Save	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
TNová	TNový	k2eAgFnSc1d1
záložka	záložka	k1gFnSc1
(	(	kIx(
<g/>
webový	webový	k2eAgInSc1d1
prohlížeč	prohlížeč	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
UPodtržení	UPodtržení	k1gNnSc1
(	(	kIx(
<g/>
Underline	Underlin	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
VVložit	VVložit	k1gFnSc1
obsah	obsah	k1gInSc4
schránky	schránka	k1gFnSc2
(	(	kIx(
<g/>
Paste	pást	k5eAaImRp2nP
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
WZavřít	WZavřít	k1gMnSc1
okno	okno	k1gNnSc4
nebo	nebo	k8xC
záložku	záložka	k1gFnSc4
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
XPřesunout	XPřesunout	k1gFnSc1
výběr	výběr	k1gInSc4
do	do	k7c2
schránky	schránka	k1gFnSc2
(	(	kIx(
<g/>
Cut	Cut	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
YZopakování	YZopakování	k1gNnSc4
poslední	poslední	k2eAgFnSc2d1
akce	akce	k1gFnSc2
(	(	kIx(
<g/>
Redo	Redo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
ZVrátit	zvrátit	k5eAaPmF
zpět	zpět	k6eAd1
poslední	poslední	k2eAgFnSc4d1
akci	akce	k1gFnSc4
(	(	kIx(
<g/>
Undo	Undo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
EscOtevře	EscOtevř	k1gInPc1
Nabídku	nabídka	k1gFnSc4
Start	start	k1gInSc1
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
Alt	Alt	kA
+	+	kIx~
DeleteSpustí	DeleteSpustý	k2eAgMnPc1d1
Správce	správce	k1gMnSc1
úloh	úloha	k1gFnPc2
<g/>
;	;	kIx,
Ve	v	k7c6
Windows	Windows	kA
9	#num#	k4
<g/>
x	x	k?
otevře	otevřít	k5eAaPmIp3nS
dialogUkončit	dialogUkončit	k5eAaImF,k5eAaPmF
program	program	k1gInSc4
<g/>
,	,	kIx,
dvojí	dvojí	k4xRgNnSc4
stisknutí	stisknutí	k1gNnSc4
restartuje	restartovat	k5eAaBmIp3nS
počítač	počítač	k1gInSc1
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
⇧	⇧	k?
Shift	Shift	kA
+	+	kIx~
EscSpustí	EscSpustý	k2eAgMnPc1d1
Správce	správce	k1gMnSc1
úloh	úloha	k1gFnPc2
v	v	k7c6
systémech	systém	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
předchozí	předchozí	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
vykonává	vykonávat	k5eAaImIp3nS
jinou	jiný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
klávesnice	klávesnice	k1gFnSc1
QWERTZ	QWERTZ	kA
</s>
<s>
Esc	Esc	k?
</s>
<s>
F1	F1	k4
</s>
<s>
F2	F2	k4
</s>
<s>
F3	F3	k4
</s>
<s>
F4	F4	k4
</s>
<s>
F5	F5	k4
</s>
<s>
F6	F6	k4
</s>
<s>
F7	F7	k4
</s>
<s>
F8	F8	k4
</s>
<s>
F9	F9	k4
</s>
<s>
F10	F10	k4
</s>
<s>
F11	F11	k4
</s>
<s>
F12	F12	k4
</s>
<s>
PrtScSysRq	PrtScSysRq	k?
</s>
<s>
ScrLk	ScrLk	k6eAd1
</s>
<s>
Pause	pausa	k1gFnSc3
</s>
<s>
Ins	Ins	k?
</s>
<s>
Home	Home	k6eAd1
</s>
<s>
PgUp	PgUp	k1gMnSc1
</s>
<s>
NumLk	NumLk	k6eAd1
</s>
<s>
/	/	kIx~
</s>
<s>
*	*	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Del	Del	k?
</s>
<s>
End	End	k?
</s>
<s>
PgDn	PgDn	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
↑	↑	k?
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Ent	Ent	k?
</s>
<s>
←	←	k?
</s>
<s>
↓	↓	k?
</s>
<s>
→	→	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
