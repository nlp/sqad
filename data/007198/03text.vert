<s>
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
také	také	k9	také
baskytara	baskytara	k1gFnSc1	baskytara
nebo	nebo	k8xC	nebo
trochu	trochu	k6eAd1	trochu
nepřesně	přesně	k6eNd1	přesně
basa	basa	k1gFnSc1	basa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc1d1	strunný
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hudbě	hudba	k1gFnSc6	hudba
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
funkci	funkce	k1gFnSc4	funkce
kontrabasu	kontrabas	k1gInSc2	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
Úlohou	úloha	k1gFnSc7	úloha
basové	basový	k2eAgFnSc2d1	basová
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
především	především	k6eAd1	především
hrát	hrát	k5eAaImF	hrát
basovou	basový	k2eAgFnSc4d1	basová
linku	linka	k1gFnSc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bicími	bicí	k2eAgMnPc7d1	bicí
tak	tak	k9	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
rytmu	rytmus	k1gInSc2	rytmus
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Baskytara	baskytara	k1gFnSc1	baskytara
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
žánrech	žánr	k1gInPc6	žánr
prosadila	prosadit	k5eAaPmAgFnS	prosadit
i	i	k9	i
jako	jako	k9	jako
sólový	sólový	k2eAgInSc1d1	sólový
nástroj	nástroj	k1gInSc1	nástroj
(	(	kIx(	(
<g/>
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
latin	latina	k1gFnPc2	latina
<g/>
,	,	kIx,	,
funky	funk	k1gInPc7	funk
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
připomíná	připomínat	k5eAaImIp3nS	připomínat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
mohutnější	mohutný	k2eAgNnSc4d2	mohutnější
a	a	k8xC	a
masivnější	masivní	k2eAgNnSc4d2	masivnější
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgInSc4d2	delší
krk	krk	k1gInSc4	krk
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
mensuru	mensura	k1gFnSc4	mensura
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
silnějším	silný	k2eAgFnPc3d2	silnější
strunám	struna	k1gFnPc3	struna
je	být	k5eAaImIp3nS	být
i	i	k9	i
ladicí	ladicí	k2eAgInSc1d1	ladicí
mechanismus	mechanismus	k1gInSc1	mechanismus
robustnější	robustní	k2eAgInSc1d2	robustnější
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
struny	struna	k1gFnPc4	struna
(	(	kIx(	(
<g/>
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
baskytary	baskytara	k1gFnPc1	baskytara
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pět	pět	k4xCc4	pět
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jako	jako	k9	jako
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
šest	šest	k4xCc4	šest
strun	struna	k1gFnPc2	struna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
laděné	laděný	k2eAgFnPc1d1	laděná
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
oktávu	oktáva	k1gFnSc4	oktáva
níž	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
notovém	notový	k2eAgInSc6d1	notový
zápisu	zápis	k1gInSc6	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
pražce	pražec	k1gInPc1	pražec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
bezpražcové	bezpražcový	k2eAgFnPc1d1	bezpražcová
baskytary	baskytara	k1gFnPc1	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
neelektrické	elektrický	k2eNgFnPc4d1	neelektrická
akustické	akustický	k2eAgFnPc4d1	akustická
basové	basový	k2eAgFnPc4d1	basová
kytary	kytara	k1gFnPc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Ladění	ladění	k1gNnSc1	ladění
nástroje	nástroj	k1gInSc2	nástroj
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
u	u	k7c2	u
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřstrunná	čtyřstrunný	k2eAgFnSc1d1	čtyřstrunná
baskytara	baskytara	k1gFnSc1	baskytara
bývá	bývat	k5eAaImIp3nS	bývat
laděná	laděný	k2eAgFnSc1d1	laděná
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
kontrabas	kontrabas	k1gInSc1	kontrabas
po	po	k7c6	po
kvartách	kvarta	k1gFnPc6	kvarta
E	E	kA	E
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
G	G	kA	G
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
čtyři	čtyři	k4xCgFnPc4	čtyři
nejhlubší	hluboký	k2eAgFnPc4d3	nejhlubší
struny	struna	k1gFnPc4	struna
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
více	hodně	k6eAd2	hodně
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
hlubší	hluboký	k2eAgMnSc1d2	hlubší
H	H	kA	H
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
častěji	často	k6eAd2	často
i	i	k9	i
vysoké	vysoká	k1gFnPc4	vysoká
C.	C.	kA	C.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
u	u	k7c2	u
čtyřstrunné	čtyřstrunný	k2eAgFnSc2d1	čtyřstrunná
baskytary	baskytara	k1gFnSc2	baskytara
používá	používat	k5eAaImIp3nS	používat
ladění	ladění	k1gNnSc4	ladění
H	H	kA	H
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
D	D	kA	D
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
hlubší	hluboký	k2eAgFnPc1d2	hlubší
struny	struna	k1gFnPc1	struna
od	od	k7c2	od
5	[number]	k4	5
<g/>
strunné	strunný	k2eAgFnSc2d1	strunná
baskytary	baskytara	k1gFnSc2	baskytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
částí	část	k1gFnSc7	část
basové	basový	k2eAgFnPc1d1	basová
kytary	kytara	k1gFnPc1	kytara
jsou	být	k5eAaImIp3nP	být
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
krk	krk	k1gInSc4	krk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
krku	krk	k1gInSc2	krk
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
s	s	k7c7	s
ladicí	ladicí	k2eAgFnSc7d1	ladicí
mechanikou	mechanika	k1gFnSc7	mechanika
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
křídlovými	křídlový	k2eAgInPc7d1	křídlový
kolíky	kolík	k1gInPc7	kolík
<g/>
.	.	kIx.	.
</s>
<s>
Hmatník	hmatník	k1gInSc1	hmatník
vždy	vždy	k6eAd1	vždy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
-	-	kIx~	-
nultý	nultý	k4xOgInSc1	nultý
pražec	pražec	k1gInSc1	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
kobylkou	kobylka	k1gFnSc7	kobylka
délku	délka	k1gFnSc4	délka
struny	struna	k1gFnSc2	struna
-	-	kIx~	-
menzuru	menzura	k1gFnSc4	menzura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
pod	pod	k7c7	pod
strunami	struna	k1gFnPc7	struna
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgMnSc1d1	umístěn
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
snímačů	snímač	k1gInPc2	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
ovladače	ovladač	k1gInPc4	ovladač
a	a	k8xC	a
potenciometry	potenciometr	k1gInPc4	potenciometr
<g/>
.	.	kIx.	.
</s>
<s>
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
i	i	k9	i
výstup	výstup	k1gInSc4	výstup
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
kabelu	kabel	k1gInSc2	kabel
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
velký	velký	k2eAgInSc1d1	velký
jack	jack	k1gInSc1	jack
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
nástroj	nástroj	k1gInSc1	nástroj
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
masivní	masivní	k2eAgInSc4d1	masivní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udržel	udržet	k5eAaPmAgInS	udržet
tah	tah	k1gInSc1	tah
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgFnPc1d2	silnější
než	než	k8xS	než
u	u	k7c2	u
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
také	také	k9	také
správné	správný	k2eAgNnSc4d1	správné
vyvážení	vyvážení	k1gNnSc4	vyvážení
a	a	k8xC	a
uchycení	uchycení	k1gNnSc4	uchycení
popruhů	popruh	k1gInPc2	popruh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nástroj	nástroj	k1gInSc1	nástroj
dobře	dobře	k6eAd1	dobře
držel	držet	k5eAaImAgInS	držet
a	a	k8xC	a
pohodlně	pohodlně	k6eAd1	pohodlně
ovládal	ovládat	k5eAaImAgInS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Pražce	pražec	k1gInPc1	pražec
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
hmatník	hmatník	k1gInSc4	hmatník
na	na	k7c4	na
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
ladí	ladit	k5eAaImIp3nP	ladit
výsledný	výsledný	k2eAgInSc4d1	výsledný
tón	tón	k1gInSc4	tón
po	po	k7c6	po
půltónech	půltón	k1gInPc6	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
baskytary	baskytara	k1gFnPc1	baskytara
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
žádné	žádný	k3yNgInPc1	žádný
pražce	pražec	k1gInPc1	pražec
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
komerčně	komerčně	k6eAd1	komerčně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
baskytara	baskytara	k1gFnSc1	baskytara
Fender	Fendra	k1gFnPc2	Fendra
Precision	Precision	k1gInSc4	Precision
Bass	Bass	k1gMnSc1	Bass
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předlohou	předloha	k1gFnSc7	předloha
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
baskytar	baskytara	k1gFnPc2	baskytara
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
20	[number]	k4	20
pražců	pražec	k1gInPc2	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Basy	basa	k1gFnPc1	basa
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
často	často	k6eAd1	často
osazeny	osadit	k5eAaPmNgFnP	osadit
24	[number]	k4	24
pražci	pražec	k1gInPc7	pražec
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
oktávy	oktáva	k1gFnPc1	oktáva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
Zon	Zon	k1gFnSc1	Zon
Hyperbass	Hyperbass	k1gInSc1	Hyperbass
<g/>
,	,	kIx,	,
bezpražcová	bezpražcový	k2eAgFnSc1d1	bezpražcová
experimentální	experimentální	k2eAgFnSc1d1	experimentální
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hmatník	hmatník	k1gInSc1	hmatník
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
3	[number]	k4	3
oktávy	oktáv	k1gInPc4	oktáv
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
36	[number]	k4	36
pražcům	pražec	k1gInPc3	pražec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezpražcová	Bezpražcový	k2eAgFnSc1d1	Bezpražcový
baskytara	baskytara	k1gFnSc1	baskytara
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
zvuk	zvuk	k1gInSc4	zvuk
než	než	k8xS	než
baskytara	baskytara	k1gFnSc1	baskytara
s	s	k7c7	s
pražci	pražec	k1gInPc7	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
přitisknuta	přitisknout	k5eAaPmNgFnS	přitisknout
k	k	k7c3	k
hmatníku	hmatník	k1gInSc3	hmatník
na	na	k7c6	na
přesném	přesný	k2eAgNnSc6d1	přesné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dělá	dělat	k5eAaImIp3nS	dělat
u	u	k7c2	u
kontrabasu	kontrabas	k1gInSc2	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
malý	malý	k2eAgInSc1d1	malý
pohyb	pohyb	k1gInSc1	pohyb
prstu	prst	k1gInSc2	prst
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
na	na	k7c4	na
ladění	ladění	k1gNnSc4	ladění
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
měkčí	měkký	k2eAgInSc1d2	měkčí
<g/>
,	,	kIx,	,
kulatější	kulatý	k2eAgInSc1d2	kulatější
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mnohem	mnohem	k6eAd1	mnohem
bohatší	bohatý	k2eAgFnSc4d2	bohatší
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
hudební	hudební	k2eAgInSc1d1	hudební
výraz	výraz	k1gInSc1	výraz
-	-	kIx~	-
glissando	glissando	k1gNnSc1	glissando
<g/>
,	,	kIx,	,
vibrato	vibrato	k6eAd1	vibrato
nebo	nebo	k8xC	nebo
jemnější	jemný	k2eAgNnSc4d2	jemnější
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
intonaci	intonace	k1gFnSc4	intonace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čtvrttóny	čtvrttón	k1gInPc1	čtvrttón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
baskytaristé	baskytarista	k1gMnPc1	baskytarista
používají	používat	k5eAaImIp3nP	používat
pražcové	pražcový	k2eAgFnPc4d1	pražcová
i	i	k8xC	i
bezpražcové	bezpražcový	k2eAgFnPc4d1	bezpražcová
baskytary	baskytara	k1gFnPc4	baskytara
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
je	on	k3xPp3gFnPc4	on
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
právě	právě	k6eAd1	právě
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
<g/>
.	.	kIx.	.
</s>
<s>
Bezpražcová	Bezpražcový	k2eAgFnSc1d1	Bezpražcový
basa	basa	k1gFnSc1	basa
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
především	především	k9	především
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
a	a	k8xC	a
jazz	jazz	k1gInSc1	jazz
fusion	fusion	k1gInSc1	fusion
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
i	i	k9	i
nástroje	nástroj	k1gInPc1	nástroj
s	s	k7c7	s
pěti	pět	k4xCc7	pět
strunami	struna	k1gFnPc7	struna
-	-	kIx~	-
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
struna	struna	k1gFnSc1	struna
H	H	kA	H
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
kvartu	kvarta	k1gFnSc4	kvarta
níž	nízce	k6eAd2	nízce
než	než	k8xS	než
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
struna	struna	k1gFnSc1	struna
<g/>
.	.	kIx.	.
</s>
<s>
Šestistrunné	šestistrunný	k2eAgFnPc1d1	šestistrunná
baskytary	baskytara	k1gFnPc1	baskytara
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
nejčastěji	často	k6eAd3	často
strunu	struna	k1gFnSc4	struna
C	C	kA	C
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
kvartu	kvarta	k1gFnSc4	kvarta
výš	vysoce	k6eAd2	vysoce
než	než	k8xS	než
struna	struna	k1gFnSc1	struna
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Ladění	ladění	k1gNnSc1	ladění
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc4d1	podobné
ladění	ladění	k1gNnSc4	ladění
barytonové	barytonový	k2eAgFnSc2d1	barytonová
kytary	kytara	k1gFnSc2	kytara
-	-	kIx~	-
HEADGC	HEADGC	kA	HEADGC
oproti	oproti	k7c3	oproti
barytonovému	barytonový	k2eAgNnSc3d1	barytonové
HEADF	HEADF	kA	HEADF
<g/>
#	#	kIx~	#
<g/>
H.	H.	kA	H.
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
nástroje	nástroj	k1gInPc1	nástroj
se	s	k7c7	s
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
11	[number]	k4	11
strunami	struna	k1gFnPc7	struna
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
baskytaristů	baskytarista	k1gMnPc2	baskytarista
při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
na	na	k7c4	na
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
i	i	k9	i
sedět	sedět	k5eAaImF	sedět
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
hudebních	hudební	k2eAgNnPc6d1	hudební
tělesech	těleso	k1gNnPc6	těleso
(	(	kIx(	(
<g/>
jazzový	jazzový	k2eAgMnSc1d1	jazzový
big	big	k?	big
band	band	k1gInSc1	band
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
folkové	folkový	k2eAgFnSc6d1	folková
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c6	na
osobní	osobní	k2eAgFnSc6d1	osobní
preferenci	preference	k1gFnSc6	preference
hráče	hráč	k1gMnPc4	hráč
nebo	nebo	k8xC	nebo
na	na	k7c4	na
očekávání	očekávání	k1gNnSc4	očekávání
kapelníka	kapelník	k1gMnSc2	kapelník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
v	v	k7c6	v
sedě	sedě	k6eAd1	sedě
leží	ležet	k5eAaImIp3nS	ležet
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
u	u	k7c2	u
praváka	pravák	k1gMnSc2	pravák
<g/>
)	)	kIx)	)
na	na	k7c6	na
levém	levý	k2eAgNnSc6d1	levé
stehně	stehno	k1gNnSc6	stehno
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
kytaru	kytara	k1gFnSc4	kytara
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
stehně	stehno	k1gNnSc6	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
vyvažování	vyvažování	k1gNnSc2	vyvažování
nástroje	nástroj	k1gInSc2	nástroj
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
stehně	stehno	k1gNnSc6	stehno
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
způsobu	způsob	k1gInSc2	způsob
hry	hra	k1gFnSc2	hra
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
hrát	hrát	k5eAaImF	hrát
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c4	na
kontrabas	kontrabas	k1gInSc4	kontrabas
prsty	prst	k1gInPc4	prst
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
trsátkem	trsátko	k1gNnSc7	trsátko
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
prsty	prst	k1gInPc7	prst
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
střídáním	střídání	k1gNnSc7	střídání
ukazováku	ukazovák	k1gInSc2	ukazovák
a	a	k8xC	a
prostředníčku	prostředníček	k1gInSc2	prostředníček
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc1d1	různá
kombinace	kombinace	k1gFnPc1	kombinace
ještě	ještě	k9	ještě
s	s	k7c7	s
prsteníčkem	prsteníček	k1gInSc7	prsteníček
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
produkuje	produkovat	k5eAaImIp3nS	produkovat
spíše	spíše	k9	spíše
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
kulatý	kulatý	k2eAgInSc1d1	kulatý
zvuk	zvuk	k1gInSc1	zvuk
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
výhoda	výhoda	k1gFnSc1	výhoda
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c6	v
mnohostranném	mnohostranný	k2eAgNnSc6d1	mnohostranné
využití	využití	k1gNnSc6	využití
všech	všecek	k3xTgInPc2	všecek
prstů	prst	k1gInPc2	prst
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
s	s	k7c7	s
malíčkem	malíček	k1gInSc7	malíček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
lépe	dobře	k6eAd2	dobře
zvládá	zvládat	k5eAaImIp3nS	zvládat
melodicky	melodicky	k6eAd1	melodicky
složitější	složitý	k2eAgFnSc2d2	složitější
figury	figura	k1gFnSc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
hra	hra	k1gFnSc1	hra
trsátkem	trsátko	k1gNnSc7	trsátko
produkuje	produkovat	k5eAaImIp3nS	produkovat
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
zvuk	zvuk	k1gInSc1	zvuk
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychlá	rychlý	k2eAgFnSc1d1	rychlá
hra	hra	k1gFnSc1	hra
často	často	k6eAd1	často
ani	ani	k8xC	ani
neobejde	obejde	k6eNd1	obejde
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
série	série	k1gFnSc1	série
tónů	tón	k1gInPc2	tón
stejné	stejný	k2eAgFnSc2d1	stejná
výšky	výška	k1gFnSc2	výška
neslévaly	slévat	k5eNaImAgInP	slévat
<g/>
)	)	kIx)	)
a	a	k8xC	a
výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
je	být	k5eAaImIp3nS	být
především	především	k9	především
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
zvládání	zvládání	k1gNnSc1	zvládání
rytmicky	rytmicky	k6eAd1	rytmicky
složitých	složitý	k2eAgFnPc2d1	složitá
figur	figura	k1gFnPc2	figura
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
potom	potom	k6eAd1	potom
výdrž	výdrž	k1gFnSc4	výdrž
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
ale	ale	k8xC	ale
často	často	k6eAd1	často
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
typu	typ	k1gInSc6	typ
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Prsty	prst	k1gInPc1	prst
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
nebo	nebo	k8xC	nebo
funku	funk	k1gInSc6	funk
<g/>
,	,	kIx,	,
trsátko	trsátko	k1gNnSc4	trsátko
zase	zase	k9	zase
v	v	k7c4	v
heavy	heav	k1gInPc4	heav
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc4	ten
ale	ale	k8xC	ale
žádné	žádný	k3yNgNnSc4	žádný
dogma	dogma	k1gNnSc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
hrají	hrát	k5eAaImIp3nP	hrát
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používají	používat	k5eAaImIp3nP	používat
nehty	nehet	k1gInPc4	nehet
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
flamenco	flamenco	k1gNnSc1	flamenco
na	na	k7c6	na
klasické	klasický	k2eAgFnSc6d1	klasická
kytaře	kytara	k1gFnSc6	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
místě	místo	k1gNnSc6	místo
jsou	být	k5eAaImIp3nP	být
rozeznívány	rozezníván	k2eAgFnPc4d1	rozezníván
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
i	i	k9	i
barva	barva	k1gFnSc1	barva
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Jaco	Jaco	k1gMnSc1	Jaco
Pastorius	Pastorius	k1gMnSc1	Pastorius
často	často	k6eAd1	často
hrál	hrát	k5eAaImAgMnS	hrát
co	co	k9	co
nejblíže	blízce	k6eAd3	blízce
kobylce	kobylka	k1gFnSc3	kobylka
<g/>
.	.	kIx.	.
</s>
<s>
Baskytara	baskytara	k1gFnSc1	baskytara
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
jasný	jasný	k2eAgInSc4d1	jasný
krátký	krátký	k2eAgInSc4d1	krátký
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
naopak	naopak	k6eAd1	naopak
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
hraní	hraní	k1gNnSc2	hraní
blíže	blíž	k1gFnSc2	blíž
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rozmáznutý	rozmáznutý	k2eAgMnSc1d1	rozmáznutý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohutný	mohutný	k2eAgInSc1d1	mohutný
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možnou	možný	k2eAgFnSc7d1	možná
technikou	technika	k1gFnSc7	technika
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
nejsou	být	k5eNaImIp3nP	být
rozeznívány	rozeznívat	k5eAaImNgFnP	rozeznívat
drnkáním	drnkání	k1gNnSc7	drnkání
ale	ale	k8xC	ale
nárazy	náraz	k1gInPc7	náraz
palce	palec	k1gInSc2	palec
proti	proti	k7c3	proti
nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
pražci	pražec	k1gInSc3	pražec
hmatníku	hmatník	k1gInSc2	hmatník
baskytary	baskytara	k1gFnSc2	baskytara
(	(	kIx(	(
<g/>
slap	slap	k1gInSc1	slap
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střídanými	střídaný	k2eAgInPc7d1	střídaný
s	s	k7c7	s
vytrháváním	vytrhávání	k1gNnSc7	vytrhávání
struny	struna	k1gFnSc2	struna
ukazováčkem	ukazováček	k1gInSc7	ukazováček
nebo	nebo	k8xC	nebo
prostředníčkem	prostředníček	k1gMnSc7	prostředníček
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
pop	pop	k1gMnSc1	pop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
technikou	technika	k1gFnSc7	technika
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
Flea	Flea	k1gMnSc1	Flea
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnSc3	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
vynašli	vynajít	k5eAaPmAgMnP	vynajít
dva	dva	k4xCgMnPc1	dva
baskytaristé	baskytarista	k1gMnPc1	baskytarista
<g/>
:	:	kIx,	:
Marcus	Marcus	k1gMnSc1	Marcus
Miller	Miller	k1gMnSc1	Miller
a	a	k8xC	a
Victor	Victor	k1gMnSc1	Victor
Wooten	Wooten	k2eAgMnSc1d1	Wooten
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc2	který
jsou	být	k5eAaImIp3nP	být
struny	struna	k1gFnPc1	struna
rozeznívány	rozeznívat	k5eAaImNgFnP	rozeznívat
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Palec	palec	k1gInSc1	palec
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
strunu	struna	k1gFnSc4	struna
<g/>
,	,	kIx,	,
zastaví	zastavit	k5eAaPmIp3nS	zastavit
se	se	k3xPyFc4	se
o	o	k7c4	o
strunu	struna	k1gFnSc4	struna
níže	nízce	k6eAd2	nízce
a	a	k8xC	a
hned	hned	k6eAd1	hned
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
nahoru	nahoru	k6eAd1	nahoru
(	(	kIx(	(
<g/>
i	i	k9	i
zde	zde	k6eAd1	zde
rozezní	rozeznět	k5eAaPmIp3nP	rozeznět
strunu	struna	k1gFnSc4	struna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
bývá	bývat	k5eAaImIp3nS	bývat
doplněna	doplnit	k5eAaPmNgFnS	doplnit
trhnutím	trhnutí	k1gNnSc7	trhnutí
ukazováku	ukazovák	k1gInSc2	ukazovák
a	a	k8xC	a
prostředníku	prostředník	k1gInSc2	prostředník
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
baskytara	baskytara	k1gFnSc1	baskytara
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
hudebních	hudební	k2eAgInPc6d1	hudební
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
původně	původně	k6eAd1	původně
používaný	používaný	k2eAgInSc1d1	používaný
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Baskytara	baskytara	k1gFnSc1	baskytara
se	se	k3xPyFc4	se
ovládá	ovládat	k5eAaImIp3nS	ovládat
snadněji	snadno	k6eAd2	snadno
než	než	k8xS	než
kontrabas	kontrabas	k1gInSc4	kontrabas
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
zesilovat	zesilovat	k5eAaImF	zesilovat
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
volnosti	volnost	k1gFnSc2	volnost
než	než	k8xS	než
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
kontrabas	kontrabas	k1gInSc4	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
hudebních	hudební	k2eAgInPc6d1	hudební
stylech	styl	k1gInPc6	styl
stává	stávat	k5eAaImIp3nS	stávat
baskytara	baskytara	k1gFnSc1	baskytara
rovnocenným	rovnocenný	k2eAgMnSc7d1	rovnocenný
sólovým	sólový	k2eAgInSc7d1	sólový
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
baskytaristů	baskytarista	k1gMnPc2	baskytarista
Fender	Fendra	k1gFnPc2	Fendra
Precision	Precision	k1gInSc1	Precision
Bass	Bass	k1gMnSc1	Bass
Akustická	akustický	k2eAgFnSc1d1	akustická
baskytara	baskytara	k1gFnSc1	baskytara
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Baskytara	baskytara	k1gFnSc1	baskytara
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Brána	brána	k1gFnSc1	brána
do	do	k7c2	do
světa	svět	k1gInSc2	svět
baskytary	baskytara	k1gFnSc2	baskytara
</s>
