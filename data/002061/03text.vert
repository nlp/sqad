<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
[	[	kIx(	[
<g/>
væ	væ	k?	væ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
je	být	k5eAaImIp3nS	být
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
metropolitní	metropolitní	k2eAgNnSc4d1	metropolitní
centrum	centrum	k1gNnSc4	centrum
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
třetí	třetí	k4xOgFnSc3	třetí
největší	veliký	k2eAgFnSc3d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Regionální	regionální	k2eAgInSc1d1	regionální
okres	okres	k1gInSc1	okres
Metro	metro	k1gNnSc1	metro
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
většího	veliký	k2eAgInSc2d2	veliký
celku	celek	k1gInSc2	celek
obecně	obecně	k6eAd1	obecně
známého	známý	k2eAgNnSc2d1	známé
jako	jako	k8xS	jako
Lower	Lower	k1gInSc1	Lower
Mainland	Mainlanda	k1gFnPc2	Mainlanda
<g/>
.	.	kIx.	.
</s>
<s>
Vancouverský	Vancouverský	k2eAgInSc1d1	Vancouverský
přístav	přístav	k1gInSc1	přístav
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
největší	veliký	k2eAgInSc1d3	veliký
objem	objem	k1gInSc1	objem
přepravy	přeprava	k1gFnSc2	přeprava
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hollywoodu	Hollywood	k1gInSc6	Hollywood
a	a	k8xC	a
New	New	k1gFnSc3	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
Vancouver	Vancouver	k1gInSc1	Vancouver
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
centrem	centr	k1gInSc7	centr
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
Hollywood	Hollywood	k1gInSc1	Hollywood
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
hostil	hostit	k5eAaImAgInS	hostit
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Fraser	Frasra	k1gFnPc2	Frasra
u	u	k7c2	u
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
úžinou	úžina	k1gFnSc7	úžina
Strait	Strait	k1gMnSc1	Strait
of	of	k?	of
Georgia	Georgia	k1gFnSc1	Georgia
a	a	k8xC	a
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Coast	Coast	k1gFnSc4	Coast
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Rychlým	rychlý	k2eAgNnSc7d1	rychlé
tempem	tempo	k1gNnSc7	tempo
se	se	k3xPyFc4	se
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
osady	osada	k1gFnSc2	osada
stalo	stát	k5eAaPmAgNnS	stát
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc4	přístav
nabyl	nabýt	k5eAaPmAgMnS	nabýt
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
431	[number]	k4	431
147	[number]	k4	147
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
aglomerace	aglomerace	k1gFnSc1	aglomerace
1,51	[number]	k4	1,51
miliónu	milión	k4xCgInSc2	milión
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
587	[number]	k4	587
891	[number]	k4	891
(	(	kIx(	(
<g/>
aglomerace	aglomerace	k1gFnSc2	aglomerace
2	[number]	k4	2
180	[number]	k4	180
737	[number]	k4	737
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
114,67	[number]	k4	114,67
km2	km2	k4	km2
(	(	kIx(	(
<g/>
aglomerace	aglomerace	k1gFnSc2	aglomerace
2	[number]	k4	2
878,52	[number]	k4	878,52
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
hranici	hranice	k1gFnSc6	hranice
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
pacifického	pacifický	k2eAgNnSc2d1	pacifické
časového	časový	k2eAgNnSc2d1	časové
pásma	pásmo	k1gNnSc2	pásmo
(	(	kIx(	(
<g/>
UTC-	UTC-	k1gFnSc1	UTC-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
upozornit	upozornit	k5eAaPmF	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
neleží	ležet	k5eNaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgFnSc1	obojí
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
kapitánovi	kapitán	k1gMnSc3	kapitán
Královského	královský	k2eAgNnSc2d1	královské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Georgi	Georg	k1gMnSc3	Georg
Vancouverovi	Vancouver	k1gMnSc3	Vancouver
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zde	zde	k6eAd1	zde
prováděl	provádět	k5eAaImAgInS	provádět
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kanadského	kanadský	k2eAgInSc2d1	kanadský
Vancouveru	Vancouver	k1gInSc2	Vancouver
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
město	město	k1gNnSc1	město
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgFnPc2d1	archeologická
vykopávek	vykopávka	k1gFnPc2	vykopávka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc4d1	možné
první	první	k4xOgNnSc4	první
osídlení	osídlení	k1gNnSc4	osídlení
oblasti	oblast	k1gFnSc2	oblast
datovat	datovat	k5eAaImF	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
2	[number]	k4	2
500	[number]	k4	500
až	až	k9	až
7	[number]	k4	7
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
pobřeží	pobřeží	k1gNnSc1	pobřeží
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
španělský	španělský	k2eAgMnSc1d1	španělský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
José	José	k1gNnSc2	José
María	María	k1gMnSc1	María
Narváez	Narváez	k1gMnSc1	Narváez
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
George	George	k1gInSc1	George
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Přistál	přistát	k5eAaPmAgMnS	přistát
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
dal	dát	k5eAaPmAgInS	dát
britské	britský	k2eAgNnSc4d1	Britské
jméno	jméno	k1gNnSc4	jméno
Burrard	Burrarda	k1gFnPc2	Burrarda
<g/>
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
Fraser	Fraser	k1gMnSc1	Fraser
<g/>
,	,	kIx,	,
průzkumník	průzkumník	k1gMnSc1	průzkumník
a	a	k8xC	a
obchodník	obchodník	k1gMnSc1	obchodník
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
společnosti	společnost	k1gFnSc2	společnost
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
splavil	splavit	k5eAaPmAgMnS	splavit
řeku	řeka	k1gFnSc4	řeka
Fraser	Fraser	k1gMnSc1	Fraser
až	až	k9	až
po	po	k7c4	po
West	West	k1gInSc4	West
Point	pointa	k1gFnPc2	pointa
Grey	Grea	k1gFnSc2	Grea
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
Univerzita	univerzita	k1gFnSc1	univerzita
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
přivedla	přivést	k5eAaPmAgFnS	přivést
Sobí	sobí	k2eAgFnSc1d1	sobí
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
řeky	řeka	k1gFnSc2	řeka
Fraser	Frasra	k1gFnPc2	Frasra
a	a	k8xC	a
položila	položit	k5eAaPmAgFnS	položit
zde	zde	k6eAd1	zde
základ	základ	k1gInSc4	základ
trvalého	trvalý	k2eAgNnSc2d1	trvalé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založené	založený	k2eAgNnSc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
McLeary	McLeara	k1gFnSc2	McLeara
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Fraser	Frasra	k1gFnPc2	Frasra
<g/>
.	.	kIx.	.
</s>
<s>
Pila	pila	k1gFnSc1	pila
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
Moodywille	Moodywilla	k1gFnSc6	Moodywilla
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
North	North	k1gMnSc1	North
Vancouver	Vancouver	k1gMnSc1	Vancouver
<g/>
)	)	kIx)	)
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
dřevozpracujícímu	dřevozpracující	k2eAgInSc3d1	dřevozpracující
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
dalších	další	k2eAgFnPc2d1	další
cest	cesta	k1gFnPc2	cesta
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
zátoky	zátoka	k1gFnSc2	zátoka
patřící	patřící	k2eAgFnSc1d1	patřící
kapitánu	kapitán	k1gMnSc3	kapitán
Edwardu	Edward	k1gMnSc3	Edward
Stampovi	Stamp	k1gMnSc3	Stamp
<g/>
.	.	kIx.	.
</s>
<s>
Stamp	Stamp	k1gMnSc1	Stamp
začal	začít	k5eAaPmAgMnS	začít
těžit	těžit	k5eAaImF	těžit
dřevo	dřevo	k1gNnSc4	dřevo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Port	porto	k1gNnPc2	porto
Alberni	Alberni	k1gMnPc2	Alberni
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
mlýn	mlýn	k1gInSc4	mlýn
v	v	k7c4	v
Brockton	Brockton	k1gInSc4	Brockton
Pointu	pointa	k1gFnSc4	pointa
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
maják	maják	k1gInSc4	maják
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Stanley	Stanlea	k1gFnSc2	Stanlea
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
silné	silný	k2eAgInPc1d1	silný
vodní	vodní	k2eAgInPc1d1	vodní
proudy	proud	k1gInPc1	proud
a	a	k8xC	a
útesy	útes	k1gInPc1	útes
ho	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
donutily	donutit	k5eAaPmAgFnP	donutit
postavit	postavit	k5eAaPmF	postavit
mlýn	mlýn	k1gInSc4	mlýn
na	na	k7c4	na
Gore	Gore	k1gNnSc4	Gore
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
známe	znát	k5eAaImIp1nP	znát
jako	jako	k8xC	jako
Hastings	Hastings	k1gInSc4	Hastings
Mill	Milla	k1gFnPc2	Milla
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
mlýna	mlýn	k1gInSc2	mlýn
postupně	postupně	k6eAd1	postupně
vznikalo	vznikat	k5eAaImAgNnS	vznikat
centrum	centrum	k1gNnSc1	centrum
dnešního	dnešní	k2eAgInSc2d1	dnešní
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
železnice	železnice	k1gFnSc2	železnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
začal	začít	k5eAaPmAgInS	začít
jeho	jeho	k3xOp3gInSc4	jeho
význam	význam	k1gInSc4	význam
postupně	postupně	k6eAd1	postupně
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
uzavření	uzavření	k1gNnSc2	uzavření
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
Gastown	Gastowna	k1gFnPc2	Gastowna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
whisky	whisky	k1gFnSc2	whisky
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zvládnou	zvládnout	k5eAaPmIp3nP	zvládnout
vypít	vypít	k5eAaPmF	vypít
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
posezení	posezení	k1gNnSc4	posezení
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
dělníky	dělník	k1gMnPc7	dělník
z	z	k7c2	z
mlýna	mlýn	k1gInSc2	mlýn
a	a	k8xC	a
s	s	k7c7	s
námořníky	námořník	k1gMnPc7	námořník
John	John	k1gMnSc1	John
Deighton	Deighton	k1gInSc1	Deighton
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
vláda	vláda	k1gFnSc1	vláda
provincie	provincie	k1gFnSc2	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
města	město	k1gNnSc2	město
přejmenované	přejmenovaný	k2eAgFnPc1d1	přejmenovaná
na	na	k7c6	na
Granville	Granvilla	k1gFnSc6	Granvilla
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
přístavem	přístav	k1gInSc7	přístav
později	pozdě	k6eAd2	pozdě
vybrané	vybraný	k2eAgNnSc1d1	vybrané
jako	jako	k8xS	jako
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
pro	pro	k7c4	pro
Kanadskou	kanadský	k2eAgFnSc4d1	kanadská
Pacifickou	pacifický	k2eAgFnSc4d1	Pacifická
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Vancouver	Vancouvero	k1gNnPc2	Vancouvero
oficiálně	oficiálně	k6eAd1	oficiálně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zápisem	zápis	k1gInSc7	zápis
do	do	k7c2	do
registru	registr	k1gInSc2	registr
jako	jako	k8xC	jako
městská	městský	k2eAgFnSc1d1	městská
samospráva	samospráva	k1gFnSc1	samospráva
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
do	do	k7c2	do
města	město	k1gNnSc2	město
dorazila	dorazit	k5eAaPmAgFnS	dorazit
Transkontinentální	transkontinentální	k2eAgFnSc1d1	transkontinentální
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc4	jméno
města	město	k1gNnSc2	město
vybral	vybrat	k5eAaPmAgMnS	vybrat
prezident	prezident	k1gMnSc1	prezident
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
CPR	CPR	kA	CPR
<g/>
)	)	kIx)	)
William	William	k1gInSc1	William
Cornelius	Cornelius	k1gInSc1	Cornelius
Van	van	k1gInSc1	van
Horne	Horn	k1gMnSc5	Horn
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
lehlo	lehnout	k5eAaPmAgNnS	lehnout
popelem	popel	k1gInSc7	popel
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
tragédie	tragédie	k1gFnSc2	tragédie
rychle	rychle	k6eAd1	rychle
vzpamatoval	vzpamatovat	k5eAaPmAgMnS	vzpamatovat
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
i	i	k8xC	i
první	první	k4xOgFnSc1	první
městská	městský	k2eAgFnSc1d1	městská
požární	požární	k2eAgFnSc1d1	požární
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osady	osada	k1gFnSc2	osada
čítající	čítající	k2eAgInSc4d1	čítající
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozrostl	rozrůst	k5eAaPmAgMnS	rozrůst
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
až	až	k6eAd1	až
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
města	město	k1gNnSc2	město
velmi	velmi	k6eAd1	velmi
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
firmách	firma	k1gFnPc6	firma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
měly	mít	k5eAaImAgFnP	mít
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
Vancouveru	Vancouver	k1gInSc2	Vancouver
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sice	sice	k8xC	sice
několik	několik	k4yIc1	několik
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
hlavním	hlavní	k2eAgInSc7d1	hlavní
průmyslem	průmysl	k1gInSc7	průmysl
zůstala	zůstat	k5eAaPmAgFnS	zůstat
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
komerční	komerční	k2eAgFnSc1d1	komerční
přeprava	přeprava	k1gFnSc1	přeprava
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
období	období	k1gNnSc4	období
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
k	k	k7c3	k
velkým	velký	k2eAgInPc3d1	velký
nepokojům	nepokoj	k1gInPc3	nepokoj
a	a	k8xC	a
stávkám	stávka	k1gFnPc3	stávka
vedeným	vedený	k2eAgFnPc3d1	vedená
hlavně	hlavně	k6eAd1	hlavně
členy	člen	k1gInPc7	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
nepokoje	nepokoj	k1gInSc2	nepokoj
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
1	[number]	k4	1
600	[number]	k4	600
stávkujících	stávkující	k2eAgMnPc2d1	stávkující
zaplavilo	zaplavit	k5eAaPmAgNnS	zaplavit
ulice	ulice	k1gFnPc4	ulice
města	město	k1gNnSc2	město
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
podmínkám	podmínka	k1gFnPc3	podmínka
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
spravovala	spravovat	k5eAaImAgFnS	spravovat
armáda	armáda	k1gFnSc1	armáda
ve	v	k7c6	v
vzdálených	vzdálený	k2eAgFnPc6d1	vzdálená
oblastech	oblast	k1gFnPc6	oblast
celé	celý	k2eAgFnSc2d1	celá
západní	západní	k2eAgFnSc2d1	západní
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
násilných	násilný	k2eAgInPc2d1	násilný
protestů	protest	k1gInPc2	protest
obrátili	obrátit	k5eAaPmAgMnP	obrátit
stávkující	stávkující	k1gMnPc1	stávkující
svůj	svůj	k3xOyFgInSc4	svůj
hněv	hněv	k1gInSc4	hněv
na	na	k7c4	na
federální	federální	k2eAgFnSc4d1	federální
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Ottawě	Ottawa	k1gFnSc6	Ottawa
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
odcestovat	odcestovat	k5eAaPmF	odcestovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
oznámili	oznámit	k5eAaPmAgMnP	oznámit
svoje	svůj	k3xOyFgInPc4	svůj
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
společenské	společenský	k2eAgNnSc4d1	společenské
hnutí	hnutí	k1gNnSc4	hnutí
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
patřilo	patřit	k5eAaImAgNnS	patřit
feministické	feministický	k2eAgNnSc1d1	feministické
hnutí	hnutí	k1gNnSc1	hnutí
a	a	k8xC	a
Svaz	svaz	k1gInSc1	svaz
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
žen	žena	k1gFnPc2	žena
požadující	požadující	k2eAgInSc4d1	požadující
zákaz	zákaz	k1gInSc4	zákaz
pití	pití	k1gNnSc2	pití
a	a	k8xC	a
distribuce	distribuce	k1gFnSc2	distribuce
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
droga	droga	k1gFnSc1	droga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ničí	ničit	k5eAaImIp3nS	ničit
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
zločinnost	zločinnost	k1gFnSc4	zločinnost
<g/>
.	.	kIx.	.
</s>
<s>
Prohibice	prohibice	k1gFnSc1	prohibice
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
po	po	k7c6	po
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
prodejem	prodej	k1gInSc7	prodej
alkoholu	alkohol	k1gInSc2	alkohol
trvající	trvající	k2eAgFnSc2d1	trvající
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
kanadský	kanadský	k2eAgInSc1d1	kanadský
protidrogový	protidrogový	k2eAgInSc1d1	protidrogový
zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
Liga	liga	k1gFnSc1	liga
za	za	k7c4	za
vyloučení	vyloučení	k1gNnSc4	vyloučení
Azijců	Azijce	k1gMnPc2	Azijce
(	(	kIx(	(
<g/>
sdružení	sdružení	k1gNnSc1	sdružení
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chtělo	chtít	k5eAaImAgNnS	chtít
zabránit	zabránit	k5eAaPmF	zabránit
lidem	lid	k1gInSc7	lid
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
)	)	kIx)	)
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
výtržnosti	výtržnost	k1gFnPc4	výtržnost
ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Chinatown	Chinatowna	k1gFnPc2	Chinatowna
a	a	k8xC	a
Japantown	Japantowna	k1gFnPc2	Japantowna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšetřovaní	vyšetřovaný	k1gMnPc1	vyšetřovaný
násilností	násilnost	k1gFnPc2	násilnost
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
poškozených	poškozený	k1gMnPc2	poškozený
byli	být	k5eAaImAgMnP	být
výrobci	výrobce	k1gMnPc1	výrobce
opia	opium	k1gNnSc2	opium
a	a	k8xC	a
že	že	k8xS	že
bělošky	běloška	k1gFnPc1	běloška
byly	být	k5eAaImAgFnP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Číňany	Číňan	k1gMnPc7	Číňan
častými	častý	k2eAgMnPc7d1	častý
návštěvníky	návštěvník	k1gMnPc7	návštěvník
kouření	kouření	k1gNnPc2	kouření
opia	opium	k1gNnSc2	opium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
odhalení	odhalení	k1gNnSc4	odhalení
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
schválili	schválit	k5eAaPmAgMnP	schválit
federální	federální	k2eAgInSc4d1	federální
zákon	zákon	k1gInSc4	zákon
zakazující	zakazující	k2eAgFnSc4d1	zakazující
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
prodej	prodej	k1gInSc4	prodej
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
opia	opium	k1gNnSc2	opium
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
než	než	k8xS	než
lékařským	lékařský	k2eAgInPc3d1	lékařský
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Sloučení	sloučení	k1gNnSc1	sloučení
s	s	k7c7	s
Point	pointa	k1gFnPc2	pointa
Grey	Grea	k1gFnPc4	Grea
a	a	k8xC	a
Jižním	jižní	k2eAgInSc7d1	jižní
Vancouverem	Vancouver	k1gInSc7	Vancouver
dalo	dát	k5eAaPmAgNnS	dát
městu	město	k1gNnSc3	město
finální	finální	k2eAgFnSc4d1	finální
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
stal	stát	k5eAaPmAgInS	stát
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
metropolí	metropol	k1gFnSc7	metropol
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1929	[number]	k4	1929
měl	mít	k5eAaImAgInS	mít
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
Vancouver	Vancouver	k1gInSc1	Vancouver
přes	přes	k7c4	přes
228	[number]	k4	228
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozprostíral	rozprostírat	k5eAaImAgMnS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
poloostrově	poloostrov	k1gInSc6	poloostrov
Burrard	Burrarda	k1gFnPc2	Burrarda
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Fraser	Frasra	k1gFnPc2	Frasra
až	až	k9	až
k	k	k7c3	k
zálivu	záliv	k1gInSc3	záliv
Burrard	Burrarda	k1gFnPc2	Burrarda
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
114,67	[number]	k4	114,67
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Terén	terén	k1gInSc1	terén
tvoří	tvořit	k5eAaImIp3nS	tvořit
nížiny	nížina	k1gFnPc4	nížina
i	i	k8xC	i
pahorkatiny	pahorkatina	k1gFnPc4	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
přímého	přímý	k2eAgInSc2d1	přímý
vlivu	vliv	k1gInSc2	vliv
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
ho	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
ostrov	ostrov	k1gInSc1	ostrov
Vancouver	Vancouvero	k1gNnPc2	Vancouvero
<g/>
,	,	kIx,	,
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
úžinou	úžina	k1gFnSc7	úžina
Strait	Straita	k1gFnPc2	Straita
of	of	k?	of
Georgia	Georgius	k1gMnSc2	Georgius
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
samotné	samotný	k2eAgNnSc1d1	samotné
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Burrard	Burrard	k1gMnSc1	Burrard
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
fjordem	fjord	k1gInSc7	fjord
Burrard	Burrarda	k1gFnPc2	Burrarda
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
ústím	ústí	k1gNnSc7	ústí
řeky	řeka	k1gFnSc2	řeka
Fraser	Frasra	k1gFnPc2	Frasra
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
městu	město	k1gNnSc3	město
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
třetí	třetí	k4xOgInSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Stanley	Stanle	k2eAgInPc4d1	Stanle
Park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
přes	přes	k7c4	přes
400	[number]	k4	400
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
staré	starý	k2eAgFnPc1d1	stará
stovky	stovka	k1gFnPc1	stovka
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc1	výška
až	až	k9	až
76	[number]	k4	76
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
přiláká	přilákat	k5eAaPmIp3nS	přilákat
každoročně	každoročně	k6eAd1	každoročně
okolo	okolo	k7c2	okolo
osmi	osm	k4xCc2	osm
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
turistů	turist	k1gMnPc2	turist
i	i	k8xC	i
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
města	město	k1gNnSc2	město
dominuje	dominovat	k5eAaImIp3nS	dominovat
pohoří	pohoří	k1gNnSc4	pohoří
North	Northa	k1gFnPc2	Northa
Shore	Shor	k1gMnSc5	Shor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
lze	lze	k6eAd1	lze
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
spatřit	spatřit	k5eAaPmF	spatřit
stratovulkán	stratovulkán	k1gInSc4	stratovulkán
Mount	Mount	k1gMnSc1	Mount
Baker	Baker	k1gMnSc1	Baker
(	(	kIx(	(
<g/>
3	[number]	k4	3
285	[number]	k4	285
m	m	kA	m
<g/>
)	)	kIx)	)
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
ostrov	ostrov	k1gInSc4	ostrov
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
pobřeží	pobřeží	k1gNnSc6	pobřeží
Sunshine	Sunshin	k1gInSc5	Sunshin
Coast	Coast	k1gMnSc1	Coast
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
kanadské	kanadský	k2eAgInPc4d1	kanadský
poměry	poměr	k1gInPc4	poměr
nezvykle	zvykle	k6eNd1	zvykle
mírné	mírný	k2eAgInPc4d1	mírný
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
má	mít	k5eAaImIp3nS	mít
oceánské	oceánský	k2eAgNnSc1d1	oceánské
klima	klima	k1gNnSc1	klima
ohřívané	ohřívaný	k2eAgNnSc1d1	ohřívané
teplým	teplý	k2eAgInSc7d1	teplý
Severním	severní	k2eAgInSc7d1	severní
tichomořským	tichomořský	k2eAgInSc7d1	tichomořský
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgInPc1d1	letní
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
slunečné	slunečný	k2eAgInPc1d1	slunečný
s	s	k7c7	s
příjemnými	příjemný	k2eAgFnPc7d1	příjemná
teplotami	teplota	k1gFnPc7	teplota
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
22	[number]	k4	22
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
okolo	okolo	k7c2	okolo
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
Celodenní	celodenní	k2eAgInPc1d1	celodenní
mrazy	mráz	k1gInPc1	mráz
a	a	k8xC	a
sněžení	sněžení	k1gNnSc1	sněžení
bývají	bývat	k5eAaImIp3nP	bývat
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
za	za	k7c4	za
rok	rok	k1gInSc4	rok
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
219	[number]	k4	219
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
úpatí	úpatí	k1gNnSc6	úpatí
hor	hora	k1gFnPc2	hora
však	však	k9	však
naprší	napršet	k5eAaPmIp3nS	napršet
ročně	ročně	k6eAd1	ročně
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
výskyt	výskyt	k1gInSc1	výskyt
srážek	srážka	k1gFnPc2	srážka
bývá	bývat	k5eAaImIp3nS	bývat
mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
a	a	k8xC	a
začátkem	začátek	k1gInSc7	začátek
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
rostou	růst	k5eAaImIp3nP	růst
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
především	především	k6eAd1	především
deštných	deštný	k2eAgInPc2d1	deštný
lesů	les	k1gInPc2	les
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
jehličnanů	jehličnan	k1gInPc2	jehličnan
a	a	k8xC	a
smíšených	smíšený	k2eAgFnPc2d1	smíšená
skupin	skupina	k1gFnPc2	skupina
javorů	javor	k1gInPc2	javor
a	a	k8xC	a
olší	olše	k1gFnPc2	olše
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgInPc4d1	typický
jehličnany	jehličnan	k1gInPc4	jehličnan
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
patří	patřit	k5eAaImIp3nS	patřit
smrk	smrk	k1gInSc4	smrk
sitka	sitka	k1gFnSc1	sitka
<g/>
,	,	kIx,	,
zerav	zerav	k1gInSc1	zerav
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
,	,	kIx,	,
jedlovec	jedlovec	k1gMnSc1	jedlovec
západní	západní	k2eAgMnSc1d1	západní
<g/>
,	,	kIx,	,
douglaska	douglaska	k1gFnSc1	douglaska
tisolistá	tisolistý	k2eAgFnSc1d1	tisolistá
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
největší	veliký	k2eAgFnSc7d3	veliký
koncentrací	koncentrace	k1gFnSc7	koncentrace
vysokých	vysoký	k2eAgInPc2d1	vysoký
stromů	strom	k1gInPc2	strom
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
pobřeží	pobřeží	k1gNnSc6	pobřeží
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Elliott	Elliott	k1gMnSc1	Elliott
Bay	Bay	k1gMnSc1	Bay
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgInPc1d1	možný
stromy	strom	k1gInPc1	strom
porovnávat	porovnávat	k5eAaImF	porovnávat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
výšky	výška	k1gFnSc2	výška
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
ve	v	k7c6	v
fjordu	fjord	k1gInSc6	fjord
Burrard	Burrard	k1gInSc1	Burrard
a	a	k8xC	a
zátoce	zátoka	k1gFnSc3	zátoka
English	Englisha	k1gFnPc2	Englisha
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
stromy	strom	k1gInPc1	strom
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Gastownu	Gastown	k1gInSc2	Gastown
<g/>
,	,	kIx,	,
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
začala	začít	k5eAaPmAgFnS	začít
nejdříve	dříve	k6eAd3	dříve
a	a	k8xC	a
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
březích	břeh	k1gInPc6	břeh
zátok	zátok	k1gInSc1	zátok
False	False	k1gFnSc1	False
Creek	Creek	k1gMnSc1	Creek
a	a	k8xC	a
English	English	k1gMnSc1	English
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pláže	pláž	k1gFnSc2	pláž
Jericho	Jericho	k1gNnSc1	Jericho
Beach	Beach	k1gInSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
ve	v	k7c4	v
Stanley	Stanlea	k1gFnPc4	Stanlea
Parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
druhé	druhý	k4xOgFnSc2	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
důkazy	důkaz	k1gInPc1	důkaz
požívání	požívání	k1gNnPc2	požívání
starých	starý	k2eAgFnPc2d1	stará
technik	technika	k1gFnPc2	technika
těžby	těžba	k1gFnSc2	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zářezy	zářez	k1gInPc1	zářez
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
po	po	k7c6	po
používání	používání	k1gNnSc6	používání
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
vrážely	vrážet	k5eAaImAgFnP	vrážet
<g/>
.	.	kIx.	.
</s>
<s>
Dřevorubec	dřevorubec	k1gMnSc1	dřevorubec
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
stál	stát	k5eAaImAgMnS	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgInS	moct
strom	strom	k1gInSc1	strom
uřezat	uřezat	k5eAaPmF	uřezat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
kmen	kmen	k1gInSc1	kmen
užší	úzký	k2eAgInSc1d2	užší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
okresu	okres	k1gInSc2	okres
West	West	k2eAgMnSc1d1	West
Vancouver	Vancouver	k1gMnSc1	Vancouver
roste	růst	k5eAaImIp3nS	růst
Arbutus	Arbutus	k1gMnSc1	Arbutus
menziesii	menziesie	k1gFnSc4	menziesie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
rostlinných	rostlinný	k2eAgMnPc2d1	rostlinný
druhu	druh	k1gInSc2	druh
dovezli	dovézt	k5eAaPmAgMnP	dovézt
kolonisté	kolonista	k1gMnPc1	kolonista
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
světadílů	světadíl	k1gInPc2	světadíl
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
pak	pak	k6eAd1	pak
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
palem	palma	k1gFnPc2	palma
<g/>
,	,	kIx,	,
blahočet	blahočet	k1gInSc1	blahočet
chilský	chilský	k2eAgInSc1d1	chilský
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
dlanitolistý	dlanitolistý	k2eAgInSc1d1	dlanitolistý
<g/>
,	,	kIx,	,
azalky	azalka	k1gFnPc1	azalka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
výsadba	výsadba	k1gFnSc1	výsadba
třešní	třešeň	k1gFnPc2	třešeň
pilovitých	pilovitý	k2eAgInPc2d1	pilovitý
neboli	neboli	k8xC	neboli
sakur	sakura	k1gFnPc2	sakura
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
město	město	k1gNnSc1	město
čtvrtí	čtvrt	k1gFnPc2	čtvrt
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
velké	velký	k2eAgFnSc2d1	velká
etnické	etnický	k2eAgFnSc2d1	etnická
různorodosti	různorodost	k1gFnSc2	různorodost
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
tvoří	tvořit	k5eAaImIp3nP	tvořit
historicky	historicky	k6eAd1	historicky
největší	veliký	k2eAgFnSc4d3	veliký
etnickou	etnický	k2eAgFnSc4d1	etnická
skupinu	skupina	k1gFnSc4	skupina
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
anglické	anglický	k2eAgFnSc2d1	anglická
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Vancouveru	Vancouver	k1gInSc2	Vancouver
velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgNnSc1d1	výrazné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c4	v
South	South	k1gInSc4	South
Granville	Granvilla	k1gFnSc3	Granvilla
a	a	k8xC	a
Kerrisdale	Kerrisdala	k1gFnSc3	Kerrisdala
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
představují	představovat	k5eAaImIp3nP	představovat
druhou	druhý	k4xOgFnSc4	druhý
nejviditelnější	viditelný	k2eAgFnSc4d3	nejviditelnější
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
komunit	komunita	k1gFnPc2	komunita
hovořící	hovořící	k2eAgInSc1d1	hovořící
čínskými	čínský	k2eAgInPc7d1	čínský
jazyky	jazyk	k1gInPc7	jazyk
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
městské	městský	k2eAgFnPc4d1	městská
čtvrti	čtvrt	k1gFnPc4	čtvrt
se	s	k7c7	s
zastoupením	zastoupení	k1gNnSc7	zastoupení
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
patří	patřit	k5eAaImIp3nP	patřit
Little	Little	k1gFnSc2	Little
Italy	Ital	k1gMnPc4	Ital
<g/>
,	,	kIx,	,
Japantown	Japantown	k1gInSc1	Japantown
<g/>
,	,	kIx,	,
Greektown	Greektown	k1gNnSc1	Greektown
a	a	k8xC	a
Punjabi	Punjabi	k1gNnSc1	Punjabi
Market	market	k1gInSc1	market
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
i	i	k9	i
jako	jako	k9	jako
Little	Little	k1gFnSc1	Little
India	indium	k1gNnSc2	indium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
asijská	asijský	k2eAgNnPc4d1	asijské
etnika	etnikum	k1gNnPc4	etnikum
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
<g/>
,	,	kIx,	,
Filipínci	Filipínec	k1gMnPc1	Filipínec
<g/>
,	,	kIx,	,
Kambodžané	Kambodžan	k1gMnPc1	Kambodžan
<g/>
,	,	kIx,	,
Korejci	Korejec	k1gMnPc1	Korejec
a	a	k8xC	a
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
emigrantů	emigrant	k1gMnPc2	emigrant
z	z	k7c2	z
Hong	Honga	k1gFnPc2	Honga
Kongu	Kongo	k1gNnSc6	Kongo
si	se	k3xPyFc3	se
vybralo	vybrat	k5eAaPmAgNnS	vybrat
Vancouver	Vancouver	k1gInSc4	Vancouver
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
bývalé	bývalý	k2eAgFnSc2d1	bývalá
britské	britský	k2eAgFnSc2d1	britská
kolonie	kolonie	k1gFnSc2	kolonie
Hong	Hong	k1gMnSc1	Hong
Kongu	Kongo	k1gNnSc6	Kongo
k	k	k7c3	k
Číně	Čína	k1gFnSc3	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c4	v
tradici	tradice	k1gFnSc4	tradice
přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
Vancouver	Vancouver	k1gInSc1	Vancouver
po	po	k7c6	po
Torontu	Toronto	k1gNnSc6	Toronto
druhou	druhý	k4xOgFnSc7	druhý
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
destinací	destinace	k1gFnSc7	destinace
pro	pro	k7c4	pro
emigranty	emigrant	k1gMnPc4	emigrant
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přívalu	příval	k1gInSc6	příval
emigrantů	emigrant	k1gMnPc2	emigrant
z	z	k7c2	z
Hong	Honga	k1gFnPc2	Honga
Kongu	Kongo	k1gNnSc6	Kongo
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgInPc1d3	veliký
etnickou	etnický	k2eAgFnSc7d1	etnická
skupinou	skupina	k1gFnSc7	skupina
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Britů	Brit	k1gMnPc2	Brit
<g/>
)	)	kIx)	)
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
následovaní	následovaný	k2eAgMnPc1d1	následovaný
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc7	Ital
<g/>
,	,	kIx,	,
Skandinávci	Skandinávec	k1gMnPc7	Skandinávec
a	a	k8xC	a
východoevropany	východoevropan	k1gMnPc7	východoevropan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
početná	početný	k2eAgFnSc1d1	početná
skupina	skupina	k1gFnSc1	skupina
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
Indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
jako	jako	k8xC	jako
První	první	k4xOgInPc4	první
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
komunita	komunita	k1gFnSc1	komunita
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
provincii	provincie	k1gFnSc6	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
incidentům	incident	k1gInPc3	incident
s	s	k7c7	s
rasovým	rasový	k2eAgInSc7d1	rasový
podtextem	podtext	k1gInSc7	podtext
<g/>
,	,	kIx,	,
rasové	rasový	k2eAgNnSc4d1	rasové
a	a	k8xC	a
etnické	etnický	k2eAgNnSc4d1	etnické
soužití	soužití	k1gNnSc4	soužití
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
relativně	relativně	k6eAd1	relativně
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
smíšených	smíšený	k2eAgNnPc2d1	smíšené
manželství	manželství	k1gNnPc2	manželství
<g/>
,	,	kIx,	,
smíšené	smíšený	k2eAgInPc1d1	smíšený
páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
oba	dva	k4xCgInPc1	dva
tradiční	tradiční	k2eAgInPc1d1	tradiční
čínské	čínský	k2eAgInPc1d1	čínský
svátky	svátek	k1gInPc1	svátek
Duanwu	Duanwus	k1gInSc2	Duanwus
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Festival	festival	k1gInSc1	festival
dračích	dračí	k2eAgInPc2d1	dračí
člunů	člun	k1gInPc2	člun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
dni	den	k1gInSc6	den
pátého	pátý	k4xOgInSc2	pátý
měsíce	měsíc	k1gInSc2	měsíc
čínského	čínský	k2eAgInSc2d1	čínský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
a	a	k8xC	a
čínský	čínský	k2eAgInSc1d1	čínský
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
prvního	první	k4xOgInSc2	první
měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
lunárním	lunární	k2eAgInSc6d1	lunární
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
,	,	kIx,	,
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
všechny	všechen	k3xTgFnPc4	všechen
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Architekti	architekt	k1gMnPc1	architekt
se	se	k3xPyFc4	se
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
podpořit	podpořit	k5eAaPmF	podpořit
stavbu	stavba	k1gFnSc4	stavba
vyšších	vysoký	k2eAgInPc2d2	vyšší
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
ve	v	k7c4	v
West	West	k1gInSc4	West
Endu	Endus	k1gInSc2	Endus
a	a	k8xC	a
v	v	k7c6	v
Downtownu	Downtown	k1gInSc6	Downtown
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
čehož	což	k3yQnSc2	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
kompaktní	kompaktní	k2eAgNnSc1d1	kompaktní
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přizpůsobené	přizpůsobený	k2eAgFnSc3d1	přizpůsobená
veřejné	veřejný	k2eAgFnSc3d1	veřejná
dopravě	doprava	k1gFnSc3	doprava
<g/>
,	,	kIx,	,
chodcům	chodec	k1gMnPc3	chodec
a	a	k8xC	a
cyklistům	cyklista	k1gMnPc3	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
se	se	k3xPyFc4	se
v	v	k7c6	v
Downtownu	Downtown	k1gInSc6	Downtown
podle	podle	k7c2	podle
sčítaní	sčítaný	k2eAgMnPc1d1	sčítaný
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
121	[number]	k4	121
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
hektar	hektar	k1gInSc4	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
zvyšování	zvyšování	k1gNnSc2	zvyšování
hustoty	hustota	k1gFnSc2	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zamezit	zamezit	k5eAaPmF	zamezit
nekontrolovanému	kontrolovaný	k2eNgNnSc3d1	nekontrolované
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
poloze	poloha	k1gFnSc3	poloha
města	město	k1gNnSc2	město
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgNnSc7d1	poslední
městem	město	k1gNnSc7	město
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
transkontinentální	transkontinentální	k2eAgFnSc2d1	transkontinentální
dálnice	dálnice	k1gFnSc2	dálnice
a	a	k8xC	a
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Vancouver	Vancouver	k1gInSc1	Vancouver
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
centra	centrum	k1gNnPc4	centrum
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
Port	port	k1gInSc1	port
of	of	k?	of
Vancouver	Vancouver	k1gInSc1	Vancouver
(	(	kIx(	(
<g/>
kód	kód	k1gInSc1	kód
CXH	CXH	kA	CXH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
přístavů	přístav	k1gInPc2	přístav
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
přepraví	přepravit	k5eAaPmIp3nS	přepravit
ročně	ročně	k6eAd1	ročně
do	do	k7c2	do
90	[number]	k4	90
zemí	zem	k1gFnPc2	zem
výrobky	výrobek	k1gInPc1	výrobek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
převyšující	převyšující	k2eAgFnSc6d1	převyšující
43	[number]	k4	43
miliard	miliarda	k4xCgFnPc2	miliarda
Kanadských	kanadský	k2eAgInPc2d1	kanadský
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
CAD	CAD	kA	CAD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
pro	pro	k7c4	pro
kanadský	kanadský	k2eAgInSc4d1	kanadský
hrubý	hrubý	k2eAgInSc4d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
a	a	k8xC	a
8,9	[number]	k4	8,9
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
exportu	export	k1gInSc6	export
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
Vancouver	Vancouver	k1gInSc1	Vancouver
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
kanadského	kanadský	k2eAgInSc2d1	kanadský
dřevozpracujícího	dřevozpracující	k2eAgInSc2d1	dřevozpracující
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
hornictví	hornictví	k1gNnSc2	hornictví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
poslední	poslední	k2eAgFnSc4d1	poslední
dekádu	dekáda	k1gFnSc4	dekáda
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
stal	stát	k5eAaPmAgInS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
centrem	centr	k1gInSc7	centr
softwarového	softwarový	k2eAgInSc2d1	softwarový
<g/>
,	,	kIx,	,
filmového	filmový	k2eAgInSc2d1	filmový
a	a	k8xC	a
biotechnologického	biotechnologický	k2eAgInSc2d1	biotechnologický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
turistickým	turistický	k2eAgFnPc3d1	turistická
destinacím	destinace	k1gFnPc3	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
často	často	k6eAd1	často
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
Stanley	Stanlea	k1gFnPc1	Stanlea
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Park	park	k1gInSc1	park
Královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnPc1	zahrada
a	a	k8xC	a
hluboké	hluboký	k2eAgInPc1d1	hluboký
lesy	les	k1gInPc1	les
vyzdvihující	vyzdvihující	k2eAgInPc1d1	vyzdvihující
se	se	k3xPyFc4	se
po	po	k7c6	po
úpatí	úpatí	k1gNnSc6	úpatí
hor.	hor.	k?	hor.
Značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
nábřeží	nábřeží	k1gNnPc2	nábřeží
a	a	k8xC	a
vrcholků	vrcholek	k1gInPc2	vrcholek
hor	hora	k1gFnPc2	hora
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
multikulturním	multikulturní	k2eAgInSc7d1	multikulturní
a	a	k8xC	a
multietnickým	multietnický	k2eAgInSc7d1	multietnický
charakterem	charakter	k1gInSc7	charakter
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
charakter	charakter	k1gInSc4	charakter
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Výletní	výletní	k2eAgFnPc1d1	výletní
lodě	loď	k1gFnPc1	loď
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
přepraví	přepravit	k5eAaPmIp3nS	přepravit
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
pasažérů	pasažér	k1gMnPc2	pasažér
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
cestují	cestovat	k5eAaImIp3nP	cestovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
do	do	k7c2	do
státu	stát	k1gInSc2	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
města	město	k1gNnSc2	město
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
svoji	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
je	být	k5eAaImIp3nS	být
drahé	drahý	k2eAgNnSc4d1	drahé
město	město	k1gNnSc4	město
s	s	k7c7	s
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
cenami	cena	k1gFnPc7	cena
bytů	byt	k1gInPc2	byt
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
průměrného	průměrný	k2eAgInSc2d1	průměrný
dvojpodlažního	dvojpodlažní	k2eAgInSc2d1	dvojpodlažní
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
988	[number]	k4	988
500	[number]	k4	500
CAD	CAD	kA	CAD
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
489	[number]	k4	489
889	[number]	k4	889
CAD	CAD	kA	CAD
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
411	[number]	k4	411
456	[number]	k4	456
CAD	CAD	kA	CAD
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
nejdražším	drahý	k2eAgNnSc7d3	nejdražší
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
)	)	kIx)	)
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
přijala	přijmout	k5eAaPmAgFnS	přijmout
několik	několik	k4yIc4	několik
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
cen	cena	k1gFnPc2	cena
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
podporu	podpora	k1gFnSc4	podpora
stavby	stavba	k1gFnSc2	stavba
družstevních	družstevní	k2eAgInPc2d1	družstevní
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
upřednostňování	upřednostňování	k1gNnSc4	upřednostňování
stavby	stavba	k1gFnSc2	stavba
výškových	výškový	k2eAgFnPc2d1	výšková
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
neklesající	klesající	k2eNgInSc1d1	neklesající
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
družstevních	družstevní	k2eAgFnPc2d1	družstevní
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
začal	začít	k5eAaPmAgInS	začít
koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Financovaný	financovaný	k2eAgInSc1d1	financovaný
byl	být	k5eAaImAgInS	být
převážně	převážně	k6eAd1	převážně
velkým	velký	k2eAgInSc7d1	velký
přílivem	příliv	k1gInSc7	příliv
kapitálu	kapitál	k1gInSc2	kapitál
emigrantů	emigrant	k1gMnPc2	emigrant
z	z	k7c2	z
Hong	Honga	k1gFnPc2	Honga
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
připojením	připojení	k1gNnSc7	připojení
k	k	k7c3	k
Číně	Čína	k1gFnSc3	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Výškové	výškový	k2eAgFnPc1d1	výšková
obytné	obytný	k2eAgFnPc1d1	obytná
budovy	budova	k1gFnPc1	budova
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
období	období	k1gNnSc2	období
dnes	dnes	k6eAd1	dnes
dominují	dominovat	k5eAaImIp3nP	dominovat
centru	centr	k1gInSc3	centr
města	město	k1gNnSc2	město
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Yaletown	Yaletowna	k1gFnPc2	Yaletowna
a	a	k8xC	a
Coal	Coala	k1gFnPc2	Coala
Harbour	Harboura	k1gFnPc2	Harboura
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
stanic	stanice	k1gFnPc2	stanice
nadzemního	nadzemní	k2eAgNnSc2d1	nadzemní
metra	metro	k1gNnSc2	metro
SkyTrain	SkyTraina	k1gFnPc2	SkyTraina
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
na	na	k7c6	na
prvních	první	k4xOgInPc6	první
pěti	pět	k4xCc6	pět
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
měst	město	k1gNnPc2	město
podle	podle	k7c2	podle
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
a	a	k8xC	a
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
života	život	k1gInSc2	život
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
provedeném	provedený	k2eAgInSc6d1	provedený
Economist	Economist	k1gInSc1	Economist
Intelligence	Intelligence	k1gFnSc2	Intelligence
Unit	Unita	k1gFnPc2	Unita
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
výzkum	výzkum	k1gInSc4	výzkum
prováděný	prováděný	k2eAgMnSc1d1	prováděný
Mercer	Mercer	k1gMnSc1	Mercer
Human	Human	k1gMnSc1	Human
Resource	Resourka	k1gFnSc6	Resourka
Consulting	Consulting	k1gInSc1	Consulting
zařadil	zařadit	k5eAaPmAgInS	zařadit
město	město	k1gNnSc4	město
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
byl	být	k5eAaImAgInS	být
Vancouver	Vancouver	k1gInSc1	Vancouver
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Salzburkem	Salzburk	k1gInSc7	Salzburk
a	a	k8xC	a
Oslo	Oslo	k1gNnSc1	Oslo
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
žebříčku	žebříček	k1gInSc2	žebříček
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třídě	třída	k1gFnSc6	třída
také	také	k9	také
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
hostil	hostit	k5eAaImAgInS	hostit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
místní	místní	k2eAgInSc4d1	místní
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
další	další	k2eAgFnSc1d1	další
významná	významný	k2eAgFnSc1d1	významná
světová	světový	k2eAgFnSc1d1	světová
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
Světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
-	-	kIx~	-
Expo	Expo	k1gNnSc1	Expo
<g/>
.	.	kIx.	.
</s>
<s>
Navštívilo	navštívit	k5eAaPmAgNnS	navštívit
ho	on	k3xPp3gNnSc2	on
20	[number]	k4	20
111	[number]	k4	111
578	[number]	k4	578
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Expo	Expo	k1gNnSc4	Expo
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
staveb	stavba	k1gFnPc2	stavba
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
veřejný	veřejný	k2eAgInSc1d1	veřejný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
SkyTrain	SkyTraina	k1gFnPc2	SkyTraina
<g/>
,	,	kIx,	,
Plaza	plaz	k1gMnSc2	plaz
of	of	k?	of
Nations	Nations	k1gInSc1	Nations
<g/>
,	,	kIx,	,
Science	Science	k1gFnSc1	Science
World	World	k1gMnSc1	World
a	a	k8xC	a
Canada	Canada	k1gFnSc1	Canada
Place	plac	k1gInSc6	plac
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
starostů	starosta	k1gMnPc2	starosta
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
spravuje	spravovat	k5eAaImIp3nS	spravovat
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
Vancouveru	Vancouver	k1gInSc2	Vancouver
(	(	kIx(	(
<g/>
Vancouver	Vancouver	k1gInSc1	Vancouver
City	city	k1gNnSc1	city
Council	Council	k1gInSc1	Council
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
radě	rada	k1gFnSc6	rada
zasedá	zasedat	k5eAaImIp3nS	zasedat
deset	deset	k4xCc1	deset
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
s	s	k7c7	s
devítičlenným	devítičlenný	k2eAgInSc7d1	devítičlenný
školním	školní	k2eAgInSc7d1	školní
výborem	výbor	k1gInSc7	výbor
a	a	k8xC	a
sedmičlenným	sedmičlenný	k2eAgInSc7d1	sedmičlenný
výborem	výbor	k1gInSc7	výbor
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
výbory	výbor	k1gInPc1	výbor
a	a	k8xC	a
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
jsou	být	k5eAaImIp3nP	být
voleny	volen	k2eAgInPc1d1	volen
většinovým	většinový	k2eAgInSc7d1	většinový
volebním	volební	k2eAgInSc7d1	volební
systémem	systém	k1gInSc7	systém
na	na	k7c4	na
tříleté	tříletý	k2eAgNnSc4d1	tříleté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
west	west	k2eAgInSc1d1	west
side	side	k1gInSc1	side
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgFnSc1d2	bohatší
oproti	oproti	k7c3	oproti
východní	východní	k2eAgFnSc3d1	východní
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
většinou	většina	k1gFnSc7	většina
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
a	a	k8xC	a
liberální	liberální	k2eAgFnSc2d1	liberální
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
east	east	k1gInSc1	east
side	sid	k1gFnSc2	sid
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
strany	strana	k1gFnPc4	strana
z	z	k7c2	z
levicově	levicově	k6eAd1	levicově
orientovaného	orientovaný	k2eAgNnSc2d1	orientované
politického	politický	k2eAgNnSc2d1	politické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
samosprávních	samosprávní	k2eAgFnPc2d1	samosprávní
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
ve	v	k7c6	v
federálních	federální	k2eAgFnPc6d1	federální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
polarizaci	polarizace	k1gFnSc3	polarizace
politických	politický	k2eAgInPc2d1	politický
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
důležitých	důležitý	k2eAgFnPc2d1	důležitá
věcí	věc	k1gFnPc2	věc
umějí	umět	k5eAaImIp3nP	umět
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
strany	strana	k1gFnPc1	strana
najít	najít	k5eAaPmF	najít
politický	politický	k2eAgInSc1d1	politický
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
městských	městský	k2eAgInPc2d1	městský
parků	park	k1gInPc2	park
a	a	k8xC	a
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
městské	městský	k2eAgFnSc2d1	městská
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
jako	jako	k8xC	jako
protiváha	protiváha	k1gFnSc1	protiváha
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
veřejný	veřejný	k2eAgInSc4d1	veřejný
rozvoj	rozvoj	k1gInSc4	rozvoj
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgNnPc7d1	hlavní
tématy	téma	k1gNnPc7	téma
mající	mající	k2eAgInPc1d1	mající
širokou	široký	k2eAgFnSc4d1	široká
podporu	podpora	k1gFnSc4	podpora
napříč	napříč	k7c7	napříč
politickým	politický	k2eAgNnSc7d1	politické
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
volbách	volba	k1gFnPc6	volba
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
města	město	k1gNnSc2	město
Sam	Sam	k1gMnSc1	Sam
Sullivan	Sullivan	k1gMnSc1	Sullivan
<g/>
,	,	kIx,	,
kandidát	kandidát	k1gMnSc1	kandidát
centristické	centristický	k2eAgFnSc2d1	centristická
NPA	NPA	kA	NPA
(	(	kIx(	(
<g/>
Non-Partisan	Non-Partisan	k1gInSc1	Non-Partisan
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
porazil	porazit	k5eAaPmAgMnS	porazit
Jima	Jimus	k1gMnSc4	Jimus
Greena	Green	k1gMnSc4	Green
z	z	k7c2	z
centristické	centristický	k2eAgFnSc2d1	centristická
strany	strana	k1gFnSc2	strana
VVA	VVA	kA	VVA
(	(	kIx(	(
<g/>
Vision	vision	k1gInSc1	vision
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
zasedá	zasedat	k5eAaImIp3nS	zasedat
pět	pět	k4xCc4	pět
členů	člen	k1gInPc2	člen
za	za	k7c4	za
NPA	NPA	kA	NPA
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc1	čtyři
za	za	k7c2	za
VVN	VVN	kA	VVN
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
za	za	k7c4	za
levicovou	levicový	k2eAgFnSc4d1	levicová
COPE	cop	k1gInSc5	cop
(	(	kIx(	(
<g/>
Coalition	Coalition	k1gInSc1	Coalition
of	of	k?	of
Progressive	Progressiev	k1gFnSc2	Progressiev
Electors	Electorsa	k1gFnPc2	Electorsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NPA	NPA	kA	NPA
má	mít	k5eAaImIp3nS	mít
taktéž	taktéž	k?	taktéž
šest	šest	k4xCc4	šest
křesel	křeslo	k1gNnPc2	křeslo
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
výboru	výbor	k1gInSc6	výbor
a	a	k8xC	a
pět	pět	k4xCc1	pět
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
křesel	křeslo	k1gNnPc2	křeslo
obsadila	obsadit	k5eAaPmAgFnS	obsadit
COPE	cop	k1gInSc5	cop
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
starosta	starosta	k1gMnSc1	starosta
Larry	Larra	k1gFnSc2	Larra
Campbell	Campbell	k1gMnSc1	Campbell
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nekandidovat	kandidovat	k5eNaImF	kandidovat
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
Kanadského	kanadský	k2eAgInSc2d1	kanadský
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
legislativním	legislativní	k2eAgNnSc6d1	legislativní
zastoupení	zastoupení	k1gNnSc6	zastoupení
provincie	provincie	k1gFnSc2	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
Vancouver	Vancouver	k1gInSc4	Vancouver
deset	deset	k4xCc1	deset
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
získala	získat	k5eAaPmAgFnS	získat
liberální	liberální	k2eAgFnSc1d1	liberální
BC	BC	kA	BC
Liberal	Liberal	k1gFnSc1	Liberal
Party	party	k1gFnSc1	party
a	a	k8xC	a
BC	BC	kA	BC
New	New	k1gFnSc1	New
Democratic	Democratice	k1gFnPc2	Democratice
Party	parta	k1gFnSc2	parta
každá	každý	k3xTgFnSc1	každý
pět	pět	k4xCc4	pět
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadské	kanadský	k2eAgFnSc6d1	kanadská
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
je	být	k5eAaImIp3nS	být
Vancouver	Vancouver	k1gMnSc1	Vancouver
reprezentovaný	reprezentovaný	k2eAgMnSc1d1	reprezentovaný
pěti	pět	k4xCc2	pět
členy	člen	k1gMnPc4	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
federálních	federální	k2eAgFnPc6d1	federální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
získala	získat	k5eAaPmAgFnS	získat
Liberal	Liberal	k1gFnSc1	Liberal
Party	party	k1gFnSc1	party
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
čtyři	čtyři	k4xCgNnPc4	čtyři
křesla	křeslo	k1gNnPc4	křeslo
a	a	k8xC	a
New	New	k1gFnPc4	New
Democratic	Democratice	k1gFnPc2	Democratice
Party	party	k1gFnSc6	party
jedno	jeden	k4xCgNnSc4	jeden
křeslo	křeslo	k1gNnSc4	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byli	být	k5eAaImAgMnP	být
znovuzvolení	znovuzvolený	k2eAgMnPc1d1	znovuzvolený
ti	ten	k3xDgMnPc1	ten
samí	samý	k3xTgMnPc1	samý
zástupci	zástupce	k1gMnPc1	zástupce
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeden	jeden	k4xCgInSc1	jeden
člen	člen	k1gInSc1	člen
liberálů	liberál	k1gMnPc2	liberál
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přestoupit	přestoupit	k5eAaPmF	přestoupit
ke	k	k7c3	k
konzervativcům	konzervativec	k1gMnPc3	konzervativec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nyní	nyní	k6eAd1	nyní
mají	mít	k5eAaImIp3nP	mít
liberálové	liberál	k1gMnPc1	liberál
tři	tři	k4xCgNnPc4	tři
křesla	křeslo	k1gNnPc4	křeslo
a	a	k8xC	a
konzervativci	konzervativec	k1gMnPc7	konzervativec
a	a	k8xC	a
NDP	NDP	kA	NDP
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Bývalého	bývalý	k2eAgMnSc4d1	bývalý
starostu	starosta	k1gMnSc4	starosta
Larryho	Larry	k1gMnSc4	Larry
Cabpbella	Cabpbell	k1gMnSc4	Cabpbell
lidé	člověk	k1gMnPc1	člověk
zvolili	zvolit	k5eAaPmAgMnP	zvolit
z	z	k7c2	z
části	část	k1gFnSc2	část
také	také	k9	také
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
ochotu	ochota	k1gFnSc4	ochota
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
alternativní	alternativní	k2eAgInPc4d1	alternativní
způsoby	způsob	k1gInPc4	způsob
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
drogám	droga	k1gFnPc3	droga
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
si	se	k3xPyFc3	se
osvojilo	osvojit	k5eAaPmAgNnS	osvojit
čtyřbodovou	čtyřbodový	k2eAgFnSc4d1	čtyřbodová
protidrogovou	protidrogový	k2eAgFnSc4d1	protidrogová
strategii	strategie	k1gFnSc4	strategie
kombinující	kombinující	k2eAgNnSc1d1	kombinující
snížení	snížení	k1gNnSc1	snížení
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
rizika	riziko	k1gNnSc2	riziko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výměny	výměna	k1gFnSc2	výměna
jehel	jehla	k1gFnPc2	jehla
<g/>
)	)	kIx)	)
s	s	k7c7	s
léčbou	léčba	k1gFnSc7	léčba
<g/>
,	,	kIx,	,
postihem	postih	k1gInSc7	postih
a	a	k8xC	a
prevencí	prevence	k1gFnSc7	prevence
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strategie	strategie	k1gFnSc1	strategie
byla	být	k5eAaImAgFnS	být
odezvou	odezva	k1gFnSc7	odezva
radnice	radnice	k1gFnSc2	radnice
na	na	k7c4	na
narůstající	narůstající	k2eAgInSc4d1	narůstající
počet	počet	k1gInSc4	počet
HIV	HIV	kA	HIV
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
a	a	k8xC	a
nakažených	nakažený	k2eAgMnPc2d1	nakažený
uživatelů	uživatel	k1gMnPc2	uživatel
drog	droga	k1gFnPc2	droga
Hepatitidou	hepatitida	k1gFnSc7	hepatitida
typu	typ	k1gInSc2	typ
C	C	kA	C
ve	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Downtown	Downtowna	k1gFnPc2	Downtowna
Eastside	Eastsid	k1gInSc5	Eastsid
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
čtvrť	čtvrť	k1gFnSc4	čtvrť
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
značná	značný	k2eAgFnSc1d1	značná
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
,	,	kIx,	,
prostituce	prostituce	k1gFnSc1	prostituce
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nárůst	nárůst	k1gInSc4	nárůst
epidemie	epidemie	k1gFnSc2	epidemie
AIDS	AIDS	kA	AIDS
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
oblasti	oblast	k1gFnSc2	oblast
Lower	Lower	k1gInSc1	Lower
Mainland	Mainland	k1gInSc1	Mainland
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Divize	divize	k1gFnSc2	divize
E	E	kA	E
Královské	královský	k2eAgFnSc2d1	královská
kanadské	kanadský	k2eAgFnSc2d1	kanadská
jízdní	jízdní	k2eAgFnSc2d1	jízdní
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
RCMP	RCMP	kA	RCMP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořádek	pořádek	k1gInSc4	pořádek
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vlastní	vlastní	k2eAgFnSc1d1	vlastní
městská	městský	k2eAgFnSc1d1	městská
policie	policie	k1gFnSc1	policie
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Vancouver	Vancouver	k1gInSc4	Vancouver
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Westminster	Westminstra	k1gFnPc2	Westminstra
a	a	k8xC	a
Port	porta	k1gFnPc2	porta
Moody	Mooda	k1gFnSc2	Mooda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
města	město	k1gNnSc2	město
Vancouver	Vancouvero	k1gNnPc2	Vancouvero
má	mít	k5eAaImIp3nS	mít
1174	[number]	k4	1174
členů	člen	k1gInPc2	člen
a	a	k8xC	a
rozpočet	rozpočet	k1gInSc1	rozpočet
bezmála	bezmála	k6eAd1	bezmála
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
CAD	CAD	kA	CAD
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
policii	policie	k1gFnSc3	policie
patří	patřit	k5eAaImIp3nS	patřit
jednotka	jednotka	k1gFnSc1	jednotka
na	na	k7c6	na
kolech	kolo	k1gNnPc6	kolo
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
psovodů	psovod	k1gMnPc2	psovod
<g/>
,	,	kIx,	,
přístavní	přístavní	k2eAgFnSc1d1	přístavní
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
jízdní	jízdní	k2eAgFnSc1d1	jízdní
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
Stanley	Stanlea	k1gFnSc2	Stanlea
Parku	park	k1gInSc2	park
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
i	i	k9	i
ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Downtown	Downtowna	k1gFnPc2	Downtowna
Eastside	Eastsid	k1gInSc5	Eastsid
a	a	k8xC	a
West	West	k1gMnSc1	West
End	End	k1gMnSc1	End
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dopravní	dopravní	k2eAgFnSc1d1	dopravní
policie	policie	k1gFnSc1	policie
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Greater	Greater	k1gMnSc1	Greater
Vancouver	Vancouver	k1gMnSc1	Vancouver
Transportation	Transportation	k1gInSc4	Transportation
Authority	Authorita	k1gFnSc2	Authorita
Police	police	k1gFnSc2	police
Service	Service	k1gFnSc2	Service
(	(	kIx(	(
<g/>
GVTAPS	GVTAPS	kA	GVTAPS
<g/>
)	)	kIx)	)
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
policejní	policejní	k2eAgFnSc7d1	policejní
pravomocí	pravomoc	k1gFnSc7	pravomoc
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
i	i	k9	i
protiteroristická	protiteroristický	k2eAgFnSc1d1	protiteroristická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
trestních	trestní	k2eAgInPc2d1	trestní
činů	čin	k1gInPc2	čin
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Kanadě	Kanada	k1gFnSc6	Kanada
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
značně	značně	k6eAd1	značně
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
páchání	páchání	k1gNnSc4	páchání
drobných	drobný	k2eAgInPc2d1	drobný
majetkových	majetkový	k2eAgInPc2d1	majetkový
trestních	trestní	k2eAgInPc2d1	trestní
činů	čin	k1gInPc2	čin
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
problematických	problematický	k2eAgFnPc6d1	problematická
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejvíce	nejvíce	k6eAd1	nejvíce
postižená	postižený	k2eAgNnPc4d1	postižené
severoamerická	severoamerický	k2eAgNnPc4d1	severoamerické
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
a	a	k8xC	a
směřoval	směřovat	k5eAaImAgInS	směřovat
z	z	k7c2	z
mostu	most	k1gInSc2	most
Granville	Granville	k1gNnSc2	Granville
Street	Streeta	k1gFnPc2	Streeta
na	na	k7c4	na
Westminster	Westminster	k1gInSc4	Westminster
Avenue	avenue	k1gFnSc2	avenue
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgInSc1d1	nynější
Main	Main	k1gInSc1	Main
Street	Streeta	k1gFnPc2	Streeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
společnost	společnost	k1gFnSc1	společnost
Westminster	Westminster	k1gMnSc1	Westminster
and	and	k?	and
Vancouver	Vancouver	k1gMnSc1	Vancouver
Tramway	Tramwa	k2eAgInPc4d1	Tramwa
Company	Compan	k1gInPc4	Compan
provozovat	provozovat	k5eAaImF	provozovat
první	první	k4xOgFnSc4	první
kanadskou	kanadský	k2eAgFnSc4d1	kanadská
meziměstskou	meziměstský	k2eAgFnSc4d1	meziměstská
linku	linka	k1gFnSc4	linka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
rozvoji	rozvoj	k1gInSc3	rozvoj
okrajových	okrajový	k2eAgFnPc2d1	okrajová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
fungovala	fungovat	k5eAaImAgFnS	fungovat
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
meziměstská	meziměstský	k2eAgFnSc1d1	meziměstská
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
později	pozdě	k6eAd2	pozdě
nahradily	nahradit	k5eAaPmAgInP	nahradit
městské	městský	k2eAgInPc1d1	městský
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
plánu	plán	k1gInSc2	plán
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zakázat	zakázat	k5eAaPmF	zakázat
stavbu	stavba	k1gFnSc4	stavba
dalších	další	k2eAgFnPc2d1	další
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
dálnice	dálnice	k1gFnSc1	dálnice
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
obvodě	obvod	k1gInSc6	obvod
je	být	k5eAaImIp3nS	být
Highway	Highwaa	k1gMnSc2	Highwaa
one	one	k?	one
procházející	procházející	k2eAgInSc4d1	procházející
přes	přes	k7c4	přes
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
TransLink	TransLink	k1gInSc1	TransLink
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc1	síť
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
za	za	k7c4	za
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
a	a	k8xC	a
trolejbusovou	trolejbusový	k2eAgFnSc4d1	trolejbusová
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
rychlou	rychlý	k2eAgFnSc4d1	rychlá
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
síť	síť	k1gFnSc4	síť
B-Line	B-Lin	k1gInSc5	B-Lin
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc4d1	osobní
trajektovou	trajektový	k2eAgFnSc4d1	trajektová
dopravu	doprava	k1gFnSc4	doprava
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
SeaBus	SeaBus	k1gInSc4	SeaBus
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
železnici	železnice	k1gFnSc4	železnice
West	West	k1gInSc1	West
Coast	Coast	k1gMnSc1	Coast
Express	express	k1gInSc1	express
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
Expo	Expo	k1gNnSc1	Expo
line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
Millennium	millennium	k1gNnSc1	millennium
line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
nově	nově	k6eAd1	nově
Canada	Canada	k1gFnSc1	Canada
line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
otevřena	otevřen	k2eAgFnSc1d1	otevřena
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
linka	linka	k1gFnSc1	linka
spojuje	spojovat	k5eAaImIp3nS	spojovat
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
nedaleké	daleký	k2eNgNnSc4d1	nedaleké
město	město	k1gNnSc4	město
Richmond	Richmonda	k1gFnPc2	Richmonda
s	s	k7c7	s
downtownem	downtown	k1gInSc7	downtown
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
SeaBus	SeaBus	k1gInSc4	SeaBus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trasy	trasa	k1gFnSc2	trasa
lehkého	lehký	k2eAgNnSc2d1	lehké
metra	metro	k1gNnSc2	metro
SkyTrain	SkyTraina	k1gFnPc2	SkyTraina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Pacific	Pacific	k1gMnSc1	Pacific
Central	Central	k1gMnSc1	Central
Station	station	k1gInSc4	station
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
trasa	trasa	k1gFnSc1	trasa
kanadské	kanadský	k2eAgFnSc2d1	kanadská
železnice	železnice	k1gFnSc2	železnice
VIA	via	k7c4	via
Rail	Rail	k1gInSc4	Rail
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
kanadsko-americká	kanadskomerický	k2eAgFnSc1d1	kanadsko-americká
železnice	železnice	k1gFnSc1	železnice
Amtrak	Amtrak	k1gMnSc1	Amtrak
Cascades	Cascades	k1gMnSc1	Cascades
a	a	k8xC	a
do	do	k7c2	do
pohoří	pohoří	k1gNnSc2	pohoří
Rocky	rock	k1gInPc4	rock
Mountains	Mountainsa	k1gFnPc2	Mountainsa
směřuje	směřovat	k5eAaImIp3nS	směřovat
turisticko-naučná	turistickoaučný	k2eAgFnSc1d1	turisticko-naučná
trasa	trasa	k1gFnSc1	trasa
kanadské	kanadský	k2eAgFnSc2d1	kanadská
společnosti	společnost	k1gFnSc2	společnost
Rocky	rock	k1gInPc1	rock
Mountaineer	Mountainera	k1gFnPc2	Mountainera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Vancouveru	Vancouver	k1gInSc2	Vancouver
leží	ležet	k5eAaImIp3nS	ležet
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Vancouver	Vancouver	k1gInSc1	Vancouver
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
kód	kód	k1gInSc1	kód
YVR	YVR	kA	YVR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
druhé	druhý	k4xOgNnSc1	druhý
nejrušnější	rušný	k2eAgNnSc1d3	nejrušnější
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
lety	let	k1gInPc4	let
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Richmond	Richmonda	k1gFnPc2	Richmonda
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sea	Sea	k1gFnSc1	Sea
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
sídlí	sídlet	k5eAaImIp3nS	sídlet
HeliJet	HeliJet	k1gInSc1	HeliJet
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
zabezpečující	zabezpečující	k2eAgFnSc4d1	zabezpečující
přepravu	přeprava	k1gFnSc4	přeprava
vrtulníky	vrtulník	k1gInPc1	vrtulník
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
letištěm	letiště	k1gNnSc7	letiště
a	a	k8xC	a
přístavy	přístav	k1gInPc7	přístav
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
a	a	k8xC	a
Victorii	Victorie	k1gFnSc6	Victorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sa	sa	k?	sa
nachází	nacházet	k5eAaImIp3nS	nacházet
dva	dva	k4xCgInPc4	dva
terminály	terminál	k1gInPc4	terminál
pro	pro	k7c4	pro
trajekty	trajekt	k1gInPc4	trajekt
spoločnosti	spoločnost	k1gFnSc2	spoločnost
BC	BC	kA	BC
Ferries	Ferries	k1gInSc1	Ferries
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Horseshoe	Horsesho	k1gMnSc2	Horsesho
Bay	Bay	k1gMnSc2	Bay
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
West	West	k1gMnSc1	West
Vancouver	Vancouver	k1gMnSc1	Vancouver
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Delta	delta	k1gFnSc1	delta
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Tsawwassen	Tsawwassna	k1gFnPc2	Tsawwassna
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
provincie	provincie	k1gFnSc2	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
hodně	hodně	k6eAd1	hodně
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
díky	díky	k7c3	díky
částečným	částečný	k2eAgFnPc3d1	částečná
provinčním	provinční	k2eAgFnPc3d1	provinční
dotacím	dotace	k1gFnPc3	dotace
-	-	kIx~	-
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
církevní	církevní	k2eAgFnPc1d1	církevní
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
platit	platit	k5eAaImF	platit
školné	školné	k1gNnSc4	školné
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
školy	škola	k1gFnPc1	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
frankofonní	frankofonní	k2eAgFnSc3d1	frankofonní
školní	školní	k2eAgFnSc3d1	školní
oblasti	oblast	k1gFnSc3	oblast
Conseil	Conseila	k1gFnPc2	Conseila
scolaire	scolair	k1gInSc5	scolair
francophone	francophon	k1gInSc5	francophon
de	de	k?	de
la	la	k1gNnSc7	la
Colombie-Britannique	Colombie-Britanniqu	k1gFnSc2	Colombie-Britanniqu
(	(	kIx(	(
<g/>
CSF	CSF	kA	CSF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Vancouveru	Vancouver	k1gInSc2	Vancouver
stojí	stát	k5eAaImIp3nP	stát
dvě	dva	k4xCgFnPc1	dva
největší	veliký	k2eAgFnPc1d3	veliký
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Lower	Lowra	k1gFnPc2	Lowra
Mainland	Mainland	k1gInSc1	Mainland
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
-	-	kIx~	-
University	universita	k1gFnSc2	universita
of	of	k?	of
British	British	k1gInSc1	British
Columbia	Columbia	k1gFnSc1	Columbia
(	(	kIx(	(
<g/>
UBC	UBC	kA	UBC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
Simona	Simona	k1gFnSc1	Simona
Frasera	Frasera	k1gFnSc1	Frasera
-	-	kIx~	-
Simon	Simon	k1gMnSc1	Simon
Fraser	Fraser	k1gMnSc1	Fraser
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
SFU	SFU	kA	SFU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UBC	UBC	kA	UBC
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
Point	pointa	k1gFnPc2	pointa
Grey	Grea	k1gFnSc2	Grea
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
Pacific	Pacifice	k1gFnPc2	Pacifice
Spirit	Spirit	k1gInSc1	Spirit
Regional	Regional	k1gMnSc1	Regional
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
patřící	patřící	k2eAgFnSc6d1	patřící
k	k	k7c3	k
univerzitě	univerzita	k1gFnSc3	univerzita
-	-	kIx~	-
University	universita	k1gFnSc2	universita
Endowment	Endowment	k1gInSc1	Endowment
Lands	Lands	k1gInSc1	Lands
(	(	kIx(	(
<g/>
UEL	UEL	kA	UEL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tvoří	tvořit	k5eAaImIp3nS	tvořit
zelený	zelený	k2eAgInSc1d1	zelený
pás	pás	k1gInSc1	pás
mezi	mezi	k7c7	mezi
univerzitou	univerzita	k1gFnSc7	univerzita
a	a	k8xC	a
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
univerzity	univerzita	k1gFnSc2	univerzita
spadá	spadat	k5eAaImIp3nS	spadat
k	k	k7c3	k
metropolitní	metropolitní	k2eAgFnSc3d1	metropolitní
oblasti	oblast	k1gFnSc3	oblast
regionálního	regionální	k2eAgInSc2d1	regionální
okresu	okres	k1gInSc2	okres
Velký	velký	k2eAgInSc1d1	velký
Vancouver	Vancouver	k1gInSc1	Vancouver
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Electoral	Electoral	k1gMnSc2	Electoral
Area	Ares	k1gMnSc2	Ares
A	A	kA	A
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jako	jako	k9	jako
nezačleněná	začleněný	k2eNgFnSc1d1	nezačleněná
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
celku	celek	k1gInSc6	celek
Lower	Lower	k1gMnSc1	Lower
Mainland	Mainland	k1gInSc1	Mainland
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
není	být	k5eNaImIp3nS	být
spravován	spravován	k2eAgInSc4d1	spravován
městskou	městský	k2eAgFnSc4d1	městská
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
RCMP	RCMP	kA	RCMP
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
městský	městský	k2eAgInSc1d1	městský
hasičský	hasičský	k2eAgInSc1d1	hasičský
sbor	sbor	k1gInSc1	sbor
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
UEL	UEL	kA	UEL
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
všechna	všechen	k3xTgFnSc1	všechen
pošta	pošta	k1gFnSc1	pošta
směřující	směřující	k2eAgFnSc1d1	směřující
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
adresu	adresa	k1gFnSc4	adresa
uvedený	uvedený	k2eAgInSc4d1	uvedený
Vancouver	Vancouver	k1gInSc4	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
UBC	UBC	kA	UBC
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
tři	tři	k4xCgInPc4	tři
kampusy	kampus	k1gInPc4	kampus
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
Vancouver	Vancouver	k1gMnSc1	Vancouver
General	General	k1gMnSc1	General
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
na	na	k7c4	na
Robson	Robson	k1gInSc4	Robson
Square	square	k1gInSc1	square
a	a	k8xC	a
nejmenší	malý	k2eAgInPc1d3	nejmenší
v	v	k7c4	v
Great	Great	k2eAgInSc4d1	Great
Northern	Northern	k1gInSc4	Northern
Way	Way	k1gFnSc2	Way
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kelowna	Kelown	k1gInSc2	Kelown
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc1	jeden
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
UBC	UBC	kA	UBC
Okanagan	Okanagan	k1gInSc1	Okanagan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultách	fakulta	k1gFnPc6	fakulta
UBC	UBC	kA	UBC
studuje	studovat	k5eAaImIp3nS	studovat
přes	přes	k7c4	přes
45	[number]	k4	45
000	[number]	k4	000
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Simona	Simona	k1gFnSc1	Simona
Frasera	Frasera	k1gFnSc1	Frasera
stojí	stát	k5eAaImIp3nS	stát
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Burnaby	Burnaba	k1gFnSc2	Burnaba
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
Simonovi	Simon	k1gMnSc6	Simon
Fraserovi	Fraser	k1gMnSc6	Fraser
<g/>
.	.	kIx.	.
56	[number]	k4	56
procent	procento	k1gNnPc2	procento
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
jde	jít	k5eAaImIp3nS	jít
z	z	k7c2	z
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
39	[number]	k4	39
procent	procento	k1gNnPc2	procento
ze	z	k7c2	z
školného	školné	k1gNnSc2	školné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
zhruba	zhruba	k6eAd1	zhruba
24	[number]	k4	24
850	[number]	k4	850
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
1095	[number]	k4	1095
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
4621	[number]	k4	4621
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
SFU	SFU	kA	SFU
vlastní	vlastní	k2eAgInPc4d1	vlastní
tři	tři	k4xCgInPc4	tři
kampusy	kampus	k1gInPc4	kampus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c4	v
Burnaby	Burnab	k1gInPc4	Burnab
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
v	v	k7c6	v
Harbour	Harbour	k1gMnSc1	Harbour
Centre	centr	k1gInSc5	centr
a	a	k8xC	a
okolních	okolní	k2eAgFnPc6d1	okolní
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc3	označení
SFU	SFU	kA	SFU
Vancouver	Vancouver	k1gInSc4	Vancouver
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Surrey	Surrea	k1gFnSc2	Surrea
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
moderní	moderní	k2eAgFnSc3d1	moderní
architektuře	architektura	k1gFnSc3	architektura
bývá	bývat	k5eAaImIp3nS	bývat
okolí	okolí	k1gNnSc1	okolí
SFU	SFU	kA	SFU
v	v	k7c4	v
Surrey	Surrea	k1gFnPc4	Surrea
častým	častý	k2eAgNnSc7d1	časté
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
Sci-fi	scii	k1gFnSc2	sci-fi
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Battlestar	Battlestar	k1gMnSc1	Battlestar
Galactica	Galactica	k1gMnSc1	Galactica
a	a	k8xC	a
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
soukromou	soukromý	k2eAgFnSc4d1	soukromá
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
univerzitu	univerzita	k1gFnSc4	univerzita
Trinity	Trinita	k1gFnSc2	Trinita
Western	Western	kA	Western
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
TWU	TWU	kA	TWU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UBC	UBC	kA	UBC
a	a	k8xC	a
SFU	SFU	kA	SFU
mají	mít	k5eAaImIp3nP	mít
svoje	svůj	k3xOyFgFnPc4	svůj
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
fakulty	fakulta	k1gFnSc2	fakulta
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
British	British	k1gMnSc1	British
Columbia	Columbia	k1gFnSc1	Columbia
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
polytechnické	polytechnický	k2eAgNnSc4d1	polytechnické
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
akademie	akademie	k1gFnPc4	akademie
a	a	k8xC	a
univerzity	univerzita	k1gFnPc4	univerzita
náleží	náležet	k5eAaImIp3nS	náležet
Vancouver	Vancouver	k1gInSc1	Vancouver
Community	Communita	k1gFnSc2	Communita
College	Colleg	k1gMnSc2	Colleg
<g/>
,	,	kIx,	,
Langara	Langar	k1gMnSc2	Langar
College	Colleg	k1gMnSc4	Colleg
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgMnPc4d1	umělecký
Emily	Emil	k1gMnPc4	Emil
Carr	Carra	k1gFnPc2	Carra
institute	institut	k1gInSc5	institut
of	of	k?	of
art	art	k?	art
and	and	k?	and
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
58	[number]	k4	58
a	a	k8xC	a
Vancouver	Vancouver	k1gInSc1	Vancouver
Film	film	k1gInSc1	film
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
VFS	VFS	kA	VFS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
přibude	přibýt	k5eAaPmIp3nS	přibýt
k	k	k7c3	k
univerzitám	univerzita	k1gFnPc3	univerzita
soukromá	soukromý	k2eAgNnPc4d1	soukromé
Fairleigh	Fairleigh	k1gInSc4	Fairleigh
Dickinson	Dickinson	k1gNnSc4	Dickinson
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
stavby	stavba	k1gFnPc4	stavba
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
katedrála	katedrála	k1gFnSc1	katedrála
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
Cathedral	Cathedral	k1gMnSc1	Cathedral
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc1	hotel
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
downtownu	downtown	k1gInSc2	downtown
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
moderních	moderní	k2eAgFnPc2d1	moderní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
Harbour	Harboura	k1gFnPc2	Harboura
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
Soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
města	město	k1gNnSc2	město
Vancouver	Vancouver	k1gInSc1	Vancouver
s	s	k7c7	s
přilehlým	přilehlý	k2eAgNnSc7d1	přilehlé
náměstím	náměstí	k1gNnSc7	náměstí
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
Robson	Robson	k1gMnSc1	Robson
Square	square	k1gInSc1	square
a	a	k8xC	a
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
s	s	k7c7	s
náměstím	náměstí	k1gNnSc7	náměstí
připomínající	připomínající	k2eAgMnSc1d1	připomínající
Koloseum	Koloseum	k1gNnSc4	Koloseum
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
díky	díky	k7c3	díky
filmům	film	k1gInPc3	film
Šestý	šestý	k4xOgInSc1	šestý
den	den	k1gInSc1	den
a	a	k8xC	a
Hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
či	či	k8xC	či
seriálům	seriál	k1gInPc3	seriál
Battlestar	Battlestara	k1gFnPc2	Battlestara
Galactica	Galactic	k1gInSc2	Galactic
a	a	k8xC	a
Smallville	Smallville	k1gFnSc2	Smallville
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulic	ulice	k1gFnPc2	ulice
Burrard	Burrard	k1gMnSc1	Burrard
a	a	k8xC	a
Nelson	Nelson	k1gMnSc1	Nelson
stojí	stát	k5eAaImIp3nS	stát
bývalá	bývalý	k2eAgFnSc1d1	bývalá
centrála	centrála	k1gFnSc1	centrála
společnosti	společnost	k1gFnSc2	společnost
BC	BC	kA	BC
Hydro	hydra	k1gFnSc5	hydra
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
obytný	obytný	k2eAgInSc4d1	obytný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
bývalého	bývalý	k2eAgInSc2d1	bývalý
kanadského	kanadský	k2eAgInSc2d1	kanadský
pavilonu	pavilon	k1gInSc2	pavilon
z	z	k7c2	z
Expa	Expo	k1gNnSc2	Expo
86	[number]	k4	86
<g/>
,	,	kIx,	,
Canada	Canada	k1gFnSc1	Canada
Place	plac	k1gInSc6	plac
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
střechou	střecha	k1gFnSc7	střecha
připomínající	připomínající	k2eAgInPc1d1	připomínající
bílé	bílý	k2eAgInPc1d1	bílý
stany	stan	k1gInPc1	stan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
září	září	k1gNnSc2	září
2004	[number]	k4	2004
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
a	a	k8xC	a
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
trojnásobně	trojnásobně	k6eAd1	trojnásobně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimní	zimní	k2eAgFnSc2d1	zimní
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zastane	zastanout	k5eAaPmIp3nS	zastanout
funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vysílacího	vysílací	k2eAgNnSc2d1	vysílací
střediska	středisko	k1gNnSc2	středisko
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něho	on	k3xPp3gMnSc2	on
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
nový	nový	k2eAgInSc1d1	nový
hotel	hotel	k1gInSc1	hotel
s	s	k7c7	s
obytnými	obytný	k2eAgInPc7d1	obytný
prostory	prostor	k1gInPc7	prostor
The	The	k1gMnSc1	The
Fairmont	Fairmont	k1gMnSc1	Fairmont
Pacific	Pacific	k1gMnSc1	Pacific
Rim	Rim	k1gMnSc1	Rim
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
dosahovat	dosahovat	k5eAaImF	dosahovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
140	[number]	k4	140
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Kongresového	kongresový	k2eAgNnSc2d1	Kongresové
centra	centrum	k1gNnSc2	centrum
je	být	k5eAaImIp3nS	být
i	i	k9	i
Cruise	Cruise	k1gFnSc1	Cruise
Ship	Ship	k1gMnSc1	Ship
Terminal	Terminal	k1gMnSc1	Terminal
a	a	k8xC	a
hotel	hotel	k1gInSc1	hotel
Pan-Pacific	Pan-Pacifice	k1gFnPc2	Pan-Pacifice
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
dominující	dominující	k2eAgInPc1d1	dominující
výhledu	výhled	k1gInSc6	výhled
na	na	k7c4	na
jih	jih	k1gInSc4	jih
jsou	být	k5eAaImIp3nP	být
Městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
pavilon	pavilon	k1gInSc1	pavilon
Centennial	Centennial	k1gInSc1	Centennial
nemocnice	nemocnice	k1gFnSc2	nemocnice
Vancouver	Vancouver	k1gMnSc1	Vancouver
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
stály	stát	k5eAaImAgFnP	stát
v	v	k7c4	v
downtovnu	downtovna	k1gFnSc4	downtovna
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
budovy	budova	k1gFnSc2	budova
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
Province	province	k1gFnSc1	province
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Dominion	dominion	k1gNnSc1	dominion
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Sun	Sun	kA	Sun
Tower	Tower	k1gInSc1	Tower
a	a	k8xC	a
Marine	Marin	k1gInSc5	Marin
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Art	Art	k1gMnSc1	Art
deco	deco	k1gMnSc1	deco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
též	též	k9	též
postavená	postavený	k2eAgFnSc1d1	postavená
Galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
-	-	kIx~	-
Vancouver	Vancouver	k1gInSc1	Vancouver
Art	Art	k1gFnPc2	Art
Gallery	Galler	k1gMnPc4	Galler
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
budovy	budova	k1gFnPc4	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
One	One	k1gFnSc2	One
Wall	Walla	k1gFnPc2	Walla
Centre	centr	k1gInSc5	centr
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
o	o	k7c4	o
metr	metr	k1gInSc4	metr
méně	málo	k6eAd2	málo
má	mít	k5eAaImIp3nS	mít
Shaw	Shaw	k1gMnSc1	Shaw
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
Harbour	Harbour	k1gMnSc1	Harbour
Centre	centr	k1gInSc5	centr
se	s	k7c7	s
146	[number]	k4	146
metry	metr	k1gMnPc7	metr
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
Granville	Granville	k1gInSc1	Granville
Square	square	k1gInSc1	square
a	a	k8xC	a
Royal	Royal	k1gInSc1	Royal
Centre	centr	k1gInSc5	centr
se	se	k3xPyFc4	se
142	[number]	k4	142
a	a	k8xC	a
141	[number]	k4	141
metry	metr	k1gInPc7	metr
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
a	a	k8xC	a
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
momentálně	momentálně	k6eAd1	momentálně
stavěných	stavěný	k2eAgMnPc2d1	stavěný
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
budoucí	budoucí	k2eAgInSc4d1	budoucí
Living	Living	k1gInSc4	Living
Shangri-La	Shangri-Lus	k1gMnSc2	Shangri-Lus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
197	[number]	k4	197
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
budovou	budova	k1gFnSc7	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
schválila	schválit	k5eAaPmAgFnS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
směrnici	směrnice	k1gFnSc4	směrnice
View	View	k1gFnSc2	View
Protection	Protection	k1gInSc1	Protection
Guidelines	Guidelines	k1gInSc1	Guidelines
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zavést	zavést	k5eAaPmF	zavést
v	v	k7c4	v
downtownu	downtowna	k1gFnSc4	downtowna
omezení	omezení	k1gNnSc4	omezení
výšky	výška	k1gFnSc2	výška
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
na	na	k7c4	na
91	[number]	k4	91
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
pohoří	pohoří	k1gNnSc4	pohoří
North	Northa	k1gFnPc2	Northa
Shore	Shor	k1gInSc5	Shor
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
však	však	k9	však
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
siluleta	siluleta	k1gFnSc1	siluleta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
a	a	k8xC	a
chýbí	chýbit	k5eAaPmIp3nS	chýbit
jí	on	k3xPp3gFnSc3	on
vizuální	vizuální	k2eAgFnSc1d1	vizuální
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypracovaní	vypracovaný	k2eAgMnPc1d1	vypracovaný
studie	studie	k1gFnPc1	studie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
siluletě	silulet	k1gInSc6	silulet
města	město	k1gNnSc2	město
by	by	kYmCp3nP	by
změna	změna	k1gFnSc1	změna
prospěla	prospět	k5eAaPmAgFnS	prospět
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
změnila	změnit	k5eAaPmAgFnS	změnit
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
problematice	problematika	k1gFnSc3	problematika
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
určila	určit	k5eAaPmAgFnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
downtovnu	downtovna	k1gFnSc4	downtovna
mají	mít	k5eAaImIp3nP	mít
5	[number]	k4	5
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
budovy	budova	k1gFnPc4	budova
mohly	moct	k5eAaImAgFnP	moct
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
limit	limit	k1gInSc4	limit
o	o	k7c4	o
137	[number]	k4	137
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
limit	limit	k1gInSc1	limit
dal	dát	k5eAaPmAgInS	dát
překročit	překročit	k5eAaPmF	překročit
o	o	k7c4	o
122	[number]	k4	122
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
pět	pět	k4xCc4	pět
míst	místo	k1gNnPc2	místo
ze	z	k7c2	z
sedmi	sedm	k4xCc6	sedm
vytipovaných	vytipovaný	k2eAgInPc2d1	vytipovaný
zastavěno	zastavěn	k2eAgNnSc1d1	zastavěno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
právě	právě	k6eAd1	právě
probíhá	probíhat	k5eAaImIp3nS	probíhat
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgFnPc2d1	nová
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Trvalá	trvalý	k2eAgFnSc1d1	trvalá
sbírka	sbírka	k1gFnSc1	sbírka
galerie	galerie	k1gFnSc2	galerie
umění	umění	k1gNnSc2	umění
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
7900	[number]	k4	7900
exponátů	exponát	k1gInPc2	exponát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
oceněná	oceněný	k2eAgFnSc1d1	oceněná
na	na	k7c4	na
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
kanadských	kanadský	k2eAgInPc2d1	kanadský
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
CAD	CAD	kA	CAD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
antropologie	antropologie	k1gFnSc2	antropologie
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
UBC	UBC	kA	UBC
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavním	hlavní	k2eAgMnSc7d1	hlavní
muzeum	muzeum	k1gNnSc1	muzeum
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
indiánů	indián	k1gMnPc2	indián
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgNnSc1d1	námořní
muzeum	muzeum	k1gNnSc1	muzeum
Vancouver	Vancouver	k1gInSc1	Vancouver
Maritime	Maritim	k1gInSc5	Maritim
Museum	museum	k1gNnSc1	museum
vlastní	vlastní	k2eAgFnSc2d1	vlastní
St.	st.	kA	st.
Roch	roch	k0	roch
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přeplavat	přeplavat	k5eAaPmF	přeplavat
z	z	k7c2	z
Atlantického	atlantický	k2eAgMnSc2d1	atlantický
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
Severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
průjezdem	průjezd	k1gInSc7	průjezd
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
Museum	museum	k1gNnSc1	museum
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
civilním	civilní	k2eAgInSc7d1	civilní
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
interaktivní	interaktivní	k2eAgNnSc1d1	interaktivní
muzeum	muzeum	k1gNnSc1	muzeum
Science	Science	k1gFnSc2	Science
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
divadelní	divadelní	k2eAgFnPc4d1	divadelní
společnosti	společnost	k1gFnPc4	společnost
se	se	k3xPyFc4	se
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Arts	Arts	k1gInSc4	Arts
Club	club	k1gInSc1	club
Theatre	Theatr	k1gInSc5	Theatr
Company	Compana	k1gFnPc1	Compana
<g/>
,	,	kIx,	,
Vancouver	Vancouver	k1gInSc1	Vancouver
Playhouse	Playhouse	k1gFnSc2	Playhouse
Theatre	Theatr	k1gInSc5	Theatr
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
Bard	bard	k1gMnSc1	bard
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Beach	Bea	k1gFnPc6	Bea
<g/>
.	.	kIx.	.
</s>
<s>
Medi	Medi	k6eAd1	Medi
menší	malý	k2eAgNnSc1d2	menší
patří	patřit	k5eAaImIp3nS	patřit
Studio	studio	k1gNnSc1	studio
58	[number]	k4	58
<g/>
,	,	kIx,	,
Touchstone	Touchston	k1gInSc5	Touchston
Theatre	Theatr	k1gInSc5	Theatr
<g/>
,	,	kIx,	,
Carousel	Carousela	k1gFnPc2	Carousela
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
United	United	k1gMnSc1	United
Players	Playersa	k1gFnPc2	Playersa
of	of	k?	of
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
Theatre	Theatr	k1gInSc5	Theatr
Under	Under	k1gInSc4	Under
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
-	-	kIx~	-
Divadlo	divadlo	k1gNnSc1	divadlo
pod	pod	k7c7	pod
hvězdami	hvězda	k1gFnPc7	hvězda
vystupující	vystupující	k2eAgMnPc1d1	vystupující
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
ve	v	k7c4	v
Stanley	Stanlea	k1gFnPc4	Stanlea
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
a	a	k8xC	a
Festival	festival	k1gInSc1	festival
alternativního	alternativní	k2eAgNnSc2d1	alternativní
divadla	divadlo	k1gNnSc2	divadlo
Fringe	Fringe	k1gNnSc1	Fringe
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
konají	konat	k5eAaImIp3nP	konat
v	v	k7c4	v
Rogers	Rogers	k1gInSc4	Rogers
Arena	Aren	k1gInSc2	Aren
<g/>
,	,	kIx,	,
Divadle	divadlo	k1gNnSc6	divadlo
Královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
BC	BC	kA	BC
Place	plac	k1gInSc6	plac
a	a	k8xC	a
v	v	k7c6	v
Pacific	Pacifice	k1gFnPc2	Pacifice
Coliseum	Coliseum	k1gNnSc4	Coliseum
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc4d2	menší
koncerty	koncert	k1gInPc4	koncert
pak	pak	k6eAd1	pak
v	v	k7c4	v
Plaza	plaz	k1gMnSc4	plaz
of	of	k?	of
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
,	,	kIx,	,
divadlech	divadlo	k1gNnPc6	divadlo
Orpheum	Orpheum	k1gNnSc4	Orpheum
a	a	k8xC	a
Vogue	Vogue	k1gNnSc4	Vogue
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgInPc3d1	významný
hudebním	hudební	k2eAgInPc3d1	hudební
festivalům	festival	k1gInPc3	festival
pořádaným	pořádaný	k2eAgInPc3d1	pořádaný
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
patří	patřit	k5eAaImIp3nS	patřit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazzový	jazzový	k2eAgInSc1d1	jazzový
festival	festival	k1gInSc1	festival
a	a	k8xC	a
Festival	festival	k1gInSc1	festival
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
profesionální	profesionální	k2eAgInPc1d1	profesionální
orchestry	orchestr	k1gInPc1	orchestr
<g/>
,	,	kIx,	,
Rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
orchestr	orchestr	k1gInSc1	orchestr
CBC	CBC	kA	CBC
a	a	k8xC	a
Symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
a	a	k8xC	a
početná	početný	k2eAgFnSc1d1	početná
čínská	čínský	k2eAgFnSc1d1	čínská
populace	populace	k1gFnSc1	populace
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc1	několik
svých	svůj	k3xOyFgFnPc2	svůj
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
indo-kanadských	indoanadský	k2eAgMnPc2d1	indo-kanadský
umělců	umělec	k1gMnPc2	umělec
si	se	k3xPyFc3	se
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jméno	jméno	k1gNnSc4	jméno
i	i	k8xC	i
v	v	k7c6	v
Bollywoodu	Bollywood	k1gInSc6	Bollywood
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
odvětvích	odvětví	k1gNnPc6	odvětví
indického	indický	k2eAgInSc2d1	indický
zábavního	zábavní	k2eAgInSc2d1	zábavní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgInSc1d1	noční
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
městy	město	k1gNnPc7	město
poměrně	poměrně	k6eAd1	poměrně
omezený	omezený	k2eAgMnSc1d1	omezený
díky	díky	k7c3	díky
brzké	brzký	k2eAgFnSc3d1	brzká
zavírací	zavírací	k2eAgFnSc3d1	zavírací
době	doba	k1gFnSc3	doba
pro	pro	k7c4	pro
bary	bar	k1gInPc4	bar
a	a	k8xC	a
noční	noční	k2eAgInPc1d1	noční
kluby	klub	k1gInPc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Neochota	neochota	k1gFnSc1	neochota
úřadů	úřad	k1gInPc2	úřad
dovolit	dovolit	k5eAaPmF	dovolit
jejich	jejich	k3xOp3gFnSc4	jejich
delší	dlouhý	k2eAgFnSc4d2	delší
otvírací	otvírací	k2eAgFnSc4d1	otvírací
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
začíná	začínat	k5eAaImIp3nS	začínat
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
zavírací	zavírací	k2eAgFnSc7d1	zavírací
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
regulaci	regulace	k1gFnSc4	regulace
a	a	k8xC	a
projevilo	projevit	k5eAaPmAgNnS	projevit
snahu	snaha	k1gFnSc4	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
downtownu	downtownout	k5eAaImIp1nS	downtownout
zábavní	zábavní	k2eAgFnSc1d1	zábavní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Granville	Granville	k1gFnSc2	Granville
street	streeta	k1gFnPc2	streeta
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgNnSc4d1	mírné
klima	klima	k1gNnSc4	klima
a	a	k8xC	a
blízkost	blízkost	k1gFnSc4	blízkost
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
oceánu	oceán	k1gInSc2	oceán
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
města	město	k1gNnSc2	město
populární	populární	k2eAgInSc4d1	populární
cíl	cíl	k1gInSc4	cíl
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gMnPc6	jeho
obyvatelích	obyvatel	k1gMnPc6	obyvatel
<g/>
,	,	kIx,	,
Vancouver	Vancouver	k1gInSc1	Vancouver
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
městech	město	k1gNnPc6	město
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Montreal	Montreal	k1gInSc1	Montreal
a	a	k8xC	a
Halifax	Halifax	k1gInSc4	Halifax
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
nejštíhlejší	štíhlý	k2eAgFnSc4d3	nejštíhlejší
populaci	populace	k1gFnSc4	populace
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
existuje	existovat	k5eAaImIp3nS	existovat
přes	přes	k7c4	přes
1	[number]	k4	1
298	[number]	k4	298
hektarů	hektar	k1gInPc2	hektar
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
404	[number]	k4	404
hektary	hektar	k1gInPc7	hektar
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnPc4d3	veliký
Stanley	Stanlea	k1gFnPc4	Stanlea
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
taktéž	taktéž	k?	taktéž
leží	ležet	k5eAaImIp3nS	ležet
několik	několik	k4yIc4	několik
velkých	velký	k2eAgFnPc2d1	velká
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
mnohdy	mnohdy	k6eAd1	mnohdy
navzájem	navzájem	k6eAd1	navzájem
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
seskupení	seskupení	k1gNnSc1	seskupení
pláží	pláž	k1gFnPc2	pláž
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Stanley	Stanlea	k1gFnSc2	Stanlea
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
od	od	k7c2	od
False	False	k1gFnSc2	False
Creeku	Creek	k1gInSc2	Creek
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
oblast	oblast	k1gFnSc4	oblast
univerzity	univerzita	k1gFnSc2	univerzita
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
18	[number]	k4	18
kilometrů	kilometr	k1gInPc2	kilometr
pláží	pláž	k1gFnPc2	pláž
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
First	First	k1gMnSc1	First
beach	beach	k1gMnSc1	beach
<g/>
,	,	kIx,	,
Jericho	Jericho	k1gNnSc1	Jericho
<g/>
,	,	kIx,	,
Kitsilano	Kitsilana	k1gFnSc5	Kitsilana
beach	beach	k1gMnSc1	beach
<g/>
,	,	kIx,	,
Second	Second	k1gMnSc1	Second
beach	beach	k1gMnSc1	beach
<g/>
,	,	kIx,	,
Spanish	Spanish	k1gMnSc1	Spanish
bank	banka	k1gFnPc2	banka
east	east	k1gInSc1	east
<g/>
,	,	kIx,	,
Locarno	Locarno	k1gNnSc1	Locarno
<g/>
,	,	kIx,	,
Spanish	Spanish	k1gMnSc1	Spanish
bank	banka	k1gFnPc2	banka
west	west	k1gMnSc1	west
<g/>
,	,	kIx,	,
Spanish	Spanish	k1gMnSc1	Spanish
bank	banka	k1gFnPc2	banka
expansion	expansion	k1gInSc1	expansion
<g/>
,	,	kIx,	,
Third	Third	k1gMnSc1	Third
beach	beach	k1gMnSc1	beach
a	a	k8xC	a
Sunset	Sunset	k1gMnSc1	Sunset
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pláží	pláž	k1gFnPc2	pláž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provozovat	provozovat	k5eAaImF	provozovat
mnoho	mnoho	k4c4	mnoho
vodních	vodní	k2eAgInPc2d1	vodní
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
si	se	k3xPyFc3	se
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
nadšenci	nadšenec	k1gMnPc1	nadšenec
do	do	k7c2	do
člunů	člun	k1gInPc2	člun
a	a	k8xC	a
jachet	jachta	k1gFnPc2	jachta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
North	Northa	k1gFnPc2	Northa
Shore	Shor	k1gInSc5	Shor
leží	ležet	k5eAaImIp3nS	ležet
tři	tři	k4xCgInPc1	tři
lyžařsky	lyžařsky	k6eAd1	lyžařsky
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
hory	hora	k1gFnPc4	hora
se	s	k7c7	s
sjezdovkami	sjezdovka	k1gFnPc7	sjezdovka
<g/>
,	,	kIx,	,
Cypress	Cypress	k1gInSc4	Cypress
Mountain	Mountaina	k1gFnPc2	Mountaina
<g/>
,	,	kIx,	,
Grouse	Grouse	k1gFnSc1	Grouse
Mountain	Mountain	k1gMnSc1	Mountain
a	a	k8xC	a
Mount	Mount	k1gMnSc1	Mount
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jsou	být	k5eAaImIp3nP	být
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
asi	asi	k9	asi
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
cesty	cesta	k1gFnSc2	cesta
autem	auto	k1gNnSc7	auto
od	od	k7c2	od
downtownu	downtown	k1gInSc2	downtown
<g/>
.	.	kIx.	.
</s>
<s>
Skrz	skrz	k7c4	skrz
pohoří	pohoří	k1gNnSc4	pohoří
vedou	vést	k5eAaImIp3nP	vést
světoznámé	světoznámý	k2eAgFnPc4d1	světoznámá
cesty	cesta	k1gFnPc4	cesta
pro	pro	k7c4	pro
horskou	horský	k2eAgFnSc4d1	horská
cyklistiku	cyklistika	k1gFnSc4	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	Řek	k1gMnPc4	Řek
Capillano	Capillana	k1gFnSc5	Capillana
<g/>
,	,	kIx,	,
Seymour	Seymour	k1gMnSc1	Seymour
a	a	k8xC	a
záliv	záliv	k1gInSc1	záliv
Lynn	Lynn	k1gMnSc1	Lynn
(	(	kIx(	(
<g/>
Lynn	Lynn	k1gMnSc1	Lynn
Creek	Creek	k1gMnSc1	Creek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
od	od	k7c2	od
downtownu	downtown	k1gInSc2	downtown
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
autem	auto	k1gNnSc7	auto
nabízí	nabízet	k5eAaImIp3nS	nabízet
výzvu	výzva	k1gFnSc4	výzva
pro	pro	k7c4	pro
milovníky	milovník	k1gMnPc4	milovník
raftingu	rafting	k1gInSc2	rafting
a	a	k8xC	a
kajaku	kajak	k1gInSc2	kajak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
turistické	turistický	k2eAgNnSc4d1	turistické
místo	místo	k1gNnSc4	místo
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
cesta	cesta	k1gFnSc1	cesta
nad	nad	k7c7	nad
Clevelandovou	Clevelandový	k2eAgFnSc7d1	Clevelandová
přehradou	přehrada	k1gFnSc7	přehrada
a	a	k8xC	a
visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
Capilano	Capilana	k1gFnSc5	Capilana
Suspension	Suspension	k1gInSc4	Suspension
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
klenoucí	klenoucí	k2eAgFnSc2d1	klenoucí
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Capilano	Capilana	k1gFnSc5	Capilana
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
North	North	k1gMnSc1	North
Vancouver	Vancouver	k1gMnSc1	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
136	[number]	k4	136
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
tyčí	tyč	k1gFnPc2	tyč
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
ho	on	k3xPp3gMnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
800	[number]	k4	800
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
město	město	k1gNnSc1	město
pořádá	pořádat	k5eAaImIp3nS	pořádat
každoroční	každoroční	k2eAgInSc4d1	každoroční
maraton	maraton	k1gInSc4	maraton
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
kilometrový	kilometrový	k2eAgInSc1d1	kilometrový
běh	běh	k1gInSc1	běh
Vancouver	Vancouver	k1gInSc1	Vancouver
Sun	Sun	kA	Sun
Run	run	k1gInSc1	run
se	se	k3xPyFc4	se
běží	běžet	k5eAaImIp3nS	běžet
každý	každý	k3xTgInSc1	každý
duben	duben	k1gInSc1	duben
<g/>
.	.	kIx.	.
126	[number]	k4	126
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Vancouveru	Vancouver	k1gInSc2	Vancouver
leží	ležet	k5eAaImIp3nS	ležet
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
rezort	rezort	k1gInSc4	rezort
Whistler	Whistler	k1gInSc1	Whistler
Blackcomb	Blackcomb	k1gInSc1	Blackcomb
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
časopisem	časopis	k1gInSc7	časopis
Sky	Sky	k1gFnSc2	Sky
Magazine	Magazin	k1gInSc5	Magazin
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
lyžařské	lyžařský	k2eAgNnSc4d1	lyžařské
středisko	středisko	k1gNnSc4	středisko
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
3	[number]	k4	3
307	[number]	k4	307
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
největším	veliký	k2eAgInSc7d3	veliký
lyžařským	lyžařský	k2eAgInSc7d1	lyžařský
rezortem	rezort	k1gInSc7	rezort
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městy	město	k1gNnPc7	město
Whistler	Whistler	k1gMnSc1	Whistler
a	a	k8xC	a
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
konaly	konat	k5eAaImAgFnP	konat
Zimní	zimní	k2eAgFnPc1d1	zimní
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
Hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
existuje	existovat	k5eAaImIp3nS	existovat
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
klubu	klub	k1gInSc2	klub
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucks	k1gInSc1	Canucks
hrajícího	hrající	k2eAgMnSc2d1	hrající
profesionální	profesionální	k2eAgInSc4d1	profesionální
hokej	hokej	k1gInSc4	hokej
v	v	k7c6	v
Severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
divizi	divize	k1gFnSc6	divize
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Juniorský	juniorský	k2eAgInSc1d1	juniorský
tým	tým	k1gInSc1	tým
Vancouver	Vancouver	k1gInSc1	Vancouver
Giants	Giants	k1gInSc4	Giants
hraje	hrát	k5eAaImIp3nS	hrát
Western	western	k1gInSc1	western
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
finále	finále	k1gNnSc4	finále
Memorial	Memorial	k1gMnSc1	Memorial
Cupu	cup	k1gInSc2	cup
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
náležící	náležící	k2eAgInSc1d1	náležící
nejlepšímu	dobrý	k2eAgInSc3d3	nejlepší
týmu	tým	k1gInSc3	tým
z	z	k7c2	z
kanadských	kanadský	k2eAgFnPc2d1	kanadská
juniorských	juniorský	k2eAgFnPc2d1	juniorská
soutěží	soutěž	k1gFnPc2	soutěž
OHL	OHL	kA	OHL
<g/>
,	,	kIx,	,
QJMHL	QJMHL	kA	QJMHL
a	a	k8xC	a
WHL	WHL	kA	WHL
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
městě	město	k1gNnSc6	město
fungoval	fungovat	k5eAaImAgInS	fungovat
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
Vancouver	Vancouver	k1gMnSc1	Vancouver
Millionaires	Millionaires	k1gMnSc1	Millionaires
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
Pacific	Pacific	k1gMnSc1	Pacific
Coast	Coast	k1gMnSc1	Coast
Hockey	Hocke	k2eAgFnPc4d1	Hocke
League	Leagu	k1gFnPc4	Leagu
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
působili	působit	k5eAaImAgMnP	působit
Grizzlies	Grizzlies	k1gMnSc1	Grizzlies
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kanadských	kanadský	k2eAgInPc2d1	kanadský
basketbalových	basketbalový	k2eAgInPc2d1	basketbalový
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
<s>
Grizzlies	Grizzlies	k1gInSc4	Grizzlies
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
prodáni	prodán	k2eAgMnPc1d1	prodán
a	a	k8xC	a
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Memphisu	Memphis	k1gInSc2	Memphis
<g/>
.	.	kIx.	.
</s>
<s>
Baseball	baseball	k1gInSc1	baseball
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
klub	klub	k1gInSc1	klub
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
Canadians	Canadiansa	k1gFnPc2	Canadiansa
<g/>
,	,	kIx,	,
působící	působící	k2eAgInPc1d1	působící
v	v	k7c6	v
Severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
Profesionální	profesionální	k2eAgInSc1d1	profesionální
klub	klub	k1gInSc4	klub
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
BC	BC	kA	BC
Lions	Lions	k1gInSc1	Lions
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Kanadské	kanadský	k2eAgFnSc6d1	kanadská
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
-	-	kIx~	-
CFL	CFL	kA	CFL
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Grey	Gre	k1gMnPc4	Gre
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
vítězi	vítěz	k1gMnPc7	vítěz
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Fotbal	fotbal	k1gInSc1	fotbal
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
profesionální	profesionální	k2eAgInSc1d1	profesionální
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
Whitecaps	Whitecaps	k1gInSc1	Whitecaps
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
NASL	NASL	kA	NASL
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
nový	nový	k2eAgInSc1d1	nový
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
86	[number]	k4	86
<g/>
ers	ers	k?	ers
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Whitecaps	Whitecaps	k1gInSc4	Whitecaps
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Whitecaps	Whitecaps	k1gInSc4	Whitecaps
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
Tým	tým	k1gInSc1	tým
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
mužské	mužský	k2eAgInPc4d1	mužský
<g/>
,	,	kIx,	,
ženské	ženský	k2eAgInPc4d1	ženský
a	a	k8xC	a
mládežnické	mládežnický	k2eAgInPc4d1	mládežnický
týmy	tým	k1gInPc4	tým
pod	pod	k7c4	pod
jednu	jeden	k4xCgFnSc4	jeden
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Whitecaps	Whitecaps	k6eAd1	Whitecaps
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
První	první	k4xOgFnSc6	první
divizi	divize	k1gFnSc6	divize
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
USL	USL	kA	USL
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2011	[number]	k4	2011
bude	být	k5eAaImBp3nS	být
klub	klub	k1gInSc4	klub
hrát	hrát	k5eAaImF	hrát
severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
profesionální	profesionální	k2eAgFnSc4d1	profesionální
soutěž	soutěž	k1gFnSc4	soutěž
Major	major	k1gMnSc1	major
League	League	k1gFnPc2	League
Soccer	Soccer	k1gMnSc1	Soccer
<g/>
.	.	kIx.	.
</s>
<s>
Galský	galský	k2eAgInSc1d1	galský
fotbal	fotbal	k1gInSc1	fotbal
Galský	galský	k2eAgInSc1d1	galský
fotbal	fotbal	k1gInSc1	fotbal
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
míčové	míčový	k2eAgFnSc2d1	Míčová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
hrají	hrát	k5eAaImIp3nP	hrát
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
v	v	k7c6	v
irsky	irsky	k6eAd1	irsky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
Harps	Harpsa	k1gFnPc2	Harpsa
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Západokanadské	Západokanadský	k2eAgFnSc6d1	Západokanadský
divizi	divize	k1gFnSc6	divize
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
má	mít	k5eAaImIp3nS	mít
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	se	k3xPyFc3	se
Mistrovství	mistrovství	k1gNnSc4	mistrovství
západní	západní	k2eAgFnSc2d1	západní
Kanady	Kanada	k1gFnSc2	Kanada
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Vancouveru	Vancouvera	k1gFnSc4	Vancouvera
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
dějištěm	dějiště	k1gNnSc7	dějiště
každoročního	každoroční	k2eAgNnSc2d1	každoroční
mistrovství	mistrovství	k1gNnSc2	mistrovství
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
ve	v	k7c6	v
skateboardingu	skateboarding	k1gInSc6	skateboarding
<g/>
,	,	kIx,	,
Slam	slam	k1gInSc1	slam
City	city	k1gNnSc1	city
Jam	jam	k1gInSc1	jam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gMnSc1	Vancouver
stal	stát	k5eAaPmAgMnS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
je	být	k5eAaImIp3nS	být
také	také	k9	také
kolébkou	kolébka	k1gFnSc7	kolébka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
ekologické	ekologický	k2eAgFnSc2d1	ekologická
organizace	organizace	k1gFnSc2	organizace
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
pokusném	pokusný	k2eAgInSc6d1	pokusný
atomovém	atomový	k2eAgInSc6d1	atomový
výbuchu	výbuch	k1gInSc6	výbuch
Milrow	Milrow	k1gFnSc2	Milrow
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Amčitka	Amčitka	k1gFnSc1	Amčitka
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Aleuty	Aleuty	k1gFnPc4	Aleuty
u	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
organizace	organizace	k1gFnSc1	organizace
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Make	Make	k1gInSc1	Make
A	a	k8xC	a
Wave	Wave	k1gFnSc1	Wave
Committee	Committee	k1gFnSc1	Committee
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
provádění	provádění	k1gNnSc3	provádění
dalších	další	k2eAgInPc2d1	další
testovacích	testovací	k2eAgInPc2d1	testovací
výbuchů	výbuch	k1gInPc2	výbuch
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
chystaly	chystat	k5eAaImAgInP	chystat
provést	provést	k5eAaPmF	provést
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
aktivisté	aktivista	k1gMnPc1	aktivista
vypluli	vyplout	k5eAaPmAgMnP	vyplout
na	na	k7c6	na
rybářské	rybářský	k2eAgFnSc6d1	rybářská
lodí	loď	k1gFnSc7	loď
Phyllis	Phyllis	k1gFnSc1	Phyllis
Cormack	Cormack	k1gMnSc1	Cormack
už	už	k6eAd1	už
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
(	(	kIx(	(
<g/>
Zelený	zelený	k2eAgInSc1d1	zelený
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
přírodu	příroda	k1gFnSc4	příroda
i	i	k8xC	i
touhu	touha	k1gFnSc4	touha
zbavit	zbavit	k5eAaPmF	zbavit
svět	svět	k1gInSc4	svět
jaderné	jaderný	k2eAgFnSc2d1	jaderná
hrozby	hrozba	k1gFnSc2	hrozba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
protestovali	protestovat	k5eAaBmAgMnP	protestovat
proti	proti	k7c3	proti
americkým	americký	k2eAgInPc3d1	americký
atomovým	atomový	k2eAgInPc3d1	atomový
testům	test	k1gInPc3	test
na	na	k7c6	na
Amchitce	Amchitka	k1gFnSc6	Amchitka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
útočištěm	útočiště	k1gNnSc7	útočiště
tisíců	tisíc	k4xCgInPc2	tisíc
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
vyder	vydra	k1gFnPc2	vydra
mořských	mořský	k2eAgFnPc2d1	mořská
<g/>
,	,	kIx,	,
orlů	orel	k1gMnPc2	orel
<g/>
,	,	kIx,	,
sokolů	sokol	k1gMnPc2	sokol
stěhovavých	stěhovavý	k2eAgNnPc2d1	stěhovavé
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
epicentru	epicentrum	k1gNnSc6	epicentrum
častých	častý	k2eAgNnPc2d1	časté
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Protestní	protestní	k2eAgFnSc1d1	protestní
plavba	plavba	k1gFnSc1	plavba
sice	sice	k8xC	sice
nezabránila	zabránit	k5eNaPmAgFnS	zabránit
třetímu	třetí	k4xOgMnSc3	třetí
pokusnému	pokusný	k2eAgInSc3d1	pokusný
výbuchu	výbuch	k1gInSc3	výbuch
nazvanému	nazvaný	k2eAgInSc3d1	nazvaný
Cannikin	Cannikin	k1gInSc4	Cannikin
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
organizace	organizace	k1gFnSc2	organizace
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
<g/>
.	.	kIx.	.
</s>
<s>
Pacific	Pacific	k1gMnSc1	Pacific
Newspaper	Newspaper	k1gMnSc1	Newspaper
Group	Group	k1gMnSc1	Group
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
vydává	vydávat	k5eAaPmIp3nS	vydávat
dvoje	dvoje	k4xRgFnPc4	dvoje
největší	veliký	k2eAgFnPc4d3	veliký
noviny	novina	k1gFnPc4	novina
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Vancouver	Vancouver	k1gInSc1	Vancouver
Sun	Sun	kA	Sun
a	a	k8xC	a
The	The	k1gFnSc1	The
Province	province	k1gFnSc1	province
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
noviny	novina	k1gFnPc4	novina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
patří	patřit	k5eAaImIp3nS	patřit
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
Metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
24	[number]	k4	24
Hours	Hoursa	k1gFnPc2	Hoursa
a	a	k8xC	a
Vancouver	Vancouver	k1gMnSc1	Vancouver
Courier	Courier	k1gMnSc1	Courier
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgNnPc4d1	nezávislé
periodika	periodikum	k1gNnPc4	periodikum
jsou	být	k5eAaImIp3nP	být
The	The	k1gMnSc1	The
West	West	k1gMnSc1	West
Ender	Ender	k1gMnSc1	Ender
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Georgia	Georgium	k1gNnSc2	Georgium
Straight	Straight	k1gMnSc1	Straight
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Republic	Republice	k1gFnPc2	Republice
a	a	k8xC	a
Only	Onla	k1gFnSc2	Onla
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
stanice	stanice	k1gFnPc1	stanice
se	s	k7c7	s
zpravodajstvím	zpravodajství	k1gNnSc7	zpravodajství
vysílající	vysílající	k2eAgFnSc2d1	vysílající
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
jsou	být	k5eAaImIp3nP	být
CBC	CBC	kA	CBC
Radio	radio	k1gNnSc4	radio
One	One	k1gFnSc2	One
<g/>
,	,	kIx,	,
CKNW	CKNW	kA	CKNW
a	a	k8xC	a
CKWX	CKWX	kA	CKWX
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
vysílají	vysílat	k5eAaImIp3nP	vysílat
tyto	tento	k3xDgFnPc1	tento
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
-	-	kIx~	-
veřejnoprávní	veřejnoprávní	k2eAgFnSc7d1	veřejnoprávní
CBC	CBC	kA	CBC
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
CityTV	CityTV	k1gFnSc1	CityTV
<g/>
,	,	kIx,	,
Global	globat	k5eAaImAgMnS	globat
TV	TV	kA	TV
<g/>
,	,	kIx,	,
Channel	Channel	k1gInSc1	Channel
M	M	kA	M
a	a	k8xC	a
CTV	CTV	kA	CTV
(	(	kIx(	(
<g/>
Channel	Channel	k1gInSc1	Channel
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgNnSc1	první
město	město	k1gNnSc1	město
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gInSc1	Vancouver
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
systému	systém	k1gInSc3	systém
partnerských	partnerský	k2eAgFnPc2d1	partnerská
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
partnerská	partnerský	k2eAgNnPc4d1	partnerské
města	město	k1gNnPc4	město
Vancouveru	Vancouver	k1gInSc2	Vancouver
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
Jokohama	Jokohama	k1gFnSc1	Jokohama
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
-	-	kIx~	-
rok	rok	k1gInSc1	rok
1965	[number]	k4	1965
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
-	-	kIx~	-
rok	rok	k1gInSc4	rok
1978	[number]	k4	1978
Kanton	Kanton	k1gInSc1	Kanton
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
-	-	kIx~	-
rok	rok	k1gInSc1	rok
1985	[number]	k4	1985
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
rok	rok	k1gInSc1	rok
1986	[number]	k4	1986
</s>
