<s>
Diecéze	diecéze	k1gFnSc1	diecéze
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
dioecesis	dioecesis	k1gInSc1	dioecesis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
církví	církev	k1gFnPc2	církev
s	s	k7c7	s
episkopální	episkopální	k2eAgFnSc7d1	episkopální
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
biskup	biskup	k1gMnSc1	biskup
s	s	k7c7	s
úřadem	úřad	k1gInSc7	úřad
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
nazývaným	nazývaný	k2eAgNnSc7d1	nazývané
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Diecéze	diecéze	k1gFnSc1	diecéze
se	se	k3xPyFc4	se
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
územně	územně	k6eAd1	územně
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
institucionálně	institucionálně	k6eAd1	institucionálně
či	či	k8xC	či
funkčně	funkčně	k6eAd1	funkčně
<g/>
.	.	kIx.	.
</s>
<s>
Podřazenou	podřazený	k2eAgFnSc7d1	podřazená
jednotkou	jednotka	k1gFnSc7	jednotka
diecéze	diecéze	k1gFnSc2	diecéze
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
farnosti	farnost	k1gFnPc4	farnost
či	či	k8xC	či
svazky	svazek	k1gInPc4	svazek
farností	farnost	k1gFnPc2	farnost
(	(	kIx(	(
<g/>
děkanáty	děkanát	k1gInPc1	děkanát
<g/>
,	,	kIx,	,
vikariáty	vikariát	k1gInPc1	vikariát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nadřazená	nadřazený	k2eAgFnSc1d1	nadřazená
jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
církví	církev	k1gFnPc2	církev
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
u	u	k7c2	u
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
jde	jít	k5eAaImIp3nS	jít
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c6	o
církevní	církevní	k2eAgFnSc6d1	církevní
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
diecéze	diecéze	k1gFnSc1	diecéze
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
sídelním	sídelní	k2eAgMnSc7d1	sídelní
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
neboli	neboli	k8xC	neboli
metropolitou	metropolita	k1gMnSc7	metropolita
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
i	i	k9	i
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jsou	být	k5eAaImIp3nP	být
sufragány	sufragán	k1gMnPc7	sufragán
<g/>
;	;	kIx,	;
např.	např.	kA	např.
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
cambraiská	cambraiská	k1gFnSc1	cambraiská
<g/>
,	,	kIx,	,
či	či	k8xC	či
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
trnavská	trnavský	k2eAgFnSc1d1	Trnavská
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc4	pojem
diecéze	diecéze	k1gFnSc2	diecéze
používají	používat	k5eAaImIp3nP	používat
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
Anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
v	v	k7c6	v
ČR	ČR	kA	ČR
též	též	k9	též
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
východní	východní	k2eAgFnSc2d1	východní
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
používají	používat	k5eAaImIp3nP	používat
zpravidla	zpravidla	k6eAd1	zpravidla
pojem	pojem	k1gInSc4	pojem
eparchie	eparchie	k1gFnSc2	eparchie
(	(	kIx(	(
<g/>
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
eparcha	eparcha	k1gMnSc1	eparcha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
pak	pak	k6eAd1	pak
pojem	pojem	k1gInSc4	pojem
diecéze	diecéze	k1gFnSc2	diecéze
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
různými	různý	k2eAgInPc7d1	různý
termíny	termín	k1gInPc7	termín
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
biskupská	biskupský	k2eAgFnSc1d1	biskupská
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mají	mít	k5eAaImIp3nP	mít
diecézní	diecézní	k2eAgNnSc4d1	diecézní
uspořádání	uspořádání	k1gNnSc4	uspořádání
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
církve	církev	k1gFnPc1	církev
buďto	buďto	k8xC	buďto
diecézní	diecézní	k2eAgNnPc1d1	diecézní
uspořádání	uspořádání	k1gNnPc1	uspořádání
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
diecéze	diecéze	k1gFnSc1	diecéze
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
nevytyčují	vytyčovat	k5eNaImIp3nP	vytyčovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řeckokatolická	řeckokatolický	k2eAgFnSc1d1	řeckokatolická
církev	církev	k1gFnSc1	církev
zde	zde	k6eAd1	zde
vytyčuje	vytyčovat	k5eAaImIp3nS	vytyčovat
pouze	pouze	k6eAd1	pouze
apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
exarchát	exarchát	k1gInSc1	exarchát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starořeckého	starořecký	k2eAgInSc2d1	starořecký
"	"	kIx"	"
<g/>
dioikesis	dioikesis	k1gFnSc1	dioikesis
<g/>
"	"	kIx"	"
<g/>
̈	̈	k?	̈
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
administrace	administrace	k1gFnSc1	administrace
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
dioecesis	dioecesis	k1gFnSc1	dioecesis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Označovalo	označovat	k5eAaImAgNnS	označovat
jednotku	jednotka	k1gFnSc4	jednotka
finanční	finanční	k2eAgFnSc2d1	finanční
správy	správa	k1gFnSc2	správa
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
zavedenou	zavedený	k2eAgFnSc4d1	zavedená
za	za	k7c4	za
Diokleciána	Dioklecián	k1gMnSc4	Dioklecián
s	s	k7c7	s
12	[number]	k4	12
provinciemi	provincie	k1gFnPc7	provincie
(	(	kIx(	(
<g/>
Africa	Afric	k2eAgFnSc1d1	Africa
<g/>
,	,	kIx,	,
Asiana	Asiana	k1gFnSc1	Asiana
<g/>
,	,	kIx,	,
Britania	Britanium	k1gNnSc2	Britanium
<g/>
,	,	kIx,	,
Galia	galium	k1gNnSc2	galium
<g/>
,	,	kIx,	,
Hispania	Hispanium	k1gNnSc2	Hispanium
<g/>
,	,	kIx,	,
Italia	Italium	k1gNnSc2	Italium
<g/>
,	,	kIx,	,
Moesia	Moesium	k1gNnSc2	Moesium
<g/>
,	,	kIx,	,
Oriens	Oriensa	k1gFnPc2	Oriensa
<g/>
,	,	kIx,	,
Pannonia	Pannonium	k1gNnSc2	Pannonium
<g/>
,	,	kIx,	,
Pontica	Ponticum	k1gNnSc2	Ponticum
<g/>
,	,	kIx,	,
Thracia	Thracium	k1gNnSc2	Thracium
<g/>
,	,	kIx,	,
Vienensis	Vienensis	k1gFnSc2	Vienensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
2	[number]	k4	2
846	[number]	k4	846
řádných	řádný	k2eAgFnPc2d1	řádná
diecézí	diecéze	k1gFnPc2	diecéze
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
Svatý	svatý	k2eAgInSc4d1	svatý
stolec	stolec	k1gInSc4	stolec
<g/>
,	,	kIx,	,
640	[number]	k4	640
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
(	(	kIx(	(
<g/>
provincií	provincie	k1gFnPc2	provincie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
9	[number]	k4	9
patriarchátů	patriarchát	k1gInPc2	patriarchát
<g/>
,	,	kIx,	,
4	[number]	k4	4
hlavní	hlavní	k2eAgFnSc1d1	hlavní
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
<g/>
,	,	kIx,	,
550	[number]	k4	550
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
<g/>
,	,	kIx,	,
77	[number]	k4	77
samostatných	samostatný	k2eAgFnPc2d1	samostatná
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
a	a	k8xC	a
2	[number]	k4	2
205	[number]	k4	205
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
katolických	katolický	k2eAgFnPc6d1	katolická
církvích	církev	k1gFnPc6	církev
(	(	kIx(	(
<g/>
východního	východní	k2eAgInSc2d1	východní
ritu	rit	k1gInSc2	rit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
řeckokatolické	řeckokatolický	k2eAgFnPc1d1	řeckokatolická
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obdoba	obdoba	k1gFnSc1	obdoba
k	k	k7c3	k
diecézi	diecéze	k1gFnSc3	diecéze
nazývá	nazývat	k5eAaImIp3nS	nazývat
eparchie	eparchie	k1gFnSc1	eparchie
<g/>
.	.	kIx.	.
</s>
<s>
Nadřazenou	nadřazený	k2eAgFnSc7d1	nadřazená
jednotkou	jednotka	k1gFnSc7	jednotka
jí	on	k3xPp3gFnSc2	on
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
církevní	církevní	k2eAgFnPc1d1	církevní
provincie	provincie	k1gFnPc1	provincie
<g/>
,	,	kIx,	,
podřazené	podřazený	k2eAgFnPc1d1	podřazená
pak	pak	k6eAd1	pak
farnosti	farnost	k1gFnPc4	farnost
či	či	k8xC	či
svazky	svazek	k1gInPc4	svazek
farností	farnost	k1gFnPc2	farnost
<g/>
,	,	kIx,	,
nazývanými	nazývaný	k2eAgInPc7d1	nazývaný
vikariáty	vikariát	k1gInPc7	vikariát
(	(	kIx(	(
<g/>
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
děkanáty	děkanát	k1gInPc1	děkanát
(	(	kIx(	(
<g/>
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
sídelní	sídelní	k2eAgInSc1d1	sídelní
biskup	biskup	k1gInSc1	biskup
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
k	k	k7c3	k
ruce	ruka	k1gFnSc3	ruka
pomocné	pomocný	k2eAgInPc4d1	pomocný
biskupy	biskup	k1gInPc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
diecéze	diecéze	k1gFnSc1	diecéze
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
nejstarší	starý	k2eAgFnSc1d3	nejstarší
či	či	k8xC	či
historicky	historicky	k6eAd1	historicky
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
)	)	kIx)	)
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
arcidiecézí	arcidiecéze	k1gFnSc7	arcidiecéze
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
zastává	zastávat	k5eAaImIp3nS	zastávat
post	post	k1gInSc4	post
metropolity	metropolita	k1gMnSc2	metropolita
-	-	kIx~	-
představeného	představený	k1gMnSc2	představený
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
církevní	církevní	k2eAgFnSc1d1	církevní
provincie	provincie	k1gFnSc1	provincie
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
pražská	pražský	k2eAgFnSc1d1	Pražská
(	(	kIx(	(
<g/>
973	[number]	k4	973
<g/>
)	)	kIx)	)
diecéze	diecéze	k1gFnSc1	diecéze
českobudějovická	českobudějovický	k2eAgFnSc1d1	českobudějovická
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
diecéze	diecéze	k1gFnSc1	diecéze
královéhradecká	královéhradecký	k2eAgFnSc1d1	Královéhradecká
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
diecéze	diecéze	k1gFnSc1	diecéze
litoměřická	litoměřický	k2eAgFnSc1d1	Litoměřická
(	(	kIx(	(
<g/>
1655	[number]	k4	1655
<g/>
)	)	kIx)	)
diecéze	diecéze	k1gFnSc1	diecéze
plzeňská	plzeňská	k1gFnSc1	plzeňská
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Moravská	moravský	k2eAgFnSc1d1	Moravská
církevní	církevní	k2eAgFnSc1d1	církevní
provincie	provincie	k1gFnSc1	provincie
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
(	(	kIx(	(
<g/>
1063	[number]	k4	1063
<g/>
)	)	kIx)	)
diecéze	diecéze	k1gFnSc1	diecéze
brněnská	brněnský	k2eAgFnSc1d1	brněnská
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
diecéze	diecéze	k1gFnSc1	diecéze
ostravsko-opavská	ostravskopavský	k2eAgFnSc1d1	ostravsko-opavská
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
působící	působící	k2eAgFnSc1d1	působící
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
českých	český	k2eAgFnPc2d1	Česká
diecézí	diecéze	k1gFnPc2	diecéze
(	(	kIx(	(
<g/>
pražskou	pražský	k2eAgFnSc4d1	Pražská
<g/>
,	,	kIx,	,
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
<g/>
,	,	kIx,	,
královéhradeckou	královéhradecký	k2eAgFnSc4d1	Královéhradecká
<g/>
,	,	kIx,	,
olomouckou	olomoucký	k2eAgFnSc4d1	olomoucká
a	a	k8xC	a
plzeňskou	plzeňský	k2eAgFnSc4d1	Plzeňská
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
(	(	kIx(	(
<g/>
bratislavskou	bratislavský	k2eAgFnSc4d1	Bratislavská
<g/>
)	)	kIx)	)
diecézi	diecéze	k1gFnSc4	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
14	[number]	k4	14
církevních	církevní	k2eAgFnPc2d1	církevní
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
metropolia	metropolius	k1gMnSc2	metropolius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
14	[number]	k4	14
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
a	a	k8xC	a
27	[number]	k4	27
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
.	.	kIx.	.
</s>
