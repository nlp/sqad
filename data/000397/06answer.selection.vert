<s>
Diecéze	diecéze	k1gFnSc1	diecéze
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
dioecesis	dioecesis	k1gInSc1	dioecesis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
církví	církev	k1gFnPc2	církev
s	s	k7c7	s
episkopální	episkopální	k2eAgFnSc7d1	episkopální
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
biskup	biskup	k1gMnSc1	biskup
s	s	k7c7	s
úřadem	úřad	k1gInSc7	úřad
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
nazývaným	nazývaný	k2eAgNnSc7d1	nazývané
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
