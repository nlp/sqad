<s>
Kolumbus	Kolumbus	k1gMnSc1
nebyl	být	k5eNaImAgMnS
prvním	první	k4xOgMnSc7
Evropanem	Evropan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Ameriku	Amerika	k1gFnSc4
–	–	k?
o	o	k7c4
pět	pět	k4xCc4
století	století	k1gNnPc2
dříve	dříve	k6eAd2
jejích	její	k3xOp3gInPc2
břehů	břeh	k1gInPc2
dosáhla	dosáhnout	k5eAaPmAgFnS
norská	norský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Leifem	Leif	k1gInSc7
Erikssonem	Eriksson	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
založila	založit	k5eAaPmAgFnS
kolonii	kolonie	k1gFnSc4
na	na	k7c6
dnešním	dnešní	k2eAgInSc6d1
Newfoundlandu	Newfoundland	k1gInSc6
<g/>
.	.	kIx.
</s>