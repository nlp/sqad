<s>
Kačice	kačice	k1gFnSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
obci	obec	k1gFnSc6
v	v	k7c6
polské	polský	k2eAgFnSc6d1
části	část	k1gFnSc6
Těšínska	Těšínsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
vesnici	vesnice	k1gFnSc6
ve	v	k7c6
středních	střední	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Kačice	kačice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kačice	kačice	k1gFnSc1
Kaczyce	Kaczyce	k1gFnSc2
Hornické	hornický	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
v	v	k7c4
KačicíchPoloha	KačicíchPoloh	k1gMnSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
4	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
vojvodství	vojvodství	k1gNnSc6
</s>
<s>
Slezské	Slezská	k1gFnPc1
okres	okres	k1gInSc1
</s>
<s>
Těšín	Těšín	k1gInSc1
</s>
<s>
Kačice	kačice	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
9,27	9,27	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
3	#num#	k4
173	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
342,3	342,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Etnické	etnický	k2eAgNnSc4d1
složení	složení	k1gNnSc4
</s>
<s>
Poláci	Polák	k1gMnPc1
<g/>
,	,	kIx,
Slezané	Slezan	k1gMnPc1
Náboženské	náboženský	k2eAgFnSc2d1
složení	složení	k1gNnSc3
</s>
<s>
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
<g/>
,	,	kIx,
luteráni	luterán	k1gMnPc1
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
starostenství	starostenství	k1gNnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
Mokswik	Mokswik	k1gInSc4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
32	#num#	k4
PSČ	PSČ	kA
</s>
<s>
43-417	43-417	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
SCI	SCI	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kačice	kačice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Kaczyce	Kaczyce	k1gFnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Katschitz	Katschitz	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
vesnice	vesnice	k1gFnPc4
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Polsku	Polsko	k1gNnSc6
ve	v	k7c6
Slezském	slezský	k2eAgNnSc6d1
vojvodství	vojvodství	k1gNnSc6
v	v	k7c6
okrese	okres	k1gInSc6
Těšín	Těšín	k1gInSc1
v	v	k7c6
gmině	gmina	k1gFnSc6
Žibřidovice	Žibřidovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Těšínského	Těšínského	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
přímo	přímo	k6eAd1
u	u	k7c2
českých	český	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
–	–	k?
na	na	k7c6
západě	západ	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
Rájem	ráj	k1gInSc7
<g/>
,	,	kIx,
městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
Karviné	Karviná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
tří	tři	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
severních	severní	k2eAgFnPc2d1
Dolních	dolní	k2eAgFnPc2d1
Kačic	kačice	k1gFnPc2
(	(	kIx(
<g/>
neboli	neboli	k8xC
Podsviňošova	Podsviňošův	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
centrálních	centrální	k2eAgFnPc2d1
Horních	horní	k2eAgFnPc2d1
Kačic	kačice	k1gFnPc2
a	a	k8xC
jihozápadního	jihozápadní	k2eAgInSc2d1
Otrubkova	Otrubkův	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
3	#num#	k4
173	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
rozloha	rozloha	k1gFnSc1
obce	obec	k1gFnSc2
činí	činit	k5eAaImIp3nS
9,27	9,27	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
Kačice	kačice	k1gFnSc2
je	být	k5eAaImIp3nS
patronymického	patronymický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
odvozený	odvozený	k2eAgMnSc1d1
od	od	k7c2
jména	jméno	k1gNnSc2
rodiny	rodina	k1gFnSc2
Kačových	Kačův	k2eAgMnPc2d1
<g/>
,	,	kIx,
pravděpodobných	pravděpodobný	k2eAgMnPc2d1
zakladatelů	zakladatel	k1gMnPc2
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
ní	on	k3xPp3gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1332	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdějšími	pozdní	k2eAgMnPc7d2
majiteli	majitel	k1gMnPc7
obce	obec	k1gFnSc2
byli	být	k5eAaImAgMnP
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Spensové	Spensová	k1gFnSc2
z	z	k7c2
Boodenu	Booden	k1gInSc2
a	a	k8xC
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
konce	konec	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Larischové	Larischová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
Těšínska	Těšínsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
byly	být	k5eAaImAgInP
Kačice	kačice	k1gFnPc4
rozhodnutím	rozhodnutí	k1gNnSc7
Konference	konference	k1gFnSc2
velvyslanců	velvyslanec	k1gMnPc2
připojeny	připojen	k2eAgFnPc1d1
k	k	k7c3
Polsku	Polsko	k1gNnSc3
a	a	k8xC
tím	ten	k3xDgNnSc7
odtrženy	odtrhnout	k5eAaPmNgFnP
od	od	k7c2
nejbližšího	blízký	k2eAgNnSc2d3
městského	městský	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
–	–	k?
Fryštátu	Fryštát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
je	být	k5eAaImIp3nS
dřevěný	dřevěný	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Povýšení	povýšení	k1gNnSc2
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1620	#num#	k4
postavený	postavený	k2eAgInSc1d1
původně	původně	k6eAd1
v	v	k7c6
Ruptawě	Ruptawa	k1gFnSc6
<g/>
,	,	kIx,
čtvrti	čtvrt	k1gFnSc6
Jastrzębie-Zdroje	Jastrzębie-Zdroj	k1gInSc2
a	a	k8xC
přenesený	přenesený	k2eAgMnSc1d1
do	do	k7c2
Kačic	kačice	k1gFnPc2
v	v	k7c6
letech	let	k1gInPc6
1971	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
Kačic	kačice	k1gFnPc2
je	být	k5eAaImIp3nS
geomorfologicky	geomorfologicky	k6eAd1
součástí	součást	k1gFnSc7
Ostravské	ostravský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
a	a	k8xC
přímo	přímo	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
ostravsko-karvinský	ostravsko-karvinský	k2eAgInSc4d1
uhelný	uhelný	k2eAgInSc4d1
revír	revír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusné	pokusný	k2eAgInPc1d1
vrty	vrt	k1gInPc1
zde	zde	k6eAd1
probíhaly	probíhat	k5eAaImAgInP
od	od	k7c2
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
zahájení	zahájení	k1gNnSc3
výstavby	výstavba	k1gFnSc2
dolu	dol	k1gInSc2
však	však	k9
došlo	dojít	k5eAaPmAgNnS
až	až	k9
na	na	k7c6
konci	konec	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černouhelný	černouhelný	k2eAgInSc1d1
Důl	důl	k1gInSc1
Morcinek	Morcinek	k1gInSc1
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
se	se	k3xPyFc4
však	však	k9
potýkal	potýkat	k5eAaImAgMnS
s	s	k7c7
ekonomickými	ekonomický	k2eAgFnPc7d1
potížemi	potíž	k1gFnPc7
a	a	k8xC
po	po	k7c6
desíti	deset	k4xCc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
konstatována	konstatován	k2eAgFnSc1d1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
trvala	trvat	k5eAaImAgFnS
ztrátovost	ztrátovost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1998	#num#	k4
až	až	k8xS
2001	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
likvidace	likvidace	k1gFnSc1
dolu	dol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
likvidace	likvidace	k1gFnSc1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
neuváženou	uvážený	k2eNgFnSc4d1
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
důkazem	důkaz	k1gInSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zájem	zájem	k1gInSc4
o	o	k7c4
vydobytí	vydobytí	k1gNnSc4
uhelných	uhelný	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
dolu	dol	k1gInSc2
Morcinek	Morcinek	k1gInSc4
z	z	k7c2
české	český	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
z	z	k7c2
dolu	dol	k1gInSc2
ČSM	ČSM	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Kačicích	kačice	k1gFnPc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc1d1
zastávka	zastávka	k1gFnSc1
na	na	k7c6
trati	trať	k1gFnSc6
Těšín	Těšín	k1gInSc1
–	–	k?
Žibřidovice	Žibřidovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vesnice	vesnice	k1gFnSc2
dojíždějí	dojíždět	k5eAaImIp3nP
autobusy	autobus	k1gInPc1
linky	linka	k1gFnSc2
č.	č.	k?
32	#num#	k4
těšínské	těšínské	k1gNnSc4
MHD	MHD	kA
<g/>
,	,	kIx,
u	u	k7c2
hranic	hranice	k1gFnPc2
obce	obec	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
konečná	konečná	k1gFnSc1
linky	linka	k1gFnSc2
č.	č.	k?
514	#num#	k4
MHD	MHD	kA
Karviná	Karviná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HOSÁK	HOSÁK	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
místopis	místopis	k1gInSc1
Země	zem	k1gFnSc2
moravskoslezské	moravskoslezský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
,	,	kIx,
reprint	reprint	k1gInSc1
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1225	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
887	#num#	k4
<g/>
,	,	kIx,
920	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jana-Kartas	Jana-Kartas	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
728	#num#	k4
<g/>
↑	↑	k?
DAVÍDEK	Davídek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
názvech	název	k1gInPc6
a	a	k8xC
jménech	jméno	k1gNnPc6
Těšínska	Těšínsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
<g/>
:	:	kIx,
Slezský	slezský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DAVÍDEK	Davídek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
názvech	název	k1gInPc6
a	a	k8xC
jménech	jméno	k1gNnPc6
Těšínska	Těšínsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
<g/>
:	:	kIx,
Slezský	slezský	k2eAgInSc1d1
studijní	studijní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARTOSZEK	BARTOSZEK	kA
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ludwik	Ludwik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wypisy	Wypis	k1gInPc7
z	z	k7c2
dziejów	dziejów	k?
Zebrzydowic	Zebrzydowice	k1gFnPc2
<g/>
,	,	kIx,
Kaczyc	Kaczyc	k1gFnSc1
<g/>
,	,	kIx,
Kończyc	Kończyc	k1gFnSc1
Małych	Małych	k1gMnSc1
i	i	k8xC
Marklowic	Marklowic	k1gMnSc1
Górnych	Górnych	k1gMnSc1
<g/>
.	.	kIx.
1305	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žibřidovice	Žibřidovice	k1gFnSc1
<g/>
:	:	kIx,
Gminny	Gminna	k1gFnPc1
Ośrodek	Ośrodek	k1gInSc4
Kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
83	#num#	k4
<g/>
-	-	kIx~
<g/>
908896	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Webový	webový	k2eAgInSc1d1
portál	portál	k1gInSc1
Kačic	kačice	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
Kačicích	kačice	k1gFnPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kaczyce	Kaczyce	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
Těšínska	Těšínsko	k1gNnSc2
Česko	Česko	k1gNnSc1
</s>
<s>
Albrechtice	Albrechtice	k1gFnPc1
(	(	kIx(
<g/>
Olbrachcice	Olbrachcice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bocanovice	Bocanovice	k1gFnSc1
(	(	kIx(
<g/>
Boconowice	Boconowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bohumín	Bohumín	k1gInSc1
(	(	kIx(
<g/>
Bogumin	Bogumin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bruzovice	Bruzovice	k1gFnSc1
(	(	kIx(
<g/>
Bruzowice	Bruzowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bukovec	Bukovec	k1gInSc1
(	(	kIx(
<g/>
Bukowiec	Bukowiec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bystřice	Bystřice	k1gFnSc1
(	(	kIx(
<g/>
Bystrzyca	Bystrzyca	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc4d1
Těšín	Těšín	k1gInSc4
(	(	kIx(
<g/>
Czeski	Czeske	k1gFnSc4
Cieszyn	Cieszyna	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dětmarovice	Dětmarovice	k1gFnSc1
(	(	kIx(
<g/>
Dziećmorowice	Dziećmorowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dobrá	dobrá	k1gFnSc1
(	(	kIx(
<g/>
Dobra	dobro	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Dobratice	Dobratice	k1gFnSc1
(	(	kIx(
<g/>
Dobracice	Dobracice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Domaslavice	Domaslavice	k1gFnSc1
(	(	kIx(
<g/>
Domasłowice	Domasłowice	k1gFnSc1
Dolne	Doln	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Lomná	lomný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Łomna	Łomen	k2eAgFnSc1d1
Dolna	Dolna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dolní	dolní	k2eAgFnSc2d1
Lutyně	Lutyně	k1gFnSc2
(	(	kIx(
<g/>
Lutynia	Lutynium	k1gNnSc2
Dolna	Doln	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Tošanovice	Tošanovice	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Toszonowice	Toszonowice	k1gFnSc1
Dolne	Doln	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Doubrava	Doubrava	k1gFnSc1
(	(	kIx(
<g/>
Dąbrowa	Dąbrowa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Havířov	Havířov	k1gInSc1
(	(	kIx(
<g/>
Hawierzów	Hawierzów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Hnojník	hnojník	k1gInSc1
(	(	kIx(
<g/>
Gnojnik	Gnojnik	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnPc4d1
Bludovice	Bludovice	k1gFnPc4
(	(	kIx(
<g/>
Błędowice	Błędowice	k1gFnPc4
Górne	Górn	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnSc1d1
Domaslavice	Domaslavice	k1gFnSc1
(	(	kIx(
<g/>
Domasłowice	Domasłowice	k1gFnSc1
Górne	Górn	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnSc1d1
Lomná	lomný	k2eAgFnSc1d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Łomna	Łomna	k1gFnSc1
Górna	Górna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnSc1d1
Suchá	Suchá	k1gFnSc1
(	(	kIx(
<g/>
Sucha	sucho	k1gNnSc2
Górna	Górn	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnSc1d1
Tošanovice	Tošanovice	k1gFnSc1
(	(	kIx(
<g/>
Toszonowice	Toszonowice	k1gFnSc1
Górne	Górn	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Hrádek	hrádek	k1gInSc1
(	(	kIx(
<g/>
Gródek	Gródek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hrčava	Hrčava	k1gFnSc1
(	(	kIx(
<g/>
Herczawa	Herczawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Chotěbuz	Chotěbuz	k1gFnSc1
(	(	kIx(
<g/>
Kocobędz	Kocobędz	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Jablunkov	Jablunkov	k1gInSc1
(	(	kIx(
<g/>
Jabłonków	Jabłonków	k1gFnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Janovice	Janovice	k1gFnPc1
(	(	kIx(
<g/>
Janowice	Janowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kaňovice	Kaňovice	k1gFnSc1
(	(	kIx(
<g/>
Kaniowice	Kaniowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Karviná	Karviná	k1gFnSc1
(	(	kIx(
<g/>
Karwina	Karwina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Komorní	komorní	k2eAgFnSc1d1
Lhotka	Lhotka	k1gFnSc1
(	(	kIx(
<g/>
Ligotka	ligotka	k1gFnSc1
Kameralna	Kameralna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Košařiska	Košařiska	k1gFnSc1
(	(	kIx(
<g/>
Koszarzyska	Koszarzyska	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Krásná	krásný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Krasna	Krasna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Lučina	lučina	k1gFnSc1
(	(	kIx(
<g/>
Łucyna	Łucyna	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Malenovice	Malenovice	k1gFnSc1
(	(	kIx(
<g/>
Malenowice	Malenowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Milíkov	Milíkov	k1gInSc1
(	(	kIx(
<g/>
Milików	Milików	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Morávka	Morávek	k1gMnSc2
(	(	kIx(
<g/>
Morawka	Morawka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mosty	most	k1gInPc1
u	u	k7c2
Jablunkova	Jablunkův	k2eAgInSc2d1
(	(	kIx(
<g/>
Mosty	most	k1gInPc4
koło	koło	k6eAd1
Jabłunkowa	Jabłunkow	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Návsí	náves	k1gFnPc2
(	(	kIx(
<g/>
Nawsie	Nawsie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nižní	nižní	k2eAgFnSc2d1
Lhoty	Lhota	k1gFnSc2
(	(	kIx(
<g/>
Ligota	Ligota	k1gFnSc1
Dolna	Dolna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Nošovice	Nošovice	k1gFnSc1
(	(	kIx(
<g/>
Noszowice	Noszowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nýdek	Nýdek	k1gInSc1
(	(	kIx(
<g/>
Nydek	Nydek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Orlová	Orlová	k1gFnSc1
(	(	kIx(
<g/>
Orłowa	Orłowa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pazderna	pazderna	k1gFnSc1
(	(	kIx(
<g/>
Październa	Październa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Petrovice	Petrovice	k1gFnPc4
u	u	k7c2
Karviné	Karviná	k1gFnSc2
(	(	kIx(
<g/>
Piotrowice	Piotrowic	k1gMnSc2
koło	koło	k1gMnSc1
Karwiny	Karwina	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Petřvald	Petřvald	k1gInSc1
(	(	kIx(
<g/>
Pietwałd	Pietwałd	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Písečná	písečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pioseczna	Pioseczna	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Písek	Písek	k1gInSc1
(	(	kIx(
<g/>
Piosek	Piosek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Pražmo	pražmo	k1gNnSc1
(	(	kIx(
<g/>
Prażmo	Prażma	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Pržno	Pržno	k1gNnSc1
(	(	kIx(
<g/>
Prżno	Prżno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Raškovice	Raškovice	k1gFnSc1
(	(	kIx(
<g/>
Raszkowice	Raszkowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ropice	Ropice	k1gFnSc1
(	(	kIx(
<g/>
Ropica	Ropica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rychvald	Rychvald	k1gInSc1
(	(	kIx(
<g/>
Rychwałd	Rychwałd	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Řeka	řeka	k1gFnSc1
(	(	kIx(
<g/>
Rzeka	Rzeka	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Sedliště	sedliště	k1gNnSc1
(	(	kIx(
<g/>
Siedliszcze	Siedliszcze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Smilovice	Smilovice	k1gFnSc1
(	(	kIx(
<g/>
Śmiłowice	Śmiłowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Soběšovice	Soběšovice	k1gFnSc1
(	(	kIx(
<g/>
Szobiszowice	Szobiszowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
Stare	star	k1gInSc5
Miasto	Miasta	k1gMnSc5
<g/>
)	)	kIx)
•	•	k?
Stonava	Stonava	k1gFnSc1
(	(	kIx(
<g/>
Stonawa	Stonawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Střítež	Střítež	k1gFnSc1
(	(	kIx(
<g/>
Trzycież	Trzycież	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Šenov	Šenov	k1gInSc1
(	(	kIx(
<g/>
Szonów	Szonów	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Těrlicko	Těrlicko	k1gNnSc1
(	(	kIx(
<g/>
Cierlicko	Cierlicko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Třanovice	Třanovice	k1gFnSc1
(	(	kIx(
<g/>
Trzanowice	Trzanowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Třinec	Třinec	k1gInSc1
(	(	kIx(
<g/>
Trzyniec	Trzyniec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Václavovice	Václavovice	k1gFnSc1
(	(	kIx(
<g/>
Więcłowice	Więcłowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vělopolí	Vělopolí	k1gNnSc1
(	(	kIx(
<g/>
Wielopole	Wielopole	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vendryně	Vendryně	k1gFnSc2
(	(	kIx(
<g/>
Wędrynia	Wędrynium	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Vojkovice	Vojkovice	k1gFnSc1
(	(	kIx(
<g/>
Wojkowice	Wojkowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vyšní	vyšní	k2eAgFnSc2d1
Lhoty	Lhota	k1gFnSc2
(	(	kIx(
<g/>
Ligota	Ligota	k1gFnSc1
Górna	Górna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Žermanice	Žermanice	k1gInPc4
(	(	kIx(
<g/>
Żermanice	Żermanice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
Těšínsku	Těšínsko	k1gNnSc3
částečně	částečně	k6eAd1
<g/>
:	:	kIx,
Baška	Baška	k1gFnSc1
(	(	kIx(
<g/>
Baszka	Baszka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Frýdek-Místek	Frýdek-Místka	k1gFnPc2
(	(	kIx(
<g/>
Frydek-Mistek	Frydek-Mistka	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Frýdlant	Frýdlant	k1gInSc1
nad	nad	k7c7
Ostravicí	Ostravice	k1gFnSc7
(	(	kIx(
<g/>
Frydlant	Frydlant	k1gMnSc1
nad	nad	k7c7
Ostrawicą	Ostrawicą	k1gFnSc7
<g/>
)	)	kIx)
•	•	k?
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Ostrawa	Ostrawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ostravice	Ostravice	k1gFnSc1
(	(	kIx(
<g/>
Ostrawica	Ostrawica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Řepiště	řepiště	k1gNnSc1
(	(	kIx(
<g/>
Rzepiszcze	Rzepiszcze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Staré	Staré	k2eAgInPc1d1
Hamry	Hamry	k1gInPc1
(	(	kIx(
<g/>
Stare	star	k1gInSc5
Hamry	Hamry	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
Sviadnov	Sviadnov	k1gInSc1
(	(	kIx(
<g/>
Świadnów	Świadnów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vratimov	Vratimov	k1gInSc1
(	(	kIx(
<g/>
Racimów	Racimów	k1gFnSc1
<g/>
)	)	kIx)
Polsko	Polsko	k1gNnSc1
</s>
<s>
Bažanovice	Bažanovice	k1gFnSc1
(	(	kIx(
<g/>
Bażanowice	Bażanowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bělovicko	Bělovicko	k1gNnSc1
(	(	kIx(
<g/>
Bielowicko	Bielowicko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Běry	Běra	k1gMnSc2
(	(	kIx(
<g/>
Biery	Biera	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Blatnice	Blatnice	k1gFnPc1
(	(	kIx(
<g/>
Bładnice	Bładnice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bonkov	Bonkov	k1gInSc1
(	(	kIx(
<g/>
Bąków	Bąków	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Brenná	Brenný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Brenna	Brenna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Brúnov	Brúnov	k1gInSc1
(	(	kIx(
<g/>
Bronów	Bronów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Březůvka	Březůvka	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Brzezówka	Brzezówka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Čechovice-Dědice	Čechovice-Dědice	k1gFnSc1
(	(	kIx(
<g/>
Czechowice-Dziedzice	Czechowice-Dziedzice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Děhylov	Děhylov	k1gInSc1
(	(	kIx(
<g/>
Dzięgielów	Dzięgielów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dolní	dolní	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
(	(	kIx(
<g/>
Międzyrzecze	Międzyrzecze	k1gFnSc1
Dolne	Doln	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Dubovec	Dubovec	k1gInSc1
(	(	kIx(
<g/>
Dębowiec	Dębowiec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Drahomyšl	Drahomyšl	k1gInSc1
(	(	kIx(
<g/>
Drogomyśl	Drogomyśl	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Frelichov	Frelichov	k1gInSc1
(	(	kIx(
<g/>
Frelichów	Frelichów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Harbutovice	Harbutovice	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Harbutowice	Harbutowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Hažlach	Hažlach	k1gInSc1
(	(	kIx(
<g/>
Hażlach	Hażlach	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hodišov	Hodišov	k1gInSc1
(	(	kIx(
<g/>
Godziszów	Godziszów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Holešov	Holešov	k1gInSc1
(	(	kIx(
<g/>
Goleszów	Goleszów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnSc1d1
Líštná	Líštný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leszna	Leszna	k1gFnSc1
Górna	Górna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgFnSc1d1
Marklovice	Marklovice	k1gFnSc1
(	(	kIx(
<g/>
Marklowice	Marklowice	k1gFnSc1
Górne	Górn	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Horní	horní	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
(	(	kIx(
<g/>
Międzyrzecze	Międzyrzecze	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Górne	Górn	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Hradec	Hradec	k1gInSc1
(	(	kIx(
<g/>
Grodziec	Grodziec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Humna	humno	k1gNnSc2
(	(	kIx(
<g/>
Gumna	Gumna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Chyby	chyba	k1gFnPc1
(	(	kIx(
<g/>
Chybie	Chybie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jistebná	Jistebný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Istebna	Istebna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jasenice	Jasenice	k1gFnSc1
(	(	kIx(
<g/>
Jasienica	Jasienica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Javoří	javoří	k1gNnSc1
(	(	kIx(
<g/>
Jaworze	Jaworze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Javořinka	Javořinka	k1gFnSc1
(	(	kIx(
<g/>
Jaworzynka	Jaworzynka	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Jilovnice	Jilovnice	k1gFnSc1
(	(	kIx(
<g/>
Iłownica	Iłownica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jiskřičín	Jiskřičín	k1gInSc1
(	(	kIx(
<g/>
Iskrzyczyn	Iskrzyczyn	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kačice	kačice	k1gFnSc1
(	(	kIx(
<g/>
Kaczyce	Kaczyce	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Koňákov	Koňákov	k1gInSc1
(	(	kIx(
<g/>
Koniaków	Koniaków	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kostkovice	Kostkovice	k1gFnSc1
(	(	kIx(
<g/>
Kostkowice	Kostkowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kovále	Kovála	k1gFnSc6
(	(	kIx(
<g/>
Kowale	Kowala	k1gFnSc6
<g/>
)	)	kIx)
•	•	k?
Kozákovice	Kozákovice	k1gFnSc1
(	(	kIx(
<g/>
Kozakowice	Kozakowice	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Kyčice	Kyčice	k1gFnSc1
(	(	kIx(
<g/>
Kiczyce	Kiczyce	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kyselov	Kyselov	k1gInSc1
(	(	kIx(
<g/>
Kisielów	Kisielów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Landek	Landek	k1gInSc1
(	(	kIx(
<g/>
Landek	Landek	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Lazy	Lazy	k?
(	(	kIx(
<g/>
Łazy	Łaza	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Lhota	Lhota	k1gFnSc1
(	(	kIx(
<g/>
Ligota	Ligota	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Loučka	loučka	k1gFnSc1
(	(	kIx(
<g/>
Łączka	Łączka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Malé	Malé	k2eAgFnSc2d1
Hůrky	hůrka	k1gFnSc2
(	(	kIx(
<g/>
Górki	Górki	k1gNnSc1
Małe	Mał	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Malé	Malé	k2eAgFnPc1d1
Kunčice	Kunčice	k1gFnPc1
(	(	kIx(
<g/>
Kończyce	Kończyce	k1gFnSc1
Małe	Małe	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mazančovice	Mazančovice	k1gFnSc1
(	(	kIx(
<g/>
Mazańcowice	Mazańcowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mezisvětí	Mezisvětí	k1gNnSc1
(	(	kIx(
<g/>
Międzyświeć	Międzyświeć	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mnich	mnich	k1gMnSc1
(	(	kIx(
<g/>
Mnich	mnich	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Ohrazená	ohrazený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ogrodzona	Ogrodzona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ochaby	Ochaba	k1gMnSc2
(	(	kIx(
<g/>
Ochaby	Ochaba	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Pohoří	pohoří	k1gNnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Pogórze	Pogórze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pohvizdov	Pohvizdov	k1gInSc1
(	(	kIx(
<g/>
Pogwizdów	Pogwizdów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prstec	prstec	k1gInSc1
(	(	kIx(
<g/>
Pierściec	Pierściec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Pruchná	Pruchná	k1gFnSc1
(	(	kIx(
<g/>
Pruchna	Pruchna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Puncov	Puncov	k1gInSc1
(	(	kIx(
<g/>
Puńców	Puńców	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Roztropice	Roztropice	k1gFnSc1
(	(	kIx(
<g/>
Roztropice	Roztropice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rudice	Rudice	k1gFnPc1
(	(	kIx(
<g/>
Rudzica	Rudzica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rudník	rudník	k1gMnSc1
(	(	kIx(
<g/>
Rudnik	Rudnik	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Semorad	Semorad	k1gInSc1
(	(	kIx(
<g/>
Simoradz	Simoradz	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Skočov	Skočov	k1gInSc1
(	(	kIx(
<g/>
Skoczów	Skoczów	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Strumeň	strumeň	k1gInSc1
(	(	kIx(
<g/>
Strumień	Strumień	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Svěntošůvka	Svěntošůvka	k1gFnSc1
(	(	kIx(
<g/>
Świętoszówka	Świętoszówka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Těšín	Těšín	k1gInSc1
(	(	kIx(
<g/>
Cieszyn	Cieszyn	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tisovnice	Tisovnice	k1gFnSc1
(	(	kIx(
<g/>
Cisownica	Cisownica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ustroň	Ustroň	k1gFnSc1
(	(	kIx(
<g/>
Ustroń	Ustroń	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Velké	velký	k2eAgFnSc2d1
Hůrky	hůrka	k1gFnSc2
(	(	kIx(
<g/>
Górki	Górk	k1gFnSc2
Wielkie	Wielkie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Velké	velký	k2eAgFnPc1d1
Kunčice	Kunčice	k1gFnPc1
(	(	kIx(
<g/>
Kończyce	Kończyce	k1gFnSc1
Wielkie	Wielkie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Věščút	Věščút	k1gInSc1
(	(	kIx(
<g/>
Wieszczęta	Wieszczęta	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vilémovice	Vilémovice	k1gFnSc1
(	(	kIx(
<g/>
Wilamowice	Wilamowice	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Visla	Visla	k1gFnSc1
(	(	kIx(
<g/>
Wisła	Wisła	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vislice	Vislice	k1gFnSc1
(	(	kIx(
<g/>
Wiślica	Wiślica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Záblatí	Záblatí	k1gNnSc1
(	(	kIx(
<g/>
Zabłocie	Zabłocie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Záboří	Záboří	k1gNnSc1
(	(	kIx(
<g/>
Zaborze	Zaborze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
(	(	kIx(
<g/>
Zabrzeg	Zabrzeg	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Zámrsk	Zámrsk	k1gInSc1
(	(	kIx(
<g/>
Zamarski	Zamarski	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Záříčí	Záříčí	k1gNnSc1
(	(	kIx(
<g/>
Zarzecze	Zarzecze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zbytky	zbytek	k1gInPc1
(	(	kIx(
<g/>
Zbytków	Zbytków	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Žibřidovice	Žibřidovice	k1gFnSc1
(	(	kIx(
<g/>
Zebrzydowice	Zebrzydowice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
Těšínsku	Těšínsko	k1gNnSc3
částečně	částečně	k6eAd1
<g/>
:	:	kIx,
Bílsko-Bělá	Bílsko-Bělý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bielsko-Biała	Bielsko-Biała	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bystrá	bystrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bystra	Bystra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slezsko	Slezsko	k1gNnSc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
</s>
