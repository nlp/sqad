<s>
Stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ag	Ag	k1gFnSc2	Ag
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Argentum	argentum	k1gNnSc4	argentum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
kov	kov	k1gInSc4	kov
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
a	a	k8xC	a
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
vodivostí	vodivost	k1gFnSc7	vodivost
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc6	výroba
CD	CD	kA	CD
i	i	k8xC	i
DVD	DVD	kA	DVD
nosičů	nosič	k1gInPc2	nosič
a	a	k8xC	a
šperkařství	šperkařství	k1gNnPc2	šperkařství
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgFnPc1d1	nezbytná
pro	pro	k7c4	pro
fotografický	fotografický	k2eAgInSc4d1	fotografický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
i	i	k8xC	i
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mechanické	mechanický	k2eAgFnSc6d1	mechanická
a	a	k8xC	a
metalurgické	metalurgický	k2eAgFnSc6d1	metalurgická
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zpracovatelné	zpracovatelný	k2eAgNnSc1d1	zpracovatelné
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
kujnost	kujnost	k1gFnSc4	kujnost
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
odlévá	odlévat	k5eAaImIp3nS	odlévat
(	(	kIx(	(
<g/>
dobrá	dobrý	k2eAgFnSc1d1	dobrá
zatékavost	zatékavost	k1gFnSc1	zatékavost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-orbitalu	drbital	k1gMnSc6	d-orbital
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
+1	+1	k4	+1
<g/>
,	,	kIx,	,
sloučeniny	sloučenina	k1gFnPc1	sloučenina
dvojmocného	dvojmocný	k2eAgNnSc2d1	dvojmocné
stříbra	stříbro	k1gNnSc2	stříbro
jsou	být	k5eAaImIp3nP	být
nestálé	stálý	k2eNgInPc1d1	nestálý
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
silné	silný	k2eAgFnPc4d1	silná
oxidační	oxidační	k2eAgFnPc4d1	oxidační
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Kationt	kationt	k1gInSc1	kationt
trojmocného	trojmocný	k2eAgNnSc2d1	trojmocné
stříbra	stříbro	k1gNnSc2	stříbro
Ag	Ag	k1gFnSc2	Ag
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
nestálosti	nestálost	k1gFnSc2	nestálost
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
velkými	velký	k2eAgInPc7d1	velký
anionty	anion	k1gInPc7	anion
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
stříbro	stříbro	k1gNnSc4	stříbro
řazeno	řazen	k2eAgNnSc4d1	řazeno
mezi	mezi	k7c4	mezi
drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
značnou	značný	k2eAgFnSc7d1	značná
chemickou	chemický	k2eAgFnSc7d1	chemická
stabilitou	stabilita	k1gFnSc7	stabilita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
především	především	k6eAd1	především
díky	díky	k7c3	díky
jejím	její	k3xOp3gFnPc3	její
silným	silný	k2eAgFnPc3d1	silná
oxidačním	oxidační	k2eAgFnPc3d1	oxidační
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
3	[number]	k4	3
Ag	Ag	k1gFnPc2	Ag
+	+	kIx~	+
4	[number]	k4	4
HNO3	HNO3	k1gFnSc2	HNO3
→	→	k?	→
3	[number]	k4	3
AgNO	AgNO	k1gFnSc1	AgNO
<g/>
3	[number]	k4	3
+	+	kIx~	+
NO	no	k0	no
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
stříbro	stříbro	k1gNnSc1	stříbro
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
vůči	vůči	k7c3	vůči
koncentrované	koncentrovaný	k2eAgFnSc3d1	koncentrovaná
kyselině	kyselina	k1gFnSc3	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
taktéž	taktéž	k?	taktéž
oxidačně	oxidačně	k6eAd1	oxidačně
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
zředěné	zředěný	k2eAgFnSc3d1	zředěná
H2SO4	H2SO4	k1gFnSc3	H2SO4
je	být	k5eAaImIp3nS	být
však	však	k9	však
stříbro	stříbro	k1gNnSc1	stříbro
netečné	netečný	k2eAgNnSc1d1	netečné
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
dalších	další	k2eAgFnPc2d1	další
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
se	se	k3xPyFc4	se
stříbro	stříbro	k1gNnSc1	stříbro
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
i	i	k9	i
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
kyanidů	kyanid	k1gInPc2	kyanid
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyanostříbrnanového	kyanostříbrnanový	k2eAgInSc2d1	kyanostříbrnanový
iontu	ion	k1gInSc2	ion
[	[	kIx(	[
<g/>
Ag	Ag	k1gMnSc1	Ag
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
čistém	čistý	k2eAgInSc6d1	čistý
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
stříbro	stříbro	k1gNnSc1	stříbro
neomezeně	omezeně	k6eNd1	omezeně
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
však	však	k9	však
i	i	k9	i
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
množství	množství	k1gNnSc1	množství
sulfanu	sulfan	k1gInSc2	sulfan
(	(	kIx(	(
<g/>
sirovodíku	sirovodík	k1gInSc2	sirovodík
<g/>
)	)	kIx)	)
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stříbro	stříbro	k1gNnSc1	stříbro
začalo	začít	k5eAaPmAgNnS	začít
černat	černat	k5eAaImF	černat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
vzniká	vznikat	k5eAaImIp3nS	vznikat
vrstva	vrstva	k1gFnSc1	vrstva
sulfidu	sulfid	k1gInSc2	sulfid
stříbrného	stříbrný	k2eAgMnSc4d1	stříbrný
Ag	Ag	k1gMnSc4	Ag
<g/>
2	[number]	k4	2
<g/>
S.	S.	kA	S.
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
stříbro	stříbro	k1gNnSc1	stříbro
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
0,07	[number]	k4	0,07
<g/>
-	-	kIx~	-
<g/>
0,1	[number]	k4	0,1
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
mikrogramy	mikrogram	k1gInPc4	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
stříbra	stříbro	k1gNnSc2	stříbro
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
bilion	bilion	k4xCgInSc4	bilion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
stříbro	stříbro	k1gNnSc1	stříbro
obvykle	obvykle	k6eAd1	obvykle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
však	však	k9	však
i	i	k9	i
jako	jako	k8xS	jako
ryzí	ryzí	k2eAgInSc1d1	ryzí
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
stříbro	stříbro	k1gNnSc1	stříbro
příměsí	příměs	k1gFnPc2	příměs
v	v	k7c6	v
ryzím	ryzí	k2eAgNnSc6d1	ryzí
přírodním	přírodní	k2eAgNnSc6d1	přírodní
zlatě	zlato	k1gNnSc6	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
stříbra	stříbro	k1gNnSc2	stříbro
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
akantit	akantit	k1gInSc1	akantit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vysokoteplotní	vysokoteplotní	k2eAgFnSc1d1	vysokoteplotní
modifikace	modifikace	k1gFnSc1	modifikace
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
179	[number]	k4	179
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
argentit	argentit	k1gInSc1	argentit
Ag	Ag	k1gFnSc2	Ag
<g/>
2	[number]	k4	2
<g/>
S.	S.	kA	S.
Jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
získávání	získávání	k1gNnSc4	získávání
stříbra	stříbro	k1gNnSc2	stříbro
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
rudy	ruda	k1gFnPc1	ruda
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
nebo	nebo	k8xC	nebo
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
používanou	používaný	k2eAgFnSc7d1	používaná
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
získávání	získávání	k1gNnSc4	získávání
i	i	k8xC	i
čištění	čištění	k1gNnSc4	čištění
ryzího	ryzí	k2eAgNnSc2d1	ryzí
stříbra	stříbro	k1gNnSc2	stříbro
je	být	k5eAaImIp3nS	být
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
<g/>
,	,	kIx,	,
z	z	k7c2	z
halogenidů	halogenid	k1gInPc2	halogenid
je	být	k5eAaImIp3nS	být
však	však	k9	však
možno	možno	k6eAd1	možno
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
ryzí	ryzí	k2eAgNnSc1d1	ryzí
získat	získat	k5eAaPmF	získat
i	i	k9	i
pyrometalurgicky	pyrometalurgicky	k6eAd1	pyrometalurgicky
přímým	přímý	k2eAgNnSc7d1	přímé
tavením	tavení	k1gNnSc7	tavení
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
světovými	světový	k2eAgMnPc7d1	světový
producenty	producent	k1gMnPc7	producent
stříbra	stříbro	k1gNnSc2	stříbro
jsou	být	k5eAaImIp3nP	být
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
dobývalo	dobývat	k5eAaImAgNnS	dobývat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
lokalitou	lokalita	k1gFnSc7	lokalita
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
kromě	kromě	k7c2	kromě
dobývání	dobývání	k1gNnSc2	dobývání
a	a	k8xC	a
rafinace	rafinace	k1gFnSc2	rafinace
stříbra	stříbro	k1gNnSc2	stříbro
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
královská	královský	k2eAgFnSc1d1	královská
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
raženy	razit	k5eAaImNgInP	razit
známé	známý	k2eAgInPc1d1	známý
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
groše	groš	k1gInPc1	groš
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
lokality	lokalita	k1gFnPc4	lokalita
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
rud	ruda	k1gFnPc2	ruda
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc1d1	elementární
stříbro	stříbro	k1gNnSc1	stříbro
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
využíváno	využívat	k5eAaImNgNnS	využívat
především	především	k9	především
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
unikátní	unikátní	k2eAgFnPc4d1	unikátní
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
-	-	kIx~	-
vynikající	vynikající	k2eAgFnSc4d1	vynikající
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
a	a	k8xC	a
teplotní	teplotní	k2eAgFnSc4d1	teplotní
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
dobrou	dobrý	k2eAgFnSc4d1	dobrá
chemickou	chemický	k2eAgFnSc4d1	chemická
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
vlivům	vliv	k1gInPc3	vliv
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odrazivost	odrazivost	k1gFnSc4	odrazivost
pro	pro	k7c4	pro
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jak	jak	k6eAd1	jak
kovové	kovový	k2eAgNnSc4d1	kovové
elementární	elementární	k2eAgNnSc4d1	elementární
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
především	především	k9	především
jeho	jeho	k3xOp3gFnSc2	jeho
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
kovového	kovový	k2eAgNnSc2d1	kovové
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
záznamové	záznamový	k2eAgNnSc1d1	záznamové
médium	médium	k1gNnSc1	médium
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
discích	disk	k1gInPc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
vakuově	vakuově	k6eAd1	vakuově
nanáší	nanášet	k5eAaImIp3nS	nanášet
na	na	k7c4	na
plastovou	plastový	k2eAgFnSc4d1	plastová
podložku	podložka	k1gFnSc4	podložka
a	a	k8xC	a
po	po	k7c4	po
překrytí	překrytí	k1gNnSc4	překrytí
další	další	k2eAgFnSc7d1	další
plastovou	plastový	k2eAgFnSc7d1	plastová
vrstvou	vrstva	k1gFnSc7	vrstva
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
stopy	stop	k1gInPc1	stop
generované	generovaný	k2eAgInPc1d1	generovaný
laserem	laser	k1gInSc7	laser
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poté	poté	k6eAd1	poté
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
uloženého	uložený	k2eAgInSc2d1	uložený
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvláště	zvláště	k6eAd1	zvláště
důležité	důležitý	k2eAgFnPc4d1	důležitá
aplikace	aplikace	k1gFnPc4	aplikace
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
počítačové	počítačový	k2eAgInPc1d1	počítačový
hard	hard	k1gInSc1	hard
disky	disk	k1gInPc1	disk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
směsí	směs	k1gFnSc7	směs
stříbra	stříbro	k1gNnSc2	stříbro
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
platiny	platina	k1gFnSc2	platina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
levnějších	levný	k2eAgFnPc2d2	levnější
variant	varianta	k1gFnPc2	varianta
záznamových	záznamový	k2eAgNnPc2d1	záznamové
médií	médium	k1gNnPc2	médium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stříbro	stříbro	k1gNnSc1	stříbro
naopak	naopak	k6eAd1	naopak
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
hliníkem	hliník	k1gInSc7	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
optické	optický	k2eAgFnPc1d1	optická
odrazivosti	odrazivost	k1gFnPc1	odrazivost
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
již	již	k6eAd1	již
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kvalitních	kvalitní	k2eAgNnPc2d1	kvalitní
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
stříbra	stříbro	k1gNnSc2	stříbro
nanášena	nanášen	k2eAgFnSc1d1	nanášena
na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
podložku	podložka	k1gFnSc4	podložka
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
skleněnou	skleněný	k2eAgFnSc7d1	skleněná
deskou	deska	k1gFnSc7	deska
je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
atmosférickými	atmosférický	k2eAgInPc7d1	atmosférický
plyny	plyn	k1gInPc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
jako	jako	k8xC	jako
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
je	být	k5eAaImIp3nS	být
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
pamětních	pamětní	k2eAgFnPc2d1	pamětní
mincí	mince	k1gFnPc2	mince
a	a	k8xC	a
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgFnPc1d1	stříbrná
mince	mince	k1gFnPc1	mince
byly	být	k5eAaImAgFnP	být
raženy	razit	k5eAaImNgFnP	razit
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
<g/>
)	)	kIx)	)
i	i	k9	i
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
(	(	kIx(	(
<g/>
Pražské	pražský	k2eAgInPc1d1	pražský
groše	groš	k1gInPc1	groš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
medaile	medaile	k1gFnSc1	medaile
udělována	udělovat	k5eAaImNgFnS	udělovat
sportovci	sportovec	k1gMnPc7	sportovec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsadil	obsadit	k5eAaPmAgMnS	obsadit
druhé	druhý	k4xOgNnSc4	druhý
pořadí	pořadí	k1gNnSc4	pořadí
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
disciplíně	disciplína	k1gFnSc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
tyto	tento	k3xDgFnPc4	tento
medaile	medaile	k1gFnPc4	medaile
pouze	pouze	k6eAd1	pouze
pokrývány	pokrýván	k2eAgInPc1d1	pokrýván
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
vrstvou	vrstva	k1gFnSc7	vrstva
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
různých	různý	k2eAgFnPc2d1	různá
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vydávány	vydáván	k2eAgFnPc4d1	vydávána
pamětní	pamětní	k2eAgFnPc4d1	pamětní
mince	mince	k1gFnPc4	mince
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
buď	buď	k8xC	buď
z	z	k7c2	z
čistého	čistý	k2eAgNnSc2d1	čisté
stříbra	stříbro	k1gNnSc2	stříbro
nebo	nebo	k8xC	nebo
stříbrem	stříbro	k1gNnSc7	stříbro
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1	kovové
stříbro	stříbro	k1gNnSc1	stříbro
i	i	k8xC	i
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
základním	základní	k2eAgInSc7d1	základní
prvkem	prvek	k1gInSc7	prvek
vysoce	vysoce	k6eAd1	vysoce
účinných	účinný	k2eAgInPc2d1	účinný
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
elektrických	elektrický	k2eAgInPc2d1	elektrický
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
baterií	baterie	k1gFnPc2	baterie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
náramkových	náramkový	k2eAgFnPc6d1	náramková
hodinkách	hodinka	k1gFnPc6	hodinka
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
malých	malý	k2eAgInPc6d1	malý
elektrických	elektrický	k2eAgInPc6d1	elektrický
spotřebičích	spotřebič	k1gInPc6	spotřebič
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrozinkové	Stříbrozinkový	k2eAgInPc1d1	Stříbrozinkový
akumulátory	akumulátor	k1gInPc1	akumulátor
mají	mít	k5eAaImIp3nP	mít
kladné	kladný	k2eAgFnPc4d1	kladná
desky	deska	k1gFnPc4	deska
z	z	k7c2	z
porézního	porézní	k2eAgNnSc2d1	porézní
sintrovaného	sintrovaný	k2eAgNnSc2d1	sintrovaný
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
záporné	záporný	k2eAgInPc4d1	záporný
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
70	[number]	k4	70
%	%	kIx~	%
lehčí	lehký	k2eAgInPc4d2	lehčí
a	a	k8xC	a
objemově	objemově	k6eAd1	objemově
asi	asi	k9	asi
o	o	k7c4	o
60	[number]	k4	60
%	%	kIx~	%
menší	malý	k2eAgInPc4d2	menší
než	než	k8xS	než
olověné	olověný	k2eAgInPc4d1	olověný
akumulátory	akumulátor	k1gInPc4	akumulátor
<g/>
,	,	kIx,	,
vynikají	vynikat	k5eAaImIp3nP	vynikat
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
odolností	odolnost	k1gFnSc7	odolnost
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
teplot	teplota	k1gFnPc2	teplota
-	-	kIx~	-
až	až	k9	až
+40	+40	k4	+40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
vydrží	vydržet	k5eAaPmIp3nS	vydržet
vybíjení	vybíjení	k1gNnSc1	vybíjení
velkým	velký	k2eAgInSc7d1	velký
proudem	proud	k1gInSc7	proud
a	a	k8xC	a
snesou	snést	k5eAaPmIp3nP	snést
zkraty	zkrat	k1gInPc1	zkrat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
pečlivé	pečlivý	k2eAgFnSc2d1	pečlivá
údržby	údržba	k1gFnSc2	údržba
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
krátká	krátký	k2eAgFnSc1d1	krátká
doba	doba	k1gFnSc1	doba
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntetické	syntetický	k2eAgFnSc6d1	syntetická
chemii	chemie	k1gFnSc6	chemie
jsou	být	k5eAaImIp3nP	být
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
využívány	využíván	k2eAgFnPc1d1	využívána
jako	jako	k8xC	jako
katalyzátory	katalyzátor	k1gInPc7	katalyzátor
některých	některý	k3yIgFnPc2	některý
oxidačních	oxidační	k2eAgFnPc2d1	oxidační
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
formaldehydu	formaldehyd	k1gInSc2	formaldehyd
oxidací	oxidace	k1gFnSc7	oxidace
methanolu	methanol	k1gInSc2	methanol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
magických	magický	k2eAgFnPc6d1	magická
aplikacích	aplikace	k1gFnPc6	aplikace
bylo	být	k5eAaImAgNnS	být
stříbro	stříbro	k1gNnSc1	stříbro
odpradávna	odpradávna	k6eAd1	odpradávna
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
účinný	účinný	k2eAgInSc4d1	účinný
prostředek	prostředek	k1gInSc4	prostředek
proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
temných	temný	k2eAgFnPc2d1	temná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
skutečně	skutečně	k6eAd1	skutečně
účinných	účinný	k2eAgInPc2d1	účinný
způsobů	způsob	k1gInPc2	způsob
likvidace	likvidace	k1gFnSc2	likvidace
vlkodlaka	vlkodlak	k1gMnSc4	vlkodlak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnPc2	jeho
zastřelení	zastřelení	k1gNnPc2	zastřelení
kulkou	kulka	k1gFnSc7	kulka
z	z	k7c2	z
čistého	čistý	k2eAgNnSc2d1	čisté
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
stříbro	stříbro	k1gNnSc1	stříbro
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
měkké	měkký	k2eAgNnSc1d1	měkké
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
náchylné	náchylný	k2eAgNnSc1d1	náchylné
k	k	k7c3	k
černání	černání	k1gNnSc3	černání
při	při	k7c6	při
styku	styk	k1gInSc6	styk
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
praktické	praktický	k2eAgFnPc4d1	praktická
aplikace	aplikace	k1gFnPc4	aplikace
obvykle	obvykle	k6eAd1	obvykle
slévá	slévat	k5eAaImIp3nS	slévat
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zlepší	zlepšit	k5eAaPmIp3nP	zlepšit
jak	jak	k6eAd1	jak
jeho	jeho	k3xOp3gInPc1	jeho
mechanické	mechanický	k2eAgInPc1d1	mechanický
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vzhledové	vzhledový	k2eAgFnPc4d1	vzhledová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Klenotnické	klenotnický	k2eAgFnPc1d1	klenotnická
zlaté	zlatý	k2eAgFnPc1d1	zlatá
slitiny	slitina	k1gFnPc1	slitina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
určité	určitý	k2eAgNnSc4d1	určité
procento	procento	k1gNnSc4	procento
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
klenotnické	klenotnický	k2eAgNnSc1d1	klenotnické
stříbro	stříbro	k1gNnSc1	stříbro
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
slitinou	slitina	k1gFnSc7	slitina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kolem	kolem	k7c2	kolem
90	[number]	k4	90
%	%	kIx~	%
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
mědí	mědit	k5eAaImIp3nP	mědit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
povrchové	povrchový	k2eAgFnSc2d1	povrchová
kvality	kvalita	k1gFnSc2	kvalita
stříbrných	stříbrný	k2eAgInPc2d1	stříbrný
šperků	šperk	k1gInPc2	šperk
(	(	kIx(	(
<g/>
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
tyto	tento	k3xDgInPc1	tento
předměty	předmět	k1gInPc1	předmět
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
velmi	velmi	k6eAd1	velmi
tenkými	tenký	k2eAgFnPc7d1	tenká
vrstvičkami	vrstvička	k1gFnPc7	vrstvička
kovového	kovový	k2eAgInSc2d1	kovový
rhodia	rhodium	k1gNnPc5	rhodium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
nacházejí	nacházet	k5eAaImIp3nP	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
slitiny	slitina	k1gFnSc2	slitina
stříbra	stříbro	k1gNnSc2	stříbro
především	především	k9	především
v	v	k7c6	v
dentálních	dentální	k2eAgFnPc6d1	dentální
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
neškodnost	neškodnost	k1gFnSc1	neškodnost
a	a	k8xC	a
chemická	chemický	k2eAgFnSc1d1	chemická
odolnost	odolnost	k1gFnSc1	odolnost
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
především	především	k9	především
ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
s	s	k7c7	s
převládajícím	převládající	k2eAgInSc7d1	převládající
obsahem	obsah	k1gInSc7	obsah
palladia	palladion	k1gNnSc2	palladion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dentálních	dentální	k2eAgFnPc2d1	dentální
slitin	slitina	k1gFnPc2	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
dentálního	dentální	k2eAgNnSc2d1	dentální
využití	využití	k1gNnSc2	využití
stříbra	stříbro	k1gNnSc2	stříbro
jsou	být	k5eAaImIp3nP	být
amalgámy	amalgáma	k1gFnPc1	amalgáma
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
slitiny	slitina	k1gFnPc1	slitina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
výplně	výplň	k1gFnPc1	výplň
otvorů	otvor	k1gInPc2	otvor
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
po	po	k7c6	po
odvrtání	odvrtání	k1gNnSc6	odvrtání
zubního	zubní	k2eAgInSc2d1	zubní
kazu	kaz	k1gInSc2	kaz
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc7	jejich
hlavními	hlavní	k2eAgFnPc7d1	hlavní
složkami	složka	k1gFnPc7	složka
je	být	k5eAaImIp3nS	být
rtuť	rtuť	k1gFnSc1	rtuť
a	a	k8xC	a
slitiny	slitina	k1gFnSc2	slitina
stříbra	stříbro	k1gNnSc2	stříbro
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
cínem	cín	k1gInSc7	cín
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
patří	patřit	k5eAaImIp3nS	patřit
slitinám	slitina	k1gFnPc3	slitina
stříbra	stříbro	k1gNnSc2	stříbro
jako	jako	k8xS	jako
základu	základ	k1gInSc2	základ
pájek	pájka	k1gFnPc2	pájka
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
především	především	k6eAd1	především
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgFnPc1d1	stříbrná
pájky	pájka	k1gFnPc1	pájka
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
vodivostí	vodivost	k1gFnSc7	vodivost
<g/>
,	,	kIx,	,
tvrdostí	tvrdost	k1gFnSc7	tvrdost
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
vysokým	vysoký	k2eAgInSc7d1	vysoký
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc4	slitina
stříbra	stříbro	k1gNnSc2	stříbro
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
<g/>
,	,	kIx,	,
kadmiem	kadmium	k1gNnSc7	kadmium
a	a	k8xC	a
zinkem	zinek	k1gInSc7	zinek
slouží	sloužit	k5eAaImIp3nP	sloužit
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
jako	jako	k9	jako
spojovací	spojovací	k2eAgInSc4d1	spojovací
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
plošných	plošný	k2eAgInPc2d1	plošný
spojů	spoj	k1gInPc2	spoj
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
a	a	k8xC	a
mince	mince	k1gFnSc1	mince
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
ražené	ražený	k2eAgFnPc1d1	ražená
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
platidlem	platidlo	k1gNnSc7	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
měkkost	měkkost	k1gFnSc4	měkkost
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
razily	razit	k5eAaImAgInP	razit
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
a	a	k8xC	a
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
mincí	mince	k1gFnSc7	mince
byl	být	k5eAaImAgInS	být
tolar	tolar	k1gInSc1	tolar
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
tolar	tolar	k1gInSc1	tolar
ražený	ražený	k2eAgInSc1d1	ražený
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
a	a	k8xC	a
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
a	a	k8xC	a
stálým	stálý	k2eAgNnSc7d1	stálé
platidlem	platidlo	k1gNnSc7	platidlo
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgInSc4d1	dnešní
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tolar	tolar	k1gInSc1	tolar
byl	být	k5eAaImAgInS	být
platidlem	platidlo	k1gNnSc7	platidlo
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
slov	slovo	k1gNnPc2	slovo
tolar	tolar	k1gInSc4	tolar
a	a	k8xC	a
dolar	dolar	k1gInSc4	dolar
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
mincí	mince	k1gFnSc7	mince
byl	být	k5eAaImAgInS	být
groš	groš	k1gInSc1	groš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
známý	známý	k2eAgInSc1d1	známý
Pražský	pražský	k2eAgInSc1d1	pražský
groš	groš	k1gInSc1	groš
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
platil	platit	k5eAaImAgInS	platit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
tolar	tolar	k1gInSc1	tolar
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
XAG	XAG	kA	XAG
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
troyské	troyský	k2eAgFnSc2d1	troyská
unce	unce	k1gFnSc2	unce
stříbra	stříbro	k1gNnSc2	stříbro
jako	jako	k8xC	jako
platidla	platidlo	k1gNnSc2	platidlo
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
ISO	ISO	kA	ISO
4217	[number]	k4	4217
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
sloučeninou	sloučenina	k1gFnSc7	sloučenina
stříbra	stříbro	k1gNnSc2	stříbro
dusičnan	dusičnan	k1gInSc4	dusičnan
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
AgNO	AgNO	k1gMnSc1	AgNO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
připravit	připravit	k5eAaPmF	připravit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc6d1	vysoká
čistotě	čistota	k1gFnSc6	čistota
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
čistého	čistý	k2eAgNnSc2d1	čisté
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
poté	poté	k6eAd1	poté
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
výrobě	výroba	k1gFnSc6	výroba
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
iontů	ion	k1gInPc2	ion
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
Ag	Ag	k1gFnSc7	Ag
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vznik	vznik	k1gInSc4	vznik
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
černání	černání	k1gNnSc1	černání
stříbrných	stříbrný	k2eAgInPc2d1	stříbrný
předmětů	předmět	k1gInPc2	předmět
působením	působení	k1gNnSc7	působení
i	i	k8xC	i
stopových	stopový	k2eAgNnPc2d1	stopové
množství	množství	k1gNnPc2	množství
sulfanu	sulfan	k1gInSc2	sulfan
(	(	kIx(	(
<g/>
sirovodíku	sirovodík	k1gInSc2	sirovodík
<g/>
)	)	kIx)	)
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ag	Ag	k?	Ag
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejméně	málo	k6eAd3	málo
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
známé	známý	k2eAgFnPc4d1	známá
soli	sůl	k1gFnPc4	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Halogenidy	Halogenida	k1gFnPc1	Halogenida
stříbra	stříbro	k1gNnSc2	stříbro
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
většinou	většina	k1gFnSc7	většina
nerozpustné	rozpustný	k2eNgFnSc2d1	nerozpustná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgFnSc1d1	nacházející
hlavní	hlavní	k2eAgNnSc4d1	hlavní
využití	využití	k1gNnSc4	využití
ve	v	k7c6	v
fotografickém	fotografický	k2eAgInSc6d1	fotografický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
poněkud	poněkud	k6eAd1	poněkud
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
vlastností	vlastnost	k1gFnPc2	vlastnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
k	k	k7c3	k
důkazu	důkaz	k1gInSc3	důkaz
i	i	k8xC	i
stanovení	stanovení	k1gNnSc4	stanovení
některých	některý	k3yIgInPc2	některý
anionů	anion	k1gInPc2	anion
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
AgCl	AgCl	k1gInSc4	AgCl
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
amoniaku	amoniak	k1gInSc6	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
argentometrické	argentometrický	k2eAgFnSc6d1	argentometrický
titraci	titrace	k1gFnSc6	titrace
chloridů	chlorid	k1gInPc2	chlorid
roztokem	roztok	k1gInSc7	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
fotografických	fotografický	k2eAgInPc2d1	fotografický
filmů	film	k1gInPc2	film
a	a	k8xC	a
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
AgBr	AgBr	k1gInSc4	AgBr
je	být	k5eAaImIp3nS	být
slabě	slabě	k6eAd1	slabě
nažloutlý	nažloutlý	k2eAgMnSc1d1	nažloutlý
a	a	k8xC	a
v	v	k7c6	v
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
poměrně	poměrně	k6eAd1	poměrně
obtížně	obtížně	k6eAd1	obtížně
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
argentometrické	argentometrický	k2eAgFnSc6d1	argentometrický
titraci	titrace	k1gFnSc6	titrace
bromidů	bromid	k1gInPc2	bromid
roztokem	roztok	k1gInSc7	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
fotografických	fotografický	k2eAgInPc2d1	fotografický
filmů	film	k1gInPc2	film
a	a	k8xC	a
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc1	jodid
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
AgI	AgI	k1gMnSc1	AgI
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgMnSc1d1	žlutý
a	a	k8xC	a
v	v	k7c6	v
amoniaku	amoniak	k1gInSc6	amoniak
zcela	zcela	k6eAd1	zcela
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
argentometrické	argentometrický	k2eAgFnSc6d1	argentometrický
titraci	titrace	k1gFnSc6	titrace
jodidů	jodid	k1gInPc2	jodid
roztokem	roztok	k1gInSc7	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
např.	např.	kA	např.
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
umělé	umělý	k2eAgNnSc4d1	umělé
vyvolání	vyvolání	k1gNnSc4	vyvolání
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
krystalky	krystalka	k1gFnPc1	krystalka
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
rozptylovány	rozptylovat	k5eAaImNgInP	rozptylovat
z	z	k7c2	z
letadla	letadlo	k1gNnSc2	letadlo
do	do	k7c2	do
oblaků	oblak	k1gInPc2	oblak
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
kondenzační	kondenzační	k2eAgNnPc1d1	kondenzační
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
první	první	k4xOgFnPc1	první
kapky	kapka	k1gFnPc1	kapka
dešťové	dešťový	k2eAgFnSc2d1	dešťová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
pouze	pouze	k6eAd1	pouze
AgF	AgF	k1gFnSc7	AgF
<g/>
.	.	kIx.	.
</s>
<s>
Argentometrickou	Argentometrický	k2eAgFnSc7d1	Argentometrický
titrací	titrace	k1gFnSc7	titrace
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
například	například	k6eAd1	například
množství	množství	k1gNnPc4	množství
halogenidových	halogenidový	k2eAgInPc2d1	halogenidový
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Uveďme	uvést	k5eAaPmRp1nP	uvést
příklad	příklad	k1gInSc4	příklad
<g/>
:	:	kIx,	:
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
určit	určit	k5eAaPmF	určit
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
obsah	obsah	k1gInSc1	obsah
chloridových	chloridový	k2eAgInPc2d1	chloridový
aniontů	anion	k1gInPc2	anion
<g/>
,	,	kIx,	,
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
ho	on	k3xPp3gNnSc4	on
výpočtem	výpočet	k1gInSc7	výpočet
ze	z	k7c2	z
součinu	součin	k1gInSc2	součin
rozpustnosti	rozpustnost	k1gFnSc2	rozpustnost
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
NaCl	NaCl	k1gInSc1	NaCl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
určování	určování	k1gNnSc6	určování
ale	ale	k9	ale
musíme	muset	k5eAaImIp1nP	muset
mít	mít	k5eAaImF	mít
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
titrací	titrace	k1gFnSc7	titrace
vzniká	vznikat	k5eAaImIp3nS	vznikat
sraženina	sraženina	k1gFnSc1	sraženina
<g/>
,	,	kIx,	,
že	že	k8xS	že
sraženinu	sraženina	k1gFnSc4	sraženina
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
jen	jen	k9	jen
chloridový	chloridový	k2eAgInSc1d1	chloridový
anion	anion	k1gInSc1	anion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bromidový	bromidový	k2eAgMnSc1d1	bromidový
a	a	k8xC	a
jodidový	jodidový	k2eAgMnSc1d1	jodidový
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
daleko	daleko	k6eAd1	daleko
ochotněji	ochotně	k6eAd2	ochotně
než	než	k8xS	než
chloridový	chloridový	k2eAgInSc1d1	chloridový
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
musíme	muset	k5eAaImIp1nP	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledek	výsledek	k1gInSc1	výsledek
výpočtu	výpočet	k1gInSc2	výpočet
nebude	být	k5eNaImBp3nS	být
úplně	úplně	k6eAd1	úplně
přesný	přesný	k2eAgInSc1d1	přesný
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vyvolávání	vyvolávání	k1gNnSc2	vyvolávání
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
stříbra	stříbro	k1gNnSc2	stříbro
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
celého	celý	k2eAgNnSc2d1	celé
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
odvětví	odvětví	k1gNnSc2	odvětví
-	-	kIx~	-
fotografického	fotografický	k2eAgInSc2d1	fotografický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
výrobou	výroba	k1gFnSc7	výroba
produktů	produkt	k1gInPc2	produkt
pro	pro	k7c4	pro
získávání	získávání	k1gNnSc4	získávání
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
těžkých	těžký	k2eAgInPc2d1	těžký
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organizmu	organizmus	k1gInSc6	organizmus
přítomno	přítomen	k2eAgNnSc1d1	přítomno
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
stopových	stopový	k2eAgNnPc6d1	stopové
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stříbro	stříbro	k1gNnSc1	stříbro
obecně	obecně	k6eAd1	obecně
působí	působit	k5eAaImIp3nS	působit
především	především	k9	především
baktericidně	baktericidně	k6eAd1	baktericidně
a	a	k8xC	a
desinfekčně	desinfekčně	k6eAd1	desinfekčně
(	(	kIx(	(
<g/>
koloidní	koloidní	k2eAgNnSc1d1	koloidní
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Baktericidních	baktericidní	k2eAgFnPc2d1	baktericidní
vlastností	vlastnost	k1gFnPc2	vlastnost
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
při	při	k7c6	při
jednorázové	jednorázový	k2eAgFnSc6d1	jednorázová
dezinfekci	dezinfekce	k1gFnSc6	dezinfekce
menších	malý	k2eAgInPc2d2	menší
zdrojů	zdroj	k1gInPc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
studny	studna	k1gFnSc2	studna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vojenských	vojenský	k2eAgFnPc2d1	vojenská
pohotovostních	pohotovostní	k2eAgFnPc2d1	pohotovostní
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožní	umožnit	k5eAaPmIp3nP	umožnit
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
získat	získat	k5eAaPmF	získat
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
i	i	k9	i
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
znečistěných	znečistěný	k2eAgInPc2d1	znečistěný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stříbro	stříbro	k1gNnSc1	stříbro
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
antiseptikum	antiseptikum	k1gNnSc1	antiseptikum
<g/>
;	;	kIx,	;
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c4	na
mokvající	mokvající	k2eAgFnPc4d1	mokvající
popáleniny	popálenina	k1gFnPc4	popálenina
je	být	k5eAaImIp3nS	být
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
sůl	sůl	k1gFnSc1	sůl
sulfadiazinu	sulfadiazin	k1gInSc2	sulfadiazin
(	(	kIx(	(
<g/>
preparát	preparát	k1gInSc1	preparát
DERMAZIN	DERMAZIN	kA	DERMAZIN
<g/>
)	)	kIx)	)
Historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgInS	být
pekelný	pekelný	k2eAgInSc1d1	pekelný
kamínek	kamínek	k1gInSc1	kamínek
(	(	kIx(	(
<g/>
dusičnan	dusičnan	k1gInSc1	dusičnan
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
<g/>
)	)	kIx)	)
některými	některý	k3yIgFnPc7	některý
národy	národ	k1gInPc7	národ
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
očního	oční	k2eAgInSc2d1	oční
trachomu	trachom	k1gInSc2	trachom
<g/>
.	.	kIx.	.
</s>
<s>
Baktericidní	baktericidní	k2eAgInPc1d1	baktericidní
a	a	k8xC	a
fungicidní	fungicidní	k2eAgInPc1d1	fungicidní
účinky	účinek	k1gInPc1	účinek
nanostříbra	nanostříbra	k6eAd1	nanostříbra
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
výraznější	výrazný	k2eAgFnPc1d2	výraznější
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nanostříbro	nanostříbro	k6eAd1	nanostříbro
součástí	součást	k1gFnSc7	součást
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
rehabilitačních	rehabilitační	k2eAgInPc2d1	rehabilitační
<g/>
,	,	kIx,	,
preventivních	preventivní	k2eAgInPc2d1	preventivní
a	a	k8xC	a
léčebných	léčebný	k2eAgInPc2d1	léčebný
preparátů	preparát	k1gInPc2	preparát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
textilních	textilní	k2eAgInPc2d1	textilní
výrobků	výrobek	k1gInPc2	výrobek
se	s	k7c7	s
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Nanostříbro	Nanostříbro	k6eAd1	Nanostříbro
také	také	k9	také
snáze	snadno	k6eAd2	snadno
proniká	pronikat	k5eAaImIp3nS	pronikat
tkáněmi	tkáň	k1gFnPc7	tkáň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
či	či	k8xC	či
suplement	suplement	k1gInSc1	suplement
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
i	i	k9	i
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
hlubších	hluboký	k2eAgFnPc2d2	hlubší
vrstev	vrstva	k1gFnPc2	vrstva
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
či	či	k8xC	či
celého	celý	k2eAgInSc2d1	celý
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
dávkách	dávka	k1gFnPc6	dávka
a	a	k8xC	a
koncentracích	koncentrace	k1gFnPc6	koncentrace
je	být	k5eAaImIp3nS	být
však	však	k9	však
působení	působení	k1gNnSc4	působení
stříbra	stříbro	k1gNnSc2	stříbro
na	na	k7c4	na
organizmus	organizmus	k1gInSc4	organizmus
negativní	negativní	k2eAgInSc4d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
styku	styk	k1gInSc6	styk
pokožky	pokožka	k1gFnSc2	pokožka
roztoky	roztok	k1gInPc1	roztok
Ag	Ag	k1gFnSc2	Ag
<g/>
+	+	kIx~	+
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
tmavých	tmavý	k2eAgFnPc2d1	tmavá
skvrn	skvrna	k1gFnPc2	skvrna
-	-	kIx~	-
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
vysoký	vysoký	k2eAgInSc1d1	vysoký
přísun	přísun	k1gInSc1	přísun
stříbra	stříbro	k1gNnSc2	stříbro
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
ukládání	ukládání	k1gNnSc1	ukládání
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
především	především	k9	především
do	do	k7c2	do
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
případné	případný	k2eAgFnSc6d1	případná
karcinogenitě	karcinogenita	k1gFnSc6	karcinogenita
a	a	k8xC	a
míře	míra	k1gFnSc6	míra
toxických	toxický	k2eAgInPc2d1	toxický
účinků	účinek	k1gInPc2	účinek
solí	sůl	k1gFnPc2	sůl
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
zde	zde	k6eAd1	zde
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
výzkum	výzkum	k1gInSc1	výzkum
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nanočástice	nanočástika	k1gFnSc6	nanočástika
stříbra	stříbro	k1gNnSc2	stříbro
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
30	[number]	k4	30
nanometrů	nanometr	k1gInPc2	nanometr
a	a	k8xC	a
menší	malý	k2eAgInPc1d2	menší
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
vyvíjejících	vyvíjející	k2eAgFnPc6d1	vyvíjející
se	se	k3xPyFc4	se
embryí	embryo	k1gNnPc2	embryo
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
závažné	závažný	k2eAgFnPc1d1	závažná
malformace	malformace	k1gFnPc1	malformace
včetně	včetně	k7c2	včetně
krevních	krevní	k2eAgInPc2d1	krevní
výronů	výron	k1gInPc2	výron
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
otoků	otok	k1gInPc2	otok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
úhynu	úhyn	k1gInSc3	úhyn
rybích	rybí	k2eAgFnPc2d1	rybí
larev	larva	k1gFnPc2	larva
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
je	být	k5eAaImIp3nS	být
komoditou	komodita	k1gFnSc7	komodita
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
burzách	burza	k1gFnPc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
burza	burza	k1gFnSc1	burza
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
průběžně	průběžně	k6eAd1	průběžně
výsledky	výsledek	k1gInPc4	výsledek
obchodování	obchodování	k1gNnSc2	obchodování
tzv.	tzv.	kA	tzv.
Lodnon	Lodnon	k1gNnSc1	Lodnon
FIX	fixum	k1gNnPc2	fixum
a	a	k8xC	a
London	London	k1gMnSc1	London
SPOT	spot	k1gInSc1	spot
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
cena	cena	k1gFnSc1	cena
stříbra	stříbro	k1gNnSc2	stříbro
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
udávaná	udávaný	k2eAgFnSc1d1	udávaná
v	v	k7c6	v
dolarech	dolar	k1gInPc6	dolar
za	za	k7c4	za
trojskou	trojský	k2eAgFnSc4d1	Trojská
unci	unce	k1gFnSc4	unce
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
/	/	kIx~	/
<g/>
oz	oz	k?	oz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
