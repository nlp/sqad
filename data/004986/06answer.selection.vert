<s>
Etruština	etruština	k1gFnSc1	etruština
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
latinou	latina	k1gFnSc7	latina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
pouze	pouze	k6eAd1	pouze
nevelké	velký	k2eNgNnSc1d1	nevelké
množství	množství	k1gNnSc1	množství
psaných	psaný	k2eAgFnPc2d1	psaná
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
přejatá	přejatý	k2eAgNnPc1d1	přejaté
latinská	latinský	k2eAgNnPc1d1	latinské
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
jako	jako	k8xS	jako
například	například	k6eAd1	například
Parma	Parma	k1gFnSc1	Parma
<g/>
.	.	kIx.	.
</s>
