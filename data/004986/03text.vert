<s>
Etruština	etruština	k1gFnSc1	etruština
byl	být	k5eAaImAgInS	být
starověký	starověký	k2eAgInSc1d1	starověký
jazyk	jazyk	k1gInSc1	jazyk
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
Etrurii	Etrurie	k1gFnSc6	Etrurie
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Toskánsko	Toskánsko	k1gNnSc4	Toskánsko
a	a	k8xC	a
sever	sever	k1gInSc4	sever
Lazia	Lazium	k1gNnSc2	Lazium
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
částech	část	k1gFnPc6	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
,	,	kIx,	,
Benátska	Benátsk	k1gInSc2	Benátsk
a	a	k8xC	a
Emilia-Romagna	Emilia-Romagn	k1gInSc2	Emilia-Romagn
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byli	být	k5eAaImAgMnP	být
Etruskové	Etrusk	k1gMnPc1	Etrusk
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
Galy	Gal	k1gMnPc4	Gal
<g/>
.	.	kIx.	.
</s>
<s>
Etruština	etruština	k1gFnSc1	etruština
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
latinou	latina	k1gFnSc7	latina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
pouze	pouze	k6eAd1	pouze
nevelké	velký	k2eNgNnSc1d1	nevelké
množství	množství	k1gNnSc1	množství
psaných	psaný	k2eAgFnPc2d1	psaná
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
přejatá	přejatý	k2eAgNnPc1d1	přejaté
latinská	latinský	k2eAgNnPc1d1	latinské
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
jako	jako	k8xS	jako
například	například	k6eAd1	například
Parma	Parma	k1gFnSc1	Parma
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Původ	původ	k1gInSc1	původ
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
etruského	etruský	k2eAgInSc2d1	etruský
jazyka	jazyk	k1gInSc2	jazyk
byl	být	k5eAaImAgInS	být
předmětem	předmět	k1gInSc7	předmět
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zčásti	zčásti	k6eAd1	zčásti
vyjasnily	vyjasnit	k5eAaPmAgFnP	vyjasnit
až	až	k9	až
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nalezení	nalezení	k1gNnSc2	nalezení
dvoujazyčného	dvoujazyčný	k2eAgInSc2d1	dvoujazyčný
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bilingua	bilingua	k1gFnSc1	bilingua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
stejný	stejný	k2eAgInSc4d1	stejný
text	text	k1gInSc4	text
psaný	psaný	k2eAgInSc4d1	psaný
jazykem	jazyk	k1gInSc7	jazyk
Féničanů	Féničan	k1gMnPc2	Féničan
i	i	k8xC	i
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
zlaté	zlatý	k2eAgFnPc1d1	zlatá
destičky	destička	k1gFnPc1	destička
obsahující	obsahující	k2eAgFnSc2d1	obsahující
dvoujazyčné	dvoujazyčný	k2eAgFnSc2d1	dvoujazyčná
věnování	věnování	k1gNnSc2	věnování
fénické	fénický	k2eAgFnSc6d1	fénická
bohyni	bohyně	k1gFnSc6	bohyně
Aštart	Aštart	k1gInSc1	Aštart
od	od	k7c2	od
krále	král	k1gMnSc2	král
etruského	etruský	k2eAgNnSc2d1	etruské
města	město	k1gNnSc2	město
Caere	Caer	k1gInSc5	Caer
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Cerveteri	Cerveteri	k1gNnSc1	Cerveteri
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
Thefarie	Thefarie	k1gFnSc2	Thefarie
Velianas	Velianasa	k1gFnPc2	Velianasa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
významný	významný	k2eAgInSc1d1	významný
archeologický	archeologický	k2eAgInSc1d1	archeologický
nález	nález	k1gInSc1	nález
byl	být	k5eAaImAgInS	být
učiněn	učiněn	k2eAgInSc1d1	učiněn
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Pyrgi	Pyrgi	k1gNnSc2	Pyrgi
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
přístav	přístav	k1gInSc4	přístav
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Caere	Caer	k1gMnSc5	Caer
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
předmětů	předmět	k1gInPc2	předmět
popsaných	popsaný	k2eAgInPc2d1	popsaný
dvoujazyčným	dvoujazyčný	k2eAgInSc7d1	dvoujazyčný
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dosud	dosud	k6eAd1	dosud
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
nalezený	nalezený	k2eAgInSc4d1	nalezený
etruský	etruský	k2eAgInSc4d1	etruský
text	text	k1gInSc4	text
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
považuje	považovat	k5eAaImIp3nS	považovat
"	"	kIx"	"
<g/>
náboženský	náboženský	k2eAgInSc1d1	náboženský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
červeným	červený	k2eAgMnSc7d1	červený
a	a	k8xC	a
černým	černý	k2eAgMnSc7d1	černý
inkoustem	inkoust	k1gMnSc7	inkoust
kaligrafován	kaligrafován	k2eAgInSc4d1	kaligrafován
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Záhřebské	záhřebský	k2eAgFnSc2d1	Záhřebská
mumie	mumie	k1gFnSc2	mumie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
mumii	mumie	k1gFnSc4	mumie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
...	...	k?	...
odtud	odtud	k6eAd1	odtud
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
ovšem	ovšem	k9	ovšem
i	i	k9	i
přes	přes	k7c4	přes
další	další	k2eAgInPc4d1	další
nálezy	nález	k1gInPc4	nález
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
význam	význam	k1gInSc4	význam
většiny	většina	k1gFnSc2	většina
etruských	etruský	k2eAgNnPc2d1	etruské
slov	slovo	k1gNnPc2	slovo
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Genetické	genetický	k2eAgInPc1d1	genetický
výzkumy	výzkum	k1gInPc1	výzkum
o	o	k7c6	o
Etruscích	Etrusk	k1gMnPc6	Etrusk
a	a	k8xC	a
Původ	původ	k1gInSc4	původ
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejpravděpodobnější	pravděpodobný	k2eAgMnPc4d3	nejpravděpodobnější
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
etruština	etruština	k1gFnSc1	etruština
je	být	k5eAaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
pouze	pouze	k6eAd1	pouze
rodině	rodina	k1gFnSc3	rodina
tyrhenských	tyrhenský	k2eAgInPc2d1	tyrhenský
jazyků	jazyk	k1gInPc2	jazyk
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
a	a	k8xC	a
nepříbuzná	příbuzný	k2eNgFnSc1d1	nepříbuzná
všem	všecek	k3xTgFnPc3	všecek
ostatním	ostatní	k2eAgFnPc3d1	ostatní
jazykovým	jazykový	k2eAgFnPc3d1	jazyková
rodinám	rodina	k1gFnPc3	rodina
jež	jenž	k3xRgNnSc4	jenž
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
uznáváno	uznáván	k2eAgNnSc1d1	uznáváno
<g/>
,	,	kIx,	,
že	že	k8xS	že
rétština	rétština	k1gFnSc1	rétština
a	a	k8xC	a
lémnoština	lémnoština	k1gFnSc1	lémnoština
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnSc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
Lémnos	Lémnos	k1gMnSc1	Lémnos
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Naturalis	Naturalis	k1gInSc4	Naturalis
historia	historium	k1gNnSc2	historium
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Kapitoly	kapitola	k1gFnPc4	kapitola
o	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaPmAgMnS	napsat
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
o	o	k7c6	o
obyvatelích	obyvatel	k1gMnPc6	obyvatel
Alp	Alpy	k1gFnPc2	Alpy
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rétové	Rét	k1gMnPc1	Rét
a	a	k8xC	a
Vindelikové	Vindelik	k1gMnPc1	Vindelik
<g/>
"	"	kIx"	"
množstvím	množství	k1gNnSc7	množství
měst	město	k1gNnPc2	město
sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
Norrikánci	Norrikánec	k1gMnPc7	Norrikánec
<g/>
.	.	kIx.	.
</s>
<s>
Galové	Gal	k1gMnPc1	Gal
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Réťané	Réťan	k1gMnPc1	Réťan
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Etrusků	Etrusk	k1gMnPc2	Etrusk
jež	jenž	k3xRgNnSc1	jenž
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
byli	být	k5eAaImAgMnP	být
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Raeta	Raeto	k1gNnSc2	Raeto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rétové	Rét	k1gMnPc1	Rét
jsou	být	k5eAaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
s	s	k7c7	s
Etrusky	etrusky	k6eAd1	etrusky
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
panují	panovat	k5eAaImIp3nP	panovat
nejasnosti	nejasnost	k1gFnPc4	nejasnost
a	a	k8xC	a
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
současní	současný	k2eAgMnPc1d1	současný
vědci	vědec	k1gMnPc1	vědec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
gramatických	gramatický	k2eAgInPc2d1	gramatický
výzkumů	výzkum	k1gInPc2	výzkum
a	a	k8xC	a
výzkumů	výzkum	k1gInPc2	výzkum
slovní	slovní	k2eAgFnPc1d1	slovní
zásoby	zásoba	k1gFnPc1	zásoba
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyrhénská	tyrhénský	k2eAgFnSc1d1	Tyrhénská
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
indoevropskou	indoevropský	k2eAgFnSc7d1	indoevropská
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
závěry	závěr	k1gInPc4	závěr
však	však	k9	však
chybí	chybit	k5eAaPmIp3nP	chybit
informace	informace	k1gFnPc1	informace
a	a	k8xC	a
především	především	k9	především
původní	původní	k2eAgInPc4d1	původní
texty	text	k1gInPc4	text
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
vědců	vědec	k1gMnPc2	vědec
tak	tak	k9	tak
etruština	etruština	k1gFnSc1	etruština
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
izolovaným	izolovaný	k2eAgInSc7d1	izolovaný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Důkladnému	důkladný	k2eAgInSc3d1	důkladný
výzkumu	výzkum	k1gInSc3	výzkum
jazyka	jazyk	k1gInSc2	jazyk
Etrusků	Etrusk	k1gMnPc2	Etrusk
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
např.	např.	kA	např.
německý	německý	k2eAgMnSc1d1	německý
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Helmut	Helmut	k1gMnSc1	Helmut
Rix	Rix	k1gMnSc1	Rix
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
etruského	etruský	k2eAgNnSc2d1	etruské
písma	písmo	k1gNnSc2	písmo
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
existenci	existence	k1gFnSc4	existence
i	i	k9	i
latinská	latinský	k2eAgFnSc1d1	Latinská
abeceda	abeceda	k1gFnSc1	abeceda
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
stará	starý	k2eAgFnSc1d1	stará
italická	italický	k2eAgFnSc1d1	italická
abeceda	abeceda	k1gFnSc1	abeceda
pro	pro	k7c4	pro
latinu	latina	k1gFnSc4	latina
přejata	přejat	k2eAgFnSc1d1	přejata
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
eubojské	eubojský	k2eAgFnSc2d1	eubojský
varianty	varianta	k1gFnSc2	varianta
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
tak	tak	k6eAd1	tak
sahá	sahat	k5eAaImIp3nS	sahat
k	k	k7c3	k
západosemitským	západosemitský	k2eAgInPc3d1	západosemitský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
byla	být	k5eAaImAgFnS	být
abeceda	abeceda	k1gFnSc1	abeceda
používaná	používaný	k2eAgFnSc1d1	používaná
Etrusky	etrusky	k6eAd1	etrusky
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
na	na	k7c6	na
území	území	k1gNnSc6	území
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
od	od	k7c2	od
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
severní	severní	k2eAgInSc4d1	severní
alpský	alpský	k2eAgInSc4d1	alpský
region	region	k1gInSc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ji	on	k3xPp3gFnSc4	on
adoptovaly	adoptovat	k5eAaPmAgFnP	adoptovat
umberské	umberský	k2eAgFnPc1d1	umberský
<g/>
,	,	kIx,	,
venétské	venétský	k2eAgFnPc1d1	venétský
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
latinské	latinský	k2eAgInPc1d1	latinský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
převzali	převzít	k5eAaPmAgMnP	převzít
písmeno	písmeno	k1gNnSc4	písmeno
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
fonetické	fonetický	k2eAgNnSc4d1	fonetické
vyjádření	vyjádření	k1gNnSc4	vyjádření
"	"	kIx"	"
<g/>
K	K	kA	K
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
etruské	etruský	k2eAgFnSc2d1	etruská
abecedy	abeceda	k1gFnSc2	abeceda
pronikly	proniknout	k5eAaPmAgFnP	proniknout
také	také	k9	také
mezi	mezi	k7c4	mezi
germánské	germánský	k2eAgInPc4d1	germánský
runové	runový	k2eAgInPc4d1	runový
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
