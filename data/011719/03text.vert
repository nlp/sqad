<p>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
vysílač	vysílač	k1gMnSc1	vysílač
Praděd	praděd	k1gMnSc1	praděd
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
obdivuhodným	obdivuhodný	k2eAgFnPc3d1	obdivuhodná
stavbám	stavba	k1gFnPc3	stavba
svého	své	k1gNnSc2	své
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
vysílače	vysílač	k1gInSc2	vysílač
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
pevným	pevný	k2eAgMnSc7d1	pevný
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
umělým	umělý	k2eAgInSc7d1	umělý
<g/>
)	)	kIx)	)
bodem	bod	k1gInSc7	bod
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
mezi	mezi	k7c7	mezi
1637	[number]	k4	1637
a	a	k8xC	a
1638	[number]	k4	1638
metry	metr	k1gInPc4	metr
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
vrchol	vrchol	k1gInSc1	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Vysílač	vysílač	k1gInSc1	vysílač
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
ve	v	k7c6	v
slezském	slezský	k2eAgNnSc6d1	Slezské
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Malá	Malá	k1gFnSc1	Malá
Morávka	Morávek	k1gMnSc2	Morávek
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
obce	obec	k1gFnSc2	obec
Malá	Malá	k1gFnSc1	Malá
Morávka	Morávek	k1gMnSc2	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
s	s	k7c7	s
historickou	historický	k2eAgFnSc7d1	historická
zemskou	zemský	k2eAgFnSc7d1	zemská
hranicí	hranice	k1gFnSc7	hranice
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
stavby	stavba	k1gFnSc2	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Stavba	stavba	k1gFnSc1	stavba
původní	původní	k2eAgFnSc2d1	původní
rozhledny	rozhledna	k1gFnSc2	rozhledna
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Pradědu	praděd	k1gMnSc6	praděd
chýše	chýš	k1gFnSc2	chýš
později	pozdě	k6eAd2	pozdě
doplněná	doplněný	k2eAgFnSc1d1	doplněná
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
budovou	budova	k1gFnSc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
rozhledny	rozhledna	k1gFnSc2	rozhledna
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
snažil	snažit	k5eAaImAgMnS	snažit
Turistický	turistický	k2eAgInSc4d1	turistický
spolek	spolek	k1gInSc4	spolek
Jesenicka	Jesenicko	k1gNnSc2	Jesenicko
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
první	první	k4xOgInPc4	první
dochované	dochovaný	k2eAgInPc4d1	dochovaný
stavební	stavební	k2eAgInPc4d1	stavební
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
jsou	být	k5eAaImIp3nP	být
předloženy	předložit	k5eAaPmNgFnP	předložit
Moravskoslezským	moravskoslezský	k2eAgInSc7d1	moravskoslezský
sudetským	sudetský	k2eAgInSc7d1	sudetský
horským	horský	k2eAgInSc7d1	horský
spolkem	spolek	k1gInSc7	spolek
ve	v	k7c6	v
Frývaldově	Frývaldův	k2eAgFnSc6d1	Frývaldova
stavební	stavební	k2eAgFnSc6d1	stavební
plány	plán	k1gInPc1	plán
architekta	architekt	k1gMnSc2	architekt
Františka	František	k1gMnSc2	František
von	von	k1gInSc4	von
Neumanna	Neumann	k1gMnSc2	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
Okresním	okresní	k2eAgNnSc7d1	okresní
hejtmanstvím	hejtmanství	k1gNnSc7	hejtmanství
v	v	k7c6	v
Bruntále	Bruntál	k1gInSc6	Bruntál
a	a	k8xC	a
vyjasnění	vyjasnění	k1gNnSc6	vyjasnění
situace	situace	k1gFnSc2	situace
s	s	k7c7	s
majiteli	majitel	k1gMnPc7	majitel
okolních	okolní	k2eAgNnPc2d1	okolní
panství	panství	k1gNnPc2	panství
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
povolena	povolit	k5eAaPmNgFnS	povolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
začala	začít	k5eAaPmAgFnS	začít
vlastní	vlastní	k2eAgFnSc1d1	vlastní
výstavba	výstavba	k1gFnSc1	výstavba
rozhledny	rozhledna	k1gFnSc2	rozhledna
–	–	k?	–
Strážní	strážní	k1gMnSc1	strážní
věže	věž	k1gFnSc2	věž
Habsburk	Habsburk	k1gMnSc1	Habsburk
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Habsburgwarte	Habsburgwart	k1gInSc5	Habsburgwart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
věž	věž	k1gFnSc1	věž
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
nedalekých	daleký	k2eNgFnPc2d1	nedaleká
Tabulových	tabulový	k2eAgFnPc2d1	Tabulová
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Dík	dík	k1gInSc1	dík
za	za	k7c2	za
dokončení	dokončení	k1gNnSc2	dokončení
stavby	stavba	k1gFnSc2	stavba
patřil	patřit	k5eAaImAgInS	patřit
členům	člen	k1gMnPc3	člen
turistického	turistický	k2eAgInSc2d1	turistický
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
stavbu	stavba	k1gFnSc4	stavba
financovali	financovat	k5eAaBmAgMnP	financovat
a	a	k8xC	a
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
podařilo	podařit	k5eAaPmAgNnS	podařit
rozhlednu	rozhledna	k1gFnSc4	rozhledna
zkolaudovat	zkolaudovat	k5eAaPmF	zkolaudovat
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
připomínala	připomínat	k5eAaImAgFnS	připomínat
gotickou	gotický	k2eAgFnSc4d1	gotická
hradní	hradní	k2eAgFnSc4d1	hradní
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
půdorys	půdorys	k1gInSc4	půdorys
15	[number]	k4	15
<g/>
×	×	k?	×
<g/>
14,5	[number]	k4	14,5
m	m	kA	m
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
32,5	[number]	k4	32,5
m.	m.	k?	m.
V	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
7	[number]	k4	7
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
byla	být	k5eAaImAgFnS	být
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
plošina	plošina	k1gFnSc1	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Stavbě	stavba	k1gFnSc3	stavba
bylo	být	k5eAaImAgNnS	být
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
číslo	číslo	k1gNnSc1	číslo
popisné	popisný	k2eAgNnSc1d1	popisné
207	[number]	k4	207
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Malá	Malá	k1gFnSc1	Malá
Morávka	Morávek	k1gMnSc2	Morávek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úpadek	úpadek	k1gInSc4	úpadek
původní	původní	k2eAgFnSc2d1	původní
stavby	stavba	k1gFnSc2	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
však	však	k9	však
období	období	k1gNnSc1	období
úpadku	úpadek	k1gInSc2	úpadek
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
jednak	jednak	k8xC	jednak
volba	volba	k1gFnSc1	volba
zcela	zcela	k6eAd1	zcela
špatného	špatný	k2eAgInSc2d1	špatný
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
ve	v	k7c6	v
vlhku	vlhko	k1gNnSc6	vlhko
se	se	k3xPyFc4	se
drolící	drolící	k2eAgFnPc1d1	drolící
<g/>
.	.	kIx.	.
</s>
<s>
Drsné	drsný	k2eAgFnPc4d1	drsná
horské	horský	k2eAgFnPc4d1	horská
a	a	k8xC	a
povětrnostní	povětrnostní	k2eAgFnPc4d1	povětrnostní
podmínky	podmínka	k1gFnPc4	podmínka
nemohl	moct	k5eNaImAgInS	moct
dlouho	dlouho	k6eAd1	dlouho
vydržet	vydržet	k5eAaPmF	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
nemohlo	moct	k5eNaImAgNnS	moct
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
pečováno	pečován	k2eAgNnSc1d1	pečováno
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
rozhledna	rozhledna	k1gFnSc1	rozhledna
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
přece	přece	k9	přece
jen	jen	k6eAd1	jen
dočkala	dočkat	k5eAaPmAgFnS	dočkat
rekonstrukční	rekonstrukční	k2eAgFnPc4d1	rekonstrukční
pozornosti	pozornost	k1gFnPc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
však	však	k9	však
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
rozhlednu	rozhledna	k1gFnSc4	rozhledna
využívali	využívat	k5eAaPmAgMnP	využívat
Němci	Němec	k1gMnPc1	Němec
pro	pro	k7c4	pro
meteorologické	meteorologický	k2eAgInPc4d1	meteorologický
účely	účel	k1gInPc4	účel
a	a	k8xC	a
letecká	letecký	k2eAgNnPc4d1	letecké
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
Němci	Němec	k1gMnPc1	Němec
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
turistickou	turistický	k2eAgFnSc4d1	turistická
Poštovní	poštovní	k2eAgFnSc4d1	poštovní
chatu	chata	k1gFnSc4	chata
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
a	a	k8xC	a
totálně	totálně	k6eAd1	totálně
nasazení	nasazený	k2eAgMnPc1d1	nasazený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
opět	opět	k6eAd1	opět
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
spolek	spolek	k1gInSc1	spolek
Turista	turista	k1gMnSc1	turista
Praha	Praha	k1gFnSc1	Praha
plánoval	plánovat	k5eAaImAgMnS	plánovat
její	její	k3xOp3gFnSc3	její
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
zchátralá	zchátralý	k2eAgFnSc1d1	zchátralá
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
snahy	snaha	k1gFnPc1	snaha
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
téměř	téměř	k6eAd1	téměř
marné	marný	k2eAgNnSc1d1	marné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
neuskutečnila	uskutečnit	k5eNaPmAgFnS	uskutečnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
zřítila	zřítit	k5eAaPmAgFnS	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
tak	tak	k6eAd1	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
budova	budova	k1gFnSc1	budova
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
útulna	útulna	k1gFnSc1	útulna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavba	stavba	k1gFnSc1	stavba
současného	současný	k2eAgInSc2d1	současný
vysílače	vysílač	k1gInSc2	vysílač
===	===	k?	===
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
vysílače	vysílač	k1gInSc2	vysílač
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Jana	Jan	k1gMnSc2	Jan
Lišky	Liška	k1gMnSc2	Liška
ze	z	k7c2	z
Stavoprojektu	Stavoprojekt	k1gInSc2	Stavoprojekt
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
stavby	stavba	k1gFnSc2	stavba
bylo	být	k5eAaImAgNnS	být
pokrýt	pokrýt	k5eAaPmF	pokrýt
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
i	i	k9	i
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
silnice	silnice	k1gFnSc1	silnice
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
Ovčárny	ovčárna	k1gFnSc2	ovčárna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zásobování	zásobování	k1gNnSc3	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
dokončení	dokončení	k1gNnSc1	dokončení
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
termín	termín	k1gInSc1	termín
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
špatným	špatný	k2eAgFnPc3d1	špatná
stavebním	stavební	k2eAgFnPc3d1	stavební
podmínkám	podmínka	k1gFnPc3	podmínka
posunut	posunout	k5eAaPmNgInS	posunout
až	až	k9	až
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
vedle	vedle	k7c2	vedle
vysílací	vysílací	k2eAgFnSc2d1	vysílací
techniky	technika	k1gFnSc2	technika
i	i	k8xC	i
restaurace	restaurace	k1gFnSc2	restaurace
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
a	a	k8xC	a
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysílač	vysílač	k1gInSc1	vysílač
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
betonový	betonový	k2eAgMnSc1d1	betonový
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
veřejně	veřejně	k6eAd1	veřejně
přístupnou	přístupný	k2eAgFnSc7d1	přístupná
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1	vyhlídková
plošinou	plošina	k1gFnSc7	plošina
a	a	k8xC	a
strojovnou	strojovna	k1gFnSc7	strojovna
výtahu	výtah	k1gInSc2	výtah
navazuje	navazovat	k5eAaImIp3nS	navazovat
ocelový	ocelový	k2eAgInSc4d1	ocelový
tubus	tubus	k1gInSc4	tubus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
laminátový	laminátový	k2eAgInSc4d1	laminátový
anténní	anténní	k2eAgInSc4d1	anténní
nástavec	nástavec	k1gInSc4	nástavec
<g/>
.	.	kIx.	.
</s>
<s>
Vysílač	vysílač	k1gInSc1	vysílač
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
146,5	[number]	k4	146,5
metru	metr	k1gInSc2	metr
–	–	k?	–
původní	původní	k2eAgFnSc1d1	původní
výška	výška	k1gFnSc1	výška
162	[number]	k4	162
metrů	metr	k1gInPc2	metr
klesla	klesnout	k5eAaPmAgFnS	klesnout
po	po	k7c6	po
výměně	výměna	k1gFnSc6	výměna
anténního	anténní	k2eAgInSc2d1	anténní
nástavce	nástavec	k1gInSc2	nástavec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
veřejně	veřejně	k6eAd1	veřejně
přístupné	přístupný	k2eAgFnSc2d1	přístupná
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
73	[number]	k4	73
m	m	kA	m
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
vidět	vidět	k5eAaImF	vidět
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
,	,	kIx,	,
Lysá	Lysá	k1gFnSc1	Lysá
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Radhošť	Radhošť	k1gFnSc1	Radhošť
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
a	a	k8xC	a
Malou	malý	k2eAgFnSc4d1	malá
Fatru	Fatra	k1gFnSc4	Fatra
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Vysílač	vysílač	k1gInSc1	vysílač
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
vysílače	vysílač	k1gInSc2	vysílač
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
pevným	pevný	k2eAgMnSc7d1	pevný
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
umělým	umělý	k2eAgInSc7d1	umělý
<g/>
)	)	kIx)	)
bodem	bod	k1gInSc7	bod
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
mezi	mezi	k7c7	mezi
1637	[number]	k4	1637
a	a	k8xC	a
1638	[number]	k4	1638
metry	metr	k1gInPc4	metr
leží	ležet	k5eAaImIp3nS	ležet
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
vrchol	vrchol	k1gInSc4	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	nejvýše	k6eAd1	nejvýše
soustavně	soustavně	k6eAd1	soustavně
dostupnou	dostupný	k2eAgFnSc7d1	dostupná
je	být	k5eAaImIp3nS	být
plošina	plošina	k1gFnSc1	plošina
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
na	na	k7c4	na
navazující	navazující	k2eAgFnSc4d1	navazující
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
anténní	anténní	k2eAgInSc4d1	anténní
nástavec	nástavec	k1gInSc4	nástavec
lze	lze	k6eAd1	lze
vystoupat	vystoupat	k5eAaPmF	vystoupat
jen	jen	k9	jen
při	při	k7c6	při
odstávce	odstávka	k1gFnSc6	odstávka
provozu	provoz	k1gInSc2	provoz
vysílače	vysílač	k1gInSc2	vysílač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vysílač	vysílač	k1gInSc4	vysílač
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
zamýšleny	zamýšlen	k2eAgInPc1d1	zamýšlen
i	i	k8xC	i
speciální	speciální	k2eAgInPc1d1	speciální
designové	designový	k2eAgInPc1d1	designový
doplňky	doplněk	k1gInPc1	doplněk
interiéru	interiér	k1gInSc2	interiér
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nábytek	nábytek	k1gInSc1	nábytek
či	či	k8xC	či
jídelní	jídelní	k2eAgInSc1d1	jídelní
servis	servis	k1gInSc1	servis
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
,	,	kIx,	,
jakých	jaký	k3yIgFnPc2	jaký
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
vysílač	vysílač	k1gInSc1	vysílač
Ještěd	Ještěd	k1gInSc1	Ještěd
<g/>
,	,	kIx,	,
dostavěný	dostavěný	k2eAgInSc1d1	dostavěný
kratce	kratce	k?	kratce
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
stavby	stavba	k1gFnSc2	stavba
na	na	k7c6	na
Pradědu	praděd	k1gMnSc6	praděd
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspory	úspor	k1gInPc4	úspor
se	se	k3xPyFc4	se
však	však	k9	však
záměr	záměr	k1gInSc1	záměr
realizace	realizace	k1gFnSc2	realizace
nedočkal	dočkat	k5eNaPmAgInS	dočkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interiér	interiér	k1gInSc1	interiér
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
doplněn	doplnit	k5eAaPmNgInS	doplnit
i	i	k9	i
uměleckými	umělecký	k2eAgInPc7d1	umělecký
díly	díl	k1gInPc7	díl
–	–	k?	–
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
byla	být	k5eAaImAgFnS	být
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
plastika	plastika	k1gFnSc1	plastika
Kapka	kapka	k1gFnSc1	kapka
vyvěrající	vyvěrající	k2eAgFnSc1d1	vyvěrající
z	z	k7c2	z
jesenických	jesenický	k2eAgInPc2d1	jesenický
hvozdů	hvozd	k1gInPc2	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
požární	požární	k2eAgFnSc3d1	požární
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
veřejnosti	veřejnost	k1gFnSc2	veřejnost
nepřístupné	přístupný	k2eNgFnSc2d1	nepřístupná
zasedací	zasedací	k2eAgFnSc2d1	zasedací
místnosti	místnost	k1gFnSc2	místnost
Českých	český	k2eAgFnPc2d1	Česká
Radiokomunikací	radiokomunikace	k1gFnPc2	radiokomunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
vysílače	vysílač	k1gInSc2	vysílač
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
TV	TV	kA	TV
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
VHF	VHF	kA	VHF
a	a	k8xC	a
UHF	UHF	kA	UHF
analogových	analogový	k2eAgFnPc2d1	analogová
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
nejsou	být	k5eNaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
žádné	žádný	k3yNgFnPc1	žádný
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
analogové	analogový	k2eAgNnSc1d1	analogové
vysílání	vysílání	k1gNnSc1	vysílání
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rádia	rádio	k1gNnSc2	rádio
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
VKV	VKV	kA	VKV
FM	FM	kA	FM
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
je	být	k5eAaImIp3nS	být
provozováno	provozovat	k5eAaImNgNnS	provozovat
následujících	následující	k2eAgInPc2d1	následující
8	[number]	k4	8
stanic	stanice	k1gFnPc2	stanice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Šenkýř	Šenkýř	k1gMnSc1	Šenkýř
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
:	:	kIx,	:
Nejvýše	vysoce	k6eAd3	vysoce
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
?	?	kIx.	?
</s>
<s>
Kdepak	kdepak	k9	kdepak
<g/>
,	,	kIx,	,
Praděd	praděd	k1gMnSc1	praděd
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
magazín	magazín	k1gInSc1	magazín
Pátek	pátek	k1gInSc1	pátek
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rozhledna	rozhledna	k1gFnSc1	rozhledna
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
vysílač	vysílač	k1gMnSc1	vysílač
Praděd	praděd	k1gMnSc1	praděd
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.ejeseniky.com	www.ejeseniky.com	k1gInSc1	www.ejeseniky.com
</s>
</p>
