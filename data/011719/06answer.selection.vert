<s>
Vrchol	vrchol	k1gInSc1	vrchol
vysílače	vysílač	k1gInSc2	vysílač
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
pevným	pevný	k2eAgMnSc7d1	pevný
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
umělým	umělý	k2eAgInSc7d1	umělý
<g/>
)	)	kIx)	)
bodem	bod	k1gInSc7	bod
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
mezi	mezi	k7c7	mezi
1637	[number]	k4	1637
a	a	k8xC	a
1638	[number]	k4	1638
metry	metr	k1gInPc4	metr
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
vrchol	vrchol	k1gInSc1	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
<g/>
.	.	kIx.	.
</s>
