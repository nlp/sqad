<s>
Patent	patent	k1gInSc1	patent
je	být	k5eAaImIp3nS	být
zákonná	zákonný	k2eAgFnSc1d1	zákonná
ochrana	ochrana	k1gFnSc1	ochrana
vynálezů	vynález	k1gInPc2	vynález
zaručující	zaručující	k2eAgFnSc1d1	zaručující
vlastníkovi	vlastník	k1gMnSc3	vlastník
patentu	patent	k1gInSc2	patent
výhradní	výhradní	k2eAgNnSc4d1	výhradní
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
průmyslovému	průmyslový	k2eAgNnSc3d1	průmyslové
využití	využití	k1gNnSc3	využití
vynálezu	vynález	k1gInSc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
udělování	udělování	k1gNnSc2	udělování
patentů	patent	k1gInPc2	patent
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
527	[number]	k4	527
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vynálezech	vynález	k1gInPc6	vynález
a	a	k8xC	a
zlepšovacích	zlepšovací	k2eAgInPc6d1	zlepšovací
návrzích	návrh	k1gInPc6	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
patenty	patent	k1gInPc7	patent
udělují	udělovat	k5eAaImIp3nP	udělovat
na	na	k7c4	na
vynálezy	vynález	k1gInPc4	vynález
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
vynálezecké	vynálezecký	k2eAgFnSc2d1	vynálezecká
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
průmyslově	průmyslově	k6eAd1	průmyslově
využitelné	využitelný	k2eAgInPc1d1	využitelný
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
stavu	stav	k1gInSc2	stav
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Stavem	stav	k1gInSc7	stav
techniky	technika	k1gFnSc2	technika
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
přede	před	k7c7	před
dnem	den	k1gInSc7	den
přihlášení	přihlášení	k1gNnSc1	přihlášení
patentu	patent	k1gInSc2	patent
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vynálezy	vynález	k1gInPc4	vynález
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
nepovažují	považovat	k5eNaImIp3nP	považovat
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
objevy	objev	k1gInPc4	objev
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgFnPc4d1	vědecká
teorie	teorie	k1gFnPc4	teorie
a	a	k8xC	a
matematické	matematický	k2eAgFnPc4d1	matematická
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
pouhé	pouhý	k2eAgFnPc4d1	pouhá
vnější	vnější	k2eAgFnPc4d1	vnější
úpravy	úprava	k1gFnPc4	úprava
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
plány	plán	k1gInPc1	plán
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
způsoby	způsob	k1gInPc1	způsob
vykonávání	vykonávání	k1gNnSc2	vykonávání
duševní	duševní	k2eAgFnSc2d1	duševní
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
programy	program	k1gInPc4	program
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
pouhé	pouhý	k2eAgNnSc1d1	pouhé
uvedení	uvedení	k1gNnSc1	uvedení
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
patentu	patent	k1gInSc2	patent
má	mít	k5eAaImIp3nS	mít
výlučné	výlučný	k2eAgNnSc1d1	výlučné
právo	právo	k1gNnSc1	právo
vynález	vynález	k1gInSc4	vynález
využívat	využívat	k5eAaPmF	využívat
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
výrobek	výrobek	k1gInSc1	výrobek
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
,	,	kIx,	,
uvádět	uvádět	k5eAaImF	uvádět
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
nebo	nebo	k8xC	nebo
upotřebit	upotřebit	k5eAaPmF	upotřebit
postup	postup	k1gInSc4	postup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
poskytnout	poskytnout	k5eAaPmF	poskytnout
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
vynálezu	vynález	k1gInSc2	vynález
jiným	jiný	k2eAgFnPc3d1	jiná
osobám	osoba	k1gFnPc3	osoba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
licenční	licenční	k2eAgFnSc7d1	licenční
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
převést	převést	k5eAaPmF	převést
patent	patent	k1gInSc4	patent
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
patent	patent	k1gInSc1	patent
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
platit	platit	k5eAaImF	platit
tzv.	tzv.	kA	tzv.
udržovací	udržovací	k2eAgInPc4d1	udržovací
poplatky	poplatek	k1gInPc4	poplatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státu	stát	k1gInSc6	stát
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
možná	možný	k2eAgFnSc1d1	možná
délka	délka	k1gFnSc1	délka
patentové	patentový	k2eAgFnSc2d1	patentová
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
řeckém	řecký	k2eAgNnSc6d1	řecké
městě	město	k1gNnSc6	město
Sybaris	Sybaris	k1gFnSc2	Sybaris
(	(	kIx(	(
<g/>
nacházejícím	nacházející	k2eAgInPc3d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
jižní	jižní	k2eAgFnSc2d1	jižní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
podporován	podporován	k2eAgInSc1d1	podporován
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
objevil	objevit	k5eAaPmAgMnS	objevit
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
nebo	nebo	k8xC	nebo
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
luxusem	luxus	k1gInSc7	luxus
a	a	k8xC	a
podporován	podporovat	k5eAaImNgMnS	podporovat
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
výzkumu	výzkum	k1gInSc6	výzkum
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Florentinský	florentinský	k2eAgMnSc1d1	florentinský
architekt	architekt	k1gMnSc1	architekt
Filippo	Filippa	k1gFnSc5	Filippa
Brunelleschi	Brunelleschi	k1gNnPc6	Brunelleschi
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
patent	patent	k1gInSc1	patent
na	na	k7c4	na
člun	člun	k1gInSc4	člun
se	s	k7c7	s
zdvihacím	zdvihací	k2eAgNnSc7d1	zdvihací
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
převážel	převážet	k5eAaImAgMnS	převážet
mramor	mramor	k1gInSc4	mramor
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Arno	Arno	k6eAd1	Arno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1421	[number]	k4	1421
<g/>
.	.	kIx.	.
</s>
<s>
Patent	patent	k1gInSc1	patent
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
však	však	k9	však
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1474	[number]	k4	1474
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Benátská	benátský	k2eAgFnSc1d1	Benátská
republika	republika	k1gFnSc1	republika
přijala	přijmout	k5eAaPmAgFnS	přijmout
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgNnSc2	který
nové	nový	k2eAgInPc1d1	nový
vynálezy	vynález	k1gInPc1	vynález
<g/>
,	,	kIx,	,
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
než	než	k8xS	než
byly	být	k5eAaImAgInP	být
uvedené	uvedený	k2eAgInPc1d1	uvedený
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
předány	předat	k5eAaPmNgInP	předat
úřadu	úřad	k1gInSc3	úřad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získaly	získat	k5eAaPmAgInP	získat
právo	právo	k1gNnSc4	právo
bránit	bránit	k5eAaImF	bránit
ostatním	ostatní	k1gNnSc7	ostatní
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
používání	používání	k1gNnSc6	používání
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
za	za	k7c4	za
krále	král	k1gMnSc4	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
následovala	následovat	k5eAaImAgFnS	následovat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
"	"	kIx"	"
<g/>
Statutem	statut	k1gInSc7	statut
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
patenty	patent	k1gInPc1	patent
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uděleny	udělit	k5eAaPmNgInP	udělit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
projekty	projekt	k1gInPc4	projekt
nových	nový	k2eAgInPc2d1	nový
vynálezů	vynález	k1gInPc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
panování	panování	k1gNnSc2	panování
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
1702	[number]	k4	1702
<g/>
–	–	k?	–
<g/>
1714	[number]	k4	1714
<g/>
)	)	kIx)	)
právníci	právník	k1gMnPc1	právník
anglického	anglický	k2eAgInSc2d1	anglický
dvora	dvůr	k1gInSc2	dvůr
zavedli	zavést	k5eAaPmAgMnP	zavést
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vždy	vždy	k6eAd1	vždy
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
přiložen	přiložen	k2eAgInSc4d1	přiložen
písemný	písemný	k2eAgInSc4d1	písemný
popis	popis	k1gInSc4	popis
vynálezu	vynález	k1gInSc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Patentový	patentový	k2eAgInSc1d1	patentový
systém	systém	k1gInSc1	systém
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
souvislosti	souvislost	k1gFnPc4	souvislost
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
Statutem	statut	k1gInSc7	statut
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byly	být	k5eAaImAgInP	být
patenty	patent	k1gInPc1	patent
udělovány	udělovat	k5eAaImNgInP	udělovat
monarchií	monarchie	k1gFnSc7	monarchie
a	a	k8xC	a
také	také	k9	také
ostatními	ostatní	k2eAgFnPc7d1	ostatní
institucemi	instituce	k1gFnPc7	instituce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgNnP	být
třeba	třeba	k6eAd1	třeba
"	"	kIx"	"
<g/>
Maison	Maison	k1gInSc1	Maison
du	du	k?	du
Roi	Roi	k1gFnSc2	Roi
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc4d1	Nové
vynálezy	vynález	k1gInPc4	vynález
prověřovala	prověřovat	k5eAaImAgFnS	prověřovat
Akademie	akademie	k1gFnSc1	akademie
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
popis	popis	k1gInSc4	popis
vynálezu	vynález	k1gInSc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgNnSc1d1	skutečné
využití	využití	k1gNnSc1	využití
vynálezu	vynález	k1gInSc2	vynález
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
zpřístupnění	zpřístupnění	k1gNnSc4	zpřístupnění
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
francouzský	francouzský	k2eAgInSc1d1	francouzský
patentový	patentový	k2eAgInSc1d1	patentový
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
<s>
Patenty	patent	k1gInPc1	patent
byly	být	k5eAaImAgInP	být
uděleny	udělit	k5eAaPmNgInP	udělit
bez	bez	k7c2	bez
prověřování	prověřování	k1gNnSc2	prověřování
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
autorské	autorský	k2eAgNnSc1d1	autorské
právo	právo	k1gNnSc1	právo
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c7	za
hlavní	hlavní	k2eAgFnSc7d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
období	období	k1gNnSc2	období
z	z	k7c2	z
let	léto	k1gNnPc2	léto
konfederace	konfederace	k1gFnSc2	konfederace
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
přijaly	přijmout	k5eAaPmAgInP	přijmout
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
patentové	patentový	k2eAgInPc4d1	patentový
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
kongres	kongres	k1gInSc1	kongres
přijal	přijmout	k5eAaPmAgInS	přijmout
zákon	zákon	k1gInSc4	zákon
"	"	kIx"	"
<g/>
Patent	patent	k1gInSc1	patent
Act	Act	k1gFnSc2	Act
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
patentu	patent	k1gInSc6	patent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
a	a	k8xC	a
první	první	k4xOgInSc1	první
patent	patent	k1gInSc1	patent
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1790	[number]	k4	1790
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Samuela	Samuel	k1gMnSc4	Samuel
Hopkinse	Hopkins	k1gMnSc4	Hopkins
z	z	k7c2	z
Vermontu	Vermont	k1gInSc2	Vermont
na	na	k7c4	na
výrobní	výrobní	k2eAgFnSc4d1	výrobní
techniku	technika	k1gFnSc4	technika
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
praxí	praxe	k1gFnPc2	praxe
s	s	k7c7	s
patentem	patent	k1gInSc7	patent
dodat	dodat	k5eAaPmF	dodat
dva	dva	k4xCgInPc4	dva
funkční	funkční	k2eAgInPc4d1	funkční
prototypy	prototyp	k1gInPc4	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
patentů	patent	k1gInPc2	patent
neustále	neustále	k6eAd1	neustále
strmě	strmě	k6eAd1	strmě
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
úloze	úloha	k1gFnSc6	úloha
patentů	patent	k1gInPc2	patent
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
inovace	inovace	k1gFnPc4	inovace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistých	jistý	k2eAgNnPc6d1	jisté
odvětvích	odvětví	k1gNnPc6	odvětví
je	být	k5eAaImIp3nS	být
lhůta	lhůta	k1gFnSc1	lhůta
příliš	příliš	k6eAd1	příliš
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
pak	pak	k6eAd1	pak
příliš	příliš	k6eAd1	příliš
krátká	krátký	k2eAgFnSc1d1	krátká
(	(	kIx(	(
<g/>
zkrácena	zkrátit	k5eAaPmNgFnS	zkrátit
o	o	k7c4	o
mnohaleté	mnohaletý	k2eAgFnPc4d1	mnohaletá
klinické	klinický	k2eAgFnPc4d1	klinická
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c4	na
léky	lék	k1gInPc4	lék
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patentování	patentování	k1gNnSc1	patentování
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
také	také	k9	také
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
negativní	negativní	k2eAgFnPc4d1	negativní
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Patenty	patent	k1gInPc1	patent
zneužívají	zneužívat	k5eAaImIp3nP	zneužívat
i	i	k9	i
patentoví	patentový	k2eAgMnPc1d1	patentový
trollové	troll	k1gMnPc1	troll
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
patent	patent	k1gInSc4	patent
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Monopol	monopol	k1gInSc1	monopol
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
Užitný	užitný	k2eAgInSc1d1	užitný
vzor	vzor	k1gInSc4	vzor
Softwarový	softwarový	k2eAgInSc4d1	softwarový
patent	patent	k1gInSc4	patent
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
patent	patent	k1gInSc1	patent
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
patent	patent	k1gInSc4	patent
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Úřad	úřad	k1gInSc1	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Evropský	evropský	k2eAgInSc1d1	evropský
patentový	patentový	k2eAgInSc1d1	patentový
úřad	úřad	k1gInSc1	úřad
Espacenet	Espacenet	k1gInSc1	Espacenet
-	-	kIx~	-
online	onlinout	k5eAaPmIp3nS	onlinout
služba	služba	k1gFnSc1	služba
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
EPÚ	EPÚ	kA	EPÚ
pro	pro	k7c4	pro
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
patentů	patent	k1gInPc2	patent
zdarma	zdarma	k6eAd1	zdarma
</s>
