<p>
<s>
Plexis	Plexis	k1gFnSc1	Plexis
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Plexis	Plexis	k1gInSc1	Plexis
PM	PM	kA	PM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
punková	punkový	k2eAgFnSc1d1	punková
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
členem	člen	k1gMnSc7	člen
je	být	k5eAaImIp3nS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
frontman	frontman	k1gMnSc1	frontman
Petr	Petr	k1gMnSc1	Petr
Hošek	Hošek	k1gMnSc1	Hošek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
kolem	kolem	k7c2	kolem
mladého	mladý	k2eAgMnSc2d1	mladý
vyšehradského	vyšehradský	k2eAgMnSc2d1	vyšehradský
punkáče	punkáč	k1gMnSc2	punkáč
Petra	Petr	k1gMnSc2	Petr
"	"	kIx"	"
<g/>
Sida	Sidus	k1gMnSc2	Sidus
<g/>
"	"	kIx"	"
Hoška	Hošek	k1gMnSc2	Hošek
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1984	[number]	k4	1984
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
jako	jako	k9	jako
Plexis	Plexis	k1gFnSc1	Plexis
PM	PM	kA	PM
(	(	kIx(	(
<g/>
punk	punk	k1gMnSc1	punk
music	music	k1gMnSc1	music
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
první	první	k4xOgFnSc6	první
sestavě	sestava	k1gFnSc6	sestava
odehráli	odehrát	k5eAaPmAgMnP	odehrát
Plexis	Plexis	k1gInSc4	Plexis
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyprodaný	vyprodaný	k2eAgInSc1d1	vyprodaný
dvojkoncert	dvojkoncert	k1gInSc1	dvojkoncert
na	na	k7c6	na
Chmelnici	chmelnice	k1gFnSc6	chmelnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
odešli	odejít	k5eAaPmAgMnP	odejít
baskytarista	baskytarista	k1gMnSc1	baskytarista
Marek	Marek	k1gMnSc1	Marek
Sibřina	Sibřina	k1gFnSc1	Sibřina
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Martin	Martin	k1gMnSc1	Martin
Bíňovec	Bíňovec	k1gMnSc1	Bíňovec
a	a	k8xC	a
Plexis	Plexis	k1gFnSc1	Plexis
nadále	nadále	k6eAd1	nadále
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
jako	jako	k8xC	jako
trio	trio	k1gNnSc4	trio
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
zahrál	zahrát	k5eAaPmAgInS	zahrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
zpěvák	zpěvák	k1gMnSc1	zpěvák
Visacího	visací	k2eAgInSc2d1	visací
zámku	zámek	k1gInSc2	zámek
Jan	Jan	k1gMnSc1	Jan
Haubert	Haubert	k1gMnSc1	Haubert
<g/>
.	.	kIx.	.
</s>
<s>
Odehráli	odehrát	k5eAaPmAgMnP	odehrát
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
legendárních	legendární	k2eAgInPc2d1	legendární
koncertů	koncert	k1gInPc2	koncert
(	(	kIx(	(
<g/>
Stará	starý	k2eAgFnSc1d1	stará
Lysá	Lysá	k1gFnSc1	Lysá
a	a	k8xC	a
Hudební	hudební	k2eAgInSc1d1	hudební
parník	parník	k1gInSc1	parník
Vltava	Vltava	k1gFnSc1	Vltava
–	–	k?	–
obě	dva	k4xCgFnPc1	dva
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
koncertě	koncert	k1gInSc6	koncert
na	na	k7c6	na
Opatově	opatův	k2eAgInSc6d1	opatův
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
přichází	přicházet	k5eAaImIp3nS	přicházet
rozpad	rozpad	k1gInSc1	rozpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
sestava	sestava	k1gFnSc1	sestava
hrála	hrát	k5eAaImAgFnS	hrát
punk	punk	k1gInSc4	punk
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
The	The	k1gFnPc2	The
Exploited	Exploited	k1gInSc4	Exploited
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
vtipnými	vtipný	k2eAgInPc7d1	vtipný
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
nebylo	být	k5eNaImAgNnS	být
až	až	k9	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
slyšet	slyšet	k5eAaImF	slyšet
(	(	kIx(	(
<g/>
Hošek	Hošek	k1gMnSc1	Hošek
mezitím	mezitím	k6eAd1	mezitím
spoluzaložil	spoluzaložit	k5eAaPmAgMnS	spoluzaložit
Do	do	k7c2	do
Řady	řada	k1gFnSc2	řada
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
v	v	k7c4	v
Michael	Michael	k1gMnSc1	Michael
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Uncle	Uncle	k1gNnSc7	Uncle
<g/>
,	,	kIx,	,
Vitáček	Vitáček	k1gInSc1	Vitáček
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Brožem	Brož	k1gMnSc7	Brož
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
Visacím	visací	k2eAgInSc6d1	visací
zámku	zámek	k1gInSc6	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Tito	tento	k3xDgMnPc1	tento
noví	nový	k2eAgMnPc1d1	nový
Plexis	Plexis	k1gFnPc1	Plexis
už	už	k6eAd1	už
nehrají	hrát	k5eNaImIp3nP	hrát
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
The	The	k1gMnSc1	The
Exploited	Exploited	k1gMnSc1	Exploited
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
vlivy	vliv	k1gInPc4	vliv
street-rocku	streetock	k1gInSc2	street-rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
nahráli	nahrát	k5eAaPmAgMnP	nahrát
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
rebel	rebel	k1gMnSc1	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
zní	znět	k5eAaImIp3nS	znět
Plexis	Plexis	k1gFnSc4	Plexis
ještě	ještě	k9	ještě
punkově	punkově	k6eAd1	punkově
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
White	Whit	k1gInSc5	Whit
Killer	Killer	k1gMnSc1	Killer
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
III	III	kA	III
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
streetrocková	streetrockový	k2eAgNnPc1d1	streetrockový
<g/>
.	.	kIx.	.
</s>
<s>
Obrat	obrat	k1gInSc1	obrat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
punku	punk	k1gInSc3	punk
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Plexis	Plexis	k1gFnPc1	Plexis
předskakují	předskakovat	k5eAaImIp3nP	předskakovat
legendárním	legendární	k2eAgMnSc6d1	legendární
Ramones	Ramonesa	k1gFnPc2	Ramonesa
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Hošek	Hošek	k1gMnSc1	Hošek
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chce	chtít	k5eAaImIp3nS	chtít
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
skupinu	skupina	k1gFnSc4	skupina
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
nové	nový	k2eAgMnPc4d1	nový
členy	člen	k1gMnPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Dušan	Dušan	k1gMnSc1	Dušan
Lébl	Lébl	k1gMnSc1	Lébl
(	(	kIx(	(
<g/>
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Švec	Švec	k1gMnSc1	Švec
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
To	to	k9	to
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
nahrané	nahraný	k2eAgFnPc4d1	nahraná
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
sestavě	sestava	k1gFnSc6	sestava
už	už	k6eAd1	už
zní	znět	k5eAaImIp3nS	znět
zase	zase	k9	zase
punkově	punkově	k6eAd1	punkově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
připojí	připojit	k5eAaPmIp3nS	připojit
druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jen	jen	k9	jen
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
TO	to	k9	to
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
déle	dlouho	k6eAd2	dlouho
jak	jak	k8xS	jak
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
následuje	následovat	k5eAaImIp3nS	následovat
album	album	k1gNnSc4	album
Už	už	k6eAd1	už
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
kroutí	kroutit	k5eAaImIp3nS	kroutit
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
soubor	soubor	k1gInSc1	soubor
odmlčí	odmlčet	k5eAaPmIp3nS	odmlčet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
koncertem	koncert	k1gInSc7	koncert
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
20	[number]	k4	20
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
klubu	klub	k1gInSc6	klub
Abaton	Abaton	k1gInSc1	Abaton
<g/>
.	.	kIx.	.
</s>
<s>
Zahrají	zahrát	k5eAaPmIp3nP	zahrát
tam	tam	k6eAd1	tam
ještě	ještě	k9	ještě
E	E	kA	E
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
SPS	SPS	kA	SPS
<g/>
,	,	kIx,	,
Do	do	k7c2	do
Řady	řada	k1gFnSc2	řada
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
překvapení	překvapení	k1gNnSc1	překvapení
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Sid	Sid	k1gMnSc1	Sid
Vicious	Vicious	k1gMnSc1	Vicious
kapela	kapela	k1gFnSc1	kapela
Tři	tři	k4xCgFnPc1	tři
Sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahraje	zahrát	k5eAaPmIp3nS	zahrát
ještě	ještě	k9	ještě
tři	tři	k4xCgFnPc4	tři
písně	píseň	k1gFnPc4	píseň
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
měli	mít	k5eAaImAgMnP	mít
Plexis	Plexis	k1gFnSc4	Plexis
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechny	všechen	k3xTgFnPc1	všechen
punkové	punkový	k2eAgFnPc1d1	punková
skupiny	skupina	k1gFnPc1	skupina
mnoho	mnoho	k4c1	mnoho
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
StB	StB	k1gFnSc7	StB
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
sám	sám	k3xTgMnSc1	sám
Hošek	Hošek	k1gMnSc1	Hošek
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
oficiálních	oficiální	k2eAgInPc6d1	oficiální
seznamech	seznam	k1gInPc6	seznam
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
vedený	vedený	k2eAgMnSc1d1	vedený
jako	jako	k8xS	jako
agent	agent	k1gMnSc1	agent
StB	StB	k1gMnPc1	StB
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Sid	Sid	k1gFnSc2	Sid
<g/>
.	.	kIx.	.
</s>
<s>
Hošek	Hošek	k1gMnSc1	Hošek
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
odmítá	odmítat	k5eAaImIp3nS	odmítat
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
organizátor	organizátor	k1gInSc1	organizátor
Trutnovského	trutnovský	k2eAgInSc2d1	trutnovský
festivalu	festival	k1gInSc2	festival
Martin	Martin	k2eAgInSc1d1	Martin
Věchet	věchet	k1gInSc1	věchet
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelé	pořadatel	k1gMnPc1	pořadatel
Českého	český	k2eAgInSc2d1	český
Woodstocku	Woodstock	k1gInSc2	Woodstock
Petra	Petr	k1gMnSc4	Petr
Hoška	Hošek	k1gMnSc4	Hošek
požadali	požadat	k5eAaPmAgMnP	požadat
o	o	k7c4	o
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
nereagoval	reagovat	k5eNaBmAgInS	reagovat
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
festivalu	festival	k1gInSc6	festival
Plexis	Plexis	k1gFnSc2	Plexis
nevystupují	vystupovat	k5eNaImIp3nP	vystupovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Plexis	Plexis	k1gInSc1	Plexis
oslaví	oslavit	k5eAaPmIp3nS	oslavit
25	[number]	k4	25
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
během	během	k7c2	během
večera	večer	k1gInSc2	večer
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
KC	KC	kA	KC
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
kolegové	kolega	k1gMnPc1	kolega
z	z	k7c2	z
punkové	punkový	k2eAgFnSc2d1	punková
scény	scéna	k1gFnSc2	scéna
jako	jako	k8xC	jako
S.	S.	kA	S.
<g/>
P.	P.	kA	P.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
V.Ú.	V.Ú.	k1gFnSc1	V.Ú.
nebo	nebo	k8xC	nebo
E	E	kA	E
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
E.	E.	kA	E.
Přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
zahrál	zahrát	k5eAaPmAgMnS	zahrát
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
písních	píseň	k1gFnPc6	píseň
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bubeník	bubeník	k1gMnSc1	bubeník
Plexis	Plexis	k1gFnSc2	Plexis
Adolf	Adolf	k1gMnSc1	Adolf
Vitáček	Vitáček	k1gMnSc1	Vitáček
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
také	také	k9	také
album	album	k1gNnSc1	album
Best	Besta	k1gFnPc2	Besta
Off	Off	k1gFnPc2	Off
,	,	kIx,	,
které	který	k3yIgFnPc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výběr	výběr	k1gInSc1	výběr
22	[number]	k4	22
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
skladeb	skladba	k1gFnPc2	skladba
plus	plus	k6eAd1	plus
3	[number]	k4	3
nové	nový	k2eAgFnSc2d1	nová
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
kytarista	kytarista	k1gMnSc1	kytarista
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
časově	časově	k6eAd1	časově
náročného	náročný	k2eAgNnSc2d1	náročné
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Tři	tři	k4xCgFnPc1	tři
Sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Plexis	Plexis	k1gFnPc1	Plexis
zažívají	zažívat	k5eAaImIp3nP	zažívat
pauzu	pauza	k1gFnSc4	pauza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
Plexis	Plexis	k1gFnSc2	Plexis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
hraje	hrát	k5eAaImIp3nS	hrát
jen	jen	k9	jen
ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nekoncertuje	koncertovat	k5eNaImIp3nS	koncertovat
se	s	k7c7	s
Sestrama	Sestrama	k?	Sestrama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
Plexis	Plexis	k1gFnSc4	Plexis
na	na	k7c6	na
letních	letní	k2eAgInPc6d1	letní
festivalech	festival	k1gInPc6	festival
začínají	začínat	k5eAaImIp3nP	začínat
odtajňovat	odtajňovat	k5eAaImF	odtajňovat
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
deska	deska	k1gFnSc1	deska
vychází	vycházet	k5eAaImIp3nS	vycházet
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vohul	Vohula	k1gFnPc2	Vohula
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
u	u	k7c2	u
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Papagájův	Papagájův	k2eAgMnSc1d1	Papagájův
Hlasatel	hlasatel	k1gMnSc1	hlasatel
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
klasickém	klasický	k2eAgInSc6d1	klasický
CD	CD	kA	CD
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
na	na	k7c6	na
vinylu	vinyl	k1gInSc6	vinyl
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ještě	ještě	k6eAd1	ještě
DVD	DVD	kA	DVD
Antihero	Antihero	k1gNnSc1	Antihero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
životních	životní	k2eAgFnPc6d1	životní
eskapádách	eskapáda	k1gFnPc6	eskapáda
frontmana	frontman	k1gMnSc2	frontman
Petra	Petr	k1gMnSc2	Petr
Hoška	Hošek	k1gMnSc2	Hošek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2017	[number]	k4	2017
vychází	vycházet	k5eAaImIp3nS	vycházet
nová	nový	k2eAgFnSc1d1	nová
řadová	řadový	k2eAgFnSc1d1	řadová
deska	deska	k1gFnSc1	deska
"	"	kIx"	"
Kašpar	Kašpar	k1gMnSc1	Kašpar
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
5	[number]	k4	5
starších	starý	k2eAgFnPc2d2	starší
předělávek	předělávka	k1gFnPc2	předělávka
od	od	k7c2	od
spřízněných	spřízněný	k2eAgFnPc2d1	spřízněná
kapel	kapela	k1gFnPc2	kapela
(	(	kIx(	(
Nežfaleš	Nežfaleš	k1gMnSc1	Nežfaleš
<g/>
,	,	kIx,	,
Našrot	Našrot	k1gMnSc1	Našrot
<g/>
,	,	kIx,	,
SPS	SPS	kA	SPS
<g/>
,	,	kIx,	,
Degradace	degradace	k1gFnSc1	degradace
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Fialky	fialka	k1gFnSc2	fialka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
dálnici	dálnice	k1gFnSc4	dálnice
D1	D1	k1gFnSc4	D1
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
zemřela	zemřít	k5eAaPmAgFnS	zemřít
dvaadvacetiletá	dvaadvacetiletý	k2eAgFnSc1d1	dvaadvacetiletá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
zraněni	zranit	k5eAaPmNgMnP	zranit
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Řadová	řadový	k2eAgNnPc4d1	řadové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Půlnoční	půlnoční	k2eAgMnSc1d1	půlnoční
rebel	rebel	k1gMnSc1	rebel
-	-	kIx~	-
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
White	White	k5eAaPmIp2nP	White
killer	killer	k1gInSc4	killer
-	-	kIx~	-
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
III	III	kA	III
-	-	kIx~	-
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
To	to	k9	to
-	-	kIx~	-
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
kroutí	kroutit	k5eAaImIp3nS	kroutit
nohy	noha	k1gFnPc4	noha
-	-	kIx~	-
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Vohul	Vohout	k5eAaPmAgInS	Vohout
to	ten	k3xDgNnSc4	ten
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Kašpar	Kašpar	k1gMnSc1	Kašpar
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
-	-	kIx~	-
2017	[number]	k4	2017
</s>
</p>
<p>
<s>
===	===	k?	===
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
Plexis	Plexis	k1gFnSc1	Plexis
XX	XX	kA	XX
-	-	kIx~	-
disk	disk	k1gInSc1	disk
I	I	kA	I
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Rock	rock	k1gInSc1	rock
For	forum	k1gNnPc2	forum
People	People	k1gFnSc1	People
5	[number]	k4	5
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
disk	disk	k1gInSc1	disk
II	II	kA	II
natočen	natočit	k5eAaBmNgInS	natočit
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
20	[number]	k4	20
let	léto	k1gNnPc2	léto
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Abaton	Abaton	k1gInSc1	Abaton
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2004-	[number]	k4	2004-
oficialně	oficialně	k6eAd1	oficialně
nevydáno	vydán	k2eNgNnSc4d1	nevydáno
</s>
</p>
<p>
<s>
Nesmyslný	smyslný	k2eNgMnSc1d1	nesmyslný
narozeniny	narozeniny	k1gFnPc4	narozeniny
-	-	kIx~	-
záznam	záznam	k1gInSc4	záznam
pořízen	pořízen	k2eAgInSc4d1	pořízen
v	v	k7c6	v
Retro	retro	k1gNnSc6	retro
Music	Musice	k1gFnPc2	Musice
Hall	Hallum	k1gNnPc2	Hallum
8	[number]	k4	8
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
40	[number]	k4	40
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
Petra	Petr	k1gMnSc4	Petr
Hoška	Hošek	k1gMnSc4	Hošek
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
byl	být	k5eAaImAgInS	být
pořízen	pořídit	k5eAaPmNgInS	pořídit
také	také	k9	také
audiozáznam-	audiozáznam-	k?	audiozáznam-
oficialně	oficialně	k6eAd1	oficialně
nevydáno	vydán	k2eNgNnSc1d1	nevydáno
</s>
</p>
<p>
<s>
Antihero	Antihero	k1gNnSc1	Antihero
-	-	kIx~	-
oficiálně	oficiálně	k6eAd1	oficiálně
vydaný	vydaný	k2eAgInSc1d1	vydaný
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
životních	životní	k2eAgFnPc6d1	životní
eskapádách	eskapáda	k1gFnPc6	eskapáda
Petra	Petr	k1gMnSc2	Petr
Hoška	Hošek	k1gMnSc2	Hošek
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
hudební	hudební	k2eAgMnSc1d1	hudební
publicista	publicista	k1gMnSc1	publicista
Radim	Radim	k1gMnSc1	Radim
Řezníček	Řezníček	k1gMnSc1	Řezníček
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bonus	bonus	k1gInSc1	bonus
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
disku	disk	k1gInSc6	disk
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
ve	v	k7c6	v
Strenici	Strenice	k1gFnSc6	Strenice
u	u	k7c2	u
Mladé	mladý	k2eAgFnSc2d1	mladá
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnPc1	kompilace
a	a	k8xC	a
demo	demo	k2eAgFnPc1d1	demo
===	===	k?	===
</s>
</p>
<p>
<s>
Demo	demo	k2eAgInPc2d1	demo
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Akce	akce	k1gFnSc1	akce
Punk	punk	k1gMnSc1	punk
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rebelie	rebelie	k1gFnSc1	rebelie
punk	punk	k1gInSc1	punk
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
oi	oi	k?	oi
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Best	Best	k1gInSc1	Best
of	of	k?	of
25	[number]	k4	25
let	léto	k1gNnPc2	léto
-	-	kIx~	-
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Live	Live	k1gNnSc4	Live
nahrávky	nahrávka	k1gFnPc1	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
parník	parník	k1gInSc1	parník
Vltava	Vltava	k1gFnSc1	Vltava
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
Lysá	Lysá	k1gFnSc1	Lysá
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chmelnice	chmelnice	k1gFnSc1	chmelnice
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
hlavních	hlavní	k2eAgFnPc2d1	hlavní
sestav	sestava	k1gFnPc2	sestava
</s>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgInSc1d1	původní
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
"	"	kIx"	"
<g/>
Sid	Sid	k1gMnSc1	Sid
<g/>
"	"	kIx"	"
Hošek	Hošek	k1gMnSc1	Hošek
–	–	k?	–
hlavní	hlavní	k2eAgInSc1d1	hlavní
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Sibřina	Sibřina	k1gFnSc1	Sibřina
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Bíňovec	Bíňovec	k1gMnSc1	Bíňovec
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Áda	Áda	k?	Áda
Vitáček	Vitáček	k1gInSc1	Vitáček
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
</s>
</p>
<p>
<s>
===	===	k?	===
1991	[number]	k4	1991
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hošek	Hošek	k1gMnSc1	Hošek
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brož	Brož	k1gMnSc1	Brož
–	–	k?	–
basa	basa	k1gFnSc1	basa
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Johny	John	k1gMnPc4	John
Chaos	chaos	k1gInSc4	chaos
Jukl	juknout	k5eAaPmAgInS	juknout
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
P.	P.	kA	P.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Arva	Arva	k1gFnSc1	Arva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Áda	Áda	k?	Áda
Vitáček	Vitáček	k1gMnSc1	Vitáček
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
Wanastovi	Wanastův	k2eAgMnPc1d1	Wanastův
Vjecy	Vjeca	k1gFnSc2	Vjeca
<g/>
,	,	kIx,	,
Inkoust	inkoust	k1gInSc4	inkoust
pana	pan	k1gMnSc2	pan
Boučka	Bouček	k1gMnSc2	Bouček
<g/>
,	,	kIx,	,
Moped	moped	k1gInSc1	moped
<g/>
,	,	kIx,	,
Orlík	Orlík	k1gInSc1	Orlík
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
1992	[number]	k4	1992
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hošek	Hošek	k1gMnSc1	Hošek
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
</s>
</p>
<p>
<s>
Jarda	Jarda	k1gMnSc1	Jarda
Stuchlý	Stuchlý	k2eAgMnSc1d1	Stuchlý
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
(	(	kIx(	(
dále	daleko	k6eAd2	daleko
Majklův	Majklův	k2eAgMnSc1d1	Majklův
Strýček	strýček	k1gMnSc1	strýček
<g/>
,	,	kIx,	,
Maradona	Maradona	k1gFnSc1	Maradona
Jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
Vanessa	Vanessa	k1gFnSc1	Vanessa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Johny	John	k1gMnPc4	John
Chaos	chaos	k1gInSc1	chaos
Jukl	juknout	k5eAaPmAgMnS	juknout
–	–	k?	–
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc4	vokál
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Kolací	kolace	k1gFnPc2	kolace
–	–	k?	–
basa	basa	k1gFnSc1	basa
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
Ström	Ström	k1gInSc1	Ström
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
1996-2017	[number]	k4	1996-2017
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hošek	Hošek	k1gMnSc1	Hošek
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
basa	basa	k1gFnSc1	basa
</s>
</p>
<p>
<s>
Dušan	Dušan	k1gMnSc1	Dušan
Lébl	Lébl	k1gMnSc1	Lébl
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Petr	Petr	k1gMnSc1	Petr
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999-	[number]	k4	1999-
2010	[number]	k4	2010
(	(	kIx(	(
P.	P.	kA	P.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
Scvrklej	Scvrklej	k?	Scvrklej
mozek	mozek	k1gInSc1	mozek
<g/>
,	,	kIx,	,
Arva	Arva	k1gFnSc1	Arva
<g/>
,	,	kIx,	,
Harashi	Harashi	k1gNnSc1	Harashi
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Švec	Švec	k1gMnSc1	Švec
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
</s>
</p>
<p>
<s>
===	===	k?	===
Současná	současný	k2eAgFnSc1d1	současná
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hošek	Hošek	k1gMnSc1	Hošek
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
kapela	kapela	k1gFnSc1	kapela
Maradona	Maradona	k1gFnSc1	Maradona
Jazz	jazz	k1gInSc4	jazz
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
jako	jako	k9	jako
Dj	Dj	k1gMnSc5	Dj
Mucho	Mucha	k1gMnSc5	Mucha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dušan	Dušan	k1gMnSc1	Dušan
Lébl	Lébl	k1gMnSc1	Lébl
-	-	kIx~	-
voc	voc	k?	voc
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
kapela	kapela	k1gFnSc1	kapela
Apple	Apple	kA	Apple
Juice	Juice	k1gFnSc1	Juice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Švec	Švec	k1gMnSc1	Švec
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
kapela	kapela	k1gFnSc1	kapela
Egotrip	Egotrip	k1gMnSc1	Egotrip
<g/>
,	,	kIx,	,
Slaves	Slaves	k1gMnSc1	Slaves
Of	Of	k1gFnSc2	Of
Stadium	stadium	k1gNnSc1	stadium
Rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eda	Eda	k1gMnSc1	Eda
Fröhlich	Fröhlich	k1gMnSc1	Fröhlich
-	-	kIx~	-
basa	basa	k1gFnSc1	basa
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
apple	apple	k6eAd1	apple
juice	juiko	k6eAd1	juiko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Plexis	Plexis	k1gFnSc2	Plexis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Plexis	Plexis	k1gFnSc2	Plexis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Hoškem	Hošek	k1gMnSc7	Hošek
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Kytary	kytara	k1gFnSc2	kytara
a	a	k8xC	a
řev	řev	k1gInSc4	řev
aneb	aneb	k?	aneb
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
za	za	k7c7	za
zdí	zeď	k1gFnSc7	zeď
<g/>
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
