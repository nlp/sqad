<s>
Matematik	matematik	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Matematik	matematik	k1gMnSc1
je	být	k5eAaImIp3nS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
primární	primární	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
studuje	studovat	k5eAaImIp3nS
a	a	k8xC
zkoumá	zkoumat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
matematika	matematika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
je	být	k5eAaImIp3nS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
používá	používat	k5eAaImIp3nS
matematiku	matematika	k1gFnSc4
a	a	k8xC
přináší	přinášet	k5eAaImIp3nS
nové	nový	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
na	na	k7c6
poli	pole	k1gNnSc6
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
nová	nový	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
a	a	k8xC
principy	princip	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
aplikují	aplikovat	k5eAaBmIp3nP
matematickou	matematický	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
nejsou	být	k5eNaImIp3nP
chápáni	chápat	k5eAaImNgMnP
jako	jako	k9
matematici	matematik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
inženýři	inženýr	k1gMnPc1
<g/>
,	,	kIx,
ekonomové	ekonom	k1gMnPc1
<g/>
,	,	kIx,
fyzici	fyzik	k1gMnPc1
<g/>
,	,	kIx,
počítačoví	počítačový	k2eAgMnPc1d1
specialisté	specialista	k1gMnPc1
apod.	apod.	kA
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ženy	žena	k1gFnPc1
se	se	k3xPyFc4
nestávají	stávat	k5eNaImIp3nP
tak	tak	k9
často	často	k6eAd1
matematičkami	matematička	k1gFnPc7
<g/>
,	,	kIx,
protože	protože	k8xS
jako	jako	k9
dívky	dívka	k1gFnPc1
mají	mít	k5eAaImIp3nP
lepší	dobrý	k2eAgFnPc4d2
čtenářské	čtenářský	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
základní	základní	k2eAgFnPc4d1
metodou	metoda	k1gFnSc7
výzkumu	výzkum	k1gInSc2
v	v	k7c6
matematice	matematika	k1gFnSc6
není	být	k5eNaImIp3nS
provádění	provádění	k1gNnSc4
praktických	praktický	k2eAgInPc2d1
experimentů	experiment	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matematika	matematika	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
matematických	matematický	k2eAgInPc6d1
důkazech	důkaz	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravdivost	pravdivost	k1gFnSc1
nové	nový	k2eAgFnSc2d1
</s>
<s>
teorie	teorie	k1gFnSc1
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
od	od	k7c2
již	již	k6eAd1
existujících	existující	k2eAgNnPc2d1
pravdivých	pravdivý	k2eAgNnPc2d1
tvrzení	tvrzení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Počítačové	počítačový	k2eAgInPc1d1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
praktické	praktický	k2eAgInPc1d1
experimenty	experiment	k1gInPc1
nejčastěji	často	k6eAd3
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
ověřování	ověřování	k1gNnSc3
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
postupuje	postupovat	k5eAaImIp3nS
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
nové	nový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
správným	správný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experiment	experiment	k1gInSc1
není	být	k5eNaImIp3nS
regulérní	regulérní	k2eAgInSc4d1
matematický	matematický	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Uživatel	uživatel	k1gMnSc1
Discordu	Discorda	k1gFnSc4
pod	pod	k7c7
nickem	nicek	k1gInSc7
Matematik	matematik	k1gMnSc1
(	(	kIx(
<g/>
Matematik	matematik	k1gMnSc1
<g/>
#	#	kIx~
<g/>
4694	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
potomkem	potomek	k1gMnSc7
ženy	žena	k1gFnSc2
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
Brně	Brno	k1gNnSc6
v	v	k7c6
ulici	ulice	k1gFnSc6
Bratislavská	bratislavský	k2eAgFnSc1d1
a	a	k8xC
lidoopa	lidoop	k1gMnSc2
přesněji	přesně	k6eAd2
Orangutána	orangután	k1gMnSc4
bornejského	bornejský	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
době	doba	k1gFnSc6
páření	páření	k1gNnSc2
byl	být	k5eAaImAgInS
na	na	k7c6
útěku	útěk	k1gInSc6
ze	z	k7c2
Zoo	zoo	k1gFnSc2
ve	v	k7c6
Dvoře	Dvůr	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://medicalxpress.com/news/2016-11-theory-debunks-consensus-math-abilities.html	http://medicalxpress.com/news/2016-11-theory-debunks-consensus-math-abilities.html	k1gInSc1
-	-	kIx~
New	New	k1gFnSc1
theory	theora	k1gFnSc2
debunks	debunksa	k1gFnPc2
consensus	consensus	k1gMnSc1
that	that	k1gMnSc1
math	math	k1gMnSc1
abilities	abilities	k1gMnSc1
are	ar	k1gInSc5
innate	innat	k1gInSc5
<g/>
↑	↑	k?
https://phys.org/news/2019-07-girls-advantage-gender-gap-math.html	https://phys.org/news/2019-07-girls-advantage-gender-gap-math.html	k1gInSc1
-	-	kIx~
Study	stud	k1gInPc1
suggests	suggestsa	k1gFnPc2
girls	girl	k1gFnPc2
<g/>
'	'	kIx"
advantage	advantage	k1gInSc1
in	in	k?
reading	reading	k1gInSc1
explains	explains	k1gInSc1
gender	gender	k1gInSc1
gap	gap	k?
in	in	k?
math	math	k1gMnSc1
fields	fields	k6eAd1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
matematiků	matematik	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
matematik	matematika	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
matematik	matematik	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4037945-0	4037945-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85082138	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85082138	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
