<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
inženýři	inženýr	k1gMnPc1
<g/>
,	,	kIx,
ekonomové	ekonom	k1gMnPc1
<g/>
,	,	kIx,
fyzici	fyzik	k1gMnPc1
<g/>
,	,	kIx,
počítačoví	počítačový	k2eAgMnPc1d1
specialisté	specialista	k1gMnPc1
apod.	apod.	kA
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ženy	žena	k1gFnPc1
se	se	k3xPyFc4
nestávají	stávat	k5eNaImIp3nP
tak	tak	k9
často	často	k6eAd1
matematičkami	matematička	k1gFnPc7
<g/>
,	,	kIx,
protože	protože	k8xS
jako	jako	k9
dívky	dívka	k1gFnPc1
mají	mít	k5eAaImIp3nP
lepší	dobrý	k2eAgFnPc4d2
čtenářské	čtenářský	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>