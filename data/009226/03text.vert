<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
chřipka	chřipka	k1gFnSc1	chřipka
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
chřipkové	chřipkový	k2eAgFnSc2d1	chřipková
pandemie	pandemie	k1gFnSc2	pandemie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Smrtnost	smrtnost	k1gFnSc1	smrtnost
pandemie	pandemie	k1gFnSc1	pandemie
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
mezi	mezi	k7c7	mezi
50	[number]	k4	50
a	a	k8xC	a
100	[number]	k4	100
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
10	[number]	k4	10
až	až	k8xS	až
20	[number]	k4	20
%	%	kIx~	%
z	z	k7c2	z
celkově	celkově	k6eAd1	celkově
nakažených	nakažený	k2eAgMnPc2d1	nakažený
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
příčiny	příčina	k1gFnPc4	příčina
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
a	a	k8xC	a
neobvyklého	obvyklý	k2eNgInSc2d1	neobvyklý
věkového	věkový	k2eAgInSc2d1	věkový
profilu	profil	k1gInSc2	profil
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
efekt	efekt	k1gInSc1	efekt
"	"	kIx"	"
<g/>
cytokinové	cytokinové	k2eAgFnSc1d1	cytokinové
bouře	bouře	k1gFnSc1	bouře
<g/>
"	"	kIx"	"
–	–	k?	–
virus	virus	k1gInSc1	virus
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
příznaky	příznak	k1gInPc4	příznak
<g/>
,	,	kIx,	,
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgInS	způsobit
přehnanou	přehnaný	k2eAgFnSc4d1	přehnaná
reakci	reakce	k1gFnSc4	reakce
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
více	hodně	k6eAd2	hodně
umírali	umírat	k5eAaImAgMnP	umírat
lidé	člověk	k1gMnPc1	člověk
se	s	k7c7	s
silnějším	silný	k2eAgInSc7d2	silnější
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
především	především	k9	především
dospělí	dospělí	k1gMnPc1	dospělí
mezi	mezi	k7c7	mezi
20	[number]	k4	20
a	a	k8xC	a
40	[number]	k4	40
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
vedly	vést	k5eAaImAgInP	vést
také	také	k9	také
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
španělská	španělský	k2eAgFnSc1d1	španělská
chřipka	chřipka	k1gFnSc1	chřipka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přenosem	přenos	k1gInSc7	přenos
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
H5N1	H5N1	k1gFnSc1	H5N1
–	–	k?	–
podtyp	podtyp	k1gInSc4	podtyp
viru	vir	k1gInSc2	vir
ptačí	ptačí	k2eAgFnSc2d1	ptačí
chřipky	chřipka	k1gFnSc2	chřipka
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
rozšíření	rozšíření	k1gNnSc4	rozšíření
ve	v	k7c6	v
světových	světový	k2eAgFnPc6d1	světová
ptačích	ptačí	k2eAgFnPc6d1	ptačí
populacích	populace	k1gFnPc6	populace
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
USA	USA	kA	USA
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Válečná	válečný	k2eAgFnSc1d1	válečná
cenzura	cenzura	k1gFnSc1	cenzura
však	však	k9	však
počátek	počátek	k1gInSc4	počátek
pandemie	pandemie	k1gFnSc2	pandemie
zastřela	zastřít	k5eAaPmAgFnS	zastřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
Španělsku	Španělsko	k1gNnSc6	Španělsko
bylo	být	k5eAaImAgNnS	být
povědomí	povědomí	k1gNnSc1	povědomí
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
dalo	dát	k5eAaPmAgNnS	dát
onemocnění	onemocnění	k1gNnSc1	onemocnění
i	i	k8xC	i
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
historik	historik	k1gMnSc1	historik
lékařství	lékařství	k1gNnSc2	lékařství
Harald	Harald	k1gMnSc1	Harald
Salfellner	Salfellner	k1gMnSc1	Salfellner
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyhodnocení	vyhodnocení	k1gNnSc6	vyhodnocení
demografických	demografický	k2eAgFnPc2d1	demografická
statistik	statistika	k1gFnPc2	statistika
a	a	k8xC	a
reprezentativních	reprezentativní	k2eAgFnPc2d1	reprezentativní
sond	sonda	k1gFnPc2	sonda
z	z	k7c2	z
úmrtních	úmrtní	k2eAgFnPc2d1	úmrtní
matrik	matrika	k1gFnPc2	matrika
<g/>
,	,	kIx,	,
že	že	k8xS	že
excesová	excesový	k2eAgFnSc1d1	excesový
mortalita	mortalita	k1gFnSc1	mortalita
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
španělské	španělský	k2eAgFnSc2d1	španělská
chřipky	chřipka	k1gFnSc2	chřipka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1920	[number]	k4	1918-1920
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
44	[number]	k4	44
000	[number]	k4	000
až	až	k9	až
75	[number]	k4	75
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
číslu	číslo	k1gNnSc3	číslo
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přidat	přidat	k5eAaPmF	přidat
asi	asi	k9	asi
2000	[number]	k4	2000
až	až	k9	až
5000	[number]	k4	5000
vojenských	vojenský	k2eAgFnPc2d1	vojenská
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
úmrtních	úmrtní	k2eAgFnPc6d1	úmrtní
matrikách	matrika	k1gFnPc6	matrika
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
oběti	oběť	k1gFnPc1	oběť
chřipkové	chřipkový	k2eAgFnSc2d1	chřipková
epidemie	epidemie	k1gFnSc2	epidemie
již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
si	se	k3xPyFc3	se
však	však	k9	však
všímají	všímat	k5eAaImIp3nP	všímat
umírání	umírání	k1gNnSc4	umírání
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
například	například	k6eAd1	například
Prager	Prager	k1gMnSc1	Prager
Tagblatt	Tagblatt	k1gMnSc1	Tagblatt
uvádí	uvádět	k5eAaImIp3nS	uvádět
12	[number]	k4	12
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
(	(	kIx(	(
<g/>
pražskou	pražský	k2eAgFnSc4d1	Pražská
<g/>
)	)	kIx)	)
oběť	oběť	k1gFnSc4	oběť
koncipienta	koncipient	k1gMnSc2	koncipient
pražského	pražský	k2eAgNnSc2d1	Pražské
policejního	policejní	k2eAgNnSc2d1	policejní
ředitelství	ředitelství	k1gNnSc2	ředitelství
JUDr.	JUDr.	kA	JUDr.
Egona	Egon	k1gMnSc4	Egon
M.	M.	kA	M.
Proroka	prorok	k1gMnSc4	prorok
ze	z	k7c2	z
Smíchova	Smíchov	k1gInSc2	Smíchov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
chřipky	chřipka	k1gFnSc2	chřipka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgFnPc7d1	další
oběťmi	oběť	k1gFnPc7	oběť
<g />
.	.	kIx.	.
</s>
<s>
byli	být	k5eAaImAgMnP	být
malíři	malíř	k1gMnPc1	malíř
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInPc4	Schiel
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Gilman	Gilman	k1gMnSc1	Gilman
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Autengruber	Autengruber	k1gMnSc1	Autengruber
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
Čeněk	Čeněk	k1gMnSc1	Čeněk
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Edmond	Edmond	k1gMnSc1	Edmond
Rostand	Rostand	k1gInSc1	Rostand
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Margit	Margit	k1gInSc1	Margit
Kaffka	Kaffka	k1gFnSc1	Kaffka
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
zakladatelé	zakladatel	k1gMnPc1	zakladatel
Dodge	Dodg	k1gInSc2	Dodg
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
John	John	k1gMnSc1	John
Francis	Francis	k1gFnSc1	Francis
Dodge	Dodge	k1gFnSc1	Dodge
a	a	k8xC	a
Horace	Horace	k1gFnSc1	Horace
Elgin	Elgin	k1gInSc1	Elgin
Dodge	Dodge	k1gFnSc1	Dodge
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Gaby	Gaba	k1gFnSc2	Gaba
Deslys	Deslysa	k1gFnPc2	Deslysa
<g/>
,	,	kIx,	,
herečky	herečka	k1gFnPc1	herečka
Vera	Vera	k1gMnSc1	Vera
Cholodnaja	Cholodnaja	k1gMnSc1	Cholodnaja
<g/>
,	,	kIx,	,
Myrtle	Myrtle	k1gFnSc1	Myrtle
Gonzalez	Gonzalez	k1gMnSc1	Gonzalez
a	a	k8xC	a
Márta	Márta	k1gMnSc1	Márta
Szentgyörgyi	Szentgyörgy	k1gFnSc2	Szentgyörgy
či	či	k8xC	či
syn	syn	k1gMnSc1	syn
slavného	slavný	k2eAgMnSc2d1	slavný
spisovatele	spisovatel	k1gMnSc2	spisovatel
Arthura	Arthur	k1gMnSc2	Arthur
Conana	Conan	k1gMnSc2	Conan
Doyla	Doyla	k1gMnSc1	Doyla
Arthur	Arthur	k1gMnSc1	Arthur
Alleyne	Alleyn	k1gInSc5	Alleyn
Kingsley	Kingsley	k1gInPc4	Kingsley
Doyle	Doyle	k1gNnSc7	Doyle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SALFELLNER	SALFELLNER	kA	SALFELLNER
<g/>
,	,	kIx,	,
Harald	Harald	k1gMnSc1	Harald
<g/>
.	.	kIx.	.
</s>
<s>
Pandemie	pandemie	k1gFnSc1	pandemie
španělské	španělský	k2eAgFnSc2d1	španělská
chřipky	chřipka	k1gFnSc2	chřipka
1918	[number]	k4	1918
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c4	na
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
a	a	k8xC	a
středoevropské	středoevropský	k2eAgInPc4d1	středoevropský
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Disertační	disertační	k2eAgFnSc1d1	disertační
práce	práce	k1gFnSc1	práce
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
dějin	dějiny	k1gFnPc2	dějiny
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
LF	LF	kA	LF
UK	UK	kA	UK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
197	[number]	k4	197
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SALFELLNER	SALFELLNER	kA	SALFELLNER
<g/>
,	,	kIx,	,
Harald	Harald	k1gMnSc1	Harald
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
chřipka	chřipka	k1gFnSc1	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
pandemie	pandemie	k1gFnSc2	pandemie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vitalis	Vitalis	k1gInSc1	Vitalis
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
168	[number]	k4	168
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7253	[number]	k4	7253
<g/>
-	-	kIx~	-
<g/>
332	[number]	k4	332
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
španělská	španělský	k2eAgFnSc1d1	španělská
chřipka	chřipka	k1gFnSc1	chřipka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
