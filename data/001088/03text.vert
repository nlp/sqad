<s>
Wayne	Waynout	k5eAaPmIp3nS	Waynout
Mark	Mark	k1gMnSc1	Mark
Rooney	Roonea	k1gFnSc2	Roonea
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
profesionální	profesionální	k2eAgMnSc1d1	profesionální
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nyní	nyní	k6eAd1	nyní
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
anglický	anglický	k2eAgInSc4d1	anglický
klub	klub	k1gInSc4	klub
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
a	a	k8xC	a
anglickou	anglický	k2eAgFnSc4d1	anglická
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
jako	jako	k8xC	jako
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kanonýrem	kanonýr	k1gMnSc7	kanonýr
anglické	anglický	k2eAgFnSc2d1	anglická
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
druhým	druhý	k4xOgMnSc7	druhý
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
Premier	Premira	k1gFnPc2	Premira
League	League	k1gInSc1	League
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2002	[number]	k4	2002
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
střelcem	střelec	k1gMnSc7	střelec
Premier	Premira	k1gFnPc2	Premira
League	League	k1gInSc1	League
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
16	[number]	k4	16
let	léto	k1gNnPc2	léto
a	a	k8xC	a
360	[number]	k4	360
dní	den	k1gInPc2	den
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
Everton	Everton	k1gInSc4	Everton
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
gól	gól	k1gInSc1	gól
byl	být	k5eAaImAgInS	být
vítězný	vítězný	k2eAgMnSc1d1	vítězný
a	a	k8xC	a
Everton	Everton	k1gInSc1	Everton
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
porazil	porazit	k5eAaPmAgMnS	porazit
Arsenal	Arsenal	k1gMnSc4	Arsenal
FC	FC	kA	FC
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
ligová	ligový	k2eAgFnSc1d1	ligová
porážka	porážka	k1gFnSc1	porážka
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Golden	Goldno	k1gNnPc2	Goldno
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uděluje	udělovat	k5eAaImIp3nS	udělovat
italský	italský	k2eAgInSc1d1	italský
sportovní	sportovní	k2eAgInSc1d1	sportovní
deník	deník	k1gInSc1	deník
Tuttosport	Tuttosport	k1gInSc1	Tuttosport
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
evropskými	evropský	k2eAgFnPc7d1	Evropská
novinami	novina	k1gFnPc7	novina
vítězi	vítěz	k1gMnSc3	vítěz
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
působícího	působící	k2eAgNnSc2d1	působící
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ME	ME	kA	ME
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
doslova	doslova	k6eAd1	doslova
senzací	senzace	k1gFnSc7	senzace
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
mnoho	mnoho	k4c1	mnoho
klubů	klub	k1gInPc2	klub
snažilo	snažit	k5eAaImAgNnS	snažit
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
kádru	kádr	k1gInSc2	kádr
<g/>
.	.	kIx.	.
</s>
<s>
Everton	Everton	k1gInSc1	Everton
však	však	k9	však
prohlašoval	prohlašovat	k5eAaImAgInS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nepustí	pustit	k5eNaPmIp3nP	pustit
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
týdnech	týden	k1gInPc6	týden
spekulací	spekulace	k1gFnPc2	spekulace
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
Rooney	Roonea	k1gFnSc2	Roonea
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c4	v
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
přestupového	přestupový	k2eAgNnSc2d1	přestupové
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
do	do	k7c2	do
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc4	United
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
klub	klub	k1gInSc4	klub
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
zlomeného	zlomený	k2eAgInSc2d1	zlomený
malíčku	malíček	k1gInSc2	malíček
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2004	[number]	k4	2004
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
proti	proti	k7c3	proti
tureckému	turecký	k2eAgNnSc3d1	turecké
Fenerbahçe	Fenerbahçe	k1gNnSc3	Fenerbahçe
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
ohromující	ohromující	k2eAgInSc1d1	ohromující
-	-	kIx~	-
Manchester	Manchester	k1gInSc1	Manchester
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
Rooney	Roonea	k1gFnSc2	Roonea
dal	dát	k5eAaPmAgMnS	dát
3	[number]	k4	3
góly	gól	k1gInPc4	gól
a	a	k8xC	a
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
přihrál	přihrát	k5eAaPmAgInS	přihrát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Hráč	hráč	k1gMnSc1	hráč
roku	rok	k1gInSc2	rok
Premier	Premier	k1gMnSc1	Premier
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
PFA	PFA	kA	PFA
Player	Player	k1gMnSc1	Player
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
mu	on	k3xPp3gNnSc3	on
skončila	skončit	k5eAaPmAgFnS	skončit
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
ji	on	k3xPp3gFnSc4	on
prodloužit	prodloužit	k5eAaPmF	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
využít	využít	k5eAaPmF	využít
Manchester	Manchester	k1gInSc4	Manchester
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Rooney	Roonea	k1gFnSc2	Roonea
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
ve	v	k7c6	v
28	[number]	k4	28
<g/>
.	.	kIx.	.
ligovém	ligový	k2eAgNnSc6d1	ligové
kole	kolo	k1gNnSc6	kolo
jedním	jeden	k4xCgInSc7	jeden
gólem	gól	k1gInSc7	gól
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
výhře	výhra	k1gFnSc6	výhra
United	United	k1gInSc1	United
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
proti	proti	k7c3	proti
Norwichi	Norwich	k1gInSc3	Norwich
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
zařídil	zařídit	k5eAaPmAgMnS	zařídit
svým	svůj	k3xOyFgInSc7	svůj
gólem	gól	k1gInSc7	gól
vítězství	vítězství	k1gNnPc2	vítězství
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
nad	nad	k7c7	nad
hostujícím	hostující	k2eAgInSc7d1	hostující
Readingem	Reading	k1gInSc7	Reading
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
34	[number]	k4	34
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Premier	Premira	k1gFnPc2	Premira
League	League	k1gNnPc2	League
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
porazil	porazit	k5eAaPmAgInS	porazit
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
Aston	Astona	k1gFnPc2	Astona
Villu	Vill	k1gInSc2	Vill
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Rooney	Roonea	k1gFnPc1	Roonea
mohl	moct	k5eAaImAgInS	moct
v	v	k7c6	v
předstihu	předstih	k1gInSc6	předstih
slavit	slavit	k5eAaImF	slavit
se	s	k7c7	s
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
ligový	ligový	k2eAgInSc4d1	ligový
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
dvacátý	dvacátý	k4xOgMnSc1	dvacátý
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
klubu	klub	k1gInSc2	klub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc1	United
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
5	[number]	k4	5
<g/>
×	×	k?	×
vítězem	vítěz	k1gMnSc7	vítěz
anglické	anglický	k2eAgInPc1d1	anglický
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gInSc2	Leagu
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyhrát	vyhrát	k5eAaPmF	vyhrát
i	i	k9	i
prestižní	prestižní	k2eAgFnSc4d1	prestižní
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Vynikal	vynikat	k5eAaImAgMnS	vynikat
i	i	k9	i
v	v	k7c6	v
reprezentačním	reprezentační	k2eAgInSc6d1	reprezentační
dresu	dres	k1gInSc6	dres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
anglické	anglický	k2eAgFnSc2d1	anglická
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
střelcem	střelec	k1gMnSc7	střelec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
evropských	evropský	k2eAgInPc2d1	evropský
šampionátů	šampionát	k1gInPc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc1	rekord
(	(	kIx(	(
<g/>
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
střelec	střelec	k1gMnSc1	střelec
ME	ME	kA	ME
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
překonán	překonat	k5eAaPmNgInS	překonat
již	již	k9	již
o	o	k7c4	o
4	[number]	k4	4
dny	den	k1gInPc7	den
později	pozdě	k6eAd2	pozdě
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
záložníkem	záložník	k1gMnSc7	záložník
Johanem	Johan	k1gMnSc7	Johan
Vonlanthenem	Vonlanthen	k1gMnSc7	Vonlanthen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
EURA	euro	k1gNnSc2	euro
2012	[number]	k4	2012
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
zápase	zápas	k1gInSc6	zápas
na	na	k7c4	na
MS	MS	kA	MS
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
proti	proti	k7c3	proti
domácímu	domácí	k1gMnSc3	domácí
San	San	k1gFnSc2	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
drtivým	drtivý	k2eAgNnSc7d1	drtivé
vítězstvím	vítězství	k1gNnSc7	vítězství
Anglie	Anglie	k1gFnSc2	Anglie
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Rooney	Roone	k1gMnPc4	Roone
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
vsítil	vsítit	k5eAaPmAgMnS	vsítit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
proti	proti	k7c3	proti
domácí	domácí	k2eAgFnSc3d1	domácí
Černé	Černá	k1gFnSc3	Černá
Hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
pouze	pouze	k6eAd1	pouze
remizovala	remizovat	k5eAaPmAgFnS	remizovat
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Roy	Roy	k1gMnSc1	Roy
Hodgson	Hodgson	k1gMnSc1	Hodgson
jej	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Angličané	Angličan	k1gMnPc1	Angličan
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
již	již	k6eAd1	již
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
D	D	kA	D
<g/>
,	,	kIx,	,
obsadili	obsadit	k5eAaPmAgMnP	obsadit
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
bodem	bod	k1gInSc7	bod
poslední	poslední	k2eAgFnSc4d1	poslední
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Rooney	Roonea	k1gFnPc4	Roonea
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
EURO	euro	k1gNnSc4	euro
2016	[number]	k4	2016
proti	proti	k7c3	proti
týmu	tým	k1gInSc3	tým
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
národním	národní	k2eAgInSc6d1	národní
týmu	tým	k1gInSc6	tým
50	[number]	k4	50
<g/>
.	.	kIx.	.
gól	gól	k1gInSc4	gól
a	a	k8xC	a
překonal	překonat	k5eAaPmAgMnS	překonat
tak	tak	k6eAd1	tak
49	[number]	k4	49
gólů	gól	k1gInPc2	gól
předchozího	předchozí	k2eAgMnSc2d1	předchozí
rekordmana	rekordman	k1gMnSc2	rekordman
Bobbyho	Bobby	k1gMnSc2	Bobby
Charltona	Charlton	k1gMnSc2	Charlton
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
trenéra	trenér	k1gMnSc2	trenér
Roye	Roy	k1gMnSc2	Roy
Hodgsona	Hodgson	k1gMnSc2	Hodgson
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k8xC	i
EURA	euro	k1gNnSc2	euro
2016	[number]	k4	2016
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
Anglie	Anglie	k1gFnSc1	Anglie
suverénně	suverénně	k6eAd1	suverénně
probojovala	probojovat	k5eAaPmAgFnS	probojovat
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
bodu	bod	k1gInSc2	bod
z	z	k7c2	z
kvalifikační	kvalifikační	k2eAgFnSc2d1	kvalifikační
skupiny	skupina	k1gFnSc2	skupina
E.	E.	kA	E.
</s>
