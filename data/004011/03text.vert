<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
houbové	houbový	k2eAgInPc1d1	houbový
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kvasinek	kvasinka	k1gFnPc2	kvasinka
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
vřeckovýtrusných	vřeckovýtrusný	k2eAgFnPc2d1	vřeckovýtrusná
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
však	však	k9	však
i	i	k9	i
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
hub	houba	k1gFnPc2	houba
stopkovýtrusných	stopkovýtrusný	k2eAgFnPc2d1	stopkovýtrusná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
společně	společně	k6eAd1	společně
netvoří	tvořit	k5eNaImIp3nS	tvořit
taxonomickou	taxonomický	k2eAgFnSc4d1	Taxonomická
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Netvoří	tvořit	k5eNaImIp3nP	tvořit
plodnice	plodnice	k1gFnPc1	plodnice
<g/>
,	,	kIx,	,
množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
nepohlavně	pohlavně	k6eNd1	pohlavně
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
způsob	způsob	k1gInSc1	způsob
dělení	dělení	k1gNnSc2	dělení
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
pučení	pučení	k1gNnSc4	pučení
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
množit	množit	k5eAaImF	množit
i	i	k9	i
sexuálně	sexuálně	k6eAd1	sexuálně
tvorbou	tvorba	k1gFnSc7	tvorba
vřecek	vřecko	k1gNnPc2	vřecko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
uzavřená	uzavřený	k2eAgNnPc4d1	uzavřené
v	v	k7c6	v
žádných	žádný	k3yNgFnPc6	žádný
plodnicích	plodnice	k1gFnPc6	plodnice
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
askokarpech	askokarp	k1gInPc6	askokarp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Netvoří	tvořit	k5eNaImIp3nP	tvořit
žádné	žádný	k3yNgFnPc4	žádný
pravé	pravý	k2eAgFnPc4d1	pravá
myceliální	myceliální	k2eAgFnPc4d1	myceliální
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pseudomycelium	pseudomycelium	k1gNnSc1	pseudomycelium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
koloniím	kolonie	k1gFnPc3	kolonie
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
využívány	využíván	k2eAgInPc1d1	využíván
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
a	a	k8xC	a
biotechnologiích	biotechnologie	k1gFnPc6	biotechnologie
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
piva	pivo	k1gNnSc2	pivo
nebo	nebo	k8xC	nebo
chleba	chléb	k1gInSc2	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
pro	pro	k7c4	pro
kávu	káva	k1gFnSc4	káva
a	a	k8xC	a
kakao	kakao	k1gNnSc4	kakao
mají	mít	k5eAaImIp3nP	mít
ale	ale	k8xC	ale
větší	veliký	k2eAgFnSc4d2	veliký
biodiverzitu	biodiverzita	k1gFnSc4	biodiverzita
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc2	jejich
schopnosti	schopnost	k1gFnSc2	schopnost
kvašení	kvašení	k1gNnSc2	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
původci	původce	k1gMnPc7	původce
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Candida	Candida	k1gFnSc1	Candida
albicans	albicans	k1gInSc1	albicans
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
jsou	být	k5eAaImIp3nP	být
lidmi	člověk	k1gMnPc7	člověk
využívány	využívat	k5eAaImNgInP	využívat
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
chemické	chemický	k2eAgFnSc2d1	chemická
analýzy	analýza	k1gFnSc2	analýza
keramiky	keramika	k1gFnSc2	keramika
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
pravěké	pravěký	k2eAgFnSc2d1	pravěká
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Gruzie	Gruzie	k1gFnSc2	Gruzie
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
byly	být	k5eAaImAgInP	být
kvašené	kvašený	k2eAgInPc1d1	kvašený
nápoje	nápoj	k1gInPc1	nápoj
používány	používat	k5eAaImNgInP	používat
nejméně	málo	k6eAd3	málo
7000	[number]	k4	7000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Keramika	keramika	k1gFnSc1	keramika
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
kvašené	kvašený	k2eAgFnSc2d1	kvašená
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
medu	med	k1gInSc2	med
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kyseliny	kyselina	k1gFnSc2	kyselina
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
vinnou	vinný	k2eAgFnSc4d1	vinná
révu	réva	k1gFnSc4	réva
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
Crataegus	Crataegus	k1gInSc4	Crataegus
pinnatifida	pinnatifida	k1gFnSc1	pinnatifida
či	či	k8xC	či
jiný	jiný	k2eAgInSc1d1	jiný
druh	druh	k1gInSc1	druh
Čínského	čínský	k2eAgInSc2d1	čínský
hlohu	hloh	k1gInSc2	hloh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
kvasinky	kvasinka	k1gFnPc4	kvasinka
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Antoni	Antoň	k1gFnSc3	Antoň
van	van	k1gInSc4	van
Leeuwenhoek	Leeuwenhoek	k1gMnSc1	Leeuwenhoek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
výsledky	výsledek	k1gInPc1	výsledek
pozorování	pozorování	k1gNnPc2	pozorování
malých	malý	k2eAgFnPc2d1	malá
kuliček	kulička	k1gFnPc2	kulička
v	v	k7c6	v
pivě	pivo	k1gNnSc6	pivo
pomocí	pomocí	k7c2	pomocí
primitivního	primitivní	k2eAgInSc2d1	primitivní
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Schwann	Schwann	k1gMnSc1	Schwann
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
popřel	popřít	k5eAaPmAgMnS	popřít
účast	účast	k1gFnSc4	účast
kyslíku	kyslík	k1gInSc2	kyslík
při	při	k7c6	při
kvasném	kvasný	k2eAgInSc6d1	kvasný
procesu	proces	k1gInSc6	proces
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
kvasící	kvasící	k2eAgFnSc6d1	kvasící
tekutině	tekutina	k1gFnSc6	tekutina
se	se	k3xPyFc4	se
rozmnožující	rozmnožující	k2eAgFnSc2d1	rozmnožující
kvasinky	kvasinka	k1gFnSc2	kvasinka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
cukernou	cukerný	k2eAgFnSc4d1	cukerná
houbu	houba	k1gFnSc4	houba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
rodové	rodový	k2eAgNnSc1d1	rodové
označení	označení	k1gNnSc1	označení
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
moderních	moderní	k2eAgInPc2d1	moderní
sekvenovacích	sekvenovací	k2eAgInPc2d1	sekvenovací
principů	princip	k1gInPc2	princip
umožnil	umožnit	k5eAaPmAgInS	umožnit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
sekvenování	sekvenování	k1gNnPc2	sekvenování
genomu	genom	k1gInSc2	genom
kvasinky	kvasinka	k1gFnSc2	kvasinka
jako	jako	k8xC	jako
vůbec	vůbec	k9	vůbec
prvního	první	k4xOgInSc2	první
eukaryotního	eukaryotní	k2eAgInSc2d1	eukaryotní
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc4	kvasinka
jsou	být	k5eAaImIp3nP	být
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
v	v	k7c6	v
mnohém	mnohý	k2eAgInSc6d1	mnohý
směru	směr	k1gInSc6	směr
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
velmi	velmi	k6eAd1	velmi
užitečné	užitečný	k2eAgNnSc1d1	užitečné
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
kvasinky	kvasinka	k1gFnPc1	kvasinka
<g/>
"	"	kIx"	"
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
kvasným	kvasný	k2eAgInPc3d1	kvasný
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
nález	nález	k1gInSc1	nález
nádobek	nádobka	k1gFnPc2	nádobka
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
neolitické	neolitický	k2eAgFnSc2d1	neolitická
kuchyně	kuchyně	k1gFnSc2	kuchyně
v	v	k7c6	v
Hajji	Hajj	k1gInSc6	Hajj
Firuz	Firuz	k1gMnSc1	Firuz
Tepe	tepat	k5eAaImIp3nS	tepat
(	(	kIx(	(
<g/>
8500	[number]	k4	8500
<g/>
–	–	k?	–
<g/>
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Kvašení	kvašení	k1gNnSc1	kvašení
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Babylónu	Babylón	k1gInSc6	Babylón
v	v	k7c6	v
období	období	k1gNnSc6	období
6000	[number]	k4	6000
<g/>
–	–	k?	–
<g/>
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ze	z	k7c2	z
zkvašeného	zkvašený	k2eAgInSc2d1	zkvašený
odvaru	odvar	k1gInSc2	odvar
z	z	k7c2	z
naklíčeného	naklíčený	k2eAgNnSc2d1	naklíčené
obilí	obilí	k1gNnSc2	obilí
připravoval	připravovat	k5eAaImAgMnS	připravovat
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
neboli	neboli	k8xC	neboli
kvasnice	kvasnice	k1gFnPc1	kvasnice
nelze	lze	k6eNd1	lze
zaměnit	zaměnit	k5eAaPmF	zaměnit
za	za	k7c2	za
droždí	droždí	k1gNnSc2	droždí
neboť	neboť	k8xC	neboť
droždí	droždí	k1gNnSc2	droždí
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
druhotný	druhotný	k2eAgInSc1d1	druhotný
výrobek	výrobek	k1gInSc1	výrobek
z	z	k7c2	z
kvasnic	kvasnice	k1gFnPc2	kvasnice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
drožďárenský	drožďárenský	k2eAgInSc1d1	drožďárenský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
bylo	být	k5eAaImAgNnS	být
droždí	droždí	k1gNnSc1	droždí
prodáváno	prodávat	k5eAaImNgNnS	prodávat
v	v	k7c6	v
tekuté	tekutý	k2eAgFnSc6d1	tekutá
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
zdokonalení	zdokonalení	k1gNnSc6	zdokonalení
Tebbenhofem	Tebbenhof	k1gInSc7	Tebbenhof
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
k	k	k7c3	k
lisovanému	lisovaný	k2eAgNnSc3d1	lisované
droždí	droždí	k1gNnSc3	droždí
<g/>
.	.	kIx.	.
</s>
<s>
Prvořadý	prvořadý	k2eAgInSc4d1	prvořadý
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
kvasném	kvasný	k2eAgInSc6d1	kvasný
průmyslu	průmysl	k1gInSc6	průmysl
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lihu	líh	k1gInSc2	líh
<g/>
,	,	kIx,	,
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
pekařského	pekařský	k2eAgNnSc2d1	pekařské
droždí	droždí	k1gNnSc2	droždí
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
Saccharomyces	Saccharomyces	k1gMnSc1	Saccharomyces
cerevisiae	cerevisiaat	k5eAaPmIp3nS	cerevisiaat
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
některých	některý	k3yIgMnPc2	některý
mléčných	mléčný	k2eAgMnPc2d1	mléčný
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krmivářském	krmivářský	k2eAgInSc6d1	krmivářský
průmyslu	průmysl	k1gInSc6	průmysl
nabývají	nabývat	k5eAaImIp3nP	nabývat
význam	význam	k1gInSc4	význam
krmné	krmný	k2eAgFnSc2d1	krmná
směsi	směs	k1gFnSc2	směs
z	z	k7c2	z
kvasnic	kvasnice	k1gFnPc2	kvasnice
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
využívány	využívat	k5eAaPmNgInP	využívat
pro	pro	k7c4	pro
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
účely	účel	k1gInPc4	účel
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
biologicky	biologicky	k6eAd1	biologicky
cenných	cenný	k2eAgFnPc2d1	cenná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
bohatý	bohatý	k2eAgInSc4d1	bohatý
obsah	obsah	k1gInSc4	obsah
dobře	dobře	k6eAd1	dobře
stravitelných	stravitelný	k2eAgFnPc2d1	stravitelná
bílkovin	bílkovina	k1gFnPc2	bílkovina
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
40	[number]	k4	40
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
zejména	zejména	k9	zejména
komplexu	komplex	k1gInSc2	komplex
vitamínu	vitamín	k1gInSc2	vitamín
B	B	kA	B
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platí	platit	k5eAaImIp3nS	platit
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
pivovarské	pivovarský	k2eAgFnPc4d1	Pivovarská
kvasinky	kvasinka	k1gFnPc4	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
nervových	nervový	k2eAgNnPc2d1	nervové
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
při	při	k7c6	při
zánětlivých	zánětlivý	k2eAgFnPc6d1	zánětlivá
kožních	kožní	k2eAgFnPc6d1	kožní
chorobách	choroba	k1gFnPc6	choroba
<g/>
,	,	kIx,	,
při	při	k7c6	při
poruchách	porucha	k1gFnPc6	porucha
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
jaterních	jaterní	k2eAgFnPc6d1	jaterní
chorobách	choroba	k1gFnPc6	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
kvasinky	kvasinka	k1gFnSc2	kvasinka
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
použít	použít	k5eAaPmF	použít
i	i	k9	i
na	na	k7c4	na
odhalování	odhalování	k1gNnSc4	odhalování
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
molekul	molekula	k1gFnPc2	molekula
dinitrotoluenu	dinitrotoluen	k1gInSc2	dinitrotoluen
(	(	kIx(	(
<g/>
DNT	DNT	kA	DNT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
převažujícího	převažující	k2eAgInSc2d1	převažující
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
významu	význam	k1gInSc2	význam
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
kvasinek	kvasinka	k1gFnPc2	kvasinka
patogenních	patogenní	k2eAgMnPc2d1	patogenní
<g/>
,	,	kIx,	,
či	či	k8xC	či
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
výskyt	výskyt	k1gInSc1	výskyt
kvasinek	kvasinka	k1gFnPc2	kvasinka
coby	coby	k?	coby
škodlivého	škodlivý	k2eAgInSc2d1	škodlivý
kontaminantu	kontaminant	k1gInSc2	kontaminant
ve	v	k7c6	v
výrobním	výrobní	k2eAgInSc6d1	výrobní
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
eukaryotická	eukaryotický	k2eAgFnSc1d1	eukaryotická
buňka	buňka	k1gFnSc1	buňka
a	a	k8xC	a
organela	organela	k1gFnSc1	organela
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
velkou	velká	k1gFnSc4	velká
tvarovou	tvarový	k2eAgFnSc4d1	tvarová
<g/>
,	,	kIx,	,
velikostní	velikostní	k2eAgFnSc4d1	velikostní
či	či	k8xC	či
barevnou	barevný	k2eAgFnSc4d1	barevná
diverzitu	diverzita	k1gFnSc4	diverzita
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
kvasinky	kvasinka	k1gFnPc4	kvasinka
kulaté	kulatý	k2eAgFnPc4d1	kulatá
nebo	nebo	k8xC	nebo
oválné	oválný	k2eAgFnPc4d1	oválná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
buňky	buňka	k1gFnPc4	buňka
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
citronovitého	citronovitý	k2eAgInSc2d1	citronovitý
<g/>
,	,	kIx,	,
vajíčkovitého	vajíčkovitý	k2eAgInSc2d1	vajíčkovitý
<g/>
,	,	kIx,	,
lahvovitého	lahvovitý	k2eAgInSc2d1	lahvovitý
či	či	k8xC	či
vláknitého	vláknitý	k2eAgInSc2d1	vláknitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
mezi	mezi	k7c7	mezi
samotnými	samotný	k2eAgFnPc7d1	samotná
buňkami	buňka	k1gFnPc7	buňka
jednoho	jeden	k4xCgInSc2	jeden
kmene	kmen	k1gInSc2	kmen
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
morfologické	morfologický	k2eAgFnPc4d1	morfologická
a	a	k8xC	a
barevné	barevný	k2eAgFnPc4d1	barevná
heterogenity	heterogenita	k1gFnPc4	heterogenita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
změnami	změna	k1gFnPc7	změna
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
ukažme	ukázat	k5eAaPmRp1nP	ukázat
strukturu	struktura	k1gFnSc4	struktura
kvasinky	kvasinka	k1gFnSc2	kvasinka
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisiae	k1gFnSc4	cerevisiae
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
cerevisiae	cerevisiae	k6eAd1	cerevisiae
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
elipsoidní	elipsoidní	k2eAgFnPc1d1	elipsoidní
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
od	od	k7c2	od
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
μ	μ	k?	μ
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
ose	osa	k1gFnSc6	osa
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
μ	μ	k?	μ
na	na	k7c6	na
menší	malý	k2eAgFnSc6d2	menší
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
objem	objem	k1gInSc1	objem
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
29-55	[number]	k4	29-55
μ	μ	k?	μ
<g/>
3	[number]	k4	3
pro	pro	k7c4	pro
haploidni	haploidnout	k5eAaPmRp2nS	haploidnout
resp.	resp.	kA	resp.
diploidní	diploidní	k2eAgFnPc4d1	diploidní
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Shrnutí	shrnutí	k1gNnSc1	shrnutí
makromolekulárních	makromolekulární	k2eAgFnPc2d1	makromolekulární
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
kvasince	kvasinka	k1gFnSc6	kvasinka
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
Kidby	Kidba	k1gFnSc2	Kidba
a	a	k8xC	a
Davis	Davis	k1gFnSc2	Davis
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
model	model	k1gInSc1	model
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
S.	S.	kA	S.
cerevisae	cerevisa	k1gMnSc2	cerevisa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
rámcově	rámcově	k6eAd1	rámcově
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
má	mít	k5eAaImIp3nS	mít
buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
tři	tři	k4xCgFnPc4	tři
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
:	:	kIx,	:
vnější	vnější	k2eAgFnPc4d1	vnější
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc4d1	střední
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
polysacharid-proteinové	polysacharidroteinový	k2eAgInPc1d1	polysacharid-proteinový
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
spojeny	spojit	k5eAaPmNgInP	spojit
fosfodiesterovými	fosfodiesterův	k2eAgFnPc7d1	fosfodiesterův
vazbami	vazba	k1gFnPc7	vazba
<g/>
.	.	kIx.	.
vnější	vnější	k2eAgFnSc7d1	vnější
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mannanproteiny	mannanprotein	k1gInPc4	mannanprotein
spojené	spojený	k2eAgInPc4d1	spojený
disulfidovými	disulfidový	k2eAgInPc7d1	disulfidový
můstky	můstek	k1gInPc7	můstek
<g/>
.	.	kIx.	.
střední	střední	k1gMnSc1	střední
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
tvořena	tvořen	k2eAgFnSc1d1	tvořena
β	β	k?	β
<g/>
1,6	[number]	k4	1,6
glukanem	glukan	k1gInSc7	glukan
<g/>
,	,	kIx,	,
glukanproteiny	glukanproteina	k1gFnSc2	glukanproteina
a	a	k8xC	a
mannanproteiny	mannanproteina	k1gFnSc2	mannanproteina
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
<g/>
,	,	kIx,	,
přiléhající	přiléhající	k2eAgFnSc2d1	přiléhající
na	na	k7c4	na
cytoplazmatickou	cytoplazmatický	k2eAgFnSc4d1	cytoplazmatická
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
mikrokrystalického	mikrokrystalický	k2eAgInSc2d1	mikrokrystalický
β	β	k?	β
<g/>
1,3	[number]	k4	1,3
<g/>
-glukanu	lukanout	k5eAaPmIp1nS	-glukanout
V	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
stěně	stěna	k1gFnSc6	stěna
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
hydrolytické	hydrolytický	k2eAgInPc4d1	hydrolytický
enzymy	enzym	k1gInPc4	enzym
glykoproteinového	glykoproteinový	k2eAgInSc2d1	glykoproteinový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
β	β	k?	β
(	(	kIx(	(
<g/>
invertáza	invertáza	k1gFnSc1	invertáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polysacharidy	polysacharid	k1gInPc1	polysacharid
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jednosložkové	jednosložkový	k2eAgInPc4d1	jednosložkový
(	(	kIx(	(
<g/>
homopolysacharidy	homopolysacharid	k1gInPc4	homopolysacharid
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vícesložkové	vícesložkový	k2eAgInPc4d1	vícesložkový
(	(	kIx(	(
<g/>
heteropolysacharidy	heteropolysacharid	k1gInPc4	heteropolysacharid
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
galaktomannany	galaktomannana	k1gFnSc2	galaktomannana
<g/>
,	,	kIx,	,
xylomannany	xylomannana	k1gFnSc2	xylomannana
<g/>
,	,	kIx,	,
arabomannany	arabomannana	k1gFnSc2	arabomannana
aj.	aj.	kA	aj.
Polysacharidy	polysacharid	k1gInPc1	polysacharid
určují	určovat	k5eAaImIp3nP	určovat
především	především	k9	především
imunologické	imunologický	k2eAgFnPc1d1	imunologická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
antigeny	antigen	k1gInPc4	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
mannanproteiny	mannanprotein	k1gInPc4	mannanprotein
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
antigenní	antigenní	k2eAgInSc1d1	antigenní
charakter	charakter	k1gInSc1	charakter
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
délkou	délka	k1gFnSc7	délka
bočních	boční	k2eAgInPc2d1	boční
řetězců	řetězec	k1gInPc2	řetězec
mannosylových	mannosylový	k2eAgInPc2d1	mannosylový
zbytků	zbytek	k1gInPc2	zbytek
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgInPc2d1	spojený
α	α	k?	α
-1,2	-1,2	k4	-1,2
a	a	k8xC	a
α	α	k?	α
-1,3	-1,3	k4	-1,3
vazbami	vazba	k1gFnPc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Polysacharid	polysacharid	k1gInSc1	polysacharid
-	-	kIx~	-
proteinové	proteinový	k2eAgFnPc1d1	proteinová
komponenty	komponenta	k1gFnPc1	komponenta
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
flokulačních	flokulační	k2eAgFnPc6d1	flokulační
schopnostech	schopnost	k1gFnPc6	schopnost
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
stálé	stálý	k2eAgFnPc1d1	stálá
struktury	struktura	k1gFnPc1	struktura
na	na	k7c6	na
buněčné	buněčný	k2eAgFnSc6d1	buněčná
stěně	stěna	k1gFnSc6	stěna
vznikají	vznikat	k5eAaImIp3nP	vznikat
jizvy	jizva	k1gFnPc1	jizva
po	po	k7c6	po
pučení	pučení	k1gNnSc6	pučení
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
separaci	separace	k1gFnSc4	separace
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
po	po	k7c6	po
dělení	dělení	k1gNnSc6	dělení
kvasinkových	kvasinkový	k2eAgFnPc2d1	kvasinková
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
vývoj	vývoj	k1gInSc4	vývoj
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
tzv.	tzv.	kA	tzv.
architekturu	architektura	k1gFnSc4	architektura
její	její	k3xOp3gFnSc2	její
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
jizev	jizva	k1gFnPc2	jizva
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
stavbu	stavba	k1gFnSc4	stavba
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
např.	např.	kA	např.
v	v	k7c6	v
optickém	optický	k2eAgNnSc6d1	optické
mikroskopu	mikroskop	k1gInSc6	mikroskop
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
fluorescence	fluorescence	k1gFnSc2	fluorescence
primulinu	primulin	k2eAgFnSc4d1	primulin
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
specificky	specificky	k6eAd1	specificky
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
mikrofibrily	mikrofibril	k1gInPc4	mikrofibril
<g/>
.	.	kIx.	.
</s>
<s>
Jizva	jizva	k1gFnSc1	jizva
na	na	k7c6	na
dceřiné	dceřiný	k2eAgFnSc6d1	dceřiná
buňce	buňka	k1gFnSc6	buňka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
jizva	jizva	k1gFnSc1	jizva
zrodu	zrod	k1gInSc2	zrod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jiným	jiný	k2eAgInSc7d1	jiný
buněčným	buněčný	k2eAgInSc7d1	buněčný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
jizev	jizva	k1gFnPc2	jizva
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oddělily	oddělit	k5eAaPmAgInP	oddělit
nové	nový	k2eAgInPc1d1	nový
pupeny	pupen	k1gInPc1	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
buňky	buňka	k1gFnSc2	buňka
může	moct	k5eAaImIp3nS	moct
vypučet	vypučet	k5eAaPmF	vypučet
jen	jen	k9	jen
omezený	omezený	k2eAgInSc4d1	omezený
počet	počet	k1gInSc4	počet
nových	nový	k2eAgFnPc2d1	nová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
buňka	buňka	k1gFnSc1	buňka
stárne	stárnout	k5eAaImIp3nS	stárnout
<g/>
.	.	kIx.	.
</s>
<s>
Cytoplazmatická	Cytoplazmatický	k2eAgFnSc1d1	Cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
plazmalema	plazmalema	k1gFnSc1	plazmalema
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
elastický	elastický	k2eAgInSc4d1	elastický
obal	obal	k1gInSc4	obal
protoplastu	protoplast	k1gInSc2	protoplast
<g/>
,	,	kIx,	,
osmotickou	osmotický	k2eAgFnSc4d1	osmotická
bariéru	bariéra	k1gFnSc4	bariéra
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
transport	transport	k1gInSc4	transport
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
především	především	k6eAd1	především
z	z	k7c2	z
fosfolipidů	fosfolipid	k1gInPc2	fosfolipid
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
cytoplazmatické	cytoplazmatický	k2eAgFnSc2d1	cytoplazmatická
membrány	membrána	k1gFnSc2	membrána
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejlépe	dobře	k6eAd3	dobře
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mozaikového	mozaikový	k2eAgInSc2d1	mozaikový
modelu	model	k1gInSc2	model
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
molekuly	molekula	k1gFnPc1	molekula
fosfolipidů	fosfolipid	k1gInPc2	fosfolipid
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vrstvách	vrstva	k1gFnPc6	vrstva
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
polární	polární	k2eAgFnPc1d1	polární
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
vnější	vnější	k2eAgInSc4d1	vnější
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
nepolární	polární	k2eNgNnSc1d1	nepolární
uvnitř	uvnitř	k7c2	uvnitř
membrány	membrána	k1gFnSc2	membrána
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Lipidová	Lipidový	k2eAgNnPc4d1	Lipidový
dvouvrstva	dvouvrstvo	k1gNnPc4	dvouvrstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plazmalemu	plazmalema	k1gFnSc4	plazmalema
kvasinek	kvasinka	k1gFnPc2	kvasinka
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
invaginace	invaginace	k1gFnPc4	invaginace
do	do	k7c2	do
cytoplasmy	cytoplasma	k1gFnSc2	cytoplasma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
četné	četný	k2eAgFnPc1d1	četná
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
exponenciální	exponenciální	k2eAgFnSc6d1	exponenciální
fázi	fáze	k1gFnSc6	fáze
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
zřetelně	zřetelně	k6eAd1	zřetelně
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
jadernou	jaderný	k2eAgFnSc7d1	jaderná
membránou	membrána	k1gFnSc7	membrána
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
chromatin	chromatin	k1gInSc4	chromatin
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
komplex	komplex	k1gInSc1	komplex
dsDNA	dsDNA	k?	dsDNA
<g/>
,	,	kIx,	,
histonů	histon	k1gInPc2	histon
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
nehistonové	histonový	k2eNgFnSc2d1	histonový
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
eukaryoriotických	eukaryoriotický	k2eAgFnPc2d1	eukaryoriotický
buněk	buňka	k1gFnPc2	buňka
mají	mít	k5eAaImIp3nP	mít
kvasinky	kvasinka	k1gFnPc1	kvasinka
tzv.	tzv.	kA	tzv.
endomitózu	endomitóza	k1gFnSc4	endomitóza
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dělení	dělení	k1gNnSc1	dělení
jádra	jádro	k1gNnSc2	jádro
probíhá	probíhat	k5eAaImIp3nS	probíhat
bez	bez	k7c2	bez
rozrušení	rozrušení	k1gNnSc2	rozrušení
jaderné	jaderný	k2eAgFnSc2d1	jaderná
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kvasinek	kvasinka	k1gFnPc2	kvasinka
mimořádně	mimořádně	k6eAd1	mimořádně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
udělat	udělat	k5eAaPmF	udělat
dobrý	dobrý	k2eAgInSc4d1	dobrý
karyotyp	karyotyp	k1gInSc4	karyotyp
a	a	k8xC	a
zjistit	zjistit	k5eAaPmF	zjistit
tak	tak	k6eAd1	tak
přesný	přesný	k2eAgInSc4d1	přesný
počet	počet	k1gInSc4	počet
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaderné	jaderný	k2eAgFnSc6d1	jaderná
membráně	membrána	k1gFnSc6	membrána
se	se	k3xPyFc4	se
diferencuje	diferencovat	k5eAaImIp3nS	diferencovat
tzv.	tzv.	kA	tzv.
polární	polární	k2eAgInSc1d1	polární
tělísko	tělísko	k1gNnSc4	tělísko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
iniciaci	iniciace	k1gFnSc6	iniciace
tvorby	tvorba	k1gFnSc2	tvorba
pupene	pupen	k1gInSc5	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Biomembrány	Biomembrat	k5eAaImNgFnP	Biomembrat
tvoří	tvořit	k5eAaImIp3nP	tvořit
povrchové	povrchový	k2eAgFnPc1d1	povrchová
struktury	struktura	k1gFnPc1	struktura
organel	organela	k1gFnPc2	organela
i	i	k8xC	i
systém	systém	k1gInSc1	systém
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
membrán	membrána	k1gFnPc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
buňku	buňka	k1gFnSc4	buňka
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
reakční	reakční	k2eAgInPc4d1	reakční
prostory	prostor	k1gInPc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
sekreci	sekrece	k1gFnSc3	sekrece
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
jsou	být	k5eAaImIp3nP	být
syntetizovány	syntetizován	k2eAgInPc1d1	syntetizován
peptidy	peptid	k1gInPc1	peptid
a	a	k8xC	a
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
povrchu	povrch	k1gInSc6	povrch
ER	ER	kA	ER
(	(	kIx(	(
<g/>
drsné	drsný	k2eAgNnSc1d1	drsné
ER	ER	kA	ER
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zakotveny	zakotven	k2eAgInPc4d1	zakotven
ribozomy	ribozom	k1gInPc4	ribozom
<g/>
,	,	kIx,	,
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
volně	volně	k6eAd1	volně
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
eukaryot	eukaryota	k1gFnPc2	eukaryota
se	se	k3xPyFc4	se
ribozom	ribozom	k1gInSc1	ribozom
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
podjednotky	podjednotka	k1gFnPc4	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
podjednotka	podjednotka	k1gFnSc1	podjednotka
katalyzuje	katalyzovat	k5eAaBmIp3nS	katalyzovat
syntézu	syntéza	k1gFnSc4	syntéza
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
ribozomů	ribozom	k1gInPc2	ribozom
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
jadérko	jadérko	k1gNnSc1	jadérko
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
povrch	povrch	k1gInSc1	povrch
ER	ER	kA	ER
tvoří	tvořit	k5eAaImIp3nS	tvořit
hladké	hladký	k2eAgNnSc4d1	hladké
ER	ER	kA	ER
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ER	ER	kA	ER
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
posttranslačním	posttranslační	k2eAgFnPc3d1	posttranslační
úpravám	úprava	k1gFnPc3	úprava
<g/>
,	,	kIx,	,
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
určení	určení	k1gNnSc1	určení
se	s	k7c7	s
proteiny	protein	k1gInPc7	protein
dostávají	dostávat	k5eAaImIp3nP	dostávat
pomocí	pomocí	k7c2	pomocí
sekrečních	sekreční	k2eAgInPc2d1	sekreční
měchýřků	měchýřek	k1gInPc2	měchýřek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
tvoří	tvořit	k5eAaImIp3nS	tvořit
Golgiho	Golgi	k1gMnSc4	Golgi
aparát	aparát	k1gInSc1	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
u	u	k7c2	u
kvasinek	kvasinka	k1gFnPc2	kvasinka
lamely	lamela	k1gFnSc2	lamela
<g/>
,	,	kIx,	,
cisterny	cisterna	k1gFnSc2	cisterna
a	a	k8xC	a
tubuly	tubulus	k1gInPc4	tubulus
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
a	a	k8xC	a
živočišných	živočišný	k2eAgFnPc2d1	živočišná
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
(	(	kIx(	(
<g/>
enchylema	enchylema	k1gFnSc1	enchylema
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
dvěma	dva	k4xCgFnPc7	dva
membránami	membrána	k1gFnPc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
ER	ER	kA	ER
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
povrch	povrch	k1gInSc4	povrch
cytoplazmatické	cytoplazmatický	k2eAgFnSc2d1	cytoplazmatická
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
jadernou	jaderný	k2eAgFnSc7d1	jaderná
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomy	Ribozom	k1gInPc1	Ribozom
kvasinek	kvasinka	k1gFnPc2	kvasinka
mají	mít	k5eAaImIp3nP	mít
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
eukaryot	eukaryota	k1gFnPc2	eukaryota
hodnotu	hodnota	k1gFnSc4	hodnota
sedimentačního	sedimentační	k2eAgInSc2d1	sedimentační
koeficientu	koeficient	k1gInSc2	koeficient
kolem	kolem	k7c2	kolem
80	[number]	k4	80
<g/>
S.	S.	kA	S.
Menší	malý	k2eAgFnSc1d2	menší
podjednotka	podjednotka	k1gFnSc1	podjednotka
o	o	k7c6	o
koeficientu	koeficient	k1gInSc6	koeficient
40S	[number]	k4	40S
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
molekuly	molekula	k1gFnSc2	molekula
18S	[number]	k4	18S
rRNA	rRNA	k?	rRNA
a	a	k8xC	a
z	z	k7c2	z
30	[number]	k4	30
±	±	k?	±
5	[number]	k4	5
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
;	;	kIx,	;
větší	veliký	k2eAgFnSc1d2	veliký
podjednotka	podjednotka	k1gFnSc1	podjednotka
o	o	k7c6	o
60S	[number]	k4	60S
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
28S	[number]	k4	28S
rRNA	rRNA	k?	rRNA
<g/>
,	,	kIx,	,
5S	[number]	k4	5S
rRNA	rRNA	k?	rRNA
<g/>
,	,	kIx,	,
5,8	[number]	k4	5,8
<g/>
S	s	k7c7	s
rRNA	rRNA	k?	rRNA
a	a	k8xC	a
40	[number]	k4	40
±	±	k?	±
5	[number]	k4	5
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
jsou	být	k5eAaImIp3nP	být
organely	organela	k1gFnPc4	organela
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
na	na	k7c4	na
respiraci	respirace	k1gFnSc4	respirace
a	a	k8xC	a
oxidativní	oxidativní	k2eAgFnSc4d1	oxidativní
fosforylaci	fosforylace	k1gFnSc4	fosforylace
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
genetickým	genetický	k2eAgInSc7d1	genetický
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
proteosyntézou	proteosyntéza	k1gFnSc7	proteosyntéza
<g/>
.	.	kIx.	.
<g/>
Tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
počet	počet	k1gInSc4	počet
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ovlivňovány	ovlivňován	k2eAgInPc4d1	ovlivňován
různými	různý	k2eAgInPc7d1	různý
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
reprimujícího	reprimující	k2eAgNnSc2d1	reprimující
dýchání	dýchání	k1gNnSc2	dýchání
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
podíl	podíl	k1gInSc1	podíl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
objemu	objem	k1gInSc6	objem
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinková	kvasinkový	k2eAgFnSc1d1	kvasinková
buňka	buňka	k1gFnSc1	buňka
zpravidla	zpravidla	k6eAd1	zpravidla
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
kulatou	kulatý	k2eAgFnSc4d1	kulatá
vakuolu	vakuola	k1gFnSc4	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
pučení	pučený	k2eAgMnPc1d1	pučený
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
malých	malý	k2eAgFnPc2d1	malá
vakuol	vakuola	k1gFnPc2	vakuola
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
splývají	splývat	k5eAaImIp3nP	splývat
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
vakuolu	vakuola	k1gFnSc4	vakuola
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dvě	dva	k4xCgFnPc4	dva
vakuoly	vakuola	k1gFnPc4	vakuola
(	(	kIx(	(
<g/>
ve	v	k7c6	v
stacionární	stacionární	k2eAgFnSc6d1	stacionární
fázi	fáze	k1gFnSc6	fáze
růstu	růst	k1gInSc2	růst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
vakuol	vakuola	k1gFnPc2	vakuola
je	být	k5eAaImIp3nS	být
proměnlivý	proměnlivý	k2eAgMnSc1d1	proměnlivý
od	od	k7c2	od
0,3	[number]	k4	0,3
do	do	k7c2	do
3	[number]	k4	3
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
jsou	být	k5eAaImIp3nP	být
membránou	membrána	k1gFnSc7	membrána
zvanou	zvaný	k2eAgFnSc7d1	zvaná
tonoplast	tonoplast	k1gInSc4	tonoplast
<g/>
.	.	kIx.	.
80	[number]	k4	80
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
volných	volný	k2eAgFnPc2d1	volná
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
v	v	k7c6	v
kvasinkách	kvasinka	k1gFnPc6	kvasinka
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
ve	v	k7c6	v
vakuolách	vakuola	k1gFnPc6	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
enzymy	enzym	k1gInPc1	enzym
hydrolázy	hydroláza	k1gFnSc2	hydroláza
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
proteáz	proteáza	k1gFnPc2	proteáza
<g/>
,	,	kIx,	,
ribonukleázy	ribonukleáza	k1gFnSc2	ribonukleáza
či	či	k8xC	či
esterázy	esteráza	k1gFnSc2	esteráza
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
enzymy	enzym	k1gInPc7	enzym
se	se	k3xPyFc4	se
ve	v	k7c6	v
vakuolách	vakuola	k1gFnPc6	vakuola
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
funkčně	funkčně	k6eAd1	funkčně
narušené	narušený	k2eAgFnPc4d1	narušená
organely	organela	k1gFnPc4	organela
<g/>
.	.	kIx.	.
</s>
<s>
Cytosol	Cytosol	k1gInSc1	Cytosol
je	být	k5eAaImIp3nS	být
tekutá	tekutý	k2eAgFnSc1d1	tekutá
frakce	frakce	k1gFnSc1	frakce
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
po	po	k7c6	po
separaci	separace	k1gFnSc6	separace
buněčných	buněčný	k2eAgFnPc2d1	buněčná
organel	organela	k1gFnPc2	organela
o	o	k7c4	o
pH	ph	kA	ph
6,2	[number]	k4	6,2
až	až	k9	až
6,4	[number]	k4	6,4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
koloidním	koloidní	k2eAgInSc6d1	koloidní
charakteru	charakter	k1gInSc6	charakter
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
rozpuštěné	rozpuštěný	k2eAgFnPc1d1	rozpuštěná
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
glykogen	glykogen	k1gInSc1	glykogen
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
makromolekulární	makromolekulární	k2eAgFnPc1d1	makromolekulární
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
buněčný	buněčný	k2eAgInSc4d1	buněčný
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
kvasinky	kvasinka	k1gFnPc1	kvasinka
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
omezit	omezit	k5eAaPmF	omezit
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
buňky	buňka	k1gFnSc2	buňka
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vznikají	vznikat	k5eAaImIp3nP	vznikat
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
pseudomycelia	pseudomycelia	k1gFnSc1	pseudomycelia
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
tvar	tvar	k1gInSc4	tvar
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
<g/>
.	.	kIx.	.
</s>
<s>
Buněčným	buněčný	k2eAgInSc7d1	buněčný
cyklem	cyklus	k1gInSc7	cyklus
rozumíme	rozumět	k5eAaImIp1nP	rozumět
obecně	obecně	k6eAd1	obecně
buněčné	buněčný	k2eAgInPc4d1	buněčný
procesy	proces	k1gInPc4	proces
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
následujícími	následující	k2eAgFnPc7d1	následující
děleními	dělení	k1gNnPc7	dělení
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobně	podrobně	k6eAd1	podrobně
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
znám	znát	k5eAaImIp1nS	znát
především	především	k9	především
u	u	k7c2	u
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisae	cerevisa	k1gFnSc2	cerevisa
a	a	k8xC	a
u	u	k7c2	u
Schizosaccharomyces	Schizosaccharomycesa	k1gFnPc2	Schizosaccharomycesa
pombe	pombat	k5eAaPmIp3nS	pombat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
Lindner	Lindner	k1gInSc1	Lindner
objevil	objevit	k5eAaPmAgInS	objevit
Schizosaccharomyces	Schizosaccharomyces	k1gInSc4	Schizosaccharomyces
pombe	pombat	k5eAaPmIp3nS	pombat
v	v	k7c6	v
prosném	prosný	k2eAgNnSc6d1	prosný
pivu	pivo	k1gNnSc6	pivo
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
místní	místní	k2eAgNnSc1d1	místní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
pombe	pomb	k1gMnSc5	pomb
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pro	pro	k7c4	pro
genetické	genetický	k2eAgInPc4d1	genetický
výzkumy	výzkum	k1gInPc4	výzkum
byl	být	k5eAaImAgInS	být
izolován	izolován	k2eAgInSc1d1	izolován
Ursem	Urs	k1gMnSc7	Urs
Leupoldem	Leupold	k1gMnSc7	Leupold
(	(	kIx(	(
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
Berne	Bern	k1gInSc5	Bern
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
z	z	k7c2	z
kvasničné	kvasničný	k2eAgFnSc2d1	kvasničná
kultury	kultura	k1gFnSc2	kultura
S.	S.	kA	S.
liquefacis	liquefacis	k1gInSc1	liquefacis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
normálního	normální	k2eAgInSc2d1	normální
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
jsou	být	k5eAaImIp3nP	být
kvasinky	kvasinka	k1gFnPc1	kvasinka
haploidní	haploidní	k2eAgFnSc1d1	haploidní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
kopii	kopie	k1gFnSc6	kopie
od	od	k7c2	od
každého	každý	k3xTgInSc2	každý
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
jedinou	jediný	k2eAgFnSc4d1	jediná
kopii	kopie	k1gFnSc4	kopie
od	od	k7c2	od
každého	každý	k3xTgInSc2	každý
genu	gen	k1gInSc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Haploidní	haploidní	k2eAgFnPc1d1	haploidní
kvasinky	kvasinka	k1gFnPc1	kvasinka
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jak	jak	k6eAd1	jak
recesivní	recesivní	k2eAgFnSc1d1	recesivní
tak	tak	k8xS	tak
dominantní	dominantní	k2eAgFnSc1d1	dominantní
mutace	mutace	k1gFnSc1	mutace
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
ve	v	k7c6	v
výsledném	výsledný	k2eAgInSc6d1	výsledný
mutantním	mutantní	k2eAgInSc6d1	mutantní
fenotypu	fenotyp	k1gInSc6	fenotyp
<g/>
.	.	kIx.	.
</s>
<s>
Haploidní	haploidní	k2eAgFnPc1d1	haploidní
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
asexuálně	asexuálně	k6eAd1	asexuálně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mitózy	mitóza	k1gFnSc2	mitóza
<g/>
.	.	kIx.	.
</s>
<s>
Dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
buňka	buňka	k1gFnSc1	buňka
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c6	na
pólu	pólo	k1gNnSc6	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
doroste	dorůst	k5eAaPmIp3nS	dorůst
do	do	k7c2	do
dospělce	dospělec	k1gMnSc2	dospělec
<g/>
,	,	kIx,	,
přestane	přestat	k5eAaPmIp3nS	přestat
růst	růst	k1gInSc4	růst
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přehrádku	přehrádka	k1gFnSc4	přehrádka
v	v	k7c6	v
prostředku	prostředek	k1gInSc6	prostředek
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Přehrádka	přehrádka	k1gFnSc1	přehrádka
dělí	dělit	k5eAaImIp3nS	dělit
matečnou	matečný	k2eAgFnSc4d1	matečná
buňku	buňka	k1gFnSc4	buňka
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
ekvivalentních	ekvivalentní	k2eAgFnPc2d1	ekvivalentní
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
příhodných	příhodný	k2eAgFnPc2d1	příhodná
podmínek	podmínka	k1gFnPc2	podmínka
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
celý	celý	k2eAgInSc1d1	celý
dělící	dělící	k2eAgInSc1d1	dělící
cyklus	cyklus	k1gInSc1	cyklus
za	za	k7c4	za
3	[number]	k4	3
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
kvasinky	kvasinka	k1gFnPc1	kvasinka
často	často	k6eAd1	často
nutričně	nutričně	k6eAd1	nutričně
deprivovány	deprivován	k2eAgFnPc1d1	deprivován
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
S.	S.	kA	S.
pombe	pombat	k5eAaPmIp3nS	pombat
dimorfní	dimorfní	k2eAgFnSc7d1	dimorfní
kvasinkou	kvasinka	k1gFnSc7	kvasinka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
přepnout	přepnout	k5eAaPmF	přepnout
<g/>
"	"	kIx"	"
z	z	k7c2	z
kvasinkové	kvasinkový	k2eAgFnSc2d1	kvasinková
morfologie	morfologie	k1gFnSc2	morfologie
do	do	k7c2	do
pseudohyfálního	pseudohyfální	k2eAgInSc2d1	pseudohyfální
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
dceřiné	dceřiný	k2eAgFnPc1d1	dceřiná
buňky	buňka	k1gFnPc1	buňka
spojeny	spojit	k5eAaPmNgFnP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Pseudohyfální	Pseudohyfální	k2eAgInSc1d1	Pseudohyfální
růst	růst	k1gInSc1	růst
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
buňkám	buňka	k1gFnPc3	buňka
se	se	k3xPyFc4	se
efektivněji	efektivně	k6eAd2	efektivně
šířit	šířit	k5eAaImF	šířit
a	a	k8xC	a
shánět	shánět	k5eAaImF	shánět
se	se	k3xPyFc4	se
po	po	k7c6	po
nových	nový	k2eAgInPc6d1	nový
nutričních	nutriční	k2eAgInPc6d1	nutriční
zdrojích	zdroj	k1gInPc6	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
pombe	pombat	k5eAaPmIp3nS	pombat
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
partnerské	partnerský	k2eAgInPc4d1	partnerský
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
"	"	kIx"	"
<g/>
+	+	kIx~	+
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
etapa	etapa	k1gFnSc1	etapa
následována	následovat	k5eAaImNgFnS	následovat
nutriční	nutriční	k2eAgFnSc7d1	nutriční
deficiencí	deficience	k1gFnSc7	deficience
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
partnerské	partnerský	k2eAgInPc1d1	partnerský
typy	typ	k1gInPc1	typ
konjugují	konjugovat	k5eAaImIp3nP	konjugovat
a	a	k8xC	a
fúzují	fúzovat	k5eAaBmIp3nP	fúzovat
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
Následně	následně	k6eAd1	následně
fúzují	fúzovat	k5eAaBmIp3nP	fúzovat
jádra	jádro	k1gNnPc4	jádro
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
diploidní	diploidní	k2eAgFnSc4d1	diploidní
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
zygota	zygota	k1gFnSc1	zygota
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
podstupují	podstupovat	k5eAaImIp3nP	podstupovat
zygoty	zygota	k1gFnPc1	zygota
meiózu	meióza	k1gFnSc4	meióza
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
sporulace	sporulace	k1gFnSc2	sporulace
a	a	k8xC	a
formování	formování	k1gNnSc2	formování
4	[number]	k4	4
<g/>
-sporového	porový	k2eAgInSc2d1	-sporový
zygotického	zygotický	k2eAgInSc2d1	zygotický
konglomerátu	konglomerát	k1gInSc2	konglomerát
<g/>
.	.	kIx.	.
</s>
<s>
Stěna	stěna	k1gFnSc1	stěna
toho	ten	k3xDgInSc2	ten
útvaru	útvar	k1gInSc2	útvar
autolyzuje	autolyzovat	k5eAaBmIp3nS	autolyzovat
<g/>
,	,	kIx,	,
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
haploidní	haploidní	k2eAgInPc4d1	haploidní
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
přežít	přežít	k5eAaPmF	přežít
delší	dlouhý	k2eAgNnSc4d2	delší
období	období	k1gNnSc4	období
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
životní	životní	k2eAgFnPc1d1	životní
podmínky	podmínka	k1gFnPc1	podmínka
zlepší	zlepšit	k5eAaPmIp3nP	zlepšit
<g/>
,	,	kIx,	,
spory	spor	k1gInPc1	spor
začnou	začít	k5eAaPmIp3nP	začít
růst	růst	k1gInSc1	růst
a	a	k8xC	a
životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
haploidních	haploidní	k2eAgFnPc2d1	haploidní
buněk	buňka	k1gFnPc2	buňka
znovu	znovu	k6eAd1	znovu
začne	začít	k5eAaPmIp3nS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Diploidní	diploidní	k2eAgFnPc1d1	diploidní
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
středově	středově	k6eAd1	středově
štěpí	štěpit	k5eAaImIp3nS	štěpit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
haploidní	haploidní	k2eAgFnPc1d1	haploidní
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
širší	široký	k2eAgInSc4d2	širší
než	než	k8xS	než
haploidní	haploidní	k2eAgInSc4d1	haploidní
<g/>
.	.	kIx.	.
</s>
<s>
Haploidní	haploidní	k2eAgFnPc1d1	haploidní
buňky	buňka	k1gFnPc1	buňka
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
7-8	[number]	k4	7-8
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
narozené	narozený	k2eAgNnSc1d1	narozené
<g/>
)	)	kIx)	)
do	do	k7c2	do
12-15	[number]	k4	12-15
(	(	kIx(	(
<g/>
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
<g/>
)	)	kIx)	)
mikrometrů	mikrometr	k1gInPc2	mikrometr
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
3-4	[number]	k4	3-4
mikrometry	mikrometr	k1gInPc7	mikrometr
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
diploidní	diploidní	k2eAgFnPc1d1	diploidní
buňky	buňka	k1gFnPc1	buňka
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
11-14	[number]	k4	11-14
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
narozené	narozený	k2eAgNnSc1d1	narozené
<g/>
)	)	kIx)	)
do	do	k7c2	do
20	[number]	k4	20
-25	-25	k4	-25
(	(	kIx(	(
<g/>
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
<g/>
)	)	kIx)	)
mikrometrů	mikrometr	k1gInPc2	mikrometr
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
4-5	[number]	k4	4-5
mikrometrů	mikrometr	k1gInPc2	mikrometr
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Diploidní	diploidní	k2eAgFnPc1d1	diploidní
buňky	buňka	k1gFnPc1	buňka
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
mitóze	mitóza	k1gFnSc6	mitóza
do	do	k7c2	do
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
živin	živina	k1gFnPc2	živina
Kvasinky	kvasinka	k1gFnSc2	kvasinka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
nepohlavně	pohlavně	k6eNd1	pohlavně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
pučením	pučení	k1gNnSc7	pučení
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
druhů	druh	k1gInPc2	druh
tzv.	tzv.	kA	tzv.
poltivých	poltivý	k2eAgFnPc2d1	poltivý
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pohlavně	pohlavně	k6eAd1	pohlavně
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
spor	spora	k1gFnPc2	spora
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pučení	pučení	k1gNnSc2	pučení
(	(	kIx(	(
<g/>
nepohlavní	pohlavní	k2eNgFnSc1d1	nepohlavní
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
mateřské	mateřský	k2eAgFnSc6d1	mateřská
buňce	buňka	k1gFnSc6	buňka
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
pupen	pupen	k1gInSc1	pupen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
velikosti	velikost	k1gFnSc2	velikost
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pupen	pupen	k1gInSc1	pupen
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kvasinky	kvasinka	k1gFnSc2	kvasinka
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pučení	pučení	k1gNnSc2	pučení
monopolární	monopolární	k2eAgFnSc2d1	monopolární
<g/>
,	,	kIx,	,
bipolární	bipolární	k2eAgFnSc2d1	bipolární
a	a	k8xC	a
multipolární	multipolární	k2eAgFnSc2d1	multipolární
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mohou	moct	k5eAaImIp3nP	moct
pupeny	pupen	k1gInPc4	pupen
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
zcela	zcela	k6eAd1	zcela
náhodně	náhodně	k6eAd1	náhodně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
kvasinek	kvasinka	k1gFnPc2	kvasinka
lze	lze	k6eAd1	lze
kromě	kromě	k7c2	kromě
nepohlavního	pohlavní	k2eNgNnSc2d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
pohlavní	pohlavní	k2eAgNnSc4d1	pohlavní
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
kvasinky	kvasinka	k1gFnPc1	kvasinka
(	(	kIx(	(
<g/>
schopné	schopný	k2eAgFnPc1d1	schopná
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
)	)	kIx)	)
sporulují	sporulovat	k5eAaPmIp3nP	sporulovat
při	při	k7c6	při
nedostatečném	dostatečný	k2eNgInSc6d1	nedostatečný
přísunu	přísun	k1gInSc6	přísun
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohlavním	pohlavní	k2eAgNnSc6d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
spolu	spolu	k6eAd1	spolu
splynou	splynout	k5eAaPmIp3nP	splynout
(	(	kIx(	(
<g/>
konjugují	konjugovat	k5eAaImIp3nP	konjugovat
<g/>
)	)	kIx)	)
dvě	dva	k4xCgFnPc1	dva
buňky	buňka	k1gFnPc1	buňka
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
spojí	spojit	k5eAaPmIp3nP	spojit
i	i	k9	i
jejich	jejich	k3xOp3gNnSc2	jejich
jádra	jádro	k1gNnSc2	jádro
–	–	k?	–
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
karyogamii	karyogamie	k1gFnSc3	karyogamie
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
dojde	dojít	k5eAaPmIp3nS	dojít
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mezitím	mezitím	k6eAd1	mezitím
existuje	existovat	k5eAaImIp3nS	existovat
dikaryontní	dikaryontní	k2eAgFnSc1d1	dikaryontní
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
buňky	buňka	k1gFnSc2	buňka
se	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
jádry	jádro	k1gNnPc7	jádro
dělí	dělit	k5eAaImIp3nP	dělit
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
vznik	vznik	k1gInSc4	vznik
dikaryotickému	dikaryotický	k2eAgNnSc3d1	dikaryotické
myceliu	mycelium	k1gNnSc3	mycelium
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
především	především	k9	především
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
bazidiomycet	bazidiomycet	k5eAaImF	bazidiomycet
<g/>
.	.	kIx.	.
</s>
<s>
Konjugací	konjugace	k1gFnSc7	konjugace
dvou	dva	k4xCgFnPc6	dva
kvasinkových	kvasinkový	k2eAgFnPc2d1	kvasinková
buněk	buňka	k1gFnPc2	buňka
vzniká	vznikat	k5eAaImIp3nS	vznikat
zygota	zygota	k1gFnSc1	zygota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
později	pozdě	k6eAd2	pozdě
sporuluje	sporulovat	k5eAaBmIp3nS	sporulovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
asku	askus	k1gInSc2	askus
nebo	nebo	k8xC	nebo
bazidia	bazidium	k1gNnSc2	bazidium
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sporulaci	sporulace	k1gFnSc3	sporulace
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
meiotické	meiotický	k2eAgNnSc1d1	meiotické
dělení	dělení	k1gNnSc1	dělení
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
4	[number]	k4	4
spory	spor	k1gInPc7	spor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
asku	askus	k1gInSc6	askus
8	[number]	k4	8
<g/>
,	,	kIx,	,
16	[number]	k4	16
i	i	k8xC	i
více	hodně	k6eAd2	hodně
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k9	ale
u	u	k7c2	u
kvasinek	kvasinka	k1gFnPc2	kvasinka
se	se	k3xPyFc4	se
sleduje	sledovat	k5eAaImIp3nS	sledovat
silný	silný	k2eAgInSc1d1	silný
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
u	u	k7c2	u
mnohých	mnohý	k2eAgInPc2d1	mnohý
druhů	druh	k1gInPc2	druh
spolu	spolu	k6eAd1	spolu
splývají	splývat	k5eAaImIp3nP	splývat
dvě	dva	k4xCgFnPc4	dva
somatické	somatický	k2eAgFnPc4d1	somatická
(	(	kIx(	(
<g/>
diploidní	diploidní	k2eAgFnPc4d1	diploidní
<g/>
)	)	kIx)	)
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
konjugaci	konjugace	k1gFnSc3	konjugace
vůbec	vůbec	k9	vůbec
nedochází	docházet	k5eNaImIp3nS	docházet
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
imperfektní	imperfektní	k2eAgFnPc4d1	imperfektní
kvasinky	kvasinka	k1gFnPc4	kvasinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
genetické	genetický	k2eAgFnSc2d1	genetická
mapy	mapa	k1gFnSc2	mapa
kvasinek	kvasinka	k1gFnPc2	kvasinka
byly	být	k5eAaImAgFnP	být
vypracovány	vypracován	k2eAgFnPc1d1	vypracována
různé	různý	k2eAgFnPc1d1	různá
metody	metoda	k1gFnPc1	metoda
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
procesu	proces	k1gInSc2	proces
meiózy	meióza	k1gFnSc2	meióza
nebo	nebo	k8xC	nebo
mitózy	mitóza	k1gFnSc2	mitóza
<g/>
,	,	kIx,	,
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používaná	používaný	k2eAgFnSc1d1	používaná
tetrádová	tetrádový	k2eAgFnSc1d1	tetrádový
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
mapovací	mapovací	k2eAgFnSc7d1	mapovací
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
tvorba	tvorba	k1gFnSc1	tvorba
vřecek	vřecko	k1gNnPc2	vřecko
se	s	k7c7	s
4	[number]	k4	4
sporami	spora	k1gFnPc7	spora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mikromanipulační	mikromanipulační	k2eAgFnSc7d1	mikromanipulační
technikou	technika	k1gFnSc7	technika
(	(	kIx(	(
<g/>
po	po	k7c6	po
natrávení	natrávení	k1gNnSc6	natrávení
stěny	stěna	k1gFnSc2	stěna
vřecka	vřecko	k1gNnSc2	vřecko
<g/>
)	)	kIx)	)
izolují	izolovat	k5eAaBmIp3nP	izolovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
získají	získat	k5eAaPmIp3nP	získat
4	[number]	k4	4
haploidní	haploidní	k2eAgFnPc1d1	haploidní
meitoické	meitoická	k1gFnPc1	meitoická
"	"	kIx"	"
<g/>
klony	klon	k1gInPc1	klon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
genů	gen	k1gInPc2	gen
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
chromosomech	chromosom	k1gInPc6	chromosom
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinkový	kvasinkový	k2eAgInSc1d1	kvasinkový
chromosom	chromosom	k1gInSc1	chromosom
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
lineární	lineární	k2eAgInSc1d1	lineární
molekulu	molekula	k1gFnSc4	molekula
dsDNA	dsDNA	k?	dsDNA
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
1,107	[number]	k4	1,107
Mbp	Mbp	k1gFnPc2	Mbp
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
repetice	repetice	k1gFnSc1	repetice
(	(	kIx(	(
<g/>
rozptýlené	rozptýlený	k2eAgNnSc1d1	rozptýlené
a	a	k8xC	a
krátké	krátký	k2eAgNnSc1d1	krátké
tandemové	tandemový	k2eAgNnSc1d1	tandemové
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
genové	genový	k2eAgFnPc1d1	genová
repetice	repetice	k1gFnPc1	repetice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvasinkách	kvasinka	k1gFnPc6	kvasinka
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
cca	cca	kA	cca
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
molekul	molekula	k1gFnPc2	molekula
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kultivačních	kultivační	k2eAgFnPc6d1	kultivační
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
růstové	růstový	k2eAgFnSc6d1	růstová
fázi	fáze	k1gFnSc6	fáze
aj.	aj.	kA	aj.
mitochondriální	mitochondriální	k2eAgInSc4d1	mitochondriální
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
mtDNA	mtDNA	k?	mtDNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
mtDNA	mtDNA	k?	mtDNA
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nS	lišit
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
108	[number]	k4	108
kb	kb	kA	kb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kružnicové	kružnicový	k2eAgFnPc1d1	kružnicová
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
i	i	k9	i
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
.	.	kIx.	.
</s>
<s>
Plazmidová	Plazmidový	k2eAgFnSc1d1	Plazmidový
DNA	dna	k1gFnSc1	dna
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
buňky	buňka	k1gFnPc4	buňka
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nekóduje	kódovat	k5eNaBmIp3nS	kódovat
esenciální	esenciální	k2eAgFnSc1d1	esenciální
životní	životní	k2eAgFnSc1d1	životní
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
kmenů	kmen	k1gInPc2	kmen
kvasinky	kvasinka	k1gFnSc2	kvasinka
S.	S.	kA	S.
cerevisiae	cerevisia	k1gMnSc2	cerevisia
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
však	však	k9	však
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
nalézt	nalézt	k5eAaBmF	nalézt
kruhový	kruhový	k2eAgInSc4d1	kruhový
2	[number]	k4	2
μ	μ	k?	μ
plazmid	plazmida	k1gFnPc2	plazmida
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
kvasinka	kvasinka	k1gFnSc1	kvasinka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
plazmidovou	plazmidový	k2eAgFnSc4d1	plazmidový
DNA	dno	k1gNnPc4	dno
asi	asi	k9	asi
v	v	k7c6	v
60	[number]	k4	60
kopiích	kopie	k1gFnPc6	kopie
v	v	k7c6	v
haploidním	haploidní	k2eAgInSc6d1	haploidní
a	a	k8xC	a
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
počet	počet	k1gInSc1	počet
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
diploidním	diploidní	k2eAgNnSc6d1	diploidní
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
plazmidu	plazmid	k1gInSc2	plazmid
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kvasinky	kvasinka	k1gFnPc4	kvasinka
značena	značen	k2eAgFnSc1d1	značena
jako	jako	k9	jako
cir	cir	k?	cir
<g/>
+	+	kIx~	+
resp.	resp.	kA	resp.
cir-	cir-	k?	cir-
<g/>
.	.	kIx.	.
2	[number]	k4	2
μ	μ	k?	μ
plazmid	plazmid	k1gInSc1	plazmid
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
coby	coby	k?	coby
základ	základ	k1gInSc1	základ
vektorů	vektor	k1gInPc2	vektor
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
genových	genový	k2eAgFnPc6d1	genová
manipulacích	manipulace	k1gFnPc6	manipulace
u	u	k7c2	u
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
představuje	představovat	k5eAaImIp3nS	představovat
velice	velice	k6eAd1	velice
účinný	účinný	k2eAgInSc4d1	účinný
způsob	způsob	k1gInSc4	způsob
využití	využití	k1gNnSc4	využití
cukru	cukr	k1gInSc2	cukr
kvasinkou	kvasinka	k1gFnSc7	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
molekuly	molekula	k1gFnSc2	molekula
glukózy	glukóza	k1gFnSc2	glukóza
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
12	[number]	k4	12
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
kvasinka	kvasinka	k1gFnSc1	kvasinka
získá	získat	k5eAaPmIp3nS	získat
energii	energie	k1gFnSc4	energie
postačující	postačující	k2eAgFnSc4d1	postačující
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
až	až	k9	až
38	[number]	k4	38
molekul	molekula	k1gFnPc2	molekula
ATP.	atp.	kA	atp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
38	[number]	k4	38
:	:	kIx,	:
A	a	k8xC	a
D	D	kA	D
P	P	kA	P
:	:	kIx,	:
+	+	kIx~	+
38	[number]	k4	38
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
O	O	kA	O
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
O	O	kA	O
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
6	[number]	k4	6
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
+	+	kIx~	+
38	[number]	k4	38
:	:	kIx,	:
A	a	k8xC	a
T	T	kA	T
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
O	O	kA	O
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
O	O	kA	O
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
38	[number]	k4	38
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ADP	ADP	kA	ADP
<g/>
}	}	kIx)	}
+38	+38	k4	+38
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
PO	Po	kA	Po
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
CO	co	k3yInSc1	co
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
O	O	kA	O
<g/>
}	}	kIx)	}
+38	+38	k4	+38
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ATP	atp	kA	atp
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Vysoký	vysoký	k2eAgInSc1d1	vysoký
energetický	energetický	k2eAgInSc1d1	energetický
zisk	zisk	k1gInSc1	zisk
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
počáteční	počáteční	k2eAgFnSc4d1	počáteční
energetickou	energetický	k2eAgFnSc4d1	energetická
investici	investice	k1gFnSc4	investice
do	do	k7c2	do
syntézy	syntéza	k1gFnSc2	syntéza
a	a	k8xC	a
údržby	údržba	k1gFnSc2	údržba
komplexního	komplexní	k2eAgInSc2d1	komplexní
enzymového	enzymový	k2eAgInSc2d1	enzymový
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
a	a	k8xC	a
nevyplatí	vyplatit	k5eNaPmIp3nS	vyplatit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
glukóza	glukóza	k1gFnSc1	glukóza
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
a	a	k8xC	a
přísun	přísun	k1gInSc1	přísun
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
kynoucím	kynoucí	k2eAgNnSc6d1	kynoucí
těstě	těsto	k1gNnSc6	těsto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
kvasinka	kvasinka	k1gFnSc1	kvasinka
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
k	k	k7c3	k
lihovému	lihový	k2eAgNnSc3d1	lihové
kvašení	kvašení	k1gNnSc3	kvašení
(	(	kIx(	(
<g/>
etanolové	etanolový	k2eAgFnSc3d1	etanolová
fermentaci	fermentace	k1gFnSc3	fermentace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
houby	houba	k1gFnSc2	houba
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
organizmy	organizmus	k1gInPc1	organizmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
superskupiny	superskupina	k1gFnPc4	superskupina
Opisthokonta	Opisthokonto	k1gNnSc2	Opisthokonto
<g/>
,	,	kIx,	,
domény	doména	k1gFnSc2	doména
(	(	kIx(	(
<g/>
nadříše	nadříše	k1gFnSc1	nadříše
<g/>
)	)	kIx)	)
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Netvoří	tvořit	k5eNaImIp3nS	tvořit
však	však	k9	však
žádnou	žádný	k3yNgFnSc4	žádný
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
taxonomickou	taxonomický	k2eAgFnSc4d1	Taxonomická
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
je	on	k3xPp3gInPc4	on
jednotně	jednotně	k6eAd1	jednotně
definovat	definovat	k5eAaBmF	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
takové	takový	k3xDgFnPc1	takový
jsou	být	k5eAaImIp3nP	být
roztroušeny	roztrousit	k5eAaPmNgFnP	roztrousit
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
odděleních	oddělení	k1gNnPc6	oddělení
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
jako	jako	k9	jako
houby	houba	k1gFnPc4	houba
vřeckovýtrusné	vřeckovýtrusný	k2eAgFnPc4d1	vřeckovýtrusná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stopkovýtrusné	stopkovýtrusný	k2eAgFnPc1d1	stopkovýtrusná
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedený	uvedený	k2eAgInSc1d1	uvedený
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vyjmenování	vyjmenování	k1gNnSc4	vyjmenování
základních	základní	k2eAgFnPc2d1	základní
čeledí	čeleď	k1gFnPc2	čeleď
kvasinek	kvasinka	k1gFnPc2	kvasinka
vychází	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
The	The	k1gFnSc2	The
yeasts	yeasts	k6eAd1	yeasts
<g/>
,	,	kIx,	,
a	a	k8xC	a
taxonomic	taxonomic	k1gMnSc1	taxonomic
study	stud	k1gInPc1	stud
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Vřeckovýtrusné	Vřeckovýtrusný	k2eAgFnPc1d1	Vřeckovýtrusný
kvasinky	kvasinka	k1gFnPc1	kvasinka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
skupinách	skupina	k1gFnPc6	skupina
<g/>
:	:	kIx,	:
Primitivní	primitivní	k2eAgFnPc1d1	primitivní
archiaskomycety	archiaskomyceta	k1gFnPc1	archiaskomyceta
<g/>
:	:	kIx,	:
čeledi	čeleď	k1gFnPc1	čeleď
Schizosaccharomycetaceae	Schizosaccharomycetacea	k1gFnPc1	Schizosaccharomycetacea
<g/>
,	,	kIx,	,
Taphrinaceae	Taphrinacea	k1gInPc1	Taphrinacea
<g/>
,	,	kIx,	,
Protomycetaceae	Protomycetaceae	k1gNnPc1	Protomycetaceae
<g/>
,	,	kIx,	,
Pneumocystidaceae	Pneumocystidacea	k1gInPc1	Pneumocystidacea
Pravé	pravá	k1gFnSc2	pravá
vřeckovýtrusné	vřeckovýtrusný	k2eAgInPc1d1	vřeckovýtrusný
<g/>
:	:	kIx,	:
rody	rod	k1gInPc1	rod
Endomyces	Endomycesa	k1gFnPc2	Endomycesa
a	a	k8xC	a
Oosporidium	Oosporidium	k1gNnSc1	Oosporidium
Hemiaskomycety	Hemiaskomycet	k2eAgFnPc1d1	Hemiaskomycet
<g/>
:	:	kIx,	:
čeledi	čeleď	k1gFnPc1	čeleď
Ascoidaceae	Ascoidaceae	k1gNnPc2	Ascoidaceae
<g/>
,	,	kIx,	,
Cepaloascaceae	Cepaloascacea	k1gMnSc4	Cepaloascacea
<g/>
,	,	kIx,	,
Dipodascaceae	Dipodascacea	k1gMnSc4	Dipodascacea
<g/>
,	,	kIx,	,
Endomycetaceae	Endomycetacea	k1gMnSc4	Endomycetacea
<g/>
,	,	kIx,	,
Eremotheciaceae	Eremotheciacea	k1gMnSc4	Eremotheciacea
<g/>
,	,	kIx,	,
Lipomycetaceae	Lipomycetacea	k1gMnSc4	Lipomycetacea
<g/>
,	,	kIx,	,
Metschnikowiaceae	Metschnikowiacea	k1gMnSc4	Metschnikowiacea
<g/>
,	,	kIx,	,
Saccharomycetaceae	Saccharomycetacea	k1gMnSc4	Saccharomycetacea
<g/>
,	,	kIx,	,
Saccharomycodaceae	Saccharomycodacea	k1gMnSc4	Saccharomycodacea
<g/>
,	,	kIx,	,
Saccharomycopsidaceae	Saccharomycopsidacea	k1gMnSc4	Saccharomycopsidacea
<g/>
,	,	kIx,	,
Candidaceae	Candidacea	k1gMnSc4	Candidacea
Stopkovýtrusné	Stopkovýtrusný	k2eAgFnSc2d1	Stopkovýtrusný
kvasinky	kvasinka	k1gFnSc2	kvasinka
se	se	k3xPyFc4	se
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
nachází	nacházet	k5eAaImIp3nS	nacházet
zejména	zejména	k9	zejména
v	v	k7c6	v
čeledích	čeleď	k1gFnPc6	čeleď
Spolobolomycetaceae	Spolobolomycetaceae	k1gNnSc2	Spolobolomycetaceae
<g/>
,	,	kIx,	,
Cryptococcaceae	Cryptococcaceae	k1gNnSc2	Cryptococcaceae
<g/>
,	,	kIx,	,
Filobasidiaceae	Filobasidiaceae	k1gNnSc2	Filobasidiaceae
<g/>
,	,	kIx,	,
Teliosporaceae	Teliosporaceae	k1gNnSc2	Teliosporaceae
a	a	k8xC	a
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
Tremellales	Tremellales	k1gInSc1	Tremellales
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kvasinky	kvasinka	k1gFnPc1	kvasinka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
<g/>
,	,	kIx,	,
Kloeckera	Kloeckero	k1gNnPc1	Kloeckero
<g/>
,	,	kIx,	,
Torulopsis	Torulopsis	k1gFnPc1	Torulopsis
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
kvašení	kvašení	k1gNnSc4	kvašení
(	(	kIx(	(
<g/>
alkoholové	alkoholový	k2eAgFnSc2d1	alkoholová
<g/>
,	,	kIx,	,
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgFnSc2d1	mléčná
<g/>
,	,	kIx,	,
máselné	máselný	k2eAgFnSc2d1	máselná
<g/>
,	,	kIx,	,
propionové	propionový	k2eAgFnSc2d1	propionová
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
rozložení	rozložení	k1gNnSc2	rozložení
kvasinkové	kvasinkový	k2eAgFnSc2d1	kvasinková
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vývinu	vývin	k1gInSc2	vývin
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
a	a	k8xC	a
spodní	spodní	k2eAgFnSc6d1	spodní
<g/>
.	.	kIx.	.
</s>
<s>
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisia	k1gFnSc2	cerevisia
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
využívaná	využívaný	k2eAgFnSc1d1	využívaná
kvasinka	kvasinka	k1gFnSc1	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc3	výroba
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
pečení	pečení	k1gNnSc2	pečení
chleba	chléb	k1gInSc2	chléb
aj.	aj.	kA	aj.
Průmyslově	průmyslově	k6eAd1	průmyslově
využívané	využívaný	k2eAgInPc4d1	využívaný
kmeny	kmen	k1gInPc4	kmen
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
staletí	staletí	k1gNnPc2	staletí
šlechtěné	šlechtěný	k2eAgFnSc2d1	šlechtěná
a	a	k8xC	a
polyploidní	polyploidní	k2eAgFnSc2d1	polyploidní
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
kvasinka	kvasinka	k1gFnSc1	kvasinka
jako	jako	k8xC	jako
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
eukaryotický	eukaryotický	k2eAgInSc1d1	eukaryotický
organismus	organismus	k1gInSc1	organismus
také	také	k9	také
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
modelem	model	k1gInSc7	model
v	v	k7c6	v
molekulární	molekulární	k2eAgFnSc6d1	molekulární
biologii	biologie	k1gFnSc6	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinka	kvasinka	k1gFnSc1	kvasinka
Rhodotorula	Rhodotorula	k1gFnSc1	Rhodotorula
glutinis	glutinis	k1gFnSc1	glutinis
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
k	k	k7c3	k
významným	významný	k2eAgInPc3d1	významný
průmyslovým	průmyslový	k2eAgInPc3d1	průmyslový
kmenům	kmen	k1gInPc3	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
zejména	zejména	k9	zejména
k	k	k7c3	k
obohacování	obohacování	k1gNnSc3	obohacování
krmných	krmný	k2eAgFnPc2d1	krmná
směsí	směs	k1gFnPc2	směs
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
obsahu	obsah	k1gInSc3	obsah
karotenoidů	karotenoid	k1gInPc2	karotenoid
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
produkovat	produkovat	k5eAaImF	produkovat
i	i	k8xC	i
ve	v	k7c6	v
stresových	stresový	k2eAgFnPc6d1	stresová
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinka	kvasinka	k1gFnSc1	kvasinka
Phaffia	Phaffia	k1gFnSc1	Phaffia
rhodozyma	rhodozyma	k1gFnSc1	rhodozyma
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
karotenoidního	karotenoidní	k2eAgNnSc2d1	karotenoidní
barviva	barvivo	k1gNnSc2	barvivo
astaxanthinu	astaxanthinout	k5eAaPmIp1nS	astaxanthinout
jako	jako	k9	jako
dietní	dietní	k2eAgInSc1d1	dietní
doplněk	doplněk	k1gInSc1	doplněk
výživy	výživa	k1gFnSc2	výživa
lososů	losos	k1gMnPc2	losos
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
hydrolyzované	hydrolyzovaný	k2eAgFnPc1d1	hydrolyzovaná
buňky	buňka	k1gFnPc1	buňka
přidány	přidán	k2eAgFnPc1d1	přidána
ke	k	k7c3	k
krmivu	krmivo	k1gNnSc3	krmivo
<g/>
,	,	kIx,	,
astaxanthin	astaxanthin	k2eAgInSc1d1	astaxanthin
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
absorbován	absorbovat	k5eAaBmNgMnS	absorbovat
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
barvu	barva	k1gFnSc4	barva
lososího	lososí	k2eAgNnSc2d1	lososí
masa	maso	k1gNnSc2	maso
z	z	k7c2	z
růžové	růžový	k2eAgFnSc2d1	růžová
na	na	k7c4	na
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
barvu	barva	k1gFnSc4	barva
žloutku	žloutek	k1gInSc2	žloutek
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Yarrowia	Yarrowia	k1gFnSc1	Yarrowia
lipolytica	lipolytica	k1gMnSc1	lipolytica
(	(	kIx(	(
<g/>
zástupce	zástupce	k1gMnSc1	zástupce
poltivých	poltivý	k2eAgFnPc2d1	poltivý
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
metabolizovat	metabolizovat	k5eAaImF	metabolizovat
ropné	ropný	k2eAgInPc4d1	ropný
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
citronové	citronový	k2eAgFnSc2d1	citronová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
patogenní	patogenní	k2eAgInPc4d1	patogenní
druhy	druh	k1gInPc4	druh
kvasinek	kvasinka	k1gFnPc2	kvasinka
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
zástupci	zástupce	k1gMnPc7	zástupce
rodů	rod	k1gInPc2	rod
Candida	Candid	k1gMnSc2	Candid
<g/>
,	,	kIx,	,
Cryptococcus	Cryptococcus	k1gInSc1	Cryptococcus
<g/>
,	,	kIx,	,
Trichosporon	Trichosporon	k1gInSc1	Trichosporon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
různá	různý	k2eAgFnSc1d1	různá
kožní	kožní	k2eAgFnSc1d1	kožní
<g/>
,	,	kIx,	,
slizniční	slizniční	k2eAgFnSc1d1	slizniční
aj.	aj.	kA	aj.
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Candida	Candida	k1gFnSc1	Candida
albicans	albicans	k1gInSc1	albicans
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
imperfektních	imperfektní	k2eAgFnPc2d1	imperfektní
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patogen	patogen	k1gInSc1	patogen
urogenitálního	urogenitální	k2eAgInSc2d1	urogenitální
traktu	trakt	k1gInSc2	trakt
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
potlačena	potlačen	k2eAgFnSc1d1	potlačena
přirozená	přirozený	k2eAgFnSc1d1	přirozená
poševní	poševní	k2eAgFnSc1d1	poševní
mikroflóra	mikroflóra	k1gFnSc1	mikroflóra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
léčba	léčba	k1gFnSc1	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
začít	začít	k5eAaPmF	začít
množit	množit	k5eAaImF	množit
a	a	k8xC	a
působit	působit	k5eAaImF	působit
značné	značný	k2eAgFnPc4d1	značná
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Cryptococcus	Cryptococcus	k1gInSc1	Cryptococcus
neoformans	oformans	k6eNd1	oformans
je	být	k5eAaImIp3nS	být
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
patogen	patogen	k1gInSc4	patogen
napadající	napadající	k2eAgInSc4d1	napadající
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
přenašeči	přenašeč	k1gMnPc7	přenašeč
jsou	být	k5eAaImIp3nP	být
holubi	holub	k1gMnPc1	holub
<g/>
.	.	kIx.	.
</s>
<s>
Trichosporon	Trichosporon	k1gInSc1	Trichosporon
cutaneum	cutaneum	k1gInSc1	cutaneum
je	být	k5eAaImIp3nS	být
kožní	kožní	k2eAgInSc1d1	kožní
patogen	patogen	k1gInSc1	patogen
(	(	kIx(	(
<g/>
zástupce	zástupce	k1gMnSc1	zástupce
poltivých	poltivý	k2eAgFnPc2d1	poltivý
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
žijící	žijící	k2eAgFnSc1d1	žijící
na	na	k7c6	na
vousech	vous	k1gInPc6	vous
a	a	k8xC	a
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
<g/>
.	.	kIx.	.
</s>
