<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
je	on	k3xPp3gMnPc4	on
největší	veliký	k2eAgInSc1d3	veliký
internetový	internetový	k2eAgInSc1d1	internetový
server	server	k1gInSc1	server
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
videosouborů	videosoubor	k1gInPc2	videosoubor
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
PayPalu	PayPal	k1gInSc2	PayPal
Chad	Chad	k1gInSc1	Chad
Hurley	Hurlea	k1gFnSc2	Hurlea
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Chen	Chen	k1gMnSc1	Chen
a	a	k8xC	a
Jawed	Jawed	k1gMnSc1	Jawed
Karim	Karim	k1gMnSc1	Karim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
zakoupen	zakoupit	k5eAaPmNgMnS	zakoupit
společností	společnost	k1gFnSc7	společnost
Google	Google	k1gNnSc2	Google
za	za	k7c4	za
1,65	[number]	k4	1,65
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
asi	asi	k9	asi
37	[number]	k4	37
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k6eAd1	Google
nyní	nyní	k6eAd1	nyní
provozuje	provozovat	k5eAaImIp3nS	provozovat
tuto	tento	k3xDgFnSc4	tento
stránku	stránka	k1gFnSc4	stránka
jako	jako	k8xS	jako
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
uživatelské	uživatelský	k2eAgInPc4d1	uživatelský
účty	účet	k1gInPc4	účet
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
společnostmi	společnost	k1gFnPc7	společnost
jsou	být	k5eAaImIp3nP	být
propojené	propojený	k2eAgFnPc1d1	propojená
<g/>
.	.	kIx.	.
</s>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
povoluje	povolovat	k5eAaImIp3nS	povolovat
svým	svůj	k3xOyFgMnPc3	svůj
uživatelům	uživatel	k1gMnPc3	uživatel
nahrát	nahrát	k5eAaPmF	nahrát
videa	video	k1gNnPc4	video
<g/>
,	,	kIx,	,
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
hodnotit	hodnotit	k5eAaImF	hodnotit
<g/>
,	,	kIx,	,
sdílet	sdílet	k5eAaImF	sdílet
a	a	k8xC	a
komentovat	komentovat	k5eAaBmF	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
videoklipy	videoklip	k1gInPc1	videoklip
<g/>
,	,	kIx,	,
TV	TV	kA	TV
klipy	klip	k1gInPc1	klip
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgNnPc1d1	hudební
videa	video	k1gNnPc1	video
<g/>
,	,	kIx,	,
trailery	trailer	k1gInPc1	trailer
k	k	k7c3	k
filmům	film	k1gInPc3	film
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
jako	jako	k8xS	jako
například	například	k6eAd1	například
video-blogy	videologa	k1gFnPc1	video-bloga
<g/>
,	,	kIx,	,
krátká	krátký	k2eAgNnPc1d1	krátké
originální	originální	k2eAgNnPc1d1	originální
videa	video	k1gNnPc1	video
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vzdělávací	vzdělávací	k2eAgNnPc1d1	vzdělávací
videa	video	k1gNnPc1	video
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
má	mít	k5eAaImIp3nS	mít
YouTube	YouTub	k1gInSc5	YouTub
i	i	k9	i
české	český	k2eAgNnSc4d1	české
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
spustěná	spustěný	k2eAgFnSc1d1	spustěný
25	[number]	k4	25
<g/>
.	.	kIx.	.
služba	služba	k1gFnSc1	služba
Google	Google	k1gFnSc2	Google
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k1gInSc1	Google
kromě	kromě	k7c2	kromě
českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
serveru	server	k1gInSc2	server
přinesl	přinést	k5eAaPmAgInS	přinést
také	také	k9	také
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
22	[number]	k4	22
<g/>
.	.	kIx.	.
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
desátou	desátá	k1gFnSc4	desátá
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
YouTube	YouTub	k1gInSc5	YouTub
lokalizován	lokalizovat	k5eAaBmNgInS	lokalizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
server	server	k1gInSc1	server
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
publikační	publikační	k2eAgFnSc2d1	publikační
platformy	platforma	k1gFnSc2	platforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
placená	placený	k2eAgFnSc1d1	placená
verze	verze	k1gFnSc1	verze
YouTube	YouTub	k1gInSc5	YouTub
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
YouTube	YouTub	k1gInSc5	YouTub
Red	Red	k1gFnSc1	Red
<g/>
,	,	kIx,	,
umožnující	umožnující	k2eAgFnSc1d1	umožnující
vypnout	vypnout	k5eAaPmF	vypnout
reklamy	reklama	k1gFnPc4	reklama
u	u	k7c2	u
videí	video	k1gNnPc2	video
a	a	k8xC	a
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
obsah	obsah	k1gInSc4	obsah
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
video	video	k1gNnSc1	video
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
publikováno	publikovat	k5eAaBmNgNnS	publikovat
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
"	"	kIx"	"
<g/>
Me	Me	k1gFnSc4	Me
at	at	k?	at
the	the	k?	the
zoo	zoo	k1gFnSc1	zoo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Já	já	k3xPp1nSc1	já
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
autor	autor	k1gMnSc1	autor
komentoval	komentovat	k5eAaBmAgMnS	komentovat
expozici	expozice	k1gFnSc4	expozice
slonů	slon	k1gMnPc2	slon
v	v	k7c6	v
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
v	v	k7c6	v
americkém	americký	k2eAgMnSc6d1	americký
San	San	k1gMnSc6	San
Diegu	Dieg	k1gMnSc6	Dieg
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
měla	mít	k5eAaImAgFnS	mít
necelých	celý	k2eNgFnPc2d1	necelá
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
na	na	k7c4	na
server	server	k1gInSc4	server
nahrál	nahrát	k5eAaBmAgMnS	nahrát
Jawed	Jawed	k1gMnSc1	Jawed
Karim	Karim	k1gMnSc1	Karim
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
server	server	k1gInSc1	server
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
sdílení	sdílení	k1gNnSc4	sdílení
domácích	domácí	k2eAgNnPc2d1	domácí
videí	video	k1gNnPc2	video
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
ale	ale	k9	ale
přibyly	přibýt	k5eAaPmAgInP	přibýt
hudební	hudební	k2eAgInPc4d1	hudební
videoklipy	videoklip	k1gInPc4	videoklip
<g/>
,	,	kIx,	,
videohry	videohra	k1gFnPc4	videohra
nebo	nebo	k8xC	nebo
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
obsažených	obsažený	k2eAgNnPc2d1	obsažené
videí	video	k1gNnPc2	video
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
fenomény	fenomén	k1gInPc1	fenomén
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
světového	světový	k2eAgInSc2d1	světový
nebo	nebo	k8xC	nebo
národních	národní	k2eAgInPc2d1	národní
internetů	internet	k1gInPc2	internet
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
zhlédnutí	zhlédnutí	k1gNnSc2	zhlédnutí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
založení	založení	k1gNnSc2	založení
se	se	k3xPyFc4	se
zlepšovaly	zlepšovat	k5eAaImAgFnP	zlepšovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
možnosti	možnost	k1gFnPc1	možnost
záznamových	záznamový	k2eAgNnPc2d1	záznamové
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
přenosové	přenosový	k2eAgFnSc2d1	přenosová
kapacity	kapacita	k1gFnSc2	kapacita
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
roste	růst	k5eAaImIp3nS	růst
také	také	k9	také
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
kvalita	kvalita	k1gFnSc1	kvalita
videí	video	k1gNnPc2	video
na	na	k7c6	na
serveru	server	k1gInSc6	server
<g/>
.	.	kIx.	.
</s>
<s>
Průběžně	průběžně	k6eAd1	průběžně
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
a	a	k8xC	a
přizpůsoboval	přizpůsobovat	k5eAaImAgInS	přizpůsobovat
i	i	k9	i
layout	layout	k1gInSc1	layout
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
nabídka	nabídka	k1gFnSc1	nabídka
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
s	s	k7c7	s
vyššími	vysoký	k2eAgInPc7d2	vyšší
technickými	technický	k2eAgInPc7d1	technický
nároky	nárok	k1gInPc7	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Youtuber	Youtuber	k1gInSc1	Youtuber
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
roustoucí	roustoucí	k2eAgFnSc7d1	roustoucí
popularitou	popularita	k1gFnSc7	popularita
YouTube	YouTub	k1gInSc5	YouTub
se	se	k3xPyFc4	se
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
i	i	k9	i
fenomén	fenomén	k1gInSc1	fenomén
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgInPc2d1	zvaný
YouTuberů	YouTuber	k1gInPc2	YouTuber
(	(	kIx(	(
<g/>
YouTubers	YouTubers	k1gInSc1	YouTubers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
mladé	mladý	k2eAgMnPc4d1	mladý
tvůrce	tvůrce	k1gMnPc4	tvůrce
obsahu	obsah	k1gInSc2	obsah
(	(	kIx(	(
<g/>
videí	video	k1gNnPc2	video
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kolem	kolem	k7c2	kolem
sebe	se	k3xPyFc2	se
budují	budovat	k5eAaImIp3nP	budovat
komunitu	komunita	k1gFnSc4	komunita
svých	svůj	k3xOyFgMnPc2	svůj
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
i	i	k9	i
termín	termín	k1gInSc1	termín
influenceři	influencer	k1gMnPc1	influencer
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgMnPc4	svůj
fanoušky	fanoušek	k1gMnPc4	fanoušek
(	(	kIx(	(
<g/>
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
)	)	kIx)	)
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
(	(	kIx(	(
<g/>
Facebook	Facebook	k1gInSc4	Facebook
<g/>
,	,	kIx,	,
Pinterest	Pinterest	k1gFnSc4	Pinterest
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
YouTubeři	YouTuber	k1gMnPc1	YouTuber
nejsou	být	k5eNaImIp3nP	být
klasickými	klasický	k2eAgFnPc7d1	klasická
celospolečenskými	celospolečenský	k2eAgFnPc7d1	celospolečenská
celebritami	celebrita	k1gFnPc7	celebrita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
cílovou	cílový	k2eAgFnSc4d1	cílová
skupinu	skupina	k1gFnSc4	skupina
mívají	mívat	k5eAaImIp3nP	mívat
větší	veliký	k2eAgFnSc4d2	veliký
signifikanci	signifikance	k1gFnSc4	signifikance
než	než	k8xS	než
celebrity	celebrita	k1gFnPc4	celebrita
z	z	k7c2	z
tradičních	tradiční	k2eAgNnPc2d1	tradiční
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
prosadit	prosadit	k5eAaPmF	prosadit
jedině	jedině	k6eAd1	jedině
<g/>
,	,	kIx,	,
když	když	k8xS	když
produkují	produkovat	k5eAaImIp3nP	produkovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
když	když	k8xS	když
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Pojítkem	pojítko	k1gNnSc7	pojítko
mezi	mezi	k7c7	mezi
celebritou	celebrita	k1gFnSc7	celebrita
a	a	k8xC	a
divákem	divák	k1gMnSc7	divák
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
vztah	vztah	k1gInSc1	vztah
médium-divák	médiumivák	k1gInSc1	médium-divák
i	i	k8xC	i
reálný	reálný	k2eAgInSc1d1	reálný
obsahový	obsahový	k2eAgInSc1d1	obsahový
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnSc1	komunikace
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
rovnostářská	rovnostářský	k2eAgFnSc1d1	rovnostářská
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
sedí	sedit	k5eAaImIp3nP	sedit
u	u	k7c2	u
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
YouTuberem	YouTuber	k1gInSc7	YouTuber
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
video	video	k1gNnSc4	video
lze	lze	k6eAd1	lze
napsat	napsat	k5eAaBmF	napsat
komentář	komentář	k1gInSc4	komentář
a	a	k8xC	a
u	u	k7c2	u
tradičních	tradiční	k2eAgFnPc2d1	tradiční
celebrit	celebrita	k1gFnPc2	celebrita
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
spotřebitelem	spotřebitel	k1gMnSc7	spotřebitel
nějaký	nějaký	k3yIgInSc4	nějaký
typ	typ	k1gInSc4	typ
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zakládá	zakládat	k5eAaImIp3nS	zakládat
nerovnost	nerovnost	k1gFnSc1	nerovnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
YouTubeři	YouTuber	k1gMnPc1	YouTuber
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
v	v	k7c6	v
marketingu	marketing	k1gInSc6	marketing
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc6	on
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
komunita	komunita	k1gFnSc1	komunita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
stávají	stávat	k5eAaImIp3nP	stávat
mainstreamovými	mainstreamový	k2eAgFnPc7d1	mainstreamová
celebritami	celebrita	k1gFnPc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
vlastních	vlastní	k2eAgInPc6d1	vlastní
festivalech	festival	k1gInPc6	festival
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Utubering	Utubering	k1gInSc1	Utubering
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgFnPc6d1	společenská
akcích	akce	k1gFnPc6	akce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zlatý	zlatý	k2eAgInSc1d1	zlatý
Slavík	slavík	k1gInSc1	slavík
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
agentury	agentura	k1gFnPc1	agentura
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	on	k3xPp3gNnSc4	on
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
GameBoost	GameBoost	k1gInSc1	GameBoost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
také	také	k9	také
orgány	orgány	k1gFnPc1	orgány
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
generací	generace	k1gFnSc7	generace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
<g/>
,	,	kIx,	,
propagace	propagace	k1gFnSc1	propagace
určitých	určitý	k2eAgFnPc2d1	určitá
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
doporučení	doporučení	k1gNnSc4	doporučení
jako	jako	k8xC	jako
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
řízení	řízení	k1gNnSc2	řízení
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
první	první	k4xOgFnSc4	první
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
YouTubery	YouTuber	k1gInPc7	YouTuber
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
navázala	navázat	k5eAaPmAgFnS	navázat
Rada	rada	k1gFnSc1	rada
vlády	vláda	k1gFnSc2	vláda
pro	pro	k7c4	pro
udržitelný	udržitelný	k2eAgInSc4d1	udržitelný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Rotou	rota	k1gFnSc7	rota
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
Cílů	cíl	k1gInPc2	cíl
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
video	video	k1gNnSc1	video
mělo	mít	k5eAaImAgNnS	mít
během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
více	hodně	k6eAd2	hodně
zhlédnutí	zhlédnutí	k1gNnSc2	zhlédnutí
než	než	k8xS	než
Obamova	Obamův	k2eAgFnSc1d1	Obamova
řeč	řeč	k1gFnSc1	řeč
na	na	k7c4	na
podobné	podobný	k2eAgNnSc4d1	podobné
téma	téma	k1gNnSc4	téma
v	v	k7c6	v
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
YouTubeři	YouTuber	k1gMnPc1	YouTuber
dostávají	dostávat	k5eAaImIp3nP	dostávat
za	za	k7c2	za
jejich	jejich	k3xOp3gFnPc2	jejich
počet	počet	k1gInSc1	počet
odběratelů	odběratel	k1gMnPc2	odběratel
tvz	tvz	k?	tvz
<g/>
.	.	kIx.	.
</s>
<s>
Play	play	k0	play
Button	Button	k1gInSc1	Button
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
play	play	k0	play
buttonů	button	k1gInPc2	button
<g/>
:	:	kIx,	:
Silver	Silver	k1gInSc1	Silver
Play	play	k0	play
Button	Button	k1gInSc1	Button
(	(	kIx(	(
<g/>
Stříbrný	stříbrný	k1gInSc1	stříbrný
Play	play	k0	play
Button	Button	k1gInSc4	Button
<g/>
)	)	kIx)	)
-	-	kIx~	-
dostávají	dostávat	k5eAaImIp3nP	dostávat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
získání	získání	k1gNnSc4	získání
100	[number]	k4	100
000	[number]	k4	000
odběratelů	odběratel	k1gMnPc2	odběratel
Golden	Goldna	k1gFnPc2	Goldna
Play	play	k0	play
Button	Button	k1gInSc1	Button
(	(	kIx(	(
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
Play	play	k0	play
Button	Button	k1gInSc4	Button
<g/>
)	)	kIx)	)
-	-	kIx~	-
dostávají	dostávat	k5eAaImIp3nP	dostávat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
odběratelů	odběratel	k1gMnPc2	odběratel
Diamond	Diamond	k1gInSc4	Diamond
Play	play	k0	play
Button	Button	k1gInSc1	Button
(	(	kIx(	(
<g/>
Diamantový	diamantový	k2eAgInSc1d1	diamantový
Play	play	k0	play
Button	Button	k1gInSc4	Button
<g/>
)	)	kIx)	)
-	-	kIx~	-
dostávají	dostávat	k5eAaImIp3nP	dostávat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
odběratelů	odběratel	k1gMnPc2	odběratel
YouTube	YouTub	k1gInSc5	YouTub
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
dvě	dva	k4xCgFnPc4	dva
miliardy	miliarda	k4xCgFnPc4	miliarda
přístupů	přístup	k1gInPc2	přístup
denně	denně	k6eAd1	denně
a	a	k8xC	a
každou	každý	k3xTgFnSc4	každý
minutou	minutý	k2eAgFnSc4d1	minutá
uživatelé	uživatel	k1gMnPc1	uživatel
nahrají	nahrát	k5eAaPmIp3nP	nahrát
300	[number]	k4	300
hodin	hodina	k1gFnPc2	hodina
nových	nový	k2eAgNnPc2d1	nové
videí	video	k1gNnPc2	video
<g/>
.	.	kIx.	.
</s>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
je	on	k3xPp3gNnSc4	on
tak	tak	k9	tak
po	po	k7c6	po
internetovém	internetový	k2eAgMnSc6d1	internetový
vyhledávači	vyhledávač	k1gMnSc6	vyhledávač
Google	Googla	k1gFnSc6	Googla
druhou	druhý	k4xOgFnSc7	druhý
nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3	nejnavštěvovanější
webovou	webový	k2eAgFnSc7d1	webová
službou	služba	k1gFnSc7	služba
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
přístup	přístup	k1gInSc1	přístup
blokován	blokován	k2eAgInSc1d1	blokován
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
kritizováno	kritizován	k2eAgNnSc1d1	kritizováno
porušování	porušování	k1gNnSc1	porušování
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
a	a	k8xC	a
cenzorských	cenzorský	k2eAgFnPc2d1	cenzorská
organizací	organizace	k1gFnPc2	organizace
pak	pak	k9	pak
přílišná	přílišný	k2eAgFnSc1d1	přílišná
svoboda	svoboda	k1gFnSc1	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgMnPc7d1	jiný
autory	autor	k1gMnPc7	autor
naopak	naopak	k6eAd1	naopak
cenzura	cenzura	k1gFnSc1	cenzura
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
pomlouvačný	pomlouvačný	k2eAgInSc4d1	pomlouvačný
<g/>
,	,	kIx,	,
zesměšňující	zesměšňující	k2eAgInSc4d1	zesměšňující
obsah	obsah	k1gInSc4	obsah
či	či	k8xC	či
zobrazování	zobrazování	k1gNnSc4	zobrazování
násilných	násilný	k2eAgFnPc2d1	násilná
scén	scéna	k1gFnPc2	scéna
nebo	nebo	k8xC	nebo
sexu	sex	k1gInSc2	sex
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
plánovaným	plánovaný	k2eAgNnSc7d1	plánované
předplatným	předplatné	k1gNnSc7	předplatné
na	na	k7c4	na
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
hudební	hudební	k2eAgFnSc4d1	hudební
službu	služba	k1gFnSc4	služba
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
YouTube	YouTub	k1gInSc5	YouTub
podepsání	podepsání	k1gNnSc3	podepsání
nové	nový	k2eAgFnSc2d1	nová
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
interprety	interpret	k1gMnPc7	interpret
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
podmínky	podmínka	k1gFnPc1	podmínka
se	se	k3xPyFc4	se
ale	ale	k9	ale
nelíbí	líbit	k5eNaImIp3nS	líbit
některým	některý	k3yIgNnPc3	některý
nezávislým	závislý	k2eNgNnPc3d1	nezávislé
vydavatelstvím	vydavatelství	k1gNnPc3	vydavatelství
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
asociace	asociace	k1gFnSc1	asociace
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tato	tento	k3xDgFnSc1	tento
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
novým	nový	k2eAgFnPc3d1	nová
smluvním	smluvní	k2eAgFnPc3d1	smluvní
podmínkám	podmínka	k1gFnPc3	podmínka
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
vládní	vládní	k2eAgFnPc4d1	vládní
intervence	intervence	k1gFnPc4	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
YouTube	YouTub	k1gInSc5	YouTub
navštíví	navštívit	k5eAaPmIp3nS	navštívit
měsíčně	měsíčně	k6eAd1	měsíčně
5,1	[number]	k4	5,1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
unikátních	unikátní	k2eAgMnPc2d1	unikátní
českých	český	k2eAgMnPc2d1	český
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
alespoň	alespoň	k9	alespoň
jednou	jednou	k6eAd1	jednou
82	[number]	k4	82
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
připojených	připojený	k2eAgMnPc2d1	připojený
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulé	minulý	k2eAgFnSc6d1	minulá
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
videí	video	k1gNnPc2	video
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
Adobe	Adobe	kA	Adobe
Flash	Flash	k1gInSc1	Flash
Player	Player	k1gInSc1	Player
plug-in	plugn	k1gInSc1	plug-in
v	v	k7c6	v
internetovém	internetový	k2eAgMnSc6d1	internetový
prohlížeči	prohlížeč	k1gMnSc6	prohlížeč
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
přehrávač	přehrávač	k1gInSc1	přehrávač
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c4	na
HTML5	HTML5	k1gFnSc4	HTML5
API	API	kA	API
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
žádný	žádný	k3yNgInSc1	žádný
externí	externí	k2eAgInSc1d1	externí
dodatek	dodatek	k1gInSc1	dodatek
a	a	k8xC	a
YouTube	YouTub	k1gInSc5	YouTub
se	se	k3xPyFc4	se
o	o	k7c4	o
Flash	Flash	k1gInSc4	Flash
Playerovou	Playerový	k2eAgFnSc4d1	Playerový
verzi	verze	k1gFnSc4	verze
nestará	starat	k5eNaImIp3nS	starat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
YouTube	YouTub	k1gInSc5	YouTub
může	moct	k5eAaImIp3nS	moct
nahrát	nahrát	k5eAaBmF	nahrát
video	video	k1gNnSc1	video
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
do	do	k7c2	do
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ověřeni	ověřen	k2eAgMnPc1d1	ověřen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
skrz	skrz	k7c4	skrz
telefonní	telefonní	k2eAgNnSc4d1	telefonní
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
nahrávat	nahrávat	k5eAaImF	nahrávat
až	až	k9	až
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
videa	video	k1gNnPc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
mohly	moct	k5eAaImAgInP	moct
nahrávat	nahrávat	k5eAaImF	nahrávat
jakkoliv	jakkoliv	k6eAd1	jakkoliv
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
videa	video	k1gNnPc4	video
<g/>
,	,	kIx,	,
limit	limit	k1gInSc4	limit
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uživatelé	uživatel	k1gMnPc1	uživatel
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
obsah	obsah	k1gInSc4	obsah
s	s	k7c7	s
autorskými	autorský	k2eAgNnPc7d1	autorské
právy	právo	k1gNnPc7	právo
-	-	kIx~	-
jako	jako	k9	jako
například	například	k6eAd1	například
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
minutový	minutový	k2eAgInSc1d1	minutový
limit	limit	k1gInSc1	limit
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Videa	video	k1gNnPc1	video
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
lze	lze	k6eAd1	lze
nahrát	nahrát	k5eAaPmF	nahrát
také	také	k9	také
z	z	k7c2	z
mobilu	mobil	k1gInSc2	mobil
<g/>
.	.	kIx.	.
</s>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
nahrát	nahrát	k5eAaBmF	nahrát
videa	video	k1gNnPc4	video
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
formátech	formát	k1gInPc6	formát
<g/>
:	:	kIx,	:
.	.	kIx.	.
<g/>
AVI	AVI	kA	AVI
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
MKV	MKV	kA	MKV
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
DivX	DivX	k1gFnSc1	DivX
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
FLV	FLV	kA	FLV
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
ogg	ogg	k?	ogg
a	a	k8xC	a
.	.	kIx.	.
<g/>
ogv	ogv	k?	ogv
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
video	video	k1gNnSc4	video
formáty	formát	k1gInPc4	formát
jako	jako	k8xC	jako
například	například	k6eAd1	například
MPEG	MPEG	kA	MPEG
<g/>
,	,	kIx,	,
MPEG-	MPEG-	k1gFnSc1	MPEG-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
VOB	VOB	kA	VOB
a	a	k8xC	a
.	.	kIx.	.
<g/>
WMV	WMV	kA	WMV
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
YouTube	YouTub	k1gInSc5	YouTub
povolovalo	povolovat	k5eAaImAgNnS	povolovat
jednu	jeden	k4xCgFnSc4	jeden
kvalitu	kvalita	k1gFnSc4	kvalita
videa	video	k1gNnSc2	video
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
320	[number]	k4	320
<g/>
x	x	k?	x
<g/>
240	[number]	k4	240
pixelů	pixel	k1gInPc2	pixel
s	s	k7c7	s
MP3	MP3	k1gMnSc7	MP3
audiem	audius	k1gMnSc7	audius
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
možností	možnost	k1gFnPc2	možnost
-	-	kIx~	-
240	[number]	k4	240
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
360	[number]	k4	360
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
480	[number]	k4	480
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
720	[number]	k4	720
<g/>
p	p	k?	p
(	(	kIx(	(
<g/>
HD	HD	kA	HD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1080	[number]	k4	1080
<g/>
p	p	k?	p
(	(	kIx(	(
<g/>
HD	HD	kA	HD
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
720	[number]	k4	720
<g/>
p	p	k?	p
možnost	možnost	k1gFnSc1	možnost
přidat	přidat	k5eAaPmF	přidat
videa	video	k1gNnSc2	video
s	s	k7c7	s
60	[number]	k4	60
<g/>
FPS	FPS	kA	FPS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
YouTube	YouTub	k1gInSc5	YouTub
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
nahrát	nahrát	k5eAaBmF	nahrát
videa	video	k1gNnPc4	video
v	v	k7c6	v
4	[number]	k4	4
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
4096	[number]	k4	4096
<g/>
×	×	k?	×
<g/>
3072	[number]	k4	3072
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
možnost	možnost	k1gFnSc1	možnost
nahrát	nahrát	k5eAaBmF	nahrát
videa	video	k1gNnSc2	video
v	v	k7c6	v
8	[number]	k4	8
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
7680	[number]	k4	7680
<g/>
×	×	k?	×
<g/>
4320	[number]	k4	4320
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Peter	Peter	k1gMnSc1	Peter
Bradshaw	Bradshaw	k1gMnSc1	Bradshaw
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatelé	uživatel	k1gMnPc1	uživatel
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
nahrávat	nahrávat	k5eAaImF	nahrávat
3D	[number]	k4	3D
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
YouTube	YouTub	k1gInSc5	YouTub
podporuje	podporovat	k5eAaImIp3nS	podporovat
také	také	k9	také
360	[number]	k4	360
stupňová	stupňový	k2eAgNnPc1d1	stupňové
videa	video	k1gNnPc1	video
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
video	video	k1gNnSc4	video
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
pouhým	pouhý	k2eAgNnSc7d1	pouhé
kliknutím	kliknutí	k1gNnSc7	kliknutí
a	a	k8xC	a
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mobilní	mobilní	k2eAgFnSc1d1	mobilní
verze	verze	k1gFnSc1	verze
otáčením	otáčení	k1gNnSc7	otáčení
mobilu	mobil	k1gInSc2	mobil
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
již	již	k6eAd1	již
YouTube	YouTub	k1gInSc5	YouTub
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
Ellacoya	Ellacoyum	k1gNnSc2	Ellacoyum
Networks	Networksa	k1gFnPc2	Networksa
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgFnPc4d1	vyrábějící
analyzéry	analyzéra	k1gFnPc4	analyzéra
síťového	síťový	k2eAgInSc2d1	síťový
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
YouTube	YouTub	k1gInSc5	YouTub
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
internetovém	internetový	k2eAgInSc6d1	internetový
provozu	provoz	k1gInSc6	provoz
10	[number]	k4	10
%	%	kIx~	%
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
18	[number]	k4	18
%	%	kIx~	%
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
streamované	streamovaný	k2eAgNnSc4d1	streamované
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
videem	video	k1gNnSc7	video
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
nejvíce	hodně	k6eAd3	hodně
zhlédnutí	zhlédnutí	k1gNnSc1	zhlédnutí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Gangnam	Gangnam	k1gInSc1	Gangnam
Style	styl	k1gInSc5	styl
od	od	k7c2	od
jihokorejského	jihokorejský	k2eAgNnSc2d1	jihokorejské
rappera	rappero	k1gNnSc2	rappero
PSY	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
den	den	k1gInSc4	den
konspirátory	konspirátor	k1gMnPc4	konspirátor
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
konce	konec	k1gInSc2	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
21	[number]	k4	21
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hranice	hranice	k1gFnSc2	hranice
1	[number]	k4	1
miliardy	miliarda	k4xCgFnPc4	miliarda
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
2,728	[number]	k4	2,728
miliardy	miliarda	k4xCgFnPc4	miliarda
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nejsledovanějším	sledovaný	k2eAgNnSc7d3	nejsledovanější
videem	video	k1gNnSc7	video
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
Youtube	Youtub	k1gInSc5	Youtub
je	on	k3xPp3gFnPc4	on
překvapivě	překvapivě	k6eAd1	překvapivě
"	"	kIx"	"
<g/>
Mňam	mňam	k0	mňam
Mňam	mňam	k0	mňam
bobík	bobík	k1gInSc1	bobík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
klip	klip	k1gInSc4	klip
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
pouštějí	pouštět	k5eAaImIp3nP	pouštět
malé	malý	k2eAgFnPc1d1	malá
děti	dítě	k1gFnPc1	dítě
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většina	k1gFnSc7	většina
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
pak	pak	k6eAd1	pak
roste	růst	k5eAaImIp3nS	růst
rychlým	rychlý	k2eAgNnSc7d1	rychlé
tempem	tempo	k1gNnSc7	tempo
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
(	(	kIx(	(
<g/>
k	k	k7c3	k
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
37,7	[number]	k4	37,7
milionu	milion	k4xCgInSc2	milion
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
.	.	kIx.	.
</s>
