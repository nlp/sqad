<s>
Poleň	Poleň	k1gFnSc1
</s>
<s>
Poleň	Poleň	k1gFnSc1
obec	obec	k1gFnSc1
Poleň	Poleň	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0322	CZ0322	k4
541788	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Klatovy	Klatovy	k1gInPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klatovy	Klatovy	k1gInPc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
322	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Plzeňský	plzeňský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
32	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
282	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
17,65	17,65	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
458	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
339	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
5	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
5	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
5	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Poleň	Poleň	k1gFnSc1
4733901	#num#	k4
Klatovy	Klatovy	k1gInPc1
1	#num#	k4
ou@polen.jz.cz	ou@polen.jz.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Lidinský	Lidinský	k2eAgMnSc1d1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.polen.cz	www.polen.cz	k1gInSc1
</s>
<s>
Poleň	Poleň	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Poleň	Poleň	k1gFnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Polyně	Polyně	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Klatovy	Klatovy	k1gInPc1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Plzeňský	plzeňský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
je	být	k5eAaImIp3nS
evidováno	evidovat	k5eAaImNgNnS
pět	pět	k4xCc1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
žije	žít	k5eAaImIp3nS
282	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
není	být	k5eNaImIp3nS
evidována	evidován	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
ulice	ulice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
adresy	adresa	k1gFnPc1
v	v	k7c6
obci	obec	k1gFnSc6
mají	mít	k5eAaImIp3nP
PSČ	PSČ	kA
339	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Ves	ves	k1gFnSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
výšce	výška	k1gFnSc6
458	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
části	část	k1gFnSc2
v	v	k7c6
údolí	údolí	k1gNnSc6
potoka	potok	k1gInSc2
Poleňky	Poleňka	k1gFnSc2
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
kostela	kostel	k1gInSc2
a	a	k8xC
návsi	náves	k1gFnSc2
<g/>
,	,	kIx,
však	však	k9
pokrývá	pokrývat	k5eAaImIp3nS
nevelkou	velký	k2eNgFnSc4d1
hůrku	hůrka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
východní	východní	k2eAgFnSc2d1
a	a	k8xC
jižní	jižní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
chráněna	chráněn	k2eAgFnSc1d1
pohořím	pohořet	k5eAaPmIp1nS
Bítovy	Bítov	k1gInPc1
<g/>
,	,	kIx,
s	s	k7c7
vrchy	vrch	k1gInPc4
Velký	velký	k2eAgInSc1d1
Bítov	Bítov	k1gInSc1
(	(	kIx(
<g/>
713	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Malý	malý	k2eAgInSc1d1
Bítov	Bítov	k1gInSc1
(	(	kIx(
<g/>
668	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Doubrava	Doubrava	k1gMnSc1
(	(	kIx(
<g/>
727	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1245	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listině	listina	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
královna	královna	k1gFnSc1
Kunhuta	Kunhuta	k1gFnSc1
prodává	prodávat	k5eAaImIp3nS
Přeštický	přeštický	k2eAgInSc1d1
újezd	újezd	k1gInSc1
kladrubskému	kladrubský	k2eAgInSc3d1
klášteru	klášter	k1gInSc3
<g/>
,	,	kIx,
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c7
svědky	svědek	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
tento	tento	k3xDgInSc4
újezd	újezd	k1gInSc4
obcházeli	obcházet	k5eAaImAgMnP
a	a	k8xC
hranice	hranice	k1gFnPc4
jeho	jeho	k3xOp3gFnPc4
vymezovali	vymezovat	k5eAaImAgMnP
<g/>
,	,	kIx,
Blažej	Blažej	k1gMnSc1
z	z	k7c2
Poleně	poleno	k1gNnSc6
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Budivoje	Budivoj	k1gInSc2
ze	z	k7c2
Švihova	Švihův	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgInS
vystavěn	vystavět	k5eAaPmNgInS
nedaleko	nedaleko	k7c2
Poleně	poleno	k1gNnSc6
hrad	hrad	k1gInSc1
Pušperk	Pušperk	k1gInSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
původně	původně	k6eAd1
Fuchsberg	Fuchsberg	k1gInSc4
<g/>
,	,	kIx,
sdílela	sdílet	k5eAaImAgFnS
Poleň	Poleň	k1gFnSc1
po	po	k7c6
staletí	staletí	k1gNnSc6
osudy	osud	k1gInPc4
tohoto	tento	k3xDgInSc2
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
částech	část	k1gFnPc6
obce	obec	k1gFnSc2
Poleň	Poleň	k1gFnSc4
</s>
<s>
RokPoleňPoleňkaMlýnecZdeslav	RokPoleňPoleňkaMlýnecZdeslav	k1gMnSc1
</s>
<s>
194840051123102	#num#	k4
</s>
<s>
1970254577492	#num#	k4
</s>
<s>
1980168425981	#num#	k4
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
bylo	být	k5eAaImAgNnS
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
k	k	k7c3
trvalému	trvalý	k2eAgInSc3d1
pobytu	pobyt	k1gInSc3
(	(	kIx(
<g/>
nebo	nebo	k8xC
jakémukoliv	jakýkoliv	k3yIgInSc3
platnému	platný	k2eAgInSc3d1
pobytu	pobyt	k1gInSc3
cizince	cizinec	k1gMnSc2
<g/>
,	,	kIx,
azylanta	azylant	k1gMnSc2
<g/>
)	)	kIx)
přihlášeno	přihlásit	k5eAaPmNgNnS
300	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
133	#num#	k4
mužů	muž	k1gMnPc2
nad	nad	k7c4
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
21	#num#	k4
chlapců	chlapec	k1gMnPc2
do	do	k7c2
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
127	#num#	k4
žen	žena	k1gFnPc2
nad	nad	k7c4
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
19	#num#	k4
dívek	dívka	k1gFnPc2
do	do	k7c2
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Poleni	Polen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Markéty	Markéta	k1gFnSc2
<g/>
,	,	kIx,
zřícenina	zřícenina	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
něm	on	k3xPp3gInSc6
oltářní	oltářní	k2eAgInPc1d1
obraz	obraz	k1gInSc1
Madony	Madona	k1gFnSc2
s	s	k7c7
dítětem	dítě	k1gNnSc7
</s>
<s>
Kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Salvátora	Salvátor	k1gMnSc2
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
Pasička	Pasička	k1gFnSc1
</s>
<s>
Fara	fara	k1gFnSc1
</s>
<s>
Poleňská	Poleňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dobrovského	Dobrovského	k2eAgFnSc1d1
<g/>
)	)	kIx)
lípa	lípa	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Markéty	Markéta	k1gFnSc2
</s>
<s>
Zřícenina	zřícenina	k1gFnSc1
kostela	kostel	k1gInSc2
sv.	sv.	kA
Markéty	Markéta	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
měl	mít	k5eAaImAgInS
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
svoji	svůj	k3xOyFgFnSc4
správu	správa	k1gFnSc4
<g/>
.	.	kIx.
i	i	k9
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
pod	pod	k7c7
patronátem	patronát	k1gInSc7
držitele	držitel	k1gMnSc2
Poleně	poleno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgNnSc6d1
století	století	k1gNnSc6
již	již	k6eAd1
nebyl	být	k5eNaImAgInS
osazován	osazovat	k5eAaImNgInS
a	a	k8xC
sloužil	sloužit	k5eAaImAgInS
jako	jako	k8xS,k8xC
hřbitovní	hřbitovní	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císařem	Císař	k1gMnSc7
Josefem	Josef	k1gMnSc7
roku	rok	k1gInSc2
1786	#num#	k4
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
vlastní	vlastnit	k5eAaImIp3nS
zkáze	zkáza	k1gFnSc3
ponechán	ponechán	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
v	v	k7c6
něm	on	k3xPp3gInSc6
tři	tři	k4xCgInPc4
oltáře	oltář	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
střechou	střecha	k1gFnSc7
byla	být	k5eAaImAgFnS
jednoduchá	jednoduchý	k2eAgFnSc1d1
zvonička	zvonička	k1gFnSc1
se	s	k7c7
zvoncem	zvonec	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
podle	podle	k7c2
báje	báj	k1gFnSc2
sám	sám	k3xTgMnSc1
v	v	k7c6
době	doba	k1gFnSc6
nebezpečí	nebezpečí	k1gNnSc2
rozezvučel	rozezvučet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Oba	dva	k4xCgInPc4
kostely	kostel	k1gInPc4
obepínal	obepínat	k5eAaImAgInS
hřbitov	hřbitov	k1gInSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vchod	vchod	k1gInSc1
do	do	k7c2
nich	on	k3xPp3gMnPc2
vedl	vést	k5eAaImAgInS
po	po	k7c6
něm.	něm.	k?
K	k	k7c3
hřbitovu	hřbitov	k1gInSc3
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
pověst	pověst	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
pohanští	pohanský	k2eAgMnPc1d1
předkové	předek	k1gMnPc1
obětovali	obětovat	k5eAaBmAgMnP
na	na	k7c6
dubovém	dubový	k2eAgInSc6d1
pařezu	pařez	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
vystavění	vystavění	k1gNnSc6
kostela	kostel	k1gInSc2
sv.	sv.	kA
Markéty	Markéta	k1gFnSc2
zazděn	zazdít	k5eAaPmNgInS
do	do	k7c2
oltáře	oltář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Markéty	Markéta	k1gFnSc2
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgInS
zvon	zvon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
zvonil	zvonit	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
od	od	k7c2
sebe	sebe	k3xPyFc4
vždy	vždy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
obci	obec	k1gFnSc3
hrozilo	hrozit	k5eAaImAgNnS
neštěstí	neštěstí	k1gNnSc1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
mor	mor	k1gInSc1
<g/>
,	,	kIx,
sucho	sucho	k1gNnSc1
či	či	k8xC
bouře	bouře	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposled	naposled	k6eAd1
takto	takto	k6eAd1
zvonil	zvonit	k5eAaImAgMnS
při	při	k7c6
vpádu	vpád	k1gInSc6
z	z	k7c2
Horní	horní	k2eAgFnSc2d1
Falce	Falc	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyhořely	vyhořet	k5eAaPmAgInP
Klatovy	Klatovy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
přitom	přitom	k6eAd1
vždy	vždy	k6eAd1
zvonil	zvonit	k5eAaImAgInS
přesmutným	přesmutný	k2eAgInSc7d1
hlasem	hlas	k1gInSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
později	pozdě	k6eAd2
sňat	sňat	k2eAgMnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
lid	lid	k1gInSc1
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
hlásku	hlásek	k1gInSc2
neděsil	děsit	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
</s>
<s>
Původně	původně	k6eAd1
zasvěcený	zasvěcený	k2eAgMnSc1d1
Matce	matka	k1gFnSc3
Boží	boží	k2eAgFnSc6d1
<g/>
,	,	kIx,
připomíná	připomínat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
farní	farní	k2eAgInSc1d1
roku	rok	k1gInSc3
1355	#num#	k4
<g/>
,	,	kIx,
jsa	být	k5eAaImSgMnS
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
pod	pod	k7c7
patronátem	patronát	k1gInSc7
držitelů	držitel	k1gMnPc2
Pušperka	Pušperka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
svědčí	svědčit	k5eAaImIp3nS
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
doznala	doznat	k5eAaPmAgFnS
však	však	k9
opravami	oprava	k1gFnPc7
mnohých	mnohý	k2eAgFnPc2d1
změn	změna	k1gFnPc2
zejména	zejména	k9
v	v	k7c4
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
roku	rok	k1gInSc2
1739	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
kostelem	kostel	k1gInSc7
stojí	stát	k5eAaImIp3nS
poleňská	poleňský	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
nejcennější	cenný	k2eAgNnSc1d3
je	být	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
obraz	obraz	k1gInSc4
Madona	Madona	k1gFnSc1
s	s	k7c7
dítětem	dítě	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
olejová	olejový	k2eAgFnSc1d1
malba	malba	k1gFnSc1
na	na	k7c6
dřevěné	dřevěný	k2eAgFnSc6d1
desce	deska	k1gFnSc6
<g/>
,	,	kIx,
mistrné	mistrný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Lombardské	lombardský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
z	z	k7c2
počátku	počátek	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
byl	být	k5eAaImAgInS
přinesen	přinést	k5eAaPmNgInS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
vojínem	vojín	k1gMnSc7
z	z	k7c2
Itálie	Itálie	k1gFnSc2
do	do	k7c2
blízkého	blízký	k2eAgInSc2d1
Kobzova	Kobzův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
byl	být	k5eAaImAgMnS
kostelu	kostel	k1gInSc3
věnován	věnován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Obraz	obraz	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roku	rok	k1gInSc6
1936	#num#	k4
poslán	poslat	k5eAaPmNgInS
do	do	k7c2
Prahy	Praha	k1gFnSc2
k	k	k7c3
pořízení	pořízení	k1gNnSc3
barevné	barevný	k2eAgFnSc2d1
reprodukce	reprodukce	k1gFnSc2
a	a	k8xC
tu	tu	k6eAd1
odborníci	odborník	k1gMnPc1
vyslovili	vyslovit	k5eAaPmAgMnP
domněnku	domněnka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c6
dílo	dílo	k1gNnSc1
Lukase	Lukasa	k1gFnSc6
Cranacha	Cranach	k1gMnSc2
st.	st.	kA
Srovnání	srovnání	k1gNnPc4
poleňské	poleňský	k2eAgFnSc2d1
Madony	Madona	k1gFnSc2
s	s	k7c7
mistrovými	mistrův	k2eAgInPc7d1
díly	díl	k1gInPc7
doby	doba	k1gFnSc2
okolo	okolo	k7c2
roku	rok	k1gInSc2
1520	#num#	k4
přesvědčí	přesvědčit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tu	tu	k6eAd1
jde	jít	k5eAaImIp3nS
i	i	k9
o	o	k7c4
dílo	dílo	k1gNnSc4
nejvyšší	vysoký	k2eAgFnSc2d3
jakosti	jakost	k1gFnSc2
umělecké	umělecký	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
původu	původ	k1gInSc3
poleňské	poleňský	k2eAgFnSc2d1
Madony	Madona	k1gFnSc2
vypráví	vyprávět	k5eAaImIp3nS
poleňský	poleňský	k2eAgMnSc1d1
písmák	písmák	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Kotáb	Kotáb	k1gMnSc1
<g/>
,	,	kIx,
dlouholetý	dlouholetý	k2eAgMnSc1d1
kronikář	kronikář	k1gMnSc1
<g/>
,	,	kIx,
následující	následující	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
podklad	podklad	k1gInSc4
jednak	jednak	k8xC
v	v	k7c6
záznamech	záznam	k1gInPc6
kronik	kronika	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
ji	on	k3xPp3gFnSc4
slyšel	slyšet	k5eAaImAgMnS
vykládat	vykládat	k5eAaImF
od	od	k7c2
starých	starý	k2eAgInPc2d1
pamětníků	pamětník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
těchto	tento	k3xDgFnPc2
pověstí	pověst	k1gFnPc2
byl	být	k5eAaImAgInS
obraz	obraz	k1gInSc1
za	za	k7c4
válek	válek	k1gInSc4
při	při	k7c6
tažení	tažení	k1gNnSc6
vojska	vojsko	k1gNnSc2
zachráněn	zachráněn	k2eAgInSc4d1
z	z	k7c2
hořícího	hořící	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezdá	zdát	k5eNaPmIp3nS,k5eNaImIp3nS
se	se	k3xPyFc4
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
obraz	obraz	k1gInSc1
rozměrů	rozměr	k1gInPc2
87	#num#	k4
x	x	k?
57	#num#	k4
cm	cm	kA
na	na	k7c6
dřevěné	dřevěný	k2eAgFnSc6d1
desce	deska	k1gFnSc6
nosil	nosit	k5eAaImAgMnS
nějaký	nějaký	k3yIgMnSc1
voják	voják	k1gMnSc1
v	v	k7c6
tornistře	tornistra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snad	snad	k9
byl	být	k5eAaImAgInS
v	v	k7c6
držení	držení	k1gNnSc6
pluku	pluk	k1gInSc2
jako	jako	k8xC,k8xS
plukovní	plukovní	k2eAgInSc4d1
amulet	amulet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
válek	válka	k1gFnPc2
se	se	k3xPyFc4
vojska	vojsko	k1gNnPc1
rozpadávala	rozpadávat	k5eAaImAgNnP
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
vraceli	vracet	k5eAaImAgMnP
do	do	k7c2
svých	svůj	k3xOyFgInPc2
domovů	domov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
takovém	takový	k3xDgInSc6
cestování	cestování	k1gNnSc6
byl	být	k5eAaImAgInS
ponechán	ponechat	k5eAaPmNgInS
velmi	velmi	k6eAd1
nemocný	mocný	k2eNgMnSc1d1,k2eAgMnSc1d1
voják	voják	k1gMnSc1
v	v	k7c6
Kobzově	Kobzův	k2eAgInSc6d1
mlýně	mlýn	k1gInSc6
(	(	kIx(
<g/>
dodnes	dodnes	k6eAd1
zachovaná	zachovaný	k2eAgFnSc1d1
usedlost	usedlost	k1gFnSc1
čp.	čp.	k?
1	#num#	k4
v	v	k7c6
obci	obec	k1gFnSc6
Mlýnec	mlýnec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vojína	vojín	k1gMnSc2
ujala	ujmout	k5eAaPmAgFnS
a	a	k8xC
ošetřovala	ošetřovat	k5eAaImAgFnS
ho	on	k3xPp3gInSc4
mlynářka	mlynářka	k1gFnSc1
Kobzová	Kobzová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojín	vojín	k1gMnSc1
s	s	k7c7
sebou	se	k3xPyFc7
přinesl	přinést	k5eAaPmAgMnS
do	do	k7c2
mlýna	mlýn	k1gInSc2
zmíněný	zmíněný	k2eAgInSc4d1
obraz	obraz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pověsti	pověst	k1gFnSc2
není	být	k5eNaImIp3nS
patrno	patrn	k2eAgNnSc1d1
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
nemocí	nemoc	k1gFnSc7
voják	voják	k1gMnSc1
trpěl	trpět	k5eAaImAgMnS
<g/>
,	,	kIx,
jaké	jaký	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
byl	být	k5eAaImAgInS
národnosti	národnost	k1gFnPc4
<g/>
,	,	kIx,
zda	zda	k8xS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgMnS
důstojník	důstojník	k1gMnSc1
nebo	nebo	k8xC
prostý	prostý	k2eAgMnSc1d1
voják	voják	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prý	prý	k9
se	se	k3xPyFc4
záhy	záhy	k6eAd1
zotavil	zotavit	k5eAaPmAgMnS
a	a	k8xC
jako	jako	k9
odměnu	odměna	k1gFnSc4
Kobzové	Kobzová	k1gFnSc2
obraz	obraz	k1gInSc1
ponechal	ponechat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kobzová	Kobzová	k1gFnSc1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
pobožná	pobožný	k2eAgFnSc1d1
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
obraz	obraz	k1gInSc4
doma	doma	k6eAd1
zavěšený	zavěšený	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
snech	sen	k1gInPc6
mívala	mívat	k5eAaImAgFnS
vidění	vidění	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
obraz	obraz	k1gInSc1
na	na	k7c4
zdi	zeď	k1gFnPc4
houpá	houpat	k5eAaImIp3nS
a	a	k8xC
že	že	k8xS
Madona	Madona	k1gFnSc1
prosí	prosit	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
kostelu	kostel	k1gInSc3
byl	být	k5eAaImAgInS
darován	darován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těchto	tento	k3xDgNnPc2
vidění	vidění	k1gNnSc6
Kobzová	Kobzová	k1gFnSc1
uposlechla	uposlechnout	k5eAaPmAgFnS
a	a	k8xC
na	na	k7c4
faru	fara	k1gFnSc4
poleňskou	poleňský	k2eAgFnSc4d1
obraz	obraz	k1gInSc4
donesla	donést	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Obraz	obraz	k1gInSc1
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
kostele	kostel	k1gInSc6
po	po	k7c4
dobu	doba	k1gFnSc4
delší	dlouhý	k2eAgFnSc4d2
padesáti	padesát	k4xCc2
let	léto	k1gNnPc2
jen	jen	k9
o	o	k7c4
oltář	oltář	k1gInSc4
opřen	opřen	k2eAgInSc4d1
a	a	k8xC
nijak	nijak	k6eAd1
nezabudován	zabudován	k2eNgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
tehdejší	tehdejší	k2eAgMnPc1d1
faráři	farář	k1gMnPc1
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
vzhledem	vzhledem	k7c3
k	k	k7c3
původu	původ	k1gInSc3
obrazu	obraz	k1gInSc2
<g/>
,	,	kIx,
nechtěli	chtít	k5eNaImAgMnP
na	na	k7c4
tento	tento	k3xDgInSc4
obraz	obraz	k1gInSc4
jako	jako	k8xC,k8xS
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
nebo	nebo	k8xC
majetek	majetek	k1gInSc4
církevní	církevní	k2eAgInSc4d1
hleděti	hledět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
pozdější	pozdní	k2eAgMnSc1d2
byl	být	k5eAaImAgInS
na	na	k7c6
oltáři	oltář	k1gInSc6
důstojně	důstojně	k6eAd1
umístěn	umístit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bočním	boční	k2eAgInSc6d1
„	„	k?
<g/>
Mariánském	mariánský	k2eAgInSc6d1
<g/>
“	“	k?
oltáři	oltář	k1gInSc6
byl	být	k5eAaImAgMnS
umístěn	umístit	k5eAaPmNgMnS
až	až	k6eAd1
do	do	k7c2
září	září	k1gNnSc2
1967	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
noci	noc	k1gFnSc6
ze	z	k7c2
zamčeného	zamčený	k2eAgInSc2d1
kostela	kostel	k1gInSc2
odcizen	odcizen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc7d1
náhodou	náhoda	k1gFnSc7
byl	být	k5eAaImAgInS
nalezen	nalézt	k5eAaBmNgInS,k5eAaPmNgInS
v	v	k7c6
červnu	červen	k1gInSc6
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gInSc4
tým	tým	k1gInSc4
zlodějů	zloděj	k1gMnPc2
měl	mít	k5eAaImAgMnS
připravený	připravený	k2eAgMnSc1d1
k	k	k7c3
vyvezení	vyvezení	k1gNnSc3
do	do	k7c2
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poleňském	poleňský	k2eAgInSc6d1
kostele	kostel	k1gInSc6
je	být	k5eAaImIp3nS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
umístěna	umístěn	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
,	,	kIx,
originál	originál	k1gInSc1
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Národní	národní	k2eAgFnSc6d1
galerii	galerie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
Pasička	Pasička	k1gFnSc1
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
stával	stávat	k5eAaImAgInS
dříve	dříve	k6eAd2
(	(	kIx(
<g/>
v	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
snad	snad	k9
i	i	k9
dávno	dávno	k6eAd1
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
)	)	kIx)
po	po	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
mlýnského	mlýnský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obydlí	obydlí	k1gNnSc1
v	v	k7c6
něm	on	k3xPp3gMnSc6
nebylo	být	k5eNaImAgNnS
<g/>
,	,	kIx,
mlynář	mlynář	k1gMnSc1
bydlel	bydlet	k5eAaImAgMnS
v	v	k7c6
kamenné	kamenný	k2eAgFnSc6d1
chalupě	chalupa	k1gFnSc6
na	na	k7c6
stráni	stráň	k1gFnSc6
nad	nad	k7c7
mlýnem	mlýn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1660	#num#	k4
mlýn	mlýn	k1gInSc1
zpustl	zpustnout	k5eAaPmAgInS
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
ho	on	k3xPp3gMnSc4
zapálili	zapálit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1699	#num#	k4
počala	počnout	k5eAaPmAgFnS
poleňská	poleňský	k2eAgFnSc1d1
vrchnost	vrchnost	k1gFnSc1
budovat	budovat	k5eAaImF
nový	nový	k2eAgInSc4d1
mlýn	mlýn	k1gInSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
už	už	k9
i	i	k9
s	s	k7c7
obydlím	obydlí	k1gNnSc7
<g/>
,	,	kIx,
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
třech	tři	k4xCgNnPc2
století	století	k1gNnPc2
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
deset	deset	k4xCc1
majitelů	majitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
něm	on	k3xPp3gNnSc6
žili	žít	k5eAaImAgMnP
Štolbovi	Štolbův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejich	jejich	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
sloužil	sloužit	k5eAaImAgMnS
jejich	jejich	k3xOp3gFnPc3
dcerám	dcera	k1gFnPc3
a	a	k8xC
vnučce	vnučka	k1gFnSc3
s	s	k7c7
rodinou	rodina	k1gFnSc7
k	k	k7c3
rekreaci	rekreace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpoledne	odpoledne	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1984	#num#	k4
při	při	k7c6
vypalování	vypalování	k1gNnSc6
suché	suchý	k2eAgFnSc2d1
trávy	tráva	k1gFnSc2
mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
údajně	údajně	k6eAd1
chodil	chodit	k5eAaImAgMnS
i	i	k9
„	„	k?
<g/>
modrý	modrý	k2eAgMnSc1d1
abbé	abbé	k1gMnSc1
<g/>
“	“	k?
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
shořel	shořet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jednom	jeden	k4xCgNnSc6
z	z	k7c2
nejstarších	starý	k2eAgNnPc2d3
stavení	stavení	k1gNnPc2
v	v	k7c6
obci	obec	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
i	i	k9
v	v	k7c6
evidenci	evidence	k1gFnSc6
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgFnP
jen	jen	k9
zčernalé	zčernalý	k2eAgFnPc1d1
zdi	zeď	k1gFnPc1
a	a	k8xC
komín	komín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Škola	škola	k1gFnSc1
v	v	k7c6
Poleni	Polen	k2eAgMnPc1d1
</s>
<s>
Původní	původní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
všude	všude	k6eAd1
bývalo	bývat	k5eAaImAgNnS
zvykem	zvyk	k1gInSc7
<g/>
,	,	kIx,
stávala	stávat	k5eAaImAgFnS
v	v	k7c6
nejbližším	blízký	k2eAgNnSc6d3
okolí	okolí	k1gNnSc6
fary	fara	k1gFnSc2
a	a	k8xC
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
stará	starý	k2eAgFnSc1d1
<g/>
“	“	k?
škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
domek	domek	k1gInSc1
č.	č.	k?
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystavěna	vystavěn	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
z	z	k7c2
farního	farní	k2eAgNnSc2d1
dříví	dříví	k1gNnSc2
na	na	k7c6
části	část	k1gFnSc6
farní	farní	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
patrno	patrn	k2eAgNnSc1d1
ještě	ještě	k6eAd1
dnes	dnes	k6eAd1
z	z	k7c2
rozlohy	rozloha	k1gFnSc2
staveniště	staveniště	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
malá	malý	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nemohla	moct	k5eNaImAgFnS
svým	svůj	k3xOyFgInPc3
účelům	účel	k1gInPc3
vyhovovat	vyhovovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byla	být	k5eAaImAgFnS
r.	r.	kA
1790	#num#	k4
prodána	prodat	k5eAaPmNgFnS
za	za	k7c4
100	#num#	k4
zlatých	zlatý	k2eAgNnPc2d1
rýnských	rýnské	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
stavěna	stavit	k5eAaImNgFnS
hned	hned	k6eAd1
nad	nad	k7c7
starou	starý	k2eAgFnSc7d1
školou	škola	k1gFnSc7
rovněž	rovněž	k9
na	na	k7c6
farním	farní	k2eAgInSc6d1
zahradním	zahradní	k2eAgInSc6d1
pozemku	pozemek	k1gInSc6
a	a	k8xC
také	také	k9
z	z	k7c2
farního	farní	k2eAgNnSc2d1
dříví	dříví	k1gNnSc2
nová	nový	k2eAgFnSc1d1
školní	školní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
tak	tak	k6eAd1
velká	velký	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
škola	škola	k1gFnSc1
stará	starý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládala	skládat	k5eAaImAgFnS
se	se	k3xPyFc4
z	z	k7c2
učebny	učebna	k1gFnSc2
se	s	k7c7
4	#num#	k4
okny	okno	k1gNnPc7
<g/>
,	,	kIx,
malého	malý	k2eAgInSc2d1
bytu	byt	k1gInSc2
učitelova	učitelův	k2eAgInSc2d1
<g/>
,	,	kIx,
chlévku	chlévek	k1gInSc2
kravského	kravský	k2eAgMnSc2d1
a	a	k8xC
stodůlky	stodůlka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvorek	dvorek	k1gInSc1
neměla	mít	k5eNaImAgFnS
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
divu	div	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
koncem	koncem	k7c2
roku	rok	k1gInSc2
1790	#num#	k4
byla	být	k5eAaImAgFnS
uznána	uznat	k5eAaPmNgFnS
nepostačitelnou	postačitelný	k2eNgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dítky	dítko	k1gNnPc7
vyučovány	vyučován	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
střídavě	střídavě	k6eAd1
<g/>
,	,	kIx,
malé	malý	k2eAgNnSc4d1
odpoledne	odpoledne	k1gNnSc4
<g/>
,	,	kIx,
větší	veliký	k2eAgNnSc4d2
dopoledne	dopoledne	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sumáři	sumář	k1gInSc6
dětí	dítě	k1gFnPc2
triviální	triviální	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Poleni	Polen	k2eAgMnPc1d1
roku	rok	k1gInSc2
1820	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
348	#num#	k4
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
uznáno	uznat	k5eAaPmNgNnS
za	za	k7c4
nutné	nutný	k2eAgNnSc4d1
opatřit	opatřit	k5eAaPmF
druhou	druhý	k4xOgFnSc4
učebnu	učebna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
vyhlédnuta	vyhlédnut	k2eAgFnSc1d1
v	v	k7c6
domku	domek	k1gInSc6
č.	č.	k?
51	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
není	být	k5eNaImIp3nS
mnoho	mnoho	k6eAd1
vzdálen	vzdálit	k5eAaPmNgMnS
od	od	k7c2
staré	starý	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1855	#num#	k4
bylo	být	k5eAaImAgNnS
započato	započat	k2eAgNnSc1d1
se	s	k7c7
stavbou	stavba	k1gFnSc7
nové	nový	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ne	ne	k9
na	na	k7c6
starém	starý	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
či	či	k8xC
na	na	k7c6
zříceninách	zřícenina	k1gFnPc6
kostela	kostel	k1gInSc2
sv.	sv.	kA
Markéty	Markéta	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
projektováno	projektovat	k5eAaBmNgNnS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
na	na	k7c6
stráni	stráň	k1gFnSc6
pod	pod	k7c7
farou	fara	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
byla	být	k5eAaImAgFnS
do	do	k7c2
ní	on	k3xPp3gFnSc2
převedena	převést	k5eAaPmNgFnS
mládež	mládež	k1gFnSc1
ze	z	k7c2
starých	starý	k2eAgFnPc2d1
školních	školní	k2eAgFnPc2d1
místností	místnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
svému	svůj	k3xOyFgInSc3
účelu	účel	k1gInSc3
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
malý	malý	k2eAgInSc4d1
počet	počet	k1gInSc4
dětí	dítě	k1gFnPc2
byla	být	k5eAaImAgFnS
definitivně	definitivně	k6eAd1
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
všechny	všechen	k3xTgFnPc1
děti	dítě	k1gFnPc1
navštěvovaly	navštěvovat	k5eAaImAgFnP
školu	škola	k1gFnSc4
v	v	k7c6
Chudenicích	Chudenice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Poleňský	Poleňský	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
</s>
<s>
Na	na	k7c6
poleňském	poleňský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
náhrobek	náhrobek	k1gInSc1
národního	národní	k2eAgMnSc2d1
buditele	buditel	k1gMnSc2
a	a	k8xC
obrozence	obrozenec	k1gMnSc2
<g/>
,	,	kIx,
filologa	filolog	k1gMnSc2
<g/>
,	,	kIx,
lexikografa	lexikograf	k1gMnSc2
<g/>
,	,	kIx,
spisovatele	spisovatel	k1gMnSc2
<g/>
,	,	kIx,
pedagoga	pedagog	k1gMnSc2
<g/>
,	,	kIx,
básníka	básník	k1gMnSc2
a	a	k8xC
překladatele	překladatel	k1gMnSc2
Josefa	Josef	k1gMnSc2
Franty	Franta	k1gMnSc2
Šumavského	šumavský	k2eAgInSc2d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1796	#num#	k4
Poleň	Poleň	k1gFnSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1857	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Poleň	Poleň	k1gFnSc1
</s>
<s>
Mlýnec	mlýnec	k1gInSc1
</s>
<s>
Poleňka	Poleňka	k1gFnSc1
</s>
<s>
Pušperk	Pušperk	k1gInSc1
</s>
<s>
Zdeslav	Zdeslav	k1gMnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Zřícenina	zřícenina	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
obec	obec	k1gFnSc4
</s>
<s>
Místní	místní	k2eAgInPc1d1
domy	dům	k1gInPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresy	adresa	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HOSTAŠ	HOSTAŠ	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
KOTÁB	KOTÁB	kA
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
(	(	kIx(
<g/>
edd	edd	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pamětní	pamětní	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
městečka	městečko	k1gNnSc2
Poleně	poleno	k1gNnSc6
<g/>
,	,	kIx,
rukopis	rukopis	k1gInSc4
<g/>
,	,	kIx,
nepublikováno	publikován	k2eNgNnSc4d1
</s>
<s>
ŠORM	Šorm	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověsti	pověst	k1gFnSc2
o	o	k7c6
českých	český	k2eAgInPc6d1
zvonech	zvon	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
V.	V.	kA
Kotrba	kotrba	k1gFnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JIRÁK	Jirák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
OÚ	OÚ	kA
POLEŃ	POLEŃ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poleň	Poleň	k1gFnSc1
<g/>
:	:	kIx,
750	#num#	k4
let	léto	k1gNnPc2
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klatovy	Klatovy	k1gInPc4
<g/>
:	:	kIx,
Arkáda	arkáda	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
Hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
Království	království	k1gNnSc2
českého	český	k2eAgInSc2d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
elektr	elektr	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Čížek	Čížek	k1gMnSc1
-	-	kIx~
ViGo	ViGo	k1gMnSc1
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Poleň	Poleň	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Poleň	Poleň	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
turistik	turistika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Fotogalerie	Fotogalerie	k1gFnSc2
na	na	k7c6
serveru	server	k1gInSc6
Turistik	turistika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
risy	risa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Statistické	statistický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
-	-	kIx~
Regionální	regionální	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
servis	servis	k1gInSc1
</s>
<s>
Poleň	Poleň	k1gFnSc1
<g/>
,	,	kIx,
Czech	Czech	k1gInSc1
-	-	kIx~
Facebook	Facebook	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
Skupina	skupina	k1gFnSc1
obce	obec	k1gFnSc2
Poleň	Poleň	k1gFnSc1
v	v	k7c6
komunitní	komunitní	k2eAgFnSc6d1
síti	síť	k1gFnSc6
Facebook	Facebook	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
Poleň	Poleň	k1gFnSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
MlýnecPoleňPoleňkaPušperkZdeslav	MlýnecPoleňPoleňkaPušperkZdeslav	k1gMnSc1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Klatovy	Klatovy	k1gInPc1
</s>
<s>
Běhařov	Běhařov	k1gInSc1
•	•	k?
Běšiny	Běšina	k1gFnSc2
•	•	k?
Bezděkov	Bezděkov	k1gInSc1
•	•	k?
Biřkov	Biřkov	k1gInSc1
•	•	k?
Bolešiny	Bolešina	k1gFnSc2
•	•	k?
Břežany	Břežany	k1gInPc4
•	•	k?
Budětice	Budětice	k1gFnSc2
•	•	k?
Bukovník	Bukovník	k1gInSc1
•	•	k?
Čachrov	Čachrov	k1gInSc1
•	•	k?
Černíkov	Černíkov	k1gInSc1
•	•	k?
Červené	Červená	k1gFnSc2
Poříčí	Poříčí	k1gNnSc2
•	•	k?
Číhaň	Číhaň	k1gMnSc1
•	•	k?
Čímice	Čímice	k1gFnSc2
•	•	k?
Dešenice	Dešenice	k1gFnSc2
•	•	k?
Dlažov	Dlažov	k1gInSc1
•	•	k?
Dlouhá	Dlouhá	k1gFnSc1
Ves	ves	k1gFnSc1
•	•	k?
Dobršín	Dobršína	k1gFnPc2
•	•	k?
Dolany	Dolany	k1gInPc1
•	•	k?
Domoraz	Domoraz	k1gInSc1
•	•	k?
Dražovice	Dražovice	k1gFnSc1
•	•	k?
Frymburk	Frymburk	k1gInSc4
•	•	k?
Hamry	Hamry	k1gInPc4
•	•	k?
Hartmanice	Hartmanice	k1gFnSc2
•	•	k?
Hejná	Hejný	k2eAgFnSc1d1
•	•	k?
Hlavňovice	Hlavňovice	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Hnačov	Hnačov	k1gInSc1
•	•	k?
Horažďovice	Horažďovice	k1gFnPc4
•	•	k?
Horská	Horská	k1gFnSc1
Kvilda	Kvilda	k1gFnSc1
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Hradešice	Hradešice	k1gFnSc2
•	•	k?
Chanovice	Chanovice	k1gFnSc2
•	•	k?
Chlistov	Chlistov	k1gInSc1
•	•	k?
Chudenice	Chudenice	k1gFnPc4
•	•	k?
Chudenín	Chudenín	k1gMnSc1
•	•	k?
Janovice	Janovice	k1gFnPc4
nad	nad	k7c7
Úhlavou	Úhlava	k1gFnSc7
•	•	k?
Javor	javor	k1gInSc4
•	•	k?
Ježovy	Ježův	k2eAgFnSc2d1
•	•	k?
Kašperské	kašperský	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
•	•	k?
Kejnice	Kejnice	k1gFnSc1
•	•	k?
Klatovy	Klatovy	k1gInPc1
•	•	k?
Klenová	klenový	k2eAgFnSc1d1
•	•	k?
Kolinec	Kolinec	k1gMnSc1
•	•	k?
Kovčín	Kovčín	k1gMnSc1
•	•	k?
Křenice	Křenice	k1gFnSc2
•	•	k?
Kvášňovice	Kvášňovice	k1gFnSc2
•	•	k?
Lomec	Lomec	k1gInSc1
•	•	k?
Malý	malý	k2eAgInSc1d1
Bor	bor	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maňovice	Maňovice	k1gFnSc1
•	•	k?
Měčín	Měčína	k1gFnPc2
•	•	k?
Mezihoří	mezihoří	k1gNnSc4
•	•	k?
Mlýnské	mlýnský	k2eAgNnSc4d1
Struhadlo	struhadlo	k1gNnSc4
•	•	k?
Modrava	Modrava	k1gFnSc1
•	•	k?
Mochtín	Mochtína	k1gFnPc2
•	•	k?
Mokrosuky	Mokrosuk	k1gInPc1
•	•	k?
Myslív	Myslív	k1gInSc1
•	•	k?
Myslovice	Myslovice	k1gFnSc2
•	•	k?
Nalžovské	Nalžovský	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
•	•	k?
Nehodiv	hodit	k5eNaPmDgInS
•	•	k?
Nezamyslice	Nezamyslice	k1gFnPc4
•	•	k?
Nezdice	Nezdice	k1gFnSc2
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
•	•	k?
Nýrsko	Nýrsko	k1gNnSc1
•	•	k?
Obytce	Obytec	k1gInSc2
•	•	k?
Olšany	Olšany	k1gInPc1
•	•	k?
Ostřetice	Ostřetika	k1gFnSc3
•	•	k?
Pačejov	Pačejov	k1gInSc1
•	•	k?
Petrovice	Petrovice	k1gFnSc2
u	u	k7c2
Sušice	Sušice	k1gFnSc2
•	•	k?
Plánice	plánice	k1gFnSc2
•	•	k?
Podmokly	Podmokly	k1gInPc4
•	•	k?
Poleň	Poleň	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Prášily	prášit	k5eAaImAgInP
•	•	k?
Předslav	Předslav	k1gMnSc1
•	•	k?
Rabí	rabí	k1gMnSc1
•	•	k?
Rejštejn	Rejštejn	k1gMnSc1
•	•	k?
Slatina	slatina	k1gFnSc1
•	•	k?
Soběšice	Soběšice	k1gFnSc2
•	•	k?
Srní	srní	k2eAgInSc1d1
•	•	k?
Strašín	Strašín	k1gInSc1
•	•	k?
Strážov	Strážov	k1gInSc1
•	•	k?
Sušice	Sušice	k1gFnSc1
•	•	k?
Svéradice	Svéradice	k1gFnSc2
•	•	k?
Švihov	Švihov	k1gInSc1
•	•	k?
Tužice	Tužice	k1gFnSc2
•	•	k?
Týnec	Týnec	k1gInSc1
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Plánice	plánice	k1gFnSc2
•	•	k?
Velhartice	Velhartice	k1gFnPc4
•	•	k?
Velké	velká	k1gFnSc2
Hydčice	Hydčice	k1gFnSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Bor	bor	k1gInSc1
•	•	k?
Vrhaveč	Vrhaveč	k1gInSc1
•	•	k?
Vřeskovice	Vřeskovice	k1gFnSc1
•	•	k?
Zavlekov	Zavlekov	k1gInSc4
•	•	k?
Zborovy	Zborov	k1gInPc4
•	•	k?
Železná	železný	k2eAgFnSc1d1
Ruda	ruda	k1gFnSc1
•	•	k?
Žihobce	Žihobce	k1gMnSc1
•	•	k?
Žichovice	Žichovice	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
