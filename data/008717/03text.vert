<p>
<s>
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
staré	starý	k2eAgFnSc2d1	stará
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nařídil	nařídit	k5eAaPmAgMnS	nařídit
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
husitského	husitský	k2eAgNnSc2d1	husitské
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
opevnit	opevnit	k5eAaPmF	opevnit
systémem	systém	k1gInSc7	systém
vysokých	vysoký	k2eAgInPc2d1	vysoký
násypů	násyp	k1gInPc2	násyp
a	a	k8xC	a
valů	val	k1gInPc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Dunajské	dunajský	k2eAgFnSc2d1	Dunajská
ulice	ulice	k1gFnSc2	ulice
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
hotel	hotel	k1gInSc1	hotel
Danubia	Danubium	k1gNnSc2	Danubium
Gate	Gat	k1gInSc2	Gat
a	a	k8xC	a
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc3	opevnění
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Dunajské	dunajský	k2eAgFnSc2d1	Dunajská
brány	brána	k1gFnSc2	brána
bylo	být	k5eAaImAgNnS	být
zbouráno	zbourat	k5eAaPmNgNnS	zbourat
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Dunajské	dunajský	k2eAgFnSc2d1	Dunajská
brány	brána	k1gFnSc2	brána
stál	stát	k5eAaImAgInS	stát
městský	městský	k2eAgInSc1d1	městský
lazaret	lazaret	k1gInSc1	lazaret
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
Hřbitov	hřbitov	k1gInSc1	hřbitov
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
250	[number]	k4	250
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
veřejné	veřejný	k2eAgFnPc1d1	veřejná
městské	městský	k2eAgFnPc1d1	městská
studny	studna	k1gFnPc1	studna
<g/>
.	.	kIx.	.
</s>
<s>
Nedochovaly	dochovat	k5eNaPmAgInP	dochovat
se	se	k3xPyFc4	se
žádné	žádný	k3yNgInPc1	žádný
obrázky	obrázek	k1gInPc1	obrázek
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
archivu	archiv	k1gInSc6	archiv
je	být	k5eAaImIp3nS	být
mapka	mapka	k1gFnSc1	mapka
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
brána	brána	k1gFnSc1	brána
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
poangličtěného	poangličtěný	k2eAgInSc2d1	poangličtěný
názvu	název	k1gInSc2	název
hotelu	hotel	k1gInSc2	hotel
"	"	kIx"	"
<g/>
Danubia	Danubia	k1gFnSc1	Danubia
gate	gat	k1gFnSc2	gat
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
už	už	k6eAd1	už
nic	nic	k3yNnSc1	nic
nepřipomíná	připomínat	k5eNaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
brána	brána	k1gFnSc1	brána
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
