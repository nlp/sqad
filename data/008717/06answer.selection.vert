<s>
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
staré	starý	k2eAgFnSc2d1	stará
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nařídil	nařídit	k5eAaPmAgMnS	nařídit
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
husitského	husitský	k2eAgNnSc2d1	husitské
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
opevnit	opevnit	k5eAaPmF	opevnit
systémem	systém	k1gInSc7	systém
vysokých	vysoký	k2eAgInPc2d1	vysoký
násypů	násyp	k1gInPc2	násyp
a	a	k8xC	a
valů	val	k1gInPc2	val
<g/>
.	.	kIx.	.
</s>
