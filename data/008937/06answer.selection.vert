<s>
Alabama	Alabama	k1gFnSc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ˌ	ˌ	k?
<g/>
]	]	kIx)
IPA	IPA	kA
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
State	status	k1gInSc5
of	of	k?
Alabama	Alabama	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stát	stát	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
východních	východní	k2eAgInPc2d1
jižních	jižní	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
regionu	region	k1gInSc6
USA	USA	kA
<g/>
.	.	kIx.
</s>