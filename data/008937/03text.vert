<p>
<s>
Alabama	Alabama	k1gFnSc1	Alabama
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Alabama	Alabama	k1gNnSc4	Alabama
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
východních	východní	k2eAgInPc2d1	východní
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Alabama	Alabama	k1gFnSc1	Alabama
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Mississippi	Mississippi	k1gFnSc7	Mississippi
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Tennessee	Tennessee	k1gFnSc7	Tennessee
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Georgií	Georgie	k1gFnSc7	Georgie
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Floridou	Florida	k1gFnSc7	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Mexický	mexický	k2eAgInSc4d1	mexický
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
135	[number]	k4	135
765	[number]	k4	765
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Alabama	Alabama	k1gFnSc1	Alabama
30	[number]	k4	30
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
4,9	[number]	k4	4,9
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
37	[number]	k4	37
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Montgomery	Montgomera	k1gFnSc2	Montgomera
s	s	k7c7	s
200	[number]	k4	200
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Birmingham	Birmingham	k1gInSc4	Birmingham
s	s	k7c7	s
210	[number]	k4	210
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Mobile	mobile	k1gNnSc1	mobile
(	(	kIx(	(
<g/>
190	[number]	k4	190
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Huntsville	Huntsville	k1gFnSc1	Huntsville
(	(	kIx(	(
<g/>
190	[number]	k4	190
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tuscaloosa	Tuscaloosa	k1gFnSc1	Tuscaloosa
(	(	kIx(	(
<g/>
100	[number]	k4	100
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alabamě	Alabamě	k6eAd1	Alabamě
patří	patřit	k5eAaImIp3nS	patřit
85	[number]	k4	85
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Cheaha	Cheaha	k1gMnSc1	Cheaha
Mountain	Mountain	k1gMnSc1	Mountain
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
736	[number]	k4	736
m	m	kA	m
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
říční	říční	k2eAgInSc1d1	říční
systém	systém	k1gInSc1	systém
Mobile-Alabama-Coosa	Mobile-Alabama-Coosa	k1gFnSc1	Mobile-Alabama-Coosa
a	a	k8xC	a
řeky	řeka	k1gFnPc1	řeka
Chattahoochee	Chattahoochee	k1gNnPc2	Chattahoochee
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Georgií	Georgie	k1gFnSc7	Georgie
<g/>
,	,	kIx,	,
a	a	k8xC	a
Conecuh	Conecuha	k1gFnPc2	Conecuha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Alabamy	Alabam	k1gInPc7	Alabam
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
prozkoumáno	prozkoumán	k2eAgNnSc4d1	prozkoumáno
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
trvalé	trvalý	k2eAgNnSc4d1	trvalé
evropské	evropský	k2eAgNnSc4d1	Evropské
osídlení	osídlení	k1gNnSc4	osídlení
založili	založit	k5eAaPmAgMnP	založit
až	až	k9	až
Francouzi	Francouz	k1gMnPc1	Francouz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
(	(	kIx(	(
<g/>
u	u	k7c2	u
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Mobile	mobile	k1gNnSc2	mobile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
oblast	oblast	k1gFnSc1	oblast
součástí	součást	k1gFnPc2	součást
jejich	jejich	k3xOp3gFnSc2	jejich
louisianské	louisianský	k2eAgFnSc2d1	louisianská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
britské	britský	k2eAgFnSc2d1	britská
Západní	západní	k2eAgFnSc2d1	západní
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
si	se	k3xPyFc3	se
území	území	k1gNnSc4	území
Alabamy	Alabam	k1gInPc1	Alabam
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
coby	coby	k?	coby
součást	součást	k1gFnSc1	součást
Floridy	Florida	k1gFnSc2	Florida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
USA	USA	kA	USA
mississippské	mississippský	k2eAgNnSc1d1	mississippský
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
dnešních	dnešní	k2eAgInPc2d1	dnešní
států	stát	k1gInPc2	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
Alabama	Alabamum	k1gNnSc2	Alabamum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
odděleno	oddělen	k2eAgNnSc4d1	odděleno
vlastní	vlastní	k2eAgNnSc4d1	vlastní
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stalo	stát	k5eAaPmAgNnS	stát
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
získalo	získat	k5eAaPmAgNnS	získat
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
podle	podle	k7c2	podle
indiánské	indiánský	k2eAgFnSc2d1	indiánská
kultury	kultura	k1gFnSc2	kultura
Alabamů	Alabam	k1gInPc2	Alabam
<g/>
.	.	kIx.	.
</s>
<s>
Alabama	Alabama	k1gFnSc1	Alabama
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1819	[number]	k4	1819
stala	stát	k5eAaPmAgFnS	stát
22	[number]	k4	22
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgNnP	být
Alabama	Alabamum	k1gNnPc1	Alabamum
v	v	k7c6	v
letech	let	k1gInPc6	let
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
součástí	součást	k1gFnPc2	součást
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
k	k	k7c3	k
Unii	unie	k1gFnSc3	unie
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
připojena	připojen	k2eAgFnSc1d1	připojena
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgInPc1d1	dnešní
Alabamy	Alabam	k1gInPc1	Alabam
se	se	k3xPyFc4	se
přely	přít	k5eAaImAgInP	přít
hned	hned	k6eAd1	hned
tři	tři	k4xCgFnPc4	tři
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
ji	on	k3xPp3gFnSc4	on
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
získali	získat	k5eAaPmAgMnP	získat
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
když	když	k8xS	když
obsadili	obsadit	k5eAaPmAgMnP	obsadit
území	území	k1gNnSc4	území
Louisiany	Louisiana	k1gFnSc2	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
USA	USA	kA	USA
koupily	koupit	k5eAaPmAgFnP	koupit
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
Louisianu	Louisiana	k1gFnSc4	Louisiana
a	a	k8xC	a
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Mississippi	Mississippi	k1gFnPc1	Mississippi
<g/>
,	,	kIx,	,
zvažovaly	zvažovat	k5eAaImAgFnP	zvažovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
odkoupit	odkoupit	k5eAaPmF	odkoupit
Alabamu	Alabam	k1gInSc3	Alabam
<g/>
,	,	kIx,	,
či	či	k8xC	či
ne	ne	k9	ne
<g/>
,	,	kIx,	,
a	a	k8xC	a
zda	zda	k8xS	zda
ji	on	k3xPp3gFnSc4	on
začlenit	začlenit	k5eAaPmF	začlenit
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Alabama	Alabama	k1gFnSc1	Alabama
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
stala	stát	k5eAaPmAgFnS	stát
22	[number]	k4	22
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1861	[number]	k4	1861
Alabama	Alabama	k1gFnSc1	Alabama
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
byl	být	k5eAaImAgInS	být
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
odpor	odpor	k1gInSc4	odpor
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
bojů	boj	k1gInPc2	boj
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
na	na	k7c6	na
území	území	k1gNnSc6	území
Alabamy	Alabam	k1gInPc4	Alabam
neodehrálo	odehrát	k5eNaPmAgNnS	odehrát
<g/>
,	,	kIx,	,
bojovalo	bojovat	k5eAaImAgNnS	bojovat
proti	proti	k7c3	proti
Unii	unie	k1gFnSc3	unie
120	[number]	k4	120
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
Alabamy	Alabam	k1gInPc7	Alabam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
provizorní	provizorní	k2eAgFnSc1d1	provizorní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
řízená	řízený	k2eAgFnSc1d1	řízená
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
unie	unie	k1gFnSc2	unie
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Alabama	Alabamum	k1gNnSc2	Alabamum
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
trpěla	trpět	k5eAaImAgFnS	trpět
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
přetrvávání	přetrvávání	k1gNnSc3	přetrvávání
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Alabama	Alabama	k1gFnSc1	Alabama
zažila	zažít	k5eAaPmAgFnS	zažít
rozkvět	rozkvět	k1gInSc4	rozkvět
<g/>
,	,	kIx,	,
ekonomika	ekonomika	k1gFnSc1	ekonomika
státu	stát	k1gInSc2	stát
přešla	přejít	k5eAaPmAgFnS	přejít
od	od	k7c2	od
zemědělství	zemědělství	k1gNnSc2	zemědělství
k	k	k7c3	k
těžkému	těžký	k2eAgInSc3d1	těžký
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
,	,	kIx,	,
těžbě	těžba	k1gFnSc3	těžba
nerostů	nerost	k1gInPc2	nerost
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
a	a	k8xC	a
technologiím	technologie	k1gFnPc3	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
a	a	k8xC	a
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
množství	množství	k1gNnSc4	množství
vojenských	vojenský	k2eAgNnPc2d1	vojenské
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
těch	ten	k3xDgMnPc2	ten
americké	americký	k2eAgMnPc4d1	americký
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
amerických	americký	k2eAgFnPc2d1	americká
leteckých	letecký	k2eAgFnPc2d1	letecká
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Alabama	Alabama	k1gFnSc1	Alabama
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jižní	jižní	k2eAgInPc4d1	jižní
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgInSc1d1	jihozápadní
cíp	cíp	k1gInSc1	cíp
je	být	k5eAaImIp3nS	být
pobřeží	pobřeží	k1gNnSc4	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
stát	stát	k1gInSc1	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Tennessee	Tennesse	k1gFnSc2	Tennesse
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Georgie	Georgie	k1gFnSc2	Georgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
je	být	k5eAaImIp3nS	být
Alabama	Alabama	k1gFnSc1	Alabama
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
135	[number]	k4	135
765	[number]	k4	765
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3,2	[number]	k4	3,2
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
Alabamy	Alabam	k1gInPc1	Alabam
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Alabama	Alabama	k1gFnSc1	Alabama
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
země	země	k1gFnSc1	země
s	s	k7c7	s
mírným	mírný	k2eAgInSc7d1	mírný
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
k	k	k7c3	k
Mexickému	mexický	k2eAgInSc3d1	mexický
zálivu	záliv	k1gInSc3	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
hustě	hustě	k6eAd1	hustě
zalesněna	zalesněn	k2eAgFnSc1d1	zalesněna
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obdělávaná	obdělávaný	k2eAgFnSc1d1	obdělávaná
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
úrodná	úrodný	k2eAgFnSc1d1	úrodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Cheaha	Cheaha	k1gMnSc1	Cheaha
Mountain	Mountain	k1gMnSc1	Mountain
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
736	[number]	k4	736
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
0	[number]	k4	0
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šířka	šířka	k1gFnSc1	šířka
státu	stát	k1gInSc2	stát
činí	činit	k5eAaImIp3nS	činit
306	[number]	k4	306
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
státu	stát	k1gInSc2	stát
činí	činit	k5eAaImIp3nS	činit
531	[number]	k4	531
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
HDP	HDP	kA	HDP
Alabamy	Alabam	k1gInPc4	Alabam
je	být	k5eAaImIp3nS	být
115	[number]	k4	115
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
činí	činit	k5eAaImIp3nS	činit
23	[number]	k4	23
471	[number]	k4	471
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
dolarů	dolar	k1gInPc2	dolar
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
úroveň	úroveň	k1gFnSc1	úroveň
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
jako	jako	k8xS	jako
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
produkty	produkt	k1gInPc1	produkt
<g/>
:	:	kIx,	:
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc1	dobytek
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc1d1	malý
stromky	stromek	k1gInPc1	stromek
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
výsadbu	výsadba	k1gFnSc4	výsadba
(	(	kIx(	(
<g/>
školky	školka	k1gFnSc2	školka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arašídy	arašíd	k1gInPc1	arašíd
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
soja	soja	k1gFnSc1	soja
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
artikly	artikl	k1gInPc4	artikl
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k6eAd1	hlavně
<g/>
:	:	kIx,	:
zpracování	zpracování	k1gNnSc4	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
řeziva	řezivo	k1gNnSc2	řezivo
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
hornictví	hornictví	k1gNnPc1	hornictví
<g/>
,	,	kIx,	,
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgNnPc1d1	dopravní
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
textilní	textilní	k2eAgFnSc1d1	textilní
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
4	[number]	k4	4
779	[number]	k4	779
736	[number]	k4	736
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
4	[number]	k4	4
833	[number]	k4	833
722	[number]	k4	722
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
36,5	[number]	k4	36,5
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rasové	rasový	k2eAgNnSc4d1	rasové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
68,5	[number]	k4	68,5
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
67,0	[number]	k4	67,0
%	%	kIx~	%
+	+	kIx~	+
běloši	běloch	k1gMnPc1	běloch
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
1,5	[number]	k4	1,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
26,2	[number]	k4	26,2
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
</s>
</p>
<p>
<s>
0,6	[number]	k4	0,6
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
</s>
</p>
<p>
<s>
1,1	[number]	k4	1,1
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
</s>
</p>
<p>
<s>
2,0	[number]	k4	2,0
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
</s>
</p>
<p>
<s>
1,5	[number]	k4	1,5
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasObyvatelé	rasObyvatel	k1gMnPc1	rasObyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
3,9	[number]	k4	3,9
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
92	[number]	k4	92
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Alabamy	Alabam	k1gInPc4	Alabam
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Protestantů	protestant	k1gMnPc2	protestant
bylo	být	k5eAaImAgNnS	být
80	[number]	k4	80
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
největší	veliký	k2eAgFnPc4d3	veliký
denominace	denominace	k1gFnPc4	denominace
byli	být	k5eAaImAgMnP	být
baptisté	baptista	k1gMnPc1	baptista
<g/>
,	,	kIx,	,
následovaní	následovaný	k2eAgMnPc1d1	následovaný
metodisty	metodista	k1gMnSc2	metodista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Montgomery	Montgomera	k1gFnPc4	Montgomera
(	(	kIx(	(
<g/>
201	[number]	k4	201
568	[number]	k4	568
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Birmingham	Birmingham	k1gInSc1	Birmingham
(	(	kIx(	(
<g/>
849	[number]	k4	849
194	[number]	k4	194
obyvatel	obyvatel	k1gMnPc2	obyvatel
včetně	včetně	k7c2	včetně
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Alabamě	Alabamě	k6eAd1	Alabamě
je	být	k5eAaImIp3nS	být
neoficiálně	neoficiálně	k6eAd1	neoficiálně
přezdíváno	přezdíván	k2eAgNnSc1d1	přezdíváno
Yellowhammer	Yellowhammer	k1gInSc1	Yellowhammer
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Strnadí	strnadí	k2eAgInSc1d1	strnadí
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
též	též	k9	též
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
Dixie	Dixie	k1gFnSc2	Dixie
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Srdce	srdce	k1gNnSc1	srdce
Dixie	Dixie	k1gFnSc2	Dixie
<g/>
.	.	kIx.	.
</s>
<s>
Státním	státní	k2eAgInSc7d1	státní
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
borovice	borovice	k1gFnSc1	borovice
bahenní	bahenní	k2eAgFnSc1d1	bahenní
a	a	k8xC	a
státní	státní	k2eAgFnSc7d1	státní
květinou	květina	k1gFnSc7	květina
kamélie	kamélie	k1gFnSc2	kamélie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
We	We	k1gMnSc1	We
defend	defend	k1gMnSc1	defend
our	our	k?	our
rights	rights	k1gInSc1	rights
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
zlatobýl	zlatobýl	k1gInSc1	zlatobýl
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
borovice	borovice	k1gFnSc2	borovice
bahenní	bahenní	k2eAgFnSc2d1	bahenní
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
datel	datel	k1gMnSc1	datel
zlatý	zlatý	k1gInSc1	zlatý
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Sweet	Sweeta	k1gFnPc2	Sweeta
home	hom	k1gMnSc2	hom
Alabama	Alabam	k1gMnSc2	Alabam
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
státě	stát	k1gInSc6	stát
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
oficiálně	oficiálně	k6eAd1	oficiálně
zrušen	zrušit	k5eAaPmNgInS	zrušit
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
smíšená	smíšený	k2eAgNnPc4d1	smíšené
manželství	manželství	k1gNnPc4	manželství
s	s	k7c7	s
černochy	černoch	k1gMnPc7	černoch
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
již	již	k6eAd1	již
33	[number]	k4	33
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
rozsudkem	rozsudek	k1gInSc7	rozsudek
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tato	tento	k3xDgNnPc1	tento
manželství	manželství	k1gNnPc4	manželství
legalizoval	legalizovat	k5eAaBmAgInS	legalizovat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
místní	místní	k2eAgInPc4d1	místní
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alabama	Alabamum	k1gNnSc2	Alabamum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Alabama	Alabama	k1gFnSc1	Alabama
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Alabama	Alabamum	k1gNnSc2	Alabamum
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
