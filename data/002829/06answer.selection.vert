<s>
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1891	[number]	k4	1891
Háj	háj	k1gInSc4	háj
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
