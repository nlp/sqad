<p>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
(	(	kIx(	(
<g/>
fonémika	fonémika	k1gFnSc1	fonémika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
fonetika	fonetika	k1gFnSc1	fonetika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
stránku	stránka	k1gFnSc4	stránka
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
fonetiky	fonetika	k1gFnSc2	fonetika
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
zajímají	zajímat	k5eAaImIp3nP	zajímat
pouze	pouze	k6eAd1	pouze
zvukové	zvukový	k2eAgInPc4d1	zvukový
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
nějakou	nějaký	k3yIgFnSc4	nějaký
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
schopnost	schopnost	k1gFnSc4	schopnost
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
význam	význam	k1gInSc4	význam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c4	o
funkci	funkce	k1gFnSc4	funkce
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
fonetika	fonetika	k1gFnSc1	fonetika
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
tvorbě	tvorba	k1gFnSc6	tvorba
hlásek	hlásek	k1gInSc4	hlásek
ve	v	k7c6	v
zvukovém	zvukový	k2eAgNnSc6d1	zvukové
ústrojí	ústrojí	k1gNnSc6	ústrojí
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
šíření	šíření	k1gNnSc1	šíření
a	a	k8xC	a
vnímání	vnímání	k1gNnSc1	vnímání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Foném	foném	k1gInSc4	foném
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zájmem	zájem	k1gInSc7	zájem
zkoumání	zkoumání	k1gNnSc2	zkoumání
ve	v	k7c6	v
fonologii	fonologie	k1gFnSc6	fonologie
je	být	k5eAaImIp3nS	být
foném	foném	k1gInSc4	foném
–	–	k?	–
nejmenší	malý	k2eAgFnSc1d3	nejmenší
zvuková	zvukový	k2eAgFnSc1d1	zvuková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
slova	slovo	k1gNnPc4	slovo
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnostní	výslovnostní	k2eAgFnPc1d1	výslovnostní
varianty	varianta	k1gFnPc1	varianta
fonému	foném	k1gInSc2	foném
jsou	být	k5eAaImIp3nP	být
alofony	alofon	k1gInPc1	alofon
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
např.	např.	kA	např.
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
fonémy	foném	k1gInPc1	foném
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
ŋ	ŋ	k?	ŋ
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
náš	náš	k3xOp1gInSc4	náš
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
banka	banka	k1gFnSc1	banka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
nemůže	moct	k5eNaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovované	vyslovovaný	k2eAgNnSc1d1	vyslovované
[	[	kIx(	[
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
ŋ	ŋ	k?	ŋ
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
alofony	alofon	k1gInPc4	alofon
jediného	jediný	k2eAgInSc2d1	jediný
fonému	foném	k1gInSc2	foném
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
tento	tento	k3xDgInSc4	tento
rozdíl	rozdíl	k1gInSc4	rozdíl
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
thin	thin	k1gNnSc4	thin
[	[	kIx(	[
<g/>
θ	θ	k?	θ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
)	)	kIx)	)
vs	vs	k?	vs
<g/>
.	.	kIx.	.
thing	thing	k1gMnSc1	thing
[	[	kIx(	[
<g/>
θ	θ	k?	θ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
věc	věc	k1gFnSc1	věc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alofonní	Alofonní	k2eAgFnSc1d1	Alofonní
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
především	především	k9	především
zájmem	zájem	k1gInSc7	zájem
fonetiky	fonetika	k1gFnSc2	fonetika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
které	který	k3yIgNnSc1	který
slovo	slovo	k1gNnSc1	slovo
správně	správně	k6eAd1	správně
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
musí	muset	k5eAaImIp3nS	muset
zajímat	zajímat	k5eAaImF	zajímat
i	i	k9	i
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
význam	význam	k1gInSc4	význam
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morfoném	Morfoný	k2eAgMnSc6d1	Morfoný
==	==	k?	==
</s>
</p>
<p>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
také	také	k9	také
studuje	studovat	k5eAaImIp3nS	studovat
zákonitosti	zákonitost	k1gFnPc4	zákonitost
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
k	k	k7c3	k
hláskovým	hláskový	k2eAgFnPc3d1	hlásková
změnám	změna	k1gFnPc3	změna
(	(	kIx(	(
<g/>
alternacím	alternace	k1gFnPc3	alternace
<g/>
)	)	kIx)	)
při	pře	k1gFnSc3	pře
ohýbání	ohýbání	k1gNnSc2	ohýbání
a	a	k8xC	a
odvozování	odvozování	k1gNnSc1	odvozování
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
nestuduje	studovat	k5eNaImIp3nS	studovat
však	však	k9	však
ohýbání	ohýbání	k1gNnSc1	ohýbání
slov	slovo	k1gNnPc2	slovo
samotné	samotný	k2eAgInPc1d1	samotný
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
morfologie	morfologie	k1gFnSc2	morfologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
měkčení	měkčení	k1gNnSc1	měkčení
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
/	/	kIx~	/
na	na	k7c4	na
/	/	kIx~	/
<g/>
ts	ts	k0	ts
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
<c>
)	)	kIx)	)
při	při	k7c6	při
připojení	připojení	k1gNnSc6	připojení
přípony	přípona	k1gFnSc2	přípona
-e	-e	k?	-e
ke	k	k7c3	k
kmeni	kmen	k1gInSc3	kmen
matk	matk	k6eAd1	matk
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
fonologickými	fonologický	k2eAgNnPc7d1	fonologické
pravidly	pravidlo	k1gNnPc7	pravidlo
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
fonémů	foném	k1gInPc2	foném
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
ohýbání	ohýbání	k1gNnSc6	ohýbání
a	a	k8xC	a
odvozování	odvozování	k1gNnSc6	odvozování
střídají	střídat	k5eAaImIp3nP	střídat
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
pozici	pozice	k1gFnSc6	pozice
daného	daný	k2eAgInSc2d1	daný
morfému	morfém	k1gInSc2	morfém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
morfoném	morfoný	k2eAgNnSc6d1	morfoný
(	(	kIx(	(
<g/>
morfofoném	morfofoné	k1gNnSc6	morfofoné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Distinktivní	distinktivní	k2eAgInPc4d1	distinktivní
rysy	rys	k1gInPc4	rys
==	==	k?	==
</s>
</p>
<p>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
studuje	studovat	k5eAaImIp3nS	studovat
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
fonémy	foném	k1gInPc7	foném
i	i	k8xC	i
zvukové	zvukový	k2eAgInPc4d1	zvukový
jevy	jev	k1gInPc4	jev
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
úrovni	úroveň	k1gFnSc6	úroveň
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
suprasegmentální	suprasegmentální	k2eAgInPc1d1	suprasegmentální
jevy	jev	k1gInPc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc4	tento
jevy	jev	k1gInPc4	jev
schopnost	schopnost	k1gFnSc4	schopnost
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
význam	význam	k1gInSc4	význam
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
distinktivní	distinktivní	k2eAgInPc1d1	distinktivní
rysy	rys	k1gInPc1	rys
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
a	a	k8xC	a
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
i	i	k9	i
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
určitého	určitý	k2eAgInSc2d1	určitý
rysu	rys	k1gInSc2	rys
u	u	k7c2	u
fonémů	foném	k1gInPc2	foném
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
fonologickou	fonologický	k2eAgFnSc4d1	fonologická
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
distinktivním	distinktivní	k2eAgInSc7d1	distinktivní
rysem	rys	k1gInSc7	rys
délka	délka	k1gFnSc1	délka
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
pata	pata	k1gFnSc1	pata
a	a	k8xC	a
pátá	pátá	k1gFnSc1	pátá
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
(	(	kIx(	(
<g/>
pevný	pevný	k2eAgInSc1d1	pevný
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
významy	význam	k1gInPc4	význam
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
distinktivní	distinktivní	k2eAgInSc4d1	distinktivní
rys	rys	k1gInSc4	rys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
slabice	slabika	k1gFnSc6	slabika
(	(	kIx(	(
<g/>
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
význam	význam	k1gInSc4	význam
jinak	jinak	k6eAd1	jinak
stejných	stejný	k2eAgNnPc2d1	stejné
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
samohlásek	samohláska	k1gFnPc2	samohláska
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
přízvuku	přízvuk	k1gInSc6	přízvuk
(	(	kIx(	(
<g/>
přízvučná	přízvučný	k2eAgFnSc1d1	přízvučná
samohláska	samohláska	k1gFnSc1	samohláska
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
nepřízvučná	přízvučný	k2eNgFnSc1d1	nepřízvučná
krátká	krátká	k1gFnSc1	krátká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
distinktivní	distinktivní	k2eAgInSc4d1	distinktivní
rys	rys	k1gInSc4	rys
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
<g/>
Distinktivními	distinktivní	k2eAgInPc7d1	distinktivní
rysy	rys	k1gInPc7	rys
vytvářejícími	vytvářející	k2eAgInPc7d1	vytvářející
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
fonologické	fonologický	k2eAgFnSc2d1	fonologická
opozice	opozice	k1gFnSc2	opozice
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
délka	délka	k1gFnSc1	délka
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
x	x	k?	x
krátká	krátká	k1gFnSc1	krátká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znělost	znělost	k1gFnSc1	znělost
(	(	kIx(	(
<g/>
znělá	znělý	k2eAgFnSc1d1	znělá
x	x	k?	x
neznělá	znělý	k2eNgFnSc1d1	neznělá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zaokrouhlenost	zaokrouhlenost	k1gFnSc1	zaokrouhlenost
(	(	kIx(	(
<g/>
zaokrouhlená	zaokrouhlený	k2eAgFnSc1d1	zaokrouhlená
x	x	k?	x
nezaokrouhlená	zaokrouhlený	k2eNgFnSc1d1	nezaokrouhlená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otevřenost	otevřenost	k1gFnSc1	otevřenost
(	(	kIx(	(
<g/>
otevřená	otevřený	k2eAgFnSc1d1	otevřená
x	x	k?	x
středová	středový	k2eAgFnSc1d1	středová
x	x	k?	x
zavřená	zavřený	k2eAgFnSc1d1	zavřená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednost	přednost	k1gFnSc4	přednost
(	(	kIx(	(
<g/>
přední	přední	k2eAgMnSc1d1	přední
x	x	k?	x
střední	střední	k1gMnSc1	střední
x	x	k?	x
zadní	zadní	k2eAgFnPc1d1	zadní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palatalizace	palatalizace	k1gFnSc1	palatalizace
(	(	kIx(	(
<g/>
palatalizovaná	palatalizovaný	k2eAgFnSc1d1	palatalizovaný
x	x	k?	x
nepalatalizovaná	palatalizovaný	k2eNgFnSc1d1	palatalizovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
,	,	kIx,	,
intonace	intonace	k1gFnSc1	intonace
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fonologická	fonologický	k2eAgFnSc1d1	fonologická
transkripce	transkripce	k1gFnSc1	transkripce
==	==	k?	==
</s>
</p>
<p>
<s>
Přepis	přepis	k1gInSc1	přepis
textu	text	k1gInSc2	text
pomocí	pomocí	k7c2	pomocí
fonémů	foném	k1gInPc2	foném
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
mezi	mezi	k7c4	mezi
lomítka	lomítko	k1gNnPc4	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
jednotlivého	jednotlivý	k2eAgInSc2d1	jednotlivý
fonému	foném	k1gInSc2	foném
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
znak	znak	k1gInSc1	znak
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
alofonu	alofon	k1gInSc2	alofon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
způsob	způsob	k1gInSc4	způsob
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
kɲ	kɲ	k?	kɲ
<g/>
/	/	kIx~	/
<g/>
Fonetická	fonetický	k2eAgFnSc1d1	fonetická
transkripce	transkripce	k1gFnSc1	transkripce
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
co	co	k3yInSc4	co
nejpřesnější	přesný	k2eAgInSc1d3	nejpřesnější
záznam	záznam	k1gInSc1	záznam
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
,	,	kIx,	,
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
hranatých	hranatý	k2eAgFnPc2d1	hranatá
závorek	závorka	k1gFnPc2	závorka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
kɲ	kɲ	k?	kɲ
<g/>
]	]	kIx)	]
<g/>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
odlišit	odlišit	k5eAaPmF	odlišit
grafický	grafický	k2eAgInSc4d1	grafický
zápis	zápis	k1gInSc4	zápis
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
podle	podle	k7c2	podle
příslušných	příslušný	k2eAgNnPc2d1	příslušné
pravidel	pravidlo	k1gNnPc2	pravidlo
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
text	text	k1gInSc4	text
mezi	mezi	k7c4	mezi
ostré	ostrý	k2eAgFnPc4d1	ostrá
závorky	závorka	k1gFnPc4	závorka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tzv.	tzv.	kA	tzv.
krize	krize	k1gFnSc2	krize
fonetiky	fonetika	k1gFnSc2	fonetika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dávala	dávat	k5eAaImAgFnS	dávat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dosti	dosti	k6eAd1	dosti
přesný	přesný	k2eAgInSc4d1	přesný
popis	popis	k1gInSc4	popis
artikulace	artikulace	k1gFnSc2	artikulace
hlásek	hlásek	k1gInSc4	hlásek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
přestával	přestávat	k5eAaImAgInS	přestávat
stačit	stačit	k5eAaBmF	stačit
potřebám	potřeba	k1gFnPc3	potřeba
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
hlásky	hlásek	k1gInPc4	hlásek
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
naznačili	naznačit	k5eAaPmAgMnP	naznačit
někteří	některý	k3yIgMnPc1	některý
jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
se	se	k3xPyFc4	se
však	však	k9	však
této	tento	k3xDgFnSc3	tento
problematice	problematika	k1gFnSc3	problematika
věnovali	věnovat	k5eAaImAgMnP	věnovat
představitelé	představitel	k1gMnPc1	představitel
tzv.	tzv.	kA	tzv.
pražské	pražský	k2eAgFnSc2d1	Pražská
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Trubeckoj	Trubeckoj	k1gInSc4	Trubeckoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
fonologie	fonologie	k1gFnSc2	fonologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
dílo	dílo	k1gNnSc4	dílo
Základy	základ	k1gInPc1	základ
fonologie	fonologie	k1gFnSc1	fonologie
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
myšlenky	myšlenka	k1gFnPc4	myšlenka
fonologie	fonologie	k1gFnSc2	fonologie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
definici	definice	k1gFnSc4	definice
fonému	foném	k1gInSc2	foném
jako	jako	k8xS	jako
základní	základní	k2eAgFnPc4d1	základní
fonologické	fonologický	k2eAgFnPc4d1	fonologická
jednotky	jednotka	k1gFnPc4	jednotka
a	a	k8xC	a
popis	popis	k1gInSc4	popis
systému	systém	k1gInSc2	systém
fonologických	fonologický	k2eAgFnPc2d1	fonologická
opozic	opozice	k1gFnPc2	opozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fonologie	fonologie	k1gFnPc1	fonologie
si	se	k3xPyFc3	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lingvistiky	lingvistika	k1gFnSc2	lingvistika
získala	získat	k5eAaPmAgFnS	získat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
některé	některý	k3yIgFnPc1	některý
její	její	k3xOp3gFnPc1	její
metody	metoda	k1gFnPc1	metoda
(	(	kIx(	(
<g/>
popis	popis	k1gInSc1	popis
pomocí	pomocí	k7c2	pomocí
systému	systém	k1gInSc2	systém
opozic	opozice	k1gFnPc2	opozice
<g/>
)	)	kIx)	)
převzaty	převzít	k5eAaPmNgFnP	převzít
i	i	k8xC	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
TRUBETZKOY	TRUBETZKOY	kA	TRUBETZKOY
<g/>
,	,	kIx,	,
Nikolai	Nikolai	k1gNnSc1	Nikolai
Sergeyevitsch	Sergeyevitscha	k1gFnPc2	Sergeyevitscha
<g/>
.	.	kIx.	.
</s>
<s>
Grundzüge	Grundzüge	k6eAd1	Grundzüge
der	drát	k5eAaImRp2nS	drát
Phonologie	Phonologie	k1gFnSc5	Phonologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Travaux	Travaux	k1gInSc1	Travaux
du	du	k?	du
Cercle	cercle	k1gInSc1	cercle
liguistique	liguistique	k1gInSc1	liguistique
de	de	k?	de
Prague	Prague	k1gInSc1	Prague
7	[number]	k4	7
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠIŠKA	Šiška	k1gMnSc1	Šiška
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
.	.	kIx.	.
</s>
<s>
Fonetika	fonetika	k1gFnSc1	fonetika
a	a	k8xC	a
fonologie	fonologie	k1gFnSc1	fonologie
<g/>
.	.	kIx.	.
</s>
<s>
Fragmenty	fragment	k1gInPc1	fragment
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
PF	PF	kA	PF
UP	UP	kA	UP
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
češtiny	čeština	k1gFnSc2	čeština
</s>
</p>
<p>
<s>
Fonetika	fonetika	k1gFnSc1	fonetika
</s>
</p>
