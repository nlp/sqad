<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zájmem	zájem	k1gInSc7	zájem
zkoumání	zkoumání	k1gNnSc2	zkoumání
ve	v	k7c6	v
fonologii	fonologie	k1gFnSc6	fonologie
je	být	k5eAaImIp3nS	být
foném	foném	k1gInSc4	foném
–	–	k?	–
nejmenší	malý	k2eAgFnSc1d3	nejmenší
zvuková	zvukový	k2eAgFnSc1d1	zvuková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
slova	slovo	k1gNnPc4	slovo
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
