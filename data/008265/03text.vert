<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1533	[number]	k4	1533
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
královna	královna	k1gFnSc1	královna
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1558	[number]	k4	1558
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
Anny	Anna	k1gFnSc2	Anna
Boleynové	Boleynová	k1gFnSc2	Boleynová
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
pátou	pátá	k1gFnSc7	pátá
a	a	k8xC	a
poslední	poslední	k2eAgFnSc7d1	poslední
panovnicí	panovnice	k1gFnSc7	panovnice
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
nevlastní	vlastní	k2eNgFnSc2d1	nevlastní
sestry	sestra	k1gFnSc2	sestra
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
<g/>
,	,	kIx,	,
za	za	k7c2	za
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgNnP	být
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
protestantského	protestantský	k2eAgNnSc2d1	protestantské
povstání	povstání	k1gNnSc2	povstání
téměř	téměř	k6eAd1	téměř
rok	rok	k1gInSc4	rok
vězněna	věznit	k5eAaImNgFnS	věznit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokázala	dokázat	k5eAaPmAgFnS	dokázat
sestavit	sestavit	k5eAaPmF	sestavit
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
královskou	královský	k2eAgFnSc4d1	královská
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
se	se	k3xPyFc4	se
na	na	k7c4	na
rady	rada	k1gFnPc4	rada
svých	svůj	k3xOyFgMnPc2	svůj
poradců	poradce	k1gMnPc2	poradce
vedených	vedený	k2eAgMnPc2d1	vedený
Williamem	William	k1gInSc7	William
Cecilem	Cecil	k1gMnSc7	Cecil
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
baronem	baron	k1gMnSc7	baron
z	z	k7c2	z
Burghley	Burghlea	k1gFnSc2	Burghlea
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
počinů	počin	k1gInPc2	počin
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
upevnění	upevnění	k1gNnSc1	upevnění
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
(	(	kIx(	(
<g/>
protestantské	protestantský	k2eAgFnSc2d1	protestantská
<g/>
)	)	kIx)	)
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
opatrná	opatrný	k2eAgFnSc1d1	opatrná
v	v	k7c6	v
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
výbojích	výboj	k1gInPc6	výboj
<g/>
,	,	kIx,	,
porážka	porážka	k1gFnSc1	porážka
španělské	španělský	k2eAgFnSc2d1	španělská
Armady	Armada	k1gFnSc2	Armada
roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
jí	on	k3xPp3gFnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
nehynoucí	hynoucí	k2eNgFnSc4d1	nehynoucí
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
vítězství	vítězství	k1gNnPc2	vítězství
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
dvacet	dvacet	k4xCc1	dvacet
let	léto	k1gNnPc2	léto
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
oslavována	oslavován	k2eAgFnSc1d1	oslavována
jako	jako	k8xC	jako
panovnice	panovnice	k1gFnSc1	panovnice
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historikové	historik	k1gMnPc1	historik
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
opatrní	opatrný	k2eAgMnPc1d1	opatrný
v	v	k7c6	v
úsudku	úsudek	k1gInSc6	úsudek
o	o	k7c6	o
jejích	její	k3xOp3gInPc6	její
úspěších	úspěch	k1gInPc6	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
popudlivou	popudlivý	k2eAgFnSc4d1	popudlivá
a	a	k8xC	a
občas	občas	k6eAd1	občas
nerozhodnou	rozhodný	k2eNgFnSc4d1	nerozhodná
panovnici	panovnice	k1gFnSc4	panovnice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
některé	některý	k3yIgInPc1	některý
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
a	a	k8xC	a
vojenské	vojenský	k2eAgInPc1d1	vojenský
neúspěchy	neúspěch	k1gInPc1	neúspěch
snížily	snížit	k5eAaPmAgFnP	snížit
její	její	k3xOp3gFnSc4	její
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
má	mít	k5eAaImIp3nS	mít
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
pověst	pověst	k1gFnSc4	pověst
svéhlavé	svéhlavý	k2eAgFnSc2d1	svéhlavá
a	a	k8xC	a
charismatické	charismatický	k2eAgFnSc2d1	charismatická
královny	královna	k1gFnSc2	královna
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vládci	vládce	k1gMnPc1	vládce
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
museli	muset	k5eAaImAgMnP	muset
čelit	čelit	k5eAaImF	čelit
vnitřním	vnitřní	k2eAgInPc3d1	vnitřní
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
jejich	jejich	k3xOp3gNnSc2	jejich
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
jejích	její	k3xOp3gInPc2	její
nevlastních	vlastní	k2eNgInPc2d1	nevlastní
sourozenců	sourozenec	k1gMnPc2	sourozenec
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
bylo	být	k5eAaImAgNnS	být
oněch	onen	k3xDgInPc2	onen
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
44	[number]	k4	44
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
anglickou	anglický	k2eAgFnSc7d1	anglická
panovnicí	panovnice	k1gFnSc7	panovnice
<g/>
,	,	kIx,	,
obdobím	období	k1gNnSc7	období
cenné	cenný	k2eAgFnSc2d1	cenná
stability	stabilita	k1gFnSc2	stabilita
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
vláda	vláda	k1gFnSc1	vláda
většinou	většinou	k6eAd1	většinou
není	být	k5eNaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
tudorovská	tudorovský	k2eAgFnSc1d1	Tudorovská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
byla	být	k5eAaImAgFnS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
dcerou	dcera	k1gFnSc7	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Tudora	tudor	k1gMnSc4	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
alžbětinská	alžbětinský	k2eAgFnSc1d1	Alžbětinská
doba	doba	k1gFnSc1	doba
či	či	k8xC	či
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
rozvojem	rozvoj	k1gInSc7	rozvoj
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
anglické	anglický	k2eAgFnSc2d1	anglická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc7	jejichž
představiteli	představitel	k1gMnPc7	představitel
byli	být	k5eAaImAgMnP	být
mj.	mj.	kA	mj.
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
a	a	k8xC	a
Christopher	Christophra	k1gFnPc2	Christophra
Marlowe	Marlow	k1gFnSc2	Marlow
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
důležitost	důležitost	k1gFnSc1	důležitost
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
přikládá	přikládat	k5eAaImIp3nS	přikládat
úspěchům	úspěch	k1gInPc3	úspěch
v	v	k7c4	v
dobývání	dobývání	k1gNnSc4	dobývání
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
moři	moře	k1gNnPc7	moře
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Alžbětině	Alžbětin	k2eAgFnSc6d1	Alžbětina
době	doba	k1gFnSc6	doba
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
Francis	Francis	k1gInSc4	Francis
Drake	Drak	k1gFnSc2	Drak
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Hawkins	Hawkins	k1gInSc1	Hawkins
a	a	k8xC	a
Walter	Walter	k1gMnSc1	Walter
Raleigh	Raleigh	k1gMnSc1	Raleigh
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
počátkem	počátkem	k7c2	počátkem
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
postavení	postavení	k1gNnSc2	postavení
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1533	[number]	k4	1533
jako	jako	k8xS	jako
dcera	dcera	k1gFnSc1	dcera
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
Anny	Anna	k1gFnSc2	Anna
Boleynové	Boleynová	k1gFnSc2	Boleynová
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
babičkách	babička	k1gFnPc6	babička
Alžbětě	Alžběta	k1gFnSc3	Alžběta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Alžbětě	Alžběta	k1gFnSc3	Alžběta
Howardové	Howardový	k2eAgFnPc1d1	Howardový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
narození	narození	k1gNnSc6	narození
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
legitimní	legitimní	k2eAgFnSc7d1	legitimní
dědičkou	dědička	k1gFnSc7	dědička
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
manželství	manželství	k1gNnSc2	manželství
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Aragonskou	aragonský	k2eAgFnSc7d1	Aragonská
odvolal	odvolat	k5eAaPmAgMnS	odvolat
legitimitu	legitimita	k1gFnSc4	legitimita
dcery	dcera	k1gFnSc2	dcera
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
svazku	svazek	k1gInSc2	svazek
Marie	Maria	k1gFnSc2	Maria
Tudorovny	Tudorovna	k1gFnSc2	Tudorovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
přál	přát	k5eAaImAgMnS	přát
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Anna	Anna	k1gFnSc1	Anna
Boleynová	Boleynová	k1gFnSc1	Boleynová
prodělala	prodělat	k5eAaPmAgFnS	prodělat
dva	dva	k4xCgInPc4	dva
potraty	potrat	k1gInPc4	potrat
<g/>
,	,	kIx,	,
došel	dojít	k5eAaPmAgMnS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
ji	on	k3xPp3gFnSc4	on
nechal	nechat	k5eAaPmAgInS	nechat
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětě	Alžběta	k1gFnSc3	Alžběta
byly	být	k5eAaImAgInP	být
tehdy	tehdy	k6eAd1	tehdy
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
nelegitimního	legitimní	k2eNgMnSc4d1	nelegitimní
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
postupně	postupně	k6eAd1	postupně
dostalo	dostat	k5eAaPmAgNnS	dostat
–	–	k?	–
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
–	–	k?	–
dokonalého	dokonalý	k2eAgNnSc2d1	dokonalé
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
studovala	studovat	k5eAaImAgFnS	studovat
mimo	mimo	k7c4	mimo
jiné	jiný	k1gMnPc4	jiný
několik	několik	k4yIc1	několik
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
angličtiny	angličtina	k1gFnSc2	angličtina
také	také	k9	také
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
italštinu	italština	k1gFnSc4	italština
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1550	[number]	k4	1550
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
studium	studium	k1gNnSc1	studium
formálně	formálně	k6eAd1	formálně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvzdělanějších	vzdělaný	k2eAgFnPc2d3	nejvzdělanější
žen	žena	k1gFnPc2	žena
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
<g/>
,	,	kIx,	,
když	když	k8xS	když
Alžbětě	Alžběta	k1gFnSc3	Alžběta
bylo	být	k5eAaImAgNnS	být
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Parrová	Parrová	k1gFnSc1	Parrová
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
Jindřichova	Jindřichův	k2eAgFnSc1d1	Jindřichova
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
znovu	znovu	k6eAd1	znovu
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
Thomase	Thomas	k1gMnSc2	Thomas
Seymoura	Seymour	k1gMnSc2	Seymour
a	a	k8xC	a
manželé	manžel	k1gMnPc1	manžel
si	se	k3xPyFc3	se
vzali	vzít	k5eAaPmAgMnP	vzít
Alžbětu	Alžběta	k1gFnSc4	Alžběta
na	na	k7c6	na
opatrování	opatrování	k1gNnSc6	opatrování
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
zažila	zažít	k5eAaPmAgFnS	zažít
emocionální	emocionální	k2eAgFnSc4d1	emocionální
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Seymour	Seymour	k1gMnSc1	Seymour
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
trávil	trávit	k5eAaImAgMnS	trávit
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
ložnici	ložnice	k1gFnSc6	ložnice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Kateřina	Kateřina	k1gFnSc1	Kateřina
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
zálety	zálet	k1gInPc4	zálet
svého	svůj	k1gMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
<g/>
,	,	kIx,	,
ukončila	ukončit	k5eAaPmAgFnS	ukončit
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
poslala	poslat	k5eAaPmAgFnS	poslat
Alžbětu	Alžběta	k1gFnSc4	Alžběta
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Kateřiny	Kateřina	k1gFnSc2	Kateřina
se	se	k3xPyFc4	se
Seymour	Seymour	k1gMnSc1	Seymour
snažil	snažit	k5eAaImAgMnS	snažit
Alžbětu	Alžběta	k1gFnSc4	Alžběta
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vzala	vzít	k5eAaPmAgFnS	vzít
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejího	její	k3xOp3gMnSc4	její
bratra	bratr	k1gMnSc4	bratr
a	a	k8xC	a
královskou	královský	k2eAgFnSc4d1	královská
radu	rada	k1gFnSc4	rada
to	ten	k3xDgNnSc1	ten
představovalo	představovat	k5eAaImAgNnS	představovat
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
převzetí	převzetí	k1gNnSc2	převzetí
moci	moct	k5eAaImF	moct
Seymourem	Seymour	k1gInSc7	Seymour
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
ho	on	k3xPp3gInSc4	on
obvinili	obvinit	k5eAaPmAgMnP	obvinit
ze	z	k7c2	z
závažného	závažný	k2eAgInSc2d1	závažný
hrdelního	hrdelní	k2eAgInSc2d1	hrdelní
zločinu	zločin	k1gInSc2	zločin
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
anglického	anglický	k2eAgNnSc2d1	anglické
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
felony	felona	k1gFnSc2	felona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
rada	rada	k1gFnSc1	rada
jej	on	k3xPp3gMnSc4	on
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Eduardovi	Eduard	k1gMnSc6	Eduard
nakrátko	nakrátko	k6eAd1	nakrátko
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
příbuzná	příbuzná	k1gFnSc1	příbuzná
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
Jane	Jan	k1gMnSc5	Jan
Greyová	Greyová	k1gFnSc1	Greyová
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčená	přesvědčený	k2eAgFnSc1d1	přesvědčená
katolička	katolička	k1gFnSc1	katolička
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
Tudorovna	Tudorovna	k1gFnSc1	Tudorovna
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dcera	dcera	k1gFnSc1	dcera
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
a	a	k8xC	a
majestátně	majestátně	k6eAd1	majestátně
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
provolávat	provolávat	k5eAaImF	provolávat
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
po	po	k7c6	po
prohlášení	prohlášení	k1gNnSc6	prohlášení
Jany	Jana	k1gFnSc2	Jana
královnou	královna	k1gFnSc7	královna
ji	on	k3xPp3gFnSc4	on
Marie	Marie	k1gFnSc1	Marie
I.	I.	kA	I.
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
a	a	k8xC	a
usedla	usednout	k5eAaPmAgFnS	usednout
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
královská	královský	k2eAgFnSc1d1	královská
rada	rada	k1gFnSc1	rada
schválila	schválit	k5eAaPmAgFnS	schválit
zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zakázaném	zakázaný	k2eAgNnSc6d1	zakázané
středověkém	středověký	k2eAgNnSc6d1	středověké
právu	právo	k1gNnSc6	právo
útrpném	útrpný	k2eAgNnSc6d1	útrpné
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
Janu	Jana	k1gFnSc4	Jana
Greyovou	Greyová	k1gFnSc4	Greyová
popravit	popravit	k5eAaPmF	popravit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
dalších	další	k2eAgMnPc2d1	další
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Marie	Marie	k1gFnSc1	Marie
I.	I.	kA	I.
slibovala	slibovat	k5eAaImAgFnS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dovolí	dovolit	k5eAaPmIp3nS	dovolit
protestantské	protestantský	k2eAgFnSc3d1	protestantská
církvi	církev	k1gFnSc3	církev
volné	volný	k2eAgNnSc1d1	volné
působení	působení	k1gNnSc1	působení
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
slib	slib	k1gInSc4	slib
porušila	porušit	k5eAaPmAgFnS	porušit
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
rušit	rušit	k5eAaImF	rušit
protestantské	protestantský	k2eAgFnPc4d1	protestantská
reformy	reforma	k1gFnPc4	reforma
zavedené	zavedený	k2eAgFnPc4d1	zavedená
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
počáteční	počáteční	k2eAgFnSc1d1	počáteční
obliba	obliba	k1gFnSc1	obliba
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
vdát	vdát	k5eAaPmF	vdát
za	za	k7c2	za
španělského	španělský	k2eAgMnSc2d1	španělský
prince	princ	k1gMnSc2	princ
Filipa	Filip	k1gMnSc2	Filip
(	(	kIx(	(
<g/>
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1554	[number]	k4	1554
vzpouru	vzpoura	k1gFnSc4	vzpoura
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
vidělo	vidět	k5eAaImAgNnS	vidět
naději	naděje	k1gFnSc4	naděje
v	v	k7c6	v
Alžbětě	Alžběta	k1gFnSc6	Alžběta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
názory	názor	k1gInPc1	názor
byly	být	k5eAaImAgInP	být
protestantské	protestantský	k2eAgInPc1d1	protestantský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
vzpoury	vzpoura	k1gFnSc2	vzpoura
byla	být	k5eAaImAgFnS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
důsledně	důsledně	k6eAd1	důsledně
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
sestřenice	sestřenice	k1gFnSc2	sestřenice
<g/>
,	,	kIx,	,
mladistvé	mladistvý	k2eAgFnSc2d1	mladistvá
Jany	Jana	k1gFnSc2	Jana
Greyové	Greyová	k1gFnSc2	Greyová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
popravena	popravit	k5eAaPmNgFnS	popravit
<g/>
,	,	kIx,	,
strávila	strávit	k5eAaPmAgFnS	strávit
Alžběta	Alžběta	k1gFnSc1	Alžběta
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
jako	jako	k8xC	jako
vězeňkyně	vězeňkyně	k1gFnPc4	vězeňkyně
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Toweru	Towero	k1gNnSc6	Towero
a	a	k8xC	a
poté	poté	k6eAd1	poté
asi	asi	k9	asi
rok	rok	k1gInSc4	rok
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
na	na	k7c6	na
jednom	jeden	k4xCgMnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
panství	panství	k1gNnPc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
falešných	falešný	k2eAgNnPc6d1	falešné
Mariiných	Mariin	k2eAgNnPc6d1	Mariino
těhotenstvích	těhotenství	k1gNnPc6	těhotenství
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
královna	královna	k1gFnSc1	královna
asi	asi	k9	asi
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
brána	brát	k5eAaImNgFnS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
jako	jako	k8xC	jako
korunní	korunní	k2eAgFnSc1d1	korunní
princezna	princezna	k1gFnSc1	princezna
resp.	resp.	kA	resp.
příští	příští	k2eAgFnSc1d1	příští
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1558	[number]	k4	1558
ji	on	k3xPp3gFnSc4	on
Marie	Marie	k1gFnSc1	Marie
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
svojí	svůj	k3xOyFgFnSc7	svůj
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
sama	sám	k3xTgFnSc1	sám
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
vaječníku	vaječník	k1gInSc2	vaječník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
příčinou	příčina	k1gFnSc7	příčina
jejích	její	k3xOp3gNnPc2	její
zdánlivých	zdánlivý	k2eAgNnPc2d1	zdánlivé
těhotenství	těhotenství	k1gNnPc2	těhotenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInSc1	její
soukromý	soukromý	k2eAgInSc1d1	soukromý
názor	názor	k1gInSc1	názor
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
církevní	církevní	k2eAgFnSc1d1	církevní
politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
pragmatickým	pragmatický	k2eAgInSc7d1	pragmatický
přístupem	přístup	k1gInSc7	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
Alžbětiných	Alžbětin	k2eAgInPc2d1	Alžbětin
hlavních	hlavní	k2eAgInPc2d1	hlavní
problémů	problém	k1gInPc2	problém
byla	být	k5eAaImAgFnS	být
legitimita	legitimita	k1gFnSc1	legitimita
jejího	její	k3xOp3gInSc2	její
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
podle	podle	k7c2	podle
protestantského	protestantský	k2eAgNnSc2d1	protestantské
práva	právo	k1gNnSc2	právo
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
nelegitimním	legitimní	k2eNgMnSc7d1	nelegitimní
potomkem	potomek	k1gMnSc7	potomek
a	a	k8xC	a
poté	poté	k6eAd1	poté
nebyla	být	k5eNaImAgFnS	být
podle	podle	k7c2	podle
katolických	katolický	k2eAgInPc2d1	katolický
zákonů	zákon	k1gInPc2	zákon
legitimizována	legitimizován	k2eAgFnSc1d1	legitimizován
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nepřátelským	přátelský	k2eNgInPc3d1	nepřátelský
postojům	postoj	k1gInPc3	postoj
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nárokům	nárok	k1gInPc3	nárok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
činil	činit	k5eAaImAgInS	činit
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
vdovec	vdovec	k1gMnSc1	vdovec
po	po	k7c6	po
Marii	Maria	k1gFnSc6	Maria
I.	I.	kA	I.
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
velkým	velký	k2eAgInSc7d1	velký
zdrojem	zdroj	k1gInSc7	zdroj
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
pohledu	pohled	k1gInSc2	pohled
učinil	učinit	k5eAaPmAgInS	učinit
právoplatnou	právoplatný	k2eAgFnSc7d1	právoplatná
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
rádci	rádce	k1gMnPc1	rádce
vnímali	vnímat	k5eAaImAgMnP	vnímat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tak	tak	k9	tak
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
přijat	přijmout	k5eAaPmNgInS	přijmout
protestantismus	protestantismus	k1gInSc1	protestantismus
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
dalšího	další	k2eAgInSc2d1	další
rozvoje	rozvoj	k1gInSc2	rozvoj
Anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
prvky	prvek	k1gInPc7	prvek
katolicismu	katolicismus	k1gInSc2	katolicismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
radikální	radikální	k2eAgInSc1d1	radikální
puritánský	puritánský	k2eAgInSc1d1	puritánský
pohled	pohled	k1gInSc1	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
začal	začít	k5eAaPmAgInS	začít
parlament	parlament	k1gInSc1	parlament
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
zákonech	zákon	k1gInPc6	zákon
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
reformách	reforma	k1gFnPc6	reforma
započatých	započatý	k2eAgFnPc2d1	započatá
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
panovníkem	panovník	k1gMnSc7	panovník
jako	jako	k8xC	jako
vrchním	vrchní	k2eAgMnSc7d1	vrchní
představitelem	představitel	k1gMnSc7	představitel
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
záměry	záměra	k1gFnPc1	záměra
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
podporou	podpora	k1gFnSc7	podpora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
měla	mít	k5eAaImAgFnS	mít
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
když	když	k8xS	když
využila	využít	k5eAaPmAgFnS	využít
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
některých	některý	k3yIgMnPc2	některý
církevních	církevní	k2eAgMnPc2d1	církevní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
canterburského	canterburský	k2eAgMnSc2d1	canterburský
<g/>
,	,	kIx,	,
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
záměry	záměr	k1gInPc4	záměr
prosadila	prosadit	k5eAaPmAgFnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
svrchovanosti	svrchovanost	k1gFnSc6	svrchovanost
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1559	[number]	k4	1559
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
veřejní	veřejný	k2eAgMnPc1d1	veřejný
činitelé	činitel	k1gMnPc1	činitel
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
přísahat	přísahat	k5eAaImF	přísahat
oddanost	oddanost	k1gFnSc4	oddanost
panovnici	panovnice	k1gFnSc4	panovnice
jako	jako	k8xC	jako
vrchnímu	vrchní	k2eAgMnSc3d1	vrchní
hodnostáři	hodnostář	k1gMnSc3	hodnostář
Anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
vystavili	vystavit	k5eAaPmAgMnP	vystavit
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
zbavení	zbavení	k1gNnSc2	zbavení
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zákony	zákon	k1gInPc4	zákon
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
kacířství	kacířství	k1gNnSc4	kacířství
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
neopakovala	opakovat	k5eNaImAgFnS	opakovat
perzekuce	perzekuce	k1gFnSc1	perzekuce
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
jednotě	jednota	k1gFnSc6	jednota
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
povinnou	povinný	k2eAgFnSc4d1	povinná
návštěvu	návštěva	k1gFnSc4	návštěva
anglikánských	anglikánský	k2eAgFnPc2d1	anglikánská
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
a	a	k8xC	a
povinnost	povinnost	k1gFnSc4	povinnost
používání	používání	k1gNnSc2	používání
Knihy	kniha	k1gFnSc2	kniha
společných	společný	k2eAgFnPc2d1	společná
modliteb	modlitba	k1gFnPc2	modlitba
(	(	kIx(	(
<g/>
Book	Book	k1gMnSc1	Book
of	of	k?	of
Common	Common	k1gMnSc1	Common
Prayer	Prayer	k1gMnSc1	Prayer
<g/>
)	)	kIx)	)
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
revidované	revidovaný	k2eAgFnSc2d1	revidovaná
roku	rok	k1gInSc2	rok
1552	[number]	k4	1552
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otázka	otázka	k1gFnSc1	otázka
sňatku	sňatek	k1gInSc2	sňatek
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
byl	být	k5eAaImAgInS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
otázkou	otázka	k1gFnSc7	otázka
její	její	k3xOp3gInSc4	její
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neprovdala	provdat	k5eNaPmAgFnS	provdat
a	a	k8xC	a
důvod	důvod	k1gInSc1	důvod
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gInSc4	její
zážitek	zážitek	k1gInSc4	zážitek
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
Seymourem	Seymour	k1gMnSc7	Seymour
<g/>
.	.	kIx.	.
</s>
<s>
Nepotřebovala	potřebovat	k5eNaImAgFnS	potřebovat
muže	muž	k1gMnSc4	muž
jako	jako	k8xC	jako
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
sňatkem	sňatek	k1gInSc7	sňatek
by	by	kYmCp3nS	by
riskovala	riskovat	k5eAaBmAgFnS	riskovat
ztrátu	ztráta	k1gFnSc4	ztráta
kontroly	kontrola	k1gFnSc2	kontrola
nebo	nebo	k8xC	nebo
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
politiky	politika	k1gFnSc2	politika
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
by	by	kYmCp3nS	by
její	její	k3xOp3gInSc1	její
sňatek	sňatek	k1gInSc1	sňatek
mohl	moct	k5eAaImAgInS	moct
přinést	přinést	k5eAaPmF	přinést
následníka	následník	k1gMnSc4	následník
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
ale	ale	k8xC	ale
možná	možná	k9	možná
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
neplodná	plodný	k2eNgFnSc1d1	neplodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
dostávala	dostávat	k5eAaImAgFnS	dostávat
nabídky	nabídka	k1gFnPc4	nabídka
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
reálně	reálně	k6eAd1	reálně
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
Robert	Robert	k1gMnSc1	Robert
Dudley	Dudlea	k1gFnSc2	Dudlea
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
pouze	pouze	k6eAd1	pouze
baron	baron	k1gMnSc1	baron
z	z	k7c2	z
Denbigh	Denbigha	k1gFnPc2	Denbigha
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Northumberlandu	Northumberland	k1gInSc2	Northumberland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
její	její	k3xOp3gMnSc1	její
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
milenec	milenec	k1gMnSc1	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Amy	Amy	k1gMnPc2	Amy
Robsart	Robsarta	k1gFnPc2	Robsarta
zemřela	zemřít	k5eAaPmAgFnS	zemřít
za	za	k7c2	za
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
pádem	pád	k1gInSc7	pád
ze	z	k7c2	z
schodů	schod	k1gInPc2	schod
se	s	k7c7	s
zlomeným	zlomený	k2eAgInSc7d1	zlomený
vazem	vaz	k1gInSc7	vaz
<g/>
,	,	kIx,	,
propukl	propuknout	k5eAaPmAgInS	propuknout
skandál	skandál	k1gInSc1	skandál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nepřekonatelnou	překonatelný	k2eNgFnSc7d1	nepřekonatelná
překážkou	překážka	k1gFnSc7	překážka
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
společný	společný	k2eAgInSc4d1	společný
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Robert	Robert	k1gMnSc1	Robert
zabil	zabít	k5eAaPmAgMnS	zabít
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
volný	volný	k2eAgInSc1d1	volný
pro	pro	k7c4	pro
Alžbětu	Alžběta	k1gFnSc4	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
nijak	nijak	k6eAd1	nijak
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Robert	Robert	k1gMnSc1	Robert
měl	mít	k5eAaImAgMnS	mít
skutečně	skutečně	k6eAd1	skutečně
něco	něco	k6eAd1	něco
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
smrtí	smrt	k1gFnSc7	smrt
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
zpráv	zpráva	k1gFnPc2	zpráva
byla	být	k5eAaImAgFnS	být
Amy	Amy	k1gFnSc1	Amy
těžce	těžce	k6eAd1	těžce
nemocná	mocný	k2eNgFnSc1d1	mocný
s	s	k7c7	s
rakovinou	rakovina	k1gFnSc7	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgInPc4d1	nedávný
pokroky	pokrok	k1gInPc4	pokrok
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
spontánní	spontánní	k2eAgFnPc4d1	spontánní
zlomeniny	zlomenina	k1gFnPc4	zlomenina
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Chůze	chůze	k1gFnPc1	chůze
ze	z	k7c2	z
schodů	schod	k1gInPc2	schod
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
dostačující	dostačující	k2eAgNnPc1d1	dostačující
na	na	k7c4	na
zlomeninu	zlomenina	k1gFnSc4	zlomenina
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Lady	lady	k1gFnSc4	lady
Dudley	Dudlea	k1gFnSc2	Dudlea
stala	stát	k5eAaPmAgFnS	stát
fatální	fatální	k2eAgFnSc1d1	fatální
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
vážně	vážně	k6eAd1	vážně
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c6	o
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Dudleym	Dudleym	k1gInSc4	Dudleym
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
oba	dva	k4xCgInPc4	dva
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1575	[number]	k4	1575
<g/>
,	,	kIx,	,
během	během	k7c2	během
divadelního	divadelní	k2eAgNnSc2d1	divadelní
představení	představení	k1gNnSc2	představení
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Kenilworth	Kenilwortha	k1gFnPc2	Kenilwortha
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
Robert	Robert	k1gMnSc1	Robert
naposledy	naposledy	k6eAd1	naposledy
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jej	on	k3xPp3gInSc4	on
znovu	znovu	k6eAd1	znovu
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Dudley	Dudlea	k1gFnPc1	Dudlea
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Leicesteru	Leicester	k1gInSc2	Leicester
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
Alžbětiným	Alžbětin	k2eAgMnSc7d1	Alžbětin
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
aféře	aféra	k1gFnSc6	aféra
s	s	k7c7	s
Dudleym	Dudleymum	k1gNnPc2	Dudleymum
se	se	k3xPyFc4	se
Alžběta	Alžběta	k1gFnSc1	Alžběta
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
případnému	případný	k2eAgInSc3d1	případný
sňatku	sňatek	k1gInSc3	sňatek
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
zdrženlivě	zdrženlivě	k6eAd1	zdrženlivě
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
naléhal	naléhat	k5eAaImAgMnS	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vybrala	vybrat	k5eAaPmAgFnS	vybrat
ženicha	ženich	k1gMnSc4	ženich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
vyhýbavě	vyhýbavě	k6eAd1	vyhýbavě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
nemoci	nemoc	k1gFnSc6	nemoc
parlament	parlament	k1gInSc1	parlament
požadoval	požadovat	k5eAaImAgInS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
vdala	vdát	k5eAaPmAgFnS	vdát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
určila	určit	k5eAaPmAgFnS	určit
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
vyloučilo	vyloučit	k5eAaPmAgNnS	vyloučit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
nátlak	nátlak	k1gInSc4	nátlak
odročila	odročit	k5eAaPmAgFnS	odročit
jednání	jednání	k1gNnSc3	jednání
parlamentu	parlament	k1gInSc2	parlament
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1566	[number]	k4	1566
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
zvýšit	zvýšit	k5eAaPmF	zvýšit
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
starším	starý	k2eAgMnPc3d2	starší
členům	člen	k1gMnPc3	člen
vlády	vláda	k1gFnSc2	vláda
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neprovdá	provdat	k5eNaPmIp3nS	provdat
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
rádce	rádce	k1gMnSc1	rádce
William	William	k1gInSc4	William
Cecil	Cecil	k1gMnSc1	Cecil
začal	začít	k5eAaPmAgMnS	začít
hledat	hledat	k5eAaImF	hledat
řešení	řešení	k1gNnSc4	řešení
nástupnického	nástupnický	k2eAgInSc2d1	nástupnický
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
neochota	neochota	k1gFnSc1	neochota
jmenovat	jmenovat	k5eAaImF	jmenovat
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
ale	ale	k8xC	ale
zajistila	zajistit	k5eAaPmAgFnS	zajistit
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
jejího	její	k3xOp3gNnSc2	její
postavení	postavení	k1gNnSc2	postavení
proti	proti	k7c3	proti
možnému	možný	k2eAgNnSc3d1	možné
spiknutí	spiknutí	k1gNnSc3	spiknutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nervozita	nervozita	k1gFnSc1	nervozita
obklopující	obklopující	k2eAgFnSc4d1	obklopující
nevyřešenou	vyřešený	k2eNgFnSc4d1	nevyřešená
otázku	otázka	k1gFnSc4	otázka
následnictví	následnictví	k1gNnSc2	následnictví
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
patrná	patrný	k2eAgFnSc1d1	patrná
i	i	k9	i
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
drama	drama	k1gNnSc1	drama
Gorboduc	Gorboduc	k1gInSc1	Gorboduc
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
též	též	k9	též
Ferrex	Ferrex	k1gInSc4	Ferrex
and	and	k?	and
Porrex	Porrex	k1gInSc1	Porrex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
Thomas	Thomas	k1gMnSc1	Thomas
Sackville	Sackville	k1gFnSc2	Sackville
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Northon	Northon	k1gMnSc1	Northon
<g/>
,	,	kIx,	,
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
hrou	hra	k1gFnSc7	hra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navíc	navíc	k6eAd1	navíc
využívá	využívat	k5eAaPmIp3nS	využívat
legendy	legenda	k1gFnPc1	legenda
o	o	k7c6	o
králi	král	k1gMnSc6	král
Artušovi	Artuš	k1gMnSc6	Artuš
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
otázkou	otázka	k1gFnSc7	otázka
následnictví	následnictví	k1gNnSc2	následnictví
je	být	k5eAaImIp3nS	být
The	The	k1gMnSc1	The
Misfortunes	Misfortunes	k1gMnSc1	Misfortunes
of	of	k?	of
Arthur	Arthur	k1gMnSc1	Arthur
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
,	,	kIx,	,
1587	[number]	k4	1587
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
politiky	politika	k1gFnSc2	politika
vůči	vůči	k7c3	vůči
Skotsku	Skotsko	k1gNnSc3	Skotsko
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
odchodu	odchod	k1gInSc2	odchod
Francouzů	Francouz	k1gMnPc2	Francouz
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obávala	obávat	k5eAaImAgFnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
chce	chtít	k5eAaImIp3nS	chtít
napadnout	napadnout	k5eAaPmF	napadnout
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
dosadit	dosadit	k5eAaPmF	dosadit
skotskou	skotský	k2eAgFnSc4d1	skotská
královnu	královna	k1gFnSc4	královna
Marii	Maria	k1gFnSc4	Maria
Stuartovnu	Stuartovna	k1gFnSc4	Stuartovna
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rada	rada	k1gFnSc1	rada
ji	on	k3xPp3gFnSc4	on
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyslala	vyslat	k5eAaPmAgFnS	vyslat
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
skotských	skotská	k1gFnPc2	skotská
protestantských	protestantský	k2eAgMnPc2d1	protestantský
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
výsledky	výsledek	k1gInPc1	výsledek
tažení	tažení	k1gNnSc2	tažení
nebyly	být	k5eNaImAgInP	být
přesvědčivé	přesvědčivý	k2eAgInPc1d1	přesvědčivý
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hrozby	hrozba	k1gFnSc2	hrozba
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
vrátila	vrátit	k5eAaPmAgFnS	vrátit
roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
země	země	k1gFnSc1	země
stabilizovanou	stabilizovaný	k2eAgFnSc4d1	stabilizovaná
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
řízena	řídit	k5eAaImNgFnS	řídit
radou	rada	k1gFnSc7	rada
protestantsky	protestantsky	k6eAd1	protestantsky
zaměřených	zaměřený	k2eAgMnPc2d1	zaměřený
šlechticů	šlechtic	k1gMnPc2	šlechtic
podporovaných	podporovaný	k2eAgFnPc2d1	podporovaná
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
ratifikovat	ratifikovat	k5eAaBmF	ratifikovat
mírovou	mírový	k2eAgFnSc4d1	mírová
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1565	[number]	k4	1565
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Stuarta	Stuart	k1gMnSc4	Stuart
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
uchazečem	uchazeč	k1gMnSc7	uchazeč
o	o	k7c4	o
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
nepopulárním	populární	k2eNgMnSc7d1	nepopulární
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1567	[number]	k4	1567
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
spiklenci	spiklenec	k1gMnPc7	spiklenec
vedenými	vedený	k2eAgFnPc7d1	vedená
Jamesem	James	k1gMnSc7	James
Hepburnem	Hepburn	k1gInSc7	Hepburn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
Marie	Marie	k1gFnSc1	Marie
provdala	provdat	k5eAaPmAgFnS	provdat
a	a	k8xC	a
zavdala	zavdat	k5eAaPmAgFnS	zavdat
tak	tak	k6eAd1	tak
příčinu	příčina	k1gFnSc4	příčina
k	k	k7c3	k
podezření	podezření	k1gNnSc3	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Stuartově	Stuartův	k2eAgFnSc6d1	Stuartova
vraždě	vražda	k1gFnSc6	vražda
podílela	podílet	k5eAaImAgFnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
Mariině	Mariin	k2eAgFnSc3d1	Mariina
porážce	porážka	k1gFnSc3	porážka
a	a	k8xC	a
uvěznění	uvěznění	k1gNnSc3	uvěznění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
nechtěla	chtít	k5eNaImAgFnS	chtít
angažovat	angažovat	k5eAaBmF	angažovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
nechala	nechat	k5eAaPmAgFnS	nechat
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
záminkou	záminka	k1gFnSc7	záminka
několika	několik	k4yIc2	několik
vzpour	vzpoura	k1gFnPc2	vzpoura
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
asi	asi	k9	asi
neangažovala	angažovat	k5eNaBmAgFnS	angažovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
katolických	katolický	k2eAgInPc6d1	katolický
nepokojích	nepokoj	k1gInPc6	nepokoj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gFnSc4	její
dosazení	dosazení	k1gNnSc1	dosazení
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1571	[number]	k4	1571
až	až	k9	až
1586	[number]	k4	1586
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětin	Alžbětin	k2eAgMnSc1d1	Alžbětin
hlavní	hlavní	k2eAgMnSc1d1	hlavní
vyzvědač	vyzvědač	k1gMnSc1	vyzvědač
Francis	Francis	k1gFnSc2	Francis
Walsingham	Walsingham	k1gInSc1	Walsingham
avšak	avšak	k8xC	avšak
předložil	předložit	k5eAaPmAgInS	předložit
dopisy	dopis	k1gInPc4	dopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Marie	Marie	k1gFnSc1	Marie
poslala	poslat	k5eAaPmAgFnS	poslat
vzbouřencům	vzbouřenec	k1gMnPc3	vzbouřenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
královské	královský	k2eAgFnSc3d1	královská
radě	rada	k1gFnSc3	rada
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Alžbětu	Alžběta	k1gFnSc4	Alžběta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
souzena	soudit	k5eAaImNgFnS	soudit
za	za	k7c4	za
zradu	zrada	k1gFnSc4	zrada
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1587	[number]	k4	1587
popravena	popravit	k5eAaPmNgFnS	popravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
okupaci	okupace	k1gFnSc6	okupace
Le	Le	k1gFnSc2	Le
Havre	Havr	k1gInSc5	Havr
byla	být	k5eAaImAgFnS	být
další	další	k2eAgFnSc7d1	další
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
výpravou	výprava	k1gFnSc7	výprava
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
podpora	podpora	k1gFnSc1	podpora
protestantských	protestantský	k2eAgMnPc2d1	protestantský
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1584	[number]	k4	1584
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
alianci	aliance	k1gFnSc3	aliance
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
ligou	liga	k1gFnSc7	liga
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
vliv	vliv	k1gInSc1	vliv
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
invaze	invaze	k1gFnSc2	invaze
Španělů	Španěl	k1gMnPc2	Španěl
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
reagovala	reagovat	k5eAaBmAgFnS	reagovat
uzavřením	uzavření	k1gNnSc7	uzavření
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Nonsuchu	Nonsuch	k1gInSc2	Nonsuch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
zahájením	zahájení	k1gNnSc7	zahájení
anglo-španělské	anglo-španělský	k2eAgFnSc2d1	anglo-španělský
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
podpisem	podpis	k1gInSc7	podpis
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
1588	[number]	k4	1588
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
španělská	španělský	k2eAgFnSc1d1	španělská
Armada	Armada	k1gFnSc1	Armada
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
flotila	flotila	k1gFnSc1	flotila
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořila	podpořit	k5eAaPmAgFnS	podpořit
španělskou	španělský	k2eAgFnSc4d1	španělská
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Anglie	Anglie	k1gFnSc2	Anglie
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Parmy	Parma	k1gFnSc2	Parma
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
špatného	špatný	k2eAgInSc2d1	špatný
odhadu	odhad	k1gInSc2	odhad
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
nešťastných	šťastný	k2eNgFnPc2d1	nešťastná
náhod	náhoda	k1gFnPc2	náhoda
a	a	k8xC	a
útoku	útok	k1gInSc2	útok
zápalných	zápalný	k2eAgFnPc2d1	zápalná
anglických	anglický	k2eAgFnPc2d1	anglická
lodí	loď	k1gFnPc2	loď
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
španělská	španělský	k2eAgFnSc1d1	španělská
flotila	flotila	k1gFnSc1	flotila
velké	velký	k2eAgFnSc2d1	velká
ztráty	ztráta	k1gFnSc2	ztráta
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gravelines	Gravelinesa	k1gFnPc2	Gravelinesa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
útok	útok	k1gInSc4	útok
zápalných	zápalný	k2eAgFnPc2d1	zápalná
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
vyrazily	vyrazit	k5eAaPmAgInP	vyrazit
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
a	a	k8xC	a
podél	podél	k7c2	podél
irského	irský	k2eAgNnSc2d1	irské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
flotila	flotila	k1gFnSc1	flotila
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
další	další	k2eAgFnPc4d1	další
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
dorazily	dorazit	k5eAaPmAgInP	dorazit
jen	jen	k6eAd1	jen
zbytky	zbytek	k1gInPc1	zbytek
původní	původní	k2eAgFnSc2d1	původní
mocné	mocný	k2eAgFnSc2d1	mocná
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
si	se	k3xPyFc3	se
nebyli	být	k5eNaImAgMnP	být
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
ztráty	ztráta	k1gFnPc4	ztráta
Španělé	Španěl	k1gMnPc1	Španěl
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
a	a	k8xC	a
tak	tak	k6eAd1	tak
chystali	chystat	k5eAaImAgMnP	chystat
obranu	obrana	k1gFnSc4	obrana
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
invazi	invaze	k1gFnSc3	invaze
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
konány	konán	k2eAgFnPc4d1	konána
děkovné	děkovný	k2eAgFnPc4d1	děkovná
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
španělského	španělský	k2eAgNnSc2d1	španělské
vojska	vojsko	k1gNnSc2	vojsko
byla	být	k5eAaImAgFnS	být
využita	využít	k5eAaPmNgFnS	využít
jako	jako	k8xC	jako
oslava	oslava	k1gFnSc1	oslava
Alžběty	Alžběta	k1gFnSc2	Alžběta
i	i	k8xC	i
protestantské	protestantský	k2eAgFnSc2d1	protestantská
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
vítězství	vítězství	k1gNnSc1	vítězství
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
konec	konec	k1gInSc4	konec
války	válka	k1gFnSc2	válka
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dále	daleko	k6eAd2	daleko
ovládalo	ovládat	k5eAaImAgNnS	ovládat
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
hrozilo	hrozit	k5eAaImAgNnS	hrozit
případnou	případný	k2eAgFnSc7d1	případná
další	další	k2eAgFnSc7d1	další
invazí	invaze	k1gFnSc7	invaze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
protestant	protestant	k1gMnSc1	protestant
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
poslala	poslat	k5eAaPmAgFnS	poslat
mu	on	k3xPp3gMnSc3	on
Alžběta	Alžběta	k1gFnSc1	Alžběta
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
nástup	nástup	k1gInSc1	nástup
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odporem	odpor	k1gInSc7	odpor
katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
obávala	obávat	k5eAaImAgFnS	obávat
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
francouzských	francouzský	k2eAgInPc2d1	francouzský
přístavů	přístav	k1gInPc2	přístav
Španěly	Španěly	k1gInPc4	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tažení	tažení	k1gNnSc1	tažení
bylo	být	k5eAaImAgNnS	být
neúčinné	účinný	k2eNgNnSc1d1	neúčinné
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Willoughby	Willoughba	k1gFnSc2	Willoughba
neuposlechl	uposlechnout	k5eNaPmAgMnS	uposlechnout
Alžbětiny	Alžbětin	k2eAgInPc4d1	Alžbětin
příkazy	příkaz	k1gInPc4	příkaz
a	a	k8xC	a
potloukal	potloukat	k5eAaImAgMnS	potloukat
se	s	k7c7	s
severní	severní	k2eAgFnSc7d1	severní
oblastí	oblast	k1gFnSc7	oblast
Francie	Francie	k1gFnSc2	Francie
s	s	k7c7	s
4000	[number]	k4	4000
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
neúspěšné	úspěšný	k2eNgNnSc4d1	neúspěšné
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vojsko	vojsko	k1gNnSc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Johna	John	k1gMnSc2	John
Nerreyse	Nerreys	k1gMnSc2	Nerreys
v	v	k7c6	v
Bretani	Bretaň	k1gFnSc6	Bretaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
tažení	tažení	k1gNnSc6	tažení
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
její	její	k3xOp3gFnSc1	její
ztráta	ztráta	k1gFnSc1	ztráta
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
veliteli	velitel	k1gMnPc7	velitel
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
vládla	vládnout	k5eAaImAgFnS	vládnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
katoličtí	katolický	k2eAgMnPc1d1	katolický
obyvatelé	obyvatel	k1gMnPc1	obyvatel
využívali	využívat	k5eAaPmAgMnP	využívat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
možnost	možnost	k1gFnSc4	možnost
spiknutí	spiknutí	k1gNnSc2	spiknutí
s	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
zde	zde	k6eAd1	zde
politiku	politika	k1gFnSc4	politika
darování	darování	k1gNnSc2	darování
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
svým	svůj	k3xOyFgMnPc3	svůj
dvořanům	dvořan	k1gMnPc3	dvořan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
omezila	omezit	k5eAaPmAgFnS	omezit
možnost	možnost	k1gFnSc4	možnost
vytvoření	vytvoření	k1gNnSc2	vytvoření
základny	základna	k1gFnSc2	základna
vzpour	vzpoura	k1gFnPc2	vzpoura
podporovaných	podporovaný	k2eAgFnPc2d1	podporovaná
Španěly	Španěly	k1gInPc4	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potlačování	potlačování	k1gNnSc6	potlačování
vzpour	vzpoura	k1gFnPc2	vzpoura
byla	být	k5eAaImAgFnS	být
uplatňována	uplatňován	k2eAgFnSc1d1	uplatňována
taktika	taktika	k1gFnSc1	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
a	a	k8xC	a
zabíjení	zabíjení	k1gNnSc4	zabíjení
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
při	při	k7c6	při
potlačení	potlačení	k1gNnSc6	potlačení
vzpoury	vzpoura	k1gFnSc2	vzpoura
roku	rok	k1gInSc2	rok
1582	[number]	k4	1582
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
Irů	Ir	k1gMnPc2	Ir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1594	[number]	k4	1594
až	až	k9	až
1603	[number]	k4	1603
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
velká	velký	k2eAgFnSc1d1	velká
vzpoura	vzpoura	k1gFnSc1	vzpoura
vedená	vedený	k2eAgFnSc1d1	vedená
Hughem	Hugh	k1gInSc7	Hugh
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neilem	Neil	k1gMnSc7	Neil
<g/>
,	,	kIx,	,
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
vyslala	vyslat	k5eAaPmAgFnS	vyslat
Alžběta	Alžběta	k1gFnSc1	Alžběta
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
vojsko	vojsko	k1gNnSc1	vojsko
vedené	vedený	k2eAgFnSc2d1	vedená
jejím	její	k3xOp3gMnSc7	její
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
Robertem	Robert	k1gMnSc7	Robert
Devereuxem	Devereux	k1gInSc7	Devereux
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Essexu	Essex	k1gInSc2	Essex
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
výsledku	výsledek	k1gInSc2	výsledek
a	a	k8xC	a
bez	bez	k7c2	bez
jejího	její	k3xOp3gNnSc2	její
svolení	svolení	k1gNnSc2	svolení
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Charlesem	Charles	k1gMnSc7	Charles
Blountem	Blount	k1gMnSc7	Blount
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
trvalo	trvalo	k1gNnSc1	trvalo
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
vzbouřence	vzbouřenec	k1gMnSc4	vzbouřenec
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
malé	malý	k2eAgInPc4d1	malý
politické	politický	k2eAgInPc4d1	politický
neúspěchy	neúspěch	k1gInPc4	neúspěch
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
"	"	kIx"	"
<g/>
neporazitelné	porazitelný	k2eNgFnSc2d1	neporazitelná
<g/>
"	"	kIx"	"
španělské	španělský	k2eAgFnSc2d1	španělská
Armady	Armada	k1gFnSc2	Armada
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
živoucí	živoucí	k2eAgFnSc7d1	živoucí
legendou	legenda	k1gFnSc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Zkáza	zkáza	k1gFnSc1	zkáza
loďstva	loďstvo	k1gNnSc2	loďstvo
byla	být	k5eAaImAgFnS	být
nejvíce	nejvíce	k6eAd1	nejvíce
ponižující	ponižující	k2eAgFnSc7d1	ponižující
porážkou	porážka	k1gFnSc7	porážka
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
historii	historie	k1gFnSc6	historie
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
naopak	naopak	k6eAd1	naopak
poté	poté	k6eAd1	poté
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
období	období	k1gNnSc2	období
míru	mír	k1gInSc2	mír
a	a	k8xC	a
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alžbětin	Alžbětin	k2eAgMnSc1d1	Alžbětin
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
rádce	rádce	k1gMnSc1	rádce
William	William	k1gInSc4	William
Cecil	Cecil	k1gMnSc1	Cecil
zemřel	zemřít	k5eAaPmAgMnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1598	[number]	k4	1598
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
úřad	úřad	k1gInSc4	úřad
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Robert	Robert	k1gMnSc1	Robert
Cecil	Cecil	k1gMnSc1	Cecil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgMnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
nebyla	být	k5eNaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
jmenovat	jmenovat	k5eAaImF	jmenovat
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Cecil	Cecil	k1gMnSc1	Cecil
začal	začít	k5eAaPmAgMnS	začít
připravovat	připravovat	k5eAaImF	připravovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
nekonfliktní	konfliktní	k2eNgNnSc4d1	nekonfliktní
předání	předání	k1gNnSc4	předání
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgInS	navázat
tajná	tajný	k2eAgNnPc4d1	tajné
jednání	jednání	k1gNnPc4	jednání
se	s	k7c7	s
skotským	skotský	k1gInSc7	skotský
králem	král	k1gMnSc7	král
Jakubem	Jakub	k1gMnSc7	Jakub
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc4d1	velký
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Cecil	Cecil	k1gMnSc1	Cecil
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Jakubovi	Jakub	k1gMnSc3	Jakub
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
na	na	k7c4	na
Alžbětu	Alžběta	k1gFnSc4	Alžběta
působit	působit	k5eAaImF	působit
přátelským	přátelský	k2eAgInSc7d1	přátelský
dojmem	dojem	k1gInSc7	dojem
a	a	k8xC	a
naklonil	naklonit	k5eAaPmAgMnS	naklonit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
sama	sám	k3xTgMnSc4	sám
však	však	k9	však
možná	možná	k6eAd1	možná
plánovala	plánovat	k5eAaImAgFnS	plánovat
určit	určit	k5eAaPmF	určit
svého	svůj	k3xOyFgMnSc4	svůj
vzdáleného	vzdálený	k2eAgMnSc4d1	vzdálený
bratrance	bratranec	k1gMnSc4	bratranec
Jakuba	Jakub	k1gMnSc2	Jakub
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alžbětin	Alžbětin	k2eAgInSc1d1	Alžbětin
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
až	až	k9	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1602	[number]	k4	1602
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
série	série	k1gFnSc2	série
úmrtí	úmrtí	k1gNnSc2	úmrtí
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gMnPc7	její
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
známými	známá	k1gFnPc7	známá
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
deprese	deprese	k1gFnSc1	deprese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
stav	stav	k1gInSc4	stav
byl	být	k5eAaImAgInS	být
poznamenán	poznamenán	k2eAgInSc1d1	poznamenán
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
obdobím	období	k1gNnSc7	období
záchvatů	záchvat	k1gInPc2	záchvat
melancholie	melancholie	k1gFnSc2	melancholie
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1603	[number]	k4	1603
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
Robert	Robert	k1gMnSc1	Robert
Cecil	Cecil	k1gMnSc1	Cecil
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
rada	rada	k1gFnSc1	rada
Jakuba	Jakub	k1gMnSc2	Jakub
králem	král	k1gMnSc7	král
Anglie	Anglie	k1gFnSc2	Anglie
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Očekávání	očekávání	k1gNnSc4	očekávání
od	od	k7c2	od
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
byla	být	k5eAaImAgFnS	být
veliká	veliký	k2eAgFnSc1d1	veliká
a	a	k8xC	a
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
míru	mír	k1gInSc2	mír
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
daní	daň	k1gFnPc2	daň
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
naplněna	naplněn	k2eAgFnSc1d1	naplněna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Roberta	Robert	k1gMnSc2	Robert
Cecila	Cecil	k1gMnSc2	Cecil
roku	rok	k1gInSc2	rok
1612	[number]	k4	1612
byla	být	k5eAaImAgFnS	být
uplatňována	uplatňován	k2eAgFnSc1d1	uplatňována
stávající	stávající	k2eAgFnSc1d1	stávající
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
poté	poté	k6eAd1	poté
Jakub	Jakub	k1gMnSc1	Jakub
začal	začít	k5eAaPmAgMnS	začít
stranit	stranit	k5eAaImF	stranit
svým	svůj	k3xOyFgMnPc3	svůj
příznivcům	příznivec	k1gMnPc3	příznivec
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
Alžbětino	Alžbětin	k2eAgNnSc4d1	Alžbětino
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
oslavována	oslavovat	k5eAaImNgFnS	oslavovat
jako	jako	k8xS	jako
protestantská	protestantský	k2eAgFnSc1d1	protestantská
hrdinka	hrdinka	k1gFnSc1	hrdinka
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
byl	být	k5eAaImAgMnS	být
popisován	popisovat	k5eAaImNgMnS	popisovat
jako	jako	k8xC	jako
sympatizant	sympatizant	k1gMnSc1	sympatizant
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
podezírána	podezírán	k2eAgFnSc1d1	podezírána
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
se	se	k3xPyFc4	se
také	také	k9	také
zintenzivnila	zintenzivnit	k5eAaPmAgFnS	zintenzivnit
snaha	snaha	k1gFnSc1	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
první	první	k4xOgNnSc4	první
trvale	trvale	k6eAd1	trvale
osídlenou	osídlený	k2eAgFnSc4d1	osídlená
anglickou	anglický	k2eAgFnSc4d1	anglická
kolonii	kolonie	k1gFnSc4	kolonie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Realizace	realizace	k1gFnSc1	realizace
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
sir	sir	k1gMnSc1	sir
Walter	Walter	k1gMnSc1	Walter
Raleigh	Raleigh	k1gMnSc1	Raleigh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc4	jeho
plány	plán	k1gInPc4	plán
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
území	území	k1gNnSc4	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
oblast	oblast	k1gFnSc1	oblast
současné	současný	k2eAgFnSc2d1	současná
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
a	a	k8xC	a
Virgínie	Virgínie	k1gFnSc2	Virgínie
<g/>
)	)	kIx)	)
skončily	skončit	k5eAaPmAgFnP	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
Anglikánskou	anglikánský	k2eAgFnSc4d1	anglikánská
církev	církev	k1gFnSc4	církev
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
národního	národní	k2eAgNnSc2d1	národní
uvědomění	uvědomění	k1gNnSc2	uvědomění
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
španělské	španělský	k2eAgFnSc2d1	španělská
Armady	Armada	k1gFnSc2	Armada
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
obávanou	obávaný	k2eAgFnSc7d1	obávaná
a	a	k8xC	a
respektovanou	respektovaný	k2eAgFnSc7d1	respektovaná
panovnicí	panovnice	k1gFnSc7	panovnice
i	i	k8xC	i
u	u	k7c2	u
takových	takový	k3xDgFnPc2	takový
zemí	zem	k1gFnPc2	zem
jako	jako	k8xC	jako
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
její	její	k3xOp3gFnSc7	její
vládou	vláda	k1gFnSc7	vláda
získal	získat	k5eAaPmAgInS	získat
anglický	anglický	k2eAgInSc1d1	anglický
národ	národ	k1gInSc1	národ
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
a	a	k8xC	a
pocit	pocit	k1gInSc1	pocit
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Angličanům	Angličan	k1gMnPc3	Angličan
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
až	až	k9	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
tudorovským	tudorovský	k2eAgInSc7d1	tudorovský
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
míněním	mínění	k1gNnSc7	mínění
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
svých	svůj	k3xOyFgMnPc2	svůj
rádců	rádce	k1gMnPc2	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
panovnic	panovnice	k1gFnPc2	panovnice
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odrazy	odraz	k1gInPc1	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
–	–	k?	–
koprodukční	koprodukční	k2eAgInSc4d1	koprodukční
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
britský	britský	k2eAgInSc1d1	britský
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
:	:	kIx,	:
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
–	–	k?	–
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
I	i	k9	i
of	of	k?	of
England	Englanda	k1gFnPc2	Englanda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KOVÁŘ	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
léta	léto	k1gNnPc1	léto
alžbětinské	alžbětinský	k2eAgFnSc2d1	Alžbětinská
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
10	[number]	k4	10
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVÁŘ	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětinská	alžbětinský	k2eAgFnSc1d1	Alžbětinská
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Stabilizace	stabilizace	k1gFnSc1	stabilizace
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
1561	[number]	k4	1561
<g/>
-	-	kIx~	-
<g/>
1567	[number]	k4	1567
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
250	[number]	k4	250
<g/>
-	-	kIx~	-
<g/>
256	[number]	k4	256
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVÁŘ	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Shaping	Shaping	k1gInSc1	Shaping
of	of	k?	of
the	the	k?	the
Elisabethan	Elisabethan	k1gInSc1	Elisabethan
Monarchy	monarcha	k1gMnSc2	monarcha
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
Some	Some	k1gFnPc3	Some
Aspects	Aspects	k1gInSc1	Aspects
of	of	k?	of
the	the	k?	the
Forming	Forming	k1gInSc1	Forming
<g/>
,	,	kIx,	,
Functioning	Functioning	k1gInSc1	Functioning
and	and	k?	and
Problems	Problems	k1gInSc1	Problems
of	of	k?	of
Early	earl	k1gMnPc4	earl
Elisabethan	Elisabethan	k1gMnSc1	Elisabethan
State	status	k1gInSc5	status
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1561	[number]	k4	1561
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85899	[number]	k4	85899
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVÁŘ	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Struggle	Struggle	k1gFnSc1	Struggle
for	forum	k1gNnPc2	forum
Character	Charactrum	k1gNnPc2	Charactrum
of	of	k?	of
the	the	k?	the
State	status	k1gInSc5	status
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Transformations	Transformations	k1gInSc1	Transformations
of	of	k?	of
Elisabethan	Elisabethan	k1gInSc1	Elisabethan
England	England	k1gInSc1	England
in	in	k?	in
the	the	k?	the
Context	Context	k1gInSc1	Context
of	of	k?	of
International	International	k1gFnSc1	International
Relations	Relations	k1gInSc1	Relations
in	in	k?	in
1560	[number]	k4	1560
<g/>
s.	s.	k?	s.
Prague	Prague	k1gNnSc2	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85899	[number]	k4	85899
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LOADES	LOADES	kA	LOADES
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
I	I	kA	I
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Golden	Goldna	k1gFnPc2	Goldna
Reign	Reign	k1gMnSc1	Reign
of	of	k?	of
Gloriana	Gloriana	k1gFnSc1	Gloriana
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
National	National	k1gMnSc1	National
Archives	Archives	k1gMnSc1	Archives
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
903365	[number]	k4	903365
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMANOVÁ	Zemanová	k1gFnSc1	Zemanová
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětinské	alžbětinský	k2eAgNnSc1d1	alžbětinské
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
londýnská	londýnský	k2eAgFnSc1d1	londýnská
inspirace	inspirace	k1gFnSc1	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
163	[number]	k4	163
<g/>
-	-	kIx~	-
<g/>
168	[number]	k4	168
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMANOVÁ	Zemanová	k1gFnSc1	Zemanová
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětinské	alžbětinský	k2eAgNnSc1d1	alžbětinské
umění	umění	k1gNnSc1	umění
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
