<s>
Želva	želva	k1gFnSc1	želva
uhlířská	uhlířský	k2eAgFnSc1d1	uhlířská
(	(	kIx(	(
<g/>
Chelonoidis	Chelonoidis	k1gFnSc1	Chelonoidis
carbonaria	carbonarium	k1gNnSc2	carbonarium
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Geochelone	Geochelon	k1gInSc5	Geochelon
carbonaria	carbonarium	k1gNnSc2	carbonarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
želva	želva	k1gFnSc1	želva
obývající	obývající	k2eAgFnSc2d1	obývající
vlhké	vlhký	k2eAgFnSc2d1	vlhká
savany	savana	k1gFnSc2	savana
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
žádné	žádný	k3yNgInPc1	žádný
její	její	k3xOp3gInPc1	její
oficiální	oficiální	k2eAgInPc1d1	oficiální
poddruhy	poddruh	k1gInPc1	poddruh
,	,	kIx,	,
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
se	se	k3xPyFc4	se
však	však	k9	však
některé	některý	k3yIgInPc1	některý
drobné	drobný	k2eAgInPc1d1	drobný
rozlišovací	rozlišovací	k2eAgInPc1d1	rozlišovací
znaky	znak	k1gInPc1	znak
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zbarvení	zbarvení	k1gNnSc1	zbarvení
plastronu	plastron	k1gInSc2	plastron
<g/>
)	)	kIx)	)
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
nehibernuje	hibernovat	k5eNaPmIp3nS	hibernovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
snižuje	snižovat	k5eAaImIp3nS	snižovat
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
černý	černý	k2eAgInSc4d1	černý
karapax	karapax	k1gInSc4	karapax
se	s	k7c7	s
žlutými	žlutý	k2eAgFnPc7d1	žlutá
<g/>
,	,	kIx,	,
naoranžovělými	naoranžovělý	k2eAgFnPc7d1	naoranžovělá
nebo	nebo	k8xC	nebo
načervenalými	načervenalý	k2eAgFnPc7d1	načervenalá
skvrnami	skvrna	k1gFnPc7	skvrna
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
štítku	štítek	k1gInSc6	štítek
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
asi	asi	k9	asi
50	[number]	k4	50
cm	cm	kA	cm
.	.	kIx.	.
</s>
<s>
Plastron	plastron	k1gInSc1	plastron
je	být	k5eAaImIp3nS	být
žlutohnědý	žlutohnědý	k2eAgInSc1d1	žlutohnědý
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarvit	k5eAaPmNgFnP	zbarvit
do	do	k7c2	do
béžova	béžovo	k1gNnSc2	béžovo
s	s	k7c7	s
jasnými	jasný	k2eAgFnPc7d1	jasná
žlutými	žlutý	k2eAgFnPc7d1	žlutá
nebo	nebo	k8xC	nebo
oranžovými	oranžový	k2eAgFnPc7d1	oranžová
skvrnami	skvrna	k1gFnPc7	skvrna
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
příjemnému	příjemný	k2eAgNnSc3d1	příjemné
zbarvení	zbarvení	k1gNnSc3	zbarvení
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
druhem	druh	k1gInSc7	druh
chovaným	chovaný	k2eAgInSc7d1	chovaný
uměle	uměle	k6eAd1	uměle
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
nepatrný	patrný	k2eNgInSc1d1	patrný
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mívají	mívat	k5eAaImIp3nP	mívat
delší	dlouhý	k2eAgInSc4d2	delší
krunýř	krunýř	k1gInSc4	krunýř
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
těžší	těžký	k2eAgMnSc1d2	těžší
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vyklenutý	vyklenutý	k2eAgInSc4d1	vyklenutý
plastron	plastron	k1gInSc4	plastron
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
malé	malý	k2eAgNnSc1d1	malé
zaškrcení	zaškrcený	k2eAgMnPc1d1	zaškrcený
krunýře	krunýř	k1gInSc2	krunýř
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
přesýpacích	přesýpací	k2eAgFnPc2d1	přesýpací
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
širší	široký	k2eAgInSc4d2	širší
ocas	ocas	k1gInSc4	ocas
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgInPc4d1	známý
žádné	žádný	k3yNgInPc4	žádný
poddruhy	poddruh	k1gInPc4	poddruh
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
vlhké	vlhký	k2eAgFnSc2d1	vlhká
savany	savana	k1gFnSc2	savana
,	,	kIx,	,
méně	málo	k6eAd2	málo
okolí	okolí	k1gNnSc1	okolí
močálů	močál	k1gInPc2	močál
tropických	tropický	k2eAgInPc2d1	tropický
deštných	deštný	k2eAgInPc2d1	deštný
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
<g/>
,	,	kIx,	,
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
,	,	kIx,	,
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
,	,	kIx,	,
Venezuele	Venezuela	k1gFnSc6	Venezuela
a	a	k8xC	a
Argentině	Argentina	k1gFnSc6	Argentina
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
uměle	uměle	k6eAd1	uměle
vysazena	vysazen	k2eAgFnSc1d1	vysazena
na	na	k7c6	na
mnohých	mnohý	k2eAgInPc6d1	mnohý
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
Barbadosu	Barbadosa	k1gFnSc4	Barbadosa
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc4	Trinidad
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Lucii	Lucie	k1gFnSc6	Lucie
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
vlhko	vlhko	k1gNnSc4	vlhko
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
želvy	želva	k1gFnSc2	želva
uhlířské	uhlířský	k2eAgInPc1d1	uhlířský
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
masožravě	masožravě	k6eAd1	masožravě
<g/>
,	,	kIx,	,
hledají	hledat	k5eAaImIp3nP	hledat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
červy	červ	k1gMnPc4	červ
a	a	k8xC	a
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
býložraví	býložravý	k2eAgMnPc1d1	býložravý
,	,	kIx,	,
živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
různými	různý	k2eAgInPc7d1	různý
plody	plod	k1gInPc7	plod
a	a	k8xC	a
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
však	však	k9	však
ani	ani	k9	ani
drobnou	drobný	k2eAgFnSc7d1	drobná
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
potravou	potrava	k1gFnSc7	potrava
nebo	nebo	k8xC	nebo
zbytky	zbytek	k1gInPc7	zbytek
živočichů	živočich	k1gMnPc2	živočich
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
roste	růst	k5eAaImIp3nS	růst
jejich	jejich	k3xOp3gNnSc4	jejich
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
dorostou	dorůst	k5eAaPmIp3nP	dorůst
velikosti	velikost	k1gFnSc3	velikost
asi	asi	k9	asi
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
2-15	[number]	k4	2-15
vajec	vejce	k1gNnPc2	vejce
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
je	on	k3xPp3gInPc4	on
zahrabává	zahrabávat	k5eAaImIp3nS	zahrabávat
do	do	k7c2	do
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
vypozorovány	vypozorovat	k5eAaPmNgInP	vypozorovat
i	i	k9	i
příklady	příklad	k1gInPc1	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nakladla	naklást	k5eAaPmAgFnS	naklást
do	do	k7c2	do
hnízd	hnízdo	k1gNnPc2	hnízdo
ve	v	k7c6	v
směsici	směsice	k1gFnSc6	směsice
listí	listí	k1gNnSc1	listí
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
za	za	k7c4	za
4-6	[number]	k4	4-6
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velká	velká	k1gFnSc1	velká
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
4,5	[number]	k4	4,5
cm	cm	kA	cm
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
bývají	bývat	k5eAaImIp3nP	bývat
samice	samice	k1gFnPc1	samice
schopné	schopný	k2eAgFnPc1d1	schopná
snášet	snášet	k5eAaImF	snášet
vejce	vejce	k1gNnSc4	vejce
kdykoli	kdykoli	k6eAd1	kdykoli
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
zápasy	zápas	k1gInPc1	zápas
mezi	mezi	k7c7	mezi
samci	samec	k1gInPc7	samec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
postranním	postranní	k2eAgInSc7d1	postranní
trháním	trhání	k1gNnPc3	trhání
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pohyby	pohyb	k1gInPc1	pohyb
hlavy	hlava	k1gFnSc2	hlava
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
želv	želva	k1gFnPc2	želva
různé	různý	k2eAgFnPc4d1	různá
<g/>
,	,	kIx,	,
a	a	k8xC	a
prosto	prost	k2eAgNnSc1d1	prosto
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
rozeznávání	rozeznávání	k1gNnSc2	rozeznávání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
souboj	souboj	k1gInSc1	souboj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
samci	samec	k1gMnPc1	samec
snaží	snažit	k5eAaImIp3nP	snažit
otočit	otočit	k5eAaPmF	otočit
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgMnSc4	druhý
na	na	k7c4	na
krunýř	krunýř	k1gInSc4	krunýř
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
ten	ten	k3xDgInSc4	ten
největší	veliký	k2eAgInSc4d3	veliký
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
uhlířská	uhlířský	k2eAgFnSc1d1	uhlířská
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
prostorné	prostorný	k2eAgNnSc4d1	prostorné
terárium	terárium	k1gNnSc4	terárium
s	s	k7c7	s
dostatečně	dostatečně	k6eAd1	dostatečně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
umístěním	umístění	k1gNnSc7	umístění
rostlin	rostlina	k1gFnPc2	rostlina
do	do	k7c2	do
terária	terárium	k1gNnSc2	terárium
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
substrát	substrát	k1gInSc1	substrát
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
použít	použít	k5eAaPmF	použít
jemný	jemný	k2eAgInSc4d1	jemný
písek	písek	k1gInSc4	písek
nebo	nebo	k8xC	nebo
třísky	tříska	k1gFnPc4	tříska
některých	některý	k3yIgInPc2	některý
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
udržují	udržovat	k5eAaImIp3nP	udržovat
vlhkost	vlhkost	k1gFnSc4	vlhkost
při	při	k7c6	při
stále	stále	k6eAd1	stále
suchém	suchý	k2eAgInSc6d1	suchý
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tvorbě	tvorba	k1gFnSc3	tvorba
plísní	plíseň	k1gFnPc2	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
stále	stále	k6eAd1	stále
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
velkou	velký	k2eAgFnSc4d1	velká
misku	miska	k1gFnSc4	miska
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
stinná	stinný	k2eAgNnPc4d1	stinné
místa	místo	k1gNnPc4	místo
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teráriu	terárium	k1gNnSc6	terárium
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
také	také	k9	také
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
dostávalo	dostávat	k5eAaImAgNnS	dostávat
<g/>
.	.	kIx.	.
</s>
<s>
Uhlířky	uhlířka	k1gFnPc1	uhlířka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
krmeny	krmit	k5eAaImNgInP	krmit
rostlinami	rostlina	k1gFnPc7	rostlina
bohatými	bohatý	k2eAgFnPc7d1	bohatá
na	na	k7c4	na
vápník	vápník	k1gInSc4	vápník
(	(	kIx(	(
<g/>
pampelišky	pampeliška	k1gFnPc4	pampeliška
<g/>
,	,	kIx,	,
vinné	vinný	k2eAgInPc4d1	vinný
lístky	lístek	k1gInPc4	lístek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovocem	ovoce	k1gNnSc7	ovoce
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
jahody	jahoda	k1gFnPc1	jahoda
<g/>
,	,	kIx,	,
broskev	broskev	k1gFnSc1	broskev
<g/>
,	,	kIx,	,
ananas	ananas	k1gInSc1	ananas
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
nebo	nebo	k8xC	nebo
kiwi	kiwi	k1gNnSc1	kiwi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
,	,	kIx,	,
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
živočišných	živočišný	k2eAgFnPc2d1	živočišná
bílkovin	bílkovina	k1gFnPc2	bílkovina
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
želvu	želva	k1gFnSc4	želva
je	být	k5eAaImIp3nS	být
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
lov	lov	k1gInSc1	lov
pro	pro	k7c4	pro
konzumaci	konzumace	k1gFnSc4	konzumace
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
přírodního	přírodní	k2eAgInSc2d1	přírodní
výskytu	výskyt	k1gInSc2	výskyt
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
přispívá	přispívat	k5eAaImIp3nS	přispívat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydrží	vydržet	k5eAaPmIp3nS	vydržet
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
bez	bez	k7c2	bez
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
uhlířské	uhlířský	k2eAgFnPc1d1	uhlířská
jsou	být	k5eAaImIp3nP	být
sbírány	sbírán	k2eAgFnPc1d1	sbírána
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
posílány	posílat	k5eAaImNgFnP	posílat
do	do	k7c2	do
měst	město	k1gNnPc2	město
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
velkou	velký	k2eAgFnSc4d1	velká
lahůdku	lahůdka	k1gFnSc4	lahůdka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
želva	želva	k1gFnSc1	želva
je	být	k5eAaImIp3nS	být
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
také	také	k9	také
úbytkem	úbytek	k1gInSc7	úbytek
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
upravováno	upravován	k2eAgNnSc1d1	upravováno
pro	pro	k7c4	pro
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
a	a	k8xC	a
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
tak	tak	k6eAd1	tak
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
výskyt	výskyt	k1gInSc4	výskyt
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
ne	ne	k9	ne
tak	tak	k6eAd1	tak
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
jako	jako	k8xS	jako
lov	lov	k1gInSc4	lov
a	a	k8xC	a
úbytek	úbytek	k1gInSc4	úbytek
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odchyt	odchyt	k1gInSc1	odchyt
želv	želva	k1gFnPc2	želva
pro	pro	k7c4	pro
domácí	domácí	k2eAgInSc4d1	domácí
chov	chov	k1gInSc4	chov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
přílohou	příloha	k1gFnSc7	příloha
II	II	kA	II
Washingtonské	washingtonský	k2eAgFnSc2d1	Washingtonská
úmluvy	úmluva	k1gFnSc2	úmluva
.	.	kIx.	.
</s>
