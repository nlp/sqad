<s>
Lutecium	lutecium	k1gNnSc1	lutecium
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dosti	dosti	k6eAd1	dosti
vzácný	vzácný	k2eAgInSc4d1	vzácný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
0,75	[number]	k4	0,75
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
