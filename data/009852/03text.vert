<p>
<s>
Oakland	Oakland	k1gInSc1	Oakland
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
okresu	okres	k1gInSc2	okres
Alameda	Alamed	k1gMnSc2	Alamed
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Sanfranciského	sanfranciský	k2eAgInSc2d1	sanfranciský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Oaklandu	Oakland	k1gInSc6	Oakland
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
jím	on	k3xPp3gNnSc7	on
důležitá	důležitý	k2eAgFnSc1d1	důležitá
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
společnosti	společnost	k1gFnSc2	společnost
Amtrak	Amtrak	k1gInSc1	Amtrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žil	žít	k5eAaImAgMnS	žít
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
známá	známý	k2eAgFnSc1d1	známá
punkrocková	punkrockový	k2eAgFnSc1d1	punkrocková
kapela	kapela	k1gFnSc1	kapela
Green	Grena	k1gFnPc2	Grena
Day	Day	k1gFnPc2	Day
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
390	[number]	k4	390
724	[number]	k4	724
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rasové	rasový	k2eAgNnSc4d1	rasové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
34,5	[number]	k4	34,5
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
28,0	[number]	k4	28,0
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
</s>
</p>
<p>
<s>
0,8	[number]	k4	0,8
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
</s>
</p>
<p>
<s>
16,8	[number]	k4	16,8
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
0,6	[number]	k4	0,6
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
</s>
</p>
<p>
<s>
13,7	[number]	k4	13,7
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
</s>
</p>
<p>
<s>
5,6	[number]	k4	5,6
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasObyvatelé	rasObyvatel	k1gMnPc1	rasObyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
25,4	[number]	k4	25,4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<g/>
Etnické	etnický	k2eAgNnSc1d1	etnické
složení	složení	k1gNnSc1	složení
města	město	k1gNnSc2	město
prošlo	projít	k5eAaPmAgNnS	projít
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
století	století	k1gNnSc6	století
velkými	velký	k2eAgFnPc7d1	velká
proměnami	proměna	k1gFnPc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
bělochů	běloch	k1gMnPc2	běloch
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
z	z	k7c2	z
95,3	[number]	k4	95,3
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
na	na	k7c4	na
25,9	[number]	k4	25,9
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
černochů	černoch	k1gMnPc2	černoch
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
až	až	k9	až
na	na	k7c4	na
47	[number]	k4	47
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
gentrifikaci	gentrifikace	k1gFnSc3	gentrifikace
a	a	k8xC	a
černoši	černoch	k1gMnPc1	černoch
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tvoří	tvořit	k5eAaImIp3nP	tvořit
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
a	a	k8xC	a
Hispánci	Hispánek	k1gMnPc1	Hispánek
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
černochů	černoch	k1gMnPc2	černoch
si	se	k3xPyFc3	se
Oakland	Oakland	k1gInSc4	Oakland
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
pověst	pověst	k1gFnSc4	pověst
jednoho	jeden	k4xCgNnSc2	jeden
z	z	k7c2	z
nejnebezpečnějších	bezpečný	k2eNgNnPc2d3	nejnebezpečnější
měst	město	k1gNnPc2	město
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
dekádách	dekáda	k1gFnPc6	dekáda
mnoho	mnoho	k6eAd1	mnoho
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
Oakland	Oakland	k1gInSc4	Oakland
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
loupeží	loupež	k1gFnPc2	loupež
a	a	k8xC	a
krádeží	krádež	k1gFnPc2	krádež
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc6	počet
vražd	vražda	k1gFnPc2	vražda
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Oaklandu	Oakland	k1gInSc6	Oakland
sídlí	sídlet	k5eAaImIp3nS	sídlet
tým	tým	k1gInSc1	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Oakland	Oakland	k1gInSc1	Oakland
Raiders	Raiders	k1gInSc1	Raiders
hrající	hrající	k2eAgFnSc2d1	hrající
NFL	NFL	kA	NFL
a	a	k8xC	a
baseballový	baseballový	k2eAgInSc1d1	baseballový
tým	tým	k1gInSc1	tým
Oakland	Oaklanda	k1gFnPc2	Oaklanda
Athletics	Athleticsa	k1gFnPc2	Athleticsa
hrající	hrající	k2eAgFnSc7d1	hrající
MLB	MLB	kA	MLB
a	a	k8xC	a
tým	tým	k1gInSc1	tým
Golden	Goldna	k1gFnPc2	Goldna
State	status	k1gInSc5	status
Warriors	Warriors	k1gInSc1	Warriors
hrající	hrající	k2eAgFnSc4d1	hrající
basketbalovou	basketbalový	k2eAgFnSc4d1	basketbalová
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
</s>
</p>
<p>
<s>
Sanfranciský	sanfranciský	k2eAgInSc1d1	sanfranciský
záliv	záliv	k1gInSc1	záliv
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oakland	Oaklanda	k1gFnPc2	Oaklanda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
