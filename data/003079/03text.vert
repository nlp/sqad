<s>
Řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
také	také	k9	také
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc1d1	matematický
výraz	výraz	k1gInSc1	výraz
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
posloupnost	posloupnost	k1gFnSc1	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
řady	řada	k1gFnSc2	řada
tvořeny	tvořit	k5eAaImNgFnP	tvořit
čísly	čísnout	k5eAaPmAgFnP	čísnout
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
závisí	záviset	k5eAaImIp3nS	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
pořadovém	pořadový	k2eAgNnSc6d1	pořadové
čísle	číslo	k1gNnSc6	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
číselných	číselný	k2eAgFnPc6d1	číselná
řadách	řada	k1gFnPc6	řada
(	(	kIx(	(
<g/>
řadách	řada	k1gFnPc6	řada
s	s	k7c7	s
konstantními	konstantní	k2eAgMnPc7d1	konstantní
členy	člen	k1gMnPc7	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
řady	řada	k1gFnSc2	řada
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
záviset	záviset	k5eAaImF	záviset
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
pořadovém	pořadový	k2eAgNnSc6d1	pořadové
čísle	číslo	k1gNnSc6	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
parametrech	parametr	k1gInPc6	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
řady	řada	k1gFnPc4	řada
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
funkční	funkční	k2eAgFnSc7d1	funkční
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
také	také	k9	také
funkcionální	funkcionální	k2eAgFnSc6d1	funkcionální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1	funkční
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnSc4d1	funkční
řadu	řada	k1gFnSc4	řada
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
získáme	získat	k5eAaPmIp1nP	získat
z	z	k7c2	z
funkční	funkční	k2eAgFnSc2d1	funkční
posloupnosti	posloupnost	k1gFnSc2	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
výraz	výraz	k1gInSc1	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
+	+	kIx~	+
⋯	⋯	k?	⋯
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
}	}	kIx)	}
:	:	kIx,	:
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
průnik	průnik	k1gInSc1	průnik
definičních	definiční	k2eAgInPc2d1	definiční
oborů	obor	k1gInPc2	obor
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
až	až	k6eAd1	až
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Zvolíme	zvolit	k5eAaPmIp1nP	zvolit
<g/>
-li	i	k?	-li
libovolné	libovolný	k2eAgFnPc4d1	libovolná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
získáme	získat	k5eAaPmIp1nP	získat
<g />
.	.	kIx.	.
</s>
<s hack="1">
číselnou	číselný	k2eAgFnSc4d1	číselná
řadu	řada	k1gFnSc4	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
posloupnosti	posloupnost	k1gFnSc2	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
}	}	kIx)	}
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
posloupnost	posloupnost	k1gFnSc4	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
členy	člen	k1gInPc1	člen
jsou	být	k5eAaImIp3nP	být
určeny	určen	k2eAgInPc1d1	určen
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tedy	tedy	k9	tedy
(	(	kIx(	(
<g/>
konečný	konečný	k2eAgInSc4d1	konečný
<g/>
)	)	kIx)	)
součet	součet	k1gInSc4	součet
prvních	první	k4xOgMnPc2	první
n	n	k0	n
prvků	prvek	k1gInPc2	prvek
posloupnosti	posloupnost	k1gFnSc2	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Posloupnost	posloupnost	k1gFnSc4	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
posloupnost	posloupnost	k1gFnSc4	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
nebo	nebo	k8xC	nebo
sumaci	sumace	k1gFnSc4	sumace
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
této	tento	k3xDgFnSc2	tento
posloupnosti	posloupnost	k1gFnSc2	posloupnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tým	ým	k6eAd1	-tým
částečným	částečný	k2eAgInSc7d1	částečný
součtem	součet	k1gInSc7	součet
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
limity	limita	k1gFnSc2	limita
posloupnosti	posloupnost	k1gFnSc2	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
řada	řada	k1gFnSc1	řada
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
ztotožňován	ztotožňovat	k5eAaImNgInS	ztotožňovat
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
součtem	součet	k1gInSc7	součet
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
posloupnost	posloupnost	k1gFnSc1	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
konečnou	konečný	k2eAgFnSc4d1	konečná
limitu	limita	k1gFnSc4	limita
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}}	}}}	k?	}}}
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
bodově	bodově	k6eAd1	bodově
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
v	v	k7c6	v
případě	případ	k1gInSc6	případ
funkční	funkční	k2eAgFnSc2d1	funkční
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
uvedená	uvedený	k2eAgFnSc1d1	uvedená
limita	limita	k1gFnSc1	limita
neexistuje	existovat	k5eNaImIp3nS	existovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
-	-	kIx~	-
posloupnost	posloupnost	k1gFnSc4	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
je	být	k5eAaImIp3nS	být
oscilující	oscilující	k2eAgFnSc1d1	oscilující
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
=	=	kIx~	=
±	±	k?	±
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pm	pm	k?	pm
\	\	kIx~	\
<g/>
infty	infta	k1gMnSc2	infta
}	}	kIx)	}
(	(	kIx(	(
<g/>
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
divergentní	divergentní	k2eAgFnSc1d1	divergentní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
číselné	číselný	k2eAgFnPc4d1	číselná
řady	řada	k1gFnPc4	řada
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
řady	řada	k1gFnSc2	řada
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
funkční	funkční	k2eAgFnPc4d1	funkční
řady	řada	k1gFnPc4	řada
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
řady	řada	k1gFnSc2	řada
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
...	...	k?	...
<g/>
}	}	kIx)	}
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc2	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1,2	[number]	k4	1,2
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
konvergentní	konvergentní	k2eAgNnSc1d1	konvergentní
tehdy	tehdy	k6eAd1	tehdy
a	a	k8xC	a
jen	jen	k6eAd1	jen
<g />
.	.	kIx.	.
</s>
<s hack="1">
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
konvergují	konvergovat	k5eAaImIp3nP	konvergovat
<g/>
-li	i	k?	-li
obě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
β	β	k?	β
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
:	:	kIx,	:
Konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
-li	i	k?	-li
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
pak	pak	k6eAd1	pak
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
c	c	k0	c
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
ca_	ca_	k?	ca_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
pak	pak	k6eAd1	pak
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
získáme	získat	k5eAaPmIp1nP	získat
přidáním	přidání	k1gNnSc7	přidání
nebo	nebo	k8xC	nebo
odebráním	odebrání	k1gNnSc7	odebrání
konečného	konečný	k2eAgInSc2d1	konečný
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
divergentní	divergentní	k2eAgFnSc1d1	divergentní
také	také	k9	také
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
přidáním	přidání	k1gNnSc7	přidání
nebo	nebo	k8xC	nebo
odebráním	odebrání	k1gNnSc7	odebrání
konečného	konečný	k2eAgInSc2d1	konečný
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
funkčních	funkční	k2eAgFnPc2d1	funkční
řad	řada	k1gFnPc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
označujeme	označovat	k5eAaImIp1nP	označovat
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
všech	všecek	k3xTgMnPc2	všecek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
která	který	k3yRgNnPc4	který
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
řada	řada	k1gFnSc1	řada
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
obor	obor	k1gInSc1	obor
konvergence	konvergence	k1gFnSc2	konvergence
dané	daný	k2eAgFnSc2d1	daná
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
ale	ale	k8xC	ale
nekonverguje	konvergovat	k5eNaImIp3nS	konvergovat
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
neabsolutně	absolutně	k6eNd1	absolutně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
i	i	k9	i
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnSc2	infta
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
absolutně	absolutně	k6eAd1	absolutně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnPc4d1	konvergentní
řady	řada	k1gFnPc4	řada
platí	platit	k5eAaImIp3nP	platit
komutativní	komutativní	k2eAgInPc1d1	komutativní
<g/>
,	,	kIx,	,
asociativní	asociativní	k2eAgInPc1d1	asociativní
a	a	k8xC	a
distributivní	distributivní	k2eAgInPc1d1	distributivní
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Přesouváním	přesouvání	k1gNnSc7	přesouvání
členů	člen	k1gInPc2	člen
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nS	změnit
konvergence	konvergence	k1gFnSc1	konvergence
ani	ani	k8xC	ani
součet	součet	k1gInSc1	součet
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
dvě	dva	k4xCgFnPc4	dva
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
se	s	k7c7	s
součty	součet	k1gInPc7	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
...	...	k?	...
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
funkcí	funkce	k1gFnPc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
infty	infta	k1gFnSc2	infta
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
označíme	označit	k5eAaPmIp1nP	označit
jako	jako	k9	jako
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
konvergentní	konvergentní	k2eAgFnSc7d1	konvergentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
oblasti	oblast	k1gFnSc6	oblast
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
}	}	kIx)	}
komplexní	komplexní	k2eAgFnPc4d1	komplexní
roviny	rovina	k1gFnPc4	rovina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
existuje	existovat	k5eAaImIp3nS	existovat
takové	takový	k3xDgNnSc1	takový
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
a	a	k8xC	a
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
ε	ε	k?	ε
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgInPc4d1	libovolný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
>	>	kIx)	>
N	N	kA	N
(	(	kIx(	(
ε	ε	k?	ε
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
>	>	kIx)	>
<g/>
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
∈	∈	k?	∈
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
s	s	k7c7	s
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
-s	-s	k?	-s
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
reálné	reálný	k2eAgFnSc2d1	reálná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
oblast	oblast	k1gFnSc1	oblast
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
}	}	kIx)	}
představuje	představovat	k5eAaImIp3nS	představovat
interval	interval	k1gInSc4	interval
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
řad	řada	k1gFnPc2	řada
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
tzv.	tzv.	kA	tzv.
zbytek	zbytek	k1gInSc4	zbytek
řady	řada	k1gFnSc2	řada
po	po	k7c6	po
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tém	é	k1gNnSc7	-té
součtu	součet	k1gInSc2	součet
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
s	s	k7c7	s
-	-	kIx~	-
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
s-s_	s_	k?	s-s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Podmínku	podmínka	k1gFnSc4	podmínka
konvergence	konvergence	k1gFnSc2	konvergence
řady	řada	k1gFnSc2	řada
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
také	také	k9	také
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
řada	řada	k1gFnSc1	řada
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
k	k	k7c3	k
libovolnému	libovolný	k2eAgNnSc3d1	libovolné
kladnému	kladný	k2eAgNnSc3d1	kladné
číslu	číslo	k1gNnSc3	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
existuje	existovat	k5eAaImIp3nS	existovat
takové	takový	k3xDgNnSc4	takový
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
(	(	kIx(	(
ε	ε	k?	ε
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgInPc4d1	libovolný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
>	>	kIx)	>
N	N	kA	N
(	(	kIx(	(
ε	ε	k?	ε
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
>	>	kIx)	>
<g/>
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
nerovnost	nerovnost	k1gFnSc1	nerovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
s	s	k7c7	s
-	-	kIx~	-
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
<	<	kIx(	<
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
|	|	kIx~	|
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
|	|	kIx~	|
<g/>
s-s_	s_	k?	s-s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
:	:	kIx,	:
Nutnou	nutný	k2eAgFnSc7d1	nutná
podmínkou	podmínka	k1gFnSc7	podmínka
konvergence	konvergence	k1gFnSc2	konvergence
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
součet	součet	k1gInSc1	součet
řady	řada	k1gFnSc2	řada
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
vyjádříme	vyjádřit	k5eAaPmIp1nP	vyjádřit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
-tý	ý	k?	-tý
částečný	částečný	k2eAgInSc4d1	částečný
součet	součet	k1gInSc4	součet
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
zbytek	zbytek	k1gInSc1	zbytek
řady	řada	k1gFnSc2	řada
po	po	k7c6	po
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
-tém	ém	k6eAd1	-tém
částečném	částečný	k2eAgInSc6d1	částečný
součtu	součet	k1gInSc6	součet
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nutnou	nutný	k2eAgFnSc4d1	nutná
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
postačující	postačující	k2eAgFnSc4d1	postačující
podmínku	podmínka	k1gFnSc4	podmínka
konvergence	konvergence	k1gFnSc2	konvergence
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
-	-	kIx~	-
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gMnPc7	inft
}	}	kIx)	}
<g/>
R_	R_	k1gMnPc7	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
s-s_	s_	k?	s-s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Nutná	nutný	k2eAgFnSc1d1	nutná
a	a	k8xC	a
postačující	postačující	k2eAgFnSc1d1	postačující
podmínka	podmínka	k1gFnSc1	podmínka
konvergence	konvergence	k1gFnSc2	konvergence
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
vyjadřována	vyjadřovat	k5eAaImNgFnS	vyjadřovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
Bolzanova-Cauchyova	Bolzanova-Cauchyův	k2eAgNnSc2d1	Bolzanova-Cauchyův
kritéria	kritérion	k1gNnSc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
řada	řada	k1gFnSc1	řada
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
-li	i	k?	-li
k	k	k7c3	k
libovolnému	libovolný	k2eAgMnSc3d1	libovolný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
takové	takový	k3xDgNnSc1	takový
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
(	(	kIx(	(
ε	ε	k?	ε
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
libovolná	libovolný	k2eAgFnSc1d1	libovolná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
>	>	kIx)	>
N	N	kA	N
(	(	kIx(	(
ε	ε	k?	ε
)	)	kIx)	)
,	,	kIx,	,
n	n	k0	n
>	>	kIx)	>
N	N	kA	N
(	(	kIx(	(
ε	ε	k?	ε
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
>	>	kIx)	>
<g/>
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
>	>	kIx)	>
<g/>
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
|	|	kIx~	|
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
-s_	-s_	k?	-s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
:	:	kIx,	:
Určení	určení	k1gNnSc1	určení
součtu	součet	k1gInSc2	součet
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
tedy	tedy	k9	tedy
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
o	o	k7c4	o
konvergenci	konvergence	k1gFnSc4	konvergence
nebo	nebo	k8xC	nebo
divergenci	divergence	k1gFnSc4	divergence
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
poměrně	poměrně	k6eAd1	poměrně
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
postačující	postačující	k2eAgNnSc1d1	postačující
nahradit	nahradit	k5eAaPmF	nahradit
součet	součet	k1gInSc4	součet
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jejím	její	k3xOp3gInSc6	její
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
-tým	ým	k6eAd1	-tým
částečným	částečný	k2eAgInSc7d1	částečný
součtem	součet	k1gInSc7	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
řad	řada	k1gFnPc2	řada
se	se	k3xPyFc4	se
chyba	chyba	k1gFnSc1	chyba
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
s	s	k7c7	s
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
-s	-s	k?	-s
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
náhradou	náhrada	k1gFnSc7	náhrada
dopouštíme	dopouštět	k5eAaImIp1nP	dopouštět
<g/>
,	,	kIx,	,
s	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
divergentních	divergentní	k2eAgFnPc2d1	divergentní
řad	řada	k1gFnPc2	řada
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
důležité	důležitý	k2eAgNnSc1d1	důležité
umět	umět	k5eAaImF	umět
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c4	o
konvergenci	konvergence	k1gFnSc4	konvergence
nebo	nebo	k8xC	nebo
divergenci	divergence	k1gFnSc4	divergence
dané	daný	k2eAgFnSc2d1	daná
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
bychom	by	kYmCp1nP	by
získali	získat	k5eAaPmAgMnP	získat
součet	součet	k1gInSc4	součet
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
podmínky	podmínka	k1gFnPc1	podmínka
konvergence	konvergence	k1gFnSc2	konvergence
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
kritéria	kritérion	k1gNnSc2	kritérion
konvergence	konvergence	k1gFnSc2	konvergence
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
konvergence	konvergence	k1gFnSc2	konvergence
řad	řada	k1gFnPc2	řada
ulehčují	ulehčovat	k5eAaImIp3nP	ulehčovat
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
o	o	k7c6	o
konvergenci	konvergence	k1gFnSc6	konvergence
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
divergenci	divergence	k1gFnSc6	divergence
<g/>
)	)	kIx)	)
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Kritérií	kritérion	k1gNnPc2	kritérion
pro	pro	k7c4	pro
určování	určování	k1gNnSc4	určování
konvergence	konvergence	k1gFnSc2	konvergence
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgInSc1	každý
řešený	řešený	k2eAgInSc1d1	řešený
případ	případ	k1gInSc1	případ
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
posuzovat	posuzovat	k5eAaImF	posuzovat
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
zvolit	zvolit	k5eAaPmF	zvolit
vhodné	vhodný	k2eAgNnSc4d1	vhodné
kritérium	kritérium	k1gNnSc4	kritérium
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srovnávacím	srovnávací	k2eAgInSc6d1	srovnávací
(	(	kIx(	(
<g/>
porovnávacím	porovnávací	k2eAgInSc6d1	porovnávací
<g/>
)	)	kIx)	)
kritériu	kritérion	k1gNnSc3	kritérion
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
dvě	dva	k4xCgFnPc4	dva
řady	řada	k1gFnPc4	řada
s	s	k7c7	s
nezápornými	záporný	k2eNgInPc7d1	nezáporný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
∑	∑	k?	∑
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
≤	≤	k?	≤
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
minorantní	minorantní	k2eAgFnSc4d1	minorantní
řadu	řada	k1gFnSc4	řada
(	(	kIx(	(
<g/>
minorantu	minorant	k1gMnSc6	minorant
<g/>
)	)	kIx)	)
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
jako	jako	k8xC	jako
majorantní	majorantní	k2eAgFnSc4d1	majorantní
řadu	řada	k1gFnSc4	řada
(	(	kIx(	(
<g/>
majorantu	majorant	k1gMnSc6	majorant
<g/>
)	)	kIx)	)
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
majoranta	majorant	k1gMnSc2	majorant
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
také	také	k9	také
minoranta	minorant	k1gMnSc4	minorant
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
-li	i	k?	-li
minoranta	minorant	k1gMnSc2	minorant
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
diverguje	divergovat	k5eAaImIp3nS	divergovat
také	také	k9	také
majoranta	majorant	k1gMnSc4	majorant
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podílovém	podílový	k2eAgNnSc6d1	podílové
kritériu	kritérion	k1gNnSc6	kritérion
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
řada	řada	k1gFnSc1	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
-li	i	k?	-li
reálné	reálný	k2eAgNnSc4d1	reálné
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
<	<	kIx(	<
q	q	k?	q
<	<	kIx(	<
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
<	<	kIx(	<
<g/>
q	q	k?	q
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
q	q	k?	q
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≥	≥	k?	≥
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Alembertovo	Alembertův	k2eAgNnSc1d1	Alembertovo
kritérium	kritérium	k1gNnSc1	kritérium
<g/>
.	.	kIx.	.
</s>
<s>
Zavedeme	zavést	k5eAaPmIp1nP	zavést
<g/>
-li	i	k?	-li
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
veličinu	veličina	k1gFnSc4	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
pak	pak	k6eAd1	pak
dostáváme	dostávat	k5eAaImIp1nP	dostávat
tzv.	tzv.	kA	tzv.
limitní	limitní	k2eAgInPc4d1	limitní
podílové	podílový	k2eAgInPc4d1	podílový
kritérium	kritérium	k1gNnSc4	kritérium
konvergence	konvergence	k1gFnPc4	konvergence
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konvergentní	konvergentní	k2eAgFnPc1d1	konvergentní
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
divergentní	divergentní	k2eAgFnPc1d1	divergentní
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
>	>	kIx)	>
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
konvergentní	konvergentní	k2eAgFnSc7d1	konvergentní
nebo	nebo	k8xC	nebo
divergentní	divergentní	k2eAgFnSc7d1	divergentní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odmocninovém	odmocninový	k2eAgInSc6d1	odmocninový
(	(	kIx(	(
<g/>
Cauchyově	Cauchyov	k1gInSc6	Cauchyov
<g/>
)	)	kIx)	)
kritériu	kritérion	k1gNnSc3	kritérion
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
≤	≤	k?	≤
q	q	k?	q
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
q	q	k?	q
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
a	a	k8xC	a
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
q	q	k?	q
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
řada	řada	k1gFnSc1	řada
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
zavedeme	zavést	k5eAaPmIp1nP	zavést
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
limitní	limitní	k2eAgNnSc4d1	limitní
odmocninové	odmocninový	k2eAgNnSc4d1	odmocninové
kritérium	kritérium	k1gNnSc4	kritérium
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
,	,	kIx,	,
divergentní	divergentní	k2eAgFnPc1d1	divergentní
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
>	>	kIx)	>
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
konvergovat	konvergovat	k5eAaImF	konvergovat
nebo	nebo	k8xC	nebo
divergovat	divergovat	k5eAaImF	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Raabeova	Raabeův	k2eAgNnSc2d1	Raabeův
kritéria	kritérion	k1gNnSc2	kritérion
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konvergentní	konvergentní	k2eAgMnSc1d1	konvergentní
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
takové	takový	k3xDgNnSc1	takový
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
>	>	kIx)	>
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
1	[number]	k4	1
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
≥	≥	k?	≥
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
r	r	kA	r
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
1	[number]	k4	1
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
≤	≤	k?	≤
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
zavedeme	zavést	k5eAaPmIp1nP	zavést
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
1	[number]	k4	1
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
na	na	k7c6	na
základě	základ	k1gInSc6	základ
limitního	limitní	k2eAgNnSc2d1	limitní
Raabeova	Raabeův	k2eAgNnSc2d1	Raabeův
kritéria	kritérion	k1gNnSc2	kritérion
určíme	určit	k5eAaPmIp1nP	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g />
.	.	kIx.	.
</s>
<s hack="1">
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
>	>	kIx)	>
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
diverguje	divergovat	k5eAaImIp3nS	divergovat
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
konvergovat	konvergovat	k5eAaImF	konvergovat
i	i	k8xC	i
divergovat	divergovat	k5eAaImF	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
členy	člen	k1gMnPc4	člen
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
diskrétní	diskrétní	k2eAgFnSc4d1	diskrétní
proměnnou	proměnná	k1gFnSc4	proměnná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
spojitou	spojitý	k2eAgFnSc7d1	spojitá
proměnnou	proměnná	k1gFnSc7	proměnná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
}	}	kIx)	}
bude	být	k5eAaImBp3nS	být
spojitou	spojitý	k2eAgFnSc7d1	spojitá
a	a	k8xC	a
klesající	klesající	k2eAgFnSc7d1	klesající
funkcí	funkce	k1gFnSc7	funkce
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
1	[number]	k4	1
,	,	kIx,	,
+	+	kIx~	+
∞	∞	k?	∞
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
integrálního	integrální	k2eAgNnSc2d1	integrální
kritéria	kritérion	k1gNnSc2	kritérion
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konvergentní	konvergentní	k2eAgMnSc1d1	konvergentní
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
integrál	integrál	k1gInSc1	integrál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
integrál	integrál	k1gInSc1	integrál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
diverguje	divergovat	k5eAaImIp3nS	divergovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
alternující	alternující	k2eAgFnPc4d1	alternující
řady	řada	k1gFnPc4	řada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zapíšeme	zapsat	k5eAaPmIp1nP	zapsat
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
≥	≥	k?	≥
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
Leibnizovo	Leibnizův	k2eAgNnSc4d1	Leibnizovo
kritérium	kritérium	k1gNnSc4	kritérium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
kritéria	kritérion	k1gNnSc2	kritérion
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
uvedená	uvedený	k2eAgFnSc1d1	uvedená
alternující	alternující	k2eAgFnSc1d1	alternující
řada	řada	k1gFnSc1	řada
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
>	>	kIx)	>
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
od	od	k7c2	od
určitého	určitý	k2eAgInSc2d1	určitý
indexu	index	k1gInSc2	index
ryze	ryze	k6eAd1	ryze
monotónně	monotónně	k6eAd1	monotónně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
kladná	kladný	k2eAgFnSc1d1	kladná
posloupnost	posloupnost	k1gFnSc1	posloupnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
existují	existovat	k5eAaImIp3nP	existovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
,	,	kIx,	,
α	α	k?	α
<g />
.	.	kIx.	.
</s>
<s hack="1">
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
kladné	kladný	k2eAgFnPc1d1	kladná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
a	a	k8xC	a
omezená	omezený	k2eAgFnSc1d1	omezená
posloupnost	posloupnost	k1gFnSc1	posloupnost
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
∈	∈	k?	∈
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
q	q	k?	q
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
q-	q-	k?	q-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Když	když	k8xS	když
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
když	když	k8xS	když
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
>	>	kIx)	>
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
>	>	kIx)	>
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
když	když	k8xS	když
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
≤	≤	k?	≤
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
leq	leq	k?	leq
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
reálná	reálný	k2eAgFnSc1d1	reálná
posloupnost	posloupnost	k1gFnSc4	posloupnost
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
komplexní	komplexní	k2eAgFnSc4d1	komplexní
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
jistého	jistý	k2eAgInSc2d1	jistý
indexu	index	k1gInSc2	index
monotonní	monotonní	k2eAgFnSc4d1	monotonní
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
omezenou	omezený	k2eAgFnSc4d1	omezená
posloupnost	posloupnost	k1gFnSc4	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
reálná	reálný	k2eAgFnSc1d1	reálná
posloupnost	posloupnost	k1gFnSc4	posloupnost
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
komplexní	komplexní	k2eAgFnSc4d1	komplexní
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
monotonní	monotonní	k2eAgFnSc1d1	monotonní
a	a	k8xC	a
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
verze	verze	k1gFnSc1	verze
Abelova	Abelův	k2eAgNnSc2d1	Abelův
kritéria	kritérion	k1gNnSc2	kritérion
stejnoměrné	stejnoměrný	k2eAgFnSc2d1	stejnoměrná
konvergence	konvergence	k1gFnSc2	konvergence
pro	pro	k7c4	pro
řady	řada	k1gFnPc4	řada
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
konečného	konečný	k2eAgInSc2d1	konečný
počtu	počet	k1gInSc2	počet
čísel	číslo	k1gNnPc2	číslo
nezáleží	záležet	k5eNaImIp3nP	záležet
na	na	k7c6	na
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
jsou	být	k5eAaImIp3nP	být
sčítány	sčítán	k2eAgFnPc1d1	sčítána
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nekonečně	konečně	k6eNd1	konečně
mnoha	mnoho	k4c3	mnoho
sčítancích	sčítanec	k1gInPc6	sčítanec
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
být	být	k5eAaImF	být
nemusí	muset	k5eNaImIp3nS	muset
<g/>
.	.	kIx.	.
</s>
<s>
Přerovnáním	přerovnání	k1gNnSc7	přerovnání
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
podle	podle	k7c2	podle
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
nazývá	nazývat	k5eAaImIp3nS	nazývat
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
φ	φ	k?	φ
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
phi	phi	k?	phi
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
bijekce	bijekce	k1gFnSc1	bijekce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
její	její	k3xOp3gNnSc4	její
každé	každý	k3xTgNnSc4	každý
přerovnání	přerovnání	k1gNnSc4	přerovnání
je	být	k5eAaImIp3nS	být
také	také	k9	také
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
řada	řada	k1gFnSc1	řada
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc4d1	stejný
součet	součet	k1gInSc4	součet
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
neabsolutně	absolutně	k6eNd1	absolutně
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
reálná	reálný	k2eAgFnSc1d1	reálná
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ke	k	k7c3	k
každému	každý	k3xTgMnSc3	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
̄	̄	k?	̄
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}}}	}}}	k?	}}}
existuje	existovat	k5eAaImIp3nS	existovat
přerovnání	přerovnání	k1gNnSc1	přerovnání
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
φ	φ	k?	φ
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
phi	phi	k?	phi
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
součet	součet	k1gInSc4	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
existuje	existovat	k5eAaImIp3nS	existovat
oscilující	oscilující	k2eAgNnSc4d1	oscilující
přerovnání	přerovnání	k1gNnSc4	přerovnání
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
ψ	ψ	k?	ψ
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
<g/>
:	:	kIx,	:
Označme	označit	k5eAaPmRp1nP	označit
K	k	k7c3	k
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
rovné	rovný	k2eAgFnSc2d1	rovná
součtu	součet	k1gInSc6	součet
kladných	kladný	k2eAgMnPc2d1	kladný
členů	člen	k1gMnPc2	člen
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
jich	on	k3xPp3gMnPc2	on
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
jako	jako	k8xS	jako
součet	součet	k1gInSc1	součet
řady	řada	k1gFnSc2	řada
s	s	k7c7	s
vynecháním	vynechání	k1gNnSc7	vynechání
nekladných	kladný	k2eNgInPc2d1	kladný
členů	člen	k1gInPc2	člen
nebo	nebo	k8xC	nebo
ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
jako	jako	k9	jako
supremum	supremum	k1gNnSc1	supremum
součtů	součet	k1gInPc2	součet
konečných	konečný	k2eAgFnPc2d1	konečná
množin	množina	k1gFnPc2	množina
kladných	kladný	k2eAgMnPc2d1	kladný
členů	člen	k1gMnPc2	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
buď	buď	k8xC	buď
Z	z	k7c2	z
součet	součet	k1gInSc1	součet
záporných	záporný	k2eAgInPc2d1	záporný
členů	člen	k1gInPc2	člen
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
tři	tři	k4xCgFnPc4	tři
možnosti	možnost	k1gFnPc4	možnost
<g/>
:	:	kIx,	:
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
K	K	kA	K
i	i	k8xC	i
Z	Z	kA	Z
jsou	být	k5eAaImIp3nP	být
konečné	konečný	k2eAgInPc1d1	konečný
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
přerovnání	přerovnání	k1gNnSc6	přerovnání
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
K	K	kA	K
<g/>
+	+	kIx~	+
<g/>
Z.b	Z.b	k1gMnSc2	Z.b
<g/>
)	)	kIx)	)
přesně	přesně	k6eAd1	přesně
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
konečné	konečný	k2eAgNnSc1d1	konečné
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
přerovnání	přerovnání	k1gNnSc6	přerovnání
diverguje	divergovat	k5eAaImIp3nS	divergovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
je	být	k5eAaImIp3nS	být
nekonečnéc	nekonečnéc	k1gFnSc1	nekonečnéc
<g/>
)	)	kIx)	)
Obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
nekonečná	konečný	k2eNgNnPc4d1	nekonečné
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
přerovnání	přerovnání	k1gNnPc1	přerovnání
konvergující	konvergující	k2eAgNnPc1d1	konvergující
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
s	s	k7c7	s
sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejprve	nejprve	k6eAd1	nejprve
budeme	být	k5eAaImBp1nP	být
nejdříve	dříve	k6eAd3	dříve
vkládat	vkládat	k5eAaImF	vkládat
kladné	kladný	k2eAgInPc4d1	kladný
čeny	čeny	k1gInPc4	čeny
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
největšími	veliký	k2eAgFnPc7d3	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
posloupnost	posloupnost	k1gFnSc4	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
(	(	kIx(	(
<g/>
známe	znát	k5eAaImIp1nP	znát
<g/>
-li	i	k?	-li
prvních	první	k4xOgInPc2	první
n	n	k0	n
prvků	prvek	k1gInPc2	prvek
vytvářeného	vytvářený	k2eAgNnSc2d1	vytvářené
přerovnání	přerovnání	k1gNnSc2	přerovnání
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
prvních	první	k4xOgNnPc6	první
n	n	k0	n
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
<g/>
)	)	kIx)	)
nepřesáhne	přesáhnout	k5eNaPmIp3nS	přesáhnout
s.	s.	k?	s.
Poté	poté	k6eAd1	poté
budeme	být	k5eAaImBp1nP	být
vkládat	vkládat	k5eAaImF	vkládat
záporné	záporný	k2eAgInPc4d1	záporný
členy	člen	k1gInPc4	člen
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
absolutní	absolutní	k2eAgFnSc6d1	absolutní
hodnotě	hodnota	k1gFnSc6	hodnota
největší	veliký	k2eAgFnSc6d3	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
posloupnost	posloupnost	k1gFnSc4	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
neklesne	klesnout	k5eNaPmIp3nS	klesnout
pod	pod	k7c7	pod
s.	s.	k?	s.
Tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
opakujeme	opakovat	k5eAaImIp1nP	opakovat
donekonečna	donekonečna	k6eAd1	donekonečna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
řada	řada	k1gFnSc1	řada
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nulové	nulový	k2eAgInPc4d1	nulový
členy	člen	k1gInPc4	člen
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
"	"	kIx"	"
<g/>
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
<g/>
"	"	kIx"	"
vložíme	vložit	k5eAaPmIp1nP	vložit
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
všechny	všechen	k3xTgMnPc4	všechen
nevyčerpáme	vyčerpat	k5eNaPmIp1nP	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
lze	lze	k6eAd1	lze
formalizovat	formalizovat	k5eAaBmF	formalizovat
pomocí	pomocí	k7c2	pomocí
věty	věta	k1gFnSc2	věta
o	o	k7c4	o
definici	definice	k1gFnSc4	definice
rekurzí	rekurze	k1gFnPc2	rekurze
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
K	K	kA	K
i	i	k8xC	i
Z	Z	kA	Z
jsou	být	k5eAaImIp3nP	být
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
index	index	k1gInSc4	index
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgInSc7	jenž
by	by	kYmCp3nS	by
již	již	k6eAd1	již
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
též	též	k9	též
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgMnPc4	všechen
členy	člen	k1gMnPc4	člen
původní	původní	k2eAgFnSc2d1	původní
řady	řada	k1gFnSc2	řada
budou	být	k5eAaImBp3nP	být
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c6	o
přerovnání	přerovnání	k1gNnSc6	přerovnání
<g/>
.	.	kIx.	.
</s>
<s>
Zbývá	zbývat	k5eAaImIp3nS	zbývat
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
posloupnost	posloupnost	k1gFnSc4	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
k	k	k7c3	k
s.	s.	k?	s.
Pro	pro	k7c4	pro
libovolné	libovolný	k2eAgFnPc4d1	libovolná
ε	ε	k?	ε
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
konvergence	konvergence	k1gFnSc2	konvergence
existuje	existovat	k5eAaImIp3nS	existovat
index	index	k1gInSc1	index
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgMnPc4	všechen
členy	člen	k1gMnPc4	člen
původní	původní	k2eAgFnSc2d1	původní
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
absolutní	absolutní	k2eAgFnSc6d1	absolutní
hodnotě	hodnota	k1gFnSc6	hodnota
větší	veliký	k2eAgFnSc6d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
ε	ε	k?	ε
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
přerovnání	přerovnání	k1gNnSc2	přerovnání
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
před	před	k7c4	před
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
další	další	k2eAgInSc4d1	další
index	index	k1gInSc4	index
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
indexu	index	k1gInSc2	index
leží	ležet	k5eAaImIp3nS	ležet
všechny	všechen	k3xTgInPc4	všechen
částečné	částečný	k2eAgInPc4d1	částečný
součty	součet	k1gInPc4	součet
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
(	(	kIx(	(
<g/>
s-ε	s-ε	k?	s-ε
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
+	+	kIx~	+
<g/>
ε	ε	k?	ε
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
s	s	k7c7	s
překročena	překročit	k5eAaPmNgFnS	překročit
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
ihned	ihned	k6eAd1	ihned
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Přerovnaná	přerovnaný	k2eAgFnSc1d1	přerovnaný
řada	řada	k1gFnSc1	řada
tedy	tedy	k9	tedy
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
k	k	k7c3	k
s.	s.	k?	s.
Oscilující	oscilující	k2eAgFnPc4d1	oscilující
řady	řada	k1gFnPc4	řada
lze	lze	k6eAd1	lze
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
<g/>
-li	i	k?	-li
částečný	částečný	k2eAgInSc1d1	částečný
součet	součet	k1gInSc1	součet
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
přidáváme	přidávat	k5eAaImIp1nP	přidávat
záporné	záporný	k2eAgInPc4d1	záporný
členy	člen	k1gInPc4	člen
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
částečný	částečný	k2eAgInSc1d1	částečný
součet	součet	k1gInSc1	součet
neklesne	klesnout	k5eNaPmIp3nS	klesnout
pod	pod	k7c4	pod
-1	-1	k4	-1
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
přidáváme	přidávat	k5eAaImIp1nP	přidávat
kladné	kladný	k2eAgFnPc1d1	kladná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
absolutně	absolutně	k6eAd1	absolutně
konvergentní	konvergentní	k2eAgFnPc4d1	konvergentní
řady	řada	k1gFnPc4	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n-k	n	k?	n-k
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Částečné	částečný	k2eAgInPc4d1	částečný
součty	součet	k1gInPc4	součet
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g/>
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Označme	označit	k5eAaPmRp1nP	označit
<g/>
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g/>
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
+	+	kIx~	+
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
...	...	k?	...
<g/>
+	+	kIx~	+
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
Césarovsky	Césarovsky	k1gFnSc1	Césarovsky
sumovatelná	sumovatelný	k2eAgFnSc1d1	sumovatelný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
i	i	k9	i
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
>	>	kIx)	>
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
lim_	lim_	k?	lim_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
>	>	kIx)	>
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnPc4	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Řadu	řada	k1gFnSc4	řada
označíme	označit	k5eAaPmIp1nP	označit
symbolem	symbol	k1gInSc7	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
C	C	kA	C
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
pokud	pokud	k8xS	pokud
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
i	i	k9	i
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
>	>	kIx)	>
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
lim_	lim_	k?	lim_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
>	>	kIx)	>
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
následující	následující	k2eAgInSc4d1	následující
prvek	prvek	k1gInSc4	prvek
konstantním	konstantní	k2eAgInSc7d1	konstantní
násobkem	násobek	k1gInSc7	násobek
předchozího	předchozí	k2eAgInSc2d1	předchozí
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
8	[number]	k4	8
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
16	[number]	k4	16
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
8	[number]	k4	8
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
over	over	k1gInSc1	over
16	[number]	k4	16
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
geometrická	geometrický	k2eAgFnSc1d1	geometrická
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
z	z	k7c2	z
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Aritmetická	aritmetický	k2eAgFnSc1d1	aritmetická
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
každý	každý	k3xTgInSc1	každý
následující	následující	k2eAgInSc1d1	následující
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
zvětšen	zvětšit	k5eAaPmNgInS	zvětšit
o	o	k7c4	o
konstantní	konstantní	k2eAgFnSc4d1	konstantní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
3	[number]	k4	3
+	+	kIx~	+
5	[number]	k4	5
+	+	kIx~	+
7	[number]	k4	7
+	+	kIx~	+
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
1	[number]	k4	1
+	+	kIx~	+
2	[number]	k4	2
(	(	kIx(	(
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
)	)	kIx)	)
]	]	kIx)	]
:	:	kIx,	:
=	=	kIx~	=
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
...	...	k?	...
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Harmonická	harmonický	k2eAgFnSc1d1	harmonická
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
over	over	k1gInSc1	over
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
:	:	kIx,	:
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
splněna	splněn	k2eAgFnSc1d1	splněna
nutná	nutný	k2eAgFnSc1d1	nutná
podmínka	podmínka	k1gFnSc1	podmínka
pro	pro	k7c4	pro
konvergenci	konvergence	k1gFnSc4	konvergence
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součet	součet	k1gInSc1	součet
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
roven	roven	k2eAgInSc1d1	roven
nekonečnu	nekonečno	k1gNnSc3	nekonečno
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
řada	řada	k1gFnSc1	řada
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
harmonická	harmonický	k2eAgFnSc1d1	harmonická
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
prvního	první	k4xOgMnSc2	první
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
harmonickým	harmonický	k2eAgInSc7d1	harmonický
průměrem	průměr	k1gInSc7	průměr
sousedních	sousední	k2eAgMnPc2d1	sousední
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
řada	řada	k1gFnSc1	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
všechny	všechen	k3xTgMnPc4	všechen
členy	člen	k1gMnPc4	člen
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
podmínce	podmínka	k1gFnSc6	podmínka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
s	s	k7c7	s
kladnými	kladný	k2eAgInPc7d1	kladný
členy	člen	k1gInPc7	člen
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
součet	součet	k1gInSc4	součet
<g/>
.	.	kIx.	.
</s>
<s>
Alternující	alternující	k2eAgFnSc1d1	alternující
řada	řada	k1gFnSc1	řada
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
členy	člen	k1gMnPc4	člen
pravidelně	pravidelně	k6eAd1	pravidelně
střídají	střídat	k5eAaImIp3nP	střídat
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
dy	dy	k?	dy
limitu	limit	k1gInSc6	limit
Teleskopická	teleskopický	k2eAgFnSc1d1	teleskopická
řada	řada	k1gFnSc1	řada
Mocninné	mocninný	k2eAgFnSc2d1	mocninná
řady	řada	k1gFnSc2	řada
</s>
