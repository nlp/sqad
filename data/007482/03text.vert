<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
<g/>
.	.	kIx.	.
</s>
<s>
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
lidské	lidský	k2eAgNnSc1d1	lidské
osídlení	osídlení	k1gNnSc1	osídlení
zde	zde	k6eAd1	zde
však	však	k9	však
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
<g/>
,	,	kIx,	,
přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
slovanské	slovanský	k2eAgNnSc1d1	slovanské
hradiště	hradiště	k1gNnSc1	hradiště
Staré	Staré	k2eAgInPc1d1	Staré
Zámky	zámek	k1gInPc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
území	území	k1gNnSc6	území
současného	současný	k2eAgNnSc2d1	současné
Brna	Brno	k1gNnSc2	Brno
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dokladem	doklad	k1gInSc7	doklad
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
území	území	k1gNnSc2	území
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
rukou	ruka	k1gFnPc2	ruka
opracovaný	opracovaný	k2eAgInSc1d1	opracovaný
kámen	kámen	k1gInSc1	kámen
nalezený	nalezený	k2eAgInSc1d1	nalezený
na	na	k7c6	na
Červeném	červený	k2eAgInSc6d1	červený
kopci	kopec	k1gInSc6	kopec
starý	starý	k2eAgInSc4d1	starý
přibližně	přibližně	k6eAd1	přibližně
700	[number]	k4	700
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Brna	Brno	k1gNnSc2	Brno
žil	žít	k5eAaImAgMnS	žít
také	také	k9	také
člověk	člověk	k1gMnSc1	člověk
kromaňonský	kromaňonský	k2eAgMnSc1d1	kromaňonský
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
nalezištěm	naleziště	k1gNnSc7	naleziště
se	se	k3xPyFc4	se
stopami	stopa	k1gFnPc7	stopa
osídlení	osídlení	k1gNnSc2	osídlení
starší	starý	k2eAgFnPc4d2	starší
doby	doba	k1gFnPc4	doba
kamenné	kamenný	k2eAgFnPc4d1	kamenná
je	být	k5eAaImIp3nS	být
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
i	i	k9	i
osídlení	osídlení	k1gNnSc4	osídlení
z	z	k7c2	z
období	období	k1gNnSc2	období
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
(	(	kIx(	(
<g/>
eneolit	eneolit	k1gInSc1	eneolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
mladší	mladý	k2eAgFnSc7d2	mladší
dobou	doba	k1gFnSc7	doba
kamennou	kamenný	k2eAgFnSc7d1	kamenná
(	(	kIx(	(
<g/>
neolit	neolit	k1gInSc1	neolit
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
prostor	prostor	k1gInSc1	prostor
Brna	Brno	k1gNnSc2	Brno
osídlen	osídlen	k2eAgInSc1d1	osídlen
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
obdobích	období	k1gNnPc6	období
pravěku	pravěk	k1gInSc2	pravěk
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
zde	zde	k6eAd1	zde
existovaly	existovat	k5eAaImAgInP	existovat
sídelní	sídelní	k2eAgInPc1d1	sídelní
útvary	útvar	k1gInPc1	útvar
centrálního	centrální	k2eAgInSc2d1	centrální
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
archeologická	archeologický	k2eAgNnPc1d1	Archeologické
naleziště	naleziště	k1gNnPc1	naleziště
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Bystrc	Bystrc	k1gFnSc1	Bystrc
(	(	kIx(	(
<g/>
sídliště	sídliště	k1gNnSc1	sídliště
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
moravskou	moravský	k2eAgFnSc7d1	Moravská
malovanou	malovaný	k2eAgFnSc7d1	malovaná
keramikou	keramika	k1gFnSc7	keramika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hradisko	hradisko	k1gNnSc1	hradisko
u	u	k7c2	u
Obřan	Obřana	k1gFnPc2	Obřana
(	(	kIx(	(
<g/>
hradiště	hradiště	k1gNnSc1	hradiště
z	z	k7c2	z
období	období	k1gNnSc2	období
popelnicových	popelnicový	k2eAgFnPc2d1	popelnicová
polí	pole	k1gFnPc2	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgInPc1d1	Staré
Zámky	zámek	k1gInPc1	zámek
u	u	k7c2	u
Líšně	Líšeň	k1gFnSc2	Líšeň
(	(	kIx(	(
<g/>
hradiště	hradiště	k1gNnSc1	hradiště
z	z	k7c2	z
období	období	k1gNnSc2	období
eneolitu	eneolit	k1gInSc2	eneolit
a	a	k8xC	a
doby	doba	k1gFnSc2	doba
hradištní	hradištní	k2eAgFnSc2d1	hradištní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Holásky	Holásek	k1gMnPc4	Holásek
(	(	kIx(	(
<g/>
mohyly	mohyla	k1gFnSc2	mohyla
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
halštatské	halštatský	k2eAgFnSc2d1	halštatská
<g/>
,	,	kIx,	,
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc4	Brno-Maloměřice
(	(	kIx(	(
<g/>
sídliště	sídliště	k1gNnSc4	sídliště
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
laténské	laténský	k2eAgNnSc4d1	laténské
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řečkovice	Řečkovice	k1gFnSc1	Řečkovice
(	(	kIx(	(
<g/>
osada	osada	k1gFnSc1	osada
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
halštatské	halštatský	k2eAgFnSc2d1	halštatská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kotlářská	kotlářský	k2eAgFnSc1d1	Kotlářská
ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
hrob	hrob	k1gInSc1	hrob
kovotepce	kovotepec	k1gMnSc2	kovotepec
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
archeologické	archeologický	k2eAgInPc4d1	archeologický
nálezy	nález	k1gInPc4	nález
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
kotlina	kotlina	k1gFnSc1	kotlina
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
příhodná	příhodný	k2eAgFnSc1d1	příhodná
k	k	k7c3	k
osídlení	osídlení	k1gNnSc3	osídlení
(	(	kIx(	(
<g/>
vlídné	vlídný	k2eAgNnSc4d1	vlídné
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
úrodné	úrodný	k2eAgFnPc1d1	úrodná
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
komunikační	komunikační	k2eAgInSc1d1	komunikační
uzel	uzel	k1gInSc1	uzel
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
kampusu	kampus	k1gInSc6	kampus
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Bohunice	Bohunice	k1gFnPc4	Bohunice
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
nalezeny	nalezen	k2eAgInPc1d1	nalezen
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
opevněné	opevněný	k2eAgFnSc2d1	opevněná
osady	osada	k1gFnSc2	osada
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
6-8	[number]	k4	6-8
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
odborného	odborný	k2eAgInSc2d1	odborný
archeologického	archeologický	k2eAgInSc2d1	archeologický
průzkumu	průzkum	k1gInSc2	průzkum
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
četné	četný	k2eAgInPc1d1	četný
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
dobové	dobový	k2eAgFnSc2d1	dobová
keramiky	keramika	k1gFnSc2	keramika
<g/>
,	,	kIx,	,
úlomky	úlomek	k1gInPc7	úlomek
pazourků	pazourek	k1gInPc2	pazourek
<g/>
,	,	kIx,	,
kostí	kost	k1gFnPc2	kost
aj.	aj.	kA	aj.
Z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
osadu	osada	k1gFnSc4	osada
kupců	kupec	k1gMnPc2	kupec
a	a	k8xC	a
lovců	lovec	k1gMnPc2	lovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ovlivňovali	ovlivňovat	k5eAaImAgMnP	ovlivňovat
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dohady	dohad	k1gInPc4	dohad
<g/>
,	,	kIx,	,
že	že	k8xS	že
osada	osada	k1gFnSc1	osada
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
obehnána	obehnat	k5eAaPmNgFnS	obehnat
i	i	k9	i
vodním	vodní	k2eAgInSc7d1	vodní
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
dostatek	dostatek	k1gInSc1	dostatek
důkazů	důkaz	k1gInPc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
tzv.	tzv.	kA	tzv.
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
Brna	Brno	k1gNnSc2	Brno
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
osídlení	osídlení	k1gNnSc1	osídlení
eneolitické	eneolitický	k2eAgNnSc1d1	eneolitické
<g/>
,	,	kIx,	,
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
až	až	k8xS	až
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
a	a	k8xC	a
halštatu	halštat	k1gInSc2	halštat
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
zatím	zatím	k6eAd1	zatím
ojedinělé	ojedinělý	k2eAgInPc4d1	ojedinělý
hroby	hrob	k1gInPc4	hrob
rámcově	rámcově	k6eAd1	rámcově
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
osídlení	osídlení	k1gNnSc1	osídlení
tohoto	tento	k3xDgInSc2	tento
areálu	areál	k1gInSc2	areál
počíná	počínat	k5eAaImIp3nS	počínat
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
–	–	k?	–
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
dnešním	dnešní	k2eAgNnSc7d1	dnešní
náměstím	náměstí	k1gNnSc7	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
ulicí	ulice	k1gFnSc7	ulice
Nádražní	nádražní	k2eAgFnSc7d1	nádražní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
získány	získat	k5eAaPmNgInP	získat
také	také	k6eAd1	také
doklady	doklad	k1gInPc1	doklad
využívání	využívání	k1gNnSc2	využívání
jižních	jižní	k2eAgInPc2d1	jižní
svahů	svah	k1gInPc2	svah
při	při	k7c6	při
ulici	ulice	k1gFnSc6	ulice
Pekařské	pekařský	k2eAgFnSc6d1	Pekařská
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgNnSc1d1	bohaté
pravěké	pravěký	k2eAgNnSc1d1	pravěké
osídlení	osídlení	k1gNnSc1	osídlení
počínaje	počínaje	k7c7	počínaje
eneolitem	eneolit	k1gInSc7	eneolit
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
také	také	k9	také
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
prostoru	prostor	k1gInSc6	prostor
Dornychu	Dornych	k1gInSc2	Dornych
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
řecký	řecký	k2eAgMnSc1d1	řecký
geograf	geograf	k1gMnSc1	geograf
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
o	o	k7c6	o
osadě	osada	k1gFnSc6	osada
Meliodunon	Meliodunona	k1gFnPc2	Meliodunona
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
První	první	k4xOgInPc4	první
doklady	doklad	k1gInPc1	doklad
slovanského	slovanský	k2eAgNnSc2d1	slovanské
osídlení	osídlení	k1gNnSc2	osídlení
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
plnilo	plnit	k5eAaImAgNnS	plnit
centrální	centrální	k2eAgFnSc4d1	centrální
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
prostoru	prostor	k1gInSc6	prostor
hradiště	hradiště	k1gNnSc2	hradiště
Staré	Staré	k2eAgInPc1d1	Staré
Zámky	zámek	k1gInPc1	zámek
u	u	k7c2	u
Líšně	Líšeň	k1gFnSc2	Líšeň
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc1d1	správní
středisko	středisko	k1gNnSc1	středisko
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
význam	význam	k1gInSc4	význam
mělo	mít	k5eAaImAgNnS	mít
již	již	k9	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sídliště	sídliště	k1gNnSc2	sídliště
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Křížové	Křížová	k1gFnSc2	Křížová
<g/>
,	,	kIx,	,
Křídlovické	Křídlovický	k2eAgFnSc2d1	Křídlovická
<g/>
,	,	kIx,	,
Ypsilantiho	Ypsilanti	k1gMnSc2	Ypsilanti
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snad	snad	k9	snad
již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
opevněné	opevněný	k2eAgNnSc1d1	opevněné
(	(	kIx(	(
<g/>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
zde	zde	k6eAd1	zde
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nepřerušeně	přerušeně	k6eNd1	přerušeně
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
opevnění	opevnění	k1gNnSc1	opevnění
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
levobřeží	levobřeží	k1gNnSc4	levobřeží
Svratky	Svratka	k1gFnSc2	Svratka
postavena	postaven	k2eAgFnSc1d1	postavena
rotunda	rotunda	k1gFnSc1	rotunda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
byly	být	k5eAaImAgInP	být
zachyceny	zachytit	k5eAaPmNgInP	zachytit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
Mendlově	Mendlův	k2eAgNnSc6d1	Mendlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
přestavěn	přestavěn	k2eAgInSc4d1	přestavěn
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1323	[number]	k4	1323
založila	založit	k5eAaPmAgFnS	založit
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gFnSc1	Rejčka
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
současný	současný	k2eAgInSc4d1	současný
kostel	kostel	k1gInSc4	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
sloužící	sloužící	k1gFnSc2	sloužící
však	však	k9	však
již	již	k9	již
nově	nově	k6eAd1	nově
založenému	založený	k2eAgInSc3d1	založený
klášteru	klášter	k1gInSc3	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
bylo	být	k5eAaImAgNnS	být
i	i	k8xC	i
významné	významný	k2eAgNnSc1d1	významné
centrum	centrum	k1gNnSc1	centrum
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
státu	stát	k1gInSc2	stát
po	po	k7c4	po
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Moravy	Morava	k1gFnSc2	Morava
touto	tento	k3xDgFnSc7	tento
dynastií	dynastie	k1gFnSc7	dynastie
roku	rok	k1gInSc2	rok
1019	[number]	k4	1019
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
zřejmě	zřejmě	k6eAd1	zřejmě
sídlila	sídlit	k5eAaImAgNnP	sídlit
údělná	údělný	k2eAgNnPc1d1	údělné
knížata	kníže	k1gNnPc1	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
zaručená	zaručený	k2eAgFnSc1d1	zaručená
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
<g/>
,	,	kIx,	,
věrohodné	věrohodný	k2eAgFnPc1d1	věrohodná
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
údaje	údaj	k1gInPc1	údaj
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
listinných	listinný	k2eAgNnPc6d1	listinné
falzech	falzum	k1gNnPc6	falzum
hlásících	hlásící	k2eAgNnPc6d1	hlásící
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
raně	raně	k6eAd1	raně
středověká	středověký	k2eAgFnSc1d1	středověká
aglomerace	aglomerace	k1gFnSc1	aglomerace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
kromě	kromě	k7c2	kromě
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
starobrněnského	starobrněnský	k2eAgInSc2d1	starobrněnský
kláštera	klášter	k1gInSc2	klášter
ještě	ještě	k9	ještě
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
podhradí	podhradí	k1gNnSc1	podhradí
na	na	k7c4	na
pravobřeží	pravobřeží	k1gNnSc4	pravobřeží
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
,	,	kIx,	,
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
svahy	svah	k1gInPc4	svah
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Pekařské	pekařský	k2eAgFnSc2d1	Pekařská
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
zejména	zejména	k9	zejména
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
také	také	k9	také
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
kostelík	kostelík	k1gInSc1	kostelík
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
s	s	k7c7	s
kryptou	krypta	k1gFnSc7	krypta
<g/>
;	;	kIx,	;
kolem	kolem	k6eAd1	kolem
bylo	být	k5eAaImAgNnS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
soudobé	soudobý	k2eAgNnSc1d1	soudobé
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
opevnění	opevnění	k1gNnSc4	opevnění
na	na	k7c6	na
přístupné	přístupný	k2eAgFnSc6d1	přístupná
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Palisádový	palisádový	k2eAgInSc1d1	palisádový
žlab	žlab	k1gInSc1	žlab
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
domu	dům	k1gInSc2	dům
Petrov	Petrov	k1gInSc1	Petrov
8	[number]	k4	8
(	(	kIx(	(
<g/>
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
)	)	kIx)	)
však	však	k9	však
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
existenci	existence	k1gFnSc4	existence
hrazeného	hrazený	k2eAgInSc2d1	hrazený
dvorce	dvorec	k1gInSc2	dvorec
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
správního	správní	k2eAgInSc2d1	správní
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
archeologických	archeologický	k2eAgInPc2d1	archeologický
výzkumů	výzkum	k1gInPc2	výzkum
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlený	k2eAgMnPc1d1	osídlený
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
navázalo	navázat	k5eAaPmAgNnS	navázat
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
archeologicky	archeologicky	k6eAd1	archeologicky
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
také	také	k9	také
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
ulic	ulice	k1gFnPc2	ulice
Dornych	Dornycha	k1gFnPc2	Dornycha
a	a	k8xC	a
Spálená	spálený	k2eAgFnSc1d1	spálená
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
přišli	přijít	k5eAaPmAgMnP	přijít
němečtí	německý	k2eAgMnPc1d1	německý
(	(	kIx(	(
<g/>
rakouští	rakouský	k2eAgMnPc1d1	rakouský
<g/>
)	)	kIx)	)
a	a	k8xC	a
valonští	valonský	k2eAgMnPc1d1	valonský
osídlenci	osídlenec	k1gMnPc1	osídlenec
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
osídlení	osídlení	k1gNnSc1	osídlení
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
celé	celý	k2eAgNnSc4d1	celé
pozdější	pozdní	k2eAgNnSc4d2	pozdější
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
v	v	k7c6	v
hradbách	hradba	k1gFnPc6	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
vrcholil	vrcholit	k5eAaImAgInS	vrcholit
vznikem	vznik	k1gInSc7	vznik
institucionálního	institucionální	k2eAgNnSc2d1	institucionální
města	město	k1gNnSc2	město
asi	asi	k9	asi
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
možná	možná	k9	možná
již	již	k6eAd1	již
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
rakouských	rakouský	k2eAgNnPc2d1	rakouské
měst	město	k1gNnPc2	město
Enže	Enž	k1gInSc2	Enž
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1243	[number]	k4	1243
Brnu	Brno	k1gNnSc6	Brno
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
udělil	udělit	k5eAaPmAgInS	udělit
městská	městský	k2eAgNnPc4d1	Městské
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
patrně	patrně	k6eAd1	patrně
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
polovinou	polovina	k1gFnSc7	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
kamenná	kamenný	k2eAgFnSc1d1	kamenná
hradba	hradba	k1gFnSc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
město	město	k1gNnSc1	město
právo	práv	k2eAgNnSc1d1	právo
volit	volit	k5eAaImF	volit
rychtáře	rychtář	k1gMnPc4	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1349	[number]	k4	1349
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
trvalým	trvalý	k2eAgInSc7d1	trvalý
sídlem	sídlo	k1gNnSc7	sídlo
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Olomouce	Olomouc	k1gFnSc2	Olomouc
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
moravským	moravský	k2eAgNnSc7d1	Moravské
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
zasedal	zasedat	k5eAaImAgInS	zasedat
zde	zde	k6eAd1	zde
i	i	k9	i
zemský	zemský	k2eAgInSc4d1	zemský
soud	soud	k1gInSc4	soud
a	a	k8xC	a
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vedla	vést	k5eAaImAgFnS	vést
jedna	jeden	k4xCgFnSc1	jeden
řada	řada	k1gFnSc1	řada
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
sevřený	sevřený	k2eAgInSc1d1	sevřený
hradbami	hradba	k1gFnPc7	hradba
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
necelých	celý	k2eNgFnPc2d1	necelá
37	[number]	k4	37
ha	ha	kA	ha
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
v	v	k7c6	v
lucemburské	lucemburský	k2eAgFnSc6d1	Lucemburská
době	doba	k1gFnSc6	doba
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
kolem	kolem	k7c2	kolem
8	[number]	k4	8
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
války	válka	k1gFnPc1	válka
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1428	[number]	k4	1428
a	a	k8xC	a
1430	[number]	k4	1430
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pevnou	pevný	k2eAgFnSc7d1	pevná
baštou	bašta	k1gFnSc7	bašta
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
přispěl	přispět	k5eAaPmAgMnS	přispět
jeho	on	k3xPp3gInSc4	on
převážně	převážně	k6eAd1	převážně
německý	německý	k2eAgInSc4d1	německý
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
město	město	k1gNnSc4	město
nedobyli	dobýt	k5eNaPmAgMnP	dobýt
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
však	však	k9	však
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
blízké	blízký	k2eAgFnPc1d1	blízká
vsi	ves	k1gFnPc1	ves
a	a	k8xC	a
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
JIž	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
první	první	k4xOgInPc4	první
zděné	zděný	k2eAgInPc4d1	zděný
měšťanské	měšťanský	k2eAgInPc4d1	měšťanský
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ještě	ještě	k9	ještě
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
desetiletí	desetiletí	k1gNnSc2	desetiletí
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
převážně	převážně	k6eAd1	převážně
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
přechodu	přechod	k1gInSc2	přechod
ke	k	k7c3	k
zděné	zděný	k2eAgFnSc3d1	zděná
architektuře	architektura	k1gFnSc3	architektura
se	se	k3xPyFc4	se
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
zástavby	zástavba	k1gFnSc2	zástavba
výrazně	výrazně	k6eAd1	výrazně
urychlil	urychlit	k5eAaPmAgInS	urychlit
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
mělo	mít	k5eAaImAgNnS	mít
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
zděný	zděný	k2eAgInSc4d1	zděný
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
cihly	cihla	k1gFnPc1	cihla
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
cihlových	cihlový	k2eAgFnPc2d1	cihlová
staveb	stavba	k1gFnPc2	stavba
lze	lze	k6eAd1	lze
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1641	[number]	k4	1641
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Olomouce	Olomouc	k1gFnSc2	Olomouc
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
faktickým	faktický	k2eAgNnSc7d1	faktické
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Moravy	Morava	k1gFnSc2	Morava
mj.	mj.	kA	mj.
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
narychlo	narychlo	k6eAd1	narychlo
přemístěny	přemístěn	k2eAgFnPc4d1	přemístěna
zemské	zemský	k2eAgFnPc4d1	zemská
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
sporu	spor	k1gInSc2	spor
obou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
učinil	učinit	k5eAaImAgMnS	učinit
však	však	k9	však
až	až	k9	až
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přiznal	přiznat	k5eAaPmAgMnS	přiznat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
označení	označení	k1gNnSc4	označení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
definitivně	definitivně	k6eAd1	definitivně
Brnu	Brno	k1gNnSc3	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1643	[number]	k4	1643
a	a	k8xC	a
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
bylo	být	k5eAaImAgNnS	být
samotné	samotný	k2eAgNnSc1d1	samotné
Brno	Brno	k1gNnSc1	Brno
neúspěšně	úspěšně	k6eNd1	úspěšně
obléháno	obléhat	k5eAaImNgNnS	obléhat
osmnáctitisícovým	osmnáctitisícový	k2eAgNnSc7d1	osmnáctitisícové
švédským	švédský	k2eAgNnSc7d1	švédské
vojskem	vojsko	k1gNnSc7	vojsko
generála	generál	k1gMnSc2	generál
Torstensona	Torstenson	k1gMnSc2	Torstenson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
Brno	Brno	k1gNnSc4	Brno
použít	použít	k5eAaPmF	použít
jako	jako	k8xC	jako
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
finální	finální	k2eAgInSc4d1	finální
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
Torstensonovu	Torstensonův	k2eAgNnSc3d1	Torstensonův
vojsku	vojsko	k1gNnSc3	vojsko
přidalo	přidat	k5eAaPmAgNnS	přidat
ještě	ještě	k9	ještě
desetitisícové	desetitisícový	k2eAgNnSc1d1	desetitisícové
vojsko	vojsko	k1gNnSc1	vojsko
sedmihradského	sedmihradský	k2eAgMnSc2d1	sedmihradský
knížete	kníže	k1gMnSc2	kníže
Jiřího	Jiří	k1gMnSc2	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
část	část	k1gFnSc1	část
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
odvolána	odvolat	k5eAaPmNgFnS	odvolat
k	k	k7c3	k
Lednici	Lednice	k1gFnSc3	Lednice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
obléhání	obléhání	k1gNnSc1	obléhání
město	město	k1gNnSc4	město
bránilo	bránit	k5eAaImAgNnS	bránit
pouze	pouze	k6eAd1	pouze
1476	[number]	k4	1476
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vojáci	voják	k1gMnPc1	voják
tvořili	tvořit	k5eAaImAgMnP	tvořit
necelou	celý	k2eNgFnSc4d1	necelá
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tvrdosti	tvrdost	k1gFnSc3	tvrdost
a	a	k8xC	a
nasazení	nasazení	k1gNnSc4	nasazení
obránců	obránce	k1gMnPc2	obránce
a	a	k8xC	a
geniální	geniální	k2eAgFnSc4d1	geniální
organizaci	organizace	k1gFnSc4	organizace
obrany	obrana	k1gFnSc2	obrana
důstojníkem	důstojník	k1gMnSc7	důstojník
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
Raduitem	Raduit	k1gInSc7	Raduit
de	de	k?	de
Souches	Souchesa	k1gFnPc2	Souchesa
však	však	k9	však
Švédové	Švéd	k1gMnPc1	Švéd
neuspěli	uspět	k5eNaPmAgMnP	uspět
a	a	k8xC	a
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
8	[number]	k4	8
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
byli	být	k5eAaImAgMnP	být
donuceni	donucen	k2eAgMnPc1d1	donucen
obléhání	obléhání	k1gNnSc3	obléhání
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
obléháno	obléhán	k2eAgNnSc1d1	obléháno
pruskými	pruský	k2eAgNnPc7d1	pruské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1749	[number]	k4	1749
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
založen	založit	k5eAaPmNgInS	založit
moravský	moravský	k2eAgInSc1d1	moravský
soudní	soudní	k2eAgInSc1d1	soudní
a	a	k8xC	a
politický	politický	k2eAgInSc1d1	politický
senát	senát	k1gInSc1	senát
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
textilní	textilní	k2eAgFnSc1d1	textilní
manufaktura	manufaktura	k1gFnSc1	manufaktura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
brněnské	brněnský	k2eAgNnSc1d1	brněnské
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
,	,	kIx,	,
podléhající	podléhající	k2eAgFnSc1d1	podléhající
však	však	k9	však
olomouckému	olomoucký	k2eAgNnSc3d1	olomoucké
arcibiskupství	arcibiskupství	k1gNnSc3	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nocoval	nocovat	k5eAaImAgMnS	nocovat
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
před	před	k7c7	před
důležitou	důležitý	k2eAgFnSc7d1	důležitá
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
francouzských	francouzský	k2eAgNnPc2d1	francouzské
vojsk	vojsko	k1gNnPc2	vojsko
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
značnou	značný	k2eAgFnSc7d1	značná
přítěží	přítěž	k1gFnSc7	přítěž
<g/>
,	,	kIx,	,
spotřebovávala	spotřebovávat	k5eAaImAgFnS	spotřebovávat
množství	množství	k1gNnSc4	množství
potravinových	potravinový	k2eAgFnPc2d1	potravinová
zásob	zásoba	k1gFnPc2	zásoba
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
měšťanů	měšťan	k1gMnPc2	měšťan
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
šířit	šířit	k5eAaImF	šířit
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
bylo	být	k5eAaImAgNnS	být
císařským	císařský	k2eAgInSc7d1	císařský
dekretem	dekret	k1gInSc7	dekret
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
založeno	založit	k5eAaPmNgNnS	založit
Františkovo	Františkův	k2eAgNnSc1d1	Františkovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhé	druhý	k4xOgNnSc4	druhý
nejstarší	starý	k2eAgNnSc4d3	nejstarší
(	(	kIx(	(
<g/>
po	po	k7c6	po
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
)	)	kIx)	)
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1839	[number]	k4	1839
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
železnicí	železnice	k1gFnSc7	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
veřejné	veřejný	k2eAgNnSc1d1	veřejné
osvětlení	osvětlení	k1gNnSc1	osvětlení
plynovými	plynový	k2eAgFnPc7d1	plynová
lampami	lampa	k1gFnPc7	lampa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
zaveden	zavést	k5eAaPmNgInS	zavést
telegraf	telegraf	k1gInSc1	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
působil	působit	k5eAaImAgInS	působit
vrchní	vrchní	k2eAgInSc1d1	vrchní
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
působnost	působnost	k1gFnSc1	působnost
se	se	k3xPyFc4	se
vztahovala	vztahovat	k5eAaImAgFnS	vztahovat
jak	jak	k6eAd1	jak
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
České	český	k2eAgNnSc4d1	české
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojeno	připojen	k2eAgNnSc1d1	připojeno
19	[number]	k4	19
dalších	další	k2eAgNnPc2d1	další
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
na	na	k7c4	na
1	[number]	k4	1
732	[number]	k4	732
hektarů	hektar	k1gInPc2	hektar
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc4	jeho
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
území	území	k1gNnSc1	území
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zhruba	zhruba	k6eAd1	zhruba
krylo	krýt	k5eAaImAgNnS	krýt
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
okresy	okres	k1gInPc4	okres
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stály	stát	k5eAaImAgInP	stát
okresní	okresní	k2eAgInPc1d1	okresní
výbory	výbor	k1gInPc1	výbor
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
představeným	představený	k1gMnSc7	představený
a	a	k8xC	a
náměstkem	náměstek	k1gMnSc7	náměstek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zachovaly	zachovat	k5eAaPmAgInP	zachovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přestavěnou	přestavěný	k2eAgFnSc7d1	přestavěná
Měnínskou	Měnínský	k2eAgFnSc7d1	Měnínská
bránou	brána	k1gFnSc7	brána
jen	jen	k9	jen
krátké	krátký	k2eAgInPc1d1	krátký
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
přiléhající	přiléhající	k2eAgMnSc1d1	přiléhající
k	k	k7c3	k
ulicím	ulice	k1gFnPc3	ulice
Husova	Husův	k2eAgNnSc2d1	Husovo
<g/>
,	,	kIx,	,
Bašty	Bašta	k1gMnSc2	Bašta
a	a	k8xC	a
k	k	k7c3	k
Denisovým	Denisový	k2eAgInPc3d1	Denisový
Sadům	sad	k1gInPc3	sad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zboření	zboření	k1gNnSc6	zboření
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
nastal	nastat	k5eAaPmAgInS	nastat
prudký	prudký	k2eAgInSc1d1	prudký
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1869	[number]	k4	1869
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1873	[number]	k4	1873
přinesl	přinést	k5eAaPmAgInS	přinést
první	první	k4xOgFnSc4	první
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
technický	technický	k2eAgInSc1d1	technický
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1881	[number]	k4	1881
a	a	k8xC	a
1882	[number]	k4	1882
bylo	být	k5eAaImAgNnS	být
nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
osvětleno	osvětlen	k2eAgNnSc1d1	osvětleno
jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
evropské	evropský	k2eAgNnSc1d1	Evropské
divadlo	divadlo	k1gNnSc1	divadlo
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
žárovkami	žárovka	k1gFnPc7	žárovka
T.	T.	kA	T.
A.	A.	kA	A.
Edisona	Edison	k1gMnSc2	Edison
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
první	první	k4xOgNnSc4	první
české	český	k2eAgNnSc4d1	české
knihkupectví	knihkupectví	k1gNnSc4	knihkupectví
Joži	Joža	k1gMnSc2	Joža
Barviče	barvič	k1gMnSc2	barvič
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
i	i	k9	i
řeč	řeč	k1gFnSc4	řeč
profesora	profesor	k1gMnSc2	profesor
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1882	[number]	k4	1882
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
založené	založený	k2eAgFnSc2d1	založená
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
asanace	asanace	k1gFnSc1	asanace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
238	[number]	k4	238
domů	dům	k1gInPc2	dům
a	a	k8xC	a
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
čeští	český	k2eAgMnPc1d1	český
vlastenci	vlastenec	k1gMnPc1	vlastenec
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
jako	jako	k8xC	jako
poslanec	poslanec	k1gMnSc1	poslanec
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
Říšské	říšský	k2eAgFnSc2d1	říšská
Rady	rada	k1gFnSc2	rada
sbíral	sbírat	k5eAaImAgInS	sbírat
podpisy	podpis	k1gInPc4	podpis
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
založení	založení	k1gNnSc2	založení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
německé	německý	k2eAgFnSc3d1	německá
menšině	menšina	k1gFnSc3	menšina
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
to	ten	k3xDgNnSc1	ten
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
existence	existence	k1gFnSc2	existence
čtyř	čtyři	k4xCgInPc2	čtyři
okresních	okresní	k2eAgInPc2d1	okresní
výborů	výbor	k1gInPc2	výbor
neosvědčila	osvědčit	k5eNaPmAgNnP	osvědčit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1905	[number]	k4	1905
vydán	vydán	k2eAgInSc1d1	vydán
nový	nový	k2eAgInSc1d1	nový
statut	statut	k1gInSc1	statut
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
okresní	okresní	k2eAgInSc4d1	okresní
výbory	výbor	k1gInPc7	výbor
rušil	rušit	k5eAaImAgMnS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
každého	každý	k3xTgInSc2	každý
okresu	okres	k1gInSc2	okres
pak	pak	k6eAd1	pak
volilo	volit	k5eAaImAgNnS	volit
městské	městský	k2eAgNnSc1d1	Městské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
představeného	představený	k1gMnSc4	představený
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
náměstky	náměstek	k1gMnPc4	náměstek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
konal	konat	k5eAaImAgInS	konat
sjezd	sjezd	k1gInSc4	sjezd
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Volkstag	Volkstag	k1gInSc1	Volkstag
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potyčkám	potyčka	k1gFnPc3	potyčka
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Němců	Němec	k1gMnPc2	Němec
byla	být	k5eAaImAgFnS	být
povolána	povolán	k2eAgFnSc1d1	povolána
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Brna	Brno	k1gNnSc2	Brno
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
jeden	jeden	k4xCgMnSc1	jeden
český	český	k2eAgMnSc1d1	český
dělník	dělník	k1gMnSc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
převzato	převzít	k5eAaPmNgNnS	převzít
Brno	Brno	k1gNnSc4	Brno
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
univerzita	univerzita	k1gFnSc1	univerzita
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
zvěrolékařská	zvěrolékařský	k2eAgFnSc1d1	zvěrolékařská
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
nynější	nynější	k2eAgFnSc1d1	nynější
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
a	a	k8xC	a
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
univerzita	univerzita	k1gFnSc1	univerzita
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
školy	škola	k1gFnPc1	škola
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
typu	typ	k1gInSc2	typ
následovaly	následovat	k5eAaImAgFnP	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
;	;	kIx,	;
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
také	také	k9	také
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
213	[number]	k4	213
<g/>
/	/	kIx~	/
<g/>
1919	[number]	k4	1919
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
o	o	k7c4	o
sloučení	sloučení	k1gNnSc4	sloučení
sousedních	sousední	k2eAgFnPc2d1	sousední
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
<g/>
,	,	kIx,	,
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojena	připojit	k5eAaPmNgFnS	připojit
2	[number]	k4	2
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Husovice	Husovice	k1gFnPc1	Husovice
a	a	k8xC	a
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
<g/>
)	)	kIx)	)
a	a	k8xC	a
21	[number]	k4	21
dalších	další	k2eAgFnPc2d1	další
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
Brněnské	brněnský	k2eAgFnPc1d1	brněnská
Ivanovice	Ivanovice	k1gFnPc1	Ivanovice
<g/>
,	,	kIx,	,
Černovice	Černovice	k1gFnPc1	Černovice
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnPc1d1	dolní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Juliánov	Juliánov	k1gInSc1	Juliánov
<g/>
,	,	kIx,	,
Jundrov	Jundrov	k1gInSc1	Jundrov
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Kohoutovice	Kohoutovice	k1gFnPc1	Kohoutovice
<g/>
,	,	kIx,	,
Komárov	Komárov	k1gInSc1	Komárov
<g/>
,	,	kIx,	,
Komín	Komín	k1gInSc1	Komín
<g/>
,	,	kIx,	,
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
Maloměřice	Maloměřice	k1gFnPc1	Maloměřice
<g/>
,	,	kIx,	,
Medlánky	Medlánek	k1gInPc1	Medlánek
<g/>
,	,	kIx,	,
Obřany	Obřan	k1gInPc1	Obřan
<g/>
,	,	kIx,	,
Přízřenice	Přízřenice	k1gFnSc1	Přízřenice
<g/>
,	,	kIx,	,
Řečkovice	Řečkovice	k1gFnSc1	Řečkovice
<g/>
,	,	kIx,	,
Slatina	slatina	k1gFnSc1	slatina
<g/>
,	,	kIx,	,
Tuřany	Tuřana	k1gFnPc1	Tuřana
<g/>
,	,	kIx,	,
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
a	a	k8xC	a
Židenice	Židenice	k1gFnPc1	Židenice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
Brno	Brno	k1gNnSc1	Brno
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
12	[number]	k4	12
379	[number]	k4	379
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Velká	velký	k2eAgFnSc1d1	velká
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1928	[number]	k4	1928
bylo	být	k5eAaImAgNnS	být
Výstavou	výstava	k1gFnSc7	výstava
soudobé	soudobý	k2eAgFnSc2d1	soudobá
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1929	[number]	k4	1929
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
proslulá	proslulý	k2eAgFnSc1d1	proslulá
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
Vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
členové	člen	k1gMnPc1	člen
Národní	národní	k2eAgFnSc2d1	národní
obce	obec	k1gFnSc2	obec
fašistické	fašistický	k2eAgFnSc2d1	fašistická
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Židenický	Židenický	k2eAgInSc4d1	Židenický
puč	puč	k1gInSc4	puč
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
armádou	armáda	k1gFnSc7	armáda
potlačen	potlačit	k5eAaPmNgMnS	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
brněnští	brněnský	k2eAgMnPc1d1	brněnský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
menšina	menšina	k1gFnSc1	menšina
čítala	čítat	k5eAaImAgFnS	čítat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
přes	přes	k7c4	přes
50.000	[number]	k4	50.000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
puč	puč	k1gInSc4	puč
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
obsadili	obsadit	k5eAaPmAgMnP	obsadit
ještě	ještě	k9	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
německé	německý	k2eAgFnSc2d1	německá
okupační	okupační	k2eAgFnSc2d1	okupační
armády	armáda	k1gFnSc2	armáda
důležité	důležitý	k2eAgFnSc2d1	důležitá
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
starosta	starosta	k1gMnSc1	starosta
Rudolf	Rudolf	k1gMnSc1	Rudolf
Spazier	Spazier	k1gMnSc1	Spazier
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vydat	vydat	k5eAaPmF	vydat
brněnským	brněnský	k2eAgMnPc3d1	brněnský
Němcům	Němec	k1gMnPc3	Němec
radnici	radnice	k1gFnSc4	radnice
a	a	k8xC	a
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
přepadeny	přepaden	k2eAgInPc1d1	přepaden
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
studenti	student	k1gMnPc1	student
odvlečeni	odvlečen	k2eAgMnPc1d1	odvlečen
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
z	z	k7c2	z
Kounicových	Kounicový	k2eAgFnPc2d1	Kounicová
kolejí	kolej	k1gFnPc2	kolej
stala	stát	k5eAaPmAgFnS	stát
věznice	věznice	k1gFnSc1	věznice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
popravy	poprava	k1gFnPc1	poprava
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
odtud	odtud	k6eAd1	odtud
vypravovány	vypravován	k2eAgFnPc1d1	vypravována
transporty	transporta	k1gFnPc1	transporta
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojen	připojen	k2eAgInSc4d1	připojen
městys	městys	k1gInSc4	městys
Líšeň	Líšeň	k1gFnSc1	Líšeň
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
několikrát	několikrát	k6eAd1	několikrát
stalo	stát	k5eAaPmAgNnS	stát
terčem	terč	k1gInSc7	terč
anglo-amerických	anglomerický	k2eAgInPc2d1	anglo-americký
náletů	nálet	k1gInPc2	nálet
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Bratislavsko-brněnské	bratislavskorněnský	k2eAgFnSc2d1	bratislavsko-brněnský
operace	operace	k1gFnSc2	operace
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
prvnímu	první	k4xOgNnSc3	první
jednání	jednání	k1gNnSc4	jednání
Revoluční	revoluční	k2eAgInSc1d1	revoluční
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
třemi	tři	k4xCgInPc7	tři
náměstky	náměstek	k1gMnPc7	náměstek
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
předměstích	předměstí	k1gNnPc6	předměstí
i	i	k8xC	i
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
městě	město	k1gNnSc6	město
26	[number]	k4	26
místních	místní	k2eAgInPc2d1	místní
národních	národní	k2eAgInPc2d1	národní
výborů	výbor	k1gInPc2	výbor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
neměly	mít	k5eNaImAgFnP	mít
vždy	vždy	k6eAd1	vždy
přesně	přesně	k6eAd1	přesně
vymezené	vymezený	k2eAgFnPc4d1	vymezená
kompetence	kompetence	k1gFnPc4	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
vysídlení	vysídlení	k1gNnSc1	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Brněnský	brněnský	k2eAgInSc1d1	brněnský
pochod	pochod	k1gInSc1	pochod
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
moravských	moravský	k2eAgMnPc2d1	moravský
Němců	Němec	k1gMnPc2	Němec
odvedeno	odveden	k2eAgNnSc1d1	odvedeno
pěšky	pěšky	k6eAd1	pěšky
na	na	k7c4	na
rakouské	rakouský	k2eAgNnSc4d1	rakouské
území	území	k1gNnSc4	území
a	a	k8xC	a
1691	[number]	k4	1691
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
MNV	MNV	kA	MNV
pak	pak	k8xC	pak
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
působily	působit	k5eAaImAgInP	působit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1947	[number]	k4	1947
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
obvodními	obvodní	k2eAgFnPc7d1	obvodní
radami	rada	k1gFnPc7	rada
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
10	[number]	k4	10
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
existujících	existující	k2eAgFnPc2d1	existující
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
správní	správní	k2eAgFnSc3d1	správní
reformě	reforma	k1gFnSc3	reforma
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
komunismu	komunismus	k1gInSc2	komunismus
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
k	k	k7c3	k
několika	několik	k4yIc3	několik
dalším	další	k2eAgFnPc3d1	další
správním	správní	k2eAgFnPc3d1	správní
reformám	reforma	k1gFnPc3	reforma
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojena	připojit	k5eAaPmNgFnS	připojit
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
oblast	oblast	k1gFnSc1	oblast
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
katastrů	katastr	k1gInPc2	katastr
obcí	obec	k1gFnPc2	obec
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
Kníničky	Knínička	k1gFnPc1	Knínička
<g/>
,	,	kIx,	,
Rozdrojovice	Rozdrojovice	k1gFnPc1	Rozdrojovice
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgFnPc1d1	Moravská
Knínice	Knínice	k1gFnPc1	Knínice
<g/>
,	,	kIx,	,
Chudčice	Chudčice	k1gFnSc1	Chudčice
a	a	k8xC	a
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
<g/>
)	)	kIx)	)
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
31,8	[number]	k4	31,8
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
bylo	být	k5eAaImAgNnS	být
zbudováno	zbudován	k2eAgNnSc1d1	zbudováno
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Brno-Tuřany	Brno-Tuřan	k1gMnPc4	Brno-Tuřan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
Černovicích	Černovice	k1gFnPc6	Černovice
výstavba	výstavba	k1gFnSc1	výstavba
prvního	první	k4xOgNnSc2	první
brněnského	brněnský	k2eAgNnSc2d1	brněnské
panelového	panelový	k2eAgNnSc2d1	panelové
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
masivní	masivní	k2eAgFnSc1d1	masivní
výstavba	výstavba	k1gFnSc1	výstavba
panelových	panelový	k2eAgNnPc2d1	panelové
sídlišť	sídliště	k1gNnPc2	sídliště
pak	pak	k6eAd1	pak
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
podepsala	podepsat	k5eAaPmAgFnS	podepsat
na	na	k7c4	na
vzhledu	vzhled	k1gInSc2	vzhled
mnoha	mnoho	k4c2	mnoho
brněnských	brněnský	k2eAgFnPc2d1	brněnská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojeny	připojen	k2eAgFnPc1d1	připojena
obce	obec	k1gFnPc1	obec
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
Holásky	Holásek	k1gMnPc4	Holásek
<g/>
,	,	kIx,	,
Kníničky	Knínička	k1gFnPc4	Knínička
<g/>
,	,	kIx,	,
Mokrá	mokrat	k5eAaImIp3nS	mokrat
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
Moravan	Moravan	k1gMnSc1	Moravan
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgFnSc1d1	Nové
Moravany	Moravan	k1gMnPc7	Moravan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
rozloha	rozloha	k1gFnSc1	rozloha
Brna	Brno	k1gNnSc2	Brno
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
na	na	k7c4	na
18	[number]	k4	18
069	[number]	k4	069
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
bylo	být	k5eAaImAgNnS	být
zbudováno	zbudován	k2eAgNnSc1d1	zbudováno
i	i	k8xC	i
nové	nový	k2eAgNnSc1d1	nové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna	Brno	k1gNnSc2	Brno
provedena	proveden	k2eAgFnSc1d1	provedena
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
katastrální	katastrální	k2eAgFnSc1d1	katastrální
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1971	[number]	k4	1971
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojeny	připojit	k5eAaPmNgFnP	připojit
obce	obec	k1gFnPc1	obec
Bosonohy	Bosonohy	k?	Bosonohy
<g/>
,	,	kIx,	,
Dvorska	Dvorska	k1gFnSc1	Dvorska
<g/>
,	,	kIx,	,
Chrlice	Chrlice	k1gFnPc1	Chrlice
<g/>
,	,	kIx,	,
Ivanovice	Ivanovice	k1gFnPc1	Ivanovice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Jehnice	jehnice	k1gFnSc1	jehnice
<g/>
,	,	kIx,	,
Ořešín	Ořešín	k1gInSc1	Ořešín
<g/>
,	,	kIx,	,
Soběšice	Soběšice	k1gFnSc1	Soběšice
a	a	k8xC	a
Žebětín	Žebětín	k1gInSc1	Žebětín
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
připojen	připojit	k5eAaPmNgInS	připojit
Útěchov	Útěchov	k1gInSc1	Útěchov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
závodní	závodní	k2eAgFnSc1d1	závodní
trať	trať	k1gFnSc1	trať
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
začala	začít	k5eAaPmAgFnS	začít
řada	řada	k1gFnSc1	řada
bývalých	bývalý	k2eAgFnPc2d1	bývalá
obcí	obec	k1gFnPc2	obec
požadovat	požadovat	k5eAaImF	požadovat
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
čtvrtě	čtvrt	k1gFnPc4	čtvrt
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Kníničky	Knínička	k1gFnPc1	Knínička
<g/>
)	)	kIx)	)
uvažovaly	uvažovat	k5eAaImAgFnP	uvažovat
i	i	k9	i
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
schválil	schválit	k5eAaPmAgInS	schválit
poslední	poslední	k2eAgInSc1d1	poslední
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
usnesení	usnesení	k1gNnSc2	usnesení
č.	č.	k?	č.
XXIII	XXIII	kA	XXIII
<g/>
/	/	kIx~	/
<g/>
110	[number]	k4	110
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
k	k	k7c3	k
územnímu	územní	k2eAgNnSc3d1	územní
členění	členění	k1gNnSc3	členění
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
v	v	k7c6	v
hrubých	hrubý	k2eAgInPc6d1	hrubý
rysech	rys	k1gInPc6	rys
stanovil	stanovit	k5eAaPmAgInS	stanovit
nové	nový	k2eAgNnSc4d1	nové
členění	členění	k1gNnSc4	členění
Brna	Brno	k1gNnSc2	Brno
na	na	k7c4	na
29	[number]	k4	29
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
začaly	začít	k5eAaPmAgFnP	začít
existovat	existovat	k5eAaImF	existovat
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
se	se	k3xPyFc4	se
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Heršpicích	Heršpice	k1gFnPc6	Heršpice
a	a	k8xC	a
Přízřenicích	Přízřenice	k1gFnPc6	Přízřenice
konalo	konat	k5eAaImAgNnS	konat
místní	místní	k2eAgNnSc1d1	místní
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
oddělení	oddělení	k1gNnSc6	oddělení
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
nové	nový	k2eAgFnSc2d1	nová
obce	obec	k1gFnSc2	obec
Dolní	dolní	k2eAgFnSc2d1	dolní
Heršpice-Přízřenice	Heršpice-Přízřenice	k1gFnSc2	Heršpice-Přízřenice
<g/>
,	,	kIx,	,
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
snahách	snaha	k1gFnPc6	snaha
podrobněji	podrobně	k6eAd2	podrobně
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
článek	článek	k1gInSc1	článek
Dolní	dolní	k2eAgFnSc2d1	dolní
Heršpice-Přízřenice	Heršpice-Přízřenice	k1gFnSc2	Heršpice-Přízřenice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
významných	významný	k2eAgFnPc2d1	významná
institucí	instituce	k1gFnPc2	instituce
s	s	k7c7	s
celostátní	celostátní	k2eAgFnSc7d1	celostátní
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
nebo	nebo	k8xC	nebo
naposledy	naposledy	k6eAd1	naposledy
úřad	úřad	k1gInSc1	úřad
Veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	právo	k1gNnPc2	právo
–	–	k?	–
ombudsmana	ombudsman	k1gMnSc2	ombudsman
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
územěsprávního	územěsprávní	k2eAgNnSc2d1	územěsprávní
však	však	k9	však
význam	význam	k1gInSc1	význam
Brna	Brno	k1gNnSc2	Brno
nebývale	nebývale	k6eAd1	nebývale
klesl	klesnout	k5eAaPmAgInS	klesnout
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
relativně	relativně	k6eAd1	relativně
malého	malý	k2eAgInSc2d1	malý
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna	Brno	k1gNnSc2	Brno
k	k	k7c3	k
postupně	postupně	k6eAd1	postupně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
výstavba	výstavba	k1gFnSc1	výstavba
dalších	další	k2eAgInPc2d1	další
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
rekonstrukce	rekonstrukce	k1gFnPc1	rekonstrukce
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
fasád	fasáda	k1gFnPc2	fasáda
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
domů	dům	k1gInPc2	dům
v	v	k7c6	v
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zanedbávaném	zanedbávaný	k2eAgNnSc6d1	zanedbávané
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
velkého	velký	k2eAgInSc2d1	velký
špalíčku	špalíček	k1gInSc2	špalíček
ke	k	k7c3	k
zboření	zboření	k1gNnSc3	zboření
historicky	historicky	k6eAd1	historicky
cenných	cenný	k2eAgInPc2d1	cenný
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
vybombardována	vybombardován	k2eAgFnSc1d1	vybombardována
už	už	k9	už
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
zde	zde	k6eAd1	zde
poněkud	poněkud	k6eAd1	poněkud
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgInP	být
také	také	k9	také
zrekonstruovány	zrekonstruovat	k5eAaPmNgInP	zrekonstruovat
Dům	dům	k1gInSc1	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lipé	Lipé	k1gNnSc2	Lipé
a	a	k8xC	a
Dům	dům	k1gInSc1	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
často	často	k6eAd1	často
kritizovaná	kritizovaný	k2eAgFnSc1d1	kritizovaná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
náměstí	náměstí	k1gNnSc2	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydlážděno	vydláždit	k5eAaPmNgNnS	vydláždit
žulovými	žulový	k2eAgInPc7d1	žulový
bloky	blok	k1gInPc7	blok
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
fontána	fontána	k1gFnSc1	fontána
s	s	k7c7	s
verši	verš	k1gInPc7	verš
brněnského	brněnský	k2eAgMnSc2d1	brněnský
básníka	básník	k1gMnSc2	básník
Jana	Jan	k1gMnSc2	Jan
Skácela	Skácel	k1gMnSc2	Skácel
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
pítko	pítko	k1gNnSc1	pítko
a	a	k8xC	a
květinová	květinový	k2eAgFnSc1d1	květinová
výzdoba	výzdoba	k1gFnSc1	výzdoba
na	na	k7c4	na
sloupy	sloup	k1gInPc4	sloup
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
.	.	kIx.	.
</s>
