<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
<g/>
.	.	kIx.	.
</s>
