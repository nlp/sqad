<s>
Hamsa	Hamsa	k1gFnSc1	Hamsa
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
خ	خ	k?	خ
chamsa	chamsa	k1gFnSc1	chamsa
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ח	ח	k?	ח
<g/>
ַ	ַ	k?	ַ
<g/>
מ	מ	k?	מ
<g/>
ְ	ְ	k?	ְ
<g/>
ס	ס	k?	ס
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
též	též	k9	též
"	"	kIx"	"
<g/>
Fátimina	Fátimin	k2eAgFnSc1d1	Fátimin
ruka	ruka	k1gFnSc1	ruka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
"	"	kIx"	"
<g/>
Ruka	ruka	k1gFnSc1	ruka
Fátimy	Fátima	k1gFnSc2	Fátima
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Ruka	ruka	k1gFnSc1	ruka
Marie	Maria	k1gFnSc2	Maria
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc4	symbol
a	a	k8xC	a
amulet	amulet	k1gInSc4	amulet
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
lidské	lidský	k2eAgFnSc2d1	lidská
ruky	ruka	k1gFnSc2	ruka
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývá	bývat	k5eAaImIp3nS	bývat
běžně	běžně	k6eAd1	běžně
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xS	jako
šperk	šperk	k1gInSc1	šperk
či	či	k8xC	či
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
