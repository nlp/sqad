<p>
<s>
Hamsa	Hamsa	k1gFnSc1	Hamsa
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
خ	خ	k?	خ
chamsa	chamsa	k1gFnSc1	chamsa
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ח	ח	k?	ח
<g/>
ַ	ַ	k?	ַ
<g/>
מ	מ	k?	מ
<g/>
ְ	ְ	k?	ְ
<g/>
ס	ס	k?	ס
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
též	též	k9	též
"	"	kIx"	"
<g/>
Fátimina	Fátimin	k2eAgFnSc1d1	Fátimin
ruka	ruka	k1gFnSc1	ruka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
"	"	kIx"	"
<g/>
Ruka	ruka	k1gFnSc1	ruka
Fátimy	Fátima	k1gFnSc2	Fátima
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Ruka	ruka	k1gFnSc1	ruka
Marie	Maria	k1gFnSc2	Maria
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc4	symbol
a	a	k8xC	a
amulet	amulet	k1gInSc4	amulet
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
lidské	lidský	k2eAgFnSc2d1	lidská
ruky	ruka	k1gFnSc2	ruka
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývá	bývat	k5eAaImIp3nS	bývat
běžně	běžně	k6eAd1	běžně
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xS	jako
šperk	šperk	k1gInSc1	šperk
či	či	k8xC	či
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
dlaň	dlaň	k1gFnSc4	dlaň
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
společnostech	společnost	k1gFnPc6	společnost
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
znamení	znamení	k1gNnSc4	znamení
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Hamsa	Hamsa	k1gFnSc1	Hamsa
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
především	především	k9	především
před	před	k7c7	před
uhrančivým	uhrančivý	k2eAgInSc7d1	uhrančivý
pohledem	pohled	k1gInSc7	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc4	tento
amulet	amulet	k1gInSc4	amulet
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
ozdoben	ozdobit	k5eAaPmNgInS	ozdobit
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
označuje	označovat	k5eAaImIp3nS	označovat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
hamsa	hamsa	k1gFnSc1	hamsa
<g/>
"	"	kIx"	"
nejen	nejen	k6eAd1	nejen
číslici	číslice	k1gFnSc4	číslice
5	[number]	k4	5
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
používání	používání	k1gNnSc2	používání
amuletu	amulet	k1gInSc2	amulet
spadají	spadat	k5eAaImIp3nP	spadat
až	až	k9	až
do	do	k7c2	do
časů	čas	k1gInPc2	čas
starověké	starověký	k2eAgFnSc2d1	starověká
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Irák	Irák	k1gInSc1	Irák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlaň	dlaň	k1gFnSc4	dlaň
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
jako	jako	k8xC	jako
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
ochranný	ochranný	k2eAgInSc4d1	ochranný
znak	znak	k1gInSc4	znak
nacházíme	nacházet	k5eAaImIp1nP	nacházet
mezi	mezi	k7c7	mezi
mezopotámskými	mezopotámský	k2eAgInPc7d1	mezopotámský
artefakty	artefakt	k1gInPc7	artefakt
<g/>
,	,	kIx,	,
na	na	k7c6	na
amuletech	amulet	k1gInPc6	amulet
sumerské	sumerský	k2eAgFnSc2d1	sumerská
bohyně	bohyně	k1gFnSc2	bohyně
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
života	život	k1gInSc2	život
a	a	k8xC	a
války	válka	k1gFnSc2	válka
Ištar	Ištara	k1gFnPc2	Ištara
<g/>
/	/	kIx~	/
<g/>
Inanny	Inanna	k1gFnSc2	Inanna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
symbolům	symbol	k1gInPc3	symbol
božské	božský	k2eAgFnSc2d1	božská
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Venušina	Venušin	k2eAgFnSc1d1	Venušina
či	či	k8xC	či
Afroditina	Afroditin	k2eAgFnSc1d1	Afroditina
ruka	ruka	k1gFnSc1	ruka
nebo	nebo	k8xC	nebo
Mariina	Mariin	k2eAgFnSc1d1	Mariina
ruka	ruka	k1gFnSc1	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
chránit	chránit	k5eAaImF	chránit
ženy	žena	k1gFnPc4	žena
před	před	k7c7	před
uhranutím	uhranutí	k1gNnSc7	uhranutí
<g/>
,	,	kIx,	,
podporovat	podporovat	k5eAaImF	podporovat
plodnost	plodnost	k1gFnSc4	plodnost
a	a	k8xC	a
laktaci	laktace	k1gFnSc4	laktace
<g/>
,	,	kIx,	,
napomáhat	napomáhat	k5eAaImF	napomáhat
zdravému	zdravý	k2eAgNnSc3d1	zdravé
těhotenství	těhotenství	k1gNnSc3	těhotenství
a	a	k8xC	a
posilovat	posilovat	k5eAaImF	posilovat
slabé	slabý	k2eAgMnPc4d1	slabý
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
je	být	k5eAaImIp3nS	být
i	i	k9	i
Buddhovo	Buddhův	k2eAgNnSc1d1	Buddhovo
symbolické	symbolický	k2eAgNnSc1d1	symbolické
gesto	gesto	k1gNnSc1	gesto
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
mudra	mudra	k1gFnSc1	mudra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hamsa	Hams	k1gMnSc2	Hams
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hamsa	Hams	k1gMnSc2	Hams
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
