<p>
<s>
Mazanec	mazanec	k1gInSc1	mazanec
či	či	k8xC	či
velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
bochník	bochník	k1gInSc1	bochník
je	být	k5eAaImIp3nS	být
bochánek	bochánek	k1gInSc1	bochánek
pečený	pečený	k2eAgInSc1d1	pečený
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
ze	z	k7c2	z
sladkého	sladký	k2eAgNnSc2d1	sladké
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
na	na	k7c4	na
vánočku	vánočka	k1gFnSc4	vánočka
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInPc4d1	patřící
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
druhy	druh	k1gInPc4	druh
českého	český	k2eAgNnSc2d1	české
obřadního	obřadní	k2eAgNnSc2d1	obřadní
pečiva	pečivo	k1gNnSc2	pečivo
<g/>
.	.	kIx.	.
</s>
<s>
Pečení	pečení	k1gNnSc1	pečení
mazance	mazanec	k1gInSc2	mazanec
probíhalo	probíhat	k5eAaImAgNnS	probíhat
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c4	na
Bílou	bílý	k2eAgFnSc4d1	bílá
sobotu	sobota	k1gFnSc4	sobota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
koho	kdo	k3yQnSc4	kdo
byl	být	k5eAaImAgInS	být
mazanec	mazanec	k1gInSc1	mazanec
dříve	dříve	k6eAd2	dříve
určen	určit	k5eAaPmNgInS	určit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lišilo	lišit	k5eAaImAgNnS	lišit
jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
čeledíny	čeledín	k1gMnPc4	čeledín
se	se	k3xPyFc4	se
dělal	dělat	k5eAaImAgMnS	dělat
mazanec	mazanec	k1gInSc4	mazanec
z	z	k7c2	z
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
pro	pro	k7c4	pro
návštěvy	návštěva	k1gFnSc2	návštěva
bylo	být	k5eAaImAgNnS	být
těsto	těsto	k1gNnSc1	těsto
obohaceno	obohatit	k5eAaPmNgNnS	obohatit
rozinkami	rozinka	k1gFnPc7	rozinka
a	a	k8xC	a
mandlemi	mandle	k1gFnPc7	mandle
<g/>
.	.	kIx.	.
<g/>
Hutnější	hutný	k2eAgNnSc1d2	hutnější
těsto	těsto	k1gNnSc1	těsto
mívá	mívat	k5eAaImIp3nS	mívat
nažloutlou	nažloutlý	k2eAgFnSc4d1	nažloutlá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zapečené	zapečený	k2eAgInPc1d1	zapečený
rozinky	rozinka	k1gFnPc4	rozinka
či	či	k8xC	či
kousky	kousek	k1gInPc1	kousek
kandovaného	kandovaný	k2eAgNnSc2d1	kandované
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Nahoře	nahoře	k6eAd1	nahoře
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
před	před	k7c7	před
pečením	pečení	k1gNnSc7	pečení
udělán	udělat	k5eAaPmNgInS	udělat
ostrým	ostrý	k2eAgInSc7d1	ostrý
předmětem	předmět	k1gInSc7	předmět
kříž	kříž	k1gInSc1	kříž
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
posypán	posypat	k5eAaPmNgInS	posypat
krájenými	krájený	k2eAgFnPc7d1	krájená
mandlemi	mandle	k1gFnPc7	mandle
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
receptura	receptura	k1gFnSc1	receptura
mazance	mazanec	k1gInSc2	mazanec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
použitém	použitý	k2eAgInSc6d1	použitý
receptu	recept	k1gInSc6	recept
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
mazanec	mazanec	k1gInSc1	mazanec
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
označoval	označovat	k5eAaImAgInS	označovat
i	i	k9	i
koláč	koláč	k1gInSc1	koláč
z	z	k7c2	z
krajíčků	krajíček	k1gInPc2	krajíček
sušeného	sušený	k2eAgInSc2d1	sušený
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
namočil	namočit	k5eAaPmAgMnS	namočit
do	do	k7c2	do
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
prokládal	prokládat	k5eAaImAgMnS	prokládat
se	se	k3xPyFc4	se
makovou	makový	k2eAgFnSc4d1	maková
či	či	k8xC	či
mandlovou	mandlový	k2eAgFnSc7d1	Mandlová
kaší	kaše	k1gFnSc7	kaše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
české	český	k2eAgInPc1d1	český
kuchařské	kuchařský	k2eAgInPc1d1	kuchařský
předpisy	předpis	k1gInPc1	předpis
na	na	k7c4	na
mazanec	mazanec	k1gInSc4	mazanec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některé	některý	k3yIgNnSc1	některý
byly	být	k5eAaImAgFnP	být
také	také	k9	také
určené	určený	k2eAgFnPc1d1	určená
na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
pekly	péct	k5eAaImAgFnP	péct
i	i	k9	i
mimo	mimo	k7c4	mimo
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těsta	těsto	k1gNnSc2	těsto
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nedával	dávat	k5eNaImAgMnS	dávat
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nebyl	být	k5eNaImAgInS	být
ještě	ještě	k9	ještě
příliš	příliš	k6eAd1	příliš
znám	znám	k2eAgMnSc1d1	znám
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
dávala	dávat	k5eAaImAgFnS	dávat
sladká	sladký	k2eAgFnSc1d1	sladká
smetana	smetana	k1gFnSc1	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
bílá	bílý	k2eAgFnSc1d1	bílá
mouka	mouka	k1gFnSc1	mouka
<g/>
,	,	kIx,	,
kvasnice	kvasnice	k1gFnPc1	kvasnice
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
koření	koření	k1gNnSc1	koření
(	(	kIx(	(
<g/>
zmiňovány	zmiňován	k2eAgFnPc1d1	zmiňována
jsou	být	k5eAaImIp3nP	být
šafrán	šafrán	k1gInSc1	šafrán
a	a	k8xC	a
hřebíček	hřebíček	k1gInSc1	hřebíček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
sladký	sladký	k2eAgInSc4d1	sladký
tvaroh	tvaroh	k1gInSc4	tvaroh
v	v	k7c6	v
majetnějších	majetný	k2eAgFnPc6d2	majetnější
rodinách	rodina	k1gFnPc6	rodina
<g/>
.	.	kIx.	.
<g/>
Dříve	dříve	k6eAd2	dříve
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
majetkových	majetkový	k2eAgInPc6d1	majetkový
poměrech	poměr	k1gInPc6	poměr
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
jeden	jeden	k4xCgMnSc1	jeden
velký	velký	k2eAgInSc1d1	velký
mazanec	mazanec	k1gInSc1	mazanec
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
každý	každý	k3xTgInSc4	každý
svůj	svůj	k3xOyFgInSc4	svůj
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
majetnějších	majetný	k2eAgFnPc2d2	majetnější
rodin	rodina	k1gFnPc2	rodina
dostane	dostat	k5eAaPmIp3nS	dostat
malý	malý	k2eAgInSc1d1	malý
bochánek	bochánek	k1gInSc1	bochánek
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
člena	člen	k1gMnSc4	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
beránek	beránek	k1gInSc1	beránek
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Beranová	Beranová	k1gFnSc1	Beranová
–	–	k?	–
Jídlo	jídlo	k1gNnSc1	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc1	pití
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
a	a	k8xC	a
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-1340-7	[number]	k4	80-200-1340-7
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mazanec	mazanec	k1gInSc1	mazanec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc1	postup
pečení	pečení	k1gNnSc2	pečení
</s>
</p>
<p>
<s>
Mazanec	mazanec	k1gInSc1	mazanec
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
druhy	druh	k1gInPc4	druh
českého	český	k2eAgInSc2d1	český
obřadního	obřadní	k2eAgInSc2d1	obřadní
pečiva	pečivo	k1gNnSc2	pečivo
</s>
</p>
