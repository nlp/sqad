<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
české	český	k2eAgInPc1d1	český
kuchařské	kuchařský	k2eAgInPc1d1	kuchařský
předpisy	předpis	k1gInPc1	předpis
na	na	k7c4	na
mazanec	mazanec	k1gInSc4	mazanec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některé	některý	k3yIgNnSc1	některý
byly	být	k5eAaImAgFnP	být
také	také	k9	také
určené	určený	k2eAgFnPc1d1	určená
na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
pekly	péct	k5eAaImAgFnP	péct
i	i	k9	i
mimo	mimo	k7c4	mimo
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
.	.	kIx.	.
</s>
