<s>
Synekdocha	synekdocha	k1gFnSc1	synekdocha
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
syn-ek-doché	synkochý	k2eAgFnPc1d1	syn-ek-dochý
<g/>
,	,	kIx,	,
sdílená	sdílený	k2eAgFnSc1d1	sdílená
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
míněná	míněný	k2eAgFnSc1d1	míněná
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jazyková	jazykový	k2eAgFnSc1d1	jazyková
či	či	k8xC	či
rétorická	rétorický	k2eAgFnSc1d1	rétorická
figura	figura	k1gFnSc1	figura
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
celku	celek	k1gInSc2	celek
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
části	část	k1gFnSc2	část
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
název	název	k1gInSc4	název
části	část	k1gFnSc2	část
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
"	"	kIx"	"
<g/>
noční	noční	k2eAgNnSc1d1	noční
nebe	nebe	k1gNnSc1	nebe
zářilo	zářit	k5eAaImAgNnS	zářit
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
hvězdy	hvězda	k1gFnPc1	hvězda
svítily	svítit	k5eAaImAgFnP	svítit
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
rodnou	rodný	k2eAgFnSc7d1	rodná
střechou	střecha	k1gFnSc7	střecha
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Synekdocha	synekdocha	k1gFnSc1	synekdocha
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tropy	trop	k1gMnPc7	trop
a	a	k8xC	a
blízce	blízce	k6eAd1	blízce
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
metonymií	metonymie	k1gFnSc7	metonymie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přesnějším	přesný	k2eAgNnSc6d2	přesnější
rozlišování	rozlišování	k1gNnSc6	rozlišování
je	být	k5eAaImIp3nS	být
synekdocha	synekdocha	k1gFnSc1	synekdocha
použití	použití	k1gNnSc2	použití
názvu	název	k1gInSc2	název
části	část	k1gFnSc2	část
pro	pro	k7c4	pro
celek	celek	k1gInSc4	celek
nebo	nebo	k8xC	nebo
opačně	opačně	k6eAd1	opačně
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
nebo	nebo	k8xC	nebo
podřazeného	podřazený	k2eAgInSc2d1	podřazený
pojmu	pojem	k1gInSc2	pojem
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
metonymie	metonymie	k1gFnSc1	metonymie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
nebo	nebo	k8xC	nebo
věcné	věcný	k2eAgNnSc4d1	věcné
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
názvy	název	k1gInPc7	název
a	a	k8xC	a
metafora	metafora	k1gFnSc1	metafora
znamená	znamenat	k5eAaImIp3nS	znamenat
použití	použití	k1gNnSc4	použití
nepříbuzného	příbuzný	k2eNgInSc2d1	nepříbuzný
pojmu	pojem	k1gInSc2	pojem
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Celek	celek	k1gInSc1	celek
místo	místo	k7c2	místo
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
totum	totum	k1gInSc1	totum
pro	pro	k7c4	pro
parte	parte	k1gNnSc4	parte
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Sál	sál	k1gInSc1	sál
rázem	rázem	k6eAd1	rázem
ztichl	ztichnout	k5eAaPmAgInS	ztichnout
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
sále	sál	k1gInSc6	sál
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Otevřete	otevřít	k5eAaPmIp2nP	otevřít
<g/>
!	!	kIx.	!
</s>
<s>
Policie	policie	k1gFnSc1	policie
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
Otevřete	otevřít	k5eAaPmRp2nP	otevřít
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
policista	policista	k1gMnSc1	policista
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
stav	stav	k1gInSc4	stav
pohotovosti	pohotovost	k1gFnSc2	pohotovost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
Hygienická	hygienický	k2eAgFnSc1d1	hygienická
stanice	stanice	k1gFnSc1	stanice
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
stav	stav	k1gInSc4	stav
pohotovosti	pohotovost	k1gFnSc2	pohotovost
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Washingtonem	Washington	k1gInSc7	Washington
a	a	k8xC	a
Teheránem	Teherán	k1gInSc7	Teherán
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
vztahy	vztah	k1gInPc1	vztah
politických	politický	k2eAgFnPc2d1	politická
špiček	špička	k1gFnPc2	špička
USA	USA	kA	USA
a	a	k8xC	a
Íránu	Írán	k1gInSc3	Írán
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Pentagon	Pentagon	k1gInSc1	Pentagon
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
velení	velení	k1gNnSc6	velení
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Wall	Wall	k1gInSc1	Wall
Street	Street	k1gInSc1	Street
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
korporace	korporace	k1gFnPc1	korporace
obchodující	obchodující	k2eAgFnPc1d1	obchodující
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
a	a	k8xC	a
světových	světový	k2eAgFnPc6d1	světová
burzách	burza	k1gFnPc6	burza
<g/>
"	"	kIx"	"
Část	část	k1gFnSc1	část
místo	místo	k7c2	místo
celku	celek	k1gInSc2	celek
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
pars	pars	k1gInSc1	pars
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Mít	mít	k5eAaImF	mít
střechu	střecha	k1gFnSc4	střecha
nad	nad	k7c7	nad
<g />
.	.	kIx.	.
</s>
<s>
hlavou	hlava	k1gFnSc7	hlava
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
byt	byt	k1gInSc4	byt
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Hlava	hlava	k1gFnSc1	hlava
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
A	a	k9	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
tyčí	tyč	k1gFnPc2	tyč
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
kukuřičná	kukuřičný	k2eAgNnPc4d1	kukuřičné
zrna	zrno	k1gNnPc4	zrno
<g/>
"	"	kIx"	"
místo	místo	k7c2	místo
"	"	kIx"	"
<g/>
vzrostlé	vzrostlý	k2eAgFnSc2d1	vzrostlá
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Nezval	Nezval	k1gMnSc1	Nezval
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgInS	mít
plavý	plavý	k2eAgInSc1d1	plavý
vlas	vlas	k1gInSc1	vlas
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
vlasy	vlas	k1gInPc1	vlas
<g/>
"	"	kIx"	"
Podřízený	podřízený	k2eAgInSc1d1	podřízený
pojem	pojem	k1gInSc1	pojem
místo	místo	k7c2	místo
nadřízeného	nadřízený	k1gMnSc2	nadřízený
"	"	kIx"	"
<g/>
Vydělat	vydělat	k5eAaPmF	vydělat
na	na	k7c6	na
chleba	chléb	k1gInSc2	chléb
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
na	na	k7c4	na
obživu	obživa	k1gFnSc4	obživa
<g/>
"	"	kIx"	"
Určitý	určitý	k2eAgInSc4d1	určitý
místo	místo	k7c2	místo
těžko	těžko	k6eAd1	těžko
definovatelného	definovatelný	k2eAgMnSc2d1	definovatelný
"	"	kIx"	"
<g/>
Zažila	zažít	k5eAaPmAgFnS	zažít
si	se	k3xPyFc3	se
svých	svůj	k3xOyFgNnPc2	svůj
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
slávy	sláva	k1gFnSc2	sláva
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
své	svůj	k3xOyFgNnSc4	svůj
období	období	k1gNnSc4	období
slávy	sláva	k1gFnSc2	sláva
<g/>
"	"	kIx"	"
Nadřízený	nadřízený	k2eAgInSc1d1	nadřízený
pojem	pojem	k1gInSc1	pojem
místo	místo	k7c2	místo
podřízeného	podřízený	k1gMnSc2	podřízený
"	"	kIx"	"
<g/>
Opeřenec	opeřenec	k1gMnSc1	opeřenec
promluvil	promluvit	k5eAaPmAgMnS	promluvit
<g/>
"	"	kIx"	"
místo	místo	k7c2	místo
"	"	kIx"	"
<g/>
papoušek	papoušek	k1gMnSc1	papoušek
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Severočeši	Severočech	k1gMnPc1	Severočech
nakonec	nakonec	k6eAd1	nakonec
remizovali	remizovat	k5eAaPmAgMnP	remizovat
<g/>
"	"	kIx"	"
místo	místo	k6eAd1	místo
hráči	hráč	k1gMnPc1	hráč
libereckého	liberecký	k2eAgInSc2d1	liberecký
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
týmu	tým	k1gInSc2	tým
Singulár	singulár	k1gInSc1	singulár
místo	místo	k7c2	místo
plurálu	plurál	k1gInSc2	plurál
"	"	kIx"	"
<g/>
Daňový	daňový	k2eAgMnSc1d1	daňový
poplatník	poplatník	k1gMnSc1	poplatník
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
všichni	všechen	k3xTgMnPc1	všechen
poplatníci	poplatník	k1gMnPc1	poplatník
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
Rus	Rus	k1gMnSc1	Rus
nenechá	nechat	k5eNaPmIp3nS	nechat
líbit	líbit	k5eAaImF	líbit
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
"	"	kIx"	"
Plurál	plurál	k1gInSc1	plurál
místo	místo	k7c2	místo
singuláru	singulár	k1gInSc2	singulár
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pluralis	pluralis	k1gFnSc1	pluralis
majestatis	majestatis	k1gFnSc1	majestatis
<g/>
.	.	kIx.	.
</s>
