<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jazyková	jazykový	k2eAgFnSc1d1	jazyková
figura	figura	k1gFnSc1	figura
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
celku	celek	k1gInSc2	celek
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
části	část	k1gFnSc2	část
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
?	?	kIx.	?
</s>
