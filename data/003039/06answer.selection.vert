<s>
Synekdocha	synekdocha	k1gFnSc1	synekdocha
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
syn-ek-doché	synkochý	k2eAgFnPc1d1	syn-ek-dochý
<g/>
,	,	kIx,	,
sdílená	sdílený	k2eAgFnSc1d1	sdílená
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
míněná	míněný	k2eAgFnSc1d1	míněná
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jazyková	jazykový	k2eAgFnSc1d1	jazyková
či	či	k8xC	či
rétorická	rétorický	k2eAgFnSc1d1	rétorická
figura	figura	k1gFnSc1	figura
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
celku	celek	k1gInSc2	celek
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
části	část	k1gFnSc2	část
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
název	název	k1gInSc4	název
části	část	k1gFnSc2	část
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
