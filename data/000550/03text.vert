<s>
Květen	květen	k1gInSc1	květen
je	být	k5eAaImIp3nS	být
pátý	pátý	k4xOgInSc4	pátý
měsíc	měsíc	k1gInSc4	měsíc
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
květen	květen	k1gInSc1	květen
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
lidovými	lidový	k2eAgFnPc7d1	lidová
oslavami	oslava	k1gFnPc7	oslava
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stavění	stavění	k1gNnSc4	stavění
májek	májek	k1gInSc1	májek
nebo	nebo	k8xC	nebo
pálení	pálení	k1gNnSc1	pálení
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
květnovou	květnový	k2eAgFnSc4d1	květnová
neděli	neděle	k1gFnSc4	neděle
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
matek	matka	k1gFnPc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
květen	květena	k1gFnPc2	květena
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xC	jako
leden	leden	k1gInSc4	leden
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
roce	rok	k1gInSc6	rok
nezačíná	začínat	k5eNaImIp3nS	začínat
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xC	jako
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
Mokrý	mokrý	k2eAgInSc1d1	mokrý
máj	máj	k1gInSc1	máj
-	-	kIx~	-
v	v	k7c6	v
stodole	stodola	k1gFnSc6	stodola
ráj	ráj	k1gInSc1	ráj
Svatá	svatý	k2eAgFnSc1d1	svatá
Žofie	Žofie	k1gFnSc1	Žofie
políčka	políčko	k1gNnSc2	políčko
často	často	k6eAd1	často
zalije	zalít	k5eAaPmIp3nS	zalít
Na	na	k7c4	na
Urbana	Urban	k1gMnSc4	Urban
pěkný	pěkný	k2eAgInSc1d1	pěkný
<g/>
,	,	kIx,	,
teplý	teplý	k2eAgInSc1d1	teplý
den	den	k1gInSc1	den
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
suchý	suchý	k2eAgInSc1d1	suchý
červenec	červenec	k1gInSc1	červenec
i	i	k8xC	i
srpen	srpen	k1gInSc1	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
nedbá	nedbat	k5eAaImIp3nS	nedbat
toho	ten	k3xDgNnSc2	ten
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
míti	mít	k5eAaImF	mít
vína	víno	k1gNnPc4	víno
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Déšť	déšť	k1gInSc1	déšť
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
je	být	k5eAaImIp3nS	být
ohňová	ohňový	k2eAgFnSc1d1	ohňová
rána	rána	k1gFnSc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
za	za	k7c4	za
kamna	kamna	k1gNnPc4	kamna
vlezem	vlezem	k?	vlezem
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
ještě	ještě	k6eAd1	ještě
tam	tam	k6eAd1	tam
budem	budem	k?	budem
<g/>
,	,	kIx,	,
trnopuk	trnopuk	k1gInSc1	trnopuk
s	s	k7c7	s
kamen	kamna	k1gNnPc2	kamna
fuk	fuk	k6eAd1	fuk
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
květen	květena	k1gFnPc2	květena
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
kvést	kvést	k5eAaImF	kvést
(	(	kIx(	(
<g/>
rostliny	rostlina	k1gFnPc1	rostlina
kvetou	kvést	k5eAaImIp3nP	kvést
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
máj	máj	k1gFnSc1	máj
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
květen	květena	k1gFnPc2	květena
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
Jungmannově	Jungmannův	k2eAgInSc6d1	Jungmannův
překladu	překlad	k1gInSc6	překlad
Ataly	Atala	k1gFnSc2	Atala
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
jako	jako	k8xC	jako
poetismus	poetismus	k1gInSc1	poetismus
a	a	k8xC	a
překlad	překlad	k1gInSc1	překlad
za	za	k7c4	za
fr.	fr.	k?	fr.
lune	lune	k1gNnSc4	lune
des	des	k1gNnPc2	des
fleurs	fleursa	k1gFnPc2	fleursa
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Jungmanna	Jungmann	k1gMnSc4	Jungmann
zřejmě	zřejmě	k6eAd1	zřejmě
působilo	působit	k5eAaImAgNnS	působit
polské	polský	k2eAgNnSc1d1	polské
slovo	slovo	k1gNnSc1	slovo
kwiecień	kwiecień	k?	kwiecień
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
duben	duben	k1gInSc1	duben
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
lásky	láska	k1gFnSc2	láska
měsíc	měsíc	k1gInSc4	měsíc
obětí	oběť	k1gFnPc2	oběť
komunismu	komunismus	k1gInSc2	komunismus
měsíc	měsíc	k1gInSc4	měsíc
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
květen	květena	k1gFnPc2	květena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
květen	květen	k1gInSc4	květen
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
