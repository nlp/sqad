<s>
Adrián	Adrián	k1gMnSc1	Adrián
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
podoba	podoba	k1gFnSc1	podoba
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Hadrián	Hadrián	k1gMnSc1	Hadrián
<g/>
.	.	kIx.	.
</s>
<s>
Vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
muž	muž	k1gMnSc1	muž
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
z	z	k7c2	z
města	město	k1gNnSc2	město
Hadrie	Hadrie	k1gFnSc2	Hadrie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
Adrian	Adrian	k1gMnSc1	Adrian
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
kalendáři	kalendář	k1gInSc6	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
forma	forma	k1gFnSc1	forma
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Adriána	Adrián	k1gMnSc4	Adrián
nebo	nebo	k8xC	nebo
Adriana	Adrian	k1gMnSc4	Adrian
<g/>
.	.	kIx.	.
</s>
<s>
Adriánek	Adriánka	k1gFnPc2	Adriánka
<g/>
,	,	kIx,	,
Áďa	Áďa	k1gFnPc2	Áďa
<g/>
,	,	kIx,	,
Adýsek	Adýska	k1gFnPc2	Adýska
<g/>
,	,	kIx,	,
Adrianko	Adrianka	k1gFnSc5	Adrianka
<g/>
,	,	kIx,	,
Rian	Rian	k1gMnSc1	Rian
Slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Adrián	Adrián	k1gMnSc1	Adrián
Německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Adrián	Adrián	k1gMnSc1	Adrián
nebo	nebo	k8xC	nebo
Hadrian	Hadrian	k1gMnSc1	Hadrian
Anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Adrian	Adriana	k1gFnPc2	Adriana
Srbocharvátsky	srbocharvátsky	k6eAd1	srbocharvátsky
<g/>
:	:	kIx,	:
Adrijan	Adrijan	k1gInSc1	Adrijan
Italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Adriano	Adriana	k1gFnSc5	Adriana
Francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Adrien	Adriena	k1gFnPc2	Adriena
Holandsky	holandsky	k6eAd1	holandsky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Adriaan	Adriaana	k1gFnPc2	Adriaana
Maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Adorján	Adorján	k1gMnSc1	Adorján
nebo	nebo	k8xC	nebo
Adrián	Adrián	k1gMnSc1	Adrián
Adrian	Adrian	k1gMnSc1	Adrian
Belew	Belew	k1gMnSc1	Belew
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
Adrian	Adriana	k1gFnPc2	Adriana
Thaws	Thaws	k1gInSc1	Thaws
-	-	kIx~	-
britský	britský	k2eAgMnSc1d1	britský
rapper	rapper	k1gMnSc1	rapper
Adrian	Adrian	k1gMnSc1	Adrian
de	de	k?	de
Vries	Vries	k1gMnSc1	Vries
-	-	kIx~	-
sochař	sochař	k1gMnSc1	sochař
Adrian	Adrian	k1gMnSc1	Adrian
Carmack	Carmack	k1gMnSc1	Carmack
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
4	[number]	k4	4
zakladatelů	zakladatel	k1gMnPc2	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Id	Ida	k1gFnPc2	Ida
Software	software	k1gInSc1	software
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
-	-	kIx~	-
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
kytaristů	kytarista	k1gMnPc2	kytarista
heavy	heava	k1gFnSc2	heava
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
Adrian	Adriana	k1gFnPc2	Adriana
Sutil	sutit	k5eAaImAgInS	sutit
-	-	kIx~	-
německý	německý	k2eAgInSc1d1	německý
pilot	pilot	k1gInSc1	pilot
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
Adrian	Adrian	k1gMnSc1	Adrian
Bjurman	Bjurman	k1gMnSc1	Bjurman
-	-	kIx~	-
švédský	švédský	k2eAgMnSc1d1	švédský
režisér	režisér	k1gMnSc1	režisér
Adrian	Adrian	k1gMnSc1	Adrian
Mutu	Mutu	k?	Mutu
-	-	kIx~	-
rumunský	rumunský	k2eAgMnSc1d1	rumunský
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
