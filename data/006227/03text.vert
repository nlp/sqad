<s>
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnPc4d1	ebenová
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
české	český	k2eAgFnSc2d1	Česká
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
bratři	bratr	k1gMnPc1	bratr
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
a	a	k8xC	a
David	David	k1gMnSc1	David
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
získala	získat	k5eAaPmAgFnS	získat
skupina	skupina	k1gFnSc1	skupina
interpretační	interpretační	k2eAgFnSc4d1	interpretační
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Porta	porto	k1gNnSc2	porto
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
příliš	příliš	k6eAd1	příliš
nekoncertovali	koncertovat	k5eNaImAgMnP	koncertovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Vřešťála	Vřešťál	k1gMnSc2	Vřešťál
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Karla	Karel	k1gMnSc2	Karel
Plíhala	Plíhal	k1gMnSc2	Plíhal
ale	ale	k8xC	ale
natočili	natočit	k5eAaBmAgMnP	natočit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
muzikanty	muzikant	k1gMnPc7	muzikant
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Etc	Etc	k1gFnSc2	Etc
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
kontrabasistou	kontrabasista	k1gMnSc7	kontrabasista
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Honzákem	Honzák	k1gMnSc7	Honzák
a	a	k8xC	a
Ivou	Iva	k1gFnSc7	Iva
Bittovou	Bittová	k1gFnSc7	Bittová
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
album	album	k1gNnSc1	album
Tichá	Tichá	k1gFnSc1	Tichá
domácnost	domácnost	k1gFnSc1	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aktivita	aktivita	k1gFnSc1	aktivita
kapely	kapela	k1gFnSc2	kapela
byla	být	k5eAaImAgFnS	být
minimální	minimální	k2eAgFnSc1d1	minimální
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
koncertují	koncertovat	k5eAaImIp3nP	koncertovat
až	až	k9	až
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
s	s	k7c7	s
albem	album	k1gNnSc7	album
Já	já	k3xPp1nSc1	já
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
kromě	kromě	k7c2	kromě
hostů	host	k1gMnPc2	host
z	z	k7c2	z
Tiché	Tiché	k2eAgFnSc2d1	Tiché
domácnosti	domácnost	k1gFnSc2	domácnost
zpívá	zpívat	k5eAaImIp3nS	zpívat
jednu	jeden	k4xCgFnSc4	jeden
píseň	píseň	k1gFnSc4	píseň
také	také	k9	také
Jiří	Jiří	k1gMnSc1	Jiří
Schmitzer	Schmitzer	k1gMnSc1	Schmitzer
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roli	role	k1gFnSc6	role
Senecta	Senect	k1gInSc2	Senect
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
písni	píseň	k1gFnSc6	píseň
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
Jiří	Jiří	k1gMnSc1	Jiří
Bartoška	Bartoška	k1gMnSc1	Bartoška
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tito	tento	k3xDgMnPc1	tento
hosté	host	k1gMnPc1	host
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Předota	Předota	k1gFnSc1	Předota
a	a	k8xC	a
Ondřej	Ondřej	k1gMnSc1	Ondřej
Maňour	Maňour	k1gMnSc1	Maňour
ze	z	k7c2	z
souboru	soubor	k1gInSc2	soubor
Schola	Schola	k1gFnSc1	Schola
Gregoriana	Gregoriana	k1gFnSc1	Gregoriana
Pragensis	Pragensis	k1gFnSc1	Pragensis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
David	David	k1gMnSc1	David
Eben	eben	k1gInSc1	eben
<g/>
,	,	kIx,	,
také	také	k9	také
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
koncertním	koncertní	k2eAgNnSc6d1	koncertní
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
<g/>
,	,	kIx,	,
EP	EP	kA	EP
<g/>
,	,	kIx,	,
Panton	Panton	k1gInSc1	Panton
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
chodí	chodit	k5eAaImIp3nS	chodit
<g/>
,	,	kIx,	,
EP	EP	kA	EP
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Malé	Malé	k2eAgFnPc1d1	Malé
písně	píseň	k1gFnPc1	píseň
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Cesty	cesta	k1gFnPc1	cesta
'	'	kIx"	'
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
Panton	Panton	k1gInSc1	Panton
na	na	k7c6	na
Portě	porta	k1gFnSc6	porta
<g/>
,	,	kIx,	,
LP	LP	kA	LP
sampler	sampler	k1gInSc1	sampler
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
–	–	k?	–
píseň	píseň	k1gFnSc1	píseň
Sůl	sůl	k1gFnSc4	sůl
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
očích	oko	k1gNnPc6	oko
Drobné	drobný	k2eAgFnSc2d1	drobná
skladby	skladba	k1gFnSc2	skladba
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
EP	EP	kA	EP
sampler	sampler	k1gInSc1	sampler
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
V	v	k7c6	v
limitu	limit	k1gInSc6	limit
Adventní	adventní	k2eAgFnPc4d1	adventní
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
koledy	koleda	k1gFnPc4	koleda
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
1990	[number]	k4	1990
–	–	k?	–
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
Kubišovou	Kubišová	k1gFnSc7	Kubišová
Tichá	Tichá	k1gFnSc1	Tichá
domácnost	domácnost	k1gFnSc1	domácnost
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
BMG	BMG	kA	BMG
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Malé	Malé	k2eAgFnPc1d1	Malé
písně	píseň	k1gFnPc1	píseň
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
BMG	BMG	kA	BMG
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1996	[number]	k4	1996
–	–	k?	–
reedice	reedice	k1gFnSc2	reedice
LP	LP	kA	LP
doplněná	doplněný	k2eAgNnPc4d1	doplněné
skladbami	skladba	k1gFnPc7	skladba
z	z	k7c2	z
EP	EP	kA	EP
Sloni	sloň	k1gFnSc2wR	sloň
v	v	k7c6	v
porcelánu	porcelán	k1gInSc6	porcelán
<g/>
,	,	kIx,	,
CD	CD	kA	CD
sampler	sampler	k1gInSc4	sampler
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
–	–	k?	–
písně	píseň	k1gFnSc2	píseň
Boby	Bob	k1gMnPc7	Bob
a	a	k8xC	a
Kočka	kočka	k1gFnSc1	kočka
Já	já	k3xPp1nSc1	já
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
BMG	BMG	kA	BMG
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Ebeni	Ebeň	k1gMnPc7	Ebeň
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
(	(	kIx(	(
<g/>
Live	Live	k1gFnPc6	Live
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
BMG	BMG	kA	BMG
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Havěť	havěť	k1gFnSc1	havěť
všelijaká	všelijaký	k3yIgFnSc1	všelijaký
<g/>
,	,	kIx,	,
CD	CD	kA	CD
sampler	sampler	k1gInSc1	sampler
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
Sloni	sloň	k1gFnSc2wR	sloň
Pohádky	pohádka	k1gFnSc2	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gMnPc2	Grimm
s	s	k7c7	s
písničkami	písnička	k1gFnPc7	písnička
bratří	bratr	k1gMnPc2	bratr
Ebenů	eben	k1gInPc2	eben
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
Music	Musice	k1gFnPc2	Musice
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
50	[number]	k4	50
miniatur	miniatura	k1gFnPc2	miniatura
<g/>
,	,	kIx,	,
CD	CD	kA	CD
sampler	sampler	k1gInSc1	sampler
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
V	v	k7c6	v
limitu	limit	k1gInSc6	limit
Chlebíčky	chlebíček	k1gInPc1	chlebíček
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Čas	čas	k1gInSc1	čas
holin	holina	k1gFnPc2	holina
<g/>
,	,	kIx,	,
CD	CD	kA	CD
i	i	k8xC	i
LP	LP	kA	LP
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
Marek	marka	k1gFnPc2	marka
Eben	eben	k1gInSc4	eben
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
živém	živé	k1gNnSc6	živé
DVD	DVD	kA	DVD
skupiny	skupina	k1gFnSc2	skupina
Sto	sto	k4xCgNnSc4	sto
zvířat	zvíře	k1gNnPc2	zvíře
Jste	být	k5eAaImIp2nP	být
normální	normální	k2eAgInPc1d1	normální
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
píseň	píseň	k1gFnSc4	píseň
Já	já	k3xPp1nSc1	já
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
nahráli	nahrát	k5eAaBmAgMnP	nahrát
také	také	k9	také
CD	CD	kA	CD
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sázel	sázet	k5eAaImAgMnS	sázet
stromy	strom	k1gInPc4	strom
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
novely	novela	k1gFnSc2	novela
Jeana	Jean	k1gMnSc2	Jean
Giona	Gion	k1gMnSc2	Gion
čte	číst	k5eAaImIp3nS	číst
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
složil	složit	k5eAaPmAgMnS	složit
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc4	eben
složil	složit	k5eAaPmAgMnS	složit
také	také	k9	také
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
texty	text	k1gInPc4	text
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
dramatizaci	dramatizace	k1gFnSc4	dramatizace
knihy	kniha	k1gFnSc2	kniha
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
kalendář	kalendář	k1gInSc1	kalendář
Josteina	Jostein	k1gMnSc2	Jostein
Gaardera	Gaarder	k1gMnSc2	Gaarder
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
Profil	profil	k1gInSc4	profil
skupiny	skupina	k1gFnSc2	skupina
Bratři	bratřit	k5eAaImRp2nS	bratřit
Ebenové	ebenový	k2eAgNnSc4d1	ebenové
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
režie	režie	k1gFnSc2	režie
Jan	Jan	k1gMnSc1	Jan
Bonaventura	Bonaventura	k1gFnSc1	Bonaventura
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Folk	folk	k1gInSc1	folk
&	&	k?	&
Country	country	k2eAgInSc1d1	country
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
neuvedeno	uveden	k2eNgNnSc1d1	neuvedeno
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
EAN	EAN	kA	EAN
8595071	[number]	k4	8595071
900134	[number]	k4	900134
<g/>
,	,	kIx,	,
zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
,	,	kIx,	,
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
-	-	kIx~	-
www.bratriebenove.cz	www.bratriebenove.cz	k1gInSc1	www.bratriebenove.cz
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgInPc1d1	ebenový
servírují	servírovat	k5eAaBmIp3nP	servírovat
hudební	hudební	k2eAgInPc1d1	hudební
Chlebíčky	chlebíček	k1gInPc1	chlebíček
Video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
-	-	kIx~	-
Ja	Ja	k?	Ja
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
dělám	dělat	k5eAaImIp1nS	dělat
Video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
Bratri	Bratri	k1gNnSc1	Bratri
Ebenove	Ebenov	k1gInSc5	Ebenov
-	-	kIx~	-
Folkloreček	Folkloreček	k1gInSc1	Folkloreček
</s>
