<p>
<s>
Hayato	Hayato	k6eAd1	Hayato
Okamura	Okamura	k1gFnSc1	Okamura
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
pokřtěný	pokřtěný	k2eAgMnSc1d1	pokřtěný
jako	jako	k8xS	jako
Josef	Josef	k1gMnSc1	Josef
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česko-japonský	českoaponský	k2eAgMnSc1d1	česko-japonský
tlumočník	tlumočník	k1gMnSc1	tlumočník
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
politika	politikum	k1gNnSc2	politikum
Tomia	Tomium	k1gNnSc2	Tomium
Okamury	Okamura	k1gFnSc2	Okamura
a	a	k8xC	a
architekta	architekt	k1gMnSc2	architekt
Osamu	Osam	k1gInSc2	Osam
Okamury	Okamura	k1gFnSc2	Okamura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
české	český	k2eAgFnSc3d1	Česká
matce	matka	k1gFnSc3	matka
Heleně	Helena	k1gFnSc3	Helena
Okamurové	Okamurový	k2eAgFnPc4d1	Okamurový
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Holíkové	Holíková	k1gFnSc3	Holíková
a	a	k8xC	a
japonskému	japonský	k2eAgMnSc3d1	japonský
otci	otec	k1gMnSc3	otec
Matsuovi	Matsu	k1gMnSc3	Matsu
Okamurovi	Okamur	k1gMnSc3	Okamur
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
,	,	kIx,	,
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sourozenci	sourozenec	k1gMnPc7	sourozenec
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
religionistiku	religionistika	k1gFnSc4	religionistika
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
jako	jako	k9	jako
tlumočník	tlumočník	k1gMnSc1	tlumočník
japonštiny	japonština	k1gFnSc2	japonština
a	a	k8xC	a
průvodce	průvodce	k1gMnSc1	průvodce
japonských	japonský	k2eAgMnPc2d1	japonský
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
je	být	k5eAaImIp3nS	být
Rakušanka	Rakušanka	k1gFnSc1	Rakušanka
s	s	k7c7	s
česko-ukrajinsko-ruskými	českokrajinskouský	k2eAgInPc7d1	česko-ukrajinsko-ruský
kořeny	kořen	k1gInPc7	kořen
Ludmila	Ludmila	k1gFnSc1	Ludmila
Okamurová	Okamurový	k2eAgFnSc1d1	Okamurový
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
mají	mít	k5eAaImIp3nP	mít
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
pomoci	pomoc	k1gFnSc6	pomoc
křesťanským	křesťanský	k2eAgMnPc3d1	křesťanský
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
iniciativy	iniciativa	k1gFnSc2	iniciativa
Kroměřížská	kroměřížský	k2eAgFnSc1d1	Kroměřížská
výzva	výzva	k1gFnSc1	výzva
hledající	hledající	k2eAgFnSc1d1	hledající
vhodného	vhodný	k2eAgMnSc2d1	vhodný
kandidáta	kandidát	k1gMnSc2	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
podzimních	podzimní	k2eAgFnPc6d1	podzimní
sněmovních	sněmovní	k2eAgFnPc6d1	sněmovní
volbách	volba	k1gFnPc6	volba
2017	[number]	k4	2017
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
5	[number]	k4	5
053	[number]	k4	053
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mandát	mandát	k1gInSc4	mandát
poslance	poslanec	k1gMnSc2	poslanec
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
23	[number]	k4	23
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
se	se	k3xPyFc4	se
však	však	k9	však
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
chce	chtít	k5eAaImIp3nS	chtít
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
domluvit	domluvit	k5eAaPmF	domluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
26	[number]	k4	26
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidovci	lidovec	k1gMnSc3	lidovec
žádného	žádný	k3yNgMnSc4	žádný
kandidáta	kandidát	k1gMnSc4	kandidát
ještě	ještě	k6eAd1	ještě
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
možná	možný	k2eAgFnSc1d1	možná
podpora	podpora	k1gFnSc1	podpora
jiného	jiný	k2eAgMnSc2d1	jiný
kandidáta	kandidát	k1gMnSc2	kandidát
na	na	k7c6	na
Praze	Praha	k1gFnSc6	Praha
8	[number]	k4	8
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
lékaře	lékař	k1gMnSc4	lékař
Pavla	Pavel	k1gMnSc4	Pavel
Dungla	Dungl	k1gMnSc4	Dungl
(	(	kIx(	(
<g/>
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
kandidátem	kandidát	k1gMnSc7	kandidát
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
23	[number]	k4	23
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
<g/>
,	,	kIx,	,
podpořili	podpořit	k5eAaPmAgMnP	podpořit
jej	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
Zelení	zelenit	k5eAaImIp3nS	zelenit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
11,74	[number]	k4	11,74
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c6	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
kandidátky	kandidátka	k1gFnSc2	kandidátka
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
https://web.archive.org/web/20170411062926/http://178.79.133.112/2017/04/04/ln-populist-party-leaders-brother-run-kdu-%C4%8Dsl	[url]	k1gInSc1	https://web.archive.org/web/20170411062926/http://178.79.133.112/2017/04/04/ln-populist-party-leaders-brother-run-kdu-%C4%8Dsl
</s>
</p>
<p>
<s>
https://zpravy.idnes.cz/rozhovor-s-hayato-okamurou-bratrem-tomia-okamury-fv8-/domaci.aspx?c=A160721_2260974_domaci_jav	[url]	k1gInSc1	https://zpravy.idnes.cz/rozhovor-s-hayato-okamurou-bratrem-tomia-okamury-fv8-/domaci.aspx?c=A160721_2260974_domaci_jav
</s>
</p>
<p>
<s>
http://www.blesk.cz/clanek/zpravy-politika/461585/hayato-okamura-o-sve-rodine-mame-5-deti-mluvime-s-nimi-i-nemecky.html	[url]	k1gMnSc1	http://www.blesk.cz/clanek/zpravy-politika/461585/hayato-okamura-o-sve-rodine-mame-5-deti-mluvime-s-nimi-i-nemecky.html
</s>
</p>
<p>
<s>
https://zpravy.idnes.cz/okamura-starsi-nabizi-pomoc-krestanskym-uprchlikum-z-iraku-pln-/domaci.aspx?c=A151209_143630_domaci_fer	[url]	k1gMnSc1	https://zpravy.idnes.cz/okamura-starsi-nabizi-pomoc-krestanskym-uprchlikum-z-iraku-pln-/domaci.aspx?c=A151209_143630_domaci_fer
</s>
</p>
