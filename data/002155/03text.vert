<s>
Boise	Boise	k1gFnSc1	Boise
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
boisí	boisit	k5eAaPmIp3nS	boisit
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Idaho	Ida	k1gMnSc2	Ida
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Ada	Ada	kA	Ada
County	Count	k1gInPc7	Count
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žilo	žít	k5eAaImAgNnS	žít
205	[number]	k4	205
671	[number]	k4	671
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
616	[number]	k4	616
561	[number]	k4	561
včetně	včetně	k7c2	včetně
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
oblasti	oblast	k1gFnSc6	oblast
severovýchodu	severovýchod	k1gInSc2	severovýchod
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Boise	Boise	k6eAd1	Boise
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
Idaho	Ida	k1gMnSc2	Ida
<g/>
,	,	kIx,	,
jen	jen	k9	jen
asi	asi	k9	asi
66	[number]	k4	66
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Oregonem	Oregon	k1gInSc7	Oregon
a	a	k8xC	a
180	[number]	k4	180
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
širokém	široký	k2eAgInSc6d1	široký
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
rovinatém	rovinatý	k2eAgMnSc6d1	rovinatý
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
obklopeném	obklopený	k2eAgInSc6d1	obklopený
<g/>
,	,	kIx,	,
povrchu	povrch	k1gInSc2	povrch
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
824	[number]	k4	824
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
km	km	kA	km
jak	jak	k8xS	jak
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
severozápad	severozápad	k1gInSc1	severozápad
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
vrcholky	vrcholek	k1gInPc1	vrcholek
úpatí	úpatí	k1gNnSc2	úpatí
Skalistých	skalistý	k2eAgInPc2d1	skalistý
hor.	hor.	k?	hor.
Kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Boise	Boise	k1gFnSc2	Boise
River	Rivra	k1gFnPc2	Rivra
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
Boise	Bois	k1gInSc6	Bois
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
zřetelným	zřetelný	k2eAgNnSc7d1	zřetelné
střídáním	střídání	k1gNnSc7	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
s	s	k7c7	s
několika	několik	k4yIc7	několik
dny	den	k1gInPc7	den
hutné	hutný	k2eAgFnSc2d1	hutná
sněhové	sněhový	k2eAgFnSc2d1	sněhová
nadílky	nadílka	k1gFnSc2	nadílka
<g/>
,	,	kIx,	,
jara	jaro	k1gNnSc2	jaro
jsou	být	k5eAaImIp3nP	být
teplá	teplé	k1gNnPc4	teplé
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc1	teplota
v	v	k7c4	v
Boise	Bois	k1gMnPc4	Bois
překračovat	překračovat	k5eAaImF	překračovat
30	[number]	k4	30
<g/>
,	,	kIx,	,
rekordně	rekordně	k6eAd1	rekordně
i	i	k9	i
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
podprůměrné	podprůměrný	k2eAgFnPc1d1	podprůměrná
<g/>
,	,	kIx,	,
nejsušším	suchý	k2eAgInSc7d3	nejsušší
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
srpen	srpen	k1gInSc1	srpen
<g/>
,	,	kIx,	,
nejvydatnějším	vydatný	k2eAgInSc7d3	nejvydatnější
duben	duben	k1gInSc4	duben
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
neosídlené	osídlený	k2eNgFnSc6d1	neosídlená
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získalo	získat	k5eAaPmAgNnS	získat
statut	statut	k1gInSc4	statut
města	město	k1gNnSc2	město
a	a	k8xC	a
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Idaho	Ida	k1gMnSc2	Ida
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Lewistone	Lewiston	k1gInSc5	Lewiston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Boise	Boise	k1gFnSc2	Boise
je	být	k5eAaImIp3nS	být
však	však	k9	však
starší	starý	k2eAgFnSc1d2	starší
<g/>
:	:	kIx,	:
oblast	oblast	k1gFnSc1	oblast
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
místem	místem	k6eAd1	místem
protéká	protékat	k5eAaImIp3nS	protékat
a	a	k8xC	a
která	který	k3yRgFnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zelenající	zelenající	k2eAgInSc1d1	zelenající
se	s	k7c7	s
<g/>
,	,	kIx,	,
zalesněné	zalesněný	k2eAgNnSc1d1	zalesněné
údolí	údolí	k1gNnSc1	údolí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Riviè	Riviè	k1gFnSc2	Riviè
Boise	Boise	k1gFnSc2	Boise
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
francouzsky	francouzsky	k6eAd1	francouzsky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
Kanaďanů	Kanaďan	k1gMnPc2	Kanaďan
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
64	[number]	k4	64
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Boise	Boise	k1gFnSc2	Boise
<g/>
,	,	kIx,	,
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Oregonem	Oregon	k1gInSc7	Oregon
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
starší	starý	k2eAgFnSc1d2	starší
pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	(
<g/>
Fort	Fort	k?	Fort
Boise	Boise	k1gFnSc1	Boise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Hudson	Hudson	k1gMnSc1	Hudson
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bay	Bay	k1gFnSc7	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
205	[number]	k4	205
671	[number]	k4	671
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
89,0	[number]	k4	89,0
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,2	[number]	k4	3,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,0	[number]	k4	3,0
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
7,1	[number]	k4	7,1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zabírá	zabírat	k5eAaImIp3nS	zabírat
Boise	Boise	k1gFnSc1	Boise
relativně	relativně	k6eAd1	relativně
velkou	velký	k2eAgFnSc4d1	velká
oblast	oblast	k1gFnSc4	oblast
-	-	kIx~	-
168	[number]	k4	168
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
Downtown	Downtown	k1gNnSc1	Downtown
Boise	Boise	k1gFnSc2	Boise
<g/>
)	)	kIx)	)
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
komerčními	komerční	k2eAgFnPc7d1	komerční
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
okolními	okolní	k2eAgFnPc7d1	okolní
čtvrtěmi	čtvrt	k1gFnPc7	čtvrt
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
velkým	velký	k2eAgInSc7d1	velký
podílem	podíl	k1gInSc7	podíl
městské	městský	k2eAgFnSc2d1	městská
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
Boise	Boise	k1gFnSc2	Boise
zahrnjí	zahrnje	k1gFnPc2	zahrnje
<g/>
:	:	kIx,	:
Downtown	Downtown	k1gMnSc1	Downtown
Boise	Boise	k1gFnSc2	Boise
The	The	k1gMnSc1	The
North	North	k1gMnSc1	North
End	End	k1gMnSc1	End
East	East	k1gMnSc1	East
End	End	k1gMnSc1	End
Northwest	Northwest	k1gMnSc1	Northwest
Boise	Boise	k1gFnSc2	Boise
South	South	k1gMnSc1	South
East	East	k1gMnSc1	East
Boise	Boise	k1gFnSc2	Boise
Southwest	Southwest	k1gMnSc1	Southwest
Boise	Boise	k1gFnSc2	Boise
West	West	k1gMnSc1	West
Boise	Boise	k1gFnSc2	Boise
Warm	Warm	k1gMnSc1	Warm
Springs	Springsa	k1gFnPc2	Springsa
The	The	k1gMnSc1	The
Boise	Boise	k1gFnSc2	Boise
Bench	Bench	k1gMnSc1	Bench
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Boise	Bois	k1gInSc6	Bois
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
následující	následující	k2eAgInPc1d1	následující
kluby	klub	k1gInPc1	klub
<g/>
:	:	kIx,	:
Čita	čit	k2eAgNnPc1d1	čit
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Guernica	Guernic	k1gInSc2	Guernic
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Boise	Boise	k1gFnSc2	Boise
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boise	Boise	k1gFnSc2	Boise
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
