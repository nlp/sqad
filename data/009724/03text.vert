<p>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
národopisná	národopisný	k2eAgFnSc1d1	národopisná
slavnost	slavnost	k1gFnSc1	slavnost
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
folklorní	folklorní	k2eAgInPc4d1	folklorní
festivaly	festival	k1gInPc4	festival
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
trvá	trvat	k5eAaImIp3nS	trvat
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
obce	obec	k1gFnPc1	obec
slováckého	slovácký	k2eAgNnSc2d1	Slovácké
Kyjovska	Kyjovsko	k1gNnSc2	Kyjovsko
zde	zde	k6eAd1	zde
představují	představovat	k5eAaImIp3nP	představovat
své	svůj	k3xOyFgInPc4	svůj
lidové	lidový	k2eAgInPc4d1	lidový
kroje	kroj	k1gInPc4	kroj
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
–	–	k?	–
jízdu	jízda	k1gFnSc4	jízda
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
Skoronice	Skoronice	k1gFnSc1	Skoronice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavění	stavění	k1gNnSc1	stavění
máje	máj	k1gInSc2	máj
(	(	kIx(	(
<g/>
Bukovany	Bukovan	k1gMnPc7	Bukovan
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
přehlídka	přehlídka	k1gFnSc1	přehlídka
dechových	dechový	k2eAgFnPc2d1	dechová
a	a	k8xC	a
cimbálových	cimbálový	k2eAgFnPc2d1	cimbálová
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc2d1	taneční
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
jarmark	jarmark	k?	jarmark
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
přehlídka	přehlídka	k1gFnSc1	přehlídka
dětských	dětský	k2eAgInPc2d1	dětský
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
krojovaný	krojovaný	k2eAgInSc1d1	krojovaný
průvod	průvod	k1gInSc1	průvod
aj.	aj.	kA	aj.
Festival	festival	k1gInSc1	festival
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
letní	letní	k2eAgNnSc4d1	letní
kino	kino	k1gNnSc4	kino
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
zde	zde	k6eAd1	zde
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
tradičních	tradiční	k2eAgFnPc2d1	tradiční
folklorních	folklorní	k2eAgFnPc2d1	folklorní
slavností	slavnost	k1gFnPc2	slavnost
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc1	rok
nevychází	vycházet	k5eNaImIp3nS	vycházet
z	z	k7c2	z
žádné	žádný	k3yNgFnSc2	žádný
náboženské	náboženský	k2eAgFnSc2d1	náboženská
tradice	tradice	k1gFnSc2	tradice
nebo	nebo	k8xC	nebo
svátku	svátek	k1gInSc2	svátek
patrona	patron	k1gMnSc2	patron
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Sokola	Sokol	k1gMnSc2	Sokol
a	a	k8xC	a
Národních	národní	k2eAgFnPc2d1	národní
jednot	jednota	k1gFnPc2	jednota
chtěli	chtít	k5eAaImAgMnP	chtít
původně	původně	k6eAd1	původně
ukázat	ukázat	k5eAaPmF	ukázat
všechny	všechen	k3xTgInPc4	všechen
místní	místní	k2eAgInPc4d1	místní
lidové	lidový	k2eAgInPc4d1	lidový
zvyky	zvyk	k1gInPc4	zvyk
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
probíhají	probíhat	k5eAaImIp3nP	probíhat
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
a	a	k8xC	a
probudit	probudit	k5eAaPmF	probudit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
slovácké	slovácký	k2eAgInPc4d1	slovácký
kroje	kroj	k1gInPc4	kroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
značně	značně	k6eAd1	značně
vytrácely	vytrácet	k5eAaImAgFnP	vytrácet
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
tak	tak	k6eAd1	tak
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
folklorního	folklorní	k2eAgInSc2d1	folklorní
festivalu	festival	k1gInSc2	festival
ve	v	k7c6	v
Strážnici	Strážnice	k1gFnSc6	Strážnice
nebo	nebo	k8xC	nebo
vlčnovské	vlčnovský	k2eAgFnSc2d1	vlčnovská
jízdy	jízda	k1gFnSc2	jízda
králů	král	k1gMnPc2	král
patří	patřit	k5eAaImIp3nS	patřit
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
folklórní	folklórní	k2eAgFnPc4d1	folklórní
události	událost	k1gFnPc4	událost
východní	východní	k2eAgFnSc2d1	východní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
slavnosti	slavnost	k1gFnSc6	slavnost
pouze	pouze	k6eAd1	pouze
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
Dolňácka	Dolňácko	k1gNnSc2	Dolňácko
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
krojovaných	krojovaný	k2eAgInPc2d1	krojovaný
sborů	sbor	k1gInPc2	sbor
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
počet	počet	k1gInSc4	počet
platících	platící	k2eAgMnPc2d1	platící
návštěvníků	návštěvník	k1gMnPc2	návštěvník
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
21	[number]	k4	21
000	[number]	k4	000
a	a	k8xC	a
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
zde	zde	k6eAd1	zde
asi	asi	k9	asi
3	[number]	k4	3
500	[number]	k4	500
účinkujících	účinkující	k1gMnPc2	účinkující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krojovaném	krojovaný	k2eAgInSc6d1	krojovaný
průvodu	průvod	k1gInSc6	průvod
šlo	jít	k5eAaImAgNnS	jít
2	[number]	k4	2
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
33	[number]	k4	33
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc1	rok
uspořádán	uspořádán	k2eAgInSc1d1	uspořádán
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
místního	místní	k2eAgMnSc4d1	místní
Sokola	Sokol	k1gMnSc4	Sokol
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
nesl	nést	k5eAaImAgMnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
sokolských	sokolská	k1gFnPc2	sokolská
oslav	oslava	k1gFnPc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Župních	župní	k2eAgNnPc2d1	župní
cvičení	cvičení	k1gNnPc2	cvičení
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgInP	zúčastnit
sokolské	sokolský	k2eAgInPc1d1	sokolský
soubory	soubor	k1gInPc1	soubor
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
dopoledne	dopoledne	k1gNnSc2	dopoledne
sokolská	sokolská	k1gFnSc1	sokolská
jednota	jednota	k1gFnSc1	jednota
z	z	k7c2	z
Vlčnova	Vlčnov	k1gInSc2	Vlčnov
předvedla	předvést	k5eAaPmAgFnS	předvést
Jízdu	jízda	k1gFnSc4	jízda
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
prošel	projít	k5eAaPmAgMnS	projít
městem	město	k1gNnSc7	město
krojovaný	krojovaný	k2eAgInSc1d1	krojovaný
průvod	průvod	k1gInSc1	průvod
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c6	na
cvičišti	cvičiště	k1gNnSc6	cvičiště
u	u	k7c2	u
sokolovny	sokolovna	k1gFnSc2	sokolovna
byly	být	k5eAaImAgInP	být
předvedeny	předveden	k2eAgInPc1d1	předveden
lidové	lidový	k2eAgInPc1d1	lidový
zvyky	zvyk	k1gInPc1	zvyk
<g/>
:	:	kIx,	:
Dožínky	dožínky	k1gFnPc1	dožínky
(	(	kIx(	(
<g/>
obce	obec	k1gFnPc1	obec
Ježov	Ježovo	k1gNnPc2	Ježovo
<g/>
,	,	kIx,	,
Kelčany	Kelčan	k1gMnPc7	Kelčan
<g/>
,	,	kIx,	,
Žádovice	Žádovice	k1gFnSc1	Žádovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahájení	zahájení	k1gNnSc1	zahájení
hodů	hod	k1gInPc2	hod
(	(	kIx(	(
<g/>
Lanžhot	Lanžhot	k1gInSc1	Lanžhot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnec	věnec	k1gInSc4	věnec
(	(	kIx(	(
<g/>
Bukovany	Bukovan	k1gMnPc4	Bukovan
<g/>
,	,	kIx,	,
Sobůlky	Sobůlek	k1gMnPc4	Sobůlek
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
stínání	stínání	k1gNnSc1	stínání
berana	beran	k1gMnSc2	beran
(	(	kIx(	(
<g/>
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mlácení	mlácení	k1gNnSc1	mlácení
kačera	kačer	k1gMnSc2	kačer
(	(	kIx(	(
<g/>
Boršov	Boršov	k1gInSc1	Boršov
<g/>
,	,	kIx,	,
Nětčice	Nětčice	k1gFnSc1	Nětčice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šlahačka	šlahačka	k1gFnSc1	šlahačka
(	(	kIx(	(
<g/>
Vlkoš	Vlkoš	k1gMnSc1	Vlkoš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
Milotice	Milotice	k1gFnSc1	Milotice
<g/>
,	,	kIx,	,
Šardice	Šardice	k1gFnSc1	Šardice
<g/>
)	)	kIx)	)
a	a	k8xC	a
kácení	kácení	k1gNnSc1	kácení
máje	máj	k1gInSc2	máj
(	(	kIx(	(
<g/>
Svatobořice	Svatobořice	k1gFnSc1	Svatobořice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slavnosti	slavnost	k1gFnPc4	slavnost
doplňovala	doplňovat	k5eAaImAgFnS	doplňovat
velká	velký	k2eAgFnSc1d1	velká
národopisná	národopisný	k2eAgFnSc1d1	národopisná
výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
dramatu	drama	k1gNnSc2	drama
Maryša	Maryša	k1gFnSc1	Maryša
Aloise	Alois	k1gMnSc2	Alois
a	a	k8xC	a
Viléma	Vilém	k1gMnSc2	Vilém
Mrštíků	Mrštík	k1gInPc2	Mrštík
a	a	k8xC	a
krojovaná	krojovaný	k2eAgFnSc1d1	krojovaná
Jízda	jízda	k1gFnSc1	jízda
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
popularizaci	popularizace	k1gFnSc3	popularizace
slavností	slavnost	k1gFnPc2	slavnost
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
jejich	jejich	k3xOp3gNnSc1	jejich
filmování	filmování	k1gNnSc1	filmování
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
promítání	promítání	k1gNnSc1	promítání
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
slavnosti	slavnost	k1gFnSc2	slavnost
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
až	až	k9	až
30	[number]	k4	30
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Přítomni	přítomen	k2eAgMnPc1d1	přítomen
byli	být	k5eAaImAgMnP	být
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
novináři	novinář	k1gMnPc1	novinář
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
příznivé	příznivý	k2eAgInPc4d1	příznivý
ohlasy	ohlas	k1gInPc4	ohlas
byl	být	k5eAaImAgInS	být
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc4	rok
opakován	opakován	k2eAgInSc4d1	opakován
hned	hned	k6eAd1	hned
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
slavnosti	slavnost	k1gFnPc1	slavnost
byly	být	k5eAaImAgFnP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
nepříznivé	příznivý	k2eNgFnSc3d1	nepříznivá
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
nakonec	nakonec	k6eAd1	nakonec
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pětadvacetileté	pětadvacetiletý	k2eAgFnSc6d1	pětadvacetiletá
přestávce	přestávka	k1gFnSc6	přestávka
se	se	k3xPyFc4	se
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc1	rok
znovu	znovu	k6eAd1	znovu
konal	konat	k5eAaImAgInS	konat
23	[number]	k4	23
<g/>
.	.	kIx.	.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
a	a	k8xC	a
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
pořádání	pořádání	k1gNnSc4	pořádání
Slováckého	slovácký	k2eAgInSc2d1	slovácký
roku	rok	k1gInSc2	rok
pravidelně	pravidelně	k6eAd1	pravidelně
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
Slováckých	slovácký	k2eAgInPc2d1	slovácký
roků	rok	k1gInPc2	rok
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1927	[number]	k4	1927
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1931	[number]	k4	1931
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1957	[number]	k4	1957
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
==	==	k?	==
Kyjovské	kyjovský	k2eAgFnPc1d1	Kyjovská
letní	letní	k2eAgFnPc1d1	letní
slavnosti	slavnost	k1gFnPc1	slavnost
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc1	rok
koná	konat	k5eAaImIp3nS	konat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
pořádají	pořádat	k5eAaImIp3nP	pořádat
Kyjovské	kyjovský	k2eAgFnPc1d1	Kyjovská
letní	letní	k2eAgFnPc1d1	letní
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
trvají	trvat	k5eAaImIp3nP	trvat
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
víkend	víkend	k1gInSc4	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
především	především	k6eAd1	především
s	s	k7c7	s
dožínkami	dožínky	k1gFnPc7	dožínky
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
bývá	bývat	k5eAaImIp3nS	bývat
jarmark	jarmark	k?	jarmark
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
s	s	k7c7	s
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
programem	program	k1gInSc7	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nezaměřuje	zaměřovat	k5eNaImIp3nS	zaměřovat
jen	jen	k9	jen
na	na	k7c4	na
folklor	folklor	k1gInSc4	folklor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
bývá	bývat	k5eAaImIp3nS	bývat
pravidelně	pravidelně	k6eAd1	pravidelně
Žalmanův	Žalmanův	k2eAgInSc4d1	Žalmanův
folkový	folkový	k2eAgInSc4d1	folkový
Kyjov	Kyjov	k1gInSc4	Kyjov
na	na	k7c6	na
letním	letní	k2eAgNnSc6d1	letní
kině	kino	k1gNnSc6	kino
nebo	nebo	k8xC	nebo
výstava	výstava	k1gFnSc1	výstava
a	a	k8xC	a
ochutnávka	ochutnávka	k1gFnSc1	ochutnávka
vín	víno	k1gNnPc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
dopoledne	dopoledne	k1gNnSc2	dopoledne
je	být	k5eAaImIp3nS	být
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
pouť	pouť	k1gFnSc1	pouť
a	a	k8xC	a
poutní	poutní	k2eAgFnSc1d1	poutní
mše	mše	k1gFnSc1	mše
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
před	před	k7c7	před
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
shromáždí	shromáždět	k5eAaImIp3nS	shromáždět
množství	množství	k1gNnSc1	množství
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
krojích	kroj	k1gInPc6	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
krojovaný	krojovaný	k2eAgInSc1d1	krojovaný
dožínkový	dožínkový	k2eAgInSc1d1	dožínkový
průvod	průvod	k1gInSc1	průvod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
oba	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
bývá	bývat	k5eAaImIp3nS	bývat
výstava	výstava	k1gFnSc1	výstava
králíků	králík	k1gMnPc2	králík
<g/>
,	,	kIx,	,
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
chovatelů	chovatel	k1gMnPc2	chovatel
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
program	program	k1gInSc1	program
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
vystoupení	vystoupení	k1gNnSc4	vystoupení
šermířů	šermíř	k1gMnPc2	šermíř
<g/>
,	,	kIx,	,
cimbálových	cimbálový	k2eAgFnPc2d1	cimbálová
a	a	k8xC	a
dechových	dechový	k2eAgFnPc2d1	dechová
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
Slováckého	slovácký	k2eAgInSc2d1	slovácký
souboru	soubor	k1gInSc2	soubor
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
mažoretek	mažoretka	k1gFnPc2	mažoretka
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Národopisný	národopisný	k2eAgInSc1d1	národopisný
festival	festival	k1gInSc1	festival
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
Dolňácka	Dolňácko	k1gNnSc2	Dolňácko
Milotice	Milotice	k1gFnSc2	Milotice
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SYNEK	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
obraz	obraz	k1gInSc1	obraz
národopisné	národopisný	k2eAgFnSc2d1	národopisná
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
:	:	kIx,	:
Region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
81	[number]	k4	81
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MĚCHUROVÁ	MĚCHUROVÁ	kA	MĚCHUROVÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Národopisná	národopisný	k2eAgFnSc1d1	národopisná
slavnost	slavnost	k1gFnSc1	slavnost
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
hudební	hudební	k2eAgFnSc2d1	hudební
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PAVLÍK	Pavlík	k1gMnSc1	Pavlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Návraty	návrat	k1gInPc1	návrat
Kyjovska	Kyjovsko	k1gNnSc2	Kyjovsko
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
s	s	k7c7	s
Domem	dům	k1gInSc7	dům
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
173	[number]	k4	173
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Kyjově	kyjově	k6eAd1	kyjově
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
tradic	tradice	k1gFnPc2	tradice
</s>
</p>
<p>
<s>
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc1	rok
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Folklorní	folklorní	k2eAgInSc1d1	folklorní
magazín	magazín	k1gInSc1	magazín
</s>
</p>
