<p>
<s>
Iggy	Igga	k1gFnPc1	Igga
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
James	James	k1gMnSc1	James
Newell	Newell	k1gMnSc1	Newell
Osterberg	Osterberg	k1gMnSc1	Osterberg
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
Muskegon	Muskegona	k1gFnPc2	Muskegona
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
Michigan	Michigan	k1gInSc4	Michigan
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
punkrockový	punkrockový	k2eAgMnSc1d1	punkrockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
občasný	občasný	k2eAgMnSc1d1	občasný
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Kafe	Kafe	k?	Kafe
a	a	k8xC	a
cigára	cigára	k1gFnSc1	cigára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
omezený	omezený	k2eAgInSc1d1	omezený
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
Pop	pop	k1gInSc1	pop
je	být	k5eAaImIp3nS	být
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
vzorů	vzor	k1gInPc2	vzor
punk	punk	k1gInSc4	punk
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
také	také	k9	také
nazýván	nazýván	k2eAgMnSc1d1	nazýván
jako	jako	k8xS	jako
Kmotr	kmotr	k1gMnSc1	kmotr
punku	punk	k1gInSc2	punk
nebo	nebo	k8xC	nebo
Rockový	rockový	k2eAgMnSc1d1	rockový
leguán	leguán	k1gMnSc1	leguán
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
velice	velice	k6eAd1	velice
proslulý	proslulý	k2eAgMnSc1d1	proslulý
svou	svůj	k3xOyFgFnSc7	svůj
dynamikou	dynamika	k1gFnSc7	dynamika
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pop	pop	k1gMnSc1	pop
byl	být	k5eAaImAgMnS	být
zpěvákem	zpěvák	k1gMnSc7	zpěvák
kapely	kapela	k1gFnSc2	kapela
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
podobu	podoba	k1gFnSc4	podoba
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
punku	punk	k1gInSc2	punk
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
Stooges	Stooges	k1gMnSc1	Stooges
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
Ramones	Ramones	k1gMnSc1	Ramones
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Stooges	Stooges	k1gInSc1	Stooges
se	se	k3xPyFc4	se
neblaze	blaze	k6eNd1	blaze
proslavili	proslavit	k5eAaPmAgMnP	proslavit
svými	svůj	k3xOyFgInPc7	svůj
koncerty	koncert	k1gInPc7	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
Pop	pop	k1gInSc1	pop
skákal	skákat	k5eAaImAgInS	skákat
z	z	k7c2	z
pódia	pódium	k1gNnSc2	pódium
(	(	kIx(	(
<g/>
tímto	tento	k3xDgNnSc7	tento
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc4d1	objeven
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
stage	stage	k1gInSc1	stage
diving	diving	k1gInSc1	diving
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
hrudí	hruď	k1gFnSc7	hruď
pomazanou	pomazaný	k2eAgFnSc4d1	pomazaná
syrovým	syrový	k2eAgNnSc7d1	syrové
masem	maso	k1gNnSc7	maso
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
burákovým	burákův	k2eAgNnSc7d1	burákův
máslem	máslo	k1gNnSc7	máslo
nebo	nebo	k8xC	nebo
pořezaný	pořezaný	k2eAgInSc1d1	pořezaný
rozbitými	rozbitý	k2eAgFnPc7d1	rozbitá
lahvemi	lahev	k1gFnPc7	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
zpěváků	zpěvák	k1gMnPc2	zpěvák
tyto	tento	k3xDgInPc4	tento
Popovy	popův	k2eAgInPc4d1	popův
kousky	kousek	k1gInPc4	kousek
imitovalo	imitovat	k5eAaBmAgNnS	imitovat
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
už	už	k6eAd1	už
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
divoký	divoký	k2eAgInSc1d1	divoký
jako	jako	k8xC	jako
za	za	k7c2	za
časů	čas	k1gInPc2	čas
The	The	k1gMnPc2	The
Stooges	Stoogesa	k1gFnPc2	Stoogesa
<g/>
,	,	kIx,	,
Pop	pop	k1gInSc1	pop
byl	být	k5eAaImAgInS	být
střídavě	střídavě	k6eAd1	střídavě
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
jako	jako	k8xS	jako
sólový	sólový	k2eAgMnSc1d1	sólový
zpěvák	zpěvák	k1gMnSc1	zpěvák
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
nejznámější	známý	k2eAgFnPc4d3	nejznámější
písničky	písnička	k1gFnPc4	písnička
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Lust	Lust	k1gInSc1	Lust
for	forum	k1gNnPc2	forum
Life	Lif	k1gFnSc2	Lif
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Bored	Bored	k1gInSc1	Bored
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Passenger	Passenger	k1gMnSc1	Passenger
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
básničce	básnička	k1gFnSc6	básnička
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Morrisona	Morrison	k1gMnSc2	Morrison
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
Iggy	Igga	k1gFnPc4	Igga
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollové	rollový	k2eAgFnSc2d1	rollová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Iggy	Iggy	k1gInPc4	Iggy
Pop	pop	k1gInSc1	pop
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Nina	Nina	k1gFnSc1	Nina
Alu	ala	k1gFnSc4	ala
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Iggy	Igga	k1gFnPc1	Igga
rád	rád	k6eAd1	rád
řídí	řídit	k5eAaImIp3nP	řídit
svoje	svůj	k3xOyFgNnSc4	svůj
Ferrari	ferrari	k1gNnSc4	ferrari
F430	F430	k1gFnSc1	F430
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
občasné	občasný	k2eAgFnPc4d1	občasná
přestávky	přestávka	k1gFnPc4	přestávka
stále	stále	k6eAd1	stále
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Torontu	Toronto	k1gNnSc6	Toronto
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
NXNE	NXNE	kA	NXNE
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
i	i	k9	i
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Newell	Newell	k1gMnSc1	Newell
Osterberg	Osterberg	k1gMnSc1	Osterberg
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
Muskegonu	Muskegon	k1gInSc6	Muskegon
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
Louelly	Louella	k1gFnSc2	Louella
(	(	kIx(	(
<g/>
za	za	k7c4	za
svobodna	svoboden	k2eAgFnSc1d1	svobodna
Christensen	Christensen	k1gInSc1	Christensen
<g/>
;	;	kIx,	;
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jamese	Jamese	k1gFnSc1	Jamese
Newella	Newella	k1gFnSc1	Newella
Osterberga	Osterberga	k1gFnSc1	Osterberga
<g/>
,	,	kIx,	,
Sr	Sr	k1gFnSc1	Sr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc4d1	bývalý
učitele	učitel	k1gMnSc4	učitel
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
trenéra	trenér	k1gMnSc2	trenér
baseballu	baseball	k1gInSc2	baseball
na	na	k7c4	na
Fordson	Fordson	k1gInSc4	Fordson
High	High	k1gInSc1	High
School	School	k1gInSc1	School
v	v	k7c6	v
Dearbornu	Dearborn	k1gInSc6	Dearborn
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
.	.	kIx.	.
</s>
<s>
Osterberg	Osterberg	k1gMnSc1	Osterberg
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c4	v
trailer	trailer	k1gInSc4	trailer
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
mobilheimech	mobilheim	k1gInPc6	mobilheim
či	či	k8xC	či
automobilech	automobil	k1gInPc6	automobil
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ypsilanti	Ypsilant	k1gMnPc1	Ypsilant
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
má	mít	k5eAaImIp3nS	mít
anglický	anglický	k2eAgInSc1d1	anglický
a	a	k8xC	a
irský	irský	k2eAgInSc1d1	irský
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
matčiny	matčin	k2eAgInPc1d1	matčin
norský	norský	k2eAgMnSc1d1	norský
a	a	k8xC	a
dánský	dánský	k2eAgMnSc1d1	dánský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
adoptovaný	adoptovaný	k2eAgMnSc1d1	adoptovaný
švédsko-americkou	švédskomerický	k2eAgFnSc4d1	švédsko-americký
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
jejich	jejich	k3xOp3gNnSc4	jejich
jméno	jméno	k1gNnSc4	jméno
-	-	kIx~	-
Österberg	Österberg	k1gInSc1	Österberg
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
The	The	k1gMnSc1	The
Iguanas	Iguanas	k1gMnSc1	Iguanas
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
bubeník	bubeník	k1gMnSc1	bubeník
(	(	kIx(	(
<g/>
vyšel	vyjít	k5eAaPmAgInS	vyjít
1	[number]	k4	1
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
odešel	odejít	k5eAaPmAgMnS	odejít
už	už	k9	už
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
do	do	k7c2	do
detroitské	detroitský	k2eAgFnSc2d1	detroitská
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Prime	prim	k1gInSc5	prim
Movers	Moversa	k1gFnPc2	Moversa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
přezdívku	přezdívka	k1gFnSc4	přezdívka
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
založil	založit	k5eAaPmAgMnS	založit
legendární	legendární	k2eAgFnSc4d1	legendární
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnSc1	The
Psychedelic	Psychedelice	k1gFnPc2	Psychedelice
Stooges	Stooges	k1gMnSc1	Stooges
(	(	kIx(	(
<g/>
následně	následně	k6eAd1	následně
Iggy	Igga	k1gMnSc2	Igga
&	&	k?	&
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
Asheton	Asheton	k1gInSc1	Asheton
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Scott	Scott	k2eAgInSc1d1	Scott
Asheton	Asheton	k1gInSc1	Asheton
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Alexander	Alexandra	k1gFnPc2	Alexandra
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
produkoval	produkovat	k5eAaImAgMnS	produkovat
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
si	se	k3xPyFc3	se
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
následovaly	následovat	k5eAaImAgFnP	následovat
personální	personální	k2eAgFnPc1d1	personální
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
:	:	kIx,	:
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
přišli	přijít	k5eAaPmAgMnP	přijít
Steve	Steve	k1gMnSc1	Steve
Mackay	Mackaa	k1gFnSc2	Mackaa
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Cheatham	Cheatham	k1gInSc1	Cheatham
a	a	k8xC	a
James	James	k1gMnSc1	James
Williamson	Williamson	k1gMnSc1	Williamson
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
však	však	k8xC	však
Cheatham	Cheatham	k1gInSc1	Cheatham
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
D.	D.	kA	D.
Alexandrem	Alexandr	k1gMnSc7	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgMnS	být
Zeke	Zeke	k1gFnSc4	Zeke
Zettner	Zettner	k1gMnSc1	Zettner
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
vystřídaný	vystřídaný	k2eAgInSc1d1	vystřídaný
Jimmym	Jimmym	k1gInSc1	Jimmym
Reccou	Reccá	k1gFnSc4	Reccá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
ale	ale	k9	ale
kapela	kapela	k1gFnSc1	kapela
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
byly	být	k5eAaImAgInP	být
drogy	droga	k1gFnSc2	droga
(	(	kIx(	(
<g/>
Iggy	Igga	k1gFnSc2	Igga
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
Stooges	Stoogesa	k1gFnPc2	Stoogesa
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
Asheton	Asheton	k1gInSc1	Asheton
<g/>
,	,	kIx,	,
Scott	Scott	k2eAgInSc1d1	Scott
Asheton	Asheton	k1gInSc1	Asheton
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Williamson	Williamson	k1gMnSc1	Williamson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Scott	Scott	k2eAgInSc1d1	Scott
Thurston	Thurston	k1gInSc1	Thurston
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
používali	používat	k5eAaImAgMnP	používat
název	název	k1gInSc4	název
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
&	&	k?	&
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
ale	ale	k9	ale
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
opět	opět	k6eAd1	opět
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
-	-	kIx~	-
Jamese	Jamese	k1gFnSc1	Jamese
Williamsona	Williamsona	k1gFnSc1	Williamsona
nahradil	nahradit	k5eAaPmAgInS	nahradit
Scott	Scott	k2eAgInSc1d1	Scott
Thurston	Thurston	k1gInSc1	Thurston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
Iggy	Igg	k1gMnPc7	Igg
opět	opět	k6eAd1	opět
léčil	léčit	k5eAaImAgMnS	léčit
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
spolupráce	spolupráce	k1gFnSc2	spolupráce
byla	být	k5eAaImAgFnS	být
oboustranná	oboustranný	k2eAgFnSc1d1	oboustranná
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Pop	pop	k1gInSc1	pop
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
na	na	k7c4	na
Bowieho	Bowie	k1gMnSc4	Bowie
albu	alba	k1gFnSc4	alba
Low	Low	k1gFnSc1	Low
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
ceněné	ceněný	k2eAgNnSc1d1	ceněné
album	album	k1gNnSc1	album
The	The	k1gMnSc1	The
Idiot	idiot	k1gMnSc1	idiot
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
první	první	k4xOgFnSc4	první
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
v	v	k7c4	v
Iggyho	Iggy	k1gMnSc4	Iggy
koncertní	koncertní	k2eAgNnSc1d1	koncertní
kapele	kapela	k1gFnSc3	kapela
objevili	objevit	k5eAaPmAgMnP	objevit
mj.	mj.	kA	mj.
Glen	Glen	k1gMnSc1	Glen
Matlock	Matlock	k1gMnSc1	Matlock
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
-	-	kIx~	-
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jackie	Jackie	k1gFnSc1	Jackie
Clark	Clark	k1gInSc1	Clark
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
-	-	kIx~	-
Ike	Ike	k1gFnSc1	Ike
&	&	k?	&
Tina	Tina	k1gFnSc1	Tina
Turner	turner	k1gMnSc1	turner
<g/>
)	)	kIx)	)
a	a	k8xC	a
Scott	Scott	k2eAgInSc1d1	Scott
Thurston	Thurston	k1gInSc1	Thurston
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
New	New	k1gMnSc1	New
Values	Values	k1gMnSc1	Values
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
S.	S.	kA	S.
Thurston	Thurston	k1gInSc1	Thurston
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Asheton	Asheton	k1gInSc1	Asheton
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Williamson	Williamson	k1gInSc1	Williamson
a	a	k8xC	a
Fred	Fred	k1gMnSc1	Fred
"	"	kIx"	"
<g/>
Sonic	Sonic	k1gMnSc1	Sonic
<g/>
"	"	kIx"	"
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
Soldier	Soldira	k1gFnPc2	Soldira
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ceněna	cenit	k5eAaImNgFnS	cenit
a	a	k8xC	a
mezi	mezi	k7c7	mezi
muzikanty	muzikant	k1gMnPc7	muzikant
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
objevují	objevovat	k5eAaImIp3nP	objevovat
mj.	mj.	kA	mj.
Barry	Barro	k1gNnPc7	Barro
Andrews	Andrewsa	k1gFnPc2	Andrewsa
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
XTC	XTC	kA	XTC
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Bowie	Bowie	k1gFnPc1	Bowie
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Simple	Simple	k1gFnSc2	Simple
Minds	Minds	k1gInSc1	Minds
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
G.	G.	kA	G.
Matlock	Matlock	k1gInSc1	Matlock
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
Iggy	Igg	k2eAgInPc1d1	Igg
Pop	pop	k1gInSc1	pop
prosadil	prosadit	k5eAaPmAgInS	prosadit
tanečním	taneční	k2eAgInSc7d1	taneční
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
Bang	Bang	k1gInSc1	Bang
Bang	Bang	k1gInSc1	Bang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
spoluautorem	spoluautor	k1gMnSc7	spoluautor
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Chrisem	Chris	k1gInSc7	Chris
Steinem	Stein	k1gMnSc7	Stein
na	na	k7c6	na
hudbě	hudba	k1gFnSc6	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Rule	rula	k1gFnSc6	rula
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sid	Sid	k1gFnSc2	Sid
a	a	k8xC	a
Nancy	Nancy	k1gFnSc2	Nancy
(	(	kIx(	(
<g/>
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Barva	barva	k1gFnSc1	barva
peněz	peníze	k1gInPc2	peníze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
větší	veliký	k2eAgInSc1d2	veliký
hit	hit	k1gInSc1	hit
-	-	kIx~	-
"	"	kIx"	"
<g/>
Real	Real	k1gInSc1	Real
Wild	Wild	k1gMnSc1	Wild
Child	Child	k1gMnSc1	Child
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
coververze	coververze	k1gFnSc1	coververze
písně	píseň	k1gFnSc2	píseň
Johnnyho	Johnny	k1gMnSc2	Johnny
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Keefa	Keef	k1gMnSc4	Keef
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
Instinct	Instincta	k1gFnPc2	Instincta
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
produkoval	produkovat	k5eAaImAgMnS	produkovat
Bill	Bill	k1gMnSc1	Bill
Laswell	Laswell	k1gMnSc1	Laswell
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
Steve	Steve	k1gMnSc1	Steve
Jones	Jones	k1gMnSc1	Jones
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
-	-	kIx~	-
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Cry	Cry	k1gFnSc2	Cry
Baby	baba	k1gFnSc2	baba
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
též	též	k9	též
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hardware	hardware	k1gInSc1	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
si	se	k3xPyFc3	se
vedlo	vést	k5eAaImAgNnS	vést
album	album	k1gNnSc1	album
Brick	Bricka	k1gFnPc2	Bricka
by	by	kYmCp3nS	by
Brick	Brick	k1gInSc1	Brick
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Candy	Canda	k1gFnSc2	Canda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Kate	kat	k1gInSc5	kat
Piersonovou	Piersonový	k2eAgFnSc4d1	Piersonový
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B-	B-	k1gFnSc2	B-
<g/>
52	[number]	k4	52
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Iggy	Igga	k1gFnSc2	Igga
natočil	natočit	k5eAaBmAgMnS	natočit
album	album	k1gNnSc4	album
Préliminaires	Préliminaires	k1gInSc1	Préliminaires
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
překvapivě	překvapivě	k6eAd1	překvapivě
odklonil	odklonit	k5eAaPmAgMnS	odklonit
od	od	k7c2	od
punku	punk	k1gInSc2	punk
k	k	k7c3	k
jazzu	jazz	k1gInSc3	jazz
a	a	k8xC	a
blues	blues	k1gFnSc3	blues
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Se	s	k7c7	s
Stooges	Stoogesa	k1gFnPc2	Stoogesa
====	====	k?	====
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
-	-	kIx~	-
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
-	-	kIx~	-
Fun	Fun	k1gFnSc1	Fun
House	house	k1gNnSc1	house
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
-	-	kIx~	-
Raw	Raw	k1gMnSc1	Raw
Power	Power	k1gMnSc1	Power
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
Metallic	Metallice	k1gFnPc2	Metallice
K.O.	K.O.	k1gMnPc2	K.O.
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgInSc1d1	koncertní
záznam	záznam	k1gInSc1	záznam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
-	-	kIx~	-
Open	Open	k1gInSc1	Open
Up	Up	k1gFnSc2	Up
and	and	k?	and
Bleed	Bleed	k1gInSc1	Bleed
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgInSc1d1	koncertní
záznam	záznam	k1gInSc1	záznam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
The	The	k1gFnSc1	The
Weirdness	Weirdnessa	k1gFnPc2	Weirdnessa
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
Ready	ready	k0	ready
to	ten	k3xDgNnSc1	ten
Die	Die	k1gMnSc7	Die
</s>
</p>
<p>
<s>
====	====	k?	====
S	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Williamsonem	Williamson	k1gMnSc7	Williamson
====	====	k?	====
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
Kill	Kill	k1gInSc1	Kill
City	city	k1gNnSc1	city
</s>
</p>
<p>
<s>
====	====	k?	====
Sólová	sólový	k2eAgFnSc1d1	sólová
kariéra	kariéra	k1gFnSc1	kariéra
====	====	k?	====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Studiovky	Studiovka	k1gFnSc2	Studiovka
=====	=====	k?	=====
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
The	The	k1gMnSc1	The
Idiot	idiot	k1gMnSc1	idiot
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
Lust	Lust	k1gInSc1	Lust
for	forum	k1gNnPc2	forum
Life	Lif	k1gFnSc2	Lif
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
-	-	kIx~	-
New	New	k1gMnSc1	New
Values	Values	k1gMnSc1	Values
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
-	-	kIx~	-
Soldier	Soldier	k1gInSc1	Soldier
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Party	party	k1gFnSc1	party
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
-	-	kIx~	-
Zombie	Zombie	k1gFnSc2	Zombie
Birdhouse	Birdhouse	k1gFnSc2	Birdhouse
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
-	-	kIx~	-
Blah	blaho	k1gNnPc2	blaho
Blah	blaho	k1gNnPc2	blaho
Blah	blaho	k1gNnPc2	blaho
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
-	-	kIx~	-
Instinct	Instinct	k1gInSc1	Instinct
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
Brick	Brick	k1gInSc1	Brick
by	by	kYmCp3nS	by
Brick	Brick	k1gInSc4	Brick
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
-	-	kIx~	-
American	American	k1gMnSc1	American
Caesar	Caesar	k1gMnSc1	Caesar
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
-	-	kIx~	-
Naughty	Naughta	k1gFnSc2	Naughta
Little	Little	k1gFnSc2	Little
Doggie	Doggie	k1gFnSc2	Doggie
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Avenue	avenue	k1gFnSc2	avenue
B	B	kA	B
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
-	-	kIx~	-
Beat	beat	k1gInSc4	beat
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
Up	Up	k1gMnPc2	Up
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Skull	Skull	k1gInSc1	Skull
Ring	ring	k1gInSc1	ring
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Préliminaires	Préliminaires	k1gInSc1	Préliminaires
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
Aprè	Aprè	k1gFnSc1	Aprè
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Post	post	k1gInSc1	post
Pop	pop	k1gInSc1	pop
Depression	Depression	k1gInSc1	Depression
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Živě	Živa	k1gFnSc6	Živa
=====	=====	k?	=====
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
-	-	kIx~	-
TV	TV	kA	TV
Eye	Eye	k1gFnSc1	Eye
Live	Live	k1gFnSc1	Live
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
-	-	kIx~	-
Berlin	berlina	k1gFnPc2	berlina
91	[number]	k4	91
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
-	-	kIx~	-
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
<g/>
...	...	k?	...
<g/>
Live	Live	k1gInSc1	Live
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Kompilace	kompilace	k1gFnSc2	kompilace
=====	=====	k?	=====
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
-	-	kIx~	-
Pop	pop	k1gMnSc1	pop
Music	Music	k1gMnSc1	Music
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
-	-	kIx~	-
Nude	Nud	k1gInSc2	Nud
&	&	k?	&
Rude	Rud	k1gMnSc2	Rud
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gMnSc1	pop
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
A	a	k8xC	a
Million	Million	k1gInSc1	Million
in	in	k?	in
Prizes	Prizes	k1gInSc1	Prizes
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Anthology	Antholog	k1gMnPc7	Antholog
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
Song	song	k1gInSc1	song
to	ten	k3xDgNnSc1	ten
Song	song	k1gInSc1	song
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gutterdammerung	Gutterdammerung	k1gInSc1	Gutterdammerung
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
...	...	k?	...
Vicious	Vicious	k1gInSc1	Vicious
</s>
</p>
<p>
<s>
Suck	Suck	k1gInSc1	Suck
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
....	....	k?	....
Ivan	Ivan	k1gMnSc1	Ivan
</s>
</p>
<p>
<s>
Art	Art	k?	Art
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
....	....	k?	....
Gordon	Gordon	k1gMnSc1	Gordon
Ohr	Ohr	k1gMnSc1	Ohr
</s>
</p>
<p>
<s>
Iggy	Igga	k1gFnPc1	Igga
and	and	k?	and
the	the	k?	the
Stooges	Stooges	k1gInSc1	Stooges
<g/>
:	:	kIx,	:
Escaped	Escaped	k1gInSc1	Escaped
Maniacs	Maniacs	k1gInSc1	Maniacs
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lil	lít	k5eAaImAgInS	lít
<g/>
'	'	kIx"	'
Bush	Bush	k1gMnSc1	Bush
<g/>
:	:	kIx,	:
Resident	resident	k1gMnSc1	resident
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
"	"	kIx"	"
....	....	k?	....
Lil	lít	k5eAaImAgMnS	lít
<g/>
'	'	kIx"	'
Rummy	Rumm	k1gInPc1	Rumm
(	(	kIx(	(
<g/>
13	[number]	k4	13
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Persepolis	Persepolis	k1gFnSc1	Persepolis
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
:	:	kIx,	:
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
....	....	k?	....
Strýc	strýc	k1gMnSc1	strýc
Anouche	Anouch	k1gFnSc2	Anouch
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
American	American	k1gMnSc1	American
Dad	Dad	k1gMnSc1	Dad
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
....	....	k?	....
Jerry	Jerra	k1gFnPc1	Jerra
(	(	kIx(	(
<g/>
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wayne	Waynout	k5eAaImIp3nS	Waynout
County	Count	k1gInPc4	Count
Ramblin	Ramblina	k1gFnPc2	Ramblina
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
....	....	k?	....
Simon	Simon	k1gMnSc1	Simon
</s>
</p>
<p>
<s>
Driv	Driv	k1gInSc1	Driv
<g/>
3	[number]	k4	3
<g/>
r	r	kA	r
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
VG	VG	kA	VG
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
....	....	k?	....
Baccus	Baccus	k1gInSc1	Baccus
<g/>
,	,	kIx,	,
Další	další	k2eAgInSc1d1	další
hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
Coffee	Coffee	k1gFnSc1	Coffee
and	and	k?	and
Cigarettes	Cigarettes	k1gInSc1	Cigarettes
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
....	....	k?	....
Iggy	Igg	k2eAgFnPc4d1	Igg
(	(	kIx(	(
<g/>
segment	segment	k1gInSc4	segment
"	"	kIx"	"
<g/>
Somewhere	Somewher	k1gMnSc5	Somewher
in	in	k?	in
California	Californium	k1gNnPc4	Californium
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fastlane	Fastlan	k1gMnSc5	Fastlan
<g/>
"	"	kIx"	"
....	....	k?	....
Teddy	Tedd	k1gInPc1	Tedd
McNair	McNaira	k1gFnPc2	McNaira
(	(	kIx(	(
<g/>
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Snow	Snow	k?	Snow
Day	Day	k1gFnSc1	Day
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
....	....	k?	....
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Zellweger	Zellweger	k1gMnSc1	Zellweger
</s>
</p>
<p>
<s>
The	The	k?	The
Rugrats	Rugrats	k1gInSc1	Rugrats
Movie	Movie	k1gFnSc1	Movie
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
....	....	k?	....
Novorozeně	novorozeně	k6eAd1	novorozeně
</s>
</p>
<p>
<s>
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Stanice	stanice	k1gFnSc1	stanice
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gMnSc1	Space
Nine	Nine	k1gInSc1	Nine
....	....	k?	....
Yelgrun	Yelgrun	k1gInSc1	Yelgrun
(	(	kIx(	(
<g/>
epizoda	epizoda	k1gFnSc1	epizoda
"	"	kIx"	"
<g/>
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
neohrožených	ohrožený	k2eNgInPc2d1	neohrožený
Ferengů	Fereng	k1gInPc2	Fereng
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Brave	brav	k1gInSc5	brav
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
....	....	k?	....
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jí	jíst	k5eAaImIp3nS	jíst
ptačí	ptačí	k2eAgNnSc4d1	ptačí
stehno	stehno	k1gNnSc4	stehno
</s>
</p>
<p>
<s>
The	The	k?	The
Crow	Crow	k1gFnSc1	Crow
<g/>
:	:	kIx,	:
City	city	k1gNnSc1	city
of	of	k?	of
Angels	Angelsa	k1gFnPc2	Angelsa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
....	....	k?	....
Curve	Curev	k1gFnPc1	Curev
</s>
</p>
<p>
<s>
Dead	Dead	k1gMnSc1	Dead
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
....	....	k?	....
Salvatore	Salvator	k1gMnSc5	Salvator
'	'	kIx"	'
<g/>
Sally	Sallo	k1gNnPc7	Sallo
<g/>
'	'	kIx"	'
Jenko	Jenko	k1gNnSc1	Jenko
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
Girl	girl	k1gFnSc2	girl
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
....	....	k?	....
Rat	Rat	k1gFnSc1	Rat
Face	Fac	k1gFnSc2	Fac
</s>
</p>
<p>
<s>
Atolladero	Atolladero	k1gNnSc1	Atolladero
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
....	....	k?	....
Madden	Maddna	k1gFnPc2	Maddna
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Pete	Pete	k1gInSc1	Pete
&	&	k?	&
Pete	Pete	k1gInSc1	Pete
<g/>
"	"	kIx"	"
....	....	k?	....
James	James	k1gMnSc1	James
Mecklenberg	Mecklenberg	k1gMnSc1	Mecklenberg
(	(	kIx(	(
<g/>
3	[number]	k4	3
epizody	epizoda	k1gFnPc4	epizoda
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Coffee	Coffee	k1gFnSc1	Coffee
and	and	k?	and
Cigarettes	Cigarettesa	k1gFnPc2	Cigarettesa
III	III	kA	III
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
....	....	k?	....
Iggy	Iggy	k1gInPc1	Iggy
</s>
</p>
<p>
<s>
Cry-Baby	Cry-Baba	k1gFnPc1	Cry-Baba
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
....	....	k?	....
Strýc	strýc	k1gMnSc1	strýc
Belvedere	Belveder	k1gMnSc5	Belveder
Rickettes	Rickettes	k1gInSc1	Rickettes
</s>
</p>
<p>
<s>
Hardware	hardware	k1gInSc1	hardware
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
....	....	k?	....
Vzteklý	vzteklý	k2eAgMnSc1d1	vzteklý
Bob	Bob	k1gMnSc1	Bob
</s>
</p>
<p>
<s>
The	The	k?	The
Color	Color	k1gInSc1	Color
of	of	k?	of
Money	Monea	k1gFnSc2	Monea
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
....	....	k?	....
Hráč	hráč	k1gMnSc1	hráč
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
</s>
</p>
<p>
<s>
Sid	Sid	k?	Sid
and	and	k?	and
Nancy	Nancy	k1gFnSc2	Nancy
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
....	....	k?	....
Host	host	k1gMnSc1	host
</s>
</p>
<p>
<s>
Rock	rock	k1gInSc1	rock
&	&	k?	&
Rule	rula	k1gFnSc6	rula
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
....	....	k?	....
Monstrum	monstrum	k1gNnSc1	monstrum
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
dimenze	dimenze	k1gFnSc2	dimenze
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hold	hold	k1gInSc4	hold
Tight	Tight	k2eAgInSc4d1	Tight
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
....	....	k?	....
Hostující	hostující	k2eAgMnSc1d1	hostující
umělec	umělec	k1gMnSc1	umělec
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
