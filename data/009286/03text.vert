<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Filipín	Filipíny	k1gFnPc2	Filipíny
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
Emiliem	Emilium	k1gNnSc7	Emilium
Aguinaldem	Aguinald	k1gMnSc7	Aguinald
a	a	k8xC	a
přijata	přijat	k2eAgFnSc1d1	přijata
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
země	země	k1gFnSc1	země
získala	získat	k5eAaPmAgFnS	získat
samostatnost	samostatnost	k1gFnSc4	samostatnost
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
Filipíny	Filipíny	k1gFnPc4	Filipíny
spadaly	spadat	k5eAaPmAgInP	spadat
pod	pod	k7c4	pod
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
ušita	ušít	k5eAaPmNgFnS	ušít
místní	místní	k2eAgFnSc7d1	místní
obyvatelkou	obyvatelka	k1gFnSc7	obyvatelka
Marcalou	Marcalý	k2eAgFnSc7d1	Marcalý
de	de	k?	de
Agonicillou	Agonicilla	k1gFnSc7	Agonicilla
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
změna	změna	k1gFnSc1	změna
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
dvou	dva	k4xCgInPc2	dva
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
–	–	k?	–
modrého	modré	k1gNnSc2	modré
a	a	k8xC	a
červeného	červené	k1gNnSc2	červené
–	–	k?	–
s	s	k7c7	s
rovnostranným	rovnostranný	k2eAgInSc7d1	rovnostranný
žerďovým	žerďův	k2eAgInSc7d1	žerďův
klínem	klín	k1gInSc7	klín
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
klínu	klín	k1gInSc2	klín
je	být	k5eAaImIp3nS	být
vyobrazeno	vyobrazen	k2eAgNnSc1d1	vyobrazeno
žluté	žlutý	k2eAgNnSc1d1	žluté
<g/>
/	/	kIx~	/
<g/>
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
slunce	slunce	k1gNnSc4	slunce
s	s	k7c7	s
osmi	osm	k4xCc7	osm
paprsky	paprsek	k1gInPc7	paprsek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
představuje	představovat	k5eAaImIp3nS	představovat
prvních	první	k4xOgNnPc6	první
osm	osm	k4xCc1	osm
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
rohu	roh	k1gInSc6	roh
klínu	klín	k1gInSc2	klín
jsou	být	k5eAaImIp3nP	být
zlaté	zlatý	k2eAgFnPc1d1	zlatá
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
hrotem	hrot	k1gInSc7	hrot
směřujícím	směřující	k2eAgInSc7d1	směřující
od	od	k7c2	od
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
ostrovy	ostrov	k1gInPc1	ostrov
Filipín	Filipíny	k1gFnPc2	Filipíny
–	–	k?	–
Luzon	Luzon	k1gMnSc1	Luzon
<g/>
,	,	kIx,	,
Visayas	Visayas	k1gMnSc1	Visayas
a	a	k8xC	a
Mindanao	Mindanao	k1gMnSc1	Mindanao
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
klínu	klín	k1gInSc2	klín
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
rovnost	rovnost	k1gFnSc1	rovnost
a	a	k8xC	a
svornost	svornost	k1gFnSc1	svornost
<g/>
,	,	kIx,	,
modrá	modrat	k5eAaImIp3nS	modrat
mír	mír	k1gInSc1	mír
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
udatnost	udatnost	k1gFnSc1	udatnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
filipínské	filipínský	k2eAgFnSc2d1	filipínská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
rozmístění	rozmístění	k1gNnSc2	rozmístění
barevných	barevný	k2eAgInPc2d1	barevný
pruhů	pruh	k1gInPc2	pruh
indikuje	indikovat	k5eAaBmIp3nS	indikovat
mírový	mírový	k2eAgInSc4d1	mírový
či	či	k8xC	či
válečný	válečný	k2eAgInSc4d1	válečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
je	být	k5eAaImIp3nS	být
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
umístěn	umístit	k5eAaPmNgInS	umístit
nahoře	nahoře	k6eAd1	nahoře
nebo	nebo	k8xC	nebo
při	při	k7c6	při
svislém	svislý	k2eAgNnSc6d1	svislé
zavěšení	zavěšení	k1gNnSc6	zavěšení
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
zavěšuje	zavěšovat	k5eAaImIp3nS	zavěšovat
obráceně	obráceně	k6eAd1	obráceně
(	(	kIx(	(
<g/>
dole	dole	k6eAd1	dole
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
rozvržením	rozvržení	k1gNnSc7	rozvržení
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
české	český	k2eAgFnSc3d1	Česká
vlajce	vlajka	k1gFnSc3	vlajka
(	(	kIx(	(
<g/>
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
poskládání	poskládání	k1gNnSc6	poskládání
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
poměru	poměr	k1gInSc3	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
tvaru	tvar	k1gInSc2	tvar
klínu	klín	k1gInSc2	klín
–	–	k?	–
český	český	k2eAgInSc1d1	český
není	být	k5eNaImIp3nS	být
rovnostranný	rovnostranný	k2eAgInSc1d1	rovnostranný
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
žlutých	žlutý	k2eAgFnPc6d1	žlutá
<g/>
/	/	kIx~	/
<g/>
zlatých	zlatý	k2eAgFnPc6d1	zlatá
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
První	první	k4xOgFnSc2	první
filipínské	filipínský	k2eAgFnSc2d1	filipínská
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
filipínské	filipínský	k2eAgFnSc6d1	filipínská
vlajce	vlajka	k1gFnSc6	vlajka
světle	světle	k6eAd1	světle
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
odstínem	odstín	k1gInSc7	odstín
námořní	námořní	k2eAgFnSc2d1	námořní
modré	modrý	k2eAgFnSc2d1	modrá
jako	jako	k8xS	jako
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
byly	být	k5eAaImAgInP	být
Filipíny	Filipíny	k1gFnPc4	Filipíny
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
používána	používat	k5eAaImNgFnS	používat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
původní	původní	k2eAgFnSc1d1	původní
filipínská	filipínský	k2eAgFnSc1d1	filipínská
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
fialovým	fialový	k2eAgInSc7d1	fialový
odstínem	odstín	k1gInSc7	odstín
horního	horní	k2eAgInSc2d1	horní
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
až	až	k9	až
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
modrý	modrý	k2eAgInSc1d1	modrý
odstín	odstín	k1gInSc1	odstín
pruhu	pruh	k1gInSc2	pruh
vlajky	vlajka	k1gFnSc2	vlajka
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
přijata	přijat	k2eAgFnSc1d1	přijata
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
tmavě	tmavě	k6eAd1	tmavě
modrým	modrý	k2eAgInSc7d1	modrý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kompromisu	kompromis	k1gInSc3	kompromis
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
odstín	odstín	k1gInSc1	odstín
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
královskou	královský	k2eAgFnSc4d1	královská
modř	modř	k1gFnSc4	modř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filipínské	filipínský	k2eAgFnPc1d1	filipínská
historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Filipín	Filipíny	k1gFnPc2	Filipíny
</s>
</p>
<p>
<s>
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
