<s>
Rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
observatoř	observatoř	k1gFnSc1	observatoř
Chandra	chandra	k1gFnSc1	chandra
je	být	k5eAaImIp3nS	být
rentgenový	rentgenový	k2eAgInSc4d1	rentgenový
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
přezdívky	přezdívka	k1gFnSc2	přezdívka
indického	indický	k2eAgMnSc2d1	indický
astrofyzika	astrofyzik	k1gMnSc2	astrofyzik
Subrahmanyana	Subrahmanyan	k1gMnSc2	Subrahmanyan
Chandrasekhara	Chandrasekhar	k1gMnSc2	Chandrasekhar
<g/>
.	.	kIx.	.
</s>
<s>
Observatoř	observatoř	k1gFnSc4	observatoř
vynesl	vynést	k5eAaPmAgInS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
raketoplán	raketoplán	k1gInSc4	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
během	během	k7c2	během
mise	mise	k1gFnSc2	mise
STS-	STS-	k1gFnSc2	STS-
<g/>
93	[number]	k4	93
<g/>
.	.	kIx.	.
</s>
<s>
Chandru	chandra	k1gFnSc4	chandra
postavila	postavit	k5eAaPmAgFnS	postavit
firma	firma	k1gFnSc1	firma
TRW	TRW	kA	TRW
Space	Space	k1gFnSc1	Space
&	&	k?	&
Electronics	Electronicsa	k1gFnPc2	Electronicsa
Group	Group	k1gInSc4	Group
z	z	k7c2	z
Redondo	Redondo	k6eAd1	Redondo
Beach	Beacha	k1gFnPc2	Beacha
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
observatoře	observatoř	k1gFnSc2	observatoř
je	být	k5eAaImIp3nS	být
středisko	středisko	k1gNnSc1	středisko
NASA	NASA	kA	NASA
Marshall	Marshall	k1gMnSc1	Marshall
Space	Space	k1gMnSc1	Space
Flight	Flight	k1gMnSc1	Flight
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
MSFC	MSFC	kA	MSFC
<g/>
)	)	kIx)	)
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Huntsville	Huntsvill	k1gInSc6	Huntsvill
<g/>
.	.	kIx.	.
</s>
<s>
Chandra	chandra	k1gFnSc1	chandra
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
projekty	projekt	k1gInPc4	projekt
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc4d1	celkový
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
vývoj	vývoj	k1gInSc4	vývoj
se	se	k3xPyFc4	se
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
na	na	k7c6	na
1,5	[number]	k4	1,5
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
,	,	kIx,	,
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
a	a	k8xC	a
Comptonovou	Comptonový	k2eAgFnSc7d1	Comptonová
gama	gama	k1gNnSc1	gama
observatoří	observatoř	k1gFnSc7	observatoř
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
tzv.	tzv.	kA	tzv.
Velkých	velká	k1gFnPc6	velká
kosmických	kosmický	k2eAgFnPc2d1	kosmická
observatoří	observatoř	k1gFnPc2	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
13,8	[number]	k4	13,8
x	x	k?	x
19,5	[number]	k4	19,5
m	m	kA	m
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
4	[number]	k4	4
800	[number]	k4	800
kg	kg	kA	kg
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
náklad	náklad	k1gInSc4	náklad
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
kdy	kdy	k6eAd1	kdy
raketoplán	raketoplán	k1gInSc4	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
vynesl	vynést	k5eAaPmAgInS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Úlohou	úloha	k1gFnSc7	úloha
observatoře	observatoř	k1gFnSc2	observatoř
je	být	k5eAaImIp3nS	být
vytvářet	vytvářet	k5eAaImF	vytvářet
podrobné	podrobný	k2eAgInPc4d1	podrobný
snímky	snímek	k1gInPc4	snímek
a	a	k8xC	a
spektra	spektrum	k1gNnPc4	spektrum
kosmických	kosmický	k2eAgInPc2d1	kosmický
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
zdrojů	zdroj	k1gInPc2	zdroj
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
i	i	k8xC	i
spektrální	spektrální	k2eAgFnSc7d1	spektrální
rozlišovací	rozlišovací	k2eAgFnSc7d1	rozlišovací
schopností	schopnost	k1gFnSc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rentgenové	rentgenový	k2eAgFnPc1d1	rentgenová
dvojhvězdy	dvojhvězda	k1gFnPc1	dvojhvězda
<g/>
,	,	kIx,	,
supernovy	supernova	k1gFnPc1	supernova
<g/>
,	,	kIx,	,
pulzary	pulzar	k1gInPc1	pulzar
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnPc1d1	aktivní
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
,	,	kIx,	,
mezigalaktická	mezigalaktický	k2eAgFnSc1d1	mezigalaktická
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
kvazary	kvazar	k1gInPc1	kvazar
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
rentgenové	rentgenový	k2eAgFnSc6d1	rentgenová
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
atmosféra	atmosféra	k1gFnSc1	atmosféra
ho	on	k3xPp3gMnSc4	on
nepropouští	propouštět	k5eNaImIp3nS	propouštět
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
život	život	k1gInSc1	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Chandra	chandra	k1gFnSc1	chandra
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
měkkém	měkký	k2eAgInSc6d1	měkký
<g/>
"	"	kIx"	"
rentgenovém	rentgenový	k2eAgNnSc6d1	rentgenové
záření	záření	k1gNnSc6	záření
od	od	k7c2	od
1	[number]	k4	1
keV	keV	k?	keV
do	do	k7c2	do
10	[number]	k4	10
keV	keV	k?	keV
<g/>
.	.	kIx.	.
</s>
<s>
Chandra	chandra	k1gFnSc1	chandra
je	být	k5eAaImIp3nS	být
tříose	tříose	k6eAd1	tříose
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
družice	družice	k1gFnSc1	družice
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
14	[number]	k4	14
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Přístrojový	přístrojový	k2eAgInSc1d1	přístrojový
úsek	úsek	k1gInSc1	úsek
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
11,8	[number]	k4	11,8
m	m	kA	m
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
nízkého	nízký	k2eAgInSc2d1	nízký
osmibokého	osmiboký	k2eAgInSc2d1	osmiboký
hranolu	hranol	k1gInSc2	hranol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavená	vybavený	k2eAgFnSc1d1	vybavená
párem	pár	k1gInSc7	pár
slunečních	sluneční	k2eAgInPc2d1	sluneční
panelů	panel	k1gInPc2	panel
s	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
19,51	[number]	k4	19,51
m.	m.	k?	m.
Panely	panel	k1gInPc7	panel
dodávají	dodávat	k5eAaImIp3nP	dodávat
2,35	[number]	k4	2,35
kW	kW	kA	kW
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
dobíjí	dobíjet	k5eAaImIp3nS	dobíjet
tři	tři	k4xCgFnPc4	tři
akumulátorové	akumulátorový	k2eAgFnPc4d1	akumulátorová
baterie	baterie	k1gFnPc4	baterie
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
40	[number]	k4	40
Ah	ah	k0	ah
<g/>
.	.	kIx.	.
</s>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
vybavena	vybavit	k5eAaPmNgFnS	vybavit
systémem	systém	k1gInSc7	systém
orientačních	orientační	k2eAgFnPc2d1	orientační
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenový	rentgenový	k2eAgInSc1d1	rentgenový
dalekohled	dalekohled	k1gInSc1	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
1,2	[number]	k4	1,2
m	m	kA	m
s	s	k7c7	s
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
10,05	[number]	k4	10,05
m	m	kA	m
je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc1d1	tvořený
4	[number]	k4	4
soubory	soubor	k1gInPc1	soubor
parabolických	parabolický	k2eAgNnPc2d1	parabolické
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
4	[number]	k4	4
soubory	soubor	k1gInPc1	soubor
hyperbolických	hyperbolický	k2eAgNnPc2d1	hyperbolické
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
0,85	[number]	k4	0,85
m	m	kA	m
<g/>
,	,	kIx,	,
zorným	zorný	k2eAgNnSc7d1	zorné
polem	pole	k1gNnSc7	pole
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
1,0	[number]	k4	1,0
<g/>
°	°	k?	°
a	a	k8xC	a
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
0,5	[number]	k4	0,5
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zrcadla	zrcadlo	k1gNnPc1	zrcadlo
jsou	být	k5eAaImIp3nP	být
sestavená	sestavený	k2eAgNnPc1d1	sestavené
okolo	okolo	k7c2	okolo
společné	společný	k2eAgFnSc2d1	společná
optické	optický	k2eAgFnSc2d1	optická
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
iridiem	iridium	k1gNnSc7	iridium
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
odrazu	odraz	k1gInSc2	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Dopadající	dopadající	k2eAgNnSc1d1	dopadající
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
odrazí	odrazit	k5eAaPmIp3nS	odrazit
od	od	k7c2	od
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
ploch	plocha	k1gFnPc2	plocha
parabol	parabola	k1gFnPc2	parabola
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
od	od	k7c2	od
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
ploch	plocha	k1gFnPc2	plocha
hyperbol	hyperbola	k1gFnPc2	hyperbola
a	a	k8xC	a
pak	pak	k6eAd1	pak
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
kameru	kamera	k1gFnSc4	kamera
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zrcadlům	zrcadlo	k1gNnPc3	zrcadlo
jsou	být	k5eAaImIp3nP	být
připojené	připojený	k2eAgInPc4d1	připojený
tyto	tento	k3xDgInPc4	tento
přístroje	přístroj	k1gInPc4	přístroj
<g/>
:	:	kIx,	:
ACIS	ACIS	kA	ACIS
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
CCD	CCD	kA	CCD
Imaging	Imaging	k1gInSc1	Imaging
Spectrometer	Spectrometer	k1gInSc1	Spectrometer
<g/>
)	)	kIx)	)
–	–	k?	–
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
spektrometr	spektrometr	k1gInSc4	spektrometr
pro	pro	k7c4	pro
rozsah	rozsah	k1gInSc4	rozsah
energií	energie	k1gFnPc2	energie
0.2	[number]	k4	0.2
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
keV	keV	k?	keV
HRC	hrc	k0	hrc
(	(	kIx(	(
<g/>
High	High	k1gInSc1	High
Resolution	Resolution	k1gInSc1	Resolution
Camera	Camera	k1gFnSc1	Camera
<g/>
)	)	kIx)	)
–	–	k?	–
kamera	kamera	k1gFnSc1	kamera
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
pro	pro	k7c4	pro
zorné	zorný	k2eAgNnSc4d1	zorné
pole	pole	k1gNnSc4	pole
31	[number]	k4	31
<g/>
'	'	kIx"	'
×	×	k?	×
31	[number]	k4	31
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
rozlišení	rozlišení	k1gNnSc1	rozlišení
0,5	[number]	k4	0,5
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
časové	časový	k2eAgNnSc1d1	časové
rozlišení	rozlišení	k1gNnSc1	rozlišení
16	[number]	k4	16
ms.	ms.	k?	ms.
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
částí	část	k1gFnSc7	část
kamery	kamera	k1gFnSc2	kamera
je	být	k5eAaImIp3nS	být
mikrokanálová	mikrokanálový	k2eAgFnSc1d1	mikrokanálový
destička	destička	k1gFnSc1	destička
(	(	kIx(	(
<g/>
MCP	MCP	kA	MCP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zorné	zorný	k2eAgNnSc1d1	zorné
pole	pole	k1gNnSc1	pole
kamery	kamera	k1gFnSc2	kamera
je	být	k5eAaImIp3nS	být
31	[number]	k4	31
<g/>
́	́	k?	́
×	×	k?	×
31	[number]	k4	31
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
úhlová	úhlový	k2eAgFnSc1d1	úhlová
velikost	velikost	k1gFnSc1	velikost
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
schopnost	schopnost	k1gFnSc1	schopnost
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,5	[number]	k4	0,5
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pozemské	pozemský	k2eAgInPc1d1	pozemský
dalekohledy	dalekohled	k1gInPc1	dalekohled
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
takovéto	takovýto	k3xDgFnPc1	takovýto
rozlišovací	rozlišovací	k2eAgFnPc1d1	rozlišovací
schopnosti	schopnost	k1gFnPc1	schopnost
jen	jen	k9	jen
za	za	k7c2	za
výjimečně	výjimečně	k6eAd1	výjimečně
dobrých	dobrý	k2eAgFnPc2d1	dobrá
pozorovacích	pozorovací	k2eAgFnPc2d1	pozorovací
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
HETG	HETG	kA	HETG
(	(	kIx(	(
<g/>
High	High	k1gMnSc1	High
Energy	Energ	k1gMnPc7	Energ
Transmission	Transmission	k1gInSc4	Transmission
Grating	Grating	k1gInSc4	Grating
<g/>
)	)	kIx)	)
–	–	k?	–
transparentní	transparentní	k2eAgFnSc1d1	transparentní
mřížka	mřížka	k1gFnSc1	mřížka
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
energie	energie	k1gFnPc4	energie
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
spektrální	spektrální	k2eAgNnSc4d1	spektrální
rozlišení	rozlišení	k1gNnSc4	rozlišení
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
DE	DE	k?	DE
<g/>
=	=	kIx~	=
<g/>
60	[number]	k4	60
až	až	k9	až
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
rozsah	rozsah	k1gInSc4	rozsah
energií	energie	k1gFnPc2	energie
0,4	[number]	k4	0,4
–	–	k?	–
10	[number]	k4	10
keV	keV	k?	keV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
LETG	LETG	kA	LETG
(	(	kIx(	(
<g/>
Low	Low	k1gMnSc1	Low
Energy	Energ	k1gMnPc7	Energ
Transmission	Transmission	k1gInSc4	Transmission
Grating	Grating	k1gInSc4	Grating
<g/>
)	)	kIx)	)
–	–	k?	–
transparentní	transparentní	k2eAgFnSc1d1	transparentní
mřížka	mřížka	k1gFnSc1	mřížka
pro	pro	k7c4	pro
nízké	nízký	k2eAgFnPc4d1	nízká
energie	energie	k1gFnPc4	energie
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
spektrální	spektrální	k2eAgNnSc4d1	spektrální
rozlišení	rozlišení	k1gNnSc4	rozlišení
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
DE	DE	k?	DE
<g/>
=	=	kIx~	=
<g/>
40	[number]	k4	40
až	až	k9	až
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
rozsah	rozsah	k1gInSc4	rozsah
energií	energie	k1gFnPc2	energie
0,09	[number]	k4	0,09
–	–	k?	–
3	[number]	k4	3
keV	keV	k?	keV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mřížky	mřížka	k1gFnPc1	mřížka
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
odklon	odklon	k1gInSc4	odklon
chodu	chod	k1gInSc2	chod
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
paprsků	paprsek	k1gInPc2	paprsek
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
energiích	energie	k1gFnPc6	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mřížkového	mřížkový	k2eAgInSc2d1	mřížkový
spektrografu	spektrograf	k1gInSc2	spektrograf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hranolového	hranolový	k2eAgInSc2d1	hranolový
spektrografu	spektrograf	k1gInSc2	spektrograf
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mřížkového	mřížkový	k2eAgInSc2d1	mřížkový
spektrografu	spektrograf	k1gInSc2	spektrograf
disperze	disperze	k1gFnSc1	disperze
stejná	stejná	k1gFnSc1	stejná
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Mřížky	mřížka	k1gFnPc1	mřížka
se	se	k3xPyFc4	se
zasouvají	zasouvat	k5eAaImIp3nP	zasouvat
do	do	k7c2	do
dráhy	dráha	k1gFnSc2	dráha
paprsků	paprsek	k1gInPc2	paprsek
mezi	mezi	k7c4	mezi
objektiv	objektiv	k1gInSc4	objektiv
a	a	k8xC	a
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
mřížky	mřížka	k1gFnPc4	mřížka
zařazené	zařazený	k2eAgFnPc4d1	zařazená
za	za	k7c4	za
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
<g/>
,	,	kIx,	,
dalekohled	dalekohled	k1gInSc1	dalekohled
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
rentgenový	rentgenový	k2eAgInSc4d1	rentgenový
spektrograf	spektrograf	k1gInSc4	spektrograf
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
observatoř	observatoř	k1gFnSc1	observatoř
Chandra	chandra	k1gFnSc1	chandra
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
AXAF	AXAF	kA	AXAF
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
X-ray	Xaa	k1gFnSc2	X-raa
Astrophysics	Astrophysicsa	k1gFnPc2	Astrophysicsa
Facility	Facilita	k1gFnSc2	Facilita
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
vysláním	vyslání	k1gNnSc7	vyslání
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
NASA	NASA	kA	NASA
počítala	počítat	k5eAaImAgFnS	počítat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Schválení	schválení	k1gNnSc1	schválení
projektu	projekt	k1gInSc2	projekt
však	však	k9	však
neustále	neustále	k6eAd1	neustále
oddalovaly	oddalovat	k5eAaImAgFnP	oddalovat
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
poté	poté	k6eAd1	poté
havárie	havárie	k1gFnSc1	havárie
raketoplánu	raketoplán	k1gInSc2	raketoplán
Challenger	Challengero	k1gNnPc2	Challengero
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
Kongres	kongres	k1gInSc1	kongres
projekt	projekt	k1gInSc1	projekt
schválil	schválit	k5eAaPmAgInS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
observatoř	observatoř	k1gFnSc1	observatoř
obsahovat	obsahovat	k5eAaImF	obsahovat
dalekohled	dalekohled	k1gInSc4	dalekohled
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přehodnocení	přehodnocení	k1gNnSc1	přehodnocení
projektu	projekt	k1gInSc2	projekt
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1993	[number]	k4	1993
zredukovalo	zredukovat	k5eAaPmAgNnS	zredukovat
přístrojové	přístrojový	k2eAgNnSc1d1	přístrojové
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšena	zmenšen	k2eAgFnSc1d1	zmenšena
byla	být	k5eAaImAgFnS	být
i	i	k9	i
sběrná	sběrný	k2eAgFnSc1d1	sběrná
plocha	plocha	k1gFnSc1	plocha
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
namísto	namísto	k7c2	namísto
zlatých	zlatý	k2eAgNnPc2d1	Zlaté
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
(	(	kIx(	(
<g/>
což	což	k9	což
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
plán	plán	k1gInSc1	plán
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
použilo	použít	k5eAaPmAgNnS	použít
iridium	iridium	k1gNnSc1	iridium
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
termín	termín	k1gInSc1	termín
startu	start	k1gInSc2	start
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustálé	neustálý	k2eAgFnPc1d1	neustálá
komplikace	komplikace	k1gFnPc1	komplikace
spojené	spojený	k2eAgFnPc1d1	spojená
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
dalekohledu	dalekohled	k1gInSc2	dalekohled
a	a	k8xC	a
testováním	testování	k1gNnSc7	testování
družice	družice	k1gFnSc2	družice
vedly	vést	k5eAaImAgInP	vést
stále	stále	k6eAd1	stále
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
odkladům	odklad	k1gInPc3	odklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
dalekohled	dalekohled	k1gInSc1	dalekohled
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
významného	významný	k2eAgMnSc2d1	významný
amerického	americký	k2eAgMnSc2d1	americký
astrofyzika	astrofyzik	k1gMnSc2	astrofyzik
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
Subrahmanyana	Subrahmanyan	k1gMnSc4	Subrahmanyan
Chandrasekhara	Chandrasekhar	k1gMnSc4	Chandrasekhar
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
též	též	k9	též
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
poslat	poslat	k5eAaPmF	poslat
observatoř	observatoř	k1gFnSc4	observatoř
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
excentrickou	excentrický	k2eAgFnSc4d1	excentrická
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
85	[number]	k4	85
%	%	kIx~	%
doby	doba	k1gFnSc2	doba
svého	svůj	k3xOyFgInSc2	svůj
letu	let	k1gInSc2	let
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
nad	nad	k7c7	nad
rušivými	rušivý	k2eAgInPc7d1	rušivý
radiačními	radiační	k2eAgInPc7d1	radiační
pásy	pás	k1gInPc7	pás
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
STS-	STS-	k1gFnSc2	STS-
<g/>
93	[number]	k4	93
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
odkladech	odklad	k1gInPc6	odklad
startu	start	k1gInSc2	start
se	se	k3xPyFc4	se
Chandra	chandra	k1gFnSc1	chandra
vydala	vydat	k5eAaPmAgFnS	vydat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
nákladovém	nákladový	k2eAgInSc6d1	nákladový
prostoru	prostor	k1gInSc6	prostor
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
probíhal	probíhat	k5eAaImAgInS	probíhat
za	za	k7c2	za
dramatických	dramatický	k2eAgFnPc2d1	dramatická
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
pět	pět	k4xCc1	pět
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
startu	start	k1gInSc6	start
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Columbie	Columbia	k1gFnSc2	Columbia
ke	k	k7c3	k
zkratu	zkrat	k1gInSc3	zkrat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
elektrických	elektrický	k2eAgFnPc2d1	elektrická
baterií	baterie	k1gFnPc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc7d2	veliký
problémem	problém	k1gInSc7	problém
však	však	k8xC	však
byly	být	k5eAaImAgInP	být
krátkodobé	krátkodobý	k2eAgInPc1d1	krátkodobý
výpadky	výpadek	k1gInPc1	výpadek
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
řídicích	řídicí	k2eAgInPc2d1	řídicí
systémů	systém	k1gInPc2	systém
SSME	SSME	kA	SSME
motorů	motor	k1gInPc2	motor
č.	č.	k?	č.
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
Columbia	Columbia	k1gFnSc1	Columbia
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
10	[number]	k4	10
km	km	kA	km
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
původní	původní	k2eAgInSc4d1	původní
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
misi	mise	k1gFnSc4	mise
neohrozilo	ohrozit	k5eNaPmAgNnS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
otevření	otevření	k1gNnSc4	otevření
dveří	dveře	k1gFnPc2	dveře
nákladového	nákladový	k2eAgInSc2d1	nákladový
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
obvyklých	obvyklý	k2eAgFnPc6d1	obvyklá
kontrolách	kontrola	k1gFnPc6	kontrola
dostala	dostat	k5eAaPmAgFnS	dostat
posádka	posádka	k1gFnSc1	posádka
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
vypuštění	vypuštění	k1gNnSc3	vypuštění
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Chandra	chandra	k1gFnSc1	chandra
byla	být	k5eAaImAgFnS	být
připojená	připojený	k2eAgFnSc1d1	připojená
k	k	k7c3	k
raketovému	raketový	k2eAgInSc3d1	raketový
stupni	stupeň	k1gInSc3	stupeň
IUS	IUS	kA	IUS
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgInS	mít
umožnit	umožnit	k5eAaPmF	umožnit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Astronautka	astronautka	k1gFnSc1	astronautka
Colemanová	Colemanová	k1gFnSc1	Colemanová
přepojila	přepojit	k5eAaPmAgFnS	přepojit
systémy	systém	k1gInPc4	systém
IUS	IUS	kA	IUS
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
baterie	baterie	k1gFnPc4	baterie
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
UT	UT	kA	UT
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
odpojeny	odpojen	k2eAgInPc1d1	odpojen
kabely	kabel	k1gInPc1	kabel
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
systém	systém	k1gInSc1	systém
observatoře	observatoř	k1gFnSc2	observatoř
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
urychlujícího	urychlující	k2eAgInSc2d1	urychlující
stupně	stupeň	k1gInSc2	stupeň
s	s	k7c7	s
raketoplánem	raketoplán	k1gInSc7	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
déle	dlouho	k6eAd2	dlouho
byl	být	k5eAaImAgInS	být
nosný	nosný	k2eAgInSc1d1	nosný
prstenec	prstenec	k1gInSc1	prstenec
nastaven	nastavit	k5eAaPmNgInS	nastavit
do	do	k7c2	do
polohy	poloha	k1gFnSc2	poloha
pro	pro	k7c4	pro
vypuštění	vypuštění	k1gNnSc4	vypuštění
IUS	IUS	kA	IUS
(	(	kIx(	(
<g/>
58	[number]	k4	58
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
UT	UT	kA	UT
se	se	k3xPyFc4	se
Chandra	chandra	k1gFnSc1	chandra
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
pohonná	pohonný	k2eAgFnSc1d1	pohonná
raketa	raketa	k1gFnSc1	raketa
oddělily	oddělit	k5eAaPmAgFnP	oddělit
od	od	k7c2	od
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
observatoře	observatoř	k1gFnSc2	observatoř
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
převzalo	převzít	k5eAaPmAgNnS	převzít
středisko	středisko	k1gNnSc1	středisko
Onizuka	Onizuk	k1gMnSc2	Onizuk
AFB	AFB	kA	AFB
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Piloti	pilot	k1gMnPc1	pilot
Columbie	Columbia	k1gFnSc2	Columbia
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
UT	UT	kA	UT
úhybný	úhybný	k2eAgInSc4d1	úhybný
manévr	manévr	k1gInSc4	manévr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
raketoplán	raketoplán	k1gInSc1	raketoplán
v	v	k7c6	v
času	čas	k1gInSc6	čas
zážehu	zážeh	k1gInSc2	zážeh
motoru	motor	k1gInSc2	motor
IUS	IUS	kA	IUS
v	v	k7c6	v
bezpečné	bezpečný	k2eAgFnSc6d1	bezpečná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zážehu	zážeh	k1gInSc3	zážeh
motoru	motor	k1gInSc2	motor
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
UT	UT	kA	UT
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
117	[number]	k4	117
<g/>
sekundovém	sekundový	k2eAgInSc6d1	sekundový
zážehu	zážeh	k1gInSc6	zážeh
a	a	k8xC	a
odhození	odhození	k1gNnSc6	odhození
motoru	motor	k1gInSc2	motor
byla	být	k5eAaImAgFnS	být
Chandra	chandra	k1gFnSc1	chandra
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c7	mezi
330	[number]	k4	330
až	až	k9	až
72	[number]	k4	72
031	[number]	k4	031
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
i	i	k9	i
rozevření	rozevření	k1gNnSc1	rozevření
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
a	a	k8xC	a
navedení	navedení	k1gNnSc2	navedení
na	na	k7c4	na
konečnou	konečný	k2eAgFnSc4d1	konečná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
9	[number]	k4	9
652	[number]	k4	652
–	–	k?	–
139	[number]	k4	139
189	[number]	k4	189
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Columbie	Columbia	k1gFnPc4	Columbia
úspěšně	úspěšně	k6eAd1	úspěšně
přistála	přistát	k5eAaImAgFnS	přistát
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
středisku	středisko	k1gNnSc6	středisko
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
v	v	k7c6	v
čase	čas	k1gInSc6	čas
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
UT	UT	kA	UT
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
ventil	ventil	k1gInSc1	ventil
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
započalo	započnout	k5eAaPmAgNnS	započnout
odplynění	odplynění	k1gNnSc4	odplynění
detektorů	detektor	k1gInPc2	detektor
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
další	další	k2eAgInPc1d1	další
motorické	motorický	k2eAgInPc1d1	motorický
manévry	manévr	k1gInPc1	manévr
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
družice	družice	k1gFnSc1	družice
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
operační	operační	k2eAgFnSc4d1	operační
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
nad	nad	k7c4	nad
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
Van	van	k1gInSc4	van
Allenův	Allenův	k2eAgInSc1d1	Allenův
radiační	radiační	k2eAgInSc1d1	radiační
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ale	ale	k8xC	ale
taková	takový	k3xDgFnSc1	takový
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
vyslání	vyslání	k1gNnSc4	vyslání
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nenadálé	nenadálý	k2eAgFnSc2d1	nenadálá
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Chandra	chandra	k1gFnSc1	chandra
provedla	provést	k5eAaPmAgFnS	provést
první	první	k4xOgInSc4	první
zkušební	zkušební	k2eAgInSc4d1	zkušební
snímkování	snímkování	k1gNnSc4	snímkování
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
kalibrační	kalibrační	k2eAgNnSc1d1	kalibrační
pozorování	pozorování	k1gNnSc1	pozorování
bodového	bodový	k2eAgInSc2d1	bodový
rentgenového	rentgenový	k2eAgInSc2d1	rentgenový
zdroje	zdroj	k1gInSc2	zdroj
LMC	LMC	kA	LMC
X-1	X-1	k1gMnSc1	X-1
Velkém	velký	k2eAgNnSc6d1	velké
Magellanově	Magellanův	k2eAgNnSc6d1	Magellanovo
mračnu	mračno	k1gNnSc6	mračno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
180	[number]	k4	180
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
testu	test	k1gInSc3	test
ostrosti	ostrost	k1gFnSc2	ostrost
posloužil	posloužit	k5eAaPmAgInS	posloužit
zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
supernovy	supernova	k1gFnSc2	supernova
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fotografie	fotografia	k1gFnPc4	fotografia
zbytků	zbytek	k1gInPc2	zbytek
supernovy	supernova	k1gFnSc2	supernova
Cassiopeia	Cassiopeium	k1gNnSc2	Cassiopeium
A	a	k9	a
a	a	k8xC	a
kvasaru	kvasar	k1gInSc2	kvasar
PKS	PKS	kA	PKS
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
752	[number]	k4	752
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
září	září	k1gNnSc2	září
NASA	NASA	kA	NASA
postupně	postupně	k6eAd1	postupně
zveřejňovala	zveřejňovat	k5eAaImAgFnS	zveřejňovat
nové	nový	k2eAgInPc4d1	nový
snímky	snímek	k1gInPc4	snímek
z	z	k7c2	z
Chandry	chandra	k1gFnSc2	chandra
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
chyby	chyba	k1gFnSc2	chyba
ve	v	k7c4	v
směrové	směrový	k2eAgFnPc4d1	směrová
orientace	orientace	k1gFnPc4	orientace
k	k	k7c3	k
dočasnému	dočasný	k2eAgNnSc3d1	dočasné
přerušení	přerušení	k1gNnSc3	přerušení
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc1	týž
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
chybu	chyba	k1gFnSc4	chyba
ve	v	k7c6	v
stabilizačním	stabilizační	k2eAgInSc6d1	stabilizační
systému	systém	k1gInSc6	systém
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
obnovit	obnovit	k5eAaPmF	obnovit
přerušená	přerušený	k2eAgNnPc4d1	přerušené
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
aktivní	aktivní	k2eAgFnSc1d1	aktivní
životnost	životnost	k1gFnSc1	životnost
Chandry	chandra	k1gFnSc2	chandra
se	se	k3xPyFc4	se
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
však	však	k9	však
NASA	NASA	kA	NASA
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
kontrakt	kontrakt	k1gInSc4	kontrakt
se	se	k3xPyFc4	se
Smithsonian	Smithsonian	k1gInSc1	Smithsonian
Astrophysical	Astrophysical	k1gFnSc2	Astrophysical
Observatory	Observator	k1gInPc1	Observator
(	(	kIx(	(
<g/>
SAO	SAO	kA	SAO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistila	zajistit	k5eAaPmAgFnS	zajistit
pokračování	pokračování	k1gNnSc4	pokračování
mise	mise	k1gFnSc2	mise
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dalších	další	k2eAgNnPc2d1	další
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
tj.	tj.	kA	tj.
do	do	k7c2	do
konce	konec	k1gInSc2	konec
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
silné	silný	k2eAgFnSc3d1	silná
sluneční	sluneční	k2eAgFnSc3d1	sluneční
aktivitě	aktivita	k1gFnSc3	aktivita
byla	být	k5eAaImAgFnS	být
vědecká	vědecký	k2eAgFnSc1d1	vědecká
práce	práce	k1gFnSc1	práce
již	již	k6eAd1	již
dvakrát	dvakrát	k6eAd1	dvakrát
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
mezi	mezi	k7c7	mezi
24	[number]	k4	24
<g/>
.	.	kIx.	.
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chandra	chandra	k1gFnSc1	chandra
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
observatoř	observatoř	k1gFnSc1	observatoř
Chandra	chandra	k1gFnSc1	chandra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
mise	mise	k1gFnSc2	mise
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chandra	chandra	k1gFnSc1	chandra
na	na	k7c4	na
Aldebaran	Aldebaran	k1gInSc4	Aldebaran
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chandra	chandra	k1gFnSc1	chandra
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Space-	Space-	k1gMnSc2	Space-
<g/>
40	[number]	k4	40
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chandra	chandra	k1gFnSc1	chandra
X-Ray	X-Raa	k1gFnSc2	X-Raa
Observatory	Observator	k1gInPc1	Observator
(	(	kIx(	(
<g/>
CXO	CXO	kA	CXO
<g/>
)	)	kIx)	)
v	v	k7c6	v
MEK	mek	k0	mek
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Průběh	průběh	k1gInSc1	průběh
letu	let	k1gInSc2	let
STS-93	STS-93	k1gFnSc2	STS-93
v	v	k7c6	v
MEK	mek	k0	mek
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KOSMONAUTICKÉ	kosmonautický	k2eAgFnSc2d1	kosmonautická
AKTUALITY	aktualita	k1gFnSc2	aktualita
-	-	kIx~	-
NEPILOTOVANÉ	pilotovaný	k2eNgInPc1d1	nepilotovaný
LETY	let	k1gInPc1	let
<g/>
:	:	kIx,	:
Observatoř	observatoř	k1gFnSc1	observatoř
pro	pro	k7c4	pro
rentgenovou	rentgenový	k2eAgFnSc4d1	rentgenová
a	a	k8xC	a
gama	gama	k1gNnSc1	gama
astronomii	astronomie	k1gFnSc3	astronomie
Chandra	chandra	k1gFnSc1	chandra
</s>
