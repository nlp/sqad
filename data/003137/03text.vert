<s>
Prase	prase	k1gNnSc1	prase
domácí	domácí	k2eAgFnSc2d1	domácí
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Sus	Sus	k1gMnSc2	Sus
scrofa	scrof	k1gMnSc2	scrof
f.	f.	k?	f.
domestica	domesticus	k1gMnSc2	domesticus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
domácí	domácí	k2eAgNnSc1d1	domácí
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
chované	chovaný	k2eAgNnSc1d1	chované
hospodářsky	hospodářsky	k6eAd1	hospodářsky
především	především	k9	především
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
vyšlechtěné	vyšlechtěný	k2eAgNnSc4d1	vyšlechtěné
z	z	k7c2	z
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgNnSc2d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gFnSc4	Sus
scrofa	scrof	k1gMnSc2	scrof
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
domácí	domácí	k2eAgMnSc1d1	domácí
je	být	k5eAaImIp3nS	být
nepřežvýkavý	přežvýkavý	k2eNgMnSc1d1	nepřežvýkavý
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
prasatovitých	prasatovitý	k2eAgMnPc2d1	prasatovitý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
svému	svůj	k3xOyFgMnSc3	svůj
divokému	divoký	k2eAgMnSc3d1	divoký
předkovi	předek	k1gMnSc3	předek
-	-	kIx~	-
praseti	prase	k1gNnSc3	prase
divokému	divoký	k2eAgNnSc3d1	divoké
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
proporčně	proporčně	k6eAd1	proporčně
delší	dlouhý	k2eAgNnSc4d2	delší
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgInPc1d2	kratší
končetiny	končetina	k1gFnPc4	končetina
a	a	k8xC	a
menší	malý	k2eAgFnSc4d2	menší
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
u	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
prasat	prase	k1gNnPc2	prase
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
hlava	hlava	k1gFnSc1	hlava
v	v	k7c4	v
nápadný	nápadný	k2eAgInSc4d1	nápadný
rypák	rypák	k1gInSc4	rypák
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
šikmé	šikmý	k2eAgFnPc1d1	šikmá
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc1	tvar
boltců	boltec	k1gInPc2	boltec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buďto	buďto	k8xC	buďto
malé	malý	k2eAgNnSc1d1	malé
a	a	k8xC	a
zašpičetělé	zašpičetělý	k2eAgNnSc1d1	zašpičetělý
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgNnSc2d1	divoké
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prasata	prase	k1gNnPc1	prase
přímouchá	přímouchat	k5eAaPmIp3nS	přímouchat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
velké	velký	k2eAgNnSc4d1	velké
<g/>
,	,	kIx,	,
oválné	oválný	k2eAgNnSc4d1	oválné
a	a	k8xC	a
převislé	převislý	k2eAgNnSc4d1	převislé
přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prasata	prase	k1gNnPc1	prase
klapouchá	klapouchat	k5eAaPmIp3nS	klapouchat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
domácích	domácí	k2eAgNnPc2d1	domácí
prasat	prase	k1gNnPc2	prase
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
lysá	lysý	k2eAgFnSc1d1	Lysá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
řídkými	řídký	k2eAgFnPc7d1	řídká
štětinami	štětina	k1gFnPc7	štětina
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
plemen	plemeno	k1gNnPc2	plemeno
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
srst	srst	k1gFnSc1	srst
velmi	velmi	k6eAd1	velmi
hustá	hustý	k2eAgFnSc1d1	hustá
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vlnitá	vlnitý	k2eAgFnSc1d1	vlnitá
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
domácích	domácí	k2eAgNnPc2d1	domácí
prasat	prase	k1gNnPc2	prase
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
růžové	růžový	k2eAgNnSc1d1	růžové
či	či	k8xC	či
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
u	u	k7c2	u
asijských	asijský	k2eAgNnPc2d1	asijské
a	a	k8xC	a
jihoamerických	jihoamerický	k2eAgNnPc2d1	jihoamerické
plemen	plemeno	k1gNnPc2	plemeno
je	být	k5eAaImIp3nS	být
však	však	k9	však
častěji	často	k6eAd2	často
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgFnPc1d1	šedá
nebo	nebo	k8xC	nebo
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
hnědě	hnědě	k6eAd1	hnědě
nebo	nebo	k8xC	nebo
rezavě	rezavě	k6eAd1	rezavě
zbarvená	zbarvený	k2eAgNnPc1d1	zbarvené
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
zbarvením	zbarvení	k1gNnSc7	zbarvení
praseti	prase	k1gNnSc3	prase
divokému	divoký	k2eAgNnSc3d1	divoké
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
tamworthské	tamworthské	k1gNnSc1	tamworthské
nebo	nebo	k8xC	nebo
durocké	durocký	k2eAgNnSc1d1	durocký
prase	prase	k1gNnSc1	prase
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
plemena	plemeno	k1gNnPc1	plemeno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hampshirské	hampshirský	k2eAgNnSc1d1	hampshirský
nebo	nebo	k8xC	nebo
přeštické	přeštický	k2eAgNnSc1d1	přeštické
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
strakatá	strakatý	k2eAgNnPc1d1	strakaté
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgNnPc1d1	jiné
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
pietrainské	pietrainský	k2eAgNnSc1d1	pietrainský
nebo	nebo	k8xC	nebo
gloucesterské	gloucesterský	k2eAgNnSc1d1	gloucesterský
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
skvrnitá	skvrnitý	k2eAgNnPc1d1	skvrnité
<g/>
.	.	kIx.	.
</s>
<s>
Nápadným	nápadný	k2eAgInSc7d1	nápadný
rozdílem	rozdíl	k1gInSc7	rozdíl
oproti	oproti	k7c3	oproti
praseti	prase	k1gNnSc3	prase
divokému	divoký	k2eAgNnSc3d1	divoké
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
ocásku	ocásek	k1gInSc2	ocásek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
zatočený	zatočený	k2eAgInSc1d1	zatočený
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
prasat	prase	k1gNnPc2	prase
domácích	domácí	k2eAgMnPc2d1	domácí
velmi	velmi	k6eAd1	velmi
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
30	[number]	k4	30
kg	kg	kA	kg
až	až	k6eAd1	až
do	do	k7c2	do
400	[number]	k4	400
kg	kg	kA	kg
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
50	[number]	k4	50
cm	cm	kA	cm
až	až	k6eAd1	až
do	do	k7c2	do
250	[number]	k4	250
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
od	od	k7c2	od
35	[number]	k4	35
cm	cm	kA	cm
do	do	k7c2	do
90	[number]	k4	90
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
prase	prase	k1gNnSc1	prase
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
zastřešující	zastřešující	k2eAgInSc1d1	zastřešující
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
však	však	k9	však
pojem	pojem	k1gInSc1	pojem
prase	prase	k1gNnSc4	prase
<g/>
,	,	kIx,	,
prasě	prasě	k1gMnSc2	prasě
používal	používat	k5eAaImAgMnS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
(	(	kIx(	(
<g/>
prosię	prosię	k?	prosię
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinštině	ukrajinština	k1gFnSc6	ukrajinština
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
či	či	k8xC	či
ruštině	ruština	k1gFnSc6	ruština
(	(	kIx(	(
<g/>
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vykastrovaní	vykastrovaný	k2eAgMnPc1d1	vykastrovaný
samci	samec	k1gMnPc1	samec
prasete	prase	k1gNnSc2	prase
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
vepři	vepř	k1gMnPc1	vepř
<g/>
,	,	kIx,	,
nevykastrovaní	vykastrovaný	k2eNgMnPc1d1	nevykastrovaný
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kanci	kanec	k1gMnPc1	kanec
<g/>
.	.	kIx.	.
</s>
<s>
Kanci	kanec	k1gMnPc1	kanec
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
vhodní	vhodný	k2eAgMnPc1d1	vhodný
pro	pro	k7c4	pro
potravinářskou	potravinářský	k2eAgFnSc4d1	potravinářská
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
maso	maso	k1gNnSc1	maso
jistého	jistý	k2eAgNnSc2d1	jisté
procenta	procento	k1gNnSc2	procento
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
páchne	páchnout	k5eAaImIp3nS	páchnout
(	(	kIx(	(
<g/>
zápach	zápach	k1gInSc1	zápach
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
genetické	genetický	k2eAgFnSc6d1	genetická
predispozici	predispozice	k1gFnSc6	predispozice
a	a	k8xC	a
nedaří	dařit	k5eNaImIp3nS	dařit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
eliminovat	eliminovat	k5eAaBmF	eliminovat
selekcí	selekce	k1gFnSc7	selekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prasnice	prasnice	k1gFnSc1	prasnice
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
význam	význam	k1gInSc4	význam
zúžen	zúžen	k2eAgInSc4d1	zúžen
na	na	k7c4	na
samice	samice	k1gFnPc4	samice
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
rodila	rodit	k5eAaImAgFnS	rodit
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
svině	svině	k1gFnSc1	svině
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
významu	význam	k1gInSc6	význam
jako	jako	k8xS	jako
synonymum	synonymum	k1gNnSc1	synonymum
prasnice	prasnice	k1gFnSc2	prasnice
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bachyně	bachyně	k1gFnSc1	bachyně
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
v	v	k7c6	v
říji	říje	k1gFnSc6	říje
se	se	k3xPyFc4	se
bouká	boukat	k5eAaImIp3nS	boukat
(	(	kIx(	(
<g/>
regionálně	regionálně	k6eAd1	regionálně
řouká	řoukat	k5eAaPmIp3nS	řoukat
<g/>
,	,	kIx,	,
houká	houkat	k5eAaImIp3nS	houkat
nebo	nebo	k8xC	nebo
chruje	chrout	k5eAaImIp3nS	chrout
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gravidní	gravidní	k2eAgFnSc1d1	gravidní
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
pojmem	pojem	k1gInSc7	pojem
březí	březí	k1gNnSc2	březí
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
porodila	porodit	k5eAaPmAgFnS	porodit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oprasila	oprasit	k5eAaPmAgFnS	oprasit
<g/>
.	.	kIx.	.
</s>
<s>
Vykastrovaná	vykastrovaný	k2eAgFnSc1d1	vykastrovaná
samice	samice	k1gFnSc1	samice
prasete	prase	k1gNnSc2	prase
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nunva	nunva	k1gFnSc1	nunva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
staročeského	staročeský	k2eAgInSc2d1	staročeský
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
jeptišku	jeptiška	k1gFnSc4	jeptiška
nunvicě	nunvicě	k1gMnSc2	nunvicě
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
angl.	angl.	k?	angl.
nun	nun	k?	nun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sele	sele	k1gNnSc1	sele
nebo	nebo	k8xC	nebo
podsvinče	podsvinče	k1gNnSc1	podsvinče
<g/>
,	,	kIx,	,
po	po	k7c6	po
odstavení	odstavení	k1gNnSc6	odstavení
běhoun	běhoun	k1gMnSc1	běhoun
<g/>
.	.	kIx.	.
</s>
<s>
Jatečné	jatečný	k2eAgNnSc1d1	jatečné
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
chované	chovaný	k2eAgNnSc1d1	chované
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
krmník	krmník	k1gInSc1	krmník
nebo	nebo	k8xC	nebo
pečenáč	pečenáč	k1gInSc1	pečenáč
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
domestikace	domestikace	k1gFnSc1	domestikace
prasat	prase	k1gNnPc2	prase
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Patrně	patrně	k6eAd1	patrně
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
kulturních	kulturní	k2eAgNnPc6d1	kulturní
centrech	centrum	k1gNnPc6	centrum
-	-	kIx~	-
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
nebo	nebo	k8xC	nebo
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
byl	být	k5eAaImAgInS	být
domestikován	domestikován	k2eAgInSc1d1	domestikován
místní	místní	k2eAgInSc1d1	místní
menší	malý	k2eAgInSc1d2	menší
poddruh	poddruh	k1gInSc1	poddruh
-	-	kIx~	-
prasata	prase	k1gNnPc4	prase
páskovaná	páskovaný	k2eAgNnPc4d1	páskované
<g/>
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
vittatus	vittatus	k1gMnSc1	vittatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Úrodného	úrodný	k2eAgInSc2d1	úrodný
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
domestikaci	domestikace	k1gFnSc3	domestikace
větších	veliký	k2eAgInPc2d2	veliký
poddruhů	poddruh	k1gInPc2	poddruh
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgInSc2d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
preferovanou	preferovaný	k2eAgFnSc7d1	preferovaná
barvou	barva	k1gFnSc7	barva
byla	být	k5eAaImAgFnS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
prasata	prase	k1gNnPc1	prase
odedávna	odedávna	k6eAd1	odedávna
chovala	chovat	k5eAaImAgFnS	chovat
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
ohrádkách	ohrádka	k1gFnPc6	ohrádka
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
vykrmovala	vykrmovat	k5eAaImAgFnS	vykrmovat
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
tučnosti	tučnost	k1gFnSc2	tučnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
byla	být	k5eAaImAgFnS	být
chována	chovat	k5eAaImNgFnS	chovat
ve	v	k7c6	v
stádech	stádo	k1gNnPc6	stádo
<g/>
,	,	kIx,	,
hlídaných	hlídaný	k2eAgNnPc6d1	hlídané
pastevci	pastevec	k1gMnPc1	pastevec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
ohradách	ohrada	k1gFnPc6	ohrada
s	s	k7c7	s
přístřešky	přístřešek	k1gInPc7	přístřešek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
popisuje	popisovat	k5eAaImIp3nS	popisovat
například	například	k6eAd1	například
Homérova	Homérův	k2eAgFnSc1d1	Homérova
Odyssea	Odyssea	k1gFnSc1	Odyssea
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
způsob	způsob	k1gInSc1	způsob
chovu	chov	k1gInSc2	chov
byl	být	k5eAaImAgInS	být
uplatňován	uplatňovat	k5eAaImNgInS	uplatňovat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prasata	prase	k1gNnPc1	prase
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
pásla	pásnout	k5eAaImAgFnS	pásnout
volně	volně	k6eAd1	volně
v	v	k7c6	v
lese	les	k1gInSc6	les
a	a	k8xC	a
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
zvířat	zvíře	k1gNnPc2	zvíře
porazila	porazit	k5eAaPmAgFnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ponechaných	ponechaný	k2eAgMnPc2d1	ponechaný
na	na	k7c4	na
chov	chov	k1gInSc4	chov
<g/>
,	,	kIx,	,
přezimovalo	přezimovat	k5eAaBmAgNnS	přezimovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6	v
obydlí	obydlí	k1gNnSc6	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
škodlivosti	škodlivost	k1gFnSc2	škodlivost
prasat	prase	k1gNnPc2	prase
v	v	k7c6	v
lese	les	k1gInSc6	les
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
volná	volný	k2eAgFnSc1d1	volná
pastva	pastva	k1gFnSc1	pastva
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
vrchností	vrchnost	k1gFnPc2	vrchnost
omezována	omezován	k2eAgFnSc1d1	omezována
a	a	k8xC	a
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc1	století
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
prasata	prase	k1gNnPc1	prase
chovat	chovat	k5eAaImF	chovat
spíše	spíše	k9	spíše
ve	v	k7c6	v
chlévech	chlév	k1gInPc6	chlév
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vykrmovala	vykrmovat	k5eAaImAgFnS	vykrmovat
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
tučnosti	tučnost	k1gFnSc2	tučnost
<g/>
.	.	kIx.	.
</s>
<s>
Krmila	krmit	k5eAaImAgFnS	krmit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
kuchyňskými	kuchyňský	k2eAgInPc7d1	kuchyňský
zbytky	zbytek	k1gInPc7	zbytek
a	a	k8xC	a
pomyjemi	pomyje	k1gFnPc7	pomyje
<g/>
,	,	kIx,	,
žaludy	žalud	k1gInPc7	žalud
nebo	nebo	k8xC	nebo
šrotem	šrot	k1gInSc7	šrot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mlátem	mláto	k1gNnSc7	mláto
a	a	k8xC	a
kozím	kozí	k2eAgNnSc7d1	kozí
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Chlévy	chlév	k1gInPc1	chlév
byly	být	k5eAaImAgFnP	být
zpravidla	zpravidla	k6eAd1	zpravidla
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnPc1d1	tmavá
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
větrané	větraný	k2eAgInPc1d1	větraný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ani	ani	k9	ani
nekydal	kydat	k5eNaImAgMnS	kydat
hnůj	hnůj	k1gInSc4	hnůj
-	-	kIx~	-
prase	prase	k1gNnSc1	prase
tak	tak	k6eAd1	tak
žilo	žít	k5eAaImAgNnS	žít
vlastně	vlastně	k9	vlastně
uvězněné	uvězněný	k2eAgNnSc1d1	uvězněné
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
hnoji	hnůj	k1gInSc6	hnůj
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
předsudky	předsudek	k1gInPc1	předsudek
o	o	k7c6	o
prasečí	prasečí	k2eAgFnSc6d1	prasečí
špinavosti	špinavost	k1gFnSc6	špinavost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soukromých	soukromý	k2eAgInPc6d1	soukromý
chovech	chov	k1gInPc6	chov
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
ustájení	ustájení	k1gNnSc2	ustájení
ještě	ještě	k9	ještě
přežívá	přežívat	k5eAaImIp3nS	přežívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
chovatelů	chovatel	k1gMnPc2	chovatel
přece	přece	k9	přece
jen	jen	k9	jen
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
dbá	dbát	k5eAaImIp3nS	dbát
o	o	k7c4	o
hygienu	hygiena	k1gFnSc4	hygiena
ustájení	ustájení	k1gNnSc2	ustájení
a	a	k8xC	a
hnůj	hnůj	k1gInSc1	hnůj
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
prasata	prase	k1gNnPc4	prase
dosud	dosud	k6eAd1	dosud
chována	chovat	k5eAaImNgNnP	chovat
extenzivně	extenzivně	k6eAd1	extenzivně
<g/>
,	,	kIx,	,
buďto	buďto	k8xC	buďto
ve	v	k7c6	v
výbězích	výběh	k1gInPc6	výběh
<g/>
,	,	kIx,	,
vybavených	vybavený	k2eAgFnPc6d1	vybavená
boudou	bouda	k1gFnSc7	bouda
a	a	k8xC	a
kalištěm	kaliště	k1gNnSc7	kaliště
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
stádech	stádo	k1gNnPc6	stádo
<g/>
,	,	kIx,	,
hlídaných	hlídaný	k2eAgNnPc6d1	hlídané
pastevci	pastevec	k1gMnPc1	pastevec
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
jsou	být	k5eAaImIp3nP	být
prasata	prase	k1gNnPc1	prase
chována	chován	k2eAgNnPc1d1	chováno
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
trendy	trend	k1gInPc7	trend
ekologického	ekologický	k2eAgNnSc2d1	ekologické
zemědělství	zemědělství	k1gNnSc2	zemědělství
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
i	i	k9	i
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc1	prase
z	z	k7c2	z
ekologického	ekologický	k2eAgInSc2d1	ekologický
chovu	chov	k1gInSc2	chov
jsou	být	k5eAaImIp3nP	být
zdravější	zdravý	k2eAgMnPc1d2	zdravější
<g/>
,	,	kIx,	,
netrpí	trpět	k5eNaImIp3nP	trpět
stresem	stres	k1gInSc7	stres
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
mnohem	mnohem	k6eAd1	mnohem
chutnější	chutný	k2eAgNnSc4d2	chutnější
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
z	z	k7c2	z
velkochovů	velkochov	k1gInPc2	velkochov
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
chov	chov	k1gInSc1	chov
prasat	prase	k1gNnPc2	prase
ve	v	k7c6	v
velkokapacitních	velkokapacitní	k2eAgInPc6d1	velkokapacitní
vepřínech	vepřín	k1gInPc6	vepřín
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
nejsou	být	k5eNaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
jejich	jejich	k3xOp3gFnPc4	jejich
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
příliš	příliš	k6eAd1	příliš
uspokojivé	uspokojivý	k2eAgFnPc1d1	uspokojivá
<g/>
.	.	kIx.	.
</s>
<s>
Vepříny	vepřín	k1gInPc1	vepřín
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
odchovny	odchovna	k1gFnPc4	odchovna
selat	sele	k1gNnPc2	sele
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
prasata	prase	k1gNnPc1	prase
rozmnožována	rozmnožován	k2eAgNnPc1d1	rozmnožováno
<g/>
,	,	kIx,	,
a	a	k8xC	a
výkrmny	výkrmna	k1gFnPc1	výkrmna
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
výkrmu	výkrm	k1gInSc3	výkrm
jatečných	jatečný	k2eAgFnPc2d1	jatečná
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
jsou	být	k5eAaImIp3nP	být
prasata	prase	k1gNnPc1	prase
ustájena	ustájen	k2eAgNnPc1d1	ustájeno
v	v	k7c6	v
kotcích	kotec	k1gInPc6	kotec
s	s	k7c7	s
roštovou	roštový	k2eAgFnSc7d1	roštová
podlahou	podlaha	k1gFnSc7	podlaha
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
kusů	kus	k1gInPc2	kus
stejné	stejný	k2eAgFnSc2d1	stejná
věkové	věkový	k2eAgFnSc2d1	věková
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
trpí	trpět	k5eAaImIp3nP	trpět
stresem	stres	k1gInSc7	stres
<g/>
.	.	kIx.	.
</s>
<s>
Vepřín	vepřín	k1gInSc1	vepřín
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
venkovní	venkovní	k2eAgInPc4d1	venkovní
výběhy	výběh	k1gInPc4	výběh
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
plemenných	plemenný	k2eAgNnPc2d1	plemenné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
prasat	prase	k1gNnPc2	prase
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
chováno	chovat	k5eAaImNgNnS	chovat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
produkce	produkce	k1gFnSc1	produkce
prasat	prase	k1gNnPc2	prase
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
chovalo	chovat	k5eAaImAgNnS	chovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4.001.000	[number]	k4	4.001.000
kusů	kus	k1gInPc2	kus
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
876	[number]	k4	876
834	[number]	k4	834
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stav	stav	k1gInSc4	stav
chovaných	chovaný	k2eAgNnPc2d1	chované
prasat	prase	k1gNnPc2	prase
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
dále	daleko	k6eAd2	daleko
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
prasata	prase	k1gNnPc1	prase
nejpočetněji	početně	k6eAd3	početně
chovanými	chovaný	k2eAgFnPc7d1	chovaná
velkými	velký	k2eAgFnPc7d1	velká
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
zvířaty	zvíře	k1gNnPc7	zvíře
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
i	i	k9	i
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
vepřového	vepřový	k2eAgNnSc2d1	vepřové
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
domácí	domácí	k2eAgMnSc1d1	domácí
je	být	k5eAaImIp3nS	být
chováno	chován	k2eAgNnSc1d1	chováno
především	především	k9	především
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prasat	prase	k1gNnPc2	prase
proto	proto	k8xC	proto
nepřežije	přežít	k5eNaPmIp3nS	přežít
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
poražena	poražen	k2eAgNnPc1d1	poraženo
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
stanovené	stanovený	k2eAgFnSc3d1	stanovená
porážkové	porážkový	k2eAgFnSc3d1	porážková
hmotnosti	hmotnost	k1gFnSc3	hmotnost
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
této	tento	k3xDgFnSc2	tento
hmotnosti	hmotnost	k1gFnSc2	hmotnost
již	již	k6eAd1	již
prase	prase	k1gNnSc1	prase
neroste	růst	k5eNaImIp3nS	růst
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
tuční	tučnět	k5eAaImIp3nS	tučnět
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
chovaná	chovaný	k2eAgNnPc1d1	chované
pro	pro	k7c4	pro
sádlo	sádlo	k1gNnSc4	sádlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
porážejí	porážet	k5eAaImIp3nP	porážet
až	až	k9	až
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
až	až	k9	až
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jednoho	jeden	k4xCgNnSc2	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Déle	dlouho	k6eAd2	dlouho
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nechávají	nechávat	k5eAaImIp3nP	nechávat
žít	žít	k5eAaImF	žít
také	také	k9	také
chovné	chovný	k2eAgFnPc1d1	chovná
prasnice	prasnice	k1gFnPc1	prasnice
a	a	k8xC	a
plemenní	plemenný	k2eAgMnPc1d1	plemenný
kanci	kanec	k1gMnPc1	kanec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
sádla	sádlo	k1gNnSc2	sádlo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
vepřová	vepřový	k2eAgFnSc1d1	vepřová
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
krupon	krupon	k1gInSc1	krupon
<g/>
,	,	kIx,	,
štětiny	štětina	k1gFnPc1	štětina
a	a	k8xC	a
prasečí	prasečí	k2eAgInSc1d1	prasečí
hnůj	hnůj	k1gInSc1	hnůj
či	či	k8xC	či
kejda	kejda	k1gFnSc1	kejda
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc1	prase
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
využívána	využíván	k2eAgNnPc1d1	využíváno
jako	jako	k8xC	jako
pracovní	pracovní	k2eAgNnPc1d1	pracovní
zvířata	zvíře	k1gNnPc1	zvíře
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
lanýžů	lanýž	k1gInPc2	lanýž
<g/>
,	,	kIx,	,
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
se	se	k3xPyFc4	se
prasata	prase	k1gNnPc1	prase
používala	používat	k5eAaImAgNnP	používat
také	také	k9	také
ke	k	k7c3	k
zneškodňování	zneškodňování	k1gNnSc3	zneškodňování
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Pro	pro	k7c4	pro
podobnost	podobnost	k1gFnSc4	podobnost
svých	svůj	k3xOyFgInPc2	svůj
orgánů	orgán	k1gInPc2	orgán
s	s	k7c7	s
lidskými	lidský	k2eAgMnPc7d1	lidský
je	být	k5eAaImIp3nS	být
prase	prase	k1gNnSc1	prase
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejpoužívanějších	používaný	k2eAgNnPc2d3	nejpoužívanější
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Zakrslá	zakrslý	k2eAgNnPc4d1	zakrslé
plemena	plemeno	k1gNnPc4	plemeno
prasat	prase	k1gNnPc2	prase
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
někdy	někdy	k6eAd1	někdy
chovají	chovat	k5eAaImIp3nP	chovat
také	také	k9	také
jako	jako	k9	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
domácí	domácí	k2eAgMnSc1d1	domácí
je	být	k5eAaImIp3nS	být
všežravé	všežravý	k2eAgNnSc1d1	všežravé
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
se	se	k3xPyFc4	se
prasata	prase	k1gNnPc1	prase
krmí	krmit	k5eAaImIp3nP	krmit
převážně	převážně	k6eAd1	převážně
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběnými	vyráběný	k2eAgFnPc7d1	vyráběná
granulemi	granule	k1gFnPc7	granule
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
používaná	používaný	k2eAgNnPc4d1	používané
krmiva	krmivo	k1gNnPc4	krmivo
patří	patřit	k5eAaImIp3nP	patřit
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
obilný	obilný	k2eAgInSc4d1	obilný
šrot	šrot	k1gInSc4	šrot
a	a	k8xC	a
kuchyňské	kuchyňský	k2eAgInPc4d1	kuchyňský
odpadky	odpadek	k1gInPc4	odpadek
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc1	prase
domácí	domácí	k2eAgNnPc1d1	domácí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
uniknou	uniknout	k5eAaPmIp3nP	uniknout
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
zdivočí	zdivočet	k5eAaPmIp3nP	zdivočet
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
rychle	rychle	k6eAd1	rychle
přibývá	přibývat	k5eAaImIp3nS	přibývat
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
plodné	plodný	k2eAgNnSc1d1	plodné
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
mívá	mívat	k5eAaImIp3nS	mívat
8	[number]	k4	8
až	až	k9	až
14	[number]	k4	14
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
mívá	mívat	k5eAaImIp3nS	mívat
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
cca	cca	kA	cca
6-12	[number]	k4	6-12
selat	sele	k1gNnPc2	sele
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
vrhu	vrh	k1gInSc6	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
samic	samice	k1gFnPc2	samice
své	svůj	k3xOyFgFnSc2	svůj
mláďata	mládě	k1gNnPc4	mládě
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
prasnice	prasnice	k1gFnSc2	prasnice
neolíže	olízat	k5eNaPmIp3nS	olízat
<g/>
.	.	kIx.	.
</s>
<s>
Selata	sele	k1gNnPc1	sele
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
předníma	přední	k2eAgFnPc7d1	přední
nebo	nebo	k8xC	nebo
zadníma	zadní	k2eAgFnPc7d1	zadní
nohama	noha	k1gFnPc7	noha
napřed	napřed	k6eAd1	napřed
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
se	se	k3xPyFc4	se
postaví	postavit	k5eAaPmIp3nP	postavit
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
bradavky	bradavka	k1gFnPc1	bradavka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
pít	pít	k5eAaImF	pít
mateřské	mateřský	k2eAgNnSc4d1	mateřské
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
přetrhne	přetrhnout	k5eAaPmIp3nS	přetrhnout
pupeční	pupeční	k2eAgFnSc1d1	pupeční
šňůra	šňůra	k1gFnSc1	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prasnice	prasnice	k1gFnSc1	prasnice
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
selata	sele	k1gNnPc4	sele
zalehne	zalehnout	k5eAaPmIp3nS	zalehnout
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
sežere	sežrat	k5eAaPmIp3nS	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
péči	péče	k1gFnSc3	péče
o	o	k7c4	o
tělo	tělo	k1gNnSc4	tělo
patří	patřit	k5eAaImIp3nS	patřit
bahenní	bahenní	k2eAgFnSc1d1	bahenní
koupel	koupel	k1gFnSc1	koupel
(	(	kIx(	(
<g/>
vyválení	vyválení	k1gNnSc1	vyválení
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc4	ten
tato	tento	k3xDgFnSc1	tento
koupel	koupel	k1gFnSc1	koupel
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
prasata	prase	k1gNnPc4	prase
ochladí	ochladit	k5eAaPmIp3nP	ochladit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemají	mít	k5eNaImIp3nP	mít
potní	potní	k2eAgFnPc4d1	potní
žlázy	žláza	k1gFnPc4	žláza
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
potit	potit	k5eAaImF	potit
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
si	se	k3xPyFc3	se
otřou	otřít	k5eAaPmIp3nP	otřít
bahno	bahno	k1gNnSc4	bahno
o	o	k7c4	o
nějaký	nějaký	k3yIgInSc4	nějaký
pařez	pařez	k1gInSc4	pařez
nebo	nebo	k8xC	nebo
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
dotěrných	dotěrný	k2eAgMnPc2d1	dotěrný
parazitů	parazit	k1gMnPc2	parazit
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
klíšťata	klíště	k1gNnPc1	klíště
<g/>
,	,	kIx,	,
blechy	blecha	k1gFnPc1	blecha
a	a	k8xC	a
komáři	komár	k1gMnPc1	komár
<g/>
.	.	kIx.	.
</s>
<s>
Prasata	prase	k1gNnPc1	prase
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
velice	velice	k6eAd1	velice
čistotná	čistotný	k2eAgNnPc1d1	čistotné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
vydává	vydávat	k5eAaImIp3nS	vydávat
zvuky	zvuk	k1gInPc4	zvuk
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
chrochtání	chrochtání	k1gNnSc1	chrochtání
nebo	nebo	k8xC	nebo
kvičení	kvičení	k1gNnSc1	kvičení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
porostlé	porostlý	k2eAgNnSc1d1	porostlé
štětinami	štětina	k1gFnPc7	štětina
<g/>
.	.	kIx.	.
</s>
<s>
Chodí	chodit	k5eAaImIp3nP	chodit
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
paznehtech	pazneht	k1gInPc6	pazneht
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bývá	bývat	k5eAaImIp3nS	bývat
prase	prase	k1gNnSc1	prase
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
lenosti	lenost	k1gFnSc2	lenost
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
17,5	[number]	k4	17,5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
prase	prase	k1gNnSc1	prase
symbolem	symbol	k1gInSc7	symbol
štěstí	štěstit	k5eAaImIp3nS	štěstit
a	a	k8xC	a
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
antiky	antika	k1gFnSc2	antika
bylo	být	k5eAaImAgNnS	být
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
obětním	obětní	k2eAgNnSc7d1	obětní
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
spojeným	spojený	k2eAgNnSc7d1	spojené
s	s	k7c7	s
plodností	plodnost	k1gFnPc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
prasata	prase	k1gNnPc1	prase
obětovala	obětovat	k5eAaBmAgNnP	obětovat
i	i	k9	i
při	při	k7c6	při
Eleusínských	Eleusínský	k2eAgFnPc6d1	Eleusínský
mystériích	mystérie	k1gFnPc6	mystérie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
symbolem	symbol	k1gInSc7	symbol
lenosti	lenost	k1gFnSc2	lenost
a	a	k8xC	a
obžerství	obžerství	k1gNnSc2	obžerství
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc2	dva
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
světci	světec	k1gMnPc7	světec
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
Antonínem	Antonín	k1gMnSc7	Antonín
a	a	k8xC	a
sv.	sv.	kA	sv.
Vendelínem	Vendelín	k1gMnSc7	Vendelín
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
zároveň	zároveň	k6eAd1	zároveň
uctíváni	uctíván	k2eAgMnPc1d1	uctíván
jako	jako	k9	jako
ochránci	ochránce	k1gMnPc1	ochránce
před	před	k7c7	před
nemocemi	nemoc	k1gFnPc7	nemoc
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
středověku	středověk	k1gInSc2	středověk
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
satira	satira	k1gFnSc1	satira
o	o	k7c6	o
vepříkově	vepříkův	k2eAgFnSc6d1	vepříkův
závěti	závěť	k1gFnSc6	závěť
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
učinil	učinit	k5eAaImAgMnS	učinit
před	před	k7c7	před
zabíjačkou	zabíjačka	k1gFnSc7	zabíjačka
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
mají	mít	k5eAaImIp3nP	mít
konzumaci	konzumace	k1gFnSc4	konzumace
vepřového	vepřový	k2eAgNnSc2d1	vepřové
masa	maso	k1gNnSc2	maso
zakázanou	zakázaný	k2eAgFnSc7d1	zakázaná
náboženskými	náboženský	k2eAgInPc7d1	náboženský
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc4	prase
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
nečisté	čistý	k2eNgNnSc4d1	nečisté
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákaz	zákaz	k1gInSc1	zákaz
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
racionální	racionální	k2eAgInSc4d1	racionální
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
konzumentů	konzument	k1gMnPc2	konzument
před	před	k7c7	před
napadením	napadení	k1gNnSc7	napadení
parazitickým	parazitický	k2eAgMnSc7d1	parazitický
svalovcem	svalovec	k1gMnSc7	svalovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
prasata	prase	k1gNnPc1	prase
chována	chovat	k5eAaImNgNnP	chovat
volně	volně	k6eAd1	volně
a	a	k8xC	a
maso	maso	k1gNnSc1	maso
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
jatkách	jatka	k1gFnPc6	jatka
prohlíženo	prohlížen	k2eAgNnSc1d1	prohlíženo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nákazy	nákaza	k1gFnPc4	nákaza
svalovcem	svalovec	k1gMnSc7	svalovec
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgFnSc2d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
také	také	k9	také
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zakrslá	zakrslý	k2eAgNnPc4d1	zakrslé
plemena	plemeno	k1gNnPc4	plemeno
prasat	prase	k1gNnPc2	prase
chována	chován	k2eAgFnSc1d1	chována
jako	jako	k8xC	jako
domácí	domácí	k2eAgMnSc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Číňany	Číňan	k1gMnPc4	Číňan
a	a	k8xC	a
Japonce	Japonec	k1gMnPc4	Japonec
má	mít	k5eAaImIp3nS	mít
prase	prase	k1gNnSc1	prase
i	i	k8xC	i
symbolický	symbolický	k2eAgInSc1d1	symbolický
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dvanáctým	dvanáctý	k4xOgNnSc7	dvanáctý
zvířetem	zvíře	k1gNnSc7	zvíře
čínského	čínský	k2eAgInSc2d1	čínský
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
i	i	k9	i
zde	zde	k6eAd1	zde
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
blahobytem	blahobyt	k1gInSc7	blahobyt
<g/>
,	,	kIx,	,
štěstím	štěstí	k1gNnSc7	štěstí
a	a	k8xC	a
oslavami	oslava	k1gFnPc7	oslava
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Oslovení	oslovení	k1gNnSc1	oslovení
"	"	kIx"	"
<g/>
prase	prase	k1gNnSc1	prase
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
už	už	k6eAd1	už
dlouho	dlouho	k6eAd1	dlouho
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
nadávka	nadávka	k1gFnSc1	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
tak	tak	k9	tak
označován	označován	k2eAgMnSc1d1	označován
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nečistotný	čistotný	k2eNgMnSc1d1	nečistotný
<g/>
,	,	kIx,	,
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nevhodně	vhodně	k6eNd1	vhodně
chová	chovat	k5eAaImIp3nS	chovat
(	(	kIx(	(
<g/>
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
podoby	podoba	k1gFnSc2	podoba
chovu	chov	k1gInSc2	chov
způsobené	způsobený	k2eAgNnSc1d1	způsobené
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
jsou	být	k5eAaImIp3nP	být
jinak	jinak	k6eAd1	jinak
velice	velice	k6eAd1	velice
čistotná	čistotný	k2eAgNnPc4d1	čistotné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
svině	svině	k1gFnSc1	svině
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
ještě	ještě	k6eAd1	ještě
silnější	silný	k2eAgFnSc1d2	silnější
nadávka	nadávka	k1gFnSc1	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
tak	tak	k9	tak
označován	označován	k2eAgMnSc1d1	označován
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zákeřný	zákeřný	k2eAgMnSc1d1	zákeřný
nebo	nebo	k8xC	nebo
záměrně	záměrně	k6eAd1	záměrně
škodí	škodit	k5eAaImIp3nS	škodit
jiným	jiný	k2eAgMnPc3d1	jiný
lidem	člověk	k1gMnPc3	člověk
(	(	kIx(	(
<g/>
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kdy	kdy	k6eAd1	kdy
bachyně	bachyně	k1gFnSc2	bachyně
<g/>
/	/	kIx~	/
<g/>
svině	svině	k1gFnSc1	svině
má	mít	k5eAaImIp3nS	mít
zrovna	zrovna	k9	zrovna
podsvinčata	podsvinče	k1gNnPc4	podsvinče
<g/>
/	/	kIx~	/
<g/>
selata	sele	k1gNnPc4	sele
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
bachyně	bachyně	k1gFnSc2	bachyně
<g/>
/	/	kIx~	/
<g/>
svině	svině	k1gFnSc1	svině
nevypočitatelná	vypočitatelný	k2eNgFnSc1d1	nevypočitatelná
a	a	k8xC	a
zákeřná	zákeřný	k2eAgFnSc1d1	zákeřná
→	→	k?	→
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
mláďata	mládě	k1gNnPc4	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
původně	původně	k6eAd1	původně
žilo	žít	k5eAaImAgNnS	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
transportováno	transportovat	k5eAaBmNgNnS	transportovat
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
