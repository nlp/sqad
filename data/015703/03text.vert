<s>
Alexej	Alexej	k1gMnSc1
Leonťjev	Leonťjev	k1gMnSc1
</s>
<s>
Alexej	Alexej	k1gMnSc1
Leonťjev	Leonťjev	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
jul	jul	k?
<g/>
.	.	kIx.
/	/	kIx~
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1903	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
Moskva	Moskva	k1gFnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1979	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Moskva	Moskva	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
infarkt	infarkt	k1gInSc4
myokardu	myokard	k1gInSc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Kuncevský	Kuncevský	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Faculty	Facult	k1gInPc1
of	of	k?
Social	Social	k1gInSc1
Sciences	Sciences	k1gMnSc1
of	of	k?
Moscow	Moscow	k1gMnSc1
State	status	k1gInSc5
University	universita	k1gFnSc2
Povolání	povolání	k1gNnSc6
</s>
<s>
psycholog	psycholog	k1gMnSc1
<g/>
,	,	kIx,
akademik	akademik	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
literatury	literatura	k1gFnSc2
faktu	fakt	k1gInSc2
a	a	k8xC
filozof	filozof	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Lomonosovova	Lomonosovův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Leninova	Leninův	k2eAgInSc2d1
cenaLeninův	cenaLeninův	k2eAgInSc4d1
řádŘád	řádŘáda	k1gFnPc2
rudého	rudý	k2eAgInSc2d1
praporu	prapor	k1gInSc2
práceOdznak	práceOdznak	k6eAd1
ctiUshinsky	ctiUshinsky	k6eAd1
Medal	Medal	k1gInSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Alexej	Alexej	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Leonťjev	Leonťjev	k1gMnSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
А	А	k?
<g/>
́	́	k?
<g/>
й	й	k?
Н	Н	k?
<g/>
́	́	k?
<g/>
е	е	k?
Л	Л	k?
<g/>
́	́	k?
<g/>
н	н	k?
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1903	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1979	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sovětský	sovětský	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitel	představitel	k1gMnSc1
vývojové	vývojový	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žák	Žák	k1gMnSc1
Lva	lev	k1gInSc2
Vygotského	Vygotský	k2eAgInSc2d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
kulturně-historickou	kulturně-historický	k2eAgFnSc4d1
psychologii	psychologie	k1gFnSc4
pozvolna	pozvolna	k6eAd1
opouštěl	opouštět	k5eAaImAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
osobité	osobitý	k2eAgNnSc1d1
"	"	kIx"
<g/>
teorie	teorie	k1gFnSc1
aktivity	aktivita	k1gFnSc2
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
po	po	k7c6
Leonťjevově	Leonťjevův	k2eAgInSc6d1
příchodu	příchod	k1gInSc6
na	na	k7c4
katedru	katedra	k1gFnSc4
psychologie	psychologie	k1gFnSc2
Lomonosovovy	Lomonosovův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
rektorem	rektor	k1gMnSc7
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
Fakulty	fakulta	k1gFnSc2
psychologie	psychologie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
nejvlivnějším	vlivný	k2eAgInSc7d3
směrem	směr	k1gInSc7
v	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
psychologii	psychologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
aktivity	aktivita	k1gFnSc2
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
lidská	lidský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
se	se	k3xPyFc4
hodnotí	hodnotit	k5eAaImIp3nS
na	na	k7c6
třech	tři	k4xCgFnPc6
úrovních	úroveň	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
Leonťjeva	Leonťjevo	k1gNnSc2
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
psychologických	psychologický	k2eAgFnPc2d1
škol	škola	k1gFnPc2
směšovány	směšován	k2eAgFnPc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
činnost	činnost	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gInPc4
motivy	motiv	k1gInPc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
akce	akce	k1gFnPc4
a	a	k8xC
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
operace	operace	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
prostředky	prostředek	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
П	П	k?
в	в	k?
с	с	k?
у	у	k?
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Д	Д	k?
<g/>
.	.	kIx.
С	С	k?
<g/>
.	.	kIx.
Л	Л	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
В	В	k?
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
П	П	k?
д	д	k?
в	в	k?
и	и	k?
с	с	k?
п	п	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Л	Л	k?
п	п	k?
о	о	k?
п	п	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
</s>
<s>
Rozumový	rozumový	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Dědictví	dědictví	k1gNnSc3
Komenského	Komenského	k2eAgNnSc2d1
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reforma	reforma	k1gFnSc1
školy	škola	k1gFnSc2
a	a	k8xC
úkoly	úkol	k1gInPc4
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Výzkumný	výzkumný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pedagogický	pedagogický	k2eAgInSc1d1
1959	#num#	k4
</s>
<s>
Problémy	problém	k1gInPc1
psychického	psychický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
vědomí	vědomí	k1gNnSc1
<g/>
,	,	kIx,
osobnost	osobnost	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Svboboda	Svboboda	k1gFnSc1
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Myšlenkové	myšlenkový	k2eAgInPc1d1
procesy	proces	k1gInPc1
při	při	k7c6
osvojování	osvojování	k1gNnSc6
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
:	:	kIx,
řízení	řízení	k1gNnSc2
procesu	proces	k1gInSc2
osvojování	osvojování	k1gNnSc4
cizího	cizí	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
ze	z	k7c2
statí	stať	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Krajský	krajský	k2eAgInSc4d1
pedagogický	pedagogický	k2eAgInSc4d1
ústav	ústav	k1gInSc4
1981	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.igs.net	www.igs.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Veer	Veer	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
v.D.	v.D.	k?
and	and	k?
Valsiner	Valsiner	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Understanding	Understanding	k1gInSc1
Vygotsky	Vygotsky	k1gFnSc1
<g/>
:	:	kIx,
a	a	k8xC
quest	quest	k1gFnSc1
for	forum	k1gNnPc2
synthesis	synthesis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Blackwell	Blackwell	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Yasnitsky	Yasnitsky	k6eAd1
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vygotsky	Vygotsky	k1gFnSc1
Circle	Circle	k1gNnSc2
as	as	k1gNnPc2
a	a	k8xC
Personal	Personal	k1gFnPc3
Network	network	k1gInSc1
of	of	k?
Scholars	Scholars	k1gInSc1
<g/>
:	:	kIx,
Restoring	Restoring	k1gInSc1
Connections	Connectionsa	k1gFnPc2
Between	Between	k2eAgMnSc1d1
People	People	k1gMnSc1
and	and	k?
Ideas	Ideas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrative	Integrativ	k1gInSc5
Psychological	Psychological	k1gMnPc4
and	and	k?
Behavioral	Behavioral	k1gFnSc1
Science	Science	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Fakulty	fakulta	k1gFnSc2
psychologie	psychologie	k1gFnSc2
Lomonosovovy	Lomonosovův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c4
Marxists	Marxists	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kup	kup	k1gInSc1
<g/>
19960000056975	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119171120	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1034	#num#	k4
6230	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82047708	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131377	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82047708	#num#	k4
</s>
