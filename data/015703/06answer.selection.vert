<s desamb="1">
Žák	Žák	k1gMnSc1
Lva	lev	k1gInSc2
Vygotského	Vygotský	k2eAgInSc2d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
kulturně-historickou	kulturně-historický	k2eAgFnSc4d1
psychologii	psychologie	k1gFnSc4
pozvolna	pozvolna	k6eAd1
opouštěl	opouštět	k5eAaImAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
osobité	osobitý	k2eAgNnSc1d1
"	"	kIx"
<g/>
teorie	teorie	k1gFnSc1
aktivity	aktivita	k1gFnSc2
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
po	po	k7c6
Leonťjevově	Leonťjevův	k2eAgInSc6d1
příchodu	příchod	k1gInSc6
na	na	k7c4
katedru	katedra	k1gFnSc4
psychologie	psychologie	k1gFnSc2
Lomonosovovy	Lomonosovův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
rektorem	rektor	k1gMnSc7
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
Fakulty	fakulta	k1gFnSc2
psychologie	psychologie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
nejvlivnějším	vlivný	k2eAgInSc7d3
směrem	směr	k1gInSc7
v	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
psychologii	psychologie	k1gFnSc6
<g/>
.	.	kIx.
</s>