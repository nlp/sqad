<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stmívání	stmívání	k1gNnSc2	stmívání
(	(	kIx(	(
<g/>
filmová	filmový	k2eAgFnSc1d1	filmová
série	série	k1gFnSc1	série
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stmívání	stmívání	k1gNnSc1	stmívání
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Twilight	Twilight	k1gMnSc1	Twilight
Saga	Saga	k1gMnSc1	Saga
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
čtyř	čtyři	k4xCgInPc2	čtyři
románů	román	k1gInPc2	román
s	s	k7c7	s
upíří	upíří	k2eAgFnSc7d1	upíří
tematikou	tematika	k1gFnSc7	tematika
americké	americký	k2eAgFnSc2d1	americká
autorky	autorka	k1gFnSc2	autorka
Stephenie	Stephenie	k1gFnSc2	Stephenie
Meyerové	Meyerová	k1gFnSc2	Meyerová
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
romány	román	k1gInPc1	román
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
období	období	k1gNnSc4	období
v	v	k7c6	v
životě	život	k1gInSc6	život
Isabelly	Isabella	k1gFnSc2	Isabella
"	"	kIx"	"
<g/>
Belly	bell	k1gInPc1	bell
<g/>
"	"	kIx"	"
Swanové	Swanová	k1gFnPc1	Swanová
<g/>
,	,	kIx,	,
dospívající	dospívající	k2eAgFnPc1d1	dospívající
dívky	dívka	k1gFnPc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
z	z	k7c2	z
Arizony	Arizona	k1gFnSc2	Arizona
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Forks	Forksa	k1gFnPc2	Forksa
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
Charliemu	Charlie	k1gMnSc3	Charlie
Swanovi	Swan	k1gMnSc3	Swan
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
škole	škola	k1gFnSc6	škola
najde	najít	k5eAaPmIp3nS	najít
spoustu	spousta	k1gFnSc4	spousta
přátel	přítel	k1gMnPc2	přítel
nejvíce	hodně	k6eAd3	hodně
ji	on	k3xPp3gFnSc4	on
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
Edward	Edward	k1gMnSc1	Edward
Cullen	Cullen	k2eAgMnSc1d1	Cullen
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jí	on	k3xPp3gFnSc7	on
přátelé	přítel	k1gMnPc1	přítel
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodina	rodina	k1gFnSc1	rodina
Cullenových	Cullenová	k1gFnPc2	Cullenová
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
Bella	Bella	k1gMnSc1	Bella
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Edwarda	Edward	k1gMnSc2	Edward
velmi	velmi	k6eAd1	velmi
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
si	se	k3xPyFc3	se
všimne	všimnout	k5eAaPmIp3nS	všimnout
na	na	k7c6	na
Edwardovi	Edward	k1gMnSc6	Edward
jeho	jeho	k3xOp3gFnSc7	jeho
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gFnSc1	Bella
je	být	k5eAaImIp3nS	být
zvědavá	zvědavý	k2eAgFnSc1d1	zvědavá
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
tak	tak	k6eAd1	tak
pátrá	pátrat	k5eAaImIp3nS	pátrat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dopátrá	dopátrat	k5eAaPmIp3nS	dopátrat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Edward	Edward	k1gMnSc1	Edward
upír	upír	k1gMnSc1	upír
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
strach	strach	k1gInSc4	strach
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
aby	aby	k9	aby
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
taky	taky	k9	taky
udělal	udělat	k5eAaPmAgInS	udělat
upíra	upír	k1gMnSc4	upír
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
valné	valný	k2eAgFnSc2d1	valná
části	část	k1gFnSc2	část
napsán	napsat	k5eAaBmNgInS	napsat
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
dospívající	dospívající	k2eAgFnSc2d1	dospívající
Isabelly	Isabella	k1gFnSc2	Isabella
(	(	kIx(	(
<g/>
Belly	bell	k1gInPc1	bell
<g/>
)	)	kIx)	)
Swanové	Swanová	k1gFnPc1	Swanová
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
třetí	třetí	k4xOgFnSc2	třetí
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vloženy	vložen	k2eAgFnPc1d1	vložena
části	část	k1gFnPc1	část
vyprávění	vyprávění	k1gNnSc4	vyprávění
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
Jacoba	Jacoba	k1gFnSc1	Jacoba
Blacka	Blacka	k1gFnSc1	Blacka
<g/>
.	.	kIx.	.
</s>
<s>
Půlnoční	půlnoční	k2eAgNnSc1d1	půlnoční
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pátý	pátý	k4xOgInSc4	pátý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
díl	díl	k1gInSc1	díl
ságy	sága	k1gFnSc2	sága
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
příběhu	příběh	k1gInSc2	příběh
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Edwarda	Edward	k1gMnSc2	Edward
Cullena	Cullena	k1gFnSc1	Cullena
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Belle	bell	k1gInSc5	bell
druhou	druhý	k4xOgFnSc7	druhý
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
ságy	sága	k1gFnSc2	sága
Stmívání	stmívání	k1gNnSc2	stmívání
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Twilight	Twilight	k2eAgInSc1d1	Twilight
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
publikován	publikován	k2eAgInSc1d1	publikován
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgFnP	následovat
knihy	kniha	k1gFnPc4	kniha
Nový	nový	k2eAgInSc4d1	nový
měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
New	New	k1gMnSc1	New
Moon	Moon	k1gMnSc1	Moon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zatmění	zatmění	k1gNnSc1	zatmění
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Eclipse	Eclips	k1gMnSc2	Eclips
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rozbřesk	rozbřesk	k1gInSc4	rozbřesk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Breaking	Breaking	k1gInSc1	Breaking
dawn	dawna	k1gFnPc2	dawna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ságy	sága	k1gFnSc2	sága
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
i	i	k9	i
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
Půlnoční	půlnoční	k2eAgNnSc4d1	půlnoční
slunce	slunce	k1gNnSc4	slunce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Midnight	Midnight	k1gMnSc1	Midnight
Sun	Sun	kA	Sun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
vydané	vydaný	k2eAgNnSc1d1	vydané
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
prvního	první	k4xOgMnSc2	první
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
Twilight	Twilight	k1gInSc1	Twilight
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgFnP	získat
knihy	kniha	k1gFnPc1	kniha
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sága	sága	k1gFnSc1	sága
Stmívání	stmívání	k1gNnSc2	stmívání
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
upírská	upírský	k2eAgFnSc1d1	upírská
romance	romance	k1gFnSc1	romance
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dočkala	dočkat	k5eAaPmAgFnS	dočkat
filmového	filmový	k2eAgNnSc2d1	filmové
zpracování	zpracování	k1gNnSc2	zpracování
a	a	k8xC	a
žánrově	žánrově	k6eAd1	žánrově
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
akční	akční	k2eAgInSc4d1	akční
romantický	romantický	k2eAgInSc4d1	romantický
fantasy	fantas	k1gInPc4	fantas
thriller	thriller	k1gInSc1	thriller
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gFnSc1	Bella
Swanová	Swanová	k1gFnSc1	Swanová
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
z	z	k7c2	z
Phoenixu	Phoenix	k1gInSc2	Phoenix
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žila	žít	k5eAaImAgFnS	žít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Forks	Forksa	k1gFnPc2	Forksa
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washingtonu	Washington	k1gInSc2	Washington
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
tak	tak	k6eAd1	tak
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
cestovat	cestovat	k5eAaImF	cestovat
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
novým	nový	k2eAgMnSc7d1	nový
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
hráčem	hráč	k1gMnSc7	hráč
nižší	nízký	k2eAgFnSc2d2	nižší
baseballové	baseballový	k2eAgFnSc2d1	baseballová
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Bella	Bella	k1gMnSc1	Bella
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
do	do	k7c2	do
Forks	Forksa	k1gFnPc2	Forksa
<g/>
,	,	kIx,	,
potkává	potkávat	k5eAaImIp3nS	potkávat
tajemného	tajemný	k2eAgNnSc2d1	tajemné
<g/>
,	,	kIx,	,
pohledného	pohledný	k2eAgMnSc2d1	pohledný
mladíka	mladík	k1gMnSc2	mladík
Edwarda	Edward	k1gMnSc2	Edward
Cullena	Cullena	k1gFnSc1	Cullena
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
proti	proti	k7c3	proti
vlastní	vlastní	k2eAgFnSc3d1	vlastní
vůli	vůle	k1gFnSc3	vůle
velmi	velmi	k6eAd1	velmi
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bella	Bella	k1gMnSc1	Bella
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Edward	Edward	k1gMnSc1	Edward
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
rodiny	rodina	k1gFnSc2	rodina
upírů	upír	k1gMnPc2	upír
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
raději	rád	k6eAd2	rád
pije	pít	k5eAaImIp3nS	pít
krev	krev	k1gFnSc4	krev
zvířat	zvíře	k1gNnPc2	zvíře
než	než	k8xS	než
lidskou	lidský	k2eAgFnSc4d1	lidská
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
a	a	k8xC	a
Bella	Bella	k1gMnSc1	Bella
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
sadistický	sadistický	k2eAgMnSc1d1	sadistický
upír	upír	k1gMnSc1	upír
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
upířího	upíří	k2eAgInSc2d1	upíří
klanu	klan	k1gInSc2	klan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgMnSc1d1	posedlý
stopováním	stopování	k1gNnPc3	stopování
Belly	bell	k1gInPc4	bell
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Cullenů	Cullen	k1gInPc2	Cullen
Bellu	bell	k1gInSc2	bell
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
uteče	utéct	k5eAaPmIp3nS	utéct
do	do	k7c2	do
Phoenixu	Phoenix	k1gInSc2	Phoenix
do	do	k7c2	do
státu	stát	k1gInSc2	stát
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
podvedena	podvést	k5eAaPmNgFnS	podvést
Jamesem	James	k1gInSc7	James
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
zabít	zabít	k5eAaPmF	zabít
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
konfrontace	konfrontace	k1gFnSc2	konfrontace
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
je	být	k5eAaImIp3nS	být
vážně	vážně	k6eAd1	vážně
zraněna	zranit	k5eAaPmNgFnS	zranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Edward	Edward	k1gMnSc1	Edward
ji	on	k3xPp3gFnSc4	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Forks	Forksa	k1gFnPc2	Forksa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
nehrozí	hrozit	k5eNaImIp3nP	hrozit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
James	James	k1gMnSc1	James
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
opouští	opouštět	k5eAaImIp3nS	opouštět
Forks	Forks	k1gInSc4	Forks
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Edward	Edward	k1gMnSc1	Edward
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
Bellin	Bellin	k2eAgInSc4d1	Bellin
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
upadá	upadat	k5eAaImIp3nS	upadat
do	do	k7c2	do
hluboké	hluboký	k2eAgFnSc2d1	hluboká
deprese	deprese	k1gFnSc2	deprese
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
silné	silný	k2eAgNnSc4d1	silné
přátelství	přátelství	k1gNnSc4	přátelství
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Jacobem	Jacob	k1gMnSc7	Jacob
Blackem	Black	k1gInSc7	Black
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Bella	Bella	k1gMnSc1	Bella
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
přeměnit	přeměnit	k5eAaPmF	přeměnit
se	se	k3xPyFc4	se
ve	v	k7c4	v
vlka	vlk	k1gMnSc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
vlci	vlk	k1gMnPc1	vlk
jeho	on	k3xPp3gInSc2	on
kmene	kmen	k1gInSc2	kmen
musí	muset	k5eAaImIp3nS	muset
Bellu	bell	k1gInSc2	bell
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
Victorií	Victorie	k1gFnSc7	Victorie
<g/>
,	,	kIx,	,
upírkou	upírka	k1gFnSc7	upírka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pomstít	pomstít	k5eAaPmF	pomstít
smrt	smrt	k1gFnSc4	smrt
svého	svůj	k3xOyFgMnSc2	svůj
druha	druh	k1gMnSc2	druh
Jamese	Jamese	k1gFnSc2	Jamese
zabitím	zabití	k1gNnPc3	zabití
Belly	bell	k1gInPc4	bell
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
nedorozumění	nedorozumění	k1gNnSc3	nedorozumění
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yRgNnSc3	který
Edward	Edward	k1gMnSc1	Edward
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bella	Bella	k1gFnSc1	Bella
je	být	k5eAaImIp3nS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
ve	v	k7c6	v
Volteře	Voltera	k1gFnSc6	Voltera
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastaví	zastavit	k5eAaPmIp3nS	zastavit
ho	on	k3xPp3gMnSc4	on
Bella	Bella	k1gMnSc1	Bella
<g/>
,	,	kIx,	,
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
Edwardovou	Edwardův	k2eAgFnSc7d1	Edwardova
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
<g/>
.	.	kIx.	.
</s>
<s>
Setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
Volturiovými	Volturiový	k2eAgInPc7d1	Volturiový
<g/>
,	,	kIx,	,
mocným	mocný	k2eAgInSc7d1	mocný
klanem	klan	k1gInSc7	klan
upírů	upír	k1gMnPc2	upír
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Bella	Bella	k1gMnSc1	Bella
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
a	a	k8xC	a
Edward	Edward	k1gMnSc1	Edward
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
shledávají	shledávat	k5eAaImIp3nP	shledávat
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
a	a	k8xC	a
Cullenovi	Cullen	k1gMnSc3	Cullen
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Forks	Forksa	k1gFnPc2	Forksa
<g/>
.	.	kIx.	.
</s>
<s>
Upírka	upírek	k1gMnSc4	upírek
Victorie	Victorie	k1gFnSc1	Victorie
(	(	kIx(	(
<g/>
Jamesova	Jamesův	k2eAgFnSc1d1	Jamesova
družka	družka	k1gFnSc1	družka
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
"	"	kIx"	"
<g/>
Stmívání	stmívání	k1gNnSc2	stmívání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
stále	stále	k6eAd1	stále
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
a	a	k8xC	a
proto	proto	k8xC	proto
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
armádu	armáda	k1gFnSc4	armáda
upírů-novorozenců	upírůovorozenec	k1gMnPc2	upírů-novorozenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Cullenových	Cullenová	k1gFnPc2	Cullenová
a	a	k8xC	a
pomoci	pomoct	k5eAaPmF	pomoct
jí	on	k3xPp3gFnSc3	on
tak	tak	k9	tak
k	k	k7c3	k
zavraždění	zavraždění	k1gNnSc6	zavraždění
Belly	bell	k1gInPc1	bell
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
Bella	Bella	k1gFnSc1	Bella
nucena	nutit	k5eAaImNgFnS	nutit
si	se	k3xPyFc3	se
vybrat	vybrat	k5eAaPmF	vybrat
mezi	mezi	k7c7	mezi
svým	svůj	k3xOyFgInSc7	svůj
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
Edwardem	Edward	k1gMnSc7	Edward
a	a	k8xC	a
přátelstvím	přátelství	k1gNnSc7	přátelství
s	s	k7c7	s
Jacobem	Jacob	k1gInSc7	Jacob
<g/>
.	.	kIx.	.
</s>
<s>
Edwardova	Edwardův	k2eAgFnSc1d1	Edwardova
upíří	upíří	k2eAgFnSc1d1	upíří
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
Jacobova	Jacobův	k2eAgFnSc1d1	Jacobova
vlčí	vlčí	k2eAgFnSc1d1	vlčí
smečka	smečka	k1gFnSc1	smečka
spojují	spojovat	k5eAaImIp3nP	spojovat
síly	síla	k1gFnPc1	síla
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
zničení	zničení	k1gNnSc3	zničení
Victorie	Victorie	k1gFnSc2	Victorie
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
armády	armáda	k1gFnSc2	armáda
upírů	upír	k1gMnPc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bella	Bella	k1gMnSc1	Bella
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
Edwardově	Edwardův	k2eAgFnSc3d1	Edwardova
lásce	láska	k1gFnSc3	láska
před	před	k7c7	před
Jacobem	Jacob	k1gInSc7	Jacob
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
ji	on	k3xPp3gFnSc4	on
nakonec	nakonec	k6eAd1	nakonec
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Krátký	krátký	k2eAgInSc4d1	krátký
druhý	druhý	k4xOgInSc4	druhý
život	život	k1gInSc4	život
Bree	Bree	k1gNnSc2	Bree
Tannerové	Tannerová	k1gFnSc2	Tannerová
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Short	Shorta	k1gFnPc2	Shorta
Second	Second	k1gMnSc1	Second
Life	Lif	k1gFnSc2	Lif
of	of	k?	of
Bree	Bree	k1gInSc1	Bree
Tanner	Tanner	k1gInSc1	Tanner
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
stejné	stejný	k2eAgFnPc4d1	stejná
události	událost	k1gFnPc4	událost
jako	jako	k8xC	jako
Zatmění	zatmění	k1gNnPc4	zatmění
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
mladičké	mladičký	k2eAgMnPc4d1	mladičký
upírky	upírek	k1gMnPc4	upírek
Bree	Bre	k1gFnSc2	Bre
Tannerové	Tannerová	k1gFnSc2	Tannerová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
zabita	zabít	k5eAaPmNgFnS	zabít
Volturiovými	Volturiový	k2eAgInPc7d1	Volturiový
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
a	a	k8xC	a
také	také	k9	také
poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
ságy	sága	k1gFnSc2	sága
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
patří	patřit	k5eAaImIp3nS	patřit
Belle	bell	k1gInSc5	bell
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
Jacobovi	Jacobův	k2eAgMnPc1d1	Jacobův
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
opět	opět	k6eAd1	opět
Belle	bell	k1gInSc5	bell
<g/>
.	.	kIx.	.
</s>
<s>
Bella	Bella	k1gMnSc1	Bella
a	a	k8xC	a
Edward	Edward	k1gMnSc1	Edward
jsou	být	k5eAaImIp3nP	být
oddáni	oddat	k5eAaPmNgMnP	oddat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Bella	Bella	k1gMnSc1	Bella
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
těhotenství	těhotenství	k1gNnSc1	těhotenství
postupuje	postupovat	k5eAaImIp3nS	postupovat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
silně	silně	k6eAd1	silně
ji	on	k3xPp3gFnSc4	on
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
zemře	zemřít	k5eAaPmIp3nS	zemřít
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
své	svůj	k3xOyFgNnSc1	svůj
a	a	k8xC	a
Edwardovy	Edwardův	k2eAgFnPc1d1	Edwardova
napůl	napůl	k6eAd1	napůl
upíří	upíří	k2eAgFnPc1d1	upíří
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
lidské	lidský	k2eAgFnPc4d1	lidská
dcery	dcera	k1gFnPc4	dcera
Renesmee	Renesmee	k1gNnSc2	Renesmee
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
však	však	k9	však
vpraví	vpravit	k5eAaPmIp3nS	vpravit
do	do	k7c2	do
Belly	bell	k1gInPc1	bell
svůj	svůj	k3xOyFgInSc4	svůj
upíří	upíří	k2eAgInSc4d1	upíří
jed	jed	k1gInSc4	jed
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc7	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
a	a	k8xC	a
změní	změnit	k5eAaPmIp3nS	změnit
ji	on	k3xPp3gFnSc4	on
tak	tak	k9	tak
v	v	k7c6	v
upírku	upírek	k1gMnSc6	upírek
<g/>
.	.	kIx.	.
</s>
<s>
Sestřenice	sestřenice	k1gFnSc1	sestřenice
Cullenů	Cullen	k1gInPc2	Cullen
Irina	Irien	k2eAgFnSc1d1	Irina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
Bellu	bell	k1gInSc3	bell
moc	moc	k6eAd1	moc
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
uvidí	uvidět	k5eAaPmIp3nS	uvidět
Renesmee	Renesmee	k1gFnSc1	Renesmee
a	a	k8xC	a
splete	splést	k5eAaPmIp3nS	splést
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
nesmrtelným	smrtelný	k2eNgNnSc7d1	nesmrtelné
upířím	upíří	k2eAgNnSc7d1	upíří
dítětem	dítě	k1gNnSc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Informuje	informovat	k5eAaBmIp3nS	informovat
Volturiovy	Volturiův	k2eAgFnPc1d1	Volturiův
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existence	existence	k1gFnSc1	existence
takových	takový	k3xDgFnPc2	takový
bytostí	bytost	k1gFnPc2	bytost
porušuje	porušovat	k5eAaImIp3nS	porušovat
zákon	zákon	k1gInSc1	zákon
upírů	upír	k1gMnPc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
Cullenovi	Cullenův	k2eAgMnPc1d1	Cullenův
shromáždí	shromáždit	k5eAaPmIp3nP	shromáždit
svědky-upíry	svědkypír	k1gMnPc4	svědky-upír
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mohou	moct	k5eAaImIp3nP	moct
dosvědčit	dosvědčit	k5eAaPmF	dosvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Renesmee	Renesmee	k1gInSc1	Renesmee
není	být	k5eNaImIp3nS	být
nesmrtelné	smrtelný	k2eNgNnSc4d1	nesmrtelné
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
konfrontaci	konfrontace	k1gFnSc4	konfrontace
Cullenovi	Cullen	k1gMnSc3	Cullen
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
svědci	svědek	k1gMnPc1	svědek
přesvědčí	přesvědčit	k5eAaPmIp3nP	přesvědčit
Volturiovy	Volturiův	k2eAgInPc4d1	Volturiův
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
nepředstavuje	představovat	k5eNaImIp3nS	představovat
pro	pro	k7c4	pro
upíry	upír	k1gMnPc4	upír
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnPc1	jejich
tajemství	tajemství	k1gNnPc1	tajemství
žádné	žádný	k3yNgNnSc4	žádný
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
a	a	k8xC	a
Volturiovi	Volturiův	k2eAgMnPc1d1	Volturiův
je	on	k3xPp3gNnSc4	on
nechávají	nechávat	k5eAaImIp3nP	nechávat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
na	na	k7c6	na
autorčiných	autorčin	k2eAgFnPc6d1	autorčina
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Stephenie	Stephenie	k1gFnSc2	Stephenie
zatím	zatím	k6eAd1	zatím
nehodlá	hodlat	k5eNaImIp3nS	hodlat
dopisovat	dopisovat	k5eAaImF	dopisovat
pátý	pátý	k4xOgInSc4	pátý
a	a	k8xC	a
prý	prý	k9	prý
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
upírské	upírský	k2eAgFnSc2d1	upírská
ságy	sága	k1gFnSc2	sága
-	-	kIx~	-
Půlnoční	půlnoční	k2eAgNnSc1d1	půlnoční
slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
Midnight	Midnight	k1gInSc1	Midnight
Sun	Sun	kA	Sun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
Twilight	Twilight	k1gInSc4	Twilight
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Edwarda	Edward	k1gMnSc2	Edward
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
původní	původní	k2eAgFnSc1d1	původní
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Belly	bell	k1gInPc1	bell
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
dala	dát	k5eAaPmAgFnS	dát
své	svůj	k3xOyFgFnPc4	svůj
kopie	kopie	k1gFnPc4	kopie
rukopisu	rukopis	k1gInSc2	rukopis
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
moc	moc	k6eAd1	moc
dobře	dobře	k6eAd1	dobře
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
o	o	k7c4	o
koho	kdo	k3yQnSc4	kdo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
a	a	k8xC	a
moc	moc	k6eAd1	moc
ji	on	k3xPp3gFnSc4	on
to	ten	k3xDgNnSc4	ten
mrzí	mrzet	k5eAaImIp3nS	mrzet
<g/>
.	.	kIx.	.
</s>
<s>
Ztratila	ztratit	k5eAaPmAgFnS	ztratit
tak	tak	k9	tak
důležité	důležitý	k2eAgNnSc4d1	důležité
vlákno	vlákno	k1gNnSc4	vlákno
a	a	k8xC	a
příběh	příběh	k1gInSc1	příběh
zatím	zatím	k6eAd1	zatím
nehodlá	hodlat	k5eNaImIp3nS	hodlat
dopsat	dopsat	k5eAaPmF	dopsat
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
věnovat	věnovat	k5eAaImF	věnovat
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejněné	zveřejněný	k2eAgFnPc4d1	zveřejněná
kapitoly	kapitola	k1gFnPc4	kapitola
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
i	i	k9	i
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
dala	dát	k5eAaPmAgFnS	dát
Stephenie	Stephenie	k1gFnSc1	Stephenie
jednu	jeden	k4xCgFnSc4	jeden
kopii	kopie	k1gFnSc4	kopie
k	k	k7c3	k
přečtení	přečtení	k1gNnSc3	přečtení
i	i	k8xC	i
představiteli	představitel	k1gMnPc7	představitel
Edwarda	Edward	k1gMnSc2	Edward
Cullena	Cullena	k1gFnSc1	Cullena
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Pattinson	Pattinson	k1gMnSc1	Pattinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
vžil	vžít	k5eAaPmAgMnS	vžít
do	do	k7c2	do
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Edwardovi	Edward	k1gMnSc3	Edward
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
nudným	nudný	k2eAgMnSc7d1	nudný
<g/>
...	...	k?	...
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
objeví	objevit	k5eAaPmIp3nS	objevit
nová	nový	k2eAgFnSc1d1	nová
žákyně	žákyně	k1gFnSc1	žákyně
-	-	kIx~	-
Isabella	Isabella	k1gFnSc1	Isabella
Swanová	Swanová	k1gFnSc1	Swanová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
si	se	k3xPyFc3	se
Edward	Edward	k1gMnSc1	Edward
všimne	všimnout	k5eAaPmIp3nS	všimnout
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
číst	číst	k5eAaImF	číst
její	její	k3xOp3gFnPc4	její
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
neuvěřitelnou	uvěřitelný	k2eNgFnSc4d1	neuvěřitelná
chuť	chuť	k1gFnSc4	chuť
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
třetí	třetí	k4xOgMnSc1	třetí
<g/>
?	?	kIx.	?
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
potřebu	potřeba	k1gFnSc4	potřeba
ji	on	k3xPp3gFnSc4	on
chránit	chránit	k5eAaImF	chránit
před	před	k7c4	před
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
Bella	Bella	k1gFnSc1	Bella
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
podivně	podivně	k6eAd1	podivně
často	často	k6eAd1	často
<g/>
...	...	k?	...
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
Editor	editor	k1gInSc1	editor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choic	k1gMnSc4	Choic
A	a	k9	a
Publishers	Publishers	k1gInSc4	Publishers
Weekly	Weekl	k1gInPc7	Weekl
Best	Best	k1gMnSc1	Best
Book	Book	k1gMnSc1	Book
of	of	k?	of
the	the	k?	the
Year	Year	k1gMnSc1	Year
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Library	Librara	k1gFnSc2	Librara
Association	Association	k1gInSc1	Association
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
Ten	ten	k3xDgInSc1	ten
Best	Best	k2eAgInSc1d1	Best
Book	Book	k1gInSc1	Book
for	forum	k1gNnPc2	forum
Young	Younga	k1gFnPc2	Younga
Adults	Adultsa	k1gFnPc2	Adultsa
<g/>
"	"	kIx"	"
and	and	k?	and
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
Ten	ten	k3xDgMnSc1	ten
Books	Booksa	k1gFnPc2	Booksa
for	forum	k1gNnPc2	forum
Reluctant	Reluctant	k1gMnSc1	Reluctant
Readers	Readers	k1gInSc1	Readers
<g/>
"	"	kIx"	"
Přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
jazyků	jazyk	k1gInPc2	jazyk
Newyorský	newyorský	k2eAgInSc4d1	newyorský
bestseller	bestseller	k1gInSc4	bestseller
Nejlépe	dobře	k6eAd3	dobře
prodávaná	prodávaný	k2eAgFnSc1d1	prodávaná
kniha	kniha	k1gFnSc1	kniha
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
</s>
