<p>
<s>
Kolovrat	kolovrat	k1gInSc1	kolovrat
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnPc4	zařízení
ke	k	k7c3	k
spřádání	spřádání	k1gNnSc3	spřádání
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
i	i	k8xC	i
živočišných	živočišný	k2eAgFnPc2d1	živočišná
textilních	textilní	k2eAgFnPc2d1	textilní
vláken	vlákna	k1gFnPc2	vlákna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Vynálezce	vynálezce	k1gMnSc1	vynálezce
kolovratu	kolovrat	k1gInSc2	kolovrat
není	být	k5eNaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
a	a	k8xC	a
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předení	předení	k1gNnPc4	předení
na	na	k7c6	na
kolovratu	kolovrat	k1gInSc6	kolovrat
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc4d1	původní
formu	forma	k1gFnSc4	forma
kolovratu	kolovrat	k1gInSc2	kolovrat
připomíná	připomínat	k5eAaImIp3nS	připomínat
indická	indický	k2eAgFnSc1d1	indická
charkha	charkha	k1gFnSc1	charkha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
hořejší	horní	k2eAgFnSc1d2	hořejší
snímek	snímek	k1gInSc1	snímek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
ještě	ještě	k9	ještě
i	i	k9	i
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
kolo	kolo	k1gNnSc1	kolo
se	se	k3xPyFc4	se
pohánělo	pohánět	k5eAaImAgNnS	pohánět
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
otáčky	otáčka	k1gFnPc4	otáčka
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
přenášely	přenášet	k5eAaImAgFnP	přenášet
na	na	k7c4	na
vřeteno	vřeteno	k1gNnSc4	vřeteno
s	s	k7c7	s
cívkou	cívka	k1gFnSc7	cívka
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
navíjela	navíjet	k5eAaImAgFnS	navíjet
příze	příze	k1gFnSc1	příze
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
mezi	mezi	k7c7	mezi
prsty	prst	k1gInPc7	prst
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
vřetenem	vřeteno	k1gNnSc7	vřeteno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vynalezen	vynalezen	k2eAgInSc1d1	vynalezen
kolovrat	kolovrat	k1gInSc1	kolovrat
s	s	k7c7	s
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
naznačeno	naznačit	k5eAaPmNgNnS	naznačit
na	na	k7c6	na
nákresu	nákres	k1gInSc6	nákres
vpravo	vpravo	k6eAd1	vpravo
dole	dole	k6eAd1	dole
<g/>
:	:	kIx,	:
<g/>
Pohyb	pohyb	k1gInSc1	pohyb
pedálu	pedál	k1gInSc2	pedál
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
klikovou	klikový	k2eAgFnSc4d1	kliková
hřídel	hřídel	k1gFnSc4	hřídel
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
s	s	k7c7	s
hnacím	hnací	k2eAgNnSc7d1	hnací
kolem	kolo	k1gNnSc7	kolo
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otáčky	otáčka	k1gFnPc1	otáčka
hnacího	hnací	k2eAgNnSc2d1	hnací
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c4	na
přeslen	přeslen	k1gInSc4	přeslen
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
na	na	k7c6	na
vřetenu	vřeten	k1gInSc6	vřeten
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
a	a	k8xC	a
(	(	kIx(	(
<g/>
u	u	k7c2	u
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
na	na	k7c6	na
cívce	cívka	k1gFnSc6	cívka
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vřetenu	vřeten	k1gInSc6	vřeten
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
nasazeno	nasazen	k2eAgNnSc4d1	nasazeno
dvouramenné	dvouramenný	k2eAgNnSc4d1	dvouramenné
křídlo	křídlo	k1gNnSc4	křídlo
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
s	s	k7c7	s
vodiči	vodič	k1gInPc7	vodič
niti	nit	k1gFnSc2	nit
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsluha	obsluha	k1gFnSc1	obsluha
kolovratu	kolovrat	k1gInSc2	kolovrat
uvádí	uvádět	k5eAaImIp3nS	uvádět
šlapáním	šlapání	k1gNnSc7	šlapání
na	na	k7c4	na
pedál	pedál	k1gInSc4	pedál
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
křídlo	křídlo	k1gNnSc4	křídlo
a	a	k8xC	a
cívku	cívka	k1gFnSc4	cívka
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
chomáč	chomáč	k1gInSc1	chomáč
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
přidává	přidávat	k5eAaImIp3nS	přidávat
pramínky	pramínek	k1gInPc7	pramínek
na	na	k7c4	na
konec	konec	k1gInSc4	konec
niti	nit	k1gFnSc2	nit
<g/>
.	.	kIx.	.
</s>
<s>
Otáčkami	otáčka	k1gFnPc7	otáčka
vřetene	vřeten	k1gInSc5	vřeten
se	se	k3xPyFc4	se
nit	nit	k1gFnSc1	nit
zakrucuje	zakrucovat	k5eAaImIp3nS	zakrucovat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
navíjí	navíjet	k5eAaImIp3nS	navíjet
na	na	k7c4	na
cívku	cívka	k1gFnSc4	cívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
výrobní	výrobní	k2eAgInSc1d1	výrobní
výkon	výkon	k1gInSc1	výkon
dosažitelný	dosažitelný	k2eAgInSc1d1	dosažitelný
s	s	k7c7	s
kolovratem	kolovrat	k1gInSc7	kolovrat
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
s	s	k7c7	s
350	[number]	k4	350
metry	metr	k1gMnPc7	metr
příze	příz	k1gFnSc2	příz
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
byl	být	k5eAaImAgInS	být
kolovrat	kolovrat	k1gInSc1	kolovrat
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
už	už	k6eAd1	už
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
před	před	k7c7	před
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1313	[number]	k4	1313
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgInSc1d1	známý
kolovrat	kolovrat	k1gInSc1	kolovrat
s	s	k7c7	s
více	hodně	k6eAd2	hodně
vřeteny	vřeten	k1gInPc7	vřeten
<g/>
,	,	kIx,	,
poháněný	poháněný	k2eAgInSc1d1	poháněný
vodním	vodní	k2eAgNnSc7d1	vodní
kolem	kolo	k1gNnSc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
evtl	evtl	k1gInSc1	evtl
<g/>
.	.	kIx.	.
používat	používat	k5eAaImF	používat
ke	k	k7c3	k
skaní	skaní	k1gNnSc3	skaní
<g/>
,	,	kIx,	,
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
však	však	k9	však
nic	nic	k3yNnSc1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
jen	jen	k9	jen
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
nákres	nákres	k1gInSc1	nákres
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
kolovratu	kolovrat	k1gInSc2	kolovrat
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
Speciálně	speciálně	k6eAd1	speciálně
upravený	upravený	k2eAgInSc1d1	upravený
kolovrat	kolovrat	k1gInSc1	kolovrat
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
ke	k	k7c3	k
komerční	komerční	k2eAgFnSc3d1	komerční
výrobě	výroba	k1gFnSc3	výroba
příze	příz	k1gFnSc2	příz
z	z	k7c2	z
kokosových	kokosový	k2eAgNnPc2d1	kokosové
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
0,5	[number]	k4	0,5
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
amatérské	amatérský	k2eAgFnSc3d1	amatérská
výrobě	výroba	k1gFnSc3	výroba
příze	příz	k1gFnSc2	příz
a	a	k8xC	a
jako	jako	k9	jako
dekorační	dekorační	k2eAgInSc1d1	dekorační
předmět	předmět	k1gInSc1	předmět
se	se	k3xPyFc4	se
nabízí	nabízet	k5eAaImIp3nS	nabízet
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
napodobeniny	napodobenina	k1gFnSc2	napodobenina
několika	několik	k4yIc2	několik
druhů	druh	k1gMnPc2	druh
historických	historický	k2eAgInPc2d1	historický
kolovratů	kolovrat	k1gInPc2	kolovrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Joan	Joan	k1gMnSc1	Joan
Whittaker	Whittaker	k1gMnSc1	Whittaker
Cummer	Cummer	k1gMnSc1	Cummer
<g/>
:	:	kIx,	:
A	A	kA	A
Book	Book	k1gInSc1	Book
of	of	k?	of
Spinning	Spinning	k1gInSc1	Spinning
Wheels	Wheels	k1gInSc1	Wheels
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
E.	E.	kA	E.
Randall	Randall	k1gMnSc1	Randall
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0914339465	[number]	k4	978-0914339465
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolovrat	kolovrat	k1gInSc1	kolovrat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kolovrat	kolovrat	k1gInSc1	kolovrat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
