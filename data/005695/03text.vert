<s>
Sumó	Sumó	k?	Sumó
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
相	相	k?	相
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc1d1	starý
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
mající	mající	k2eAgMnSc1d1	mající
svoje	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
šintoismu	šintoismus	k1gInSc6	šintoismus
<g/>
.	.	kIx.	.
</s>
<s>
Japonským	japonský	k2eAgInSc7d1	japonský
národním	národní	k2eAgInSc7d1	národní
sportem	sport	k1gInSc7	sport
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sumó	sumó	k?	sumó
během	během	k7c2	během
období	období	k1gNnSc2	období
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
1600	[number]	k4	1600
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
sportu	sport	k1gInSc6	sport
se	se	k3xPyFc4	se
však	však	k9	však
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
kronice	kronika	k1gFnSc6	kronika
Kodžiki	Kodžik	k1gFnSc2	Kodžik
z	z	k7c2	z
roku	rok	k1gInSc2	rok
712	[number]	k4	712
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
sport	sport	k1gInSc1	sport
plný	plný	k2eAgInSc1d1	plný
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
precizních	precizní	k2eAgNnPc2d1	precizní
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
pevně	pevně	k6eAd1	pevně
daných	daný	k2eAgInPc2d1	daný
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
sumó	sumó	k?	sumó
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
na	na	k7c6	na
čtvercovém	čtvercový	k2eAgNnSc6d1	čtvercové
vyvýšeném	vyvýšený	k2eAgNnSc6d1	vyvýšené
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
cm	cm	kA	cm
<g/>
)	)	kIx)	)
ringu	ring	k1gInSc2	ring
"	"	kIx"	"
<g/>
dohjó	dohjó	k?	dohjó
<g/>
"	"	kIx"	"
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
snopy	snop	k1gInPc7	snop
z	z	k7c2	z
rýžové	rýžový	k2eAgFnSc2d1	rýžová
slámy	sláma	k1gFnSc2	sláma
vyznačeno	vyznačen	k2eAgNnSc4d1	vyznačeno
kruhové	kruhový	k2eAgNnSc4d1	kruhové
zápasiště	zápasiště	k1gNnSc4	zápasiště
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
4,57	[number]	k4	4,57
m.	m.	k?	m.
Cílem	cíl	k1gInSc7	cíl
zápasu	zápas	k1gInSc2	zápas
je	být	k5eAaImIp3nS	být
vytlačit	vytlačit	k5eAaPmF	vytlačit
soupeře	soupeř	k1gMnPc4	soupeř
za	za	k7c4	za
okraj	okraj	k1gInSc4	okraj
vyznačeného	vyznačený	k2eAgNnSc2d1	vyznačené
zápasiště	zápasiště	k1gNnSc2	zápasiště
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gInSc4	on
donutit	donutit	k5eAaPmF	donutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dotkl	dotknout	k5eAaPmAgInS	dotknout
povrchu	povrch	k1gInSc3	povrch
země	zem	k1gFnSc2	zem
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
jinou	jiný	k2eAgFnSc7d1	jiná
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
chodidlo	chodidlo	k1gNnSc1	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
především	především	k6eAd1	především
vyvést	vyvést	k5eAaPmF	vyvést
svého	svůj	k3xOyFgMnSc4	svůj
soupeře	soupeř	k1gMnSc4	soupeř
z	z	k7c2	z
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostnímu	slavnostní	k2eAgInSc3d1	slavnostní
vstupu	vstup	k1gInSc3	vstup
zápasníků	zápasník	k1gMnPc2	zápasník
před	před	k7c7	před
vlastními	vlastní	k2eAgInPc7d1	vlastní
zápasy	zápas	k1gInPc7	zápas
do	do	k7c2	do
ringu	ring	k1gInSc2	ring
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
dohjóiri	dohjóiri	k1gNnSc1	dohjóiri
(	(	kIx(	(
<g/>
土	土	k?	土
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obřadní	obřadní	k2eAgNnSc4d1	obřadní
představení	představení	k1gNnSc4	představení
zápasníků	zápasník	k1gMnPc2	zápasník
<g/>
.	.	kIx.	.
</s>
<s>
Soupeři	soupeř	k1gMnPc1	soupeř
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
zápasem	zápas	k1gInSc7	zápas
tráví	trávit	k5eAaImIp3nP	trávit
čas	čas	k1gInSc4	čas
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
předváděním	předvádění	k1gNnSc7	předvádění
obřadních	obřadní	k2eAgInPc2d1	obřadní
dřepů	dřep	k1gInPc2	dřep
"	"	kIx"	"
<g/>
šiko	šiko	k6eAd1	šiko
<g/>
"	"	kIx"	"
a	a	k8xC	a
soubojem	souboj	k1gInSc7	souboj
očí	oko	k1gNnPc2	oko
se	s	k7c7	s
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
oba	dva	k4xCgMnPc1	dva
zapasníci	zapasník	k1gMnPc1	zapasník
vystoupí	vystoupit	k5eAaPmIp3nP	vystoupit
z	z	k7c2	z
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
napijí	napít	k5eAaBmIp3nP	napít
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
vody	voda	k1gFnSc2	voda
síly	síla	k1gFnSc2	síla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
otřou	otřít	k5eAaPmIp3nP	otřít
si	se	k3xPyFc3	se
ústa	ústa	k1gNnPc4	ústa
"	"	kIx"	"
<g/>
papírem	papír	k1gInSc7	papír
síly	síla	k1gFnSc2	síla
<g/>
"	"	kIx"	"
a	a	k8xC	a
poté	poté	k6eAd1	poté
naberou	nabrat	k5eAaPmIp3nP	nabrat
do	do	k7c2	do
hrsti	hrst	k1gFnSc2	hrst
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
rozhodí	rozhodit	k5eAaPmIp3nS	rozhodit
po	po	k7c6	po
zápasišti	zápasiště	k1gNnSc6	zápasiště
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
zahnat	zahnat	k5eAaPmF	zahnat
zlé	zlý	k2eAgMnPc4d1	zlý
duchy	duch	k1gMnPc4	duch
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
gjódži	gjódž	k1gFnSc3	gjódž
(	(	kIx(	(
<g/>
行	行	k?	行
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oblečen	obléct	k5eAaPmNgMnS	obléct
do	do	k7c2	do
kostýmu	kostým	k1gInSc2	kostým
šlechtice	šlechtic	k1gMnSc2	šlechtic
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
pasem	pas	k1gInSc7	pas
nosí	nosit	k5eAaImIp3nS	nosit
malý	malý	k2eAgInSc1d1	malý
nůž	nůž	k1gInSc1	nůž
tantó	tantó	k?	tantó
jako	jako	k8xC	jako
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mylného	mylný	k2eAgNnSc2d1	mylné
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
spáchal	spáchat	k5eAaPmAgMnS	spáchat
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
sebevraždu	sebevražda	k1gFnSc4	sebevražda
seppuku	seppuk	k1gInSc2	seppuk
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
uctivost	uctivost	k1gFnSc1	uctivost
velí	velet	k5eAaImIp3nS	velet
i	i	k9	i
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chyby	chyba	k1gFnSc2	chyba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
odchod	odchod	k1gInSc4	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
žádost	žádost	k1gFnSc1	žádost
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
zamítnuta	zamítnut	k2eAgFnSc1d1	zamítnuta
<g/>
.	.	kIx.	.
</s>
<s>
Zápasům	zápas	k1gInPc3	zápas
přihlížejí	přihlížet	k5eAaImIp3nP	přihlížet
také	také	k9	také
soudci	soudce	k1gMnPc1	soudce
šinpan	šinpany	k1gInPc2	šinpany
(	(	kIx(	(
<g/>
審	審	k?	審
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sedí	sedit	k5eAaImIp3nP	sedit
kolem	kolem	k7c2	kolem
zápasiště	zápasiště	k1gNnSc2	zápasiště
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
82	[number]	k4	82
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
"	"	kIx"	"
<g/>
vítězných	vítězný	k2eAgFnPc2d1	vítězná
technik	technika	k1gFnPc2	technika
<g/>
"	"	kIx"	"
zvaných	zvaný	k2eAgFnPc2d1	zvaná
kimarite	kimarit	k1gInSc5	kimarit
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
sestavila	sestavit	k5eAaPmAgFnS	sestavit
Japonská	japonský	k2eAgFnSc1d1	japonská
asociace	asociace	k1gFnSc1	asociace
sumó	sumó	k?	sumó
(	(	kIx(	(
<g/>
日	日	k?	日
<g/>
;	;	kIx,	;
Nihon	Nihon	k1gMnSc1	Nihon
Sumó	Sumó	k1gFnSc2	Sumó
Kjókai	Kjóka	k1gFnSc2	Kjóka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřicet	čtyřicet	k4xCc1	čtyřicet
osm	osm	k4xCc1	osm
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
technik	technika	k1gFnPc2	technika
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c2	za
klasické	klasický	k2eAgFnSc2d1	klasická
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Sumó	Sumó	k?	Sumó
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
neobyčejně	obyčejně	k6eNd1	obyčejně
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
neexistuje	existovat	k5eNaImIp3nS	existovat
členění	členění	k1gNnSc1	členění
zápasníků	zápasník	k1gMnPc2	zápasník
do	do	k7c2	do
váhových	váhový	k2eAgFnPc2d1	váhová
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
utkají	utkat	k5eAaPmIp3nP	utkat
dva	dva	k4xCgMnPc1	dva
zápasníci	zápasník	k1gMnPc1	zápasník
s	s	k7c7	s
obrovským	obrovský	k2eAgInSc7d1	obrovský
hmotnostním	hmotnostní	k2eAgInSc7d1	hmotnostní
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
sumu	suma	k1gFnSc4	suma
nesmírně	smírně	k6eNd1	smírně
důležitá	důležitý	k2eAgFnSc1d1	důležitá
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
ideální	ideální	k2eAgFnSc1d1	ideální
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
nízko	nízko	k6eAd1	nízko
položené	položený	k2eAgNnSc1d1	položené
těžiště	těžiště	k1gNnSc1	těžiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžší	těžký	k2eAgMnSc1d2	těžší
zápasník	zápasník	k1gMnSc1	zápasník
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
zápasníci	zápasník	k1gMnPc1	zápasník
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
jasně	jasně	k6eAd1	jasně
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
obratností	obratnost	k1gFnSc7	obratnost
a	a	k8xC	a
technikou	technika	k1gFnSc7	technika
lze	lze	k6eAd1	lze
hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
deficit	deficit	k1gInSc1	deficit
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
souboje	souboj	k1gInSc2	souboj
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
sumó	sumó	k?	sumó
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
styly	styl	k1gInPc4	styl
<g/>
:	:	kIx,	:
yotsu-sumo	yotsuumo	k6eAd1	yotsu-sumo
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
strategií	strategie	k1gFnSc7	strategie
je	být	k5eAaImIp3nS	být
uchopit	uchopit	k5eAaPmF	uchopit
soupeřův	soupeřův	k2eAgInSc4d1	soupeřův
pás	pás	k1gInSc4	pás
a	a	k8xC	a
vítězné	vítězný	k2eAgFnPc4d1	vítězná
techniky	technika	k1gFnPc4	technika
docílit	docílit	k5eAaPmF	docílit
pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgInSc2	tento
úchopu	úchop	k1gInSc2	úchop
<g/>
)	)	kIx)	)
a	a	k8xC	a
oshi-sumo	oshiumo	k6eAd1	oshi-sumo
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
strategií	strategie	k1gFnSc7	strategie
je	být	k5eAaImIp3nS	být
nepustit	pustit	k5eNaPmF	pustit
si	se	k3xPyFc3	se
soupeře	soupeř	k1gMnSc4	soupeř
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
a	a	k8xC	a
porazit	porazit	k5eAaPmF	porazit
jej	on	k3xPp3gInSc4	on
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
úchopu	úchop	k1gInSc2	úchop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězné	vítězný	k2eAgFnPc1d1	vítězná
techniky	technika	k1gFnPc1	technika
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
kihonwaza	kihonwaza	k1gFnSc1	kihonwaza
-	-	kIx~	-
základní	základní	k2eAgFnPc4d1	základní
techniky	technika	k1gFnPc4	technika
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc4d1	typické
<g/>
:	:	kIx,	:
yorikiri	yorikiri	k1gNnSc4	yorikiri
<g/>
,	,	kIx,	,
oshidashi	oshidashi	k6eAd1	oshidashi
nagete	nagat	k5eAaPmIp2nP	nagat
-	-	kIx~	-
hody	hod	k1gInPc1	hod
<g/>
,	,	kIx,	,
typické	typický	k2eAgInPc1d1	typický
<g/>
:	:	kIx,	:
uwatenage	uwatenag	k1gInPc1	uwatenag
<g/>
,	,	kIx,	,
uwatedashinage	uwatedashinag	k1gInPc1	uwatedashinag
kakete	kake	k1gNnSc2	kake
-	-	kIx~	-
techniky	technika	k1gFnSc2	technika
postavené	postavený	k2eAgFnPc1d1	postavená
na	na	k7c4	na
podražení	podražení	k1gNnSc4	podražení
nohy	noha	k1gFnSc2	noha
<g/>
/	/	kIx~	/
<g/>
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
sotogake	sotogakus	k1gMnSc5	sotogakus
<g/>
,	,	kIx,	,
uchigake	uchigakat	k5eAaPmIp3nS	uchigakat
hinerite	hinerit	k1gInSc5	hinerit
-	-	kIx~	-
techniky	technika	k1gFnPc4	technika
postavené	postavený	k2eAgFnPc4d1	postavená
na	na	k7c6	na
pákách	páka	k1gFnPc6	páka
a	a	k8xC	a
krutech	krut	k1gInPc6	krut
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc4d1	typické
<g/>
:	:	kIx,	:
tsukiotoshi	tsukiotoshi	k1gNnSc4	tsukiotoshi
<g/>
,	,	kIx,	,
tottari	tottari	k6eAd1	tottari
sorite	sorites	k1gInSc5	sorites
-	-	kIx~	-
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
soupeř	soupeř	k1gMnSc1	soupeř
padá	padat	k5eAaImIp3nS	padat
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
tokushuwaza	tokushuwaz	k1gMnSc2	tokushuwaz
-	-	kIx~	-
speciální	speciální	k2eAgFnPc1d1	speciální
techniky	technika	k1gFnPc1	technika
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
<g/>
:	:	kIx,	:
hatakikomi	hatakiko	k1gFnPc7	hatakiko
<g/>
,	,	kIx,	,
okuridashi	okuridashi	k6eAd1	okuridashi
hiwaza	hiwaza	k1gFnSc1	hiwaza
-	-	kIx~	-
pět	pět	k4xCc4	pět
speciálních	speciální	k2eAgFnPc2d1	speciální
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
de-facto	deacto	k1gNnSc4	de-facto
nejsou	být	k5eNaImIp3nP	být
vítěznými	vítězný	k2eAgFnPc7d1	vítězná
technikami	technika	k1gFnPc7	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
selháním	selhání	k1gNnSc7	selhání
prohravšího	prohravší	k2eAgMnSc2d1	prohravší
zápasníka	zápasník	k1gMnSc2	zápasník
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
nezpůsobil	způsobit	k5eNaPmAgMnS	způsobit
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
nezaviněné	zaviněný	k2eNgNnSc1d1	nezaviněné
vyšlápnutí	vyšlápnutí	k1gNnSc1	vyšlápnutí
<g/>
,	,	kIx,	,
zakopnutí	zakopnutí	k1gNnSc1	zakopnutí
<g/>
,	,	kIx,	,
uklouznutí	uklouznutí	k1gNnSc1	uklouznutí
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Zápasníci	zápasník	k1gMnPc1	zápasník
nosí	nosit	k5eAaImIp3nP	nosit
břišní	břišní	k2eAgInSc4d1	břišní
pás	pás	k1gInSc4	pás
mawaši	mawasat	k5eAaPmIp1nSwK	mawasat
(	(	kIx(	(
<g/>
廻	廻	k?	廻
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
ovinutý	ovinutý	k2eAgMnSc1d1	ovinutý
kolem	kolem	k7c2	kolem
slabin	slabina	k1gFnPc2	slabina
<g/>
,	,	kIx,	,
pasu	pas	k1gInSc2	pas
a	a	k8xC	a
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
zauzlovaný	zauzlovaný	k2eAgInSc4d1	zauzlovaný
<g/>
.	.	kIx.	.
</s>
<s>
Zápasníci	zápasník	k1gMnPc1	zápasník
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sekitori	sekitor	k1gFnSc2	sekitor
(	(	kIx(	(
<g/>
関	関	k?	関
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
právo	právo	k1gNnSc4	právo
mít	mít	k5eAaImF	mít
během	během	k7c2	během
turnajů	turnaj	k1gInPc2	turnaj
svázané	svázaný	k2eAgInPc4d1	svázaný
vlasy	vlas	k1gInPc4	vlas
do	do	k7c2	do
tradičního	tradiční	k2eAgInSc2d1	tradiční
účesu	účes	k1gInSc2	účes
středověkých	středověký	k2eAgMnPc2d1	středověký
šlechticů	šlechtic	k1gMnPc2	šlechtic
čonmage	čonmag	k1gFnSc2	čonmag
(	(	kIx(	(
<g/>
丁	丁	k?	丁
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
oičómage	oičómag	k1gInSc2	oičómag
(	(	kIx(	(
<g/>
お	お	k?	お
<g/>
)	)	kIx)	)
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
uzel	uzel	k1gInSc4	uzel
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
listu	list	k1gInSc2	list
ginkgo	ginkgo	k1gMnSc1	ginkgo
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgMnPc1d1	profesionální
zápasníci	zápasník	k1gMnPc1	zápasník
závodí	závodit	k5eAaImIp3nP	závodit
za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
stáje	stáj	k1gFnPc4	stáj
heja	heja	k0	heja
(	(	kIx(	(
<g/>
部	部	k?	部
<g/>
)	)	kIx)	)
a	a	k8xC	a
utkávají	utkávat	k5eAaImIp3nP	utkávat
se	se	k3xPyFc4	se
v	v	k7c6	v
hierarchicky	hierarchicky	k6eAd1	hierarchicky
uspořádané	uspořádaný	k2eAgFnSc3d1	uspořádaná
struktuře	struktura	k1gFnSc3	struktura
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vzestup	vzestup	k1gInSc1	vzestup
zápasníka	zápasník	k1gMnSc2	zápasník
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc1	jeho
pokles	pokles	k1gInSc1	pokles
<g/>
)	)	kIx)	)
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
soutěží	soutěž	k1gFnPc2	soutěž
závisí	záviset	k5eAaImIp3nS	záviset
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
poměru	poměr	k1gInSc6	poměr
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
na	na	k7c6	na
konci	konec	k1gInSc6	konec
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
výher	výhra	k1gFnPc2	výhra
než	než	k8xS	než
proher	prohra	k1gFnPc2	prohra
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kači-koši	kačioš	k1gMnSc3	kači-koš
a	a	k8xC	a
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
případě	případ	k1gInSc6	případ
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zápasníkovi	zápasník	k1gMnSc3	zápasník
postup	postup	k1gInSc1	postup
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Opak	opak	k1gInSc1	opak
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
proher	prohra	k1gFnPc2	prohra
<g/>
,	,	kIx,	,
než	než	k8xS	než
výher	výhra	k1gFnPc2	výhra
v	v	k7c6	v
turnaji	turnaj	k1gInSc6	turnaj
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
make-koši	makeoš	k1gMnPc1	make-koš
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
zápasníka	zápasník	k1gMnSc4	zápasník
sestup	sestup	k1gInSc4	sestup
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
(	(	kIx(	(
<g/>
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc4	dva
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
hodnosti	hodnost	k1gFnPc4	hodnost
<g/>
:	:	kIx,	:
jokozuna	jokozuna	k1gFnSc1	jokozuna
a	a	k8xC	a
ō	ō	k?	ō
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgMnPc4	který
platí	platit	k5eAaImIp3nS	platit
speciální	speciální	k2eAgNnPc4d1	speciální
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznamu	seznam	k1gInSc3	seznam
zápasníků	zápasník	k1gMnPc2	zápasník
pro	pro	k7c4	pro
příští	příští	k2eAgInSc4d1	příští
turnaj	turnaj	k1gInSc4	turnaj
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
banzuke	banzuke	k6eAd1	banzuke
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
neměnnou	neměnný	k2eAgFnSc7d1	neměnná
pozicí	pozice	k1gFnSc7	pozice
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
šampion	šampion	k1gMnSc1	šampion
jokozuna	jokozuna	k1gFnSc1	jokozuna
(	(	kIx(	(
<g/>
横	横	k?	横
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zápasníkovi	zápasník	k1gMnSc3	zápasník
nemožnost	nemožnost	k1gFnSc4	nemožnost
sestoupit	sestoupit	k5eAaPmF	sestoupit
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
zápasníka	zápasník	k1gMnSc2	zápasník
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
jokozuny	jokozuna	k1gFnSc2	jokozuna
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
vysoce	vysoce	k6eAd1	vysoce
nadprůměrná	nadprůměrný	k2eAgFnSc1d1	nadprůměrná
výkonnost	výkonnost	k1gFnSc1	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nepředstavitelné	představitelný	k2eNgNnSc1d1	nepředstavitelné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jokozuna	jokozuna	k1gFnSc1	jokozuna
zakončil	zakončit	k5eAaPmAgInS	zakončit
turnaj	turnaj	k1gInSc1	turnaj
s	s	k7c7	s
negativní	negativní	k2eAgFnSc7d1	negativní
bilancí	bilance	k1gFnSc7	bilance
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
kupříkladu	kupříkladu	k6eAd1	kupříkladu
i	i	k9	i
prohra	prohra	k1gFnSc1	prohra
třikrát	třikrát	k6eAd1	třikrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
či	či	k8xC	či
neschopnost	neschopnost	k1gFnSc1	neschopnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
na	na	k7c6	na
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
turnajových	turnajový	k2eAgInPc2d1	turnajový
titulů	titul	k1gInPc2	titul
v	v	k7c6	v
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
neméně	málo	k6eNd2	málo
důležitým	důležitý	k2eAgInSc7d1	důležitý
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c6	na
jokozunu	jokozun	k1gInSc6	jokozun
je	být	k5eAaImIp3nS	být
ctnost	ctnost	k1gFnSc1	ctnost
(	(	kIx(	(
<g/>
品	品	k?	品
hinkaku	hinkak	k1gInSc2	hinkak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
kategorie	kategorie	k1gFnSc1	kategorie
obnášející	obnášející	k2eAgFnSc1d1	obnášející
jak	jak	k8xC	jak
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
vystupování	vystupování	k1gNnSc1	vystupování
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
i	i	k8xC	i
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
celkové	celkový	k2eAgNnSc4d1	celkové
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vystupování	vystupování	k1gNnSc4	vystupování
včetně	včetně	k7c2	včetně
adherence	adherence	k1gFnSc2	adherence
k	k	k7c3	k
tradicím	tradice	k1gFnPc3	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Jokozuna	Jokozuna	k1gFnSc1	Jokozuna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
těmto	tento	k3xDgInPc3	tento
enormním	enormní	k2eAgInPc3d1	enormní
nárokům	nárok	k1gInPc3	nárok
na	na	k7c4	na
výkonnost	výkonnost	k1gFnSc4	výkonnost
a	a	k8xC	a
vystupování	vystupování	k1gNnSc4	vystupování
dostát	dostát	k5eAaPmF	dostát
buď	buď	k8xC	buď
ukončí	ukončit	k5eAaPmIp3nS	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
sám	sám	k3xTgInSc1	sám
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
donucen	donucen	k2eAgInSc1d1	donucen
tlakem	tlak	k1gInSc7	tlak
asociace	asociace	k1gFnSc2	asociace
<g/>
,	,	kIx,	,
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
divácké	divácký	k2eAgFnSc2d1	divácká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
odstupoval	odstupovat	k5eAaImAgMnS	odstupovat
jokozuna	jokozuna	k1gFnSc1	jokozuna
kvůli	kvůli	k7c3	kvůli
ztrátě	ztráta	k1gFnSc3	ztráta
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historie	historie	k1gFnSc1	historie
zná	znát	k5eAaImIp3nS	znát
i	i	k9	i
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zápasník	zápasník	k1gMnSc1	zápasník
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hodnosti	hodnost	k1gFnSc2	hodnost
donucen	donutit	k5eAaPmNgMnS	donutit
ukončit	ukončit	k5eAaPmF	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poklesků	poklesek	k1gInPc2	poklesek
v	v	k7c4	v
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
vystupování	vystupování	k1gNnSc4	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
příjem	příjem	k1gInSc1	příjem
dostávají	dostávat	k5eAaImIp3nP	dostávat
pouze	pouze	k6eAd1	pouze
zápasníci	zápasník	k1gMnPc1	zápasník
dvou	dva	k4xCgFnPc2	dva
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ti	ten	k3xDgMnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
užívat	užívat	k5eAaImF	užívat
titulu	titul	k1gInSc2	titul
sekitori	sekitor	k1gFnSc2	sekitor
(	(	kIx(	(
<g/>
vrcholový	vrcholový	k2eAgMnSc1d1	vrcholový
zápasník	zápasník	k1gMnSc1	zápasník
<g/>
)	)	kIx)	)
a	a	k8xC	a
jen	jen	k9	jen
oni	onen	k3xDgMnPc1	onen
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
mít	mít	k5eAaImF	mít
během	během	k7c2	během
turnajů	turnaj	k1gInPc2	turnaj
svázané	svázaný	k2eAgInPc4d1	svázaný
vlasy	vlas	k1gInPc4	vlas
do	do	k7c2	do
účesu	účes	k1gInSc2	účes
oičómage	oičómag	k1gInSc2	oičómag
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
šest	šest	k4xCc4	šest
velkých	velký	k2eAgInPc2d1	velký
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
japonských	japonský	k2eAgNnPc6d1	Japonské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
turnajů	turnaj	k1gInPc2	turnaj
činí	činit	k5eAaImIp3nS	činit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
patnáct	patnáct	k4xCc4	patnáct
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
spolu	spolu	k6eAd1	spolu
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
následuje	následovat	k5eAaImIp3nS	následovat
postup	postup	k1gInSc1	postup
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
nižších	nízký	k2eAgFnPc6d2	nižší
divizích	divize	k1gFnPc6	divize
(	(	kIx(	(
<g/>
džonokuči	džonokuč	k1gFnSc3	džonokuč
序	序	k?	序
<g/>
,	,	kIx,	,
džonidan	džonidan	k1gMnSc1	džonidan
序	序	k?	序
<g/>
,	,	kIx,	,
sandanme	sandanmat	k5eAaPmIp3nS	sandanmat
三	三	k?	三
a	a	k8xC	a
makušita	makušit	k2eAgFnSc1d1	makušit
幕	幕	k?	幕
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zápasník	zápasník	k1gMnSc1	zápasník
postoupil	postoupit	k5eAaPmAgMnS	postoupit
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
poměru	poměr	k1gInSc3	poměr
alespoň	alespoň	k9	alespoň
4	[number]	k4	4
vítězství	vítězství	k1gNnPc2	vítězství
ku	k	k7c3	k
3	[number]	k4	3
prohrám	prohra	k1gFnPc3	prohra
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
divize	divize	k1gFnSc1	divize
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
džúrjó	džúrjó	k?	džúrjó
(	(	kIx(	(
<g/>
十	十	k?	十
<g/>
)	)	kIx)	)
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
makuuči	makuuč	k1gFnSc6	makuuč
(	(	kIx(	(
<g/>
幕	幕	k?	幕
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
zápasníci	zápasník	k1gMnPc1	zápasník
bojují	bojovat	k5eAaImIp3nP	bojovat
jednou	jeden	k4xCgFnSc7	jeden
denně	denně	k6eAd1	denně
po	po	k7c6	po
celých	celá	k1gFnPc6	celá
15	[number]	k4	15
dní	den	k1gInPc2	den
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zápasník	zápasník	k1gMnSc1	zápasník
divize	divize	k1gFnSc1	divize
makuuči	makuuč	k1gFnSc3	makuuč
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
výher	výhra	k1gFnPc2	výhra
<g/>
.	.	kIx.	.
</s>
<s>
Výchova	výchova	k1gFnSc1	výchova
mladých	mladý	k2eAgMnPc2d1	mladý
adeptů	adept	k1gMnPc2	adept
ve	v	k7c6	v
stájích	stáj	k1gFnPc6	stáj
(	(	kIx(	(
<g/>
sumotori	sumotori	k6eAd1	sumotori
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
přísná	přísný	k2eAgFnSc1d1	přísná
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
žáci	žák	k1gMnPc1	žák
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
pro	pro	k7c4	pro
služebně	služebně	k6eAd1	služebně
starší	starý	k2eAgMnPc4d2	starší
zápasníky	zápasník	k1gMnPc4	zápasník
veškerou	veškerý	k3xTgFnSc4	veškerý
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
uklízení	uklízení	k1gNnSc2	uklízení
či	či	k8xC	či
vaření	vaření	k1gNnSc2	vaření
<g/>
.	.	kIx.	.
</s>
<s>
Zápasníci	zápasník	k1gMnPc1	zápasník
tráví	trávit	k5eAaImIp3nP	trávit
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
ženaté	ženatý	k2eAgMnPc4d1	ženatý
muže	muž	k1gMnPc4	muž
<g/>
)	)	kIx)	)
veškerou	veškerý	k3xTgFnSc4	veškerý
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
zápasnické	zápasnický	k2eAgFnSc2d1	zápasnická
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
stáj	stáj	k1gFnSc4	stáj
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
svojí	svůj	k3xOyFgFnSc7	svůj
absolutní	absolutní	k2eAgFnSc7d1	absolutní
kontrolou	kontrola	k1gFnSc7	kontrola
jediný	jediný	k2eAgMnSc1d1	jediný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ojakata	ojakata	k1gFnSc1	ojakata
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ojakataové	ojakataus	k1gMnPc1	ojakataus
jsou	být	k5eAaImIp3nP	být
bývalí	bývalý	k2eAgMnPc1d1	bývalý
zápasníci	zápasník	k1gMnPc1	zápasník
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
Japonské	japonský	k2eAgFnSc2d1	japonská
asociace	asociace	k1gFnSc2	asociace
sumó	sumó	k?	sumó
<g/>
.	.	kIx.	.
</s>
<s>
Stájí	stáj	k1gFnSc7	stáj
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
celkem	celkem	k6eAd1	celkem
44	[number]	k4	44
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
okamisan	okamisan	k1gInSc1	okamisan
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
vedoucího	vedoucí	k1gMnSc2	vedoucí
stáje	stáj	k1gFnSc2	stáj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stájí	stáj	k1gFnPc2	stáj
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
rodinné	rodinný	k2eAgInPc4d1	rodinný
vztahy	vztah	k1gInPc4	vztah
(	(	kIx(	(
<g/>
majitel	majitel	k1gMnSc1	majitel
je	být	k5eAaImIp3nS	být
pan	pan	k1gMnSc1	pan
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc4	žádný
přestupy	přestup	k1gInPc4	přestup
<g/>
.	.	kIx.	.
</s>
<s>
Zápasník	zápasník	k1gMnSc1	zápasník
může	moct	k5eAaImIp3nS	moct
stáj	stáj	k1gFnSc4	stáj
opustit	opustit	k5eAaPmF	opustit
jen	jen	k6eAd1	jen
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
–	–	k?	–
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ukončením	ukončení	k1gNnSc7	ukončení
sportovní	sportovní	k2eAgFnSc2d1	sportovní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
začíná	začínat	k5eAaImIp3nS	začínat
velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
níže	nízce	k6eAd2	nízce
v	v	k7c4	v
hierarchii	hierarchie	k1gFnSc4	hierarchie
je	být	k5eAaImIp3nS	být
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dříve	dříve	k6eAd2	dříve
vstává	vstávat	k5eAaImIp3nS	vstávat
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgInPc1d3	nejmladší
začínají	začínat	k5eAaImIp3nP	začínat
již	již	k9	již
ve	v	k7c4	v
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
zápasníci	zápasník	k1gMnPc1	zápasník
"	"	kIx"	"
<g/>
královské	královský	k2eAgFnSc2d1	královská
<g/>
"	"	kIx"	"
divize	divize	k1gFnSc2	divize
makuuči	makuuč	k1gInSc3	makuuč
přicházejí	přicházet	k5eAaImIp3nP	přicházet
na	na	k7c4	na
tréninky	trénink	k1gInPc4	trénink
až	až	k9	až
po	po	k7c6	po
osmé	osmý	k4xOgFnSc6	osmý
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Fyzickým	fyzický	k2eAgInSc7d1	fyzický
předpokladem	předpoklad	k1gInSc7	předpoklad
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
úspěchu	úspěch	k1gInSc2	úspěch
jsou	být	k5eAaImIp3nP	být
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
hbitost	hbitost	k1gFnSc4	hbitost
a	a	k8xC	a
pružnost	pružnost	k1gFnSc4	pružnost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
co	co	k3yInSc4	co
možná	možná	k9	možná
nejníže	nízce	k6eAd3	nízce
umístěným	umístěný	k2eAgNnSc7d1	umístěné
těžištěm	těžiště	k1gNnSc7	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zápasníci	zápasník	k1gMnPc1	zápasník
pravidelně	pravidelně	k6eAd1	pravidelně
provádějí	provádět	k5eAaImIp3nP	provádět
tři	tři	k4xCgNnPc1	tři
tradiční	tradiční	k2eAgNnPc1d1	tradiční
cvičení	cvičení	k1gNnPc1	cvičení
<g/>
:	:	kIx,	:
Ke	k	k7c3	k
cvičení	cvičení	k1gNnSc3	cvičení
šiko	šiko	k6eAd1	šiko
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
postoje	postoj	k1gInPc4	postoj
s	s	k7c7	s
široce	široko	k6eAd1	široko
rozkročenými	rozkročený	k2eAgNnPc7d1	rozkročené
chodidly	chodidlo	k1gNnPc7	chodidlo
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
se	se	k3xPyFc4	se
nadechnout	nadechnout	k5eAaPmF	nadechnout
<g/>
,	,	kIx,	,
naklonit	naklonit	k5eAaPmF	naklonit
se	se	k3xPyFc4	se
tělem	tělo	k1gNnSc7	tělo
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
zvednout	zvednout	k5eAaPmF	zvednout
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
co	co	k3yInSc1	co
nejvíce	hodně	k6eAd3	hodně
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
dupnout	dupnout	k5eAaPmF	dupnout
s	s	k7c7	s
hlasitým	hlasitý	k2eAgNnSc7d1	hlasité
vydechnutím	vydechnutí	k1gNnSc7	vydechnutí
Matawari	Matawar	k1gFnSc2	Matawar
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
sezení	sezení	k1gNnSc1	sezení
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
nohama	noha	k1gFnPc7	noha
roztaženýma	roztažený	k2eAgFnPc7d1	roztažená
co	co	k8xS	co
nejvíce	hodně	k6eAd3	hodně
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zápasník	zápasník	k1gMnSc1	zápasník
snaží	snažit	k5eAaImIp3nS	snažit
předklonit	předklonit	k5eAaPmF	předklonit
co	co	k3yQnSc1	co
nejvíce	hodně	k6eAd3	hodně
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
přitisknout	přitisknout	k5eAaPmF	přitisknout
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
celou	celý	k2eAgFnSc7d1	celá
horní	horní	k2eAgFnSc7d1	horní
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
Teppo	Teppa	k1gFnSc5	Teppa
je	být	k5eAaImIp3nS	být
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
sloup	sloup	k1gInSc4	sloup
<g/>
,	,	kIx,	,
zatlučený	zatlučený	k2eAgInSc4d1	zatlučený
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zápasník	zápasník	k1gMnSc1	zápasník
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
bije	bít	k5eAaImIp3nS	bít
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
rukou	ruka	k1gFnSc7	ruka
střídavě	střídavě	k6eAd1	střídavě
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
a	a	k8xC	a
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
cvičení	cvičení	k1gNnSc1	cvičení
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
schopnosti	schopnost	k1gFnPc4	schopnost
načasování	načasování	k1gNnSc2	načasování
a	a	k8xC	a
koordinace	koordinace	k1gFnSc2	koordinace
První	první	k4xOgFnSc1	první
denní	denní	k2eAgFnSc1d1	denní
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejhojnější	hojný	k2eAgFnSc7d3	nejhojnější
<g/>
)	)	kIx)	)
jídlo	jídlo	k1gNnSc1	jídlo
celého	celý	k2eAgInSc2d1	celý
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
čankonabe	čankonabat	k5eAaPmIp3nS	čankonabat
(	(	kIx(	(
<g/>
ち	ち	k?	ち
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
kalorické	kalorický	k2eAgFnPc4d1	kalorická
směsi	směs	k1gFnPc4	směs
z	z	k7c2	z
mořských	mořský	k2eAgFnPc2d1	mořská
chaluh	chaluha	k1gFnPc2	chaluha
<g/>
,	,	kIx,	,
kuřecího	kuřecí	k2eAgInSc2d1	kuřecí
<g/>
,	,	kIx,	,
vepřového	vepřový	k2eAgMnSc2d1	vepřový
a	a	k8xC	a
rybího	rybí	k2eAgNnSc2d1	rybí
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
tofu	tofu	k1gNnSc2	tofu
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Zápasníci	zápasník	k1gMnPc1	zápasník
jedí	jíst	k5eAaImIp3nP	jíst
čankonabe	čankonabat	k5eAaPmIp3nS	čankonabat
společně	společně	k6eAd1	společně
s	s	k7c7	s
miskami	miska	k1gFnPc7	miska
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
zapíjejí	zapíjet	k5eAaImIp3nP	zapíjet
vše	všechen	k3xTgNnSc4	všechen
pivem	pivo	k1gNnSc7	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnSc2	jméno
všech	všecek	k3xTgMnPc2	všecek
aktivních	aktivní	k2eAgMnPc2d1	aktivní
zápasníků	zápasník	k1gMnPc2	zápasník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
listina	listina	k1gFnSc1	listina
banzuke	banzuke	k1gFnSc1	banzuke
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Japonská	japonský	k2eAgFnSc1d1	japonská
asociace	asociace	k1gFnSc1	asociace
sumó	sumó	k?	sumó
vydává	vydávat	k5eAaPmIp3nS	vydávat
vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
každým	každý	k3xTgMnSc7	každý
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
každoročních	každoroční	k2eAgInPc2d1	každoroční
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gInSc1	originál
tohoto	tento	k3xDgInSc2	tento
dokumentu	dokument	k1gInSc2	dokument
je	být	k5eAaImIp3nS	být
vyhotoven	vyhotovit	k5eAaPmNgInS	vyhotovit
na	na	k7c6	na
rýžovém	rýžový	k2eAgInSc6d1	rýžový
papíru	papír	k1gInSc6	papír
Ogawa-waši	Ogawaaš	k1gMnSc3	Ogawa-waš
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
107	[number]	k4	107
<g/>
x	x	k?	x
<g/>
78	[number]	k4	78
cm	cm	kA	cm
a	a	k8xC	a
píší	psát	k5eAaImIp3nP	psát
jej	on	k3xPp3gMnSc4	on
dva	dva	k4xCgMnPc1	dva
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
možnému	možný	k2eAgInSc3d1	možný
úniku	únik	k1gInSc2	únik
informací	informace	k1gFnPc2	informace
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
izolováni	izolovat	k5eAaBmNgMnP	izolovat
od	od	k7c2	od
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
zápasník	zápasník	k1gMnSc1	zápasník
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
většími	veliký	k2eAgInPc7d2	veliký
znaky	znak	k1gInPc7	znak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
listiny	listina	k1gFnSc2	listina
jsou	být	k5eAaImIp3nP	být
uvedeni	uveden	k2eAgMnPc1d1	uveden
zápasníci	zápasník	k1gMnPc1	zápasník
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
divize	divize	k1gFnSc2	divize
makuuči	makuuč	k1gFnSc3	makuuč
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
seznam	seznam	k1gInSc4	seznam
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
písemný	písemný	k2eAgInSc1d1	písemný
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
listině	listina	k1gFnSc6	listina
banzuke	banzuke	k6eAd1	banzuke
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
května	květen	k1gInSc2	květen
1699	[number]	k4	1699
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
turnaj	turnaj	k1gInSc1	turnaj
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
kjótském	kjótský	k2eAgInSc6d1	kjótský
chrámu	chrám	k1gInSc6	chrám
Okazaki	Okazak	k1gFnSc2	Okazak
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
banzuke	banzuk	k1gInPc1	banzuk
byly	být	k5eAaImAgInP	být
vyhotoveny	vyhotovit	k5eAaPmNgInP	vyhotovit
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zápasníků	zápasník	k1gMnPc2	zápasník
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
tři	tři	k4xCgNnPc4	tři
jména	jméno	k1gNnPc4	jméno
–	–	k?	–
občanské	občanský	k2eAgNnSc1d1	občanské
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
šikona	šikona	k1gFnSc1	šikona
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dostává	dostávat	k5eAaImIp3nS	dostávat
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
své	svůj	k3xOyFgFnSc2	svůj
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
přebírá	přebírat	k5eAaImIp3nS	přebírat
některou	některý	k3yIgFnSc4	některý
ze	z	k7c2	z
stájí	stáj	k1gFnPc2	stáj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
novým	nový	k2eAgMnPc3d1	nový
zápasníkům	zápasník	k1gMnPc3	zápasník
zpravidla	zpravidla	k6eAd1	zpravidla
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
majitelé	majitel	k1gMnPc1	majitel
stájí	stáj	k1gFnPc2	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
složeniny	složenina	k1gFnSc2	složenina
několika	několik	k4yIc2	několik
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
má	mít	k5eAaImIp3nS	mít
správný	správný	k2eAgMnSc1d1	správný
zápasník	zápasník	k1gMnSc1	zápasník
sumó	sumó	k?	sumó
disponovat	disponovat	k5eAaBmF	disponovat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
málo	málo	k4c6	málo
případech	případ	k1gInPc6	případ
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
si	se	k3xPyFc3	se
zápasníci	zápasník	k1gMnPc1	zápasník
zvolili	zvolit	k5eAaPmAgMnP	zvolit
své	svůj	k3xOyFgNnSc4	svůj
občanské	občanský	k2eAgNnSc4d1	občanské
jméno	jméno	k1gNnSc4	jméno
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
sportovní	sportovní	k2eAgMnPc4d1	sportovní
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
sportovního	sportovní	k2eAgNnSc2d1	sportovní
jména	jméno	k1gNnSc2	jméno
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c4	mezi
zápasníky	zápasník	k1gMnPc4	zápasník
množství	množství	k1gNnSc1	množství
róninů	rónin	k1gInPc2	rónin
(	(	kIx(	(
<g/>
samurajů	samuraj	k1gMnPc2	samuraj
bez	bez	k7c2	bez
pána	pán	k1gMnSc2	pán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nechtěli	chtít	k5eNaImAgMnP	chtít
zápasit	zápasit	k5eAaImF	zápasit
pod	pod	k7c7	pod
svými	svůj	k3xOyFgNnPc7	svůj
skutečnými	skutečný	k2eAgNnPc7d1	skutečné
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Obřad	obřad	k1gInSc1	obřad
"	"	kIx"	"
<g/>
stříhání	stříhání	k1gNnSc1	stříhání
vlasů	vlas	k1gInPc2	vlas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
danpacušiki	danpacušiki	k6eAd1	danpacušiki
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obřad	obřad	k1gInSc4	obřad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zápasník	zápasník	k1gMnSc1	zápasník
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
(	(	kIx(	(
<g/>
intai-zumó	intaiumó	k?	intai-zumó
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápasník	zápasník	k1gMnSc1	zápasník
si	se	k3xPyFc3	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
oblékne	obléknout	k5eAaPmIp3nS	obléknout
své	svůj	k3xOyFgNnSc4	svůj
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
kimono	kimono	k1gNnSc4	kimono
a	a	k8xC	a
s	s	k7c7	s
naolejovanými	naolejovaný	k2eAgInPc7d1	naolejovaný
vlasy	vlas	k1gInPc7	vlas
se	se	k3xPyFc4	se
posadí	posadit	k5eAaPmIp3nS	posadit
do	do	k7c2	do
středu	střed	k1gInSc2	střed
zápasnického	zápasnický	k2eAgInSc2d1	zápasnický
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
stojí	stát	k5eAaImIp3nS	stát
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
s	s	k7c7	s
pozlacenými	pozlacený	k2eAgFnPc7d1	pozlacená
nůžkami	nůžky	k1gFnPc7	nůžky
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnPc1d1	sportovní
kolegové	kolega	k1gMnPc1	kolega
a	a	k8xC	a
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
odstřihávají	odstřihávat	k5eAaImIp3nP	odstřihávat
prameny	pramen	k1gInPc4	pramen
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
zápasníkem	zápasník	k1gMnSc7	zápasník
sumó	sumó	k?	sumó
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Pavel	Pavel	k1gMnSc1	Pavel
Bojar	bojar	k1gMnSc1	bojar
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Takanojama	Takanojam	k1gMnSc4	Takanojam
Šuntaró	Šuntaró	k1gFnSc2	Šuntaró
<g/>
.	.	kIx.	.
</s>
