<p>
<s>
Černí	černý	k2eAgMnPc1d1	černý
baroni	baron	k1gMnPc1	baron
jsou	být	k5eAaImIp3nP	být
satirický	satirický	k2eAgInSc4d1	satirický
román	román	k1gInSc4	román
Miloslava	Miloslav	k1gMnSc2	Miloslav
Švandrlíka	Švandrlík	k1gMnSc2	Švandrlík
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
Pomocných	pomocný	k2eAgInPc2d1	pomocný
technických	technický	k2eAgInPc2d1	technický
praporů	prapor	k1gInPc2	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
ovšem	ovšem	k9	ovšem
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
reality	realita	k1gFnSc2	realita
Technických	technický	k2eAgInPc2d1	technický
praporů	prapor	k1gInPc2	prapor
(	(	kIx(	(
<g/>
TP	TP	kA	TP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
Pomocných	pomocný	k2eAgInPc2d1	pomocný
technických	technický	k2eAgInPc2d1	technický
praporů	prapor	k1gInPc2	prapor
(	(	kIx(	(
<g/>
PTP	PTP	kA	PTP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Podtitul	podtitul	k1gInSc4	podtitul
Válčili	válčit	k5eAaImAgMnP	válčit
jsme	být	k5eAaImIp1nP	být
za	za	k7c4	za
Čepičky	čepička	k1gFnPc4	čepička
upomíná	upomínat	k5eAaImIp3nS	upomínat
na	na	k7c4	na
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
ministra	ministr	k1gMnSc4	ministr
obrany	obrana	k1gFnSc2	obrana
Alexeje	Alexej	k1gMnSc4	Alexej
Čepičku	čepička	k1gFnSc4	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
námětem	námět	k1gInSc7	námět
pro	pro	k7c4	pro
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geneze	geneze	k1gFnSc1	geneze
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
má	mít	k5eAaImIp3nS	mít
román	román	k1gInSc4	román
17	[number]	k4	17
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
kapitolou	kapitola	k1gFnSc7	kapitola
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Kefalín	Kefalína	k1gFnPc2	Kefalína
<g/>
,	,	kIx,	,
zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
z	z	k7c2	z
prodloužení	prodloužení	k1gNnSc2	prodloužení
služby	služba	k1gFnSc2	služba
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
opije	opít	k5eAaPmIp3nS	opít
jablečným	jablečný	k2eAgNnSc7d1	jablečné
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
zabaveno	zabavit	k5eAaPmNgNnS	zabavit
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
vydání	vydání	k1gNnPc1	vydání
vycházela	vycházet	k5eAaImAgNnP	vycházet
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
dílo	dílo	k1gNnSc1	dílo
šířilo	šířit	k5eAaImAgNnS	šířit
samizdatově	samizdatově	k6eAd1	samizdatově
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
300000	[number]	k4	300000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
29	[number]	k4	29
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Miloslav	Miloslav	k1gMnSc1	Miloslav
Švandrlík	Švandrlík	k1gMnSc1	Švandrlík
nikdy	nikdy	k6eAd1	nikdy
u	u	k7c2	u
Pomocných	pomocný	k2eAgInPc2d1	pomocný
technických	technický	k2eAgInPc2d1	technický
praporů	prapor	k1gInPc2	prapor
nesloužil	sloužit	k5eNaImAgInS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
nástupnické	nástupnický	k2eAgFnSc2d1	nástupnická
organizace	organizace	k1gFnSc2	organizace
Technických	technický	k2eAgInPc2d1	technický
praporů	prapor	k1gInPc2	prapor
(	(	kIx(	(
<g/>
TP	TP	kA	TP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
zařízeních	zařízení	k1gNnPc6	zařízení
již	již	k6eAd1	již
však	však	k9	však
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
i	i	k9	i
politicky	politicky	k6eAd1	politicky
spolehliví	spolehlivý	k2eAgMnPc1d1	spolehlivý
a	a	k8xC	a
celkové	celkový	k2eAgFnPc1d1	celková
poměry	poměra	k1gFnPc1	poměra
byly	být	k5eAaImAgFnP	být
snesitelnější	snesitelný	k2eAgFnPc1d2	snesitelnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
PTP	PTP	kA	PTP
sloužili	sloužit	k5eAaImAgMnP	sloužit
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
nespolehliví	spolehlivý	k2eNgMnPc1d1	nespolehlivý
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
za	za	k7c4	za
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
TP	TP	kA	TP
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgInS	sloužit
Švandrlík	Švandrlík	k1gInSc1	Švandrlík
<g/>
,	,	kIx,	,
sloužili	sloužit	k5eAaImAgMnP	sloužit
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
velmi	velmi	k6eAd1	velmi
sníženou	snížený	k2eAgFnSc4d1	snížená
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ne	ne	k9	ne
za	za	k7c4	za
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
armáda	armáda	k1gFnSc1	armáda
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
do	do	k7c2	do
bojových	bojový	k2eAgInPc2d1	bojový
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
<g/>
Takže	takže	k9	takže
tito	tento	k3xDgMnPc1	tento
tvz	tvz	k?	tvz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
štáckriplové	štáckripl	k1gMnPc1	štáckripl
<g/>
"	"	kIx"	"
plnili	plnit	k5eAaImAgMnP	plnit
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Knížka	knížka	k1gFnSc1	knížka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
dostat	dostat	k5eAaPmF	dostat
samotný	samotný	k2eAgInSc1d1	samotný
fenomén	fenomén	k1gInSc1	fenomén
PTP	PTP	kA	PTP
<g/>
/	/	kIx~	/
<g/>
TP	TP	kA	TP
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
falešnou	falešný	k2eAgFnSc4d1	falešná
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
PTP	PTP	kA	PTP
jako	jako	k9	jako
"	"	kIx"	"
<g/>
veselé	veselý	k2eAgFnSc6d1	veselá
<g/>
"	"	kIx"	"
vojně	vojna	k1gFnSc6	vojna
plné	plný	k2eAgFnSc6d1	plná
absurdních	absurdní	k2eAgFnPc2d1	absurdní
situací	situace	k1gFnPc2	situace
s	s	k7c7	s
pologramotnými	pologramotný	k2eAgFnPc7d1	pologramotná
nadřízenými	nadřízená	k1gFnPc7	nadřízená
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vojákům	voják	k1gMnPc3	voják
nehrozilo	hrozit	k5eNaImAgNnS	hrozit
žádné	žádný	k3yNgNnSc1	žádný
reálné	reálný	k2eAgNnSc1d1	reálné
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Realitou	realita	k1gFnSc7	realita
PTP	PTP	kA	PTP
však	však	k8xC	však
byly	být	k5eAaImAgFnP	být
špatné	špatný	k2eAgFnPc4d1	špatná
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
i	i	k8xC	i
pracovní	pracovní	k2eAgFnPc4d1	pracovní
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
pracovních	pracovní	k2eAgInPc2d1	pracovní
úrazů	úraz	k1gInPc2	úraz
včetně	včetně	k7c2	včetně
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
pétépáci	pétépáci	k?	pétépáci
tento	tento	k3xDgInSc4	tento
falešný	falešný	k2eAgInSc4d1	falešný
obraz	obraz	k1gInSc4	obraz
Švandrlíkovi	Švandrlíkův	k2eAgMnPc1d1	Švandrlíkův
často	často	k6eAd1	často
vytýkali	vytýkat	k5eAaImAgMnP	vytýkat
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Technické	technický	k2eAgInPc1d1	technický
prapory	prapor	k1gInPc1	prapor
byly	být	k5eAaImAgFnP	být
logickým	logický	k2eAgNnSc7d1	logické
pokračováním	pokračování	k1gNnSc7	pokračování
'	'	kIx"	'
<g/>
pétépáků	pétépáků	k?	pétépáků
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
ve	v	k7c6	v
třiapadesátém	třiapadesátý	k4xOgInSc6	třiapadesátý
roce	rok	k1gInSc6	rok
narukoval	narukovat	k5eAaPmAgMnS	narukovat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
neřeklo	říct	k5eNaPmAgNnS	říct
<g/>
...	...	k?	...
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xS	jako
u	u	k7c2	u
PTP	PTP	kA	PTP
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
ne	ne	k9	ne
už	už	k6eAd1	už
asi	asi	k9	asi
tak	tak	k6eAd1	tak
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
osmačtyřicátém	osmačtyřicátý	k4xOgInSc6	osmačtyřicátý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Námětem	námět	k1gInSc7	námět
blízké	blízký	k2eAgInPc4d1	blízký
romány	román	k1gInPc4	román
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
Tankový	tankový	k2eAgInSc4d1	tankový
prapor	prapor	k1gInSc4	prapor
a	a	k8xC	a
Kunderův	Kunderův	k2eAgInSc4d1	Kunderův
Žert	žert	k1gInSc4	žert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
