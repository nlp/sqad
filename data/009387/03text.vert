<p>
<s>
Bratčice	Bratčice	k1gFnPc1	Bratčice
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
Dyjsko-svrateckém	dyjskovratecký	k2eAgInSc6d1	dyjsko-svratecký
úvalu	úval	k1gInSc6	úval
<g/>
,	,	kIx,	,
obcí	obec	k1gFnSc7	obec
protékají	protékat	k5eAaImIp3nP	protékat
potoky	potok	k1gInPc1	potok
Lejtna	Lejtn	k1gInSc2	Lejtn
a	a	k8xC	a
Šatava	Šatava	k1gFnSc1	Šatava
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
708	[number]	k4	708
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgNnPc4d1	velké
ložiska	ložisko	k1gNnPc4	ložisko
vysoce	vysoce	k6eAd1	vysoce
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
těží	těžet	k5eAaImIp3nS	těžet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Bratčic	Bratčice	k1gFnPc2	Bratčice
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
skládka	skládka	k1gFnSc1	skládka
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
firmou	firma	k1gFnSc7	firma
STAVOS	STAVOS	kA	STAVOS
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
vinařské	vinařský	k2eAgFnSc6d1	vinařská
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
viniční	viniční	k2eAgFnPc1d1	viniční
tratě	trať	k1gFnPc1	trať
Široké-Klinky	Široké-Klinka	k1gFnPc1	Široké-Klinka
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgFnPc1d1	Staré
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Bratčicích	Bratčice	k1gFnPc6	Bratčice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1537	[number]	k4	1537
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Jiřího	Jiří	k1gMnSc2	Jiří
Žabky	Žabka	k1gMnSc2	Žabka
z	z	k7c2	z
Limberka	Limberka	k1gFnSc1	Limberka
<g/>
,	,	kIx,	,
místokancléře	místokancléř	k1gMnSc2	místokancléř
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
jistě	jistě	k9	jistě
jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgFnSc4d2	starší
ves	ves	k1gFnSc4	ves
(	(	kIx(	(
<g/>
snad	snad	k9	snad
už	už	k6eAd1	už
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
majetkem	majetek	k1gInSc7	majetek
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Kounicích	Kounice	k1gFnPc6	Kounice
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
zániku	zánik	k1gInSc6	zánik
ji	on	k3xPp3gFnSc4	on
získali	získat	k5eAaPmAgMnP	získat
právě	právě	k9	právě
Žabkové	Žabka	k1gMnPc1	Žabka
z	z	k7c2	z
Limberka	Limberka	k1gFnSc1	Limberka
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1567	[number]	k4	1567
připojili	připojit	k5eAaPmAgMnP	připojit
Bratčice	Bratčice	k1gFnPc4	Bratčice
k	k	k7c3	k
prštickému	prštický	k2eAgInSc3d1	prštický
statku	statek	k1gInSc3	statek
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
se	se	k3xPyFc4	se
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
vsi	ves	k1gFnSc2	ves
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
více	hodně	k6eAd2	hodně
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1732	[number]	k4	1732
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
židlochovickému	židlochovický	k2eAgNnSc3d1	židlochovické
panství	panství	k1gNnSc3	panství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
1980	[number]	k4	1980
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
obcí	obec	k1gFnPc2	obec
Bratčice	Bratčice	k1gFnSc2	Bratčice
<g/>
,	,	kIx,	,
Ledce	Ledce	k1gFnSc2	Ledce
<g/>
,	,	kIx,	,
Sobotovice	Sobotovice	k1gFnSc2	Sobotovice
a	a	k8xC	a
Syrovice	syrovice	k1gFnSc2	syrovice
pod	pod	k7c4	pod
společný	společný	k2eAgInSc4d1	společný
Místní	místní	k2eAgInSc4d1	místní
národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
v	v	k7c6	v
Syrovicích	syrovice	k1gFnPc6	syrovice
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
formálnímu	formální	k2eAgNnSc3d1	formální
sloučení	sloučení	k1gNnSc3	sloučení
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
integrovaném	integrovaný	k2eAgInSc6d1	integrovaný
Místním	místní	k2eAgInSc6d1	místní
národním	národní	k2eAgInSc6d1	národní
výboře	výboř	k1gFnSc2	výboř
měly	mít	k5eAaImAgFnP	mít
Bratčice	Bratčice	k1gFnPc1	Bratčice
21	[number]	k4	21
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
osamostatnění	osamostatnění	k1gNnSc3	osamostatnění
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1779	[number]	k4	1779
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
</s>
</p>
<p>
<s>
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
bratří	bratr	k1gMnPc2	bratr
Jakšů	Jakš	k1gMnPc2	Jakš
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
padlých	padlý	k1gMnPc2	padlý
z	z	k7c2	z
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Bratčická	Bratčický	k2eAgFnSc1d1	Bratčický
rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
sousedních	sousední	k2eAgFnPc2d1	sousední
Sobotovic	Sobotovice	k1gFnPc2	Sobotovice
</s>
</p>
<p>
<s>
==	==	k?	==
Společenský	společenský	k2eAgInSc4d1	společenský
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Samospráva	samospráva	k1gFnSc1	samospráva
obce	obec	k1gFnSc2	obec
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
moravskou	moravský	k2eAgFnSc4d1	Moravská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vilda	Vilda	k1gMnSc1	Vilda
Jakš	Jakš	k1gMnSc1	Jakš
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
boxer	boxer	k1gMnSc1	boxer
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
letec	letec	k1gMnSc1	letec
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Bratčice	Bratčice	k1gFnSc2	Bratčice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bratčice	Bratčice	k1gFnSc2	Bratčice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bratčice	Bratčice	k1gFnSc2	Bratčice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
