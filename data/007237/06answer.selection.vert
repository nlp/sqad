<s>
Počátkem	počátkem	k7c2	počátkem
strojního	strojní	k2eAgInSc2d1	strojní
pohonu	pohon	k1gInSc2	pohon
v	v	k7c6	v
kolejové	kolejový	k2eAgFnSc6d1	kolejová
dopravě	doprava	k1gFnSc6	doprava
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1804	[number]	k4	1804
<g/>
,	,	kIx,	,
když	když	k8xS	když
Richard	Richard	k1gMnSc1	Richard
Trevithick	Trevithick	k1gMnSc1	Trevithick
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
první	první	k4xOgFnSc4	první
parní	parní	k2eAgFnSc4d1	parní
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
