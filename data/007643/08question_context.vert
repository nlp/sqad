<s>
Gobi	Gobi	k1gFnSc1	Gobi
(	(	kIx(	(
<g/>
mongolsky	mongolsky	k6eAd1	mongolsky
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Gov	Gov	k1gFnSc1	Gov
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
戈	戈	k?	戈
<g/>
,	,	kIx,	,
pinyin	pinyin	k2eAgMnSc1d1	pinyin
Gē	Gē	k1gMnSc1	Gē
Shā	Shā	k1gMnSc1	Shā
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Ke-pi	Ke	k1gFnSc2	Ke-p
ša-mo	šao	k6eAd1	ša-mo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
a	a	k8xC	a
největších	veliký	k2eAgFnPc2d3	veliký
pouští	poušť	k1gFnPc2	poušť
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
suchým	suchý	k2eAgFnPc3d1	suchá
oblastem	oblast	k1gFnPc3	oblast
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
nepravidelností	nepravidelnost	k1gFnPc2	nepravidelnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
značně	značně	k6eAd1	značně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
jižním	jižní	k2eAgNnSc6d1	jižní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
ohraničena	ohraničen	k2eAgFnSc1d1	ohraničena
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Altaj	Altaj	k1gInSc4	Altaj
a	a	k8xC	a
travnatými	travnatý	k2eAgFnPc7d1	travnatá
stepmi	step	k1gFnPc7	step
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
náhorní	náhorní	k2eAgFnSc7d1	náhorní
plošinou	plošina	k1gFnSc7	plošina
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Čínskou	čínský	k2eAgFnSc7d1	čínská
nížinou	nížina	k1gFnSc7	nížina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
Gobi	Gobi	k1gFnPc2	Gobi
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
jednotně	jednotně	k6eAd1	jednotně
definovány	definovat	k5eAaBmNgFnP	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
nejednoznačnost	nejednoznačnost	k1gFnSc1	nejednoznačnost
samotného	samotný	k2eAgInSc2d1	samotný
výrazu	výraz	k1gInSc2	výraz
г	г	k?	г
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
i	i	k8xC	i
Vnějším	vnější	k2eAgNnSc6d1	vnější
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
neoznačuje	označovat	k5eNaImIp3nS	označovat
pouze	pouze	k6eAd1	pouze
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
obecněji	obecně	k6eAd2	obecně
typ	typ	k1gInSc4	typ
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
Gobi	Gobi	k1gFnSc2	Gobi
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
veškeré	veškerý	k3xTgFnPc4	veškerý
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oblasti	oblast	k1gFnPc4	oblast
od	od	k7c2	od
Pamíru	Pamír	k1gInSc2	Pamír
(	(	kIx(	(
<g/>
77	[number]	k4	77
<g/>
°	°	k?	°
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
k	k	k7c3	k
Chinganu	Chingan	k1gMnSc3	Chingan
(	(	kIx(	(
<g/>
116	[number]	k4	116
až	až	k9	až
118	[number]	k4	118
<g/>
°	°	k?	°
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
Altaje	Altaj	k1gInSc2	Altaj
<g/>
,	,	kIx,	,
Changaje	Changaj	k1gInSc2	Changaj
a	a	k8xC	a
úpatí	úpatí	k1gNnSc6	úpatí
Zabajkalských	zabajkalský	k2eAgFnPc2d1	zabajkalský
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
severu	sever	k1gInSc6	sever
po	po	k7c4	po
Kchun-lun	Kchunun	k1gInSc4	Kchun-lun
a	a	k8xC	a
Severočínskou	severočínský	k2eAgFnSc4d1	Severočínská
nížinu	nížina	k1gFnSc4	nížina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
vymezení	vymezení	k1gNnSc1	vymezení
by	by	kYmCp3nS	by
pod	pod	k7c7	pod
Gobi	Gobi	k1gFnPc7	Gobi
řadilo	řadit	k5eAaImAgNnS	řadit
mj.	mj.	kA	mj.
i	i	k9	i
poušť	poušť	k1gFnSc1	poušť
Taklamakan	Taklamakany	k1gInPc2	Taklamakany
<g/>
,	,	kIx,	,
Džungarskou	Džungarský	k2eAgFnSc4d1	Džungarská
pánev	pánev	k1gFnSc4	pánev
a	a	k8xC	a
Ordoskou	Ordoska	k1gFnSc7	Ordoska
plošinu	plošina	k1gFnSc4	plošina
<g/>
;	;	kIx,	;
tyto	tento	k3xDgFnPc4	tento
pouště	poušť	k1gFnPc4	poušť
nicméně	nicméně	k8xC	nicméně
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
pánve	pánev	k1gFnPc4	pánev
a	a	k8xC	a
přinejmenším	přinejmenším	k6eAd1	přinejmenším
z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Théodore	Théodor	k1gMnSc5	Théodor
Monod	Monoda	k1gFnPc2	Monoda
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
omezil	omezit	k5eAaPmAgInS	omezit
Gobi	Gobi	k1gNnSc3	Gobi
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
a	a	k8xC	a
sever	sever	k1gInSc1	sever
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgNnSc4d1	pouštní
pohoří	pohoří	k1gNnSc4	pohoří
Alašan	Alašana	k1gFnPc2	Alašana
a	a	k8xC	a
Pej-šan	Pej-šana	k1gFnPc2	Pej-šana
na	na	k7c6	na
západě	západ	k1gInSc6	západ
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
už	už	k6eAd1	už
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatné	samostatný	k2eAgInPc4d1	samostatný
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
Altaj	Altaj	k1gInSc1	Altaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Gobi	Gobi	k1gFnSc2	Gobi
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
severomongolskou	severomongolský	k2eAgFnSc4d1	severomongolský
step	step	k1gFnSc4	step
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ji	on	k3xPp3gFnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Mandžusko	Mandžusko	k1gNnSc1	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
Gobi	Gobi	k1gFnSc1	Gobi
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
70	[number]	k4	70
km	km	kA	km
k	k	k7c3	k
Pekingu	Peking	k1gInSc3	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
300	[number]	k4	300
000	[number]	k4	000
km2	km2	k4	km2
se	se	k3xPyFc4	se
Gobi	Gobi	k1gFnSc1	Gobi
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
pouště	poušť	k1gFnPc4	poušť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>

