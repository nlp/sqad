<s>
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
nežná	žný	k2eNgFnSc1d1	žný
revolúcia	revolúcia	k1gFnSc1	revolúcia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
období	období	k1gNnSc2	období
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
mezi	mezi	k7c7	mezi
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
přeměně	přeměna	k1gFnSc6	přeměna
politického	politický	k2eAgNnSc2d1	politické
zřízení	zřízení	k1gNnSc2	zřízení
na	na	k7c4	na
pluralitní	pluralitní	k2eAgFnSc4d1	pluralitní
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
změn	změna	k1gFnPc2	změna
přispěl	přispět	k5eAaPmAgInS	přispět
rozpad	rozpad	k1gInSc1	rozpad
bývalého	bývalý	k2eAgInSc2d1	bývalý
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
narůstající	narůstající	k2eAgFnSc4d1	narůstající
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
s	s	k7c7	s
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
a	a	k8xC	a
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
událostí	událost	k1gFnPc2	událost
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
demonstranti	demonstrant	k1gMnPc1	demonstrant
napadeni	napadnout	k5eAaPmNgMnP	napadnout
Veřejnou	veřejný	k2eAgFnSc7d1	veřejná
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
revoluce	revoluce	k1gFnSc1	revoluce
provázena	provázen	k2eAgFnSc1d1	provázena
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
během	běh	k1gInSc7	běh
státního	státní	k2eAgInSc2d1	státní
převratu	převrat	k1gInSc2	převrat
nebyl	být	k5eNaImAgInS	být
zmařen	zmařen	k2eAgInSc1d1	zmařen
jediný	jediný	k2eAgInSc1d1	jediný
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nenásilný	násilný	k2eNgInSc4d1	nenásilný
charakter	charakter	k1gInSc4	charakter
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
revoluce	revoluce	k1gFnSc1	revoluce
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nebylo	být	k5eNaImAgNnS	být
pro	pro	k7c4	pro
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
použíto	použíto	k1gNnSc1	použíto
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
revoluční	revoluční	k2eAgFnSc1d1	revoluční
<g/>
,	,	kIx,	,
celospolečenská	celospolečenský	k2eAgFnSc1d1	celospolečenská
změna	změna	k1gFnSc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
poprvé	poprvé	k6eAd1	poprvé
užil	užít	k5eAaPmAgMnS	užít
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Pitharta	Pithart	k1gMnSc2	Pithart
panuje	panovat	k5eAaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
nějací	nějaký	k3yIgMnPc1	nějaký
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
novináři	novinář	k1gMnPc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
využívat	využívat	k5eAaImF	využívat
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
revoluce	revoluce	k1gFnPc4	revoluce
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
získána	získat	k5eAaPmNgFnS	získat
bez	bez	k7c2	bez
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Využíval	využívat	k5eAaImAgMnS	využívat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
i	i	k9	i
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
"	"	kIx"	"
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
barevné	barevný	k2eAgFnSc2d1	barevná
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
prezidenta	prezident	k1gMnSc2	prezident
Eduarda	Eduard	k1gMnSc2	Eduard
Ševardnadzeho	Ševardnadze	k1gMnSc2	Ševardnadze
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Listopad	listopad	k1gInSc1	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začínal	začínat	k5eAaImAgInS	začínat
hroutit	hroutit	k5eAaImF	hroutit
Východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
díky	díky	k7c3	díky
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
občanů	občan	k1gMnPc2	občan
s	s	k7c7	s
politickým	politický	k2eAgInSc7d1	politický
a	a	k8xC	a
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
vývojem	vývoj	k1gInSc7	vývoj
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
masové	masový	k2eAgInPc4d1	masový
protesty	protest	k1gInPc4	protest
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
nezávislých	závislý	k2eNgInPc2d1	nezávislý
odborů	odbor	k1gInPc2	odbor
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
členy	člen	k1gInPc4	člen
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
okolo	okolo	k7c2	okolo
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
nátlacích	nátlak	k1gInPc6	nátlak
a	a	k8xC	a
protestech	protest	k1gInPc6	protest
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
k	k	k7c3	k
legalizaci	legalizace	k1gFnSc3	legalizace
Solidarity	solidarita	k1gFnSc2	solidarita
a	a	k8xC	a
k	k	k7c3	k
částečným	částečný	k2eAgFnPc3d1	částečná
svobodným	svobodný	k2eAgFnPc3d1	svobodná
volbám	volba	k1gFnPc3	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
drtivě	drtivě	k6eAd1	drtivě
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
nekomunističtí	komunistický	k2eNgMnPc1d1	nekomunistický
zástupci	zástupce	k1gMnPc1	zástupce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
dominový	dominový	k2eAgInSc1d1	dominový
efekt	efekt	k1gInSc1	efekt
pádů	pád	k1gInPc2	pád
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
pády	pád	k1gInPc1	pád
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
ve	v	k7c6	v
Východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
spadající	spadající	k2eAgFnPc4d1	spadající
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
představy	představ	k1gInPc1	představ
o	o	k7c6	o
demokratickém	demokratický	k2eAgNnSc6d1	demokratické
pokračování	pokračování	k1gNnSc6	pokračování
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
zdály	zdát	k5eAaImAgFnP	zdát
opodstatněné	opodstatněný	k2eAgFnPc1d1	opodstatněná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
36	[number]	k4	36
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
představy	představa	k1gFnSc2	představa
demokratického	demokratický	k2eAgInSc2d1	demokratický
vývoje	vývoj	k1gInSc2	vývoj
vzaly	vzít	k5eAaPmAgInP	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
komunisté	komunista	k1gMnPc1	komunista
uchvátili	uchvátit	k5eAaPmAgMnP	uchvátit
moc	moc	k6eAd1	moc
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
státního	státní	k2eAgInSc2d1	státní
převratu	převrat	k1gInSc2	převrat
(	(	kIx(	(
<g/>
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
jako	jako	k8xS	jako
Vítězný	vítězný	k2eAgInSc4d1	vítězný
únor	únor	k1gInSc4	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
roky	rok	k1gInPc4	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
k	k	k7c3	k
vykonstruovaným	vykonstruovaný	k2eAgInPc3d1	vykonstruovaný
politickým	politický	k2eAgInPc3d1	politický
procesům	proces	k1gInPc3	proces
proti	proti	k7c3	proti
politickým	politický	k2eAgMnPc3d1	politický
oponentům	oponent	k1gMnPc3	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
zastrašována	zastrašovat	k5eAaImNgFnS	zastrašovat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
stalinistických	stalinistický	k2eAgFnPc2d1	stalinistická
praktik	praktika	k1gFnPc2	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
částečnému	částečný	k2eAgNnSc3d1	částečné
uvolnění	uvolnění	k1gNnSc3	uvolnění
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyústily	vyústit	k5eAaPmAgInP	vyústit
v	v	k7c4	v
Pražské	pražský	k2eAgNnSc4d1	Pražské
jaro	jaro	k1gNnSc4	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
krátko	krátko	k6eAd1	krátko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odklonu	odklon	k1gInSc3	odklon
od	od	k7c2	od
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
linie	linie	k1gFnSc2	linie
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
snaha	snaha	k1gFnSc1	snaha
vnést	vnést	k5eAaPmF	vnést
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
prvky	prvek	k1gInPc1	prvek
demokratického	demokratický	k2eAgInSc2d1	demokratický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
oproštění	oproštění	k1gNnSc1	oproštění
se	se	k3xPyFc4	se
od	od	k7c2	od
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
odsouzení	odsouzení	k1gNnSc1	odsouzení
politických	politický	k2eAgInPc2d1	politický
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
otevření	otevření	k1gNnSc4	otevření
se	se	k3xPyFc4	se
západu	západ	k1gInSc3	západ
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
socialismus	socialismus	k1gInSc1	socialismus
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
intervencí	intervence	k1gFnSc7	intervence
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
navrácen	navrácen	k2eAgInSc4d1	navrácen
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
komunistický	komunistický	k2eAgInSc4d1	komunistický
směr	směr	k1gInSc4	směr
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
období	období	k1gNnSc1	období
normalizace	normalizace	k1gFnSc2	normalizace
a	a	k8xC	a
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
období	období	k1gNnPc2	období
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
straně	strana	k1gFnSc6	strana
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
čistky	čistka	k1gFnPc4	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
třetí	třetí	k4xOgMnSc1	třetí
člen	člen	k1gMnSc1	člen
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
000	[number]	k4	000
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubčka	k1gFnPc2	Dubčka
byl	být	k5eAaImAgInS	být
odsunut	odsunout	k5eAaPmNgInS	odsunout
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
na	na	k7c4	na
post	post	k1gInSc4	post
ředitele	ředitel	k1gMnSc2	ředitel
lesního	lesní	k2eAgInSc2d1	lesní
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
nepohodlných	pohodlný	k2eNgMnPc2d1	nepohodlný
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
či	či	k8xC	či
kulturních	kulturní	k2eAgFnPc2d1	kulturní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
-	-	kIx~	-
oproti	oproti	k7c3	oproti
Polsku	Polsko	k1gNnSc3	Polsko
či	či	k8xC	či
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
-	-	kIx~	-
poměrně	poměrně	k6eAd1	poměrně
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
jistou	jistý	k2eAgFnSc4d1	jistá
míru	míra	k1gFnSc4	míra
blahobytu	blahobyt	k1gInSc2	blahobyt
a	a	k8xC	a
tedy	tedy	k9	tedy
stabilitu	stabilita	k1gFnSc4	stabilita
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
zaostávala	zaostávat	k5eAaImAgFnS	zaostávat
za	za	k7c7	za
zeměmi	zem	k1gFnPc7	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
potíže	potíž	k1gFnPc1	potíž
nedoléhaly	doléhat	k5eNaImAgFnP	doléhat
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
tak	tak	k6eAd1	tak
silně	silně	k6eAd1	silně
a	a	k8xC	a
nenutily	nutit	k5eNaImAgFnP	nutit
československé	československý	k2eAgNnSc4d1	Československé
dělnictvo	dělnictvo	k1gNnSc4	dělnictvo
k	k	k7c3	k
masovým	masový	k2eAgInPc3d1	masový
výstupům	výstup	k1gInPc3	výstup
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
a	a	k8xC	a
spojení	spojení	k1gNnSc3	spojení
se	se	k3xPyFc4	se
s	s	k7c7	s
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
se	se	k3xPyFc4	se
tak	tak	k9	tak
stále	stále	k6eAd1	stále
udržovala	udržovat	k5eAaImAgFnS	udržovat
kolem	kolem	k7c2	kolem
malé	malý	k2eAgFnSc2d1	malá
skupiny	skupina	k1gFnSc2	skupina
lidí	člověk	k1gMnPc2	člověk
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
Chartou	charta	k1gFnSc7	charta
77	[number]	k4	77
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
uvolňování	uvolňování	k1gNnSc3	uvolňování
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožnily	umožnit	k5eAaPmAgFnP	umožnit
aktivnější	aktivní	k2eAgNnSc4d2	aktivnější
vystupování	vystupování	k1gNnSc4	vystupování
opozice	opozice	k1gFnSc2	opozice
a	a	k8xC	a
kritiků	kritik	k1gMnPc2	kritik
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
uvolňování	uvolňování	k1gNnSc4	uvolňování
poměrů	poměr	k1gInPc2	poměr
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
zdánlivě	zdánlivě	k6eAd1	zdánlivě
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vůdců	vůdce	k1gMnPc2	vůdce
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
a	a	k8xC	a
Miloše	Miloš	k1gMnSc2	Miloš
Jakeše	Jakeš	k1gMnSc2	Jakeš
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
znalců	znalec	k1gMnPc2	znalec
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
nepředpokládala	předpokládat	k5eNaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
československý	československý	k2eAgInSc4d1	československý
režim	režim	k1gInSc4	režim
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
padnout	padnout	k5eAaImF	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k9	ale
mezi	mezi	k7c7	mezi
vládnoucími	vládnoucí	k2eAgFnPc7d1	vládnoucí
silami	síla	k1gFnPc7	síla
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc4	který
již	již	k6eAd1	již
postrádaly	postrádat	k5eAaImAgFnP	postrádat
podporu	podpora	k1gFnSc4	podpora
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
)	)	kIx)	)
probíhal	probíhat	k5eAaImAgInS	probíhat
boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mezi	mezi	k7c4	mezi
občany	občan	k1gMnPc4	občan
rostla	růst	k5eAaImAgFnS	růst
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
společnosti	společnost	k1gFnSc6	společnost
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
vůči	vůči	k7c3	vůči
okolnímu	okolní	k2eAgInSc3d1	okolní
světu	svět	k1gInSc3	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
dění	dění	k1gNnSc1	dění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
několik	několik	k4yIc4	několik
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nemyslitelných	myslitelný	k2eNgFnPc2d1	nemyslitelná
protivládních	protivládní	k2eAgFnPc2d1	protivládní
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1988	[number]	k4	1988
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
tzv.	tzv.	kA	tzv.
Svíčková	svíčková	k1gFnSc1	svíčková
demonstrace	demonstrace	k1gFnSc1	demonstrace
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
demonstrace	demonstrace	k1gFnPc1	demonstrace
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
okupace	okupace	k1gFnSc2	okupace
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vyhlášení	vyhlášení	k1gNnPc2	vyhlášení
Všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
deklarace	deklarace	k1gFnSc2	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
)	)	kIx)	)
na	na	k7c6	na
Škroupově	Škroupův	k2eAgNnSc6d1	Škroupovo
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
demonstrace	demonstrace	k1gFnSc1	demonstrace
byla	být	k5eAaImAgFnS	být
režimem	režim	k1gInSc7	režim
povolená	povolený	k2eAgFnSc1d1	povolená
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
rušení	rušení	k1gNnSc1	rušení
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
rozhlasů	rozhlas	k1gInPc2	rozhlas
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
1989	[number]	k4	1989
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
série	série	k1gFnSc1	série
demonstrací	demonstrace	k1gFnPc2	demonstrace
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnPc1	demonstrace
byly	být	k5eAaImAgFnP	být
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
lidovými	lidový	k2eAgFnPc7d1	lidová
milicemi	milice	k1gFnPc7	milice
potlačeny	potlačit	k5eAaPmNgFnP	potlačit
s	s	k7c7	s
dosud	dosud	k6eAd1	dosud
nevídanou	vídaný	k2eNgFnSc7d1	nevídaná
brutalitou	brutalita	k1gFnSc7	brutalita
(	(	kIx(	(
<g/>
použity	použit	k2eAgFnPc1d1	použita
byly	být	k5eAaImAgFnP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
vodní	vodní	k2eAgNnPc4d1	vodní
děla	dělo	k1gNnPc4	dělo
a	a	k8xC	a
slzný	slzný	k2eAgInSc4d1	slzný
plyn	plyn	k1gInSc4	plyn
<g/>
)	)	kIx)	)
a	a	k8xC	a
čelní	čelní	k2eAgMnPc1d1	čelní
představitelé	představitel	k1gMnPc1	představitel
opozice	opozice	k1gFnSc2	opozice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc1	název
Palachův	Palachův	k2eAgInSc4d1	Palachův
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
postup	postup	k1gInSc4	postup
režimu	režim	k1gInSc2	režim
během	během	k7c2	během
Palachova	Palachův	k2eAgInSc2d1	Palachův
týdne	týden	k1gInSc2	týden
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
petiční	petiční	k2eAgFnSc1d1	petiční
akce	akce	k1gFnSc1	akce
Několik	několik	k4yIc4	několik
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
petiční	petiční	k2eAgFnSc6d1	petiční
akci	akce	k1gFnSc6	akce
se	se	k3xPyFc4	se
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
na	na	k7c4	na
40	[number]	k4	40
000	[number]	k4	000
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
potlačené	potlačený	k2eAgFnPc1d1	potlačená
demonstrace	demonstrace	k1gFnPc1	demonstrace
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
protesty	protest	k1gInPc1	protest
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
Teplic	Teplice	k1gFnPc2	Teplice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
dní	den	k1gInPc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
ekologických	ekologický	k2eAgFnPc2d1	ekologická
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
den	den	k1gInSc1	den
před	před	k7c7	před
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
studentská	studentský	k2eAgFnSc1d1	studentská
demonstrace	demonstrace	k1gFnSc1	demonstrace
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Savarinu	Savarin	k1gInSc6	Savarin
sešel	sejít	k5eAaPmAgInS	sejít
tzv.	tzv.	kA	tzv.
Obrodný	obrodný	k2eAgInSc1d1	obrodný
proud	proud	k1gInSc1	proud
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
straně	strana	k1gFnSc6	strana
lidové	lidový	k2eAgFnSc6d1	lidová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
73	[number]	k4	73
delegátů	delegát	k1gMnPc2	delegát
z	z	k7c2	z
regionů	region	k1gInPc2	region
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
deklaraci	deklarace	k1gFnSc4	deklarace
Obrodného	obrodný	k2eAgInSc2d1	obrodný
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
straně	strana	k1gFnSc6	strana
lidové	lidový	k2eAgFnSc6d1	lidová
požadující	požadující	k2eAgFnSc3d1	požadující
demokratizaci	demokratizace	k1gFnSc3	demokratizace
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
křesťanským	křesťanský	k2eAgInPc3d1	křesťanský
základům	základ	k1gInPc3	základ
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
svatořečení	svatořečení	k1gNnSc1	svatořečení
Anežky	Anežka	k1gFnSc2	Anežka
České	český	k2eAgFnSc2d1	Česká
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
tisíců	tisíc	k4xCgInPc2	tisíc
českých	český	k2eAgMnPc2d1	český
poutníků	poutník	k1gMnPc2	poutník
<g/>
;	;	kIx,	;
obřad	obřad	k1gInSc4	obřad
přímo	přímo	k6eAd1	přímo
přenášela	přenášet	k5eAaImAgFnS	přenášet
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
údajně	údajně	k6eAd1	údajně
Pavel	Pavel	k1gMnSc1	Pavel
Tigrid	Tigrid	k1gMnSc1	Tigrid
zjišťoval	zjišťovat	k5eAaImAgMnS	zjišťovat
názory	názor	k1gInPc4	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jak	jak	k8xS	jak
i	i	k9	i
zástupci	zástupce	k1gMnPc1	zástupce
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
věděli	vědět	k5eAaImAgMnP	vědět
už	už	k6eAd1	už
před	před	k7c7	před
listopadem	listopad	k1gInSc7	listopad
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
režimem	režim	k1gInSc7	režim
ámen	ámen	k?	ámen
<g/>
:	:	kIx,	:
Sebrali	sebrat	k5eAaPmAgMnP	sebrat
mě	já	k3xPp1nSc2	já
estébáci	estébáci	k?	estébáci
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c6	o
svátcích	svátek	k1gInPc6	svátek
dělali	dělat	k5eAaImAgMnP	dělat
<g/>
,	,	kIx,	,
abysme	abysme	k?	abysme
'	'	kIx"	'
<g/>
nezlobili	zlobit	k5eNaImAgMnP	zlobit
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Seděl	sedět	k5eAaImAgMnS	sedět
jsem	být	k5eAaImIp1nS	být
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
autě	aut	k1gInSc6	aut
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
mě	já	k3xPp1nSc4	já
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Tak	tak	k9	tak
co	co	k3yRnSc1	co
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
praskne	prasknout	k5eAaPmIp3nS	prasknout
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Valtr	Valtr	k1gMnSc1	Valtr
Komárek	Komárek	k1gMnSc1	Komárek
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
myšleno	myslet	k5eAaImNgNnS	myslet
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
ÚV	ÚV	kA	ÚV
KSSS	KSSS	kA	KSSS
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
debatovali	debatovat	k5eAaImAgMnP	debatovat
o	o	k7c6	o
přechodu	přechod	k1gInSc6	přechod
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
stanné	stanný	k2eAgNnSc4d1	stanné
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
<g/>
,	,	kIx,	,
že	že	k8xS	že
oni	onen	k3xDgMnPc1	onen
ale	ale	k8xC	ale
udělají	udělat	k5eAaPmIp3nP	udělat
všechno	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
tu	tu	k6eAd1	tu
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
poklidně	poklidně	k6eAd1	poklidně
<g/>
.	.	kIx.	.
</s>
<s>
Věděli	vědět	k5eAaImAgMnP	vědět
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
už	už	k9	už
něco	něco	k3yInSc1	něco
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
to	ten	k3xDgNnSc4	ten
výročí	výročí	k1gNnSc4	výročí
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
bylo	být	k5eAaImAgNnS	být
magické	magický	k2eAgNnSc1d1	magické
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInPc4d2	veliký
protesty	protest	k1gInPc4	protest
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
že	že	k8xS	že
bychom	by	kYmCp1nP	by
to	ten	k3xDgNnSc4	ten
jako	jako	k9	jako
prognostici	prognostik	k1gMnPc1	prognostik
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
na	na	k7c4	na
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věděli	vědět	k5eAaImAgMnP	vědět
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Bushem	Bush	k1gMnSc7	Bush
a	a	k8xC	a
Gorbačovem	Gorbačovo	k1gNnSc7	Gorbačovo
je	být	k5eAaImIp3nS	být
přijatá	přijatý	k2eAgFnSc1d1	přijatá
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
ten	ten	k3xDgInSc1	ten
problém	problém	k1gInSc1	problém
musí	muset	k5eAaImIp3nS	muset
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
dořešit	dořešit	k5eAaPmF	dořešit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1989	[number]	k4	1989
naplánovala	naplánovat	k5eAaBmAgFnS	naplánovat
organizace	organizace	k1gFnSc1	organizace
Nezávislé	závislý	k2eNgFnSc2d1	nezávislá
studentské	studentská	k1gFnSc2	studentská
sdružení	sdružení	k1gNnSc2	sdružení
STUHA	stuha	k1gFnSc1	stuha
na	na	k7c6	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1989	[number]	k4	1989
pietní	pietní	k2eAgFnSc4d1	pietní
akci	akce	k1gFnSc4	akce
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
uzavření	uzavření	k1gNnSc2	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
nacisty	nacista	k1gMnPc4	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
odehrát	odehrát	k5eAaPmF	odehrát
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Albertově	Albertův	k2eAgInSc6d1	Albertův
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
účelem	účel	k1gInSc7	účel
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
uctění	uctění	k1gNnSc1	uctění
památky	památka	k1gFnSc2	památka
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
radikálnější	radikální	k2eAgFnSc2d2	radikálnější
části	část	k1gFnSc2	část
studentů	student	k1gMnPc2	student
mínili	mínit	k5eAaImAgMnP	mínit
využít	využít	k5eAaPmF	využít
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
společenskou	společenský	k2eAgFnSc7d1	společenská
situací	situace	k1gFnSc7	situace
a	a	k8xC	a
k	k	k7c3	k
volání	volání	k1gNnSc3	volání
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
organizaci	organizace	k1gFnSc3	organizace
pietní	pietní	k2eAgFnSc2d1	pietní
akce	akce	k1gFnSc2	akce
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
pražská	pražský	k2eAgFnSc1d1	Pražská
organizace	organizace	k1gFnSc1	organizace
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
akce	akce	k1gFnSc1	akce
oficiálně	oficiálně	k6eAd1	oficiálně
povolena	povolen	k2eAgFnSc1d1	povolena
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
se	se	k3xPyFc4	se
organizátoři	organizátor	k1gMnPc1	organizátor
vzdát	vzdát	k5eAaPmF	vzdát
původně	původně	k6eAd1	původně
zamýšleného	zamýšlený	k2eAgInSc2d1	zamýšlený
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
pochod	pochod	k1gInSc4	pochod
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
měla	mít	k5eAaImAgFnS	mít
demonstrace	demonstrace	k1gFnSc1	demonstrace
zamířit	zamířit	k5eAaPmF	zamířit
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
demonstrace	demonstrace	k1gFnPc4	demonstrace
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
ústředním	ústřední	k2eAgInSc7d1	ústřední
výborem	výbor	k1gInSc7	výbor
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
obvodním	obvodní	k2eAgInSc7d1	obvodní
národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Organizátoři	organizátor	k1gMnPc1	organizátor
demonstrace	demonstrace	k1gFnSc2	demonstrace
zatím	zatím	k6eAd1	zatím
vyzývali	vyzývat	k5eAaImAgMnP	vyzývat
zúčastněné	zúčastněný	k2eAgFnPc4d1	zúčastněná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
na	na	k7c6	na
manifestaci	manifestace	k1gFnSc6	manifestace
přinesli	přinést	k5eAaPmAgMnP	přinést
květiny	květina	k1gFnPc4	květina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
průběhu	průběh	k1gInSc6	průběh
demonstrace	demonstrace	k1gFnSc2	demonstrace
se	se	k3xPyFc4	se
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
studentů	student	k1gMnPc2	student
nepodílely	podílet	k5eNaImAgInP	podílet
žádné	žádný	k3yNgFnPc4	žádný
disidentské	disidentský	k2eAgFnPc4d1	disidentská
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
představitelé	představitel	k1gMnPc1	představitel
moci	moc	k1gFnSc2	moc
nedostali	dostat	k5eNaPmAgMnP	dostat
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
zakázání	zakázání	k1gNnSc3	zakázání
či	či	k8xC	či
potlačení	potlačení	k1gNnSc3	potlačení
demonstrace	demonstrace	k1gFnSc1	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
disidentů	disident	k1gMnPc2	disident
se	se	k3xPyFc4	se
demonstrace	demonstrace	k1gFnSc1	demonstrace
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
měly	mít	k5eAaImAgFnP	mít
ale	ale	k8xC	ale
děti	dítě	k1gFnPc1	dítě
disidentů	disident	k1gMnPc2	disident
Marek	Marek	k1gMnSc1	Marek
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Dus	dus	k1gInSc1	dus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
společně	společně	k6eAd1	společně
se	s	k7c7	s
Šimonem	Šimon	k1gMnSc7	Šimon
Pánkem	Pánek	k1gMnSc7	Pánek
měli	mít	k5eAaImAgMnP	mít
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
nasměrování	nasměrování	k1gNnSc6	nasměrování
průvodu	průvod	k1gInSc2	průvod
do	do	k7c2	do
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
po	po	k7c4	po
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
stalo	stát	k5eAaPmAgNnS	stát
roznětkou	roznětka	k1gFnSc7	roznětka
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
zpráva	zpráva	k1gFnSc1	zpráva
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
pro	pro	k7c4	pro
objasnění	objasnění	k1gNnSc4	objasnění
událostí	událost	k1gFnPc2	událost
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
jediná	jediný	k2eAgFnSc1d1	jediná
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
svědčila	svědčit	k5eAaImAgFnS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
některá	některý	k3yIgFnSc1	některý
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
podnikla	podniknout	k5eAaPmAgFnS	podniknout
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
krok	krok	k1gInSc4	krok
vedoucí	vedoucí	k2eAgInPc4d1	vedoucí
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
společenského	společenský	k2eAgInSc2d1	společenský
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
představitelé	představitel	k1gMnPc1	představitel
mocenských	mocenský	k2eAgFnPc2d1	mocenská
složek	složka	k1gFnPc2	složka
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
měli	mít	k5eAaImAgMnP	mít
z	z	k7c2	z
chystané	chystaný	k2eAgFnSc2d1	chystaná
akce	akce	k1gFnSc2	akce
značné	značný	k2eAgFnSc2d1	značná
obavy	obava	k1gFnSc2	obava
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
povolenou	povolený	k2eAgFnSc4d1	povolená
pietní	pietní	k2eAgFnSc4d1	pietní
akci	akce	k1gFnSc4	akce
spoluorganizovanou	spoluorganizovaný	k2eAgFnSc4d1	spoluorganizovaná
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
ji	on	k3xPp3gFnSc4	on
možné	možný	k2eAgNnSc1d1	možné
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodu	důvod	k1gInSc2	důvod
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
schůzka	schůzka	k1gFnSc1	schůzka
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Františka	František	k1gMnSc2	František
Kincla	Kincl	k1gMnSc2	Kincl
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
KSČ	KSČ	kA	KSČ
Jakešem	Jakeš	k1gMnSc7	Jakeš
a	a	k8xC	a
s	s	k7c7	s
krajským	krajský	k2eAgMnSc7d1	krajský
tajemníkem	tajemník	k1gMnSc7	tajemník
Štěpánem	Štěpán	k1gMnSc7	Štěpán
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
pořádkové	pořádkový	k2eAgFnPc1d1	pořádková
služby	služba	k1gFnPc1	služba
mají	mít	k5eAaImIp3nP	mít
udržet	udržet	k5eAaPmF	udržet
klid	klid	k1gInSc4	klid
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
proti	proti	k7c3	proti
demonstrujícím	demonstrující	k2eAgFnPc3d1	demonstrující
zasahovat	zasahovat	k5eAaImF	zasahovat
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
rozkazu	rozkaz	k1gInSc2	rozkaz
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Františka	František	k1gMnSc2	František
Kincla	Kincl	k1gMnSc2	Kincl
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
udržet	udržet	k5eAaPmF	udržet
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
pořádek	pořádek	k1gInSc1	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
sešli	sejít	k5eAaPmAgMnP	sejít
studenti	student	k1gMnPc1	student
pražských	pražský	k2eAgFnPc2d1	Pražská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
se	se	k3xPyFc4	se
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
nacházelo	nacházet	k5eAaImAgNnS	nacházet
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
až	až	k9	až
600	[number]	k4	600
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
narůstal	narůstat	k5eAaImAgInS	narůstat
nově	nově	k6eAd1	nově
příchozími	příchozí	k1gMnPc7	příchozí
<g/>
.	.	kIx.	.
</s>
<s>
Manifestace	manifestace	k1gFnSc1	manifestace
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
zpěvem	zpěv	k1gInSc7	zpěv
písně	píseň	k1gFnSc2	píseň
Gaudeamus	Gaudeamus	k1gInSc1	Gaudeamus
igitur	igitura	k1gFnPc2	igitura
a	a	k8xC	a
projevem	projev	k1gInSc7	projev
Martina	Martin	k1gMnSc2	Martin
Klímy	Klíma	k1gMnSc2	Klíma
z	z	k7c2	z
uskupení	uskupení	k1gNnSc2	uskupení
Nezávislých	závislý	k2eNgMnPc2d1	nezávislý
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
počet	počet	k1gInSc4	počet
účastníků	účastník	k1gMnPc2	účastník
až	až	k6eAd1	až
na	na	k7c4	na
15	[number]	k4	15
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgInSc1d1	další
zdroj	zdroj	k1gInSc1	zdroj
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
davu	dav	k1gInSc6	dav
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
manifestace	manifestace	k1gFnSc2	manifestace
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
pořadatelé	pořadatel	k1gMnPc1	pořadatel
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
k	k	k7c3	k
pochodu	pochod	k1gInSc3	pochod
na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
ke	k	k7c3	k
hrobu	hrob	k1gInSc3	hrob
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
lidí	člověk	k1gMnPc2	člověk
chtěla	chtít	k5eAaImAgFnS	chtít
ale	ale	k8xC	ale
směřovat	směřovat	k5eAaImF	směřovat
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
měli	mít	k5eAaImAgMnP	mít
původně	původně	k6eAd1	původně
demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hlásal	hlásat	k5eAaImAgInS	hlásat
leták	leták	k1gInSc1	leták
vytištěný	vytištěný	k2eAgInSc1d1	vytištěný
k	k	k7c3	k
manifestaci	manifestace	k1gFnSc3	manifestace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
odhodlanými	odhodlaný	k2eAgMnPc7d1	odhodlaný
byl	být	k5eAaImAgMnS	být
i	i	k9	i
poručík	poručík	k1gMnSc1	poručík
StB	StB	k1gMnSc1	StB
Ludvík	Ludvík	k1gMnSc1	Ludvík
Zifčák	Zifčák	k1gMnSc1	Zifčák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
studenta	student	k1gMnSc4	student
Martina	Martin	k1gMnSc4	Martin
Šmída	Šmíd	k1gMnSc4	Šmíd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
postupně	postupně	k6eAd1	postupně
dorazil	dorazit	k5eAaPmAgInS	dorazit
dav	dav	k1gInSc1	dav
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zcela	zcela	k6eAd1	zcela
zaplnili	zaplnit	k5eAaPmAgMnP	zaplnit
prostranství	prostranství	k1gNnSc4	prostranství
před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
byla	být	k5eAaImAgFnS	být
demonstrace	demonstrace	k1gFnSc1	demonstrace
oficiálně	oficiálně	k6eAd1	oficiálně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
začátku	začátek	k1gInSc2	začátek
demonstrace	demonstrace	k1gFnSc2	demonstrace
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
skandování	skandování	k1gNnSc3	skandování
protikomunistických	protikomunistický	k2eAgNnPc2d1	protikomunistické
hesel	heslo	k1gNnPc2	heslo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c4	na
stovku	stovka	k1gFnSc4	stovka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Jakeše	Jakeš	k1gMnPc4	Jakeš
do	do	k7c2	do
koše	koš	k1gInSc2	koš
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krajský	krajský	k2eAgMnSc1d1	krajský
tajemník	tajemník	k1gMnSc1	tajemník
KSČ	KSČ	kA	KSČ
Štěpán	Štěpán	k1gMnSc1	Štěpán
se	se	k3xPyFc4	se
u	u	k7c2	u
velitelů	velitel	k1gMnPc2	velitel
SNB	SNB	kA	SNB
dožadoval	dožadovat	k5eAaImAgMnS	dožadovat
rozehnání	rozehnání	k1gNnSc4	rozehnání
demonstrace	demonstrace	k1gFnSc2	demonstrace
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
nevyhověli	vyhovět	k5eNaPmAgMnP	vyhovět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
drželi	držet	k5eAaImAgMnP	držet
rozkazu	rozkaz	k1gInSc2	rozkaz
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
"	"	kIx"	"
<g/>
nezasahovat	zasahovat	k5eNaImF	zasahovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
oficiální	oficiální	k2eAgFnSc2d1	oficiální
části	část	k1gFnSc2	část
demonstrace	demonstrace	k1gFnSc2	demonstrace
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
neplánovaně	plánovaně	k6eNd1	plánovaně
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zastavit	zastavit	k5eAaPmF	zastavit
pochod	pochod	k1gInSc4	pochod
přehradily	přehradit	k5eAaPmAgFnP	přehradit
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
složky	složka	k1gFnPc1	složka
Vyšehradskou	vyšehradský	k2eAgFnSc4d1	Vyšehradská
ulici	ulice	k1gFnSc4	ulice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
davu	dav	k1gInSc2	dav
tlačila	tlačit	k5eAaImAgFnS	tlačit
na	na	k7c4	na
předek	předek	k1gInSc4	předek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
paniky	panika	k1gFnSc2	panika
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
policejní	policejní	k2eAgInSc1d1	policejní
kordon	kordon	k1gInSc1	kordon
byl	být	k5eAaImAgInS	být
protržen	protrhnout	k5eAaPmNgInS	protrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Zablokování	zablokování	k1gNnSc1	zablokování
přístupu	přístup	k1gInSc2	přístup
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
zabránily	zabránit	k5eAaPmAgFnP	zabránit
až	až	k9	až
přivolané	přivolaný	k2eAgFnPc1d1	přivolaná
posily	posila	k1gFnPc1	posila
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
průvodu	průvod	k1gInSc2	průvod
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
opět	opět	k6eAd1	opět
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
přes	přes	k7c4	přes
Plaveckou	plavecký	k2eAgFnSc4d1	plavecká
ulici	ulice	k1gFnSc4	ulice
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
pochodu	pochod	k1gInSc6	pochod
po	po	k7c4	po
nábřeží	nábřeží	k1gNnSc4	nábřeží
Vltavy	Vltava	k1gFnSc2	Vltava
až	až	k9	až
k	k	k7c3	k
Národnímu	národní	k2eAgNnSc3d1	národní
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
davu	dav	k1gInSc2	dav
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Složky	složka	k1gFnPc4	složka
Sboru	sbor	k1gInSc2	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
měly	mít	k5eAaImAgFnP	mít
rozkaz	rozkaz	k1gInSc4	rozkaz
zabránit	zabránit	k5eAaPmF	zabránit
průvodu	průvod	k1gInSc6	průvod
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
či	či	k8xC	či
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
přišel	přijít	k5eAaPmAgInS	přijít
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
dav	dav	k1gInSc1	dav
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
zablokovat	zablokovat	k5eAaPmF	zablokovat
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgInSc1d1	policejní
kordon	kordon	k1gInSc1	kordon
zatarasil	zatarasit	k5eAaPmAgInS	zatarasit
Most	most	k1gInSc1	most
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gInSc2	máj
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zabránil	zabránit	k5eAaPmAgMnS	zabránit
davu	dav	k1gInSc3	dav
odbočit	odbočit	k5eAaPmF	odbočit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
zahnuli	zahnout	k5eAaPmAgMnP	zahnout
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
třídu	třída	k1gFnSc4	třída
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
byla	být	k5eAaImAgFnS	být
kordonem	kordon	k1gInSc7	kordon
přehrazena	přehrazen	k2eAgFnSc1d1	přehrazena
Národní	národní	k2eAgFnSc1d1	národní
třída	třída	k1gFnSc1	třída
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Perštýna	Perštýna	k1gFnSc1	Perštýna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
čelo	čelo	k1gNnSc1	čelo
demonstrace	demonstrace	k1gFnSc2	demonstrace
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
,	,	kIx,	,
účastníci	účastník	k1gMnPc1	účastník
si	se	k3xPyFc3	se
sedli	sednout	k5eAaPmAgMnP	sednout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
před	před	k7c4	před
pořádkové	pořádkový	k2eAgFnPc4d1	pořádková
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
začaly	začít	k5eAaPmAgFnP	začít
spontánně	spontánně	k6eAd1	spontánně
zasunovat	zasunovat	k5eAaImF	zasunovat
za	za	k7c4	za
štíty	štít	k1gInPc4	štít
příslušníků	příslušník	k1gMnPc2	příslušník
pohotovostního	pohotovostní	k2eAgInSc2d1	pohotovostní
pluku	pluk	k1gInSc2	pluk
květiny	květina	k1gFnSc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
policejní	policejní	k2eAgMnPc1d1	policejní
velitelé	velitel	k1gMnPc1	velitel
obávali	obávat	k5eAaImAgMnP	obávat
opakování	opakování	k1gNnSc4	opakování
předchozí	předchozí	k2eAgFnSc2d1	předchozí
situace	situace	k1gFnSc2	situace
z	z	k7c2	z
Vyšehradské	vyšehradský	k2eAgFnSc2d1	Vyšehradská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dav	dav	k1gInSc1	dav
uzávěru	uzávěr	k1gInSc2	uzávěr
obešel	obejít	k5eAaPmAgInS	obejít
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
o	o	k7c4	o
čtvrt	čtvrt	k1xP	čtvrt
hodiny	hodina	k1gFnSc2	hodina
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
ulic	ulice	k1gFnPc2	ulice
Mikulandské	Mikulandský	k2eAgFnSc2d1	Mikulandská
a	a	k8xC	a
Voršilské	voršilský	k2eAgFnSc2d1	Voršilská
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
zablokování	zablokování	k1gNnSc3	zablokování
ústupu	ústup	k1gInSc2	ústup
Národní	národní	k2eAgFnSc7d1	národní
třídou	třída	k1gFnSc7	třída
zpět	zpět	k6eAd1	zpět
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Národnímu	národní	k2eAgNnSc3d1	národní
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
demonstrantů	demonstrant	k1gMnPc2	demonstrant
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
policejní	policejní	k2eAgInPc4d1	policejní
kordony	kordon	k1gInPc4	kordon
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
pokojné	pokojný	k2eAgFnSc6d1	pokojná
a	a	k8xC	a
nenásilné	násilný	k2eNgFnSc6d1	nenásilná
demonstraci	demonstrace	k1gFnSc6	demonstrace
za	za	k7c4	za
provolávání	provolávání	k1gNnSc4	provolávání
hesel	heslo	k1gNnPc2	heslo
jako	jako	k8xS	jako
Máme	mít	k5eAaImIp1nP	mít
holé	holý	k2eAgFnPc4d1	holá
ruce	ruka	k1gFnPc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
demonstrace	demonstrace	k1gFnSc2	demonstrace
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
demonstrantům	demonstrant	k1gMnPc3	demonstrant
umožňován	umožňovat	k5eAaImNgInS	umožňovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
volný	volný	k2eAgInSc4d1	volný
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
čtvrt	čtvrt	k1gFnSc4	čtvrt
na	na	k7c4	na
devět	devět	k4xCc4	devět
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
obklíčení	obklíčení	k1gNnSc4	obklíčení
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
policejní	policejní	k2eAgInSc1d1	policejní
kordon	kordon	k1gInSc1	kordon
postupující	postupující	k2eAgInSc1d1	postupující
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
začal	začít	k5eAaPmAgInS	začít
prostor	prostor	k1gInSc4	prostor
zahušťovat	zahušťovat	k5eAaImF	zahušťovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohotovostní	pohotovostní	k2eAgInSc1d1	pohotovostní
pluk	pluk	k1gInSc1	pluk
veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
Odbor	odbor	k1gInSc1	odbor
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
určení	určení	k1gNnSc2	určení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
červené	červený	k2eAgInPc1d1	červený
barety	baret	k1gInPc1	baret
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
začaly	začít	k5eAaPmAgFnP	začít
demonstrující	demonstrující	k2eAgInPc1d1	demonstrující
surově	surově	k6eAd1	surově
bít	bít	k5eAaImF	bít
obušky	obušek	k1gInPc4	obušek
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
byli	být	k5eAaImAgMnP	být
účastníci	účastník	k1gMnPc1	účastník
demonstrace	demonstrace	k1gFnSc2	demonstrace
vyzývání	vyzývání	k1gNnSc2	vyzývání
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgFnPc4d1	jediná
únikové	únikový	k2eAgFnPc4d1	úniková
cesty	cesta	k1gFnPc4	cesta
vedly	vést	k5eAaImAgInP	vést
skrz	skrz	k7c4	skrz
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
uličky	ulička	k1gFnSc2	ulička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
demonstranti	demonstrant	k1gMnPc1	demonstrant
brutálně	brutálně	k6eAd1	brutálně
biti	bít	k5eAaImNgMnP	bít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
byla	být	k5eAaImAgFnS	být
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
demonstrace	demonstrace	k1gFnSc2	demonstrace
násilně	násilně	k6eAd1	násilně
rozptýlena	rozptýlen	k2eAgFnSc1d1	rozptýlena
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
účastníci	účastník	k1gMnPc1	účastník
byli	být	k5eAaImAgMnP	být
následně	následně	k6eAd1	následně
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
a	a	k8xC	a
naloženi	naložit	k5eAaPmNgMnP	naložit
do	do	k7c2	do
připravených	připravený	k2eAgInPc2d1	připravený
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
rozehnání	rozehnání	k1gNnSc6	rozehnání
demonstrace	demonstrace	k1gFnSc1	demonstrace
docházelo	docházet	k5eAaImAgNnS	docházet
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
pořádkových	pořádkový	k2eAgFnPc2d1	pořádková
jednotek	jednotka	k1gFnPc2	jednotka
k	k	k7c3	k
napadání	napadání	k1gNnSc3	napadání
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
či	či	k8xC	či
skupinek	skupinka	k1gFnPc2	skupinka
přihlížejících	přihlížející	k2eAgInPc2d1	přihlížející
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
lékařská	lékařský	k2eAgFnSc1d1	lékařská
komise	komise	k1gFnSc1	komise
později	pozdě	k6eAd2	pozdě
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
568	[number]	k4	568
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
zásahu	zásah	k1gInSc2	zásah
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
utéci	utéct	k5eAaPmF	utéct
<g/>
,	,	kIx,	,
zamířila	zamířit	k5eAaPmAgFnS	zamířit
do	do	k7c2	do
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hovořila	hovořit	k5eAaImAgFnS	hovořit
s	s	k7c7	s
herci	herec	k1gMnPc7	herec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
s	s	k7c7	s
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Hanzlíkem	Hanzlík	k1gMnSc7	Hanzlík
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Lábusem	Lábus	k1gMnSc7	Lábus
<g/>
,	,	kIx,	,
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Krobotem	Krobotem	k?	Krobotem
<g/>
)	)	kIx)	)
o	o	k7c6	o
policejním	policejní	k2eAgInSc6d1	policejní
zásahu	zásah	k1gInSc6	zásah
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
projednávání	projednávání	k1gNnSc6	projednávání
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
stávky	stávka	k1gFnSc2	stávka
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
možných	možný	k2eAgInPc6d1	možný
mrtvých	mrtvý	k2eAgInPc6d1	mrtvý
lidech	lid	k1gInPc6	lid
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
demonstrujících	demonstrující	k2eAgFnPc2d1	demonstrující
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
se	s	k7c7	s
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
zásahu	zásah	k1gInSc6	zásah
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
proti	proti	k7c3	proti
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
aktivizaci	aktivizace	k1gFnSc3	aktivizace
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
podporu	podpora	k1gFnSc4	podpora
studentům	student	k1gMnPc3	student
a	a	k8xC	a
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
zásahem	zásah	k1gInSc7	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
události	událost	k1gFnSc6	událost
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
v	v	k7c6	v
divadlech	divadlo	k1gNnPc6	divadlo
a	a	k8xC	a
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
začaly	začít	k5eAaPmAgFnP	začít
organizovat	organizovat	k5eAaBmF	organizovat
stávkové	stávkový	k2eAgInPc4d1	stávkový
výbory	výbor	k1gInPc4	výbor
požadující	požadující	k2eAgNnSc4d1	požadující
vyšetření	vyšetření	k1gNnSc4	vyšetření
a	a	k8xC	a
potrestání	potrestání	k1gNnSc1	potrestání
osob	osoba	k1gFnPc2	osoba
zodpovědných	zodpovědný	k2eAgInPc2d1	zodpovědný
za	za	k7c4	za
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
zásah	zásah	k1gInSc4	zásah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Realistickém	realistický	k2eAgNnSc6d1	realistické
divadle	divadlo	k1gNnSc6	divadlo
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
týdenní	týdenní	k2eAgFnSc1d1	týdenní
stávka	stávka	k1gFnSc1	stávka
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
herci	herec	k1gMnPc1	herec
přidali	přidat	k5eAaPmAgMnP	přidat
ke	k	k7c3	k
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
týdenní	týdenní	k2eAgFnSc4d1	týdenní
stávku	stávka	k1gFnSc4	stávka
zahájili	zahájit	k5eAaPmAgMnP	zahájit
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
a	a	k8xC	a
studenti	student	k1gMnPc1	student
požadovali	požadovat	k5eAaImAgMnP	požadovat
důsledné	důsledný	k2eAgNnSc4d1	důsledné
vyšetření	vyšetření	k1gNnSc4	vyšetření
večerního	večerní	k2eAgInSc2d1	večerní
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
<g/>
,	,	kIx,	,
jména	jméno	k1gNnPc1	jméno
odpovědných	odpovědný	k2eAgFnPc2d1	odpovědná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
vyšetření	vyšetření	k1gNnSc4	vyšetření
a	a	k8xC	a
případné	případný	k2eAgNnSc4d1	případné
potrestání	potrestání	k1gNnSc4	potrestání
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
členové	člen	k1gMnPc1	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
opozice	opozice	k1gFnSc2	opozice
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
zprvu	zprvu	k6eAd1	zprvu
významnou	významný	k2eAgFnSc4d1	významná
aktivitu	aktivita	k1gFnSc4	aktivita
nevyvíjeli	vyvíjet	k5eNaImAgMnP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
SNB	SNB	kA	SNB
nejprve	nejprve	k6eAd1	nejprve
nebyli	být	k5eNaImAgMnP	být
o	o	k7c6	o
rozsahu	rozsah	k1gInSc6	rozsah
a	a	k8xC	a
důsledcích	důsledek	k1gInPc6	důsledek
zásahu	zásah	k1gInSc2	zásah
úplně	úplně	k6eAd1	úplně
informováni	informovat	k5eAaBmNgMnP	informovat
a	a	k8xC	a
událostem	událost	k1gFnPc3	událost
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
nepřikládali	přikládat	k5eNaImAgMnP	přikládat
velkou	velký	k2eAgFnSc4d1	velká
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
mezi	mezi	k7c7	mezi
demonstrujícími	demonstrující	k2eAgFnPc7d1	demonstrující
šířit	šířit	k5eAaImF	šířit
fáma	fáma	k1gFnSc1	fáma
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
údajně	údajně	k6eAd1	údajně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
student	student	k1gMnSc1	student
Martin	Martin	k1gMnSc1	Martin
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
fámu	fáma	k1gFnSc4	fáma
si	se	k3xPyFc3	se
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Dražská	Dražský	k2eAgFnSc1d1	Dražská
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
ošetření	ošetření	k1gNnSc4	ošetření
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
lehce	lehko	k6eAd1	lehko
zraněná	zraněný	k2eAgFnSc1d1	zraněná
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
sama	sám	k3xTgFnSc1	sám
uvedla	uvést	k5eAaPmAgFnS	uvést
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
ke	k	k7c3	k
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
udělala	udělat	k5eAaPmAgFnS	udělat
to	ten	k3xDgNnSc1	ten
prý	prý	k9	prý
jen	jen	k9	jen
ze	z	k7c2	z
vzteku	vztek	k1gInSc2	vztek
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Dražská	Dražský	k2eAgFnSc1d1	Dražská
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Petra	Petr	k1gMnSc2	Petr
Uhla	Uhlus	k1gMnSc2	Uhlus
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
vysílání	vysílání	k1gNnSc2	vysílání
rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
této	tento	k3xDgFnSc2	tento
zprávy	zpráva	k1gFnSc2	zpráva
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
radikalizaci	radikalizace	k1gFnSc4	radikalizace
převážně	převážně	k6eAd1	převážně
mimopražských	mimopražský	k2eAgMnPc2d1	mimopražský
studentů	student	k1gMnPc2	student
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
kauza	kauza	k1gFnSc1	kauza
byla	být	k5eAaImAgFnS	být
vykonstruována	vykonstruovat	k5eAaPmNgFnS	vykonstruovat
agenty	agent	k1gMnPc7	agent
StB	StB	k1gMnSc7	StB
a	a	k8xC	a
že	že	k8xS	že
údajný	údajný	k2eAgMnSc1d1	údajný
mrtvý	mrtvý	k1gMnSc1	mrtvý
byl	být	k5eAaImAgMnS	být
člen	člen	k1gMnSc1	člen
StB	StB	k1gMnSc1	StB
poručík	poručík	k1gMnSc1	poručík
Ludvík	Ludvík	k1gMnSc1	Ludvík
Zifčák	Zifčák	k1gMnSc1	Zifčák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
skutečně	skutečně	k6eAd1	skutečně
Zifčák	Zifčák	k1gInSc4	Zifčák
po	po	k7c6	po
zákroku	zákrok	k1gInSc6	zákrok
VB	VB	kA	VB
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gNnSc3	jeho
bezvědomí	bezvědomí	k1gNnSc3	bezvědomí
nebylo	být	k5eNaImAgNnS	být
předstírané	předstíraný	k2eAgNnSc1d1	předstírané
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zákroku	zákrok	k1gInSc2	zákrok
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
psychického	psychický	k2eAgNnSc2d1	psychické
vypětí	vypětí	k1gNnSc2	vypětí
-	-	kIx~	-
omdlel	omdlet	k5eAaPmAgMnS	omdlet
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
studenti	student	k1gMnPc1	student
a	a	k8xC	a
i	i	k8xC	i
Dražská	Dražský	k2eAgFnSc1d1	Dražská
sama	sám	k3xTgFnSc1	sám
viděla	vidět	k5eAaImAgFnS	vidět
bezvládné	bezvládný	k2eAgNnSc4d1	bezvládné
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
odnášení	odnášení	k1gNnSc4	odnášení
do	do	k7c2	do
sanitky	sanitka	k1gFnSc2	sanitka
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
odjezd	odjezd	k1gInSc1	odjezd
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
"	"	kIx"	"
<g/>
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zametení	zametení	k1gNnSc4	zametení
stop	stopa	k1gFnPc2	stopa
<g/>
"	"	kIx"	"
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
vztekem	vztek	k1gInSc7	vztek
a	a	k8xC	a
bezmocí	bezmoc	k1gFnSc7	bezmoc
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
fámě	fáma	k1gFnSc3	fáma
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
víkend	víkend	k1gInSc4	víkend
chodili	chodit	k5eAaImAgMnP	chodit
lidé	člověk	k1gMnPc1	člověk
zapalovat	zapalovat	k5eAaImF	zapalovat
svíčky	svíčka	k1gFnPc4	svíčka
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uctili	uctít	k5eAaPmAgMnP	uctít
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
i	i	k8xC	i
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
během	během	k7c2	během
zásahu	zásah	k1gInSc2	zásah
zraněni	zranit	k5eAaPmNgMnP	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
nijak	nijak	k6eAd1	nijak
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
vydal	vydat	k5eAaPmAgMnS	vydat
genpor.	genpor.	k?	genpor.
Alojz	Alojz	k1gMnSc1	Alojz
Lorenc	Lorenc	k1gMnSc1	Lorenc
(	(	kIx(	(
<g/>
náměstek	náměstek	k1gMnSc1	náměstek
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
)	)	kIx)	)
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc2	který
nemají	mít	k5eNaImIp3nP	mít
složky	složka	k1gFnPc1	složka
StB	StB	k1gFnPc2	StB
nijak	nijak	k6eAd1	nijak
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rozkaz	rozkaz	k1gInSc4	rozkaz
později	pozdě	k6eAd2	pozdě
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechtěl	chtít	k5eNaImAgMnS	chtít
více	hodně	k6eAd2	hodně
pobouřit	pobouřit	k5eAaPmF	pobouřit
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
brutální	brutální	k2eAgInSc1d1	brutální
zásah	zásah	k1gInSc1	zásah
policejních	policejní	k2eAgFnPc2d1	policejní
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Činoherním	činoherní	k2eAgInSc6d1	činoherní
klubu	klub	k1gInSc6	klub
představitelé	představitel	k1gMnPc1	představitel
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
postupu	postup	k1gInSc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
společná	společný	k2eAgFnSc1d1	společná
platforma	platforma	k1gFnSc1	platforma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
jednali	jednat	k5eAaImAgMnP	jednat
zástupci	zástupce	k1gMnPc1	zástupce
iniciativy	iniciativa	k1gFnSc2	iniciativa
Most	most	k1gInSc4	most
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Adamcem	Adamec	k1gMnSc7	Adamec
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
vládnoucími	vládnoucí	k2eAgFnPc7d1	vládnoucí
strukturami	struktura	k1gFnPc7	struktura
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
požadovalo	požadovat	k5eAaImAgNnS	požadovat
odstoupení	odstoupení	k1gNnSc3	odstoupení
zkorumpovaných	zkorumpovaný	k2eAgMnPc2d1	zkorumpovaný
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
propuštění	propuštění	k1gNnSc4	propuštění
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
podporu	podpora	k1gFnSc4	podpora
plánované	plánovaný	k2eAgInPc4d1	plánovaný
generální	generální	k2eAgInPc4d1	generální
stávce	stávec	k1gInPc4	stávec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
Verejnosť	Verejnosť	k1gFnSc4	Verejnosť
proti	proti	k7c3	proti
násiliu	násilium	k1gNnSc3	násilium
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Milanem	Milan	k1gMnSc7	Milan
Kňažkem	Kňažek	k1gMnSc7	Kňažek
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
nezapojovala	zapojovat	k5eNaImAgFnS	zapojovat
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
přebírání	přebírání	k1gNnSc2	přebírání
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
jako	jako	k8xS	jako
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
částečně	částečně	k6eAd1	částečně
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
během	během	k7c2	během
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
fámě	fáma	k1gFnSc3	fáma
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
studenta	student	k1gMnSc2	student
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
obviněn	obviněn	k2eAgMnSc1d1	obviněn
Petr	Petr	k1gMnSc1	Petr
Uhl	Uhl	k1gMnSc1	Uhl
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
úmrtí	úmrtí	k1gNnSc4	úmrtí
studenta	student	k1gMnSc2	student
objevila	objevit	k5eAaPmAgFnS	objevit
a	a	k8xC	a
kdo	kdo	k3yQnSc1	kdo
stál	stát	k5eAaImAgMnS	stát
za	za	k7c7	za
jejím	její	k3xOp3gNnSc7	její
šířením	šíření	k1gNnSc7	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Uhl	Uhl	k1gMnSc1	Uhl
již	již	k6eAd1	již
nejspíše	nejspíše	k9	nejspíše
tušil	tušit	k5eAaImAgMnS	tušit
den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fámu	fáma	k1gFnSc4	fáma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpráva	zpráva	k1gFnSc1	zpráva
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
agenturami	agentura	k1gFnPc7	agentura
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
šířila	šířit	k5eAaImAgFnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
bylo	být	k5eAaImAgNnS	být
skrz	skrz	k7c4	skrz
ministryni	ministryně	k1gFnSc4	ministryně
školství	školství	k1gNnSc2	školství
vydáno	vydán	k2eAgNnSc4d1	vydáno
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
student	student	k1gMnSc1	student
Martin	Martin	k1gMnSc1	Martin
Šmíd	Šmíd	k1gMnSc1	Šmíd
byl	být	k5eAaImAgMnS	být
objeven	objeven	k2eAgMnSc1d1	objeven
živý	živý	k1gMnSc1	živý
<g/>
.	.	kIx.	.
</s>
<s>
Ministryně	ministryně	k1gFnSc1	ministryně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
snažila	snažit	k5eAaImAgFnS	snažit
dementovat	dementovat	k5eAaBmF	dementovat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
údajné	údajný	k2eAgFnSc6d1	údajná
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Objeveni	objevit	k5eAaPmNgMnP	objevit
byli	být	k5eAaImAgMnP	být
dokonce	dokonce	k9	dokonce
dva	dva	k4xCgMnPc1	dva
studenti	student	k1gMnPc1	student
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
studovali	studovat	k5eAaImAgMnP	studovat
druhý	druhý	k4xOgInSc4	druhý
ročník	ročník	k1gInSc4	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
ve	v	k7c6	v
večerním	večerní	k2eAgNnSc6d1	večerní
vysílání	vysílání	k1gNnSc6	vysílání
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
oběma	dva	k4xCgMnPc7	dva
studenty	student	k1gMnPc7	student
Záznam	záznam	k1gInSc1	záznam
však	však	k9	však
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
<g/>
,	,	kIx,	,
před	před	k7c7	před
anonymní	anonymní	k2eAgFnSc7d1	anonymní
bílou	bílý	k2eAgFnSc7d1	bílá
stěnou	stěna	k1gFnSc7	stěna
připomínající	připomínající	k2eAgFnSc3d1	připomínající
nemocnici	nemocnice	k1gFnSc3	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
z	z	k7c2	z
třesoucí	třesoucí	k2eAgFnSc2d1	třesoucí
se	se	k3xPyFc4	se
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
s	s	k7c7	s
vypadávajícími	vypadávající	k2eAgFnPc7d1	vypadávající
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
kvalitou	kvalita	k1gFnSc7	kvalita
připomínal	připomínat	k5eAaImAgMnS	připomínat
pirátské	pirátský	k2eAgFnPc4d1	pirátská
videokazety	videokazeta	k1gFnPc4	videokazeta
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
podvrh	podvrh	k1gInSc4	podvrh
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
tomuto	tento	k3xDgMnSc3	tento
dementování	dementování	k1gNnSc1	dementování
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
mrtvém	mrtvý	k1gMnSc6	mrtvý
studentovi	student	k1gMnSc6	student
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
konala	konat	k5eAaImAgFnS	konat
smuteční	smuteční	k2eAgFnSc1d1	smuteční
akce	akce	k1gFnSc1	akce
za	za	k7c4	za
údajně	údajně	k6eAd1	údajně
ubitého	ubitý	k2eAgMnSc4d1	ubitý
studenta	student	k1gMnSc4	student
Martina	Martin	k1gMnSc4	Martin
Šmída	Šmíd	k1gMnSc4	Šmíd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
sešel	sejít	k5eAaPmAgInS	sejít
dav	dav	k1gInSc1	dav
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Národní	národní	k2eAgFnSc3d1	národní
třídě	třída	k1gFnSc3	třída
a	a	k8xC	a
přes	přes	k7c4	přes
most	most	k1gInSc4	most
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mostě	most	k1gInSc6	most
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gFnSc2	máj
byl	být	k5eAaImAgInS	být
však	však	k9	však
pořádkovými	pořádkový	k2eAgFnPc7d1	pořádková
silami	síla	k1gFnPc7	síla
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pondělí	pondělí	k1gNnSc2	pondělí
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
pražských	pražský	k2eAgFnPc2d1	Pražská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
národní	národní	k2eAgFnPc1d1	národní
vlády	vláda	k1gFnPc1	vláda
i	i	k8xC	i
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
ČSSR	ČSSR	kA	ČSSR
vyjádřily	vyjádřit	k5eAaPmAgFnP	vyjádřit
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
policejním	policejní	k2eAgInSc7d1	policejní
zákrokem	zákrok	k1gInSc7	zákrok
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
demonstranty	demonstrant	k1gMnPc7	demonstrant
za	za	k7c2	za
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
skrze	skrze	k?	skrze
stávky	stávek	k1gInPc4	stávek
vytvářen	vytvářen	k2eAgInSc4d1	vytvářen
nátlak	nátlak	k1gInSc4	nátlak
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
konaly	konat	k5eAaImAgFnP	konat
pokojné	pokojný	k2eAgFnPc1d1	pokojná
demonstrace	demonstrace	k1gFnPc1	demonstrace
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
demonstrace	demonstrace	k1gFnPc1	demonstrace
probíhaly	probíhat	k5eAaImAgFnP	probíhat
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
sešlo	sejít	k5eAaPmAgNnS	sejít
okolo	okolo	k7c2	okolo
čtyř	čtyři	k4xCgFnPc2	čtyři
hodin	hodina	k1gFnPc2	hodina
odpoledne	odpoledne	k1gNnSc2	odpoledne
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
až	až	k9	až
150	[number]	k4	150
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
až	až	k9	až
40	[number]	k4	40
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
režim	režim	k1gInSc4	režim
složité	složitý	k2eAgNnSc1d1	složité
zastavit	zastavit	k5eAaPmF	zastavit
demonstrace	demonstrace	k1gFnPc4	demonstrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
zcela	zcela	k6eAd1	zcela
vymkly	vymknout	k5eAaPmAgFnP	vymknout
předchozím	předchozí	k2eAgFnPc3d1	předchozí
demonstracím	demonstrace	k1gFnPc3	demonstrace
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
špičky	špička	k1gFnPc1	špička
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
víkend	víkend	k1gInSc4	víkend
o	o	k7c4	o
vývoj	vývoj	k1gInSc4	vývoj
situace	situace	k1gFnSc2	situace
údajně	údajně	k6eAd1	údajně
příliš	příliš	k6eAd1	příliš
nestaraly	starat	k5eNaImAgFnP	starat
a	a	k8xC	a
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
úspěšným	úspěšný	k2eAgInPc3d1	úspěšný
protikrokům	protikrok	k1gInPc3	protikrok
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
politbyra	politbyro	k1gNnSc2	politbyro
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
Jan	Jan	k1gMnSc1	Jan
Fojtík	Fojtík	k1gMnSc1	Fojtík
údajně	údajně	k6eAd1	údajně
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
"	"	kIx"	"
<g/>
Tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
konec	konec	k1gInSc1	konec
<g/>
!	!	kIx.	!
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
žádnou	žádný	k3yNgFnSc7	žádný
naší	náš	k3xOp1gFnSc7	náš
kontrademonstrací	kontrademonstrace	k1gFnSc7	kontrademonstrace
nepřebijem	nepřebijem	k?	nepřebijem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
začaly	začít	k5eAaPmAgFnP	začít
vycházet	vycházet	k5eAaImF	vycházet
první	první	k4xOgFnPc1	první
tištěné	tištěný	k2eAgFnPc1d1	tištěná
necenzurované	cenzurovaný	k2eNgFnPc1d1	necenzurovaná
oficiální	oficiální	k2eAgFnPc1d1	oficiální
noviny	novina	k1gFnPc1	novina
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
slovo	slovo	k1gNnSc1	slovo
spadající	spadající	k2eAgNnSc1d1	spadající
pod	pod	k7c4	pod
Československou	československý	k2eAgFnSc4d1	Československá
stranu	strana	k1gFnSc4	strana
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
</s>
<s>
Delegace	delegace	k1gFnSc1	delegace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
se	se	k3xPyFc4	se
dopoledne	dopoledne	k6eAd1	dopoledne
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
díky	díky	k7c3	díky
aktivitě	aktivita	k1gFnSc3	aktivita
iniciativy	iniciativa	k1gFnSc2	iniciativa
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Stávka	stávka	k1gFnSc1	stávka
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
území	území	k1gNnSc2	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
jednotně	jednotně	k6eAd1	jednotně
<g/>
.	.	kIx.	.
</s>
<s>
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
pokračující	pokračující	k2eAgFnSc4d1	pokračující
demonstraci	demonstrace	k1gFnSc4	demonstrace
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
současně	současně	k6eAd1	současně
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
první	první	k4xOgFnSc1	první
manifestace	manifestace	k1gFnSc1	manifestace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
Melantrichova	Melantrichův	k2eAgNnSc2d1	Melantrichovo
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
promluvil	promluvit	k5eAaPmAgMnS	promluvit
poprvé	poprvé	k6eAd1	poprvé
před	před	k7c7	před
diváky	divák	k1gMnPc7	divák
spisovatel	spisovatel	k1gMnSc1	spisovatel
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
těchto	tento	k3xDgInPc2	tento
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
klíče	klíč	k1gInPc1	klíč
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
cinkot	cinkot	k1gInSc1	cinkot
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
ČSSR	ČSSR	kA	ČSSR
zasedalo	zasedat	k5eAaImAgNnS	zasedat
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c4	o
nasazení	nasazení	k1gNnSc4	nasazení
vojska	vojsko	k1gNnSc2	vojsko
proti	proti	k7c3	proti
demonstrantům	demonstrant	k1gMnPc3	demonstrant
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
vlády	vláda	k1gFnSc2	vláda
si	se	k3xPyFc3	se
uvědomovalo	uvědomovat	k5eAaImAgNnS	uvědomovat
závažnost	závažnost	k1gFnSc4	závažnost
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
schůzka	schůzka	k1gFnSc1	schůzka
zástupců	zástupce	k1gMnPc2	zástupce
veřejnosti	veřejnost	k1gFnSc2	veřejnost
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Adamcem	Adamec	k1gMnSc7	Adamec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
a	a	k8xC	a
schůzka	schůzka	k1gFnSc1	schůzka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
konala	konat	k5eAaImAgFnS	konat
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gFnSc2	jeho
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
Miloš	Miloš	k1gMnSc1	Miloš
Jakeš	Jakeš	k1gMnSc1	Jakeš
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
linie	linie	k1gFnSc1	linie
socialistického	socialistický	k2eAgInSc2d1	socialistický
rozvoje	rozvoj	k1gInSc2	rozvoj
státu	stát	k1gInSc2	stát
nebude	být	k5eNaImBp3nS	být
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Prahy	Praha	k1gFnSc2	Praha
přesunovaly	přesunovat	k5eAaImAgFnP	přesunovat
značné	značný	k2eAgFnPc1d1	značná
pořádkové	pořádkový	k2eAgFnPc1d1	pořádková
posily	posila	k1gFnPc1	posila
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Lidových	lidový	k2eAgFnPc2d1	lidová
milic	milice	k1gFnPc2	milice
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
až	až	k9	až
o	o	k7c4	o
4	[number]	k4	4
000	[number]	k4	000
milicionářů	milicionář	k1gMnPc2	milicionář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
spor	spor	k1gInSc1	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
milice	milice	k1gFnSc1	milice
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
povolal	povolat	k5eAaPmAgMnS	povolat
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Jakeš	Jakeš	k1gMnSc1	Jakeš
odmítal	odmítat	k5eAaImAgMnS	odmítat
nést	nést	k5eAaImF	nést
za	za	k7c4	za
jejich	jejich	k3xOp3gInSc4	jejich
příjezd	příjezd	k1gInSc4	příjezd
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k9	ale
vyvracel	vyvracet	k5eAaImAgMnS	vyvracet
bývalý	bývalý	k2eAgMnSc1d1	bývalý
náčelník	náčelník	k1gMnSc1	náčelník
hlavního	hlavní	k2eAgInSc2d1	hlavní
štábu	štáb	k1gInSc2	štáb
Lidových	lidový	k2eAgFnPc2d1	lidová
milic	milice	k1gFnPc2	milice
Miroslav	Miroslav	k1gMnSc1	Miroslav
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Tomášek	Tomášek	k1gMnSc1	Tomášek
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobýval	pobývat	k5eAaImAgMnS	pobývat
okolo	okolo	k7c2	okolo
svatořečení	svatořečení	k1gNnSc2	svatořečení
Anežky	Anežka	k1gFnSc2	Anežka
České	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
"	"	kIx"	"
<g/>
Všemu	všecek	k3xTgNnSc3	všecek
lidu	lido	k1gNnSc3	lido
Československa	Československo	k1gNnSc2	Československo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
podpořil	podpořit	k5eAaPmAgMnS	podpořit
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
demokratické	demokratický	k2eAgFnPc4d1	demokratická
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
demonstrace	demonstrace	k1gFnPc4	demonstrace
také	také	k9	také
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
sešlo	sejít	k5eAaPmAgNnS	sejít
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
oslavující	oslavující	k2eAgFnSc1d1	oslavující
Alexandra	Alexandra	k1gFnSc1	Alexandra
Dubčeka	Dubčeek	k1gInSc2	Dubčeek
a	a	k8xC	a
dožadující	dožadující	k2eAgNnSc4d1	dožadující
se	se	k3xPyFc4	se
odstoupení	odstoupení	k1gNnSc4	odstoupení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Dubček	Dubček	k1gMnSc1	Dubček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
projevem	projev	k1gInSc7	projev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
SNP	SNP	kA	SNP
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
podporu	podpora	k1gFnSc4	podpora
demonstrujícím	demonstrující	k2eAgNnSc7d1	demonstrující
a	a	k8xC	a
požadovat	požadovat	k5eAaImF	požadovat
společenské	společenský	k2eAgFnPc4d1	společenská
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
řečníci	řečník	k1gMnPc1	řečník
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Milan	Milan	k1gMnSc1	Milan
Kňažko	Kňažka	k1gFnSc5	Kňažka
a	a	k8xC	a
Ján	Ján	k1gMnSc1	Ján
Budaj	Budaj	k1gMnSc1	Budaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
opět	opět	k6eAd1	opět
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodiny	hodina	k1gFnPc1	hodina
začaly	začít	k5eAaPmAgFnP	začít
proslovy	proslov	k1gInPc4	proslov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
předseda	předseda	k1gMnSc1	předseda
Městského	městský	k2eAgInSc2d1	městský
výboru	výbor	k1gInSc2	výbor
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
socialistické	socialistický	k2eAgFnSc2d1	socialistická
Petr	Petr	k1gMnSc1	Petr
Mišoň	Mišoň	k1gMnSc1	Mišoň
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Petr	Petr	k1gMnSc1	Petr
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Odložil	odložit	k5eAaPmAgMnS	odložit
<g/>
,	,	kIx,	,
či	či	k8xC	či
Jiří	Jiří	k1gMnSc1	Jiří
Černý	Černý	k1gMnSc1	Černý
vyzývající	vyzývající	k2eAgInSc4d1	vyzývající
k	k	k7c3	k
pravdivým	pravdivý	k2eAgFnPc3d1	pravdivá
informacím	informace	k1gFnPc3	informace
ve	v	k7c6	v
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
skončilo	skončit	k5eAaPmAgNnS	skončit
prvním	první	k4xOgMnSc7	první
masovým	masový	k2eAgNnSc7d1	masové
veřejným	veřejný	k2eAgNnSc7d1	veřejné
vystoupením	vystoupení	k1gNnSc7	vystoupení
Marty	Marta	k1gFnSc2	Marta
Kubišové	Kubišové	k2eAgFnSc2d1	Kubišové
po	po	k7c6	po
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zpívala	zpívat	k5eAaImAgFnS	zpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Modlitba	modlitba	k1gFnSc1	modlitba
pro	pro	k7c4	pro
Martu	Marta	k1gFnSc4	Marta
<g/>
"	"	kIx"	"
a	a	k8xC	a
následně	následně	k6eAd1	následně
zpěvem	zpěv	k1gInSc7	zpěv
státní	státní	k2eAgFnSc2d1	státní
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
byly	být	k5eAaImAgFnP	být
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgFnP	připojit
ke	k	k7c3	k
stávce	stávka	k1gFnSc3	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
Lidových	lidový	k2eAgFnPc2d1	lidová
milic	milice	k1gFnPc2	milice
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
až	až	k9	až
o	o	k7c4	o
17	[number]	k4	17
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
lidové	lidový	k2eAgFnPc1d1	lidová
milice	milice	k1gFnPc1	milice
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Miloše	Miloš	k1gMnSc2	Miloš
Jakeše	Jakeš	k1gMnSc2	Jakeš
odvolány	odvolán	k2eAgInPc1d1	odvolán
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
zasedalo	zasedat	k5eAaImAgNnS	zasedat
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
zaznělo	zaznět	k5eAaImAgNnS	zaznět
<g/>
,	,	kIx,	,
že	že	k8xS	že
akce	akce	k1gFnSc1	akce
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
nebyla	být	k5eNaImAgFnS	být
náhodná	náhodný	k2eAgFnSc1d1	náhodná
a	a	k8xC	a
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
opozičními	opoziční	k2eAgFnPc7d1	opoziční
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
proti	proti	k7c3	proti
demonstrujícím	demonstrující	k2eAgFnPc3d1	demonstrující
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc4	Pithart
napsal	napsat	k5eAaPmAgMnS	napsat
stávkujícím	stávkující	k2eAgMnSc7d1	stávkující
studentům	student	k1gMnPc3	student
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
mírnil	mírnit	k5eAaImAgInS	mírnit
radikalismus	radikalismus	k1gInSc1	radikalismus
studentů	student	k1gMnPc2	student
a	a	k8xC	a
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
házením	házení	k1gNnSc7	házení
všech	všecek	k3xTgMnPc2	všecek
protivníků	protivník	k1gMnPc2	protivník
do	do	k7c2	do
"	"	kIx"	"
<g/>
jednoho	jeden	k4xCgMnSc2	jeden
pytle	pytel	k1gInSc2	pytel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
armády	armáda	k1gFnSc2	armáda
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
bylo	být	k5eAaImAgNnS	být
odsouhlaseno	odsouhlasen	k2eAgNnSc1d1	odsouhlaseno
<g/>
,	,	kIx,	,
že	že	k8xS	že
armáda	armáda	k1gFnSc1	armáda
stojí	stát	k5eAaImIp3nS	stát
jasně	jasně	k6eAd1	jasně
za	za	k7c4	za
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Václavík	Václavík	k1gMnSc1	Václavík
dokonce	dokonce	k9	dokonce
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
použít	použít	k5eAaPmF	použít
armádu	armáda	k1gFnSc4	armáda
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
pořádku	pořádek	k1gInSc2	pořádek
<g/>
;	;	kIx,	;
návrh	návrh	k1gInSc1	návrh
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
publikaci	publikace	k1gFnSc3	publikace
prohlášení	prohlášení	k1gNnSc2	prohlášení
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
opět	opět	k6eAd1	opět
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
své	svůj	k3xOyFgInPc4	svůj
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nebyly	být	k5eNaImAgInP	být
splněny	splnit	k5eAaPmNgInP	splnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzývalo	vyzývat	k5eAaImAgNnS	vyzývat
občany	občan	k1gMnPc4	občan
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
se	se	k3xPyFc4	se
k	k	k7c3	k
plánované	plánovaný	k2eAgFnSc3d1	plánovaná
generální	generální	k2eAgFnSc3d1	generální
stávce	stávka	k1gFnSc3	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgMnSc1d1	komunistický
poslanec	poslanec	k1gMnSc1	poslanec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Štěpán	Štěpán	k1gMnSc1	Štěpán
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
mítink	mítink	k1gInSc4	mítink
dělníků	dělník	k1gMnPc2	dělník
do	do	k7c2	do
vysočanské	vysočanský	k2eAgFnSc2d1	Vysočanská
ČKD	ČKD	kA	ČKD
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
-	-	kIx~	-
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
projevu	projev	k1gInSc2	projev
nazval	nazvat	k5eAaBmAgMnS	nazvat
protestující	protestující	k2eAgMnPc4d1	protestující
studenty	student	k1gMnPc4	student
"	"	kIx"	"
<g/>
patnáctiletými	patnáctiletý	k2eAgFnPc7d1	patnáctiletá
dětmi	dítě	k1gFnPc7	dítě
<g/>
"	"	kIx"	"
a	a	k8xC	a
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
dělníky	dělník	k1gMnPc4	dělník
vypískán	vypískán	k2eAgMnSc1d1	vypískán
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc4	tisíc
dělníků	dělník	k1gMnPc2	dělník
z	z	k7c2	z
ČKD	ČKD	kA	ČKD
přidalo	přidat	k5eAaPmAgNnS	přidat
k	k	k7c3	k
demonstraci	demonstrace	k1gFnSc3	demonstrace
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Vypískání	vypískání	k1gNnSc1	vypískání
Miroslava	Miroslav	k1gMnSc2	Miroslav
Štěpána	Štěpán	k1gMnSc2	Štěpán
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
zúčastněnými	zúčastněný	k2eAgFnPc7d1	zúčastněná
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
zlomový	zlomový	k2eAgInSc4d1	zlomový
okamžik	okamžik	k1gInSc4	okamžik
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
totiž	totiž	k9	totiž
existovala	existovat	k5eAaImAgFnS	existovat
reálná	reálný	k2eAgFnSc1d1	reálná
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
revoluce	revoluce	k1gFnSc1	revoluce
bude	být	k5eAaImBp3nS	být
vojensky	vojensky	k6eAd1	vojensky
potlačena	potlačen	k2eAgFnSc1d1	potlačena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnPc4	událost
komunisté	komunista	k1gMnPc1	komunista
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztratili	ztratit	k5eAaPmAgMnP	ztratit
podporu	podpora	k1gFnSc4	podpora
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
celá	celý	k2eAgNnPc1d1	celé
desetiletí	desetiletí	k1gNnPc1	desetiletí
odvolávali	odvolávat	k5eAaImAgMnP	odvolávat
-	-	kIx~	-
pracujícího	pracující	k2eAgInSc2d1	pracující
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
komunisté	komunista	k1gMnPc1	komunista
víceméně	víceméně	k9	víceméně
jen	jen	k9	jen
opatrně	opatrně	k6eAd1	opatrně
ustupovali	ustupovat	k5eAaImAgMnP	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
se	se	k3xPyFc4	se
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
odehrála	odehrát	k5eAaPmAgFnS	odehrát
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
největší	veliký	k2eAgFnSc4d3	veliký
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
přes	přes	k7c4	přes
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Melantrich	Melantricha	k1gFnPc2	Melantricha
postupně	postupně	k6eAd1	postupně
hovořil	hovořit	k5eAaImAgMnS	hovořit
herec	herec	k1gMnSc1	herec
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
starší	starší	k1gMnSc1	starší
či	či	k8xC	či
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
byla	být	k5eAaImAgFnS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
opět	opět	k6eAd1	opět
zpěvem	zpěv	k1gInSc7	zpěv
státní	státní	k2eAgFnSc2d1	státní
hymny	hymna	k1gFnSc2	hymna
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Marty	Marta	k1gFnSc2	Marta
Kubišové	Kubišová	k1gFnSc2	Kubišová
a	a	k8xC	a
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
protestních	protestní	k2eAgFnPc2d1	protestní
akcí	akce	k1gFnPc2	akce
účastnilo	účastnit	k5eAaImAgNnS	účastnit
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
okolo	okolo	k7c2	okolo
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
značných	značný	k2eAgFnPc2d1	značná
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
KSČ	KSČ	kA	KSČ
zasedal	zasedat	k5eAaImAgInS	zasedat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
skončilo	skončit	k5eAaPmAgNnS	skončit
po	po	k7c6	po
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodině	hodina	k1gFnSc3	hodina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Miloš	Miloš	k1gMnSc1	Miloš
Jakeš	Jakeš	k1gMnSc1	Jakeš
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
vedením	vedení	k1gNnSc7	vedení
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
umožnit	umožnit	k5eAaPmF	umožnit
obměnu	obměna	k1gFnSc4	obměna
kádrového	kádrový	k2eAgNnSc2d1	kádrové
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
funkci	funkce	k1gFnSc6	funkce
nadále	nadále	k6eAd1	nadále
setrvával	setrvávat	k5eAaImAgMnS	setrvávat
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
stanu	stan	k1gInSc2	stan
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
v	v	k7c6	v
Laterně	laterna	k1gFnSc6	laterna
magice	magika	k1gFnSc6	magika
během	během	k7c2	během
proslovu	proslov	k1gInSc2	proslov
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
veřejně	veřejně	k6eAd1	veřejně
přečtena	přečten	k2eAgFnSc1d1	přečtena
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
následovaly	následovat	k5eAaImAgFnP	následovat
mohutné	mohutný	k2eAgFnPc1d1	mohutná
oslavy	oslava	k1gFnPc1	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
KSČ	KSČ	kA	KSČ
se	s	k7c7	s
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
až	až	k9	až
do	do	k7c2	do
pozdních	pozdní	k2eAgFnPc2d1	pozdní
večerních	večerní	k2eAgFnPc2d1	večerní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
skončení	skončení	k1gNnSc6	skončení
okolo	okolo	k7c2	okolo
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
tisková	tiskový	k2eAgFnSc1d1	tisková
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
bylo	být	k5eAaImAgNnS	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Hoření	hoření	k1gNnSc2	hoření
představeno	představen	k2eAgNnSc4d1	představeno
nové	nový	k2eAgNnSc4d1	nové
vedení	vedení	k1gNnSc4	vedení
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vyjádření	vyjádření	k1gNnPc2	vyjádření
Hoření	hoření	k1gNnPc2	hoření
bylo	být	k5eAaImAgNnS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgMnS	mít
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
požadavcích	požadavek	k1gInPc6	požadavek
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
konference	konference	k1gFnSc1	konference
skončila	skončit	k5eAaPmAgFnS	skončit
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k6eAd1	půl
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
kriticky	kriticky	k6eAd1	kriticky
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
vedení	vedení	k1gNnSc3	vedení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nepovažovalo	považovat	k5eNaImAgNnS	považovat
kádrové	kádrový	k2eAgFnPc4d1	kádrová
změny	změna	k1gFnPc4	změna
za	za	k7c4	za
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
udělil	udělit	k5eAaPmAgMnS	udělit
amnestii	amnestie	k1gFnSc4	amnestie
těmto	tento	k3xDgMnPc3	tento
sedmi	sedm	k4xCc2	sedm
politickým	politický	k2eAgMnPc3d1	politický
vězňům	vězeň	k1gMnPc3	vězeň
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Jánu	Ján	k1gMnSc3	Ján
Čarnogurskému	Čarnogurský	k2eAgMnSc3d1	Čarnogurský
<g/>
,	,	kIx,	,
PhDr.	PhDr.	kA	PhDr.
Miroslavu	Miroslav	k1gMnSc3	Miroslav
Kusému	kusý	k2eAgMnSc3d1	kusý
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
Jiřímu	Jiří	k1gMnSc3	Jiří
Rumlovi	Ruml	k1gMnSc3	Ruml
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Petru	Petr	k1gMnSc3	Petr
Uhlovi	Uhla	k1gMnSc3	Uhla
<g/>
,	,	kIx,	,
Rudolfu	Rudolf	k1gMnSc3	Rudolf
Zemanovi	Zeman	k1gMnSc3	Zeman
<g/>
,	,	kIx,	,
Ivanu	Ivan	k1gMnSc3	Ivan
Jirousovi	Jirous	k1gMnSc3	Jirous
<g/>
,	,	kIx,	,
Ivanu	Ivan	k1gMnSc3	Ivan
Polanskému	Polanský	k1gMnSc3	Polanský
a	a	k8xC	a
amnestován	amnestován	k2eAgMnSc1d1	amnestován
byl	být	k5eAaImAgMnS	být
taktéž	taktéž	k?	taktéž
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
VOKNO	VOKNO	kA	VOKNO
František	František	k1gMnSc1	František
Stárek	Stárek	k1gMnSc1	Stárek
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgNnSc2	tento
doporučilo	doporučit	k5eAaPmAgNnS	doporučit
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
i	i	k9	i
nestraníci	nestraník	k1gMnPc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k1gNnSc1	dopoledne
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
sloužil	sloužit	k5eAaImAgMnS	sloužit
kardinál	kardinál	k1gMnSc1	kardinál
Tomášek	Tomášek	k1gMnSc1	Tomášek
mši	mše	k1gFnSc4	mše
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
svatořečení	svatořečení	k1gNnSc2	svatořečení
Anežky	Anežka	k1gFnSc2	Anežka
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
jím	jíst	k5eAaImIp1nS	jíst
podpořený	podpořený	k2eAgInSc1d1	podpořený
dav	dav	k1gInSc1	dav
(	(	kIx(	(
<g/>
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc1	tisíc
před	před	k7c7	před
chrámem	chrám	k1gInSc7	chrám
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
manifestaci	manifestace	k1gFnSc3	manifestace
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
demonstrovalo	demonstrovat	k5eAaBmAgNnS	demonstrovat
kolem	kolem	k7c2	kolem
750	[number]	k4	750
až	až	k9	až
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
svojí	svůj	k3xOyFgFnSc7	svůj
podporu	podpora	k1gFnSc4	podpora
Občanskému	občanský	k2eAgInSc3d1	občanský
fóru	fór	k1gInSc3	fór
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
začala	začít	k5eAaPmAgFnS	začít
dění	dění	k1gNnSc4	dění
šířit	šířit	k5eAaImF	šířit
přímými	přímý	k2eAgInPc7d1	přímý
přenosy	přenos	k1gInPc7	přenos
a	a	k8xC	a
současně	současně	k6eAd1	současně
vydala	vydat	k5eAaPmAgFnS	vydat
prohlášení	prohlášení	k1gNnSc3	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Československým	československý	k2eAgInSc7d1	československý
rozhlasem	rozhlas	k1gInSc7	rozhlas
a	a	k8xC	a
pražským	pražský	k2eAgNnSc7d1	Pražské
metrem	metro	k1gNnSc7	metro
chystají	chystat	k5eAaImIp3nP	chystat
podpořit	podpořit	k5eAaPmF	podpořit
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
demonstrace	demonstrace	k1gFnSc2	demonstrace
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
i	i	k9	i
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hutka	Hutka	k1gMnSc1	Hutka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
byl	být	k5eAaImAgInS	být
vpuštěn	vpustit	k5eAaPmNgInS	vpustit
do	do	k7c2	do
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
funkcionářů	funkcionář	k1gMnPc2	funkcionář
KSČ	KSČ	kA	KSČ
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
také	také	k6eAd1	také
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
východisko	východisko	k1gNnSc4	východisko
ze	z	k7c2	z
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
konat	konat	k5eAaImF	konat
příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
primátor	primátor	k1gMnSc1	primátor
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
stávkující	stávkující	k1gMnPc4	stávkující
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zachovali	zachovat	k5eAaPmAgMnP	zachovat
provoz	provoz	k1gInSc4	provoz
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
další	další	k2eAgFnSc1d1	další
masová	masový	k2eAgFnSc1d1	masová
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
řečníky	řečník	k1gMnPc4	řečník
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jak	jak	k8xS	jak
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Ladislav	Ladislav	k1gMnSc1	Ladislav
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
slíbil	slíbit	k5eAaPmAgMnS	slíbit
pokračování	pokračování	k1gNnSc3	pokračování
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
dialogu	dialog	k1gInSc6	dialog
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
odrazoval	odrazovat	k5eAaImAgMnS	odrazovat
od	od	k7c2	od
generální	generální	k2eAgFnSc2d1	generální
stávky	stávka	k1gFnSc2	stávka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ztratil	ztratit	k5eAaPmAgInS	ztratit
přízeň	přízeň	k1gFnSc4	přízeň
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
demonstrace	demonstrace	k1gFnSc1	demonstrace
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
lidský	lidský	k2eAgInSc1d1	lidský
řetěz	řetěz	k1gInSc1	řetěz
z	z	k7c2	z
Letné	Letná	k1gFnSc2	Letná
až	až	k9	až
k	k	k7c3	k
Pražskému	pražský	k2eAgInSc3d1	pražský
hradu	hrad	k1gInSc3	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
v	v	k7c6	v
Obecním	obecní	k2eAgInSc6d1	obecní
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
setkávají	setkávat	k5eAaImIp3nP	setkávat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
delegace	delegace	k1gFnSc2	delegace
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
s	s	k7c7	s
federálním	federální	k2eAgMnSc7d1	federální
premiérem	premiér	k1gMnSc7	premiér
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Adamcem	Adamec	k1gMnSc7	Adamec
na	na	k7c6	na
prvních	první	k4xOgInPc6	první
politických	politický	k2eAgInPc6d1	politický
rozhovorech	rozhovor	k1gInPc6	rozhovor
zprostředkovaných	zprostředkovaný	k2eAgFnPc2d1	zprostředkovaná
Iniciativou	iniciativa	k1gFnSc7	iniciativa
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
odstoupilo	odstoupit	k5eAaPmAgNnS	odstoupit
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
ČSL	ČSL	kA	ČSL
a	a	k8xC	a
předalo	předat	k5eAaPmAgNnS	předat
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
tzv.	tzv.	kA	tzv.
Obrodnému	obrodný	k2eAgInSc3d1	obrodný
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
jednoznačně	jednoznačně	k6eAd1	jednoznačně
podpořila	podpořit	k5eAaPmAgFnS	podpořit
sametovou	sametový	k2eAgFnSc4d1	sametová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
mezi	mezi	k7c4	mezi
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
až	až	k9	až
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
dlouho	dlouho	k6eAd1	dlouho
avizovaná	avizovaný	k2eAgFnSc1d1	avizovaná
dvouhodinová	dvouhodinový	k2eAgFnSc1d1	dvouhodinová
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
na	na	k7c4	na
75	[number]	k4	75
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Provozy	provoz	k1gInPc1	provoz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ke	k	k7c3	k
stávce	stávec	k1gInSc2	stávec
přidaly	přidat	k5eAaPmAgInP	přidat
manifestačně	manifestačně	k6eAd1	manifestačně
vyjádřením	vyjádření	k1gNnSc7	vyjádření
solidarity	solidarita	k1gFnSc2	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Stávka	stávka	k1gFnSc1	stávka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
Konec	konec	k1gInSc1	konec
vlády	vláda	k1gFnSc2	vláda
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
sešlo	sejít	k5eAaPmAgNnS	sejít
v	v	k7c6	v
poledních	polední	k2eAgFnPc6d1	polední
hodinách	hodina	k1gFnPc6	hodina
okolo	okolo	k7c2	okolo
250	[number]	k4	250
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
až	až	k6eAd1	až
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Hymnu	hymna	k1gFnSc4	hymna
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gottem	Gott	k1gMnSc7	Gott
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
demonstrací	demonstrace	k1gFnPc2	demonstrace
a	a	k8xC	a
rozpadu	rozpad	k1gInSc2	rozpad
ostatních	ostatní	k2eAgInPc2d1	ostatní
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
se	se	k3xPyFc4	se
octla	octnout	k5eAaPmAgFnS	octnout
i	i	k9	i
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
občanů	občan	k1gMnPc2	občan
také	také	k9	také
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
mocenského	mocenský	k2eAgInSc2d1	mocenský
monopolu	monopol	k1gInSc2	monopol
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
slíbil	slíbit	k5eAaPmAgMnS	slíbit
Ladislav	Ladislav	k1gMnSc1	Ladislav
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
že	že	k8xS	že
připraví	připravit	k5eAaPmIp3nS	připravit
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
schválilo	schválit	k5eAaPmAgNnS	schválit
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zrušení	zrušení	k1gNnSc4	zrušení
ústavního	ústavní	k2eAgInSc2d1	ústavní
článku	článek	k1gInSc2	článek
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
úlohy	úloha	k1gFnSc2	úloha
KSČ	KSČ	kA	KSČ
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
výchovy	výchova	k1gFnSc2	výchova
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
články	článek	k1gInPc1	článek
narážely	narážet	k5eAaImAgInP	narážet
na	na	k7c4	na
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
úlohu	úloha	k1gFnSc4	úloha
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
článku	článek	k1gInSc2	článek
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
i	i	k9	i
většina	většina	k1gFnSc1	většina
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
výuky	výuka	k1gFnSc2	výuka
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
kterých	který	k3yRgFnPc2	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
vyučovány	vyučován	k2eAgFnPc4d1	vyučována
společenské	společenský	k2eAgFnPc4d1	společenská
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Adamec	Adamec	k1gMnSc1	Adamec
představil	představit	k5eAaPmAgMnS	představit
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
svoji	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
15	[number]	k4	15
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
1	[number]	k4	1
lidovce	lidovec	k1gMnSc2	lidovec
<g/>
,	,	kIx,	,
1	[number]	k4	1
socialisty	socialist	k1gMnPc7	socialist
a	a	k8xC	a
3	[number]	k4	3
nestraníků	nestraník	k1gMnPc2	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
že	že	k8xS	že
zcela	zcela	k6eAd1	zcela
nepochopilo	pochopit	k5eNaPmAgNnS	pochopit
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
již	již	k6eAd1	již
dále	daleko	k6eAd2	daleko
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
zůstali	zůstat	k5eAaPmAgMnP	zůstat
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgNnSc4d1	nové
vedení	vedení	k1gNnSc4	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
okamžitě	okamžitě	k6eAd1	okamžitě
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
kritizovalo	kritizovat	k5eAaImAgNnS	kritizovat
a	a	k8xC	a
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
podporu	podpora	k1gFnSc4	podpora
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
dále	daleko	k6eAd2	daleko
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
komunistů	komunista	k1gMnPc2	komunista
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
by	by	kYmCp3nS	by
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
další	další	k2eAgFnSc4d1	další
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
i	i	k8xC	i
přesto	přesto	k8xC	přesto
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
Gustávem	Gustáv	k1gMnSc7	Gustáv
Husákem	Husák	k1gMnSc7	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
letech	let	k1gInPc6	let
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
otevřely	otevřít	k5eAaPmAgFnP	otevřít
hranice	hranice	k1gFnPc1	hranice
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
svobodně	svobodně	k6eAd1	svobodně
vycestovat	vycestovat	k5eAaPmF	vycestovat
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
Rakouska	Rakousko	k1gNnSc2	Rakousko
bez	bez	k7c2	bez
výjezdních	výjezdní	k2eAgFnPc2d1	výjezdní
doložek	doložka	k1gFnPc2	doložka
či	či	k8xC	či
celních	celní	k2eAgNnPc2d1	celní
prohlášení	prohlášení	k1gNnPc2	prohlášení
(	(	kIx(	(
<g/>
během	během	k7c2	během
víkendu	víkend	k1gInSc2	víkend
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Rakousko	Rakousko	k1gNnSc1	Rakousko
až	až	k8xS	až
250	[number]	k4	250
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ohlas	ohlas	k1gInSc1	ohlas
veřejnosti	veřejnost	k1gFnSc2	veřejnost
na	na	k7c4	na
představenou	představený	k2eAgFnSc4d1	představená
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechal	nechat	k5eNaPmAgMnS	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
již	již	k6eAd1	již
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
další	další	k2eAgFnSc1d1	další
masová	masový	k2eAgFnSc1d1	masová
demonstrace	demonstrace	k1gFnSc1	demonstrace
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
sešly	sejít	k5eAaPmAgInP	sejít
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Závěrem	závěrem	k7c2	závěrem
demonstrace	demonstrace	k1gFnSc2	demonstrace
zazpíval	zazpívat	k5eAaPmAgMnS	zazpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Krylem	Kryl	k1gMnSc7	Kryl
státní	státní	k2eAgFnSc4d1	státní
hymnu	hymna	k1gFnSc4	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
21	[number]	k4	21
letech	let	k1gInPc6	let
nucené	nucený	k2eAgFnSc2d1	nucená
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
prezident	prezident	k1gMnSc1	prezident
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
nová	nový	k2eAgFnSc1d1	nová
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Adamcem	Adamec	k1gMnSc7	Adamec
a	a	k8xC	a
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgMnS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
Marián	Marián	k1gMnSc1	Marián
Čalfa	Čalf	k1gMnSc2	Čalf
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
doporučení	doporučení	k1gNnSc1	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
jisté	jistý	k2eAgFnPc4d1	jistá
výhrady	výhrada	k1gFnPc4	výhrada
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jednáním	jednání	k1gNnPc3	jednání
s	s	k7c7	s
Čalfou	Čalfý	k2eAgFnSc7d1	Čalfý
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
přijmout	přijmout	k5eAaPmF	přijmout
jejich	jejich	k3xOp3gInPc4	jejich
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
budoucího	budoucí	k2eAgInSc2d1	budoucí
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
současně	současně	k6eAd1	současně
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
Miloše	Miloš	k1gMnSc4	Miloš
Jakeše	Jakeš	k1gMnSc4	Jakeš
a	a	k8xC	a
Miroslava	Miroslav	k1gMnSc4	Miroslav
Štěpána	Štěpán	k1gMnSc4	Štěpán
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
straně	strana	k1gFnSc6	strana
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
"	"	kIx"	"
<g/>
hrubé	hrubý	k2eAgFnPc1d1	hrubá
politické	politický	k2eAgFnPc1d1	politická
chyby	chyba	k1gFnPc1	chyba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
definitivně	definitivně	k6eAd1	definitivně
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
pro	pro	k7c4	pro
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
po	po	k7c6	po
13	[number]	k4	13
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
komunistický	komunistický	k2eAgMnSc1d1	komunistický
prezident	prezident	k1gMnSc1	prezident
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
první	první	k4xOgFnSc4	první
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
vládu	vláda	k1gFnSc4	vláda
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
komunistů	komunista	k1gMnPc2	komunista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
míst	místo	k1gNnPc2	místo
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
7	[number]	k4	7
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
nestraníky	nestraník	k1gMnPc4	nestraník
<g/>
,	,	kIx,	,
2	[number]	k4	2
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
ČSS	ČSS	kA	ČSS
a	a	k8xC	a
2	[number]	k4	2
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
lidovce	lidovec	k1gMnPc4	lidovec
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Husák	Husák	k1gMnSc1	Husák
následně	následně	k6eAd1	následně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
federálnímu	federální	k2eAgInSc3d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
svoji	svůj	k3xOyFgFnSc4	svůj
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
<s>
StB	StB	k?	StB
se	se	k3xPyFc4	se
již	již	k6eAd1	již
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
začala	začít	k5eAaPmAgFnS	začít
likvidovat	likvidovat	k5eAaBmF	likvidovat
některé	některý	k3yIgInPc4	některý
citlivé	citlivý	k2eAgInPc4d1	citlivý
dokumenty	dokument	k1gInPc4	dokument
v	v	k7c6	v
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
lomu	lom	k1gInSc6	lom
nedaleko	nedaleko	k7c2	nedaleko
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
politickému	politický	k2eAgInSc3d1	politický
vývoji	vývoj	k1gInSc3	vývoj
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
otevření	otevření	k1gNnSc6	otevření
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
se	s	k7c7	s
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
odstraňováním	odstraňování	k1gNnSc7	odstraňování
"	"	kIx"	"
<g/>
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
<g/>
"	"	kIx"	"
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
SRN	SRN	kA	SRN
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
uvolňováním	uvolňování	k1gNnSc7	uvolňování
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
začali	začít	k5eAaPmAgMnP	začít
vracet	vracet	k5eAaImF	vracet
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
emigranti	emigrant	k1gMnPc1	emigrant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
po	po	k7c4	po
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
roky	rok	k1gInPc4	rok
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
ČSSR	ČSSR	kA	ČSSR
vydal	vydat	k5eAaPmAgInS	vydat
po	po	k7c6	po
50	[number]	k4	50
letech	léto	k1gNnPc6	léto
emigrace	emigrace	k1gFnSc2	emigrace
významný	významný	k2eAgMnSc1d1	významný
továrník	továrník	k1gMnSc1	továrník
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
či	či	k8xC	či
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
vydavatel	vydavatel	k1gMnSc1	vydavatel
exilového	exilový	k2eAgInSc2d1	exilový
týdeníku	týdeník	k1gInSc2	týdeník
Pavel	Pavel	k1gMnSc1	Pavel
Tigrid	Tigrid	k1gMnSc1	Tigrid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
sjezdu	sjezd	k1gInSc6	sjezd
KSČ	KSČ	kA	KSČ
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
je	být	k5eAaImIp3nS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
rozpustit	rozpustit	k5eAaPmF	rozpustit
lidové	lidový	k2eAgFnSc2d1	lidová
milice	milice	k1gFnSc2	milice
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
pohotovost	pohotovost	k1gFnSc1	pohotovost
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
přijela	přijet	k5eAaPmAgFnS	přijet
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Koordinačního	koordinační	k2eAgNnSc2d1	koordinační
centra	centrum	k1gNnSc2	centrum
OF	OF	kA	OF
první	první	k4xOgFnSc1	první
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
portugalského	portugalský	k2eAgMnSc4d1	portugalský
prezidenta	prezident	k1gMnSc4	prezident
Mária	Márium	k1gNnSc2	Márium
Soarese	Soarese	k1gFnSc2	Soarese
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
kooptační	kooptační	k2eAgInSc1d1	kooptační
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dočasně	dočasně	k6eAd1	dočasně
umožnil	umožnit	k5eAaPmAgMnS	umožnit
Federálnímu	federální	k2eAgNnSc3d1	federální
shromáždění	shromáždění	k1gNnSc3	shromáždění
a	a	k8xC	a
národním	národní	k2eAgFnPc3d1	národní
radám	rada	k1gFnPc3	rada
dosazovat	dosazovat	k5eAaImF	dosazovat
nové	nový	k2eAgMnPc4d1	nový
poslance	poslanec	k1gMnPc4	poslanec
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
bez	bez	k7c2	bez
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
čerstvě	čerstvě	k6eAd1	čerstvě
kooptovaný	kooptovaný	k2eAgMnSc1d1	kooptovaný
poslanec	poslanec	k1gMnSc1	poslanec
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubček	k1gMnSc1	Dubček
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
po	po	k7c6	po
41	[number]	k4	41
letech	léto	k1gNnPc6	léto
prvním	první	k4xOgMnSc6	první
nekomunistickým	komunistický	k2eNgMnSc7d1	nekomunistický
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
naprosto	naprosto	k6eAd1	naprosto
jednomyslně	jednomyslně	k6eAd1	jednomyslně
i	i	k8xC	i
komunistickými	komunistický	k2eAgMnPc7d1	komunistický
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
nedávna	nedávno	k1gNnSc2	nedávno
politickým	politický	k2eAgMnSc7d1	politický
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
ukončila	ukončit	k5eAaPmAgFnS	ukončit
i	i	k8xC	i
studentské	studentský	k2eAgFnSc2d1	studentská
stávky	stávka	k1gFnSc2	stávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pokračování	pokračování	k1gNnSc3	pokračování
demokratizace	demokratizace	k1gFnSc2	demokratizace
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
připravit	připravit	k5eAaPmF	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
konání	konání	k1gNnSc4	konání
svobodných	svobodný	k2eAgFnPc2d1	svobodná
demokratických	demokratický	k2eAgFnPc2d1	demokratická
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožnil	umožnit	k5eAaPmAgInS	umožnit
politickým	politický	k2eAgFnPc3d1	politická
stranám	strana	k1gFnPc3	strana
odvolat	odvolat	k5eAaPmF	odvolat
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
parlamentů	parlament	k1gInPc2	parlament
i	i	k9	i
z	z	k7c2	z
národních	národní	k2eAgInPc2d1	národní
výborů	výbor	k1gInPc2	výbor
všech	všecek	k3xTgInPc2	všecek
stupňů	stupeň	k1gInPc2	stupeň
poslance	poslanec	k1gMnSc2	poslanec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neskýtali	skýtat	k5eNaImAgMnP	skýtat
záruku	záruka	k1gFnSc4	záruka
demokratického	demokratický	k2eAgNnSc2d1	demokratické
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
kooptace	kooptace	k1gFnSc1	kooptace
poslanců	poslanec	k1gMnPc2	poslanec
i	i	k9	i
do	do	k7c2	do
národních	národní	k2eAgInPc2d1	národní
výborů	výbor	k1gInPc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
silnou	silný	k2eAgFnSc4d1	silná
vlnu	vlna	k1gFnSc4	vlna
rezignací	rezignace	k1gFnPc2	rezignace
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
skutečně	skutečně	k6eAd1	skutečně
politickými	politický	k2eAgFnPc7d1	politická
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
Národní	národní	k2eAgFnSc7d1	národní
frontou	fronta	k1gFnSc7	fronta
ČSSR	ČSSR	kA	ČSSR
odvoláni	odvolat	k5eAaPmNgMnP	odvolat
ti	ten	k3xDgMnPc1	ten
nežádoucí	žádoucí	k2eNgMnPc1d1	nežádoucí
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ještě	ještě	k6eAd1	ještě
nerezignovali	rezignovat	k5eNaBmAgMnP	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
kooptace	kooptace	k1gFnPc1	kooptace
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
byly	být	k5eAaImAgInP	být
provedeny	proveden	k2eAgInPc1d1	proveden
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
schůzi	schůze	k1gFnSc4	schůze
parlamentu	parlament	k1gInSc2	parlament
též	též	k6eAd1	též
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
zkrácení	zkrácení	k1gNnSc4	zkrácení
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
a	a	k8xC	a
o	o	k7c6	o
červnových	červnový	k2eAgFnPc6d1	červnová
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
oficiálně	oficiálně	k6eAd1	oficiálně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc4d1	nový
nástupnický	nástupnický	k2eAgInSc4d1	nástupnický
stát	stát	k1gInSc4	stát
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sdružoval	sdružovat	k5eAaImAgInS	sdružovat
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
národy	národ	k1gInPc4	národ
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
rozdělení	rozdělení	k1gNnSc3	rozdělení
obou	dva	k4xCgInPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
první	první	k4xOgFnPc4	první
demokratické	demokratický	k2eAgFnPc4d1	demokratická
volby	volba	k1gFnPc4	volba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
nekomunistické	komunistický	k2eNgFnSc3d1	nekomunistická
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
mohli	moct	k5eAaImAgMnP	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
mezi	mezi	k7c7	mezi
několika	několik	k4yIc7	několik
desítkami	desítka	k1gFnPc7	desítka
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
soutěžily	soutěžit	k5eAaImAgFnP	soutěžit
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sdružovalo	sdružovat	k5eAaImAgNnS	sdružovat
mnoho	mnoho	k4c1	mnoho
demokratických	demokratický	k2eAgInPc2d1	demokratický
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
výsledků	výsledek	k1gInPc2	výsledek
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
jako	jako	k8xS	jako
jasný	jasný	k2eAgMnSc1d1	jasný
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
když	když	k8xS	když
získalo	získat	k5eAaPmAgNnS	získat
51	[number]	k4	51
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
obdobně	obdobně	k6eAd1	obdobně
smýšlející	smýšlející	k2eAgMnSc1d1	smýšlející
Verejnosť	Verejnosť	k1gMnSc1	Verejnosť
proti	proti	k7c3	proti
násiliu	násilium	k1gNnSc3	násilium
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS	získat
13	[number]	k4	13
%	%	kIx~	%
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nedostala	dostat	k5eNaPmAgFnS	dostat
do	do	k7c2	do
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
abdikace	abdikace	k1gFnSc2	abdikace
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
předání	předání	k1gNnSc4	předání
moci	moc	k1gFnSc2	moc
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
opozičních	opoziční	k2eAgFnPc2d1	opoziční
řad	řada	k1gFnPc2	řada
neočekával	očekávat	k5eNaImAgInS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
faktu	faktum	k1gNnSc3	faktum
nebylo	být	k5eNaImAgNnS	být
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
připraveno	připravit	k5eAaPmNgNnS	připravit
zcela	zcela	k6eAd1	zcela
převzít	převzít	k5eAaPmF	převzít
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
muselo	muset	k5eAaImAgNnS	muset
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
kompromisní	kompromisní	k2eAgNnSc4d1	kompromisní
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
zcela	zcela	k6eAd1	zcela
odstavit	odstavit	k5eAaPmF	odstavit
komunistický	komunistický	k2eAgInSc4d1	komunistický
aparát	aparát	k1gInSc4	aparát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
opoziční	opoziční	k2eAgFnPc1d1	opoziční
síly	síla	k1gFnPc1	síla
neměly	mít	k5eNaImAgFnP	mít
dostatek	dostatek	k1gInSc4	dostatek
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
význačných	význačný	k2eAgFnPc2d1	význačná
pozic	pozice	k1gFnPc2	pozice
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
nebyla	být	k5eNaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zakázána	zakázat	k5eAaPmNgFnS	zakázat
jako	jako	k8xS	jako
totalitní	totalitní	k2eAgMnSc1d1	totalitní
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
označují	označovat	k5eAaImIp3nP	označovat
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
jako	jako	k8xC	jako
druh	druh	k1gInSc4	druh
politického	politický	k2eAgInSc2d1	politický
obchodu	obchod	k1gInSc2	obchod
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zákaz	zákaz	k1gInSc1	zákaz
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
nebyl	být	k5eNaImAgInS	být
prosazen	prosadit	k5eAaPmNgInS	prosadit
za	za	k7c4	za
zvolení	zvolení	k1gNnSc4	zvolení
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Soudí	soudit	k5eAaImIp3nS	soudit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákaz	zákaz	k1gInSc1	zákaz
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vydán	vydat	k5eAaPmNgInS	vydat
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
návrh	návrh	k1gInSc1	návrh
tehdy	tehdy	k6eAd1	tehdy
neprošel	projít	k5eNaPmAgInS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
upuštění	upuštění	k1gNnSc4	upuštění
od	od	k7c2	od
komunistického	komunistický	k2eAgNnSc2d1	komunistické
řízení	řízení	k1gNnSc2	řízení
státu	stát	k1gInSc2	stát
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
i	i	k8xC	i
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
aspektu	aspekt	k1gInSc6	aspekt
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
československá	československý	k2eAgFnSc1d1	Československá
společnost	společnost	k1gFnSc1	společnost
transformovat	transformovat	k5eAaBmF	transformovat
na	na	k7c4	na
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
přeměnit	přeměnit	k5eAaPmF	přeměnit
řízené	řízený	k2eAgNnSc4d1	řízené
hospodářství	hospodářství	k1gNnSc4	hospodářství
na	na	k7c4	na
tržní	tržní	k2eAgInSc4d1	tržní
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
by	by	kYmCp3nS	by
stát	stát	k1gInSc1	stát
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
minimální	minimální	k2eAgFnSc4d1	minimální
možnost	možnost	k1gFnSc4	možnost
zasahování	zasahování	k1gNnSc2	zasahování
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
transformace	transformace	k1gFnSc2	transformace
hospodářství	hospodářství	k1gNnSc2	hospodářství
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
privatizace	privatizace	k1gFnSc2	privatizace
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
převedení	převedení	k1gNnSc2	převedení
do	do	k7c2	do
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vlnách	vlna	k1gFnPc6	vlna
kupónové	kupónový	k2eAgFnSc2d1	kupónová
privatizace	privatizace	k1gFnSc2	privatizace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
přechodu	přechod	k1gInSc2	přechod
hospodářství	hospodářství	k1gNnSc2	hospodářství
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ekonom	ekonom	k1gMnSc1	ekonom
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
začala	začít	k5eAaPmAgFnS	začít
probíhat	probíhat	k5eAaImF	probíhat
vlna	vlna	k1gFnSc1	vlna
restitucí	restituce	k1gFnPc2	restituce
soukromého	soukromý	k2eAgInSc2d1	soukromý
majetku	majetek	k1gInSc2	majetek
původním	původní	k2eAgMnPc3d1	původní
majitelům	majitel	k1gMnPc3	majitel
vyvlastněným	vyvlastněný	k2eAgMnPc3d1	vyvlastněný
během	během	k7c2	během
znárodnění	znárodnění	k1gNnSc2	znárodnění
<g/>
.	.	kIx.	.
</s>
<s>
Prohlubující	prohlubující	k2eAgFnSc4d1	prohlubující
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
Slovensko	Slovensko	k1gNnSc1	Slovensko
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
dvou	dva	k4xCgInPc2	dva
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
evropských	evropský	k2eAgFnPc2d1	Evropská
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
ke	k	k7c3	k
společnému	společný	k2eAgInSc3d1	společný
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zásah	zásah	k1gInSc4	zásah
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
bylo	být	k5eAaImAgNnS	být
stíháno	stíhat	k5eAaImNgNnS	stíhat
28	[number]	k4	28
příslušníků	příslušník	k1gMnPc2	příslušník
SNB	SNB	kA	SNB
<g/>
,	,	kIx,	,
11	[number]	k4	11
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
trest	trest	k1gInSc4	trest
(	(	kIx(	(
<g/>
4	[number]	k4	4
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nepodmíněný	podmíněný	k2eNgMnSc1d1	nepodmíněný
<g/>
;	;	kIx,	;
dostal	dostat	k5eAaPmAgMnS	dostat
jej	on	k3xPp3gMnSc4	on
policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zbil	zbít	k5eAaPmAgMnS	zbít
těhotnou	těhotný	k2eAgFnSc4d1	těhotná
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Velitelé	velitel	k1gMnPc1	velitel
zásahu	zásah	k1gInSc2	zásah
dostali	dostat	k5eAaPmAgMnP	dostat
tříletý	tříletý	k2eAgInSc4d1	tříletý
podmíněný	podmíněný	k2eAgInSc4d1	podmíněný
trest	trest	k1gInSc4	trest
za	za	k7c4	za
zneužití	zneužití	k1gNnSc4	zneužití
pravomoci	pravomoc	k1gFnSc2	pravomoc
veřejného	veřejný	k2eAgInSc2d1	veřejný
činitele	činitel	k1gInSc2	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
objasnění	objasnění	k1gNnSc4	objasnění
událostí	událost	k1gFnPc2	událost
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
Zásah	zásah	k1gInSc1	zásah
proti	proti	k7c3	proti
manifestujícím	manifestující	k2eAgFnPc3d1	manifestující
nebyl	být	k5eNaImAgInS	být
předem	předem	k6eAd1	předem
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
nebyl	být	k5eNaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
vliv	vliv	k1gInSc1	vliv
KGB	KGB	kA	KGB
na	na	k7c6	na
událostech	událost	k1gFnPc6	událost
17	[number]	k4	17
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
zásah	zásah	k1gInSc1	zásah
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
neměl	mít	k5eNaImAgInS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
politického	politický	k2eAgNnSc2d1	politické
vedení	vedení	k1gNnSc2	vedení
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
kolem	kolem	k7c2	kolem
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
velká	velký	k2eAgFnSc1d1	velká
řada	řada	k1gFnSc1	řada
nezodpovězených	zodpovězený	k2eNgFnPc2d1	nezodpovězená
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
rozebíraná	rozebíraný	k2eAgFnSc1d1	rozebíraná
otázka	otázka	k1gFnSc1	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
moc	moc	k1gFnSc1	moc
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vládní	vládní	k2eAgFnSc2d1	vládní
struktury	struktura	k1gFnSc2	struktura
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
StB	StB	k1gFnSc7	StB
o	o	k7c6	o
celé	celý	k2eAgFnSc6d1	celá
události	událost	k1gFnSc6	událost
věděly	vědět	k5eAaImAgFnP	vědět
a	a	k8xC	a
jestli	jestli	k8xS	jestli
nebyla	být	k5eNaImAgFnS	být
revoluce	revoluce	k1gFnSc1	revoluce
částečně	částečně	k6eAd1	částečně
připravena	připravit	k5eAaPmNgFnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
zásah	zásah	k1gInSc4	zásah
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byl	být	k5eAaImAgMnS	být
připraven	připraven	k2eAgMnSc1d1	připraven
StB	StB	k1gMnSc1	StB
a	a	k8xC	a
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nástrojem	nástroj	k1gInSc7	nástroj
vnitropolitického	vnitropolitický	k2eAgInSc2d1	vnitropolitický
boje	boj	k1gInSc2	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládané	předpokládaný	k2eAgFnPc1d1	předpokládaná
demonstrace	demonstrace	k1gFnPc1	demonstrace
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
zásahu	zásah	k1gInSc3	zásah
mohly	moct	k5eAaImAgInP	moct
posloužit	posloužit	k5eAaPmF	posloužit
jako	jako	k9	jako
páka	páka	k1gFnSc1	páka
na	na	k7c4	na
odvolání	odvolání	k1gNnSc4	odvolání
vládnoucích	vládnoucí	k2eAgFnPc2d1	vládnoucí
špiček	špička	k1gFnPc2	špička
a	a	k8xC	a
dosazení	dosazení	k1gNnPc2	dosazení
nových	nový	k2eAgMnPc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Zásah	zásah	k1gInSc1	zásah
však	však	k9	však
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
bouři	bouře	k1gFnSc4	bouře
nevole	nevole	k1gFnSc2	nevole
<g/>
,	,	kIx,	,
přerostla	přerůst	k5eAaPmAgFnS	přerůst
do	do	k7c2	do
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nedala	dát	k5eNaPmAgFnS	dát
nikterak	nikterak	k6eAd1	nikterak
zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
ani	ani	k8xC	ani
usměrnit	usměrnit	k5eAaPmF	usměrnit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
pád	pád	k1gInSc4	pád
komunistické	komunistický	k2eAgFnSc2d1	komunistická
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
uváděné	uváděný	k2eAgInPc4d1	uváděný
body	bod	k1gInPc4	bod
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Údajná	údajný	k2eAgFnSc1d1	údajná
existence	existence	k1gFnSc1	existence
druhého	druhý	k4xOgInSc2	druhý
velitelského	velitelský	k2eAgInSc2d1	velitelský
štábu	štáb	k1gInSc2	štáb
během	během	k7c2	během
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Údajná	údajný	k2eAgFnSc1d1	údajná
přítomnost	přítomnost	k1gFnSc1	přítomnost
sovětského	sovětský	k2eAgMnSc2d1	sovětský
generála	generál	k1gMnSc2	generál
v	v	k7c6	v
řídícím	řídící	k2eAgInSc6d1	řídící
štábu	štáb	k1gInSc6	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Omdlení	omdlení	k1gNnSc1	omdlení
příslušníka	příslušník	k1gMnSc2	příslušník
StB	StB	k1gMnSc2	StB
Zifčáka	Zifčák	k1gMnSc2	Zifčák
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
interpretováno	interpretovat	k5eAaBmNgNnS	interpretovat
jako	jako	k9	jako
předstírání	předstírání	k1gNnSc1	předstírání
smrti	smrt	k1gFnSc2	smrt
studenta	student	k1gMnSc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dezinformace	dezinformace	k1gFnSc1	dezinformace
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
vyburcovat	vyburcovat	k5eAaPmF	vyburcovat
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
Nečinnost	nečinnost	k1gFnSc1	nečinnost
mocenských	mocenský	k2eAgFnPc2d1	mocenská
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
revoluci	revoluce	k1gFnSc4	revoluce
potlačit	potlačit	k5eAaPmF	potlačit
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nečinnost	nečinnost	k1gFnSc1	nečinnost
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
spekulace	spekulace	k1gFnPc4	spekulace
podpořil	podpořit	k5eAaPmAgMnS	podpořit
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
důstojník	důstojník	k1gMnSc1	důstojník
StB	StB	k1gMnSc1	StB
Zifčák	Zifčák	k1gMnSc1	Zifčák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
například	například	k6eAd1	například
Zifčák	Zifčák	k1gMnSc1	Zifčák
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
StB	StB	k1gFnSc2	StB
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
studentské	studentský	k2eAgNnSc4d1	studentské
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
"	"	kIx"	"
<g/>
Nezávislého	závislý	k2eNgNnSc2d1	nezávislé
studentského	studentský	k2eAgNnSc2d1	studentské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
demonstraci	demonstrace	k1gFnSc3	demonstrace
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
svolalo	svolat	k5eAaPmAgNnS	svolat
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
předalo	předat	k5eAaPmAgNnS	předat
do	do	k7c2	do
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Zifčáka	Zifčák	k1gMnSc2	Zifčák
financované	financovaný	k2eAgFnSc2d1	financovaná
CIA	CIA	kA	CIA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hlasu	hlas	k1gInSc2	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
úkol	úkol	k1gInSc4	úkol
průvod	průvod	k1gInSc4	průvod
navést	navést	k5eAaPmF	navést
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
sehrát	sehrát	k5eAaPmF	sehrát
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
studenta	student	k1gMnSc4	student
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
údajně	údajně	k6eAd1	údajně
přiznal	přiznat	k5eAaPmAgMnS	přiznat
také	také	k9	také
Zifčákův	Zifčákův	k2eAgMnSc1d1	Zifčákův
nadřízený	nadřízený	k1gMnSc1	nadřízený
major	major	k1gMnSc1	major
Petr	Petr	k1gMnSc1	Petr
Žák	Žák	k1gMnSc1	Žák
<g/>
.	.	kIx.	.
</s>
<s>
Šiření	Šiření	k1gNnSc1	Šiření
zprávy	zpráva	k1gFnSc2	zpráva
měla	mít	k5eAaImAgFnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Drážská	Drážský	k2eAgFnSc1d1	Drážský
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
členem	člen	k1gInSc7	člen
Zifčákovy	Zifčákův	k2eAgFnSc2d1	Zifčákova
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
prokuratuře	prokuratura	k1gFnSc6	prokuratura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
ovšem	ovšem	k9	ovšem
Zifčák	Zifčák	k1gInSc1	Zifčák
původně	původně	k6eAd1	původně
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Dražská	Dražský	k2eAgFnSc1d1	Dražská
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
studenta	student	k1gMnSc2	student
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
<g/>
,	,	kIx,	,
a	a	k8xC	a
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
spolupracovnicí	spolupracovnice	k1gFnSc7	spolupracovnice
StB	StB	k1gFnPc2	StB
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
verze	verze	k1gFnPc1	verze
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
organizováným	organizováný	k2eAgNnSc7d1	organizováný
předáním	předání	k1gNnSc7	předání
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
vlastně	vlastně	k9	vlastně
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
zásadnější	zásadní	k2eAgFnSc3d2	zásadnější
mocenské	mocenský	k2eAgFnSc3d1	mocenská
změně	změna	k1gFnSc3	změna
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
předmětem	předmět	k1gInSc7	předmět
spekulací	spekulace	k1gFnPc2	spekulace
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
míry	míra	k1gFnSc2	míra
bylo	být	k5eAaImAgNnS	být
revolucí	revoluce	k1gFnSc7	revoluce
přeměněno	přeměněn	k2eAgNnSc4d1	přeměněno
personální	personální	k2eAgNnSc4d1	personální
obsazení	obsazení	k1gNnSc4	obsazení
postů	post	k1gInPc2	post
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
vládnutí	vládnutí	k1gNnSc4	vládnutí
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
či	či	k8xC	či
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
častokrát	častokrát	k6eAd1	častokrát
měli	mít	k5eAaImAgMnP	mít
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
agenti	agent	k1gMnPc1	agent
a	a	k8xC	a
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
StB	StB	k1gFnSc2	StB
(	(	kIx(	(
<g/>
např.	např.	kA	např.
současný	současný	k2eAgMnSc1d1	současný
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
-	-	kIx~	-
premiér	premiér	k1gMnSc1	premiér
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
2009	[number]	k4	2009
-	-	kIx~	-
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1980	[number]	k4	1980
-	-	kIx~	-
1989	[number]	k4	1989
člen	člen	k1gInSc1	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Sacher	Sachra	k1gFnPc2	Sachra
-	-	kIx~	-
první	první	k4xOgMnSc1	první
polistopadový	polistopadový	k2eAgMnSc1d1	polistopadový
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
byl	být	k5eAaImAgMnS	být
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Státní	státní	k2eAgFnSc2d1	státní
Bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
umožnil	umožnit	k5eAaPmAgInS	umožnit
skartovat	skartovat	k5eAaBmF	skartovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
dokumentů	dokument	k1gInPc2	dokument
StB	StB	k1gFnSc2	StB
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
Tříska	Tříska	k1gMnSc1	Tříska
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
kupónové	kupónový	k2eAgFnSc2d1	kupónová
privatizace	privatizace	k1gFnSc2	privatizace
a	a	k8xC	a
agent	agent	k1gMnSc1	agent
StB	StB	k1gMnSc1	StB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chartista	chartista	k1gMnSc1	chartista
Václav	Václav	k1gMnSc1	Václav
Žák	Žák	k1gMnSc1	Žák
v	v	k7c6	v
článku	článek	k1gInSc6	článek
pro	pro	k7c4	pro
Britské	britský	k2eAgInPc4d1	britský
listy	list	k1gInPc4	list
označil	označit	k5eAaPmAgMnS	označit
revoluci	revoluce	k1gFnSc3	revoluce
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
sametový	sametový	k2eAgInSc1d1	sametový
podvod	podvod	k1gInSc1	podvod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
