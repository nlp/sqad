<s>
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
nežná	žný	k2eNgFnSc1d1	žný
revolúcia	revolúcia	k1gFnSc1	revolúcia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
období	období	k1gNnSc2	období
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
mezi	mezi	k7c7	mezi
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
přeměně	přeměna	k1gFnSc6	přeměna
politického	politický	k2eAgNnSc2d1	politické
zřízení	zřízení	k1gNnSc2	zřízení
na	na	k7c4	na
pluralitní	pluralitní	k2eAgFnSc4d1	pluralitní
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
