<s>
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redona	k1gFnPc2	Redona
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Bertrand-Jean	Bertrand-Jean	k1gMnSc1	Bertrand-Jean
Redon	Redon	k1gMnSc1	Redon
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1840	[number]	k4	1840
Bordeaux	Bordeaux	k1gNnSc4	Bordeaux
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
symbolismu	symbolismus	k1gInSc2	symbolismus
a	a	k8xC	a
dekadence	dekadence	k1gFnSc2	dekadence
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
