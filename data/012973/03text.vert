<p>
<s>
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redona	k1gFnPc2	Redona
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Bertrand-Jean	Bertrand-Jean	k1gMnSc1	Bertrand-Jean
Redon	Redon	k1gMnSc1	Redon
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1840	[number]	k4	1840
Bordeaux	Bordeaux	k1gNnSc4	Bordeaux
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
symbolismu	symbolismus	k1gInSc2	symbolismus
a	a	k8xC	a
dekadence	dekadence	k1gFnSc2	dekadence
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Bertrandovi	Bertrand	k1gMnSc3	Bertrand
a	a	k8xC	a
Marii-Odile	Marii-Odil	k1gMnSc5	Marii-Odil
Redonovým	Redonův	k2eAgFnPc3d1	Redonův
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1840	[number]	k4	1840
v	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
vrátili	vrátit	k5eAaPmAgMnP	vrátit
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
otec	otec	k1gMnSc1	otec
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
úspěchem	úspěch	k1gInSc7	úspěch
farmařil	farmařit	k5eAaImAgInS	farmařit
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Redonovou	Redonový	k2eAgFnSc7d1	Redonový
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
Francouzkou	Francouzka	k1gFnSc7	Francouzka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Louisiany	Louisiana	k1gFnSc2	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
jméno	jméno	k1gNnSc4	jméno
Redon	Redona	k1gFnPc2	Redona
použil	použít	k5eAaPmAgInS	použít
jako	jako	k9	jako
pseudonym	pseudonym	k1gInSc1	pseudonym
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
slabé	slabý	k2eAgFnSc3d1	slabá
tělesné	tělesný	k2eAgFnSc3d1	tělesná
konstituci	konstituce	k1gFnSc3	konstituce
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
po	po	k7c4	po
narození	narození	k1gNnSc4	narození
svěřen	svěřit	k5eAaPmNgMnS	svěřit
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
kojné	kojná	k1gFnSc2	kojná
a	a	k8xC	a
následně	následně	k6eAd1	následně
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
rodinném	rodinný	k2eAgInSc6d1	rodinný
statku	statek	k1gInSc6	statek
Peyrelebade	Peyrelebad	k1gInSc5	Peyrelebad
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Listrac-de-Durè	Listrace-Durè	k1gFnSc2	Listrac-de-Durè
<g/>
,	,	kIx,	,
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Bordeaux	Bordeaux	k1gNnSc2	Bordeaux
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
izolaci	izolace	k1gFnSc6	izolace
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
probouzel	probouzet	k5eAaImAgInS	probouzet
bizarní	bizarní	k2eAgInSc1d1	bizarní
a	a	k8xC	a
fantasmagorické	fantasmagorický	k2eAgInPc1d1	fantasmagorický
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
později	pozdě	k6eAd2	pozdě
podstatou	podstata	k1gFnSc7	podstata
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nepodařilo	podařit	k5eNaPmAgNnS	podařit
plně	plně	k6eAd1	plně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
letech	léto	k1gNnPc6	léto
strávil	strávit	k5eAaPmAgMnS	strávit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
pěstounkou	pěstounka	k1gFnSc7	pěstounka
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
zdejší	zdejší	k2eAgFnPc4d1	zdejší
muzea	muzeum	k1gNnPc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
dramatické	dramatický	k2eAgInPc1d1	dramatický
obrazy	obraz	k1gInPc1	obraz
jej	on	k3xPp3gMnSc4	on
silně	silně	k6eAd1	silně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
žil	žít	k5eAaImAgMnS	žít
až	až	k6eAd1	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kresba	kresba	k1gFnSc1	kresba
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
začal	začít	k5eAaPmAgMnS	začít
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
kurzy	kurz	k1gInPc4	kurz
malíře	malíř	k1gMnSc2	malíř
Stanislase	Stanislasa	k1gFnSc3	Stanislasa
Gorina	Gorin	k2eAgFnSc1d1	Gorin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
Redonovi	Redon	k1gMnSc6	Redon
podporoval	podporovat	k5eAaImAgInS	podporovat
jeho	jeho	k3xOp3gNnSc4	jeho
zanícení	zanícení	k1gNnSc4	zanícení
pro	pro	k7c4	pro
malířství	malířství	k1gNnSc4	malířství
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
ho	on	k3xPp3gNnSc4	on
s	s	k7c7	s
díly	díl	k1gInPc7	díl
Jeana-Francoise	Jeana-Francoise	k1gFnSc2	Jeana-Francoise
Milleta	Milleta	k1gFnSc1	Milleta
<g/>
,	,	kIx,	,
Eugè	Eugè	k1gFnSc1	Eugè
Delacroixe	Delacroixe	k1gFnSc1	Delacroixe
<g/>
,	,	kIx,	,
Camille	Camill	k1gMnSc2	Camill
Corota	Corot	k1gMnSc2	Corot
a	a	k8xC	a
Gustava	Gustav	k1gMnSc2	Gustav
Moreaua	Moreauus	k1gMnSc2	Moreauus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
ze	z	k7c2	z
jmenovaných	jmenovaný	k2eAgMnPc2d1	jmenovaný
malířů	malíř	k1gMnPc2	malíř
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	let	k1gInPc6	let
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc1	jejichž
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
architektem	architekt	k1gMnSc7	architekt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
École	Écola	k1gFnSc6	Écola
des	des	k1gNnSc2	des
Beaux-Arts	Beaux-Artsa	k1gFnPc2	Beaux-Artsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
téže	týž	k3xTgFnSc6	týž
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
ateliéru	ateliér	k1gInSc2	ateliér
malíře	malíř	k1gMnSc2	malíř
Léona	Léon	k1gMnSc2	Léon
Gérôma	Gérôm	k1gMnSc2	Gérôm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
skončilo	skončit	k5eAaPmAgNnS	skončit
studium	studium	k1gNnSc4	studium
rozchodem	rozchod	k1gInSc7	rozchod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Redon	Redon	k1gMnSc1	Redon
nechtěl	chtít	k5eNaImAgMnS	chtít
nechat	nechat	k5eAaPmF	nechat
vtěsnat	vtěsnat	k5eAaPmF	vtěsnat
do	do	k7c2	do
schémat	schéma	k1gNnPc2	schéma
akademického	akademický	k2eAgNnSc2d1	akademické
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Bordeaux	Bordeaux	k1gNnSc2	Bordeaux
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
grafikem	grafik	k1gMnSc7	grafik
Rodolphem	Rodolph	k1gInSc7	Rodolph
Bresdinem	Bresdin	k1gMnSc7	Bresdin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
naučil	naučit	k5eAaPmAgMnS	naučit
grafickým	grafický	k2eAgFnPc3d1	grafická
technikám	technika	k1gFnPc3	technika
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
leptu	lept	k1gInSc2	lept
a	a	k8xC	a
litografie	litografie	k1gFnSc2	litografie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
tohoto	tento	k3xDgMnSc2	tento
umělce	umělec	k1gMnSc2	umělec
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
série	série	k1gFnSc1	série
11	[number]	k4	11
leptů	lept	k1gInPc2	lept
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
r.	r.	kA	r.
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
orientálním	orientální	k2eAgNnSc7d1	orientální
a	a	k8xC	a
romantickým	romantický	k2eAgNnSc7d1	romantické
duchem	duch	k1gMnSc7	duch
děl	dělo	k1gNnPc2	dělo
Eugéna	Eugén	k1gInSc2	Eugén
Delacroixe	Delacroixe	k1gFnSc2	Delacroixe
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
znal	znát	k5eAaImAgMnS	znát
od	od	k7c2	od
vidění	vidění	k1gNnSc2	vidění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
olej	olej	k1gInSc4	olej
Roland	Rolanda	k1gFnPc2	Rolanda
v	v	k7c4	v
Roncevaux	Roncevaux	k1gInSc4	Roncevaux
přijat	přijmout	k5eAaPmNgMnS	přijmout
na	na	k7c4	na
výstavu	výstava	k1gFnSc4	výstava
Salón	salón	k1gInSc4	salón
v	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
obdobou	obdoba	k1gFnSc7	obdoba
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
obraz	obraz	k1gInSc1	obraz
nevystavit	vystavit	k5eNaPmF	vystavit
a	a	k8xC	a
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
novinách	novina	k1gFnPc6	novina
La	la	k1gNnSc2	la
Gironde	Girond	k1gInSc5	Girond
publikoval	publikovat	k5eAaBmAgMnS	publikovat
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
formuloval	formulovat	k5eAaImAgMnS	formulovat
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
soudobému	soudobý	k2eAgNnSc3d1	soudobé
malířství	malířství	k1gNnSc3	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
maloval	malovat	k5eAaImAgMnS	malovat
v	v	k7c6	v
Barbizonu	Barbizon	k1gInSc6	Barbizon
s	s	k7c7	s
malíři	malíř	k1gMnPc7	malíř
Barbizonské	Barbizonský	k2eAgFnSc2d1	Barbizonská
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
Corotem	Corot	k1gInSc7	Corot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Prusko-francouzskou	pruskorancouzský	k2eAgFnSc7d1	prusko-francouzská
válkou	válka	k1gFnSc7	válka
mobilizován	mobilizovat	k5eAaBmNgMnS	mobilizovat
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
válečných	válečný	k2eAgFnPc2d1	válečná
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Loiře	Loira	k1gFnSc6	Loira
poblíž	poblíž	k6eAd1	poblíž
Tours	Toursa	k1gFnPc2	Toursa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
traumatizující	traumatizující	k2eAgInSc1d1	traumatizující
zážitek	zážitek	k1gInSc1	zážitek
se	se	k3xPyFc4	se
promítl	promítnout	k5eAaPmAgInS	promítnout
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc4	jeho
domovem	domov	k1gInSc7	domov
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
ateliér	ateliér	k1gInSc1	ateliér
na	na	k7c4	na
Montparnassu	Montparnassa	k1gFnSc4	Montparnassa
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgNnSc4	každý
léto	léto	k1gNnSc4	léto
jezdil	jezdit	k5eAaImAgInS	jezdit
do	do	k7c2	do
Peyrelebade	Peyrelebad	k1gInSc5	Peyrelebad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Jules	Jules	k1gMnSc1	Jules
Boissé	Boissý	k2eAgFnSc2d1	Boissý
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
literárního	literární	k2eAgInSc2d1	literární
salónu	salón	k1gInSc2	salón
Berthe	Berth	k1gFnSc2	Berth
de	de	k?	de
Rayssac	Rayssac	k1gInSc1	Rayssac
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
malíři	malíř	k1gMnPc1	malíř
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k6eAd1	také
poznal	poznat	k5eAaPmAgMnS	poznat
svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
kreolku	kreolka	k1gFnSc4	kreolka
Camille	Camill	k1gMnSc5	Camill
Falte	Falt	k1gMnSc5	Falt
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1875	[number]	k4	1875
opět	opět	k6eAd1	opět
maloval	malovat	k5eAaImAgMnS	malovat
v	v	k7c6	v
Barbizonu	Barbizon	k1gInSc6	Barbizon
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
hlavně	hlavně	k9	hlavně
díla	dílo	k1gNnSc2	dílo
Franse	Frans	k1gMnSc2	Frans
Halse	Hals	k1gMnSc2	Hals
a	a	k8xC	a
Rembrandta	Rembrandt	k1gMnSc2	Rembrandt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
litografií	litografie	k1gFnPc2	litografie
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ve	v	k7c6	v
snu	sen	k1gInSc6	sen
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
snové	snový	k2eAgInPc4d1	snový
<g/>
,	,	kIx,	,
imaginární	imaginární	k2eAgInPc4d1	imaginární
prožitky	prožitek	k1gInPc4	prožitek
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
v	v	k7c6	v
redakčních	redakční	k2eAgFnPc6d1	redakční
místnostech	místnost	k1gFnPc6	místnost
novin	novina	k1gFnPc2	novina
La	la	k1gNnSc1	la
Vie	Vie	k1gMnSc1	Vie
Moderne	Modern	k1gInSc5	Modern
první	první	k4xOgFnSc4	první
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výstavu	výstava	k1gFnSc4	výstava
svých	svůj	k3xOyFgFnPc2	svůj
uhlokreseb	uhlokresba	k1gFnPc2	uhlokresba
<g/>
,	,	kIx,	,
ohlas	ohlas	k1gInSc1	ohlas
byl	být	k5eAaImAgInS	být
mizivý	mizivý	k2eAgInSc1d1	mizivý
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
uhlokresby	uhlokresba	k1gFnSc2	uhlokresba
podruhé	podruhé	k6eAd1	podruhé
a	a	k8xC	a
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Huysmansem	Huysmans	k1gMnSc7	Huysmans
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
Stéphanem	Stéphan	k1gInSc7	Stéphan
Mallarmém	Mallarmý	k2eAgInSc6d1	Mallarmý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
založilo	založit	k5eAaPmAgNnS	založit
asi	asi	k9	asi
400	[number]	k4	400
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
nebyla	být	k5eNaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
díla	dílo	k1gNnPc1	dílo
na	na	k7c4	na
každoroční	každoroční	k2eAgFnSc4d1	každoroční
oficiální	oficiální	k2eAgFnSc4d1	oficiální
výstavu	výstava	k1gFnSc4	výstava
Salón	salón	k1gInSc1	salón
<g/>
,	,	kIx,	,
Salón	salón	k1gInSc1	salón
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Redon	Redon	k1gInSc1	Redon
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
jeho	jeho	k3xOp3gInSc1	jeho
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
Redon	Redon	k1gInSc1	Redon
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Gauguinem	Gauguin	k1gMnSc7	Gauguin
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
na	na	k7c6	na
salónu	salón	k1gInSc6	salón
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
Skupiny	skupina	k1gFnSc2	skupina
XX	XX	kA	XX
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
,	,	kIx,	,
osmé	osmý	k4xOgFnSc2	osmý
výstavy	výstava	k1gFnSc2	výstava
impresionistů	impresionista	k1gMnPc2	impresionista
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
Mallarméova	Mallarméův	k2eAgInSc2d1	Mallarméův
podnětu	podnět	k1gInSc2	podnět
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
první	první	k4xOgFnPc4	první
výstavy	výstava	k1gFnPc4	výstava
Společnosti	společnost	k1gFnSc2	společnost
malířů-grafiků	malířůrafik	k1gMnPc2	malířů-grafik
v	v	k7c6	v
prestižní	prestižní	k2eAgFnSc6d1	prestižní
galerii	galerie	k1gFnSc6	galerie
Paula	Paul	k1gMnSc2	Paul
Durand-Ruela	Durand-Ruel	k1gMnSc2	Durand-Ruel
a	a	k8xC	a
sblížil	sblížit	k5eAaPmAgMnS	sblížit
se	se	k3xPyFc4	se
s	s	k7c7	s
malíři	malíř	k1gMnPc7	malíř
nazývajícími	nazývající	k2eAgMnPc7d1	nazývající
se	se	k3xPyFc4	se
Nabisté	Nabistý	k2eAgFnPc1d1	Nabistý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
galerii	galerie	k1gFnSc6	galerie
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
proměnila	proměnit	k5eAaPmAgFnS	proměnit
jeho	jeho	k3xOp3gFnSc1	jeho
barevná	barevný	k2eAgFnSc1d1	barevná
paleta	paleta	k1gFnSc1	paleta
<g/>
.	.	kIx.	.
</s>
<s>
Opouští	opouštět	k5eAaImIp3nP	opouštět
tóny	tón	k1gInPc1	tón
černé	černý	k2eAgInPc1d1	černý
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
pastel	pastel	k1gInSc4	pastel
a	a	k8xC	a
olejové	olejový	k2eAgFnPc4d1	olejová
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
dominuje	dominovat	k5eAaImIp3nS	dominovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dílech	díl	k1gInPc6	díl
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
opět	opět	k6eAd1	opět
cestoval	cestovat	k5eAaImAgMnS	cestovat
vystavovat	vystavovat	k5eAaImF	vystavovat
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Mallarméově	Mallarméův	k2eAgFnSc6d1	Mallarméův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sblížil	sblížit	k5eAaPmAgMnS	sblížit
s	s	k7c7	s
Gauguinem	Gauguin	k1gInSc7	Gauguin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
navštívil	navštívit	k5eAaPmAgInS	navštívit
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Prodělal	prodělat	k5eAaPmAgMnS	prodělat
také	také	k9	také
vážnou	vážný	k2eAgFnSc4d1	vážná
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
další	další	k2eAgFnSc4d1	další
tvorbu	tvorba	k1gFnSc4	tvorba
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgInS	nést
vynucený	vynucený	k2eAgInSc1d1	vynucený
prodej	prodej	k1gInSc1	prodej
statku	statek	k1gInSc2	statek
Peyrelebade	Peyrelebad	k1gInSc5	Peyrelebad
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgNnSc6	jenž
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
zde	zde	k6eAd1	zde
prožil	prožít	k5eAaPmAgMnS	prožít
poslední	poslední	k2eAgNnSc4d1	poslední
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
u	u	k7c2	u
Paula	Paul	k1gMnSc2	Paul
Durand-Ruela	Durand-Ruel	k1gMnSc2	Durand-Ruel
potřetí	potřetí	k4xO	potřetí
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
Nabisty	Nabista	k1gMnPc7	Nabista
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
mecenáš	mecenáš	k1gMnSc1	mecenáš
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Domecy	Domeca	k1gFnSc2	Domeca
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
došel	dojít	k5eAaPmAgInS	dojít
uznání	uznání	k1gNnSc4	uznání
i	i	k8xC	i
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
Řád	řád	k1gInSc1	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
galerii	galerie	k1gFnSc4	galerie
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Zavřené	zavřený	k2eAgNnSc4d1	zavřené
oči	oko	k1gNnPc1	oko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
se	s	k7c7	s
zakládáním	zakládání	k1gNnSc7	zakládání
Podzimního	podzimní	k2eAgInSc2d1	podzimní
salonu	salon	k1gInSc2	salon
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
vyhrazen	vyhrazen	k2eAgInSc4d1	vyhrazen
celý	celý	k2eAgInSc4d1	celý
jeden	jeden	k4xCgInSc4	jeden
sál	sál	k1gInSc4	sál
s	s	k7c7	s
62	[number]	k4	62
díly	dílo	k1gNnPc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
stěhování	stěhování	k1gNnSc1	stěhování
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Redon	Redon	k1gMnSc1	Redon
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
avenue	avenue	k1gFnSc6	avenue
de	de	k?	de
Wagram	Wagram	k1gInSc1	Wagram
č.	č.	k?	č.
129	[number]	k4	129
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
opatřit	opatřit	k5eAaPmF	opatřit
si	se	k3xPyFc3	se
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
zpětný	zpětný	k2eAgInSc4d1	zpětný
odkup	odkup	k1gInSc4	odkup
Peyrelebade	Peyrelebad	k1gInSc5	Peyrelebad
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
aukci	aukce	k1gFnSc4	aukce
53	[number]	k4	53
svých	svůj	k3xOyFgFnPc2	svůj
olejomaleb	olejomalba	k1gFnPc2	olejomalba
<g/>
,	,	kIx,	,
akvarelů	akvarel	k1gInPc2	akvarel
<g/>
,	,	kIx,	,
pastelů	pastel	k1gInPc2	pastel
a	a	k8xC	a
kreseb	kresba	k1gFnPc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Potřebný	potřebný	k2eAgInSc1d1	potřebný
obnos	obnos	k1gInSc1	obnos
však	však	k9	však
nezískal	získat	k5eNaPmAgInS	získat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k1gNnSc7	další
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
mecenášů	mecenáš	k1gMnPc2	mecenáš
<g/>
,	,	kIx,	,
Arthurem	Arthur	k1gMnSc7	Arthur
Fontainem	Fontain	k1gMnSc7	Fontain
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
Benátky	Benátky	k1gFnPc4	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
trávil	trávit	k5eAaImAgInS	trávit
léto	léto	k1gNnSc4	léto
v	v	k7c6	v
Biè	Biè	k1gFnSc6	Biè
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
původně	původně	k6eAd1	původně
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Juliette	Juliett	k1gInSc5	Juliett
Dodou	Dodá	k1gFnSc4	Dodá
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
sestra	sestra	k1gFnSc1	sestra
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
došlo	dojít	k5eAaPmAgNnS	dojít
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
uznání	uznání	k1gNnSc2	uznání
i	i	k9	i
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
proslulé	proslulý	k2eAgFnSc6d1	proslulá
výstavě	výstava	k1gFnSc6	výstava
Armory	Armora	k1gFnSc2	Armora
Show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
komplexní	komplexní	k2eAgFnSc7d1	komplexní
výstavou	výstava	k1gFnSc7	výstava
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
i	i	k9	i
malíři	malíř	k1gMnPc1	malíř
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
představil	představit	k5eAaPmAgMnS	představit
Redon	Redon	k1gMnSc1	Redon
40	[number]	k4	40
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Holandsko	Holandsko	k1gNnSc4	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Biè	Biè	k1gFnSc6	Biè
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redonovi	Redonův	k2eAgMnPc1d1	Redonův
měli	mít	k5eAaImAgMnP	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
již	již	k6eAd1	již
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
léta	léto	k1gNnPc4	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Ari	Ari	k1gMnSc1	Ari
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
několik	několik	k4yIc4	několik
jeho	jeho	k3xOp3gInPc2	jeho
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Ari	Ari	k?	Ari
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
odveden	odveden	k2eAgInSc1d1	odveden
k	k	k7c3	k
ženistům	ženista	k1gMnPc3	ženista
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
syna	syn	k1gMnSc4	syn
bytostně	bytostně	k6eAd1	bytostně
strachoval	strachovat	k5eAaImAgMnS	strachovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc2	jeho
návratu	návrat	k1gInSc2	návrat
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Ari	Ari	k?	Ari
Redon	Redon	k1gMnSc1	Redon
pečoval	pečovat	k5eAaImAgMnS	pečovat
o	o	k7c4	o
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
a	a	k8xC	a
literární	literární	k2eAgInSc4d1	literární
odkaz	odkaz	k1gInSc4	odkaz
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
odkázal	odkázat	k5eAaPmAgInS	odkázat
malířova	malířův	k2eAgNnPc4d1	malířovo
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
zůstala	zůstat	k5eAaPmAgNnP	zůstat
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Redon	Redon	k1gNnSc1	Redon
a	a	k8xC	a
Čechy	Čechy	k1gFnPc1	Čechy
==	==	k?	==
</s>
</p>
<p>
<s>
Zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
kontaktech	kontakt	k1gInPc6	kontakt
Redona	Redona	k1gFnSc1	Redona
s	s	k7c7	s
českým	český	k2eAgNnSc7d1	české
prostředím	prostředí	k1gNnSc7	prostředí
má	mít	k5eAaImIp3nS	mít
Zdenka	Zdenka	k1gFnSc1	Zdenka
Braunerová	Braunerová	k1gFnSc1	Braunerová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
sestra	sestra	k1gFnSc1	sestra
byla	být	k5eAaImAgFnS	být
provdána	provdat	k5eAaPmNgFnS	provdat
za	za	k7c2	za
Élémira	Élémiro	k1gNnSc2	Élémiro
Bourgese	Bourgese	k1gFnSc2	Bourgese
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
Redonových	Redonův	k2eAgMnPc2d1	Redonův
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
pobytech	pobyt	k1gInPc6	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
často	často	k6eAd1	často
Redonovy	Redonův	k2eAgFnSc2d1	Redonův
navštěvovala	navštěvovat	k5eAaImAgNnP	navštěvovat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
uvedla	uvést	k5eAaPmAgFnS	uvést
i	i	k9	i
Julia	Julius	k1gMnSc4	Julius
Zeyera	Zeyer	k1gMnSc4	Zeyer
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc2	Vilém
Mrštíka	Mrštík	k1gMnSc2	Mrštík
a	a	k8xC	a
Miloše	Miloš	k1gMnSc2	Miloš
Martena	Marten	k2eAgFnSc1d1	Martena
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
sblížil	sblížit	k5eAaPmAgMnS	sblížit
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Arnoštu	Arnošt	k1gMnSc6	Arnošt
Procházkovi	Procházka	k1gMnSc6	Procházka
byl	být	k5eAaImAgMnS	být
Marten	Marten	k2eAgMnSc1d1	Marten
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
o	o	k7c6	o
Redonovi	Redon	k1gMnSc3	Redon
psal	psát	k5eAaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
monografie	monografie	k1gFnSc2	monografie
soubor	soubor	k1gInSc1	soubor
dvanácti	dvanáct	k4xCc2	dvanáct
reprodukcí	reprodukce	k1gFnPc2	reprodukce
Redonových	Redonův	k2eAgFnPc2d1	Redonův
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sbírkách	sbírka	k1gFnPc6	sbírka
album	album	k1gNnSc1	album
šesti	šest	k4xCc2	šest
litografií	litografie	k1gFnPc2	litografie
Edgaru	Edgar	k1gMnSc6	Edgar
Poeovi	Poea	k1gMnSc6	Poea
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
litografie	litografie	k1gFnPc4	litografie
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Památník	památník	k1gInSc1	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
dvě	dva	k4xCgFnPc4	dva
litografie	litografie	k1gFnPc4	litografie
a	a	k8xC	a
lept	lept	k1gInSc4	lept
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Redonovo	Redonův	k2eAgNnSc1d1	Redonův
dílo	dílo	k1gNnSc1	dílo
nelze	lze	k6eNd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
žádného	žádný	k3yNgInSc2	žádný
z	z	k7c2	z
proudů	proud	k1gInPc2	proud
soudobého	soudobý	k2eAgNnSc2d1	soudobé
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc1	hnutí
nabistů	nabista	k1gMnPc2	nabista
<g/>
,	,	kIx,	,
barbizonská	barbizonský	k2eAgFnSc1d1	barbizonský
škola	škola	k1gFnSc1	škola
nebo	nebo	k8xC	nebo
impresionismus	impresionismus	k1gInSc1	impresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
solitér	solitér	k1gInSc1	solitér
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
nějaké	nějaký	k3yIgFnSc2	nějaký
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
malíři	malíř	k1gMnPc7	malíř
z	z	k7c2	z
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1	vyjmenovaný
směrů	směr	k1gInPc2	směr
však	však	k9	však
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
a	a	k8xC	a
pojily	pojit	k5eAaImAgFnP	pojit
ho	on	k3xPp3gInSc4	on
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
přátelské	přátelský	k2eAgFnSc2d1	přátelská
vazby	vazba	k1gFnPc1	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redon	Redon	k1gInSc1	Redon
byl	být	k5eAaImAgInS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
a	a	k8xC	a
sečtělý	sečtělý	k2eAgMnSc1d1	sečtělý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
nejen	nejen	k6eAd1	nejen
vlastní	vlastní	k2eAgFnSc1d1	vlastní
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
přírodověda	přírodověda	k1gFnSc1	přírodověda
a	a	k8xC	a
zejména	zejména	k9	zejména
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
byl	být	k5eAaImAgMnS	být
vášnivě	vášnivě	k6eAd1	vášnivě
oddán	oddat	k5eAaPmNgMnS	oddat
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
inspirativní	inspirativní	k2eAgFnSc4d1	inspirativní
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hrál	hrát	k5eAaImAgInS	hrát
sen	sen	k1gInSc1	sen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Redon	Redon	k1gInSc1	Redon
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
kontaktu	kontakt	k1gInSc6	kontakt
se	s	k7c7	s
symbolistickými	symbolistický	k2eAgMnPc7d1	symbolistický
básníky	básník	k1gMnPc7	básník
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnPc3	jejichž
básním	báseň	k1gFnPc3	báseň
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gNnSc3	jeho
vidění	vidění	k1gNnSc3	vidění
světa	svět	k1gInSc2	svět
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
nazýván	nazývat	k5eAaImNgInS	nazývat
prvním	první	k4xOgMnSc6	první
moderním	moderní	k2eAgInPc3d1	moderní
malířem	malíř	k1gMnSc7	malíř
pocitů	pocit	k1gInPc2	pocit
úzkosti	úzkost	k1gFnSc2	úzkost
a	a	k8xC	a
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc1	námět
čerpal	čerpat	k5eAaImAgMnS	čerpat
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc2	křesťanství
i	i	k8xC	i
středověkých	středověký	k2eAgMnPc2d1	středověký
bestiářů	bestiář	k1gInPc2	bestiář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
literární	literární	k2eAgFnPc1d1	literární
lásky	láska	k1gFnPc1	láska
lze	lze	k6eAd1	lze
vyčíst	vyčíst	k5eAaPmF	vyčíst
z	z	k7c2	z
názvů	název	k1gInPc2	název
deseti	deset	k4xCc2	deset
litografických	litografický	k2eAgFnPc2d1	litografická
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
více	hodně	k6eAd2	hodně
technik	technik	k1gMnSc1	technik
<g/>
:	:	kIx,	:
vedle	vedle	k7c2	vedle
oleje	olej	k1gInSc2	olej
i	i	k8xC	i
kresby	kresba	k1gFnSc2	kresba
uhlem	uhel	k1gInSc7	uhel
<g/>
,	,	kIx,	,
kvaš	kvaš	k1gFnSc4	kvaš
<g/>
,	,	kIx,	,
pastel	pastel	k1gInSc4	pastel
<g/>
,	,	kIx,	,
temperu	tempera	k1gFnSc4	tempera
a	a	k8xC	a
litografii	litografie	k1gFnSc4	litografie
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jsou	být	k5eAaImIp3nP	být
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
,	,	kIx,	,
datoval	datovat	k5eAaImAgInS	datovat
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
zjistit	zjistit	k5eAaPmF	zjistit
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
lze	lze	k6eAd1	lze
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
rozlišit	rozlišit	k5eAaPmF	rozlišit
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
etapy	etapa	k1gFnPc4	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
uhlokresbách	uhlokresba	k1gFnPc6	uhlokresba
a	a	k8xC	a
grafikách	grafika	k1gFnPc6	grafika
dominovala	dominovat	k5eAaImAgFnS	dominovat
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Redon	Redon	k1gMnSc1	Redon
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
nazývají	nazývat	k5eAaImIp3nP	nazývat
noirs	noirs	k6eAd1	noirs
–	–	k?	–
černá	černá	k1gFnSc1	černá
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
druhého	druhý	k4xOgMnSc2	druhý
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Ariho	Ariha	k1gFnSc5	Ariha
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
svůj	svůj	k3xOyFgInSc4	svůj
způsob	způsob	k1gInSc4	způsob
tvorby	tvorba	k1gFnSc2	tvorba
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
malířskou	malířský	k2eAgFnSc7d1	malířská
technikou	technika	k1gFnSc7	technika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pastel	pastel	k1gInSc1	pastel
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
mizí	mizet	k5eAaImIp3nP	mizet
fantaskní	fantaskní	k2eAgFnPc1d1	fantaskní
bytosti	bytost	k1gFnPc1	bytost
znázorňované	znázorňovaný	k2eAgFnPc1d1	znázorňovaná
v	v	k7c4	v
noirs	noirs	k1gInSc4	noirs
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
lyričtější	lyrický	k2eAgNnSc1d2	lyričtější
<g/>
,	,	kIx,	,
Redon	Redon	k1gInSc1	Redon
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
všechny	všechen	k3xTgFnPc4	všechen
barvy	barva	k1gFnPc4	barva
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
způsob	způsob	k1gInSc1	způsob
nakládání	nakládání	k1gNnSc2	nakládání
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
světlem	světlo	k1gNnSc7	světlo
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
svébytný	svébytný	k2eAgInSc1d1	svébytný
<g/>
,	,	kIx,	,
že	že	k8xS	že
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
i	i	k9	i
fauvisty	fauvista	k1gMnPc4	fauvista
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
již	již	k9	již
k	k	k7c3	k
noirs	noirs	k6eAd1	noirs
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redon	Redon	k1gMnSc1	Redon
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dekorativní	dekorativní	k2eAgFnSc2d1	dekorativní
a	a	k8xC	a
nástěnné	nástěnný	k2eAgFnSc2d1	nástěnná
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vyzdobil	vyzdobit	k5eAaPmAgMnS	vyzdobit
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
dům	dům	k1gInSc1	dům
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Ernesta	Ernest	k1gMnSc2	Ernest
Chaussona	Chausson	k1gMnSc2	Chausson
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svého	svůj	k3xOyFgMnSc2	svůj
mecenáše	mecenáš	k1gMnSc2	mecenáš
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
Robereta	Roberet	k1gMnSc2	Roberet
de	de	k?	de
Domecy	Domeca	k1gMnSc2	Domeca
<g/>
,	,	kIx,	,
v	v	k7c6	v
Domecy-sur-Cure	Domecyur-Cur	k1gMnSc5	Domecy-sur-Cur
v	v	k7c6	v
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
malíře	malíř	k1gMnSc2	malíř
Gustava	Gustav	k1gMnSc2	Gustav
Fayeta	Fayet	k1gMnSc2	Fayet
<g/>
,	,	kIx,	,
vyzdobil	vyzdobit	k5eAaPmAgInS	vyzdobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
stěny	stěna	k1gFnSc2	stěna
knihovny	knihovna	k1gFnSc2	knihovna
opatství	opatství	k1gNnSc2	opatství
Fontfroide	Fontfroid	k1gInSc5	Fontfroid
u	u	k7c2	u
Narbonne	Narbonn	k1gInSc5	Narbonn
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
panelem	panel	k1gInSc7	panel
s	s	k7c7	s
názvem	název	k1gInSc7	název
Noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
i	i	k9	i
dekorativními	dekorativní	k2eAgFnPc7d1	dekorativní
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
kreslil	kreslit	k5eAaImAgInS	kreslit
návrhy	návrh	k1gInPc4	návrh
pro	pro	k7c4	pro
státní	státní	k2eAgFnPc4d1	státní
gobelínové	gobelínový	k2eAgFnPc4d1	gobelínová
dílny	dílna	k1gFnPc4	dílna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Odilona	Odilon	k1gMnSc2	Odilon
Redona	Redon	k1gMnSc2	Redon
bylo	být	k5eAaImAgNnS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
také	také	k9	také
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Sledoval	sledovat	k5eAaImAgInS	sledovat
tvorbu	tvorba	k1gFnSc4	tvorba
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jeho	jeho	k3xOp3gMnPc7	jeho
oblíbenými	oblíbený	k2eAgMnPc7d1	oblíbený
skladateli	skladatel	k1gMnPc7	skladatel
byli	být	k5eAaImAgMnP	být
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
Beethoven	Beethoven	k1gMnSc1	Beethoven
a	a	k8xC	a
zejména	zejména	k9	zejména
Schuman	Schuman	k1gMnSc1	Schuman
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
věnoval	věnovat	k5eAaImAgInS	věnovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
pastel	pastel	k1gInSc1	pastel
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
<g/>
.	.	kIx.	.
</s>
<s>
Schumanův	Schumanův	k2eAgInSc1d1	Schumanův
portrét	portrét	k1gInSc1	portrét
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
panelu	panel	k1gInSc6	panel
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnPc1	album
litografií	litografie	k1gFnPc2	litografie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snu	sen	k1gInSc6	sen
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
</s>
</p>
<p>
<s>
Edgaru	Edgar	k1gMnSc3	Edgar
Poeovi	Poea	k1gMnSc3	Poea
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
</s>
</p>
<p>
<s>
Pocta	pocta	k1gFnSc1	pocta
Goyovi	Goya	k1gMnSc3	Goya
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
</s>
</p>
<p>
<s>
Pokušení	pokušení	k1gNnSc1	pokušení
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
</s>
</p>
<p>
<s>
Gustavu	Gustav	k1gMnSc3	Gustav
Flaubertovi	Flaubert	k1gMnSc3	Flaubert
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
</s>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
</s>
</p>
<p>
<s>
Sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
</s>
</p>
<p>
<s>
Zjevení	zjevení	k1gNnSc1	zjevení
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
neuvedená	uvedený	k2eNgFnSc1d1	neuvedená
v	v	k7c6	v
referencích	reference	k1gFnPc6	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Bacou	Bacou	k6eAd1	Bacou
<g/>
,	,	kIx,	,
Roseline	Roselin	k1gInSc5	Roselin
<g/>
,	,	kIx,	,
Odilon	Odilon	k1gInSc4	Odilon
Redon	Redona	k1gFnPc2	Redona
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
-II	-II	k?	-II
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Cailer	Cailer	k1gInSc1	Cailer
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
CLEMENT	CLEMENT	kA	CLEMENT
<g/>
,	,	kIx,	,
Russell	Russell	k1gMnSc1	Russell
T.	T.	kA	T.
Four	Four	k1gMnSc1	Four
French	French	k1gMnSc1	French
Symbolists	Symbolistsa	k1gFnPc2	Symbolistsa
<g/>
:	:	kIx,	:
A	a	k9	a
Sourcebook	Sourcebook	k1gInSc1	Sourcebook
on	on	k3xPp3gMnSc1	on
Pierre	Pierr	k1gInSc5	Pierr
Puvis	Puvis	k1gFnSc6	Puvis
de	de	k?	de
Chavannes	Chavannesa	k1gFnPc2	Chavannesa
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Moreau	Moreaa	k1gFnSc4	Moreaa
<g/>
,	,	kIx,	,
Odilon	Odilon	k1gInSc4	Odilon
Redon	Redona	k1gFnPc2	Redona
<g/>
,	,	kIx,	,
and	and	k?	and
Maurice	Maurika	k1gFnSc3	Maurika
Denis	Denisa	k1gFnPc2	Denisa
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Greenwood	Greenwood	k1gInSc1	Greenwood
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dvanáct	dvanáct	k4xCc4	dvanáct
listů	list	k1gInPc2	list
/	/	kIx~	/
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redon	k1gInSc1	Redon
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc1d1	úvodní
studie	studie	k1gFnSc1	studie
Joris	Joris	k1gFnSc2	Joris
Karl	Karla	k1gFnPc2	Karla
Huysmans	Huysmans	k1gInSc1	Huysmans
<g/>
:	:	kIx,	:
Obluda	obluda	k1gFnSc1	obluda
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
monografie	monografie	k1gFnSc1	monografie
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
</s>
</p>
<p>
<s>
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
<g/>
,	,	kIx,	,
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redon	k1gInSc1	Redon
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Kobliha	kobliha	k1gFnSc1	kobliha
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Redon	Redon	k1gInSc1	Redon
<g/>
,	,	kIx,	,
Hollar	Hollar	k1gInSc1	Hollar
IV	IV	kA	IV
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
/	/	kIx~	/
<g/>
1928	[number]	k4	1928
</s>
</p>
<p>
<s>
Marten	Marten	k2eAgMnSc1d1	Marten
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Redon	Redon	k1gNnSc1	Redon
<g/>
,	,	kIx,	,
essai	essai	k1gNnSc1	essai
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgFnSc1d1	moderní
revue	revue	k1gFnSc1	revue
XX	XX	kA	XX
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
</s>
</p>
<p>
<s>
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Předchůdce	předchůdce	k1gMnSc1	předchůdce
imaginativního	imaginativní	k2eAgNnSc2d1	imaginativní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
XVI	XVI	kA	XVI
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
</s>
</p>
<p>
<s>
Piel	Piel	k1gMnSc1	Piel
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redon	k1gNnSc1	Redon
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
;	;	kIx,	;
Meisterwerke	Meisterwerk	k1gInSc2	Meisterwerk
aus	aus	k?	aus
d.	d.	k?	d.
Sammlung	Sammlung	k1gMnSc1	Sammlung
Ian	Ian	k1gMnSc1	Ian
Woodner	Woodner	k1gMnSc1	Woodner
<g/>
;	;	kIx,	;
Museum	museum	k1gNnSc1	museum
Villa	Villo	k1gNnSc2	Villo
Stuck	Stucka	k1gFnPc2	Stucka
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
März	März	k1gInSc1	März
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Juni	Juni	k6eAd1	Juni
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
výstavní	výstavní	k2eAgInSc4d1	výstavní
katalog	katalog	k1gInSc4	katalog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-88219-375-1	[number]	k4	3-88219-375-1
</s>
</p>
<p>
<s>
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
H.	H.	kA	H.
Cyriak	Cyriak	k1gInSc1	Cyriak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Redon	Redon	k1gInSc1	Redon
(	(	kIx(	(
<g/>
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
kresbami	kresba	k1gFnPc7	kresba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
Roger-Marx	Roger-Marx	k1gInSc1	Roger-Marx
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Redon	Redon	k1gInSc1	Redon
čili	čili	k8xC	čili
práva	práv	k2eAgFnSc1d1	práva
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Tribuna	tribuna	k1gFnSc1	tribuna
II	II	kA	II
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
zf	zf	k?	zf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Básník	básník	k1gMnSc1	básník
snů	sen	k1gInPc2	sen
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
tvorba	tvorba	k1gFnSc1	tvorba
III	III	kA	III
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
č.	č.	k?	č.
38	[number]	k4	38
</s>
</p>
<p>
<s>
Vialla	Vialla	k1gMnSc1	Vialla
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redon	k1gInSc1	Redon
-	-	kIx~	-
Sa	Sa	k1gFnSc1	Sa
vie	vie	k?	vie
<g/>
,	,	kIx,	,
son	son	k1gInSc1	son
oeuvre	oeuvr	k1gInSc5	oeuvr
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ACR	ACR	kA	ACR
Edition	Edition	k1gInSc1	Edition
Internationale	Internationale	k1gFnSc1	Internationale
<g/>
,	,	kIx,	,
Courbevoie	Courbevoie	k1gFnSc1	Courbevoie
<g/>
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
192	[number]	k4	192
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redon	k1gInSc4	Redon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Obluda	obluda	k1gFnSc1	obluda
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redona	k1gFnPc2	Redona
</s>
</p>
<p>
<s>
Artencyklopedia	Artencyklopedium	k1gNnPc4	Artencyklopedium
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
complete	complést	k5eAaPmIp3nS	complést
works	works	k6eAd1	works
</s>
</p>
<p>
<s>
Webmuseum	Webmuseum	k1gNnSc1	Webmuseum
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
</s>
</p>
